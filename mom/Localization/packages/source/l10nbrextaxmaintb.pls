create or replace PACKAGE BODY L10N_BR_EXTAX_MAINT_SQL AS
--------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
--------------------------------------------------------
   FUNCTION EXPLODE_TAX_ATTRIBS(O_error_message    IN OUT VARCHAR2,
                                I_process_size     IN     NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION SPLIT_BY_PROCESS_SIZE(O_error_message  IN OUT VARCHAR2,
                                  I_process_size   IN     NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION SETUP_TAX_CALL_ROUTING_RETAIL(O_error_message    IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION MERGE_RESULTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION RESULT_TO_IL(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION RECLASS_ITEM_RESULT_TO_IL(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION FISCAL_ITEM_RECLASS_UPDATE(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION RESULT_TO_ISCL(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION UPD_ITEM_SUPP_COUNTRY_LOC(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION UPD_ITEM_SUPP_COUNTRY(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION INS_ITEM_COST_HEAD(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION INS_ITEM_COST_DETAIL(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION UPD_FUTURE_COST_CURRENT(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   --------------------------------------------------------
   --------------------------------------------------------
   FUNCTION EXPLODE_TAX_ATTRIBS_COST(O_error_message    IN OUT VARCHAR2,
                                     I_process_size     IN     NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION SETUP_TAX_CALL_ROUTING_COST(O_error_message    IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION CREATE_COST_CHANGES(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
$end
--------------------------------------------------------
FUNCTION NVP_FISCAL_SETUP(O_error_message      IN OUT VARCHAR2 )
RETURN BOOLEAN IS

   L_program          VARCHAR2(50):= 'NVP_FISCAL_SETUP';

BEGIN

   merge into l10n_br_extax_help_nv_pair target
   using (select frnv.name,
                 frnv.old_value,
                 frnv.new_value,
                 fr.item
            from l10n_br_fiscal_reclass_nv  frnv,
                 l10n_br_fiscal_reclass fr
           where frnv.reclassification_id = fr.reclassification_id) use
      on (    target.entity_type      = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
          and use.name                = target.name
          and use.item                = target.entity)
    when matched then
    update
       set target.value = use.new_value;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END NVP_FISCAL_SETUP;
--------------------------------------------------------
FUNCTION NVP_SETUP(O_error_message      IN OUT VARCHAR2,
                   I_fiscal_reclass_ind IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(50):= 'NVP_SETUP';

   L_col_names        NVP_HELPER_TBL;
   L_view             VARCHAR2(30);
   L_entity_column    VARCHAR2(30);
   L_name             L10N_BR_EXTAX_HELP_NV_PAIR.NAME%TYPE;
   L_value            L10N_BR_EXTAX_HELP_NV_PAIR.VALUE%TYPE;
   L_sql              VARCHAR2(2000) := ' ';
   L_where_sql        VARCHAR2(2000) := ' ';
   L_hh_sql           VARCHAR2(2000) := ' ';

   cursor C_GET_ATTR_NAMES is
   select a.view_col_name,
          L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
     from l10n_attrib a,
          l10n_attrib_group g,
          ext_entity e
    where a.group_id        = g.group_id
      and g.ext_entity_id   = e.ext_entity_id
      and e.base_rms_table  = 'SUPS'
      and g.base_ind        = 'N'
   union all
   select a.view_col_name,
          L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY
     from l10n_attrib a,
          l10n_attrib_group g,
          ext_entity e
    where a.group_id        = g.group_id
      and g.ext_entity_id   = e.ext_entity_id
      and e.base_rms_table  = 'WH'
      and g.base_ind        = 'N'
   union all
   select a.view_col_name,
          L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY
     from l10n_attrib a,
          l10n_attrib_group g,
          ext_entity e
    where a.group_id        = g.group_id
      and g.ext_entity_id   = e.ext_entity_id
      and e.base_rms_table  = 'STORE'
      and g.base_ind        = 'N'
   union all
   select a.view_col_name,
          L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
     from l10n_attrib a,
          l10n_attrib_group g,
          ext_entity e
    where a.group_id        = g.group_id
      and g.ext_entity_id   = e.ext_entity_id
      and e.base_rms_table  = 'ITEM_COUNTRY'
      and g.base_ind        = 'N'
   union all
   select a.view_col_name,
          L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY
     from l10n_attrib a,
          l10n_attrib_group g,
          ext_entity e
    where a.group_id        = g.group_id
      and g.ext_entity_id   = e.ext_entity_id
      and e.base_rms_table  = 'PARTNER'
      and g.base_ind        = 'N';

BEGIN

   delete from l10n_br_extax_help_nv_pair;

   open C_GET_ATTR_NAMES;
   fetch C_GET_ATTR_NAMES bulk collect into L_col_names;

   if L_col_names.count <1 then
      return TRUE;
   end if; 
   close C_GET_ATTR_NAMES;

   for i in L_col_names.first .. L_col_names.last LOOP

      if L_col_names(i).entity_type = L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY then
         L_view := 'v_br_sups';
         L_entity_column := 'supplier';
      elsif L_col_names(i).entity_type = L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY then
         L_view := 'v_br_partner';
         L_entity_column := 'partner_id';
      elsif L_col_names(i).entity_type = L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY then
         L_view := 'v_br_wh';
         L_entity_column := 'wh';
      elsif L_col_names(i).entity_type = L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY then
         L_view := 'v_br_store';
         L_entity_column := 'store';
      elsif L_col_names(i).entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY then
         L_view := 'v_br_item_country_attributes';
         L_entity_column := 'item';
      end if;

      L_hh_sql := NULL;
      L_hh_sql := 'insert into l10n_br_extax_help_nv_pair '||
                  '(name_value_pair_id, entity, entity_type, name, value) '||
                  '(select null,'
                           ||L_entity_column||','
                           ||''''||L_col_names(i).entity_type||''''||', '
                           ||''''||L_col_names(i).column_name||''''||','
                           ||L_col_names(i).column_name||
                   '  from '||L_view||
                   ' where '||L_col_names(i).column_name||' is not NULL)';
      execute immediate L_hh_sql;
   end LOOP;

   merge into l10n_br_extax_help_nv_pair target
   using (select entity,
                 entity_type,
                 name,
                 value,
                 dense_rank() over (order by name, value) as drank
            from l10n_br_extax_help_nv_pair) use
   on (   use.entity          = target.entity
      and use.entity_type     = target.entity_type
      and use.name            = target.name
      and nvl(use.value, -99) = nvl(target.value,-99))
   when matched then
      update set target.name_value_pair_id = use.drank;

   if I_fiscal_reclass_ind = 'Y' then
      if NVP_FISCAL_SETUP(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END NVP_SETUP;
--------------------------------------------------------
FUNCTION EXTAX_LOC_GROUP_SETUP(O_error_message    IN OUT VARCHAR2,
                               I_fiscal_reclass_ind  IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program     VARCHAR2(255)  := 'L10N_BR_EXTAX_MAINT_SQL.EXTAX_LOC_GROUP_SETUP';

   cursor c_loc is
      select inner.entity,
             inner.entity_type,
             inner.loc_type,
             --
             ctj.jurisdiction_code,
             ctj.jurisdiction_desc,
             st.state,
             st.description,
             ct.country_id,
             ct.country_desc,
             --
             inner.cnpj, inner.cnae_code, inner.iss_contrib_ind, inner.ipi_ind, inner.icms_contrib_ind,
             inner.st_contrib_ind, inner.rural_prod_ind, inner.simples_ind,
             inner.is_income_range_eligible, inner.is_distr_a_manufacturer, inner.icms_simples_rate, inner.tax_regime
        from (select to_char(v.store) entity,
                     NULL physical_wh,
                     L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY entity_type,
                     'S' loc_type,
                     v.cnpj,
                     ex.cnae_code,
                     v.iss_contrib_ind,
                     v.ipi_ind,
                     v.icms_contrib_ind,
                     null st_contrib_ind,
                     null rural_prod_ind,
                     null simples_ind,
                     null is_income_range_eligible,
                     null is_distr_a_manufacturer,
                     null icms_simples_rate,
                     null tax_regime
                from v_br_store v,
                     l10n_br_entity_cnae_codes ex
               where to_char(v.store) = ex.key_value_1
                 and ex.module = 'LOC'
                 and 'S' = ex.key_value_2
           union all
              select to_char(v.partner_id) entity,
                     NULL physical_wh,
                     L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY entity_type,
                     'E' loc_type,
                     v.cnpj,
                     ex.cnae_code,
                     null iss_contrib_ind,
                     v.ipi_ind,
                     v.icms_contrib_ind,
                     null st_contrib_ind,
                     null rural_prod_ind,
                     null simples_ind,
                     null is_income_range_eligible,
                     null is_distr_a_manufacturer,
                     null icms_simples_rate,
                     null tax_regime
                from v_br_partner v,
                     l10n_br_entity_cnae_codes ex
               where v.partner_id = ex.key_value_1
                 and 'E' = ex.key_value_2
                 and ex.module = 'PTNR'
           union all
              select to_char(w.wh) entity,
                     to_char(w.physical_wh) physical_wh,
                     L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY entity_type,
                     'W' loc_type,
                     v.cnpj,
                     ex.cnae_code,
                     v.iss_contrib_ind,
                     v.ipi_ind,
                     v.icms_contrib_ind,
                     null st_contrib_ind,
                     null rural_prod_ind,
                     null simples_ind,
                     null is_income_range_eligible,
                     null is_distr_a_manufacturer,
                     null icms_simples_rate,
                     null tax_regime
                from v_br_wh v,
                     wh w,
                     l10n_br_entity_cnae_codes ex
               where to_char(w.physical_wh) = ex.key_value_1
                 and w.wh = v.wh
                 and w.physical_wh != w.wh
                 and 'W' = ex.key_value_2
                 and ex.module = 'LOC'
           union all
              select distinct to_char(v.supplier) entity,
                     NULL physical_wh,
                     L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY entity_type,
                     'SUPP' loc_type,
                     v.cnpj,
                     ex.cnae_code,
                     v.iss_contrib_ind,
                     v.ipi_ind,
                     v.icms_contrib_ind,
                     v.st_contrib_ind,
                     v.rural_prod_ind,
                     v.simples_ind,
                     v.is_income_range_eligible,
                     v.is_distr_a_manufacturer,
                     v.icms_simples_rate,
                     tr.tax_regime
                from v_br_sups v,
                     l10n_br_entity_cnae_codes ex,
                     l10n_br_sup_tax_regime tr
               where to_char(v.supplier) = ex.key_value_1
                 and 'S' = ex.key_value_2
                 and ex.module = 'SUPP'
                 and v.supplier = tr.supplier(+)
               order by entity, entity_type, cnae_code) inner,
             addr,
             country_tax_jurisdiction ctj,
             country ct,
             state st
       where addr.key_value_1       = decode(inner.entity_type,L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,physical_wh,inner.entity)
         and addr.addr_type         = 01
         and addr.primary_addr_ind  = 'Y'
         and addr.module            = inner.entity_type
         --
         and addr.country_id        = ct.country_id
         and addr.country_id        = 'BR'
         --
         and addr.country_id        = st.country_id
         and addr.state             = st.state
         --
         and addr.state             = ctj.state
         and addr.country_id        = ctj.country_id
         and addr.jurisdiction_code = ctj.jurisdiction_code
    order by inner.entity,
             inner.entity_type;

   L_prev_entity               varchar2(10) := null;
   L_prev_entity_type          varchar2(4)  := null;
   L_loc_type                  varchar2(4);

   L_city                      varchar2(10 byte);
   L_city_desc                 varchar2(120 byte);
   L_state                     varchar2(3 byte);
   L_state_desc                varchar2(120 byte);
   L_country_id                varchar2(3 byte);
   L_country_desc              varchar2(120 byte);

   L_cnpj                      varchar2(255);
   L_rural_prod_ind            varchar2(255);
   L_iss_contrib_ind           varchar2(255);
   L_simples_ind               varchar2(255);
   L_ipi_ind                   varchar2(255);
   L_icms_contrib_ind          varchar2(255);
   L_st_contrib_ind            varchar2(255);
   L_cnae_code                 varchar2(255);

   L_is_income_range_eligible varchar2(255);
   L_is_distr_a_manufacturer  varchar2(255);
   L_icms_simples_rate         varchar2(255);

   L_cnae_code_concatenation   varchar2(255);
   L_tax_regime_concatenation  varchar2(500);
   L_name_value_pair_id_concat varchar2(255);

   cursor c_tax_regime is
      select tax_regime 
        from l10n_br_sup_tax_regime
       where supplier = L_prev_entity;

   cursor c_name_value is
      select name_value_pair_id
        from l10n_br_extax_help_nv_pair
       where entity      = L_prev_entity 
         and entity_type = L_prev_entity_type;

BEGIN

   if NVP_SETUP(O_error_message,
                I_fiscal_reclass_ind) = FALSE then
      return FALSE;
   end if;

   delete from l10n_br_extax_dest_group_help;

   for rec in c_loc loop

      if L_prev_entity is not null AND (rec.entity != L_prev_entity OR rec.entity_type != L_prev_entity_type) then

         L_tax_regime_concatenation := null;
         if L_prev_entity_type = L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY then
            for tr in c_tax_regime loop
               if L_tax_regime_concatenation is null then
                  L_tax_regime_concatenation := tr.tax_regime;
               else
                  L_tax_regime_concatenation := L_tax_regime_concatenation||'~'||tr.tax_regime;
               end if;
            end loop;
         end if;

         L_name_value_pair_id_concat := null;
         for nv in c_name_value loop
            if L_name_value_pair_id_concat is null then
               L_name_value_pair_id_concat := nv.name_value_pair_id;
            else
               L_name_value_pair_id_concat := L_name_value_pair_id_concat||'~'||nv.name_value_pair_id;
            end if;
         end loop;

         insert into l10n_br_extax_dest_group_help
                     (entity, entity_type, loc_type,
                      city, city_desc, state, state_desc, country_id, country_desc,
                      cnpj, rural_prod_ind, iss_contrib_ind,
                      ipi_ind, icms_contrib_ind, st_contrib_ind, cnae_code_concatenation,
                      cnae_code_group_id,simples_ind,is_income_range_eligible, is_distr_a_manufacturer,
                      icms_simples_rate, tax_regime_concat, name_value_pair_id_concat)
         values(L_prev_entity, L_prev_entity_type, L_loc_type,
                L_city, L_city_desc, L_state, L_state_desc, L_country_id, L_country_desc,
                L_cnpj, L_rural_prod_ind, L_iss_contrib_ind,
                L_ipi_ind, L_icms_contrib_ind, L_st_contrib_ind, L_cnae_code_concatenation, null,L_simples_ind,
                L_is_income_range_eligible, L_is_distr_a_manufacturer, L_icms_simples_rate,
                L_tax_regime_concatenation, L_name_value_pair_id_concat);

         L_prev_entity               := rec.entity;
         L_prev_entity_type          := rec.entity_type;
         L_loc_type                  := rec.loc_type;

         L_city                      := rec.jurisdiction_code;
         L_city_desc                 := rec.jurisdiction_desc;
         L_state                     := rec.state;
         L_state_desc                := rec.description;
         L_country_id                := rec.country_id;
         L_country_desc              := rec.country_desc;

         L_cnpj                      := rec.cnpj;
         L_rural_prod_ind            := rec.rural_prod_ind;
         L_simples_ind               := rec.simples_ind;
         L_iss_contrib_ind           := rec.iss_contrib_ind;
         L_ipi_ind                   := rec.ipi_ind;
         L_icms_contrib_ind          := rec.icms_contrib_ind;
         L_st_contrib_ind            := rec.st_contrib_ind;
         L_cnae_code_concatenation   := rec.cnae_code;

         L_is_income_range_eligible := rec.is_income_range_eligible;
         L_is_distr_a_manufacturer  := rec.is_distr_a_manufacturer;
         L_icms_simples_rate        := rec.icms_simples_rate;

      else

         if L_prev_entity is null then
            L_cnae_code_concatenation   := rec.cnae_code;
         else
            L_cnae_code_concatenation   := L_cnae_code_concatenation||'~'||rec.cnae_code;
         end if;

         L_prev_entity               := rec.entity;
         L_prev_entity_type          := rec.entity_type;
         L_loc_type                  := rec.loc_type;

         L_city                      := rec.jurisdiction_code;
         L_city_desc                 := rec.jurisdiction_desc;
         L_state                     := rec.state;
         L_state_desc                := rec.description;
         L_country_id                := rec.country_id;
         L_country_desc              := rec.country_desc;

         L_cnpj                      := rec.cnpj;
         L_rural_prod_ind            := rec.rural_prod_ind;
         L_simples_ind               := rec.simples_ind;
         L_iss_contrib_ind           := rec.iss_contrib_ind;
         L_ipi_ind                   := rec.ipi_ind;
         L_icms_contrib_ind          := rec.icms_contrib_ind;
         L_st_contrib_ind            := rec.st_contrib_ind;

         L_is_income_range_eligible := rec.is_income_range_eligible;
         L_is_distr_a_manufacturer  := rec.is_distr_a_manufacturer;
         L_icms_simples_rate        := rec.icms_simples_rate;

      end if;

   end loop;

   if L_prev_entity is not null then

      insert into l10n_br_extax_dest_group_help
                  (entity, entity_type, loc_type,
                   city, city_desc, state, state_desc, country_id, country_desc,
                   cnpj, rural_prod_ind, iss_contrib_ind,
                   ipi_ind, icms_contrib_ind, st_contrib_ind, cnae_code_concatenation,
                   cnae_code_group_id,simples_ind, is_income_range_eligible, is_distr_a_manufacturer,
                   icms_simples_rate, tax_regime_concat, name_value_pair_id_concat)
      values(L_prev_entity, L_prev_entity_type, L_loc_type,
             L_city, L_city_desc, L_state, L_state_desc, L_country_id, L_country_desc,
             L_cnpj, L_rural_prod_ind, L_iss_contrib_ind,
             L_ipi_ind, L_icms_contrib_ind, L_st_contrib_ind, L_cnae_code_concatenation, null,L_simples_ind,
             L_is_income_range_eligible, L_is_distr_a_manufacturer, L_icms_simples_rate,
             L_tax_regime_concatenation, L_name_value_pair_id_concat);

   end if;

   merge into l10n_br_extax_dest_group_help target using(
      select entity,
             entity_type,
             dense_rank() over (order by city,
                                         state,
                                         country_id,
                                         rural_prod_ind,
                                         iss_contrib_ind,
                                         simples_ind,
                                         ipi_ind,
                                         icms_contrib_ind,
                                         st_contrib_ind,
                                         cnae_code_concatenation,
                                         is_income_range_eligible,
                                         is_distr_a_manufacturer,
                                         icms_simples_rate,
                                         tax_regime_concat,
                                         name_value_pair_id_concat,
                                         cnae_code_concatenation) cnae_code_group_id
        from l10n_br_extax_dest_group_help) use_this
   on (target.entity = use_this.entity and
       target.entity_type = use_this.entity_type)
   when matched then
   update set target.cnae_code_group_id = use_this.cnae_code_group_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END EXTAX_LOC_GROUP_SETUP;
--------------------------------------------------------
FUNCTION SEED_EXTAX_SETUP(O_error_message    IN OUT VARCHAR2,
                          I_process_size     IN     NUMBER)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SEED_EXTAX_SETUP';
   L_vdate       DATE := get_vdate();

BEGIN

   delete from l10n_br_extax_stg_retail;
   delete from l10n_br_extax_res_retail;
   delete from l10n_br_extax_res_retail_det;

   if EXTAX_LOC_GROUP_SETUP(O_error_message) = FALSE then
      return FALSE;
   end if;

   insert into l10n_br_extax_stg_retail (
           city,
           city_desc,
           state,
           state_desc,
           country_id,
           country_desc,
           cnae_code,
           iss,
           ipi,
           icms,
           cnpj,
           loc_name_value_pair_id_concat,
           origin_code,
           service_ind,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           effective_date,
           pack_no,
           item,
           process_id,
           item_ref_id,
           loc_ref_id,
           item_xform_ind)
   select inner2.city,
          inner2.city_desc,
          inner2.state,
          inner2.state_desc,
          inner2.country_id,
          inner2.country_desc,
          inner2.cnae_code,
          inner2.iss,
          inner2.ipi,
          inner2.icms,
          inner2.cnpj,
          inner2.loc_name_value_pair_id_concat,
          inner2.origin_code,
          inner2.service_ind,
          inner2.classification_id,
          inner2.ncm_char_code,
          inner2.ex_ipi,
          inner2.pauta_code,
          inner2.service_code,
          inner2.federal_service,
          inner2.state_of_manufacture,
          inner2.pharma_list_type,
          inner2.item_name_value_pair_id_concat,
          inner2.dim_object,
          inner2.length,
          inner2.lwh_uom,
          inner2.weight,
          inner2.net_weight,
          inner2.weight_uom,
          inner2.liquid_volume,
          inner2.liquid_volume_uom,
          inner2.effective_date,
          inner2.pack_no,
          inner2.item,
          --
          decode(row_number() over (partition by inner2.date_svc_split, inner2.data_split
                                        order by inner2.effective_date),
                 1,l10n_br_extax_maint_sql.get_seq('Y'),
                   l10n_br_extax_maint_sql.get_seq('N')),
          --
          inner2.item_ref_id,
          inner2.loc_ref_id,
          inner2.item_xform_ind
     from (
   select inner.city,
          inner.city_desc,
          inner.state,
          inner.state_desc,
          inner.country_id,
          inner.country_desc,
          inner.cnae_code,
          inner.iss,
          inner.ipi,
          inner.icms,
          inner.cnpj,
          inner.loc_name_value_pair_id_concat,
          inner.origin_code,
          inner.service_ind,
          inner.classification_id,
          inner.ncm_char_code,
          inner.ex_ipi,
          inner.pauta_code,
          inner.service_code,
          inner.federal_service,
          inner.state_of_manufacture,
          inner.pharma_list_type,
          inner.item_name_value_pair_id_concat,
          inner.dim_object,
          inner.length,
          inner.lwh_uom,
          inner.weight,
          inner.net_weight,
          inner.weight_uom,
          inner.liquid_volume,
          inner.liquid_volume_uom,
          inner.effective_date,
          inner.pack_no,
          inner.item,
          inner.item_xform_ind,
          dense_rank() over (order by inner.service_ind,
                                      inner.ncm_char_code,
                                      inner.pauta_code,
                                      decode(inner.service_ind,'N',inner.classification_id,null),
                                      decode(inner.service_ind,'N',inner.ex_ipi,null),
                                      decode(inner.service_ind,'N',inner.state_of_manufacture,null),
                                      decode(inner.service_ind,'N',inner.pharma_list_type,null),
                                      decode(inner.service_ind,'N',inner.item_xform_ind,null),
                                      decode(inner.service_ind,'Y',inner.service_code,null),
                                      decode(inner.service_ind,'Y',inner.federal_service,null),
                                      inner.item_name_value_pair_id_concat,
                                      inner.dim_object,
                                      inner.length,
                                      inner.lwh_uom,
                                      inner.weight,
                                      inner.net_weight,
                                      inner.weight_uom,
                                      inner.liquid_volume,
                                      inner.liquid_volume_uom,
                                      inner.pack_no,
                                      inner.item) item_ref_id,
          dense_rank() over (order by inner.city,
                                      inner.state,
                                      inner.country_id,
                                      inner.cnae_code,
                                      inner.iss,
                                      inner.ipi,
                                      inner.icms,
                                      inner.cnpj,
                                      inner.loc_name_value_pair_id_concat) loc_ref_id,
          dense_rank() over (order by inner.effective_date) date_svc_split,
          ceil(row_number() over (partition by inner.effective_date
                            order by inner.city,
                                     inner.state,
                                     inner.country_id,
                                     inner.cnae_code,
                                     inner.iss,
                                     inner.ipi,
                                     inner.icms,
                                     inner.cnpj,
                                     inner.loc_name_value_pair_id_concat,
                                     --
                                     inner.service_ind,
                                     inner.ncm_char_code,
                                     inner.pauta_code,
                                     decode(inner.service_ind,'N',inner.classification_id,null),
                                     decode(inner.service_ind,'N',inner.ex_ipi,null),
                                     decode(inner.service_ind,'N',inner.state_of_manufacture,null),
                                     decode(inner.service_ind,'N',inner.pharma_list_type,null),
                                     decode(inner.service_ind,'N',inner.item_xform_ind,null),
                                     decode(inner.service_ind,'Y',inner.service_code,null),
                                     decode(inner.service_ind,'Y',inner.federal_service,null),
                                     inner.item_name_value_pair_id_concat,
                                     inner.dim_object,
                                     inner.length,
                                     inner.lwh_uom,
                                     inner.weight,
                                     inner.net_weight,
                                     inner.weight_uom,
                                     inner.liquid_volume,
                                     inner.liquid_volume_uom,
                                     inner.pack_no,
                                     inner.item ) / I_process_size) + 0 data_split
     from 
      --PACK
     (with dim as
      (select isc.item,
              iscd.dim_object,
              iscd.length,
              iscd.lwh_uom,
              iscd.weight,
              iscd.net_weight,
              iscd.weight_uom,
              iscd.liquid_volume,
              iscd.liquid_volume_uom
        from item_supp_country isc,
             item_supp_country_dim iscd
       where iscd.dim_object  = 'EA'
         and isc.item = iscd.item
         and isc.supplier = iscd.supplier
         and isc.origin_country_id = iscd.origin_country
         and isc.primary_supp_ind = 'Y'
         and isc.primary_country_ind = 'Y'),
      item_stg as
      (select pi.pack_no,
              pi.item,
              im.item_xform_ind,
              nvl(d.dim_object, '-999') dim_object,
              nvl(d.length,'-999') length,
              nvl(d.lwh_uom,'-999') lwh_uom,
              nvl(d.weight,'-999') weight,
              nvl2(d.net_weight, d.net_weight * qty, '-999') net_weight,
              nvl(d.weight_uom,'-999') weight_uom,
              nvl2(d.liquid_volume, d.liquid_volume * qty, '-999') liquid_volume,
              nvl(d.liquid_volume_uom,'-999') liquid_volume_uom
         from item_master im,
              v_packsku_qty pi,
              dim d
        where pi.pack_no = im.item
          and im.status              = 'A'
          and im.tran_level          = im.item_level
          and im.pack_ind            = 'Y'
          and im.simple_pack_ind     = 'N'
          and im.sellable_ind        = 'Y'
          and pi.item                = d.item(+)
      union all
       select pi.pack_no,
              pi.item,
              im.item_xform_ind,
              nvl(d.dim_object, '-999') dim_object,
              nvl(d.length,'-999') length,
              nvl(d.lwh_uom,'-999') lwh_uom,
              nvl(d.weight,'-999') weight,
              nvl(d.net_weight, '-999') net_weight,
              nvl(d.weight_uom,'-999') weight_uom,
              nvl(d.liquid_volume,  '-999') liquid_volume,
              nvl(d.liquid_volume_uom,'-999') liquid_volume_uom
         from item_master im,
              v_packsku_qty pi,
              dim d
        where pi.pack_no = im.item
          and im.status              = 'A'
          and im.tran_level          = im.item_level
          and im.pack_ind            = 'Y'
          and im.simple_pack_ind     = 'Y'
          and im.sellable_ind        = 'Y'
          and im.item                = d.item(+))
      select /*+ use_hash(il) index_ffs(il,pk_item_loc) */ distinct
             loc.city,
             loc.city_desc,
             loc.state,
             loc.state_desc,
             loc.country_id,
             loc.country_desc,
             loc.cnae_code_concatenation cnae_code,
             nvl(loc.iss_contrib_ind,'N') iss,
             nvl(loc.ipi_ind,'N') ipi,
             nvl(loc.icms_contrib_ind,'N') icms,
             loc.cnpj,
             nvl(loc.name_value_pair_id_concat,'-1') loc_name_value_pair_id_concat,
             --
             '0' origin_code,
             nvl(vbi.service_ind,'N') service_ind,
             nvl(vbi.classification_id,'-1') classification_id,
             nvl(vbi.ncm_char_code,'-1') ncm_char_code,
             nvl(vbi.ex_ipi,'-1') ex_ipi,
             nvl(vbi.pauta_code,'-1') pauta_code,
             nvl(vbi.service_code,'-1') service_code,
             nvl(vbi.federal_service,'-1') federal_service,
             nvl(vbi.state_of_manufacture,'-1') state_of_manufacture,
             nvl(vbi.pharma_list_type,'-1') pharma_list_type,
             nvl(nv.item_name_value_pair_id_concat,'-1') item_name_value_pair_id_concat,
             item_dim.dim_object,
             item_dim.length,
             item_dim.lwh_uom,
             item_dim.weight,
             item_dim.net_weight,
             item_dim.weight_uom,
             item_dim.liquid_volume,
             item_dim.liquid_volume_uom,
             L_vdate effective_date,
             item_dim.pack_no,
             item_dim.item,
             0 item_ref_id,
             0 loc_ref_id,
             item_dim.item_xform_ind
        from item_stg item_dim,
             l10n_br_extax_dest_group_help loc,
             item_loc il,
             v_br_item_fiscal_attrib vbi,
             (select entity item,
                     listagg (name_value_pair_id, '~')
                     within group (order by name_value_pair_id) item_name_value_pair_id_concat
                from l10n_br_extax_help_nv_pair
               where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
               group by entity) nv
       where item_dim.item          = vbi.item
         and vbi.country_id         = loc.country_id
         and vbi.origin_code        = '0'
         --
         and il.item                = item_dim.pack_no
         and loc.entity              = to_char(il.loc)
         and il.loc                  = loc.entity
         and loc.city                     is not null
         and loc.cnae_code_concatenation  is not null
         --
         and vbi.item               = nv.item(+)
      UNION ALL
      --NON-PACK
      select /*+ use_hash(il) index_ffs(il,pk_item_loc) */  distinct
             loc.city,
             loc.city_desc,
             loc.state,
             loc.state_desc,
             loc.country_id,
             loc.country_desc,
             loc.cnae_code_concatenation cnae_code,
             nvl(loc.iss_contrib_ind,'N') iss,
             nvl(loc.ipi_ind,'N') ipi,
             nvl(loc.icms_contrib_ind,'N') icms,
             loc.cnpj,
             nvl(loc.name_value_pair_id_concat,'-1') loc_name_value_pair_id_concat,
             --
             '0' origin_code,
             nvl(vbi.service_ind,'N') service_ind,
             nvl(vbi.classification_id,'-1') classification_id,
             nvl(vbi.ncm_char_code,'-1') ncm_char_code,
             nvl(vbi.ex_ipi,'-1') ex_ipi,
             nvl(vbi.pauta_code,'-1') pauta_code,
             nvl(vbi.service_code,'-1') service_code,
             nvl(vbi.federal_service,'-1') federal_service,
             nvl(vbi.state_of_manufacture,'-1') state_of_manufacture,
             nvl(vbi.pharma_list_type,'-1') pharma_list_type,
             nvl(nv.item_name_value_pair_id_concat,'-1') item_name_value_pair_id_concat,
             nvl(dim.dim_object, '-999') dim_object,
             nvl(dim.length,'-999') length,
             nvl(dim.lwh_uom,'-999') lwh_uom,
             nvl(dim.weight,'-999') weight,
             nvl(dim.net_weight,'-999') net_weight,
             nvl(dim.weight_uom,'-999') weight_uom,
             nvl(dim.liquid_volume,'-999') liquid_volume,
             nvl(dim.liquid_volume_uom,'-999') liquid_volume_uom,
             L_vdate effective_date,
             '-1' pack_no,
             '-1' item,
             0 item_ref_id,
             0 loc_ref_id,
             im.item_xform_ind
        from (select isc.item,
                     iscd.dim_object,
                     iscd.length,
                     iscd.lwh_uom,
                     iscd.weight,
                     iscd.net_weight,
                     iscd.weight_uom,
                     iscd.liquid_volume,
                     iscd.liquid_volume_uom
                from item_supp_country isc,
                     item_supp_country_dim iscd
               where iscd.dim_object  = 'EA'
                 and isc.item = iscd.item
                 and isc.supplier = iscd.supplier
                 and isc.origin_country_id = iscd.origin_country
                 and isc.primary_supp_ind = 'Y'
                 and isc.primary_country_ind = 'Y' ) dim,
             l10n_br_extax_dest_group_help loc,
             item_loc il,
             item_master im,
             v_br_item_fiscal_attrib vbi,
             (select entity item,
                     listagg (name_value_pair_id, '~')
                     within group (order by name_value_pair_id) item_name_value_pair_id_concat
                from l10n_br_extax_help_nv_pair
               where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
               group by entity) nv
       where il.item                = vbi.item
         and vbi.country_id         = loc.country_id
         and vbi.origin_code        = '0'
         --
         and loc.entity              = to_char(il.loc)
         and il.loc                  = loc.entity
         and loc.city                     is not null
         and loc.cnae_code_concatenation  is not null
         --
         and il.item                = im.item
         and im.status              = 'A'
         and im.tran_level          = im.item_level
         and im.pack_ind            = 'N'
         and im.sellable_ind        = 'Y'
         --
         and dim.item  (+)          = im.item
         --
         and vbi.item               = nv.item(+)
      ) inner) inner2;

   if SETUP_TAX_CALL_ROUTING_RETAIL(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END SEED_EXTAX_SETUP;
--------------------------------------------------------
FUNCTION SEED_EXTAX_FUTURE_SETUP(O_error_message    IN OUT VARCHAR2,
                                 I_process_size     IN     NUMBER)
RETURN BOOLEAN IS

   L_program         VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SEED_EXTAX_FUTURE_SETUP';

   L_count           NUMBER        := NULL;
   L_size            NUMBER        := TAX_PROCESSING_SIZE;
   L_start           NUMBER        := null;
   L_end             NUMBER        := null;

   L_tax_service_id  l10n_br_tax_call_stage_fsc_fdn.tax_service_id%TYPE := null;

   cursor C_GET_COUNT is
      select total_fnd_count
        from l10n_br_tax_call_res_fsc_count
       where tax_service_id = L_tax_service_id;

BEGIN

   delete from l10n_br_extax_refresh_retail;
   delete from l10n_br_extax_stg_retail;
   delete from l10n_br_extax_res_retail;
   delete from l10n_br_extax_res_retail_det;

   --

   if L10N_BR_FISCAL_FDN_QUERY_SQL.EXTAX_MAINT_QUERY_FISCAL_CODE(O_error_message,
                                                                 L_tax_service_id,
                                                                 'FUTURECHANGECOUNT',
                                                                 sysdate,
                                                                 null,
                                                                 null) = FALSE then
      return FALSE;
   end if;

   open C_GET_COUNT;
   fetch C_GET_COUNT into L_count;
   close C_GET_COUNT;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   while L_count > 0 loop

      L_end := L_count;
      L_start := L_count - L_size;
      if L_start < 1 then
         L_start := 1;
      end if;
      L_count := L_count - L_size - 1;

      if L10N_BR_FISCAL_FDN_QUERY_SQL.EXTAX_MAINT_QUERY_FISCAL_CODE(O_error_message,
                                                                    L_tax_service_id,
                                                                    'FUTURECHANGE',
                                                                    sysdate,
                                                                    L_start,
                                                                    L_end) = FALSE then
         return FALSE;
      end if;

      insert into l10n_br_extax_refresh_gtt (create_date,
                                             effective_date,
                                             classification_code,
                                             ncm_char_code,
                                             pauta_code,
                                             source_state,
                                             destination_state,
                                             reason)
         select creation_date,
                effective_date,
                nvl(fiscal_extended_parent_code,'-1'),
                nvl(fiscal_parent_code,'-1'),
                nvl(fiscal_code,'-1'),
                origin_state,
                destination_state,
                fiscal_code_description
           from l10n_br_tax_call_res_fsc_fnd
          where tax_service_id = L_tax_service_id;

      if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                   L_tax_service_id) = FALSE then
         return FALSE;
      end if;

   end loop;

   if EXPLODE_TAX_ATTRIBS(O_error_message,
                          I_process_size) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END SEED_EXTAX_FUTURE_SETUP;
--------------------------------------------------------
FUNCTION REFRESH_EXTAX_SETUP(O_error_message    IN OUT VARCHAR2,
                             I_process_size     IN     NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.REFRESH_EXTAX_SETUP';
   L_tax_service_id  l10n_br_tax_call_stage_fsc_fdn.tax_service_id%TYPE := null;
   L_refresh_date    DATE          := NULL;

   L_count           NUMBER        := NULL;
   L_size            NUMBER        := TAX_PROCESSING_SIZE;
   L_start           NUMBER        := null;
   L_end             NUMBER        := null;

   cursor C_GET_REFRESH_DATE is
     select nvl(max(last_effective_after_this_date),sysdate)
       from l10n_br_extax_refresh_config
      where refresh_needed = 'Y';

   cursor C_GET_COUNT is
      select total_fnd_count
        from l10n_br_tax_call_res_fsc_count
       where tax_service_id = L_tax_service_id;

BEGIN

   delete from l10n_br_extax_refresh_retail;
   delete from l10n_br_extax_stg_retail;
   delete from l10n_br_extax_res_retail;
   delete from l10n_br_extax_res_retail_det;
   --
   open C_GET_REFRESH_DATE;
   fetch C_GET_REFRESH_DATE into L_refresh_date;
   close C_GET_REFRESH_DATE;

   if L10N_BR_FISCAL_FDN_QUERY_SQL.EXTAX_MAINT_QUERY_FISCAL_CODE(O_error_message,
                                                                 L_tax_service_id,
                                                                 'REFRESHRULECOUNT',
                                                                 L_refresh_date,
                                                                 null,
                                                                 null) = FALSE then
      return FALSE;
   end if;

   open C_GET_COUNT;
   fetch C_GET_COUNT into L_count;
   close C_GET_COUNT;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   while L_count > 0 loop

      L_end := L_count;
      L_start := L_count - L_size;
      if L_start < 1 then
         L_start := 1;
      end if;
      L_count := L_count - L_size - 1;

      if L10N_BR_FISCAL_FDN_QUERY_SQL.EXTAX_MAINT_QUERY_FISCAL_CODE(O_error_message,
                                                                    L_tax_service_id,
                                                                    'REFRESHRULE',
                                                                    L_refresh_date,
                                                                    L_start,
                                                                    L_end) = FALSE then
         return FALSE;
      end if;


      insert into l10n_br_extax_refresh_gtt (create_date,
                                             effective_date,
                                             classification_code,
                                             ncm_char_code,
                                             pauta_code,
                                             source_state,
                                             destination_state,
                                             reason)
         select creation_date,
                nvl(effective_date,sysdate),
                nvl(fiscal_extended_parent_code,'-1'),
                nvl(fiscal_parent_code,'-1'),
                nvl(fiscal_code,'-1'),
                origin_state,
                destination_state,
                fiscal_code_description
           from l10n_br_tax_call_res_fsc_fnd
          where tax_service_id = L_tax_service_id;

      if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                   L_tax_service_id) = FALSE then
         return FALSE;
      end if;

   end loop;

   --

   if EXPLODE_TAX_ATTRIBS(O_error_message,
                          I_process_size) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END REFRESH_EXTAX_SETUP;
--------------------------------------------------------
FUNCTION FISCAL_ITM_RECLASS_EXTAX_SETUP(O_error_message    IN OUT VARCHAR2,
                                        I_process_size     IN     NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.FISCAL_ITM_RECLASS_EXTAX_SETUP';
   L_vdate            DATE := get_vdate();

BEGIN

   delete from l10n_br_extax_stg_retail;
   delete from l10n_br_extax_res_retail;
   delete from l10n_br_extax_res_retail_det;

   if EXTAX_LOC_GROUP_SETUP(O_error_message,
                            'Y') = FALSE then
      return FALSE;
   end if;

   -- For Non Pack
   insert into l10n_br_extax_stg_retail (
          city,
          city_desc,
          state,
          state_desc,
          country_id,
          country_desc,
          cnae_code,
          iss,
          ipi,
          icms,
          cnpj,
          loc_name_value_pair_id_concat,
          origin_code,
          service_ind,
          classification_id,
          ncm_char_code,
          ex_ipi,
          pauta_code,
          service_code,
          federal_service,
          state_of_manufacture,
          pharma_list_type,
          item_name_value_pair_id_concat,
          dim_object,
          length,
          lwh_uom,
          weight,
          net_weight,
          weight_uom,
          liquid_volume,
          liquid_volume_uom,
          item_xform_ind,
          effective_date,
          pack_no,
          item,
          process_id,
          item_ref_id,
          loc_ref_id)
   select distinct
          loc.city,
          loc.city_desc,
          loc.state,
          loc.state_desc,
          loc.country_id,
          loc.country_desc,
          loc.cnae_code_concatenation,
          nvl(loc.iss_contrib_ind,'N'),
          nvl(loc.ipi_ind,'N'),
          nvl(loc.icms_contrib_ind,'N'),
          loc.cnpj,
          nvl(loc.name_value_pair_id_concat,'-1'),
          --
          '0',                  --vbi.origin_code,
          nvl(i.new_service_item_ind,'N'),
          nvl(i.new_ncm_code, '-1'),
          nvl(i.new_ncm_char_code,'-1'),
          nvl(i.new_ex_ipi_code, '-1'),
          nvl(i.new_pauta_code, '-1'),
          nvl(i.new_mtr_service_code,'-1'),
          nvl(i.new_fed_service_code,'-1'),
          nvl(i.new_state_of_manufacture,'-1'),
          nvl(i.new_pharma_list_type,'-1'),
          nvl(nv.item_name_value_pair_id_concat,'-1'),
          nvl(dim.dim_object, '-999'),
          nvl(dim.weight,'-999'),
          nvl(dim.net_weight,'-999'),
          nvl(dim.weight_uom,'-999'),
          nvl(dim.length,'-999'),
          nvl(dim.lwh_uom,'-999'),
          nvl(dim.liquid_volume,'-999'),
          nvl(dim.liquid_volume_uom,'-999'),
          im.item_xform_ind,
          i.active_date,
          '-1',  -- pack no
          '-1',  -- item
          0,                  --process_id,
          0,                  --item_ref_id,
          0                   --loc_ref_id
     from l10n_br_fiscal_reclass i,
          (select isc.item,
                     iscd.dim_object,
                     iscd.length,
                     iscd.lwh_uom,
                     iscd.weight,
                     iscd.net_weight,
                     iscd.weight_uom,
                     iscd.liquid_volume,
                     iscd.liquid_volume_uom
                from item_supp_country isc,
                     item_supp_country_dim iscd,
                     l10n_br_fiscal_reclass f
               where f.item = isc.item
                 and iscd.dim_object  = 'EA'
                 and isc.item = iscd.item
                 and isc.supplier = iscd.supplier
                 and isc.origin_country_id = iscd.origin_country
                 and isc.primary_supp_ind = 'Y'
                 and isc.primary_country_ind = 'Y' ) dim,
          l10n_br_extax_dest_group_help loc,
          item_loc il,
          item_master im,
          (select entity item,
                  listagg (name_value_pair_id, '~') within group (order by name_value_pair_id) item_name_value_pair_id_concat
             from l10n_br_extax_help_nv_pair
            where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
            group by entity) nv
    where i.item                 = im.item
      and i.status               in ('N','S','A')
      and i.active_date          >= L_vdate
      and im.status              = 'A'
      and im.tran_level          = im.item_level
      and im.pack_ind            = 'N'
      --
      and il.item                = i.item
      --
      and il.loc                 = loc.entity
      and loc.entity_type        in(L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                    L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                    L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
      and loc.city                     is not null
      and loc.cnae_code_concatenation  is not null
      --
      and dim.item(+)             = im.item
      and nv.item(+)              = im.item;

   -- For Simple and Complex Pack
   insert into l10n_br_extax_stg_retail (
          city,
          city_desc,
          state,
          state_desc,
          country_id,
          country_desc,
          cnae_code,
          iss,
          ipi,
          icms,
          cnpj,
          loc_name_value_pair_id_concat,
          origin_code,
          service_ind,
          classification_id,
          ncm_char_code,
          ex_ipi,
          pauta_code,
          service_code,
          federal_service,
          state_of_manufacture,
          pharma_list_type,
          item_name_value_pair_id_concat,
          dim_object,
          length,
          lwh_uom,
          weight,
          net_weight,
          weight_uom,
          liquid_volume,
          liquid_volume_uom,
          item_xform_ind,
          effective_date,
          pack_no,
          item,
          process_id,
          item_ref_id,
          loc_ref_id)
      with dim as
      (select isc.item,
              iscd.dim_object,
              iscd.length,
              iscd.lwh_uom,
              iscd.weight,
              iscd.net_weight,
              iscd.weight_uom,
              iscd.liquid_volume,
              iscd.liquid_volume_uom
         from item_supp_country isc,
              item_supp_country_dim iscd,
              l10n_br_fiscal_reclass f
        where f.item                  = isc.item
          and iscd.dim_object         = 'EA'
          and isc.item                = iscd.item
          and isc.supplier            = iscd.supplier
          and isc.origin_country_id   = iscd.origin_country
          and isc.primary_supp_ind    = 'Y'
          and isc.primary_country_ind = 'Y'),
      item_stg as
      (select i.pack_no,
              i.item,
              i.new_service_item_ind,
              i.new_ncm_code,
              i.new_ncm_char_code,
              i.new_ex_ipi_code,
              i.new_pauta_code,
              i.new_mtr_service_code,
              i.new_fed_service_code,
              i.new_state_of_manufacture,
              i.new_pharma_list_type,
              i.active_date,
              i.simple_pack_ind,
              i.item_xform_ind,
              i.dim_object,
              i.length,
              i.lwh_uom,
              i.weight,
              i.net_weight,
              i.weight_uom,
              i.liquid_volume,
              i.liquid_volume_uom,
              max(i.comp_on_reclass) comp_on_reclass
         from (select distinct 
                      v2.pack_no,
                      v2.item,
                      f.new_service_item_ind,
                      f.new_ncm_code,
                      f.new_ncm_char_code,
                      f.new_ex_ipi_code,
                      f.new_pauta_code,
                      f.new_mtr_service_code,
                      f.new_fed_service_code,
                      f.new_state_of_manufacture,
                      f.new_pharma_list_type,
                      f.active_date,
                      im_pack.simple_pack_ind,
                      im_comp.item_xform_ind,
                      nvl(d.dim_object, '-999') dim_object,
                      nvl(d.length,'-999') length,
                      nvl(d.lwh_uom,'-999') lwh_uom,
                      nvl(d.weight,'-999') weight,
                      nvl2(d.net_weight, d.net_weight * v2.qty, '-999') net_weight,
                      nvl(d.weight_uom,'-999') weight_uom,
                      nvl2(d.liquid_volume, d.liquid_volume * v2.qty, '-999') liquid_volume,
                      nvl(d.liquid_volume_uom,'-999') liquid_volume_uom,
                      decode(f.item, v2.item, 1, 0) comp_on_reclass
                 from v_packsku_qty v1,
                      l10n_br_fiscal_reclass f,
                      v_packsku_qty v2,
                      item_master im_pack,
                      item_master im_comp,
                      dim d
                where f.item                  = v1.item
                  and f.status                in ('N','S','A')
                  and f.active_date          >= get_vdate
                  and v1.pack_no              = v2.pack_no
                  and v2.pack_no              = im_pack.item
                  and im_pack.simple_pack_ind = 'N'
                  and v2.item                 = im_comp.item
                  and im_comp.item            = d.item(+)) i
        group by i.pack_no,
                 i.item,
                 i.new_service_item_ind,
                 i.new_ncm_code,
                 i.new_ncm_char_code,
                 i.new_ex_ipi_code,
                 i.new_pauta_code,
                 i.new_mtr_service_code,
                 i.new_fed_service_code,
                 i.new_state_of_manufacture,
                 i.new_pharma_list_type,
                 i.active_date,
                 i.simple_pack_ind,
                 i.item_xform_ind,
                 i.dim_object,
                 i.length,
                 i.lwh_uom,
                 i.weight,
                 i.net_weight,
                 i.weight_uom,
                 i.liquid_volume,
                 i.liquid_volume_uom
       UNION ALL
       select i.pack_no,
              i.item,
              i.new_service_item_ind,
              i.new_ncm_code,
              i.new_ncm_char_code,
              i.new_ex_ipi_code,
              i.new_pauta_code,
              i.new_mtr_service_code,
              i.new_fed_service_code,
              i.new_state_of_manufacture,
              i.new_pharma_list_type,
              i.active_date,
              i.simple_pack_ind,
              i.item_xform_ind,
              i.dim_object,
              i.length,
              i.lwh_uom,
              i.weight,
              i.net_weight,
              i.weight_uom,
              i.liquid_volume,
              i.liquid_volume_uom,
              max(i.comp_on_reclass) comp_on_reclass
         from (select distinct
                      v2.pack_no,
                      v2.item,
                      f.active_date,
                      f.new_service_item_ind,
                      f.new_ncm_code,
                      f.new_ncm_char_code,
                      f.new_ex_ipi_code,
                      f.new_pauta_code,
                      f.new_mtr_service_code,
                      f.new_fed_service_code,
                      f.new_state_of_manufacture,
                      f.new_pharma_list_type,
                      im_pack.simple_pack_ind,
                      im_comp.item_xform_ind,
                      nvl(d.dim_object, '-999') dim_object,
                      nvl(d.length,'-999') length,
                      nvl(d.lwh_uom,'-999') lwh_uom,
                      nvl(d.weight,'-999') weight,
                      nvl(d.net_weight, '-999') net_weight,
                      nvl(d.weight_uom,'-999') weight_uom,
                      nvl(d.liquid_volume,  '-999') liquid_volume,
                      nvl(d.liquid_volume_uom,'-999') liquid_volume_uom,
                      decode(f.item, v2.item, 1, 0) comp_on_reclass
                 from v_packsku_qty v1,
                      l10n_br_fiscal_reclass f,
                      v_packsku_qty v2,
                      item_master im_pack,
                      item_master im_comp,
                      dim d
                where f.item                  = v1.item
                  and f.status                in ('N','S','A')
                  and f.active_date          >= get_vdate
                  and v1.pack_no              = v2.pack_no
                  and v2.pack_no              = im_pack.item
                  and im_pack.simple_pack_ind = 'Y'
                  and v2.item                 = im_comp.item
                  and im_pack.item            = d.item(+)) i
        group by i.pack_no,
                 i.item,
                 i.new_service_item_ind,
                 i.new_ncm_code,
                 i.new_ncm_char_code,
                 i.new_ex_ipi_code,
                 i.new_pauta_code,
                 i.new_mtr_service_code,
                 i.new_fed_service_code,
                 i.new_state_of_manufacture,
                 i.new_pharma_list_type,
                 i.active_date,
                 i.simple_pack_ind,
                 i.item_xform_ind,
                 i.dim_object,
                 i.length,
                 i.lwh_uom,
                 i.weight,
                 i.net_weight,
                 i.weight_uom,
                 i.liquid_volume,
                 i.liquid_volume_uom
      )  
   select distinct
          loc.city,
          loc.city_desc,
          loc.state,
          loc.state_desc,
          loc.country_id,
          loc.country_desc,
          loc.cnae_code_concatenation,
          nvl(loc.iss_contrib_ind,'N'),
          nvl(loc.ipi_ind,'N'),
          nvl(loc.icms_contrib_ind,'N'),
          loc.cnpj,
          nvl(loc.name_value_pair_id_concat,'-1'),
          --
          '0',                  --vbi.origin_code,
          --
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_service_item_ind, vbi.service_ind), 'N'),
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_ncm_code, vbi.classification_id), '-1'),
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_ncm_char_code, vbi.ncm_char_code), '-1'),
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_ex_ipi_code, vbi.ex_ipi), '-1'),
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_pauta_code, vbi.pauta_code), '-1'),
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_mtr_service_code, vbi.service_code), '-1'),
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_fed_service_code, vbi.federal_service), '-1'),
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_state_of_manufacture, vbi.state_of_manufacture), '-1'),
          nvl(decode(item_dim.comp_on_reclass, 1, item_dim.new_pharma_list_type, vbi.pharma_list_type), '-1'),
          --
          nvl(nv.item_name_value_pair_id_concat,'-1'),
          item_dim.dim_object,
          item_dim.length,
          item_dim.lwh_uom,
          item_dim.weight,
          item_dim.net_weight,
          item_dim.weight_uom,
          item_dim.liquid_volume,
          item_dim.liquid_volume_uom,
          item_dim.item_xform_ind,
          item_dim.active_date,
          item_dim.pack_no,  -- pack no
          item_dim.item,  -- item
          0,                  --process_id,
          0,                  --item_ref_id,
          0                   --loc_ref_id
     from item_stg item_dim,
          l10n_br_extax_dest_group_help loc,
          item_loc il,
          v_br_item_fiscal_attrib vbi,
          (select entity item,
                  listagg (name_value_pair_id, '~') 
                  within group (order by name_value_pair_id) item_name_value_pair_id_concat
             from l10n_br_extax_help_nv_pair
            where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
            group by entity) nv
    where il.item                = item_dim.pack_no
      --
      and il.loc                 = loc.entity
      and loc.entity_type        in(L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                    L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                    L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
      and loc.city                     is not null
      and loc.cnae_code_concatenation  is not null
      --
      and vbi.origin_code        = '0'
      and vbi.item               = item_dim.item
      --
      and item_dim.item          = nv.item(+);

   if SPLIT_BY_PROCESS_SIZE(O_error_message,
                            I_process_size) = FALSE then
      return FALSE;
   end if;

   if SETUP_TAX_CALL_ROUTING_RETAIL(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END FISCAL_ITM_RECLASS_EXTAX_SETUP;
--------------------------------------------------------
FUNCTION EXPLODE_TAX_ATTRIBS(O_error_message    IN OUT VARCHAR2,
                             I_process_size     IN     NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.EXPLODE_TAX_ATTRIBS';

BEGIN

   -- clean up wildcards (source and destination are *)
   insert into l10n_br_extax_refresh_gtt (create_date,
                                effective_date,
                                classification_code,
                                ncm_char_code,
                                pauta_code,
                                source_state,
                                destination_state,
                                reason)
   select r.create_date,
          r.effective_date,
          nvl(r.classification_code, '-1'),
          nvl(r.ncm_char_code, '-1'),
          nvl(r.pauta_code, '-1'),
          s.state,
          s.state,
          r.reason
     from l10n_br_extax_refresh_gtt r,
          state s
    where r.source_state      = '*'
      and r.destination_state = '*'
      and s.country_id = 'BR';

   -- clean up wildcards (source is *, destination is populated)
   update l10n_br_extax_refresh_gtt
      set source_state = destination_state
    where source_state       = '*'
      and destination_state != '*';

   -- clean up wildcards (destination is *, source is populated)
   update l10n_br_extax_refresh_gtt
      set destination_state = source_state
    where source_state      != '*'
      and destination_state  = '*';


   -- get rid of wild card rows
   delete from l10n_br_extax_refresh_gtt
    where source_state      = '*'
       or destination_state = '*';

   ----------------------------------------------------------------

   -- a b c -> a b c
   insert into l10n_br_extax_refresh_retail (
           create_date,
           effective_date,
           classification_code,
           ncm_char_code,
           pauta_code,
           source_state,
           destination_state,
           reason)
    select distinct
           gtt.create_date,
           gtt.effective_date,
           gtt.classification_code,
           gtt.ncm_char_code,
           gtt.pauta_code,
           gtt.source_state,
           gtt.destination_state,
           null
      from l10n_br_extax_refresh_gtt gtt,
           (select distinct
                   nvl(classification_id,'-1') classification_id,
                   nvl(ncm_char_code,'-1') ncm_char_code,
                   nvl(pauta_code,'-1') pauta_code,
                   origin_code
              from v_br_item_fiscal_attrib
             where origin_code = '0'
               and rownum      > 0) vbi
     where gtt.classification_code != '*'
       and gtt.ncm_char_code       != '*'
       and gtt.pauta_code          != '*'
       and gtt.classification_code  = vbi.classification_id
       and gtt.ncm_char_code        = vbi.ncm_char_code
       and gtt.pauta_code           = vbi.pauta_code
       and gtt.source_state         = gtt.destination_state;

   -- a b * -> a b * / a b c
   insert into l10n_br_extax_refresh_retail (
           create_date,
           effective_date,
           classification_code,
           ncm_char_code,
           pauta_code,
           source_state,
           destination_state,
           reason)
    select distinct
           gtt.create_date,
           gtt.effective_date,
           gtt.classification_code,
           gtt.ncm_char_code,
           vbi.pauta_code,
           gtt.source_state,
           gtt.destination_state,
           null
      from l10n_br_extax_refresh_gtt gtt,
           (select distinct
                   nvl(classification_id,'-1') classification_id,
                   nvl(ncm_char_code,'-1') ncm_char_code,
                   nvl(pauta_code,'-1') pauta_code,
                   origin_code
              from v_br_item_fiscal_attrib
             where origin_code = '0'
               and rownum      > 0) vbi
     where gtt.classification_code != '*'
       and gtt.ncm_char_code       != '*'
       and gtt.pauta_code           = '*'
       and gtt.classification_code  = vbi.classification_id
       and gtt.ncm_char_code        = vbi.ncm_char_code
       and gtt.source_state         = gtt.destination_state;

   -- a * * -> a * * / a b * / a b c
   insert into l10n_br_extax_refresh_retail (
           create_date,
           effective_date,
           classification_code,
           ncm_char_code,
           pauta_code,
           source_state,
           destination_state,
           reason)
    select distinct
           gtt.create_date,
           gtt.effective_date,
           gtt.classification_code,
           vbi.ncm_char_code,
           vbi.pauta_code,
           gtt.source_state,
           gtt.destination_state,
           null
      from l10n_br_extax_refresh_gtt gtt,
           (select distinct
                   nvl(classification_id,'-1') classification_id,
                   nvl(ncm_char_code,'-1') ncm_char_code,
                   nvl(pauta_code,'-1') pauta_code,
                   origin_code
              from v_br_item_fiscal_attrib
             where origin_code = '0'
               and rownum      > 0) vbi
     where gtt.classification_code != '*'
       and gtt.ncm_char_code        = '*'
       and gtt.pauta_code           = '*'
       and gtt.classification_code  = vbi.classification_id
       and gtt.source_state         = gtt.destination_state;

   -- * * * -> a * * / a b * / a b c
   insert into l10n_br_extax_refresh_retail (
           create_date,
           effective_date,
           classification_code,
           ncm_char_code,
           pauta_code,
           source_state,
           destination_state,
           reason)
    select distinct
           gtt.create_date,
           gtt.effective_date,
           vbi.classification_id,
           vbi.ncm_char_code,
           vbi.pauta_code,
           gtt.source_state,
           gtt.destination_state,
           null
      from (select distinct
                   create_date,
                   effective_date,
                   source_state,
                   destination_state
              from l10n_br_extax_refresh_gtt
             where classification_code  = '*'
               and ncm_char_code        = '*'
               and pauta_code           = '*'
               and source_state         = destination_state
               and rownum               > 0) gtt,
           (select distinct
                   nvl(classification_id,'-1') classification_id,
                   nvl(ncm_char_code,'-1') ncm_char_code,
                   nvl(pauta_code,'-1') pauta_code,
                   origin_code
              from v_br_item_fiscal_attrib
             where origin_code = '0'
               and rownum      > 0) vbi;

   if EXTAX_LOC_GROUP_SETUP(O_error_message) = FALSE then
      return FALSE;
   end if;

   delete from l10n_br_extax_refresh_retail r
    where r.classification_code != '-1'
      and r.ncm_char_code != '-1'
      and r.pauta_code != '-1'
      and (r.classification_code,r.ncm_char_code,r.pauta_code) not in
                  (select distinct vbi.classification_id,vbi.ncm_char_code,vbi.pauta_code 
                     from v_br_item_fiscal_attrib vbi 
                    where origin_code = '0' 
                      and vbi.classification_id IS NOT NULL
                      and vbi.ncm_char_code IS NOT NULL 
                      and vbi.pauta_code IS NOT NULL);

   delete from l10n_br_extax_refresh_retail r
    where r.classification_code IS NOT NULL
      and r.ncm_char_code IS NOT NULL
      and r.pauta_code = '-1'
      and (r.classification_code,r.ncm_char_code) not in
                  (select distinct vbi.classification_id,vbi.ncm_char_code 
                     from v_br_item_fiscal_attrib vbi 
                    where origin_code = '0' 
                      and vbi.classification_id IS NOT NULL
                      and vbi.ncm_char_code IS NOT NULL 
                      and vbi.pauta_code IS NULL);

   delete from l10n_br_extax_refresh_retail r
    where r.classification_code IS NOT NULL
      and r.ncm_char_code = '-1'
      and r.pauta_code    = '-1'
      and (r.classification_code) not in
                  (select distinct vbi.classification_id
                     from v_br_item_fiscal_attrib vbi 
                    where origin_code = '0' 
                      and vbi.classification_id IS NOT NULL
                      and vbi.ncm_char_code IS NULL 
                      and vbi.pauta_code IS NULL);

   insert into l10n_br_extax_stg_retail (
           city,
           city_desc,
           state,
           state_desc,
           country_id,
           country_desc,
           cnae_code,
           iss,
           ipi,
           icms,
           cnpj,
           loc_name_value_pair_id_concat,
           origin_code,
           service_ind,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           effective_date,
           pack_no,
           item,
           process_id,
           item_ref_id,
           loc_ref_id,
           item_xform_ind)
   select inner2.city,
          inner2.city_desc,
          inner2.state,
          inner2.state_desc,
          inner2.country_id,
          inner2.country_desc,
          inner2.cnae_code,
          inner2.iss,
          inner2.ipi,
          inner2.icms,
          inner2.cnpj,
          inner2.loc_name_value_pair_id_concat,
          inner2.origin_code,
          inner2.service_ind,
          inner2.classification_id,
          inner2.ncm_char_code,
          inner2.ex_ipi,
          inner2.pauta_code,
          inner2.service_code,
          inner2.federal_service,
          inner2.state_of_manufacture,
          inner2.pharma_list_type,
          inner2.item_name_value_pair_id_concat,
          inner2.dim_object,
          inner2.length,
          inner2.lwh_uom,
          inner2.weight,
          inner2.net_weight,
          inner2.weight_uom,
          inner2.liquid_volume,
          inner2.liquid_volume_uom,
          inner2.effective_date,
          inner2.pack_no,
          inner2.item,
          --
          decode(row_number() over (partition by inner2.date_svc_split, inner2.data_split
                                        order by inner2.effective_date),
                 1,l10n_br_extax_maint_sql.get_seq('Y'),
                   l10n_br_extax_maint_sql.get_seq('N')),
          --
          inner2.item_ref_id,
          inner2.loc_ref_id,
          inner2.item_xform_ind
     from (
   select inner.city,
          inner.city_desc,
          inner.state,
          inner.state_desc,
          inner.country_id,
          inner.country_desc,
          inner.cnae_code,
          inner.iss,
          inner.ipi,
          inner.icms,
          inner.cnpj,
          inner.loc_name_value_pair_id_concat,
          inner.origin_code,
          inner.service_ind,
          inner.classification_id,
          inner.ncm_char_code,
          inner.ex_ipi,
          inner.pauta_code,
          inner.service_code,
          inner.federal_service,
          inner.state_of_manufacture,
          inner.pharma_list_type,
          inner.item_name_value_pair_id_concat,
          inner.dim_object,
          inner.length,
          inner.lwh_uom,
          inner.weight,
          inner.net_weight,
          inner.weight_uom,
          inner.liquid_volume,
          inner.liquid_volume_uom,
          inner.effective_date,
          inner.pack_no,
          inner.item,
          inner.item_xform_ind,
          dense_rank() over (order by inner.service_ind,
                                      inner.ncm_char_code,
                                      inner.pauta_code,
                                      decode(inner.service_ind,'N',inner.classification_id,null),
                                      decode(inner.service_ind,'N',inner.ex_ipi,null),
                                      decode(inner.service_ind,'N',inner.state_of_manufacture,null),
                                      decode(inner.service_ind,'N',inner.pharma_list_type,null),
                                      decode(inner.service_ind,'N',inner.item_xform_ind,null),
                                      decode(inner.service_ind,'Y',inner.service_code,null),
                                      decode(inner.service_ind,'Y',inner.federal_service,null),
                                      inner.item_name_value_pair_id_concat,
                                      inner.dim_object,
                                      inner.length,
                                      inner.lwh_uom,
                                      inner.weight,
                                      inner.net_weight,
                                      inner.weight_uom,
                                      inner.liquid_volume,
                                      inner.liquid_volume_uom,
                                      inner.pack_no,
                                      inner.item) item_ref_id,
          dense_rank() over (order by inner.city,
                                      inner.state,
                                      inner.country_id,
                                      inner.cnae_code,
                                      inner.iss,
                                      inner.ipi,
                                      inner.icms,
                                      inner.cnpj,
                                      inner.loc_name_value_pair_id_concat) loc_ref_id,
          dense_rank() over (order by inner.effective_date) date_svc_split,
          ceil(row_number() over (partition by inner.effective_date
                            order by inner.city,
                                     inner.state,
                                     inner.country_id,
                                     inner.cnae_code,
                                     inner.iss,
                                     inner.ipi,
                                     inner.icms,
                                     inner.cnpj,
                                     inner.loc_name_value_pair_id_concat,
                                     --
                                     inner.service_ind,
                                     inner.ncm_char_code,
                                     inner.pauta_code,
                                     decode(inner.service_ind,'N',inner.classification_id,null),
                                     decode(inner.service_ind,'N',inner.ex_ipi,null),
                                     decode(inner.service_ind,'N',inner.state_of_manufacture,null),
                                     decode(inner.service_ind,'N',inner.pharma_list_type,null),
                                     decode(inner.service_ind,'N',inner.item_xform_ind,null),
                                     decode(inner.service_ind,'Y',inner.service_code,null),
                                     decode(inner.service_ind,'Y',inner.federal_service,null),
                                     inner.item_name_value_pair_id_concat,
                                     inner.dim_object,
                                     inner.length,
                                     inner.lwh_uom,
                                     inner.weight,
                                     inner.net_weight,
                                     inner.weight_uom,
                                     inner.liquid_volume,
                                     inner.liquid_volume_uom,
                                     inner.pack_no,
                                     inner.item ) / I_process_size) + 0 data_split
     from
     (
   --PACK
   with item_stg as
   (
    select i.pack_no,
           v.item,
           i.simple_pack_ind,
           i.destination_state,
           i.source_state,
           nvl(vbi.service_ind,'N') service_ind,
           vbi.classification_id classification_code,
           vbi.ncm_char_code,
           nvl(vbi.ex_ipi, '-1') ex_ipi,
           vbi.pauta_code,
           nvl(vbi.service_code,'-1') service_code,
           nvl(vbi.federal_service, '-1') federal_service,
           nvl(vbi.state_of_manufacture, '-1') state_of_manufacture,
           nvl(vbi.pharma_list_type, '-1') pharma_list_type,
           i.item_xform_ind,
           i.effective_date,
           decode(i.simple_pack_ind,'Y',i.pack_no,v.item) dim_join_item
      from (select /*+ ordered */ distinct pi.pack_no,
                   im.simple_pack_ind,
                   r.destination_state,
                   r.source_state,
                   im.item_xform_ind,
                   r.effective_date
              from item_master im,
                   v_packsku_qty pi,
                   v_br_item_fiscal_attrib vbi,
                   l10n_br_extax_refresh_retail r
             where r.classification_code = nvl(vbi.classification_id,'-1')
               and r.ncm_char_code       = nvl(vbi.ncm_char_code,'-1')
               and r.pauta_code          = nvl(vbi.pauta_code,'-1')
               and vbi.origin_code       = '0'
               --
               and vbi.item              = pi.item
               and pi.pack_no            = im.item
               and im.status             = 'A'
               and im.tran_level         = im.item_level
               and im.pack_ind           = 'Y'
               and im.sellable_ind       = 'Y'
            ) i,
           v_br_item_fiscal_attrib vbi,
           v_packsku_qty v
     where i.pack_no = v.pack_no
       and v.item    = vbi.item
   ),
   dim as
   (select distinct 
           isc.item,
           iscd.dim_object,
           iscd.length,
           iscd.lwh_uom,
           iscd.weight,
           iscd.net_weight,
           iscd.weight_uom,
           iscd.liquid_volume,
           iscd.liquid_volume_uom
      from item_supp_country isc,
           item_supp_country_dim iscd,
           item_stg
     where iscd.dim_object         = 'EA'
       and isc.item                = iscd.item
       and isc.supplier            = iscd.supplier
       and isc.origin_country_id   = iscd.origin_country
       and isc.primary_supp_ind    = 'Y'
       and isc.primary_country_ind = 'Y'
       and isc.item                = item_stg.dim_join_item
   )
   select  /*+ use_hash(il) index_ffs(il,pk_item_loc) */   distinct
          loc.city,
          loc.city_desc,
          loc.state,
          loc.state_desc,
          loc.country_id,
          loc.country_desc,
          loc.cnae_code_concatenation cnae_code,
          nvl(loc.iss_contrib_ind,'N') iss,
          nvl(loc.ipi_ind,'N') ipi,
          nvl(loc.icms_contrib_ind,'N') icms,
          loc.cnpj,
          nvl(loc.name_value_pair_id_concat,'-1') loc_name_value_pair_id_concat,
          --
          '0' origin_code,
          item_map.service_ind,
          item_map.classification_code classification_id,
          item_map.ncm_char_code,
          item_map.ex_ipi,
          item_map.pauta_code,
          item_map.service_code,
          item_map.federal_service,
          item_map.state_of_manufacture,
          item_map.pharma_list_type,
          nvl(nv.item_name_value_pair_id_concat,'-1') item_name_value_pair_id_concat,
          --
          nvl(d.dim_object, '-999') dim_object,
          nvl(d.length,'-999') length,
          nvl(d.lwh_uom,'-999') lwh_uom,
          nvl(d.weight,'-999') weight,
          nvl(d.net_weight, '-999') net_weight,
          nvl(d.weight_uom,'-999') weight_uom,
          nvl(d.liquid_volume,  '-999') liquid_volume,
          nvl(d.liquid_volume_uom,'-999') liquid_volume_uom,
          --
          item_map.item_xform_ind,
          item_map.effective_date,
          item_map.pack_no,
          item_map.item,
          0 process_id,
          0 item_ref_id,
          0 loc_ref_id
     from item_stg  item_map ,
          l10n_br_extax_dest_group_help loc,
          item_loc  il,
          dim d,
          (select entity item,
                  listagg (name_value_pair_id, '~') 
                  within group (order by name_value_pair_id) item_name_value_pair_id_concat
             from l10n_br_extax_help_nv_pair
            where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
            group by entity) nv
    where il.item                      = item_map.pack_no
      and il.loc                       = loc.entity
      --
      and loc.state                    = item_map.destination_state
      and loc.entity_type              in(L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                          L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                          L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
      and loc.city                     is not null
      and loc.cnae_code_concatenation  is not null
      --
      and il.item                      = nv.item(+)
      --
      and item_map.dim_join_item       = d.item(+)
UNION ALL
   select /*+  use_hash(il) index_ffs(il,pk_item_loc) */   distinct
          loc.city                                city,
          loc.city_desc                           city_desc,
          loc.state                               state,
          loc.state_desc                          state_desc,
          loc.country_id                          country_id,
          loc.country_desc                        country_desc,
          loc.cnae_code_concatenation             cnae_code,
          nvl(loc.iss_contrib_ind,'N')            iss,
          nvl(loc.ipi_ind,'N')                    ipi,
          nvl(loc.icms_contrib_ind,'N')           icms,
          loc.cnpj                                cnpj,
          nvl(loc.name_value_pair_id_concat,'-1') loc_name_value_pair_id_concat,
          --
          '0'                                     origin_code,
          it.service_ind                          service_ind,
          it.classification_code                  classification_id,
          it.ncm_char_code                        ncm_char_code,
          it.ex_ipi                               ex_ipi,
          it.pauta_code                           pauta_code,
          it.service_code                         service_code,
          it.federal_service                      federal_service,
          nvl(it.state_of_manufacture,'-1')       state_of_manufacture,
          nvl(it.pharma_list_type,'-1')           pharma_list_type,
          nvl(it.item_name_value_pair_id_concat,'-1')      item_name_value_pair_id_concat,
          nvl(it.dim_object, '-999')               dim_object,
          nvl(it.length,'-999')                   length,
          nvl(it.lwh_uom,'-999')                  lwh_uom,
          nvl(it.weight,'-999')                   weight,
          nvl(it.net_weight,'-999')               new_weight,
          nvl(it.weight_uom,'-999')               weight_uom,
          nvl(it.liquid_volume,'-999')            liquid_volumne,
          nvl(it.liquid_volume_uom,'-999')        liquid_volumne_uom,
          it.item_xform_ind                       item_xform_ind,
          it.effective_date                       effective_date,
          '-1'                                    pack_no,
          '-1'                                    item,
          0                                       process_id,
          0                                       item_ref_id,
          0                                       loc_ref_id
     from l10n_br_extax_dest_group_help loc,
          item_loc il,
          (select vbi.item,
                  r.destination_state,
                  r.source_state,
                  nvl(vbi.service_ind,'N') service_ind,
                  r.classification_code,
                  r.ncm_char_code,
                  nvl(vbi.ex_ipi, '-1') ex_ipi,
                  r.pauta_code  ,
                  nvl(vbi.service_code,'-1') service_code,
                  nvl(vbi.federal_service, '-1') federal_service,
                  nvl(vbi.state_of_manufacture,'-1') state_of_manufacture,
                  nvl(vbi.pharma_list_type,'-1') pharma_list_type,
                  r.effective_date,
                  im.item_xform_ind,
                  nv.item_name_value_pair_id_concat,
                  iscd.dim_object,
                  iscd.length,
                  iscd.lwh_uom,
                  iscd.weight,
                  iscd.net_weight,
                  iscd.weight_uom,
                  iscd.liquid_volume,
                  iscd.liquid_volume_uom
             from l10n_br_extax_refresh_retail r,
                  v_br_item_fiscal_attrib vbi,
                  item_master im,
                  (select entity item,
                          listagg (name_value_pair_id, '~')
                          within group (order by name_value_pair_id) item_name_value_pair_id_concat
                     from l10n_br_extax_help_nv_pair
                    where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
                    group by entity) nv,
                  (select dim.item,
                          dim.dim_object,
                          dim.length,
                          dim.lwh_uom,
                          dim.weight,
                          dim.net_weight,
                          dim.weight_uom,
                          dim.liquid_volume,
                          dim.liquid_volume_uom
                     from item_supp_country isc,
                          item_supp_country_dim dim
                    where dim.dim_object         = 'EA'
                      and isc.item                = dim.item
                      and isc.supplier            = dim.supplier
                      and isc.origin_country_id   = dim.origin_country
                      and isc.primary_supp_ind    = 'Y'
                      and isc.primary_country_ind = 'Y'
                  ) iscd
            where r.classification_code   = nvl(vbi.classification_id,'-1')
              and r.ncm_char_code         = nvl(vbi.ncm_char_code,'-1')
              and r.pauta_code            = nvl(vbi.pauta_code,'-1')
              --
              and im.item                 = vbi.item
              and im.status               = 'A'
              and im.tran_level           = im.item_level
              and im.pack_ind             = 'N'
              --
              and vbi.origin_code         = '0'
              --
              and im.item                 = nv.item(+)
              and im.item                 = iscd.item(+)
              ) it
    where il.item                      = it.item
      and il.loc                       = loc.entity
      --
      and loc.state                    = it.destination_state
      and loc.entity_type              in(L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                          L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                          L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
      and loc.city                     is not null
      and loc.cnae_code_concatenation  is not null
      ) inner) inner2;


   if SETUP_TAX_CALL_ROUTING_RETAIL(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END EXPLODE_TAX_ATTRIBS;
--------------------------------------------------------
FUNCTION SPLIT_BY_PROCESS_SIZE(O_error_message  IN OUT VARCHAR2,
                               I_process_size   IN     NUMBER)
RETURN BOOLEAN IS

   L_program        VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SPLIT_BY_PROCESS_SIZE';
   L_max_process_id NUMBER := 0;
   L_nextval        L10N_BR_EXTAX_STG_RETAIL.PROCESS_ID%TYPE;

   cursor C_DATES is
      select distinct effective_date
        from l10n_br_extax_stg_retail
      order by effective_date;

   cursor C_MAX_PROC is
      select max(process_id)
        from l10n_br_extax_stg_retail;

   L_seq_help_tbl OBJ_NUM_NUM_STR_TBL;

   cursor C_SEQ_HELPER is
      select OBJ_NUM_NUM_STR_REC(i.process_id, l10n_br_tax_service_seq.nextval, null)
        from (select distinct process_id
                from l10n_br_extax_stg_retail) i;

BEGIN

   for rec in C_DATES loop

      open C_MAX_PROC;
      fetch C_MAX_PROC into L_max_process_id;
      close C_MAX_PROC;

      merge into l10n_br_extax_stg_retail target using (
         select i.rowid,
                dense_rank() over (order by i.service_ind,
                                            i.ncm_char_code,
                                            i.pauta_code,
                                            decode(i.service_ind,'N',i.classification_id,null),
                                            decode(i.service_ind,'N',i.ex_ipi,null),
                                            decode(i.service_ind,'N',i.state_of_manufacture,null),
                                            decode(i.service_ind,'N',i.pharma_list_type,null),
                                            decode(i.service_ind,'N',i.item_xform_ind,null),
                                            decode(i.service_ind,'Y',i.service_code,null),
                                            decode(i.service_ind,'Y',i.federal_service,null),
                                            i.item_name_value_pair_id_concat,
                                            i.dim_object,
                                            i.length,
                                            i.lwh_uom,
                                            i.weight,
                                            i.net_weight,
                                            i.weight_uom,
                                            i.liquid_volume,
                                            i.liquid_volume_uom,
                                            i.pack_no,
                                            i.item  ) item_ref_id,
                dense_rank() over (order by i.city,
                                            i.state,
                                            i.country_id,
                                            i.cnae_code,
                                            i.iss,
                                            i.ipi,
                                            i.icms,
                                            i.cnpj,
                                            i.loc_name_value_pair_id_concat) loc_ref_id,
                ceil(rank() over (order by i.city,
                                           i.state,
                                           i.country_id,
                                           i.cnae_code,
                                           i.iss,
                                           i.ipi,
                                           i.icms,
                                           i.cnpj, 
                                           i.loc_name_value_pair_id_concat,
                                           --
                                           i.service_ind,
                                           i.ncm_char_code,
                                           i.pauta_code,
                                           decode(i.service_ind,'N',i.classification_id,null),
                                           decode(i.service_ind,'N',i.ex_ipi,null),
                                           decode(i.service_ind,'N',i.state_of_manufacture,null),
                                           decode(i.service_ind,'N',i.pharma_list_type,null),
                                           decode(i.service_ind,'N',i.item_xform_ind,null),
                                           decode(i.service_ind,'Y',i.service_code,null),
                                           decode(i.service_ind,'Y',i.federal_service,null),
                                           i.item_name_value_pair_id_concat,
                                           i.dim_object,
                                           i.length,
                                           i.lwh_uom,
                                           i.weight,
                                           i.net_weight,
                                           i.weight_uom,
                                           i.liquid_volume,
                                           i.liquid_volume_uom,
                                           i.pack_no,
                                           i.item )/I_process_size) + L_max_process_id process_id
          from l10n_br_extax_stg_retail i
         where i.effective_date = rec.effective_date) use_this 
      on (target.rowid = use_this.rowid)
      when matched then
      update set target.item_ref_id  = use_this.item_ref_id,
                 target.loc_ref_id   = use_this.loc_ref_id,
                 target.process_id   = use_this.process_id;

   end loop;

   open C_SEQ_HELPER;
   fetch C_SEQ_HELPER bulk collect into L_seq_help_tbl;
   close C_SEQ_HELPER;

   FORALL i in L_seq_help_tbl.first..L_seq_help_tbl.last
      update l10n_br_extax_stg_retail
         set process_id = L_seq_help_tbl(i).number_2
       where process_id = L_seq_help_tbl(i).number_1;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END SPLIT_BY_PROCESS_SIZE;
--------------------------------------------------------
FUNCTION SETUP_TAX_CALL_ROUTING_RETAIL(O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SETUP_TAX_CALL_ROUTING_RETAIL';

BEGIN

   --setup routing 
   insert into l10n_br_tax_call_stage_routing (tax_service_id,
                                               client_id,
                                               update_history_ind)
   select distinct process_id,
                   L10N_BR_TAX_SERVICE_MNGR_SQL.SEED_RETAIL,
                   'N'
     from l10n_br_extax_stg_retail;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END SETUP_TAX_CALL_ROUTING_RETAIL;
--------------------------------------------------------
FUNCTION PROCESS(O_error_message  IN OUT VARCHAR2,
                 I_process_id     IN     NUMBER)
RETURN BOOLEAN IS

   L_program               VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.PROCESS';
   L_status                VARCHAR2(10); 

BEGIN

   if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                       L_status,
                                                       I_process_id,
                                                       'CALC_TAX'   ) = FALSE then
       return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------
FUNCTION GET_REQUEST_DATA_RETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id         IN     NUMBER,
                                 O_tax_data              OUT "RIB_FiscDocColRBM_REC")
   RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'L10N_BR_EXTAX_MAINT_SQL.GET_REQUEST_DATA_RETAIL';

   L_incl_nic_ind_pis      VARCHAR2(1);
   L_incl_nic_ind_cofins   VARCHAR2(1);
   L_incl_nic_ind_icms     VARCHAR2(1);

   L_effective_date        DATE;

   L_loc_ref_id            ITEM_LOC.LOC%TYPE;

   L_loc_tbl               "RIB_FiscEntityRBO_TBL" := "RIB_FiscEntityRBO_TBL"();

   L_src_rec               "RIB_FromEntity_REC"   := "RIB_FromEntity_REC"(0,null);
   L_dest_rec              "RIB_ToEntity_REC"     := "RIB_ToEntity_REC"(0,null);
   L_src_tbl               "RIB_FromEntity_TBL"   := "RIB_FromEntity_TBL"();
   L_dest_tbl              "RIB_ToEntity_TBL"     := "RIB_ToEntity_TBL"();
   ---
   L_doc_line_item_tbl     "RIB_LineItemRBO_TBL"  := "RIB_LineItemRBO_TBL"();
   ---
   L_fiscal_doc_rec        "RIB_FiscDocRBO_REC";
   L_fiscal_doc_tbl        "RIB_FiscDocRBO_TBL"   := "RIB_FiscDocRBO_TBL"();
   ---
   L_fisc_doc_chnk_rec     "RIB_FiscDocChnkRBO_REC" := "RIB_FiscDocChnkRBO_REC"(0,null,null);
   L_fisc_doc_chnk_tbl     "RIB_FiscDocChnkRBO_TBL" := "RIB_FiscDocChnkRBO_TBL"();
   ---
   L_tbl_obj_eco_class_cd  "RIB_EcoClassCd_TBL"   := "RIB_EcoClassCd_TBL"();
   L_cnae_concat           varchar2(999);

   cursor C_GET_TAX_IND is
      select decode(v1.vat_code, 'PIS',    v1.incl_nic_ind, NULL) pis,
             decode(v2.vat_code, 'COFINS', v2.incl_nic_ind, NULL) cofins,
             decode(v3.vat_code, 'ICMS',   v3.incl_nic_ind, NULL) icms
        from vat_codes v1,
             vat_codes v2,
             vat_codes v3
       where v1.vat_code in ('PIS')
         and v2.vat_code in ('COFINS')
         and v3.vat_code in ('ICMS');

   cursor C_DATE is
      select distinct effective_date
        from l10n_br_extax_stg_retail
       where process_id = I_process_id;

   cursor C_MULTI_CNAE is
      SELECT "RIB_EcoClassCd_REC"(0,
             TRIM( SUBSTR ( txt
                          , INSTR (txt, '~', 1, level ) + 1
                          , INSTR (txt, '~', 1, level+1) - INSTR (txt, '~', 1, level) -1
                          )))
        FROM (SELECT '~'||L_cnae_concat||'~' AS txt from dual)
      CONNECT BY level <= LENGTH(txt)-LENGTH(REPLACE(txt,'~',''))-1;

   L_nv_entity_group_concat  varchar2(500);
   L_nv_table                "RIB_NameValPairRBO_TBL" := "RIB_NameValPairRBO_TBL"();

   cursor C_NAME_VALUE_PAIR is
      select "RIB_NameValPairRBO_REC"(0,name, value) from
         (select distinct name_value_pair_id, name, value
            from l10n_br_extax_help_nv_pair h,
               (SELECT TRIM( SUBSTR ( txt
                                    , INSTR (txt, '~', 1, level ) + 1
                                    , INSTR (txt, '~', 1, level+1) - INSTR (txt, '~', 1, level) -1
                                    )) grp_id
                  FROM (SELECT '~'||L_nv_entity_group_concat||'~' AS txt from dual)
                CONNECT BY level <= LENGTH(txt)-LENGTH(REPLACE(txt,'~',''))-1) g
           where h.NAME_VALUE_PAIR_ID = to_number(g.grp_id) order by name_value_pair_id);

   cursor C_GET_SRC_DEST_OBJ is
      select loc_ref_id,
             cnae_code,
             name_value_pair_id_concat,
             "RIB_FiscEntityRBO_REC"(0,
                "RIB_AddrRBO_TBL"(
                "RIB_AddrRBO_REC"
                   (0,             -- rob_oid
                    city,          -- city_id
                    null,          -- state_name
                    country_desc,  -- country_name
                    '0',           -- addr
                    '01',          -- addr_type
                    '1',           -- primary_addr_type_ind
                    'Y',           -- primary_addr_ind
                    'dummy addr',  -- add_1
                    NULL,          -- add_2
                    NULL,          -- add_3
                    city_desc,     -- city
                    state,         -- state
                    '105'         -- country_id
                              -- extOfAddrDesc
                              -- LOCOFADDRDESC
                    )), -- addrDesc_
                NULL, -- eco_class_cd_
                NULL, -- DIFF_TAX_REGIME_NAME_TBL
                loc_ref_id,  -- entity_code_
                L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,  -- entity_type_
                NULL,  -- legal_name_
                'S',   -- fiscal_type_
                NULL,  -- is_simples_contributor_
                cnpj,  -- federal_tax_reg_id_
                'N',  -- is_rural_producer_      -- is this the same for both source and destination?
                null, --is_income_range_eligible
                null, --is_distr_a_manufacturer
                null, --icms_simples_rate
                CAST(MULTISET(select "RIB_TaxContributor_REC"(0,'ISS')
                                from dual
                               where m1.ISS = 'Y'
                               union all
                               select "RIB_TaxContributor_REC"(0,'IPI')
                                 from dual
                                where m1.IPI = 'Y'
                                union all
                               select "RIB_TaxContributor_REC"(0,'ICMS')
                                 from dual
                                where m1.ICMS = 'Y') AS "RIB_TaxContributor_TBL"),  -- taxContributor_
                NULL  -- NAMEVALPAIRRBO_TBL
                       -- extOfFiscalEntityRBO_
                    ) fiscal_ent_rec  --LOCOFFISCALENTITYRBO_
        from (select distinct
                     loc_ref_id,
                     city,
                     city_desc,
                     state,
                     state_desc,
                     country_id,
                     country_desc,
                     cnpj,
                     cnae_code,
                     iss,
                     ipi,
                     icms,
                     loc_name_value_pair_id_concat name_value_pair_id_concat
                from l10n_br_extax_stg_retail
               where process_id = I_process_id) m1;

   cursor C_GET_ITEM_OBJ is
      select "RIB_LineItemRBO_REC"(0,
                item_ref_id, -- document_line_id_
                item, -- item_id_
                NULL, -- item_tran_Code_
                decode(service_ind, 'N', 'M', 'S'),  -- item_type_
                1,    -- quantity_
                'EA', -- unit_of_measure_
                1, --quantity_in_eaches_
                NULL, -- origin_doc_date_
                (case when pack_no = '-1' then null else pack_no end), -- pack_item_id_
                99.99, -- total_cost_ these are defaulted because cost does not affect the outcome, but is a required value 
                99.99, -- unit_cost_ 
                NULL, -- source_taxpayer_type_
                NULL, -- orig_fiscal_doc_number_
                NULL, -- orig_fiscal_doc_series_
                (case when dim_object = '-999' then 'EA' else dim_object end),               -- dim_object_
                (case when length = '-999' then null else length end),                       -- length_
                NULL,                                                                         -- width_
                (case when lwh_uom = '-999' then null else lwh_uom end),                     -- lwh_uom_
                (case when weight = '-999' then null else weight end),                       -- weight_
                (case when net_weight = '-999' then null else net_weight end),               -- net_weight_
                (case when weight_uom = '-999' then null else weight_uom end),               -- weight_uom_
                (case when liquid_volume = '-999' then null else liquid_volume end),         -- liquid_volume_
                (case when liquid_volume_uom = '-999' then null else liquid_volume_uom end), -- liquid_volume_uom_
                NULL, -- FREIGHT
                NULL, -- INSURANCE
                NULL, -- DISCOUNT
                NULL, -- COMMISION
                NULL, -- FREIGHT_TYPE
                NULL, -- OTHER_EXPENCES
                NULL, -- origin_fiscal_code_opr_
                NULL, -- deduced_fiscal_code_opr_
                'Y',  -- deduce_cfop_code_
                NULL, -- icms_cst_code_
                NULL, -- pis_cst_code_
                NULL, -- cofins_cst_code_
                NULL, -- deduce_icms_cst_code_
                NULL, -- deduce_pis_cst_code_
                NULL, -- deduce_cofins_cst_code_
                NULL, -- recoverable_icmsst_
                L_incl_nic_ind_cofins,   -- item_cost_contains_cofins_
                NULL,                    -- recoverable_base_icmsst_
                L_incl_nic_ind_pis,      -- item_cost_contains_pis_
                L_incl_nic_ind_icms,     -- item_cost_contains_icms_
                decode( service_ind,'N',
                      "RIB_PrdItemRBO_TBL"(
                      "RIB_PrdItemRBO_REC"(0,
                                           item_ref_id,   -- product.item_code_
                                           'dummy item desc', -- product.item_description_
                                           '0',               -- product.item_origin_
                                           'C',               -- product.item_utilization_
                                           item_xform_ind,              -- product.is_transformed_item_
                                           (case when classification_id = '-1'
                                                 then null else classification_id end),   -- fiscal_classification_code_
                                           -- send ncm_char_code.pauta_code, or ncm_char_code, or null
                                           (case when ncm_char_code != '-1' and pauta_code != '-1' 
                                                 then ncm_char_code||'.'||pauta_code
                                                 when ncm_char_code != '-1' and pauta_code = '-1'
                                                 then ncm_char_code
                                                 when ncm_char_code = '-1' and pauta_code != '-1' -- should not be possible
                                                 then pauta_code
                                                 else null
                                             end ),
                                                 --EXTOFPRDITEMRBO_TBL
                                                 --EXTOFPRDITEMRBO_TBL
                                           (case when ex_ipi = '-1' then null else ex_ipi end), -- IPI_EXCEPTION_CODE_
                                           NULL, -- PRODUCT_TYPE_
                                           (case when state_of_manufacture = '-1'
                                                 then null else state_of_manufacture end), -- STATE_OF_MANUFACTURE
                                           (case when pharma_list_type = '-1'
                                                 then null else pharma_list_type end), -- PHARMA_LIST_TYPE
                                           NULL  -- NAMEVALPAIRRBO_TBL
                                          )),
                        null),  -- PRDITEMRBO_TBL
                decode( service_ind,'Y',
                        "RIB_SvcItemRBO_TBL"(
                        "RIB_SvcItemRBO_REC"(0,
                                             item_ref_id, --ITEM_CODE_
                                             'dummy item desc', --ITEM_DESCRIPTION_
                                             (case when ncm_char_code != '-1' and pauta_code != '-1'
                                                 then ncm_char_code||'.'||pauta_code
                                                 when ncm_char_code != '-1' and pauta_code = '-1'
                                                 then ncm_char_code
                                                 when ncm_char_code = '-1' and pauta_code != '-1' -- should not be possible
                                                 then pauta_code
                                                 else null
                                               end ),
                                             federal_service, -- FEDERAL_SERVICE_CODE_
                                             service_code, --DESTINATION_SERVICE_CODE_
                                             city, --SERVICE_PROVIDER_CITY_
                                             NULL --NAMEVALPAIRRBO
                                                   --EXTOFSERVICEITEMRBO_
                                                   --LOCOFSERVICEITEMRBO_
                                             )),
                        NULL), -- SVCITEMRBO_TBL
                NULL, -- INFORMTAXRBO_TBL
                nv_tbl  -- NAMEVALPAIRRBO_TBL
                        -- extOfDocLineItemRBO_
                        -- LOCOFDOCLINEITEMRBO_
             ) doc_line_item_rec
        from (select service_ind,
                     classification_id,
                     ncm_char_code,
                     ex_ipi,
                     PAUTA_CODE,
                     service_code,
                     federal_service,
                     origin_code,
                     dim_object,
                     length,
                     lwh_uom,
                     weight,
                     net_weight,
                     weight_uom,
                     liquid_volume,
                     liquid_volume_uom,
                     item_ref_id,
                     pack_no,
                     item,
                     city,
                     item_xform_ind,
                     state_of_manufacture,
                     pharma_list_type,
                     CAST(MULTISET(select "RIB_NameValPairRBO_REC"(0,h.name, h.value)
                       from (SELECT distinct item_name_value_pair_id_concat,
                                   TRIM( SUBSTR ( txt
                                                , INSTR (txt, '~', 1, level ) + 1
                                                , INSTR (txt, '~', 1, level+1) - INSTR (txt, '~', 1, level) -1
                                                )) grp_id
                              FROM (select item_name_value_pair_id_concat,
                                           '~'||(case when item_name_value_pair_id_concat = '-1' then null
                                                 else item_name_value_pair_id_concat end)||'~' AS txt
                                      from l10n_br_extax_stg_retail
                                     where process_id = I_process_id
                                       and loc_ref_id = L_loc_ref_id)
                            CONNECT BY level <= LENGTH(txt)-LENGTH(REPLACE(txt,'~',''))-1) inner,
                            (select distinct name_value_pair_id, name, value from l10n_br_extax_help_nv_pair) h
                      where h.NAME_VALUE_PAIR_ID = to_number(inner.grp_id)
                        and inner.item_name_value_pair_id_concat = br.item_name_value_pair_id_concat)
                      as "RIB_NameValPairRBO_TBL") nv_tbl
                from l10n_br_extax_stg_retail br
               where process_id = I_process_id
                 and loc_ref_id = L_loc_ref_id);

   cursor C_GET_DOC_DETAILS is
      select "RIB_FiscDocRBO_REC"(0,
                L_dest_tbl,          -- TOENTITY_TBL
                L_src_tbl,           -- FROMENTITY_TBL
                L_doc_line_item_tbl, -- LINEITEMRBO_TBL
                NULL,                -- NAMEVALPAIRRBO_TBL
                L_effective_date,    -- due_date_
                L_effective_date,    -- fiscal_document_date_
                'FT',       -- document_type_
                NULL,       -- gross_weight_
                NULL,       -- net_weight_
                NULL,       -- operation_type_
                NULL,       -- freight
                NULL,       -- insurance
                NULL,       -- discount
                NULL,       -- commision
                NULL,       -- freight_type
                NULL,       -- other_expenses
                NULL,       -- total_cost_
                NULL,       -- tax_amount
                NULL,       -- tax_basis_amount
                NULL,       -- tax_code
                L_effective_date,    -- receipt_date_
                'O',        -- transaction_type_
                NULL,       -- is_supplier_issuer_
                NULL,       -- no_history_tracked_
                NULL,       -- process_inconclusive_rule_
                NULL,       -- approximation_mode_
                NULL,       -- decimal_precision_
                NULL,       -- calculation_status_
                'S',        -- enable_log_
                NULL,       -- calc_process_type_
                NULL,       -- nature_of_operation_
                NULL,       -- ignore_tax_calc_list_
                'ITEM',     -- document_series_
                492,        -- document_number_
                NULL       -- INFORMTAXRBO_TBL
                           -- extOfFiscalDocRBO_
                           -- LOCOFFISCALDOCRBO_
               )
        from dual;

   L_anydata_Obj         AnyData;

BEGIN

  open C_GET_TAX_IND;
  fetch C_GET_TAX_IND into L_incl_nic_ind_pis,
                           L_incl_nic_ind_cofins,
                           L_incl_nic_ind_icms;
  close C_GET_TAX_IND;
  ---
  open C_DATE;
  fetch C_DATE into L_effective_date;
  close C_DATE;

  for loc_rec in C_GET_SRC_DEST_OBJ loop

      L_loc_ref_id := loc_rec.loc_ref_id;
      L_cnae_concat := loc_rec.cnae_code;

      --eco
      open C_MULTI_CNAE;
      fetch C_MULTI_CNAE bulk collect into L_tbl_obj_eco_class_cd;
      close C_MULTI_CNAE;
      loc_rec.fiscal_ent_rec.EcoClassCd_TBL := L_tbl_obj_eco_class_cd;
      --name/value pair

      if loc_rec.name_value_pair_id_concat != '-1' then
         L_nv_entity_group_concat := loc_rec.name_value_pair_id_concat;
         open C_NAME_VALUE_PAIR;
         fetch C_NAME_VALUE_PAIR bulk collect into L_nv_table;
         close C_NAME_VALUE_PAIR;
         loc_rec.fiscal_ent_rec.NAMEVALPAIRRBO_TBL := L_nv_table;
      end if;

      L_loc_tbl.extend();
      L_loc_tbl(L_loc_tbl.count) := loc_rec.fiscal_ent_rec;

      -- Populate source TBL
      L_src_rec.fiscentityrbo_tbl := L_loc_tbl;
      L_src_tbl.extend();
      L_src_tbl(L_src_tbl.count) := L_src_rec;

      -- Populate destination TBL
      L_dest_rec.fiscentityrbo_tbl := L_loc_tbl;
      L_dest_tbl.extend();
      L_dest_tbl(L_dest_tbl.count) := L_dest_rec;

      -- Populate the document line item TBL
      open C_GET_ITEM_OBJ;
      fetch C_GET_ITEM_OBJ bulk collect into L_doc_line_item_tbl;
      close C_GET_ITEM_OBJ;

      -- Fill the request document TBL
      if L_doc_line_item_tbl is NOT NULL and L_doc_line_item_tbl.count > 0 then
         open C_GET_DOC_DETAILS;
         fetch C_GET_DOC_DETAILS into L_fiscal_doc_rec;
         close C_GET_DOC_DETAILS;

         if L_fiscal_doc_rec is NOT NULL then
            L_fiscal_doc_tbl.extend();
            L_fiscal_doc_tbl(L_fiscal_doc_tbl.count) := L_fiscal_doc_rec;
         end if;
      end if;

      -- Clear Objects
      L_loc_tbl.delete;
      L_src_tbl.delete;
      L_dest_tbl.delete;
      L_doc_line_item_tbl.delete;

   end loop;

   if L_fiscal_doc_tbl is NOT NULL and L_fiscal_doc_tbl.count > 0 then

      L_fisc_doc_chnk_rec.chunk_id                   := 1;
      L_fisc_doc_chnk_rec.fiscdocrbo_tbl             := L_fiscal_doc_tbl;
      L_fisc_doc_chnk_tbl.extend();
      L_fisc_doc_chnk_tbl(L_fisc_doc_chnk_tbl.count) := L_fisc_doc_chnk_rec;

      O_tax_data := "RIB_FiscDocColRBM_REC"(0,
                                            L_fisc_doc_chnk_tbl, -- FISCDOCCHNKRBO_TBL
                                            'N',                 -- THREADUSE
                                            NULL,                -- logs_
                                            NULL,                -- vendorType_
                                            NULL,                -- country_
                                            NULL                -- transactionType_
                                                                 -- extOfFiscalDocColRBM_
                                                );               -- LOCOFFISCALDOCCOLRBM_

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_REQUEST_DATA_RETAIL;
--------------------------------------------------------
FUNCTION SET_TAX_RESPONSE_DATA_RETAIL(O_error_message    IN OUT VARCHAR2,
                                      I_TaxData          IN     "RIB_FiscDocTaxColRBM_REC",
                                      I_process_id       IN     NUMBER)
RETURN BOOLEAN IS

   L_program        VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SET_TAX_RESPONSE_DATA_RETAIL';

   L_results_tbl    TAX_RESULT_TBL;
   L_ctr            binary_integer := 1;

   L_results_breakout_tbl          TAX_RESULT_BREAKOUT_TBL;
   L_breakout_ctr                  binary_integer := 1;

   L_loc_jurisdiction_code         varchar2(10);
   L_loc_cnpj                      varchar2(250);
   L_loc_cnae_code                 varchar2(250);
   L_loc_iss_contrib_ind           varchar2(250);
   L_loc_ipi_ind                   varchar2(250);
   L_loc_icms_contrib_ind          varchar2(250);

   L_loc_nv_pair_concat            varchar2(250);

   L_item_nv_pair_concat           varchar2(250);

   L_first                         varchar2(2);
   L_effective_date                DATE;
   L_fiscal_code                   varchar2(250);
   L_cum_tax_pct                   number(20,10);
   L_cum_tax_pct_ctr               binary_integer := 0;
   L_cum_tax_value                 number(20,10);

   L_tbl_diff_tax_regime           "RIB_DstDiffTaxRegime_TBL" := "RIB_DstDiffTaxRegime_TBL"();
   L_tbl_obj_eco_class_cd          "RIB_DstEcoClassCd_TBL"    := "RIB_DstEcoClassCd_TBL"();

   cursor c_tax_regime is
   select v.value
     from TABLE(CAST(L_tbl_diff_tax_regime as "RIB_DstDiffTaxRegime_TBL" )) v
     order by 1;

   cursor c_eco_class is
   select v.value
     from TABLE(CAST(L_tbl_obj_eco_class_cd as "RIB_DstEcoClassCd_TBL" )) v
     order by 1;

   L_nv_tbl                 "RIB_NameValPairRBO_TBL" := "RIB_NameValPairRBO_TBL"();
   L_RIB_NameValPairRBO_REC "RIB_NameValPairRBO_REC" := "RIB_NameValPairRBO_REC"(0,null,null);

   cursor c_nv_pairs is
      select listagg (inner.name_value_pair_id, '~')
                within group (order by inner.name_value_pair_id)
        from (select distinct name_value_pair_id
                from TABLE(CAST(L_nv_tbl as "RIB_NameValPairRBO_TBL")) v,
                     l10n_br_extax_help_nv_pair help
               where v.name  = help.name
                 and v.value = help.value) inner
       order by 1;

   L_item_service_code l10n_br_extax_stg_cost.service_code%TYPE := NULL;

BEGIN

   if I_TaxData is not null AND I_Taxdata.fiscdocchnktaxrbo_tbl.count > 0 then
      for a IN I_Taxdata.fiscdocchnktaxrbo_tbl.FIRST..I_Taxdata.fiscdocchnktaxrbo_tbl.LAST LOOP

         if I_Taxdata.fiscdocchnktaxrbo_tbl is not null AND I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl.count > 0 then
            for i IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl.FIRST..I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl.LAST LOOP
      
               --Grab location stuff in local variables
               L_effective_date          := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).FISCAL_DOCUMENT_DATE;
      
               L_loc_jurisdiction_code := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DST_ADDR_CITY_ID;
               L_loc_cnpj              := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DST_FEDERAL_TAX_REG_ID;
      
               L_loc_cnae_code := null;
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTECOCLASSCD_TBL is not null and
                  I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTECOCLASSCD_TBL.COUNT > 0 then
                  L_first := null;
                  L_tbl_obj_eco_class_cd := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTECOCLASSCD_TBL;
      
                  for g in c_eco_class loop
                     L_loc_cnae_code := L_loc_cnae_code || L_first ||g.value;
                     L_first := '~';
      
                  end loop;
               end if;
      
               L_nv_tbl.delete;
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL is not NULL 
                and I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL.count > 0 then 
                  for g in I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL.FIRST ..
                           I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL.LAST LOOP  
      
                     L_RIB_NameValPairRBO_REC.name  := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL(g).NAMEVALPAIRRBO.name;
                     L_RIB_NameValPairRBO_REC.value := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL(g).NAMEVALPAIRRBO.value;
                     L_nv_tbl.extend;
                     L_nv_tbl(L_nv_tbl.count) := L_RIB_NameValPairRBO_REC;
                   end LOOP;
                end if;
      
               L_loc_nv_pair_concat := null;
               open c_nv_pairs;
               fetch c_nv_pairs into L_loc_nv_pair_concat;
               close c_nv_pairs;
      
               L_loc_iss_contrib_ind := 'N';
               L_loc_ipi_ind := 'N';
               L_loc_icms_contrib_ind := 'N';
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL is not null and
                  I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL.COUNT > 0 then
      
                  for g IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL.FIRST..
                           I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL.LAST LOOP
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL(g).value = 'ISS' then
                        L_loc_iss_contrib_ind := 'Y';
                     end if;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL(g).value = 'IPI' then
                        L_loc_ipi_ind := 'Y';
                     end if;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL(g).value = 'ICMS' then
                        L_loc_icms_contrib_ind := 'Y';
                     end if;
      
                  end loop;
               end if;
               L_results_tbl(L_ctr).loc_iss_contrib_ind    := L_loc_iss_contrib_ind;
               L_results_tbl(L_ctr).loc_ipi_ind            := L_loc_ipi_ind;
               L_results_tbl(L_ctr).loc_icms_contrib_ind   := L_loc_icms_contrib_ind;

               --Item stuff for current location
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL.count > 0 then
                  for j IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL.FIRST..
                           I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL.LAST LOOP
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PrdItemTaxRBO_TBL is not NULL then
                        for k IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL.FIRST..
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL.LAST LOOP
                           L_results_tbl(L_ctr).item_origin_code :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).item_origin;
      
                           L_results_tbl(L_ctr).item_classification_id :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).fiscal_classification_code;
      
                           L_fiscal_code :=  
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).ext_fiscal_class_code;
      
                           if instr(L_fiscal_code,'.') > 0 then
                              L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE :=
                                 SUBSTR(L_fiscal_code,1, INSTRB(L_fiscal_code,'.',1,1) -1);
      
      
                              L_results_tbl(L_ctr).ITEM_PAUTA_CODE:=
                                 SUBSTR(L_fiscal_code,INSTRB(L_fiscal_code,'.',1,1) + 1);
      
                           else -- the fiscal_class_code contains just the ncm_char_code or null
                              L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE := L_fiscal_code;
                           end if;
      
                           L_results_tbl(L_ctr).ITEM_EX_IPI :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).ipi_exception_code;
                           L_results_tbl(L_ctr).state_of_manufacture := 
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).STATE_OF_MANUFACTURE;
                           L_results_tbl(L_ctr).pharma_list_type := 
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).PHARMA_LIST_TYPE;
      
                           L_results_tbl(L_ctr).item_xform_ind := 
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).is_transformed_item;
      
                           L_results_tbl(L_ctr).ITEM_SERVICE_IND :=  'N' ; 
                        end loop;
                     end if;
      
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SvcItemTaxRBO_TBL is not NULL then
                        for k IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL.FIRST..
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL.LAST LOOP

                           L_fiscal_code :=  
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL(k).ext_fiscal_class_code;
                           if instr(L_fiscal_code,'.') > 0 then
                              L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE :=
                                 SUBSTR(L_fiscal_code,1, INSTRB(L_fiscal_code,'.',1,1) -1);


                              L_results_tbl(L_ctr).ITEM_PAUTA_CODE:=
                                 SUBSTR(L_fiscal_code,INSTRB(L_fiscal_code,'.',1,1) + 1);

                           else -- the fiscal_class_code contains just the ncm_char_code or null
                              L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE := L_fiscal_code;
                           end if;

                           L_results_tbl(L_ctr).ITEM_FEDERAL_SERVICE :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL(k).federal_service_code;
                           L_results_tbl(L_ctr).ITEM_SERVICE_CODE :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL(k).dst_service_code;

                              L_results_tbl(L_ctr).item_xform_ind := 'N'; --assumes service items are not xform
                              L_results_tbl(L_ctr).ITEM_SERVICE_IND :=  'Y' ;
                        end loop;
                     end if;

                     --

                     L_results_tbl(L_ctr).item_dim_object :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).dim_object;
                     if L_results_tbl(L_ctr).item_dim_object = 'Dummy' then
                        L_results_tbl(L_ctr).item_dim_object := NULL;
                     end if;
                     L_results_tbl(L_ctr).item_length :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).length;
                     L_results_tbl(L_ctr).item_lwh_uom :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).lwh_uom;
                     L_results_tbl(L_ctr).item_weight :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).weight;
                     L_results_tbl(L_ctr).item_net_weight :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).net_weight;
                     L_results_tbl(L_ctr).item_weight_uom :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).weight_uom;
                     L_results_tbl(L_ctr).item_liquid_volume :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).liquid_volume;
                     L_results_tbl(L_ctr).item_liquid_volume_uom :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).liquid_volume_uom;

                     L_nv_tbl.delete;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).NameValPairRBO_TBL is not null then  
                        if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).NameValPairRBO_TBL.count > 0 then  
                           for l IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).NameValPairRBO_TBL.FIRST..
                               I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).NameValPairRBO_TBL.LAST LOOP 
      
                               L_nv_tbl.extend;
                               L_nv_tbl(L_nv_tbl.count) :=I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).NameValPairRBO_TBL(l); 
         
                           end loop;
                           open c_nv_pairs;
                           fetch c_nv_pairs into L_results_tbl(L_ctr).item_name_value_pair_id_concat;
                           close c_nv_pairs;
                         end if;
                     end if;
                     --push location level attribs to item level
                     L_results_tbl(L_ctr).loc_jurisdiction_code  := L_loc_jurisdiction_code;
                     L_results_tbl(L_ctr).loc_cnae_code          := L_loc_cnae_code;
                     L_results_tbl(L_ctr).loc_name_value_pair_id_concat  := L_loc_nv_pair_concat;
                     L_results_tbl(L_ctr).item_name_value_pair_id_concat := L_item_nv_pair_concat;
                     L_results_tbl(L_ctr).loc_iss_contrib_ind    := L_loc_iss_contrib_ind;
                     L_results_tbl(L_ctr).loc_ipi_ind            := L_loc_ipi_ind;
                     L_results_tbl(L_ctr).loc_icms_contrib_ind   := L_loc_icms_contrib_ind;
                     L_results_tbl(L_ctr).loc_cnpj               := L_loc_cnpj;
      
                     L_results_tbl(L_ctr).effective_date         := L_effective_date;
                     L_results_tbl(L_ctr).item                   := nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).item_id,'-1');
                     L_results_tbl(L_ctr).pack_no                := nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).pack_item_id,'-1');
                     L_cum_tax_pct := 0;
                     L_cum_tax_pct_ctr := 0;
                     L_cum_tax_value := 0;
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL.count > 0 then
                        for k IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL.FIRST..
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL.LAST LOOP
      
                           L_results_breakout_tbl(L_breakout_ctr).loc_jurisdiction_code := L_results_tbl(L_ctr).loc_jurisdiction_code;
                           L_results_breakout_tbl(L_breakout_ctr).loc_cnae_code         := L_results_tbl(L_ctr).loc_cnae_code;
                           L_results_breakout_tbl(L_breakout_ctr).loc_name_value_pair_id_concat         
                              := L_results_tbl(L_ctr).loc_name_value_pair_id_concat;
                           L_results_breakout_tbl(L_breakout_ctr).item_name_value_pair_id_concat         
                              := L_results_tbl(L_ctr).loc_name_value_pair_id_concat;
                           L_results_breakout_tbl(L_breakout_ctr).loc_iss_contrib_ind   := L_results_tbl(L_ctr).loc_iss_contrib_ind;
                           L_results_breakout_tbl(L_breakout_ctr).loc_ipi_ind           := L_results_tbl(L_ctr).loc_ipi_ind;
                           L_results_breakout_tbl(L_breakout_ctr).loc_icms_contrib_ind  := L_results_tbl(L_ctr).loc_icms_contrib_ind;
                           L_results_breakout_tbl(L_breakout_ctr).loc_cnpj              := L_results_tbl(L_ctr).loc_cnpj;
                           --
                           L_results_breakout_tbl(L_breakout_ctr).item_origin_code      := L_results_tbl(L_ctr).item_origin_code;
                           L_results_breakout_tbl(L_breakout_ctr).item_classification_id := L_results_tbl(L_ctr).item_classification_id;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_NCM_CHAR_CODE := L_results_tbl(L_ctr).item_ncm_char_code;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_EX_IPI := L_results_tbl(L_ctr).item_ex_ipi;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_PAUTA_CODE:= L_results_tbl(L_ctr).item_pauta_code;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_SERVICE_IND := L_results_tbl(L_ctr).item_service_ind ;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_SERVICE_CODE := L_results_tbl(L_ctr).item_service_code;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_FEDERAL_SERVICE := L_results_tbl(L_ctr).item_federal_service;
      
                           L_results_breakout_tbl(L_breakout_ctr).item_dim_object := L_results_tbl(L_ctr).item_dim_object ;
                           L_results_breakout_tbl(L_breakout_ctr).item_length := L_results_tbl(L_ctr).item_length ;
                           L_results_breakout_tbl(L_breakout_ctr).item_lwh_uom := L_results_tbl(L_ctr).item_lwh_uom;
                           L_results_breakout_tbl(L_breakout_ctr).item_weight := L_results_tbl(L_ctr).item_weight ;
                           L_results_breakout_tbl(L_breakout_ctr).item_net_weight := L_results_tbl(L_ctr).item_net_weight ;
                           L_results_breakout_tbl(L_breakout_ctr).item_weight_uom := L_results_tbl(L_ctr).item_weight_uom ;
                           L_results_breakout_tbl(L_breakout_ctr).item_liquid_volume := L_results_tbl(L_ctr).item_liquid_volume ;
                           L_results_breakout_tbl(L_breakout_ctr).item_liquid_volume_uom := L_results_tbl(L_ctr).item_liquid_volume_uom ;
                           L_results_breakout_tbl(L_breakout_ctr).item_xform_ind := L_results_tbl(L_ctr).item_xform_ind ;
                           L_results_breakout_tbl(L_breakout_ctr).state_of_manufacture := L_results_tbl(L_ctr).state_of_manufacture ;
                           L_results_breakout_tbl(L_breakout_ctr).pharma_list_type := L_results_tbl(L_ctr).pharma_list_type ;
                           --
                           L_results_breakout_tbl(L_breakout_ctr).effective_date         := L_results_tbl(L_ctr).effective_date;
                           L_results_breakout_tbl(L_breakout_ctr).pack_no                := L_results_tbl(L_ctr).pack_no;
                           L_results_breakout_tbl(L_breakout_ctr).item                   := L_results_tbl(L_ctr).item;
                           L_results_breakout_tbl(L_breakout_ctr).tax_code               :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_code;
                           L_results_breakout_tbl(L_breakout_ctr).tax_amount             :=
                              nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_amount,0);
                           L_results_breakout_tbl(L_breakout_ctr).tax_rate               :=
                              nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_rate,0);
                           L_results_breakout_tbl(L_breakout_ctr).tax_basis_amount       :=
                              nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_basis_amount,0);
                           L_results_breakout_tbl(L_breakout_ctr).calculation_basis       :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_rate_type;
      
                           L_breakout_ctr := L_breakout_ctr + 1;
      
                           ----
      
                           if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_rate_type = 'P' then
                              L_cum_tax_pct := L_cum_tax_pct +
                                 nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_rate,0);
                              L_cum_tax_pct_ctr := L_cum_tax_pct_ctr + 1;
                           else
                              L_cum_tax_value := L_cum_tax_value +
                                 nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_rate,0);
                           end if;
      
      
                        end loop;
                     end if;
      
                     if (L_cum_tax_pct_ctr != 0) then
                           L_results_tbl(L_ctr).cum_tax_pct := L_cum_tax_pct;
                     else
                           L_results_tbl(L_ctr).cum_tax_pct := 0;
                     end if;
                     L_results_tbl(L_ctr).cum_tax_value := L_cum_tax_value;
      
      
                     L_ctr := L_ctr + 1;
      
                  end loop;
               end if;
      
            end loop;
         end if;
      end loop;
   end if;

   --populate l10n_br_extax_res_retail

   FORALL i in L_results_tbl.FIRST..L_results_tbl.LAST
      insert into l10n_br_extax_res_retail (city,
                           cnae_code,
                           iss,
                           ipi,
                           icms,
                           cnpj,
                           loc_name_value_pair_id_concat,
                           origin_code,
                           service_ind,
                           classification_id,
                           ncm_char_code,
                           ex_ipi,
                           pauta_code,
                           service_code,
                           federal_service,
                           dim_object,
                           length,
                           lwh_uom,
                           weight,
                           net_weight,
                           weight_uom,
                           liquid_volume,
                           liquid_volume_uom,
                           item_xform_ind,
                           state_of_manufacture,
                           pharma_list_type,
                           item_name_value_pair_id_concat,
                           effective_date,
                           pack_no,
                           item,
                           cum_tax_pct,
                           cum_tax_value,
                           process_id)
      values (L_results_tbl(i).loc_jurisdiction_code,
              L_results_tbl(i).loc_cnae_code,
              L_results_tbl(i).loc_iss_contrib_ind,
              L_results_tbl(i).loc_ipi_ind,
              L_results_tbl(i).loc_icms_contrib_ind,
              L_results_tbl(i).loc_cnpj,
              nvl(L_results_tbl(i).loc_name_value_pair_id_concat,'-1'),
              nvl(L_results_tbl(i).item_origin_code,'0'),
              nvl(L_results_tbl(i).item_service_ind,'N'),
              nvl(L_results_tbl(i).item_classification_id,'-1'),
              nvl(L_results_tbl(i).item_ncm_char_code,'-1'),
              nvl(L_results_tbl(i).item_ex_ipi,'-1'),
              nvl(L_results_tbl(i).item_pauta_code,'-1'),
              nvl(L_results_tbl(i).item_service_code,'-1'),
              nvl(L_results_tbl(i).item_federal_service,'-1'),
              nvl(L_results_tbl(i).item_dim_object, '-999'),
              nvl(L_results_tbl(i).item_length,'-999'),
              nvl(L_results_tbl(i).item_lwh_uom,'-999'),
              nvl(L_results_tbl(i).item_weight,'-999'),
              nvl(L_results_tbl(i).item_net_weight,'-999'),
              nvl(L_results_tbl(i).item_weight_uom,'-999'),
              nvl(L_results_tbl(i).item_liquid_volume,'-999'),
              nvl(L_results_tbl(i).item_liquid_volume_uom,'-999'),
              L_results_tbl(i).item_xform_ind,
              nvl(L_results_tbl(i).state_of_manufacture,'-1'),
              nvl(L_results_tbl(i).pharma_list_type,'-1'),
              nvl(L_results_tbl(i).item_name_value_pair_id_concat,'-1'),
              L_results_tbl(i).effective_date,
              nvl(L_results_tbl(i).pack_no,'-1'),
              L_results_tbl(i).item,
              L_results_tbl(i).cum_tax_pct,
              L_results_tbl(i).cum_tax_value,
              I_process_id);



   FORALL i in L_results_breakout_tbl.FIRST..L_results_breakout_tbl.LAST
      insert into l10n_br_extax_res_retail_det (city,
                                    cnae_code,
                                    iss,
                                    ipi,
                                    icms,
                                    cnpj,
                                    loc_name_value_pair_id_concat,
                                    origin_code,
                                    service_ind,
                                    classification_id,
                                    ncm_char_code,
                                    ex_ipi,
                                    pauta_code,
                                    service_code,
                                    federal_service,
                                    dim_object,
                                    length,
                                    lwh_uom,
                                    weight,
                                    net_weight,
                                    weight_uom,
                                    liquid_volume,
                                    liquid_volume_uom,
                                    item_xform_ind,
                                    state_of_manufacture,
                                    pharma_list_type,
                                    item_name_value_pair_id_concat,
                                    effective_date,
                                    pack_no,
                                    item,
                                    tax_code,
                                    tax_rate,
                                    calculation_basis,
                                    tax_amount,
                                    process_id)
      values (L_results_breakout_tbl(i).loc_jurisdiction_code,
              L_results_breakout_tbl(i).loc_cnae_code,
              L_results_breakout_tbl(i).loc_iss_contrib_ind,
              L_results_breakout_tbl(i).loc_ipi_ind,
              L_results_breakout_tbl(i).loc_icms_contrib_ind,
              L_results_breakout_tbl(i).loc_cnpj,
              nvl(L_results_breakout_tbl(i).loc_name_value_pair_id_concat,'-1'),
              nvl(L_results_breakout_tbl(i).item_origin_code,'0'),
              nvl(L_results_breakout_tbl(i).item_service_ind,'N'),
              nvl(L_results_breakout_tbl(i).item_classification_id,'-1'),
              nvl(L_results_breakout_tbl(i).item_ncm_char_code,'-1'),
              nvl(L_results_breakout_tbl(i).item_ex_ipi,'-1'),
              nvl(L_results_breakout_tbl(i).item_pauta_code,'-1'),
              nvl(L_results_breakout_tbl(i).item_service_code,'-1'),
              nvl(L_results_breakout_tbl(i).item_federal_service,'-1'),
              nvl(L_results_breakout_tbl(i).item_dim_object,'-999'),
              nvl(L_results_breakout_tbl(i).item_length,'-999'),
              nvl(L_results_breakout_tbl(i).item_lwh_uom,'-999'),
              nvl(L_results_breakout_tbl(i).item_weight,'-999'),
              nvl(L_results_breakout_tbl(i).item_net_weight,'-999'),
              nvl(L_results_breakout_tbl(i).item_weight_uom,'-999'),
              nvl(L_results_breakout_tbl(i).item_liquid_volume,'-999'),
              nvl(L_results_breakout_tbl(i).item_liquid_volume_uom, '-999'),
              L_results_breakout_tbl(i).item_xform_ind,
              nvl(L_results_breakout_tbl(i).state_of_manufacture,'-1'),
              nvl(L_results_breakout_tbl(i).pharma_list_type,'-1'),
              nvl(L_results_breakout_tbl(i).item_name_value_pair_id_concat,'-1'),
              L_results_breakout_tbl(i).effective_date,
              nvl(L_results_breakout_tbl(i).pack_no,'-1'),
              L_results_breakout_tbl(i).item,
              L_results_breakout_tbl(i).tax_code,
              L_results_breakout_tbl(i).tax_rate,
              L_results_breakout_tbl(i).calculation_basis,
              L_results_breakout_tbl(i).tax_amount,
              I_process_id);


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SET_TAX_RESPONSE_DATA_RETAIL;
--------------------------------------------------------
FUNCTION RESULT_TO_IL(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.RESULT_TO_IL';

BEGIN

   -- Non Pack
   insert into l10n_br_extax_help_retail_gtt (
           item,
           loc,
           loc_type,
           effective_from_date,
           item_parent,
           item_grandparent,
           city,
           cnae_code,
           iss,
           ipi,
           icms,
           cnpj,
           loc_name_value_pair_id_concat,
           origin_code,
           service_ind,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           item_xform_ind,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           pack_no,
           cum_tax_pct,
           cum_tax_value)
   select /*+ parallel(il,12) */
          il.item,
          il.loc,
          inner.entity_type,
          inner.effective_from_date,
          il.item_parent,
          il.item_grandparent,
          inner.city,
          inner.cnae_code,
          inner.iss,
          inner.ipi,
          inner.icms,
          inner.cnpj,
          inner.loc_name_value_pair_id_concat,
          --
          inner.origin_code,
          inner.service_ind,
          inner.classification_id,
          inner.ncm_char_code,
          inner.ex_ipi,
          inner.pauta_code,
          inner.service_code,
          inner.federal_service,
          inner.dim_object,
          inner.length,
          inner.lwh_uom,
          inner.weight,
          inner.net_weight,
          inner.weight_uom,
          inner.liquid_volume,
          inner.liquid_volume_uom,
          inner.item_xform_ind,
          inner.state_of_manufacture,
          inner.pharma_list_type,
          inner.item_name_value_pair_id_concat,
          '-1',  -- Pack No
          --
          inner.cum_tax_pct,
          inner.cum_tax_value
     from item_loc il,
          (select isc.item,
                  nvl(iscd.dim_object, '-999') dim_object,
                  nvl(iscd.weight,'-999') weight,
                  nvl(iscd.net_weight,'-999') net_weight,
                  nvl(iscd.weight_uom,'-999') weight_uom,
                  nvl(iscd.length,'-999') length,
                  nvl(iscd.lwh_uom,'-999') lwh_uom,
                  nvl(iscd.liquid_volume,'-999') liquid_volume,
                  nvl(iscd.liquid_volume_uom,'-999') liquid_volume_uom
             from item_supp_country isc,
                  item_supp_country_dim iscd
            where iscd.dim_object (+) = 'EA'
              and isc.item = iscd.item (+)
              and isc.supplier = iscd.supplier (+)
              and isc.origin_country_id = iscd.origin_country (+)
              and isc.primary_supp_ind = 'Y'
              and isc.primary_country_ind = 'Y' ) dim,
          (select /*+ parallel(item_map,8) parallel(result,8)*/
                  distinct item_map.item,
                  loc_map.entity loc,
                  loc_map.entity_type,
                  loc_map.loc_type,
                  result.effective_date effective_from_date,
                  --
                  result.city,
                  result.cnae_code,
                  result.iss,
                  result.ipi,
                  result.icms,
                  result.cnpj,
                  result.loc_name_value_pair_id_concat,
                  --
                  result.origin_code,
                  result.service_ind,
                  result.classification_id,
                  result.ncm_char_code,
                  result.ex_ipi,
                  result.pauta_code,
                  result.service_code,
                  result.federal_service,
                  result.dim_object,
                  result.length,
                  result.lwh_uom,
                  result.weight,
                  result.net_weight,
                  result.weight_uom,
                  result.liquid_volume,
                  result.liquid_volume_uom,
                  result.item_xform_ind,
                  result.state_of_manufacture,
                  result.pharma_list_type,
                  result.item_name_value_pair_id_concat,
                  --
                  result.cum_tax_pct,
                  result.cum_tax_value
             from l10n_br_extax_res_retail result,
                  l10n_br_extax_dest_group_help loc_map,
                  item_master im,
                  (select v.*,
                          nv.item_name_value_pair_id_concat
                     from v_br_item_fiscal_attrib v,
                          (select entity item,
                                  listagg (name_value_pair_id, '~') within group 
                                     (order by name_value_pair_id) item_name_value_pair_id_concat
                             from l10n_br_extax_help_nv_pair
                            where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
                            group by entity) nv
                    where v.item         = nv.item(+)) item_map
            where result.origin_code               = '0'
              and item_map.origin_code             = '0'
              and result.ncm_char_code             = nvl(item_map.ncm_char_code,'-1')
              and result.pauta_code                = nvl(item_map.pauta_code,'-1')
              --
              and ((result.service_ind = 'N'
                    and result.classification_id         = nvl(item_map.classification_id,'-1')
                    and result.ex_ipi                    = nvl(item_map.ex_ipi,'-1')
                    and result.state_of_manufacture      = nvl(item_map.state_of_manufacture,'-1')
                    and im.item_xform_ind                = nvl(result.item_xform_ind,im.item_xform_ind)
                    and result.pharma_list_type          = nvl(item_map.pharma_list_type,'-1'))
                  or
                   (result.service_ind = 'Y'
                    and result.service_code              = nvl(item_map.service_code,'-1')
                    and result.federal_service           = nvl(item_map.federal_service,'-1')))
              --
              and result.item_name_value_pair_id_concat = nvl(item_map.item_name_value_pair_id_concat,'-1')
              and result.pack_no                   = '-1'
              --
              and loc_map.city                     is not null
              and loc_map.cnae_code_concatenation  is not null
              --
              and result.city                      = loc_map.city
              and result.cnae_code                 = loc_map.cnae_code_concatenation
              and result.cnpj                      = loc_map.cnpj
              and result.iss                       = nvl(loc_map.iss_contrib_ind,'N')
              and result.ipi                       = nvl(loc_map.ipi_ind,'N')
              and result.icms                      = nvl(loc_map.icms_contrib_ind,'N') 
              --
              and loc_map.entity_type                 != L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
              and result.loc_name_value_pair_id_concat = nvl(loc_map.name_value_pair_id_concat,'-1') 
              --
              and item_map.item     = im.item
              and im.status         = 'A'
              and im.tran_level     = im.item_level
              and im.pack_ind       = 'N'
              and im.sellable_ind   = 'Y'
            ) inner
  where il.item           = inner.item
    and il.loc            = inner.loc
    and inner.loc_type    = il.loc_type
    --
    and inner.item              = dim.item (+)
    and inner.dim_object        = dim.dim_object (+)
    and inner.length            = dim.length (+)
    and inner.lwh_uom           = dim.lwh_uom (+)
    and inner.weight            = dim.weight (+)
    and inner.net_weight        = dim.net_weight (+)
    and inner.weight_uom        = dim.weight_uom (+)
    and inner.liquid_volume     = dim.liquid_volume (+)
    and inner.liquid_volume_uom = dim.liquid_volume_uom (+);

   -- Simple and Complex Pack
   insert into l10n_br_extax_help_retail_gtt (
           item,
           loc,
           loc_type,
           effective_from_date,
           item_parent,
           item_grandparent,
           city,
           cnae_code,
           iss,
           ipi,
           icms,
           cnpj,
           loc_name_value_pair_id_concat,
           origin_code,
           service_ind,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           item_xform_ind,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           pack_no,
           cum_tax_pct,
           cum_tax_value)
  select /*+ parallel(il,12) parallel(im,8)*/ distinct
          il.item,
          il.loc,
          inner.entity_type,
          inner.effective_from_date,
          il.item_parent,
          il.item_grandparent,
          inner.city,
          inner.cnae_code,
          inner.iss,
          inner.ipi,
          inner.icms,
          inner.cnpj,
          inner.loc_name_value_pair_id_concat,
          --
          null, --inner.origin_code,
          null, --inner.service_ind,
          null, --inner.classification_id,
          null, --inner.ncm_char_code,
          null, --inner.ex_ipi,
          null, --inner.pauta_code,
          null, --inner.service_code,
          null, --inner.federal_service,
          null, --inner.dim_object,
          null, --inner.length,
          null, --inner.lwh_uom,
          null, --inner.weight,
          null, --inner.net_weight,
          null, --inner.weight_uom,
          null, --inner.liquid_volume,
          null, --inner.liquid_volume_uom,
          null, --item_xform_ind,
          null, --state_of_manufacture,
          null, --pharma_list_type,
          null, --item_name_value_pair_id_concat,
          inner.pack_no,
          --
          inner.cum_tax_pct,
          inner.cum_tax_value
     from item_loc il,
          item_master im,
          v_packsku_qty pi,
          (select /*+ parallel(item_map,8) parallel(result,8)*/
                  result.pack_no,
                  loc_map.entity loc,
                  loc_map.entity_type,
                  loc_map.loc_type,
                  result.effective_date effective_from_date,
                  --
                  result.city,
                  result.cnae_code,
                  result.iss,
                  result.ipi,
                  result.icms,
                  result.cnpj,
                  result.loc_name_value_pair_id_concat,
                  --
                  avg(result.cum_tax_pct) cum_tax_pct,
                  avg(result.cum_tax_value) cum_tax_value
             from l10n_br_extax_res_retail result,
                  l10n_br_extax_dest_group_help loc_map,
                  (select v.*,
                          nv.item_name_value_pair_id_concat
                     from v_br_item_country_attributes v,
                          (select entity item,
                                  listagg (name_value_pair_id, '~') within group 
                                     (order by name_value_pair_id) item_name_value_pair_id_concat
                             from l10n_br_extax_help_nv_pair
                            where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
                            group by entity) nv
                    where v.item         = nv.item(+)) item_map
            where result.origin_code       = item_map.origin_code
              and result.ncm_char_code     = nvl(item_map.ncm_char_code,'-1')
              and ((result.service_ind = 'N'
                    and result.classification_id     = nvl(item_map.classification_id,'-1')
                    and result.ex_ipi                = nvl(item_map.ex_ipi,'-1')
                    and result.state_of_manufacture  = nvl(item_map.state_of_manufacture,'-1')
                    and result.pharma_list_type      = nvl(item_map.pharma_list_type,'-1'))
                  or
                  (result.service_ind = 'Y'
                   and result.service_code     = nvl(item_map.service_code,'-1')
                   and result.federal_service  = nvl(item_map.federal_service,'-1')))
              and result.pauta_code            = nvl(item_map.pauta_code,'-1')
              --
              and result.pack_no           <> '-1'  -- Simple and Complex Pack does populate pack_no
              and result.item              = item_map.item
              --
              and loc_map.city                     is not null
              and loc_map.cnae_code_concatenation  is not null
              --
              and result.city      = loc_map.city
              and result.cnae_code = loc_map.cnae_code_concatenation
              and result.cnpj      = loc_map.cnpj
              and result.iss       = nvl(loc_map.iss_contrib_ind,'N')
              and result.ipi       = nvl(loc_map.ipi_ind,'N')
              and result.icms      = nvl(loc_map.icms_contrib_ind,'N')
              and loc_map.entity_type                 != L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
              and result.loc_name_value_pair_id_concat = nvl(loc_map.name_value_pair_id_concat,'-1') 
        group by result.pack_no,
                 loc_map.entity,
                 loc_map.entity_type,
                 loc_map.loc_type,
                 result.effective_date,
                 --
                 result.city,
                 result.cnae_code,
                 result.iss,
                 result.ipi,
                 result.icms,
                 result.cnpj,
                 result.loc_name_value_pair_id_concat
           ) inner
  where il.item            = pi.pack_no
    and inner.loc          = to_char(il.loc)
    and il.loc             = inner.loc
    and il.loc_type        = inner.loc_type
    --
    and im.status          = 'A'
    and im.tran_level      = im.item_level
    and im.pack_ind        = 'Y'
    and im.sellable_ind    = 'Y'
    and im.item            = pi.pack_no
    --
    and pi.pack_no         = inner.pack_no;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RESULT_TO_IL;
--------------------------------------------------------
FUNCTION RECLASS_ITEM_RESULT_TO_IL(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.RECLASS_ITEM_RESULT_TO_IL';

BEGIN

   -- For Non Pack and Simple Pack
   insert into l10n_br_extax_help_retail_gtt (
           item,
           loc,
           loc_type,
           effective_from_date,
           item_parent,
           item_grandparent,
           city,
           cnae_code,
           iss,
           ipi,
           icms,
           cnpj,
           loc_name_value_pair_id_concat,
           origin_code,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           item_xform_ind,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           pack_no,
           cum_tax_pct,
           cum_tax_value)
   select /*+ parallel(il,12) parallel(im,8)*/ distinct
          il.item,
          il.loc,
          inner.entity_type,
          inner.effective_from_date,
          il.item_parent,
          il.item_grandparent,
          inner.city,
          inner.cnae_code,
          inner.iss,
          inner.ipi,
          inner.icms,
          inner.cnpj,
          inner.loc_name_value_pair_id_concat,
          --
          inner.origin_code,
          inner.classification_id,
          inner.ncm_char_code,
          inner.ex_ipi,
          inner.pauta_code,
          inner.service_code,
          inner.federal_service,
          inner.dim_object,
          inner.length,
          inner.lwh_uom,
          inner.weight,
          inner.net_weight,
          inner.weight_uom,
          inner.liquid_volume,
          inner.liquid_volume_uom,
          inner.item_xform_ind,
          inner.state_of_manufacture,
          inner.pharma_list_type,
          inner.item_name_value_pair_id_concat,
          '-1', --pack_no,
          --
          inner.cum_tax_pct,
          inner.cum_tax_value
     from item_loc il,
          item_master im,
          (select isc.item,
                  nvl(iscd.dim_object, '-999') dim_object,
                  nvl(iscd.weight,'-999') weight,
                  nvl(iscd.net_weight,'-999') net_weight,
                  nvl(iscd.weight_uom,'-999') weight_uom,
                  nvl(iscd.length,'-999') length,
                  nvl(iscd.lwh_uom,'-999') lwh_uom,
                  nvl(iscd.liquid_volume,'-999') liquid_volume,
                  nvl(iscd.liquid_volume_uom,'-999') liquid_volume_uom
             from item_supp_country isc,
                  item_supp_country_dim iscd
            where iscd.dim_object (+) = 'EA'
              and isc.item = iscd.item (+)
              and isc.supplier = iscd.supplier (+)
              and isc.origin_country_id = iscd.origin_country (+)
              and isc.primary_supp_ind = 'Y'
              and isc.primary_country_ind = 'Y' ) dim,
          (select /*+ parallel(item_map,8) parallel(result,8)*/
                  distinct item_map.item,
                  loc_map.entity loc,
                  loc_map.entity_type,
                  loc_map.loc_type,
                  result.effective_date effective_from_date,
                  --
                  result.city,
                  result.cnae_code,
                  result.iss,
                  result.ipi,
                  result.icms,
                  result.cnpj,
                  result.loc_name_value_pair_id_concat,
                  --
                  result.origin_code,
                  result.classification_id,
                  result.ncm_char_code,
                  result.ex_ipi,
                  result.pauta_code,
                  result.service_code,
                  result.federal_service,
                  result.dim_object,
                  result.length,
                  result.lwh_uom,
                  result.weight,
                  result.net_weight,
                  result.weight_uom,
                  result.liquid_volume,
                  result.liquid_volume_uom,
                  result.item_xform_ind,
                  result.state_of_manufacture,
                  result.pharma_list_type,
                  result.item_name_value_pair_id_concat,
                  --
                  result.cum_tax_pct,
                  result.cum_tax_value
             from l10n_br_extax_res_retail result,
                  l10n_br_fiscal_reclass fi,
                  l10n_br_extax_dest_group_help loc_map,
                  (select v.*,
                          nv.item_name_value_pair_id_concat
                     from v_br_item_country_attributes v,
                          (select entity item,
                                  listagg (name_value_pair_id, '~') within group 
                                     (order by name_value_pair_id) item_name_value_pair_id_concat
                             from l10n_br_extax_help_nv_pair
                            where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
                            group by entity) nv
                    where v.item         = nv.item(+)) item_map
            where nvl(item_map.ncm_char_code,'-1')       = nvl(fi.old_ncm_char_code, '-1')
              and nvl(item_map.pauta_code,'-1')          = nvl(fi.old_pauta_code, '-1')
              and nvl(item_map.classification_id,'-1')   = nvl(fi.old_ncm_code, '-1')
              and nvl(item_map.ex_ipi,'-1')              = nvl(fi.old_ex_ipi_code, '-1')
              and nvl(item_map.service_code,'-1')        = nvl(fi.old_mtr_service_code, '-1')
              and nvl(item_map.federal_service ,'-1')    = nvl(fi.old_fed_service_code, '-1')
              and nvl(item_map.state_of_manufacture ,'-1') = nvl(fi.old_state_of_manufacture, '-1')
              and nvl(item_map.pharma_list_type ,'-1')     = nvl(fi.old_pharma_list_type, '-1')
              --
              and result.ncm_char_code      = nvl(fi.new_ncm_char_code, '-1')
              and result.pauta_code         = nvl(fi.new_pauta_code, '-1')
              and result.classification_id  = nvl(fi.new_ncm_code, '-1')
              and result.ex_ipi             = nvl(fi.new_ex_ipi_code,'-1')
              and result.service_code       = nvl(fi.new_mtr_service_code,'-1')
              and result.federal_service    = nvl(fi.new_fed_service_code, '-1')
              and result.state_of_manufacture  = nvl(fi.new_state_of_manufacture, '-1')
              and result.pharma_list_type      = nvl(fi.new_pharma_list_type, '-1')
              and result.item_name_value_pair_id_concat = nvl(item_map.item_name_value_pair_id_concat,'-1')
              --
              and fi.item                   = item_map.item
              --
              and loc_map.city                     is not null
              and loc_map.cnae_code_concatenation  is not null
              --
              and result.pack_no                    = '-1'
              and result.city                       = loc_map.city
              and result.cnae_code                  = loc_map.cnae_code_concatenation
              and result.cnpj                       = loc_map.cnpj
              and result.iss                        = nvl(loc_map.iss_contrib_ind,'N')
              and result.ipi                        = nvl(loc_map.ipi_ind,'N')
              and result.icms                       = nvl(loc_map.icms_contrib_ind,'N')
              and loc_map.entity_type                 != L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
              and result.loc_name_value_pair_id_concat = nvl(loc_map.name_value_pair_id_concat, '-1')
              ) inner
  where il.item                 = im.item
    and im.status               = 'A'
    and im.tran_level           = im.item_level
    and im.pack_ind             = 'N'
    and inner.item_xform_ind    = im.item_xform_ind
    and inner.item              = im.item
    and inner.loc               = il.loc
    and inner.loc_type          = il.loc_type
    --
    and inner.item              = dim.item (+)
    and inner.dim_object        = dim.dim_object (+)
    and inner.length            = dim.length (+)
    and inner.lwh_uom           = dim.lwh_uom (+)
    and inner.weight            = dim.weight (+)
    and inner.net_weight        = dim.net_weight (+)
    and inner.weight_uom        = dim.weight_uom (+)
    and inner.liquid_volume     = dim.liquid_volume (+)
    and inner.liquid_volume_uom = dim.liquid_volume_uom (+);

   -- Simple and Complex Pack
   insert into l10n_br_extax_help_retail_gtt (
           item,
           loc,
           loc_type,
           effective_from_date,
           item_parent,
           item_grandparent,
           city,
           cnae_code,
           iss,
           ipi,
           icms,
           cnpj,
           loc_name_value_pair_id_concat,
           origin_code,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           item_xform_ind,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           pack_no,
           cum_tax_pct,
           cum_tax_value)
   select /*+ parallel(il,12) parallel(im,8)*/ distinct
          il.item,
          il.loc,
          inner.entity_type,
          inner.effective_from_date,
          il.item_parent,
          il.item_grandparent,
          inner.city,
          inner.cnae_code,
          inner.iss,
          inner.ipi,
          inner.icms,
          inner.cnpj,
          inner.loc_name_value_pair_id_concat,
          --
          null, --inner.origin_code,
          null, --inner.classification_id,
          null, --inner.ncm_char_code,
          null, --inner.ex_ipi,
          null, --inner.pauta_code,
          null, --inner.service_code,
          null, --inner.federal_service,
          null, --inner.dim_object,
          null, --inner.length,
          null, --inner.lwh_uom,
          null, --inner.weight,
          null, --inner.net_weight,
          null, --inner.weight_uom,
          null, --inner.liquid_volume,
          null, --inner.liquid_volume_uom,
          null, --im.item_xform_ind,  
          null, --inner.state_of_manufacture
          null, --inner.pharma_list_type
          null, --inner.item_name_value_pair_id_concat,
          inner.pack_no,
          --
          inner.cum_tax_pct,
          inner.cum_tax_value
     from item_loc il,
          (select /*+ parallel(item_map,8) parallel(result,8)*/
                  distinct result.pack_no,
                  loc_map.entity loc,
                  loc_map.entity_type,
                  loc_map.loc_type,
                  result.effective_date effective_from_date,
                  --
                  result.city,
                  result.cnae_code,
                  result.iss,
                  result.ipi,
                  result.icms,
                  result.cnpj,
                  result.loc_name_value_pair_id_concat,
                  --
                  avg(result.cum_tax_pct) cum_tax_pct,
                  avg(result.cum_tax_value) cum_tax_value
             from l10n_br_extax_res_retail result,
                  l10n_br_extax_dest_group_help loc_map
            where result.pack_no <> '-1'
              and loc_map.city                     is not null
              and loc_map.cnae_code_concatenation  is not null
              --
              and result.city                      = loc_map.city
              and result.cnae_code                 = loc_map.cnae_code_concatenation
              and result.cnpj                      = loc_map.cnpj
              and result.iss                       = nvl(loc_map.iss_contrib_ind,'N')
              and result.ipi                       = nvl(loc_map.ipi_ind,'N')
              and result.icms                      = nvl(loc_map.icms_contrib_ind,'N')
              and loc_map.entity_type                 != L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
              and result.loc_name_value_pair_id_concat = nvl(loc_map.name_value_pair_id_concat, '-1')
           group by result.pack_no,
                  loc_map.entity,
                  loc_map.entity_type,
                  loc_map.loc_type,
                  result.effective_date,
                  --
                  result.city,
                  result.cnae_code,
                  result.iss,
                  result.ipi,
                  result.icms,
                  result.cnpj,
                  result.loc_name_value_pair_id_concat) inner
  where il.item           = inner.pack_no
    and inner.loc         = il.loc
    and inner.loc_type    = il.loc_type;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RECLASS_ITEM_RESULT_TO_IL;
--------------------------------------------------------
FUNCTION SEED_EXTAX_FINISH(O_error_message  IN OUT VARCHAR2,
                           pos_mods_tax_ind IN VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SEED_EXTAX_FINISH';

BEGIN

  if RESULT_TO_IL(O_error_message) = FALSE then
     return FALSE;
  end if;

  insert into gtax_item_rollup (
           item,
           loc,
           loc_type,
           effective_from_date,
           item_parent,
           item_grandparent,
           cum_tax_pct,
           cum_tax_value,
           currency_code,
           create_datetime,
           create_id,
           last_update_datetime,
           last_update_id)
    select distinct
           gtt.item,
           gtt.loc,
           gtt.loc_type,
           gtt.effective_from_date,
           gtt.item_parent,
           gtt.item_grandparent,
           gtt.cum_tax_pct,
           gtt.cum_tax_value,
           'BRL',
           SYSDATE,
           'SEED-REFRESH',
           SYSDATE,
           'SEED-REFRESH'
      from l10n_br_extax_help_retail_gtt gtt;

   -- pos_mods_tax_info insert is based on the pos_mods_tax_ind as store system may not need the download
   -- as the store system already have the taxes for existing items.
   if UPPER(pos_mods_tax_ind) = 'Y' then
      -- For Non Pack
      insert into pos_mods_tax_info (
              item,
              store,
              tax_type,
              effective_from,
              tax_code,
              calculation_basis,
              tax_rate,
              estimated_tax_value,
              create_datetime,
              create_id)
      select distinct gtt.item,
             gtt.loc,
             'R',                       --tax_type,
             gtt.effective_from_date,
             r.tax_code,
             r.calculation_basis,
             r.tax_rate,
             r.tax_amount,
             SYSDATE,                   --create_datetime
             'SEED-REFRESH'             --create_id
        from l10n_br_extax_help_retail_gtt gtt,
             l10n_br_extax_res_retail_det r
       where gtt.city                    = r.city
         and gtt.cnae_code               = r.cnae_code
         and gtt.iss                     = r.iss
         and gtt.ipi                     = r.ipi
         and gtt.icms                    = r.icms
         and gtt.cnpj                    = r.cnpj
         --
         and gtt.loc_name_value_pair_id_concat = r.loc_name_value_pair_id_concat
         --
         and gtt.loc_type                = 'ST'
         and gtt.origin_code             = r.origin_code
         and ((gtt.service_ind                 = 'N'
               and r.service_ind               = 'N'
               and gtt.classification_id       = r.classification_id
               and gtt.ex_ipi                  = r.ex_ipi
               and gtt.item_xform_ind          = r.item_xform_ind
               and gtt.state_of_manufacture    = r.state_of_manufacture
               and gtt.pharma_list_type        = r.pharma_list_type)
              or
              (gtt.service_ind                 = 'Y'
               and r.service_ind               = 'Y'
               and gtt.service_code            = r.service_code
               and gtt.federal_service         = r.federal_service))
         --
         and gtt.ncm_char_code           = r.ncm_char_code
         and gtt.PAUTA_CODE              = r.PAUTA_CODE
         and gtt.dim_object              = r.dim_object
         and gtt.length                  = r.length
         and gtt.lwh_uom                 = r.lwh_uom
         and gtt.weight                  = r.weight
         and gtt.net_weight              = r.net_weight
         and gtt.weight_uom              = r.weight_uom
         and gtt.liquid_volume           = r.liquid_volume
         and gtt.liquid_volume_uom       = r.liquid_volume_uom
         and gtt.item_name_value_pair_id_concat = r.item_name_value_pair_id_concat
         and gtt.pack_no                 = '-1';


      -- For Complex Pack
      insert into pos_mods_tax_info (
              item,
              store,
              tax_type,
              effective_from,
              tax_code,
              calculation_basis,
              tax_rate,
              estimated_tax_value,
              create_datetime,
              create_id)
      select inner.pack_no,
             inner.loc,
             'R',                       --tax_type,
             inner.effective_from_date,
             inner.tax_code,
             inner.calculation_basis,
             inner.tax_rate,
             inner.tax_amount,
             SYSDATE,                   --create_datetime
             'SEED-REFRESH'             --create_id
        from ( select distinct gtt.pack_no,
                      gtt.loc,
                      gtt.effective_from_date,
                      r.tax_code,
                      r.calculation_basis,
                      avg(r.tax_rate) tax_rate,
                      avg(r.tax_amount) tax_amount
                 from l10n_br_extax_help_retail_gtt gtt,
                      l10n_br_extax_res_retail_det r 
                where gtt.city                    = r.city
                  and gtt.cnae_code               = r.cnae_code
                  and gtt.iss                     = r.iss
                  and gtt.ipi                     = r.ipi
                  and gtt.icms                    = r.icms
                  and gtt.cnpj                    = r.cnpj
                  and gtt.loc_name_value_pair_id_concat = r.loc_name_value_pair_id_concat
                  and gtt.loc_type                = 'ST'
                  and gtt.pack_no                 = r.pack_no
                  and gtt.pack_no                 != '-1'
                group by gtt.pack_no,
                         gtt.loc,
                         gtt.effective_from_date,
                         r.tax_code,
                         r.calculation_basis
             ) inner;
   end if;

   --
   --
   -- After seeding is done, need to populate the refresh tables for future refresh of the tax law changes
   merge into l10n_br_extax_refresh_config target using
   (select 'Y' refresh_needed
      from dual) d
   on (target.refresh_needed = d.refresh_needed)
   when matched then
      update set target.last_effective_after_this_date  = sysdate
   when not matched then insert (refresh_needed,
                                 last_effective_after_this_date)
                         values (d.refresh_needed,
                                 sysdate);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END SEED_EXTAX_FINISH;
--------------------------------------------------------
FUNCTION MERGE_RESULTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.MERGE_RESULTS';
   L_vdate      DATE := get_vdate();

BEGIN

   -- Non Pack and Simple Pack Merge
   merge into gtax_item_rollup target using (
      select item,
             loc,
             loc_type,
             effective_from_date,
             item_parent,
             item_grandparent,
             cum_tax_pct,
             cum_tax_value
        from l10n_br_extax_help_retail_gtt) use_this
   on (target.item                = use_this.item and
       target.loc                 = use_this.loc and
       target.loc_type            = use_this.loc_type and
       trunc(target.effective_from_date) = trunc(use_this.effective_from_date))
   when matched then
   update set target.cum_tax_pct   = use_this.cum_tax_pct,
              target.cum_tax_value = use_this.cum_tax_value
   when not matched then insert (item,
                                 loc,
                                 loc_type,
                                 effective_from_date,
                                 item_parent,
                                 item_grandparent,
                                 cum_tax_pct,
                                 cum_tax_value,
                                 currency_code,
                                 create_datetime,
                                 create_id,
                                 last_update_datetime,
                                 last_update_id)
                         values (use_this.item,
                                 use_this.loc,
                                 use_this.loc_type,
                                 use_this.effective_from_date,
                                 use_this.item_parent,
                                 use_this.item_grandparent,
                                 use_this.cum_tax_pct,
                                 use_this.cum_tax_value,
                                 'BRL',
                                 SYSDATE,
                                 'INITIAL SEED',
                                 SYSDATE,
                                 'INITIAL SEED');
   --For Non Pack
   merge into pos_mods_tax_info target using (
    select distinct gtt.item,
           gtt.loc,
           'R' tax_type,
           gtt.effective_from_date,
           r.tax_code,
           r.calculation_basis,
           r.tax_rate,
           r.tax_amount
      from l10n_br_extax_help_retail_gtt gtt,
           l10n_br_extax_res_retail_det r 
     where gtt.city                    = r.city
       and gtt.cnae_code               = r.cnae_code
       and gtt.iss                     = r.iss
       and gtt.ipi                     = r.ipi
       and gtt.icms                    = r.icms
       and gtt.cnpj                    = r.cnpj
       and gtt.loc_name_value_pair_id_concat = r.loc_name_value_pair_id_concat
       and gtt.origin_code             = r.origin_code
       and gtt.classification_id       = r.classification_id
       and gtt.ncm_char_code           = r.ncm_char_code
       and gtt.ex_ipi                  = r.ex_ipi
       and gtt.PAUTA_CODE              = r.PAUTA_CODE
       and gtt.service_code            = r.service_code
       and gtt.federal_service         = r.federal_service
       and gtt.dim_object              = r.dim_object
       and gtt.length                  = r.length
       and gtt.lwh_uom                 = r.lwh_uom
       and gtt.weight                  = r.weight
       and gtt.net_weight              = r.net_weight
       and gtt.weight_uom              = r.weight_uom
       and gtt.liquid_volume           = r.liquid_volume
       and gtt.liquid_volume_uom       = r.liquid_volume_uom
       and gtt.item_xform_ind          = r.item_xform_ind
       and gtt.state_of_manufacture    = r.state_of_manufacture
       and gtt.pharma_list_type        = r.pharma_list_type
       and gtt.item_name_value_pair_id_concat = r.item_name_value_pair_id_concat
       and gtt.pack_no                 = '-1'
       and gtt.pack_no                 = r.pack_no ) use_this
   on (target.item           = use_this.item and
       target.store          = use_this.loc and
       target.effective_from = use_this.effective_from_date and
       target.tax_code       = use_this.tax_code)
   when matched then
   update set target.calculation_basis   = use_this.calculation_basis,
              target.tax_rate            = use_this.tax_rate,
              target.estimated_tax_value = use_this.tax_amount
   when not matched then insert (item,
                                 store,
                                 tax_type,
                                 effective_from,
                                 tax_code,
                                 calculation_basis,
                                 tax_rate,
                                 estimated_tax_value,
                                 create_datetime,
                                 create_id)
                         values (use_this.item,
                                 use_this.loc,
                                 use_this.tax_type,
                                 use_this.effective_from_date,
                                 use_this.tax_code,
                                 use_this.calculation_basis,
                                 use_this.tax_rate,
                                 use_this.tax_amount,
                                 SYSDATE,
                                 'INITIAL SEED');

   --For Simple and Complex Pack
   merge into pos_mods_tax_info target using (
      select distinct gtt.pack_no,
                      gtt.loc,
                      'R' tax_type,
                      gtt.effective_from_date,
                      r.tax_code,
                      r.calculation_basis,
                      avg(r.tax_rate) tax_rate,
                      avg(r.tax_amount) tax_amount
                 from l10n_br_extax_help_retail_gtt gtt,
                      l10n_br_extax_res_retail_det r
                where gtt.city                    = r.city
                  and gtt.cnae_code               = r.cnae_code
                  and gtt.iss                     = r.iss
                  and gtt.ipi                     = r.ipi
                  and gtt.icms                    = r.icms
                  and gtt.cnpj                    = r.cnpj
                  and gtt.loc_name_value_pair_id_concat = r.loc_name_value_pair_id_concat
                  and gtt.origin_code             = r.origin_code
                  and gtt.classification_id       = r.classification_id
                  and gtt.ncm_char_code           = r.ncm_char_code
                  and gtt.ex_ipi                  = r.ex_ipi
                  and gtt.PAUTA_CODE              = r.PAUTA_CODE
                  and gtt.service_code            = r.service_code
                  and gtt.federal_service         = r.federal_service
                  and gtt.dim_object              = r.dim_object
                  and gtt.length                  = r.length
                  and gtt.lwh_uom                 = r.lwh_uom
                  and gtt.weight                  = r.weight
                  and gtt.net_weight              = r.net_weight
                  and gtt.weight_uom              = r.weight_uom
                  and gtt.liquid_volume           = r.liquid_volume
                  and gtt.liquid_volume_uom       = r.liquid_volume_uom
                  and gtt.item_xform_ind          = r.item_xform_ind
                  and gtt.state_of_manufacture    = r.state_of_manufacture
                  and gtt.pharma_list_type        = r.pharma_list_type
                  and gtt.item_name_value_pair_id_concat = r.item_name_value_pair_id_concat
                  and gtt.pack_no                 = r.pack_no
                  and gtt.pack_no                 != '-1'
                group by gtt.pack_no,
                         gtt.loc,
                         gtt.effective_from_date,
                         r.tax_code,
                         r.calculation_basis ) use_this
   on (target.item           = use_this.pack_no and
       target.store          = use_this.loc and
       target.effective_from = use_this.effective_from_date and
       target.tax_code       = use_this.tax_code)
   when matched then
   update set target.calculation_basis   = use_this.calculation_basis,
              target.tax_rate            = use_this.tax_rate,
              target.estimated_tax_value = use_this.tax_amount
   when not matched then insert (item,
                                 store,
                                 tax_type,
                                 effective_from,
                                 tax_code,
                                 calculation_basis,
                                 tax_rate,
                                 estimated_tax_value,
                                 create_datetime,
                                 create_id)
                         values (use_this.pack_no,
                                 use_this.loc,
                                 use_this.tax_type,
                                 use_this.effective_from_date,
                                 use_this.tax_code,
                                 use_this.calculation_basis,
                                 use_this.tax_rate,
                                 use_this.tax_amount,
                                 SYSDATE,
                                 'INITIAL SEED');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END MERGE_RESULTS;
--------------------------------------------------------
FUNCTION REFRESH_EXTAX_FINISH(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program    VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.REFRESH_EXTAX_FINISH';
   L_vdate      DATE := get_vdate();

BEGIN

   if RESULT_TO_IL(O_error_message) = FALSE then
      return FALSE;
   end if;

   if MERGE_RESULTS(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   update l10n_br_extax_refresh_config
      set refresh_needed = 'N',
          last_effective_after_this_date = sysdate;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END REFRESH_EXTAX_FINISH;
--------------------------------------------------------
FUNCTION FISCAL_ITEM_RECLASS_FINISH(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.FISCAL_ITEM_RECLASS_FINISH';
   L_vdate      DATE := get_vdate();

BEGIN

   -- Explode the Tax attributes for Item and Locations impacted by Fiscal Reclassification
   if RECLASS_ITEM_RESULT_TO_IL(O_error_message) = FALSE then
      return FALSE;
   end if;

   -- Write or Update gtax_item_rollup for retail taxes and POS_MODS_TAX_INFO for tax download to POS
   if MERGE_RESULTS(O_error_message) = FALSE then
      return FALSE;
   end if;

   -- Need to Update the Fiscal Item record to 'P'rocessed status after gtax_item_rollup and POS_MODS_TAX_INFO
   -- records have been created for any fiscal item in future.
      -- Mark the Fiscal Item Changes to 'C'ompleted and update ITEM_COUNTRY_L10N_EXT with the new fiscal attributes
   -- once the Fiscal Attribute changes are current.

   if FISCAL_ITEM_RECLASS_UPDATE(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END FISCAL_ITEM_RECLASS_FINISH;
--------------------------------------------------------
FUNCTION FISCAL_ITEM_RECLASS_UPDATE(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.FISCAL_ITEM_RECLASS_UPDATE';
   L_vdate      DATE := get_vdate();
   L_dynamic            VARCHAR2(2000);
   L_dynamic_ins        VARCHAR2(2000);
   L_item_group_exists  VARCHAR2(1);

   cursor C_FRECLASS_ITEM is
      select fr.rowid,
             fr.item,
             fr.country_id,
             fr.new_ncm_code,
             fr.new_ncm_char_code,
             fr.new_ex_ipi_code,
             fr.new_pauta_code,
             fr.new_mtr_service_code,
             fr.new_service_item_ind,
             fr.new_origin_code,
             fr.new_fed_service_code,
             fr.new_state_of_manufacture,
             fr.new_pharma_list_type,
             fr.active_date
        from l10n_br_fiscal_reclass fr
       where fr.status <> 'C'
         and fr.country_id is not null
         and fr.active_date <= L_vdate;

   cursor C_GET_GROUP_ID is
      select distinct at.group_id,
             fr.item
        from l10n_br_fiscal_reclass_nv frnv,
             l10n_br_fiscal_reclass fr,
             l10n_attrib at,
             l10n_attrib_group ag,
             ext_entity et
       where at.view_col_name = frnv.name
         and frnv.reclassification_id = fr.reclassification_id
         and ag.group_id = at.group_id
         and et.base_rms_table = 'ITEM_COUNTRY'
         and et.ext_entity_id  = ag.ext_entity_id
         and ag.base_ind       = 'N';

   cursor C_GET_ATTRIB(L_group_id L10N_ATTRIB.GROUP_ID%TYPE, L_item ITEM_MASTER.ITEM%TYPE) is
     select at.view_col_name,
            at.attrib_storage_col,
            fr.item,
            frnv.new_value
       from l10n_attrib at,
            l10n_br_fiscal_reclass_nv frnv,
            l10n_br_fiscal_reclass fr
      where at.view_col_name       = frnv.name
        and fr.reclassification_id = frnv.reclassification_id
        and group_id = L_group_id
        and item     = L_item;


   cursor C_ITEM_GROUP_EXISTS(L_item      ITEM_COUNTRY_L10N_EXT.ITEM%TYPE,
                              L_group_id  ITEM_COUNTRY_L10N_EXT.GROUP_ID%TYPE) is
      select 'Y'
        from item_country_l10n_ext
       where item     = L_item
         and group_id = L_group_id;


   TYPE fiscal_item_type IS TABLE OF c_freclass_item%ROWTYPE INDEX BY BINARY_INTEGER;
   fiscal_item_tbl fiscal_item_type;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_FRECLASS_ITEM','l10n_br_fiscal_reclass',NULL);
   open  C_FRECLASS_ITEM;
   --
   SQL_LIB.SET_MARK('FETCH','C_FRECLASS_ITEM','l10n_br_fiscal_reclass',NULL);
   fetch C_FRECLASS_ITEM bulk collect into fiscal_item_tbl;
   --
   SQL_LIB.SET_MARK('CLOSE','C_FRECLASS_ITEM','l10n_br_fiscal_reclass',NULL);
   close C_FRECLASS_ITEM;
   --
   FORALL i in 1..fiscal_item_tbl.COUNT
      UPDATE item_country_l10n_ext
         SET varchar2_1 = fiscal_item_tbl(i).new_service_item_ind,
             varchar2_2 = fiscal_item_tbl(i).new_origin_code,
             varchar2_3 = fiscal_item_tbl(i).new_ncm_code,
             varchar2_4 = fiscal_item_tbl(i).new_ncm_char_code,
             varchar2_5 = fiscal_item_tbl(i).new_ex_ipi_code,
             varchar2_6 = fiscal_item_tbl(i).new_pauta_code,
             varchar2_7 = fiscal_item_tbl(i).new_mtr_service_code,
             varchar2_8 = fiscal_item_tbl(i).new_fed_service_code,
             varchar2_9 = fiscal_item_tbl(i).new_state_of_manufacture, 
             varchar2_10 = fiscal_item_tbl(i).new_pharma_list_type
       WHERE item = fiscal_item_tbl(i).item
         AND group_id=2
         AND country_id = fiscal_item_tbl(i).country_id
         AND l10n_country_id='BR';
   --
   FORALL i in 1..fiscal_item_tbl.COUNT
      UPDATE l10n_br_fiscal_reclass
         SET status = 'C',
             processed_date     = L_vdate,
             last_upd_datetime  = sysdate,
             last_upd_userid    = user
       WHERE rowid = fiscal_item_tbl(i).rowid;

     UPDATE l10n_br_fiscal_reclass
        SET status = 'P',
            processed_date     = L_vdate,
            last_upd_datetime  = sysdate,
            last_upd_userid    = user
      WHERE status in ( 'N','S','A')
        and country_id is not null
        and active_date >= L_vdate;

   FOR grp_rec in C_GET_GROUP_ID LOOP

      open C_ITEM_GROUP_EXISTS(grp_rec.item, grp_rec.group_id);
      fetch C_ITEM_GROUP_EXISTS into L_item_group_exists;
      close C_ITEM_GROUP_EXISTS;

      if L_item_group_exists = 'Y' then
         L_dynamic := 'update item_country_l10n_ext set ';

         for rec in C_GET_ATTRIB(grp_rec.group_id, grp_rec.item) loop
            L_dynamic := L_dynamic ||  rec.attrib_storage_col || ' ' || '=' || ''''|| rec.new_value ||'''' ||  ' ' || ','  ;
         end loop;

         L_dynamic := rtrim(L_dynamic, ',' );
         L_dynamic := L_dynamic || ' ' || 'where group_id = ' || grp_rec.group_id || ' and item = ' || grp_rec.item;

         execute immediate L_dynamic;
      else

         L_dynamic     := 'insert into item_country_l10n_ext (item, country_id, l10n_country_id, group_id, ';
         L_dynamic_ins := ') values (' || grp_rec.item || ',' || '''BR'', ''BR'' ,' || grp_rec.group_id || ',';

         for rec in C_GET_ATTRIB(grp_rec.group_id, grp_rec.item) loop
            L_dynamic      :=  L_dynamic || rec.attrib_storage_col ||',' ;
            L_dynamic_ins  :=  L_dynamic_ins || ''''|| rec.new_value ||'''' || ',' ;
         end loop;

         L_dynamic := rtrim(L_dynamic, ',' );
         L_dynamic_ins := rtrim(L_dynamic_ins, ',' ) || ')';

         L_dynamic := L_dynamic || L_dynamic_ins;
         execute immediate L_dynamic;

      end if;
   end LOOP;

  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END FISCAL_ITEM_RECLASS_UPDATE;
-----------------------------------------------------------------------------
FUNCTION SEED_GTAX_SETUP_COST(O_error_message    IN OUT VARCHAR2,
                              I_process_size     IN     NUMBER)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SEED_GTAX_SETUP_COST';
   LP_vdate      DATE := get_vdate();

BEGIN

   delete from l10n_br_extax_stg_cost;
   delete from l10n_br_extax_res_cost;
   delete from l10n_br_extax_res_cost_det;

   if EXTAX_LOC_GROUP_SETUP(O_error_message) = FALSE then
      return FALSE;
   end if;

   insert into l10n_br_extax_stg_cost (
           sup_city,
           sup_city_desc,
           sup_state,
           sup_state_desc,
           sup_country_id,
           sup_country_desc,
           sup_cnae_code,
           sup_iss,
           sup_simples,
           sup_ipi,
           sup_icms,
           sup_st,
           sup_cnpj,
           sup_rural_prod_ind,
           sup_is_income_range_eligible,
           sup_is_distr_a_manufacturer,
           sup_icms_simples_rate,
           sup_tax_regime_concat,
           sup_name_value_pair_id_concat,
           --
           loc_city,
           loc_city_desc,
           loc_state,
           loc_state_desc,
           loc_country_id,
           loc_country_desc,
           loc_cnae_code,
           loc_iss,
           loc_ipi,
           loc_icms,
           loc_cnpj,
           loc_name_value_pair_id_concat,
           --
           origin_code,
           service_ind,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           unit_cost,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           effective_date,
           pack_no,
           item,
           process_id,
           item_ref_id,
           loc_ref_id,
           sup_ref_id,
           item_xform_ind)
      select inner2.sup_city,
             inner2.sup_city_desc,
             inner2.sup_state,
             inner2.sup_state_desc,
             inner2.sup_country_id,
             inner2.sup_country_desc,
             inner2.sup_cnae_code,
             inner2.sup_iss,
             inner2.sup_simples,
             inner2.sup_ipi,
             inner2.sup_icms,
             inner2.sup_st,
             inner2.sup_cnpj,
             inner2.sup_rural_prod_ind,
             inner2.sup_is_income_range_eligible,
             inner2.sup_is_distr_a_manufacturer,
             inner2.sup_icms_simples_rate,
             inner2.sup_tax_regime_concat,
             inner2.sup_name_value_pair_id_concat,
             --
             inner2.loc_city,
             inner2.loc_city_desc,
             inner2.loc_state,
             inner2.loc_state_desc,
             inner2.loc_country_id,
             inner2.loc_country_desc,
             inner2.loc_cnae_code,
             inner2.loc_iss,
             inner2.loc_ipi,
             inner2.loc_icms,
             inner2.loc_cnpj,
             inner2.loc_name_value_pair_id_concat,
             --
             inner2.origin_code,
             inner2.service_ind,
             inner2.classification_id,
             inner2.ncm_char_code,
             inner2.ex_ipi,
             inner2.pauta_code,
             inner2.service_code,
             inner2.federal_service,
             inner2.state_of_manufacture,
             inner2.pharma_list_type,
             inner2.item_name_value_pair_id_concat,
             inner2.unit_cost ,
             inner2.dim_object,
             inner2.length,
             inner2.lwh_uom,
             inner2.weight,
             inner2.net_weight,
             inner2.weight_uom,
             inner2.liquid_volume,
             inner2.liquid_volume_uom,
             --
             inner2.effective_date,
             inner2.pack_no,
             inner2.item,
             decode(row_number() over (partition by inner2.date_split, inner2.data_split
                                           order by inner2.effective_date),
                    1,l10n_br_extax_maint_sql.get_seq('Y'),
                      l10n_br_extax_maint_sql.get_seq('N')),
             inner2.item_ref_id,
             inner2.loc_ref_id,
             inner2.sup_ref_id,
             inner2.item_xform_ind
       from (
      select inner.sup_city,
             inner.sup_city_desc,
             inner.sup_state,
             inner.sup_state_desc,
             inner.sup_country_id,
             inner.sup_country_desc,
             inner.sup_cnae_code,
             inner.sup_iss,
             inner.sup_simples,
             inner.sup_ipi,
             inner.sup_icms,
             inner.sup_st,
             inner.sup_cnpj,
             inner.sup_rural_prod_ind,
             inner.sup_is_income_range_eligible,
             inner.sup_is_distr_a_manufacturer,
             inner.sup_icms_simples_rate,
             inner.sup_tax_regime_concat,
             inner.sup_name_value_pair_id_concat,
             --
             inner.loc_city,
             inner.loc_city_desc,
             inner.loc_state,
             inner.loc_state_desc,
             inner.loc_country_id,
             inner.loc_country_desc,
             inner.loc_cnae_code,
             inner.loc_iss,
             inner.loc_ipi,
             inner.loc_icms,
             inner.loc_cnpj,
             inner.loc_name_value_pair_id_concat,
             --
             inner.origin_code,
             inner.service_ind,
             inner.classification_id,
             inner.ncm_char_code,
             inner.ex_ipi,
             inner.pauta_code,
             inner.service_code,
             inner.federal_service,
             inner.state_of_manufacture,
             inner.pharma_list_type,
             inner.item_name_value_pair_id_concat,
             inner.unit_cost ,
             inner.dim_object,
             inner.length,
             inner.lwh_uom,
             inner.weight,
             inner.net_weight,
             inner.weight_uom,
             inner.liquid_volume,
             inner.liquid_volume_uom,
             --
             inner.effective_date,
             inner.pack_no,
             inner.item,
             dense_rank() over (order by inner.effective_date) date_split,
             ceil(row_number() over (partition by inner.effective_date
                               order by inner.sup_city,
                                        inner.sup_state,
                                        inner.sup_country_id,
                                        inner.sup_cnae_code,
                                        inner.sup_iss,
                                        inner.sup_simples,
                                        inner.sup_ipi,
                                        inner.sup_icms,
                                        inner.sup_st,
                                        inner.sup_cnpj,
                                        inner.sup_rural_prod_ind,
                                        inner.sup_is_income_range_eligible,
                                        inner.sup_is_distr_a_manufacturer,
                                        inner.sup_icms_simples_rate,
                                        inner.sup_tax_regime_concat,
                                        inner.sup_name_value_pair_id_concat,
                                        --
                                        inner.loc_city,
                                        inner.loc_state,
                                        inner.loc_country_id,
                                        inner.loc_cnae_code,
                                        inner.loc_iss,
                                        inner.loc_ipi,
                                        inner.loc_icms,
                                        inner.loc_cnpj,
                                        inner.loc_name_value_pair_id_concat,
                                        --
                                        inner.service_ind,
                                        inner.ncm_char_code,
                                        inner.pauta_code,
                                        decode(inner.service_ind,'N',inner.classification_id,null),
                                        decode(inner.service_ind,'N',inner.ex_ipi,null),
                                        decode(inner.service_ind,'N',inner.state_of_manufacture,null),
                                        decode(inner.service_ind,'N',inner.pharma_list_type,null),
                                        decode(inner.service_ind,'N',inner.item_xform_ind,null),
                                        decode(inner.service_ind,'Y',inner.service_code,null),
                                        decode(inner.service_ind,'Y',inner.federal_service,null),
                                        inner.item_name_value_pair_id_concat,
                                        inner.unit_cost,
                                        inner.dim_object,
                                        inner.length,
                                        inner.lwh_uom,
                                        inner.weight,
                                        inner.net_weight,
                                        inner.weight_uom,
                                        inner.liquid_volume,
                                        inner.liquid_volume_uom,
                                        inner.pack_no,
                                        inner.item)/I_process_size) + 0 data_split,
             dense_rank() over (order by inner.service_ind,
                                         inner.ncm_char_code,
                                         inner.pauta_code,
                                         decode(inner.service_ind,'N',inner.classification_id,null),
                                         decode(inner.service_ind,'N',inner.ex_ipi,null),
                                         decode(inner.service_ind,'N',inner.state_of_manufacture,null),
                                         decode(inner.service_ind,'N',inner.pharma_list_type,null),
                                         decode(inner.service_ind,'N',inner.item_xform_ind,null),
                                         decode(inner.service_ind,'Y',inner.service_code,null),
                                         decode(inner.service_ind,'Y',inner.federal_service,null),
                                         inner.item_name_value_pair_id_concat,
                                         inner.unit_cost,
                                         inner.dim_object,
                                         inner.length,
                                         inner.lwh_uom,
                                         inner.weight,
                                         inner.net_weight,
                                         inner.weight_uom,
                                         inner.liquid_volume,
                                         inner.liquid_volume_uom,
                                         inner.pack_no,
                                         inner.item) item_ref_id,
             dense_rank() over (order by inner.loc_city,
                                         inner.loc_state,
                                         inner.loc_country_id,
                                         inner.loc_cnae_code,
                                         inner.loc_iss,
                                         inner.loc_ipi,
                                         inner.loc_icms,
                                         inner.loc_cnpj,
                                         inner.loc_name_value_pair_id_concat) loc_ref_id,
             dense_rank() over (order by inner.sup_city,
                                         inner.sup_state,
                                         inner.sup_country_id,
                                         inner.sup_cnae_code,
                                         inner.sup_iss,
                                         inner.sup_simples,
                                         inner.sup_ipi,
                                         inner.sup_icms,
                                         inner.sup_st,
                                         inner.sup_cnpj,
                                         inner.sup_rural_prod_ind,
                                         inner.sup_is_income_range_eligible,
                                         inner.sup_is_distr_a_manufacturer,
                                         inner.sup_icms_simples_rate,
                                         inner.sup_tax_regime_concat,
                                         inner.sup_name_value_pair_id_concat) sup_ref_id,
             inner.item_xform_ind
       from (
-- For Simple and Complex Pack
      with item_stg as
      -- retrieve component dimensions for a complex pack
      (select pi.pack_no,
              pi.item,
              im.dept,
              im.class,
              im.subclass,
              isc.supplier,
              isc.origin_country_id,
              im.simple_pack_ind,
              im.item_xform_ind,
              (isc.unit_cost * pi.qty )/
              decode(sum(NVL(isc.unit_cost, 0) * pi.qty) over (partition by pi.pack_no,isc.supplier,isc.origin_country_id), 0, 1,
              sum(NVL(isc.unit_cost, 0) * pi.qty) over (partition by pi.pack_no,isc.supplier,isc.origin_country_id)) conv_val,
              nvl(d.dim_object, '-999') dim_object,
              nvl(d.length,'-999') length,
              nvl(d.lwh_uom,'-999') lwh_uom,
              nvl(d.weight,'-999') weight,
              nvl2(d.net_weight, d.net_weight * pi.qty, '-999') net_weight,
              nvl(d.weight_uom,'-999') weight_uom,
              nvl2(d.liquid_volume, d.liquid_volume * pi.qty, '-999') liquid_volume,
              nvl(d.liquid_volume_uom,'-999') liquid_volume_uom
         from item_master im,
              v_packsku_qty pi,
              item_supp_country isc,
              item_supp_country_dim d
        where pi.pack_no = im.item
         and im.status              = 'A'
         and im.tran_level          = im.item_level
         and im.pack_ind            = 'Y'
         and im.simple_pack_ind     = 'N'
         and im.orderable_ind       = 'Y'
         and pi.item                = isc.item
         and 'EA'                   = d.dim_object(+)
         and pi.item                = d.item(+)
      union all
       -- retrieve the pack dimensions for a simple pack
       select pi.pack_no,
              pi.item,
              im.dept,
              im.class,
              im.subclass,
              isc.supplier,
              isc.origin_country_id,
              im.simple_pack_ind,
              im.item_xform_ind,
              1 conv_val,
              nvl(d.dim_object, '-999') dim_object,
              nvl(d.length,'-999') length,
              nvl(d.lwh_uom,'-999') lwh_uom,
              nvl(d.weight,'-999') weight,
              nvl(d.net_weight, '-999') net_weight,
              nvl(d.weight_uom,'-999') weight_uom,
              nvl(d.liquid_volume,  '-999') liquid_volume,
              nvl(d.liquid_volume_uom,'-999') liquid_volume_uom
         from item_master im,
              v_packsku_qty pi,
              item_supp_country isc,
              item_supp_country_dim  d
        where pi.pack_no = im.item
         and im.status              = 'A'
         and im.tran_level          = im.item_level
         and im.pack_ind            = 'Y'
         and im.simple_pack_ind     = 'Y'
         and im.orderable_ind       = 'Y'
         and im.item                = isc.item
         and 'EA'                   = d.dim_object(+)
         and im.item                = d.item(+))
      select /*+ PARALLEL(fc,5) */  distinct
             sup_help.city                                sup_city,
             sup_help.city_desc                           sup_city_desc,
             sup_help.state                               sup_state,
             sup_help.state_desc                          sup_state_desc,
             sup_help.country_id                          sup_country_id,
             sup_help.country_desc                        sup_country_desc,
             sup_help.cnae_code_concatenation             sup_cnae_code,
             nvl(sup_help.iss_contrib_ind,'N')            sup_iss,
             nvl(sup_help.simples_ind,'N')                sup_simples,
             nvl(sup_help.ipi_ind,'N')                    sup_ipi,
             nvl(sup_help.icms_contrib_ind,'N')           sup_icms,
             nvl(sup_help.st_contrib_ind,'N')             sup_st,
             sup_help.cnpj                                sup_cnpj,
             nvl(sup_help.rural_prod_ind,'N')             sup_rural_prod_ind,
             nvl(sup_help.is_income_range_eligible,'N')   sup_is_income_range_eligible,
             nvl(sup_help.is_distr_a_manufacturer,'N')    sup_is_distr_a_manufacturer,
             nvl(sup_help.icms_simples_rate,'-1')         sup_icms_simples_rate,
             nvl(sup_help.tax_regime_concat,'N')          sup_tax_regime_concat,
             nvl(sup_help.name_value_pair_id_concat,'-1') sup_name_value_pair_id_concat,
             --
             loc_help.city                                loc_city,
             loc_help.city_desc                           loc_city_desc,
             loc_help.state                               loc_state,
             loc_help.state_desc                          loc_state_desc,
             loc_help.country_id                          loc_country_id,
             loc_help.country_desc                        loc_country_desc,
             loc_help.cnae_code_concatenation             loc_cnae_code,
             nvl(loc_help.iss_contrib_ind,'N')            loc_iss,
             nvl(loc_help.ipi_ind,'N')                    loc_ipi,
             nvl(loc_help.icms_contrib_ind,'N')           loc_icms,
             loc_help.cnpj                                loc_cnpj,
             nvl(loc_help.name_value_pair_id_concat,'-1') loc_name_value_pair_id_concat,
             --
             '0'                                          origin_code,
             nvl(vbi.service_ind,'N')                     service_ind,
             nvl(vbi.classification_id,'-1')              classification_id,
             nvl(vbi.ncm_char_code,'-1')                  ncm_char_code,
             nvl(vbi.ex_ipi,'-1')                         ex_ipi,
             nvl(vbi.pauta_code,'-1')                     pauta_code,
             nvl(vbi.service_code,'-1')                   service_code,
             nvl(vbi.federal_service,'-1')                federal_service,
             nvl(vbi.state_of_manufacture,'-1')           state_of_manufacture,
             nvl(vbi.pharma_list_type,'-1')               pharma_list_type,
             nvl(nv.item_name_value_pair_id_concat,'-1')  item_name_value_pair_id_concat,
             (fc.base_cost * item_dim.conv_val)           unit_cost,
             item_dim.dim_object                          dim_object,
             item_dim.length                              length,
             item_dim.lwh_uom                             lwh_uom,
             item_dim.weight                              weight,
             item_dim.net_weight                          net_weight,
             item_dim.weight_uom                          weight_uom,
             item_dim.liquid_volume                       liquid_volume,
             item_dim.liquid_volume_uom                   liquid_volume_uom,
             --
             (case when
               trunc(fc.active_date) <= LP_vdate
                then LP_vdate
                else trunc(fc.active_date) end)           effective_date,
             item_dim.pack_no                             pack_no,
             item_dim.item                                item,
             0                                            process_id,
             0                                            item_ref_id,
             0                                            loc_ref_id,
             0                                            sup_ref_id,
             item_dim.item_xform_ind                      item_xform_ind
        from l10n_br_extax_dest_group_help sup_help,
             l10n_br_extax_dest_group_help loc_help,
             --
             future_cost fc,
             item_stg item_dim,
             (select v.item,
                     v.origin_code,
                     v.service_ind,
                     v.classification_id,
                     v.ncm_char_code,
                     v.ex_ipi,
                     v.pauta_code,
                     v.service_code,
                     v.federal_service,
                     v.state_of_manufacture,
                     v.pharma_list_type,
                     ca.default_loc location,
                     ca.default_loc_type loc_type
                from v_br_item_fiscal_attrib v,
                     country_attrib ca
              where v.country_id = ca.country_id) vbi,
             (select entity item,
                     listagg (name_value_pair_id, '~') within group (order by name_value_pair_id) item_name_value_pair_id_concat
                from l10n_br_extax_help_nv_pair
               where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
               group by entity) nv
       where sup_help.entity_type              = L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
         and sup_help.entity                   = to_char(fc.supplier)
         and sup_help.city                     is not null
         and sup_help.cnae_code_concatenation  is not null
         --
         and loc_help.entity_type              in(L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                  L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                                  L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
         -- Future Cost Location
         -- use virtual wh set to enable join of future_cost with loc_help
         and loc_help.entity_type              = decode(fc.loc_type,
                                                        'W',L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                        'S',L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                                        'E',L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
         and loc_help.entity                   = to_char(fc.location)
         and loc_help.city                     is not null
         and loc_help.cnae_code_concatenation  is not null
         --
         and item_dim.item                     = vbi.item
         and vbi.origin_code                   = '0'
         --
         and fc.item                           = item_dim.pack_no
         --
         and fc.supplier                      = item_dim.supplier
         and fc.origin_country_id             = item_dim.origin_country_id
         --
         and fc.item                          = nv.item(+)
union
      select /*+ PARALLEL(fc,5) */  distinct
             sup_help.city,
             sup_help.city_desc,
             sup_help.state,
             sup_help.state_desc,
             sup_help.country_id,
             sup_help.country_desc,
             sup_help.cnae_code_concatenation,
             nvl(sup_help.iss_contrib_ind,'N'),
             nvl(sup_help.simples_ind,'N'),
             nvl(sup_help.ipi_ind,'N'),
             nvl(sup_help.icms_contrib_ind,'N'),
             nvl(sup_help.st_contrib_ind,'N'),
             sup_help.cnpj,
             nvl(sup_help.rural_prod_ind,'N'),
             nvl(sup_help.is_income_range_eligible,'N'),
             nvl(sup_help.is_distr_a_manufacturer,'N'),
             nvl(sup_help.icms_simples_rate,'-1'),
             nvl(sup_help.tax_regime_concat,'N'),
             nvl(sup_help.name_value_pair_id_concat,'-1'),
             --
             loc_help.city,
             loc_help.city_desc,
             loc_help.state,
             loc_help.state_desc,
             loc_help.country_id,
             loc_help.country_desc,
             loc_help.cnae_code_concatenation,
             nvl(loc_help.iss_contrib_ind,'N'),
             nvl(loc_help.ipi_ind,'N'),
             nvl(loc_help.icms_contrib_ind,'N'),
             loc_help.cnpj,
             nvl(loc_help.name_value_pair_id_concat,'-1'),
             --
             '0',   --vbi.origin_code,
             nvl(vbi.service_ind,'N'),
             nvl(vbi.classification_id,'-1'),
             nvl(vbi.ncm_char_code,'-1'),
             nvl(vbi.ex_ipi,'-1'),
             nvl(vbi.pauta_code,'-1'),
             nvl(vbi.service_code,'-1'),
             nvl(vbi.federal_service,'-1'),
             nvl(vbi.state_of_manufacture,'-1'),
             nvl(vbi.pharma_list_type,'-1'),
             nvl(nv.item_name_value_pair_id_concat,'-1'),
             (fc.base_cost * item_dim.conv_val) unit_cost ,
             item_dim.dim_object,
             item_dim.length,
             item_dim.lwh_uom,
             item_dim.weight,
             item_dim.net_weight,
             item_dim.weight_uom,
             item_dim.liquid_volume,
             item_dim.liquid_volume_uom,
             --
             (case when trunc(fc.active_date) <= LP_vdate then LP_vdate else trunc(fc.active_date) end),
             item_dim.pack_no,  -- Pack No
             item_dim.item,  -- Item
             0,   --process_id
             0,   --item_ref_id,
             0,   --loc_ref_id,
             0,    --sup_ref_id,
             item_dim.item_xform_ind
        from l10n_br_extax_dest_group_help sup_help,
             l10n_br_extax_dest_group_help loc_help,
             --
             future_cost fc,
             item_stg item_dim,
             (select v.item,
                     v.origin_code,
                     v.service_ind,
                     v.classification_id,
                     v.ncm_char_code,
                     v.ex_ipi,
                     v.pauta_code,
                     v.service_code,
                     v.federal_service,
                     v.state_of_manufacture,
                     v.pharma_list_type,
                     ca.default_loc location,
                     ca.default_loc_type loc_type
                from v_br_item_fiscal_attrib v,
                     country_attrib ca
              where v.country_id = ca.country_id) vbi,
             (select entity item,
                     listagg (name_value_pair_id, '~') within group (order by name_value_pair_id) item_name_value_pair_id_concat
                from l10n_br_extax_help_nv_pair
               where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
               group by entity) nv
       where sup_help.entity_type              = L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
         and sup_help.entity                   = to_char(fc.supplier)
         and sup_help.city                     is not null
         and sup_help.cnae_code_concatenation  is not null
         --
         and loc_help.entity_type              in(L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                  L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                                  L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
         -- Future Cost Location
         -- Default Location
         -- External finishers are not allowed as default location so no need to decode on them in vbi set
         and loc_help.entity_type              = decode(vbi.loc_type,
                                                        'W',L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                        'S',L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY)
         and loc_help.entity                   = to_char(vbi.location)
         and NOT EXISTS (select '1'
                           from future_cost fc1
                          where fc1.item = item_dim.pack_no
                            and fc1.supplier = fc.supplier
                            and fc1.origin_country_id = fc.origin_country_id
                            and fc1.location = vbi.location
                            and fc1.loc_type = vbi.loc_type
                            and rownum = 1)
         and loc_help.city                     is not null
         and loc_help.cnae_code_concatenation  is not null
         --
         and item_dim.item                     = vbi.item
         and vbi.origin_code                   = '0'
         --
         and fc.item                           = item_dim.pack_no
         --
         and fc.supplier                      = item_dim.supplier
         and fc.origin_country_id             = item_dim.origin_country_id
         --
         and fc.item                          = nv.item(+)
UNION
-- For Non Pack
      select /*+ PARALLEL(fc,5) */ distinct
             sup_help.city,
             sup_help.city_desc,
             sup_help.state,
             sup_help.state_desc,
             sup_help.country_id,
             sup_help.country_desc,
             sup_help.cnae_code_concatenation,
             nvl(sup_help.iss_contrib_ind,'N'),
             nvl(sup_help.simples_ind,'N'),
             nvl(sup_help.ipi_ind,'N'),
             nvl(sup_help.icms_contrib_ind,'N'),
             nvl(sup_help.st_contrib_ind,'N'),
             sup_help.cnpj,
             nvl(sup_help.rural_prod_ind,'N'),
             nvl(sup_help.is_income_range_eligible,'N'),
             nvl(sup_help.is_distr_a_manufacturer,'N'),
             nvl(sup_help.icms_simples_rate,'-1'),
             nvl(sup_help.tax_regime_concat,'N'),
             nvl(sup_help.name_value_pair_id_concat,'-1'),
             --
             loc_help.city,
             loc_help.city_desc,
             loc_help.state,
             loc_help.state_desc,
             loc_help.country_id,
             loc_help.country_desc,
             loc_help.cnae_code_concatenation,
             nvl(loc_help.iss_contrib_ind,'N'),
             nvl(loc_help.ipi_ind,'N'),
             nvl(loc_help.icms_contrib_ind,'N'),
             loc_help.cnpj,
             nvl(loc_help.name_value_pair_id_concat,'-1'),
             --
             '0',   --vbi.origin_code,
             nvl(vbi.service_ind,'N'),
             nvl(vbi.classification_id,'-1'),
             nvl(vbi.ncm_char_code,'-1'),
             nvl(vbi.ex_ipi,'-1'),
             nvl(vbi.pauta_code,'-1'),
             nvl(vbi.service_code,'-1'),
             nvl(vbi.federal_service,'-1'),
             nvl(vbi.state_of_manufacture,'-1'),
             nvl(vbi.pharma_list_type,'-1'),
             nvl(nv.item_name_value_pair_id_concat,'-1'),
             fc.base_cost unit_cost,
             nvl(iscd.dim_object, '-999'),
             nvl(iscd.length,'-999'),
             nvl(iscd.lwh_uom,'-999'),
             nvl(iscd.weight,'-999'),
             nvl(iscd.net_weight,'-999'),
             nvl(iscd.weight_uom,'-999'),
             nvl(iscd.liquid_volume,'-999'),
             nvl(iscd.liquid_volume_uom,'-999'),
             --
             (case when trunc(fc.active_date) <= LP_vdate then LP_vdate else trunc(fc.active_date) end),
             '-1',  -- Pack No
             '-1',  -- Item
             0,   --process_id
             0,   --item_ref_id,
             0,   --loc_ref_id,
             0,   --sup_ref_id,
             im.item_xform_ind
        from l10n_br_extax_dest_group_help sup_help,
             l10n_br_extax_dest_group_help loc_help,
             --
             future_cost fc,
             item_supp_country_dim iscd,
             item_master im,
             (select v.item,
                     v.origin_code,
                     v.service_ind,
                     v.classification_id,
                     v.ncm_char_code,
                     v.ex_ipi,
                     v.pauta_code,
                     v.service_code,
                     v.federal_service,
                     v.state_of_manufacture,
                     v.pharma_list_type,
                     ca.default_loc location,
                     ca.default_loc_type loc_type
                from v_br_item_fiscal_attrib v,
                     country_attrib ca
              where v.country_id = ca.country_id ) vbi,
             (select entity item,
                     listagg (name_value_pair_id, '~') within group (order by name_value_pair_id) item_name_value_pair_id_concat
                from l10n_br_extax_help_nv_pair
               where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
               group by entity) nv
       where sup_help.entity_type              = L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
         and sup_help.entity                   = to_char(fc.supplier)
         and sup_help.city                     is not null
         and sup_help.cnae_code_concatenation  is not null
         --
         and loc_help.entity_type              in(L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                  L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                                  L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
         -- Future Cost Location
         and loc_help.entity_type              = decode(fc.loc_type,
                                                        'W',L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                        'S',L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                                        'E',L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
         -- use virtual wh set to enable join of future_cost with loc_help
         and loc_help.entity                   = to_char(fc.location)
         --
         and loc_help.city                     is not null
         and loc_help.cnae_code_concatenation  is not null
         --
         and fc.item                           = vbi.item
         and vbi.origin_code                   = '0'
         --
         and fc.item                           = im.item
         and im.status                         = 'A'
         and im.tran_level                     = im.item_level
         and im.pack_ind                       = 'N'
         and (im.orderable_ind      = 'Y' or
              (im.orderable_ind     = 'N' and
               im.deposit_item_type = 'A'))
         --
         and 'EA'                               = iscd.dim_object(+)
         and fc.item                            = iscd.item(+)
         and fc.supplier                        = iscd.supplier(+)
         and fc.origin_country_id               = iscd.origin_country(+)
         --
         and fc.item                            = nv.item(+)
union all
      select /*+ PARALLEL(fc,5) */ distinct
             sup_help.city,
             sup_help.city_desc,
             sup_help.state,
             sup_help.state_desc,
             sup_help.country_id,
             sup_help.country_desc,
             sup_help.cnae_code_concatenation,
             nvl(sup_help.iss_contrib_ind,'N'),
             nvl(sup_help.simples_ind,'N'),
             nvl(sup_help.ipi_ind,'N'),
             nvl(sup_help.icms_contrib_ind,'N'),
             nvl(sup_help.st_contrib_ind,'N'),
             sup_help.cnpj,
             nvl(sup_help.rural_prod_ind,'N'),
             nvl(sup_help.is_income_range_eligible,'N'),
             nvl(sup_help.is_distr_a_manufacturer,'N'),
             nvl(sup_help.icms_simples_rate,'-1'),
             nvl(sup_help.tax_regime_concat,'N'),
             nvl(sup_help.name_value_pair_id_concat,'-1'),
             --
             loc_help.city,
             loc_help.city_desc,
             loc_help.state,
             loc_help.state_desc,
             loc_help.country_id,
             loc_help.country_desc,
             loc_help.cnae_code_concatenation,
             nvl(loc_help.iss_contrib_ind,'N'),
             nvl(loc_help.ipi_ind,'N'),
             nvl(loc_help.icms_contrib_ind,'N'),
             loc_help.cnpj,
             nvl(loc_help.name_value_pair_id_concat,'-1'),
             --
             '0',   --vbi.origin_code,
             nvl(vbi.service_ind,'N'),
             nvl(vbi.classification_id,'-1'),
             nvl(vbi.ncm_char_code,'-1'),
             nvl(vbi.ex_ipi,'-1'),
             nvl(vbi.pauta_code,'-1'),
             nvl(vbi.service_code,'-1'),
             nvl(vbi.federal_service,'-1'),
             nvl(vbi.state_of_manufacture,'-1'),
             nvl(vbi.pharma_list_type,'-1'),
             nvl(nv.item_name_value_pair_id_concat,'-1'),
             fc.base_cost unit_cost,
             nvl(iscd.dim_object, '-999'),
             nvl(iscd.length,'-999'),
             nvl(iscd.lwh_uom,'-999'),
             nvl(iscd.weight,'-999'),
             nvl(iscd.net_weight,'-999'),
             nvl(iscd.weight_uom,'-999'),
             nvl(iscd.liquid_volume,'-999'),
             nvl(iscd.liquid_volume_uom,'-999'),
             --
             (case when trunc(fc.active_date) <= LP_vdate then LP_vdate else trunc(fc.active_date) end),
             '-1',  -- Pack No
             '-1',  -- Item
             0,   --process_id
             0,   --item_ref_id,
             0,   --loc_ref_id,
             0,   --sup_ref_id,
             im.item_xform_ind
        from l10n_br_extax_dest_group_help sup_help,
             l10n_br_extax_dest_group_help loc_help,
             --
             future_cost fc,
             item_supp_country_dim iscd,
             item_master im,
             (select v.item,
                     v.origin_code,
                     v.service_ind,
                     v.classification_id,
                     v.ncm_char_code,
                     v.ex_ipi,
                     v.pauta_code,
                     v.service_code,
                     v.federal_service,
                     v.state_of_manufacture,
                     v.pharma_list_type,
                     ca.default_loc location,
                     ca.default_loc_type loc_type
                from v_br_item_fiscal_attrib v,
                     country_attrib ca
              where v.country_id = ca.country_id ) vbi,
             (select entity item,
                     listagg (name_value_pair_id, '~') within group (order by name_value_pair_id) item_name_value_pair_id_concat
                from l10n_br_extax_help_nv_pair
               where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
               group by entity) nv
       where sup_help.entity_type              = L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
         and sup_help.entity                   = to_char(fc.supplier)
         and sup_help.city                     is not null
         and sup_help.cnae_code_concatenation  is not null
         --
         and loc_help.entity_type              in(L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                  L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,
                                                  L10N_BR_EXTAX_MAINT_SQL.PTNR_ENTITY)
         -- Future Cost Location
         -- Default Location
         -- External finishers are not allowed as default location so no need to decode on them in vbi set
         and loc_help.entity_type              = decode(vbi.loc_type,
                                                        'W',L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                        'S',L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY)
         and loc_help.entity                   = to_char(vbi.location)
         and NOT EXISTS (select '1'
                           from future_cost fc1
                          where fc1.item = vbi.item
                            and fc1.supplier = fc.supplier
                            and fc1.origin_country_id = fc.origin_country_id
                            and fc1.location = vbi.location
                            and fc1.loc_type = vbi.loc_type
                            and rownum = 1)
         --
         and loc_help.city                     is not null
         and loc_help.cnae_code_concatenation  is not null
         --
         and fc.item                           = vbi.item
         and vbi.origin_code                   = '0'
         --
         and fc.item                           = im.item
         and im.status                         = 'A'
         and im.tran_level                     = im.item_level
         and im.pack_ind                       = 'N'
         and (im.orderable_ind      = 'Y' or
              (im.orderable_ind     = 'N' and
               im.deposit_item_type = 'A'))
         --
         and 'EA'                               = iscd.dim_object(+)
         and fc.item                            = iscd.item(+)
         and fc.supplier                        = iscd.supplier(+)
         and fc.origin_country_id               = iscd.origin_country(+)
         --
         and fc.item                            = nv.item(+)
         ) inner) inner2;

   if SETUP_TAX_CALL_ROUTING_COST(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END SEED_GTAX_SETUP_COST;
-----------------------------------------------------------------------------
FUNCTION SEED_GTAX_FINISH_COST(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SEED_GTAX_FINISH_COST';

BEGIN

   if RESULT_TO_ISCL(O_error_message) = FALSE then
      return FALSE;
   end if;

   if UPD_ITEM_SUPP_COUNTRY_LOC(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if UPD_ITEM_SUPP_COUNTRY(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if INS_ITEM_COST_HEAD(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if INS_ITEM_COST_DETAIL(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if UPD_FUTURE_COST_CURRENT(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SEED_GTAX_FINISH_COST;
-----------------------------------------------------------------------------
FUNCTION RESULT_TO_ISCL(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.RESULT_TO_ISCL';

BEGIN

   -- Non Pack
   insert into l10n_br_extax_help_cost_gtt (
           item,
           supplier,
           origin_country_id,
           loc,
           loc_type,
           effective_from_date,
           item_parent,
           item_grandparent,
           --
           sup_city,
           sup_cnae_code,
           sup_iss,
           sup_simples,
           sup_ipi,
           sup_icms,
           sup_st,
           sup_cnpj,
           sup_is_income_range_eligible,
           sup_is_distr_a_manufacturer,
           sup_icms_simples_rate,
           sup_tax_regime_concat,
           sup_rural_prod_ind,
           sup_name_value_pair_id_concat,
           --
           loc_city,
           loc_cnae_code,
           loc_iss,
           loc_ipi,
           loc_icms,
           loc_cnpj,
           loc_name_value_pair_id_concat,
           --
           origin_code,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           unit_cost,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           item_xform_ind,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           --
           cum_tax_pct,
           cum_tax_value,
           total_tax_amount,
           total_recoverable_amount,
           total_tax_amount_nic_incl)
  select /*+ parallel(iscl,12) parallel(im,8) */ distinct
          iscl.item,
          iscl.supplier,
          iscl.origin_country_id,
          decode(inner.loc,ca.default_loc,inner.loc,iscl.location),
          inner.loc_type,
          nvl(iscl.active_date,inner.effective_from_date),
          im.item_parent,
          im.item_grandparent,
          --
          inner.sup_city,
          inner.sup_cnae_code,
          inner.sup_iss,
          inner.sup_simples,
          inner.sup_ipi,
          inner.sup_icms,
          inner.sup_st,
          inner.sup_cnpj,
          inner.sup_is_income_range_eligible,
          inner.sup_is_distr_a_manufacturer,
          inner.sup_icms_simples_rate,
          inner.sup_tax_regime_concat,
          inner.sup_rural_prod_ind,
          inner.sup_name_value_pair_id_concat,
          --
          inner.loc_city,
          inner.loc_cnae_code,
          inner.loc_iss,
          inner.loc_ipi,
          inner.loc_icms,
          inner.loc_cnpj,
          inner.loc_name_value_pair_id_concat,
          --
          inner.origin_code,
          inner.classification_id,
          inner.ncm_char_code,
          inner.ex_ipi,
          inner.pauta_code,
          inner.service_code,
          inner.federal_service,
          inner.unit_cost,
          inner.dim_object,
          inner.length,
          inner.lwh_uom,
          inner.weight,
          inner.net_weight,
          inner.weight_uom,
          inner.liquid_volume,
          inner.liquid_volume_uom,
          inner.item_xform_ind,
          inner.state_of_manufacture,
          inner.pharma_list_type,
          inner.item_name_value_pair_id_concat,
          --
          inner.cum_tax_pct,
          inner.cum_tax_value,
          inner.total_tax_amount,
          inner.total_recoverable_amount,
          inner.total_tax_amount_nic_incl
     from future_cost iscl,
          item_master im,
          item_supp_country_dim dim,
          country_attrib  ca,
          (select /*+ parallel(item_map,8) parallel(result,8)*/
                  item_map.item,
                  sup_map.entity supplier,
                  loc_map.entity loc,
                  loc_map.loc_type,
                  item_map.country_id,
                  result.effective_date effective_from_date,
                  --
                  result.sup_city,
                  result.sup_cnae_code,
                  result.sup_iss,
                  result.sup_simples,
                  result.sup_ipi,
                  result.sup_icms,
                  result.sup_st,
                  result.sup_cnpj,
                  result.sup_is_income_range_eligible,
                  result.sup_is_distr_a_manufacturer,
                  result.sup_icms_simples_rate,
                  result.sup_tax_regime_concat,
                  result.sup_rural_prod_ind,
                  result.sup_name_value_pair_id_concat,
                  --
                  result.loc_city,
                  result.loc_cnae_code,
                  result.loc_iss,
                  result.loc_ipi,
                  result.loc_icms,
                  result.loc_cnpj,
                  result.loc_name_value_pair_id_concat,
                  --
                  result.origin_code,
                  result.classification_id,
                  result.ncm_char_code,
                  result.ex_ipi,
                  result.pauta_code,
                  result.service_ind,
                  result.service_code,
                  result.federal_service,
                  result.unit_cost,
                  result.dim_object,
                  result.length,
                  result.lwh_uom,
                  result.weight,
                  result.net_weight,
                  result.weight_uom,
                  result.liquid_volume,
                  result.liquid_volume_uom,
                  result.item_xform_ind,
                  result.state_of_manufacture,
                  result.pharma_list_type,
                  result.item_name_value_pair_id_concat,
                  result.pack_no,
                  --
                  result.cum_tax_pct,
                  result.cum_tax_value,
                  result.total_tax_amount,
                  result.total_recoverable_amount,
                  result.total_tax_amount_nic_incl
             from l10n_br_extax_res_cost result,
                  l10n_br_extax_dest_group_help loc_map,
                  l10n_br_extax_dest_group_help sup_map,
                  (select v.*,
                          nv.item_name_value_pair_id_concat
                     from v_br_item_fiscal_attrib v,
                          (select entity item,
                                  listagg (name_value_pair_id, '~') within group
                                     (order by name_value_pair_id) item_name_value_pair_id_concat
                             from l10n_br_extax_help_nv_pair
                            where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
                            group by entity) nv
                    where v.item         = nv.item(+)) item_map
            where result.origin_code               = '0'
              and item_map.origin_code             = '0'
              --
              and ((result.service_ind = 'N'
                    and result.classification_id         = nvl(item_map.classification_id,'-1')
                    and result.ex_ipi                    = nvl(item_map.ex_ipi,'-1')
                    and result.state_of_manufacture      = nvl(item_map.state_of_manufacture,'-1')
                    and result.pharma_list_type          = nvl(item_map.pharma_list_type,'-1'))
                  or
                   (result.service_ind = 'Y'
                    and result.service_code              = nvl(item_map.service_code,'-1')
                    and result.federal_service           = nvl(item_map.federal_service,'-1')))
              --
              and result.ncm_char_code             = nvl(item_map.ncm_char_code,'-1')
              and result.pauta_code                = nvl(item_map.pauta_code,'-1')
              and result.item_name_value_pair_id_concat = nvl(item_map.item_name_value_pair_id_concat,'-1')
              and result.pack_no                   = '-1'
              --
              and loc_map.city                     is not null
              and loc_map.cnae_code_concatenation  is not null
              --
              and result.loc_city                  = loc_map.city
              and result.loc_cnae_code             = loc_map.cnae_code_concatenation
              and result.loc_cnpj                  = loc_map.cnpj
              and result.loc_iss                   = nvl(loc_map.iss_contrib_ind,'N')
              and result.loc_ipi                   = nvl(loc_map.ipi_ind,'N')
              and result.loc_icms                  = nvl(loc_map.icms_contrib_ind,'N')
              and loc_map.entity_type                 != L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
              and result.loc_name_value_pair_id_concat = nvl(loc_map.name_value_pair_id_concat,'-1')
              --
              and result.sup_city                  = sup_map.city
              and result.sup_cnae_code             = sup_map.cnae_code_concatenation
              and result.sup_cnpj                  = sup_map.cnpj
              and result.sup_iss                   = nvl(sup_map.iss_contrib_ind,'N')
              and result.sup_simples               = nvl(sup_map.simples_ind,'N')
              and result.sup_ipi                   = nvl(sup_map.ipi_ind,'N')
              and result.sup_icms                  = nvl(sup_map.icms_contrib_ind,'N')
              and result.sup_st                    = nvl(sup_map.st_contrib_ind,'N')
              and result.sup_is_income_range_eligible  = nvl(sup_map.is_income_range_eligible,'N')
              and result.sup_is_distr_a_manufacturer   = nvl(sup_map.is_distr_a_manufacturer,'N')
              and result.sup_icms_simples_rate         = nvl(sup_map.icms_simples_rate,'-1')
              and result.sup_tax_regime_concat         = nvl(sup_map.tax_regime_concat,'N')
              and result.sup_rural_prod_ind            = nvl(sup_map.rural_prod_ind,'N')
              and sup_map.entity_type                  = L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
              and result.sup_name_value_pair_id_concat = nvl(sup_map.name_value_pair_id_concat,'-1')
              ) inner
  where inner.item              = iscl.item
    and inner.supplier          = iscl.supplier
    and 'BR'                    = iscl.origin_country_id
    and (
         (    inner.loc               = iscl.location
          and inner.loc_type          = iscl.loc_type)
         or
        (    inner.loc               = ca.default_loc
          and inner.loc_type          = ca.default_loc_type
          and NOT EXISTS (select '1'
                           from future_cost fc1
                          where fc1.item = iscl.item
                            and fc1.supplier = iscl.supplier
                            and fc1.origin_country_id = iscl.origin_country_id
                            and fc1.location = ca.default_loc
                            and fc1.loc_type = ca.default_loc_type
                            and rownum =1)))
    --
    and inner.unit_cost         = iscl.base_cost
    --
    and ca.country_id           = inner.country_id
    --
    and iscl.item               = im.item
    and im.status               = 'A'
    and im.tran_level           = im.item_level
    and im.pack_ind             = 'N'
    and (im.orderable_ind        = 'Y' or
          (im.orderable_ind      = 'N' and
           im.deposit_item_type  = 'A'))
    --
    and ((inner.service_ind = 'N' and inner.item_xform_ind = nvl(im.item_xform_ind,'-1'))
         or (inner.service_ind = 'Y'))
    --
    and 'EA'                    = dim.dim_object(+)
    and iscl.item               = dim.item(+)
    and iscl.supplier           = dim.supplier(+)
    and iscl.origin_country_id  = dim.origin_country(+)
    --
    --
    and inner.dim_object        = nvl(dim.dim_object,'-999')
    and inner.length            = nvl(dim.length,'-999')
    and inner.lwh_uom           = nvl(dim.lwh_uom,'-999')
    and inner.weight            = nvl(dim.weight,'-999')
    and inner.net_weight        = nvl(dim.net_weight,'-999')
    and inner.weight_uom        = nvl(dim.weight_uom,'-999')
    and inner.liquid_volume     = nvl(dim.liquid_volume,'-999')
    and inner.liquid_volume_uom = nvl(dim.liquid_volume_uom,'-999');


   -- Simple and Complex Pack, Item Fiscal Attributes and Dimension info ar informational in GTT
   insert into l10n_br_extax_help_cost_gtt (
           item,
           supplier,
           origin_country_id,
           loc,
           loc_type,
           effective_from_date,
           item_parent,
           item_grandparent,
           --
           sup_city,
           sup_cnae_code,
           sup_iss,
           sup_simples,
           sup_ipi,
           sup_icms,
           sup_st,
           sup_cnpj,
           sup_is_income_range_eligible,
           sup_is_distr_a_manufacturer,
           sup_icms_simples_rate,
           sup_tax_regime_concat,
           sup_rural_prod_ind,
           sup_name_value_pair_id_concat,
           --
           loc_city,
           loc_cnae_code,
           loc_iss,
           loc_ipi,
           loc_icms,
           loc_cnpj,
           loc_name_value_pair_id_concat,
           --
           origin_code,
           classification_id,
           ncm_char_code,
           ex_ipi,
           pauta_code,
           service_code,
           federal_service,
           unit_cost,
           dim_object,
           length,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom,
           item_xform_ind,
           state_of_manufacture,
           pharma_list_type,
           item_name_value_pair_id_concat,
           --
           --
           cum_tax_pct,
           cum_tax_value,
           total_tax_amount,
           total_recoverable_amount,
           total_tax_amount_nic_incl)
  select /*+ parallel(il,12) parallel(im,8) */ distinct
          iscl.item,
          iscl.supplier,
          iscl.origin_country_id,
          decode(loc_map.entity,ca.default_loc,loc_map.entity,iscl.location),
          decode(loc_map.entity_type,L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,'S',L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,'W'),
          nvl(iscl.active_date,inner.effective_from_date),
          im.item_parent,
          im.item_grandparent,
          --
          inner.sup_city,
          inner.sup_cnae_code,
          inner.sup_iss,
          inner.sup_simples,
          inner.sup_ipi,
          inner.sup_icms,
          inner.sup_st,
          inner.sup_cnpj,
          inner.sup_is_income_range_eligible,
          inner.sup_is_distr_a_manufacturer,
          inner.sup_icms_simples_rate,
          inner.sup_tax_regime_concat,
          inner.sup_rural_prod_ind,
          inner.sup_name_value_pair_id_concat,
          --
          inner.loc_city,
          inner.loc_cnae_code,
          inner.loc_iss,
          inner.loc_ipi,
          inner.loc_icms,
          inner.loc_cnpj,
          inner.loc_name_value_pair_id_concat,
          --
          '0', --origin_code,
          '-1', --classification_id,
          '-1', --ncm_char_code,
          '-1', --ex_ipi,
          '-1', --pauta_code,
          '-1', --service_code,
          '-1', --federal_service,
          iscl.base_cost,
          '-999', --dim_object
          '-999', --length
          '-999', --lwh_uom
          '-999', -- weight
          '-999', -- net_weight
          '-999', --weight_uom,'-999'),
          '-999', --liquid_volume,'-999'),
          '-999', --liquid_volume_uom,'-999'),
          'N',-- item_xform_ind, 
          '-1', --state_of_manufacture,
          '-1', --pharma_list_type,
          '-1', --item_name_value_pair_id_concat,
          --
          inner.cum_tax_pct,
          inner.cum_tax_value,
          inner.total_tax_amount,
          inner.total_recoverable_amount,
          inner.total_tax_amount_nic_incl
     from future_cost iscl,
          item_master im,
          l10n_br_extax_dest_group_help loc_map,
          l10n_br_extax_dest_group_help sup_map,
          country_attrib ca,
          (select /*+ parallel(item_map,8) parallel(result,8)*/
                  result.pack_no item,
                  result.country_id,
                  result.effective_from_date,
                  result.item_xform_ind,
                  --
                  result.sup_city,
                  result.sup_cnae_code,
                  result.sup_iss,
                  result.sup_simples,
                  result.sup_ipi,
                  result.sup_icms,
                  result.sup_st,
                  result.sup_cnpj,
                  result.sup_is_income_range_eligible,
                  result.sup_is_distr_a_manufacturer,
                  result.sup_icms_simples_rate,
                  result.sup_tax_regime_concat,
                  result.sup_rural_prod_ind,
                  result.sup_name_value_pair_id_concat,
                  --
                  result.loc_city,
                  result.loc_cnae_code,
                  result.loc_iss,
                  result.loc_ipi,
                  result.loc_icms,
                  result.loc_cnpj,
                  result.loc_name_value_pair_id_concat,
                  --
                  avg(result.cum_tax_pct) cum_tax_pct,
                  avg(result.cum_tax_value) cum_tax_value,
                  sum(result.total_tax_amount) total_tax_amount,
                  sum(result.total_recoverable_amount) total_recoverable_amount,
                  sum(result.total_tax_amount_nic_incl) total_tax_amount_nic_incl
             from (select /*+ parallel(item_map,8) parallel(result,8)*/   distinct
                          r.pack_no,
                          r.item,
                          item_map.country_id,
                          r.effective_date effective_from_date,
                          r.item_xform_ind,
                          --
                          r.sup_city,
                          r.sup_cnae_code,
                          r.sup_iss,
                          r.sup_simples,
                          r.sup_ipi,
                          r.sup_icms,
                          r.sup_st,
                          r.sup_cnpj,
                          r.sup_is_income_range_eligible,
                          r.sup_is_distr_a_manufacturer,
                          r.sup_icms_simples_rate,
                          r.sup_tax_regime_concat,
                          r.sup_rural_prod_ind,
                          r.sup_name_value_pair_id_concat,
                          --
                          r.loc_city,
                          r.loc_cnae_code,
                          r.loc_iss,
                          r.loc_ipi,
                          r.loc_icms,
                          r.loc_cnpj,
                          r.loc_name_value_pair_id_concat,
                          --
                          null cum_tax_pct,
                          null cum_tax_value,
                          r.total_tax_amount,
                          r.total_recoverable_amount,
                          r.total_tax_amount_nic_incl
                     from l10n_br_extax_res_cost r,
                          (select v.*,
                                  nv.item_name_value_pair_id_concat
                             from v_br_item_fiscal_attrib v,
                                  (select entity item,
                                          listagg (name_value_pair_id, '~') within group
                                             (order by name_value_pair_id) item_name_value_pair_id_concat
                                     from l10n_br_extax_help_nv_pair
                                    where entity_type = L10N_BR_EXTAX_MAINT_SQL.ITEM_ENTITY
                                    group by entity) nv
                    where v.item         = nv.item(+)) item_map
                    where r.origin_code               = '0'
                      and item_map.origin_code        = '0'
                      and r.ncm_char_code             = nvl(item_map.ncm_char_code,'-1')
                      and r.pauta_code                = nvl(item_map.pauta_code,'-1')
                      and ((r.service_ind = 'N'
                            and r.classification_id         = nvl(item_map.classification_id,'-1')
                            and r.ex_ipi                    = nvl(item_map.ex_ipi,'-1')
                            and r.state_of_manufacture      = nvl(item_map.state_of_manufacture,'-1')
                            and r.pharma_list_type          = nvl(item_map.pharma_list_type,'-1'))
                          or
                           (r.service_ind = 'Y'
                            and r.service_code              = nvl(item_map.service_code,'-1')
                            and r.federal_service           = nvl(item_map.federal_service,'-1')))
                      and r.item_name_value_pair_id_concat = nvl(item_map.item_name_value_pair_id_concat,'-1')
                      and r.pack_no <> '-1' ) result
           group by result.pack_no,
                    result.country_id,
                    result.effective_from_date,
                    result.item_xform_ind,
                    --
                    result.sup_city,
                    result.sup_cnae_code,
                    result.sup_iss,
                    result.sup_simples,
                    result.sup_ipi,
                    result.sup_icms,
                    result.sup_st,
                    result.sup_cnpj,
                    result.sup_is_income_range_eligible,
                    result.sup_is_distr_a_manufacturer,
                    result.sup_icms_simples_rate,
                    result.sup_tax_regime_concat,
                    result.sup_rural_prod_ind,
                    result.sup_name_value_pair_id_concat,
                    --
                    result.loc_city,
                    result.loc_cnae_code,
                    result.loc_iss,
                    result.loc_ipi,
                    result.loc_icms,
                    result.loc_cnpj,
                    result.loc_name_value_pair_id_concat
              --
          ) inner
  where iscl.item               = inner.item
    and iscl.supplier           = sup_map.entity
    and iscl.origin_country_id  = 'BR'
    and (
         (    loc_map.entity               = iscl.location
          and loc_map.entity_type          = decode(iscl.loc_type,'W',L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                                  'S',L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY))
         or
        (    loc_map.entity               = ca.default_loc
          and loc_map.entity_type          = decode(ca.default_loc_type,'W',L10N_BR_EXTAX_MAINT_SQL.WH_ENTITY,
                                                                        'S',L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY)
          and NOT EXISTS (select '1'
                           from future_cost fc1
                          where fc1.item = iscl.item
                            and fc1.supplier = iscl.supplier
                            and fc1.origin_country_id = iscl.origin_country_id
                            and fc1.location = ca.default_loc
                            and fc1.loc_type = ca.default_loc_type
                            and rownum =1)))
    --
    and ca.country_id           = inner.country_id
    --
    and inner.item              = im.item
    and im.status               = 'A'
    and im.tran_level           = im.item_level
    and im.pack_ind             = 'Y'
    and im.orderable_ind        = 'Y'
    --
    and loc_map.entity_type             != L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
    and loc_map.city                     is not null
    and loc_map.cnae_code_concatenation  is not null
    and inner.loc_city                  = loc_map.city
    and inner.loc_cnae_code             = loc_map.cnae_code_concatenation
    and inner.loc_cnpj                  = loc_map.cnpj
    and inner.loc_iss                   = nvl(loc_map.iss_contrib_ind,'N')
    and inner.loc_ipi                   = nvl(loc_map.ipi_ind,'N')
    and inner.loc_icms                  = nvl(loc_map.icms_contrib_ind,'N')
    and inner.loc_name_value_pair_id_concat = nvl(loc_map.name_value_pair_id_concat,'-1')
    --
    and sup_map.entity_type             = L10N_BR_EXTAX_MAINT_SQL.SUPP_ENTITY
    and inner.sup_cnae_code             = sup_map.cnae_code_concatenation
    and inner.sup_cnpj                  = sup_map.cnpj
    and inner.sup_iss                   = nvl(sup_map.iss_contrib_ind,'N')
    and inner.sup_simples               = nvl(sup_map.simples_ind,'N')
    and inner.sup_ipi                   = nvl(sup_map.ipi_ind,'N')
    and inner.sup_icms                  = nvl(sup_map.icms_contrib_ind,'N')
    and inner.sup_st                    = nvl(sup_map.st_contrib_ind,'N')
    and inner.sup_is_income_range_eligible  = nvl(sup_map.is_income_range_eligible,'N')
    and inner.sup_is_distr_a_manufacturer   = nvl(sup_map.is_distr_a_manufacturer,'N')
    and inner.sup_icms_simples_rate         = nvl(sup_map.icms_simples_rate,'-1')
    and inner.sup_tax_regime_concat         = nvl(sup_map.tax_regime_concat,'N')
    and inner.sup_rural_prod_ind            = nvl(sup_map.rural_prod_ind,'N')
    and inner.sup_name_value_pair_id_concat = nvl(sup_map.name_value_pair_id_concat,'-1');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RESULT_TO_ISCL;
-----------------------------------------------------------------------------
FUNCTION UPD_ITEM_SUPP_COUNTRY_LOC(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.UPD_ITEM_SUPP_COUNTRY_LOC';
   L_vdate       DATE := get_vdate;

BEGIN

   merge into item_supp_country_loc target using (
      select distinct gtt.item,
             gtt.supplier,
             gtt.origin_country_id,
             gtt.loc,
             --
             first_value(gtt.total_tax_amount)
                over(partition by gtt.item,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.effective_from_date desc) total_tax_amount,
             first_value(gtt.total_recoverable_amount)
                over(partition by gtt.item,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.effective_from_date desc) total_recoverable_amount,
             first_value(gtt.total_tax_amount_nic_incl)
                over(partition by gtt.item,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.effective_from_date desc) total_tax_amount_nic_incl,
             first_value(ca.default_po_cost)
                over(partition by gtt.item,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.effective_from_date desc) default_po_cost
        from l10n_br_extax_help_cost_gtt gtt,
             item_country ic,
             country_attrib ca
       where gtt.item      = ic.item
         and ic.country_id = ca.country_id
         and gtt.effective_from_date <= L_vdate
      union all
      select distinct gtt.item_parent item,
             gtt.supplier,
             gtt.origin_country_id,
             gtt.loc,
             --
             first_value(gtt.total_tax_amount)
                over(partition by gtt.item_parent,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.item, gtt.effective_from_date desc) total_tax_amount,
             first_value(gtt.total_recoverable_amount)
                over(partition by gtt.item_parent,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.item, gtt.effective_from_date desc) total_recoverable_amount,
             first_value(gtt.total_tax_amount_nic_incl)
                over(partition by gtt.item_parent,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.item, gtt.effective_from_date desc) total_tax_amount_nic_incl,
             first_value(ca.default_po_cost)
                over(partition by gtt.item_parent,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.item, gtt.effective_from_date desc) default_po_cost
        from l10n_br_extax_help_cost_gtt gtt,
             item_country ic,
             country_attrib ca
       where gtt.item_parent = ic.item
         and ic.country_id   = ca.country_id
         and gtt.effective_from_date <= L_vdate
      union all
      select distinct gtt.item_grandparent item,
             gtt.supplier,
             gtt.origin_country_id,
             gtt.loc,
             --
             first_value(gtt.total_tax_amount)
                over(partition by gtt.item_grandparent,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.item, gtt.effective_from_date desc) total_tax_amount,
             first_value(gtt.total_recoverable_amount)
                over(partition by gtt.item_grandparent,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.item, gtt.effective_from_date desc) total_recoverable_amount,
             first_value(gtt.total_tax_amount_nic_incl)
                over(partition by gtt.item_grandparent,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.item, gtt.effective_from_date desc) total_tax_amount_nic_incl,
             first_value(ca.default_po_cost)
                over(partition by gtt.item_grandparent,gtt.supplier,gtt.origin_country_id,gtt.loc
                     order by gtt.item, gtt.effective_from_date desc) default_po_cost
        from l10n_br_extax_help_cost_gtt gtt,
             item_country ic,
             country_attrib ca
       where gtt.item_grandparent = ic.item
         and ic.country_id        = ca.country_id
         and gtt.effective_from_date <= L_vdate) use_this
   on (    target.item              = use_this.item
       and target.supplier          = use_this.supplier
       and target.origin_country_id = use_this.origin_country_id
       and target.loc               = use_this.loc)
   when matched then
   update set target.unit_cost            = decode(use_this.default_po_cost,
                                                   'NIC', target.unit_cost + nvl(use_this.total_tax_amount_nic_incl,0),
                                                   'BC',  target.unit_cost),
              target.negotiated_item_cost = target.unit_cost + nvl(use_this.total_tax_amount_nic_incl,0),
              target.extended_base_cost   = target.unit_cost +
                                               nvl(use_this.total_tax_amount,0) - nvl(use_this.total_recoverable_amount,0),
              target.inclusive_cost       = target.unit_cost + nvl(use_this.total_tax_amount,0),
              target.base_cost            = target.unit_cost,
              target.last_update_datetime = sysdate,
              target.last_update_id       = user;

   merge into item_loc_soh target using (
      select il.item,
             il.loc,
             iscl.negotiated_item_cost,
             iscl.extended_base_cost
        from item_loc il,
             item_supp_country_loc iscl
       where il.item          = iscl.item
         and il.loc           = iscl.loc
         and il.primary_supp  = iscl.supplier
         and il.primary_cntry = iscl.origin_country_id) use_this
   on (    target.item  = use_this.item
       and target.loc   = use_this.loc)
   when matched then
   update set target.unit_cost            = nvl(use_this.negotiated_item_cost, target.unit_cost),
              --target.av_cost              = nvl(use_this.extended_base_cost, target.av_cost), 
              target.last_update_datetime = sysdate,
              target.last_update_id       = user;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_ITEM_SUPP_COUNTRY_LOC;
-----------------------------------------------------------------------------
FUNCTION UPD_ITEM_SUPP_COUNTRY(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.UPD_ITEM_SUPP_COUNTRY';
   L_vdate       DATE := get_vdate;

BEGIN

   merge into item_supp_country target using (
      select distinct iscl.item,
             iscl.supplier,
             iscl.origin_country_id,
             --
             first_value(iscl.unit_cost)
                over(partition by gtt.item,iscl.supplier,iscl.origin_country_id
                     order by gtt.effective_from_date desc) unit_cost,
             first_value(iscl.negotiated_item_cost)
                over(partition by gtt.item,iscl.supplier,iscl.origin_country_id
                     order by gtt.effective_from_date desc) negotiated_item_cost,
             first_value(iscl.extended_base_cost)
                over(partition by gtt.item,iscl.supplier,iscl.origin_country_id
                     order by gtt.effective_from_date desc) extended_base_cost,
             first_value(iscl.inclusive_cost)
                over(partition by gtt.item,iscl.supplier,iscl.origin_country_id
                     order by gtt.effective_from_date desc) inclusive_cost,
             first_value(iscl.base_cost)
                over(partition by gtt.item,iscl.supplier,iscl.origin_country_id
                     order by gtt.effective_from_date desc) base_cost
        from item_supp_country_loc iscl,
             l10n_br_extax_help_cost_gtt gtt
       where iscl.primary_loc_ind   = 'Y'
         and iscl.item              = gtt.item
         and iscl.supplier          = gtt.supplier
         and iscl.origin_country_id = gtt.origin_country_id
         and iscl.loc               = gtt.loc
         and gtt.effective_from_date <= L_vdate
      union all
      select distinct
             iscl.item,
             iscl.supplier,
             iscl.origin_country_id,
             --
             first_value(iscl.unit_cost)
                over(partition by gtt.item_parent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) unit_cost,
             first_value(iscl.negotiated_item_cost)
                over(partition by gtt.item_parent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) negotiated_item_cost,
             first_value(iscl.extended_base_cost)
                over(partition by gtt.item_parent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) extended_base_cost,
             first_value(iscl.inclusive_cost)
                over(partition by gtt.item_parent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) inclusive_cost,
             first_value(iscl.base_cost)
                over(partition by gtt.item_parent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) base_cost
        from item_supp_country_loc iscl,
             l10n_br_extax_help_cost_gtt gtt
       where iscl.primary_loc_ind   = 'Y'
         and iscl.item              = gtt.item_parent
         and iscl.supplier          = gtt.supplier
         and iscl.origin_country_id = gtt.origin_country_id
         and iscl.loc               = gtt.loc
         and gtt.effective_from_date <= L_vdate
      union all
      select distinct
             iscl.item,
             iscl.supplier,
             iscl.origin_country_id,
             --
             first_value(iscl.unit_cost)
                over(partition by gtt.item_grandparent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) unit_cost,
             first_value(iscl.negotiated_item_cost)
                over(partition by gtt.item_grandparent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) negotiated_item_cost,
             first_value(iscl.extended_base_cost)
                over(partition by gtt.item_grandparent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) extended_base_cost,
             first_value(iscl.inclusive_cost)
                over(partition by gtt.item_grandparent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) inclusive_cost,
             first_value(iscl.base_cost)
                over(partition by gtt.item_grandparent,iscl.supplier,iscl.origin_country_id
                     order by gtt.item, gtt.effective_from_date desc) base_cost
        from item_supp_country_loc iscl,
             l10n_br_extax_help_cost_gtt gtt
       where iscl.primary_loc_ind   = 'Y'
         and iscl.item              = gtt.item_grandparent
         and iscl.supplier          = gtt.supplier
         and iscl.origin_country_id = gtt.origin_country_id
         and iscl.loc               = gtt.loc
         and gtt.effective_from_date <= L_vdate) use_this
   on (    target.item              = use_this.item
       and target.supplier          = use_this.supplier
       and target.origin_country_id = use_this.origin_country_id)
   when matched then
   update set target.unit_cost            = use_this.unit_cost,
              target.negotiated_item_cost = use_this.negotiated_item_cost,
              target.extended_base_cost   = use_this.extended_base_cost,
              target.inclusive_cost       = use_this.inclusive_cost,
              target.base_cost            = use_this.base_cost,
              target.last_update_datetime = sysdate,
              target.last_update_id       = user;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_ITEM_SUPP_COUNTRY;
-----------------------------------------------------------------------------
FUNCTION INS_ITEM_COST_HEAD(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.INS_ITEM_COST_HEAD';
   L_system_options_rec           SYSTEM_OPTIONS%ROWTYPE := NULL;
   L_vdate                        DATE := get_vdate;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
       return FALSE;
   end if;

   insert into item_cost_head(
             item,
             supplier,
             origin_country_id,
             delivery_country_id,
             prim_dlvy_ctry_ind,
             nic_static_ind,
             base_cost,
             extended_base_cost,
             negotiated_item_cost,
             inclusive_cost)
      select item,
             supplier,
             origin_country_id,
             delivery_country_id,
             prim_dlvry_ctry_ind,
             nic_static_ind,
             base_cost,
             extended_base_cost,
             negotiated_item_cost,
             inclusive_cost
        from (
      select inner.item,
             inner.supplier,
             inner.origin_country_id,
             inner.delivery_country_id,
             --
             inner.prim_dlvry_ctry_ind,
             inner.nic_static_ind,
             inner.base_cost,
             inner.extended_base_cost,
             inner.negotiated_item_cost,
             inner.inclusive_cost,
             --
             -- rank records by highest value cost fields
             dense_rank()
                over (partition by inner.item,
                                   inner.supplier,
                                   inner.origin_country_id,
                                   inner.delivery_country_id
                    order by inner.base_cost desc,
                             inner.extended_base_cost desc,
                             inner.extended_base_cost desc,
                             inner.negotiated_item_cost desc,
                             inner.inclusive_cost desc) rank
      from
     (select distinct
             gtt.item,
             gtt.supplier,
             gtt.origin_country_id,
             ic.country_id delivery_country_id,
             --
             decode(L_system_options_rec.base_country_id, ic.country_id, 'Y', 'N') prim_dlvry_ctry_ind,
             'N' nic_static_ind,
             decode(ca.item_cost_tax_incl_ind,
                    'N', isc.unit_cost,
                    'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) base_cost,
             decode(ca.item_cost_tax_incl_ind,
                    'N', isc.unit_cost,
                    'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) +
                nvl(gtt.total_tax_amount,0) - nvl(gtt.total_recoverable_amount,0) extended_base_cost,
             decode(ca.item_cost_tax_incl_ind,
                    'Y', isc.unit_cost,
                    'N', isc.unit_cost + nvl(gtt.total_tax_amount_nic_incl,0)) negotiated_item_cost,
             decode(ca.item_cost_tax_incl_ind,
                    'N', isc.unit_cost,
                    'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) +
                nvl(gtt.total_tax_amount,0) inclusive_cost,
             gtt.effective_from_date record_date,
             first_value(gtt.effective_from_date)
                over (partition by isc.item,gtt.supplier,gtt.origin_country_id,ic.country_id
                          order by gtt.effective_from_date desc) max_date
        from l10n_br_extax_help_cost_gtt gtt,
             item_supp_country isc,
             item_country ic,
             country_attrib ca
       where gtt.item              = isc.item
         and gtt.supplier          = isc.supplier
         and gtt.origin_country_id = isc.origin_country_id
         --
         and isc.item              = ic.item
         and isc.origin_country_id = ic.country_id
         --
         and ca.country_id         = ic.country_id
         --
         and gtt.loc               = ca.default_loc
         and gtt.effective_from_date <= L_vdate
      union all
      select distinct
             isc.item,
             gtt.supplier,
             gtt.origin_country_id,
             ic.country_id delivery_country_id,
             decode(L_system_options_rec.base_country_id, ic.country_id, 'Y', 'N') prim_dlvry_ctry_ind,
             'N' nic_static_ind,
             --
             first_value(
                decode(ca.item_cost_tax_incl_ind,
                       'N', isc.unit_cost,
                       'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) )
                   over(partition by gtt.item_parent,gtt.supplier,gtt.origin_country_id,ic.country_id
                        order by gtt.item) base_cost,
             first_value(
                decode(ca.item_cost_tax_incl_ind,
                       'N', isc.unit_cost,
                       'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) +
                   nvl(gtt.total_tax_amount,0) - nvl(gtt.total_recoverable_amount,0) )
                   over(partition by gtt.item_parent,gtt.supplier,gtt.origin_country_id,ic.country_id
                        order by gtt.item) extended_base_cost,
             first_value(
                decode(ca.item_cost_tax_incl_ind,
                       'Y', isc.unit_cost,
                       'N', isc.unit_cost + nvl(gtt.total_tax_amount_nic_incl,0)) )
                   over(partition by gtt.item_parent,gtt.supplier,gtt.origin_country_id,ic.country_id
                        order by gtt.item) negotiated_item_cost,
             first_value(
                decode(ca.item_cost_tax_incl_ind,
                       'N', isc.unit_cost,
                       'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) +
                   nvl(gtt.total_tax_amount,0) )
                   over(partition by gtt.item_parent,gtt.supplier,gtt.origin_country_id,ic.country_id
                        order by gtt.item) inclusive_cost,
             gtt.effective_from_date record_date,
             first_value(gtt.effective_from_date)
                over (partition by isc.item,gtt.supplier,gtt.origin_country_id,ic.country_id
                          order by gtt.effective_from_date desc) max_date
        from l10n_br_extax_help_cost_gtt gtt,
             item_supp_country isc,
             item_country ic,
             country_attrib ca
       where gtt.item_parent       = isc.item
         and gtt.supplier          = isc.supplier
         and gtt.origin_country_id = isc.origin_country_id
         --
         and isc.item              = ic.item
         and isc.origin_country_id = ic.country_id
         --
         and ca.country_id         = ic.country_id
         --
         and gtt.loc               = ca.default_loc
         and gtt.effective_from_date <= L_vdate
      union all
      select distinct
             isc.item,
             gtt.supplier,
             gtt.origin_country_id,
             ic.country_id delivery_country_id,
             decode(L_system_options_rec.base_country_id, ic.country_id, 'Y', 'N') prim_dlvry_ctry_ind,
             'N' nic_static_ind,
             --
             first_value(
                decode(ca.item_cost_tax_incl_ind,
                       'N', isc.unit_cost,
                       'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) )
                   over(partition by gtt.item_grandparent,gtt.supplier,gtt.origin_country_id,ic.country_id
                        order by gtt.item) base_cost,
             first_value(
                decode(ca.item_cost_tax_incl_ind,
                       'N', isc.unit_cost,
                       'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) +
                   nvl(gtt.total_tax_amount,0) - nvl(gtt.total_recoverable_amount,0) )
                   over(partition by gtt.item_grandparent,gtt.supplier,gtt.origin_country_id,ic.country_id
                        order by gtt.item) extended_base_cost,
             first_value(
                decode(ca.item_cost_tax_incl_ind,
                       'Y', isc.unit_cost,
                       'N', isc.unit_cost + nvl(gtt.total_tax_amount_nic_incl,0)) )
                   over(partition by gtt.item_grandparent,gtt.supplier,gtt.origin_country_id,ic.country_id
                        order by gtt.item) negotiated_item_cost,
             first_value(
                decode(ca.item_cost_tax_incl_ind,
                       'N', isc.unit_cost,
                       'Y', isc.unit_cost - nvl(gtt.total_tax_amount_nic_incl,0)) +
                   nvl(gtt.total_tax_amount,0) )
                   over(partition by gtt.item_grandparent,gtt.supplier,gtt.origin_country_id,ic.country_id
                        order by gtt.item) inclusive_cost,
             gtt.effective_from_date record_date,
             first_value(gtt.effective_from_date)
                over (partition by isc.item,gtt.supplier,gtt.origin_country_id,ic.country_id
                          order by gtt.effective_from_date desc) max_date
        from l10n_br_extax_help_cost_gtt gtt,
             item_supp_country isc,
             item_country ic,
             country_attrib ca
       where gtt.item_grandparent  = isc.item
         and gtt.supplier          = isc.supplier
         and gtt.origin_country_id = isc.origin_country_id
         --
         and isc.item              = ic.item
         and isc.origin_country_id = ic.country_id
         --
         and ca.country_id         = ic.country_id
         --
         and gtt.loc               = ca.default_loc
         and gtt.effective_from_date <= L_vdate) inner
   where inner.record_date = inner.max_date)
   where rank = 1;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INS_ITEM_COST_HEAD;
-----------------------------------------------------------------------------
FUNCTION INS_ITEM_COST_DETAIL(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.INS_ITEM_COST_DETAIL';
   L_vdate       DATE := get_vdate;

BEGIN

   insert into item_cost_detail (item,
                                 supplier,
                                 origin_country_id,
                                 delivery_country_id,
                                 cond_type,
                                 cond_value,
                                 applied_on,
                                 comp_rate,
                                 calculation_basis,
                                 recoverable_amount,
                                 modified_taxable_base)
   with item_tax_code_res as
   (select /*+ parallel(item_map,8) parallel(result,8)*/
                  distinct result.item item,
                  result.effective_date effective_from_date,
                  --
                  result.sup_city,
                  result.sup_cnae_code,
                  result.sup_iss,
                  result.sup_simples,
                  result.sup_ipi,
                  result.sup_icms,
                  result.sup_st,
                  result.sup_cnpj,
                  result.sup_is_income_range_eligible,
                  result.sup_is_distr_a_manufacturer,
                  result.sup_icms_simples_rate,
                  result.sup_rural_prod_ind,
                  result.sup_tax_regime_concat,
                  --
                  result.loc_city,
                  result.loc_cnae_code,
                  result.loc_iss,
                  result.loc_ipi,
                  result.loc_icms,
                  result.loc_cnpj,
                  --
                  nvl(result.origin_code, '0') origin_code,
                  result.classification_id,
                  result.ncm_char_code,
                  result.ex_ipi,
                  result.pauta_code,
                  result.service_code,
                  result.federal_service,
                  result.dim_object,
                  result.length,
                  result.lwh_uom,
                  result.weight,
                  result.net_weight,
                  result.weight_uom,
                  result.liquid_volume,
                  result.liquid_volume_uom,
                  result.item_xform_ind,
                  result.state_of_manufacture,
                  result.pharma_list_type,
                  --
                  result.tax_code,
                  result.calculation_basis ,
                  result.tax_amount tax_amount,
                  result.tax_basis_amount,
                  result.tax_rate,
                  result.recoverable_amount,
                  result.unit_cost,
                  result.modified_tax_basis_amount
             from l10n_br_extax_res_cost_det result,
                  v_br_item_fiscal_attrib item_map
            where result.classification_id         = nvl(item_map.classification_id,'-1')
              and result.ncm_char_code             = nvl(item_map.ncm_char_code,'-1')
              and result.ex_ipi                    = nvl(item_map.ex_ipi,'-1')
              and result.pauta_code                = nvl(item_map.pauta_code,'-1')
              and result.service_code              = nvl(item_map.service_code,'-1')
              and result.federal_service           = nvl(item_map.federal_service,'-1')
              and item_map.origin_code = nvl(result.origin_code, '0')
              and result.pack_no = '-1'
           union all
           select result.pack_no ,
                  result.effective_date effective_from_date,
                  --
                  result.sup_city,
                  result.sup_cnae_code,
                  result.sup_iss,
                  result.sup_simples,
                  result.sup_ipi,
                  result.sup_icms,
                  result.sup_st,
                  result.sup_cnpj,
                  result.sup_is_income_range_eligible,
                  result.sup_is_distr_a_manufacturer,
                  result.sup_icms_simples_rate,
                  result.sup_rural_prod_ind,
                  result.sup_tax_regime_concat,
                  --
                  result.loc_city,
                  result.loc_cnae_code,
                  result.loc_iss,
                  result.loc_ipi,
                  result.loc_icms,
                  result.loc_cnpj,
                  '0' origin_code,
                  '-1' classification_id,
                  '-1' ncm_char_code,
                  '-1' ex_ipi,
                  '-1' pauta_code,
                  '-1' service_code,
                  '-1' federal_service,
                  '-999' dim_object,
                  -999 length,
                  '-999' lwh_uom,
                  -999 weight,
                  -999 net_weight,
                  '-999' weight_uom,
                  -999 liquid_volume,
                  '-999' liquid_volume_uom,
                  'N' item_xform_ind,
                  '-1' state_of_manufacture,
                  '-1' pharma_list_type,
                  --
                  result.tax_code,
                  result.calculation_basis ,
                  sum(result.tax_amount) tax_amount,
                  DECODE(im.simple_pack_ind, 'Y', AVG(result.tax_basis_amount), NULL) tax_basis_amount,
                  DECODE(im.simple_pack_ind, 'Y', AVG(result.tax_rate), NULL) tax_rate,
                  sum(result.recoverable_amount) recoverable_amount,
                  sum(result.unit_cost) unit_cost, -- This sum is needed for complex pack component item cost sum up
                  DECODE(im.simple_pack_ind, 'Y',
                         AVG(result.modified_tax_basis_amount), NULL) modified_tax_basis_amount
        from   (select /*+ parallel(item_map,8) parallel(result,8)*/
                  distinct r.pack_no ,
                  r.item,
                  r.effective_date,
                  --
                  r.sup_city,
                  r.sup_cnae_code,
                  r.sup_iss,
                  r.sup_simples,
                  r.sup_ipi,
                  r.sup_icms,
                  r.sup_st,
                  r.sup_cnpj,
                  r.sup_is_income_range_eligible,
                  r.sup_is_distr_a_manufacturer,
                  r.sup_icms_simples_rate,
                  r.sup_rural_prod_ind,
                  r.sup_tax_regime_concat,
                  --
                  r.loc_city,
                  r.loc_cnae_code,
                  r.loc_iss,
                  r.loc_ipi,
                  r.loc_icms,
                  r.loc_cnpj,
                  --
                  r.tax_code,
                  r.calculation_basis,
                  r.tax_amount,
                  r.tax_basis_amount,
                  r.tax_rate,
                  r.recoverable_amount,
                  r.unit_cost,
                  r.modified_tax_basis_amount
             from l10n_br_extax_res_cost_det r,
                  v_br_item_fiscal_attrib item_map
            where r.classification_id         = nvl(item_map.classification_id,'-1')
              and r.ncm_char_code             = nvl(item_map.ncm_char_code,'-1')
              and r.ex_ipi                    = nvl(item_map.ex_ipi,'-1')
              and r.pauta_code                = nvl(item_map.pauta_code,'-1')
              and r.service_code              = nvl(item_map.service_code,'-1')
              and r.federal_service           = nvl(item_map.federal_service,'-1')
              and item_map.origin_code = nvl(r.origin_code,'0')
              and r.pack_no <> '-1') result,
             item_master im
       where result.pack_no = im.item
           group by result.pack_no,
                    im.simple_pack_ind,
                    result.effective_date,
                    --
                    result.sup_city,
                    result.sup_cnae_code,
                    result.sup_iss,
                    result.sup_simples,
                    result.sup_ipi,
                    result.sup_icms,
                    result.sup_st,
                    result.sup_cnpj,
                    result.sup_is_income_range_eligible,
                    result.sup_is_distr_a_manufacturer,
                    result.sup_icms_simples_rate,
                    result.sup_rural_prod_ind,
                    result.sup_tax_regime_concat,
                    --
                    result.loc_city,
                    result.loc_cnae_code,
                    result.loc_iss,
                    result.loc_ipi,
                    result.loc_icms,
                    result.loc_cnpj,
                    --
                    result.tax_code,
                    result.calculation_basis)
   select distinct gtt.item,
          gtt.supplier,
          gtt.origin_country_id,
          --
          first_value(ic.country_id)
             over(partition by gtt.item,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.effective_from_date desc) delivery_country_id,
          first_value(r.tax_code)
             over(partition by gtt.item,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.effective_from_date desc) tax_code,
          first_value(r.tax_amount)
             over(partition by gtt.item,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.effective_from_date desc) tax_amount,
          first_value(r.tax_basis_amount)
             over(partition by gtt.item,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.effective_from_date desc) tax_basis_amount,
          first_value(r.tax_rate)
             over(partition by gtt.item,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.effective_from_date desc) tax_rate,
          first_value(r.calculation_basis)
             over(partition by gtt.item,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.effective_from_date desc) calculation_basis,
          first_value(r.recoverable_amount)
             over(partition by gtt.item,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.effective_from_date desc) recoverable_amount,
          first_value(r.modified_tax_basis_amount)
             over(partition by gtt.item,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.effective_from_date desc) modified_tax_basis_amount
     from l10n_br_extax_help_cost_gtt gtt,
          item_supp_country isc,
          item_country ic,
          country_attrib ca,
          item_tax_code_res r
    where gtt.item              = isc.item
      and gtt.item              = decode(r.item,'-1',gtt.item ,r.item)
      and gtt.supplier          = isc.supplier
      and gtt.origin_country_id = isc.origin_country_id
      --
      and isc.item              = ic.item
      and isc.origin_country_id = ic.country_id
      --
      and ca.country_id         = ic.country_id
      --
      and gtt.loc               = ca.default_loc
      --
      and gtt.sup_city          = r.sup_city
      and gtt.sup_cnae_code     = r.sup_cnae_code
      and gtt.sup_iss           = r.sup_iss
      and gtt.sup_simples       = r.sup_simples
      and gtt.sup_ipi           = r.sup_ipi
      and gtt.sup_icms          = r.sup_icms
      and gtt.sup_st            = r.sup_st
      and gtt.sup_cnpj          = r.sup_cnpj
      and gtt.sup_is_income_range_eligible  = r.sup_is_income_range_eligible
      and gtt.sup_is_distr_a_manufacturer   = r.sup_is_distr_a_manufacturer
      and gtt.sup_icms_simples_rate         = r.sup_icms_simples_rate
      and gtt.sup_rural_prod_ind            = r.sup_rural_prod_ind
      and gtt.sup_tax_regime_concat         = r.sup_tax_regime_concat
      --
      and gtt.loc_city          = r.loc_city
      and gtt.loc_cnae_code     = r.loc_cnae_code
      and gtt.loc_iss           = r.loc_iss
      and gtt.loc_ipi           = r.loc_ipi
      and gtt.loc_icms          = r.loc_icms
      and gtt.loc_cnpj          = r.loc_cnpj
      --
      and nvl(gtt.origin_code,'0')       = r.origin_code
      and gtt.classification_id = r.classification_id
      and gtt.ncm_char_code     = r.ncm_char_code
      and gtt.ex_ipi            = r.ex_ipi
      and gtt.pauta_code        = r.pauta_code
      and gtt.service_code      = r.service_code
      and gtt.federal_service   = r.federal_service
      and gtt.dim_object        = r.dim_object
      and gtt.length            = r.length
      and gtt.lwh_uom           = r.lwh_uom
      and gtt.weight            = r.weight
      and gtt.net_weight        = r.net_weight
      and gtt.weight_uom        = r.weight_uom
      and gtt.liquid_volume     = r.liquid_volume
      and gtt.liquid_volume_uom = r.liquid_volume_uom
      and gtt.item_xform_ind    = r.item_xform_ind
      and gtt.state_of_manufacture = r.state_of_manufacture
      and gtt.pharma_list_type  = r.pharma_list_type
      --
      and gtt.unit_cost         = decode(r.item,'-1',r.unit_cost,gtt.unit_cost)
      --
      and gtt.effective_from_date <= L_vdate
   union all
   select distinct
          isc.item,
          gtt.supplier,
          gtt.origin_country_id,
          --
          first_value(ic.country_id)
             over(partition by gtt.item_parent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) delivery_country_id,
          first_value(r.tax_code)
             over(partition by gtt.item_parent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) tax_code,
          first_value(r.tax_amount)
             over(partition by gtt.item_parent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) tax_amount,
          first_value(r.tax_basis_amount)
             over(partition by gtt.item_parent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) tax_basis_amount,
          first_value(r.tax_rate)
             over(partition by gtt.item_parent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) tax_rate,
          first_value(r.calculation_basis)
             over(partition by gtt.item_parent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) calculation_basis,
          first_value(r.recoverable_amount)
             over(partition by gtt.item_parent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) recoverable_amount,
         first_value(r.modified_tax_basis_amount)
             over(partition by gtt.item_parent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc)  modified_tax_basis_amount
     from l10n_br_extax_help_cost_gtt gtt,
          item_supp_country isc,
          item_country ic,
          country_attrib ca,
          item_tax_code_res r
    where gtt.item_parent       = isc.item
      and gtt.item_parent       = decode(r.item,'-1',gtt.item_parent,r.item)
      and gtt.supplier          = isc.supplier
      and gtt.origin_country_id = isc.origin_country_id
      --
      and gtt.item              = ic.item
      and isc.origin_country_id = ic.country_id
      --
      and ca.country_id         = ic.country_id
      --
      and gtt.loc               = ca.default_loc
      --
      and gtt.sup_city          = r.sup_city
      and gtt.sup_cnae_code     = r.sup_cnae_code
      and gtt.sup_iss           = r.sup_iss
      and gtt.sup_simples       = r.sup_simples
      and gtt.sup_ipi           = r.sup_ipi
      and gtt.sup_icms          = r.sup_icms
      and gtt.sup_st            = r.sup_st
      and gtt.sup_cnpj          = r.sup_cnpj
      and gtt.sup_is_income_range_eligible  = r.sup_is_income_range_eligible
      and gtt.sup_is_distr_a_manufacturer   = r.sup_is_distr_a_manufacturer
      and gtt.sup_icms_simples_rate         = r.sup_icms_simples_rate
      and gtt.sup_rural_prod_ind            = r.sup_rural_prod_ind
      and gtt.sup_tax_regime_concat         = r.sup_tax_regime_concat
      --
      and gtt.loc_city          = r.loc_city
      and gtt.loc_cnae_code     = r.loc_cnae_code
      and gtt.loc_iss           = r.loc_iss
      and gtt.loc_ipi           = r.loc_ipi
      and gtt.loc_icms          = r.loc_icms
      and gtt.loc_cnpj          = r.loc_cnpj
      --
      and nvl(gtt.origin_code,'0')       = r.origin_code
      and gtt.classification_id = r.classification_id
      and gtt.ncm_char_code     = r.ncm_char_code
      and gtt.ex_ipi            = r.ex_ipi
      and gtt.pauta_code        = r.pauta_code
      and gtt.service_code      = r.service_code
      and gtt.federal_service   = r.federal_service
      and gtt.dim_object        = r.dim_object
      and gtt.length            = r.length
      and gtt.lwh_uom           = r.lwh_uom
      and gtt.weight            = r.weight
      and gtt.net_weight        = r.net_weight
      and gtt.weight_uom        = r.weight_uom
      and gtt.liquid_volume     = r.liquid_volume
      and gtt.liquid_volume_uom = r.liquid_volume_uom
      and gtt.item_xform_ind    = r.item_xform_ind
      and gtt.state_of_manufacture = r.state_of_manufacture
      and gtt.pharma_list_type  = r.pharma_list_type
      --
      and gtt.unit_cost         = decode(r.item,'-1',r.unit_cost,gtt.unit_cost)
      --
      and gtt.effective_from_date <= L_vdate
   union all
   select distinct
          isc.item,
          gtt.supplier,
          gtt.origin_country_id,
          --
          first_value(ic.country_id)
             over(partition by gtt.item_grandparent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) delivery_country_id,
          first_value(r.tax_code)
             over(partition by gtt.item_grandparent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) tax_code,
          first_value(r.tax_amount)
             over(partition by gtt.item_grandparent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) tax_amount,
          first_value(r.tax_basis_amount)
             over(partition by gtt.item_grandparent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) tax_basis_amount,
          first_value(r.tax_rate)
             over(partition by gtt.item_grandparent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) tax_rate,
          first_value(r.calculation_basis)
             over(partition by gtt.item_grandparent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) calculation_basis,
          first_value(r.recoverable_amount)
             over(partition by gtt.item_grandparent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) recoverable_amount,
         first_value(r.modified_tax_basis_amount)
             over(partition by gtt.item_grandparent,isc.supplier,isc.origin_country_id, r.tax_code
                  order by gtt.item,gtt.effective_from_date desc) modified_tax_basis_amount
     from l10n_br_extax_help_cost_gtt gtt,
          item_supp_country isc,
          item_country ic,
          country_attrib ca,
          item_tax_code_res r
    where gtt.item_grandparent  = isc.item
      and gtt.item_grandparent       = decode(r.item,'-1',gtt.item_grandparent,r.item)
      and gtt.supplier          = isc.supplier
      and gtt.origin_country_id = isc.origin_country_id
      --
      and gtt.item              = ic.item
      and isc.origin_country_id = ic.country_id
      --
      and ca.country_id         = ic.country_id
      --
      and gtt.loc               = ca.default_loc
      --
      and gtt.sup_city          = r.sup_city
      and gtt.sup_cnae_code     = r.sup_cnae_code
      and gtt.sup_iss           = r.sup_iss
      and gtt.sup_simples       = r.sup_simples
      and gtt.sup_ipi           = r.sup_ipi
      and gtt.sup_icms          = r.sup_icms
      and gtt.sup_st            = r.sup_st
      and gtt.sup_cnpj          = r.sup_cnpj
      and gtt.sup_is_income_range_eligible  = r.sup_is_income_range_eligible
      and gtt.sup_is_distr_a_manufacturer   = r.sup_is_distr_a_manufacturer
      and gtt.sup_icms_simples_rate         = r.sup_icms_simples_rate
      and gtt.sup_rural_prod_ind            = r.sup_rural_prod_ind
      and gtt.sup_tax_regime_concat         = r.sup_tax_regime_concat
      --
      and gtt.loc_city          = r.loc_city
      and gtt.loc_cnae_code     = r.loc_cnae_code
      and gtt.loc_iss           = r.loc_iss
      and gtt.loc_ipi           = r.loc_ipi
      and gtt.loc_icms          = r.loc_icms
      and gtt.loc_cnpj          = r.loc_cnpj
      --
      and nvl(gtt.origin_code,'0')       = r.origin_code
      and gtt.classification_id = r.classification_id
      and gtt.ncm_char_code     = r.ncm_char_code
      and gtt.ex_ipi            = r.ex_ipi
      and gtt.pauta_code        = r.pauta_code
      and gtt.service_code      = r.service_code
      and gtt.federal_service   = r.federal_service
      and gtt.dim_object        = r.dim_object
      and gtt.length            = r.length
      and gtt.lwh_uom           = r.lwh_uom
      and gtt.weight            = r.weight
      and gtt.net_weight        = r.net_weight
      and gtt.weight_uom        = r.weight_uom
      and gtt.liquid_volume     = r.liquid_volume
      and gtt.liquid_volume_uom = r.liquid_volume_uom
      and gtt.item_xform_ind    = r.item_xform_ind
      and gtt.state_of_manufacture = r.state_of_manufacture
      and gtt.pharma_list_type  = r.pharma_list_type
      --
      and gtt.unit_cost         = decode(r.item,'-1',r.unit_cost,gtt.unit_cost)
      --
      and gtt.effective_from_date <= L_vdate;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INS_ITEM_COST_DETAIL;
-----------------------------------------------------------------------------
FUNCTION UPD_FUTURE_COST_CURRENT(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.UPD_FUTURE_COST_CURRENT';
   L_vdate       DATE := get_vdate();

BEGIN

   merge into future_cost target using (
      select distinct
             gtt.item,
             gtt.supplier,
             gtt.origin_country_id,
             gtt.loc location,
             gtt.effective_from_date active_date,
             --
             gtt.total_tax_amount,
             gtt.total_recoverable_amount,
             gtt.total_tax_amount_nic_incl,
             ca.default_po_cost
        from l10n_br_extax_help_cost_gtt gtt,
             item_country ic,
             country_attrib ca
       where gtt.item      = ic.item
         and ic.country_id = ca.country_id) use_this
   on (    target.item              = use_this.item
       and target.supplier          = use_this.supplier
       and target.origin_country_id = use_this.origin_country_id
       and target.location          = use_this.location
       and trunc(target.active_date)       = trunc(use_this.active_date))
   when matched then
   update set target.negotiated_item_cost = target.base_cost + nvl(use_this.total_tax_amount_nic_incl,0),
              target.extended_base_cost   = target.base_cost +
                                               nvl(use_this.total_tax_amount,0) - nvl(use_this.total_recoverable_amount,0),
              target.wac_tax              = target.base_cost +
                                               nvl(use_this.total_tax_amount,0) - nvl(use_this.total_recoverable_amount,0) -
                                               nvl(target.base_cost,0),
              target.base_cost            = decode(use_this.default_po_cost,
                                                   'NIC', target.base_cost + nvl(use_this.total_tax_amount_nic_incl,0),
                                                   'BC',  target.base_cost),
              target.pricing_cost         =  target.pricing_cost +
                                             nvl(use_this.total_tax_amount,0) - nvl(use_this.total_recoverable_amount,0),
              target.net_cost             = decode(use_this.default_po_cost,
                                                   'NIC', target.net_cost + nvl(use_this.total_tax_amount_nic_incl,0),
                                                   'BC',  target.net_cost),
              target.net_net_cost         = decode(use_this.default_po_cost,
                                                   'NIC', target.net_net_cost + nvl(use_this.total_tax_amount_nic_incl,0),
                                                   'BC',  target.net_net_cost),
              target.dead_net_net_cost    = decode(use_this.default_po_cost,
                                                   'NIC', target.dead_net_net_cost + nvl(use_this.total_tax_amount_nic_incl,0),
                                                   'BC',  target.dead_net_net_cost),
              target.default_costing_type = use_this.default_po_cost;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_FUTURE_COST_CURRENT;
-----------------------------------------------------------------------------
FUNCTION FUTURE_TAX_LAW_CHANGES_COST(O_error_message    IN OUT VARCHAR2,
                                     I_process_size     IN     NUMBER,
                                     I_run_type         IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program         VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.FUTURE_TAX_LAW_CHANGES_COST';
   L_count_code      VARCHAR2(256) := NULL;
   L_query_code      VARCHAR2(256) := NULL;

   L_refresh_date    DATE          := NULL;

   L_count           NUMBER        := NULL;
   L_size            NUMBER        := TAX_PROCESSING_SIZE;
   L_start           NUMBER        := null;
   L_end             NUMBER        := null;

   cursor C_GET_REFRESH_DATE is
      select nvl(max(last_effective_after_this_date),sysdate)
        from l10n_br_extax_refresh_config
       where refresh_needed = 'Y';

   L_tax_service_id  l10n_br_tax_call_stage_fsc_fdn.tax_service_id%TYPE := null;

   cursor C_GET_COUNT is
      select total_fnd_count
        from l10n_br_tax_call_res_fsc_count
       where tax_service_id = L_tax_service_id;

BEGIN

   if EXTAX_LOC_GROUP_SETUP(O_error_message) = FALSE then
      return FALSE;
   end if;

   if I_run_type = L10N_BR_EXTAX_MAINT_SQL.SEED_RUN_TYPE then

      L_count_code      := 'FUTURECHANGECOUNT';
      L_query_code      := 'FUTURECHANGE';
      L_refresh_date    := sysdate;

   elsif I_run_type = L10N_BR_EXTAX_MAINT_SQL.REFRESH_RUN_TYPE then

      L_count_code      := 'REFRESHRULECOUNT';
      L_query_code      := 'REFRESHRULE';

      open C_GET_REFRESH_DATE;
      fetch C_GET_REFRESH_DATE into L_refresh_date;
      close C_GET_REFRESH_DATE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT');
      return FALSE;
   end if;

   delete from l10n_br_extax_refresh_cost;

   --

   if L10N_BR_FISCAL_FDN_QUERY_SQL.EXTAX_MAINT_QUERY_FISCAL_CODE(O_error_message,
                                                                 L_tax_service_id,
                                                                 L_count_code,
                                                                 L_refresh_date,
                                                                 null,
                                                                 null) = FALSE then
      return FALSE;
   end if;

   open C_GET_COUNT;
   fetch C_GET_COUNT into L_count;
   close C_GET_COUNT;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   ---------------------------------------------------------------------

   while L_count > 0 loop

      L_end := L_count;
      L_start := L_count - L_size;
      if L_start < 1 then
         L_start := 1;
      end if;
      L_count := L_count - L_size - 1;

      if L10N_BR_FISCAL_FDN_QUERY_SQL.EXTAX_MAINT_QUERY_FISCAL_CODE(O_error_message,
                                                                    L_tax_service_id,
                                                                    L_query_code,
                                                                    L_refresh_date,
                                                                    L_start,
                                                                    L_end) = FALSE then
         return FALSE;
      end if;

      insert into l10n_br_extax_refresh_gtt (create_date,
                                             effective_date,
                                             classification_code,
                                             ncm_char_code,
                                             pauta_code,
                                             source_state,
                                             destination_state,
                                             reason)
         select creation_date,
                effective_date,
                nvl(fiscal_extended_parent_code,'-1'),
                nvl(fiscal_parent_code,'-1'),
                nvl(fiscal_code,'-1'),
                origin_state,
                destination_state,
                fiscal_code_description
           from l10n_br_tax_call_res_fsc_fnd
          where tax_service_id = L_tax_service_id;

      if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                   L_tax_service_id) = FALSE then
         return FALSE;
      end if;

   end loop;

   if  EXPLODE_TAX_ATTRIBS_COST(O_error_message,
                               I_process_size) = FALSE then
      return FALSE;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END FUTURE_TAX_LAW_CHANGES_COST;
-----------------------------------------------------------------------------
FUNCTION EXPLODE_TAX_ATTRIBS_COST(O_error_message    IN OUT VARCHAR2,
                                  I_process_size     IN     NUMBER)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.EXPLODE_TAX_ATTRIBS_COST';

BEGIN

   insert into l10n_br_extax_refresh_gtt (create_date,
            effective_date,
            classification_code,
            ncm_char_code,
            pauta_code,
            source_state,
            destination_state,
            reason)
   select r.create_date,
          r.effective_date,
          r.classification_code,
          r.ncm_char_code,
          r.pauta_code,
          r.source_state,
          s.state,
          r.reason
     from l10n_br_extax_refresh_gtt r,
          (select distinct a.state
                from addr a,
                     (select to_char(store) loc, 'ST' module_type from v_br_store
                      union all
                      select partner_id loc, 'PTNR' module_type from v_br_partner where partner_type = 'P'
                      union all
                      select to_char(wh) loc, 'WH' module_type from v_br_wh) loc
               where to_char(loc.loc)    = a.key_value_1
                 and a.addr_type         = '01'
                 and a.primary_addr_ind  = 'Y'
                 and a.country_id        = 'BR'
                 and a.module            = loc.module_type) s
    where r.destination_state = '*';



   delete from l10n_br_extax_refresh_gtt where destination_state = '*';

   insert into l10n_br_extax_refresh_gtt (create_date,
                                    effective_date,
                                     classification_code,
                                     ncm_char_code,
                                     pauta_code,
                                     source_state,
                                     destination_state,
                                     reason)
   select r.create_date,
          r.effective_date,
          r.classification_code,
          r.ncm_char_code,
          r.pauta_code,
          s.state,
          r.destination_state,
          r.reason
     from l10n_br_extax_refresh_gtt r,
          (select distinct a.state
             from addr a,
                  (select to_char(supplier) loc, 'SUPP' module_type from v_br_sups) loc
            where to_char(loc.loc)    = a.key_value_1
              and a.addr_type         = '01'
              and a.primary_addr_ind  = 'Y'
              and a.country_id        = 'BR'
              and a.module            = loc.module_type) s
    where r.source_state = '*';

   delete from l10n_br_extax_refresh_gtt where source_state = '*';

   ----------------------------------------------------------------

      -- a b c -> a b c
      insert into l10n_br_extax_refresh_cost (
         create_date, effective_date, classification_code, ncm_char_code, pauta_code,
         source_state, destination_state, reason)
       select distinct
         null, gtt.effective_date, gtt.classification_code, gtt.ncm_char_code,
         gtt.pauta_code, gtt.source_state, gtt.destination_state, null
         from l10n_br_extax_refresh_gtt gtt,
         (select distinct classification_id, ncm_char_code, pauta_code, origin_code
            from v_br_item_fiscal_attrib
           where origin_code = '0'
             and rownum      > 0) vbi
        where gtt.classification_code != '*'
          and gtt.ncm_char_code       != '*'
          and gtt.pauta_code          != '*'
          and gtt.classification_code  = vbi.classification_id
          and gtt.ncm_char_code        = vbi.ncm_char_code
          and gtt.pauta_code           = vbi.pauta_code;

      -- a b * -> a b * / a b c
      insert into l10n_br_extax_refresh_cost (
         create_date, effective_date, classification_code, ncm_char_code, pauta_code,
         source_state, destination_state, reason)
       select distinct
         null, gtt.effective_date, gtt.classification_code, gtt.ncm_char_code,
         vbi.pauta_code, gtt.source_state, gtt.destination_state, null
         from l10n_br_extax_refresh_gtt gtt,
         (select distinct classification_id, ncm_char_code, pauta_code, origin_code
            from v_br_item_fiscal_attrib
           where origin_code = '0'
             and rownum      > 0) vbi
        where gtt.classification_code != '*'
          and gtt.ncm_char_code       != '*'
          and gtt.pauta_code           = '*'
          and gtt.classification_code  = vbi.classification_id
          and gtt.ncm_char_code        = vbi.ncm_char_code;

      -- a * * -> a * * / a b * / a b c
      insert into l10n_br_extax_refresh_cost (
         create_date, effective_date, classification_code, ncm_char_code, pauta_code,
         source_state, destination_state, reason)
       select distinct
         null, gtt.effective_date, gtt.classification_code, vbi.ncm_char_code,
         vbi.pauta_code, gtt.source_state, gtt.destination_state, null
         from l10n_br_extax_refresh_gtt gtt,
         (select distinct classification_id, ncm_char_code, pauta_code, origin_code
            from v_br_item_fiscal_attrib
           where origin_code = '0'
             and rownum      > 0) vbi
        where gtt.classification_code != '*'
          and gtt.ncm_char_code        = '*'
          and gtt.pauta_code           = '*'
          and gtt.classification_code  = vbi.classification_id;

      -- * * * -> a * * / a b * / a b c
      insert into l10n_br_extax_refresh_cost (
         create_date, effective_date, classification_code, ncm_char_code, pauta_code,
         source_state, destination_state, reason)
       select distinct
         null, gtt.effective_date, vbi.classification_id, vbi.ncm_char_code,
         vbi.pauta_code, gtt.source_state, gtt.destination_state, null
         from (select distinct create_date, effective_date, source_state, destination_state
            from l10n_br_extax_refresh_gtt
           where classification_code  = '*'
             and ncm_char_code        = '*'
             and pauta_code           = '*'
             and rownum               > 0) gtt,
         (select distinct classification_id, ncm_char_code, pauta_code, origin_code
            from v_br_item_fiscal_attrib
           where origin_code = '0'
             and rownum      > 0) vbi;

   delete from l10n_br_extax_refresh_cost r
    where r.classification_code IS NOT NULL
      and r.ncm_char_code IS NOT NULL
      and r.pauta_code IS NOT NULL
      and (r.classification_code,r.ncm_char_code,r.pauta_code) not in
                  (select distinct vbi.classification_id,vbi.ncm_char_code,vbi.pauta_code 
                     from v_br_item_fiscal_attrib vbi 
                    where origin_code = '0' 
                      and vbi.classification_id IS NOT NULL
                      and vbi.ncm_char_code IS NOT NULL 
                      and vbi.pauta_code IS NOT NULL);

   delete from l10n_br_extax_refresh_cost r
    where r.classification_code IS NOT NULL
      and r.ncm_char_code IS NOT NULL
      and r.pauta_code = '-1'
      and (r.classification_code,r.ncm_char_code) not in
                  (select distinct vbi.classification_id,vbi.ncm_char_code                
                     from v_br_item_fiscal_attrib vbi 
                    where origin_code = '0' 
                      and vbi.classification_id IS NOT NULL
                      and vbi.ncm_char_code IS NOT NULL 
                      and vbi.pauta_code IS NULL);

   delete from l10n_br_extax_refresh_cost r
    where r.classification_code IS NOT NULL
      and r.ncm_char_code = '-1'
      and r.pauta_code = '-1'
      and (r.classification_code) not in
                  (select distinct vbi.classification_id
                     from v_br_item_fiscal_attrib vbi
                    where origin_code = '0' 
                      and vbi.classification_id IS NOT NULL
                      and vbi.ncm_char_code IS NULL 
                      and vbi.pauta_code IS NULL);

   insert into gtax_cost_change_gtt (effective_date,
                                     supplier,
                                     origin_country_id,
                                     country_id,
                                     item,
                                     loc_type,
                                     loc,
                                     unit_cost,
                                     header_id,
                                     detail_count,
                                     cost_change_id)
      select /*+ use_hash(iscl) parallel(iscl,8) */
             inner.effective_date,
             iscl.supplier,
             iscl.origin_country_id,
             inner.country_id,
             iscl.item,
             inner.loc_type,
             iscl.loc,
             iscl.unit_cost,
             dense_rank() over(order by inner.effective_date,
                                        iscl.supplier,
                                        iscl.origin_country_id,
                                        inner.country_id) rank_value,
             row_number() over(partition by inner.effective_date,
                                            iscl.supplier,
                                            iscl.origin_country_id,
                                            inner.country_id
                               order by iscl.loc,
                                        iscl.item,
                                         iscl.unit_cost) cnt,
             null
        FROM (SELECT /*+ ORDERED */
                     r.effective_date,
                     r.source_state,
                     loc_help.country_id,
                     vbi.item,
                     sup_help.entity supp,
                     loc_help.loc_type,
                     to_number(loc_help.entity) loc
                from l10n_br_extax_refresh_cost r,
                     v_br_item_fiscal_attrib vbi,
                     item_master im,
                     item_supplier iss,
                     (select * from  l10n_br_extax_dest_group_help where loc_type = 'SUPP') sup_help,
                     (select * from  l10n_br_extax_dest_group_help where loc_type != 'SUPP') loc_help
               where r.classification_code = vbi.classification_id
                 and r.ncm_char_code       = vbi.ncm_char_code
                 and r.pauta_code          = vbi.pauta_code
                 and vbi.origin_code       = '0'
                 and vbi.item              = im.item
                 and im.status             = 'A'
                 and im.tran_level         =  im.item_level
                 and im.pack_ind           = 'N'
                 and im.item_xform_ind     = 'N'
                 and im.item               = iss.item
                 and iss.supplier          = sup_help.entity
                 and r.source_state        = sup_help.state
                 and r.destination_state   = loc_help.state
                 and exists (select 'x'
                               from item_loc il
                              where il.item = vbi.item
                                and il.loc  = loc_help.entity
                                and rownum  = 1)
                 and rownum > 0) inner,
             item_supp_country_loc iscl
       where inner.SUPP             = iscl.supplier
         and iscl.origin_country_id = 'BR'
         and inner.item             = iscl.item
         and inner.loc              = iscl.loc;

   if CREATE_COST_CHANGES(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END EXPLODE_TAX_ATTRIBS_COST;
-----------------------------------------------------------------------------
FUNCTION CREATE_COST_CHANGES(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.CREATE_COST_CHANGES';

   L_cost_change_tbl         OBJ_CC_COST_EVENT_TBL := OBJ_CC_COST_EVENT_TBL();
   L_cost_event_process_id   cost_event_cost_chg.cost_event_process_id%TYPE;

   cursor c_cost_events is
      select OBJ_CC_COST_EVENT_REC(cost_change_id, 'N')
        from gtax_cost_change_gtt
       where detail_count = 1;

BEGIN

   update gtax_cost_change_gtt gtt
      set gtt.cost_change_id = cc_sequence.nextval
    where gtt.detail_count = 1;

   merge into gtax_cost_change_gtt target using (
      select header_id,
             cost_change_id
        from gtax_cost_change_gtt
       where detail_count = 1) use_this
   on (target.header_id     = use_this.header_id and
       target.detail_count != 1)
   when matched then
   update set target.cost_change_id = use_this.cost_change_id;

   insert into cost_susp_sup_head (
           cost_change,
           cost_change_desc,
           reason,
           active_date,
           status,
           cost_change_origin,
           create_date,
           create_id,
           approval_date,
           approval_id)
   select cost_change_id,
          'EXTAX_generated',
          9,                    --Cost Change Reason is 9 (Tax Law Changes)
          effective_date,
          'A',
          'SUP',
          SYSDATE,
          'GTAX',
          SYSDATE,
          'GTAX'
     from gtax_cost_change_gtt gtt
    where gtt.detail_count = 1;

   insert into cost_susp_sup_detail_loc (
           cost_change,
           supplier,
           origin_country_id,
           item,
           loc_type,
           loc,
           bracket_value1,
           bracket_uom1,
           bracket_value2,
           unit_cost,
           cost_change_type,
           cost_change_value,
           recalc_ord_ind,
           default_bracket_ind,
           dept,
           sup_dept_seq_no,
           delivery_country_id)
   select detail.cost_change_id,
          detail.supplier,
          detail.origin_country_id,
          detail.item,
          detail.loc_type,
          detail.loc,
          null,
          null,
          null,
          detail.unit_cost,
          'A',
          detail.unit_cost,
          'N',
          'N',
          null,
          null,
          detail.country_id
     from gtax_cost_change_gtt detail;

   ----------------------------------------

   open c_cost_events;
   fetch c_cost_events bulk collect into L_cost_change_tbl;
   close c_cost_events;

   if L_cost_change_tbl is not null and L_cost_change_tbl.count > 0 then
      if FUTURE_COST_EVENT_SQL.ADD_COST_CHANGE_EVENT(O_error_message,
                                                     L_cost_event_process_id,
                                                     FUTURE_COST_EVENT_SQL.ADD_EVENT,
                                                     L_cost_change_tbl,
                                                     'Y',
                                                     'GTAX') = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END CREATE_COST_CHANGES;
-----------------------------------------------------------------------------
FUNCTION SETUP_TAX_CALL_ROUTING_COST(O_error_message    IN OUT VARCHAR2)

RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SETUP_TAX_CALL_ROUTING_COST';


BEGIN

   --setup routing
   insert into l10n_br_tax_call_stage_routing (tax_service_id,
                                               client_id,
                                               update_history_ind)
   select distinct process_id,
                   L10N_BR_TAX_SERVICE_MNGR_SQL.SEED_COST,
                   'N'
     from l10n_br_extax_stg_cost;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END SETUP_TAX_CALL_ROUTING_COST;
-----------------------------------------------------------------------------
FUNCTION PROCESS_COST(O_error_message  IN OUT VARCHAR2,
                      I_process_id     IN     NUMBER)
RETURN BOOLEAN IS

   L_program               VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.PROCESS_COST';
   L_status                VARCHAR2(10);

BEGIN

   if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                       L_status,
                                                       I_process_id,
                                                       'CALC_TAX') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END PROCESS_COST;
-----------------------------------------------------------------------------
FUNCTION GET_REQUEST_DATA_COST(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id         IN     NUMBER,
                               O_tax_data              OUT "RIB_FiscDocColRBM_REC")
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'L10N_BR_EXTAX_MAINT_SQL.GET_REQUEST_DATA_COST';

   L_incl_nic_ind_pis      VARCHAR2(1);
   L_incl_nic_ind_cofins   VARCHAR2(1);
   L_incl_nic_ind_icms     VARCHAR2(1);

   L_effective_date        DATE;

   L_nop                   FM_FISCAL_UTILIZATION.NOP%TYPE;

   L_loc_ref_id            ITEM_LOC.LOC%TYPE;
   L_sup_ref_id            SUPS.SUPPLIER%TYPE;
   L_item_cost_tax_incl_ind COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE;

   L_loc_tbl               "RIB_FiscEntityRBO_TBL" := "RIB_FiscEntityRBO_TBL"();
   L_sup_tbl               "RIB_FiscEntityRBO_TBL" := "RIB_FiscEntityRBO_TBL"();

   L_src_rec               "RIB_FromEntity_REC"   := "RIB_FromEntity_REC"(0,null);
   L_dest_rec              "RIB_ToEntity_REC"     := "RIB_ToEntity_REC"(0,null);
   L_src_tbl               "RIB_FromEntity_TBL"   := "RIB_FromEntity_TBL"();
   L_dest_tbl              "RIB_ToEntity_TBL"     := "RIB_ToEntity_TBL"();
   ---
   L_doc_line_item_tbl     "RIB_LineItemRBO_TBL"  := "RIB_LineItemRBO_TBL"();
   ---
   L_fiscal_doc_rec        "RIB_FiscDocRBO_REC";
   L_fiscal_doc_tbl        "RIB_FiscDocRBO_TBL"   := "RIB_FiscDocRBO_TBL"();
   ---
   L_fisc_doc_chnk_rec     "RIB_FiscDocChnkRBO_REC" := "RIB_FiscDocChnkRBO_REC"(0,null,null);
   L_fisc_doc_chnk_tbl     "RIB_FiscDocChnkRBO_TBL" := "RIB_FiscDocChnkRBO_TBL"();
   ---
   L_tbl_obj_eco_class_cd  "RIB_EcoClassCd_TBL"   := "RIB_EcoClassCd_TBL"();
   L_cnae_concat           varchar2(999);

   L_tax_regime_tbl        "RIB_DiffTaxRegime_TBL":= "RIB_DiffTaxRegime_TBL"();
   L_tax_regime_concat     varchar2(999);

   cursor C_GET_TAX_IND (L_TEM_COST_TAX_INCL_IND IN VARCHAR) is
      select decode(NVL(l_tem_cost_tax_incl_ind,'N'),'Y',decode(v1.vat_code, 'PIS',v1.incl_nic_ind,NULL),'N') pis,
             decode(NVL(l_tem_cost_tax_incl_ind,'N'),'Y',decode(v2.vat_code, 'COFINS', v2.incl_nic_ind, NULL),'N') cofins,
             decode(NVL(l_tem_cost_tax_incl_ind,'N'),'Y',decode(v3.vat_code, 'ICMS',   v3.incl_nic_ind, NULL),'N') icms
        from vat_codes v1,
             vat_codes v2,
             vat_codes v3
       where v1.vat_code in ('PIS')
         and v2.vat_code in ('COFINS')
         and v3.vat_code in ('ICMS');

   cursor C_DATE is
      select distinct trunc(effective_date)
        from l10n_br_extax_stg_cost
       where process_id = I_process_id;


  cursor C_TEM_COST_TAX_INCL_IND is
      select item_cost_tax_incl_ind
        from country_attrib
       where country_id='BR';

   cursor C_NOP is
      select fmso.string_value
        from fm_system_options fmso
       where fmso.variable = 'DEFAULT_NOP';

   cursor C_MULTI_CNAE is
      SELECT "RIB_EcoClassCd_REC"(0,
             TRIM( SUBSTR ( txt
                          , INSTR (txt, '~', 1, level ) + 1
                          , INSTR (txt, '~', 1, level+1) - INSTR (txt, '~', 1, level) -1
                          )))
        FROM (SELECT '~'||L_cnae_concat||'~' AS txt from dual)
      CONNECT BY level <= LENGTH(txt)-LENGTH(REPLACE(txt,'~',''))-1;

   cursor C_MULTI_TAX_REGIME is
      SELECT "RIB_DiffTaxRegime_REC"(0,
             TRIM( SUBSTR ( txt
                          , INSTR (txt, '~', 1, level ) + 1
                          , INSTR (txt, '~', 1, level+1) - INSTR (txt, '~', 1, level) -1
                          )))
        FROM (SELECT '~'||L_tax_regime_concat||'~' AS txt from dual)
      CONNECT BY level <= LENGTH(txt)-LENGTH(REPLACE(txt,'~',''))-1;

   L_nv_entity_group_concat  varchar2(500);
   L_nv_table                "RIB_NameValPairRBO_TBL" := "RIB_NameValPairRBO_TBL"();

   cursor C_NAME_VALUE_PAIR is
      select "RIB_NameValPairRBO_REC"(0,name, value) from
         (select distinct name_value_pair_id, name, value
            from l10n_br_extax_help_nv_pair h,
               (SELECT TRIM( SUBSTR ( txt
                                    , INSTR (txt, '~', 1, level ) + 1
                                    , INSTR (txt, '~', 1, level+1) - INSTR (txt, '~', 1, level) -1
                                    )) grp_id
                  FROM (SELECT '~'||L_nv_entity_group_concat||'~' AS txt from dual)
                CONNECT BY level <= LENGTH(txt)-LENGTH(REPLACE(txt,'~',''))-1) g
           where h.NAME_VALUE_PAIR_ID = to_number(g.grp_id) order by name_value_pair_id);

   --supplier
   cursor C_GET_SOURCE_OBJ is
      select sup_ref_id,
             cnae_code,
             tax_regime_concat,
             name_value_pair_id_concat,
             "RIB_FiscEntityRBO_REC"(0,
                "RIB_AddrRBO_TBL"(
                "RIB_AddrRBO_REC"
                   (0,             -- rob_oid
                    city,          -- city_id
                    null,          -- state_name
                    country_desc,  -- country_name
                    '0',           -- addr
                    '01',          -- addr_type
                    '1',           -- primary_addr_type_ind
                    'Y',           -- primary_addr_ind
                    'dummy addr',  -- add_1
                    NULL,          -- add_2
                    NULL,          -- add_3
                    city_desc,     -- city
                    state,         -- state
                    '105'         -- country_id
                                  -- extOfAddrDesc
                                   -- LOCOFADDRDESC
                    )), -- addrDesc_
                NULL, -- eco_class_cd_
                NULL, -- DIFF_TAX_REGIME_NAME_TBL
                sup_ref_id,  -- entity_code_
                'SP',  -- entity_type_
                NULL,  -- legal_name_
                'S',   -- fiscal_type_
                simples,  -- is_simples_contributor_
                cnpj,  -- federal_tax_reg_id_
                sup_rural_prod_ind,  -- is_rural_producer_      -- is this the same for both source and destination?
                (case when is_income_range_eligible = 'N' then null else is_income_range_eligible end), -- IS_INCOME_RANGE_ELIGIBLE
                (case when is_distr_a_manufacturer = 'N' then null else is_distr_a_manufacturer end), -- IS_DISTR_A_MANUFACTURER
                (case when icms_simples_rate = '-1' then null else icms_simples_rate end), -- ICMS_SIMPLES_RATE
                CAST(MULTISET(select "RIB_TaxContributor_REC"(0,'ISS')
                                from dual
                               where m1.ISS = 'Y'
                               union all
                               select "RIB_TaxContributor_REC"(0,'IPI')
                                 from dual
                                where m1.IPI = 'Y'
                                union all
                               select "RIB_TaxContributor_REC"(0,'ST')
                                 from dual
                                where m1.ST = 'Y'
                                union all
                               select "RIB_TaxContributor_REC"(0,'ICMS')
                                 from dual
                                where m1.ICMS = 'Y') AS "RIB_TaxContributor_TBL"),  -- taxContributor_
                NULL   -- NAMEVALPAIRRBO_TBL
                       -- extOfFiscalEntityRBO_
                    ) fiscal_ent_rec  --LOCOFFISCALENTITYRBO_
        from (select distinct
                     sup_ref_id,
                     sup_city          city,
                     sup_city_desc     city_desc,
                     sup_state         state,
                     sup_state_desc    state_desc,
                     sup_country_id    country_id,
                     sup_country_desc  country_desc,
                     sup_cnpj          cnpj,
                     sup_cnae_code     cnae_code,
                     sup_iss           iss,
                     sup_simples       simples,
                     sup_ipi           ipi,
                     sup_icms          icms,
                     sup_st            st,
                     sup_is_income_range_eligible  is_income_range_eligible,
                     sup_is_distr_a_manufacturer   is_distr_a_manufacturer,
                     sup_icms_simples_rate         icms_simples_rate,
                     sup_tax_regime_concat         tax_regime_concat,
                     sup_rural_prod_ind            sup_rural_prod_ind,
                     sup_name_value_pair_id_concat name_value_pair_id_concat
                from l10n_br_extax_stg_cost
               where process_id = I_process_id) m1;

   --location
   cursor C_GET_DEST_OBJ is
      select loc_ref_id,
             cnae_code,
             name_value_pair_id_concat,
             "RIB_FiscEntityRBO_REC"(0,
                "RIB_AddrRBO_TBL"(
                "RIB_AddrRBO_REC"
                   (0,             -- rob_oid
                    city,          -- city_id
                    null,          -- state_name
                    country_desc,  -- country_name
                    '0',           -- addr
                    '01',          -- addr_type
                    '1',           -- primary_addr_type_ind
                    'Y',           -- primary_addr_ind
                    'dummy addr',  -- add_1
                    NULL,          -- add_2
                    NULL,          -- add_3
                    city_desc,     -- city
                    state,         -- state
                    '105'         -- country_id
                                   -- extOfAddrDesc
                                   -- LOCOFADDRDESC
                    )), -- addrDesc_
                NULL, -- eco_class_cd_
                NULL, -- DIFF_TAX_REGIME_NAME_TBL
                loc_ref_id,  -- entity_code_
                L10N_BR_EXTAX_MAINT_SQL.ST_ENTITY,  -- entity_type_
                NULL,  -- legal_name_
                'S',   -- fiscal_type_
                null,  -- is_simples_contributor_
                cnpj,  -- federal_tax_reg_id_
                'N',  -- is_rural_producer_
                NULL, -- IS_INCOME_RANGE_ELIGIBLE
                NULL, -- IS_DISTR_A_MANUFACTURER
                NULL, -- ICMS_SIMPLES_RATE
                CAST(MULTISET(select "RIB_TaxContributor_REC"(0,'ISS')
                                from dual
                               where m1.ISS = 'Y'
                               union all
                               select "RIB_TaxContributor_REC"(0,'IPI')
                                 from dual
                                where m1.IPI = 'Y'
                                union all
                               select "RIB_TaxContributor_REC"(0,'ICMS')
                                 from dual
                                where m1.ICMS = 'Y') AS "RIB_TaxContributor_TBL"),  -- taxContributor_
                NULL
                      -- NAMEVALPAIRRBO_TBL
                      -- extOfFiscalEntityRBO_
                    ) fiscal_ent_rec  --LOCOFFISCALENTITYRBO_
        from (select distinct
                     loc_ref_id,
                     loc_city          city,
                     loc_city_desc     city_desc,
                     loc_state         state,
                     loc_state_desc    state_desc,
                     loc_country_id    country_id,
                     loc_country_desc  country_desc,
                     loc_cnpj          cnpj,
                     loc_cnae_code     cnae_code,
                     loc_iss           iss,
                     loc_ipi           ipi,
                     loc_icms          icms,
                     loc_name_value_pair_id_concat  name_value_pair_id_concat
                from l10n_br_extax_stg_cost
               where process_id = I_process_id
                 and sup_ref_id = L_sup_ref_id) m1;

   cursor C_GET_ITEM_OBJ is
      select "RIB_LineItemRBO_REC"(0,
                item_ref_id, -- document_line_id_
                item, -- item_id_
                '5401', -- item_tran_Code_
                decode(service_ind, 'N', 'M', 'S'),  -- item_type_
                1,    -- quantity_
                'EA', -- unit_of_measure_
                1, --quantity_in_eaches_
                NULL, -- origin_doc_date_
                (case when pack_no = '-1' then null else pack_no end), -- pack_item_id_
                unit_cost, -- total_cost_
                unit_cost, -- unit_cost_
                NULL, -- source_taxpayer_type_
                NULL, -- orig_fiscal_doc_number_
                NULL, -- orig_fiscal_doc_series_
                (case when dim_object = '-999' then null else dim_object end),    -- dim_object_
                (case when length = '-999' then null else length end),            -- length_
                NULL,                                                             -- width_
                (case when lwh_uom = '-999' then null else lwh_uom end),          -- lwh_uom_
                (case when weight = '-999' then null else weight end),            -- weight_
                (case when net_weight = '-999' then null else net_weight end),    -- net_weight_
                (case when weight_uom = '-999' then null else weight_uom end),    -- weight_uom_
                (case when liquid_volume = '-999' then null else liquid_volume end),         -- liquid_volume_
                (case when liquid_volume_uom = '-999' then null else liquid_volume_uom end), -- liquid_volume_uom_
                NULL, -- FREIGHT
                NULL, -- INSURANCE
                NULL, -- DISCOUNT
                NULL, -- COMMISION
                NULL, -- FREIGHT_TYPE
                NULL, -- OTHER_EXPENCES
                NULL, -- origin_fiscal_code_opr_
                NULL, -- deduced_fiscal_code_opr_
                'Y',  -- deduce_cfop_code_
                NULL, -- icms_cst_code_
                NULL, -- pis_cst_code_
                NULL, -- cofins_cst_code_
                NULL, -- deduce_icms_cst_code_
                NULL, -- deduce_pis_cst_code_
                NULL, -- deduce_cofins_cst_code_
                NULL, -- recoverable_icmsst_
                L_incl_nic_ind_cofins,   -- item_cost_contains_cofins_
                NULL,                    -- recoverable_base_icmsst_
                L_incl_nic_ind_pis,      -- item_cost_contains_pis_
                L_incl_nic_ind_icms,     -- item_cost_contains_icms_
                decode( service_ind,'N',
                      "RIB_PrdItemRBO_TBL"(
                      "RIB_PrdItemRBO_REC"(0,
                                           item_ref_id,   -- product.item_code_
                                           'dummy item desc', -- product.item_description_
                                           '0',               -- product.item_origin_
                                           'C',               -- product.item_utilization_
                                           item_xform_ind,              -- product.is_transformed_item_
                                           (case when classification_id = '-1'
                                                 then null else classification_id end),   -- fiscal_classification_code_
                                           -- send either ncm_char_code.pauta_code, or ncm_char_code or null
                                           (case when ncm_char_code != '-1' and pauta_code != '-1' 
                                                 then ncm_char_code||'.'||pauta_code
                                                 when ncm_char_code != '-1' and pauta_code = '-1'
                                                 then ncm_char_code
                                                 when ncm_char_code = '-1' and pauta_code != '-1' -- should not be possible
                                                 then pauta_code
                                                 else null
                                             end ),
                                                      --EXTOFPRDITEMRBO_TBL
                                                      --EXTOFPRDITEMRBO_TBL
                                           (CASE when ex_ipi = '-1' then null else ex_ipi end), -- IPI_EXCEPTION_CODE_
                                           NULL, -- PRODUCT_TYPE_
                                           (case when state_of_manufacture = '-1' 
                                                then null else state_of_manufacture end), -- STATE_OF_MANUFACTURE
                                           (case when pharma_list_type = '-1' 
                                                then null else pharma_list_type end), -- PHARMA_LIST_TYPE
                                           NULL  -- NAMEVALPAIRRBO_TBL
                                          )),
                        null),  -- PRDITEMRBO_TBL
                decode( service_ind,'Y',
                        "RIB_SvcItemRBO_TBL"(
                        "RIB_SvcItemRBO_REC"(0,
                                             item_ref_id, --ITEM_CODE_
                                             'dummy item desc', --ITEM_DESCRIPTION_
                                             (case when ncm_char_code != '-1' and pauta_code != '-1'
                                                 then ncm_char_code||'.'||pauta_code
                                                 when ncm_char_code != '-1' and pauta_code = '-1'
                                                 then ncm_char_code
                                                 when ncm_char_code = '-1' and pauta_code != '-1' -- should not be possible
                                                 then pauta_code
                                                 else null
                                               end ),
                                             federal_service, -- FEDERAL_SERVICE_CODE_
                                             service_code, --DESTINATION_SERVICE_CODE_
                                             loc_city, --SERVICE_PROVIDER_CITY_
                                             NULL --NAMEVALPAIRRBO
                                                   --EXTOFSERVICEITEMRBO_
                                                   --LOCOFSERVICEITEMRBO_
                                             )),
                        NULL), -- SVCITEMRBO_TBL
                NULL, -- INFORMTAXRBO_TBL
                nv_tbl -- NAMEVALPAIRRBO_TBL
                       -- extOfDocLineItemRBO_
                       -- LOCOFDOCLINEITEMRBO_
             ) doc_line_item_rec
        from (select service_ind,
                     classification_id,
                     ncm_char_code,
                     ex_ipi,
                     pauta_code,
                     service_code,
                     federal_service,
                     origin_code,
                     dim_object,
                     length,
                     lwh_uom,
                     weight,
                     net_weight,
                     weight_uom,
                     liquid_volume,
                     liquid_volume_uom,
                     item_ref_id,
                     unit_cost,
                     loc_city,
                     pack_no,
                     item,
                     item_xform_ind,
                     state_of_manufacture,
                     pharma_list_type,
                     CAST(MULTISET(select "RIB_NameValPairRBO_REC"(0,h.name, h.value)
                       from (SELECT distinct item_name_value_pair_id_concat,
                                   TRIM( SUBSTR ( txt
                                                , INSTR (txt, '~', 1, level ) + 1
                                                , INSTR (txt, '~', 1, level+1) - INSTR (txt, '~', 1, level) -1
                                                )) grp_id
                              FROM (select item_name_value_pair_id_concat,
                                           '~'||(case when item_name_value_pair_id_concat = '-1' then null
                                                 else item_name_value_pair_id_concat end)||'~' AS txt
                                      from l10n_br_extax_stg_cost
                                     where process_id = I_process_id
                                       and sup_ref_id = L_sup_ref_id
                                       and loc_ref_id = L_loc_ref_id)
                            CONNECT BY level <= LENGTH(txt)-LENGTH(REPLACE(txt,'~',''))-1) inner,
                            (select distinct name_value_pair_id, name, value from l10n_br_extax_help_nv_pair) h
                      where h.NAME_VALUE_PAIR_ID = to_number(inner.grp_id)
                        and inner.item_name_value_pair_id_concat = br.item_name_value_pair_id_concat)
                      as "RIB_NameValPairRBO_TBL") nv_tbl
                from l10n_br_extax_stg_cost br
               where process_id = I_process_id
                 and sup_ref_id = L_sup_ref_id
                 and loc_ref_id = L_loc_ref_id) fr;

   cursor C_GET_DOC_DETAILS is
      select "RIB_FiscDocRBO_REC"(0,
                L_dest_tbl,          -- TOENTITY_TBL
                L_src_tbl,           -- FROMENTITY_TBL
                L_doc_line_item_tbl, -- LINEITEMRBO_TBL
                NULL,                -- NAMEVALPAIRRBO_TBL
                L_effective_date,    -- due_date_
                L_effective_date,    -- fiscal_document_date_
                'FT',       -- document_type_
                NULL,       -- gross_weight_
                NULL,       -- net_weight_
                NULL,       -- operation_type_
                NULL,       -- freight
                NULL,       -- insurance
                NULL,       -- discount
                NULL,       -- commision
                NULL,       -- freight_type
                NULL,       -- other_expenses
                NULL,       -- total_cost_
                NULL,       -- tax_amount
                NULL,       -- tax_basis_amount
                NULL,       -- tax_code
                L_effective_date,    -- receipt_date_
                'I',        -- transaction_type_
                NULL,       -- is_supplier_issuer_
                NULL,       -- no_history_tracked_
                NULL,       -- process_inconclusive_rule_
                NULL,       -- approximation_mode_
                NULL,       -- decimal_precision_
                NULL,       -- calculation_status_
                'S',        -- enable_log_
                'REC',       -- calc_process_type_
                L_nop,       -- nature_of_operation_
                NULL,       -- ignore_tax_calc_list_
                'ITEM',     -- document_series_
                492,        -- document_number_
                NULL        -- INFORMTAXRBO_TBL
                            -- extOfFiscalDocRBO_
                            -- LOCOFFISCALDOCRBO_
               )
        from dual;

   L_anydata_Obj         AnyData;

BEGIN

   open C_TEM_COST_TAX_INCL_IND;
   fetch C_TEM_COST_TAX_INCL_IND into L_item_cost_tax_incl_ind;
   close C_TEM_COST_TAX_INCL_IND;

   open C_GET_TAX_IND(L_item_cost_tax_incl_ind);
   fetch C_GET_TAX_IND into L_incl_nic_ind_pis,
                            L_incl_nic_ind_cofins,
                            L_incl_nic_ind_icms;
   close C_GET_TAX_IND;
   ---
   open C_DATE;
   fetch C_DATE into L_effective_date;
   close C_DATE;

   ---
   open C_NOP;
   fetch C_NOP into L_nop;
   close C_NOP;

   for sup_rec in C_GET_SOURCE_OBJ loop
      L_sup_ref_id := sup_rec.sup_ref_id;
      L_cnae_concat := sup_rec.cnae_code;
      L_tax_regime_concat := sup_rec.tax_regime_concat;

      open C_MULTI_CNAE;
      fetch C_MULTI_CNAE bulk collect into L_tbl_obj_eco_class_cd;
      close C_MULTI_CNAE;
      sup_rec.fiscal_ent_rec.ECOCLASSCD_TBL := L_tbl_obj_eco_class_cd;

      --tax_regime
      open C_MULTI_TAX_REGIME;
      fetch C_MULTI_TAX_REGIME bulk collect into L_tax_regime_tbl;
      close C_MULTI_TAX_REGIME;
      sup_rec.fiscal_ent_rec.DIFFTAXREGIME_TBL := L_tax_regime_tbl;


      --name/value pair
      L_nv_entity_group_concat := sup_rec.name_value_pair_id_concat;
      open C_NAME_VALUE_PAIR;
      fetch C_NAME_VALUE_PAIR bulk collect into L_nv_table;
      close C_NAME_VALUE_PAIR;
      sup_rec.fiscal_ent_rec.NAMEVALPAIRRBO_TBL := L_nv_table;

      L_sup_tbl.extend();
      L_sup_tbl(L_sup_tbl.count) := sup_rec.fiscal_ent_rec;

      -- Populate source TBL
      L_src_rec.fiscentityrbo_tbl := L_sup_tbl;
      L_src_tbl.extend();
      L_src_tbl(L_src_tbl.count) := L_src_rec;

      for loc_rec in C_GET_DEST_OBJ loop
          L_loc_ref_id := loc_rec.loc_ref_id;
          L_cnae_concat := loc_rec.cnae_code;

          open C_MULTI_CNAE;
          fetch C_MULTI_CNAE bulk collect into L_tbl_obj_eco_class_cd;
          close C_MULTI_CNAE;
          loc_rec.fiscal_ent_rec.ECOCLASSCD_TBL := L_tbl_obj_eco_class_cd;

          --loc name/value pair
          L_nv_entity_group_concat := loc_rec.name_value_pair_id_concat;
          open C_NAME_VALUE_PAIR;
          fetch C_NAME_VALUE_PAIR bulk collect into L_nv_table;
          close C_NAME_VALUE_PAIR;
          loc_rec.fiscal_ent_rec.NAMEVALPAIRRBO_TBL := L_nv_table;

          -- Populate destination TBL
          L_loc_tbl.extend();
          L_loc_tbl(L_loc_tbl.count) := loc_rec.fiscal_ent_rec;

          -- Populate source TBL
          L_dest_rec.fiscentityrbo_tbl := L_loc_tbl;
          L_dest_tbl.extend();
          L_dest_tbl(L_dest_tbl.count) := L_dest_rec;

          -- Populate the document line item TBL
          open C_GET_ITEM_OBJ;
          fetch C_GET_ITEM_OBJ bulk collect into L_doc_line_item_tbl;
          close C_GET_ITEM_OBJ;

          -- Fill the request document TBL
          if L_doc_line_item_tbl is NOT NULL and L_doc_line_item_tbl.count > 0 then
             open C_GET_DOC_DETAILS;
             fetch C_GET_DOC_DETAILS into L_fiscal_doc_rec;
             close C_GET_DOC_DETAILS;

             if L_fiscal_doc_rec is NOT NULL then
                L_fiscal_doc_tbl.extend();
                L_fiscal_doc_tbl(L_fiscal_doc_tbl.count) := L_fiscal_doc_rec;
             end if;
          end if;

          -- Clear Objects
          L_loc_tbl.delete;
          L_dest_tbl.delete;
          L_doc_line_item_tbl.delete;

      end LOOP;

      L_sup_tbl.delete;
      L_src_tbl.delete;

   end LOOP;

   if L_fiscal_doc_tbl is NOT NULL and L_fiscal_doc_tbl.count > 0 then

      L_fisc_doc_chnk_rec.chunk_id                   := 1;
      L_fisc_doc_chnk_rec.fiscdocrbo_tbl             := L_fiscal_doc_tbl;
      L_fisc_doc_chnk_tbl.extend();
      L_fisc_doc_chnk_tbl(L_fisc_doc_chnk_tbl.count) := L_fisc_doc_chnk_rec;

      O_tax_data := "RIB_FiscDocColRBM_REC"(0,
                                            L_fisc_doc_chnk_tbl, -- FISCDOCCHNKRBO_TBL
                                            'N',                 -- THREADUSE
                                            NULL,                -- logs_
                                            NULL,                -- vendorType_
                                            NULL,                -- country_
                                            NULL                 -- transactionType_
                                                                 -- extOfFiscalDocColRBM_
                                                );               -- LOCOFFISCALDOCCOLRBM_

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END GET_REQUEST_DATA_COST;
-----------------------------------------------------------------------------
FUNCTION SET_TAX_RESPONSE_DATA_COST(O_error_message    IN OUT VARCHAR2,
                                    I_TaxData          IN     "RIB_FiscDocTaxColRBM_REC",
                                    I_process_id       IN     NUMBER)
RETURN BOOLEAN IS

   L_program                      VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.SET_TAX_RESPONSE_DATA_COST';

   L_results_tbl                  L10N_BR_EXTAX_MAINT_SQL.tax_result_tbl;
   L_ctr                          binary_integer := 1;

   L_results_breakout_tbl         L10N_BR_EXTAX_MAINT_SQL.tax_result_breakout_tbl;
   L_breakout_ctr                 binary_integer := 1;

   L_loc_jurisdiction_code        varchar2(10);
   L_loc_cnpj                     varchar2(250);
   L_loc_cnae_code                varchar2(250);
   L_loc_iss_contrib_ind          varchar2(250);
   L_loc_ipi_ind                  varchar2(250);
   L_loc_icms_contrib_ind         varchar2(250);
   L_loc_nv_pair_concat           varchar2(250);

   L_sup_jurisdiction_code        varchar2(10);
   L_sup_cnpj                     varchar2(250);
   L_sup_cnae_code                varchar2(250);
   L_sup_iss_contrib_ind          varchar2(250);
   L_sup_simples_ind              varchar2(250);
   L_sup_ipi_ind                  varchar2(250);
   L_sup_icms_contrib_ind         varchar2(250);
   L_sup_st_contrib_ind           varchar2(250);
   L_sup_is_income_range_eligible varchar2(250);
   L_sup_is_distr_a_manufacturer  varchar2(250);
   L_sup_icms_simples_rate        varchar2(250);
   L_sup_tax_regime_concat        varchar2(250);
   L_src_is_rural_producer        varchar2(250);
   L_sup_nv_pair_concat           varchar2(250);

   L_state_of_manufacture         varchar2(250);
   L_pharma_list_type             varchar2(250);
   L_item_nv_pair_concat          varchar2(250);

   L_fiscal_code                  varchar2(250);

   L_effective_date               date;

   L_cum_tax_pct                  number(20,10);
   L_cum_tax_pct_ctr              binary_integer := 0;
   L_cum_tax_value                number(20,10);

   L_total_tax_amount             number(20,10);
   L_total_tax_amount_nic_incl    number(20,10);
   L_total_recoverable_amount     number(20,10);

   L_first                        varchar2(2) := null;

   cursor c_vat_codes is
      select vat_code,
             incl_nic_ind
        from vat_codes;

   TYPE nic_incl_tab IS TABLE OF VARCHAR2(1) INDEX BY VARCHAR2(6);
   L_nic_incl_tab nic_incl_tab;

   L_dest_tbl_diff_tax_regime          "RIB_DstDiffTaxRegime_TBL" := "RIB_DstDiffTaxRegime_TBL"();
   L_src_tbl_diff_tax_regime           "RIB_SrcDiffTaxRegime_TBL" := "RIB_SrcDiffTaxRegime_TBL"();

   cursor c_src_tax_regime is
   select v.value
     from TABLE(CAST(L_src_tbl_diff_tax_regime as "RIB_SrcDiffTaxRegime_TBL" )) v
     order by 1;

   L_src_tbl_obj_eco_class_cd         "RIB_SrcEcoClassCd_TBL" := "RIB_SrcEcoClassCd_TBL"();
   L_dest_tbl_obj_eco_class_cd        "RIB_DstEcoClassCd_TBL" := "RIB_DstEcoClassCd_TBL"();

   cursor c_src_eco_class is
   select v.value
     from TABLE(CAST(L_src_tbl_obj_eco_class_cd as "RIB_SrcEcoClassCd_TBL" )) v
     order by 1;

   cursor c_dest_eco_class is
   select v.value
     from TABLE(CAST(L_dest_tbl_obj_eco_class_cd as "RIB_DstEcoClassCd_TBL" )) v
     order by 1;

   L_nv_tbl                 "RIB_NameValPairRBO_TBL" := "RIB_NameValPairRBO_TBL"();
   L_RIB_NameValPairRBO_REC "RIB_NameValPairRBO_REC" := "RIB_NameValPairRBO_REC"(0,null,null);


   cursor c_nv_pairs is
      select listagg (inner.name_value_pair_id, '~')
                within group (order by inner.name_value_pair_id)
        from (select distinct name_value_pair_id
                from TABLE(CAST(L_nv_tbl as "RIB_NameValPairRBO_TBL")) v,
                     l10n_br_extax_help_nv_pair help
               where v.name  = help.name
                 and v.value = help.value) inner
       order by 1;

  L_item_service_code l10n_br_extax_stg_cost.service_code%TYPE := NULL;

BEGIN

   for rec in c_vat_codes loop
      L_nic_incl_tab(rec.vat_code) := rec.incl_nic_ind;
   end loop;

   ---

   if I_TaxData is not null AND I_Taxdata.fiscdocchnktaxrbo_tbl.count > 0 then
      for a IN I_Taxdata.fiscdocchnktaxrbo_tbl.FIRST..I_Taxdata.fiscdocchnktaxrbo_tbl.LAST LOOP

         if I_TaxData.fiscdocchnktaxrbo_tbl is not null AND I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl.count > 0 then
            for i IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl.FIRST..I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl.LAST LOOP
      
               --Grab location stuff in local variables
               L_effective_date          := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).FISCAL_DOCUMENT_DATE;
      
      
               /*SUPPLIER - source*/
               L_sup_jurisdiction_code := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).src_addr_city_id;
               L_sup_cnpj              := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).src_federal_tax_reg_id;
               L_sup_is_income_range_eligible := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).src_is_income_range_eligible;
               L_sup_is_distr_a_manufacturer  := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).src_is_distr_a_manufacturer;
               L_sup_simples_ind              :=  I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).src_is_simples_contributor;
               L_sup_icms_simples_rate        := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).src_icms_simples_rate;
               L_src_is_rural_producer        := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).src_is_rural_producer;
               L_sup_tax_regime_concat := null;
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCDIFFTAXREGIME_TBL is not null and
                  I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCDIFFTAXREGIME_TBL.COUNT > 0 then
      
                  L_first := null;
                  L_src_tbl_diff_tax_regime := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCDIFFTAXREGIME_TBL;
      
                  for g in c_src_tax_regime loop
                     L_sup_tax_regime_concat := L_sup_tax_regime_concat || L_first ||g.value;
                     L_first := '~';
                  end loop;
      
               end if;
               L_sup_cnae_code := null;
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCECOCLASSCD_TBL is not null and
                  I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCECOCLASSCD_TBL.COUNT > 0 then
      
                  L_first := null;
                  L_src_tbl_obj_eco_class_cd := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCECOCLASSCD_TBL;
      
                  for g in c_src_eco_class loop
                     L_sup_cnae_code := L_sup_cnae_code || L_first ||g.value;
                     L_first := '~';
                  end loop;
      
               end if;
      
               -- default the indicators to 'N'
               L_sup_iss_contrib_ind := 'N';
               L_sup_ipi_ind         := 'N';
               L_sup_icms_contrib_ind := 'N';
               L_sup_st_contrib_ind  :='N';
      
      
               if I_TaxData.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcTaxContributor_TBL is not null and
                  I_TaxData.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcTaxContributor_TBL.count > 0 then
      
                  for g IN I_TaxData.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcTaxContributor_TBL.FIRST..
                           I_TaxData.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcTaxContributor_TBL.LAST LOOP
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCTAXCONTRIBUTOR_TBL(g).value = 'ISS' then
                        L_sup_iss_contrib_ind := 'Y';
                     end if;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCTAXCONTRIBUTOR_TBL(g).value = 'IPI' then
                        L_sup_ipi_ind := 'Y';
                     end if;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCTAXCONTRIBUTOR_TBL(g).value = 'ICMS' then
                        L_sup_icms_contrib_ind := 'Y';
                     end if;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SRCTAXCONTRIBUTOR_TBL(g).value = 'ST' then
                        L_sup_st_contrib_ind := 'Y';
                     end if;
      
                  end loop;
               end if;
      
               L_nv_tbl.delete;
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcNameValPair_TBL is not NULL and I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcNameValPair_TBL.count > 0 then 
                  for g in I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcNameValPair_TBL.FIRST ..
                           I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcNameValPair_TBL.LAST LOOP  
      
                     L_RIB_NameValPairRBO_REC.name  := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcNameValPair_TBL(g).NAMEVALPAIRRBO.name;
                     L_RIB_NameValPairRBO_REC.value := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).SrcNameValPair_TBL(g).NAMEVALPAIRRBO.value;
                     L_nv_tbl.extend;
                     L_nv_tbl(L_nv_tbl.count) := L_RIB_NameValPairRBO_REC;
      
                   end LOOP;
                end if;
      
               L_sup_nv_pair_concat := null;
               open c_nv_pairs;
               fetch c_nv_pairs into L_sup_nv_pair_concat;
               close c_nv_pairs;
      
               /*LOC - destination*/
               L_loc_jurisdiction_code := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).dst_addr_city_id;
               L_loc_cnpj              := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).dst_federal_tax_reg_id;
      
      
               L_loc_cnae_code := null;
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTECOCLASSCD_TBL is not null and
                  I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTECOCLASSCD_TBL.COUNT > 0 then
      
                  L_first := null;
                  L_dest_tbl_obj_eco_class_cd := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTECOCLASSCD_TBL;
      
                  for g in c_dest_eco_class loop
                     L_loc_cnae_code := L_loc_cnae_code || L_first ||g.value;
                     L_first := '~';
                  end loop;
      
               end if;
      
               -- default the indicators
               L_loc_iss_contrib_ind := 'N';
               L_loc_ipi_ind := 'N';
               L_loc_icms_contrib_ind := 'N';
      
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL is not null and
                  I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL.count > 0 then
      
                  for g IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL.FIRST..
                           I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL.LAST LOOP
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL(g).value = 'ISS' then
                        L_loc_iss_contrib_ind := 'Y';
                     end if;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL(g).value = 'IPI' then
                        L_loc_ipi_ind := 'Y';
                     end if;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DSTTAXCONTRIBUTOR_TBL(g).value = 'ICMS' then
                        L_loc_icms_contrib_ind := 'Y';
                     end if;
      
                  end loop;
               end if;
      
      
               L_nv_tbl.delete;
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL is not NULL and I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL.count > 0 then 
                  for g in I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL.FIRST ..
                           I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL.LAST LOOP  
      
                     L_RIB_NameValPairRBO_REC.name  := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL(g).NAMEVALPAIRRBO.name;
                     L_RIB_NameValPairRBO_REC.value := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).DstNameValPair_TBL(g).NAMEVALPAIRRBO.value;
                     L_nv_tbl.extend;
                     L_nv_tbl(L_nv_tbl.count) := L_RIB_NameValPairRBO_REC;
      
                   end LOOP;
                end if;
      
               L_loc_nv_pair_concat := null;
               open c_nv_pairs;
               fetch c_nv_pairs into L_loc_nv_pair_concat;
               close c_nv_pairs;
      
      
               --Item stuff for current location
               if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL.count > 0 then
                  for j IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL.FIRST..
                           I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL.LAST LOOP
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PrdItemTaxRBO_TBL is not NULL then 
                        if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL.count > 0 then
                           for k IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL.FIRST..
                                    I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL.LAST LOOP
                              L_results_tbl(L_ctr).item_origin_code :=
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).item_origin;
                              L_results_tbl(L_ctr).state_of_manufacture :=
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).state_of_manufacture;
                              L_results_tbl(L_ctr).pharma_list_type :=
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).pharma_list_type;
         
                              L_results_tbl(L_ctr).item_classification_id :=
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).fiscal_classification_code;
        
                              L_results_tbl(L_ctr).item_ex_ipi :=
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).ipi_exception_code;   
      
                              L_fiscal_code :=
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).ext_fiscal_class_code;
                              if instr(L_fiscal_code,'.') > 0 then
                                 L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE :=
                                 SUBSTR(L_fiscal_code,1, INSTRB(L_fiscal_code,'.',1,1) -1);
      
                                 L_results_tbl(L_ctr).ITEM_PAUTA_CODE:=
                                    SUBSTR(L_fiscal_code,INSTRB(L_fiscal_code,'.',1,1) + 1);
                              else -- the fiscal_class_code contains just the ncm_char_code or null
                                 L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE := L_fiscal_code;
                              end if;
      
                              L_results_tbl(L_ctr).item_xform_ind := 
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).PRDITEMTAXRBO_TBL(k).is_transformed_item; 
                              L_results_tbl(L_ctr).ITEM_SERVICE_IND :=  'N' ;
                           end loop;
                        end if;
                     end if;
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL is not null then
                        if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL.count > 0 then 
                           for k IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL.FIRST..
                                    I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL.LAST LOOP
      
                           L_fiscal_code :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL(k).ext_fiscal_class_code;
                           if instr(L_fiscal_code,'.') > 0 then
                              L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE :=
                                 SUBSTR(L_fiscal_code,1, INSTRB(L_fiscal_code,'.',1,1) -1);


                              L_results_tbl(L_ctr).ITEM_PAUTA_CODE:=
                                 SUBSTR(L_fiscal_code,INSTRB(L_fiscal_code,'.',1,1) + 1);

                           else -- the fiscal_class_code contains just the ncm_char_code or null
                              L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE := L_fiscal_code;
                           end if;

                           L_results_tbl(L_ctr).ITEM_SERVICE_CODE :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL(k).federal_service_code;
                           L_results_tbl(L_ctr).ITEM_FEDERAL_SERVICE :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).SVCITEMTAXRBO_TBL(k).dst_service_code;

                              L_results_tbl(L_ctr).item_xform_ind := 'N'; --assumes service items are not xform
                              L_results_tbl(L_ctr).ITEM_SERVICE_IND :=  'Y' ;
                           end loop;
                        end if;
                     end if;
      
      
                     L_nv_tbl.delete;
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).NameValPairRBO_TBL is not null and
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).NameValPairRBO_TBL.count > 0 then  
       
                        L_nv_tbl :=I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).NameValPairRBO_TBL;
      
                        open c_nv_pairs;
                        fetch c_nv_pairs into L_results_tbl(L_ctr).item_name_value_pair_id_concat;
                        close c_nv_pairs;
                     end if;
      
                     L_results_tbl(L_ctr).unit_cost := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).unit_cost;
      
                     --
                     L_results_tbl(L_ctr).item_dim_object :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).dim_object;
                     if L_results_tbl(L_ctr).item_dim_object = 'Dummy' then
                        L_results_tbl(L_ctr).item_dim_object := NULL;
                     end if;
                     L_results_tbl(L_ctr).item_length :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).length;
                     L_results_tbl(L_ctr).item_lwh_uom :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).lwh_uom;
                     L_results_tbl(L_ctr).item_weight :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).weight;
                     L_results_tbl(L_ctr).item_net_weight :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).net_weight;
                     L_results_tbl(L_ctr).item_weight_uom :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).weight_uom;
                     L_results_tbl(L_ctr).item_liquid_volume :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).liquid_volume;
                     L_results_tbl(L_ctr).item_liquid_volume_uom :=
                        I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).liquid_volume_uom;
      
                     --push location and supplier level attribs to item level
                     L_results_tbl(L_ctr).sup_jurisdiction_code  := L_sup_jurisdiction_code;
                     L_results_tbl(L_ctr).sup_cnae_code          := L_sup_cnae_code;
                     L_results_tbl(L_ctr).sup_iss_contrib_ind    := L_sup_iss_contrib_ind;
                     L_results_tbl(L_ctr).sup_simples_ind        := L_sup_simples_ind;
                     L_results_tbl(L_ctr).sup_ipi_ind            := L_sup_ipi_ind;
                     L_results_tbl(L_ctr).sup_icms_contrib_ind   := L_sup_icms_contrib_ind;
                     L_results_tbl(L_ctr).sup_st_contrib_ind     := L_sup_st_contrib_ind;
                     L_results_tbl(L_ctr).sup_cnpj               := L_sup_cnpj;
                     L_results_tbl(L_ctr).sup_is_income_range_eligible  := L_sup_is_income_range_eligible;
                     L_results_tbl(L_ctr).sup_is_distr_a_manufacturer   := L_sup_is_distr_a_manufacturer;
                     L_results_tbl(L_ctr).sup_icms_simples_rate         := L_sup_icms_simples_rate;
                     L_results_tbl(L_ctr).src_is_rural_producer         := L_src_is_rural_producer;
                     L_results_tbl(L_ctr).sup_tax_regime_concat         := L_sup_tax_regime_concat;
                     L_results_tbl(L_ctr).sup_name_value_pair_id_concat := L_sup_nv_pair_concat;
                     --
                     L_results_tbl(L_ctr).loc_jurisdiction_code  := L_loc_jurisdiction_code;
                     L_results_tbl(L_ctr).loc_cnae_code          := L_loc_cnae_code;
                     L_results_tbl(L_ctr).loc_iss_contrib_ind    := L_loc_iss_contrib_ind;
                     L_results_tbl(L_ctr).loc_ipi_ind            := L_loc_ipi_ind;
                     L_results_tbl(L_ctr).loc_icms_contrib_ind   := L_loc_icms_contrib_ind;
                     L_results_tbl(L_ctr).loc_cnpj               := L_loc_cnpj;
                     L_results_tbl(L_ctr).loc_name_value_pair_id_concat := L_loc_nv_pair_concat;
                     --
                     L_results_tbl(L_ctr).effective_date         := L_effective_date;
                     L_results_tbl(L_ctr).pack_no                := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).pack_item_id;
                     L_results_tbl(L_ctr).item                   := I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).item_id;
      
                     L_cum_tax_pct               := 0;
                     L_cum_tax_pct_ctr           := 0;
                     L_cum_tax_value             := 0;
                     L_total_tax_amount          := 0;
                     L_total_tax_amount_nic_incl := 0;
                     L_total_recoverable_amount  := 0;
      
                     if I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL.count > 0 then
                        for k IN I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL.FIRST..
                                 I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL.LAST LOOP
      
                           L_results_breakout_tbl(L_breakout_ctr).sup_jurisdiction_code := L_results_tbl(L_ctr).sup_jurisdiction_code;
                           L_results_breakout_tbl(L_breakout_ctr).sup_cnae_code         := L_results_tbl(L_ctr).sup_cnae_code;
                           L_results_breakout_tbl(L_breakout_ctr).sup_iss_contrib_ind   := L_results_tbl(L_ctr).sup_iss_contrib_ind;
                           L_results_breakout_tbl(L_breakout_ctr).sup_simples_ind       := L_results_tbl(L_ctr).sup_simples_ind;
                           L_results_breakout_tbl(L_breakout_ctr).sup_ipi_ind           := L_results_tbl(L_ctr).sup_ipi_ind;
                           L_results_breakout_tbl(L_breakout_ctr).sup_icms_contrib_ind  := L_results_tbl(L_ctr).sup_icms_contrib_ind;
                           L_results_breakout_tbl(L_breakout_ctr).sup_st_contrib_ind    := L_results_tbl(L_ctr).sup_st_contrib_ind;
                           L_results_breakout_tbl(L_breakout_ctr).sup_cnpj              := L_results_tbl(L_ctr).sup_cnpj;
                           L_results_breakout_tbl(L_breakout_ctr).sup_is_income_range_eligible   := L_results_tbl(L_ctr).sup_is_income_range_eligible;
                           L_results_breakout_tbl(L_breakout_ctr).sup_is_distr_a_manufacturer    := L_results_tbl(L_ctr).sup_is_distr_a_manufacturer;
                           L_results_breakout_tbl(L_breakout_ctr).sup_icms_simples_rate          := L_results_tbl(L_ctr).sup_icms_simples_rate;
                           L_results_breakout_tbl(L_breakout_ctr).src_is_rural_producer          := L_results_tbl(L_ctr).src_is_rural_producer;
                           L_results_breakout_tbl(L_breakout_ctr).sup_tax_regime_concat          := L_results_tbl(L_ctr).sup_tax_regime_concat;
                           L_results_breakout_tbl(L_breakout_ctr).sup_name_value_pair_id_concat  := L_results_tbl(L_ctr).sup_name_value_pair_id_concat;
                           --
                           L_results_breakout_tbl(L_breakout_ctr).loc_jurisdiction_code := L_results_tbl(L_ctr).loc_jurisdiction_code;
                           L_results_breakout_tbl(L_breakout_ctr).loc_cnae_code         := L_results_tbl(L_ctr).loc_cnae_code;
                           L_results_breakout_tbl(L_breakout_ctr).loc_iss_contrib_ind   := L_results_tbl(L_ctr).loc_iss_contrib_ind;
                           L_results_breakout_tbl(L_breakout_ctr).loc_ipi_ind           := L_results_tbl(L_ctr).loc_ipi_ind;
                           L_results_breakout_tbl(L_breakout_ctr).loc_icms_contrib_ind  := L_results_tbl(L_ctr).loc_icms_contrib_ind;
                           L_results_breakout_tbl(L_breakout_ctr).loc_cnpj              := L_results_tbl(L_ctr).loc_cnpj;
                           L_results_breakout_tbl(L_breakout_ctr).loc_name_value_pair_id_concat  := L_results_tbl(L_ctr).loc_name_value_pair_id_concat;
                           --
                           L_results_breakout_tbl(L_breakout_ctr).item_origin_code       := L_results_tbl(L_ctr).item_origin_code;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_CLASSIFICATION_ID := L_results_tbl(L_ctr).ITEM_CLASSIFICATION_ID;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_NCM_CHAR_CODE     := L_results_tbl(L_ctr).ITEM_NCM_CHAR_CODE;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_EX_IPI            := L_results_tbl(L_ctr).ITEM_EX_IPI;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_PAUTA_CODE        := L_results_tbl(L_ctr).ITEM_PAUTA_CODE;
                           L_results_breakout_tbl(L_breakout_ctr).item_xform_ind         := L_results_tbl(L_ctr).item_xform_ind;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_SERVICE_CODE      := L_results_tbl(L_ctr).ITEM_SERVICE_CODE;
                           L_results_breakout_tbl(L_breakout_ctr).ITEM_FEDERAL_SERVICE   := L_results_tbl(L_ctr).ITEM_FEDERAL_SERVICE;
                           L_results_breakout_tbl(L_breakout_ctr).unit_cost              := L_results_tbl(L_ctr).unit_cost;
                           L_results_breakout_tbl(L_breakout_ctr).state_of_manufacture   := L_results_tbl(L_ctr).state_of_manufacture;
                           L_results_breakout_tbl(L_breakout_ctr).pharma_list_type       := L_results_tbl(L_ctr).pharma_list_type;
                           L_results_breakout_tbl(L_breakout_ctr).item_name_value_pair_id_concat := 
                              L_results_tbl(L_ctr).item_name_value_pair_id_concat;
      
                           L_results_breakout_tbl(L_breakout_ctr).item_dim_object        := L_results_tbl(L_ctr).item_dim_object ;
                           L_results_breakout_tbl(L_breakout_ctr).item_length            := L_results_tbl(L_ctr).item_length ;
                           L_results_breakout_tbl(L_breakout_ctr).item_lwh_uom           := L_results_tbl(L_ctr).item_lwh_uom;
                           L_results_breakout_tbl(L_breakout_ctr).item_weight            := L_results_tbl(L_ctr).item_weight ;
                           L_results_breakout_tbl(L_breakout_ctr).item_net_weight        := L_results_tbl(L_ctr).item_net_weight ;
                           L_results_breakout_tbl(L_breakout_ctr).item_weight_uom        := L_results_tbl(L_ctr).item_weight_uom ;
                           L_results_breakout_tbl(L_breakout_ctr).item_liquid_volume     := L_results_tbl(L_ctr).item_liquid_volume ;
                           L_results_breakout_tbl(L_breakout_ctr).item_liquid_volume_uom := L_results_tbl(L_ctr).item_liquid_volume_uom ;
      
                           L_results_breakout_tbl(L_breakout_ctr).effective_date         := L_results_tbl(L_ctr).effective_date;
                           L_results_breakout_tbl(L_breakout_ctr).pack_no                := L_results_tbl(L_ctr).pack_no;
                           L_results_breakout_tbl(L_breakout_ctr).item                   := L_results_tbl(L_ctr).item;
      
                           L_results_breakout_tbl(L_breakout_ctr).tax_code               :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_code;
                           L_results_breakout_tbl(L_breakout_ctr).tax_amount             :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_amount;
                           L_results_breakout_tbl(L_breakout_ctr).tax_rate               :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_rate;
                           L_results_breakout_tbl(L_breakout_ctr).tax_basis_amount       :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_basis_amount;
      
                           L_results_breakout_tbl(L_breakout_ctr).recoverable_amount     :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).recoverable_amt;
      
                           if nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_basis_amount,0) = 0 then
                              L_results_breakout_tbl(L_breakout_ctr).calculation_basis := 'V';
                           else
                              L_results_breakout_tbl(L_breakout_ctr).calculation_basis := 'P';
                           end if;
                           
                           L_results_breakout_tbl(L_breakout_ctr).modified_tax_basis_amount :=
                              I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).modified_tax_basis_amount;
      
                           L_breakout_ctr := L_breakout_ctr + 1;
      
                           ----
      
                           if (nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_basis_amount,0) = 0) then
                              L_cum_tax_pct := L_cum_tax_pct +
                                 nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_rate,0);
                              L_cum_tax_pct_ctr := L_cum_tax_pct_ctr + 1;
                           else
                              L_cum_tax_value := L_cum_tax_value +
                                 nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_rate,0);
                           end if;
      
                           L_total_tax_amount          := L_total_tax_amount +
                              nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_amount,0);
                           L_total_recoverable_amount  := L_total_recoverable_amount +
                              nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).recoverable_amt,0);
      
                           if L_nic_incl_tab(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_code) = 'Y' then
                              L_total_tax_amount_nic_incl := L_total_tax_amount_nic_incl +
                                 nvl(I_Taxdata.fiscdocchnktaxrbo_tbl(a).fiscdoctaxrbo_tbl(i).LINEITEMTAXRBO_TBL(j).TAXDETRBO_TBL(k).tax_amount,0);
                           end if;
      
      
                        end loop;
                     end if;
      
                     if (L_cum_tax_pct_ctr != 0) then
                           L_results_tbl(L_ctr).cum_tax_pct := L_cum_tax_pct;
                     else
                           L_results_tbl(L_ctr).cum_tax_pct := 0;
                     end if;
                     L_results_tbl(L_ctr).cum_tax_value := L_cum_tax_value;
      
                     L_results_tbl(L_ctr).total_tax_amount          := L_total_tax_amount;
                     L_results_tbl(L_ctr).total_recoverable_amount  := L_total_recoverable_amount;
                     L_results_tbl(L_ctr).total_tax_amount_nic_incl := L_total_tax_amount_nic_incl;
      
                     L_ctr := L_ctr + 1;
      
                  end loop;
               end if;
      
            end loop;
         end if;
      end loop;
   end if;

   FORALL i in L_results_tbl.FIRST..L_results_tbl.LAST

      insert into l10n_br_extax_res_cost (sup_city,
                                sup_cnae_code,
                                sup_iss,
                                sup_simples,
                                sup_ipi,
                                sup_icms,
                                sup_st,
                                sup_cnpj,
                                sup_is_income_range_eligible,
                                sup_is_distr_a_manufacturer,
                                sup_icms_simples_rate,
                                sup_tax_regime_concat,
                                sup_rural_prod_ind,
                                sup_name_value_pair_id_concat,
                                loc_city,
                                loc_cnae_code,
                                loc_iss,
                                loc_ipi,
                                loc_icms,
                                loc_cnpj,
                                loc_name_value_pair_id_concat,
                                origin_code,
                                service_ind,
                                classification_id,
                                ncm_char_code,
                                ex_ipi,
                                pauta_code,
                                service_code,
                                federal_service,
                                unit_cost,
                                state_of_manufacture,
                                pharma_list_type,
                                item_name_value_pair_id_concat,
                                dim_object,
                                length,
                                lwh_uom,
                                weight,
                                net_weight,
                                weight_uom,
                                liquid_volume,
                                liquid_volume_uom,
                                item_xform_ind,
                                effective_date,
                                pack_no,
                                item,
                                cum_tax_pct,
                                cum_tax_value,
                                total_tax_amount,
                                total_recoverable_amount,
                                total_tax_amount_nic_incl,
                                process_id)
      values (L_results_tbl(i).sup_jurisdiction_code,
              L_results_tbl(i).sup_cnae_code,
              L_results_tbl(i).sup_iss_contrib_ind,
              L_results_tbl(i).sup_simples_ind,
              L_results_tbl(i).sup_ipi_ind,
              L_results_tbl(i).sup_icms_contrib_ind,
              L_results_tbl(i).sup_st_contrib_ind,
              L_results_tbl(i).sup_cnpj,
              nvl(L_results_tbl(i).sup_is_income_range_eligible,'N'),
              nvl(L_results_tbl(i).sup_is_distr_a_manufacturer,'N'),
              nvl(L_results_tbl(i).sup_icms_simples_rate,'-1'), 
              nvl(L_results_tbl(i).sup_tax_regime_concat,'-1'),
              nvl(L_results_tbl(i).src_is_rural_producer,'N'),
              nvl(L_results_tbl(i).sup_name_value_pair_id_concat,'-1'),
              --
              L_results_tbl(i).loc_jurisdiction_code,
              L_results_tbl(i).loc_cnae_code,
              L_results_tbl(i).loc_iss_contrib_ind,
              L_results_tbl(i).loc_ipi_ind,
              L_results_tbl(i).loc_icms_contrib_ind,
              L_results_tbl(i).loc_cnpj,
              nvl(L_results_tbl(i).loc_name_value_pair_id_concat,'-1'),
              --
              L_results_tbl(i).item_origin_code,
              nvl(L_results_tbl(i).ITEM_SERVICE_IND,'N'),
              nvl(L_results_tbl(i).item_classification_id,-1),
              nvl(L_results_tbl(i).ITEM_NCM_CHAR_CODE,-1),
              nvl(L_results_tbl(i).ITEM_EX_IPI,-1),
              nvl(L_results_tbl(i).ITEM_PAUTA_CODE,-1),
              nvl(L_results_tbl(i).ITEM_SERVICE_CODE,-1),
              nvl(L_results_tbl(i).ITEM_FEDERAL_SERVICE,-1),
              L_results_tbl(i).unit_cost,
              nvl(L_results_tbl(i).state_of_manufacture,-1),
              nvl(L_results_tbl(i).pharma_list_type,-1),
              nvl(L_results_tbl(i).item_name_value_pair_id_concat,-1),
              nvl(L_results_tbl(i).item_dim_object, '-999'),
              nvl(L_results_tbl(i).item_length,'-999'),
              nvl(L_results_tbl(i).item_lwh_uom,'-999'),
              nvl(L_results_tbl(i).item_weight,'-999'),
              nvl(L_results_tbl(i).item_net_weight,'-999'),
              nvl(L_results_tbl(i).item_weight_uom,'-999'),
              nvl(L_results_tbl(i).item_liquid_volume,'-999'),
              nvl(L_results_tbl(i).item_liquid_volume_uom,'-999'),
              nvl(L_results_tbl(i).item_xform_ind,'N'),
              trunc(L_results_tbl(i).effective_date),
              nvl(L_results_tbl(i).pack_no,'-1'),
              L_results_tbl(i).item,
              L_results_tbl(i).cum_tax_pct,
              L_results_tbl(i).cum_tax_value,
              L_results_tbl(i).total_tax_amount,
              L_results_tbl(i).total_recoverable_amount,
              L_results_tbl(i).total_tax_amount_nic_incl,
              I_process_id);

   FORALL i in L_results_breakout_tbl.FIRST..L_results_breakout_tbl.LAST
      insert into l10n_br_extax_res_cost_det (sup_city,
                                         sup_cnae_code,
                                         sup_iss,
                                         sup_simples,
                                         sup_ipi,
                                         sup_icms,
                                         sup_st,
                                         sup_cnpj,
                                         sup_is_income_range_eligible,
                                         sup_is_distr_a_manufacturer,
                                         sup_icms_simples_rate,
                                         sup_tax_regime_concat,
                                         sup_rural_prod_ind,
                                         sup_name_value_pair_id_concat,
                                         loc_city,
                                         loc_cnae_code,
                                         loc_iss,
                                         loc_ipi,
                                         loc_icms,
                                         loc_cnpj,
                                         loc_name_value_pair_id_concat,
                                         origin_code,
                                         service_ind,
                                         classification_id,
                                         ncm_char_code,
                                         ex_ipi,
                                         pauta_code,
                                         service_code,
                                         federal_service,
                                         unit_cost,
                                         state_of_manufacture,
                                         pharma_list_type,
                                         item_name_value_pair_id_concat,
                                         dim_object,
                                         length,
                                         lwh_uom,
                                         weight,
                                         net_weight,
                                         weight_uom,
                                         liquid_volume,
                                         liquid_volume_uom,
                                         item_xform_ind,
                                         effective_date,
                                         pack_no,
                                         item,
                                         tax_code,
                                         tax_rate,
                                         tax_basis_amount,
                                         tax_amount,
                                         recoverable_amount,
                                         calculation_basis,
                                         modified_tax_basis_amount,
                                         process_id)
      values (L_results_breakout_tbl(i).sup_jurisdiction_code,
              L_results_breakout_tbl(i).sup_cnae_code,
              L_results_breakout_tbl(i).sup_iss_contrib_ind,
              L_results_breakout_tbl(i).sup_simples_ind,
              L_results_breakout_tbl(i).sup_ipi_ind,
              L_results_breakout_tbl(i).sup_icms_contrib_ind,
              L_results_breakout_tbl(i).sup_st_contrib_ind,
              L_results_breakout_tbl(i).sup_cnpj,
              nvl(L_results_breakout_tbl(i).sup_is_income_range_eligible,'N'),
              nvl(L_results_breakout_tbl(i).sup_is_distr_a_manufacturer,'N'),
              nvl(L_results_breakout_tbl(i).sup_icms_simples_rate,'-1'), 
              nvl(L_results_breakout_tbl(i).sup_tax_regime_concat,'-1'),
              nvl(L_results_breakout_tbl(i).src_is_rural_producer,'N'),
              nvl(L_results_breakout_tbl(i).sup_name_value_pair_id_concat,'-1'),
              --
              L_results_breakout_tbl(i).loc_jurisdiction_code,
              L_results_breakout_tbl(i).loc_cnae_code,
              L_results_breakout_tbl(i).loc_iss_contrib_ind,
              L_results_breakout_tbl(i).loc_ipi_ind,
              L_results_breakout_tbl(i).loc_icms_contrib_ind,
              L_results_breakout_tbl(i).loc_cnpj,
              nvl(L_results_breakout_tbl(i).loc_name_value_pair_id_concat,'-1'),
              --
              L_results_breakout_tbl(i).item_origin_code,
              nvl(L_results_breakout_tbl(i).ITEM_SERVICE_IND,'N'),
              nvl(L_results_breakout_tbl(i).item_classification_id,-1),
              nvl(L_results_breakout_tbl(i).ITEM_NCM_CHAR_CODE,-1),
              nvl(L_results_breakout_tbl(i).ITEM_EX_IPI,-1),
              nvl(L_results_breakout_tbl(i).ITEM_PAUTA_CODE,-1),
              nvl(L_results_breakout_tbl(i).ITEM_SERVICE_CODE,-1),
              nvl(L_results_breakout_tbl(i).ITEM_FEDERAL_SERVICE,-1),
              L_results_breakout_tbl(i).unit_cost,
              nvl(L_results_breakout_tbl(i).state_of_manufacture,-1),
              nvl(L_results_breakout_tbl(i).pharma_list_type,-1),
              nvl(L_results_breakout_tbl(i).item_name_value_pair_id_concat,-1),
              --
              nvl(L_results_breakout_tbl(i).item_dim_object,'-999'),
              nvl(L_results_breakout_tbl(i).item_length,'-999'),
              nvl(L_results_breakout_tbl(i).item_lwh_uom,'-999'),
              nvl(L_results_breakout_tbl(i).item_weight,'-999'),
              nvl(L_results_breakout_tbl(i).item_net_weight,'-999'),
              nvl(L_results_breakout_tbl(i).item_weight_uom,'-999'),
              nvl(L_results_breakout_tbl(i).item_liquid_volume,'-999'),
              nvl(L_results_breakout_tbl(i).item_liquid_volume_uom,'-999'),
              nvl(L_results_breakout_tbl(i).item_xform_ind,'N'),
              --
              trunc(L_results_breakout_tbl(i).effective_date),
              nvl(L_results_breakout_tbl(i).pack_no,'-1'),
              L_results_breakout_tbl(i).item,
              L_results_breakout_tbl(i).tax_code,
              nvl(L_results_breakout_tbl(i).tax_rate,0),
              nvl(L_results_breakout_tbl(i).tax_basis_amount,0),
              nvl(L_results_breakout_tbl(i).tax_amount,0),
              nvl(L_results_breakout_tbl(i).recoverable_amount,0),
              L_results_breakout_tbl(i).calculation_basis,
              L_results_breakout_tbl(i).modified_tax_basis_amount,
              I_process_id);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SET_TAX_RESPONSE_DATA_COST;
-----------------------------------------------------------------------------
FUNCTION FISCAL_ITEM_RECLASS_COST(O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.FISCAL_ITEM_RECLASS_COST';
   L_process_count NUMBER := null;
   L_vdate         PERIOD.VDATE%TYPE := GET_VDATE;

   -- Collection for tax engine call
   L_iscl_tax_update_tbl          OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();

BEGIN

   --create the cost changes
   insert into gtax_cost_change_gtt (effective_date,
                                     supplier,
                                     origin_country_id,
                                     country_id,
                                     item,
                                     loc_type,
                                     loc,
                                     unit_cost,
                                     header_id,
                                     detail_count,
                                     cost_change_id)
   select distinct
          i.active_date,
          iscl.supplier,
          iscl.origin_country_id,
          mle.country_id,
          iscl.item,
          iscl.loc_type,
          iscl.loc,
          iscl.unit_cost,
          dense_rank() over(order by i.active_date,
                                     iscl.supplier,
                                     iscl.origin_country_id,
                                     mle.country_id) rank_value,
          row_number() over(partition by i.active_date,
                                         iscl.supplier,
                                         iscl.origin_country_id,
                                         mle.country_id
                            order by iscl.loc,iscl.item,iscl.unit_cost) cnt,
          null
     from l10n_br_fiscal_reclass i,
          item_supp_country_loc iscl,
          item_master im,
          mv_l10n_entity mle
    where i.item              = im.item
      and i.status            = 'P'
      and i.processed_date    = L_vdate
      and i.active_date       >= L_vdate
      and im.status           = 'A'
      and im.tran_level       = im.item_level
      and im.pack_ind         = 'N'
      --
      and iscl.item           = i.item
      --
      and to_char(iscl.loc)    = mle.entity_id
      and mle.entity           = 'LOC';

   if CREATE_COST_CHANGES(O_error_message) = FALSE then
      return FALSE;
   end if;

   --if the item is not ranged to the default location on country_attrib, 
   --ensure that the item_cost_head/detail tables are updated to reflect 
   --the fiscal reclass

   select OBJ_COMP_ITEM_COST_REC(r.item,
                                 ich.nic_static_ind,
                                 ich.supplier,
                                 ca.default_loc,
                                 ca.default_loc_type,
                                 get_vdate,
                                 'ITEMSUPPCTRYLOC',
                                 ich.origin_country_id, --i_origin_country_id
                                 ich.delivery_country_id, --i_delivery_country_id
                                 'Y', --i_prim_dlvy_ctry_ind
                                 'Y', --i_update_itemcost_ind
                                 'N', --i_update_itemcost_child_ind
                                 ich.nic_static_ind, --i_item_cost_tax_incl_ind
                                 NVL(isc.unit_cost, 0),
                                 NULL, --o_extended_base_cost
                                 NULL, --o_inclusive_cost
                                 NVL(isc.unit_cost, 0),
                                 NULL, --svat_tax_rate
                                 NULL, --tax_loc_type
                                 NULL, --pack_ind
                                 NULL, --pack_type
                                 NULL, --dept
                                 NULL, --prim_supp_currency_code
                                 NULL, --loc_prim_country
                                 NULL, --loc_prim_country_tax_incl_ind
                                 NULL, --gtax_total_tax_amount
                                 NULL, --gtax_total_tax_amount_nic
                                 NULL) --gtax_total_recover_amount
      BULK COLLECT INTO L_iscl_tax_update_tbl
      from l10n_br_fiscal_reclass r,
           item_cost_head ich,
           mv_l10n_entity mle,
           country_attrib ca,
           item_supp_country isc
     where r.item                   = ich.item
       and r.status                 = 'C'
       and r.active_date           <= L_vdate
       and r.processed_date         = L_vdate
       --
       and ca.country_id            = 'BR'
       and mle.country_id           = ca.country_id
       and mle.entity_id            = ca.default_loc
       and ((mle.entity = 'LOC' or (mle.entity = 'PTNR' and mle.entity_type = 'E')))
       --
       and isc.item                 = ich.item
       and isc.supplier             = ich.supplier
       and isc.origin_country_id    = ich.origin_country_id
       and ich.prim_dlvy_ctry_ind   = 'Y'
       --
       and not exists (select 'x'
                         from item_supp_country_loc iscl
                        where iscl.item              = ich.item
                          and iscl.supplier          = ich.supplier
                          and iscl.origin_country_id = ich.origin_country_id
                          and iscl.loc               = ca.default_loc);

   -- Call tax engine to compute tax-related costs
   if L_iscl_tax_update_tbl.COUNT > 0 then
      if ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK(O_error_message,
                                              L_iscl_tax_update_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FISCAL_ITEM_RECLASS_COST;
-----------------------------------------------------------------------------
FUNCTION GET_SEQ(I_next             IN     VARCHAR2)
RETURN NUMBER IS
   L_program       VARCHAR2(255) := 'L10N_BR_EXTAX_MAINT_SQL.GET_SEQ';

   L_return        l10n_br_extax_stg_cost.process_id%TYPE;

   cursor c_next is
      select l10n_br_tax_service_seq.nextval
        from dual;

   cursor c_curr is
      select l10n_br_tax_service_seq.currval
        from dual;

BEGIN

   if I_next = 'Y' then
      open c_next;
      fetch c_next into L_return;
      close c_next;
   else
      open c_curr;
      fetch c_curr into L_return;
      close c_curr;
   end if;

   return L_return;

END GET_SEQ;
-----------------------------------------------------------------------------
END L10N_BR_EXTAX_MAINT_SQL;
/
