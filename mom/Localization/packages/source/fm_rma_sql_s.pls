CREATE OR REPLACE PACKAGE FM_RMA_SQL AS
--------------------------------------------------------------------------------
LP_cre_type     VARCHAR2(15) := 'pendretcre';
LP_mod_type     VARCHAR2(15) := 'pendretmod';  
--------------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code          IN OUT  VARCHAR2,
                   O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_rmadesc_rec          IN      "OBJ_RMAITEMDesc_REC",
                   I_message_type         IN      VARCHAR2);
--------------------------------------------------------------------------------
END FM_RMA_SQL;
/
 
