CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DETAIL_VAL_SQL is
-----------------------------------------------------------------------------------
FUNCTION GET_QUERY_TYPE_TAXES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_query             IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_QUERY_TYPE_TAXES';
   ---
BEGIN
   ---
   O_query := 'select vfvc.vat_code, vfvc.vat_code '||
                'from vat_codes vfvc ';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_QUERY_TYPE_TAXES;
-----------------------------------------------------------------------------------
FUNCTION GET_ITEM_CLASSIFICATION(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_classification      IN OUT V_FISCAL_CLASSIFICATION.CLASSIFICATION_ID%TYPE,
                                 O_classification_desc IN OUT V_FISCAL_CLASSIFICATION.CLASSIFICATION_DESC%TYPE,
                                 I_item                IN     V_BR_ITEM_FISCAL_ATTRIB.ITEM%TYPE,
                                 I_key_value_1         IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                 I_location            IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                 I_loc_type            IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                 I_issue_date          IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_ITEM_CLASSIFICATION';
   L_key       VARCHAR2(100);
   ---
   cursor C_V_FM_ITEM_ATTRIBUTES is
      select a.classification_id      classification_id,
             b.classification_desc    classification_desc
      from   v_br_item_fiscal_attrib a,
             v_fiscal_classification b
      where b.classification_id = a.classification_id
        and a.item              = I_item;


   ---
BEGIN
   ---
   L_key := 'Item: '||I_item;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_V_FM_ITEM_ATTRIBUTES', 'V_FM_ITEM_ATTRIBUTES', L_key);
   open C_V_FM_ITEM_ATTRIBUTES;
   SQL_LIB.SET_MARK('FETCH', 'C_V_FM_ITEM_ATTRIBUTES', 'V_FM_ITEM_ATTRIBUTES', L_key);
   fetch C_V_FM_ITEM_ATTRIBUTES into O_classification, O_classification_desc;
   SQL_LIB.SET_MARK('CLOSE', 'C_V_FM_ITEM_ATTRIBUTES', 'V_FM_ITEM_ATTRIBUTES', L_key);
   close C_V_FM_ITEM_ATTRIBUTES;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_CLASSIFICATION;
-----------------------------------------------------------------------------------
FUNCTION LOV_ORDER_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                      I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                      I_item           IN     ORDLOC.ITEM%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_ORDER_NO';
   L_status    VARCHAR2(1) := 'A';
   L_pwh       BOOLEAN;
   ---
BEGIN
   ---
   if I_location_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_query := 'select distinct(a.order_no) '||
                'from ordhead a,ordloc b '||
               'where a.status = '''||L_status||''' '||
                 'and b.order_no = a.order_no ';
   ---
   if I_item is NOT NULL then
      O_query := O_query ||
                 'and b.item = '''||I_item||''' ';
   end if;
   ---
   if I_location_type = 'S' or NOT L_pwh then
      O_query := O_query ||
                    'and b.loc_type = '''||I_location_type||''' '||
                    'and b.location = '||I_location_id||' ';
   else
      O_query := O_query ||
                    'and (b.location IN (select wh.wh from wh where wh.physical_wh = '||I_location_id||')) ';
   end if;

   O_query := O_query ||
                 'and ((a.supplier = '||I_key_value_1||') '||
                 ' or (a.delivery_supplier is NOT NULL '||
                 'and a.delivery_supplier =  '||I_key_value_1||')) '||
               'order by 1 desc';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ORDER_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_ORDER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_query          IN OUT VARCHAR2,
                        I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                        I_order_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                        I_location       IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                        I_loc_type       IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                        I_unexpected_Item IN    FM_FISCAL_DOC_DETAIL.UNEXPECTED_ITEM%TYPE,
                        I_Module         IN     FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                        I_Key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_ITEM_ORDER';
   L_pwh       BOOLEAN := FALSE;
   ---
BEGIN
   ---
   if I_loc_type = 'W' then
         if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                    L_pwh,
                                    I_location) = FALSE then
            return FALSE;
         end if;
   end if;
   if I_order_no is NOT NULL then
      if I_loc_type = 'S' or NOT L_pwh then
             O_query := 'select im.item_desc, im.item, NVL((NVL(ol.qty_ordered,0) - NVL(ol.qty_received,0)),0) quantity, ol.unit_cost , im.pack_ind , NULL pack_no '||
                         'from item_master im, ordloc ol '||
                        'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                          'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                          'and ol.order_no = '||I_order_no||' '||
                          'and ol.location = '||I_location||' '||
                          'and ol.loc_type = '''||I_loc_type||''' '||
                          'and ol.item     = im.item '||
                        'union all '||
                        'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, NVL((NVL(ol.qty_ordered,0) - NVL(ol.qty_received,0)),0) quantity, ol.unit_cost  , im.pack_ind , NULL pack_no '||
                         'from item_master im, tl_shadow tl_shadow, ordloc ol '||
                        'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                          'and UPPER(im.item) = tl_shadow.key(+) '||
                          'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                          'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                          'and ol.order_no = '||I_order_no||' '||
                          'and ol.location = '||I_location||' '||
                          'and ol.loc_type = '''||I_loc_type||''' '||
                          'and ol.item     = im.item '||
                        'union all '||
                        'select (select NVL(tl_shadow.translated_value, vim2.item_desc) item_desc '||
                                 'from item_master vim2, tl_shadow tl_shadow '||
                                'where vim2.item = viscl.item '||
                                  'and LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                                  'and UPPER(vim2.item) = tl_shadow.key(+)  '||
                                  'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+)) item_desc, '||
                               'viscl.item, '||
                               'NVL((NVL(vol.qty_ordered,0) - NVL(vol.qty_received,0)),0) * vpb.pack_item_qty quantity, '||
                               'ROUND((((viscl.negotiated_item_cost * vpb.pack_item_qty) / (tot_pack.unit_cost_pack)) * (vol.unit_cost)) / (vpb.pack_item_qty),4) unit_cost ,  '||
				' ''N'' pack_ind , '||
				' vpb.pack_no pack_no ' ||
                         'from ordhead voh, '||
                              'ordloc vol, '||
                              'item_master vim, '||
                              'packitem_breakout vpb, '||
                              'item_supp_country_loc viscl, '||
			      'v_br_item_fiscal_attrib vbifa, '||
                              '(select pb2.pack_no, '||
                                      'isc2.supplier, '||
                                      'isc2.origin_country_id, '||
                                      'isc2.loc, '||
                                      'SUM(pb2.pack_item_qty * isc2.negotiated_item_cost) unit_cost_pack, '||
                                      'SUM(pb2.pack_item_qty) tot_item_pack '||
                                 'from packitem_breakout pb2, '||
                                      'item_supp_country_loc isc2 '||
                                'where isc2.item = pb2.item '||
				                                  'group by pb2.pack_no,  '||
                                      'isc2.supplier, '||
                                      'isc2.origin_country_id, '||
                                      'isc2.loc) tot_pack '||
                        'where vol.order_no = '||I_order_no||' '||
                          'and vol.location = '||I_location||' '||
                          'and vol.loc_type = '''||I_loc_type||''' '||
                          'and vol.item = vim.item '||
                          'and vim.pack_ind = ''Y'' '||
                          'and exists (select ''1'' '||
                                        'from item_master vim3 '||
                                       'where vim3.item = viscl.item '||
                                         'and UPPER(vim3.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,vim3.item_desc))  ||  ''%'') '||
                          'and vol.location = tot_pack.loc '||
                          'and voh.supplier = tot_pack.supplier '||
                          'and vpb.pack_no = tot_pack.pack_no '||
                          'and vim.item = vpb.pack_no '||
                          'and voh.order_no = vol.order_no '||
                          'and voh.supplier = viscl.supplier '||
                          'and vpb.item = viscl.item '||
                          'and viscl.loc = vol.location '||
                          'and vol.item = tot_pack.pack_no '||
                          'and vol.location = tot_pack.loc '||
                          'and vbifa.item = vpb.item '||
                          'and vbifa.country_id = viscl.origin_country_id '||
			                       'and tot_pack.origin_country_id = viscl.origin_country_id '||
                     			  'order by 1';

      else
        O_query := 'select distinct im.item_desc, im.item, NVL((NVL(sum(ol.qty_ordered),0) - NVL(sum(ol.qty_received),0)),0) quantity, ol.unit_cost , im.pack_ind , NULL pack_no '||
                         'from item_master im, ordloc ol '||
                        'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                          'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                          'and ol.order_no = '||I_order_no||' '||
			                       'and ol.location IN (select wh.wh from wh where wh.physical_wh = '||I_location||') '||
                          'and ol.loc_type = '''||I_loc_type||''' '||
                          'and ol.item     = im.item '||
                          'group by  im.item_desc,im.item,ol.unit_cost , im.pack_ind '||
                        'union all '||
                        'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, NVL((NVL(sum(ol.qty_ordered),0) - NVL(sum(ol.qty_received),0)),0) quantity, ol.unit_cost  , im.pack_ind , NULL pack_no '||
                         'from item_master im, tl_shadow tl_shadow, ordloc ol '||
                        'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                          'and UPPER(im.item) = tl_shadow.key(+) '||
                          'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                          'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  || ''%'' '||
                          'and ol.order_no = '||I_order_no||' '||
		                        'and ol.location IN (select wh.wh from wh where wh.physical_wh = '||I_location||') '||
                          'and ol.loc_type = '''||I_loc_type||''' '||
                          'and ol.item     = im.item '||
                          'group by  tl_shadow.translated_value,im.item_desc,im.item,ol.unit_cost , im.pack_ind '||
                        'union all '||
                        'select distinct (select NVL(tl_shadow.translated_value, vim2.item_desc) item_desc '||
                                 'from item_master vim2, tl_shadow tl_shadow '||
                                'where vim2.item = viscl.item '||
                                  'and LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                                  'and UPPER(vim2.item) = tl_shadow.key(+)  '||
                                  'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+)) item_desc, '||
                               'viscl.item, '||
                                '(select NVL((NVL(sum(vol1.qty_ordered),0) - NVL(sum(vol1.qty_received),0)),0) from ordloc vol1 where vol1.order_no = '||I_order_no||' and vol1.location IN (select wh.wh from wh where wh.physical_wh = '||I_location||') and vol1.item = vol.item ) * vpb.pack_item_qty quantity, '||
                               'ROUND((((viscl.negotiated_item_cost * vpb.pack_item_qty) / (tot_pack.unit_cost_pack)) * (vol.unit_cost)) / (vpb.pack_item_qty),4) unit_cost ,  '||
                     				' ''N'' pack_ind , '||
				                     ' vpb.pack_no pack_no ' ||
                         'from ordhead voh, '||
                              'ordloc vol, '||
                              'item_master vim, '||
                              'packitem_breakout vpb, '||
                              'item_supp_country_loc viscl, '||
			                           'v_br_item_fiscal_attrib vbifa, '||
                              '(select pb2.pack_no, '||
                                      'isc2.supplier, '||
                                      'isc2.origin_country_id, '||
                                      'isc2.loc, '||
                                      'SUM(pb2.pack_item_qty * isc2.negotiated_item_cost) unit_cost_pack, '||
                                      'SUM(pb2.pack_item_qty) tot_item_pack '||
                                 'from packitem_breakout pb2, '||
                                      'item_supp_country_loc isc2 '||
                                'where isc2.item = pb2.item '||
                                'group by pb2.pack_no,  '||
                                      'isc2.supplier, '||
                                      'isc2.origin_country_id, '||
                                      'isc2.loc) tot_pack '||
                        'where vol.order_no = '||I_order_no||' '||
						                    'and vol.location IN (select wh.wh from wh where wh.physical_wh = '||I_location||') '||
                          'and vol.loc_type = '''||I_loc_type||''' '||
                          'and vol.item = vim.item '||
                          'and vim.pack_ind = ''Y'' '||
                          'and exists (select ''1'' '||
                                        'from item_master vim3 '||
                                       'where vim3.item = viscl.item '||
                                         'and UPPER(vim3.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,vim3.item_desc))  ||  ''%'') '||
                          'and vol.location = tot_pack.loc '||
                          'and voh.supplier = tot_pack.supplier '||
                          'and vpb.pack_no = tot_pack.pack_no '||
                          'and vim.item = vpb.pack_no '||
                          'and voh.order_no = vol.order_no '||
                          'and voh.supplier = viscl.supplier '||
                          'and vpb.item = viscl.item '||
                          'and viscl.loc = vol.location '||
                          'and vol.item = tot_pack.pack_no '||
                          'and vol.location = tot_pack.loc '||
                          'and vbifa.item = vpb.item '||
			                       'and vbifa.country_id = viscl.origin_country_id '||
                     			  'and tot_pack.origin_country_id = viscl.origin_country_id '||
                          'order by 1';
      end if;
      ---
   else
       If I_unexpected_Item = 'Y' and I_Module = 'SUPP' and ( I_loc_type = 'S' or NOT L_pwh ) then
         O_query := 'select distinct im.item_desc, im.item, NULL quantity, NULL unit_cost, im.pack_ind, NULL pack_no '||
                      'from item_master im , item_supp_country_loc isup  '||
                     'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                       'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                       'and im.item = isup.item '||
                       'and isup.loc = '||I_location||' '||
                       'and isup.supplier = '||I_Key_value_1||' '||
                       'and isup.origin_country_id in (select string_value from fm_system_options where variable = ''DEFAULT_COUNTRY'')  '||
                    'union all '||
                    'select distinct NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, NULL quantity, NULL unit_cost, im.pack_ind, NULL pack_no '||
                      'from item_master im, tl_shadow tl_shadow ,item_supp_country_loc isup  '||
                     'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                       'and UPPER(im.item) = tl_shadow.key(+) '||
                       'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                       'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                       'and im.item = isup.item '||
		           'and isup.loc = '||I_location||' '||
                       'and isup.supplier = '||I_Key_value_1||' '||
                       'and isup.origin_country_id in (select string_value from fm_system_options where variable = ''DEFAULT_COUNTRY'')  '||
                     'order by 1';
       elsif I_unexpected_Item = 'Y' and I_Module = 'SUPP' and  L_pwh  then
         O_query := 'select distinct im.item_desc, im.item, NULL quantity, NULL unit_cost, im.pack_ind, NULL pack_no '||
                      'from item_master im , item_supp_country_loc isup  '||
                     'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                       'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                       'and im.item = isup.item '||
		           'and isup.loc IN (select wh.wh from wh where wh.physical_wh = '||I_location||') '||
                       'and isup.supplier = '||I_Key_value_1||' '||
                       'and isup.origin_country_id in (select string_value from fm_system_options where variable = ''DEFAULT_COUNTRY'')  '||
                    'union all '||
                    'select distinct NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, NULL quantity, NULL unit_cost, im.pack_ind, NULL pack_no '||
                      'from item_master im, tl_shadow tl_shadow ,item_supp_country_loc isup  '||
                     'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                       'and UPPER(im.item) = tl_shadow.key(+) '||
                       'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                       'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                       'and im.item = isup.item '||
		           'and isup.loc IN (select wh.wh from wh where wh.physical_wh = '||I_location||') '||
                       'and isup.supplier = '||I_Key_value_1||' '||
                       'and isup.origin_country_id in (select string_value from fm_system_options where variable = ''DEFAULT_COUNTRY'')  '||
                     'order by 1';
       else
         O_query := 'select im.item_desc, im.item, NULL quantity, NULL unit_cost, im.pack_ind, NULL pack_no '||
                      'from item_master im '||
                     'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                       'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                     'union all '||
                    'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, NULL quantity, NULL unit_cost, im.pack_ind, NULL pack_no '||
                      'from item_master im, tl_shadow tl_shadow '||
                     'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                       'and UPPER(im.item) = tl_shadow.key(+) '||
                       'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                       'and UPPER(im.item_desc) like ''%'' ||  UPPER(NVL( ''' || I_item_desc || ''' ,im.item_desc))  ||  ''%'' '||
                     'order by 1';
       end if;

 end if;

 return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ITEM_ORDER;
-----------------------------------------------------------------------------------
FUNCTION LOV_ITEM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_query          IN OUT VARCHAR2,
                  I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_ITEM';
   L_pack_ind  VARCHAR2(1) := 'N';
   ---
BEGIN
   ---
   if I_item_desc is NOT NULL then
      O_query := 'select im.item_desc, im.item '||
                   'from item_master im '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and im.pack_ind = '''||L_pack_ind||''' '||
                    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                  'union all '||
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item '||
                   'from item_master im, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and im.pack_ind = '''||L_pack_ind||''' '||
                    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                  'order by 1';
   else
      O_query := 'select im.item_desc, im.item '||
                   'from item_master im '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and im.pack_ind = '''||L_pack_ind||''' '||
                  'union all '||
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item '||
                   'from item_master im, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and im.pack_ind = '''||L_pack_ind||''' '||
                  'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ITEM;
-----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where_clause       IN OUT VARCHAR2,
                          I_location_id        IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                          I_location_type      IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                          I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_requisition_no     IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                          I_item               IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE)
   return BOOLEAN is
   ---
   L_program           VARCHAR2(50) := 'FM_FISCAL_DETAIL_VAL_SQL.SET_WHERE_CLAUSE';
   ---
BEGIN
   ---
   O_where_clause := 'location_id = ' || TO_CHAR(I_location_id) || ' and ';
   O_where_clause := O_where_clause || 'location_type = ''' || I_location_type || ''' and ';
   O_where_clause := O_where_clause || 'fiscal_doc_id = ' || TO_CHAR(I_fiscal_doc_id) || ' and ';

   ---
   if I_requisition_no is NOT NULL then
      O_where_clause := O_where_clause || 'requisition_no = '|| I_requisition_no ||' and ';
   end if;
   ---
   if I_item is NOT NULL then
      O_where_clause := O_where_clause || 'item = '''|| I_item ||''' and ';
   end if;
   ---
   if O_where_clause is NOT NULL then
      O_where_clause := SUBSTR(O_where_clause, 1, NVL(LENGTH(O_where_clause), 0)-4);
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE;
-----------------------------------------------------------------------------------
FUNCTION GET_ITEM_ATTRIBUTES(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_classification      IN OUT V_BR_ITEM_FISCAL_ATTRIB.CLASSIFICATION_ID%TYPE,
                             O_origin_code         IN OUT V_BR_ITEM_FISCAL_ATTRIB.ORIGIN_CODE%TYPE,
                             O_service_ind         IN OUT V_BR_ITEM_FISCAL_ATTRIB.SERVICE_IND%TYPE,
                             O_embedded_item       IN OUT VARCHAR2,
                             I_item                IN     V_BR_ITEM_FISCAL_ATTRIB.ITEM%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_ITEM_ATTRIBUTES';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_V_FM_ITEM_ATTRIBUTES is
      select a.classification_id,
             a.origin_code,
             a.service_ind
       from v_br_item_country_attributes a
       where a.item = I_item;
   ---

BEGIN
   ---
   L_key := 'Item: '||I_item;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_V_FM_ITEM_ATTRIBUTES', 'V_FM_ITEM_ATTRIBUTES', L_key);
   open C_V_FM_ITEM_ATTRIBUTES;
   SQL_LIB.SET_MARK('FETCH', 'C_V_FM_ITEM_ATTRIBUTES', 'V_FM_ITEM_ATTRIBUTES', L_key);
   fetch C_V_FM_ITEM_ATTRIBUTES into O_classification,
                                     O_origin_code,
                                     O_service_ind;
   SQL_LIB.SET_MARK('CLOSE', 'C_V_FM_ITEM_ATTRIBUTES', 'V_FM_ITEM_ATTRIBUTES', L_key);
   close C_V_FM_ITEM_ATTRIBUTES;
   ---

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_ATTRIBUTES;
-----------------------------------------------------------------------------------
FUNCTION LOV_TSF_NO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_query           IN OUT VARCHAR2,
                    I_location_id     IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type   IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                    I_doc_fiscal_type IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                    I_key_value_1     IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)

   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_TSF_NO';
   L_pwh       BOOLEAN;
   ---
BEGIN
   ---

O_query := 'select a.tsf_no tsf_no '||
             'from tsfhead a, tsfhead b '||
            'where b.from_loc_type = ''E'' '||
              'and b.from_loc = '||I_key_value_1||' '||
              'and b.to_loc_type = '''||I_location_type||''' '||
              'and (b.to_loc = '||I_location_id||' '||
              'or (b.to_loc IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id|| ')))'||
              'and a.tsf_no = b.tsf_parent_no ' ||
              'and a.status in (''S'',''C'') '||
              'union all '||
              'select b.tsf_no tsf_no '||
              'from tsfhead a, tsfhead b '||
              'where a.to_loc_type = ''E'' '||
              'and a.to_loc ='|| I_key_value_1||' '||
              'and a.tsf_no = b.tsf_parent_no ' ||
              'and b.status in (''S'',''A'') '||
              'and a.status in (''S'',''C'') ';

return TRUE;
      ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_TSF_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_EDI_NO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_query           IN OUT VARCHAR2,
                    I_location_id     IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type   IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                    I_doc_fiscal_type IN     VARCHAR2,
                    I_fiscal_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_EDI_NO';
   L_key       VARCHAR2(100);
   L_key_value_1   fm_fiscal_doc_header.key_value_1%type;
   L_key_value_2   fm_fiscal_doc_header.key_value_2%type;
   ---
   cursor C_GET_KEY_VAL is
      select key_value_1,key_value_2
        from fm_fiscal_doc_header
       where fiscal_doc_id = I_fiscal_doc_id;
   ---
BEGIN
   ---
   if (I_doc_fiscal_type = 'REP') then
      ---
      ---
      L_key := 'Fiscal_doc_id '||I_fiscal_doc_id;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_KEY_VAL', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_GET_KEY_VAL;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_KEY_VAL', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_GET_KEY_VAL into L_key_value_1,L_key_value_2;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_KEY_VAL', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_GET_KEY_VAL;
      ---
      O_query := 'select distinct th.tsf_no tsf_no '||
                   'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_utilization_attributes a,  '||
                        'fm_fiscal_doc_header fdh1, fm_schedule fs1,tsfhead th '||
                  'where fdh.module = ''PTNR'' '||
                    'and fdh.module = fdh1.module '||
                    'and fdh.key_value_2 = '''||L_key_value_2||''' '||
                    'and fdh.key_value_1 = '''||L_key_value_1||''' '||
                    'and fdh.requisition_type = ''REP'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = th.tsf_parent_no '||
                    'and  th.context_type = ''REP'' '||
                    'and th.from_loc =  fdh1.key_value_1 '||
                    'and th.from_loc_type  =  fdh1.key_value_2 '||
                    'and fdh.utilization_id   = a.utilization_id '||
                    --'and fdh1.fiscal_doc_id = '|| I_fiscal_doc_id ||' '||
                    'and ((fdh.requisition_type = ''REP'' '||
                      'and fs1.mode_type = ''ENT'' '||
                      'and fdh.key_value_1 <> fdh1.key_value_1 '||
                      'and fdh.location_id = fdh1.key_value_1 '||
                      'and fdh.key_value_2 = fdh1.key_value_2) '||
                      'or ((fdh.requisition_type <> ''REP'' '||
                       'or fs1.mode_type <> ''ENT'') '||
                      'and fdh.key_value_1 = fdh1.key_value_1 '||
                      'and fdh.key_value_2 = fdh1.key_value_2)) '||
                    'and fs1.schedule_no    = fdh1.schedule_no '||
                    'and NOT EXISTS (select 1 '||
                                      'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_schedule fs '||
                                     'where fdh.fiscal_doc_id = fdd.fiscal_doc_id '||
                                       'and fs.schedule_no = fdh.schedule_no '||
                                       'and fs.status = ''A'' '||
                                       'and fdh.status NOT IN (''I'',''D'') '||
                                       'and fdd.requisition_no = fdd.requisition_no '||
                                       'and fdh.requisition_type = fdh.requisition_type '||
                                       'and fs1.mode_type        = fs.mode_type '||
                                       'and fdh.fiscal_doc_id    <> fdh1.fiscal_doc_id '||
                                       'and fdd.item = fdd.item) '||
                  'order by 1';
   ---
   else
   ---
      O_query := 'select distinct fdd.requisition_no rtv_order_no '||
                   'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_utilization_attributes a,  '||
   --- 003 - Begin
                        'fm_fiscal_doc_header fdh1, fm_schedule fs1 '||
   --- 003 - End
                  'where ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = '''||I_doc_fiscal_type||''' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.utilization_id   = a.utilization_id '||
   --- 003 - Begin
                    'and fdh1.fiscal_doc_id = fdh.fiscal_doc_id ' ||
                    'and fdh1.fiscal_doc_id = '|| I_fiscal_doc_id ||' '||
                    'and ((fdh.requisition_type IN (''TSF'',''IC'') '||
                      'and fs1.mode_type = ''ENT'' '||
                      'and fdh.key_value_1 <> fdh1.key_value_1 '||
                      'and fdh.location_id = fdh1.key_value_1 '||
                      'and fdh.key_value_2 = fdh1.key_value_2) '||
                     'or ((fdh.requisition_type NOT IN (''TSF'',''IC'') '||
                       'or fs1.mode_type <> ''ENT'') '||
                      'and fdh.key_value_1 = fdh1.key_value_1 '||
                      'and fdh.key_value_2 = fdh1.key_value_2)) '||
                    'and fs1.schedule_no    = fdh1.schedule_no '||
                    'and NOT EXISTS (select 1 '||
                                      'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd1, fm_schedule fs '||
                                     'where fdh.fiscal_doc_id = fdd.fiscal_doc_id '||
                                       'and fs.schedule_no = fdh.schedule_no '||
                                       'and fs.status = ''A'' '||
                                       'and fdh.status NOT IN (''I'',''D'') '||
                                       'and fdd.requisition_no = fdd1.requisition_no '||
                                       'and fdh.requisition_type = fdh.requisition_type '||
                                       'and fs1.mode_type        = fs.mode_type '||
                                       'and fdh.fiscal_doc_id    <> fdh1.fiscal_doc_id '||
                                       'and fdd.item = fdd.item) '||
   --- 003 - End
                  'order by 1';
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
--- 003 - Begin
END LOV_EDI_NO;
---END LOV_RTV_NO;
--- 003 - End
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_ORDER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists         IN OUT BOOLEAN,
                             O_unique         IN OUT BOOLEAN,
                             I_order_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                             I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                             I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                             I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_SQL.VALIDATE_ITEM_ORDER';
   L_key       VARCHAR2(200);
   L_count     NUMBER;
   L_pwh       BOOLEAN := FALSE;
   ---
  -- code commented for code drop 1

   cursor C_EXISTS_ITEM_PACK_S is
      select count(*)
        from (select 'x'
                from item_master im, ordloc ol
               where ol.order_no = I_order_no
	               and ol.location = I_location_id
                 and ol.loc_type = I_location_type
                 and ol.item     = im.item
                 and im.item     = I_item
              union all
              select 'x'
                from ordloc ol, item_master im
               where ol.order_no     = I_order_no
                 and ol.location     = I_location_id
                 and ol.loc_type     = I_location_type
                 and ol.item         = im.item
                 and im.item_level   = im.tran_level
                 and (im.item_parent = I_item
                  or im.item_grandparent = I_item)
              union all
              select 'x'
                from ordloc ol, item_master im, packitem_breakout pi
               where ol.order_no = I_order_no
                 and ol.location = I_location_id
                 and ol.loc_type = I_location_type
	               and pi.pack_no = ol.item
                 and im.item = pi.item
                and im.item = I_item) a;
   ---
   cursor C_EXISTS_ITEM_PACK_WH is
      select count(*)
        from (select 'x'
                from item_master im, ordloc ol
               where ol.order_no = I_order_no
	               and ol.location IN (select wh.wh from wh where wh.physical_wh = I_location_id)
                 and ol.loc_type = I_location_type
                 and ol.item     = im.item
                 and im.item     = I_item
              union all
              select 'x'
                from ordloc ol, item_master im
               where ol.order_no     = I_order_no
                 and ol.location     IN (select wh.wh from wh where wh.physical_wh = I_location_id)
                 and ol.loc_type     = I_location_type
                 and ol.item         = im.item
                 and im.item_level   = im.tran_level
                 and (im.item_parent = I_item
                  or im.item_grandparent = I_item)
              union all
              select 'x'
                from ordloc ol, item_master im, packitem_breakout pi
               where ol.order_no = I_order_no
                 and ol.location IN (select wh.wh from wh where wh.physical_wh = I_location_id)
                 and ol.loc_type = I_location_type
	               and pi.pack_no = ol.item
                 and im.item = pi.item
                 and im.item = I_item) a;
 ---
BEGIN
   ---
   if FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_ORDLOC(O_error_message,
                                             O_exists,
                                             I_order_no,
                                             I_item,
                                             I_location_id,
                                             I_location_type) = FALSE then
      return FALSE;
   end if;
   ---
   if I_location_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---

   if I_location_type = 'S' or NOT L_pwh then
      ---
      open C_EXISTS_ITEM_PACK_S;
      ---
      fetch C_EXISTS_ITEM_PACK_S into L_count;
      if L_count is NOT NULL and L_count = 0 then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      ---
      close C_EXISTS_ITEM_PACK_S;
      if L_count is NOT NULL and L_count > 1 then
         O_unique := FALSE;
         return TRUE;
      end if;

   else
      ---
      open C_EXISTS_ITEM_PACK_WH;
      ---
      fetch C_EXISTS_ITEM_PACK_WH into L_count;
      if L_count is NOT NULL and L_count = 0 then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      ---
      close C_EXISTS_ITEM_PACK_WH;
      if L_count is NOT NULL and L_count > 1 then
         O_unique := FALSE;
         return TRUE;
      end if;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_EXISTS_ITEM_PACK_S %ISOPEN then
         close C_EXISTS_ITEM_PACK_S;
      end if;
      ---
      if C_EXISTS_ITEM_PACK_WH %ISOPEN then
         close C_EXISTS_ITEM_PACK_WH;
      end if;
      ---
      return FALSE;
END VALIDATE_ITEM_ORDER;
-----------------------------------------------------------------------------------
FUNCTION LOV_PERC_VALUE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_PERC_VALUE';
   ---
BEGIN
   ---
   O_query := 'select code_desc name, code '||
                'from code_detail '||
               'where code_type = ''FMVP'' '||
               'order by code_seq';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_PERC_VALUE;
-----------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO_REF_PO(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_query              IN OUT VARCHAR2,
                                  I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                  I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                  I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                  I_fiscal_doc_line_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                  I_complementary_ind  IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                                  I_main_item          IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_FISCAL_DOC_NO_REF_PO';
   ---
BEGIN
   ---
   O_query := 'select fdh.fiscal_doc_id, NVL(fdh.fiscal_doc_no,0) fiscal_doc_no, fdh.series_no, cd1.code_desc module, cd2.code_desc key_value_2, fdh.key_value_1, fdh.issue_date, fdd.item, fdd.quantity, fdd.unit_cost, fdd.fiscal_doc_line_id '||
                'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,
                fm_fiscal_utilization fu, code_detail cd1, code_detail cd2 '||
               'where fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                 'and fu.utilization_id = fdh.utilization_id '||
                 'and cd1.code_type = ''FIMO'' '||
                 'and cd1.code = fdh.module '||
                 'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'') '||
                 'and cd2.code (+) = fdh.key_value_2 '||
                 'and fdh.status NOT IN (''I'',''D'') '||
                 'and fdh.location_id = '|| I_location ||' '||
                 'and fdh.location_type = '''|| I_loc_type ||''' ';
   ---
   if I_fiscal_doc_line_id is NOT NULL then
      O_query := O_query ||
                 'and fdd.fiscal_doc_line_id <> '||TO_CHAR(I_fiscal_doc_line_id)||' ';
   end if;
   ---
   if I_fiscal_doc_no is NOT NULL then
      O_query := O_query ||
                 'and NVL(fdh.fiscal_doc_no,0) = '||I_fiscal_doc_no||' ';
   end if;
   O_query := O_query ||
               'order by 2';
   ---

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_FISCAL_DOC_NO_REF_PO;
-----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                      I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_ITEM_TSF';
   ---
BEGIN
   ---
   if I_item_desc is NOT NULL then
      O_query := 'select im.item_desc, fdd.item, fdd.quantity, fdd.unit_cost unit_cost, fdd.pack_ind , fdd.pack_no pack_no '||
                   'from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd ,tsfhead th, tsfhead th2 '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
		    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and fdh.module in (''PTNR'',''LOC'') '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_key_value_1)||' '||
                    'and fdh.key_value_1 = th.to_loc '||
                    'and th.tsf_no = th2.tsf_parent_no '||
                    'and fdh.requisition_type IN (''TSF'',''IC'',''REP'') '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.status in (''A'',''C'',''H'') '||
                    'and fdd.requisition_no = th.tsf_no '||
                    'and (th.tsf_no = '||I_tsf_no||' '||
                     'or th2.tsf_no = '||I_tsf_no||') '||
                    'and im.item = fdd.item '||
		    'and not exists (select 1 from tsf_xform where tsf_no = '||I_tsf_no||' or tsf_no = th2.tsf_parent_no ) '||
                  'union all '||
		  'select im.item_desc, im.item, fdd.quantity, fdd.unit_cost unit_cost, fdd.pack_ind , fdd.pack_no pack_no '||
                   'from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd ,tsfhead th, tsfhead th2, tsf_xform txh, tsf_xform_detail txd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
		    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and fdh.module in (''PTNR'',''LOC'') '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_key_value_1)||' '||
                    'and fdh.key_value_1 = th.to_loc '||
                    'and th.tsf_no = th2.tsf_parent_no '||
                    'and fdh.requisition_type IN (''TSF'',''IC'',''REP'') '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.status in (''A'',''C'',''H'') '||
                    'and fdd.requisition_no = th.tsf_no '||
                    'and (th.tsf_no = '||I_tsf_no||' '||
                     'or th2.tsf_no = '||I_tsf_no||') '||
                    'and txh.tsf_no = th.tsf_no '||
		    'and txh.tsf_xform_id = txd.tsf_xform_id '||
		    'and txd.from_item = fdd.item '||
		    'and txd.to_item = im.item '||
		    'union all '||
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, fdd.item, fdd.quantity, fdd.unit_cost unit_cost, fdd.pack_ind , fdd.pack_no pack_no '||
                   'from item_master im, tl_shadow tl_shadow, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,tsfhead th, tsfhead th2 '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
		    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and fdh.module in (''PTNR'',''LOC'') '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_key_value_1)||' '||
                    'and fdh.key_value_1 = th.to_loc '||
                    'and th.tsf_no = th2.tsf_parent_no '||
                    'and fdh.requisition_type IN (''TSF'',''IC'',''REP'') '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.status in (''A'',''C'',''H'') '||
                    'and fdd.requisition_no = th.tsf_no '||
                    'and (th.tsf_no = '||I_tsf_no||' '||
                     'or th2.tsf_no = '||I_tsf_no||') '||
                    'and im.item = fdd.item '||
		    'and not exists (select 1 from tsf_xform where tsf_no = '||I_tsf_no||' or tsf_no = th2.tsf_parent_no ) '||
		    'union all '||
		  'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, fdd.quantity, fdd.unit_cost unit_cost, fdd.pack_ind , fdd.pack_no pack_no '||
                   'from item_master im, tl_shadow tl_shadow, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,tsfhead th, tsfhead th2,  tsf_xform txh, tsf_xform_detail txd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
		    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and fdh.module in (''PTNR'',''LOC'') '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_key_value_1)||' '||
                    'and fdh.key_value_1 = th.to_loc '||
                    'and th.tsf_no = th2.tsf_parent_no '||
                    'and fdh.requisition_type IN (''TSF'',''IC'',''REP'') '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.status in (''A'',''C'',''H'') '||
                    'and fdd.requisition_no = th.tsf_no '||
                    'and (th.tsf_no = '||I_tsf_no||' '||
                     'or th2.tsf_no = '||I_tsf_no||') '||
                    'and im.item = fdd.item '||
                    'and txh.tsf_no = th.tsf_no '||
		    'and txh.tsf_xform_id = txd.tsf_xform_id '||
		    'and txd.from_item = fdd.item '||
		    'and txd.to_item = im.item '||
                  'order by 1';
   else
      O_query := 'select im.item_desc, fdd.item, fdd.quantity, fdd.unit_cost unit_cost, fdd.pack_ind , fdd.pack_no pack_no '||
                   'from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd ,tsfhead th, tsfhead th2 '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and fdh.module in (''PTNR'',''LOC'') '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_key_value_1)||' '||
                    'and fdh.key_value_1 = th.to_loc '||
                    'and th.tsf_no = th2.tsf_parent_no '||
                    'and fdh.requisition_type IN (''TSF'',''IC'',''REP'') '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.status in (''A'',''C'',''H'') '||
                    'and fdd.requisition_no = th.tsf_no '||
                    'and (th.tsf_no = '||I_tsf_no||' '||
                     'or th2.tsf_no = '||I_tsf_no||') '||
                    'and im.item = fdd.item '||
		    'and not exists (select 1 from tsf_xform where tsf_no = '||I_tsf_no||' or tsf_no = th2.tsf_parent_no ) '||
                  'union all '||
		  'select im.item_desc, im.item, fdd.quantity, fdd.unit_cost unit_cost, fdd.pack_ind , fdd.pack_no pack_no '||
                   'from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd ,tsfhead th, tsfhead th2, tsf_xform txh, tsf_xform_detail txd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and fdh.module in (''PTNR'',''LOC'') '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_key_value_1)||' '||
                    'and fdh.key_value_1 = th.to_loc '||
                    'and th.tsf_no = th2.tsf_parent_no '||
                    'and fdh.requisition_type IN (''TSF'',''IC'',''REP'') '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.status in (''A'',''C'',''H'') '||
                    'and fdd.requisition_no = th.tsf_no '||
                    'and (th.tsf_no = '||I_tsf_no||' '||
                     'or th2.tsf_no = '||I_tsf_no||') '||
                    'and txh.tsf_no = th.tsf_no '||
		    'and txh.tsf_xform_id = txd.tsf_xform_id '||
		    'and txd.from_item = fdd.item '||
		    'and txd.to_item = im.item '||
		    'union all '||
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, fdd.item, fdd.quantity, fdd.unit_cost unit_cost, fdd.pack_ind , fdd.pack_no pack_no '||
                   'from item_master im, tl_shadow tl_shadow, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,tsfhead th, tsfhead th2 '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and fdh.module in (''PTNR'',''LOC'') '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_key_value_1)||' '||
                    'and fdh.key_value_1 = th.to_loc '||
                    'and th.tsf_no = th2.tsf_parent_no '||
                    'and fdh.requisition_type IN (''TSF'',''IC'',''REP'') '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.status in (''A'',''C'',''H'') '||
                    'and fdd.requisition_no = th.tsf_no '||
                    'and (th.tsf_no = '||I_tsf_no||' '||
                     'or th2.tsf_no = '||I_tsf_no||') '||
                    'and im.item = fdd.item '||
		    'and not exists (select 1 from tsf_xform where tsf_no = '||I_tsf_no||' or tsf_no = th2.tsf_parent_no ) '||
		    'union all '||
		  'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, fdd.quantity, fdd.unit_cost unit_cost, fdd.pack_ind , fdd.pack_no pack_no '||
                   'from item_master im, tl_shadow tl_shadow, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,tsfhead th, tsfhead th2,  tsf_xform txh, tsf_xform_detail txd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and fdh.module in (''PTNR'',''LOC'') '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_key_value_1)||' '||
                    'and fdh.key_value_1 = th.to_loc '||
                    'and th.tsf_no = th2.tsf_parent_no '||
                    'and fdh.requisition_type IN (''TSF'',''IC'',''REP'') '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdh.status in (''A'',''C'',''H'') '||
                    'and fdd.requisition_no = th.tsf_no '||
                    'and (th.tsf_no = '||I_tsf_no||' '||
                     'or th2.tsf_no = '||I_tsf_no||') '||
                    'and im.item = fdd.item '||
                    'and txh.tsf_no = th.tsf_no '||
		    'and txh.tsf_xform_id = txd.tsf_xform_id '||
		    'and txd.from_item = fdd.item '||
		    'and txd.to_item = im.item '||
                  'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ITEM_TSF;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           O_unique         IN OUT BOOLEAN,
                           I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                           I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_ITEM_TSF';
   L_key       VARCHAR2(200);
   L_dummy     VARCHAR2(1);
   L_count     NUMBER;
   ---
   cursor C_EXISTS_ITEM is
      select count(*)
        from (select 'x'
                from item_master im,
                     fm_fiscal_doc_header fdh,
                     fm_fiscal_doc_detail fdd,
                     tsfhead th,
                     tsfhead th2
               where fdh.module in ('PTNR','LOC')
                 and fdh.key_value_1 = TO_CHAR(I_key_value_1)
                 and fdh.key_value_1 = th.to_loc
                 and fdh.status in ('A','C','FP','H')
                 and th.tsf_no = th2.tsf_parent_no
                 and fdh.requisition_type in ('TSF','IC','REP')
                 and fdd.fiscal_doc_id = fdh.fiscal_doc_id
                 and fdd.requisition_no = th.tsf_no
                 and (th.tsf_no = I_tsf_no
                  or th2.tsf_no = I_tsf_no)
                 and im.item = fdd.item
                 and fdd.item = I_item
	               and not exists (select 1
                                   from tsf_xform
                                  where tsf_no = I_tsf_no
                                     or tsf_no in (select tsf_parent_no from tsfhead where tsf_no = I_tsf_no))
	            union all
	            select 'x'
                from fm_fiscal_doc_header fdh,
                     fm_fiscal_doc_detail fdd,
                     tsfhead th, tsfhead th2,
                     tsf_xform txh,
                     tsf_xform_detail txd
               where fdh.module in ('PTNR','LOC')
                 and fdh.key_value_1 = TO_CHAR(I_key_value_1)
                 and fdh.key_value_1 = th.to_loc
                 and th.tsf_no = th2.tsf_parent_no
                 and fdh.requisition_type IN ('TSF','IC','REP')
                 and fdd.fiscal_doc_id = fdh.fiscal_doc_id
                 and fdh.status in ('A','C','FP','H')
                 and fdd.requisition_no = th.tsf_no
                 and (th.tsf_no = I_tsf_no
                  or th2.tsf_no = I_tsf_no)
                 and txh.tsf_no = th.tsf_no
                 and txh.tsf_xform_id = txd.tsf_xform_id
                 and txd.from_item = fdd.item
		             and txd.to_item = I_item) a;
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_tsf_no||
            ' key_value_1 = '''||I_key_value_1||''''||
            ' item = '||I_item;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_ITEM', 'ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   open C_EXISTS_ITEM;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_ITEM', 'ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_EXISTS_ITEM into L_count;
   if L_count is NOT NULL and L_count = 0 then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_ITEM', 'ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   close C_EXISTS_ITEM;
   if L_count is NOT NULL and L_count > 1 then
      O_unique := FALSE;
      return TRUE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ITEM_TSF;
-----------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO_REF(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_query              IN OUT VARCHAR2,
                               I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                               I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                               I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                               I_item               IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                               I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                               I_supplier           IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_FISCAL_DOC_NO_REF';
   L_req_type  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   ---
BEGIN
   ---
   O_query := 'select fdh.fiscal_doc_id, fdh.fiscal_doc_no, fdh.series_no, cd1.code_desc module, cd2.code_desc key_value_2, fdh.key_value_1, fdh.issue_date,fdd.quantity,fdd.unit_cost_with_disc unit_cost,fdh.entry_or_exit_date receipt_date '||
      ' from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,fm_fiscal_utilization fu, code_detail cd1, code_detail cd2, system_options so, '||
            'fm_fiscal_doc_header fdh1, fm_schedule fs '||
     'where fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
       'and fdd.item = '||I_item||' '||
       'and fu.utilization_id = fdh.utilization_id '||
       'and fdh.status IN (''A'',''C'') '||
       'and cd1.code_type = ''FIMO'' '||
       'and cd1.code = fdh.module '||
       'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'') '||
       'and cd2.code (+) = fdh.key_value_2 '||
       'and fdh.status NOT IN (''I'',''D'') '||
       'and fdh1.location_id = '||I_location||' '||
       'and fdh1.location_type = '''|| I_loc_type ||''' '||
       'and fdh1.key_value_1   = '''|| I_supplier ||''' '||
       'and fdh1.location_id = fdh.location_id ' ||
       'and fdh1.location_type = fdh.location_type ' ||
       'and fdh1.key_value_1 = fdh.key_value_1 ' ||
       'and ((so.multichannel_ind = ''Y'' '||
       'and fdh.location_type = ''W'' '||
       'and exists (select 1 '||
       '              from v_sec_wh vwhm '||
       '             where vwhm.wh = fdh.location_id)) '||
       'or (so.multichannel_ind = ''N'' '||
       'and fdh.location_type = ''W'' '||
       'and exists (select 1 '||
       '              from v_wh vwh '||
       '             where vwh.wh = fdh.location_id)) '||
       'or (fdh.location_type = ''S'' ' ||
       'and exists (select 1 '||
       '              from v_store vst '||
       '             where vst.store = fdh.location_id))) '||
       'and fdh1.fiscal_doc_id = '|| I_fiscal_doc_id ||' '||
       'and fdh.fiscal_doc_id  <> fdh1.fiscal_doc_id '||
       'and fs.schedule_no     =  fdh.schedule_no '||
       'and fs.status          IN (''A'',''C'') '||
         'and ((fdh1.requisition_type <> ''RMA'' '||
         'and fdh.requisition_type = ''PO'' '||
         'and fs.mode_type = ''ENT'') '||
         'or (fdh1.requisition_type = ''RMA'' '||
         'and fdh.requisition_type = ''OM'' '||
         'and fs.mode_type = ''EXIT'')) '||
         'union ' ||
         'select fdh.fiscal_doc_id, fdh.fiscal_doc_no, fdh.series_no, cd1.code_desc module, cd2.code_desc key_value_2, fdh.key_value_1, fdh.issue_date,fdd.quantity,fdd.unit_cost_with_disc unit_cost,fdh.entry_or_exit_date receipt_date '||
     'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,fm_fiscal_utilization fu, code_detail cd1, code_detail cd2,fm_schedule fs  ' ||
    'where fdd.pack_no = '||I_item||' '||
      'and fdd.pack_ind=''N'' ' ||
      'and cd1.code_type = ''FIMO'' '||
       'and cd1.code = fdh.module '||
      'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'') '||
       'and cd2.code (+) = fdh.key_value_2 '||
       'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
        'and fu.utilization_id = fdh.utilization_id ' ||
        'and fdh.requisition_type = ''PO'' '||
        'and fs.schedule_no     =  fdh.schedule_no ' ||
         'and fs.status         IN (''A'',''C'') ' ;


   O_query := O_query ||
               'order by 2 desc';
   ---

  return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_FISCAL_DOC_NO_REF;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_FISCAL_DOC_NO_REF(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists                  IN OUT BOOLEAN,
                                    O_fiscal_doc_line_id_ref  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                    O_fiscal_doc_id_ref       IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                    I_fiscal_doc_no           IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                    I_location                IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                    I_loc_type                IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                    I_supplier                IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                    I_item                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                                    I_fiscal_doc_line_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                    I_fiscal_doc_id           IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_FISCAL_DOC_NO_REF';
   ---
   L_key       VARCHAR2(100);
   L_count     NUMBER;
   L_fiscal_doc_line_id_ref FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID_REF%TYPE;
   L_fiscal_doc_id_ref      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID_REF%TYPE;
   L_default_wh_val    STORE.DEFAULT_WH%TYPE;
   L_req_type  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_ph_wh_val         WH.PHYSICAL_WH%TYPE;
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select  fdd.fiscal_doc_line_id, fdd.fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_fiscal_utilization fu,
             fm_fiscal_doc_header fdh1, fm_schedule fs
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.item = I_item
         and ((I_fiscal_doc_line_id is NOT NULL
         and fdd.fiscal_doc_line_id <> I_fiscal_doc_line_id)
          or (I_fiscal_doc_line_id is NULL))
         and fu.utilization_id = fdh.utilization_id
         and ((O_fiscal_doc_line_id_ref is NOT NULL
         and fdd.fiscal_doc_line_id = O_fiscal_doc_line_id_ref)
          or (O_fiscal_doc_line_id_ref is NULL))
         and NVL(fdh.fiscal_doc_no,0) = I_fiscal_doc_no
         and fdh1.location_id = I_location
         and fdh1.location_type = I_loc_type
         and fdh1.key_value_1   = I_supplier
         and fdh1.fiscal_doc_id = I_fiscal_doc_id
         and fdh.fiscal_doc_id  <> fdh1.fiscal_doc_id
         and fs.schedule_no     =  fdh.schedule_no
         and fs.status         in ('A','C')
         and ((fdh1.requisition_type <> 'RMA'
           and fdh.requisition_type = 'PO'
           and fs.mode_type = 'ENT')
           or (fdh1.requisition_type = 'RMA'
           and fdh.requisition_type = 'OM'
           and fs.mode_type = 'EXIT'))
         and fdh.status NOT IN ('I','D')
     union
    select fdh.fiscal_doc_id,fdd.fiscal_doc_line_id
      from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,fm_fiscal_utilization fu,fm_schedule fs
      where fdd.pack_no = I_item
       and fdd.fiscal_doc_id = fdh.fiscal_doc_id
       and fdd.pack_ind='N'
        and fu.utilization_id = fdh.utilization_id
        and fdh.requisition_type = 'PO'
        and fdh.schedule_no = fs.schedule_no
        and fs.status IN ('A','C');
   ---
   cursor C_COUNT_FISCAL_DOC_ID is
      select COUNT(*)
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_fiscal_utilization fu,
--- 003 - Begin
             fm_fiscal_doc_header fdh1, fm_schedule fs
--- 003 - End
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.item = I_item
         and ((I_fiscal_doc_line_id is NOT NULL
         and fdd.fiscal_doc_line_id <> I_fiscal_doc_line_id)
          or (I_fiscal_doc_line_id is NULL))
         and fu.utilization_id = fdh.utilization_id
         and ((O_fiscal_doc_line_id_ref is NOT NULL
         and fdd.fiscal_doc_line_id = O_fiscal_doc_line_id_ref)
          or (O_fiscal_doc_line_id_ref is NULL))
         and NVL(fdh.fiscal_doc_no,0) = I_fiscal_doc_no
--- 002 - Begin
---         and fdh.location_id = I_location
---         and fdh.location_type = I_loc_type
--- 002 - End
--- 003 - Begin
         and fdh1.location_id = I_location
         and fdh1.location_type = I_loc_type
         and fdh1.fiscal_doc_id = I_fiscal_doc_id
         and fdh.fiscal_doc_id  <> fdh1.fiscal_doc_id
         and fs.schedule_no     =  fdh.schedule_no
         and fs.status          IN ( 'A','C')
         and ((fdh1.requisition_type <> 'RMA'
           and fdh.requisition_type = 'PO'
           and fs.mode_type = 'ENT')
           or (fdh1.requisition_type = 'RMA'
           and fdh.requisition_type = 'OM'
           and fs.mode_type = 'EXIT'))--- 003 - End
         and fdh.status NOT IN ('I','D');
   ---
    Cursor C_GET_REQ_TYPE is
   Select requisition_type from fm_fiscal_doc_header
   where fiscal_doc_id = I_fiscal_doc_id;

   ---

   Cursor C_DEF_WH_VAL is
    select default_wh
      from store
      where store = I_location;
  ---
  Cursor C_DEF_PH_WH_VAL is
    select physical_wh
     from wh
     where wh in (select default_wh
                  from store
                  where store = I_location);
  ---
     cursor C_FM_FISCAL_DOC_HEADER_DEF_WH(L_DEF_WH  STORE.DEFAULT_WH%TYPE) is
      select distinct fdd.fiscal_doc_line_id, fdd.fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,  fm_fiscal_utilization fu,
             fm_fiscal_doc_header fdh1, fm_schedule fs
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.item = I_item
         and ((I_fiscal_doc_line_id is NOT NULL
         and fdd.fiscal_doc_line_id <> I_fiscal_doc_line_id)
          or (I_fiscal_doc_line_id is NULL))
         and fu.utilization_id = fdh.utilization_id
         and ((O_fiscal_doc_line_id_ref is NOT NULL
         and fdd.fiscal_doc_line_id = O_fiscal_doc_line_id_ref)
          or (O_fiscal_doc_line_id_ref is NULL))
         and NVL(fdh.fiscal_doc_no,0) = I_fiscal_doc_no
         and fdh1.location_id = L_DEF_WH
         --and fdh1.location_type = I_loc_type
         --and fdh1.fiscal_doc_id = I_fiscal_doc_id
         and fdh.fiscal_doc_id  <> fdh1.fiscal_doc_id
         and fs.schedule_no     =  fdh.schedule_no
         and fs.status          = 'A'
         and fdh.requisition_type = 'PO'
         and fs.mode_type = 'ENT'
         and fdh.status NOT IN ('I','D');

 ---
   cursor C_FM_FISCAL_DOC_HEADER_REP is
      select fdd.fiscal_doc_line_id, fdd.fiscal_doc_id
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_schedule fs,
             fm_fiscal_utilization fu,
             fm_utilization_attributes fa
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.item = I_item
         and ((I_fiscal_doc_line_id is NOT NULL
         and fdd.fiscal_doc_line_id <> I_fiscal_doc_line_id)
          or (I_fiscal_doc_line_id is NULL))
         and fu.utilization_id = fdh.utilization_id
         and fa.utilization_id = fu.utilization_id
         and ((O_fiscal_doc_line_id_ref is NOT NULL
         and fdd.fiscal_doc_line_id = O_fiscal_doc_line_id_ref)
          or (O_fiscal_doc_line_id_ref is NULL))
         and fdh.location_id = I_location
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D')
         and fs.schedule_no = fdh.schedule_no
         and fdh.status            != 'I'
         and fdh.requisition_type   = 'REP'
         and fs.status              = 'A'
         and fs.mode_type           <> 'ENT'
         and fa.comp_nf_ind         = 'N'
         and fu.status              = 'A'
       order by fdh.entry_or_exit_date desc,
                fdh.fiscal_doc_id desc,
                fdd.fiscal_doc_line_id desc;
 ---

BEGIN
   L_key := 'fiscal_doc_no = '||I_fiscal_doc_no ||
           ' location_id = '||TO_CHAR(I_location) ||
           ' location_type = '''||I_loc_type||''''||
           ' item = '||I_item;

   SQL_LIB.SET_MARK('OPEN', 'C_GET_REQ_TYPE', 'FM_FISCAL_DOC_HEADER', L_key);
     open C_GET_REQ_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_REQ_TYPE', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_GET_REQ_TYPE into L_req_type;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_REQ_TYPE', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_GET_REQ_TYPE;

   if (L_req_type = 'REP') then
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER_REP', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_FM_FISCAL_DOC_HEADER_REP;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER_REP', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_FM_FISCAL_DOC_HEADER_REP into O_fiscal_doc_line_id_ref, O_fiscal_doc_id_ref;
      if C_FM_FISCAL_DOC_HEADER_REP%NOTFOUND then
         ---
         O_exists := FALSE;
         O_fiscal_doc_line_id_ref := NULL;
         O_fiscal_doc_id_ref := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER_REP', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_DOC_HEADER_REP;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER_REP', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER_REP;
   ---
   else
   ---
      SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_FM_FISCAL_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_FM_FISCAL_DOC_HEADER into L_fiscal_doc_line_id_ref, L_fiscal_doc_id_ref;
      if C_FM_FISCAL_DOC_HEADER%NOTFOUND then
         ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_REQ_TYPE', 'FM_FISCAL_DOC_HEADER', L_key);
        open C_GET_REQ_TYPE;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_REQ_TYPE', 'FM_FISCAL_DOC_HEADER', L_key);
         fetch C_GET_REQ_TYPE into L_req_type;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_REQ_TYPE', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_GET_REQ_TYPE;
      SQL_LIB.SET_MARK('OPEN', 'C_DEF_WH_VAL', 'STORE', L_key);
          open C_DEF_WH_VAL;
      SQL_LIB.SET_MARK('FETCH', 'C_DEF_WH_VAL', 'STORE', L_key);
           fetch C_DEF_WH_VAL into L_default_wh_val;
      SQL_LIB.SET_MARK('CLOSE', 'C_DEF_WH_VAL', 'STORE', L_key);
         close C_DEF_WH_VAL;

      SQL_LIB.SET_MARK('OPEN', 'C_DEF_PH_WH_VAL', 'WH', L_key);
         open C_DEF_PH_WH_VAL;
         SQL_LIB.SET_MARK('FETCH', 'C_DEF_PH_WH_VAL', 'WH', L_key);
         fetch C_DEF_PH_WH_VAL into L_ph_wh_val;
         SQL_LIB.SET_MARK('CLOSE', 'C_DEF_PH_WH_VAL', 'WH', L_key);
         close C_DEF_PH_WH_VAL;
         If L_req_type = 'RMA' then
         SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER_DEF_WH', 'FM_FISCAL_DOC_HEADER', L_key);
           open C_FM_FISCAL_DOC_HEADER_DEF_WH(L_default_wh_val);
         ---
           SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER_DEF_WH', 'FM_FISCAL_DOC_HEADER', L_key);
           fetch C_FM_FISCAL_DOC_HEADER_DEF_WH into L_fiscal_doc_line_id_ref, L_fiscal_doc_id_ref;

          if C_FM_FISCAL_DOC_HEADER_DEF_WH%NOTFOUND then
             O_exists := FALSE;
             O_fiscal_doc_line_id_ref := NULL;
             O_fiscal_doc_id_ref := NULL;
           ---
           else
             O_exists := TRUE;
           end if;

            SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER_DEF_WH', 'FM_FISCAL_DOC_HEADER', L_key);
            close C_FM_FISCAL_DOC_HEADER_DEF_WH;
            return TRUE;

         ---
         else
             O_exists := FALSE;
             O_fiscal_doc_line_id_ref := NULL;
             O_fiscal_doc_id_ref := NULL;
         end if;

         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_DOC_HEADER;
         ---
         return TRUE;

         ---

      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ID;

      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ID into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ID;
      ---

      if NVL(L_count,0) > 1 then


          O_fiscal_doc_line_id_ref := NULL;
          O_fiscal_doc_id_ref := NULL;
        else
          O_fiscal_doc_line_id_ref := L_fiscal_doc_line_id_ref;
          O_fiscal_doc_id_ref := L_fiscal_doc_id_ref;

      end if;

      ---
   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_FISCAL_DOC_NO_REF;
-----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_NO_REF_RMA(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists                  IN OUT BOOLEAN,
                               O_fiscal_doc_line_id_ref  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                               O_fiscal_doc_id_ref       IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                               I_location                IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                               I_loc_type                IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                               I_item                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                               I_fiscal_doc_line_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_FISCAL_DOC_NO_REF';
   ---
   L_key       VARCHAR2(100);
   ---
   L_qty_received      ITEM_LOC_SOH.QTY_RECEIVED%TYPE;
   L_default_wh    STORE.DEFAULT_WH%TYPE;
   L_req_type      FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_PH_WH         WH.PHYSICAL_WH%TYPE;
   ---
   Cursor C_QTY_RECEIVED is
      Select qty_received
         from  item_loc_soh
         where item = I_item
           and loc =  I_location;

   Cursor C_DEF_WH is
    select default_wh
      from store
      where store = I_location;

	---
   Cursor C_DEF_PH_WH is
    select physical_wh
     from wh
     where wh in (select default_wh
                  from store
                  where store = I_location);
   cursor C_FM_FISCAL_DOC_HEADER is
      select fdd.fiscal_doc_line_id, fdd.fiscal_doc_id
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_schedule fs,
             fm_fiscal_utilization fu,
             fm_utilization_attributes fa
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.item = I_item
         and ((I_fiscal_doc_line_id is NOT NULL
         and fdd.fiscal_doc_line_id <> I_fiscal_doc_line_id)
          or (I_fiscal_doc_line_id is NULL))
         and fu.utilization_id = fdh.utilization_id
         and fa.utilization_id = fu.utilization_id
         and ((O_fiscal_doc_line_id_ref is NOT NULL
         and fdd.fiscal_doc_line_id = O_fiscal_doc_line_id_ref)
          or (O_fiscal_doc_line_id_ref is NULL))
         and fdh.location_id = I_location
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D')
         and fs.schedule_no = fdh.schedule_no
--- 003 - Begin
         and fdh.status            != 'I'
         and fdh.requisition_type   = 'PO'
         and fs.status              = 'A'
         and fs.mode_type           = 'ENT'
         and fu.status              = 'A'
         and fa.comp_nf_ind         = 'N'
         and fu.status              = 'A'
--- 003 - End
       order by fdh.entry_or_exit_date desc,
                fdh.fiscal_doc_id desc,
                fdd.fiscal_doc_line_id desc;
 ---
 cursor C_FM_FISCAL_DOC_HEADER_DEF_WH is
      select fdd.fiscal_doc_line_id, fdd.fiscal_doc_id
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_schedule fs,
             fm_fiscal_utilization fu,
             fm_utilization_attributes fa
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.item = I_item
         and ((I_fiscal_doc_line_id is NOT NULL
         and fdd.fiscal_doc_line_id <> I_fiscal_doc_line_id)
          or (I_fiscal_doc_line_id is NULL))
         and fu.utilization_id = fdh.utilization_id
         and fa.utilization_id = fu.utilization_id
         and ((O_fiscal_doc_line_id_ref is NOT NULL
         and fdd.fiscal_doc_line_id = O_fiscal_doc_line_id_ref)
          or (O_fiscal_doc_line_id_ref is NULL))
         and (fdh.location_id = L_default_wh
	  or fdh.location_id = L_PH_WH)
         --and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D')
          and fs.schedule_no = fdh.schedule_no
         and fdh.status            != 'I'
         and fdh.requisition_type   = 'PO'
         and fs.status              = 'A'
         and fs.mode_type           = 'ENT'
         and fu.status              = 'A'
         and fa.comp_nf_ind         = 'N'
         and fu.status              = 'A'
       order by fdh.entry_or_exit_date desc,
                fdh.fiscal_doc_id desc,
                fdd.fiscal_doc_line_id desc;
---

BEGIN
   ---
   L_key := 'location_id = '||TO_CHAR(I_location) ||
           ' location_type = '''||I_loc_type||''''||
           ' item = '||I_item;
   ---

   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   fetch C_FM_FISCAL_DOC_HEADER into O_fiscal_doc_line_id_ref, O_fiscal_doc_id_ref;
   if C_FM_FISCAL_DOC_HEADER%NOTFOUND then
      ---
	  SQL_LIB.SET_MARK('OPEN', 'C_QTY_RECEIVED', 'ITEM_LOC_SOH', L_key);
      open C_QTY_RECEIVED;
	 SQL_LIB.SET_MARK('FETCH', 'C_QTY_RECEIVED', 'ITEM_LOC_SOH', L_key);
      fetch C_QTY_RECEIVED into L_qty_received;
	 SQL_LIB.SET_MARK('CLOSE', 'C_QTY_RECEIVED', 'ITEM_LOC_SOH', L_key);
      close C_QTY_RECEIVED;

	   SQL_LIB.SET_MARK('OPEN', 'C_DEF_WH', 'STORE', L_key);
         open C_DEF_WH;
	   SQL_LIB.SET_MARK('FETCH', 'C_DEF_WH', 'STORE', L_key);
         fetch C_DEF_WH into L_default_wh;
	   SQL_LIB.SET_MARK('CLOSE', 'C_DEF_WH', 'STORE', L_key);
         close C_DEF_WH;

	 ---
       SQL_LIB.SET_MARK('OPEN', 'C_DEF_PH_WH', 'WH', L_key);
       open C_DEF_PH_WH;

       SQL_LIB.SET_MARK('FETCH', 'C_DEF_PH_WH', 'WH', L_key);
       fetch C_DEF_PH_WH into L_PH_WH;

       SQL_LIB.SET_MARK('CLOSE', 'C_DEF_PH_WH', 'WH', L_key);
       close C_DEF_PH_WH;

      If L_qty_received is null then
		SQL_LIB.SET_MARK('OPEN', 'C_DEF_WH', 'STORE', L_key);
              open C_DEF_WH;
		SQL_LIB.SET_MARK('FETCH', 'C_DEF_WH', 'STORE', L_key);
              fetch C_DEF_WH into L_default_wh;
		 SQL_LIB.SET_MARK('CLOSE', 'C_DEF_WH', 'STORE', L_key);
              close C_DEF_WH;
               SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER_DEF_WH', 'FM_FISCAL_DOC_HEADER', L_key);
               open C_FM_FISCAL_DOC_HEADER_DEF_WH;
               ---
               SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER_DEF_WH', 'FM_FISCAL_DOC_HEADER', L_key);
               fetch C_FM_FISCAL_DOC_HEADER_DEF_WH into O_fiscal_doc_line_id_ref, O_fiscal_doc_id_ref;
                if C_FM_FISCAL_DOC_HEADER_DEF_WH%NOTFOUND then
                   O_exists := FALSE;
                   O_fiscal_doc_line_id_ref := NULL;
                   O_fiscal_doc_id_ref := NULL;
              SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER_DEF_WH', 'FM_FISCAL_DOC_HEADER', L_key);
              close C_FM_FISCAL_DOC_HEADER_DEF_WH;
              return TRUE;
               end if;
           end if;


      O_exists := TRUE;
     --O_fiscal_doc_line_id_ref := NULL;
     --O_fiscal_doc_id_ref := NULL;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      return TRUE;
      ---
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_FISCAL_DOC_NO_REF_RMA;
-----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_NO_REF(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists                  IN OUT BOOLEAN,
                               O_fiscal_doc_line_id_ref  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                               O_fiscal_doc_id_ref       IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                               I_location                IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                               I_loc_type                IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                               I_item                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                               I_fiscal_doc_line_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_FISCAL_DOC_NO_REF';
   ---
   L_key       VARCHAR2(100);
   ---

   cursor C_FM_FISCAL_DOC_HEADER is
      select fdd.fiscal_doc_line_id, fdd.fiscal_doc_id
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
--- 003 - Begin
             fm_schedule fs,
--- 003 - End
             fm_fiscal_utilization fu,
             fm_utilization_attributes fa
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.item = I_item
         and ((I_fiscal_doc_line_id is NOT NULL
         and fdd.fiscal_doc_line_id <> I_fiscal_doc_line_id)
          or (I_fiscal_doc_line_id is NULL))
         and fu.utilization_id = fdh.utilization_id
         and fa.utilization_id = fu.utilization_id
         and ((O_fiscal_doc_line_id_ref is NOT NULL
         and fdd.fiscal_doc_line_id = O_fiscal_doc_line_id_ref)
          or (O_fiscal_doc_line_id_ref is NULL))
         and fdh.location_id = I_location
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D')
         and fs.schedule_no = fdh.schedule_no
--- 003 - Begin
         and fdh.status            != 'I'
         and fdh.requisition_type   = 'PO'
         and fs.status              = 'A'
         and fs.mode_type           = 'ENT'
         and fu.status              = 'A'
         and fa.comp_nf_ind         = 'N'
         and fu.status              = 'A'
--- 003 - End
       order by fdh.entry_or_exit_date desc,
                fdh.fiscal_doc_id desc,
                fdd.fiscal_doc_line_id desc;
   ---
BEGIN
   ---
   L_key := 'location_id = '||TO_CHAR(I_location) ||
           ' location_type = '''||I_loc_type||''''||
           ' item = '||I_item;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   fetch C_FM_FISCAL_DOC_HEADER into O_fiscal_doc_line_id_ref, O_fiscal_doc_id_ref;
   if C_FM_FISCAL_DOC_HEADER%NOTFOUND then
      ---
      O_exists := FALSE;
      O_fiscal_doc_line_id_ref := NULL;
      O_fiscal_doc_id_ref := NULL;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      return TRUE;
      ---
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_FISCAL_DOC_NO_REF;

-----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_NO_REF_REP(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists                  IN OUT BOOLEAN,
                                   O_fiscal_doc_line_id_ref  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                   O_fiscal_doc_id_ref       IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                   I_location                IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                   I_loc_type                IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                   I_item                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                                   I_fiscal_doc_line_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_FISCAL_DOC_NO_REF_REP';
   ---
   L_key       VARCHAR2(100);
   ---

   cursor C_FM_FISCAL_DOC_HEADER is
      select fdd.fiscal_doc_line_id, fdd.fiscal_doc_id
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_schedule fs,
             fm_fiscal_utilization fu,
             fm_utilization_attributes fa
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.item = I_item
         and ((I_fiscal_doc_line_id is NOT NULL
         and fdd.fiscal_doc_line_id <> I_fiscal_doc_line_id)
          or (I_fiscal_doc_line_id is NULL))
         and fu.utilization_id = fdh.utilization_id
         and fa.utilization_id = fu.utilization_id
         and ((O_fiscal_doc_line_id_ref is NOT NULL
         and fdd.fiscal_doc_line_id = O_fiscal_doc_line_id_ref)
          or (O_fiscal_doc_line_id_ref is NULL))
         and fdh.location_id = I_location
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D')
         and fs.schedule_no = fdh.schedule_no
         and fdh.status            != 'I'
         and fdh.requisition_type   = 'REP'
         and fs.status              = 'A'
         and fs.mode_type           <> 'ENT'
         and fu.status              = 'A'
         and fa.comp_nf_ind         = 'N'
         and fu.status              = 'A'
       order by fdh.entry_or_exit_date desc,
                fdh.fiscal_doc_id desc,
                fdd.fiscal_doc_line_id desc;
   ---
BEGIN
   ---
   L_key := 'location_id = '||TO_CHAR(I_location) ||
           ' location_type = '''||I_loc_type||''''||
           ' item = '||I_item;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   fetch C_FM_FISCAL_DOC_HEADER into O_fiscal_doc_line_id_ref, O_fiscal_doc_id_ref;
   if C_FM_FISCAL_DOC_HEADER%NOTFOUND then
      ---
      O_exists := FALSE;
      O_fiscal_doc_line_id_ref := NULL;
      O_fiscal_doc_id_ref := NULL;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      return TRUE;
      ---
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_FISCAL_DOC_NO_REF_REP;

-----------------------------------------------------------------------------------
--- 003 - Begin
FUNCTION VALIDATE_TSF(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists          IN OUT BOOLEAN,
                      I_requisition_no  IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id     IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                      I_location_type   IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                      I_doc_fiscal_type IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                      I_key_value_1     IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)

   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_TSF';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1)   := NULL;
   ---
   cursor C_EXISTS is
       select 'X'
         from tsfhead a, tsfhead b
        where b.from_loc_type = 'E'
          and b.from_loc = I_key_value_1
          and b.to_loc_type = I_location_type
          and (b.to_loc = I_location_id
           or (b.to_loc IN (select wh.wh from wh where wh.physical_wh =I_location_id)))
          and a.tsf_no = b.tsf_parent_no
          and a.status in ('S','C')
          and a.tsf_no = I_requisition_no
       union
       select 'X'
         from tsfhead a, tsfhead b
        where a.to_loc_type ='E'
          and a.to_loc =I_key_value_1
          and a.tsf_no = b.tsf_parent_no
          and b.status in ('S','A')
          and a.status in ('S','C')
          and b.tsf_no = I_requisition_no;

   cursor C_CHECK_HOLD is
      select 'x'
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             tsfhead t
       where fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.requisition_no = t.tsf_parent_no
         and fdh.status ='H'
         and t.from_loc_type = 'E'
         and t.from_loc = I_key_value_1
         and t.tsf_no = I_requisition_no
         and rownum = 1;

BEGIN
   ---
   L_key := 'requisition_no = '||I_requisition_no||
            ' location_id = '||I_location_id||
            ' location_type = '''||I_location_type||'''';
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      open C_EXISTS;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      fetch C_EXISTS into L_dummy;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      close C_EXISTS;

      -- check if 1st leg is in HOLD status
      if L_dummy is NULL then
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_HOLD', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
         open C_CHECK_HOLD;
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_HOLD', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
         fetch C_CHECK_HOLD into L_dummy;
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_HOLD', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
         close C_CHECK_HOLD;
      end if;

      if L_dummy is NULL then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      ---
--   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_TSF;
---END VALIDATE_RTV;
-----------------------------------------------------------------------------------
--- 003 - End
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_EDI(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists          IN OUT BOOLEAN,
                      I_edi_order_no    IN     FM_EDI_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id     IN     FM_EDI_DOC_DETAIL.LOCATION_ID%TYPE,
                      I_location_type   IN     FM_EDI_DOC_DETAIL.LOCATION_TYPE%TYPE,
                      I_doc_fiscal_type IN     VARCHAR2,
                      I_edi_doc_id      IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)

---FUNCTION VALIDATE_RTV(O_error_message  IN OUT VARCHAR2,
---                      O_exists         IN OUT BOOLEAN,
---                      I_rtv_order_no   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
---                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
---                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
   return BOOLEAN is
   ---
--- 003 - Begin
---   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_RTV';
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_EDI';
--- 003 - End
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS is
       select 'x'
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_utilization_attributes a,
--- 003 - Begin
             fm_fiscal_doc_header fdh1, fm_schedule fs1
--- 003 - End
       where ((fdh.module = 'LOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.module = 'OLOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.location_type = I_location_type
         and fdh.location_id = I_location_id))
         and fdh.requisition_type = I_doc_fiscal_type
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdh.utilization_id = a.utilization_id
--- 003 - Begin
--         and fdd.requisition_no = I_rtv_order_no
         and fdd.requisition_no = I_edi_order_no
         and fdh1.fiscal_doc_id = fdh.fiscal_doc_id
	      and fdh1.fiscal_doc_id = I_edi_doc_id
         and ((fdh.requisition_type IN ('TSF','IC')
           and fs1.mode_type = 'ENT'
           and fdh.key_value_1 <> fdh1.key_value_1
           and to_char(fdh.location_id) = fdh1.key_value_1
           and fdh.key_value_2 = fdh1.key_value_2)
          or ((fdh.requisition_type NOT IN ('TSF','IC')
            or fs1.mode_type <> 'ENT')
           and fdh.key_value_1 = fdh1.key_value_1
           and fdh.key_value_2 = fdh1.key_value_2))
         and fs1.schedule_no    = fdh1.schedule_no
         and NOT EXISTS (select 1
                           from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd1, fm_schedule fs
                          where fdh.fiscal_doc_id = fdd.fiscal_doc_id
                            and fs.schedule_no = fdh.schedule_no
                            and fs.status = 'A'
                            and fdh.status NOT IN ('I','D')
                            and fdd.requisition_no = fdd1.requisition_no
                            and fdh.requisition_type = fdh.requisition_type
                            and fs1.mode_type     = fs.mode_type
                            and fdh.fiscal_doc_id <> fdh1.fiscal_doc_id
                            and fdd.item = fdd.item);
--- 003 - End

   cursor C_EXISTS_REP is
      select 'x'
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_utilization_attributes a,
             fm_fiscal_doc_header fdh1, fm_schedule fs1,tsfhead th
       where ((fdh.module = 'LOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.module = 'OLOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.location_type = I_location_type
         and fdh.location_id = I_location_id))
         --and fdh.requisition_type = I_doc_fiscal_type
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.requisition_no = th.tsf_parent_no
         and th.context_type = 'REP'
         and th.tsf_no = I_edi_order_no
         and fdh.utilization_id = a.utilization_id
	-- and fdh.process_status in ('P','R','E')
         and fdd.requisition_no = th.tsf_parent_no
         and fdh1.fiscal_doc_id = fdh.fiscal_doc_id
         and ((fdh.requisition_type = 'REP'
           and fs1.mode_type = 'ENT'
           and fdh.key_value_1 <> fdh1.key_value_1
           and to_char(fdh.location_id) = fdh1.key_value_1
           and fdh.key_value_2 = fdh1.key_value_2)
          or ((fdh.requisition_type <> 'REP'
            or fs1.mode_type <> 'ENT')
           and fdh.key_value_1 = fdh1.key_value_1
           and fdh.key_value_2 = fdh1.key_value_2))
         and fs1.schedule_no    = fdh1.schedule_no
         and NOT EXISTS (select 1
                           from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd1, fm_schedule fs
                          where fdh.fiscal_doc_id = fdd.fiscal_doc_id
                            and fs.schedule_no = fdh.schedule_no
                            and fs.status = 'A'
                            and fdh.status NOT IN ('I','D')
                            and fdd.requisition_no = fdd1.requisition_no
                            and fdh.requisition_type = fdh.requisition_type
                            and fs1.mode_type     = fs.mode_type
                            and fdh.fiscal_doc_id <> fdh1.fiscal_doc_id
                            and fdd.item = fdd.item);
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_edi_order_no||
            ' location_id = '||I_location_id||
            ' location_type = '''||I_location_type||'''';
   ---
   if (I_doc_fiscal_type = 'REP') then
      SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_REP', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      open C_EXISTS_REP;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_REP', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      fetch C_EXISTS_REP into L_dummy;
      O_exists := C_EXISTS_REP%FOUND;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_REP', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      close C_EXISTS_REP;
      ---
   else
      SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      open C_EXISTS;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      fetch C_EXISTS into L_dummy;
      O_exists := C_EXISTS%FOUND;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
      close C_EXISTS;
      ---
   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
--- 003 - Begin
END VALIDATE_EDI;
---END VALIDATE_RTV;
--- 003 - End
-----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_RTV(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                      I_rtv_order_no   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_ITEM_RTV';
   ---
BEGIN
   ---
   if I_item_desc is NOT NULL then
--- 001 - Begin
      O_query := 'select im.item_desc, im.item, fdd.quantity, fdd.unit_cost '||
---      O_query := 'select im.item_desc, im.item '||
--- 001 - End
                   'from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = ''RTV'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = '||TO_CHAR(I_rtv_order_no)||' '||
                    'and im.item = fdd.item '||
                  'union all '||
--- 001 - Begin
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, fdd.quantity, fdd.unit_cost '||
---                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item '||
--- 001 - End
                   'from item_master im, tl_shadow tl_shadow, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = ''RTV'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = '||TO_CHAR(I_rtv_order_no)||' '||
                    'and im.item = fdd.item '||
                  'order by 1';
   else
--- 001 - Begin
      O_query := 'select im.item_desc, im.item, fdd.quantity, fdd.unit_cost '||
---      O_query := 'select im.item_desc, im.item '||
--- 001 - End
                   'from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = ''RTV'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = '||TO_CHAR(I_rtv_order_no)||' '||
                    'and im.item = fdd.item '||
                  'union all '||
--- 001 - Begin
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, fdd.quantity, fdd.unit_cost '||
---                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item '||
--- 001 - End
                   'from item_master im, tl_shadow tl_shadow, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = ''RTV'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = '||TO_CHAR(I_rtv_order_no)||' '||
                    'and im.item = fdd.item '||
                  'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ITEM_RTV;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_RTV(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           I_rtv_order_no   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                           I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                           I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_ITEM_RTV';
   L_key       VARCHAR2(200);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS_ITEM is
      select 'x'
        from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd
       where ((fdh.module = 'LOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.module = 'OLOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.location_type = I_location_type
         and fdh.location_id = I_location_id))
         and fdh.requisition_type = 'RTV'
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.requisition_no = I_rtv_order_no
         and im.item = fdd.item
         and fdd.item = I_item;
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_rtv_order_no||
            ' location_id = '||I_location_id||
            ' location_type = '''||I_location_type||''''||
            ' item = '||I_item;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_ITEM', 'V_FM_ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   open C_EXISTS_ITEM;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_ITEM', 'V_FM_ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_EXISTS_ITEM into L_dummy;
   O_exists := C_EXISTS_ITEM%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_ITEM', 'V_FM_ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   close C_EXISTS_ITEM;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ITEM_RTV;
-----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_STOCK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_query          IN OUT VARCHAR2,
                        I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                        I_stock_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                        I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                        I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_ITEM_STOCK';
   ---
BEGIN
   ---
   if I_item_desc is NOT NULL then
--- 001 - Begin
      O_query := 'select im.item_desc, im.item, fdd.quantity, fdd.unit_cost '||
---      O_query := 'select im.item_desc, im.item '||
--- 001 - End
                   'from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = ''STOCK'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = '||TO_CHAR(I_stock_no)||' '||
                    'and im.item = fdd.item '||
                  'union all '||
--- 001 - Begin
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, fdd.quantity, fdd.unit_cost '||
---                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item '||
--- 001 - End
                   'from item_master im, tl_shadow tl_shadow, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = ''STOCK'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = '||TO_CHAR(I_stock_no)||' '||
                    'and im.item = fdd.item '||
                  'order by 1';
   else
--- 001 - Begin
      O_query := 'select im.item_desc, im.item, fdd.quantity, fdd.unit_cost '||
---      O_query := 'select im.item_desc, im.item '||
--- 001 - End
                   'from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = ''STOCK'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = '||TO_CHAR(I_stock_no)||' '||
                    'and im.item = fdd.item '||
                  'union all '||
--- 001 - Begin
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, fdd.quantity, fdd.unit_cost '||
---                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item '||
--- 001 - End
                   'from item_master im, tl_shadow tl_shadow, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and ((fdh.module = ''LOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.module = ''OLOC'' '||
                    'and fdh.key_value_2 = '''||I_location_type||''' '||
                    'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                    ' or (fdh.location_type = '''||I_location_type||''' '||
                    'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                    'and fdh.requisition_type = ''STOCK'' '||
                    'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                    'and fdd.requisition_no = '||TO_CHAR(I_stock_no)||' '||
                    'and im.item = fdd.item '||
                  'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ITEM_STOCK;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_STOCK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists         IN OUT BOOLEAN,
                             I_stock_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                             I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                             I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                             I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_ITEM_STOCK';
   L_key       VARCHAR2(200);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS_ITEM is
      select 'x'
        from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd
       where ((fdh.module = 'LOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.module = 'OLOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.location_type = I_location_type
         and fdh.location_id = I_location_id))
         and fdh.requisition_type = 'STOCK'
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.requisition_no = I_stock_no
         and im.item = fdd.item
         and fdd.item = I_item;
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_stock_no||
            ' location_id = '||I_location_id||
            ' location_type = '''||I_location_type||''''||
            ' item = '||I_item;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_ITEM', 'V_ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   open C_EXISTS_ITEM;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_ITEM', 'V_ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_EXISTS_ITEM into L_dummy;
   O_exists := C_EXISTS_ITEM%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_ITEM', 'V_ITEM_MASTER, FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   close C_EXISTS_ITEM;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ITEM_STOCK;
-----------------------------------------------------------------------------------
FUNCTION LOV_RMA_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_query          IN OUT VARCHAR2,
--- 004 - Begin
                    I_fiscal_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
--- 004 - End
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_RMA_NO';
   ---
BEGIN
   ---
--- 004 - Begin
   O_query := 'select distinct frh.rma_id rma_no '||
                'from fm_rma_detail frd, '||
                     'fm_rma_head frh, '||
                     'fm_fiscal_doc_header fdh '||
               'where frd.rma_id        = frh.rma_id '||
                 'and frh.rma_status    = ''A'' '||
                 'and fdh.fiscal_doc_id = '|| I_fiscal_doc_id ||' '||
                 'and frh.cust_id       =  fdh.key_value_1 '||
                 'and fdh.key_value_2   =  ''C'' '||
                 'and fdh.module        =  ''CUST'' '||
                 'and NOT EXISTS (select 1 '||
                                    'from fm_fiscal_doc_header fdh, '||
                                         'fm_fiscal_doc_detail fdd, '||
                                         'fm_schedule fs '||
                                   'where fdh.fiscal_doc_id = fdd.fiscal_doc_id '||
                                     'and fs.schedule_no = fdh.schedule_no '||
                                     'and fs.status = ''A'' '||
                                     'and fdh.status NOT IN (''I'',''D'') '||
                                     'and fdd.requisition_no = frh.rma_id '||
                                     'and fdh.requisition_type = ''RMA'' '||
                                     'and fdd.item = frd.item) '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_RMA_NO;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_RMA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists         IN OUT BOOLEAN,
                      I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
--- 004 - Begin
                      I_fiscal_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
--- 004 - End
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_RMA';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS is
--- 004 - Begin
      select 'x'
        from fm_rma_detail frd,
             fm_rma_head frh,
             fm_fiscal_doc_header fdh
       where frd.rma_id = frh.rma_id
         and frh.rma_status    = 'A'
         and frh.rma_id        = I_rma_no
         and fdh.fiscal_doc_id = I_fiscal_doc_id
         and frh.cust_id       = fdh.key_value_1
         and fdh.key_value_2   = 'C'
         and fdh.module        = 'CUST'
         and NOT EXISTS (select 1
                           from fm_fiscal_doc_header fdh,
                                fm_fiscal_doc_detail fdd,
                                fm_schedule fs
                          where fdh.fiscal_doc_id = fdd.fiscal_doc_id
                            and fs.schedule_no = fdh.schedule_no
                            and fs.status = 'A'
                            and fdh.status NOT IN ('I','D')
                            and fdd.requisition_no = frh.rma_id
                            and fdh.requisition_type = 'RMA'
                            and fdd.item = frd.item);


   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_rma_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'FM_EDI_DOC_HEADER, FM_EDI_DOC_DETAIL', L_key);
   open C_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'FM_EDI_DOC_HEADER, FM_EDI_DOC_DETAIL', L_key);
   fetch C_EXISTS into L_dummy;
   O_exists := C_EXISTS%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', 'FM_EDI_DOC_HEADER, FM_EDI_DOC_DETAIL', L_key);
   close C_EXISTS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_RMA;
-----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_RMA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                      I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_ITEM_RMA';
   ---
BEGIN
   ---
--- 001 - Begin
   if I_item_desc is NOT NULL then
      O_query := 'select im.item_desc, im.item, frd.qty quantity, NULL uni_cost '||
                   'from item_master im, fm_rma_detail frd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and im.pack_ind = ''N'' '||
                    'and frd.rma_id  = '||I_rma_no||' '||
                    'and frd.item    = im.item '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, frd.qty quantity, NULL uni_cost '||
                   'from item_master im, tl_shadow tl_shadow, fm_rma_detail frd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and UPPER(im.item_desc) like ''%' || UPPER(I_item_desc) || '%'' '||
                    'and im.pack_ind = ''N'' '||
                    'and frd.rma_id  = '||I_rma_no||' '||
                    'and frd.item    = im.item '||
                  'order by 1';
   else
      O_query := 'select im.item_desc, im.item, frd.qty quantity, NULL uni_cost '||
                   'from item_master im, fm_rma_detail frd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and im.pack_ind = ''N'' '||
                    'and frd.rma_id  = '||I_rma_no||' '||
                    'and frd.item    = im.item '||
                  'union all '||
                 'select NVL(tl_shadow.translated_value, im.item_desc) item_desc, im.item, frd.qty quantity, NULL uni_cost '||
                   'from item_master im, tl_shadow tl_shadow, fm_rma_detail frd '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(im.item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and im.pack_ind = ''N'' '||
                    'and frd.rma_id  = '||I_rma_no||' '||
                    'and frd.item    = im.item '||
                  'order by 1';
   end if;
    ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ITEM_RMA;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_RMA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                           I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_ITEM_RMA';
   L_key       VARCHAR2(200);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS_ITEM is
      select 'x'
        from item_master im, fm_rma_detail frd
       where frd.rma_id = I_rma_no
         and im.item = frd.item
         and frd.item = I_item;
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_rma_no||
            ' item = '||I_item;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_ITEM', 'ITEM_MASTER, FM_RMA_DETAIL', L_key);
   open C_EXISTS_ITEM;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_ITEM', 'ITEM_MASTER, FM_RMA_DETAIL', L_key);
   fetch C_EXISTS_ITEM into L_dummy;
   O_exists := C_EXISTS_ITEM%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_ITEM', 'ITEM_MASTER, FM_RMA_DETAIL', L_key);
   close C_EXISTS_ITEM;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ITEM_RMA;
-----------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO_COMP_REF(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_query               IN OUT VARCHAR2,
                                    I_compl_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE,
                                    I_fiscal_doc_no       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                    I_location            IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                    I_loc_type            IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DETAIL_VAL_SQL.LOV_FISCAL_DOC_NO_COMP_REF';
   L_pwh       BOOLEAN := FALSE;
   ---
BEGIN
   ---
   if I_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_query := 'select fdh.fiscal_doc_id, NVL(fdh.fiscal_doc_no,0) fiscal_doc_no, fdh.series_no, cd1.code_desc module, cd2.code_desc key_value_2, fdh.key_value_1, fdh.issue_date, fdd.item, fdd.quantity, fdd.unit_cost, fdd.fiscal_doc_line_id '||
                'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, code_detail cd1, code_detail cd2, fm_fiscal_doc_complement fdc '||
               'where fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
                 'and fdc.compl_fiscal_doc_id = '||TO_CHAR(I_compl_fiscal_doc_id)||' '||
                 'and fdh.fiscal_doc_id = fdc.fiscal_doc_id '||
                 'and cd1.code_type = ''FIMO'' '||
                 'and cd1.code = fdh.module '||
                 'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'') '||
                 'and cd2.code (+) = fdh.key_value_2 '||
                 'and fdh.status NOT IN (''I'',''D'') ';

   if I_loc_type = 'S' or L_pwh = FALSE then
      O_query := O_query ||
                 'and fdh.location_id = '|| I_location ||' '||
                 'and fdh.location_type = '''|| I_loc_type ||''' ';
   else
      O_query := O_query ||
                 'and fdh.location_id IN (select wh.wh from wh where wh.physical_wh = '||I_location||') '||
                 'and fdh.location_type = '''|| I_loc_type ||''' ';
   end if;

   if I_fiscal_doc_no is NOT NULL then
      O_query := O_query ||
                 'and NVL(fdh.fiscal_doc_no,0) = '||I_fiscal_doc_no||' ';
   end if;
   O_query := O_query ||
               'order by 3';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_FISCAL_DOC_NO_COMP_REF;
-----------------------------------------------------------------------------------
FUNCTION VAL_FISCAL_DOC_NO_COMP_REF(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists              IN OUT BOOLEAN,
                                    O_fiscal_doc_id_ref   IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                    I_compl_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE,
                                    I_fiscal_doc_no       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                    I_location            IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                    I_loc_type            IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VAL_FISCAL_DOC_NO_COMP_REF';
   ---
   L_key       VARCHAR2(100);
   L_count     NUMBER;
   L_fiscal_doc_id_ref FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_pwh       BOOLEAN := FALSE;
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select fdh.fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_compl_fiscal_doc_id
         and fdh.fiscal_doc_id = fdc.fiscal_doc_id
         and ((O_fiscal_doc_id_ref is NOT NULL
         and fdh.fiscal_doc_id = O_fiscal_doc_id_ref)
          or (O_fiscal_doc_id_ref is NULL))
         and NVL(fdh.fiscal_doc_no,0) = I_fiscal_doc_no
         and fdh.location_id = I_location
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D');
   ---
   cursor C_COUNT_FISCAL_DOC_ID is
      select COUNT(*)
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_compl_fiscal_doc_id
         and fdh.fiscal_doc_id = fdc.fiscal_doc_id
         and ((O_fiscal_doc_id_ref is NOT NULL
         and fdh.fiscal_doc_id = O_fiscal_doc_id_ref)
          or (O_fiscal_doc_id_ref is NULL))
         and NVL(fdh.fiscal_doc_no,0) = I_fiscal_doc_no
         and fdh.location_id = I_location
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D');
   ---
   cursor C_FM_FISCAL_DOC_HEADER_VWH is
      select fdh.fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_compl_fiscal_doc_id
         and fdh.fiscal_doc_id = fdc.fiscal_doc_id
         and ((O_fiscal_doc_id_ref is NOT NULL
         and fdh.fiscal_doc_id = O_fiscal_doc_id_ref)
          or (O_fiscal_doc_id_ref is NULL))
         and NVL(fdh.fiscal_doc_no,0) = I_fiscal_doc_no
         and fdh.location_id IN (select wh.wh from wh where wh.physical_wh = I_location)
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D');
   ---
   cursor C_COUNT_FISCAL_DOC_ID_VWH is
      select COUNT(*)
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_compl_fiscal_doc_id
         and fdh.fiscal_doc_id = fdc.fiscal_doc_id
         and ((O_fiscal_doc_id_ref is NOT NULL
         and fdh.fiscal_doc_id = O_fiscal_doc_id_ref)
          or (O_fiscal_doc_id_ref is NULL))
         and NVL(fdh.fiscal_doc_no,0) = I_fiscal_doc_no
         and fdh.location_id IN (select wh.wh from wh where wh.physical_wh = I_location)
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D');
   ---
BEGIN
   ---
   if I_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_loc_type = 'S' or L_pwh = FALSE then
      ---
      L_key := 'compl_fiscal_doc_id = '||TO_CHAR(I_compl_fiscal_doc_id)||
              ' fiscal_doc_no = '||I_fiscal_doc_no ||
              ' location_id = '||TO_CHAR(I_location) ||
              ' location_type = '''||I_loc_type||'''';
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_FM_FISCAL_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_FM_FISCAL_DOC_HEADER into L_fiscal_doc_id_ref;
      if C_FM_FISCAL_DOC_HEADER%NOTFOUND then
         ---
         O_exists := FALSE;
         O_fiscal_doc_id_ref := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_DOC_HEADER;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ID;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ID into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ID;
      ---
   else
      ---
      L_key := 'compl_fiscal_doc_id = '||TO_CHAR(I_compl_fiscal_doc_id)||
              ' fiscal_doc_no = '||I_fiscal_doc_no ||
              ' location_id = '||TO_CHAR(I_location) ||
              ' location_type = '''||I_loc_type||'''';
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_FM_FISCAL_DOC_HEADER_VWH;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_FM_FISCAL_DOC_HEADER_VWH into L_fiscal_doc_id_ref;
      if C_FM_FISCAL_DOC_HEADER_VWH%NOTFOUND then
         ---
         O_exists := FALSE;
         O_fiscal_doc_id_ref := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_DOC_HEADER_VWH;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER_VWH;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ID_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ID_VWH;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ID_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ID_VWH into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ID_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ID_VWH;
      ---
   end if;
   ---
   if NVL(L_count,0) > 1 then
      O_fiscal_doc_id_ref := NULL;
   else
      O_fiscal_doc_id_ref := L_fiscal_doc_id_ref;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VAL_FISCAL_DOC_NO_COMP_REF;
-----------------------------------------------------------------------------------
FUNCTION GET_DELIVERY_SUPPLIER(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_delivery_supplier IN OUT ORDHEAD.DELIVERY_SUPPLIER%TYPE,
                               I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_DELIVERY_SUPPLIER';
   L_key       VARCHAR2(200) := 'order_no = '||TO_CHAR(I_order_no);
   ---
   cursor C_ORDER is
      select vfo.delivery_supplier
        from ordhead vfo
       where order_no = I_order_no;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDER','ORDHEAD',L_key);
   open C_ORDER;
   ---
   SQL_LIB.SET_MARK('FETCH','C_ORDER','ORDHEAD',L_key);
   fetch C_ORDER into O_delivery_supplier;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ORDER','ORDHEAD',L_key);
   close C_ORDER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DELIVERY_SUPPLIER;
-----------------------------------------------------------------------------------
FUNCTION INSERT_NON_EXIST_DOCS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program VARCHAR2(80) := 'FM_FISCAL_DETAIL_VAL_SQL.INSERT_NON_EXIST_DOCS';
   L_key     VARCHAR2(80) := 'I_schedule_no: ' || I_schedule_no;
   ---
   L_exists         BOOLEAN;
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select fdd.fiscal_doc_id,
             fdd.location_id,
             fdd.location_type,
             fdd.fiscal_doc_no
        from fm_fiscal_doc_header fdd
       where fdd.schedule_no = I_schedule_no
         and fdd.status IN ('A');
   ---
   R_fiscal_doc_header  C_FM_FISCAL_DOC_HEADER%ROWTYPE;

BEGIN
   ---
    SQL_LIB.SET_MARK('OPEN',
                    'C_FM_FISCAL_DOC_HEADER',
                    'FM_FISCAL_DOC_HEADER',
                    L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   LOOP
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_FM_FISCAL_DOC_HEADER
         into R_fiscal_doc_header ;
      EXIT when C_FM_FISCAL_DOC_HEADER%NOTFOUND;
      ---

   END LOOP;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_FM_FISCAL_DOC_HEADER',
                    'FM_FISCAL_DOC_HEADER',
                    L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   COMMIT;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1, 254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_FM_FISCAL_DOC_HEADER',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         close C_FM_FISCAL_DOC_HEADER;
      end if;

      ---
      return FALSE;
END INSERT_NON_EXIST_DOCS;

-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDLOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists         IN OUT BOOLEAN,
                         I_order_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                         I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                         I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                         I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_ORDLOC';
   L_pwh       BOOLEAN := FALSE;
   L_vwh       WH.WH%TYPE;
   ---
   cursor C_WH is
      select wh.wh
        from wh
       where wh.physical_wh = I_location_id;
   ---
BEGIN
   ---
   if I_location_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_location_type = 'S' or NOT L_pwh then
      ---
      if ORDER_ITEM_VALIDATE_SQL.EXIST_ORDLOC(O_error_message,
                                              O_exists,
                                              I_order_no,
                                              I_item,
                                              I_location_id,
                                              I_location_type) = FALSE then
         return FALSE;
      end if;
      ---
   else
      ---
      if ORDER_ITEM_VALIDATE_SQL.EXIST_ORDLOC(O_error_message,
                                              O_exists,
                                              I_order_no,
                                              I_item,
                                              I_location_id,
                                              I_location_type) = FALSE then
         return FALSE;
      end if;
      ---
      if O_exists = FALSE then
         ---
         SQL_LIB.SET_MARK('OPEN','C_WH','WH','physical_wh = '||TO_CHAR(I_location_id));
         open C_WH;
         ---
         LOOP
            SQL_LIB.SET_MARK('FETCH','C_WH','WH','physical_wh = '||TO_CHAR(I_location_id));
            fetch C_WH into L_vwh;
            EXIT when C_WH%NOTFOUND;
            ---
            if ORDER_ITEM_VALIDATE_SQL.EXIST_ORDLOC(O_error_message,
                                                    O_exists,
                                                    I_order_no,
                                                    I_item,
                                                    L_vwh,
                                                    I_location_type) = FALSE then
               return FALSE;
            end if;
            ---
            if O_exists then
               return TRUE;
            end if;
            ---
         END LOOP;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_WH','WH','physical_wh = '||TO_CHAR(I_location_id));
         close C_WH;
         ---
      end if;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ORDLOC;
-----------------------------------------------------------------------------------
FUNCTION GET_UNIQUE_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_fiscal_doc_line_id IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                           O_item               IN OUT FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           I_fiscal_doc_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_UNIQUE_DETAIL';
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id, fdd.item
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id
         and 1 = (select COUNT(*)
                    from fm_fiscal_doc_detail fdd2
                   where fdd2.fiscal_doc_id = I_fiscal_doc_id);
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_FISCAL_DOC_DETAIL','FM_FISCAL_DOC_DETAIL','fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id));
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_FISCAL_DOC_DETAIL','FM_FISCAL_DOC_DETAIL','fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id));
   fetch C_FM_FISCAL_DOC_DETAIL into O_fiscal_doc_line_id, O_item;
   ---
   if C_FM_FISCAL_DOC_DETAIL%NOTFOUND then
      O_fiscal_doc_line_id := NULL;
      O_item := NULL;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DOC_DETAIL','FM_FISCAL_DOC_DETAIL','fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id));
   close C_FM_FISCAL_DOC_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_UNIQUE_DETAIL;
-----------------------------------------------------------------------------------
--- 001 - Begin
----------------------------------------------------------------------------------
FUNCTION GET_QTY_ITEM_ORDER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_quantity       IN OUT NUMBER,
                            O_unit_cost      IN OUT NUMBER,
                            O_pack_ind       IN OUT FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                            O_pack_no        IN OUT FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_order_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                            I_location       IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                            I_loc_type       IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_QTY_ITEM_ORDER';
   L_pwh       BOOLEAN := FALSE;
   ---
   cursor C_ORDER_LOC_TYPE_S is
     select quantity,
            unit_cost,
            pack_ind,
            pack_no
       from (select NVL((NVL(ol.qty_ordered,0) - NVL(ol.qty_received,0)),0) quantity,
                    ol.unit_cost,
                    im.pack_ind,
                    NULL pack_no
               from item_master im,
                    ordloc ol
              where im.item     = I_item
                and ol.order_no = I_order_no
                and ol.location = I_location
                and ol.loc_type = I_loc_type
                and ol.item     = im.item
                and im.pack_ind = NVL(O_pack_ind,im.pack_ind)
              union all
             select NVL((NVL(vol.qty_ordered,0) - NVL(vol.qty_received,0)),0) * vpb.pack_item_qty quantity,
                    (((viscl.negotiated_item_cost * vpb.pack_item_qty) / (tot_pack.unit_cost_pack)) * (vol.unit_cost)) / (vpb.pack_item_qty) unit_cost,
                    vim.pack_ind,
                    tot_pack.pack_no
               from ordhead voh,
                    ordloc vol,
                    item_master vim,
                    packitem_breakout vpb,
                    item_supp_country_loc viscl,
                    v_br_item_fiscal_attrib vbifa,
                    (select pb2.pack_no,
                            isc2.supplier,
                            isc2.origin_country_id,
                            isc2.loc,
                            SUM(pb2.pack_item_qty * isc2.negotiated_item_cost) unit_cost_pack,
                            SUM(pb2.pack_item_qty) tot_item_pack
                       from packitem_breakout pb2,
                            item_supp_country_loc isc2
                      where isc2.item = pb2.item
                      group by pb2.pack_no,
                            isc2.supplier,
                            isc2.origin_country_id,
                            isc2.loc) tot_pack
               where vim.item = I_item
                 and voh.order_no = I_order_no
                 and vol.location = I_location
                 and vol.loc_type = I_loc_type
                 and vol.location = tot_pack.loc
                 and voh.supplier = tot_pack.supplier
                 and vpb.pack_no = tot_pack.pack_no
                 and vol.item = tot_pack.pack_no
                 and voh.order_no = vol.order_no
                 and vpb.item = viscl.item
                 and vpb.item     = vim.item
                 and voh.supplier = viscl.supplier
                 and vol.location = viscl.loc
                 and vol.location = tot_pack.loc
                 and vbifa.item = vim.item
                 and vbifa.country_id = viscl.origin_country_id
                 and tot_pack.origin_country_id = viscl.origin_country_id
                 and vim.pack_ind = NVL(O_pack_ind,vim.pack_ind)
                 and tot_pack.pack_no = NVL(O_pack_no,tot_pack.pack_no));                 

   cursor C_ORDER_LOC_TYPE_W is
      select distinct quantity,
             unit_cost,
             pack_ind,
             pack_no
        from (select NVL((NVL(sum(ol.qty_ordered),0) - NVL(sum(ol.qty_received),0)),0) quantity,
                     ol.unit_cost,
                     im.pack_ind,
                     NULL pack_no
                from item_master im,
                     ordloc ol
               where im.item     = I_item
                 and ol.order_no = I_order_no
                 and ol.location IN (select wh.wh
                                       from wh
                                      where wh.physical_wh = I_location)
                 and ol.loc_type = I_loc_type
                 and ol.item     = im.item
                 and im.pack_ind = NVL(O_pack_ind,im.pack_ind)
                group by ol.unit_cost, im.pack_ind
               union all
              select distinct (select NVL((NVL(sum(vol1.qty_ordered),0) - NVL(sum(vol1.qty_received),0)),0) from ordloc vol1 where vol1.order_no=I_order_no and vol1.item = vol.item)* vpb.pack_item_qty quantity,
                    (((viscl.negotiated_item_cost * vpb.pack_item_qty) / (tot_pack.unit_cost_pack)) * (vol.unit_cost)) / (vpb.pack_item_qty) unit_cost,
                    vim.pack_ind,
                    tot_pack.pack_no
               from ordhead voh,
                    ordloc vol,
                    item_master vim,
                    packitem_breakout vpb,
                    item_supp_country_loc viscl,
                    v_br_item_fiscal_attrib vbifa,
                    (select pb2.pack_no,
                            isc2.supplier,
                            isc2.origin_country_id,
                            isc2.loc,
                            SUM(pb2.pack_item_qty * isc2.negotiated_item_cost) unit_cost_pack,
                            SUM(pb2.pack_item_qty) tot_item_pack
                       from packitem_breakout pb2,
                            item_supp_country_loc isc2
                      where isc2.item = pb2.item
                      group by pb2.pack_no,
                            isc2.supplier,
                            isc2.origin_country_id,
                            isc2.loc) tot_pack
               where vim.item = I_item
                 and voh.order_no = I_order_no
                 and vol.location IN (select wh.wh
                                       from wh
                                      where wh.physical_wh = I_location)
                 and vol.loc_type = I_loc_type
                 and vol.location = tot_pack.loc
                 and voh.supplier = tot_pack.supplier
                 and vpb.pack_no = tot_pack.pack_no
                 and vol.item = tot_pack.pack_no
                 and voh.order_no = vol.order_no
                 and vpb.item = viscl.item
                 and vpb.item     = vim.item
                 and voh.supplier = viscl.supplier
                 and vol.location = viscl.loc
                 and vol.location = tot_pack.loc
                 and vbifa.item = vim.item
                 and vbifa.country_id = viscl.origin_country_id
		             and tot_pack.origin_country_id = viscl.origin_country_id
                 and vim.pack_ind = NVL(O_pack_ind,vim.pack_ind)
                 and tot_pack.pack_no = NVL(O_pack_no,tot_pack.pack_no));

BEGIN
   ---
   if I_order_no is NOT NULL then
      ---
      if I_loc_type = 'W' then
         if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                    L_pwh,
                                    I_location) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_loc_type = 'S' or NOT L_pwh then
	       for rec in C_ORDER_LOC_TYPE_S
         LOOP
	          if O_quantity is NOT NULL and O_unit_cost is NOT NULL then
               if rec.quantity = O_quantity  and rec.unit_cost = O_unit_cost then
                  O_quantity  := rec.quantity ;
                  O_unit_cost := rec.unit_cost ;
                  O_pack_ind  := rec.pack_ind;
                  O_pack_no   := rec.pack_no;
                  return TRUE;
               end if;
            else
	             O_quantity  := rec.quantity ;
               O_unit_cost := rec.unit_cost ;
               O_pack_ind  := rec.pack_ind;
               O_pack_no   := rec.pack_no;
	          end if;
         END LOOP;
      else
         for rec in C_ORDER_LOC_TYPE_W
         LOOP
            if O_quantity is NOT NULL and O_unit_cost is NOT NULL then
	             if rec.quantity = O_quantity  and rec.unit_cost = O_unit_cost then
                  O_quantity  := rec.quantity ;
                  O_unit_cost := rec.unit_cost ;
                  O_pack_ind  := rec.pack_ind;
                  O_pack_no   := rec.pack_no;
                  return TRUE;
               end if;
	          else
	             O_quantity  := rec.quantity ;
               O_unit_cost := rec.unit_cost ;
               O_pack_ind  := rec.pack_ind;
               O_pack_no   := rec.pack_no;
            end if;
         END LOOP;
      end if;
   else
      O_quantity := NULL;
      O_unit_cost := NULL;
      O_pack_no   := NULL;
      O_pack_ind  := NULL;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_QTY_ITEM_ORDER;
----------------------------------------------------------------------------------
FUNCTION GET_QTY_ITEM_RMA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_quantity       IN OUT NUMBER,
                          O_unit_cost      IN OUT NUMBER,
                          I_item           IN     ITEM_MASTER.ITEM%TYPE,
                          I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE)
   return BOOLEAN is
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_QTY_ITEM_RMA';
   ---
   cursor C_QTY_RMA is
      select frd.qty quantity,
             NULL    unit_cost
        from item_master im,
             fm_rma_detail frd
       where im.item     = I_item
         and im.pack_ind = 'N'
         and frd.rma_id  = I_rma_no
         and frd.item    = im.item;

BEGIN
   ---
   open C_QTY_RMA;
   fetch C_QTY_RMA into O_quantity,
                        O_unit_cost;
   close C_QTY_RMA;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_QTY_ITEM_RMA;
----------------------------------------------------------------------------------
FUNCTION GET_QTY_EDI(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_quantity        IN OUT NUMBER,
                     O_unit_cost       IN OUT NUMBER,
                     I_item            IN     ITEM_MASTER.ITEM%TYPE,
                     I_rtv_stock_tsf   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                     I_doc_fiscal_type IN     VARCHAR2,
                     I_location_id     IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                     I_location_type   IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                     I_pack_ind        IN     FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE DEFAULT NULL,
                     I_pack_no         IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE DEFAULT NULL)
   return BOOLEAN is
   L_program   VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_QTY_RTV_STOCK_TSF';
   ---
   cursor C_QTY_RTV_STOCK_TSF is
      select fdd.quantity,
             DECODE(fua.choose_doc_ind,'N',fdd.unit_cost,NULL) unit_cost
        from fm_utilization_attributes fua,
             item_master          im,
             fm_fiscal_doc_header         fdh,
             fm_fiscal_doc_detail         fdd
       where im.item = I_item
         and ((fdh.module         = 'LOC'
         and fdh.key_value_2      = I_location_type
         and fdh.key_value_1      = I_location_id)
          or (fdh.module          = 'OLOC'
         and fdh.key_value_2      = I_location_type
         and fdh.key_value_1      = I_location_id)
          or (fdh.location_type   = I_location_type
         and fdh.location_id      = I_location_id))
         and fdh.requisition_type = I_doc_fiscal_type
         and fdd.fiscal_doc_id    = fdh.fiscal_doc_id
         and (fdd.requisition_no  = I_rtv_stock_tsf
          or  fdd.requisition_no in (select tsf.tsf_parent_no from tsfhead tsf where tsf.tsf_no = I_rtv_stock_tsf))
         and im.item              = fdd.item
         and fdh.utilization_id   = fua.utilization_id
         and NVL(fdd.pack_ind,'N') = NVL(I_pack_ind,'N')
         and NVL(fdd.pack_no,'X')  = NVL(I_pack_no,'X');

BEGIN
   ---
   open C_QTY_RTV_STOCK_TSF;
   fetch C_QTY_RTV_STOCK_TSF into O_quantity,
                                  O_unit_cost;
   close C_QTY_RTV_STOCK_TSF;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_QTY_EDI;
----------------------------------------------------------------------------------
FUNCTION EXPLODE_PACK(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program      VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.EXPLODE_PACK';

   ---
   cursor C_GET_ORIG_DATA is
      select fdd.fiscal_doc_id
            ,fdd.requisition_no
	    ,fdd.item
	    ,fdd.discount_type
	    ,fdd.discount_value
	from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_line_id = I_fiscal_doc_line_id
         and fdd.unexpected_item = 'N';
   ---
   L_orig_data   C_GET_ORIG_DATA%ROWTYPE;
   ---
   cursor C_GET_DISC_APPR (P_orig_data   L_ORIG_DATA%TYPE) is
      select DECODE(P_orig_data.discount_type, 'P', P_orig_data.discount_value, 'V', P_orig_data.discount_value * (fdd.unit_cost * fdd.quantity) / pack.total_cost) appr_disc,
             fdd.fiscal_doc_line_id fiscal_doc_line_id
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             (select sum(fdd.unit_cost * fdd.quantity) total_cost
                from fm_fiscal_doc_header fdh,
                     fm_fiscal_doc_detail fdd
               where fdh.fiscal_doc_id = P_orig_data.fiscal_doc_id
                 and fdd.fiscal_doc_id = fdh.fiscal_doc_id
                 and fdd.requisition_no = P_orig_data.requisition_no
                 and fdd.pack_no = P_orig_data.item) pack
       where fdh.fiscal_doc_id = P_orig_data.fiscal_doc_id
         and fdd.fiscal_doc_id = fdh.fiscal_Doc_id
         and fdd.requisition_no = P_orig_data.requisition_no
         and fdd.pack_no = P_orig_data.item;
   ---
   L_disc_appr   C_GET_DISC_APPR%ROWTYPE;
   ---
   cursor C_LOCK_FISCAL_DOC_DEATIL(P_fiscal_doc_line_id   FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_line_id = P_fiscal_doc_line_id
         for update nowait;
   cursor C_LOCK_FISCAL_DOC_TAX_DEATIL is
      select 'X'
        from fm_fiscal_doc_tax_detail fdtd
       where fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id
         for update nowait;
   ---
BEGIN
   ---
   if I_fiscal_doc_line_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_line_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_GET_ORIG_DATA;
   fetch C_GET_ORIG_DATA into L_orig_data;
   close C_GET_ORIG_DATA;
   ---
   --- check for lock
   open C_LOCK_FISCAL_DOC_DEATIL(I_fiscal_doc_line_id);
   close C_LOCK_FISCAL_DOC_DEATIL;

   --- check for lock
   open C_LOCK_FISCAL_DOC_TAX_DEATIL;
   close C_LOCK_FISCAL_DOC_TAX_DEATIL;
   ---
   insert into fm_fiscal_doc_detail
                                    (fiscal_doc_line_id,
                                     line_no,
                                     fiscal_doc_id,
                                     location_id,
                                     location_type,
                                     requisition_no,
                                     item,
                                     classification_id,
                                     quantity,
                                     unit_cost,
                                     total_cost,
                                     total_calc_cost,
                                     freight_cost,
                                     net_cost,
                                     fiscal_doc_line_id_ref,
                                     fiscal_doc_id_ref,
                                     discount_type,
                                     discount_value,
                                     unit_cost_with_disc,
                                     entry_cfop,
                                     nf_cfop,
                                     other_expenses_cost,
                                     insurance_cost,
                                     qty_discrep_status,
                                     cost_discrep_status,
                                     tax_discrep_status,
                                     unexpected_item,
                                     inconclusive_rules,
                                     appt_qty,
                                     recoverable_base,
                                     recoverable_value,
                                     icms_cst,
                                     create_datetime,
                                     create_id,
                                     last_update_datetime,
                                     last_update_id,
                                     unit_item_disc,
                                     unit_header_disc,
                                     pack_ind,
                                     pack_no,
                                     inconcl_rule_ind)
                              Select
                                     FM_FISCAL_DOC_LINE_ID_SEQ.NEXTVAL,rownum, a.*
                                     from
                    (select distinct fdd.fiscal_doc_id,
                                     fdd.location_id,
                                     fdd.location_type,
                                     fdd.requisition_no,
                                     iscl.item,
                                     vbifa.classification_id,
                                     (pi.pack_item_qty * fdd.quantity) tot_cost,
                                     Round((iscl.negotiated_item_cost * fdd.unit_cost / tot_pack.unit_cost_pack),4),
                                     (pi.pack_item_qty * fdd.quantity) * Round((iscl.negotiated_item_cost * fdd.unit_cost / tot_pack.unit_cost_pack),4),
                                     NULL nul1,
                                     NULL nul2,
                                     NULL nul3,
                                     NULL nul4,
                                     NULL nul5,
                                     fdd.discount_type,
                                     fdd.discount_value,
                                     NULL nul6,
                                     entry_cfop,
                                     nf_cfop,
                                     NULL nul7,
                                     NULL nul8,
                                     qty_discrep_status,
                                     cost_discrep_status,
                                     tax_discrep_status,
                                     unexpected_item,
                                     inconclusive_rules,
                                     (pi.pack_item_qty * fdd.quantity) tot_cost1,
                                     recoverable_base,
                                     recoverable_value,
                                     icms_cst,
                                     SYSDATE,
                                     USER,
                                     SYSDATE date1,
                                     USER user1,
                                     NULL nul9,
                                     NULL nul10,
                                     'N',
                                     pi.pack_no,
                                     inconcl_rule_ind
                                      from packitem_breakout pi ,
                                      item_supp_country_loc iscl,
                                      ordhead oh,
                                      ordloc ol,
                                      ( select pb2.pack_no,
                                               isc2.supplier,
                                               isc2.origin_country_id,
                                               isc2.loc,
                                               SUM(pb2.pack_qty * isc2.negotiated_item_cost) unit_cost_pack,
                                               SUM(pb2.pack_qty) tot_item_pack
                                          from packitem pb2,
                                               item_supp_country_loc isc2
                                         where isc2.item = pb2.item
                                         group by pb2.pack_no,
                                                  isc2.supplier,
                                                  isc2.origin_country_id,
                                                  isc2.loc)  tot_pack ,
                                       fm_fiscal_doc_detail fdd,
                                       v_br_item_fiscal_attrib vbifa
                                 where pi.pack_no = fdd.item
                                   and pi.item = iscl.item
                                   and oh.order_no = ol.order_no
                                   and oh.supplier = iscl.supplier
                                   and ol.location = iscl.loc
                                   and ol.loc_type = iscl.loc_type
                                   and pi.pack_no = ol.item
                                   and ol.order_no = fdd.requisition_no
                                   and oh.supplier = tot_pack.supplier
                                   and ol.location = tot_pack.loc
                                   and ol.loc_type = fdd.location_type
                                   and (   ol.location = fdd.location_id
                                        or ol.location in ( select wh from wh where physical_wh = fdd.location_id)
                                        )
                                   and pi.pack_no =  tot_pack.pack_no
                                   and vbifa.item = iscl.item
                                   and fdd.fiscal_doc_line_id = I_fiscal_doc_line_id
                                   and nvl(fdd.unexpected_item,'N') = 'N'
					                              and vbifa.country_id = iscl.origin_country_id
                         					     and tot_pack.origin_country_id = iscl.origin_country_id) a;
                                     ---
   delete fm_fiscal_doc_tax_detail where fiscal_doc_line_id = I_fiscal_doc_line_id ;
   delete fm_fiscal_doc_detail where fiscal_doc_line_id = I_fiscal_doc_line_id and unexpected_item = 'N' ;
   ---
   open C_GET_DISC_APPR (L_orig_data);
   ---
   LOOP
   ---
      fetch C_GET_DISC_APPR into L_disc_appr;
      EXIT when C_GET_DISC_APPR%NOTFOUND;
      ---
      open C_LOCK_FISCAL_DOC_DEATIL(L_disc_appr.fiscal_doc_line_id);
      close C_LOCK_FISCAL_DOC_DEATIL;
      ---
      update fm_fiscal_doc_detail fdd
         set fdd.discount_value = L_disc_appr.appr_disc
       where fdd.fiscal_doc_line_id = L_disc_appr.fiscal_doc_line_id;
      ---
   END LOOP;
   ---
   close C_GET_DISC_APPR;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_DETAIL',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FISCAL_DOC_DEATIL%ISOPEN then
         close C_LOCK_FISCAL_DOC_DEATIL;
      end if;
      ---
      if C_LOCK_FISCAL_DOC_TAX_DEATIL%ISOPEN then
         close C_LOCK_FISCAL_DOC_TAX_DEATIL;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FISCAL_DOC_DEATIL%ISOPEN then
         close C_LOCK_FISCAL_DOC_DEATIL;
      end if;
      ---
      if C_LOCK_FISCAL_DOC_TAX_DEATIL%ISOPEN then
         close C_LOCK_FISCAL_DOC_TAX_DEATIL;
      end if;
      ---
      return FALSE;
END EXPLODE_PACK;
----------------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_ALL_PACK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program      VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.EXPLODE_ALL_PACK';
   L_fiscal_doc_line_id   FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   ---
   cursor C_FISCAL_DOC_PACK_DEATIL is
      select fdd.fiscal_doc_line_id
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id
         and  NVL(fdd.pack_ind,'N') = 'Y'
         and nvl(fdd.unexpected_item,'N') = 'N';

   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_FISCAL_DOC_PACK_DEATIL;
   ---
   LOOP
      fetch C_FISCAL_DOC_PACK_DEATIL into L_fiscal_doc_line_id;

   EXIT when C_FISCAL_DOC_PACK_DEATIL%NOTFOUND;

   if FM_FISCAL_DETAIL_VAL_SQL.EXPLODE_PACK(O_error_message,
                                          L_fiscal_doc_line_id) = FALSE then
          return FALSE;
   end if;

   END LOOP;
   ---
   close C_FISCAL_DOC_PACK_DEATIL;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FISCAL_DOC_PACK_DEATIL%ISOPEN then
         close C_FISCAL_DOC_PACK_DEATIL;
      end if;

      return FALSE;
END EXPLODE_ALL_PACK;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_COMP_COST_QTY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
			   I_comp_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
			   O_Comp_qty            OUT    FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
			   O_Comp_unit_cost      OUT    FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE)
   return BOOLEAN is
   ---
   L_program      VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_COMP_COST_QTY';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL';
   L_key          VARCHAR2(100) := 'Fiscal_doc_line_id: ' || TO_CHAR(I_fiscal_doc_line_id);

   ---
   cursor C_GET_COMP_QTY_COST is
      select
  	(pi.pack_qty * fdd.quantity) Quantity,
	 Round((iscl.negotiated_item_cost * fdd.unit_cost / tot_pack.unit_cost_pack),4) Unit_cost
      from packitem pi ,
	   item_supp_country_loc iscl,
	   ordhead oh,
	   ordloc ol,
	   ( select pb2.pack_no,
	       isc2.supplier,
	       isc2.origin_country_id,
	       isc2.loc,
	       SUM(pb2.pack_qty * isc2.negotiated_item_cost) unit_cost_pack,
	       SUM(pb2.pack_qty) tot_item_pack
	     from packitem pb2,
		  item_supp_country_loc isc2
	     where isc2.item = pb2.item
	     group by pb2.pack_no,
		      isc2.supplier,
		      isc2.origin_country_id,
		      isc2.loc) tot_pack ,
	     fm_fiscal_doc_detail fdd
      where pi.pack_no = fdd.item
	and pi.item = iscl.item
	and oh.order_no = ol.order_no
	and oh.supplier = iscl.supplier
	and ol.location = iscl.loc
	and pi.pack_no = ol.item
	and ol.order_no = fdd.requisition_no
	and oh.supplier = tot_pack.supplier
	and ol.location = tot_pack.loc
	and pi.pack_no =  tot_pack.pack_no
	and iscl.item  =   I_comp_item
	and fdd.fiscal_doc_line_id = I_fiscal_doc_line_id;

   ---
BEGIN
   ---
   if I_fiscal_doc_line_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_line_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_GET_COMP_QTY_COST;
   ---
   fetch C_GET_COMP_QTY_COST into O_Comp_qty,O_Comp_unit_cost;

   ---
   close C_GET_COMP_QTY_COST;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_GET_COMP_QTY_COST%ISOPEN then
         close C_GET_COMP_QTY_COST;
      end if;

      return FALSE;
END GET_COMP_COST_QTY;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_EXPLODE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program              VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_PACK_EXPLODE';
   L_pack_no              FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE;
   L_req_no               FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
   L_chk_all_Pack_item    Number;
   L_chk_pack_factor      Number;
   L_quantity             FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   ---
   cursor C_FISCAL_DOC_PACK_NO is
	   select distinct requisition_no, pack_no
	   from  fm_fiscal_doc_detail fdd
	   where fdd.fiscal_doc_id = I_fiscal_doc_id
		 and fdd.pack_no is not null;

   cursor C_VAL_ALL_PACK_COMP( P_pack_no  FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                               P_req_no   FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
		select Count(1)
		from   fm_fiscal_doc_detail fdd ,packitem_breakout pi
		where  fdd.pack_no = pi.pack_no
		and    fdd.pack_no is not null
		and    fdd.requisition_no = P_req_no
		and    fdd.fiscal_doc_id = I_fiscal_doc_id
		and    pi.pack_no = P_pack_no
		and    pi.item not in (select item from fm_fiscal_doc_detail where fiscal_doc_id = I_fiscal_doc_id and pack_no = P_pack_no and requisition_no = P_req_no);

   cursor C_VAL_PACK_COMP_FACTOR( P_pack_no  FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                                  P_req_no   FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
		select count (distinct (fdd.quantity / pi.pack_item_qty))
		from fm_fiscal_doc_detail fdd ,packitem_breakout pi
		where fdd.pack_no = pi.pack_no
		and   fdd.pack_no is not null
		and   fdd.item = pi.item
		and   fdd.requisition_no = P_req_no
		and   fdd.fiscal_doc_id = I_fiscal_doc_id
		and   fdd.pack_no = P_pack_no ;

    cursor C_VAL_PACK_COMP_FACTOR_CHK( P_pack_no  FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                                       P_req_no   FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
		select distinct (fdd.quantity / pi.pack_item_qty)
		from fm_fiscal_doc_detail fdd ,packitem_breakout pi
		where fdd.pack_no = pi.pack_no
		and   fdd.pack_no is not null
		and   fdd.item = pi.item
		and   fdd.requisition_no = P_req_no
		and   fdd.fiscal_doc_id = I_fiscal_doc_id
		and   fdd.pack_no = P_pack_no ;

   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_FISCAL_DOC_PACK_NO;
   ---
   LOOP
      fetch C_FISCAL_DOC_PACK_NO into L_req_no, L_pack_no;
      EXIT when C_FISCAL_DOC_PACK_NO%NOTFOUND;

     -- check if all comp of pack are available
      open C_VAL_ALL_PACK_COMP(L_pack_no, L_req_no);
     ---
      fetch C_VAL_ALL_PACK_COMP into L_chk_all_Pack_item;

      If L_chk_all_Pack_item > 0 then
	 O_error_message := SQL_LIB.CREATE_MSG('ORFM_PACK_COMP_NOT_MATCH',
                                                L_pack_no,
                                                NULL,
                                                NULL);
         close C_VAL_ALL_PACK_COMP;
         return FALSE;
      end if;
      ---
      close C_VAL_ALL_PACK_COMP;

      -- check if all comp of pack are with same factor

      open C_VAL_PACK_COMP_FACTOR(L_pack_no, L_req_no);
      ---
      fetch C_VAL_PACK_COMP_FACTOR into L_chk_pack_factor;

      If L_chk_pack_factor > 1 then
	 O_error_message := SQL_LIB.CREATE_MSG('ORFM_PKCMP_FACT_NOT_MATCH',
                                                L_pack_no,
                                                NULL,
                                                NULL);
         close C_VAL_PACK_COMP_FACTOR;
	 return FALSE;
      end if;
      ---
      close C_VAL_PACK_COMP_FACTOR;

      -- check if all comp of pack are with same factor but no fraction

      open C_VAL_PACK_COMP_FACTOR_CHK(L_pack_no, L_req_no);
      ---
      fetch C_VAL_PACK_COMP_FACTOR_CHK into L_quantity;

      If L_quantity <> Round(L_quantity) then
	 O_error_message := SQL_LIB.CREATE_MSG('ORFM_PKCMP_FACT_NOT_MATCH',
                                                L_pack_no,
                                                NULL,
                                                NULL);
         close C_VAL_PACK_COMP_FACTOR_CHK;
	 return FALSE;
      end if;
      ---
      close C_VAL_PACK_COMP_FACTOR_CHK;

   END LOOP;
   ---
   close C_FISCAL_DOC_PACK_NO;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FISCAL_DOC_PACK_NO%ISOPEN then
         close C_FISCAL_DOC_PACK_NO;
      end if;
      ---
      if C_VAL_ALL_PACK_COMP%ISOPEN then
	 close C_VAL_ALL_PACK_COMP;
      end if;
      ---
      if C_VAL_PACK_COMP_FACTOR%ISOPEN then
         close C_VAL_PACK_COMP_FACTOR;
      end if;
      ---
      if C_VAL_PACK_COMP_FACTOR_CHK%ISOPEN then
         close C_VAL_PACK_COMP_FACTOR_CHK;
      end if;
      ---
      return FALSE;
END VALIDATE_PACK_EXPLODE;
--------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PACK_ITEM (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists         OUT    BOOLEAN,
                          I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program      VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.CHECK_PACK_ITEM';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL';
   L_key          VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   L_fiscal_doc_line_id   FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_pack_count   NUMBER := 0;
   L_pack_ind     VARCHAR2(1) := 'Y';
   L_unexpected_item   VARCHAR2(1) := 'N';
   ---
   cursor C_FISCAL_DOC_PACK_ITEM is
      select count(1)
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id
         and  NVL(fdd.pack_ind,'N') = L_pack_ind
         and nvl(fdd.unexpected_item,'N') = L_unexpected_item;
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_FISCAL_DOC_PACK_ITEM';

   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_FISCAL_DOC_PACK_ITEM;
   ---
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_FISCAL_DOC_PACK_ITEM into L_pack_count;
      if (L_pack_count > 0) then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_FISCAL_DOC_PACK_ITEM;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FISCAL_DOC_PACK_ITEM%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FISCAL_DOC_PACK_ITEM', L_table, L_key);
         close C_FISCAL_DOC_PACK_ITEM;
      end if;

      return FALSE;
END CHECK_PACK_ITEM;
--------------------------------------------------------------------------------------------------------------
FUNCTION GET_TRIANGULATION_IND (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_triangulation_ind  OUT    VARCHAR2,
                                I_order_no           IN     ORDHEAD.ORDER_NO%TYPE)

   return BOOLEAN is
   ---
   L_program      VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_TRIANGULATION_IND ';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'ORDHEAD';
   L_key          VARCHAR2(100) := 'Order_no: ' || TO_CHAR(I_order_no);
   ---
   cursor C_GET_TRIANGULATION_IND is
                    select triangulation_ind
                    from ordhead
                    where order_no = I_order_no;
   ---
BEGIN
   L_cursor := 'C_GET_TRIANGULATION_IND';

   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_GET_TRIANGULATION_IND;
   ---
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_GET_TRIANGULATION_IND into O_triangulation_ind;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_GET_TRIANGULATION_IND;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_GET_TRIANGULATION_IND%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_TRIANGULATION_IND', L_table, L_key);
         close C_GET_TRIANGULATION_IND ;
      end if;

      return FALSE;
END GET_TRIANGULATION_IND;
---------------------------------------------------------------------------------------
FUNCTION GET_PACK_INFO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_pack_no        IN OUT FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                        O_pack_ind       IN OUT FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                        I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                        I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                        I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN is
   ---
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program      VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.GET_PACK_INFO';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL';
   L_key   VARCHAR2(500) := 'Key_value_1: ' || TO_CHAR(I_key_value_1) || 'Requisition_no: ' || TO_CHAR(I_tsf_no) || 'Item: ' || I_item;
   ---
   cursor C_GET_FISCAL_DOC_DEATIL is
      select fdd.pack_no, fdd.pack_ind
        from item_master im, fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd,tsfhead th, tsfhead th2
       where fdh.module in ('PTNR','LOC')
         and fdh.key_value_1 = TO_CHAR(I_key_value_1)
         and fdh.key_value_1 = th.to_loc
         and fdh.status in ('A','C','FP','H')
         and th.tsf_no = th2.tsf_parent_no
         and fdh.requisition_type in ('TSF','IC','REP')
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.requisition_no = th.tsf_no
         and (th.tsf_no = I_tsf_no
          or th2.tsf_no = I_tsf_no)
         and im.item = fdd.item
         and fdd.item = I_item
	       and not exists (select 1 from tsf_xform where tsf_no = I_tsf_no or tsf_no in (select tsf_parent_no from tsfhead where tsf_no = I_tsf_no))
	     union all
	    select fdd.pack_no, fdd.pack_ind
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd ,
             tsfhead th, tsfhead th2,
             tsf_xform txh, tsf_xform_detail txd
       where fdh.module in ('PTNR','LOC')
         and fdh.key_value_1 = TO_CHAR(I_key_value_1)
         and fdh.key_value_1 = th.to_loc
         and th.tsf_no = th2.tsf_parent_no
         and fdh.requisition_type IN ('TSF','IC','REP')
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdh.status in ('A','C','FP','H')
         and fdd.requisition_no = th.tsf_no
         and (th.tsf_no = I_tsf_no
          or th2.tsf_no = I_tsf_no)
         and txh.tsf_no = th.tsf_no
         and txh.tsf_xform_id = txd.tsf_xform_id
         and txd.from_item = fdd.item
		     and txd.to_item = I_item;
   ---
BEGIN
   ---
   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_1',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_GET_FISCAL_DOC_DEATIL';
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_GET_FISCAL_DOC_DEATIL;

   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_GET_FISCAL_DOC_DEATIL into O_pack_no,O_pack_ind;

   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_GET_FISCAL_DOC_DEATIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
END GET_PACK_INFO;
----------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PAYMENT_DATE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         OUT    BOOLEAN,
                            I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_program      VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.CHECK_PAYMENT_DATE';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_PAYMENTS';
   L_key          VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   L_date_count   NUMBER;
   ---
   cursor C_CHECK_PAYMENT_DATE is
      select count(1)
        from fm_fiscal_doc_payments fddp
       where fddp.fiscal_doc_id = I_fiscal_doc_id;
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_CHECK_PAYMENT_DATE';

   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_CHECK_PAYMENT_DATE;
   ---
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_CHECK_PAYMENT_DATE into L_date_count;
      if (L_date_count> 0) then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_CHECK_PAYMENT_DATE;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_CHECK_PAYMENT_DATE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_PAYMENT_DATE', L_table, L_key);
         close C_CHECK_PAYMENT_DATE;
      end if;

      return FALSE;
END CHECK_PAYMENT_DATE;
-----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_COMP_REF_DOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists        IN OUT BOOLEAN,
                                    I_fiscal_doc_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_count_pack_comp_ref_doc NUMBER;
   L_pack_no                 FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE;
   L_table                   VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL';
   L_key                     VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   L_program                 VARCHAR2(100) := 'FM_FISCAL_DETAIL_VAL_SQL.VALIDATE_PACK_COMP_REF_DOC';
   -- Distinct packs in an RTV
   CURSOR C_RTV_PACKS IS
   select distinct pack_no
     from fm_fiscal_doc_detail
    where fiscal_doc_id=I_fiscal_doc_id;

   CURSOR C_PACK_COMP_REF_DOC IS
   select count(distinct fiscal_doc_id_ref)
     from fm_fiscal_doc_detail
    where fiscal_doc_id=I_fiscal_doc_id
      and pack_no=L_pack_no;
BEGIN
   for L_pack_ind in C_RTV_PACKS
   LOOP
      L_pack_no := L_pack_ind.pack_no;
      OPEN C_PACK_COMP_REF_DOC;
      FETCH C_PACK_COMP_REF_DOC INTO  L_count_pack_comp_ref_doc;
      -- All components of pack should be associated with single Fiscal_Doc_Ref.
      if L_count_pack_comp_ref_doc > 1 then
         O_exists := true;
      end if;
      CLOSE C_PACK_COMP_REF_DOC;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_PACK_COMP_REF_DOC%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_PACK_COMP_REF_DOC', L_table, L_key);
         close C_PACK_COMP_REF_DOC;
      end if;
END VALIDATE_PACK_COMP_REF_DOC;
------------------------------------------------------------------------------------
END FM_FISCAL_DETAIL_VAL_SQL;
/