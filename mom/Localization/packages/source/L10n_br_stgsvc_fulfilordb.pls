CREATE OR REPLACE PACKAGE BODY L10N_BR_STGSVC_FULFILORD AS
-----------------------------------------------------------------------------------------------
LP_user             SVCPROV_CONTEXT.USER_NAME%TYPE     := user;
-----------------------------------------------------------------------------------------------
FUNCTION STAGE_SVC_FULFILORD_L10N(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_l10n_obj                  IN OUT   L10N_OBJ)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255)   := 'L10N_BR_STGSVC_FULFILORD.STAGE_SVC_FULFILORD_L10N';
   L_invalid_param    VARCHAR2(30)    := NULL;
   L_l10n_rib_rec     L10N_RIB_REC    := L10N_RIB_REC();
   L_process_id       SVC_BRFULFILORD.PROCESS_ID%TYPE;
   L_chunk_id         SVC_FULFILORD.CHUNK_ID%TYPE;
   L_service_name     VARCHAR2(30)    := 'createFulfilOrdColDesc';
   L_dtl_index        INTEGER   := 0;
   L_rib_obj          RIB_OBJECT;
   L_rib_brfulfilordcustdesc_rec  "RIB_FulfilOrdDesc_REC"           := NULL;
   
   L_description      FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE            := NULL;
   L_str_val          FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE           := NULL;
   L_date_val         FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE             := NULL;
   L_vartyp_num       FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE          := 'NUMBER';
   L_var_pres_ind     FM_SYSTEM_OPTIONS.VARIABLE%TYPE               := 'DEFAULT_PRESENCE_IND_CO'; 
   
   TYPE svc_brfuliflord_tbl is table of SVC_BRFULFILORD%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_brfulfilord_tbl   svc_brfuliflord_tbl;
   TYPE svc_brfulfilord_tbl is table of SVC_BRFULFILORD_PMT%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_brfulfilordpmt_tbl svc_brfulfilord_tbl;
   
   L_action_type      SVC_BRFULFILORD.ACTION_TYPE%TYPE;
   L_process_status   SVC_BRFULFILORD.PROCESS_STATUS%TYPE;
   L_fulfilord_id     SVC_BRFULFILORD.FULFILORD_ID%TYPE;

BEGIN
   
   L_l10n_rib_rec    := TREAT(IO_l10n_obj as L10N_RIB_REC);
   L_rib_obj         := L_l10n_rib_rec.rib_msg;
   L_action_type     := L_l10n_rib_rec.rib_msg_type;
   L_process_status  := L_l10n_rib_rec.status_code;
   L_fulfilord_id    := IO_l10n_obj.reference_id;
   L_rib_brfulfilordcustdesc_rec :=TREAT(L_rib_obj as "RIB_FulfilOrdDesc_REC");
   
   if L_action_type is NULL then
      L_invalid_param := 'L_action_type';
   elsif L_rib_brfulfilordcustdesc_rec is NULL then
      L_invalid_param := 'L_rib_brfulfilordcustdesc_rec';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if L_action_type != CORESVC_FULFILORD.ACTION_TYPE_CREATE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SERVICE_OPERATION',
                                            L_action_type,
                                            L_service_name,
                                            NULL);
      return FALSE;
   end if;
   
   if L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc is NULL or
      L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL is NULL or
      L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL.count<=0 then

      O_error_message := SQL_LIB.CREATE_MSG('EMPTY_CUST_ATTR_MSG',
                                            L_action_type,
                                            L_service_name,                                            
                                            NULL);
      return FALSE;
   end if;
   L_process_id   := IO_l10n_obj.process_id;
   L_chunk_id     :=IO_l10n_obj.chunk_id;
   FOR i in L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL.FIRST..
      L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL.LAST LOOP
      --build a collection of SVC_BRFULFILORD rows
      L_svc_brfulfilord_tbl(i).process_id := L_process_id;
      L_svc_brfulfilord_tbl(i).process_status := L_process_status;
      L_svc_brfulfilord_tbl(i).action_type := L_action_type;
      L_svc_brfulfilord_tbl(i).brfulfilord_id := l10n_br_fulfilord_seq.nextval;
      L_svc_brfulfilord_tbl(i).fulfilord_id := L_fulfilord_id;
      L_svc_brfulfilord_tbl(i).cust_neighborhood := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).neighborhood;
      L_svc_brfulfilord_tbl(i).cust_cpf := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).cpf;
      L_svc_brfulfilord_tbl(i).presence_ind := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).presence_ind;
      L_svc_brfulfilord_tbl(i).ie_exempt := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).ie_exempt;
      L_svc_brfulfilord_tbl(i).Taxpayer_Type := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).taxpayer_type;
      L_svc_brfulfilord_tbl(i).fiscal_address_information := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).fiscal_address_information;
      L_svc_brfulfilord_tbl(i).individual_taxpayer_id := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).individual_taxpayer_id;
      L_svc_brfulfilord_tbl(i).corporate_taxpayer_id := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).corporate_taxpayer_id;
      L_svc_brfulfilord_tbl(i).free_zone_manaus_inscrptn_id := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).free_zone_of_manaus_inscptn_id;
      L_svc_brfulfilord_tbl(i).city_inscription_id := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).city_inscription_id;
      L_svc_brfulfilord_tbl(i).state_inscription_id := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).state_inscription_id;
      L_svc_brfulfilord_tbl(i).contributor_type := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).contributor_type;
      L_svc_brfulfilord_tbl(i).tax_exception_type := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).tax_exception_type;
      L_svc_brfulfilord_tbl(i).iss_contributor := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).iss_contributor;
      L_svc_brfulfilord_tbl(i).ipi_contributor := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).ipi_contributor;
      L_svc_brfulfilord_tbl(i).icms_contributor := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).icms_contributor;
      L_svc_brfulfilord_tbl(i).pis_contributor := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).pis_contributor;
      L_svc_brfulfilord_tbl(i).cofins_contributor := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).cofins_contributor;
      L_svc_brfulfilord_tbl(i).income_range_eligible := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).is_income_range_eligible;
      L_svc_brfulfilord_tbl(i).distributor_manufacturer := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).is_distributor_manufacturer;
      L_svc_brfulfilord_tbl(i).customer_cnae := L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).customers_cnae;
      L_svc_brfulfilord_tbl(i).error_msg := NULL;
      
      --Check weather the presence indicator is null then find it from the system options
      --usually the call should be outside the loop but as there should not be more than one record in 
      --the message BrFulfilOrdCustDesc_TBL hence calling FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE to find the value.
      if L_svc_brfulfilord_tbl(i).presence_ind is NULL and L_svc_brfulfilord_tbl(i).Taxpayer_Type = 'F' then
         if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                               L_description,
                                               L_svc_brfulfilord_tbl(i).presence_ind,
                                               L_str_val,
                                               L_date_val,
                                               L_vartyp_num,
                                               L_var_pres_ind) = FALSE then
            return FALSE;
         end if;      
      end if;
      --build a collection of SVC_BRFULFILORD_PMT rows
      
      if L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).BrFulfilOrdPmtDesc_TBL is NOT NULL
         and L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).BrFulfilOrdPmtDesc_TBL.count >0 then
         FOR j in L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).BrFulfilOrdPmtDesc_TBL.FIRST ..
             L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).BrFulfilOrdPmtDesc_TBL.LAST LOOP
             L_dtl_index := L_dtl_index+1;
             L_svc_brfulfilordpmt_tbl(L_dtl_index).process_id := L_process_id;
             L_svc_brfulfilordpmt_tbl(L_dtl_index).process_status := L_process_status;
             L_svc_brfulfilordpmt_tbl(L_dtl_index).action_type := L_action_type;
             L_svc_brfulfilordpmt_tbl(L_dtl_index).brfulfilord_pmt_id := l10n_br_fulfilord_pmt_seq.nextval;
             L_svc_brfulfilordpmt_tbl(L_dtl_index).fulfilord_id := L_fulfilord_id;
             L_svc_brfulfilordpmt_tbl(L_dtl_index).cust_payment_method :=L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).BrFulfilOrdPmtDesc_TBL(j).payment_method; 
             L_svc_brfulfilordpmt_tbl(L_dtl_index).cust_payment_amount :=L_rib_brfulfilordcustdesc_rec.FulfilOrdCustDesc.LocOfFulfilOrdCustDesc.BrFulfilOrdCustDesc_TBL(i).BrFulfilOrdPmtDesc_TBL(j).payment_amount;
             L_svc_brfulfilordpmt_tbl(L_dtl_index).error_msg := NULL;            
         END LOOP;
      end if;
   END LOOP; -- BrFulfilOrdCustDesc_TBL

   --bulk insert staging table row collections into staging tables
   FORALL i in L_svc_brfulfilord_tbl.FIRST .. L_svc_brfulfilord_tbl.LAST
      insert into svc_brfulfilord(process_id,
                                   process_status,
                                   action_type,
                                   brfulfilord_id, 
                                   fulfilord_id,
                                   cust_neighborhood,
                                   cust_cpf,
                                   presence_ind,
                                   taxpayer_type,
                                   fiscal_address_information,
                                   individual_taxpayer_id,
                                   corporate_taxpayer_id,
                                   free_zone_manaus_inscrptn_id,
                                   city_inscription_id,
                                   state_inscription_id,
                                   contributor_type,
                                   tax_exception_type,
                                   iss_contributor,
                                   ipi_contributor ,
                                   icms_contributor,
                                   pis_contributor,
                                   cofins_contributor, 
                                   income_range_eligible,
                                   distributor_manufacturer,
                                   customer_cnae,
                                   ie_exempt,
                                   error_msg,
                                   create_datetime,
                                   create_id,
                                   last_update_datetime,
                                   last_update_id)
                        values (L_svc_brfulfilord_tbl(i).process_id,
                                L_svc_brfulfilord_tbl(i).process_status,
                                L_svc_brfulfilord_tbl(i).action_type,
                                L_svc_brfulfilord_tbl(i).brfulfilord_id, 
                                L_svc_brfulfilord_tbl(i).fulfilord_id,
                                L_svc_brfulfilord_tbl(i).cust_neighborhood,
                                L_svc_brfulfilord_tbl(i).cust_cpf,
                                L_svc_brfulfilord_tbl(i).presence_ind,
                                L_svc_brfulfilord_tbl(i).taxpayer_type,
                                L_svc_brfulfilord_tbl(i).fiscal_address_information,
                                L_svc_brfulfilord_tbl(i).individual_taxpayer_id,
                                L_svc_brfulfilord_tbl(i).corporate_taxpayer_id,
                                L_svc_brfulfilord_tbl(i).free_zone_manaus_inscrptn_id,
                                L_svc_brfulfilord_tbl(i).city_inscription_id,
                                L_svc_brfulfilord_tbl(i).state_inscription_id,
                                L_svc_brfulfilord_tbl(i).contributor_type,
                                L_svc_brfulfilord_tbl(i).tax_exception_type,
                                L_svc_brfulfilord_tbl(i).iss_contributor,
                                L_svc_brfulfilord_tbl(i).ipi_contributor,
                                L_svc_brfulfilord_tbl(i).icms_contributor,
                                L_svc_brfulfilord_tbl(i).pis_contributor,
                                L_svc_brfulfilord_tbl(i).cofins_contributor, 
                                L_svc_brfulfilord_tbl(i).income_range_eligible,
                                L_svc_brfulfilord_tbl(i).distributor_manufacturer,                                
                                L_svc_brfulfilord_tbl(i).customer_cnae,
                                L_svc_brfulfilord_tbl(i).ie_exempt,
                                L_svc_brfulfilord_tbl(i).error_msg,
                                SYSDATE,
                                LP_user,
                                SYSDATE,      
                                LP_user);

   --bulk insert staging table row collections into staging tables
   FORALL i in L_svc_brfulfilordpmt_tbl.FIRST .. L_svc_brfulfilordpmt_tbl.LAST
      insert into svc_brfulfilord_pmt(process_id,
                                      process_status,
                                      action_type,
                                      brfulfilord_pmt_id,
                                      fulfilord_id,
                                      cust_payment_method,
                                      cust_payment_amount,
                                      error_msg,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
                             values ( L_svc_brfulfilordpmt_tbl(i).process_id,
                                      L_svc_brfulfilordpmt_tbl(i).process_status,
                                      L_svc_brfulfilordpmt_tbl(i).action_type,
                                      L_svc_brfulfilordpmt_tbl(i).brfulfilord_pmt_id,
                                      L_svc_brfulfilordpmt_tbl(i).fulfilord_id,
                                      L_svc_brfulfilordpmt_tbl(i).cust_payment_method,
                                      L_svc_brfulfilordpmt_tbl(i).cust_payment_amount,
                                      L_svc_brfulfilordpmt_tbl(i).error_msg,
                                      SYSDATE,
                                      LP_user,
                                      SYSDATE,
                                      LP_user);

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STAGE_SVC_FULFILORD_L10N;

-----------------------------------------------------------------------------------------------
FUNCTION CLEAR_SVC_FULFILORD_L10N(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_l10n_obj       IN OUT   L10N_OBJ)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255)   := 'L10N_BR_STGSVC_FULFILORD.CLEAR_SVC_FULFILORD_L10N';
   L_invalid_param    VARCHAR2(30)    := NULL;
   L_process_id       SVC_BRFULFILORD.PROCESS_ID%TYPE;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_table            VARCHAR2(30)  := NULL;

   cursor C_LOCK_BRCUSTORDTSF is
      select 'x'
        from svc_brfulfilord
       where process_id = L_process_id
         and process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
         for update nowait;

   cursor C_LOCK_BRFULFILORD_PMT is
      select 'x'
        from SVC_BRFULFILORD_PMT pmt
       where pmt.process_id = L_process_id
         and exists (select 'x'
                       from svc_brfulfilord brf
                      where brf.process_id = L_process_id
                        and brf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and brf.fulfilord_id = pmt.fulfilord_id
                        and rownum = 1)
         for update nowait;

BEGIN
   
   L_process_id := IO_l10n_obj.process_id;
   if L_process_id is NULL then
      L_invalid_param := 'L_process_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   L_table := 'SVC_BRFULFILORD_PMT';
   open C_LOCK_BRFULFILORD_PMT;
   close C_LOCK_BRFULFILORD_PMT;

   delete from SVC_BRFULFILORD_PMT pmt
    where pmt.process_id = L_process_id
      and exists (select 'x'
                    from svc_brfulfilord ctf
                   where ctf.process_id = L_process_id
                     and ctf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                     and ctf.fulfilord_id = pmt.fulfilord_id
                     and rownum = 1);

   L_table := 'SVC_BRFULFILORD';
   open C_LOCK_BRCUSTORDTSF;
   close C_LOCK_BRCUSTORDTSF;

   delete from svc_brfulfilord
    where process_id = L_process_id
      and process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_program,
                                             to_char(L_process_id));
      return FALSE;

  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLEAR_SVC_FULFILORD_L10N;
-----------------------------------------------------------------------------------------------
END L10N_BR_STGSVC_FULFILORD;
/