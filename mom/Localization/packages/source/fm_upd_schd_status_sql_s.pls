CREATE OR REPLACE PACKAGE FM_UPD_SCHD_STATUS_SQL is
------------------------------------------------------------------------------------------------
--    Name: UPDATE_SCHDULE_STATUS
-- Purpose: This function  UPDATE the Schedule Status  to "Approved" if there are NO 
--          Quantity/Cost/Tax header/Tax detail discrepancy  exist for any NF in a SCHEDULE.
------------------------------------------------------------------------------------------------
FUNCTION UPD_SCHEDULE_STATUS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_schedule_no    IN      FM_SCHEDULE.SCHEDULE_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--    Name: NF_COST_TAX_DISCREP_EXISTS
-- Purpose: This function  UPDATE the NF Status  to "Approved" if there are NO 
--          Cost/Tax header/Tax detail discrepancy  exist for the NF .
------------------------------------------------------------------------------------------------
FUNCTION NF_COST_TAX_DISCREP_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                    O_status         OUT     FM_FISCAL_DOC_HEADER.STATUS%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
END FM_UPD_SCHD_STATUS_SQL;
/