CREATE OR REPLACE PACKAGE L10N_BR_FND_SQL AS
--------------------------------------------------------------------------------------------
-- Public constants
LP_NCM       CONSTANT VARCHAR2(30) := 'NCM';
LP_NCM_CARAC CONSTANT VARCHAR2(30) := 'NCMCARAC';
LP_NCM_IPI   CONSTANT VARCHAR2(30) := 'NCMIPI';
LP_NCM_PAUTA CONSTANT VARCHAR2(30) := 'NCMPAUTA';
LP_MASSERV   CONSTANT VARCHAR2(30) := 'MASSERV';
LP_CNAE      CONSTANT VARCHAR2(30) := 'CNAE';
LP_FEDSERV   CONSTANT VARCHAR2(30) := 'FEDSERV';

-- Public Type

TYPE TYP_l10n_fisc_attrib_rec IS RECORD
(
   wh                      NUMBER,
   l10n_country_id         L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
   group_id                L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
   varchar2_1              VARCHAR2(250),
   varchar2_2              VARCHAR2(250),
   varchar2_3              VARCHAR2(250),
   varchar2_4              VARCHAR2(250),
   varchar2_5              VARCHAR2(250),
   varchar2_6              VARCHAR2(250),
   varchar2_7              VARCHAR2(250),
   varchar2_8              VARCHAR2(250),
   varchar2_9              VARCHAR2(250),
   varchar2_10             VARCHAR2(250),
   number_11               NUMBER,
   number_12               NUMBER,
   number_13               NUMBER,
   number_14               NUMBER,
   number_15               NUMBER,
   number_16               NUMBER,
   number_17               NUMBER,
   number_18               NUMBER,
   number_19               NUMBER,
   number_20               NUMBER,
   date_21                 DATE,
   date_22                 DATE
);
TYPE TYP_l10n_fisc_attrib_tbl is TABLE of TYP_l10n_fisc_attrib_rec INDEX BY BINARY_INTEGER;

--------------------------------------------------------------------------------------------
-- Function Name: TRIB_SUBS_EXISTS
-- Purpose      : Verify if tributary substitution exists.
---------------------------------------------------------------------------------------------
FUNCTION TRIB_SUBS_EXISTS(O_error_message  IN OUT   VARCHAR2,
                          O_exists         IN OUT   BOOLEAN,
                          I_entity         IN       L10N_BR_ENTITY_TRIB_SUBS.ENTITY%TYPE,
                          I_entity_type    IN       L10N_BR_ENTITY_TRIB_SUBS.ENTITY_TYPE%TYPE,
                          I_country        IN       L10N_BR_ENTITY_TRIB_SUBS.COUNTRY_ID%TYPE,
                          I_state          IN       L10N_BR_ENTITY_TRIB_SUBS.STATE%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: TRIB_SUBS_EXISTS
-- Purpose      : Verify if tributary substitution exists. This function is for validation
---------------------------------------------------------------------------------------------
FUNCTION TRIB_SUBS_EXISTS(O_error_message IN OUT VARCHAR2,
                          O_exists        IN OUT BOOLEAN,
                          I_entity        IN     L10N_BR_ENTITY_TRIB_SUBS.ENTITY%TYPE,
                          I_entity_type   IN     L10N_BR_ENTITY_TRIB_SUBS.ENTITY_TYPE%TYPE,
                          I_country       IN     L10N_BR_ENTITY_TRIB_SUBS.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: CNAE_CODES_EXISTS
-- Purpose      : Verify if cnae codes exists.
---------------------------------------------------------------------------------------------
FUNCTION CNAE_CODES_EXISTS(O_error_message  IN OUT   VARCHAR2,
                           O_exists         IN OUT   BOOLEAN,
                           I_entity         IN       L10N_BR_ENTITY_CNAE_CODES.KEY_VALUE_1%TYPE,
                           I_entity_type    IN       L10N_BR_ENTITY_CNAE_CODES.KEY_VALUE_2%TYPE,
                           I_module         IN       L10N_BR_ENTITY_CNAE_CODES.MODULE%TYPE,
                           I_country        IN       L10N_BR_ENTITY_CNAE_CODES.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: FISCAL_ATTRIB_EXISTS
-- Purpose      : Verify if exist fiscal attributes.
---------------------------------------------------------------------------------------------
FUNCTION FISCAL_ATTRIB_EXISTS(O_error_message  IN OUT   VARCHAR2,
                              IO_l10n_obj      IN OUT   L10N_OBJ)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DEL_ENT_TRIB_SUBS
-- Purpose      : This function will delete from l10n_br_entity_trib_subs table for a given
--                store, warehouse or supplier.
--------------------------------------------------------------------------------------------
FUNCTION DEL_ENT_TRIB_SUBS(O_error_message  IN OUT   VARCHAR2,
                           IO_l10n_obj      IN OUT   L10N_OBJ)
   return BOOLEAN;
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_desc           IN OUT   VARCHAR2,
                        I_key_value      IN       VARCHAR2,
                        I_key_type       IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_desc           IN OUT   VARCHAR2,
                            I_key_value      IN       VARCHAR2,
                            I_key_type       IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_JURISDICTION_DESC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_desc            IN OUT   VARCHAR2,
                                I_key_value       IN       VARCHAR2,
                                I_key_type        IN       VARCHAR2,
                                I_key_value_1     IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_desc           IN OUT   VARCHAR2,
                           I_key_value      IN       VARCHAR2,
                           I_key_type       IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_STORE_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_desc           IN OUT   VARCHAR2,
                         I_key_value      IN       VARCHAR2,
                         I_key_type       IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_WH_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_desc           IN OUT   VARCHAR2,
                      I_key_value      IN       VARCHAR2,
                      I_key_type       IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_PARTNER_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_desc           IN OUT   VARCHAR2,
                           I_key_value      IN       VARCHAR2,
                           I_key_type       IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_OUTLOC_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_desc           IN OUT   VARCHAR2,
                          I_key_value      IN       VARCHAR2,
                          I_key_type       IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ENTITY_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_desc           IN OUT   VARCHAR2,
                              I_key_value      IN       VARCHAR2,
                              I_key_type       IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_SOB_DESC (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_desc             IN OUT   VARCHAR2,
                       I_key_value        IN       VARCHAR2,
                       I_key_type         IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
--LFAS Field Validation Functions
---------------------------------------------------------------------------------------------
-- Function Name: GET_NCM_DESC
-- Purpose      : This function will get the NCM description from l10n_br_ncm_codes table
--                for the ncm_code and country_id parameters
---------------------------------------------------------------------------------------------
FUNCTION GET_NCM_DESC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_ncm_desc       IN OUT   L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                      I_ncm_code       IN       L10N_BR_NCM_CODES.NCM_CODE%TYPE,
                      I_country_id     IN       L10N_BR_NCM_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_NCMCHAR_DESC
-- Purpose      : This function will get the ncm_char_desc from l10n_br_ncm_char_codes table
--                for the ncm_code,ncmchar_code and country_id parameters
---------------------------------------------------------------------------------------------
FUNCTION GET_NCMCHAR_DESC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_ncmchar_desc    IN OUT  L10N_BR_NCM_CHAR_CODES.NCM_CHAR_DESC%TYPE,
                          O_ncm_desc        IN OUT  L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                          I_ncmchar_code    IN      L10N_BR_NCM_CHAR_CODES.NCM_CHAR_CODE%TYPE,
                          IO_ncm_code       IN OUT  L10N_BR_NCM_CHAR_CODES.NCM_CODE%TYPE,
                          I_country_id      IN      L10N_BR_NCM_CHAR_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: MERG_INTO_NCM_CODES
-- Purpose      : This function will insert/update into l10n_br_ncm_codes table
---------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_NCM_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: MERG_INTO_NCM_CHAR_CODES
-- Purpose      : This function will insert/update into l10n_br_ncm_char_codes table
---------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_NCM_CHAR_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_IPI_DESC
-- Purpose      : This function will get the ipi_desc from l10n_br_ncm_ipi_codes table
--                for the ncm_code,ipi_code and country_id parameters
---------------------------------------------------------------------------------------------
FUNCTION GET_IPI_DESC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_ipi_desc        IN OUT  L10N_BR_NCM_IPI_CODES.IPI_DESC%TYPE,
                      O_ncm_desc        IN OUT  L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                      I_ipi_code        IN      L10N_BR_NCM_IPI_CODES.IPI_CODE%TYPE,
                      IO_ncm_code       IN OUT  L10N_BR_NCM_IPI_CODES.NCM_CODE%TYPE,
                      I_country_id      IN      L10N_BR_NCM_IPI_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: MERG_INTO_NCM_IPI_CODES
-- Purpose      : This function will insert/update into l10n_br_ncm_ipi_codes table
---------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_NCM_IPI_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_PAUTA_DESC
-- Purpose      : This function will get the pauta_desc from l10n_br_ncm_pauta_codes table
--                for the ncm_code,pauta_code, ncmchar_code and country_id parameters
---------------------------------------------------------------------------------------------
FUNCTION GET_PAUTA_DESC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_pauta_desc      IN OUT  L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_DESC%TYPE,
                        O_ncmchar_desc    IN OUT  L10N_BR_NCM_CHAR_CODES.NCM_CHAR_DESC%TYPE,
                        O_ncm_desc        IN OUT  L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                        I_pauta_code      IN      L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_CODE%TYPE,
                        IO_ncm_code       IN OUT  L10N_BR_NCM_PAUTA_CODES.NCM_CODE%TYPE,
                        IO_ncmchar_code   IN OUT  L10N_BR_NCM_PAUTA_CODES.NCM_CHAR_CODE%TYPE,
                        I_country_id      IN      L10N_BR_NCM_PAUTA_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: MERG_INTO_NCM_PAUTA_CODES
-- Purpose      : This function will insert/update into l10n_br_ncm_pauta_codes table
---------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_NCM_PAUTA_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_SERVCODE_DESC
-- Purpose      : This function will get the mastersaf_service_desc from
--                l10n_br_mastersaf_svc_codes table for the service_code parameter
---------------------------------------------------------------------------------------------
FUNCTION GET_SERVCODE_DESC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_service_desc   IN OUT   L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_DESC%TYPE,
                           I_service_code   IN       L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_CODE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: MERG_INTO_MASSERV
-- Purpose      : This function will insert/update into l10n_br_mastersaf_svc_codes table
---------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_MASSERV(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_CNAE_DESC
-- Purpose      : This function will get the cnae_desc from l10n_br_cnae_codes table
--                for the cnae_code parameter
---------------------------------------------------------------------------------------------
FUNCTION GET_CNAE_DESC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cnae_desc      IN OUT   L10N_BR_CNAE_CODES.CNAE_DESC%TYPE,
                       I_cnae_code      IN       L10N_BR_CNAE_CODES.CNAE_CODE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: MERG_INTO_CNAE_CODES
-- Purpose      : This function will insert/update into l10n_br_cnae_codes table
---------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_CNAE_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_FEDSERV_DESC
-- Purpose      : This function will get the federal_service_desc from l10n_br_federal_svc_codes table
--                for the fedserv_code parameter
---------------------------------------------------------------------------------------------
FUNCTION GET_FEDSERV_DESC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_fedserv_desc   IN OUT   L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_DESC%TYPE,
                          I_fedserv_code   IN       L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_CODE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: MERG_INTO_FEDSERV
-- Purpose      : This function will insert/update into l10n_br_federal_svc_codes table
---------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_FEDSERV(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_STATE
-- Purpose      : This function will check if the state ID passed exists in the
--                state table, retrieves the descrtiption, and defaults the state's
--                country.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STATE(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_state_desc     IN OUT   STATE.DESCRIPTION%TYPE,
                        O_country_desc   IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                        IO_country       IN OUT   STATE.COUNTRY_ID%TYPE,
                        I_jurisdiction   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                        I_state          IN       STATE.STATE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: CHK_REG_NUM_GRP
-- Purpose : This function checks the required registration number information depending on the
--           entity's taxpayer type
---------------------------------------------------------------------------------------------
FUNCTION CHK_REG_NUM_GRP(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_taxpayer_type  IN       VARCHAR2,
                         I_cnpj           IN       VARCHAR2,
                         I_ie             IN       VARCHAR2,
                         I_im             IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: CHK_SUPP_REG_NUM_GRP
-- Purpose : This function checks the required registration number information depending on the
--           supplier's taxpayer type
---------------------------------------------------------------------------------------------
FUNCTION CHK_SUPP_REG_NUM_GRP(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_taxpayer_type  IN       VARCHAR2,
                              I_cnpj           IN       VARCHAR2,
                              I_ie             IN       VARCHAR2,
                              I_im             IN       VARCHAR2,
                              I_suframa        IN       VARCHAR2,
                              I_cpf            IN       VARCHAR2,
                              I_nit            IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: CHK_PTNR_REG_NUM_GRP
-- Purpose : This function checks the required registration number information depending on the
--           partner's taxpayer type
---------------------------------------------------------------------------------------------
FUNCTION CHK_PTNR_REG_NUM_GRP(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_taxpayer_type  IN       VARCHAR2,
                              I_cnpj           IN       VARCHAR2,
                              I_ie             IN       VARCHAR2,
                              I_im             IN       VARCHAR2,
                              I_cpf            IN       VARCHAR2,
                              I_nit            IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: CHK_FISCAL_CLASS_GRP
-- Purpose : This function checks the required fiscal classification information depending on the
--           entity's taxpayer type
---------------------------------------------------------------------------------------------
FUNCTION CHK_FISCAL_CLASS_GRP(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_taxpayer_type  IN       VARCHAR2,
                              I_ipi_ind        IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: CHK_SUPP_FISCAL_CLASS_GRP
-- Purpose : This function checks the required fiscal classification information depending on the
--           supplier's taxpayer type
---------------------------------------------------------------------------------------------
FUNCTION CHK_SUPP_FISCAL_CLASS_GRP(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_taxpayer_type    IN       VARCHAR2,
                                   I_ipi_ind          IN       VARCHAR2,
                                   I_iss_contrib_ind  IN       VARCHAR2,
                                   I_rural_prod_ind   IN       VARCHAR2,
                                   I_simples_ind      IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
-- LFAS Entity Validation Functions
---------------------------------------------------------------------------------------------
-- Function : CHK_STORE_ENT
-- Purpose  : This function will check required fields for 'Corporate' tax payer type at entity level.
--            Applicable entity: Store.
------------------------------------------------------------------------------

FUNCTION CHK_STORE_ENT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_taxpayer_type  IN       VARCHAR2,
                       I_cnpj           IN       VARCHAR2,
                       I_ie             IN       VARCHAR2,
                       I_im             IN       VARCHAR2,
                       I_ipi_ind        IN       VARCHAR2,
                       I_country        IN       VARCHAR2,
                       I_store          IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function : CHK_SOB_TSFE_STR_OUTL_WH_ENT
-- Purpose  : This function will check required fields for 'Corporate' tax payer type at entity level.
--            Applicable entities: Tsfentity,Outlocs and partner.
---------------------------------------------------------------------------------------------

FUNCTION CHK_SOB_TSFE_STR_OUTL_WH_ENT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_taxpayer_type  IN       VARCHAR2,
                                      I_cnpj           IN       VARCHAR2,
                                      I_ie             IN       VARCHAR2,
                                      I_im             IN       VARCHAR2,
                                      I_ipi_ind        IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function : CHK_OUTL_ENT
-- Purpose  : This function will check required fields for 'Corporate' tax payer type at entity level
--            as well as the cnae codes..
--            Applicable entities: Outlocation
---------------------------------------------------------------------------------------------

FUNCTION CHK_OUTL_ENT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_taxpayer_type      IN       VARCHAR2,
                      I_cnpj               IN       VARCHAR2,
                      I_ie                 IN       VARCHAR2,
                      I_im                 IN       VARCHAR2,
                      I_ipi_ind            IN       VARCHAR2,
                      I_outloc_type        IN       VARCHAR2,
                      I_outloc_id          IN       VARCHAR2,
                      I_country            IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function : CHK_WH_ENT_POP_VWH
-- Purpose  : This function will check entity level validation for wh and will populate the
--            fiscal attribute of wh to its virtual wh
------------------------------------------------------------------------------
FUNCTION CHK_WH_ENT_POP_VWH(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wh                IN       VARCHAR2,
                            I_country_id        IN       VARCHAR2,
                            I_taxpayer_type     IN       VARCHAR2,
                            I_cnpj              IN       VARCHAR2,
                            I_ie                IN       VARCHAR2,
                            I_im                IN       VARCHAR2,
                            I_ipi_ind           IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function : CHK_COMP_STR_OUTL_PTNR_ENT
-- Purpose  : This function will chcek required fields depending on tax payer type.
--            Applicable entity: Partner.
------------------------------------------------------------------------------
FUNCTION CHK_PTNR_ENT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_taxpayer_type  IN     VARCHAR2,
                      I_cnpj           IN     VARCHAR2,
                      I_ie             IN     VARCHAR2,
                      I_im             IN     VARCHAR2,
                      I_ipi_ind        IN     VARCHAR2,
                      I_cpf            IN     VARCHAR2,
                      I_nit            IN     VARCHAR2,
                      I_partner_type   IN     VARCHAR2,
                      I_partner_id     IN     VARCHAR2,
                      I_country        IN     VARCHAR2)
RETURN BOOLEAN;                     
------------------------------------------------------------------------------
-- Function : CHK_COMP_STR_OUTL_PTNR_ENT
-- Purpose  : This function will chcek required fields depending on tax payer type.
--            Applicable entity: SUPS.
------------------------------------------------------------------------------
FUNCTION CHK_SUPS_ENT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_taxpayer_type    IN       VARCHAR2,
                      I_cnpj             IN       VARCHAR2,
                      I_ie               IN       VARCHAR2,
                      I_im               IN       VARCHAR2,
                      I_suframa          IN       VARCHAR2,
                      I_ipi_ind          IN       VARCHAR2,
                      I_iss_contrib_ind  IN       VARCHAR2,
                      I_cpf              IN       VARCHAR2,
                      I_nit              IN       VARCHAR2,
                      I_rural_prod_ind   IN       VARCHAR2,
                      I_simples_ind      IN       VARCHAR2,
                      I_country          IN       VARCHAR2,
                      I_supplier         IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function : DEFAULT_FISCAL_ATTRIB
-- Purpose  : This function will default fiscal attribute values for virtual warehouses.
--            Applicable entity: LOC.
------------------------------------------------------------------------------
FUNCTION DEFAULT_FISCAL_ATTRIB(O_error_message  IN OUT   VARCHAR2,
                               IO_l10n_obj      IN OUT   L10N_OBJ)
RETURN BOOLEAN;
------------------------------------------------------------------------------
FUNCTION VALIDATE_JURISDICTION (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_jurisdiction_desc IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                O_state_desc        IN OUT   STATE.DESCRIPTION%TYPE,
                                O_country_desc      IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                                IO_state            IN OUT   STATE.STATE%TYPE,
                                IO_country          IN OUT   STATE.COUNTRY_ID%TYPE,
                                I_jurisdiction      IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_country_desc    IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                          I_country         IN       COUNTRY.COUNTRY_ID%TYPE,
                          I_jurisdiction    IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                          I_state           IN       STATE.STATE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function : DEFAULT_ADDRESS_TO_ATTRIB
-- Purpose  : This function will default address fields when called from l10nflex form.
--            Applicable entity: STORE, STORE_ADD, WH, SUPS, PTNR.
-------------------------------------------------------------------------------
FUNCTION DEFAULT_ADDRESS_TO_ATTRIB(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_add_1              IN OUT ADDR.ADD_1%TYPE,
                                   IO_add_2              IN OUT ADDR.ADD_2%TYPE,
                                   IO_add_3              IN OUT ADDR.ADD_3%TYPE,
                                   IO_state              IN OUT ADDR.STATE%TYPE,
                                   IO_state_desc         IN OUT STATE.DESCRIPTION%TYPE,
                                   IO_country_id         IN OUT ADDR.COUNTRY_ID%TYPE,
                                   IO_country_desc       IN OUT COUNTRY.COUNTRY_DESC%TYPE,
                                   IO_post               IN OUT ADDR.POST%TYPE,
                                   IO_jurisdiction_code  IN OUT ADDR.JURISDICTION_CODE%TYPE,
                                   IO_jurisdiction_desc  IN OUT COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                   I_key_value_1         IN     ADDR.KEY_VALUE_1%TYPE,
                                   I_key_value_2         IN     ADDR.KEY_VALUE_2%TYPE,
                                   I_module              IN     ADDR.MODULE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function  :  CHK_ADDR_UPDATE
--Purpose   :  This function will check if the address is modified
--             and the same is not updated in corresponding l10n_ext table.
---------------------------------------------------------------------------------
FUNCTION CHK_ADDR_UPDATE(O_error_msg         IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_exists           IN  OUT VARCHAR2,
                         I_group_id          IN      L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                         I_addr_1            IN      ADDR.ADD_1%TYPE,
                         I_addr_2            IN      ADDR.ADD_2%TYPE,
                         I_addr_3            IN      ADDR.ADD_3%TYPE,
                         I_jurisdiction_code IN      ADDR.JURISDICTION_CODE%TYPE,
                         I_state             IN      ADDR.STATE%TYPE,
                         I_country           IN      ADDR.COUNTRY_ID%TYPE,
                         I_postal_code       IN      ADDR.POST%TYPE,
                         I_where_string      IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------
--Function  :  ADDR_UPDATE_L10NEXT
--Purpose   :  This function will update the corresponding l10n_ext table
--             if the address is modified.
---------------------------------------------------------------------------------
FUNCTION ADDR_UPDATE_L10N_EXT(O_error_msg    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_l10n_obj    IN OUT   L10N_OBJ)
RETURN BOOLEAN;

END L10N_BR_FND_SQL;
/