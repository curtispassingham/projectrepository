CREATE OR REPLACE PACKAGE FISCAL_CLASSIFICATION_SQL AS
---------------------------------------------------------------------------------------------
-- Define errors so procedures can raise_application_error
ERRNUM_PACKAGE_CALL        NUMBER(6) := -20020;
ERRNUM_INVALID_PARAM       NUMBER(6) := -20021;
ERRNUM_OTHERS              NUMBER(6) := -20022;
---
ERROR_PACKAGE_CALL         EXCEPTION;
ERROR_INVALID_PARAM        EXCEPTION;
ERROR_OTHERS               EXCEPTION;
RECORD_LOCKED              EXCEPTION;
---
TYPE FISCAL_CLASS_REC IS RECORD (country_id                 L10N_BR_NCM_CODES.COUNTRY_ID%TYPE,
                                 fiscal_classification_id   L10N_BR_NCM_CODES.NCM_CODE%TYPE,
                                 fiscal_classification_desc L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                                 fiscal_classification_type VARCHAR2(1),
                                 error_message              RTK_ERRORS.RTK_TEXT%TYPE,
                                 return_code                VARCHAR2(5));

TYPE FISCAL_CLASS_TBL IS TABLE OF FISCAL_CLASS_REC INDEX BY BINARY_INTEGER;
-----------------------------------------------------------------------------------------------------------
-- P_GET_FISCAL_CLASSIFICATION
-- This procedure will get a collection of fiscal records.It must be a
-- procedure because a form block is based on it.
-----------------------------------------------------------------------------------------------------------
PROCEDURE P_GET_FISCAL_CLASSIFICATION(O_FISCLASS_TABLE               IN OUT   FISCAL_CLASS_TBL,
                                      I_country_id                   IN       L10N_BR_NCM_CODES.COUNTRY_ID%TYPE,
                                      I_fiscal_classification_id     IN       L10N_BR_NCM_CODES.NCM_CODE%TYPE,
                                      I_fiscal_classification_type   IN       CODE_DETAIL.CODE%TYPE);
-----------------------------------------------------------------------------------------------------------
-- P_UPD_FISCAL_CLASSIFICATION
-- This procedure will update the respective table data based on the updated record in the collection.
-----------------------------------------------------------------------------------------------------------
PROCEDURE P_UPD_FISCAL_CLASSIFICATION(I_fisclass_table           IN       FISCAL_CLASS_TBL);
-----------------------------------------------------------------------------------------------------------
-- P_LOCK_FISCAL_CLASS
-- This procedure will lock the respective table data which needs to be updated.
-----------------------------------------------------------------------------------------------------------
PROCEDURE P_LOCK_FISCAL_CLASS(I_fisclass_table           IN   FISCAL_CLASS_TBL);
-----------------------------------------------------------------------------------------------------------
-- P_INSRT_FISCAL_CLASS
-- This procedure will insert record/s into the respective table data based on the type.
-----------------------------------------------------------------------------------------------------------
PROCEDURE P_INSRT_FISCAL_CLASS(I_fisclass_table           IN   FISCAL_CLASS_TBL);
-----------------------------------------------------------------------------------------------------------
END FISCAL_CLASSIFICATION_SQL;
/