CREATE OR REPLACE PACKAGE BODY FM_REPORTS_SQL AS

-------------------------------------------------------------------------------------------------
FUNCTION GET_MODE_TYPE(I_schedule_no IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   RETURN VARCHAR2 IS

   L_mode_type       FM_SCHEDULE.MODE_TYPE%TYPE;
   P_mode_type       CODE_DETAIL.CODE_DESC%TYPE;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE :=NULL;

BEGIN
   if FM_SCHEDULE_SQL.GET_MODE_TYPE(L_error_message,
                                    L_mode_type,
                                    I_schedule_no) = FALSE then
      return NULL;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                 'FMMO',
                                 L_mode_type,
                                 P_mode_type) = FALSE then
      return NULL;

   end if;
   return P_mode_type;
EXCEPTION
   when OTHERS then
      return NULL;
END;
-------------------------------------------------------------------------------------------------
FUNCTION GET_CITY_DESC(I_city IN COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
   RETURN VARCHAR2 IS
   ---
   O_city_desc COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   ---
   cursor CITY_DESC is
      select jurisdiction_desc
        from country_tax_jurisdiction
       where jurisdiction_code = I_city;
   ---
BEGIN
   ---
   open  CITY_DESC;
   fetch CITY_DESC into O_city_desc;
   close CITY_DESC;
   ---
   return O_city_desc;
   ---
EXCEPTION
   when OTHERS then
      return NULL;
END GET_CITY_DESC;
-------------------------------------------------------------------------------------------------
FUNCTION GET_MESSAGE(I_fiscal_doc_line_id       IN     FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   RETURN VARCHAR2 IS
   ---
   O_message      FM_FISCAL_DOC_TAX_DETAIL.LEGAL_MESSAGE_TEXT%TYPE := NULL;
   ---
   cursor C_MESSAGE is
      select legal_message_text
        from fm_fiscal_doc_tax_detail
       where fiscal_doc_line_id = I_fiscal_doc_line_id;

BEGIN
   FOR msg in C_MESSAGE LOOP
      O_message := O_message||msg.legal_message_text;
   END LOOP;

   return O_message;
   ---
EXCEPTION
   when OTHERS then
      return NULL;
END GET_MESSAGE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_CNPJ(I_cnpj IN V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   RETURN VARCHAR2 IS

   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE :=NULL;
   L_cnpj           VARCHAR2(50);

BEGIN
   if FM_FISCAL_DOCUMENT_SQL.GET_MASK_CNPJ(L_error_message,
                                           L_cnpj,
                                           I_cnpj) = FALSE then
      return -1;
   end if;

   return L_cnpj;
EXCEPTION
   when OTHERS then
      return NULL;
END;
-------------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_NO_SERVICE(I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   RETURN VARCHAR2 IS

   L_error_message RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_total         NUMBER(20,4);

BEGIN
   if FM_FISCAL_DOCUMENT_SQL.GET_TOTAL_SERVICE(L_error_message,
                                               L_total,
                                               I_fiscal_doc_id,
                                               'N') = FALSE then
      return -1;
   end if;

   return L_total;
EXCEPTION
   when OTHERS then
      return NULL;

END;
-------------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_SERVICE(I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   RETURN VARCHAR2 IS

   L_error_message RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_total         NUMBER(20,4);

BEGIN
   if FM_FISCAL_DOCUMENT_SQL.GET_TOTAL_SERVICE(L_error_message,
                                               L_total,
                                               I_fiscal_doc_id,
                                               'Y') = FALSE then
      return -1;
   end if;

   return L_total;
EXCEPTION
   when OTHERS then
      return NULL;

END;
-------------------------------------------------------------------------------------------------
FUNCTION GET_CORR_DOC_COUNT(I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   RETURN NUMBER IS

   L_count                       NUMBER(1):= 0;

   cursor C_CORR_DOC_COUNT is
      select count(*)
        from v_fm_correction_doc
       where fiscal_doc_id = I_fiscal_doc_id;

BEGIN

   open C_CORR_DOC_COUNT;
   fetch C_CORR_DOC_COUNT into L_count;
   close C_CORR_DOC_COUNT;

   return L_count;

EXCEPTION
   when OTHERS then
      return 0;

END GET_CORR_DOC_COUNT;
-------------------------------------------------------------------------------------------------
FUNCTION GET_COMPANY_HEADER
   RETURN VARCHAR2 IS

   L_company                     COMPHEAD.CO_NAME%TYPE;

   cursor C_COMPANY is
     select co_name
       from comphead;

BEGIN

   open C_COMPANY;
   fetch C_COMPANY into L_company;
   close C_COMPANY;

   return L_company;

EXCEPTION
   when OTHERS then
      return -1;

END GET_COMPANY_HEADER;
-------------------------------------------------------------------------------------------------
FUNCTION GET_LEGENT_CNPJ(I_loc      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                         I_loc_type IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   RETURN VARCHAR2 IS

   L_legent_cnpj                     V_FISCAL_ATTRIBUTES.CNPJ%TYPE;

   cursor C_LEGENT_CNPJ is
      select distinct NVL(vfa.cnpj,vfa.cpf) CNPJ
        from v_fiscal_attributes vfa, 
             tsf_entity te,
             country_tax_jurisdiction c,
             country co,
             system_options so,
             wh w,
             store st
       where vfa.module      = 'TSFENT'
         and vfa.key_value_1 = te.tsf_entity_id
         and ((NVL(I_loc_type,'X')   = 'W'
                and te.tsf_entity_id = w.tsf_entity_id
                and w.wh             = I_loc)
              or(NVL(I_loc_type,'X')  = 'S'
                 and te.tsf_entity_id = st.tsf_entity_id
                 and st.store         = I_loc))
         and c.Jurisdiction_code = vfa.city
         and c.state             = vfa.state
         and c.country_id        = co.country_id
         and co.country_id       = vfa.country_id
         and co.country_id       = 'BR'
         and NVL(so.intercompany_transfer_basis,'X') = 'T'
      union all
      select distinct NVL(vfa.cnpj,vfa.cpf) CNPJ
        from v_fiscal_attributes vfa,
             fif_gl_setup fgls,
             country_tax_jurisdiction c,
             country co,
             system_options so
       where vfa.module='SOB'
         and vfa.key_value_1=fgls.set_of_books_id
         and c.Jurisdiction_code=vfa.city
         and c.state = vfa.state
         and c.country_id = co.country_id
         and co.country_id=vfa.country_id
         and co.country_id = 'BR'
         and NVL(so.intercompany_transfer_basis,'X') = 'B';


BEGIN

   open C_LEGENT_CNPJ;
   fetch C_LEGENT_CNPJ into L_legent_cnpj;
   close C_LEGENT_CNPJ;

   return L_legent_cnpj;

EXCEPTION
   when OTHERS then
      return -1;

END GET_LEGENT_CNPJ;
-------------------------------------------------------------------------------------------------
FUNCTION GET_UOM(I_item IN ITEM_MASTER.ITEM%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE         := NULL;
   L_standard_uom                UOM_CLASS.UOM%TYPE               := NULL;
   L_standard_class              UOM_CLASS.UOM_CLASS%TYPE         := NULL;
   L_uom_conv_factor             ITEM_MASTER.UOM_CONV_FACTOR%TYPE := NULL;

BEGIN
   if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(L_error_message,
                                       L_standard_uom,
                                       L_standard_class,
                                       L_uom_conv_factor,
                                       I_item,
                                       NULL) = FALSE then
      return -1;
   end if;

   return L_standard_uom;

EXCEPTION
   when OTHERS then
      return -1;

END GET_UOM;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DESC (I_item IN ITEM_MASTER.ITEM%TYPE)
   RETURN VARCHAR2 IS

   L_item_desc                   ITEM_MASTER.ITEM_DESC%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if ITEM_ATTRIB_SQL.GET_DESC(L_error_message,
                               L_item_desc,
                               I_item) = FALSE then
      return -1;
   else
      return L_item_desc;
   end if;

EXCEPTION
   when OTHERS then
      return -1;

END GET_ITEM_DESC;
-------------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_PARTNER_NAME(I_supplier     IN SUPS.SUPPLIER%TYPE,
                               I_partner_id   IN PARTNER.PARTNER_ID%TYPE,
                               I_partner_type IN PARTNER.PARTNER_TYPE%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_sup_name                    SUPS.SUP_NAME%TYPE;
   L_partner_desc                PARTNER.PARTNER_DESC%TYPE;

BEGIN

   if I_supplier is not NULL then
      if SUPP_ATTRIB_SQL.GET_SUPP_DESC(L_error_message,
                                       I_supplier,
                                       L_sup_name) = FALSE then
         return -1;
      end if;
      return L_sup_name;
   elsif I_partner_id is not NULL then
      if PARTNER_SQL.GET_DESC(L_error_message,
                              L_partner_desc,
                              I_partner_id,
                              I_partner_type) = FALSE then
         return -1;
      end if;
      return L_partner_desc;
   end if;

EXCEPTION
   when OTHERS then
      return -1;

END GET_SUPP_PARTNER_NAME;
-------------------------------------------------------------------------------------------------
FUNCTION GET_LOC_NAME(I_loc_type IN ITEM_LOC.LOC_TYPE%TYPE,
                      I_loc      IN ITEM_LOC.LOC%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_loc_name                    PARTNER.PARTNER_DESC%TYPE;
   L_currency_code               CURRENCIES.CURRENCY_CODE%TYPE;
   L_partner_type                PARTNER.PARTNER_TYPE%TYPE := 'E';
   L_status                      PARTNER.STATUS%TYPE;
   L_principle_country_id        PARTNER.PRINCIPLE_COUNTRY_ID%TYPE;
   L_mfg_id                      PARTNER.MFG_ID%TYPE;
   L_lang                        PARTNER.LANG%TYPE;

BEGIN

   if I_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_NAME(L_error_message,
                                   I_loc,
                                   L_loc_name) = FALSE then
         return -1;
      end if;
   elsif I_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_NAME(L_error_message,
                                I_loc,
                                L_loc_name) = FALSE then
         return -1;
      end if;
   elsif I_loc_type = 'E' then
      if PARTNER_SQL.GET_INFO(L_error_message,
                              L_loc_name,
                              L_currency_code,
                              L_status,
                              L_principle_country_id,
                              L_mfg_id,
                              L_lang,
                              I_loc,
                              L_partner_type) = FALSE then
         return -1;
      end if;
   else
      return -1;
   end if;

   return L_loc_name;

EXCEPTION
   when OTHERS then
      return -1;

END GET_LOC_NAME;
-------------------------------------------------------------------------------------------------
END FM_REPORTS_SQL;
/
