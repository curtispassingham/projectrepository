CREATE OR REPLACE PACKAGE FM_FISCAL_ADDINF_SQL as
----------------------------------------------------------------------------------
-- CREATE DATE - 03-05-2007
-- CREATE USER ? Sigma.EPP
-- PROJECT / FRD - 10481 / 10481_ORFMI_FRD_001
-- DESCRIPTION ? Package associated to form orfm_fiscal_addinf.fmb
----------------------------------------------------------------------------------
-- 001 - Begin
-- CREATE DATE      - 26-06-2008
-- CREATE USER      - BCA
-- PROJECT / BUGFIX - 10481 / Issue 80 Loc
-- DESCRIPTION      ?
-- 001 - End
----------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Function Name: GET_QUERY_TYPE_TAXES
-- Purpose:       This function gets the query for type of taxes to NF.
----------------------------------------------------------------------------------
FUNCTION GET_QUERY_TYPE_TAXES(O_error_message    IN OUT VARCHAR2,
                              O_query            IN OUT VARCHAR2)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: UNIQUE_COMP_DOC
-- Purpose:       This function checks if NF has a unique complement doc.
----------------------------------------------------------------------------------
FUNCTION UNIQUE_COMP_DOC (O_error_message     IN OUT VARCHAR2,
                          O_unique            IN OUT BOOLEAN,
                          O_fiscal_doc_id     IN OUT FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE,
                          O_fiscal_doc_no     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_fiscal_doc_id     IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE,
                          I_complementary_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_COMP_FISCAL_DOC_NO
-- Purpose:       This function returns the query for record_group REC_COMPLEMENT_NF.
----------------------------------------------------------------------------------
FUNCTION LOV_COMP_FISCAL_DOC_NO(O_error_message     IN OUT VARCHAR2,
                                O_query             IN OUT VARCHAR2,
                                I_fiscal_doc_no     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                I_complementary_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                                I_location          IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                I_loc_type          IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                I_requisition_type  IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_COMP_FISCAL_DOC_NO
-- Purpose:       This function validates the complement doc number.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_COMP_FISCAL_DOC_NO(O_error_message      IN OUT VARCHAR2,
                                     O_exists             IN OUT BOOLEAN,
                                     O_comp_fiscal_doc_id IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                     I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                     I_complementary_ind  IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                                     I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                     I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                     I_requisition_type   IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_LEGAL_MESSAGE
-- Purpose:       This function validate de document legal message.
----------------------------------------------------------------------------------
/*
FUNCTION VALIDATE_LEGAL_MESSAGE(O_error_message      IN OUT VARCHAR2,
                                O_exists             IN OUT BOOLEAN,
                                O_message_id         IN OUT FM_LEGAL_MESSAGE.MESSAGE_ID%TYPE,
                                I_message_text       IN     FM_LEGAL_MESSAGE.MESSAGE_TEXT%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_LEGAL_MESSAGE
-- Purpose:       This function validate de document legal message.
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_MESSAGE_LINE(O_error_message IN OUT VARCHAR2,
                               O_message_line  IN OUT FM_FISCAL_DOC_LEGAL_MESSAGE.MESSAGE_LINE%TYPE,
                               I_fiscal_doc_id IN     FM_FISCAL_DOC_LEGAL_MESSAGE.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_LEGAL_MESSAGE
-- Purpose:       This function returns the query for record_group REC_LEGAL_MESSAGE.
----------------------------------------------------------------------------------

FUNCTION LOV_LEGAL_MESSAGE(O_error_message  IN OUT VARCHAR2,
                           O_query          IN OUT VARCHAR2)
   return BOOLEAN;
*/
----------------------------------------------------------------------------------
-- Function Name: CHANGE_COMPL_DOC
-- Purpose:       This function returns the query for record_group REC_LEGAL_MESSAGE.
----------------------------------------------------------------------------------
FUNCTION CHANGE_COMPL_DOC(O_error_message       IN OUT VARCHAR2,
                          I_complementary_ind   IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                          I_compl_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE,
                          I_fiscal_doc_id       IN     FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_PERC_VALUE
-- Purpose:       This function returns the query for record_group REC_CALC_BASE_IND.
----------------------------------------------------------------------------------
FUNCTION LOV_PERC_VALUE(O_error_message  IN OUT VARCHAR2,
                        O_query          IN OUT VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_COMP_FREIGHT_FISCAL_DOC_NO
-- Purpose:       This function returns the query for record_group REC_COMPLEMENT_NF.
----------------------------------------------------------------------------------
FUNCTION LOV_COMP_FREIGHT_FISCAL_DOC_NO(O_error_message     IN OUT VARCHAR2,
                                        O_query             IN OUT VARCHAR2,
                                        I_fiscal_doc_no     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                        I_location          IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                        I_loc_type          IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                        I_requisition_type  IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
END FM_FISCAL_ADDINF_SQL;
/