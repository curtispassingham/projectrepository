CREATE or REPLACE PACKAGE BODY FM_UTILIZATION_SQL is
------------------------------------------------------------------------------------
FUNCTION P_SET_WHERE_CLAUSE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_where_clause       IN OUT VARCHAR2,
                            I_utilization        IN     VARCHAR2)
return BOOLEAN is

      L_program   VARCHAR2(80)   := 'ORFM_UTILIZATION_SQL.P_SET_WHERE_CLAUSE';
      L_text      VARCHAR2(2000) := NULL;
BEGIN
   ---
   if I_utilization is NOT NULL then
      L_text := L_text || ' status = ''A'' and utilization_id = ''' || I_utilization || ''' and ';
   ---
   else
      L_text := L_text || ' status = ''A'' and ';
   end if;
   ---
   if L_text is NOT NULL then
      L_text := SUBSTR(L_text,1,LENGTH(L_text) - 5);
   end if;
   ---
   O_where_clause := L_text;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END P_SET_WHERE_CLAUSE;
------------------------------------------------------------------------------------
FUNCTION LOV_UTILIZATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_query          IN OUT VARCHAR2,
                         I_where_clause   IN     VARCHAR2)
return BOOLEAN is

   L_program   VARCHAR2(50) := 'ORFM_UTILIZATION_SQL.LOV_UTILIZATION';

BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'Select utilization_desc,'||
                     '    utilization_id '||
                    'from fm_fiscal_utilization '||
                   'where status = ''A'' '||
                   '  and upper(utilization_desc) like '''||upper(I_where_clause)||'%'' '||
                  'union all '||
             'select NVL(tl.translated_value,f.utilization_desc) utilization_desc,'||
                '    utilization_id '||
                'from fm_fiscal_utilization f,tl_shadow tl '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
               '  and UPPER(f.utilization_desc) = tl.key(+) '||
               '  and LANGUAGE_SQL.GET_USER_LANGUAGE = tl.lang(+) '||
               '  and upper(NVL(tl.translated_value,f.utilization_desc)) like '''||upper(I_where_clause)||'%'' '||
               '  and status = ''A'' '||
               '  order by 1';
   else
      O_query := 'Select utilization_desc,'||
                     '    utilization_id '||
                    'from fm_fiscal_utilization '||
                   'where status = ''A'' '||
                  'union all '||
             'select NVL(tl.translated_value,f.utilization_desc) utilization_desc,'||
                '    utilization_id '||
                'from fm_fiscal_utilization f,tl_shadow tl '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
               '  and UPPER(f.utilization_desc) = tl.key(+) '||
               '  and LANGUAGE_SQL.GET_USER_LANGUAGE = tl.lang(+) '||
               '  and status = ''A'' '||
               '  order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOV_UTILIZATION;
------------------------------------------------------------------------------------
FUNCTION P_SET_WHERE_CLAUSE_UTIL_REASON(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_where_clause      IN OUT VARCHAR2,
                                        I_utilization       IN     VARCHAR2)
return BOOLEAN is

   L_program   VARCHAR2(80)   := 'ORFM_UTILIZATION_SQL.P_SET_WHERE_CLAUSE_UTIL_REASON';
   L_text      VARCHAR2(2000) := NULL;
BEGIN
   ---
   if I_utilization is NOT NULL then
      L_text := L_text || ' utilization_id = ''' || I_utilization || ''' and ';
   end if;
   ---
   if L_text is NOT NULL then
      L_text := SUBSTR(L_text,1,LENGTH(L_text) - 5);
   end if;
   ---
   O_where_clause := L_text;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END P_SET_WHERE_CLAUSE_UTIL_REASON;
------------------------------------------------------------------------------------
FUNCTION P_CREATE_FM_NOP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(64) := 'FM_UTILIZATION_SQL.P_CREATE_FM_NOP';
   L_tax_service_id      L10N_BR_TAX_CALL_RES_FSC_FND.TAX_SERVICE_ID%TYPE := NULL;
BEGIN

   if L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE(O_error_message,
                                                     L_tax_service_id,
                                                     'NOP') = FALSE then
      return FALSE;
   end if;
   ---

   merge into fm_nop fn
       using(select fiscal_code, fiscal_code_description
                from l10n_br_tax_call_res_fsc_fnd
               where tax_service_id = L_tax_service_id) fnt
       on (fn.nop = fnt.fiscal_code)
     when matched then
       update set fn.nop_desc = fnt.fiscal_code_description,
                  last_update_datetime = sysdate,
                  last_update_id = user
     when not matched then
       insert (nop,
               nop_desc,
               create_datetime,
               create_id,
               last_update_datetime,
               last_update_id)
       values (fnt.fiscal_code,
               fnt.fiscal_code_description,
               sysdate,
               user,
               sysdate,
               user);
   ---
   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END P_CREATE_FM_NOP;
------------------------------------------------------------------------------------
FUNCTION P_SET_WHERE_CLAUSE_DOC_PAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_where_clause       IN OUT VARCHAR2,
                                    I_utilization        IN     VARCHAR2)
return BOOLEAN is

   L_program   VARCHAR2(80)   := 'FM_UTILIZATION_SQL.P_SET_WHERE_CLAUSE_ACC';
   L_text      VARCHAR2(2000) := NULL;
BEGIN
   ---
   if I_utilization is NOT NULL then
      L_text := L_text || ' utilization_id = ''' || I_utilization || ''' and ';
   end if;
   ---
   if L_text is NOT NULL then
      L_text := SUBSTR(L_text,1,LENGTH(L_text) - 5);
   end if;
   ---
   O_where_clause := L_text;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END P_SET_WHERE_CLAUSE_DOC_PAR;
------------------------------------------------------------------------------------
END FM_UTILIZATION_SQL;
/

