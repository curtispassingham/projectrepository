CREATE OR REPLACE PACKAGE BODY FM_FISCAL_FIND_SQL is
-----------------------------------------------------------------------------------
FUNCTION LOV_TYPE_ID(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_TYPE_ID';
   ---
BEGIN
   ---
   O_query := 'select type_desc, type_id '||
                'from fm_fiscal_doc_type '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
              'union all '||
              'select NVL(tl_shadow.translated_value, type_desc) type_desc, type_id '||
                'from fm_fiscal_doc_type, tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and UPPER(type_id) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_TYPE_ID;
-----------------------------------------------------------------------------------
FUNCTION LOV_UTILIZATION(O_error_message  IN OUT VARCHAR2,
                         O_query          IN OUT VARCHAR2,
                         I_utilization_id IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_UTILIZATION';
   ---
BEGIN
   ---
   O_query := 'select utilization_id, utilization_desc  '||
                'from fm_fiscal_utilization  '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and status = ''A'' ';
   ---
   if I_utilization_id is NOT NULL then
      O_query := O_query ||
                 'and utilization_id = ''' || I_utilization_id ||''' ';
   end if;
   ---
   O_query := O_query ||
                 'union all '||
                 'select ffu.utilization_id,
                         NVL(tl1.translated_value,ffu.utilization_desc) utilization_desc '||
                 '  from fm_fiscal_utilization ffu, tl_shadow tl1 '||
                 ' where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 '  and UPPER(ffu.utilization_desc) = tl1.key(+) '||
                 '  and LANGUAGE_SQL.GET_USER_LANGUAGE = tl1.lang(+) '||
                 '  and ffu.status = ''A'' ';
   ---
   if I_utilization_id is NOT NULL then
      O_query := O_query ||
                 'and ffu.utilization_id = ''' || I_utilization_id ||''' ';
   end if;
   ---
   O_query := O_query ||
                 'order by 1 ';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_UTILIZATION;

-----------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER(O_error_message      IN OUT VARCHAR2,
                      O_query              IN OUT VARCHAR2,
                      I_supplier_sites_ind IN SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE Default 'N')
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_SUPPLIER';
   ---
BEGIN
   ---
   if I_supplier_sites_ind = 'N' then
      O_query := 'select a.sup_name, a.supplier '||
                 'from sups a, v_br_sups_reg_num b '||
                 'where a.supplier = b.supplier '||
                 'and LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, a.sup_name) sup_name, a.supplier '||
                 'from sups a, v_br_sups_reg_num b, tl_shadow tl_shadow '||
                 'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and a.supplier = b.supplier '||
                 'and UPPER(sup_name) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                 'order by 1';
   else
     O_query := 'select a.sup_name, a.supplier '||
                 'from sups a, v_br_sups_reg_num b '||
                 'where a.supplier_parent IS NOT NULL  '|| 
                 'and a.supplier = b.supplier '||
                 'and LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, a.sup_name) sup_name, a.supplier '||
                 'from sups a, v_br_sups_reg_num b, tl_shadow tl_shadow '||
                 'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and a.supplier_parent IS NOT NULL  '|| 
                 'and a.supplier = b.supplier '||
                 'and UPPER(sup_name) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                 'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_SUPPLIER;
-----------------------------------------------------------------------------------
FUNCTION LOV_PARTNER(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_where_clause   IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_PARTNER';
   ---
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'select a.partner_desc, a.partner_id '||
                   'from partner a, v_br_partner_reg_num b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.partner_id = b.partner_id '||
                    'and a.partner_type = b.partner_type '||
                    'and a.partner_type = '''||I_where_clause||''''||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, a.partner_desc) partner_desc, a.partner_id '||
                   'from partner a, v_br_partner_reg_num b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.partner_id = b.partner_id '||
                    'and a.partner_type = b.partner_type '||
                    'and UPPER(a.partner_id) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and a.partner_type = '''||I_where_clause||''''||
                  'order by 1';
   else
      O_query := 'select a.partner_desc, a.partner_id '||
                   'from partner a, v_br_partner_reg_num b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.partner_id = b.partner_id '||
                    'and a.partner_type = b.partner_type '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, a.partner_desc) partner_desc, a.partner_id '||
                   'from partner a, v_br_partner_reg_num b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.partner_id = b.partner_id '||
                    'and a.partner_type = b.partner_type '||
                    'and UPPER(a.partner_id) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                 'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_PARTNER;
-----------------------------------------------------------------------------------
FUNCTION LOV_LOC(O_error_message  IN OUT VARCHAR2,
                 O_query          IN OUT VARCHAR2,
                 I_where_clause   IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_LOC';
   ---
BEGIN
   ---
   if I_where_clause = 'S' then
      O_query := 'select a.store_name loc_name, a.store loc '||
                   'from store a, v_br_store_reg_num b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.store = b.store '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, a.store_name) loc_name, a.store loc '||
                   'from store a, v_br_store_reg_num b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.store = b.store '||
                    'and UPPER(a.store) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                  'order by 1';
   elsif I_where_clause = 'W' then
      O_query := 'select a.wh_name loc_name, a.wh loc '||
                   'from wh a, v_br_wh_reg_num b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                   'and a.wh = b.wh '||
                  'union all '||
                 'select NVL(tl_shadow.translated_value, a.wh_name) loc_name, a.wh loc '||
                   'from wh a, v_br_wh_reg_num b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.wh = b.wh '||
                    'and UPPER(a.wh) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                   'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_LOC;
-----------------------------------------------------------------------------------
FUNCTION LOV_OUTLOC(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2,
                    I_where_clause   IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_OUTLOC';
   ---
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'select a.outloc_desc, a.outloc_id '||
                   'from outloc a, v_br_outloc_reg_num b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.outloc_type = '''||I_where_clause||''''||
                    'and a.outloc_id = b.outloc_id '||
                    'and a.outloc_type = b.outloc_type '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, a.outloc_desc) outloc_desc, a.outloc_id '||
                   'from outloc a, v_br_outloc_reg_num b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(a.outloc_id) = tl_shadow.key(+) '||
                    'and a.outloc_id = b.outloc_id '||
                    'and a.outloc_type = b.outloc_type '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and a.outloc_type = '''||I_where_clause||''''||
                   'order by 1';
   else
      O_query := 'select a.outloc_desc, a.outloc_id '||
                   'from outloc a, v_br_outloc_reg_num b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.outloc_id = b.outloc_id '||
                    'and a.outloc_type = b.outloc_type '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, a.outloc_desc) outloc_desc, a.outloc_id '||
                   'from outloc a, v_br_outloc_reg_num b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(a.outloc_id) = tl_shadow.key(+) '||
                    'and a.outloc_id = b.outloc_id '||
                    'and a.outloc_type = b.outloc_type '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                   'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_OUTLOC;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ENTITY(O_error_message  IN OUT VARCHAR2,
                         O_description    IN OUT VARCHAR2,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.VALIDATE_ENTITY';
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FISCAL_ENTITY_ATTRIB is
      select 'X'
        from v_fiscal_attributes a
       where a.module = I_module
         and a.key_value_1 = I_key_value_1
         and ((I_key_value_2 is NOT NULL
         and  a.key_value_2 = I_key_value_2)
          or I_key_value_2 is NULL)
      union 
      select 'X'
        from fm_rma_head a
       where I_module = 'CUST'
         and a.cust_id = I_key_value_1
         and ((I_key_value_2 is NOT NULL
         and  I_key_value_2 = 'C')
          or I_key_value_2 is NULL);
   ---
BEGIN
   ---
   if I_module is NOT NULL and
      I_key_value_1 is NOT NULL then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2);
      open C_FISCAL_ENTITY_ATTRIB;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2);
      fetch C_FISCAL_ENTITY_ATTRIB into L_dummy;
      if C_FISCAL_ENTITY_ATTRIB%NOTFOUND then
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_FISCAL_ENTITY_ATTRIB',
                          'V_FISCAL_ATTRIBUTES',
                          'Module = '      || I_module ||
                          ' Key_value_1 = '|| I_key_value_1 ||
                          ' Key_value_2 = '|| I_key_value_2);
         close C_FISCAL_ENTITY_ATTRIB;
         ---
         if I_module = 'SUPP' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_SUP',NULL,NULL,NULL);
         elsif I_module = 'PTNR' then
            O_error_message := SQL_LIB.CREATE_MSG('PARTNER_INVALID',NULL,NULL,NULL);
         elsif I_module = 'LOC' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_LOC',NULL,NULL,NULL);
         elsif I_module = 'OLOC' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_OUTLOC',NULL,NULL,NULL);
         elsif I_module = 'CUST' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_CUST',NULL,NULL,NULL);
         end if;
         ---
         return FALSE;
         ---
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2);
      close C_FISCAL_ENTITY_ATTRIB;
      ---
      if I_module = 'SUPP' then
         ---
         if SUPP_ATTRIB_SQL.GET_SUPP_DESC(O_error_message,
                                          I_key_value_1,
                                          O_description) = FALSE then
            return FALSE;
         end if;
         ---
   	  elsif I_module = 'PTNR' then
         ---
         if PARTNER_SQL.GET_DESC(O_error_message,
                                 O_description,
                                 I_key_value_1,
                                 I_key_value_2) = FALSE then
            return FALSE;
         end if;
         ---
   	  elsif I_module = 'LOC' then
         ---
         if LOCATION_ATTRIB_SQL.GET_NAME(O_error_message,
                                         O_description,
                                         I_key_value_1,
                                         I_key_value_2) = FALSE then
            return FALSE;
         end if;
         ---
      elsif I_module = 'OLOC' then
         ---
         if OUTSIDE_LOCATION_SQL.GET_DESC(O_error_message,
                                          O_description,
                                          I_key_value_1,
                                          I_key_value_2) = FALSE then
            return FALSE;
         end if;
         ---
     
      elsif I_module = 'CUST' then
         ---
         if FM_RMA_NF_CREATION_SQL.EXISTS_CUSTOMER(O_error_message,
                                  	     		   O_description,
                                         		   I_key_value_1) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   else
   	  O_description := NULL;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ENTITY;
-----------------------------------------------------------------------------------
FUNCTION LOV_CNPJCPF(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                     I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                     I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                     I_cnpjcpf        IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_CNPJCPF';
   ---
BEGIN
   ---
   if I_module = 'SUPP' then
      ---
      O_query := 'select a.cnpj cnpjcpf, NULL type, c.code_desc type_desc, s.supplier id_name, s.sup_name name '||
                   'from v_fiscal_attributes a, sups s, code_detail c '||
                  'where s.supplier = a.key_value_1 '||
                    'and c.code_type = ''FIMO'' '||
                    'and c.code = ''SUPP'' '||
                    'and module = '''||I_module||''' '||
                    'and cnpj is NOT NULL ';
      if I_cnpjcpf is NOT NULL then
         O_query := O_query || 'and cnpj = '''||I_cnpjcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query  ||
                  'union '||
                 'select a.cpf cnpjcpf, NULL type, c.code_desc type_desc, s.supplier id_name, s.sup_name name '||
                   'from v_fiscal_attributes a, sups s, code_detail c '||
                  'where s.supplier = a.key_value_1 '||
                    'and c.code_type = ''FIMO'' '||
                    'and c.code = ''SUPP'' '||
                    'and module = '''||I_module||''''||
                    'and cpf is NOT NULL ';
      if I_cnpjcpf is NOT NULL then
         O_query := O_query || 'and cpf = '''||I_cnpjcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query ||'order by 1';
      ---
   elsif I_module = 'PTNR' then
      ---
      O_query := 'select a.cnpj cnpjcpf, c.code type, c.code_desc type_desc, p.partner_id id_name, p.partner_desc name '||
                   'from v_fiscal_attributes a, partner p, code_detail c '||
                  'where p.partner_id = a.key_value_1 '||
                    'and p.partner_type = a.key_value_2 '||
                    'and c.code_type = ''PTAL'' '||
                    'and c.code = a.key_value_2 '||
                    'and module = '''||I_module||''' '||
                    'and cnpj is NOT NULL ';
      if I_cnpjcpf is NOT NULL then
         O_query := O_query || 'and cnpj = '''||I_cnpjcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query  ||
                  'union '||
                 'select a.cpf cnpjcpf, c.code type, c.code_desc type_desc, p.partner_id id_name, p.partner_desc name '||
                   'from v_fiscal_attributes a, partner p, code_detail c '||
                  'where p.partner_id = a.key_value_1 '||
                    'and p.partner_type = a.key_value_2 '||
                    'and c.code_type = ''PTAL'' '||
                    'and c.code = a.key_value_2 '||
                    'and module = '''||I_module||''' '||
                    'and cpf is NOT NULL ';
      if I_cnpjcpf is NOT NULL then
         O_query := O_query || 'and cpf = '''||I_cnpjcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query ||'order by 1';
      ---
   elsif I_module = 'LOC' then
      ---
      if I_key_value_2 = 'S' then
         O_query := 'select a.cnpj cnpjcpf, c.code type, c.code_desc type_desc, s.store id_name, s.store_name name '||
                      'from v_fiscal_attributes a, store s, code_detail c '||
                     'where s.store = a.key_value_1 '||
                       'and c.code_type = ''LOC4'' '||
                       'and c.code = a.key_value_2 '||
                       'and module = '''||I_module||''' '||
                       'and cnpj is NOT NULL '||
                       'and key_value_2 = '''||I_key_value_2||''' ';
         if I_cnpjcpf is NOT NULL then
            O_query := O_query || 'and cnpj = '''||I_cnpjcpf||''' ';
         end if;
         if I_key_value_1 is NOT NULL then
            O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
         end if;
         O_query := O_query  ||
                     'union '||
                    'select a.cpf cnpjcpf, c.code type, c.code_desc type_desc, s.store id_name, s.store_name name '||
                      'from v_fiscal_attributes a, store s, code_detail c '||
                     'where s.store = a.key_value_1 '||
                       'and c.code_type = ''LOC4'' '||
                       'and c.code = a.key_value_2 '||
                       'and module = '''||I_module||''' '||
                       'and cpf is NOT NULL '||
                       'and key_value_2 = '''||I_key_value_2||''' ';
         if I_cnpjcpf is NOT NULL then
            O_query := O_query || 'and cpf = '''||I_cnpjcpf||''' ';
         end if;
         if I_key_value_1 is NOT NULL then
            O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
         end if;
         O_query := O_query ||'order by 1';
         ---
      elsif  I_key_value_2 = 'W' then
         ---
         O_query := 'select a.cnpj cnpjcpf, c.code type, c.code_desc type_desc, w.wh id_name, w.wh_name name '||
                      'from v_fiscal_attributes a, wh w, code_detail c '||
                     'where w.wh = a.key_value_1 '||
                       'and c.code_type = ''LOC4'' '||
                       'and c.code = a.key_value_2 '||
                       'and module = '''||I_module||''' '||
                       'and cnpj is NOT NULL '||
                       'and key_value_2 = '''||I_key_value_2||''' ';
         if I_cnpjcpf is NOT NULL then
            O_query := O_query || 'and cnpj = '''||I_cnpjcpf||''' ';
         end if;
         if I_key_value_1 is NOT NULL then
            O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
         end if;
         O_query := O_query  ||
                     'union '||
                    'select a.cpf cnpjcpf, c.code type, c.code_desc type_desc, w.wh id_name, w.wh_name name '||
                      'from v_fiscal_attributes a, wh w, code_detail c '||
                     'where w.wh = a.key_value_1 '||
                       'and c.code_type = ''LOC4'' '||
                       'and c.code = a.key_value_2 '||
                       'and module = '''||I_module||''' '||
                       'and cpf is NOT NULL '||
                       'and key_value_2 = '''||I_key_value_2||''' ';
         if I_cnpjcpf is NOT NULL then
            O_query := O_query || 'and cpf = '''||I_cnpjcpf||''' ';
         end if;
         if I_key_value_1 is NOT NULL then
            O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
         end if;
         O_query := O_query ||'order by 1';
         ---
      end if;
      ---
   elsif I_module = 'OLOC' then
      ---
      O_query := 'select a.cnpj cnpjcpf, c.code type, c.code_desc type_desc, o.outloc_id id_name, o.outloc_desc name '||
                   'from v_fiscal_attributes a, outloc o, code_detail c '||
                  'where o.outloc_id = a.key_value_1 '||
                    'and o.outloc_type = a.key_value_2 '||
                    'and c.code_type = ''LOCT'' '||
                    'and c.code = a.key_value_2 '||
                    'and module = '''||I_module||''' '||
                    'and cnpj is NOT NULL ';
      if I_cnpjcpf is NOT NULL then
         O_query := O_query || 'and cnpj = '''||I_cnpjcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query  ||
                  'union '||
                 'select a.cpf cnpjcpf, c.code type, c.code_desc type_desc, o.outloc_id id_name, o.outloc_desc name '||
                   'from v_fiscal_attributes a, outloc o, code_detail c '||
                  'where o.outloc_id = a.key_value_1 '||
                    'and o.outloc_type = a.key_value_2 '||
                    'and c.code_type = ''LOCT'' '||
                    'and c.code = a.key_value_2 '||
                    'and module = '''||I_module||''' '||
                    'and cpf is NOT NULL ';
      if I_cnpjcpf is NOT NULL then
         O_query := O_query || 'and cpf = '''||I_cnpjcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query ||'order by 1';
      ---
   
   elsif I_module = 'CUST' then
      ---
      O_query := 'select a.cnpj cnpjcpf, NULL type, c.code_desc type_desc, a.cust_id id_name, a.cust_name name '||
                   'from fm_rma_head a, code_detail c '||
                  'where c.code_type = ''FIMO'' '||
                    'and c.code = ''CUST'' '||
                    'and cnpj is NOT NULL ';
      if I_cnpjcpf is NOT NULL then
         O_query := O_query || 'and cnpj = '''||I_cnpjcpf||''' ';
      end if;

      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and a.cust_id = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query  ||
                  'union '||
                 'select a.cpf cnpjcpf, NULL type, c.code_desc type_desc, a.cust_id id_name, a.cust_name name '||
                   'from fm_rma_head a, code_detail c '||
                  'where c.code_type = ''FIMO'' '||
                    'and c.code = ''CUST'' '||
                    'and cpf is NOT NULL ';
      if I_cnpjcpf is NOT NULL then
         O_query := O_query || 'and cpf = '''||I_cnpjcpf||''' ';
      end if;

      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and a.cust_id = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query ||'order by 1';
      ---
     
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_CNPJCPF;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_CNPJCPF(O_error_message  IN OUT VARCHAR2,
                          O_unique         IN OUT BOOLEAN,
                          O_tipo_cnpjcpf   IN OUT VARCHAR2,
                          O_key_value_1    IN OUT V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                          I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                          I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                          I_cnpjcpf        IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.VALIDATE_CNPJCPF';
   L_count     NUMBER;
   ---
   cursor C_COUNT_cnpjCPF is
      select count(*)
        from (select 'J' tipo, a.cnpj cnpjcpf
                from v_fiscal_attributes a
               where a.module = I_module
                 and ((O_key_value_1 is NOT NULL
                 and  a.key_value_1 = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and ((I_key_value_2 is NOT NULL
                 and  a.key_value_2 = I_key_value_2)
                  or I_key_value_2 is NULL)
                 and a.cnpj = I_cnpjcpf
               union all
              select 'F' tipo, a.cpf cnpjcpf
                from v_fiscal_attributes a
               where a.module = I_module
                 and ((O_key_value_1 is NOT NULL
                 and  a.key_value_1 = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and ((I_key_value_2 is NOT NULL
                 and  a.key_value_2 = I_key_value_2)
                  or I_key_value_2 is NULL)
                 and a.cpf = I_cnpjcpf
               union all
              select 'J' tipo, a.cnpj cnpjcpf
                from fm_rma_head a
               where I_module = 'CUST'
                 and ((O_key_value_1 is NOT NULL
                 and  a.cust_id = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and ((I_key_value_2 is NOT NULL
                 and  I_key_value_2 = 'C')
                  or I_key_value_2 is NULL)
                 and a.cnpj = I_cnpjcpf
              union 
              select 'F' tipo, a.cpf cnpjcpf
                from fm_rma_head a
               where I_module = 'CUST'
                 and ((O_key_value_1 is NOT NULL
                 and  a.cust_id = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and ((I_key_value_2 is NOT NULL
                 and  I_key_value_2 = 'C')
                  or I_key_value_2 is NULL)
                 and a.cpf = I_cnpjcpf) a;
   ---
   cursor C_FISCAL_ENTITY_ATTRIB is
      select 'J' tipo, a.key_value_1
        from v_fiscal_attributes a
       where a.module = I_module
         and ((O_key_value_1 is NOT NULL
         and  a.key_value_1 = O_key_value_1)
          or O_key_value_1 is NULL)
         and ((I_key_value_2 is NOT NULL
         and  a.key_value_2 = I_key_value_2)
          or I_key_value_2 is NULL)
         and a.cnpj = I_cnpjcpf
       union
      select 'F' tipo, a.key_value_1
        from v_fiscal_attributes a
       where a.module = I_module
         and ((O_key_value_1 is NOT NULL
         and  a.key_value_1 = O_key_value_1)
          or O_key_value_1 is NULL)
         and ((I_key_value_2 is NOT NULL
         and  a.key_value_2 = I_key_value_2)
          or I_key_value_2 is NULL)
         and a.cpf = I_cnpjcpf
       union
     select 'J' tipo, a.cust_id
                from fm_rma_head a
               where I_module = 'CUST'
                 and ((O_key_value_1 is NOT NULL
                 and  a.cust_id = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and ((I_key_value_2 is NOT NULL
                 and  I_key_value_2 = 'C')
                  or I_key_value_2 is NULL)
                 and a.cnpj = I_cnpjcpf
        union
       select 'F' tipo, a.cust_id
                from fm_rma_head a
               where I_module = 'CUST'
                 and ((O_key_value_1 is NOT NULL
                 and  a.cust_id = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and ((I_key_value_2 is NOT NULL
                 and  I_key_value_2 = 'C')
                  or I_key_value_2 is NULL)
                 and a.cpf = I_cnpjcpf;
   ---
BEGIN
   ---

   if I_module is NOT NULL and
      I_cnpjcpf is NOT NULL then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_COUNT_cnpjCPF',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf);
      open C_COUNT_cnpjCPF;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_COUNT_cnpjCPF',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf);
      fetch C_COUNT_cnpjCPF into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_COUNT_cnpjCPF',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf);
      close C_COUNT_cnpjCPF;
      ---
      if L_count is NOT NULL and L_count > 1 then
         O_unique := FALSE;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf);
      open C_FISCAL_ENTITY_ATTRIB;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf);
      fetch C_FISCAL_ENTITY_ATTRIB into O_tipo_cnpjcpf, O_key_value_1;
      if C_FISCAL_ENTITY_ATTRIB%NOTFOUND then
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_FISCAL_ENTITY_ATTRIB',
                          'V_FISCAL_ATTRIBUTES',
                          'Module = '      || I_module ||
                          ' Key_value_1 = '|| O_key_value_1 ||
                          ' Key_value_2 = '|| I_key_value_2 ||
                          ' CNPJ/CPF = '   || I_cnpjcpf);
         close C_FISCAL_ENTITY_ATTRIB;
         ---
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_CNPJ_CPF',NULL,NULL,NULL);
         ---
         return FALSE;
         ---
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf);
      close C_FISCAL_ENTITY_ATTRIB;
end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_CNPJCPF;
-----------------------------------------------------------------------------------
FUNCTION LOV_ORDER_NO(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2,
                      I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.LOV_ORDER_NO';
   ---
BEGIN
   ---
   O_query := 'select distinct fdd.requisition_no order_no '||
                'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
               'where fdh.location_type = '''||I_location_type||''' '||
                 'and fdh.location_id = '||TO_CHAR(I_location_id)||' '||
                 'and fdh.requisition_type = ''PO'' '||
                 'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ORDER_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_ITEM(O_error_message  IN OUT VARCHAR2,
                  O_query          IN OUT VARCHAR2,
                  I_where_clause   IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_ITEM';
   ---
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'select item_desc, item '||
                   'from item_master '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(item_desc) like ''%' || UPPER(I_where_clause) || '%'' '||
                  'union all '||
                 'select NVL(tl_shadow.translated_value, item_desc) item_desc, item '||
                   'from item_master, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and UPPER(item_desc) like ''%' || UPPER(I_where_clause) || '%'' '||
               'order by 1';
   else
      O_query := 'select item_desc, item '||
                   'from item_master '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                  'union all '||
                 'select NVL(tl_shadow.translated_value, item_desc) item_desc, item '||
                   'from item_master, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(item) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
               'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_ITEM;
-----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message      IN OUT VARCHAR2,
                          O_where_clause       IN OUT VARCHAR2,
                          I_schedule_no        IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                          I_location_id        IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                          I_location_type      IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                          I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_type_id            IN     FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                          I_utilization_id     IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE,
                          I_cfop               IN     FM_FISCAL_DOC_HEADER.NF_CFOP%TYPE,
                          I_nfe_access_key     IN     FM_FISCAL_DOC_HEADER.NFE_ACCESSKEY%TYPE,
                          I_status             IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                          I_module             IN     FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                          I_key_value_1        IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                          I_key_value_2        IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                          I_cnpj               IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
                          I_cpf                IN     V_FISCAL_ATTRIBUTES.CPF%TYPE,
                          I_issue_date         IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                          I_entry_or_exit_date IN     FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                          I_exit_hour          IN     FM_FISCAL_DOC_HEADER.EXIT_HOUR%TYPE,
                          I_requisition_type   IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                          I_requisition_no     IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                          I_item               IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                          I_mode_type          IN     FM_SCHEDULE.MODE_TYPE%TYPE,
                          I_link_nf_ind        IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program           VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.SET_WHERE_CLAUSE';
   ---
   L_description       FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_number_value      FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value      FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value        FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   ---
BEGIN
   ---
   if I_schedule_no is NOT NULL then
      O_where_clause := 'schedule_no = ' || I_schedule_no || ' and ';
   end if;

   if I_location_id is NOT NULL then
      O_where_clause := O_where_clause || 'location_id = ' || TO_CHAR(I_location_id) || ' and ';
   end if;

   if I_location_type is NOT NULL then
      O_where_clause := O_where_clause || 'location_type = ''' || I_location_type || ''' and ';
   end if;

   if I_fiscal_doc_no is NOT NULL then
      O_where_clause := O_where_clause || 'fiscal_doc_no = ' || TO_CHAR(I_fiscal_doc_no) || ' and ';
   end if;
   ---
   if I_type_id is NOT NULL then
      O_where_clause := O_where_clause || 'type_id = ' || TO_CHAR(I_type_id) || ' and ';
   end if;
   ---
   if I_utilization_id is NOT NULL then
      O_where_clause := O_where_clause || 'utilization_id = ''' || I_utilization_id || ''' and ';
   end if;
   ---
   if I_cfop is NOT NULL then
      O_where_clause := O_where_clause || 'nf_cfop = ''' || I_cfop || ''' and ';
   end if;
   ---
   if I_nfe_access_key is NOT NULL then
      O_where_clause := O_where_clause || 'nfe_accesskey = ''' || I_nfe_access_key || ''' and ';
   end if;
   ---
   if I_status is NOT NULL then
      O_where_clause := O_where_clause || 'status = ''' || I_status || ''' and ';
   else           
      O_where_clause := O_where_clause || 'status NOT IN (''I'') and ';
   end if;
   ---
   if I_module is NOT NULL then
      O_where_clause := O_where_clause || 'module = ''' || I_module || ''' and ';
   end if;
   ---
   if I_key_value_1 is NOT NULL then
      O_where_clause := O_where_clause || 'key_value_1 = ''' || I_key_value_1 || ''' and ';
   end if;
   ---
   if I_key_value_2 is NOT NULL then
      O_where_clause := O_where_clause || 'key_value_2 = ''' || I_key_value_2 || ''' and ';
   end if;
   ---
   if I_cnpj is NOT NULL and I_module <> 'CUST' then 
      O_where_clause := O_where_clause || 'exists (select 1 '||
                                     'from v_fiscal_attributes b '||
                                    'where b.module = fm_fiscal_doc_header.module '||
                                      'and b.key_value_1 = fm_fiscal_doc_header.key_value_1 '||
                                      'and b.cnpj = '''|| I_cnpj || ''') and ';
   elsif I_cnpj is NOT NULL and I_module = 'CUST' then
      O_where_clause := O_where_clause || 'exists (select 1 '||
                                     'from fm_rma_head b '||
                                    'where b.cust_id = fm_fiscal_doc_header.key_value_1 '||
                                      'and b.cnpj = '''|| I_cnpj || ''') and ';      
   end if;
   ---
   if I_cpf is NOT NULL and I_module <> 'CUST' then
      O_where_clause := O_where_clause || 'exists (select 1 '||
                                     'from v_fiscal_attributes b '||
                                    'where b.module = fm_fiscal_doc_header.module '||
                                      'and b.key_value_1 = fm_fiscal_doc_header.key_value_1 '||
                                      'and b.cnpj = '''|| I_cpf || ''') and ';
   elsif I_cpf is NOT NULL and I_module = 'CUST' then
      O_where_clause := O_where_clause || 'exists (select 1 '||
                                     'from fm_rma_head b '||
                                    'where b.cust_id = fm_fiscal_doc_header.key_value_1 '||
                                      'and b.cnpj = '''|| I_cpf || ''') and ';   
   end if;
   ---
   if I_issue_date is NOT NULL then
      O_where_clause := O_where_clause || 'TRUNC(issue_date) = TO_DATE(''' || TO_CHAR(I_issue_date,'DD/MM/YYYY') || ''',''DD/MM/YYYY'') and ';
   end if;
   ---
   if I_entry_or_exit_date is NOT NULL then
      O_where_clause := O_where_clause || 'TRUNC(entry_or_exit_date) = TO_DATE(''' || TO_CHAR(I_entry_or_exit_date,'DD/MM/YYYY') || ''',''DD/MM/YYYY'') and ';
   end if;
   ---
   if I_exit_hour is NOT NULL then
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
      	                                     L_description,
      	                                     L_number_value,
      	                                     L_string_value,
      	                                     L_date_value,
      	                                     'VARCHAR',
      	                                     'HOUR_FORMAT') = FALSE then
         return FALSE;
      end if;
      if L_string_value = 'HH:MI AM' then
         O_where_clause := O_where_clause || 'TO_CHAR(exit_hour,''HH:MI AM'') = TO_CHAR(TO_DATE(''' || TO_CHAR(I_exit_hour,'HH:MI AM') || ''',''HH:MI AM''),''HH:MI AM'') and ';
      elsif L_string_value = 'HH24:MI' then
         O_where_clause := O_where_clause || 'TO_CHAR(exit_hour,''HH24:MI'') = TO_CHAR(TO_DATE(''' || TO_CHAR(I_exit_hour,'HH24:MI') || ''',''HH24:MI''),''HH24:MI'') and ';
      end if;
   end if;
   ---
   if I_requisition_type is NOT NULL then
      O_where_clause := O_where_clause || 'requisition_type = ''' || I_requisition_type || ''' and ';
   end if;
   ---
   if I_requisition_no is NOT NULL then
      O_where_clause := O_where_clause || 'exists (select 1 '||
                                     'from fm_fiscal_doc_detail b '||
                                    'where b.fiscal_doc_id = fm_fiscal_doc_header.fiscal_doc_id '||
                                      'and b.requisition_no = '|| TO_CHAR(I_requisition_no) || ') and ';
   end if;
   ---
   if I_item is NOT NULL then
      O_where_clause := O_where_clause || 'exists (select 1 '||
                                     'from fm_fiscal_doc_detail b '||
                                    'where b.fiscal_doc_id = fm_fiscal_doc_header.fiscal_doc_id '||
                                      'and b.item = '|| TO_CHAR(I_item) || ') and ';
   end if;
   ---
   if I_mode_type is NOT NULL then
      if I_link_nf_ind = 'N' then
         if I_mode_type = 'ENT' then
            O_where_clause := O_where_clause || ' (schedule_no is NULL or
                                                  schedule_no IN (select s.schedule_no
                                                                    from fm_schedule s
                                                                   where s.schedule_no = fm_fiscal_doc_header.schedule_no
                                                                     and mode_type = '''||I_mode_type||''')) and ';
         else
            O_where_clause := O_where_clause || ' schedule_no IN (select s.schedule_no
                                                                    from fm_schedule s
                                                                   where s.schedule_no = fm_fiscal_doc_header.schedule_no
                                                                     and mode_type = '''||I_mode_type||''') and ';
         end if;
      else
         O_where_clause := O_where_clause || ' schedule_no is NULL and ';  
      end if;
   end if;
   ---
   if I_link_nf_ind = 'Y' then
      if I_status is NULL then
         O_where_clause := O_where_clause || 'status  IN (''W'',''V'',''E'',''D'') and ';
      end if;
   end if;
   ---
   if O_where_clause is NOT NULL then
      O_where_clause := SUBSTR(O_where_clause, 1, NVL(LENGTH(O_where_clause), 0)-4);
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE;
-----------------------------------------------------------------------------------
FUNCTION GET_ENTITY_INFO(O_error_message  IN OUT VARCHAR2,
                         O_cnpjcfp_type   IN OUT V_FISCAL_ATTRIBUTES.TYPE%TYPE,
                         O_cnpjcfp        IN OUT V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)

   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.GET_ENTITY_INFO';
   L_cnpj      V_FISCAL_ATTRIBUTES.CNPJ%TYPE;
   L_cpF       V_FISCAL_ATTRIBUTES.CPF%TYPE;
   ---
   cursor C_FISCAL_ENTITY_ATTRIB is
      select a.type,
             a.cnpj,
             a.cpf
        from v_fiscal_attributes a
       where a.module = I_module
         and a.key_value_1 = I_key_value_1
         and a.key_value_2 = I_key_value_2
      union      
      select NVL2(a.cnpj,'J','F') type, a.cnpj, a.cpf
                from fm_rma_head a
               where I_module = 'CUST'
                 and ((I_key_value_1    is NOT NULL
                 and  a.cust_id = I_key_value_1 )
                  or I_key_value_1  is NULL)
                 and ((I_key_value_2 is NOT NULL
                 and  I_key_value_2 = 'C')
                  or I_key_value_2 is NULL)
		  and ((O_cnpjcfp  is NOT NULL
                 and  cnpj  = O_cnpjcfp  
                  or  cpf   = O_cnpjcfp )
                  or O_cnpjcfp is NULL);
       
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_FISCAL_ENTITY_ATTRIB',
                    'V_FISCAL_ATTRIBUTES',
                    'Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1||
                    ' Key_value_2 = '|| I_key_value_2);
   open C_FISCAL_ENTITY_ATTRIB;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_FISCAL_ENTITY_ATTRIB',
                    'V_FISCAL_ATTRIBUTES',
                    'Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1||
                    ' Key_value_2 = '|| I_key_value_2);
   fetch C_FISCAL_ENTITY_ATTRIB into O_cnpjcfp_type,
                                          L_cnpj,
                                          L_cpf;
   
   if C_FISCAL_ENTITY_ATTRIB%FOUND then
      if O_cnpjcfp_type = 'J' then
         O_cnpjcfp := L_cnpj;
      elsif O_cnpjcfp_type = 'F' then
	 O_cnpjcfp := L_cpf;
      else
         O_cnpjcfp := NULL;
      end if;
   else
      O_cnpjcfp := NULL;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_FISCAL_ENTITY_ATTRIB',
                    'V_FISCAL_ATTRIBUTES',
                    'Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1||
                    ' Key_value_2 = '|| I_key_value_2);
   close C_FISCAL_ENTITY_ATTRIB;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ENTITY_INFO;
-----------------------------------------------------------------------------------
FUNCTION LOV_SCHEDULE_NO(O_error_message  IN OUT VARCHAR2,
                         O_query          IN OUT VARCHAR2,
                         I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                         I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                         I_where_clause   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                         I_mode_type      IN     FM_SCHEDULE.MODE_TYPE%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'ORFM_SCHEDULE_SQL.LOV_SCHEDULE_NO';
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'Select schedule_no, mode_type,  DECODE(mode_type,''ENT'',''Entry'',''EXIT'',''Exit'') mode_type_desc from fm_schedule fs
                   where schedule_no = '||I_where_clause||' ';
   else
      O_query := 'Select schedule_no, mode_type, DECODE(mode_type,''ENT'',''Entry'',''EXIT'',''Exit'') mode_type_desc from fm_schedule fs
                   where 1 = 1 ';
   end if;
   ---
   if I_location is NOT NULL then
      O_query := O_query ||
                 'and fs.location_id = '||I_location||' ';
   end if;
   if I_loc_type is NOT NULL then
      O_query := O_query ||
                 'and fs.location_type = '''||I_loc_type||''' ';
   end if;
   if I_mode_type is NOT NULL then
      O_query := O_query ||' and mode_type = '''||I_mode_type||'''';
   end if;

   O_query := O_query ||' order by schedule_no ';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOV_SCHEDULE_NO;
-------------------------------------------------------------------------
/*
FUNCTION LOV_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.LOV_CUSTOMER';
   ---
BEGIN
   ---
   O_query := 'select cust_name, cust_id '||
                'from customer, l10n_fiscal_entity_attr '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and module = ''CUS'' '||
                 'and key_value_1 = cust_id '||
              'union all '||
              'select NVL(tl_shadow.translated_value, cust_name) cust_name, cust_id '||
                'from customer, l10n_fiscal_entity_attr, tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and module = ''CUS'' '||
                 'and key_value_1 = cust_id '||
                 'and UPPER(cust_id) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_CUSTOMER; */
-----------------------------------------------------------------------------------
FUNCTION GET_MODE_TYPE(O_error_message  IN OUT VARCHAR2,
                       O_mode_type      IN OUT FM_SCHEDULE.MODE_TYPE%TYPE,
                       I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.GET_MODE_TYPE';
   ---
   cursor C_MODE is
      select mode_type
        from fm_schedule
       where schedule_no = I_schedule_no;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_MODE','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   open C_MODE;
   ---
   SQL_LIB.SET_MARK('FETCH','C_MODE','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   fetch C_MODE into O_mode_type;
   if C_MODE%NOTFOUND then
      O_mode_type := NULL;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_MODE','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   close C_MODE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_MODE_TYPE;
----------------------------------------------------------------------------
FUNCTION LOV_TSF_NO(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2,
                    I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.LOV_TSF_NO';
   ---
BEGIN
   ---
   O_query := 'select distinct fdd.requisition_no tsf_no '||
                'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
               'where ((fdh.module = ''LOC'' '||
                 'and fdh.key_value_2 = '''||I_location_type||''' '||
                 'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                 ' or (fdh.module = ''OLOC'' '||
                 'and fdh.key_value_2 = '''||I_location_type||''' '||
                 'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                 ' or (fdh.location_type = '''||I_location_type||''' '||
                 'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                 'and fdh.requisition_type = ''TSF'' '||
                 'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_TSF_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_RTV_NO(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2,
                    I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.LOV_RTV_NO';
   ---
BEGIN
   ---
   O_query := 'select distinct fdd.requisition_no rtv_order_no '||
                'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
               'where ((fdh.module = ''LOC'' '||
                 'and fdh.key_value_2 = '''||I_location_type||''' '||
                 'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                 ' or (fdh.module = ''OLOC'' '||
                 'and fdh.key_value_2 = '''||I_location_type||''' '||
                 'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                 ' or (fdh.location_type = '''||I_location_type||''' '||
                 'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                 'and fdh.requisition_type = ''RTV'' '||
                 'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_RTV_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_RMA_NO(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.LOV_RMA_NO';
   ---
BEGIN
   ---
   O_query := 'select frh.rma_id rma_no '||
                'from fm_rma_head frh '||
               --- 001 - Begin
               --'where frh.rma_status = ''A'' '||
               --- 001 - End
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_RMA_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_STOCK_NO(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2,
                      I_location_id    IN     FM_fiscal_DOC_HEADER.LOCATION_ID%TYPE,
                      I_location_type  IN     FM_fiscal_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.LOV_STOCK_NO';
   ---
BEGIN
   ---
   O_query := 'select distinct fdd.requisition_no rtv_order_no '||
                'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
               'where ((fdh.module = ''LOC'' '||
                 'and fdh.key_value_2 = '''||I_location_type||''' '||
                 'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                 ' or (fdh.module = ''OLOC'' '||
                 'and fdh.key_value_2 = '''||I_location_type||''' '||
                 'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                 ' or (fdh.location_type = '''||I_location_type||''' '||
                 'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                 'and fdh.requisition_type = ''STOCK'' '||
                 'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_STOCK_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_DOC_NO(O_error_message    IN OUT VARCHAR2,
                    O_query            IN OUT VARCHAR2,
                    I_location_id      IN     FM_fiscal_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type    IN     FM_fiscal_DOC_HEADER.LOCATION_TYPE%TYPE,
                    I_requisition_type IN     FM_fiscal_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.LOV_DOC_NO';
   ---
BEGIN
   ---
   O_query := 'select distinct fdd.requisition_no doc_no '||
                'from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd '||
               'where ((fdh.module = ''LOC'' '||
                 'and fdh.key_value_2 = '''||I_location_type||''' '||
                 'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                 ' or (fdh.module = ''OLOC'' '||
                 'and fdh.key_value_2 = '''||I_location_type||''' '||
                 'and fdh.key_value_1 = '||TO_CHAR(I_location_id)||') '||
                 ' or (fdh.location_type = '''||I_location_type||''' '||
                 'and fdh.location_id = '||TO_CHAR(I_location_id)||')) '||
                 'and fdh.requisition_type = '''||I_requisition_type||''' '||
                 'and fdd.fiscal_doc_id = fdh.fiscal_doc_id '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_DOC_NO;
---------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.VALIDATE_TSF';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS_TSF is
      select 'x'
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd
       where ((fdh.module = 'LOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.module = 'OLOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.location_type = I_location_type
         and fdh.location_id = I_location_id))
         and fdh.requisition_type = 'TSF'
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.requisition_no = I_tsf_no;
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_tsf_no||
            ' location_id = '||I_location_id||
            ' location_type = '''||I_location_type||'''';
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_TSF', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   open C_EXISTS_TSF;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_TSF', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_EXISTS_TSF into L_dummy;
   O_exists := C_EXISTS_TSF%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_TSF', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   close C_EXISTS_TSF;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_TSF;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_RTV(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      I_rtv_order_no   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.VALIDATE_RTV';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS is
      select 'x'
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd
       where ((fdh.module = 'LOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.module = 'OLOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.location_type = I_location_type
         and fdh.location_id = I_location_id))
         and fdh.requisition_type = 'RTV'
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.requisition_no = I_rtv_order_no;
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_rtv_order_no||
            ' location_id = '||I_location_id||
            ' location_type = '''||I_location_type||'''';
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   open C_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_EXISTS into L_dummy;
   O_exists := C_EXISTS%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   close C_EXISTS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_RTV;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_RMA(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.VALIDATE_RMA';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS is
      select 'x'
        from fm_rma_head frh
       where frh.rma_id = I_rma_no;
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_rma_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   open C_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_EXISTS into L_dummy;
   O_exists := C_EXISTS%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   close C_EXISTS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_RMA;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_STOCK(O_error_message  IN OUT VARCHAR2,
                        O_exists         IN OUT BOOLEAN,
                        I_stock_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                        I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                        I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'ORFM_FISCAL_FIND_SQL.VALIDATE_STOCK';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXISTS is
      select 'x'
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd
       where ((fdh.module = 'LOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.module = 'OLOC'
         and fdh.key_value_2 = I_location_type
         and fdh.key_value_1 = I_location_id)
          or (fdh.location_type = I_location_type
         and fdh.location_id = I_location_id))
         and fdh.requisition_type = 'STOCK'
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.requisition_no = I_stock_no;
   ---
BEGIN
   ---
   L_key := 'requisition_no = '||I_stock_no||
            ' location_id = '||I_location_id||
            ' location_type = '''||I_location_type||'''';
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   open C_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_EXISTS into L_dummy;
   O_exists := C_EXISTS%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', 'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL', L_key);
   close C_EXISTS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_STOCK;
-----------------------------------------------------------------------------------
FUNCTION LINK_SCHEDULE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                       I_schedule       IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program             VARCHAR2(100) := 'FM_FISCAL_FIND_SQL.LINK_SCHEDULE';
   L_cursor              VARCHAR2(100);
   L_table               VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER';
   L_key                 VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   L_req_type            FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_comp_nf_ind         FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind          ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   ---
   cursor C_LOCK_DOC_HEADER(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select requisition_type
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = P_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_LOCK_DOC_HEADER';
   ---
   open C_LOCK_DOC_HEADER(I_fiscal_doc_id);
   fetch C_LOCK_DOC_HEADER into L_req_type; 
   close C_LOCK_DOC_HEADER;  
   ---
   if L_req_type = 'PO' then
      if FM_FISCAL_VALIDATION_SQL.GET_TRIANG_IND(O_error_message, 
                                                 L_comp_nf_ind,
                                                 L_triang_ind,
                                                 I_fiscal_doc_id) = FALSE then
         return FALSE;
      end if;
      
      if L_triang_ind = 'Y' then
         if FM_FISCAL_VALIDATION_SQL.GET_OTHER_NF(O_error_message,
                                                  L_other_fiscal_doc_id,
                                                  L_comp_nf_ind,
                                                  I_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;
         ---
         if L_other_fiscal_doc_id is NOT NULL then
            L_key := 'Fiscal_doc_id: ' || TO_CHAR(L_other_fiscal_doc_id);
            open C_LOCK_DOC_HEADER(L_other_fiscal_doc_id);
            fetch C_LOCK_DOC_HEADER into L_req_type;            
            ---
            update fm_fiscal_doc_header fdh
               set fdh.schedule_no = I_schedule,
                   last_update_datetime = sysdate,
                   last_update_id       = user
             where fdh.fiscal_doc_id = L_other_fiscal_doc_id;
            ---
            close C_LOCK_DOC_HEADER;
            ---
         end if;
      end if;
   end if;
   ---
   update fm_fiscal_doc_header fdh
      set fdh.schedule_no = I_schedule,
          last_update_datetime = sysdate,
          last_update_id       = user
    where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program||' '||L_key,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', L_table, L_key);
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program||' '||L_key,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', L_table, L_key);
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      return FALSE;
END LINK_SCHEDULE;
-----------------------------------------------------------------------------------

END FM_FISCAL_FIND_SQL;
/