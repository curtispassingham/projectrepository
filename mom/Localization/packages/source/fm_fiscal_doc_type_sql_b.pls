create or replace PACKAGE BODY FM_FISCAL_DOC_TYPE_SQL is
-----------------------------------------------------------------------------------------------
FUNCTION EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                O_exists         IN OUT BOOLEAN,
                O_type_desc      IN OUT FM_FISCAL_DOC_TYPE.TYPE_DESC%TYPE,
                I_type_id        IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_TYPE_SQL.EXISTS';
   ---
   cursor C_FM_FISCAL_DOC_TYPE is
      select type_desc
        from fm_fiscal_doc_type
       where type_id = I_type_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_FISCAL_DOC_TYPE','FM_FISCAL_DOC_TYPE','Type id: '||I_type_id);
   open C_FM_FISCAL_DOC_TYPE;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_FISCAL_DOC_TYPE','FM_FISCAL_DOC_TYPE','Type id: '||I_type_id);
   fetch C_FM_FISCAL_DOC_TYPE into O_type_desc;
   O_exists := C_FM_FISCAL_DOC_TYPE%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DOC_TYPE','FM_FISCAL_DOC_TYPE','Type id: '||I_type_id);
   close C_FM_FISCAL_DOC_TYPE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXISTS;
-----------------------------------------------------------------------------------------------
FUNCTION EXIST_CHILD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists         IN OUT BOOLEAN,
                     I_type_id        IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
return BOOLEAN is
---
L_dummy      VARCHAR2(1);
---

cursor C_TYPE is
   select 'x'
     from fm_doc_type_utilization
    where type_id = I_type_id
      and ROWNUM = 1;
BEGIN
   O_exists := FALSE;
   ---

   SQL_LIB.SET_MARK('OPEN','C_TYPE','FM_FISCAL_DOC_TYPE_SQL','Type id: '||I_type_id);
   open C_TYPE;
   SQL_LIB.SET_MARK('FETCH','C_TYPE','FM_FISCAL_DOC_TYPE_SQL','Type id: '||I_type_id);
   fetch C_TYPE into L_dummy;
   if C_TYPE%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_TYPE','FM_FISCAL_DOC_TYPE_SQL','Type id: '||I_type_id);
   close C_TYPE;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_FISCAL_DOC_TYPE_SQL.EXIST_CHILD',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_CHILD;
-----------------------------------------------------------------------------------------------
FUNCTION EXIST_CHILD_HEADER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         IN OUT BOOLEAN,
                            I_type_id        IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
return BOOLEAN is
---
L_dummy      VARCHAR2(1);
---

cursor C_EXIST is
select 1
  from fm_doc_type_utilization a
 where a.type_id = I_type_id
   and EXISTS (select 1
                 from fm_fiscal_doc_header b
                where b.type_id = a.type_id)
union all
select 1
  from fm_fiscal_doc_header e
 where e.type_id = I_type_id
   and e.utilization_id   IN ( select c.utilization_id
                                 from fm_doc_type_utilization a
                                    , fm_fiscal_utilization c
                                where a.utilization_id = c.utilization_id
                                  and a.type_id = e.type_id);
  
BEGIN
   O_exists := FALSE;
   ---

   SQL_LIB.SET_MARK('OPEN','C_TYPE','FM_FISCAL_DOC_TYPE_SQL','Type id: '||I_type_id);
   open C_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_TYPE','FM_FISCAL_DOC_TYPE_SQL','Type id: '||I_type_id);
   fetch C_EXIST into L_dummy;
   if C_EXIST%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_TYPE','FM_FISCAL_DOC_TYPE_SQL','Type id: '||I_type_id);
   close C_EXIST;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_FISCAL_DOC_TYPE_SQL.EXIST_CHILD_HEADER',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_CHILD_HEADER;
-----------------------------------------------------------------------------------------------
FUNCTION GET_SUBSERIES_NO_IND(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_subseries_no_ind IN OUT FM_FISCAL_DOC_TYPE.SUBSERIES_NO_IND%TYPE,
                              I_type_id          IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_TYPE_SQL.GET_SUBSERIES_NO_IND';
   ---
   cursor C_FM_FISCAL_DOC_TYPE is
      select subseries_no_ind
        from fm_fiscal_doc_type
       where type_id = I_type_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_FISCAL_DOC_TYPE','FM_FISCAL_DOC_TYPE','Type id: '||I_type_id);
   open C_FM_FISCAL_DOC_TYPE;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_FISCAL_DOC_TYPE','FM_FISCAL_DOC_TYPE','Type id: '||I_type_id);
   fetch C_FM_FISCAL_DOC_TYPE into O_subseries_no_ind;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DOC_TYPE','FM_FISCAL_DOC_TYPE','Type id: '||I_type_id);
   close C_FM_FISCAL_DOC_TYPE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_SUBSERIES_NO_IND;
-----------------------------------------------------------------------------------------------
FUNCTION GET_TYPE_DESC(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_type_desc        IN OUT FM_FISCAL_DOC_TYPE.TYPE_DESC%TYPE,
                       I_type_id          IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80)   := 'FM_FISCAL_DOC_TYPE_SQL.GET_TYPE_DESC';
   L_key       VARCHAR2(50);

   cursor C_DOC_TYPE is
      select type_desc
        from fm_fiscal_doc_type
       where type_id = I_type_id;

BEGIN
   L_key := 'TYPE_ID = '||I_type_id;
   SQL_LIB.SET_MARK('OPEN', 'C_DOC_TYPE', 'FM_FISCAL_DOC_TYPE', L_key);
   open C_DOC_TYPE;
   --
   SQL_LIB.SET_MARK('FETCH', 'C_DOC_TYPE', 'FM_FISCAL_DOC_TYPE', L_key);
   fetch C_DOC_TYPE into O_type_desc;
   --
   if C_DOC_TYPE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_DOC_TYPE', 'FM_FISCAL_DOC_TYPE', L_key);
      close C_DOC_TYPE;
      O_error_message := SQL_LIB.create_msg('ORFM_INV_TYPE_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_DOC_TYPE', 'FM_FISCAL_DOC_TYPE', L_key);
      close C_DOC_TYPE;
         if LANGUAGE_SQL.TRANSLATE( O_type_desc,
                                    O_type_desc,
                                    O_error_message) = FALSE then
            return FALSE;
         end if;
      return TRUE;
   end if;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_TYPE_DESC;
-----------------------------------------------------------------------------------------------
FUNCTION LOV_TYPE_ID(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_DOC_TYPE_SQL.LOV_TYPE_ID';
   ---
BEGIN
   ---
   O_query := 'select type_id, type_desc '||
                'from fm_fiscal_doc_type '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
              'union all '||
              'select type_id, NVL(tl_shadow.translated_value, type_desc) type_desc '||
                'from fm_fiscal_doc_type, tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and UPPER(type_id) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
               'order by 2';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_TYPE_ID;
-----------------------------------------------------------------------------------------------
FUNCTION LOV_UTILIZATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(70) := 'ORFM_FISCAL_DOC_TYPE_SQL.LOV_UTILIZATION';
   L_status    VARCHAR2(1)  := 'A';
   ---
BEGIN
   ---
   O_query := 'select utilization_id, utilization_desc '||
                'from fm_fiscal_utilization '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE
                  and status = '''|| L_status ||'''   '||
              'union all '||
              'select utilization_id, NVL(tl_shadow.translated_value, utilization_desc) utilization_desc '||
                'from fm_fiscal_utilization, tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and UPPER(utilization_id) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                 'and status = '''|| L_status || '''  '||
               'order by 2';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_UTILIZATION;
-----------------------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where         IN OUT VARCHAR2,
                          I_type_id       IN NUMBER)
return BOOLEAN is

L_program VARCHAR2(100) := 'ORFM_FISCAL_DOC_TYPE_SQL.SET_WHERE_CLAUSE';

BEGIN
   ---
   if I_type_id is NOT NULL then
      O_where := ' type_id = '|| I_type_id || ' and ';
   end if;
   O_where := O_where || ' 1=1 order by type_id';
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE;
-----------------------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_UTIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_where              IN OUT VARCHAR2,
                               I_type_id            IN NUMBER)
return BOOLEAN is

    L_program   VARCHAR2(80)   := 'ORFM_FISCAL_DOC_TYPE_SQL.P_SET_WHERE_CLAUSE_UTIL';
    L_status    VARCHAR2(1)  := 'A';
    ---
 BEGIN
    ---
    if I_type_id is NOT NULL then
       O_where := ' type_id = '|| I_type_id || ' and ';
    end if;
    ---
    O_where := O_where || ' status = '''|| L_status || '''  order by utilization_id';
    return TRUE;
 EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
       return FALSE;
END SET_WHERE_CLAUSE_UTIL;
-----------------------------------------------------------------------------------------------
END FM_FISCAL_DOC_TYPE_SQL;
/

