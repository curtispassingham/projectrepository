CREATE OR REPLACE PACKAGE BODY L10N_BR_TAX_SERVICE_MNGR_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION EXECUTE_TAX_PROCESS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status           IN OUT VARCHAR2,
                             I_tax_service_id   IN     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                             I_tax_call_type    IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program             VARCHAR2(255) := 'L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS';
   L_tax_service_url     RETAIL_SERVICE_REPORT_URL.URL%TYPE;
   L_timeout             NUMBER(20) := 1800000;
   L_table               VARCHAR2(30) := 'RETAIL_SERVICE_REPORT_URL';

   cursor C_GET_URL is
     select url,
            timeout
       from retail_service_report_url
      where RS_CODE = 'RTIL';
BEGIN

   -- Check required input parameters
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      O_status := 'E';
      return FALSE;
   end if;
   ---
   if I_tax_call_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_call_type', L_program, NULL);
      O_status := 'E';
      return FALSE;
   end if;

   -- Fetch RTIL URL for tax call
   open C_GET_URL;
   fetch C_GET_URL into L_tax_service_url, L_timeout;
   if C_GET_URL%NOTFOUND then
      close C_GET_URL;
      O_error_message:= SQL_LIB.CREATE_MSG('L10N_RTIL_URL_NOT_FOUND', L_table,
                                           NULL, NULL);
      O_status := 'E';
      return FALSE;
   end if;
   close C_GET_URL;

   if L_timeout is NULL then
      L_timeout := 1800000;  --use the default
   end if;

   invokeUrl(L_tax_service_url, I_tax_call_type, I_tax_service_id, L_timeout, O_status, O_error_message);

   if O_status in ('M', 'E') then
      return FALSE;
   end if;

   O_status := 'S';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      O_status := 'E';
      return FALSE;
END EXECUTE_TAX_PROCESS;
---------------------------------------------------------------------------------------------
FUNCTION GET_TAX_REQUEST_DATA(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_status_code        IN OUT  VARCHAR2,
                              O_businessObject     IN OUT  "RIB_FiscDocColRBM_REC",
                              I_tax_service_id     IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(255) := 'L10N_BR_TAX_SERVICE_MNGR_SQL.GET_TAX_REQUEST_DATA';
   L_type                 L10N_BR_TAX_CALL_STAGE_ROUTING.CLIENT_ID%TYPE;
   L_update_history_ind   L10N_BR_TAX_CALL_STAGE_ROUTING.UPDATE_HISTORY_IND%TYPE;

   cursor C_TYPE is
     select client_id,
            update_history_ind
       from l10n_br_tax_call_stage_routing
      where tax_service_id = I_tax_service_id;

BEGIN

   -- Check required input parameter
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      O_status_code := 'E';
      return FALSE;
   end if;

   -- Fetch routing information
   open C_TYPE;
   fetch C_TYPE into L_type, L_update_history_ind;
   if C_TYPE%NOTFOUND then
      close C_TYPE;
      O_error_message:= SQL_LIB.CREATE_MSG('L10N_ROUTE_INFO_NOT_FOUND', I_tax_service_id,
                                           NULL, NULL);
      O_status_code := 'E';
      return FALSE;
   end if;
   close C_TYPE;

   if L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.RMS_TAX  then
      if L10N_BR_INT_SQL.GET_REQUEST_DATA(O_error_message,
                                          O_businessObject,
                                          I_tax_service_id) = FALSE then
         O_status_code := 'E';
         return FALSE;
      end if;
    elsif L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.RFM_TAX then
       if FM_EXT_TAXES_SQL.GET_REQUEST_DATA(O_error_message,
                                            I_tax_service_id,
                                            O_businessObject,
				     	              L_update_history_ind) = FALSE then
          O_status_code := 'E';
          return FALSE;
       end if;
    elsif L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.SEED_RETAIL then
       if L10N_BR_EXTAX_MAINT_SQL.GET_REQUEST_DATA_RETAIL(O_error_message,
                                                          I_tax_service_id,
                                                          O_businessObject) = FALSE then
          O_status_code := 'E';
          return FALSE;
       end if;
    elsif L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.SEED_COST then
       if L10N_BR_EXTAX_MAINT_SQL.GET_REQUEST_DATA_COST(O_error_message,
                                                        I_tax_service_id,
                                                        O_businessObject) = FALSE then
          O_status_code := 'E';
          return FALSE;
       end if;
    end if;

   O_status_code := 'S';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      O_status_code := 'E';
      return FALSE;
END GET_TAX_REQUEST_DATA;
---------------------------------------------------------------------------------------------
FUNCTION SET_TAX_RESPONSE_DATA(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_status_code        IN OUT  VARCHAR2,
                               I_tax_service_id     IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                               I_businessObject     IN      "RIB_FiscDocTaxColRBM_REC")
RETURN BOOLEAN IS

   L_program              VARCHAR2(255) := 'L10N_BR_TAX_SERVICE_MNGR_SQL.SET_TAX_RESPONSE_DATA';
   L_type                 L10N_BR_TAX_CALL_STAGE_ROUTING.CLIENT_ID%TYPE;
   L_update_history_ind   L10N_BR_TAX_CALL_STAGE_ROUTING.UPDATE_HISTORY_IND%TYPE;

   cursor C_TYPE is
     select client_id,
            update_history_ind
       from l10n_br_tax_call_stage_routing
      where tax_service_id = I_tax_service_id;
BEGIN

   -- Check required input parameter
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      O_status_code := 'E';
      return FALSE;
   end if;

   -- Fetch routing information
   open C_TYPE;
   fetch C_TYPE into L_type,L_update_history_ind;
   if C_TYPE%NOTFOUND then
      close C_TYPE;
      O_error_message:= SQL_LIB.CREATE_MSG('L10N_ROUTE_INFO_NOT_FOUND', I_tax_service_id,
                                           NULL, NULL);
      O_status_code := 'E';
      return FALSE;
   end if;
   close C_TYPE;

   if L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.RMS_TAX  then
      if L10N_BR_INT_SQL.STAGE_RESULTS(O_error_message,
                                       I_tax_service_id,
                                       I_businessObject) = FALSE then
         O_status_code := 'E';
         return FALSE;
      end if;
   elsif L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.RFM_TAX  and L_update_history_ind = 'N' then
       if FM_EXT_TAXES_SQL.STAGE_RESULTS(O_error_message,
                                         I_tax_service_id,
                                         I_businessObject) = FALSE then
          O_status_code := 'E';
          return FALSE;
       end if;
    elsif L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.SEED_RETAIL then
       if L10N_BR_EXTAX_MAINT_SQL.SET_TAX_RESPONSE_DATA_RETAIL(O_error_message,
                                                               I_businessObject,
                                                               I_tax_service_id) = FALSE then
          O_status_code := 'E';
          return FALSE;
       end if;
    elsif L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.SEED_COST then
       if L10N_BR_EXTAX_MAINT_SQL.SET_TAX_RESPONSE_DATA_COST(O_error_message,
                                                             I_businessObject,
                                                             I_tax_service_id) = FALSE then
          O_status_code := 'E';
          return FALSE;
       end if;
    end if;

   O_status_code := 'S';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      O_status_code := 'E';
      return FALSE;
END SET_TAX_RESPONSE_DATA;
---------------------------------------------------------------------------------------------
FUNCTION GET_FISCAL_FDN_DATA(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status_code        IN OUT  VARCHAR2,
                             O_businessObject     IN OUT  "RIB_FiscalFDNQryRBM_REC",
                             I_tax_service_id     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(255) := 'L10N_BR_TAX_SERVICE_MNGR_SQL.SET_TAX_RESPONSE_DATA';
   L_type                 L10N_BR_TAX_CALL_STAGE_ROUTING.CLIENT_ID%TYPE;
   L_update_history_ind   L10N_BR_TAX_CALL_STAGE_ROUTING.UPDATE_HISTORY_IND%TYPE;

   cursor C_TYPE is
     select client_id
       from l10n_br_tax_call_stage_routing
      where tax_service_id = I_tax_service_id;
BEGIN

   -- Check required input parameter
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      O_status_code := 'E';
      return FALSE;
   end if;

   -- Fetch routing information
   open C_TYPE;
   fetch C_TYPE into L_type;
   if C_TYPE%NOTFOUND then
      close C_TYPE;
      O_error_message:= SQL_LIB.CREATE_MSG('L10N_ROUTE_INFO_NOT_FOUND', I_tax_service_id,
                                           NULL, NULL);
      O_status_code := 'E';
      return FALSE;
   end if;
   close C_TYPE;

    if L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.FISCAL_FDN  then
       if L10N_BR_FISCAL_FDN_QUERY_SQL.GET_REQUEST_DATA(O_error_message,
                                                        O_businessObject,
                                                        I_tax_service_id) = FALSE then
          O_status_code := 'E';
          return FALSE;
       end if;
    end if;

   O_status_code := 'S';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      O_status_code := 'E';
      return FALSE;
END GET_FISCAL_FDN_DATA;
---------------------------------------------------------------------------------------------
FUNCTION SET_FISCAL_FDN_DATA(O_error_message     IN  OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status_code       IN  OUT  VARCHAR2,
                             I_tax_service_id    IN       L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE,
                             I_businessObject    IN       "RIB_FiscalFDNColRBM_REC")
RETURN BOOLEAN IS

   L_program              VARCHAR2(255) := 'L10N_BR_TAX_SERVICE_MNGR_SQL.SET_FISCAL_FDN_DATA';
   L_type                 L10N_BR_TAX_CALL_STAGE_ROUTING.CLIENT_ID%TYPE;
   L_update_history_ind   L10N_BR_TAX_CALL_STAGE_ROUTING.UPDATE_HISTORY_IND%TYPE;

   cursor C_TYPE is
     select client_id
       from l10n_br_tax_call_stage_routing
      where tax_service_id = I_tax_service_id;

BEGIN

   -- Check required input parameter
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      O_status_code := 'E';
      return FALSE;
   end if;

   -- Fetch routing information
   open C_TYPE;
   fetch C_TYPE into L_type;
   if C_TYPE%NOTFOUND then
      close C_TYPE;
      O_error_message:= SQL_LIB.CREATE_MSG('L10N_ROUTE_INFO_NOT_FOUND', I_tax_service_id,
                                           NULL, NULL);
      O_status_code := 'E';
      return FALSE;
   end if;
   close C_TYPE;

   if L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.FISCAL_FDN  then
       if L10N_BR_FISCAL_FDN_QUERY_SQL.STAGE_RESULTS(O_error_message,
                                                     I_businessObject,
                                                     I_tax_service_id) = FALSE then
          O_status_code := 'E';
          return FALSE;
       end if;
    end if;

   O_status_code := 'S';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      O_status_code := 'E';
      return FALSE;
END SET_FISCAL_FDN_DATA;
-----------------------------------------------------------------------------------------------------
FUNCTION CLEAR_TABLES(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tax_service_id   IN     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   PRAGMA          AUTONOMOUS_TRANSACTION;
   L_program       VARCHAR2(255) := 'L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES';
   L_type          L10N_BR_TAX_CALL_STAGE_ROUTING.CLIENT_ID%TYPE;
   L_table         VARCHAR2(50);

   cursor C_TYPE is
     select client_id
       from l10n_br_tax_call_stage_routing
      where tax_service_id = I_tax_service_id;

   cursor C_LOCK_ROUTING is
      select 'x'
        from l10n_br_tax_call_stage_routing
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_STAGE_RMS is
      select 'x'
        from l10n_br_tax_call_stage_rms
       where tax_service_id = I_tax_service_id
         for update nowait;

  cursor C_LOCK_TAX_CALL_STAGE_FSC_FDN is
     select 'x'
       from l10n_br_tax_call_stage_fsc_fdn
      where tax_service_id = I_tax_service_id
        for update nowait;

   cursor C_LOCK_TAX_CALL_RES_TAX_CNTRB is
      select 'x'
        from l10n_br_tax_call_res_tax_cntrb
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_RES_SRVC_PRD  is
      select 'x'
        from l10n_br_tax_call_res_srvc_prd
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_RES_ECO_CLASS  is
      select 'x'
        from l10n_br_tax_call_res_eco_class
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_RES_ITEM_RULE  is
      select 'x'
        from l10n_br_tax_call_res_item_rule
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_RES_ITEM_TAX  is
      select 'x'
        from l10n_br_tax_call_res_item_tax
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_RES_ITEM  is
      select 'x'
        from l10n_br_tax_call_res_item
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_RES  is
      select 'x'
        from l10n_br_tax_call_res
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_RES_FSC_FND is
      select 'x'
        from l10n_br_tax_call_res_fsc_fnd
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_CALL_RES_FSC_FNDCNT is
      select 'x'
        from l10n_br_tax_call_res_fsc_count
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_STAGE_ECO is
      select 'x'
        from l10n_br_tax_stage_eco
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_STAGE_FIS_ENTITY is
      select 'x'
        from l10n_br_tax_stage_fis_entity
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_STAGE_ITEM is
      select 'x'
        from l10n_br_tax_stage_item
       where tax_service_id = I_tax_service_id
       for update nowait;

   cursor C_LOCK_TAX_STAGE_NAME_VALUE is
      select 'x'
        from l10n_br_tax_stage_name_value
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_STAGE_ORDER is
      select 'x'
        from l10n_br_tax_stage_order
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_STAGE_ORDER_EXP is
      select 'x'
        from l10n_br_tax_stage_order_exp
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_STAGE_ORDER_INFO is
      select 'x'
        from l10n_br_tax_stage_order_info
       where tax_service_id = I_tax_service_id
         for update nowait;

   cursor C_LOCK_TAX_STAGE_REGIME is
      select 'x'
        from l10n_br_tax_stage_regime
       where tax_service_id = I_tax_service_id
         for update nowait;

BEGIN

   -- Check required input parameter
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      return FALSE;
   end if;

   -- Fetch routing information
   open C_TYPE;
   fetch C_TYPE into L_type;
   if C_TYPE%NOTFOUND then
      close C_TYPE;
      O_error_message:= SQL_LIB.CREATE_MSG('L10N_ROUTE_INFO_NOT_FOUND', I_tax_service_id,
                                           NULL, NULL);
     return FALSE;
   end if;
   close C_TYPE;

   if L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.RMS_TAX  then
      L_table := 'L10N_BR_TAX_CALL_STAGE_RMS';
      open C_LOCK_TAX_CALL_STAGE_RMS;
      close C_LOCK_TAX_CALL_STAGE_RMS;
      delete from l10n_br_tax_call_stage_rms
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_RES_SRVC_PRD';
      open C_LOCK_TAX_CALL_RES_SRVC_PRD;
      close C_LOCK_TAX_CALL_RES_SRVC_PRD;
      delete from l10n_br_tax_call_res_srvc_prd
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_RES_ITEM_RULE';
      open C_LOCK_TAX_CALL_RES_ITEM_RULE;
      close C_LOCK_TAX_CALL_RES_ITEM_RULE;
      delete from l10n_br_tax_call_res_item_rule
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_RES_ITEM_TAX';
      open C_LOCK_TAX_CALL_RES_ITEM_TAX;
      close C_LOCK_TAX_CALL_RES_ITEM_TAX;
      delete from l10n_br_tax_call_res_item_tax
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_RES_ITEM';
      open C_LOCK_TAX_CALL_RES_ITEM;
      close C_LOCK_TAX_CALL_RES_ITEM;
      delete from l10n_br_tax_call_res_item
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_RES_TAX_CNTRB';
      open C_LOCK_TAX_CALL_RES_TAX_CNTRB;
      close C_LOCK_TAX_CALL_RES_TAX_CNTRB;
      delete from l10n_br_tax_call_res_tax_cntrb
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_RES_ECO_CLASS';
      open C_LOCK_TAX_CALL_RES_ECO_CLASS;
      close C_LOCK_TAX_CALL_RES_ECO_CLASS;
      delete from l10n_br_tax_call_res_eco_class
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_RES';
      open C_LOCK_TAX_CALL_RES;
      close C_LOCK_TAX_CALL_RES;
      delete from l10n_br_tax_call_res
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_STAGE_ECO';
      open C_LOCK_TAX_STAGE_ECO;
      close C_LOCK_TAX_STAGE_ECO;
      delete from l10n_br_tax_stage_eco
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_STAGE_FIS_ENTITY';
      open C_LOCK_TAX_STAGE_FIS_ENTITY;
      close C_LOCK_TAX_STAGE_FIS_ENTITY;
      delete from l10n_br_tax_stage_fis_entity
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_STAGE_ITEM';
      open C_LOCK_TAX_STAGE_ITEM;
      close C_LOCK_TAX_STAGE_ITEM;
      delete from l10n_br_tax_stage_item
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_STAGE_NAME_VALUE';
      open C_LOCK_TAX_STAGE_NAME_VALUE;
      close C_LOCK_TAX_STAGE_NAME_VALUE;
      delete from l10n_br_tax_stage_name_value
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_STAGE_ORDER';
      open C_LOCK_TAX_STAGE_ORDER;
      close C_LOCK_TAX_STAGE_ORDER;
      delete from l10n_br_tax_stage_order
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_STAGE_ORDER_EXP';
      open C_LOCK_TAX_STAGE_ORDER_EXP;
      close C_LOCK_TAX_STAGE_ORDER_EXP;
      delete from l10n_br_tax_stage_order_exp
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_STAGE_ORDER_INFO';
      open C_LOCK_TAX_STAGE_ORDER_INFO;
      close C_LOCK_TAX_STAGE_ORDER_INFO;
      delete from l10n_br_tax_stage_order_info
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_STAGE_REGIME';
      open C_LOCK_TAX_STAGE_REGIME;
      close C_LOCK_TAX_STAGE_REGIME;
      delete from l10n_br_tax_stage_regime
         where tax_service_id = I_tax_service_id;

   elsif
      L_type = L10N_BR_TAX_SERVICE_MNGR_SQL.FISCAL_FDN then
      L_table := 'L10N_BR_TAX_CALL_STAGE_FSC_FDN';
      open C_LOCK_TAX_CALL_STAGE_FSC_FDN;
      close C_LOCK_TAX_CALL_STAGE_FSC_FDN;
      delete from l10n_br_tax_call_stage_fsc_fdn
       where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_STAGE_FSC_FDN';
      open C_LOCK_TAX_CALL_RES_FSC_FND ;
      close C_LOCK_TAX_CALL_RES_FSC_FND;
      delete from l10n_br_tax_call_res_fsc_fnd
         where tax_service_id = I_tax_service_id;
      ---

      L_table := 'L10N_BR_TAX_CALL_RES_FSC_COUNT';
      open C_LOCK_TAX_CALL_RES_FSC_FNDCNT ;
      close C_LOCK_TAX_CALL_RES_FSC_FNDCNT;
      delete from l10n_br_tax_call_res_fsc_count
         where tax_service_id = I_tax_service_id;

   end if;

   -- Delete routing information
   L_table := 'L10N_BR_TAX_CALL_STAGE_ROUTING';
   open C_LOCK_ROUTING;
   close C_LOCK_ROUTING;
   delete from l10n_br_tax_call_stage_routing
      where tax_service_id = I_tax_service_id;

   commit;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('L_program',
                                             L_table,
                                             I_tax_service_id);
      return FALSE;
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLEAR_TABLES;
---------------------------------------------------------------------------------------------------------------
END L10N_BR_TAX_SERVICE_MNGR_SQL;
/
