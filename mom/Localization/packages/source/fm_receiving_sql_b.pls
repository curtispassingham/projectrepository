create or replace PACKAGE BODY FM_RECEIVING_SQL as
----------------------------------------------------------------------------------
   TYPE L_DETAIL_REC is RECORD (pack                      FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                                fiscal_doc_id             FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                recv_no                   FM_RECEIVING_HEADER.RECV_NO%TYPE,
                                requisition_type          FM_RECEIVING_HEADER.REQUISITION_TYPE%TYPE,
                                location_type             FM_RECEIVING_HEADER.LOCATION_TYPE%TYPE,
                                location_id               FM_RECEIVING_HEADER.LOCATION_ID%TYPE,
                                recv_type                 FM_RECEIVING_HEADER.RECV_TYPE%TYPE,
                                currency_code             FM_RECEIVING_HEADER.CURRENCY_CODE%TYPE,
                                recv_date                 FM_RECEIVING_HEADER.RECV_DATE%TYPE,
                                document_type             FM_RECEIVING_HEADER.DOCUMENT_TYPE%TYPE,
                                status                    FM_RECEIVING_HEADER.STATUS%TYPE,
                                schedule_no               FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                                seq_no                    FM_RECEIVING_HEADER.SEQ_NO%TYPE,
                                header_seq_no             FM_RECEIVING_DETAIL.HEADER_SEQ_NO%TYPE,
                                rcvd_item                 FM_RECEIVING_DETAIL.ITEM%TYPE,
                                nf_item                   FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                                rcvd_qty                  FM_RECEIVING_DETAIL.QUANTITY%TYPE,
                                requisition_no            FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE,
                                nic_cost                  FM_RECEIVING_DETAIL.NIC_COST%TYPE,
                                ebc_cost                  FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                                fiscal_doc_line_id        FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                nf_qty                    FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                                nf_unit_cost_with_disc    FM_FISCAL_DOC_DETAIL.UNIT_COST_WITH_DISC%TYPE,
                                nf_unit_cost              FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                                nf_total_cost             FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE,
                                tax_det_discrep           FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                tax_head_discrep          FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE,
                                qty_discrep_status        FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE,
                                cost_discrep_status       FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE,
                                po_unit_cost              V_ORDLOC.UNIT_COST%TYPE,
                                po_total_cost             V_ORDLOC.UNIT_COST%TYPE,
                                freight_cost              FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE,
                                other_expenses_cost       FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE,
                                insurance_cost            FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE,
                                pack_no                   FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                                pack_ind                  FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                                pack_qty                  PACKITEM.PACK_QTY%TYPE);

----------------------------------------------------------------------------------
-- Function Name: UPDATE_RCV_DETAILS
-- Purpose:       This function updates the status in FM_RECEIVING_HEADER table
--
----------------------------------------------------------------------------------
FUNCTION UPDATE_RCV_DETAILS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_id       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            I_status              IN     FM_RECEIVING_HEADER.STATUS%TYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.FM_UPDATE_RCV_DETAILS';
   L_key                 VARCHAR2(80) := 'Fiscal_Doc_Id = '|| I_fiscal_doc_id;
   ---
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_FM_RCV_HDR is
   select 'x'
     from fm_receiving_header frh
    where frh.fiscal_doc_id = I_fiscal_doc_id
      for update nowait;
begin
   open C_LOCK_FM_RCV_HDR;
   close C_LOCK_FM_RCV_HDR;
   ---
   update FM_RECEIVING_HEADER
      set status               = I_status,
          last_update_datetime = SYSDATE,
          last_update_id       = USER
    where fiscal_doc_id        = I_fiscal_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_RECEIVING_HEADER',
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FM_RCV_HDR%ISOPEN then
         close C_LOCK_FM_RCV_HDR;
      end if;
      ---
      return FALSE;
   ---
   when OTHERS then
      ---
      if C_LOCK_FM_RCV_HDR%ISOPEN then
         close C_LOCK_FM_RCV_HDR;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
      ---
END UPDATE_RCV_DETAILS;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_RCV_STATUS
-- Purpose:       This function updates the status in FM_RECEIVING_HEADER table
--
----------------------------------------------------------------------------------
FUNCTION UPDATE_RCV_STATUS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                           I_status            IN     FM_RECEIVING_HEADER.STATUS%TYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.UPDATE_RCV_STATUS';
   L_key                 VARCHAR2(80) := 'Fiscal_Doc_Id = '|| I_fiscal_doc_id;
   ---
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_FM_RCV_HDR is
   select 'x'
     from fm_receiving_header frh
    where frh.fiscal_doc_id = I_fiscal_doc_id
      for update nowait;
begin
   ---
   open C_LOCK_FM_RCV_HDR;
   close C_LOCK_FM_RCV_HDR;
   ---
   update fm_receiving_header
      set status               = I_status,
          last_update_datetime = SYSDATE,
          last_update_id       = USER
    where fiscal_doc_id        = I_fiscal_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_RECEIVING_HEADER',
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FM_RCV_HDR%ISOPEN then
         close C_LOCK_FM_RCV_HDR;
      end if;
      ---
      return FALSE;
   ---
   when OTHERS then
      ---
      if C_LOCK_FM_RCV_HDR%ISOPEN then
         close C_LOCK_FM_RCV_HDR;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
      ---
END UPDATE_RCV_STATUS;
----------------------------------------------------------------------------------
-- Function Name: UPSERT_TAX_WAC
-- Purpose:       This function inserts records into FM_FISCAL_DOC_TAX_DETAIL_WAC
--                table, updates if already exists
----------------------------------------------------------------------------------
FUNCTION UPSERT_TAX_WAC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_record         IN     FM_FISCAL_DOC_TAX_DETAIL_WAC%ROWTYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.UPSERT_TAX_WAC';
   L_key                 VARCHAR2(80) := 'Fiscal_Doc_Line_Id = '|| I_record.fiscal_doc_line_id||' Tax Code = '||I_record.tax_code;
   ---
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_EXISTS is
   select 'x'
     from fm_fiscal_doc_tax_detail_wac
    where fiscal_doc_line_id = I_record.fiscal_doc_line_id
      and tax_code = I_record.tax_code;

   cursor C_LOCK_FM_TAX_WAC is
   select 'x'
     from fm_fiscal_doc_tax_detail_wac
    where fiscal_doc_line_id = I_record.fiscal_doc_line_id
      and tax_code = I_record.tax_code
      for update nowait;
   ---
   L_exists VARCHAR2(1):=null;
   ---
begin

   open C_EXISTS;
   fetch C_EXISTS into L_exists;
   close C_EXISTS;

   if L_exists is NOT NULL then
      ---
      open C_LOCK_FM_TAX_WAC;
      close C_LOCK_FM_TAX_WAC;
      ---
      update FM_FISCAL_DOC_TAX_DETAIL_WAC
         set tax_rate             = I_record.tax_rate,
             unit_tax_value       = I_record.unit_tax_value,
             unit_rec_value       = I_record.unit_rec_value,
             tax_basis            = I_record.tax_basis,
             rcvd_qty             = I_record.rcvd_qty,
             last_update_datetime = SYSDATE,
             last_update_id       = USER
       where fiscal_doc_line_id   = I_record.fiscal_doc_line_id
         and tax_code = I_record.tax_code;
   else
      if (( I_record.tax_rate <> 0) and (I_record.unit_tax_value <> 0) and (I_record.tax_basis <> 0)) then
         insert into FM_FISCAL_DOC_TAX_DETAIL_WAC(TAX_CODE,
                                                  FISCAL_DOC_LINE_ID,
                                                  TAX_RATE,
                                                  UNIT_TAX_VALUE,
                                                  UNIT_REC_VALUE,
                                                  TAX_BASIS,
                                                  RCVD_QTY,
                                                  CREATE_DATETIME,
                                                  CREATE_ID,
                                                  LAST_UPDATE_DATETIME,
                                                  LAST_UPDATE_ID)
                                          values (I_record.tax_code,
                                                  I_record.fiscal_doc_line_id,
                                                  I_record.tax_rate,
                                                  I_record.unit_tax_value,
                                                  I_record.unit_rec_value,
                                                  I_record.tax_basis,
                                                  I_record.rcvd_qty,
                                                  SYSDATE,
                                                  USER,
                                                  SYSDATE,
                                                  USER);
      end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_TAX_DETAIL_WAC',
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FM_TAX_WAC%ISOPEN then
         close C_LOCK_FM_TAX_WAC;
      end if;
      ---
      return FALSE;
   ---
   when OTHERS then
      ---
      if C_EXISTS%ISOPEN then
         close C_EXISTS;
      end if;
      ---
      if C_LOCK_FM_TAX_WAC%ISOPEN then
         close C_LOCK_FM_TAX_WAC;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
---
END UPSERT_TAX_WAC;

----------------------------------------------------------------------------------
-- Function Name: UPDATE_FM_RECEIVING_DETAIL
-- Purpose:       This function updates FM_RECEIVING_DETAIL with the new NIC and
--                EBC Costs for a given Line Item
----------------------------------------------------------------------------------
FUNCTION UPDATE_FM_RECEIVING_DETAIL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_seq_no          IN     FM_RECEIVING_DETAIL.SEQ_NO%TYPE,
                                    I_nic_cost        IN     FM_RECEIVING_DETAIL.NIC_COST%TYPE,
                                    I_ebc_cost        IN     FM_RECEIVING_DETAIL.EBC_COST%TYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.UPDATE_FM_RECEIVING_DETAIL';
   L_key                 VARCHAR2(80) := 'Seq No = '|| I_Seq_no;
   ---
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_FM_RCV_DET is
   select 'x'
     from fm_receiving_detail
    where seq_no = I_seq_no
      for update nowait;
begin
   ---  include code to update fm_receiving_detail table - discuss
   open C_LOCK_FM_RCV_DET;
   close C_LOCK_FM_RCV_DET;
   ---
   update FM_RECEIVING_DETAIL
      set nic_cost             = I_nic_cost,
          ebc_cost             = I_ebc_cost,
          last_update_datetime = SYSDATE,
          last_update_id       = USER
    where seq_no = I_seq_no;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_RECEIVING_DETAIL',
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FM_RCV_DET%ISOPEN then
         close C_LOCK_FM_RCV_DET;
      end if;
      ---
      return FALSE;
   ---
   when OTHERS then
      ---
      if C_LOCK_FM_RCV_DET%ISOPEN then
         close C_LOCK_FM_RCV_DET;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
---
END UPDATE_FM_RECEIVING_DETAIL;
----------------------------------------------------------------------------------
-- Function Name: get_resolution_type
-- Purpose:       This function fetches the resolution type from FM_RESOLUTION for
--                a given Line Id and Cost Discrepancy type
----------------------------------------------------------------------------------
FUNCTION GET_COST_RESOLUTION_TYPE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_fiscal_doc_line_id    IN     FM_RESOLUTION.FISCAL_DOC_LINE_ID%TYPE,
                                  I_discrep_type          IN     FM_RESOLUTION.DISCREP_TYPE%TYPE,
                                  O_resolution_type       IN OUT FM_RESOLUTION.RESOLUTION_TYPE%TYPE)
   return BOOLEAN is
   ---
   cursor C_RESOLUTION is
   select resolution_type
     from FM_RESOLUTION
    where fiscal_doc_line_id = I_fiscal_doc_line_id
      and discrep_type = I_discrep_type;
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.GET_COST_RESOLUTION_TYPE';
   L_key                 VARCHAR2(80) := 'Fiscal Doc Line Id = '|| I_fiscal_doc_line_id;
   ---
begin
   ---
   open C_RESOLUTION;
   fetch C_RESOLUTION into O_resolution_type;
   close C_RESOLUTION;
   ---
   return TRUE;
   ---
EXCEPTION
   ---
   when OTHERS then
      ---
      if C_RESOLUTION%ISOPEN then
         close C_RESOLUTION;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
---
end GET_COST_RESOLUTION_TYPE;
----------------------------------------------------------------------------------
-- Function Name: GET_TAX_RESOLUTION_TYPE
-- Purpose:       This function fetches resolution type for a given Line Id and
--                TAX Code
----------------------------------------------------------------------------------
FUNCTION GET_TAX_RESOLUTION_TYPE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_fiscal_doc_line_id    IN     FM_RESOLUTION.FISCAL_DOC_LINE_ID%TYPE,
                                 I_discrep_type          IN     FM_RESOLUTION.DISCREP_TYPE%TYPE,
                                 I_tax_code              IN     FM_RESOLUTION.TAX_CODE%TYPE,
                                 O_resolution_type       IN OUT FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                                 O_exists                IN OUT BOOLEAN)
   return BOOLEAN is
   ---
   L_res_sys                  FM_RESOLUTION.RESOLUTION_TYPE%TYPE default 'SYS';

   cursor C_RESOLUTION is
   select NVL(resolution_type, L_res_sys)
     from FM_RESOLUTION
    where fiscal_doc_line_id = I_fiscal_doc_line_id
      and tax_code = I_tax_code
      and discrep_type = I_discrep_type;
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.GET_TAX_RESOLUTION_TYPE';
   L_key                 VARCHAR2(80) := 'Fiscal Doc Line Id = '|| I_fiscal_doc_line_id||' Vat Code '||I_tax_code;
   ---
begin
   ---
   open C_RESOLUTION;
   fetch C_RESOLUTION into O_resolution_type;

   if C_RESOLUTION%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_RESOLUTION;
   ---
   return TRUE;
   ---
EXCEPTION
   ---
   when OTHERS then
      ---
      if C_RESOLUTION%ISOPEN then
         close C_RESOLUTION;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
---
end GET_TAX_RESOLUTION_TYPE;
----------------------------------------------------------------------------------
-- Function Name: GET_NIC_IND
-- Purpose:       This function gets the INCL_INC_IND for a given Tax Code
--                from  the VAT_CODES table
----------------------------------------------------------------------------------
FUNCTION GET_NIC_IND(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_vat_code       IN     VAT_CODES.VAT_CODE%TYPE,
                     O_nic_ind        IN OUT VAT_CODES.INCL_NIC_IND%TYPE)
   return BOOLEAN is
   ---
   cursor C_NIC_IND is
   select incl_nic_ind
     from VAT_CODES
    where vat_code = I_vat_code;
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.GET_NIC_IND';
   L_key                 VARCHAR2(80) := ' Vat Code '||I_vat_code;
   ---
begin
   ---
   open C_NIC_IND;
   fetch C_NIC_IND into O_nic_ind;
   close C_NIC_IND;
   ---
   return TRUE;
   ---
EXCEPTION
   ---
   when OTHERS then
      ---
      if C_NIC_IND%ISOPEN then
         close C_NIC_IND;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
---
end GET_NIC_IND;
----------------------------------------------------------------------------------
-- Function Name: GET_OBJ_TAX_VALUES
-- Purpose:       This function retrieves values from Tax Object based on tax code
----------------------------------------------------------------------------------
FUNCTION GET_OBJ_TAX_VALUES(I_line_tax_obj_tbl     IN    "RIB_TaxDetRBO_TBL",
                             O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status               IN OUT INTEGER,
                             I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                             I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                             I_tax_code             IN     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE,
                             O_tax_value            IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE,
                             O_tax_base             IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE,
                             O_modified_tax_base    IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.MODIFIED_TAX_BASIS%TYPE,
                             O_tax_rate             IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_RATE%TYPE,
                             O_tax_rec_val          IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.UNIT_REC_VALUE%TYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES';
   L_key                 VARCHAR2(80) := 'Fiscal_Doc_Line_Id = '|| I_fiscal_doc_Line_id ||' Tax Code = '||I_tax_code;
   L_exists              BOOLEAN;
   ---
begin
   ---
   L_exists := FALSE;
   ---
   FOR C_TAX_DETAIL IN (select tax_code, tax_rate, tax_basis_amount, modified_tax_basis_amount, tax_amount,
                               legal_message, tax_rate_type, reg_pct_margin_value, regulated_price, recoverable_amt
                          from table(I_line_tax_obj_tbl))

   LOOP
      ---

      if C_TAX_DETAIL.tax_Code = I_tax_code then
         O_tax_rate    := C_TAX_DETAIL.tax_Rate;
         O_tax_base    := C_TAX_DETAIL.tax_Basis_Amount;
         O_modified_tax_base    := C_TAX_DETAIL.modified_tax_Basis_Amount;
         O_tax_value   := C_TAX_DETAIL.Tax_Amount;
         O_tax_rec_val := C_TAX_DETAIL.recoverable_Amt;
         L_exists      := TRUE;

      end if;
      ---
   END LOOP;
   ---
   if NOT L_exists then
   ---
      O_tax_rate    := NULL;
      O_tax_base    := NULL;
      O_modified_tax_base := NULL;
      O_tax_value   := NULL;
      O_tax_rec_val := NULL;
   ---
   end if;
   ---

   return true;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
      ---
END GET_OBJ_TAX_VALUES;
----------------------------------------------------------------------------------
-- Function Name: CALC_FINAL_EBC
-- Purpose:       This function calls CALC_FINAL_EBC for each line item in a NF
----------------------------------------------------------------------------------
FUNCTION CALC_FINAL_EBC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_schedule_no        IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_yes                 FM_TAX_CODES.MATCHING_IND%TYPE default 'Y';
   L_no                  FM_TAX_CODES.MATCHING_IND%TYPE default 'N';
   ---
--   cursor C_NF_HEAD is
--   select fiscal_doc_id, requisition_type
--     from FM_FISCAL_DOC_HEADER ffdh
--    where ffdh.schedule_no = I_schedule_no;
   ---
   cursor C_NF_DETAIL_REG_ITEMS is
   select distinct ffdh.fiscal_doc_id, ffdh.requisition_type
     from FM_FISCAL_DOC_DETAIL ffdd, FM_FISCAL_DOC_HEADER ffdh
    where ffdd.fiscal_doc_id = ffdh.fiscal_doc_id
      and ffdd.pack_no IS NULL
      and ffdd.unexpected_item = L_no
      and ffdh.schedule_no = I_schedule_no
      and ffdh.status not in ('I','CN')
    order by ffdh.fiscal_doc_id;
   ---
   cursor C_NF_DETAIL_PACK_ITEMS is
   select distinct ffdd.pack_no, ffdh.fiscal_doc_id, ffdh.requisition_type
     from FM_FISCAL_DOC_DETAIL ffdd, FM_FISCAL_DOC_HEADER ffdh
    where ffdd.fiscal_doc_id = ffdh.fiscal_doc_id
      and ffdd.unexpected_item = L_no
      and ffdd.pack_no IS NOT NULL
      and ffdh.status not in ('I','CN')
      and ffdh.schedule_no = I_schedule_no;
   ---
--   L_nf_head             C_NF_HEAD%ROWTYPE;
   L_nf_detail           C_NF_DETAIL_REG_ITEMS%ROWTYPE;
   L_nf_detail_pack_item C_NF_DETAIL_PACK_ITEMS%ROWTYPE;
   L_rcv_hdr_status      FM_RECEIVING_HEADER.STATUS%TYPE default 'P';
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.CALC_FINAL_EBC';
   L_key                 VARCHAR2(80) := 'Schedule No = '|| I_schedule_no;
   L_old_ebc             FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   ---
   L_old_nic             FM_RECEIVING_DETAIL.NIC_COST%TYPE;
	 L_new_ebc             FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_new_nic             FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   ---
   L_po                  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE default 'PO';
   L_tsf                 FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE default 'TSF';
   L_ic                  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE default 'IC';
   L_rep                 FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE default 'REP';
   ---
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
begin
   ---
   for L_nf_detail in C_NF_DETAIL_REG_ITEMS loop
      ---
      if L_nf_detail.requisition_type = L_po then
         if FM_RECEIVING_SQL.CALC_FINAL_EBC(O_error_message,
                                            L_nf_detail.fiscal_doc_id,
                                            NULL,
                                            NULL, -- Pack_no
                                            L_old_ebc,
                                            L_old_nic,
                                            L_new_ebc,
                                            L_new_nic) = FALSE then
                        return FALSE;
         end if;

      elsif L_nf_detail.requisition_type in (L_tsf, L_ic, L_rep) then
         if FM_RECEIVING_SQL.CALC_FINAL_EBC_TSF(L_error_message,
                                                L_nf_detail.fiscal_doc_id,
                                                NULL, -- Pack_no
                                                L_old_ebc,
                                                L_old_nic,
                                                L_new_ebc,
                                                L_new_nic) = FALSE then

            O_error_message := L_error_message;
            return FALSE;
         end if;
      end if;
      ---
      if FM_RECEIVING_SQL.UPDATE_RCV_STATUS(L_error_message,
                                            L_nf_detail.fiscal_doc_id,
                                            L_rcv_hdr_status) = FALSE then
         O_error_message := L_error_message;
         return FALSE;
      end if;
      ---
   end loop;

  ---
   for L_nf_detail_pack_item in C_NF_DETAIL_PACK_ITEMS loop
      ---
      if L_nf_detail_pack_item.requisition_type = L_po then
         if FM_RECEIVING_SQL.CALC_FINAL_EBC(L_error_message,
                                            L_nf_detail_pack_item.fiscal_doc_id,
                                            NULL, --L_nf_detail.fiscal_doc_line_id,
                                            L_nf_detail_pack_item.pack_no,
                                            L_old_ebc,
                                            L_old_nic,
                                            L_new_ebc,
                                            L_new_nic) = FALSE then
                        O_error_message := L_error_message;
            return FALSE;
         end if;
      elsif L_nf_detail_pack_item.requisition_type in (L_tsf, L_ic, L_rep) then
         if FM_RECEIVING_SQL.CALC_FINAL_EBC_TSF(L_error_message,
                                                L_nf_detail_pack_item.fiscal_doc_id, --L_nf_detail.fiscal_doc_id,
                                                L_nf_detail_pack_item.pack_no,
                                                L_old_ebc,
                                                L_old_nic,
                                                L_new_ebc,
                                                L_new_nic) = FALSE then
            O_error_message := L_error_message;
            return FALSE;
         end if;
      end if;
      ---
      if FM_RECEIVING_SQL.UPDATE_RCV_STATUS(L_error_message,
                                            L_nf_detail_pack_item.fiscal_doc_id,
                                            L_rcv_hdr_status) = FALSE then
         O_error_message := L_error_message;
         return FALSE;
      end if;
      ---
   end loop;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
      ---
end CALC_FINAL_EBC;
----------------------------------------------------------------------------------
-- Function Name: CALC_FINAL_EBC
-- Purpose:       This function inserts a record into FM_CORRECTION_DOC and
--                FM_CORRECTION_TAX_DOC tables
----------------------------------------------------------------------------------
FUNCTION CALC_FINAL_EBC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                        I_fiscal_doc_line_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                        I_pack_no            IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                        O_old_ebc            IN OUT FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                        O_old_nic            IN OUT FM_RECEIVING_DETAIL.NIC_COST%TYPE,
                        O_new_ebc            IN OUT FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                        O_new_nic            IN OUT FM_RECEIVING_DETAIL.NIC_COST%TYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.CALC_FINAL_EBC';
   L_key                 VARCHAR2(80) := 'Fiscal_Doc_Line_Id = '|| I_fiscal_doc_Line_id;
   L_yes                 FM_TAX_CODES.MATCHING_IND%TYPE default 'Y';
   L_no                  FM_TAX_CODES.MATCHING_IND%TYPE default 'N';
   L_store               FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE default 'S';
   L_wh                  FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE default 'W';
   ---
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_NF_LINE_TAXES(P_fiscal_doc_line_id     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                          P_tr_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
   select 'NF' tax_source,
          vat_code,
          fiscal_doc_line_id,
          nvl(percentage_rate,0) percentage_rate,
          nvl(total_value,0) total_value,
          nvl(res_base_value,nvl(appr_base_value,tax_basis)) nf_base_value,
          nvl(res_modified_base_value,nvl(appr_modified_base_value,modified_tax_basis)) nf_modified_base_value,
          nvl(res_tax_value, nvl(appr_tax_value,total_value)) nf_tax_value,
          nvl(res_tax_rate,nvl(appr_tax_rate,percentage_rate)) nf_tax_rate,
          nvl(tax_basis,0) tax_basis,
          nvl(modified_tax_basis,0) modified_tax_basis,
          nvl(rec_value,0) rec_value,
          nvl(unit_tax_amt,0) unit_tax_amt,
          nvl(unit_rec_value,0) unit_rec_val,
          (select resolution_type
             from FM_RESOLUTION fr
            where fdtd.fiscal_doc_line_id = fr.fiscal_doc_line_id
              and fdtd.vat_code = fr.tax_code) resolution_type,
          (select ffdd.fiscal_doc_id
             from FM_FISCAL_DOC_DETAIL ffdd
            where ffdd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id) fiscal_doc_id
     from FM_FISCAL_DOC_TAX_DETAIL fdtd, FM_TAX_CODES ftc
    where fdtd.fiscal_doc_line_id in (P_fiscal_doc_line_id, P_tr_fiscal_doc_line_id)
      and fdtd.vat_code = ftc.tax_code
      and ftc.matching_ind = L_yes
  union
  select 'SYS' tax_source,
         fdtde.tax_code vat_code,
          fiscal_doc_line_id,
         nvl(tax_rate,0)  percentage_rate,
          nvl(tax_value,0) total_value,
          nvl(tax_basis,0) nf_base_value,
          nvl(modified_tax_basis,0) nf_modified_base_value,
          nvl(tax_value,0) nf_tax_value,
          nvl(tax_rate,0)  nf_tax_rate,
          nvl(tax_basis,0) tax_basis,
          nvl(modified_tax_basis,0) modified_tax_basis,
          nvl(tax_rec_value,0) rec_value,
          nvl(unit_tax_amt,0) unit_tax_amt,
          nvl(unit_rec_value,0) unit_rec_value,
          (select resolution_type
             from FM_RESOLUTION fr
            where fdtde.fiscal_doc_line_id = fr.fiscal_doc_line_id
              and fdtde.tax_code = fr.tax_code) resolution_type,
          (select ffdd.fiscal_doc_id
             from FM_FISCAL_DOC_DETAIL ffdd
            where ffdd.fiscal_doc_line_id = fdtde.fiscal_doc_line_id) fiscal_doc_id
     from FM_FISCAL_DOC_TAX_DETAIL_EXT fdtde, FM_TAX_CODES ftc
    where fdtde.fiscal_doc_line_id in ( P_fiscal_doc_line_id, P_tr_fiscal_doc_line_id)
      and fdtde.tax_code not in (select ffdtd.vat_code
                                   from fm_fiscal_doc_tax_detail ffdtd
                                  where ffdtd.fiscal_doc_line_id = fdtde.fiscal_doc_line_id)
      and fdtde.tax_code = ftc.tax_code
      and ftc.matching_ind = L_yes
    order by resolution_type;
   ---
   cursor C_SYS_LINE_TAXES(P_fiscal_doc_line_id     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                           P_tax_code               FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE) is
   select tax_code,
          nvl(tax_rate,0) tax_rate,
          nvl(tax_basis,0) tax_basis,
          nvl(modified_tax_basis,0) modified_tax_basis,
          nvl(tax_value,0) tax_value,
          tax_type,
          nvl(tax_perc_mva,0) tax_perc_mva,
          nvl(tax_val_mva, 0) tax_val_mva,
          nvl(tax_rec_value,0) tax_rec_value,
          nvl(unit_tax_amt,0) unit_tax_amt,
          nvl(unit_rec_value,0) unit_rec_value
     from FM_FISCAL_DOC_TAX_DETAIL_EXT
    where fiscal_doc_line_id = P_fiscal_doc_line_id
      and tax_code = P_tax_code;
   ---
--   cursor C_SYS_TAXES_NON_MATCH_IND (P_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
--   select  nvl(sum(unit_tax_amt),0) unit_tax_amt,
--           nvl(sum(unit_rec_value),0) unit_rec_value
--     from FM_FISCAL_DOC_TAX_DETAIL_EXT fdtde, FM_TAX_CODES ftc
--    where fdtde.fiscal_doc_line_id = P_fiscal_doc_line_id
--      and ftc.tax_code = fdtde.tax_code
--      and ftc.matching_ind = L_no;
   ---
   cursor C_SYS_TAXES_NON_MATCH_IND (P_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
   select fdtde.tax_code,
          nvl(unit_tax_amt,0) unit_tax_value,
          nvl(unit_rec_value,0) unit_rec_value,
	         nvl(tax_value,0) tax_value,
	         nvl(tax_rec_value,0) tax_rec_value,
	         tax_rate tax_rate,
	         tax_basis tax_basis,
	         modified_tax_basis modified_tax_basis
     from FM_FISCAL_DOC_TAX_DETAIL_EXT fdtde, FM_TAX_CODES ftc
    where fdtde.fiscal_doc_line_id = P_fiscal_doc_line_id
      and ftc.tax_code = fdtde.tax_code
      and ftc.matching_ind = L_no;
   ---
   cursor C_NON_MATCH_TAX is
   select tax_code
     from FM_TAX_CODES ftc
    where ftc.matching_ind = L_no;
   ---
   cursor C_GET_TRNG_IND(P_order_no  ORDHEAD.ORDER_NO%TYPE) is
   select triangulation_ind
     from ORDHEAD
    where order_no = P_order_no;
   ---
   cursor C_GET_TRNG_LINE_ID(P_order_no      FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                             P_fiscal_doc_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                             P_item          FM_FISCAL_DOC_DETAIL.ITEM%TYPE ) is
   select ffdd.fiscal_doc_line_id
     from fm_fiscal_doc_detail ffdd, fm_fiscal_doc_complement ffdc
    where ffdc.compl_fiscal_doc_id = ffdd.fiscal_doc_id
      and ffdc.fiscal_doc_id  = P_fiscal_doc_id
      and ffdd.requisition_no = P_order_no
      and ffdd.item = P_item;
   ---
   L_sys_line_taxes           C_SYS_LINE_TAXES%ROWTYPE;
   L_sys_line_taxes_null      C_SYS_LINE_TAXES%ROWTYPE;
   L_nf_line_taxes            C_NF_LINE_TAXES%ROWTYPE;
   L_non_match_tax            C_NON_MATCH_TAX%ROWTYPE;
   L_get_trng_line_id         C_GET_TRNG_LINE_ID%ROWTYPE;
   L_tax_ext                  FM_FISCAL_DOC_TAX_DETAIL_EXT%ROWTYPE;
   L_correction_doc           FM_CORRECTION_DOC%ROWTYPE;
   ---
   L_cost_resolution          FM_RESOLUTION.RESOLUTION_TYPE%TYPE;
   L_tax_resolution           FM_RESOLUTION.RESOLUTION_TYPE%TYPE;
   L_nic_ind                  VAT_CODES.INCL_NIC_IND%TYPE;
   ---
   L_resolved                 FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE default 'R';
   L_discrepant               FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE default 'D';
   L_matched                  FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE default 'M';
   ---
   L_cost_discrep_type        FM_RESOLUTION.DISCREP_TYPE%TYPE default 'C';
   L_tax_discrep_type         FM_RESOLUTION.DISCREP_TYPE%TYPE default 'T';
   L_nic_ind_flg              VAT_CODES.INCL_NIC_IND%TYPE default 'Y';
   ---
   L_action_clc               FM_CORRECTION_DOC.ACTION_TYPE%TYPE default 'CLC';
   L_action_cnft              FM_CORRECTION_DOC.ACTION_TYPE%TYPE default 'CNFT';
   L_action_clt               FM_CORRECTION_DOC.ACTION_TYPE%TYPE default 'CLT';
   ---
   L_processed                FM_RECEIVING_HEADER.STATUS%TYPE default 'P';
   L_res_nf                   FM_RESOLUTION.RESOLUTION_TYPE%TYPE default 'NF';
   L_res_sys                  FM_RESOLUTION.RESOLUTION_TYPE%TYPE default 'SYS';
   ---
   L_nic_cost                 FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   L_ebc_cost                 FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_bc_cost                  FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   ---
   L_old_nic_cost             FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   L_old_ebc_cost             FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_appr_tax_value           FM_FISCAL_DOC_TAX_DETAIL.APPR_TAX_VALUE%TYPE;
   ---
   L_unit_tax_value           FM_FISCAL_DOC_TAX_DETAIL.APPR_TAX_VALUE%TYPE;
   L_sys_tax_value            FM_FISCAL_DOC_TAX_DETAIL_WAC.UNIT_TAX_VALUE%TYPE;
   L_sys_tax_rate             FM_FISCAL_DOC_TAX_DETAIL_WAC.TAX_RATE%TYPE;
   ---
   L_sys_tax_base             FM_FISCAL_DOC_TAX_DETAIL_WAC.TAX_BASIS%TYPE;
   L_sys_modified_tax_base    FM_FISCAL_DOC_TAX_DETAIL_WAC.MODIFIED_TAX_BASIS%TYPE;
   L_sys_tax_rec_val          FM_FISCAL_DOC_TAX_DETAIL_EXT.UNIT_REC_VALUE%TYPE;
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   ---
   L_tax_disc_exists          BOOLEAN;
   L_mtr_called               BOOLEAN;
   L_status                   INTEGER;
   ---
   L_tax_discrep_status       FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE;
   L_total_tax_amount         FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   L_total_unit_tax_val       FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   ---
   L_total_rec_val            FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_REC_VALUE%TYPE;
   L_non_match_unit_tax_val   FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   L_non_match_unit_rec_val   FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_REC_VALUE%TYPE;
   ---
--   L_line_tax_obj             OBJ_TAXDETAILRBO    := OBJ_TAXDETAILRBO(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   L_line_tax_obj_tbl         "RIB_TaxDetRBO_TBL"        :=  "RIB_TaxDetRBO_TBL"();
   L_line_tax_obj_tbl_null   "RIB_TaxDetRBO_TBL"        :=  "RIB_TaxDetRBO_TBL"();
   ---
   L_fiscal_doc_id            FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_trng_ind                 ORDHEAD.TRIANGULATION_IND%TYPE;
   L_tax_tab_cnt              INTEGER:=0;
   ---
   L_pack_ebc_cost            FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_pack_bc_cost             FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_pack_nic_cost            FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   ---
   type RC is ref cursor;
   type L_DETAIL_TAB is table of L_DETAIL_REC;
   type L_WAC_TAB is table of FM_FISCAL_DOC_TAX_DETAIL_WAC%ROWTYPE;
   ---
   L_rcv_detail               L_DETAIL_TAB := L_detail_tab(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   L_tax_wac                  L_WAC_TAB := L_wac_tab();
   L_ref_cursor               RC;
   ---
   DML_ERRORS                 EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(DML_ERRORS, -24381);
   ---
begin
   L_pack_ebc_cost := 0;
   L_pack_bc_cost  := 0;
   L_pack_nic_cost := 0;
   L_tax_tab_cnt   := 0;
   ---
   if I_pack_no is null and I_fiscal_doc_line_id is null then
      open L_ref_cursor for select L_no pack,
                                   ffdh.fiscal_doc_id,
                                   frh.recv_no,
                                   frh.requisition_type,
                                   frh.location_type,
                                   frh.location_id,
                                   frh.recv_type,
                                   frh.currency_code,
                                   frh.recv_date,
                                   frh.document_type,
                                   frh.status hdr_status,
                                   ffdh.schedule_no,
                                   frd.seq_no,
                                   frd.header_seq_no,
                                   frd.item rcvd_item,
                                   ffdd.item nf_item,
                                   nvl(frd.quantity,0) rcvd_qty,
                                   frd.requisition_no,
                                   nvl(frd.nic_cost,0) nic_cost,
                                   nvl(frd.ebc_cost,0) ebc_cost,
                                   ffdd.fiscal_doc_line_id,
                                   nvl(ffdd.quantity,0) nf_qty,
                                   nvl(ffdd.unit_cost_with_disc,0) nf_unit_cost_with_disc,
                                   nvl(ffdd.unit_cost,0) nf_unit_cost,
                                   nvl(ffdd.total_cost,0) nf_total_cost,
                                   ffdd.tax_discrep_status tax_det_discrep,
                                   ffdh.tax_discrep_status tax_head_discrep,
                                   ffdd.qty_discrep_status,
                                   ffdd.cost_discrep_status,
                                   nvl(vo.unit_cost,0) po_unit_cost,
                                   sum(DECODE(frh.status, 'P', round(nvl(vo.unit_cost,0) * (nvl(vo.qty_ordered,0) - nvl(vo.qty_received,0) + nvl (frd.quantity,0) ),4), round(nvl(vo.unit_cost,0) * (nvl(vo.qty_ordered,0) - nvl(vo.qty_received,0))))) po_total_cost,
                                   nvl(ffdd.freight_cost,0) freight_cost,
                                   nvl(ffdd.other_expenses_cost,0) other_expenses_cost,
                                   nvl(ffdd.insurance_cost,0) insurance_cost,
                                   ffdd.pack_no,
                                   ffdd.pack_ind,
                                   1 pack_qty
                              from FM_RECEIVING_DETAIL frd, FM_FISCAL_DOC_DETAIL ffdd, ORDLOC vo, FM_FISCAL_DOC_HEADER ffdh, FM_RECEIVING_HEADER frh
                             where frd.item = ffdd.item
                               and frd.requisition_no = ffdd.requisition_no
                               and vo.order_no = ffdd.requisition_no
                               and vo.item = ffdd.item
                               and frh.fiscal_doc_id = ffdh.fiscal_doc_id
                               and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
                               and frh.seq_no=frd.header_seq_no
                               and ffdh.location_id = DECODE(ffdh.location_type, L_store, vo.location, L_wh,(select physical_wh from WH where wh.wh=vo.location))
                               and vo.location  in ((select wh loc
                                                       from wh
                                                      where physical_wh = ffdd.location_id)
                                                    union
                                                    (select store loc
                                                       from store
                                                      where store = ffdd.location_id))
                               and frd.reason_code is NULL
                               and nvl(ffdd.unexpected_item, L_no) = L_no
                               and ffdh.fiscal_doc_id = I_fiscal_doc_id
                              -- and (I_fiscal_doc_line_id is NOT NULL and ffdd.fiscal_doc_line_id = I_fiscal_doc_line_id)
                               and frd.quantity > 0
                               and ffdd.pack_no is NULL
                             group by ffdh.fiscal_doc_id,frh.recv_no,frh.requisition_type,frh.location_type,frh.location_id,frh.recv_type,frh.currency_code,frh.recv_date,frh.document_type,frh.status,ffdh.schedule_no,frd.seq_no,frd.header_seq_no,frd.item,
                                       ffdd.item,nvl(frd.quantity,0),frd.requisition_no,nvl(frd.nic_cost,0),nvl(frd.ebc_cost,0),ffdd.fiscal_doc_line_id,nvl(ffdd.quantity,0),nvl(ffdd.unit_cost_with_disc,0),nvl(ffdd.unit_cost,0),nvl(ffdd.total_cost,0),ffdd.tax_discrep_status,ffdh.tax_discrep_status,ffdd.qty_discrep_status,
                                       ffdd.cost_discrep_status,nvl(vo.unit_cost,0),nvl(ffdd.freight_cost,0),nvl(ffdd.other_expenses_cost,0),nvl(ffdd.insurance_cost,0) ,ffdd.pack_no,ffdd.pack_ind;
   elsif I_pack_no is null and I_fiscal_doc_line_id is not null then
      open L_ref_cursor for select L_no pack,
                                   ffdh.fiscal_doc_id,
                                   frh.recv_no,
                                   frh.requisition_type,
                                   frh.location_type,
                                   frh.location_id,
                                   frh.recv_type,
                                   frh.currency_code,
                                   frh.recv_date,
                                   frh.document_type,
                                   frh.status hdr_status,
                                   ffdh.schedule_no,
                                   frd.seq_no,
                                   frd.header_seq_no,
                                   frd.item rcvd_item,
                                   ffdd.item nf_item,
                                   nvl(frd.quantity,0) rcvd_qty,
                                   frd.requisition_no,
                                   nvl(frd.nic_cost,0) nic_cost,
                                   nvl(frd.ebc_cost,0) ebc_cost,
                                   ffdd.fiscal_doc_line_id,
                                   nvl(ffdd.quantity,0) nf_qty,
                                   nvl(ffdd.unit_cost_with_disc,0) nf_unit_cost_with_disc,
                                   nvl(ffdd.unit_cost,0) nf_unit_cost,
                                   nvl(ffdd.total_cost,0) nf_total_cost,
                                   ffdd.tax_discrep_status tax_det_discrep,
                                   ffdh.tax_discrep_status tax_head_discrep,
                                   ffdd.qty_discrep_status,
                                   ffdd.cost_discrep_status,
                                   nvl(vo.unit_cost,0) po_unit_cost,
                                   sum(DECODE(frh.status, 'P', round(nvl(vo.unit_cost,0) * (nvl(vo.qty_ordered,0) - nvl(vo.qty_received,0) + nvl (frd.quantity,0) ),4), round(nvl(vo.unit_cost,0) * (nvl(vo.qty_ordered,0) - nvl(vo.qty_received,0))))) po_total_cost,
                                   nvl(ffdd.freight_cost,0) freight_cost,
                                   nvl(ffdd.other_expenses_cost,0) other_expenses_cost,
                                   nvl(ffdd.insurance_cost,0) insurance_cost,
                                   ffdd.pack_no,
                                   ffdd.pack_ind,
                                   1 pack_qty
                              from FM_RECEIVING_DETAIL frd, FM_FISCAL_DOC_DETAIL ffdd, ORDLOC vo, FM_FISCAL_DOC_HEADER ffdh, FM_RECEIVING_HEADER frh
                             where frd.item = ffdd.item
                               and frd.requisition_no = ffdd.requisition_no
                               and vo.order_no = ffdd.requisition_no
                               and vo.item = ffdd.item
                               and frh.fiscal_doc_id = ffdh.fiscal_doc_id
                               and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
                               and frh.seq_no=frd.header_seq_no
                               and ffdh.location_id = DECODE(ffdh.location_type, L_store, vo.location, L_wh,(select physical_wh from WH where wh.wh=vo.location))
                               and vo.location  in ((select wh loc
                                                       from wh
                                                      where physical_wh = ffdd.location_id)
                                                    union
                                                    (select store loc
                                                       from store
                                                      where store = ffdd.location_id))
                               and frd.reason_code is NULL
                               and nvl(ffdd.unexpected_item, L_no) = L_no
                               and ffdh.fiscal_doc_id = I_fiscal_doc_id
                               and (I_fiscal_doc_line_id is NOT NULL and ffdd.fiscal_doc_line_id = I_fiscal_doc_line_id)
                               and frd.quantity > 0
                               and ffdd.pack_no is NULL
                             group by ffdh.fiscal_doc_id,frh.recv_no,frh.requisition_type,frh.location_type,frh.location_id,frh.recv_type,frh.currency_code,frh.recv_date,frh.document_type,frh.status,ffdh.schedule_no,frd.seq_no,frd.header_seq_no,frd.item,
                                       ffdd.item,nvl(frd.quantity,0),frd.requisition_no,nvl(frd.nic_cost,0),nvl(frd.ebc_cost,0),ffdd.fiscal_doc_line_id,nvl(ffdd.quantity,0),nvl(ffdd.unit_cost_with_disc,0),nvl(ffdd.unit_cost,0),nvl(ffdd.total_cost,0),ffdd.tax_discrep_status,ffdh.tax_discrep_status,ffdd.qty_discrep_status,
                                       ffdd.cost_discrep_status,nvl(vo.unit_cost,0),nvl(ffdd.freight_cost,0),nvl(ffdd.other_expenses_cost,0),nvl(ffdd.insurance_cost,0) ,ffdd.pack_no,ffdd.pack_ind;
   elsif I_pack_no is NOT NULL then
      open L_ref_cursor for select distinct L_yes pack,
                                   ffdh.fiscal_doc_id,
                                   frh.recv_no,
                                   frh.requisition_type,
                                   frh.location_type,
                                   frh.location_id,
                                   frh.recv_type,
                                   frh.currency_code,
                                   frh.recv_date,
                                   frh.document_type,
                                   frh.status hdr_status,
                                   ffdh.schedule_no,
                                   frd.seq_no,
                                   frd.header_seq_no,
                                   frd.item rcvd_item,
                                   ffdd.item nf_item,
                                   nvl(frd.quantity,0)*nvl(pi.pack_qty,1) rcvd_qty,
                                   frd.requisition_no,
                                   nvl(frd.nic_cost,0) nic_cost,
                                   nvl(frd.ebc_cost,0) ebc_cost,
                                   ffdd.fiscal_doc_line_id,
                                   nvl(ffdd.quantity,0) nf_qty,
                                   nvl(ffdd.unit_cost_with_disc,0) nf_unit_cost_with_disc,
                                   nvl(ffdd.unit_cost,0) nf_unit_cost,
                                   nvl(ffdd.total_cost,0) nf_total_cost,
                                   ffdd.tax_discrep_status tax_det_discrep,
                                   ffdh.tax_discrep_status tax_head_discrep,
                                   ffdd.qty_discrep_status,
                                   ffdd.cost_discrep_status,
                                   round(DECODE(ffdd.pack_no,null,nvl(vo.unit_cost,0),(((iscl.negotiated_item_cost*pi.pack_qty)/total_pack.tot_comp_cost)*vo.unit_cost))/pi.pack_qty,4) po_unit_cost,
                                   round(DECODE(ffdd.pack_no,null,nvl(vo.unit_cost,0) *  nvl(frd.quantity,0), ((((iscl.negotiated_item_cost*pi.pack_qty)/total_pack.tot_comp_cost)*vo.unit_cost)/pi.pack_qty)*frd.quantity*pi.pack_qty),4) po_total_cost,
                                   nvl(ffdd.freight_cost * pi.pack_qty,0) freight_cost,
                                   nvl(ffdd.other_expenses_cost * pi.pack_qty,0) other_expenses_cost,
                                   nvl(ffdd.insurance_cost * pi.pack_qty,0) insurance_cost,
                                   ffdd.pack_no,
                                   ffdd.pack_ind,
                                   pi.pack_qty pack_qty
                              from FM_RECEIVING_DETAIL frd,
                                   FM_FISCAL_DOC_DETAIL ffdd,
                                   ORDLOC vo,
                                   ORDHEAD oh,
                                   FM_FISCAL_DOC_HEADER ffdh,
                                   FM_RECEIVING_HEADER frh,
                                   PACKITEM pi ,
                                   ITEM_SUPP_COUNTRY_LOC iscl,
                                   (select pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc, round(sum(pi1.pack_qty * iscl1.negotiated_item_cost),4) tot_comp_cost
                                      from packitem pi1, item_supp_country_loc iscl1
                                     where pi1.item                    = iscl1.item
                                     group by pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc) total_pack
                              where (frd.item = ffdd.item or frd.item = ffdd.pack_no)
                                and frd.requisition_no = ffdd.requisition_no
                                and vo.order_no = ffdd.requisition_no
                                and (vo.item = ffdd.pack_no)
                                and ffdd.pack_no = pi.pack_no (+)
                                and ffdd.item = pi.item(+)
                                and iscl.item = pi.item
                                and total_pack.pack_no = pi.pack_no
                                and oh.order_no                  = vo.order_no
                                and oh.supplier                  = iscl.supplier
                                and oh.supplier                  = total_pack.supplier
                                and frh.fiscal_doc_id = ffdh.fiscal_doc_id
                                and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
                                and frh.seq_no=frd.header_seq_no
                                and ffdh.location_id = DECODE(ffdh.location_type, L_store, vo.location, L_wh,(select physical_wh from WH where wh.wh=vo.location))
                                and frd.reason_code is NULL
                                and nvl(ffdd.unexpected_item,L_no) = L_no
                                and ffdh.fiscal_doc_id = I_fiscal_doc_id
                                and frd.quantity > 0
                                and (I_pack_no is NOT NULL and ffdd.pack_no = I_pack_no)
                                and iscl.loc_type = vo.loc_type
                                and iscl.loc = vo.location
                                and ffdd.pack_no is not NULL
                                and frd.item = vo.item
                                and iscl.loc in ((select wh loc
                                 from wh
                                where physical_wh = ffdd.location_id)
                              union
                              (select store loc
                                 from store
                                where store = ffdd.location_id))
                                  and total_pack.loc_type = vo.loc_type
                                  and total_pack.loc = vo.location
                                  and total_pack.loc in ((select wh loc
                                                            from wh
                                                           where physical_wh = ffdd.location_id)
                                                         union
                                                         (select store loc
                                                            from store
                                                           where store = ffdd.location_id))
                                                           ;
   end if;
   ---
   loop
      fetch L_ref_cursor bulk collect into L_RCV_DETAIL limit 100;
         for i in 1..L_rcv_detail.count
         loop
            ---
            if L_rcv_detail(i).pack = L_no then
               L_pack_ebc_cost := 0;
               L_pack_bc_cost  := 0;
               L_pack_nic_cost := 0;
            end if;
            ---
            L_total_tax_amount := 0;
            L_total_rec_val    := 0;
            L_unit_tax_value   := 0;
            L_mtr_called       := FALSE;
            L_line_tax_obj_tbl := L_line_tax_obj_tbl_null;
            L_non_match_unit_tax_val := 0;
            L_non_match_unit_rec_val := 0;
            ---
            L_correction_doc.fiscal_doc_id      := L_rcv_detail(i).fiscal_doc_id;
            L_correction_doc.fiscal_doc_line_id := L_rcv_detail(i).fiscal_doc_line_id;
            ---
            if L_rcv_detail(i).cost_discrep_status = L_resolved then
               if FM_RECEIVING_SQL.GET_COST_RESOLUTION_TYPE(L_error_message,
                                                            L_rcv_detail(i).fiscal_doc_line_id,
                                                            L_cost_discrep_type,
                                                            L_cost_resolution) = FALSE then
                  O_error_message := L_error_message;
                  return FALSE;
               end if;
               ---
               if L_cost_resolution = L_res_nf then
                  L_nic_cost := L_rcv_detail(i).nf_unit_cost_with_disc;
               elsif L_cost_resolution = L_res_sys then
                  L_nic_cost := L_rcv_detail(i).po_unit_cost;
                  ---
                  L_correction_doc.action_type        := L_action_clc;
                  L_correction_doc.action_value       := L_rcv_detail(i).po_unit_cost;
                  ---
                  if FM_CORRECTION_DOC_SQL.GEN_CORRECT_DOC(L_error_message,
                                                           L_correction_doc,
                                                           null,
                                                           null,
                                                           null,
                                                           null,
                                                           null) = FALSE then
                     O_error_message := L_error_message;
                     return FALSE;
                  end if;
                  ---
               end if;
            elsif L_rcv_detail(i).cost_discrep_status = L_discrepant then
               L_nic_cost := L_rcv_detail(i).po_unit_cost;
            else
               L_nic_cost := L_rcv_detail(i).nf_unit_cost_with_disc;
            end if;
            ---
            L_old_nic_cost := L_rcv_detail(i).nic_cost;
            L_old_ebc_cost := L_rcv_detail(i).ebc_cost;
            ---
            O_old_nic := nvl(L_rcv_detail(i).nic_cost,0);
            O_old_ebc := nvl(L_rcv_detail(i).ebc_cost,0);
            ---
            if (L_rcv_detail(i).pack_no is not NULL) then
               L_nic_cost := L_nic_cost * L_rcv_detail(i).pack_qty;
            end if;
            ---
            L_pack_nic_cost := L_pack_nic_cost + L_nic_cost;
            ---
            open C_GET_TRNG_IND(L_rcv_detail(i).requisition_no);
            fetch C_GET_TRNG_IND into L_trng_ind;
            close C_GET_TRNG_IND;
            ---
            if L_trng_ind = L_yes then
               open C_GET_TRNG_LINE_ID(L_rcv_detail(i).requisition_no, L_rcv_detail(i).fiscal_doc_id, L_rcv_detail(i).nf_item);
               fetch C_GET_TRNG_LINE_ID into L_get_trng_line_id;
               close C_GET_TRNG_LINE_ID;
            end if;
             ---
            for L_nf_line_taxes in C_NF_LINE_TAXES(L_rcv_detail(i).fiscal_doc_line_id, L_get_trng_line_id.fiscal_doc_line_id) loop
               L_sys_line_taxes := L_sys_line_taxes_null;
               L_sys_tax_value := 0;
               L_sys_tax_rec_val := 0;
               ---
               open C_SYS_LINE_TAXES(L_nf_line_taxes.fiscal_doc_line_id, L_nf_line_taxes.vat_code);
               fetch C_SYS_LINE_TAXES into L_sys_line_taxes;
               ---
               if C_SYS_LINE_TAXES%ROWCOUNT = 0 then
                  null;
               end if;
               ---
               close C_SYS_LINE_TAXES;
               ---
               if FM_RECEIVING_SQL.GET_TAX_RESOLUTION_TYPE(L_error_message,
                                                           L_nf_line_taxes.fiscal_doc_line_id,
                                                           L_tax_discrep_type,
                                                           L_nf_line_taxes.vat_code,
                                                           L_tax_resolution,
                                                           L_tax_disc_exists) = FALSE then
                  O_error_message := L_error_message;
                  return FALSE;
               end if;
               ---
               L_nic_ind := NULL;
               if FM_RECEIVING_SQL.GET_NIC_IND(L_error_message,
                                               L_sys_line_taxes.tax_code,
                                               L_nic_ind) = FALSE then
                  O_error_message := L_error_message;
                  return FALSE;
               end if;
               ---
               if ((L_rcv_detail(i).nf_unit_cost <= L_rcv_detail(i).po_unit_cost and L_rcv_detail(i).cost_discrep_status = L_matched) or
                  (L_rcv_detail(i).nf_unit_cost > L_rcv_detail(i).po_unit_cost and L_rcv_detail(i).cost_discrep_status = L_matched) or
                  (L_rcv_detail(i).nf_unit_cost > L_rcv_detail(i).po_unit_cost and L_rcv_detail(i).cost_discrep_status = L_resolved and L_cost_resolution = L_res_nf)) then
                  ---
                  if NOT L_tax_disc_exists then
                     if L_rcv_detail(i).rcvd_qty = L_rcv_detail(i).nf_qty then
                        ---
                        if L_nf_line_taxes.vat_code is not null then
                           L_tax_wac.extend;
                           L_tax_tab_cnt := L_tax_tab_cnt + 1;
                           L_tax_wac(L_tax_tab_cnt).tax_code           := L_nf_line_taxes.vat_code;
                           L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id := L_nf_line_taxes.fiscal_doc_line_id;
                           L_tax_wac(L_tax_tab_cnt).tax_rate           := nvl(L_nf_line_taxes.nf_tax_rate,0);
                           L_tax_wac(L_tax_tab_cnt).unit_tax_value     := nvl(L_nf_line_taxes.unit_tax_amt,0);
                           L_tax_wac(L_tax_tab_cnt).tax_basis          := nvl(L_nf_line_taxes.nf_base_value,0);
                           L_tax_wac(L_tax_tab_cnt).modified_tax_basis := nvl(L_nf_line_taxes.nf_modified_base_value,0);
                           L_tax_wac(L_tax_tab_cnt).rcvd_qty           := nvl(L_rcv_detail(i).rcvd_qty,0);
                           L_tax_wac(L_tax_tab_cnt).unit_rec_value     := nvl(L_nf_line_taxes.unit_rec_val,0);
                           ---
                           L_total_tax_amount := nvl(L_total_tax_amount,0)+ L_tax_wac(L_tax_tab_cnt).unit_tax_value*L_rcv_detail(i).pack_qty;
                           L_total_rec_val    := nvl(L_total_rec_val,0)+  L_tax_wac(L_tax_tab_cnt).unit_rec_value*L_rcv_detail(i).pack_qty;
                        end if;
                        ---
                     elsif L_rcv_detail(i).rcvd_qty <> L_rcv_detail(i).nf_qty then
                        if L_mtr_called then
                           if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                                  L_error_message,
                                                                  L_status,
                                                                  L_nf_line_taxes.fiscal_doc_id,
                                                                  L_nf_line_taxes.fiscal_doc_line_id,
                                                                  L_nf_line_taxes.vat_code,
                                                                  L_sys_tax_value,
                                                                  L_sys_tax_base,
                                                                  L_sys_modified_tax_base,
                                                                  L_sys_tax_rate,
                                                                  L_sys_tax_rec_val) = FALSE then
                              O_error_message := L_error_message;
                              return FALSE;
                           end if;
                        else
                           FM_EXT_TAXES_SQL.GP_tax_ind := 0;
                           FM_EXT_TAXES_SQL.GP_fetch_comp_taxes := FALSE;
                           if FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(L_line_tax_obj_tbl,
                                                                  L_error_message,
                                                                  L_status,
                                                                  L_nf_line_taxes.fiscal_doc_id,
                                                                  L_nf_line_taxes.fiscal_doc_line_id,
                                                                  L_rcv_detail(i).rcvd_qty,
                                                                  L_rcv_detail(i).nf_unit_cost) = FALSE then
                              O_error_message := L_error_message;
                              return FALSE;
                           else
                              L_mtr_called := TRUE;
                              if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                                     L_error_message,
                                                                     L_status,
                                                                     L_nf_line_taxes.fiscal_doc_id,
                                                                     L_nf_line_taxes.fiscal_doc_line_id,
                                                                     L_nf_line_taxes.vat_code,
                                                                     L_sys_tax_value,
                                                                     L_sys_tax_base,
                                                                     L_sys_modified_tax_base,
                                                                     L_sys_tax_rate,
                                                                     L_sys_tax_rec_val) = FALSE then
                                 O_error_message := L_error_message;
                                 return FALSE;
                              end if;
                           end if;
                        end if;
                        ---
                        if nvl(L_sys_tax_value,0) > nvl(L_nf_line_taxes.nf_tax_value,0) then
                           ---
                           if L_nf_line_taxes.vat_code is not null then
                              L_tax_wac.extend;
                              L_tax_tab_cnt := L_tax_tab_cnt + 1;
                              L_tax_wac(L_tax_tab_cnt).tax_code             := L_nf_line_taxes.vat_code;
                              L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                              L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_nf_line_taxes.nf_tax_rate,0);
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_nf_line_taxes.unit_tax_amt,0);
                              L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_nf_line_taxes.nf_base_value,0);
                              L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_nf_line_taxes.nf_modified_base_value,0);
                              L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_nf_line_taxes.unit_rec_val,0);
                              ---
                              l_total_tax_amount := nvl(l_total_tax_amount,0)+ l_tax_wac(L_tax_tab_cnt).unit_tax_value*L_rcv_detail(i).pack_qty;
                              L_total_rec_val    := nvl(L_total_rec_val,0)+  L_tax_wac(L_tax_tab_cnt).unit_rec_value*L_rcv_detail(i).pack_qty;
                           end if;
                           ---
                        elsif nvl(L_sys_tax_value,0) <= nvl(L_nf_line_taxes.nf_tax_value,0) then
                           ---
                           if L_sys_line_taxes.tax_code is not null then
                              L_tax_wac.extend;
                              L_tax_tab_cnt := L_tax_tab_cnt + 1;
                              L_tax_wac(L_tax_tab_cnt).tax_code             := L_sys_line_taxes.tax_code;
                              L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                              L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_sys_tax_rate,0);
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_sys_tax_value,0)/L_rcv_detail(i).rcvd_qty;
                              L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_sys_tax_base,0);
                              L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_sys_modified_tax_base,0);
                              L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_sys_tax_rec_val,0)/L_rcv_detail(i).rcvd_qty;
                              ---
                              L_total_tax_amount := nvl(L_total_tax_amount,0)+ L_tax_wac(L_tax_tab_cnt).unit_tax_value*L_rcv_detail(i).pack_qty;
                              L_total_rec_val    := nvl(L_total_rec_val,0)+ L_tax_wac(L_tax_tab_cnt).unit_rec_value*L_rcv_detail(i).pack_qty;
                           end if;
                        end if;
                        ---
                     end if;
                  elsif L_tax_disc_exists then
                     if L_tax_resolution = L_res_nf then
                        ---
                        if L_nf_line_taxes.vat_code is not null then
                           L_tax_wac.extend;
                           L_tax_tab_cnt := L_tax_tab_cnt + 1;
                           L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                           L_tax_wac(L_tax_tab_cnt).tax_code             := L_nf_line_taxes.vat_code;
                           L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_nf_line_taxes.nf_tax_rate,0);
                           L_tax_wac(L_tax_tab_cnt).tax_basis            := (nvl(L_nf_line_taxes.nf_base_value,0) / nvl(L_rcv_detail(i).nf_total_cost,0)) * (nvl(L_rcv_detail(i).rcvd_qty,0) * L_rcv_detail(i).nf_unit_cost);
                           L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := (nvl(L_nf_line_taxes.nf_modified_base_value,0) / nvl(L_rcv_detail(i).nf_total_cost,0)) * (nvl(L_rcv_detail(i).rcvd_qty,0) * L_rcv_detail(i).nf_unit_cost);
                           L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                           ---
                           if L_nf_line_taxes.tax_source = L_res_nf then
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value    := ((nvl(L_nf_line_taxes.nf_tax_value,0) / nvl(l_rcv_detail(i).nf_total_cost,0)) * (nvl(L_rcv_detail(i).rcvd_qty,0) * L_rcv_detail(i).nf_unit_cost)) / nvl(L_rcv_detail(i).rcvd_qty,0);
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value    := ((nvl(L_nf_line_taxes.rec_value,0) / nvl(l_rcv_detail(i).nf_total_cost,0)) * (nvl(L_rcv_detail(i).rcvd_qty,0) * L_rcv_detail(i).nf_unit_cost)) / nvl(L_rcv_detail(i).rcvd_qty,0);
                           else
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value := 0;
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value := 0;
                           end if;
                           ---
                           L_total_tax_amount := nvl(L_total_tax_amount,0)+  L_tax_wac(L_tax_tab_cnt).unit_tax_value*L_rcv_detail(i).pack_qty ;
                           L_total_rec_val    := nvl(L_total_rec_val,0)+ L_tax_wac(L_tax_tab_cnt).unit_rec_value*L_rcv_detail(i).pack_qty ;
                        end if;
                        ---
                     elsif (L_tax_resolution = NULL) or (L_tax_resolution = L_res_sys) then
                        if L_mtr_called then
                           if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                                  L_error_message,
                                                                  L_status,
                                                                  L_nf_line_taxes.fiscal_doc_id,
                                                                  L_nf_line_taxes.fiscal_doc_line_id,
                                                                  L_nf_line_taxes.vat_code,
                                                                  L_sys_tax_value,
                                                                  L_sys_tax_base,
                                                                  L_sys_modified_tax_base,
                                                                  L_sys_tax_rate,
                                                                  L_sys_tax_rec_val) = FALSE then
                              O_error_message := L_error_message;
                              return FALSE;
                           end if;
                        else
                           FM_EXT_TAXES_SQL.GP_tax_ind := 0;
                           FM_EXT_TAXES_SQL.GP_fetch_comp_taxes := FALSE;
                           if FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(L_line_tax_obj_tbl,
                                                                  L_error_message,
                                                                  L_status,
                                                                  L_nf_line_taxes.fiscal_doc_id,
                                                                  L_nf_line_taxes.fiscal_doc_line_id,
                                                                  L_rcv_detail(i).rcvd_qty,
                                                                  L_rcv_detail(i).nf_unit_cost) = FALSE then
                              O_error_message := L_error_message;
                              return FALSE;
                           else
                              L_mtr_called := TRUE;
                              if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                                     L_error_message,
                                                                     L_status,
                                                                     L_nf_line_taxes.fiscal_doc_id,
                                                                     L_nf_line_taxes.fiscal_doc_line_id,
                                                                     L_nf_line_taxes.vat_code,
                                                                     L_sys_tax_value,
                                                                     L_sys_tax_base,
                                                                     L_sys_modified_tax_base,
                                                                     L_sys_tax_rate,
                                                                     L_sys_tax_rec_val) = FALSE then
                                 O_error_message := L_error_message;
                                 return FALSE;
                              end if;
                           end if;
                        end if;
                        ---
                        if nvl(L_sys_tax_value,0) > nvl(L_nf_line_taxes.nf_tax_value,0) then
                           ---
                           if L_nf_line_taxes.vat_code is not null then
                              L_tax_wac.extend;
                              L_tax_tab_cnt := L_tax_tab_cnt + 1;
                              L_tax_wac(L_tax_tab_cnt).tax_code             := L_nf_line_taxes.vat_code;
                              L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                              L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_nf_line_taxes.nf_tax_rate,0);
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_nf_line_taxes.unit_tax_amt,0);
                              L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_nf_line_taxes.nf_base_value,0);
                              L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_nf_line_taxes.nf_modified_base_value,0);
                              L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_nf_line_taxes.unit_rec_val,0);
                              ---
                              L_total_tax_amount := nvl(L_total_tax_amount,0)+  L_tax_wac(L_tax_tab_cnt).unit_tax_value*L_rcv_detail(i).pack_qty ;
                              L_total_rec_val    := nvl(L_total_rec_val,0)+ L_tax_wac(L_tax_tab_cnt).unit_rec_value*L_rcv_detail(i).pack_qty;
                           end if;
                          ---
                        elsif nvl(L_sys_tax_value,0) <= nvl(L_nf_line_taxes.nf_tax_value,0) then
                           ---
                           if L_sys_line_taxes.tax_code is not null then
                              L_tax_wac.extend;
                              L_tax_tab_cnt := L_tax_tab_cnt + 1;
                              L_tax_wac(L_tax_tab_cnt).tax_code             := L_sys_line_taxes.tax_code;
                              L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                              L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_sys_tax_rate,0);
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_sys_tax_value,0)/L_rcv_detail(i).rcvd_qty;
                              L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_sys_tax_base,0);
                              L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_sys_modified_tax_base,0);
                              L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_sys_tax_rec_val,0)/L_rcv_detail(i).rcvd_qty;
                              ---
                              L_total_tax_amount := nvl(L_total_tax_amount,0)+ L_tax_wac(L_tax_tab_cnt).unit_tax_value*L_rcv_detail(i).pack_qty;
                              L_total_rec_val    := nvl(L_total_rec_val,0)+ L_tax_wac(L_tax_tab_cnt).unit_rec_value*L_rcv_detail(i).pack_qty;
                           end if;
                           ---
                        end if;
                     end if;
                  end if;
                  ---
                  if L_nic_ind = L_nic_ind_flg then
                     L_unit_tax_value := nvl(L_unit_tax_value,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                  end if;
                  ---
               elsif ((L_rcv_detail(i).nf_unit_cost > L_rcv_detail(i).po_unit_cost and L_rcv_detail(i).cost_discrep_status = L_discrepant) or
                     (L_rcv_detail(i).nf_unit_cost > L_rcv_detail(i).po_unit_cost and L_cost_resolution = L_res_sys)) then
                  ---
                  if (L_rcv_detail(i).cost_discrep_status = L_discrepant) or (L_cost_resolution = L_res_sys ) then
                     if NOT L_tax_disc_exists then
                        if L_mtr_called then
                           if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                                  L_error_message,
                                                                  L_status,
                                                                  L_nf_line_taxes.fiscal_doc_id,
                                                                  L_nf_line_taxes.fiscal_doc_line_id,
                                                                  L_nf_line_taxes.vat_code,
                                                                  L_sys_tax_value,
                                                                  L_sys_tax_base,
                                                                  L_sys_modified_tax_base,
                                                                  L_sys_tax_rate,
                                                                  L_sys_tax_rec_val) = FALSE then
                              O_error_message := L_error_message;
                              return FALSE;
                           end if;
                        else
                           FM_EXT_TAXES_SQL.GP_tax_ind := 0;
                           FM_EXT_TAXES_SQL.GP_fetch_comp_taxes := FALSE;
                           if FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(L_line_tax_obj_tbl,
                                                                  L_error_message,
                                                                  L_status,
                                                                  L_nf_line_taxes.fiscal_doc_id,
                                                                  L_nf_line_taxes.fiscal_doc_line_id,
                                                                  L_rcv_detail(i).rcvd_qty,
                                                                  L_rcv_detail(i).po_unit_cost) = FALSE then
                              O_error_message := L_error_message;
                              return FALSE;
                           else
                              L_mtr_called := TRUE;
                              if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                                     L_error_message,
                                                                     L_status,
                                                                     L_nf_line_taxes.fiscal_doc_id,
                                                                     L_nf_line_taxes.fiscal_doc_line_id,
                                                                     L_nf_line_taxes.vat_code,
                                                                     L_sys_tax_value,
                                                                     L_sys_tax_base,
                                                                     L_sys_modified_tax_base,
                                                                     L_sys_tax_rate,
                                                                     L_sys_tax_rec_val) = FALSE then
                                 O_error_message := L_error_message;
                                 return FALSE;
                              end if;
                           end if;
                        end if;
                        ---
                        if nvl(L_sys_tax_value,0) > nvl(L_nf_line_taxes.nf_tax_value,0) then
                           if L_nf_line_taxes.vat_code is not null then
                              L_tax_wac.extend;
                              L_tax_tab_cnt := L_tax_tab_cnt + 1;
                              L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                              L_tax_wac(L_tax_tab_cnt).tax_code             := L_nf_line_taxes.vat_code;
                              L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_sys_tax_rate,0);
                              L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_sys_tax_base,0);
                              L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_sys_modified_tax_base,0);
                              L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_nf_line_taxes.unit_tax_amt,0);
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(l_nf_line_taxes.unit_rec_val,0);
                              ---
                              L_total_tax_amount := nvl(L_total_tax_amount,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                              L_total_rec_val    := nvl(L_total_rec_val,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_rec_value,0)*L_rcv_detail(i).pack_qty;
                           end if;
                           ---
                        elsif nvl(L_sys_tax_value,0) <= nvl(L_nf_line_taxes.nf_tax_value,0) then
                           if L_nf_line_taxes.vat_code is not null then
                              L_tax_wac.extend;
                              L_tax_tab_cnt := L_tax_tab_cnt + 1;
                              L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                              L_tax_wac(L_tax_tab_cnt).tax_code             := L_nf_line_taxes.vat_code;
                              L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_sys_tax_rate,0);
                              L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_sys_tax_base,0);
                              L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_sys_modified_tax_base,0);
                              L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value    := L_sys_tax_value/L_rcv_detail(i).rcvd_qty;
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value    := nvl(L_sys_tax_rec_val,0)/L_rcv_detail(i).rcvd_qty;
                              ---
                              L_total_tax_amount := nvl(L_total_tax_amount,0)+ L_tax_wac(L_tax_tab_cnt).unit_tax_value*L_rcv_detail(i).pack_qty;
                              L_total_rec_val    := nvl(L_total_rec_val,0)+ L_tax_wac(L_tax_tab_cnt).unit_rec_value*L_rcv_detail(i).pack_qty;
                           end if;
                        end if;
                        ---
                        if L_nic_ind = L_nic_ind_flg then
                           L_unit_tax_value := nvl(L_unit_tax_value,0)+nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                        end if;
                        ---
                     elsif L_tax_disc_exists then
                        if ( (L_tax_resolution = NULL ) or (L_tax_resolution = L_res_sys )) then
                           if L_mtr_called then
                              if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                                     L_error_message,
                                                                     L_status,
                                                                     L_nf_line_taxes.fiscal_doc_id,
                                                                     L_nf_line_taxes.fiscal_doc_line_id,
                                                                     L_nf_line_taxes.vat_code,
                                                                     L_sys_tax_value,
                                                                     L_sys_tax_base,
                                                                     L_sys_modified_tax_base,
                                                                     L_sys_tax_rate,
                                                                     L_sys_tax_rec_val) = FALSE THEN
                                 O_error_message := L_error_message;
                                 return FALSE;
                              end if;
                           else
                              FM_EXT_TAXES_SQL.GP_tax_ind := 0;
                              FM_EXT_TAXES_SQL.GP_fetch_comp_taxes := FALSE;
                              if FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(L_line_tax_obj_tbl,
                                                                     L_error_message,
                                                                     L_status,
                                                                     L_nf_line_taxes.fiscal_doc_id,
                                                                     L_nf_line_taxes.fiscal_doc_line_id,
                                                                     L_rcv_detail(i).rcvd_qty,
                                                                     L_rcv_detail(i).po_unit_cost) = FALSE then
                                 O_error_message := L_error_message;
                                 return FALSE;
                              else
                                 L_mtr_called := TRUE;
                                 if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                                        L_error_message,
                                                                        L_status,
                                                                        L_nf_line_taxes.fiscal_doc_id,
                                                                        L_nf_line_taxes.fiscal_doc_line_id,
                                                                        L_nf_line_taxes.vat_code,
                                                                        L_sys_tax_value,
                                                                        L_sys_tax_base,
                                                                        L_sys_modified_tax_base,
                                                                        L_sys_tax_rate,
                                                                        L_sys_tax_rec_val) = FALSE then
                                    O_error_message := L_error_message;
                                    return FALSE;
                                 end if;
                              end if;
                           end if;
                           ---
                           if nvl(L_sys_tax_value,0) > nvl(L_nf_line_taxes.nf_tax_value,0) then
                              if L_nf_line_taxes.vat_code is not null then
                                 L_tax_wac.extend;
                                 L_tax_tab_cnt := L_tax_tab_cnt + 1;
                                 L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                                 L_tax_wac(L_tax_tab_cnt).tax_code             := L_nf_line_taxes.vat_code;
                                 L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_nf_line_taxes.unit_tax_amt,0);
                                 L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_nf_line_taxes.nf_tax_rate,0);
                                 L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_nf_line_taxes.nf_base_value,0);
                                 L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_nf_line_taxes.nf_modified_base_value,0);
                                 L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                                 L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_nf_line_taxes.unit_rec_val,0);
                                 ---
                                 L_total_tax_amount := nvl(L_total_tax_amount,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                                 L_total_rec_val    := nvl(L_total_rec_val,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_rec_value,0)*L_rcv_detail(i).pack_qty;
                              end if;
                              ---
                           elsif L_sys_tax_value <= nvl(L_nf_line_taxes.nf_tax_value,0) then
                              if L_sys_line_taxes.tax_code is not null then
                                 L_tax_wac.extend;
                                 L_tax_tab_cnt := L_tax_tab_cnt + 1;
                                 L_tax_wac(L_tax_tab_cnt).tax_code             := L_sys_line_taxes.tax_code;
                                 L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_nf_line_taxes.fiscal_doc_line_id;
                                 L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_sys_tax_rate,0);
                                 L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_sys_tax_value,0)/L_rcv_detail(i).rcvd_qty;
                                 L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_sys_tax_base,0);
                                 L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_sys_modified_tax_base,0);
                                 L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                                 L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_sys_tax_rec_val,0)/L_rcv_detail(i).rcvd_qty;
                                 ---
                                 L_total_tax_amount := nvl(L_total_tax_amount,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                                 L_total_rec_val    := nvl(L_total_rec_val,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_rec_value,0)*L_rcv_detail(i).pack_qty;
                              end if;
                              ---
                           end if;
                           ---
                        elsif (L_tax_resolution = L_res_nf )then
                           ---
                           if L_nf_line_taxes.vat_code is not null then
                              L_tax_wac.extend;
                              L_tax_tab_cnt := L_tax_tab_cnt + 1;
                              L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   :=  L_nf_line_taxes.fiscal_doc_line_id;
                              L_tax_wac(L_tax_tab_cnt).tax_code             :=  L_nf_line_taxes.vat_code;
                              L_tax_wac(L_tax_tab_cnt).unit_tax_value       :=  ((nvl(L_nf_line_taxes.nf_tax_value,0) / nvl(L_rcv_detail(i).nf_total_cost,1)) * (nvl(L_rcv_detail(i).rcvd_qty,1) * L_rcv_detail(i).po_unit_cost)) / nvl(L_rcv_detail(i).rcvd_qty,1);
                              L_tax_wac(L_tax_tab_cnt).tax_rate             :=  nvl(L_nf_line_taxes.nf_tax_rate,0);
                              L_tax_wac(L_tax_tab_cnt).tax_basis            :=  (nvl(L_nf_line_taxes.nf_base_value,0) / nvl(L_rcv_detail(i).nf_total_cost,1)) * (nvl(L_rcv_detail(i).rcvd_qty,1) * L_rcv_detail(i).po_unit_cost);
                              L_tax_wac(L_tax_tab_cnt).modified_tax_basis   :=  (nvl(L_nf_line_taxes.nf_modified_base_value,0) / nvl(L_rcv_detail(i).nf_total_cost,1)) * (nvl(L_rcv_detail(i).rcvd_qty,1) * L_rcv_detail(i).po_unit_cost);
                              L_tax_wac(L_tax_tab_cnt).rcvd_qty             :=  nvl(L_rcv_detail(i).rcvd_qty,0);
                              L_tax_wac(L_tax_tab_cnt).unit_rec_value       :=  ((nvl(L_nf_line_taxes.rec_value,0) / nvl(L_rcv_detail(i).nf_total_cost,1)) * (nvl(L_rcv_detail(i).rcvd_qty,1) * L_rcv_detail(i).po_unit_cost)) / nvl(L_rcv_detail(i).rcvd_qty,1);
                              ---
                              L_total_tax_amount := nvl(L_total_tax_amount,0)+  L_tax_wac(L_tax_tab_cnt).unit_tax_value*L_rcv_detail(i).pack_qty ;
                              L_total_rec_val    := nvl(L_total_rec_val,0)+ L_tax_wac(L_tax_tab_cnt).unit_rec_value*L_rcv_detail(i).pack_qty;
                           end if;
                           ---
                        end if;
                        ---
                        if L_nic_ind = L_nic_ind_flg then
                           L_unit_tax_value := nvl(L_unit_tax_value,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                        end if;
                        ---
                     end if;
                  end if;
               end if;
            end loop;
            ---
            if (L_rcv_detail(i).tax_det_discrep = 'M'
            and L_rcv_detail(i).cost_discrep_status = 'M'
            and L_rcv_detail(i).qty_discrep_status = 'M'
            and L_rcv_detail(i).nf_qty = L_rcv_detail(i).rcvd_qty) then
               for L_non_match_tax in C_SYS_TAXES_NON_MATCH_IND(L_rcv_detail(i).fiscal_doc_line_id) loop
                  ---
                  L_non_match_unit_tax_val := nvl(L_non_match_unit_tax_val,0) + (nvl(L_non_match_tax.tax_value,0)/L_rcv_detail(i).rcvd_qty)*L_rcv_detail(i).pack_qty;
                  L_non_match_unit_rec_val := nvl(L_non_match_unit_rec_val,0) + (nvl(L_non_match_tax.tax_rec_value,0)/L_rcv_detail(i).rcvd_qty)*L_rcv_detail(i).pack_qty;
                  ---
                  if L_non_match_tax.tax_value is NOT NULL then
                     if L_non_match_tax.tax_code is not null then
                        L_tax_wac.extend;
                        L_tax_tab_cnt := L_tax_tab_cnt + 1;
                        L_tax_wac(L_tax_tab_cnt).tax_code             := L_non_match_tax.tax_code;
                        L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_rcv_detail(i).fiscal_doc_line_id;
                        L_tax_wac(L_tax_tab_cnt).tax_rate             := L_non_match_tax.tax_rate;
                        L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_non_match_tax.unit_tax_value,0);
                        L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_non_match_tax.tax_basis,0);
                        L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_non_match_tax.modified_tax_basis,0);
                        L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                        L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_non_match_tax.unit_rec_value,0);
                     end if;
                  end if;
                  ---
                  if FM_RECEIVING_SQL.GET_NIC_IND(L_error_message,
                                                  L_non_match_tax.tax_code,
                                                  L_nic_ind) = FALSE then
                     O_error_message := L_error_message;
                     return FALSE;
                  end if;
                  ---
                  if L_nic_ind = L_nic_ind_flg then
                     L_unit_tax_value := nvl(L_unit_tax_value,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                  end if;
                  ---
               end loop;

               if (L_trng_ind = L_yes) then
                  for L_non_match_tax in C_SYS_TAXES_NON_MATCH_IND(L_get_trng_line_id.fiscal_doc_line_id) loop
                     L_non_match_unit_tax_val := nvl(L_non_match_unit_tax_val,0) + (nvl(L_non_match_tax.tax_value,0)/L_rcv_detail(i).rcvd_qty)*L_rcv_detail(i).pack_qty;
                     L_non_match_unit_rec_val := nvl(L_non_match_unit_rec_val,0) + (nvl(L_non_match_tax.tax_rec_value,0)/L_rcv_detail(i).rcvd_qty)*L_rcv_detail(i).pack_qty;
                     ---
                     if L_non_match_tax.tax_value is NOT NULL then
                        if L_non_match_tax.tax_code is NOT NULL then
                           L_tax_wac.extend;
                           L_tax_tab_cnt := L_tax_tab_cnt + 1;
                           L_tax_wac(L_tax_tab_cnt).tax_code             := L_non_match_tax.tax_code;
                           L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_rcv_detail(i).fiscal_doc_line_id;
                           L_tax_wac(L_tax_tab_cnt).tax_rate             := L_non_match_tax.tax_rate;
                           L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_non_match_tax.unit_tax_value,0);
                           L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_non_match_tax.tax_basis,0);
                           L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_non_match_tax.modified_tax_basis,0);
                           L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                           L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_non_match_tax.unit_rec_value,0);
                        end if;
                     end if;
                     ---
                     if FM_RECEIVING_SQL.GET_NIC_IND(L_error_message,
                                                     L_non_match_tax.tax_code,
                                                     L_nic_ind) = FALSE then
                        O_error_message := L_error_message;
                        return FALSE;
                     end if;
                     ---
                     if L_nic_ind = L_nic_ind_flg then
                        L_unit_tax_value := nvl(L_unit_tax_value,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                     end if;
                  end loop;
               end if;
            else
               if not L_mtr_called then
                  FM_EXT_TAXES_SQL.GP_tax_ind := 0;
                  FM_EXT_TAXES_SQL.GP_fetch_comp_taxes := FALSE;
                  if FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(L_line_tax_obj_tbl,
                                                         L_error_message,
                                                         L_status,
                                                         L_rcv_detail(i).fiscal_doc_id,
                                                         L_rcv_detail(i).fiscal_doc_line_id,
                                                         L_rcv_detail(i).rcvd_qty,
                                                         L_nic_cost/L_rcv_detail(i).pack_qty) = FALSE then
                     O_error_message := L_error_message;
                     return FALSE;
                  else
                     L_mtr_called := TRUE;
                  end if;
               end if;
               ---
               if L_mtr_called then
                  for L_non_match_tax in C_NON_MATCH_TAX loop
                     ---
                     if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                            L_error_message,
                                                            L_status,
                                                            L_rcv_detail(i).fiscal_doc_id,
                                                            L_rcv_detail(i).fiscal_doc_line_id,
                                                            L_non_match_tax.tax_code,
                                                            L_sys_tax_value,
                                                            L_sys_tax_base,
                                                            L_sys_modified_tax_base,
                                                            L_sys_tax_rate,
                                                            L_sys_tax_rec_val) = FALSE then
                        O_error_message := L_error_message;
                        return FALSE;
                     else
                        L_non_match_unit_tax_val := nvl(L_non_match_unit_tax_val,0) + (nvl(L_sys_tax_value,0)/L_rcv_detail(i).rcvd_qty)*L_rcv_detail(i).pack_qty;
                        L_non_match_unit_rec_val := nvl(L_non_match_unit_rec_val,0) + (nvl(L_sys_tax_rec_val,0)/L_rcv_detail(i).rcvd_qty)*L_rcv_detail(i).pack_qty;
                     end if;
                     ---
                     if L_sys_tax_value is NOT NULL then
                        if L_non_match_tax.tax_code is NOT NULL then
                           L_tax_wac.extend;
                           L_tax_tab_cnt := L_tax_tab_cnt + 1;
                           L_tax_wac(L_tax_tab_cnt).tax_code             := L_non_match_tax.tax_code;
                           L_tax_wac(L_tax_tab_cnt).fiscal_doc_line_id   := L_rcv_detail(i).fiscal_doc_line_id;
                           L_tax_wac(L_tax_tab_cnt).tax_rate             := nvl(L_sys_tax_rate,0);
                           L_tax_wac(L_tax_tab_cnt).unit_tax_value       := nvl(L_sys_tax_value,0)/L_rcv_detail(i).rcvd_qty;
                           L_tax_wac(L_tax_tab_cnt).tax_basis            := nvl(L_sys_tax_base,0);
                           L_tax_wac(L_tax_tab_cnt).modified_tax_basis   := nvl(L_sys_modified_tax_base,0);
                           L_tax_wac(L_tax_tab_cnt).rcvd_qty             := nvl(L_rcv_detail(i).rcvd_qty,0);
                           L_tax_wac(L_tax_tab_cnt).unit_rec_value       := nvl(L_sys_tax_rec_val,0)/L_rcv_detail(i).rcvd_qty;
                        end if;
                     end if;
                     ---
                     if FM_RECEIVING_SQL.GET_NIC_IND(L_error_message,
                                                     L_non_match_tax.tax_code,
                                                     L_nic_ind) = FALSE then
                        O_error_message := L_error_message;
                        return FALSE;
                     end if;
                     ---
                     if L_nic_ind = L_nic_ind_flg then
                        L_unit_tax_value := nvl(L_unit_tax_value,0)+ nvl(L_tax_wac(L_tax_tab_cnt).unit_tax_value,0)*L_rcv_detail(i).pack_qty;
                     end if;
                     ---
                  end loop;
               end if;
               ---
            end if;
            ---
            L_bc_cost  := (L_nic_cost +  L_rcv_detail(i).freight_cost + L_rcv_detail(i).other_expenses_cost + L_rcv_detail(i).insurance_cost) - L_unit_tax_value;
            L_ebc_cost := L_bc_cost + ((L_total_tax_amount + L_non_match_unit_tax_val)-(L_total_rec_val + L_non_match_unit_rec_val));

            ---
            L_pack_bc_cost  := L_pack_bc_cost  + L_bc_cost;
            L_pack_ebc_cost := L_pack_ebc_cost + L_ebc_cost;
            ---
            if O_old_ebc <> L_ebc_cost then
               if FM_RECEIVING_SQL.UPDATE_FM_RECEIVING_DETAIL(L_error_message,
                                                              L_rcv_detail(i).seq_no,
                                                              L_pack_nic_cost,
                                                              L_pack_ebc_cost ) = FALSE then
                  O_error_message := L_error_message;
                  return FALSE;
               end if;
            end if;
            ---
            O_new_nic := nvl(L_pack_nic_cost,0);
            O_new_ebc := nvl(L_pack_ebc_cost,0);
            ---
         end loop;
      exit when L_ref_cursor%NOTFOUND;
   end loop;
   ---
   if L_tax_wac.count > 0 then
      ---
      forall j in 1 .. L_tax_wac.count
         delete from fm_fiscal_doc_tax_detail_wac
          where fiscal_doc_line_id = L_tax_wac(j).fiscal_doc_line_id
            and tax_code = L_tax_wac(j).tax_code;


      ---
      forall k in 1 .. L_tax_wac.count

         insert into fm_fiscal_doc_tax_detail_wac (fiscal_doc_line_id,
                                                   tax_code,
                                                   tax_rate,
                                                   tax_basis,
                                                   modified_tax_basis,
                                                   rcvd_qty,
                                                   unit_tax_value,
                                                   create_datetime,
                                                   create_id,
                                                   last_update_datetime,
                                                   last_update_id,
                                                   unit_rec_value)
                                           values (L_tax_wac(k).fiscal_doc_line_id,
                                                   L_tax_wac(k).tax_code,
                                                   L_tax_wac(k).tax_rate,
                                                   L_tax_wac(k).tax_basis,
                                                   L_tax_wac(k).modified_tax_basis,
                                                   L_tax_wac(k).rcvd_qty,
                                                   L_tax_wac(k).unit_tax_value,
                                                   SYSDATE,
                                                   USER,
                                                   SYSDATE,
                                                   USER,
                                                   L_tax_wac(k).unit_rec_value);

      ---
      L_tax_wac.delete();
      L_rcv_detail.delete();
      ---
   end if;
   close L_ref_cursor;
   return TRUE;
   ---
EXCEPTION
   ---
   when DML_ERRORS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_key,
                                            TO_CHAR(SQLCODE));
      return FALSE;
   ---
   when OTHERS then
      ---
      if C_NF_LINE_TAXES%ISOPEN then
         close C_NF_LINE_TAXES;
      end if;
      ---
      if C_SYS_LINE_TAXES%ISOPEN then
         close C_SYS_LINE_TAXES;
      end if;
      ---
      if C_SYS_TAXES_NON_MATCH_IND%ISOPEN then
         close C_SYS_TAXES_NON_MATCH_IND;
      end if;
      ---
      if C_NON_MATCH_TAX%ISOPEN then
         close C_NON_MATCH_TAX;
      end if;
      ---
      if C_GET_TRNG_IND%ISOPEN then
         close C_GET_TRNG_IND;
      end if;
      ---
      if C_GET_TRNG_LINE_ID%ISOPEN then
         close C_GET_TRNG_LINE_ID;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
---
end CALC_FINAL_EBC;
----------------------------------------------------------------------------------
-- Function Name: CALC_FINAL_CLT
-- Purpose:       This function inserts tax records into FM_CORRECTION_DOC and
--                FM_CORRECTION_TAX_DOC tables
----------------------------------------------------------------------------------
FUNCTION CALC_FINAL_CLT(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.CALC_FINAL_CLT';
   L_key                 VARCHAR2(80) := 'Fiscal_Doc_Id = '|| I_fiscal_doc_id;
   L_yes                 FM_TAX_CODES.MATCHING_IND%TYPE  default 'Y';
   ---
   L_no                  FM_TAX_CODES.MATCHING_IND%TYPE  default 'N';
   L_tax_discrep         FM_RESOLUTION.DISCREP_TYPE%TYPE default 'T';
   L_resolved            FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE default 'R';
   ---
   L_tax_discrep_type    FM_RESOLUTION.DISCREP_TYPE%TYPE default 'T';
   L_action_clc          FM_CORRECTION_DOC.ACTION_TYPE%TYPE default 'CLC';
   L_action_clt          FM_CORRECTION_DOC.ACTION_TYPE%TYPE default 'CLT';
   ---
   L_action_clq          FM_CORRECTION_DOC.ACTION_TYPE%TYPE default 'CLQ';
   L_action_cnft         FM_CORRECTION_DOC.ACTION_TYPE%TYPE default 'CNFT';
   L_res_nf              FM_RESOLUTION.RESOLUTION_TYPE%TYPE default 'NF';
   ---
   L_res_sys             FM_RESOLUTION.RESOLUTION_TYPE%TYPE default 'SYS';
   L_qty                 FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_cost                FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   ---
   L_tax_resolution      FM_RESOLUTION.RESOLUTION_TYPE%TYPE;
   L_sys_tax_value       FM_FISCAL_DOC_TAX_DETAIL_WAC.UNIT_TAX_VALUE%TYPE;
   L_sys_tax_rate        FM_FISCAL_DOC_TAX_DETAIL_WAC.TAX_RATE%TYPE;
   L_cnft_value          FM_FISCAL_DOC_TAX_DETAIL_WAC.UNIT_TAX_VALUE%TYPE;
   L_cnft_tax_basis      FM_FISCAL_DOC_TAX_DETAIL_WAC.TAX_BASIS%TYPE;
   L_cnft_modified_tax_basis      FM_FISCAL_DOC_TAX_DETAIL_WAC.MODIFIED_TAX_BASIS%TYPE;
   ---
   L_sys_tax_base        FM_FISCAL_DOC_TAX_DETAIL_WAC.TAX_BASIS%TYPE;
   L_sys_modified_tax_base        FM_FISCAL_DOC_TAX_DETAIL_WAC.MODIFIED_TAX_BASIS%TYPE;
   L_sys_tax_rec_val     FM_FISCAL_DOC_TAX_DETAIL_EXT.UNIT_REC_VALUE%TYPE;
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   ---
   L_tax_disc_exists     BOOLEAN;
   L_mtr_called          BOOLEAN;
   L_status              INTEGER;
   ---
   L_tax_discrep_status  FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE;
   L_tax_disc_cnt        NUMBER;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   ---
      cursor C_RCV_DETAIL(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
   select ffdh.fiscal_doc_id,
          ffdh.schedule_no,
          ffdd.fiscal_doc_line_id,
          nvl(ffdd.quantity,0) nf_qty,
          nvl(ffdd.unit_cost,0) nf_unit_cost,
          nvl(ffdd.total_cost,0) nf_total_cost,
          ffdd.tax_discrep_status tax_det_discrep,
          ffdh.tax_discrep_status tax_head_discrep
     from FM_FISCAL_DOC_DETAIL ffdd, FM_FISCAL_DOC_HEADER ffdh
    where ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
      and ffdd.fiscal_doc_id = P_fiscal_doc_id;
   ---
   cursor C_CORRECTION_DETAILS(P_fiscal_doc_line_id FM_CORRECTION_DOC.FISCAL_DOC_LINE_ID%TYPE,
                               P_action_type FM_CORRECTION_DOC.ACTION_TYPE%TYPE) is
   select seq_no,
          action_type,
          action_value
     from FM_CORRECTION_DOC
    where fiscal_doc_line_id = P_fiscal_doc_line_id
      and action_type = P_action_type;
   ---
   cursor C_NF_LINE_TAXES(P_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
   select  'NF' tax_source,
           vat_code,
           fiscal_doc_line_id,
           nvl(percentage_rate,0) percentage_rate,
           nvl(total_value,0) total_value,
           nvl(res_base_value,nvl(appr_base_value,tax_basis)) nf_base_value,
           nvl(res_modified_base_value,nvl(appr_modified_base_value,modified_tax_basis)) nf_modified_base_value,
           nvl(res_tax_value, nvl(appr_tax_value,total_value)) nf_tax_value,
           nvl(res_tax_rate,nvl(appr_tax_rate,percentage_rate)) nf_tax_rate,
           nvl(tax_basis,0) tax_basis,
           nvl(modified_tax_basis,0) modified_tax_basis,
           nvl(rec_value,0) rec_value,
           nvl(unit_tax_amt,0) unit_tax_amt,
           nvl(unit_rec_value,0) unit_rec_val,
          (select resolution_type
             from FM_RESOLUTION fr
            where fdtd.fiscal_doc_line_id = fr.fiscal_doc_line_id
              and fdtd.vat_code = fr.tax_code) resolution_type
     from FM_FISCAL_DOC_TAX_DETAIL fdtd, FM_TAX_CODES ftc
    where fdtd.fiscal_doc_line_id = P_fiscal_doc_line_id
      and fdtd.vat_code = ftc.tax_code
      and ftc.matching_ind = L_yes
   union
   select 'SYS' tax_source,
          fdtde.tax_code vat_code,
          fiscal_doc_line_id,
          nvl(tax_rate,0)  percentage_rate,
          nvl(tax_value,0) total_value,
		      0 nf_base_value,
		      0 nf_modified_base_value,
          0	nf_tax_value,
          nvl(tax_rate,0)  nf_tax_rate,
          nvl(tax_basis,0) tax_basis,
          nvl(modified_tax_basis,0) modified_tax_basis,
          nvl(tax_rec_value,0) rec_value,
          nvl(unit_tax_amt,0) unit_tax_amt,
          nvl(unit_rec_value,0) unit_rec_value,
          (select resolution_type
             from FM_RESOLUTION fr
            where fdtde.fiscal_doc_line_id = fr.fiscal_doc_line_id
              and fdtde.tax_code = fr.tax_code) resolution_type
     from FM_FISCAL_DOC_TAX_DETAIL_EXT fdtde,  FM_TAX_CODES ftc
    where fdtde.fiscal_doc_line_id = P_fiscal_doc_line_id
      and fdtde.tax_code not in (select ffdtd.vat_code
                                   from fm_fiscal_doc_tax_detail ffdtd
                                  where ffdtd.fiscal_doc_line_id = P_fiscal_doc_line_id)
      and fdtde.tax_code = ftc.tax_code
      and ftc.matching_ind = L_yes
      and tax_value > 0
    order by resolution_type;

   ---
   cursor C_TAX_DISCREPANT_EXISTS(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE, P_tax_code FM_RESOLUTION.TAX_CODE%TYPE) is
   select count(1)
     from FM_RESOLUTION
    where fiscal_doc_line_id = P_fiscal_doc_line_id
      and tax_code = P_tax_code
      and discrep_type = L_tax_discrep;
   ---
  cursor C_NON_MATCH_TAX(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
   select ftc.tax_code
     from FM_TAX_CODES ftc
     ,fm_fiscal_doc_tax_detail_ext ffdte
    where ffdte.tax_code = ftc.tax_code
    and ffdte.fiscal_doc_line_id = P_fiscal_doc_line_id
   and  ftc.matching_ind = L_no;
   ---
  cursor C_CLT_EXISTS(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
    select 'X'
     from fm_correction_doc
    where fiscal_doc_line_id = P_fiscal_doc_line_id
      and action_type      in (L_action_clc,L_action_clq);
   ---
   L_non_match_tax            C_NON_MATCH_TAX%ROWTYPE;
   L_rcv_detail               C_RCV_DETAIL%ROWTYPE;
   L_nf_line_taxes            C_NF_LINE_TAXES%ROWTYPE;
   L_correction_clq           C_CORRECTION_DETAILS%ROWTYPE;
   L_correction_clc           C_CORRECTION_DETAILS%ROWTYPE;
   L_correction_doc           FM_CORRECTION_DOC%ROWTYPE;
   L_correction_clq_null      C_CORRECTION_DETAILS%ROWTYPE;
   L_correction_clc_null      C_CORRECTION_DETAILS%ROWTYPE;
   ---
--   L_line_tax_obj             OBJ_TAXDETAILRBO      := OBJ_TAXDETAILRBO(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   L_line_tax_obj_tbl         "RIB_TaxDetRBO_TBL"        :=  "RIB_TaxDetRBO_TBL"();
   L_line_tax_obj_tbl_null    "RIB_TaxDetRBO_TBL"        :=  "RIB_TaxDetRBO_TBL"();
   ---
   L_trng_ind                 ORDHEAD.TRIANGULATION_IND%TYPE;
   L_compl_fiscal_doc_id      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_exists                   VARCHAR2(1)  := NULL;
   ---
begin
   ---
   for L_rcv_detail in C_RCV_DETAIL(I_fiscal_doc_id) loop
      L_key              := L_rcv_detail.fiscal_doc_line_id;
      L_mtr_called       := FALSE;
      L_line_tax_obj_tbl := L_line_tax_obj_tbl_null;
      ---
      L_correction_doc.fiscal_doc_id      := L_rcv_detail.fiscal_doc_id;
      L_correction_doc.fiscal_doc_line_id := L_rcv_detail.fiscal_doc_line_id;
      ---
      L_correction_clq := L_correction_clq_null;
      L_correction_clc := L_correction_clc_null;

      open C_CORRECTION_DETAILS(L_rcv_detail.fiscal_doc_line_id, L_action_clq);
      fetch C_CORRECTION_DETAILS into L_correction_clq;
      close C_CORRECTION_DETAILS;
      ---
      open C_CORRECTION_DETAILS(L_rcv_detail.fiscal_doc_line_id, L_action_clc);
      fetch C_CORRECTION_DETAILS into L_correction_clc;
      close C_CORRECTION_DETAILS;
      ---
      if L_correction_clq.seq_no IS NOT NULL then
         L_qty := L_correction_clq.action_value;
      else
         L_qty := L_rcv_detail.nf_qty;
      end if;
      ---
      if L_correction_clc.seq_no IS NOT NULL then
         L_cost := L_correction_clc.action_value;
      else
         L_cost := L_rcv_detail.nf_unit_cost;
      end if;
      ---
      for L_nf_line_taxes in C_NF_LINE_TAXES(L_rcv_detail.fiscal_doc_line_id) loop
         ---
         L_tax_disc_cnt :=0;
         open C_TAX_DISCREPANT_EXISTS(L_rcv_detail.fiscal_doc_line_id, L_nf_line_taxes.vat_code);
         fetch C_TAX_DISCREPANT_EXISTS into L_tax_disc_cnt;
         close C_TAX_DISCREPANT_EXISTS;
         ---
         L_tax_resolution :=NULL;
         if FM_RECEIVING_SQL.GET_TAX_RESOLUTION_TYPE(L_error_message,
                                                     L_rcv_detail.fiscal_doc_line_id,
                                                     L_tax_discrep_type,
                                                     L_nf_line_taxes.vat_code,
                                                     L_tax_resolution,
                                                     L_tax_disc_exists) = FALSE then
            O_error_message := L_error_message;
            return FALSE;
         end if;
         ---
         L_sys_tax_value   := 0;
         L_sys_tax_rate    := 0;
         L_sys_tax_base    := 0;
         L_sys_modified_tax_base    := 0;
         L_cnft_value      := 0;
         L_cnft_tax_basis  := 0;
         L_cnft_modified_tax_basis  := 0;

         if L_qty = L_rcv_detail.nf_qty and L_cost = L_rcv_detail.nf_unit_cost and (L_tax_disc_cnt = 0 or L_tax_resolution = L_res_nf) then
            null;
         else
            if (NOT L_tax_disc_exists and (L_qty <> L_rcv_detail.nf_qty  or L_cost <> L_rcv_detail.nf_unit_cost)) or
               (L_tax_disc_exists and L_tax_resolution = L_res_sys ) then

               if L_mtr_called then
                  if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                         L_error_message,
                                                         L_status,
                                                         L_rcv_detail.fiscal_doc_id,
                                                         L_rcv_detail.fiscal_doc_line_id,
                                                         L_nf_line_taxes.vat_code,
                                                         L_sys_tax_value,
                                                         L_sys_tax_base,
                                                         L_sys_modified_tax_base,
                                                         L_sys_tax_rate,
                                                         L_sys_tax_rec_val) = FALSE then
                     O_error_message := L_error_message;
                     return FALSE;
                  end if;
               else
                  FM_EXT_TAXES_SQL.GP_tax_ind := 0;
                  FM_EXT_TAXES_SQL.GP_fetch_comp_taxes := FALSE;
                  if FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(L_line_tax_obj_tbl,
                                                         L_error_message,
                                                         L_status,
                                                         L_rcv_detail.fiscal_doc_id,
                                                         L_rcv_detail.fiscal_doc_line_id,
                                                         L_qty,
                                                         L_cost) = FALSE then
                     O_error_message := L_error_message;
                     return FALSE;
                  else
                     L_mtr_called := TRUE;
                     if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                            L_error_message,
                                                            L_status,
                                                            L_rcv_detail.fiscal_doc_id,
                                                            L_rcv_detail.fiscal_doc_line_id,
                                                            L_nf_line_taxes.vat_code,
                                                            L_sys_tax_value,
                                                            L_sys_tax_base,
                                                            L_sys_modified_tax_base,
                                                            L_sys_tax_rate,
                                                            L_sys_tax_rec_val) = FALSE then
                        O_error_message := L_error_message;
                        return FALSE;
                     end if;
                  end if;
               end if;
               ---
            elsif (L_tax_disc_exists and L_tax_resolution = L_res_nf) then
               L_sys_tax_value  := (L_cost*L_qty*L_nf_line_taxes.nf_tax_value)/(L_rcv_detail.nf_unit_cost * L_rcv_detail.nf_qty);
               L_sys_tax_rate   := L_nf_line_taxes.nf_tax_rate;
               L_sys_tax_base   := L_nf_line_taxes.nf_base_value;
               L_sys_modified_tax_base   := L_nf_line_taxes.nf_modified_base_value;
            end if;
            ---
            if nvl(L_sys_tax_value,0) < nvl(L_nf_line_taxes.nf_tax_value,0) then
               L_correction_doc.action_type   := L_action_clt;
               if FM_CORRECTION_DOC_SQL.GEN_CORRECT_DOC(L_error_message,
                                                        L_correction_doc,
                                                        L_nf_line_taxes.vat_code,
                                                        L_sys_tax_base,
                                                        nvl(L_sys_tax_value,0),
                                                        L_sys_tax_rate,
                                                        L_sys_modified_tax_base) = FALSE then
                  O_error_message := L_error_message;
                  return FALSE;
               end if;
            elsif (nvl(L_sys_tax_value,0) > nvl(L_nf_line_taxes.nf_tax_value,0)) or
                  (L_nf_line_taxes.tax_source = L_res_sys) then
               L_correction_doc.action_type   := L_action_cnft;

               if L_nf_line_taxes.tax_source = L_res_sys then
                  L_cnft_value     := nvl(L_sys_tax_value,0);
                  L_cnft_tax_basis := nvl(L_sys_tax_base,0);
                  L_cnft_modified_tax_basis := nvl(L_sys_modified_tax_base,0);
               else
                   L_cnft_value     := nvl(L_sys_tax_value,0) - nvl(L_nf_line_taxes.nf_tax_value,0);

                   if nvl(L_sys_tax_base,0) <> nvl(L_nf_line_taxes.nf_base_value,0) then
                      L_cnft_tax_basis := nvl(L_sys_tax_base,0) - nvl(L_nf_line_taxes.nf_base_value,0);
                      L_cnft_modified_tax_basis := null;
                   else
                      L_cnft_tax_basis := nvl(L_nf_line_taxes.nf_base_value,0);
                      L_cnft_modified_tax_basis := null;
                   end if;
                   if nvl(L_sys_modified_tax_base,0) <> nvl(L_nf_line_taxes.nf_modified_base_value,0) then
                      L_cnft_tax_basis := null;
                      L_cnft_modified_tax_basis := nvl(L_sys_modified_tax_base,0) - nvl(L_nf_line_taxes.nf_modified_base_value,0);
                   else
                      L_cnft_tax_basis := null;
                      L_cnft_modified_tax_basis := nvl(L_nf_line_taxes.nf_modified_base_value,0);
                   end if;
               end if;

               if FM_CORRECTION_DOC_SQL.GEN_CORRECT_DOC(L_error_message,
                                                        L_correction_doc,
                                                        L_nf_line_taxes.vat_code,
                                                        L_cnft_tax_basis,
                                                        L_cnft_value,
                                                        L_nf_line_taxes.nf_tax_rate,
                                                        L_cnft_modified_tax_basis) = FALSE then
                  O_error_message := L_error_message;
                  return FALSE;
               end if;
            end if;
         end if;
            ---
      end loop;
      ---
      open C_CLT_EXISTS(L_rcv_detail.fiscal_doc_line_id);
      fetch C_CLT_EXISTS into L_exists;
      close C_CLT_EXISTS;
      ---
      if L_exists is NOT NULL THEN
         for L_non_match_tax in C_NON_MATCH_TAX(L_rcv_detail.fiscal_doc_line_id) loop
            if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                   L_error_message,
                                                   L_status,
                                                   L_rcv_detail.fiscal_doc_id,
                                                   L_rcv_detail.fiscal_doc_line_id,
                                                   L_non_match_tax.tax_code,
                                                   L_sys_tax_value,
                                                   L_sys_tax_base,
                                                   L_sys_modified_tax_base,
                                                   L_sys_tax_rate,
                                                   L_sys_tax_rec_val) = FALSE then
               O_error_message := L_error_message;
               return FALSE;
            else
               if (L_sys_tax_base is not null or L_sys_modified_tax_base is not null) and L_sys_tax_value is not null and L_sys_tax_rate is not null then
                  L_correction_doc.action_type   := L_action_clt;
                  if FM_CORRECTION_DOC_SQL.GEN_CORRECT_DOC(L_error_message,
                                                           L_correction_doc,
                                                           L_non_match_tax.tax_code,
                                                           L_sys_tax_base,
                                                           nvl(L_sys_tax_value,0),
                                                           L_sys_tax_rate,
                                                           L_sys_modified_tax_base) = FALSE then
                      O_error_message := L_error_message;
                     return FALSE;
                  end if;
               end if;
            end if;
         end loop;
      end if;
      ---
   end loop;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      ---
      if C_RCV_DETAIL%ISOPEN then
         close C_RCV_DETAIL;
      end if;
      ---
      if C_NF_LINE_TAXES%ISOPEN then
         close C_NF_LINE_TAXES;
      end if;
      ---
      if C_CORRECTION_DETAILS%ISOPEN then
         close C_CORRECTION_DETAILS;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
---
end CALC_FINAL_CLT;
-------------------------------------------------------------------------------
-- Function Name: CALC_FINAL_EBC_TSF
-- Purpose:       This function updates EBC and NIC in FM_RECEIVING_DETAIL table
--                for transfer
-----------------------------------------------------------------------------------
FUNCTION CALC_FINAL_EBC_TSF(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                            I_pack_no            IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                            O_old_ebc            IN OUT FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                            O_old_nic            IN OUT FM_RECEIVING_DETAIL.NIC_COST%TYPE,
                            O_new_ebc            IN OUT FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                            O_new_nic            IN OUT FM_RECEIVING_DETAIL.NIC_COST%TYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_RECEIVING_SQL.CALC_FINAL_EBC_TSF';
   L_key                 VARCHAR2(80) := 'Fiscal_Doc_Id = '|| I_fiscal_doc_id;
   L_yes                 FM_TAX_CODES.MATCHING_IND%TYPE default 'Y';
   L_no                  FM_TAX_CODES.MATCHING_IND%TYPE default 'N';
   L_store               FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE default 'S';
   L_wh                  FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE default 'W';
   ---
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_NF_LINE_TAXES(P_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is

      select 'NF' tax_source,
             vat_code,
             fiscal_doc_line_id,
             nvl(percentage_rate,0) percentage_rate,
             nvl(total_value,0) total_value,
             nvl(res_base_value,nvl(appr_base_value,tax_basis)) nf_base_value,
             nvl(res_modified_base_value,nvl(appr_modified_base_value,modified_tax_basis)) nf_modified_base_value,
             nvl(res_tax_value, nvl(appr_tax_value,total_value)) nf_tax_value,
             nvl(res_tax_rate,nvl(appr_tax_rate,percentage_rate)) nf_tax_rate,
             nvl(tax_basis,0) tax_basis,
             nvl(modified_tax_basis,0) modified_tax_basis,
             nvl(rec_value,0) rec_value,
             nvl(unit_tax_amt,0) unit_tax_amt,
             nvl(unit_rec_value,0) unit_rec_val,
             (select ffdd.fiscal_doc_id
                from FM_FISCAL_DOC_DETAIL ffdd
               where ffdd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id) fiscal_doc_id
        from FM_FISCAL_DOC_TAX_DETAIL fdtd, FM_TAX_CODES ftc
       where fdtd.fiscal_doc_line_id = P_fiscal_doc_line_id
         and fdtd.vat_code = ftc.tax_code
         and ftc.matching_ind = L_yes
      union
      select 'SYS' tax_source,
             fdtde.tax_code vat_code,
             fiscal_doc_line_id,
             nvl(tax_rate,0)  percentage_rate,
             nvl(tax_value,0) total_value,
             nvl(tax_basis,0) nf_base_value,
             nvl(modified_tax_basis,0) nf_modified_base_value,
             nvl(tax_value,0) nf_tax_value,
             nvl(tax_rate,0)  nf_tax_rate,
             nvl(tax_basis,0) tax_basis,
             nvl(modified_tax_basis,0) modified_tax_basis,
             nvl(tax_rec_value,0) rec_value,
             nvl(unit_tax_amt,0) unit_tax_amt,
             nvl(unit_rec_value,0) unit_rec_value,
             (select ffdd.fiscal_doc_id
                from FM_FISCAL_DOC_DETAIL ffdd
               where ffdd.fiscal_doc_line_id = fdtde.fiscal_doc_line_id) fiscal_doc_id
        from FM_FISCAL_DOC_TAX_DETAIL_EXT fdtde, FM_TAX_CODES ftc
       where fdtde.fiscal_doc_line_id = P_fiscal_doc_line_id
         and fdtde.tax_code not in (select ffdtd.vat_code
                                      from fm_fiscal_doc_tax_detail ffdtd
                                     where ffdtd.fiscal_doc_line_id = P_fiscal_doc_line_id)
         and fdtde.tax_code = ftc.tax_code
         and ftc.matching_ind = L_yes;
   ---

   cursor C_SYS_LINE_TAXES(P_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                           P_tax_code            FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE) is
      select tax_code,
             nvl(tax_rate,0) tax_rate,
             nvl(tax_basis,0) tax_basis,
             nvl(modified_tax_basis,0) modified_tax_basis,
             nvl(tax_value,0) tax_value,
             tax_type,
             nvl(tax_perc_mva,0) tax_perc_mva,
             nvl(tax_val_mva, 0) tax_val_mva,
             nvl(tax_rec_value,0) tax_rec_value,
             nvl(unit_tax_amt,0) unit_tax_amt,
             nvl(unit_rec_value,0) unit_rec_value
        from FM_FISCAL_DOC_TAX_DETAIL_EXT
       where fiscal_doc_line_id = P_fiscal_doc_line_id
         and tax_code = P_tax_code;
    ---
    cursor C_EXIT_NF_INFO(P_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
       select fdd.fiscal_doc_id,
              fdd.fiscal_doc_line_id
         from fm_fiscal_doc_detail fdd,
              fm_fiscal_doc_detail fdd2
        where fdd.fiscal_doc_line_id != P_fiscal_doc_line_id
          and fdd2.fiscal_doc_line_id = P_fiscal_doc_line_id
          and fdd.requisition_no = fdd2.requisition_no
          and fdd.item = fdd2.item;
   ---
   cursor C_GET_TSF_LOCS (P_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select t.from_loc_type,
             t.from_loc,
             t.to_loc_type,
             t.to_loc,
             d.tsf_seq_no
        from tsfhead t,
             tsfdetail d,
             fm_fiscal_doc_detail fdd
       where t.tsf_no               = d.tsf_no
         and t.tsf_no               = fdd.requisition_no
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id
   union all
      select 'W',
             t.wh,
             d.to_loc_type,
             d.to_loc,
             NULL
        from alloc_header t,
             alloc_detail d,
             fm_fiscal_doc_detail fdd
       where t.alloc_no               = d.alloc_no
         and t.alloc_no               = fdd.requisition_no
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id;

   R_get_tsf_details C_GET_TSF_LOCS%ROWTYPE;

   ---
   L_sys_line_taxes           C_SYS_LINE_TAXES%ROWTYPE;
   L_sys_line_taxes_null      C_SYS_LINE_TAXES%ROWTYPE;
   L_nf_line_taxes            C_NF_LINE_TAXES%ROWTYPE;
   ---
   L_nic_ind                  VAT_CODES.INCL_NIC_IND%TYPE;
   ---
   L_nic_ind_flg              VAT_CODES.INCL_NIC_IND%TYPE default 'Y';
   ---
   L_nic_cost                 FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   L_ebc_cost                 FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_bc_cost                  FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   ---
   L_old_nic_cost             FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   L_old_ebc_cost             FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   ---
   L_unit_tax_value           FM_FISCAL_DOC_TAX_DETAIL.APPR_TAX_VALUE%TYPE;
   ---
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   ---
   L_status                   INTEGER;
   ---
   L_total_tax_amount         FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   ---
   L_total_rec_val            FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_REC_VALUE%TYPE;
   ---
   L_fiscal_doc_id            FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_exit_nf_line_id          FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   ---
   L_sys_tax_value            FM_FISCAL_DOC_TAX_DETAIL_WAC.UNIT_TAX_VALUE%TYPE;
   L_sys_tax_rate             FM_FISCAL_DOC_TAX_DETAIL_WAC.TAX_RATE%TYPE;
   ---
   L_sys_tax_base             FM_FISCAL_DOC_TAX_DETAIL_WAC.TAX_BASIS%TYPE;
   L_sys_modified_tax_base    FM_FISCAL_DOC_TAX_DETAIL_WAC.MODIFIED_TAX_BASIS%TYPE;
   L_sys_tax_rec_val          FM_FISCAL_DOC_TAX_DETAIL_EXT.UNIT_REC_VALUE%TYPE;
   ---
--   L_line_tax_obj             OBJ_TAXDETAILRBO    := OBJ_TAXDETAILRBO(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   L_line_tax_obj_tbl         "RIB_TaxDetRBO_TBL"        :=  "RIB_TaxDetRBO_TBL"();
   L_line_tax_obj_tbl_null    "RIB_TaxDetRBO_TBL"        :=  "RIB_TaxDetRBO_TBL"();
   ---
   L_pack_ebc_cost            FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_pack_bc_cost             FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_pack_nic_cost            FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   L_total_chrgs_prim         ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_total_chrgs_fin          ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_profit_chrgs_to_loc      ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_exp_chrgs_to_loc         ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   ---
   TYPE RC is ref cursor;
   L_ref_cursor               RC;
   TYPE L_DETAIL_TAB is table of L_DETAIL_REC;
   L_rcv_detail               L_DETAIL_TAB := L_DETAIL_TAB(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   ---
   DML_ERRORS                 EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(DML_ERRORS, -24381);
   ---
begin
   L_pack_ebc_cost := 0;
   L_pack_bc_cost  := 0;
   L_pack_nic_cost := 0;
   ---
   if I_pack_no is null then
      open L_ref_cursor for select L_no pack,
                                   ffdh.fiscal_doc_id,
                                   frh.recv_no,
                                   frh.requisition_type,
                                   frh.location_type,
                                   frh.location_id,
                                   frh.recv_type,
                                   frh.currency_code,
                                   frh.recv_date,
                                   frh.document_type,
                                   frh.status hdr_status,
                                   ffdh.schedule_no,
                                   frd.seq_no,
                                   frd.header_seq_no,
                                   frd.item rcvd_item,
                                   ffdd.item nf_item,
                                   nvl(frd.quantity,0) rcvd_qty,
                                   frd.requisition_no requisition_no,
                                   nvl(frd.nic_cost,0) nic_cost,
                                   nvl(frd.ebc_cost,0) ebc_cost,
                                   ffdd.fiscal_doc_line_id,
                                   nvl(ffdd.quantity,0) nf_qty,
                                   nvl(ffdd.unit_cost_with_disc,0) nf_unit_cost_with_disc,
                                   nvl(ffdd.unit_cost,0) nf_unit_cost,
                                   nvl(ffdd.total_cost,0) nf_total_cost,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   nvl(ffdd.freight_cost,0) freight_cost,
                                   nvl(ffdd.other_expenses_cost,0) other_expenses_cost,
                                   nvl(ffdd.insurance_cost,0) insurance_cost,
                                   ffdd.pack_no,
                                   ffdd.pack_ind,
                                   1 pack_qty
                              from FM_RECEIVING_DETAIL frd,
                                   FM_FISCAL_DOC_DETAIL ffdd,
                                   TSFDETAIL vt,
                                   FM_FISCAL_DOC_HEADER ffdh,
                                   FM_RECEIVING_HEADER frh,
                                   TSFHEAD vh
                             where frd.item = ffdd.item
                               and vt.tsf_no = vh.tsf_no
                               and ((frd.requisition_no = ffdd.requisition_no and vh.tsf_no = ffdd.requisition_no)
                                or vh.tsf_parent_no = ffdd.requisition_no)
                               and vt.item = ffdd.item
                               and frh.fiscal_doc_id = ffdh.fiscal_doc_id
                               and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
                               and frh.seq_no=frd.header_seq_no
                               and frd.reason_code is NULL
                               and nvl(ffdd.unexpected_item, L_no) = L_no
                               and ffdh.fiscal_doc_id = I_fiscal_doc_id
                               and frh.requisition_type in ('TSF','IC','REP')
                               and frd.quantity > 0
                            union all
                            select L_no pack,
                                   ffdh.fiscal_doc_id,
                                   frh.recv_no,
                                   frh.requisition_type,
                                   frh.location_type,
                                   frh.location_id,
                                   frh.recv_type,
                                   frh.currency_code,
                                   frh.recv_date,
                                   frh.document_type,
                                   frh.status hdr_status,
                                   ffdh.schedule_no,
                                   frd.seq_no,
                                   frd.header_seq_no,
                                   frd.item rcvd_item,
                                   ffdd.item nf_item,
                                   nvl(frd.quantity,0) rcvd_qty,
                                   frd.requisition_no requisition_no,
                                   nvl(frd.nic_cost,0) nic_cost,
                                   nvl(frd.ebc_cost,0) ebc_cost,
                                   ffdd.fiscal_doc_line_id,
                                   nvl(ffdd.quantity,0) nf_qty,
                                   nvl(ffdd.unit_cost_with_disc,0) nf_unit_cost_with_disc,
                                   nvl(ffdd.unit_cost,0) nf_unit_cost,
                                   nvl(ffdd.total_cost,0) nf_total_cost,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   nvl(ffdd.freight_cost,0) freight_cost,
                                   nvl(ffdd.other_expenses_cost,0) other_expenses_cost,
                                   nvl(ffdd.insurance_cost,0) insurance_cost,
                                   ffdd.pack_no,
                                   ffdd.pack_ind,
                                   1 pack_qty
                              from FM_RECEIVING_DETAIL frd,
                                   FM_FISCAL_DOC_DETAIL ffdd,
                                   FM_FISCAL_DOC_HEADER ffdh,
                                   FM_RECEIVING_HEADER frh,
                                   alloc_header ah
                             where frd.item = ffdd.item
                               and frd.requisition_no = ffdd.requisition_no
                               and ah.alloc_no = ffdd.requisition_no
                               and ah.item = ffdd.item
                               and frh.fiscal_doc_id = ffdh.fiscal_doc_id
                               and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
                               and frh.seq_no=frd.header_seq_no
                               and frd.reason_code is NULL
                               and nvl(ffdd.unexpected_item, 'N') = 'N'
                               and ffdh.fiscal_doc_id = I_fiscal_doc_id
                               and frh.requisition_type in ('TSF','IC','REP')
                               and frd.quantity > 0
                          order by fiscal_doc_id;
   elsif I_pack_no is NOT NULL then
      open L_ref_cursor for select L_yes pack,
                                   ffdh.fiscal_doc_id,
                                   frh.recv_no,
                                   frh.requisition_type,
                                   frh.location_type,
                                   frh.location_id,
                                   frh.recv_type,
                                   frh.currency_code,
                                   frh.recv_date,
                                   frh.document_type,
                                   frh.status hdr_status,
                                   ffdh.schedule_no,
                                   frd.seq_no,
                                   frd.header_seq_no,
                                   frd.item rcvd_item,
                                   ffdd.item nf_item,
                                   nvl(frd.quantity,0)*nvl(pi.pack_qty,1) rcvd_qty,
                                   frd.requisition_no requisition_no,
                                   nvl(frd.nic_cost,0) nic_cost,
                                   nvl(frd.ebc_cost,0) ebc_cost,
                                   ffdd.fiscal_doc_line_id,
                                   nvl(ffdd.quantity,0) nf_qty,
                                   nvl(ffdd.unit_cost_with_disc,0) nf_unit_cost_with_disc,
                                   nvl(ffdd.unit_cost,0) nf_unit_cost,
                                   nvl(ffdd.total_cost,0) nf_total_cost,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   nvl(ffdd.freight_cost,0) freight_cost,
                                   nvl(ffdd.other_expenses_cost,0) other_expenses_cost,
                                   nvl(ffdd.insurance_cost,0) insurance_cost,
                                   ffdd.pack_no,
                                   ffdd.pack_ind,
                                   pi.pack_qty pack_qty
                              from FM_RECEIVING_DETAIL frd,
                                   FM_FISCAL_DOC_DETAIL ffdd,
                         	         TSFDETAIL vt,
                                   TSFHEAD vh,
                                   FM_FISCAL_DOC_HEADER ffdh,
	                                 FM_RECEIVING_HEADER frh,
                          	       PACKITEM pi
                             where (frd.item = ffdd.item
                                or frd.item = ffdd.pack_no)
                               and vt.tsf_no = vh.tsf_no
                               and ((frd.requisition_no = ffdd.requisition_no and vh.tsf_no = ffdd.requisition_no)
                                or vh.tsf_parent_no = ffdd.requisition_no)
                               and vt.item = decode(ffdd.pack_ind,'Y', ffdd.item, NVL(ffdd.pack_no,ffdd.item))
                               and ffdd.pack_no = pi.pack_no (+)
                               and ffdd.item = pi.item(+)
                               and frh.fiscal_doc_id = ffdh.fiscal_doc_id
                               and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
                               and frh.seq_no=frd.header_seq_no
                               and frd.reason_code is NULL
                               and nvl(ffdd.unexpected_item, L_no) = L_no
                               and ffdh.fiscal_doc_id = I_fiscal_doc_id
                               and ffdd.pack_no = I_pack_no
                               and frh.requisition_type in ('TSF','IC','REP')
                               and frd.quantity > 0
                         union all
                            select L_yes pack,
                                   ffdh.fiscal_doc_id,
                                   frh.recv_no,
                                   frh.requisition_type,
                                   frh.location_type,
                                   frh.location_id,
                                   frh.recv_type,
                                   frh.currency_code,
                                   frh.recv_date,
                                   frh.document_type,
                                   frh.status hdr_status,
                                   ffdh.schedule_no,
                                   frd.seq_no,
                                   frd.header_seq_no,
                                   frd.item rcvd_item,
                                   ffdd.item nf_item,
                                   nvl(frd.quantity,0)*nvl(pi.pack_qty,1) rcvd_qty,
                                   frd.requisition_no requisition_no,
                                   nvl(frd.nic_cost,0) nic_cost,
                                   nvl(frd.ebc_cost,0) ebc_cost,
                                   ffdd.fiscal_doc_line_id,
                                   nvl(ffdd.quantity,0) nf_qty,
                                   nvl(ffdd.unit_cost_with_disc,0) nf_unit_cost_with_disc,
                                   nvl(ffdd.unit_cost,0) nf_unit_cost,
                                   nvl(ffdd.total_cost,0) nf_total_cost,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   nvl(ffdd.freight_cost,0) freight_cost,
                                   nvl(ffdd.other_expenses_cost,0) other_expenses_cost,
                                   nvl(ffdd.insurance_cost,0) insurance_cost,
                                   ffdd.pack_no,
                                   ffdd.pack_ind,
                                   pi.pack_qty pack_qty
                              from FM_RECEIVING_DETAIL frd,
                                   FM_FISCAL_DOC_DETAIL ffdd,
                                   ALLOC_HEADER ah,
                                   FM_FISCAL_DOC_HEADER ffdh,
	                                 FM_RECEIVING_HEADER frh,
                          	       PACKITEM pi
                             where ffdh.fiscal_doc_id = I_fiscal_doc_id
                               and frh.fiscal_doc_id = ffdh.fiscal_doc_id
                               and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
                               and frh.seq_no = frd.header_seq_no
                               and ffdd.pack_no = I_pack_no
                               and (frd.item = ffdd.item
                                or frd.item = ffdd.pack_no)
                               and frd.requisition_no = ffdd.requisition_no
                               and ah.alloc_no = ffdd.requisition_no
                               and ah.item = decode(ffdd.pack_ind,'Y', ffdd.item, NVL(ffdd.pack_no,ffdd.item))
                               and ffdd.pack_no = pi.pack_no(+)
                               and ffdd.item = pi.item(+)
                               and frd.reason_code is NULL
                               and nvl(ffdd.unexpected_item, L_no) = L_no
                               and frh.requisition_type in ('TSF','IC','REP')
                               and frd.quantity > 0
                          order by fiscal_doc_id;
   end if;
   loop
      fetch L_ref_cursor bulk collect into L_RCV_DETAIL limit 100;
         for i in 1..L_rcv_detail.count
         loop
            ---
            if L_rcv_detail(i).pack = L_no then
               L_pack_ebc_cost := 0;
               L_pack_bc_cost  := 0;
               L_pack_nic_cost := 0;
            end if;
            ---
            L_total_tax_amount := 0;
            L_total_rec_val    := 0;
            L_unit_tax_value   := 0;
            L_line_tax_obj_tbl := L_line_tax_obj_tbl_null;
            ---
            L_nic_cost := L_rcv_detail(i).nf_unit_cost;

           if L_rcv_detail(i).pack_no is not NULL then
              L_nic_cost := L_nic_cost * L_rcv_detail(i).pack_qty;
           end if;

           L_old_nic_cost := L_rcv_detail(i).nic_cost;
           L_old_ebc_cost := L_rcv_detail(i).ebc_cost;
           ---
           O_old_nic := nvl(L_rcv_detail(i).nic_cost,0);
           O_old_ebc := nvl(L_rcv_detail(i).ebc_cost,0);
           ---
           L_pack_nic_cost := L_pack_nic_cost + L_nic_cost;
           ---
           for L_nf_line_taxes in C_NF_LINE_TAXES(L_rcv_detail(i).fiscal_doc_line_id) loop
           ---
              L_sys_line_taxes := L_sys_line_taxes_null;
              L_sys_tax_value := 0;
              L_sys_tax_rec_val := 0;

              open C_EXIT_NF_INFO(L_nf_line_taxes.fiscal_doc_line_id);

              fetch C_EXIT_NF_INFO into L_fiscal_doc_id, L_exit_nf_line_id;

              close C_EXIT_NF_INFO;
          ---
              open C_SYS_LINE_TAXES(L_exit_nf_line_id, L_nf_line_taxes.vat_code);

              fetch C_SYS_LINE_TAXES into L_sys_line_taxes;

              if C_SYS_LINE_TAXES%ROWCOUNT = 0 then
                 null;
              end if;

              close C_SYS_LINE_TAXES;
           ---
              L_nic_ind := NULL;
              if FM_RECEIVING_SQL.GET_NIC_IND(L_error_message,
                                              L_sys_line_taxes.tax_code,
                                              L_nic_ind) = FALSE then
                 O_error_message := L_error_message;
                 return FALSE;
              end if;
              if L_rcv_detail(i).rcvd_qty = L_rcv_detail(i).nf_qty or L_rcv_detail(i).requisition_type in ('TSF', 'IC') then
                 if L_nf_line_taxes.vat_code is not null then
                  ---
                    L_total_tax_amount := nvl(L_total_tax_amount,0)+ (nvl(L_nf_line_taxes.unit_tax_amt,0)) * L_rcv_detail(i).pack_qty;
                    L_total_rec_val    := nvl(L_total_rec_val,0)+  (nvl(L_nf_line_taxes.unit_rec_val,0)) * L_rcv_detail(i).pack_qty;
                 end if;
                  ---
              elsif L_rcv_detail(i).rcvd_qty <> L_rcv_detail(i).nf_qty then
                 if FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(L_line_tax_obj_tbl,
                                                        L_error_message,
                                                        L_status,
                                                        L_fiscal_doc_id,
                                                        L_exit_nf_line_id,
                                                        L_rcv_detail(i).rcvd_qty,
                                                        L_rcv_detail(i).nf_unit_cost) = FALSE then
                    O_error_message := L_error_message;
                    return FALSE;
                 end if;
                 if FM_RECEIVING_SQL.GET_OBJ_TAX_VALUES(L_line_tax_obj_tbl,
                                                        L_error_message,
                                                        L_status,
                                                        L_fiscal_doc_id,
                                                        L_exit_nf_line_id,
                                                        L_nf_line_taxes.vat_code,
                                                        L_sys_tax_value,
                                                        L_sys_tax_base,
                                                        L_sys_modified_tax_base,
                                                        L_sys_tax_rate,
                                                        L_sys_tax_rec_val) = FALSE then
                    O_error_message := L_error_message;
                    return FALSE;
                 end if;
            ---
                 L_total_tax_amount := nvl(L_total_tax_amount,0)+ (nvl(L_sys_tax_value,0)/L_rcv_detail(i).rcvd_qty) * L_rcv_detail(i).pack_qty;
                 L_total_rec_val    := nvl(L_total_rec_val,0)+ (nvl(L_sys_tax_rec_val,0)/L_rcv_detail(i).rcvd_qty) * L_rcv_detail(i).pack_qty;
              end if;
              if L_nic_ind = L_nic_ind_flg then
                 L_unit_tax_value := nvl(L_unit_tax_value,0)+ nvl(L_nf_line_taxes.unit_tax_amt,0) * L_rcv_detail(i).pack_qty;
              end if;
       ---
           end loop;
           open C_GET_TSF_LOCS(L_rcv_detail(i).fiscal_doc_line_id);
           fetch C_GET_TSF_LOCS into R_get_tsf_details;
           if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(L_error_message,
                                                          L_total_chrgs_prim,
                                                          L_profit_chrgs_to_loc,
                                                          L_exp_chrgs_to_loc,
                                                          L_rcv_detail(i).document_type,
                                                          L_rcv_detail(i).requisition_no,
                                                          R_get_tsf_details.tsf_seq_no,
                                                          NULL,
                                                          NULL,
                                                          L_rcv_detail(i).nf_item,
                                                          NULL,
                                                          R_get_tsf_details.from_loc,
                                                          R_get_tsf_details.from_loc_type,
                                                          R_get_tsf_details.to_loc,
                                                          R_get_tsf_details.to_loc_type) = FALSE then
              O_error_message := L_error_message;
              return FALSE;
           end if;
           close C_GET_TSF_LOCS;
           L_total_chrgs_fin  := L_total_chrgs_fin + L_total_chrgs_prim;
           ---
           L_bc_cost  := (L_nic_cost +  L_rcv_detail(i).freight_cost + L_rcv_detail(i).other_expenses_cost + L_rcv_detail(i).insurance_cost) - L_unit_tax_value - L_total_chrgs_fin;
           L_ebc_cost := L_bc_cost + (L_total_tax_amount - L_total_rec_val ) - L_total_chrgs_fin;
           ---
           L_pack_bc_cost  := L_pack_bc_cost  + L_bc_cost;
           L_pack_ebc_cost := L_pack_ebc_cost + L_ebc_cost;
           ---
           if O_old_ebc <> L_ebc_cost then
              if FM_RECEIVING_SQL.UPDATE_FM_RECEIVING_DETAIL(L_error_message,
                                                             L_rcv_detail(i).seq_no,
                                                             L_pack_nic_cost,
                                                             L_pack_ebc_cost ) = FALSE then
                 O_error_message := L_error_message;
                 return FALSE;
              end if;
           end if;
           ---
           O_new_nic := nvl(L_pack_nic_cost,0);
           O_new_ebc := nvl(L_pack_ebc_cost,0);
           ---

         end loop;
      exit when L_ref_cursor%NOTFOUND;
   end loop;
   L_rcv_detail.delete();
   close L_ref_cursor;

   return TRUE;
   ---
EXCEPTION

   when DML_ERRORS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_key,
                                            TO_CHAR(SQLCODE));
      return FALSE;

   when OTHERS then
      ---
      if C_NF_LINE_TAXES%ISOPEN then
         close C_NF_LINE_TAXES;
      end if;
      ---
      if C_SYS_LINE_TAXES%ISOPEN then
         close C_SYS_LINE_TAXES;
      end if;
      ---
      if C_EXIT_NF_INFO%ISOPEN then
         close C_EXIT_NF_INFO;
      end if;
      ---
      if C_GET_TSF_LOCS%ISOPEN then
         close C_GET_TSF_LOCS;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_key,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END CALC_FINAL_EBC_TSF;
------------------------------------------------------------------------------------------------------
END FM_RECEIVING_SQL;
/