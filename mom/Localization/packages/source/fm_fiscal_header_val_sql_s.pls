create or replace PACKAGE FM_FISCAL_HEADER_VAL_SQL as
----------------------------------------------------------------------------------
-- CREATE DATE - 26-04-2007
-- CREATE USER ? Sigma.EPP
-- PROJECT / FRD - 10481 / 10481_ORFMI_FRD_001
-- DESCRIPTION ? Package associated to form orfm_fiscal_header.fmb
----------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Function Name: GET_NEXT_FISCAL_DOC_ID
-- Purpose:       This function gets the next foscal_doc_id number.
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_FISCAL_DOC_ID(O_error_message  IN OUT VARCHAR2,
                                O_fiscal_doc_id  IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_TYPE_ID
-- Purpose:       This function returns the query for record_grup REC_TYPE_ID.
----------------------------------------------------------------------------------
FUNCTION LOV_TYPE_ID(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_utilization_id IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_SUPPLIER
-- Purpose:       This function returns the query for record_grup REC_SUPPLIER.
----------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER(O_error_message      IN OUT VARCHAR2,
                      O_query              IN OUT VARCHAR2,
                      I_supplier_sites_ind IN SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE Default 'N')
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_PARTNER
-- Purpose:       This function returns the query for record_grup REC_PARTNER.
----------------------------------------------------------------------------------
FUNCTION LOV_PARTNER(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_where_clause   IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_LOC
-- Purpose:       This function returns the query for record_grup REC_LOC.
----------------------------------------------------------------------------------
FUNCTION LOV_LOC(O_error_message  IN OUT VARCHAR2,
                 O_query          IN OUT VARCHAR2,
                 I_where_clause   IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_OUTLOC
-- Purpose:       This function returns the query for record_grup REC_OUTLOC.
----------------------------------------------------------------------------------
FUNCTION LOV_OUTLOC(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2,
                    I_where_clause   IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ENTITY
-- Purpose:       This function validates the entity of NF.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ENTITY(O_error_message  IN OUT VARCHAR2,
                         O_description    IN OUT VARCHAR2,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_ENTITY_INFO
-- Purpose:       This function gets the entity informations.
----------------------------------------------------------------------------------
FUNCTION GET_ENTITY_INFO(O_error_message  IN OUT VARCHAR2,
                         O_cnpjcfp_type   IN OUT V_FISCAL_ATTRIBUTES.TYPE%TYPE,
                         O_cnpjcfp        IN OUT V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
                         O_addr_1         IN OUT V_FISCAL_ATTRIBUTES.ADDR_1%TYPE,
                         O_addr_2         IN OUT V_FISCAL_ATTRIBUTES.ADDR_2%TYPE,
                         O_addr_3         IN OUT V_FISCAL_ATTRIBUTES.ADDR_3%TYPE,
                         O_postal_code    IN OUT V_FISCAL_ATTRIBUTES.POSTAL_CODE%TYPE,
                         O_city           IN OUT V_FISCAL_ATTRIBUTES.CITY%TYPE,
                         O_district       IN OUT V_FISCAL_ATTRIBUTES.NEIGHBORHOOD%TYPE,
                         O_state          IN OUT V_FISCAL_ATTRIBUTES.STATE%TYPE,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_CUST_INFO
-- Purpose:       This function gets the customer informations.
----------------------------------------------------------------------------------
--FUNCTION GET_CUST_INFO(O_error_message  IN OUT VARCHAR2,
--                       O_cnpjcfp_type   IN OUT V_FISCAL_ATTRIBUTES.TYPE%TYPE,
--                       O_cnpjcfp        IN OUT V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
--                       O_addr_1         IN OUT FM_RMA_HEAD.ADDR_1%TYPE,
--                       O_addr_2         IN OUT FM_RMA_HEAD.ADDR_2%TYPE,
--                       O_addr_3         IN OUT FM_RMA_HEAD.ADDR_3%TYPE,
--                       O_postal_code    IN OUT FM_RMA_HEAD.POSTAL_CODE%TYPE,
--                       O_city           IN OUT FM_RMA_HEAD.CITY%TYPE,
--                       O_district       IN OUT FM_RMA_HEAD.NEIGHBORHOOD%TYPE,
--                       O_state          IN OUT FM_RMA_HEAD.STATE%TYPE,
--                       I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
--                       I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
--                       I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
--   return BOOLEAN;


----------------------------------------------------------------------------------
-- Function Name: GET_CITY_DESC
-- Purpose:       This function gets the entity informations.
----------------------------------------------------------------------------------
FUNCTION GET_CITY_DESC(O_error_message  IN OUT VARCHAR2,
                       O_city_desc      IN OUT COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                       I_city           IN     COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)

   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_CNPJCPF
-- Purpose:       This function returns the query for record_grup REC_CNPJCPF.
----------------------------------------------------------------------------------
FUNCTION LOV_CNPJCPF(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                     I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                     I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                     I_cnpfcpf        IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_CNPJCPF
-- Purpose:       This function validates the CNPJ/CPF.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_CNPJCPF(O_error_message  IN OUT VARCHAR2,
                          O_unique         IN OUT BOOLEAN,
                          O_tipo_cnpjcpf   IN OUT VARCHAR2,
                          O_key_value_1    IN OUT V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                          I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                          I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                          I_cnpjcpf        IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_CUSTOMER
-- Purpose:       This function returns the query for record_grup REC_CUSTOMER.
----------------------------------------------------------------------------------
FUNCTION LOV_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: LOV_UTILIZATION
-- Purpose:       This function returns the query for record_grup REC_LOV_UTILIZATION.
----------------------------------------------------------------------------------
FUNCTION LOV_UTILIZATION(O_error_message    IN OUT VARCHAR2,
                         O_query            IN OUT VARCHAR2,
                         I_requisition_type IN     FM_FISCAL_UTILIZATION.REQUISITION_TYPE%TYPE,
                         I_utilization_id   IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE,
                         I_mode_type        IN     FM_FISCAL_UTILIZATION.MODE_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_CST
-- Purpose:       This function validates the CST.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_CST (O_error_message    IN OUT VARCHAR2,
                       O_exist           IN OUT BOOLEAN,
                       I_string_value     IN VARCHAR2)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_PLATE_STATE
-- Purpose:       This function returns the query for record_grup REC_PLATE_STATE.
----------------------------------------------------------------------------------
FUNCTION LOV_PLATE_STATE(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: GET_STATE_DESC
-- Purpose:       This function returns the description of the state selected
----------------------------------------------------------------------------------
FUNCTION GET_STATE_DESC(O_error_message  IN OUT VARCHAR2,
                        O_description    IN OUT STATE.DESCRIPTION%TYPE,
                        I_state          IN     STATE.STATE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_PLATE_STATE
-- Purpose:       This function validates the Plate State.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_PLATE_STATE (O_error_message   IN OUT VARCHAR2,
                               O_exist           IN OUT BOOLEAN,
                               I_state           IN STATE.STATE%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_COMPL_NF
-- Purpose:  This function validates that Complementary NF for Non Merchandise cost
--           should have only one NF in schecule.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_COMPL_NF (O_error_message   IN OUT VARCHAR2,
                            O_exist           IN OUT BOOLEAN,
                            I_utilization_id  IN     FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                            I_Schedule_no     IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CHECK_MAIN_NF_EXIST
-- Purpose:       This function checks the Main NF is attched to Complementary NF or not
----------------------------------------------------------------------------------
FUNCTION CHECK_MAIN_NF_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        OUT    BOOLEAN,
                             I_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_DOC_TYPE
-- Purpose:       This function returns the document type
----------------------------------------------------------------------------------

FUNCTION GET_DOC_TYPE(O_error_message  IN OUT VARCHAR2,
                      O_doc_type       IN OUT FM_FISCAL_DOC_HEADER.DOCUMENT_TYPE%TYPE,
                      I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: VALIDATE_SUPPLIER
-- Purpose:       This function validates the Supplier/Supplier site
----------------------------------------------------------------------------------

FUNCTION VALIDATE_SUPPLIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
END FM_FISCAL_HEADER_VAL_SQL;
/