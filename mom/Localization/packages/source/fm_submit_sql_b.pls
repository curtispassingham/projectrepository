CREATE OR REPLACE PACKAGE BODY FM_SUBMIT_SQL AS
-- Constant values
   C_SUBMIT_STATUS     CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'S';--Submit status
   C_ERROR_STATUS      CONSTANT FM_SCHEDULE.STATUS%TYPE := 'E';--Error status
   C_APPROVED_STATUS   CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'A';--Approved
   C_VALIDATE_STATUS   CONSTANT FM_SCHEDULE.STATUS%TYPE := 'V';--Validated status
   C_COMPLETE_STATUS   CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'C';--Completed status
   C_NORMAL            CONSTANT FM_RECEIVING_HEADER.RECV_TYPE%TYPE := 'NORM';
   C_PACK              CONSTANT VARCHAR2(30) := 'PACK';
   C_PACK_EMBE_ITEM    CONSTANT VARCHAR2(30) := 'PACK_EMBE_ITEM';
   C_EMBE_ITEM         CONSTANT VARCHAR2(30) := 'EMBE_ITEM';
   C_SINGLE_ITEM       CONSTANT VARCHAR2(30) := 'SINGLE_ITEM';
--------------------------------------------------------------------
   FUNCTION GET_NEXT_SEQ_HEADER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_seq_no         IN OUT FM_RECEIVING_HEADER.SEQ_NO%TYPE)
      return BOOLEAN is
      ---
      L_program   VARCHAR2(100) := 'FM_STG_RECEIVING_SQL.GET_NEXT_SEQ_HEADER';
      L_key       VARCHAR2(100) := 'fm_recv_header_seq: ';
      ---
      SEQ_MAXVAL    EXCEPTION;
      PRAGMA        EXCEPTION_INIT(SEQ_MAXVAL,-08004);
      ---
      cursor C_GET_NEXT_SEQ is
         select FM_RECV_HEADER_SEQ.NEXTVAL
           from dual;
      ---
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_NEXT_SEQ',
                       'FM_RECV_HEADER_SEQ',
                       L_key);

      open C_GET_NEXT_SEQ;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_NEXT_SEQ',
                       'FM_RECV_HEADER_SEQ',
                       L_key);

      fetch C_GET_NEXT_SEQ into O_seq_no;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_NEXT_SEQ',
                       'FM_RECV_HEADER_SEQ',
                       L_key);
      close C_GET_NEXT_SEQ;
      ---
      return TRUE;
   ---
   EXCEPTION
      when SEQ_MAXVAL then
         if C_GET_NEXT_SEQ%ISOPEN then
            close C_GET_NEXT_SEQ;
         end if;

      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1,254),
                                               L_program,
                                               to_char(SQLCODE));
         ---
         if C_GET_NEXT_SEQ%ISOPEN then
            close C_GET_NEXT_SEQ;
         end if;
         ---
         return FALSE;
   END GET_NEXT_SEQ_HEADER;
----------------------------------------------------------------------------------
   FUNCTION GET_NEXT_SEQ_LINE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_seq_no         IN OUT FM_RECEIVING_DETAIL.SEQ_NO%TYPE)
      return BOOLEAN is
      ---
      L_program   VARCHAR2(100) := 'FM_STG_RECEIVING_SQL.GET_NEXT_SEQ_LINE';
      L_key       VARCHAR2(100) := 'fm_receiving_detail: ';
      ---
      SEQ_MAXVAL    EXCEPTION;
      PRAGMA        EXCEPTION_INIT(SEQ_MAXVAL,-08004);
      ---
      cursor C_GET_NEXT_SEQ is
         select FM_RECV_DETAIL_SEQ.NEXTVAL
           from dual;
      ---
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_NEXT_SEQ',
                       'FM_RECV_DETAIL_SEQ',
                       L_key);
      open C_GET_NEXT_SEQ;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_NEXT_SEQ',
                       'FM_RECV_DETAIL_SEQ',
                       L_key);
      fetch C_GET_NEXT_SEQ into O_seq_no;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_NEXT_SEQ',
                       'FM_RECV_DETAIL_SEQ',
                       L_key);

      close C_GET_NEXT_SEQ;
      ---
      return TRUE;
      ---
   EXCEPTION
      when SEQ_MAXVAL then
         if C_GET_NEXT_SEQ%ISOPEN then
            close C_GET_NEXT_SEQ;
         end if;

      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1,254),
                                               L_program,
                                               to_char(SQLCODE));
         ---
         if C_GET_NEXT_SEQ%ISOPEN then
            close C_GET_NEXT_SEQ;
         end if;
         ---
         return FALSE;
   END GET_NEXT_SEQ_LINE;
----------------------------------------------------------------------------------
   FUNCTION GET_SUBMIT_STATUS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_submit_status IN OUT FM_SCHEDULE.SUBMITTED_IND%TYPE,
                              I_schedule_no   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   --
      L_program VARCHAR2(100) := 'FM_SUBMIT_SQL.GET_SUBMIT_STATUS';
      L_key     VARCHAR2(100) := 'schedule_no: ' || TO_CHAR(I_schedule_no);
      --
      cursor C_SUBMIT_STATUS is
         select submitted_ind
           from fm_schedule fs
          where fs.schedule_no = I_schedule_no;
      --
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_SUBMIT_STATUS',
                       'FM_SCHEDULE',
                       L_key);
      open C_SUBMIT_STATUS;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_SUBMIT_STATUS',
                       'FM_SCHEDULE',
                       L_key);
      fetch C_SUBMIT_STATUS
         into O_submit_status ;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SUBMIT_STATUS',
                       'FM_SCHEDULE',
                       L_key);
      close C_SUBMIT_STATUS;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_SUBMIT_STATUS%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_SUBMIT_STATUS',
                             'FM_SCHEDULE',
                             L_key);
            close C_SUBMIT_STATUS;
         end if;
         ---
         return FALSE;
   END GET_SUBMIT_STATUS;
   -----------------------------------------------------------------------------
   FUNCTION CLEAR_SCHEDULE_ERRORS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                                  I_program        IN     VARCHAR2)
   return BOOLEAN is
      ---
      L_program  VARCHAR2(100) := 'FM_SUBMIT_SQL.CLEAR_SCHEDULE_ERRORS';
      L_key      VARCHAR2(100) := 'schedule_no: ' || TO_CHAR(I_schedule_no);
      ---
      cursor C_GET_DOCS is
         select h.fiscal_doc_id
           from fm_fiscal_doc_header h
          where h.schedule_no = I_schedule_no;
      ---
      L_rec_docs        C_GET_DOCS%ROWTYPE;
      ---
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_DOCS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);

      open C_GET_DOCS;
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_DOCS',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_GET_DOCS into L_rec_docs;
         EXIT when C_GET_DOCS%NOTFOUND;
         ---
         if FM_ERROR_LOG_SQL.CLEAR_ERROR(O_error_message,
                                         L_rec_docs.fiscal_doc_id,
                                         I_program) = FALSE then
            if C_GET_DOCS%ISOPEN then
               ---
               SQL_LIB.SET_MARK('CLOSE',
                                'C_GET_DOCS',
                                'FM_FISCAL_DOC_HEADER',
                                L_key);
               close C_GET_DOCS;
            end if;
         ---
            return FALSE;
         end if;
         ---
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_DOCS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_GET_DOCS;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1,254),
                                               L_program,
                                               to_char(SQLCODE));
         ---
         if C_GET_DOCS%ISOPEN then
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_DOCS',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_GET_DOCS;
         end if;
         ---
         return FALSE;
   END CLEAR_SCHEDULE_ERRORS;
   -----------------------------------------------------------------------------
   FUNCTION WRITE_SCHEDULE_ERRORS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                                  I_program        IN     VARCHAR2)
   return BOOLEAN is
   ---
      L_program  VARCHAR2(100) := 'FM_SUBMIT_SQL.WRITE_SCHEDULE_ERRORS';
      L_key      VARCHAR2(100) := 'schedule_no: ' || TO_CHAR(I_schedule_no);
      ---
      cursor C_GET_DOCS is
         select h.fiscal_doc_id
           from fm_fiscal_doc_header h
          where h.schedule_no = I_schedule_no;
      ---
      L_rec_docs        C_GET_DOCS%ROWTYPE;
      ---
   BEGIN
      ---
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_DOCS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_GET_DOCS;
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_DOCS',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_GET_DOCS into L_rec_docs;
         EXIT when C_GET_DOCS%NOTFOUND;
         ---
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         L_rec_docs.fiscal_doc_id,
                                         I_program,
                                         I_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         O_error_message,
                                         'Schedule_no: '||
                                         TO_CHAR(I_schedule_no)
                                         || ' - Fiscal_doc_id: '||
                                         TO_CHAR(L_rec_docs.fiscal_doc_id)) = FALSE then
            if C_GET_DOCS%ISOPEN then
               ---
               SQL_LIB.SET_MARK('CLOSE',
                                'C_GET_DOCS',
                                'FM_FISCAL_DOC_HEADER',
                                L_key);
               close C_GET_DOCS;
            end if;
            ---
            return FALSE;
         end if;
         ---
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_DOCS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_GET_DOCS;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1,254),
                                               L_program,
                                               to_char(SQLCODE));
         ---
         if C_GET_DOCS%ISOPEN then
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_DOCS',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_GET_DOCS;
         end if;
         ---
         return FALSE;
   END WRITE_SCHEDULE_ERRORS;
   -----------------------------------------------------------------------------
   FUNCTION UPD_SUBMIT_STATUS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_submit_status IN     FM_SCHEDULE.SUBMITTED_IND%TYPE,
                              I_schedule_no   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
      ---
      RECORD_LOCKED   EXCEPTION;
      PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
      ---
      L_program VARCHAR2(50) := 'FM_SUBMIT_SQL.UPD_SUBMIT_STATUS';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      ---
      cursor C_LOCK_FM_SCHD is
         select 'X'
           from fm_schedule fs
          where fs.schedule_no = I_schedule_no
            for update nowait;
   --
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      if I_submit_status is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_submit_status',
                                               I_submit_status,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_FM_SCHD',
                       'FM_SCHEDULE',
                       L_key);
      open C_LOCK_FM_SCHD;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_FM_SCHD',
                       'FM_SCHEDULE',
                       L_key);
      close C_LOCK_FM_SCHD;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_SCHEDULE', L_key);
      update fm_schedule fs
         set fs.submitted_ind = I_submit_status
       where fs.schedule_no = I_schedule_no;
      ---
      return TRUE;
      ---
   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               'FM_SCHEDULE',
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_LOCK_FM_SCHD%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_LOCK_FM_SCHD',
                             'FM_SCHEDULE',
                             L_key);
            close C_LOCK_FM_SCHD;
         end if;
         ---
         return FALSE;
      ---
   END UPD_SUBMIT_STATUS;
   -----------------------------------------------------------------------------
   FUNCTION GET_ITEM_VALUES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_quantity       IN OUT FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE,
                            I_schedule_no    IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                            I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_req_type       IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                            I_req_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                            I_recv_type      IN     VARCHAR2,
                            I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                            I_unexp_ind      IN     FM_FISCAL_DOC_DETAIL.UNEXPECTED_ITEM%TYPE,
                            I_item_type      IN     VARCHAR2,
                            I_compl_ind      IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)
   return BOOLEAN is
      ---
      L_program VARCHAR2(100) := 'FM_STG_RECEIVING_SQL.GET_ITEM_VALUES';
      L_key     VARCHAR2(50)  := 'I_schedule_no: ' || I_schedule_no;
      ---
      L_qty                    FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE := 0;
      L_sing_item_qty          FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE := 0;
      L_pack_item_qty          FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE := 0;
      L_embe_item_qty          FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE := 0;
      L_comp_nf_ind            VARCHAR2(1) := 'N';
      L_pack_ind_n             VARCHAR2(1) := 'N';
      L_pack_ind_y             VARCHAR2(1) := 'Y';
      ---
      cursor C_GET_PACK_QTY is
      select temp.appt_qty/decode(nvl(temp.pack_no,0),0,1,SUM(pi.pack_item_qty))
        from packitem_breakout pi,
             (select NVL(SUM(fdd.appt_qty),0) appt_qty,
                    fdd.pack_no
               from fm_fiscal_doc_header fdh,
                    fm_fiscal_doc_detail fdd
              where fdd.fiscal_doc_id = fdh.fiscal_doc_id
                and fdh.schedule_no = I_schedule_no
                and fdh.fiscal_doc_id = I_fiscal_doc_id
                and fdh.requisition_type = I_req_type
                and NVL(fdd.requisition_no,0) = NVL(I_req_no,0)
                and decode(fdd.pack_ind,L_pack_ind_y,fdd.item,fdd.pack_no) = I_item
                and EXISTS (select 0
                              from fm_utilization_attributes ua,
                                   fm_fiscal_utilization u
                             where u.utilization_id = ua.utilization_id
                               and ua.comp_nf_ind = L_comp_nf_ind)
                group by fdd.pack_no) temp
        where pi.pack_no(+) = temp.pack_no
     group by temp.pack_no,temp.appt_qty;
      ---
      cursor C_GET_ITEM_QTY is
         select NVL(SUM(fdd.appt_qty), 0)
           from fm_fiscal_doc_header fdh,
                fm_fiscal_doc_detail fdd
          where fdd.fiscal_doc_id = fdh.fiscal_doc_id
            and fdh.schedule_no = I_schedule_no
            and fdh.fiscal_doc_id = I_fiscal_doc_id
            and fdh.requisition_type = I_req_type
            and NVL(fdd.requisition_no,0) = NVL(I_req_no,0)
            and fdd.item = I_item
            and fdd.unexpected_item = I_unexp_ind
            and fdd.pack_no IS NULL
            and fdd.pack_ind = L_pack_ind_n
            and EXISTS (select 'X'
                          from fm_utilization_attributes ua,
                               fm_fiscal_utilization u
                         where u.utilization_id = ua.utilization_id
                           and ua.comp_nf_ind = L_comp_nf_ind);
      ---
   BEGIN
      if I_recv_type = C_NORMAL then
         ---
         if I_item_type in (C_PACK, C_PACK_EMBE_ITEM) then
            ---
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_PACK_QTY',
                             'FM_FISCAL_DOC_DETAIL',
                             L_key);
            open C_GET_PACK_QTY;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_PACK_QTY',
                             'FM_FISCAL_DOC_DETAIL',
                             L_key);
            fetch C_GET_PACK_QTY into L_qty;
            ---
            if C_GET_PACK_QTY%NOTFOUND then
               L_qty := 0;
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_PACK_QTY',
                             'FM_FISCAL_DOC_DETAIL',
                             L_key);
            close C_GET_PACK_QTY;
         else
            if I_item_type in (C_EMBE_ITEM, C_SINGLE_ITEM) then
               ---
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_ITEM_QTY',
                                'FM_FISCAL_DOC_DETAIL',
                                L_key);
               open C_GET_ITEM_QTY;
               ---
               SQL_LIB.SET_MARK('FETCH',
                                'C_GET_ITEM_QTY',
                                'FM_FISCAL_DOC_DETAIL',
                                L_key);
               fetch C_GET_ITEM_QTY into L_sing_item_qty;
               ---
               if C_GET_ITEM_QTY%NOTFOUND then
                  L_sing_item_qty := 0;
               end if;
               ---
               SQL_LIB.SET_MARK('CLOSE',
                                'C_GET_ITEM_QTY',
                                'FM_FISCAL_DOC_DETAIL',
                                L_key);
               close C_GET_ITEM_QTY;
            end if;
            ---
            L_qty := greatest(L_sing_item_qty - L_pack_item_qty - L_embe_item_qty, 0);
            ---
         end if;
         ---
      end if;
      ---
      if I_compl_ind = 'N' then
         O_quantity   := L_qty;
      elsif I_compl_ind = 'Y' then
         O_quantity   := 0;
      end if;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1,254),
                                               L_program,
                                               to_char(SQLCODE));
         ---
         if C_GET_PACK_QTY%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_PACK_QTY',
                             'FM_FISCAL_DOC_DETAIL',
                             L_key);
            close C_GET_PACK_QTY;
         end if;
         ---
         if C_GET_ITEM_QTY%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_ITEM_QTY',
                             'FM_FISCAL_DOC_DETAIL',
                             L_key);
            close C_GET_ITEM_QTY;
         end if;
         ---
         return FALSE;
   END GET_ITEM_VALUES;
   -----------------------------------------------------------------------------
   FUNCTION POPULATE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   TYPE T_recv_hdr_tab IS TABLE OF FM_RECEIVING_HEADER%ROWTYPE index by binary_integer;
   TYPE T_recv_dtl_tab IS TABLE OF FM_RECEIVING_DETAIL%ROWTYPE index by binary_integer;
   TYPE T_rib_recv_hdr_tab IS TABLE OF FM_RIB_STG_RECEIVING_HEADER%ROWTYPE index by binary_integer;
   TYPE T_rib_recv_dtl_tab IS TABLE OF FM_RIB_STG_RECEIVING_DETAIL%ROWTYPE index by binary_integer;
   TYPE T_requisition_tab IS TABLE OF FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE index by binary_integer;
   TYPE T_item_tab        IS TABLE OF FM_RECEIVING_DETAIL.ITEM%TYPE index by binary_integer;      
   TYPE T_all_items       IS TABLE OF T_item_tab index by binary_integer;
   TYPE T_carton_rec IS RECORD (carton_no  FM_RECEIVING_DETAIL.CONTAINER_ID%TYPE, 
                                asn_no     FM_RECEIVING_DETAIL.ASN_NBR%TYPE, 
                                item       FM_RECEIVING_DETAIL.ITEM%TYPE);
   TYPE T_carton_tab IS TABLE OF T_carton_rec index by binary_integer;
   L_carton_tab        T_carton_tab;
   L_requisition_tab   T_requisition_tab;
   L_item_tab          T_all_items;  
   L_tab_recv_hdr      T_recv_hdr_tab;
   L_tab_recv_dtl      T_recv_dtl_tab;
   L_tab_rib_recv_hdr  T_rib_recv_hdr_tab;
   L_tab_rib_recv_dtl  T_rib_recv_dtl_tab;
   L_index2            INTEGER := 0;
   L_ind_recv_hdr      INTEGER := 0;
   L_ind_recv_dtl      INTEGER := 0;
   L_ind_rib_recv_hdr  INTEGER := 0;
   L_ind_rib_recv_dtl  INTEGER := 0;
   L_prev_ind_rib_dtl  INTEGER := 1;
   ---
   C_CHAR_TYPE         CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE := 'CHAR';
   C_CURRENCY          CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_CURRENCY';
   ---
   C_PO_TYPE           CONSTANT VARCHAR2(6) := 'PO';
   C_TSF_TYPE          CONSTANT VARCHAR2(6) := 'TSF';
   C_IC_TYPE           CONSTANT VARCHAR2(6) := 'IC';
   C_REP_TYPE          CONSTANT VARCHAR2(6)  := 'REP';
   C_ENTRY_TYPE        CONSTANT VARCHAR2(6) := 'ENT';
   ---
   L_program VARCHAR2(100) := 'FM_SUBMIT_SQL.POPULATE';
   L_key     VARCHAR2(50)  := 'I_schedule_no: ' || I_schedule_no;
   ---
   L_currency_code     FM_RECEIVING_HEADER.CURRENCY_CODE%TYPE;
   L_create_date       DATE := sysdate;
   L_create_id         FM_RECEIVING_HEADER.CREATE_ID%TYPE := USER;
   L_header_seq_no     FM_RECEIVING_HEADER.SEQ_NO%TYPE;
   L_detail_seq_no     FM_RECEIVING_DETAIL.SEQ_NO%TYPE;
   L_document_type     FM_RECEIVING_HEADER.DOCUMENT_TYPE%TYPE;
   L_post_date         FM_RECEIVING_HEADER.POST_DATE%TYPE;
   L_appt_qty          FM_FISCAL_DOC_DETAIL.appt_qty%TYPE := 0;
   L_unit_cost         FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE := 0;
   L_net_cost          FM_FISCAL_DOC_DETAIL.NET_COST%TYPE := 0;
   L_gross_cost        FM_FISCAL_DOC_DETAIL.NET_COST%TYPE := 0;
   L_normal            FM_RECEIVING_HEADER.RECV_TYPE%TYPE := C_NORMAL;
   L_found             BOOLEAN;
   L_asn_no            FM_RECEIVING_DETAIL.ASN_NBR%TYPE; 
   L_requisition_type  FM_RECEIVING_HEADER.REQUISITION_TYPE%TYPE;
   L_pack_ind_yes  varchar2(1) :='Y';
   L_pack_ind_no  varchar2(1) :='N';
   L_comp_nf_ind  varchar2(1) :='N';
   L_unexpected_item  varchar2(1) :='N';
   L_allow_receiving_ind  varchar2(1) :='Y';
   L_dummy             NUMBER;
   L_wh      Varchar2(1) := 'W';
   ---
   L_dummy_desc        FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date        FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_dummy_number      FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   ---
   cursor C_GET_DOC_LINES is
      select distinct
             sc.schedule_no,
             fdh.requisition_type,
             fdh.fiscal_doc_id,
             fdd.requisition_no requisition_no,
             sc.location_type,
             sc.location_id,
             fdh.module,
             fdh.key_value_1,
             fdh.key_value_2,
             L_normal recv_type,
             sc.schedule_date,
             fdd.unexpected_item unexp_ind,
             nvl(fdd.pack_no,fdd.item) item,
             fu.mode_type,
             uc.comp_nf_ind,
             C_PACK L_type,
             fdh.document_type
        from fm_schedule sc,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_utilization_attributes uc,
             fm_fiscal_utilization fu,
             item_master im
       where fdh.schedule_no = sc.schedule_no
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fu.utilization_id = fdh.utilization_id
         and uc.utilization_id = fu.utilization_id
         and decode(fdd.pack_ind,L_pack_ind_yes,fdd.item,fdd.pack_no) = im.item
         and uc.comp_nf_ind = L_comp_nf_ind
         and uc.allow_receiving_ind = L_allow_receiving_ind
         and im.pack_ind = L_pack_ind_yes
         and sc.schedule_no = I_schedule_no
         and sc.status = C_VALIDATE_STATUS
         and fdh.requisition_type = C_PO_TYPE
         and sc.mode_type = C_ENTRY_TYPE
         and fdh.status = C_SUBMIT_STATUS
      UNION ALL
      select distinct
             sc.schedule_no,
             fdh.requisition_type,
             fdh.fiscal_doc_id,
             fdd.requisition_no,
             sc.location_type,
             sc.location_id,
             fdh.module,
             fdh.key_value_1,
             fdh.key_value_2,
             L_normal recv_type,
             sc.schedule_date,
             fdd.unexpected_item unexp_ind,
             fdd.item item,
             fu.mode_type,
             uc.comp_nf_ind,
             C_SINGLE_ITEM L_type,
             fdh.document_type
        from fm_schedule sc,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_utilization_attributes uc,
             fm_fiscal_utilization fu,
             item_master im
       where fdh.schedule_no = sc.schedule_no
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fu.utilization_id = fdh.utilization_id
         and fu.utilization_id = uc.utilization_id
         and uc.comp_nf_ind = L_comp_nf_ind
         and uc.allow_receiving_ind = L_allow_receiving_ind
         and fdd.item = im.item
         and im.pack_ind = L_pack_ind_no
         and fdd.pack_no is NULL
         and fdd.pack_ind = L_pack_ind_no
         and sc.schedule_no = I_schedule_no
         and sc.status = C_VALIDATE_STATUS
         and fdh.requisition_type in (C_PO_TYPE, C_TSF_TYPE,C_IC_TYPE,C_REP_TYPE)
         and sc.mode_type = C_ENTRY_TYPE
         and fdh.status = C_SUBMIT_STATUS
      UNION ALL
    select distinct sc.schedule_no,
           fdh.requisition_type,
           fdh.fiscal_doc_id,
           th.tsf_no requisition_no,
           sc.location_type,
           sc.location_id,
           fdh.module,
           fdh.key_value_1,
           fdh.key_value_2,
           L_normal recv_type,
           sc.schedule_date,
           fdd.unexpected_item unexp_ind,
           td.item item,
           fu.mode_type,
           uc.comp_nf_ind,
           C_PACK l_type,
           fdh.document_type
      from fm_schedule sc,
           fm_fiscal_doc_header fdh,
           fm_fiscal_doc_detail fdd,
           fm_utilization_attributes uc,
           fm_fiscal_utilization fu,
           tsfhead th,
           tsfdetail td,
           item_master im
      where sc.schedule_no = I_schedule_no
        and sc.status = C_VALIDATE_STATUS
        and sc.mode_type = C_ENTRY_TYPE
        and fdh.schedule_no = sc.schedule_no
        and fdh.fiscal_doc_id = fdd.fiscal_doc_id
        and fdh.requisition_type IN (C_TSF_TYPE,C_IC_TYPE,C_REP_TYPE)
        and fdh.status = C_SUBMIT_STATUS
        and fu.utilization_id = fdh.utilization_id
        and uc.utilization_id = fu.utilization_id
        and uc.comp_nf_ind = L_comp_nf_ind
        and uc.allow_receiving_ind = L_allow_receiving_ind
        and th.tsf_no = td.tsf_no
        and fdd.unexpected_item = L_unexpected_item
        and fdd.requisition_no = th.tsf_no
        and decode(fdd.pack_ind,L_pack_ind_yes,fdd.item,fdd.pack_no) = td.item
        and td.item = im.item
        and im.pack_ind = L_pack_ind_yes
        and(fdh.location_id = decode(fdh.location_type,L_wh,(SELECT physical_wh FROM wh WHERE wh = th.to_loc),th.to_loc) or
            fdh.location_id = decode(fdh.location_type,L_wh,(SELECT physical_wh FROM wh WHERE wh = th.from_loc),th.from_loc))
       UNION ALL
    select distinct sc.schedule_no,
           fdh.requisition_type,
           fdh.fiscal_doc_id,
           ah.alloc_no requisition_no,
           sc.location_type,
           sc.location_id,
           fdh.module,
           fdh.key_value_1,
           fdh.key_value_2,
           L_normal recv_type,
           sc.schedule_date,
           fdd.unexpected_item unexp_ind,
           ah.item item,
           fu.mode_type,
           uc.comp_nf_ind,
           C_PACK l_type,
           fdh.document_type
      from fm_schedule sc,
           fm_fiscal_doc_header fdh,
           fm_fiscal_doc_detail fdd,
           fm_utilization_attributes uc,
           fm_fiscal_utilization fu,
           alloc_header ah,
           alloc_detail ad,
           item_master im
      where sc.schedule_no = I_schedule_no
        and sc.status = C_VALIDATE_STATUS
        and sc.mode_type = C_ENTRY_TYPE
        and fdh.schedule_no = sc.schedule_no
        and fdh.fiscal_doc_id = fdd.fiscal_doc_id
        and fdh.requisition_type IN (C_TSF_TYPE,C_IC_TYPE,C_REP_TYPE)
        and fdh.status = C_SUBMIT_STATUS
        and fu.utilization_id = fdh.utilization_id
        and uc.utilization_id = fu.utilization_id
        and uc.comp_nf_ind = L_comp_nf_ind
        and uc.allow_receiving_ind = L_allow_receiving_ind
        and ah.alloc_no = ad.alloc_no
        and fdd.unexpected_item = L_unexpected_item
        and fdd.requisition_no = ah.alloc_no
        and decode(fdd.pack_ind,L_pack_ind_yes,fdd.item,fdd.pack_no) = ah.item
        and ah.item = im.item
        and im.pack_ind = L_pack_ind_yes
        and(fdh.location_id = decode(fdh.location_type,L_wh,(SELECT physical_wh FROM wh WHERE wh = ad.to_loc),ad.to_loc) or
            fdh.location_id = decode(fdh.location_type,L_wh,(SELECT physical_wh FROM wh WHERE wh = ah.wh),ah.wh))
       order by schedule_no,
                fiscal_doc_id,
                mode_type,
                recv_type,
                location_type,
                location_id,
                requisition_type,
                requisition_no,
                item;
   ---
   L_rec_lines         C_GET_DOC_LINES%ROWTYPE;
   ---
   cursor C_GET_PHY_WH (P_wh  wh.WH%TYPE) is
      select w.physical_wh
        from wh w
       where w.wh = P_wh;
   ---
   L_physical_wh       wh.WH%TYPE;
   ---
   cursor C_GET_REQUISITION_NO is
      select distinct frd.requisition_no
        from fm_receiving_detail frd,
             fm_receiving_header frh
       where frd.header_seq_no = frh.seq_no
         and frh.recv_no=I_schedule_no
         and frd.requisition_no is not null;
   ---
    cursor C_GET_ITEM (P_requisition_no NUMBER) is
       select distinct frd.item
         from fm_receiving_detail frd,
              fm_receiving_header frh
        where frd.header_seq_no = frh.seq_no
          and frh.recv_no = I_schedule_no
          and frd.requisition_no = P_requisition_no
          and frd.requisition_no is not null;    
   ---
   cursor C_GET_CARTON is
      select fm_stg_ctn_seq.nextval
        from dual;

   cursor C_GET_TSF_CARTON(P_requisition_no   FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE,
                           P_item             FM_RECEIVING_DETAIL.ITEM%TYPE) is
      select sk.carton carton_no,
             sh.bol_no asn_no,
             sk.item item
        from shipment sh,
             shipsku sk,
             fm_fiscal_doc_header fdh
       where sh.shipment     = sk.shipment
         and sk.distro_no    = P_requisition_no
         and sk.item         = P_item
         and fdh.ref_no_1    = sh.bol_no
         and fdh.schedule_no = I_schedule_no
   union all
      select ct.container_id carton_no,
             ds.asn_nbr asn_no,
             it.item_id item
        from fm_stg_asnout_desc ds,
             fm_stg_asnout_distro dt,
             fm_stg_asnout_ctn ct,
             fm_stg_asnout_item it,
             fm_fiscal_doc_header fdh
       where ds.seq_no       = dt.desc_seq_no
         and dt.seq_no       = ct.distro_seq_no
         and ct.seq_no       = it.ctn_seq_no
         and dt.distro_nbr   = P_requisition_no
         and it.item_id      = P_item
         and fdh.ref_no_1    = ds.asn_nbr
         and fdh.schedule_no = I_schedule_no;

   cursor C_GET_ASN is
      select fm_stg_asn_seq.nextval
        from dual;

  cursor C_GET_REP_NO(P_requisition_no FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE) is
     select tsf.tsf_no
       from tsfhead tsf
      where tsf.tsf_parent_no = P_requisition_no
        and tsf.context_type = 'REPAIR'
     union all
     select tsf.tsf_no
       from tsfhead tsf
      where tsf.tsf_parent_no = P_requisition_no
        and NVL(tsf.context_type,'X') != 'REPAIR' ;

   cursor C_LEG_COUNT(P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select count(1)
        from tsfhead t
       where (t.tsf_parent_no = P_requisition_no
              and from_loc_type = 'E'
           or (tsf_no = P_requisition_no
          and tsf_parent_no is not null
          and from_loc_type = 'E'))
          and NVL(context_type,'X') != 'REPAIR';

   L_message_type         VARCHAR2(10);
   L_rep_no           TSFHEAD.TSF_NO%TYPE := NULL;
   L_count            INTEGER :=0;
   ---
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_recv_no',
                                                L_program,
                                                NULL);
         return FALSE;
      end if;
      ---
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_currency_code,
                                            L_dummy_date,
                                            C_CHAR_TYPE,
                                            C_CURRENCY) = FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_DOC_LINES',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_GET_DOC_LINES;
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_DOC_LINES',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_GET_DOC_LINES into L_rec_lines;
         EXIT when C_GET_DOC_LINES%NOTFOUND;
         -- Get item values
         L_appt_qty    := 0;
         L_unit_cost  := 0;
         L_net_cost   := 0;
         L_gross_cost := 0;
         ---
         if FM_SUBMIT_SQL.GET_ITEM_VALUES(O_error_message,
                                          L_appt_qty,
                                          L_rec_lines.schedule_no,
                                          L_rec_lines.fiscal_doc_id,
                                          L_rec_lines.requisition_type,
                                          L_rec_lines.requisition_no,
                                          L_rec_lines.recv_type,
                                          L_rec_lines.item,
                                          L_rec_lines.unexp_ind,
                                          L_rec_lines.L_type,
                                          L_rec_lines.comp_nf_ind) = FALSE then
            ---
            if C_GET_DOC_LINES%ISOPEN then
               ---
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_DOC_LINES',
                                'FM_FISCAL_DOC_HEADER',
                                L_key);
               close C_GET_DOC_LINES;
            end if;
            ---
            return FALSE;
         end if;
         ---
         -- For Normal Receiving it will only be populated for items tha has, at least, 1 unit.
         ---
         if ((L_rec_lines.comp_nf_ind = 'N' and L_appt_qty != 0)
              or (L_rec_lines.comp_nf_ind = 'Y' and L_unit_cost != 0)) then
            -- Get the physical WH
            ---
            if L_rec_lines.location_type = 'W' then
               ---
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_PHY_WH',
                                'WH',
                                L_key);
               open C_GET_PHY_WH (L_rec_lines.location_id);
               ---
               SQL_LIB.SET_MARK('FETCH',
                                'C_GET_PHY_WH',
                                'WH',
                                L_key);

               fetch C_GET_PHY_WH into L_physical_wh;
               ---
               if C_GET_DOC_LINES%NOTFOUND then
                  ---
                  if C_GET_PHY_WH%ISOPEN then
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_PHY_WH',
                                      'WH',
                                      L_key);
                     close C_GET_PHY_WH;
                  end if;
                  ---
                  O_error_message := SQL_LIB.CREATE_MSG('ORFM_PWH_NOT_FOUND',
                                                         NULL,
                                                         NULL,
                                                         NULL);
                  return FALSE;
               end if;
               ---
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_PHY_WH',
                                'WH',
                                L_key);

               close C_GET_PHY_WH;
               ---
            else
               L_physical_wh := NULL;
            end if;
            --- Insert into the header
            if (L_tab_recv_hdr.first is NULL)
               -- Fisrt record or any primary key field value has changed
               or
               (L_tab_recv_hdr(L_ind_recv_hdr-1).requisition_type != L_rec_lines.requisition_type or
                L_tab_recv_hdr(L_ind_recv_hdr-1).fiscal_doc_id    != L_rec_lines.fiscal_doc_id or
                L_tab_recv_hdr(L_ind_recv_hdr-1).location_type    != L_rec_lines.location_type or
                L_tab_recv_hdr(L_ind_recv_hdr-1).location_id      != NVL(L_physical_wh, L_rec_lines.location_id) or
                L_tab_recv_hdr(L_ind_recv_hdr-1).recv_type        != L_rec_lines.recv_type
               ) then
               if (L_rec_lines.requisition_type in (C_PO_TYPE,C_TSF_TYPE,C_IC_TYPE,C_REP_TYPE)
                  and L_rec_lines.unexp_ind = 'N'
                  and L_tab_rib_recv_hdr.first is NULL) then
                  -- It only enters here one time
                  L_tab_rib_recv_hdr(L_ind_rib_recv_hdr).recv_no := L_rec_lines.schedule_no;
                  L_tab_rib_recv_hdr(L_ind_rib_recv_hdr).location_type := L_rec_lines.location_type;
                  -- If a physical warehouse was found, then is inserted into the rib interface table
                  L_tab_rib_recv_hdr(L_ind_rib_recv_hdr).location_id := NVL(L_physical_wh, L_rec_lines.location_id);
                  ---
                  L_ind_rib_recv_hdr := L_ind_rib_recv_hdr + 1;
               end if;
               ---
               -- ORMS header staging table
                  ---
                  if L_rec_lines.requisition_type = C_PO_TYPE then
                     L_document_type := 'P';
                 elsif L_rec_lines.requisition_type in (C_TSF_TYPE,C_IC_TYPE,C_REP_TYPE) then
                     L_document_type := L_rec_lines.document_type;
                     L_requisition_type := L_rec_lines.requisition_type;
                  end if;
                  ---
                  if L_rec_lines.requisition_type in (C_TSF_TYPE, C_IC_TYPE) then
                     SQL_LIB.SET_MARK('OPEN','C_LEG_COUNT','TSFHEAD','tsf_parent_no '||L_rec_lines.requisition_no||'tsf_no '||L_rec_lines.requisition_no);
                     open C_LEG_COUNT(L_rec_lines.requisition_no);
         ---
                     SQL_LIB.SET_MARK('FETCH','C_LEG_COUNT','TSFHEAD','tsf_parent_no '||L_rec_lines.requisition_no||'tsf_no '||L_rec_lines.requisition_no);
                     fetch C_LEG_COUNT into L_count;

         ---
                     SQL_LIB.SET_MARK('CLOSE','C_LEG_COUNT','TSFHEAD','tsf_parent_no '||L_rec_lines.requisition_no||'tsf_no '||L_rec_lines.requisition_no);
                     close C_LEG_COUNT;
                  end if;

                  if L_rec_lines.requisition_type = C_REP_TYPE or (L_rec_lines.requisition_type in (C_TSF_TYPE, C_IC_TYPE) and L_count > 0) then
                     open C_GET_REP_NO(L_rec_lines.requisition_no);
                     fetch C_GET_REP_NO into L_rep_no;
                     close C_GET_REP_NO;
                  end if;


                  if FM_SUBMIT_SQL.GET_NEXT_SEQ_HEADER(O_error_message,
                                                       L_header_seq_no) = FALSE then
                     ---
                     if C_GET_DOC_LINES%ISOPEN then
                        ---
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_DOC_LINES',
                                         'FM_FISCAL_DOC_HEADER',
                                         L_key);
                        close C_GET_DOC_LINES;
                     end if;
                     ---
                     return FALSE;
                  end if;
                  ---
                  L_tab_recv_hdr(L_ind_recv_hdr).seq_no := L_header_seq_no;
                  L_tab_recv_hdr(L_ind_recv_hdr).recv_no := L_rec_lines.schedule_no;
                  L_tab_recv_hdr(L_ind_recv_hdr).fiscal_doc_id := L_rec_lines.fiscal_doc_id;
                  L_tab_recv_hdr(L_ind_recv_hdr).requisition_type := L_rec_lines.requisition_type;
                  L_tab_recv_hdr(L_ind_recv_hdr).location_type := L_rec_lines.location_type;
                  L_tab_recv_hdr(L_ind_recv_hdr).location_id := NVL(L_physical_wh, L_rec_lines.location_id);
                  L_tab_recv_hdr(L_ind_recv_hdr).recv_type := L_rec_lines.recv_type;
                  L_tab_recv_hdr(L_ind_recv_hdr).currency_code := L_currency_code;
                  L_tab_recv_hdr(L_ind_recv_hdr).recv_date := L_rec_lines.schedule_date;
                  L_tab_recv_hdr(L_ind_recv_hdr).document_type := L_document_type;
                  L_tab_recv_hdr(L_ind_recv_hdr).post_date := L_post_date;
                  L_tab_recv_hdr(L_ind_recv_hdr).status := C_SUBMIT_STATUS;
                  L_tab_recv_hdr(L_ind_recv_hdr).create_datetime := L_create_date;
                  L_tab_recv_hdr(L_ind_recv_hdr).create_id := L_create_id;
                  L_tab_recv_hdr(L_ind_recv_hdr).last_update_datetime := L_create_date;
                  L_tab_recv_hdr(L_ind_recv_hdr).last_update_id := L_create_id;
                  ---
                  L_ind_recv_hdr := L_ind_recv_hdr + 1;
                  ---
            end if;
            ---
            -- ORMS detail staging table
            if (L_tab_recv_hdr.first is NOT NULL) then-- It must have a header
               ---
               if FM_SUBMIT_SQL.GET_NEXT_SEQ_LINE(O_error_message,
                                                  L_detail_seq_no) = FALSE then
                  return FALSE;
               end if;

               ---
               L_tab_recv_dtl(L_ind_recv_dtl).seq_no := L_detail_seq_no;
               L_tab_recv_dtl(L_ind_recv_dtl).header_seq_no := L_header_seq_no;
               L_tab_recv_dtl(L_ind_recv_dtl).item := L_rec_lines.item;
               L_tab_recv_dtl(L_ind_recv_dtl).nic_cost := 0;
               L_tab_recv_dtl(L_ind_recv_dtl).ebc_cost := 0;
               L_tab_recv_dtl(L_ind_recv_dtl).container_id := NULL;
               if L_rep_no is NULL then
                  L_tab_recv_dtl(L_ind_recv_dtl).requisition_no := L_rec_lines.requisition_no;
               else
                  L_tab_recv_dtl(L_ind_recv_dtl).requisition_no := L_rep_no;
               end if;
               L_tab_recv_dtl(L_ind_recv_dtl).asn_nbr := NULL;
               L_tab_recv_dtl(L_ind_recv_dtl).create_datetime := L_create_date;
               L_tab_recv_dtl(L_ind_recv_dtl).create_id := L_create_id;
               L_tab_recv_dtl(L_ind_recv_dtl).last_update_datetime := L_create_date;
               L_tab_recv_dtl(L_ind_recv_dtl).last_update_id := L_create_id;
               ---
               L_ind_recv_dtl := L_ind_recv_dtl + 1;
               ---
            end if;

            -- ORWMS detail staging table
            if (L_tab_rib_recv_hdr.first is NOT NULL -- It must have a header
               and L_rec_lines.requisition_type in (C_PO_TYPE, C_TSF_TYPE, C_IC_TYPE, C_REP_TYPE)
               and L_rec_lines.unexp_ind = 'N') then
               L_found := FALSE;
               if L_tab_rib_recv_dtl.first is NOT NULL then
                  FOR i in 0..L_ind_rib_recv_dtl-L_prev_ind_rib_dtl loop
                     if (L_tab_rib_recv_dtl(i).recv_no = L_rec_lines.schedule_no
                        and L_tab_rib_recv_dtl(i).po_number = NVL(L_rep_no,L_rec_lines.requisition_no)
                        and L_tab_rib_recv_dtl(i).item = L_rec_lines.item) then
                        L_tab_rib_recv_dtl(i).ordered_qty := L_tab_rib_recv_dtl(i).ordered_qty + L_appt_qty;
                        L_found := TRUE;
                     end if;
                  END LOOP;
               end if;
               if L_found = FALSE then
                  L_tab_rib_recv_dtl(L_ind_rib_recv_dtl).recv_no := L_rec_lines.schedule_no;
                  if L_rep_no is NULL then
                     L_tab_rib_recv_dtl(L_ind_rib_recv_dtl).po_number := L_rec_lines.requisition_no;
                  else
                     L_tab_rib_recv_dtl(L_ind_rib_recv_dtl).po_number := L_rep_no;
                  end if;
                  L_tab_rib_recv_dtl(L_ind_rib_recv_dtl).item := L_rec_lines.item;
                  L_tab_rib_recv_dtl(L_ind_rib_recv_dtl).ordered_qty := L_appt_qty;
                  ---
                  L_ind_rib_recv_dtl := L_ind_rib_recv_dtl + 1;
                  ---
               end if;
            end if;
         end if;
         ---
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_DOC_LINES',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_GET_DOC_LINES;
      ---
      if L_tab_recv_hdr.first is NOT NULL
         and L_tab_recv_dtl.first is NOT NULL then
         FORALL i IN L_tab_recv_hdr.first..L_tab_recv_hdr.last
            insert into fm_receiving_header values L_tab_recv_hdr(i);
         L_tab_recv_hdr.delete;
         ---
         FORALL i IN L_tab_recv_dtl.first..L_tab_recv_dtl.last
            insert into fm_receiving_detail values L_tab_recv_dtl(i);
         L_tab_recv_dtl.delete;
         ---
      end if;
      ---
      if L_tab_rib_recv_hdr.first is NOT NULL
      and L_tab_rib_recv_dtl.first is NOT NULL then
         if L_tab_rib_recv_hdr(L_tab_rib_recv_hdr.first).location_type = 'S' or (L_tab_rib_recv_hdr(L_tab_rib_recv_hdr.first).location_type = 'W' and L_document_type in ('A','T','D')) then
            ---
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_REQUISITION_NO',
                             'FM_RECEIVING_DETAIL',
                             L_key);
            open C_GET_REQUISITION_NO;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_REQUISITION_NO',
                             'FM_RECEIVING_DETAIL',
                              L_key);
            fetch C_GET_REQUISITION_NO BULK COLLECT into L_requisition_tab;
            for L_index1 in L_requisition_tab.first .. L_requisition_tab.last loop      
               open C_GET_ITEM(L_requisition_tab(L_index1));
               fetch C_GET_ITEM BULK COLLECT into L_item_tab(L_index1);
               close C_GET_ITEM;
            end loop;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_REQUISITION_NO',
                             'FM_RECEIVING_DETAIL',
                             L_key);
            close C_GET_REQUISITION_NO;
            ---
            for L_index1 in L_requisition_tab.first..L_requisition_tab.last
            loop
               if L_document_type = 'P' or L_requisition_type = C_REP_TYPE or (L_requisition_type in (C_TSF_TYPE, C_IC_TYPE) and L_count > 0) then
                  SQL_LIB.SET_MARK('OPEN','C_GET_ASN','FM_STG_ASN_SEQ',L_key);
                  open  C_GET_ASN;
                  ---
                  SQL_LIB.SET_MARK('FETCH','C_GET_ASN','FM_STG_ASN_SEQ',L_key);
                  fetch C_GET_ASN  INTO L_carton_tab(L_index2).asn_no;
                  L_asn_no := L_carton_tab(L_index2).asn_no;
                  ---
                  SQL_LIB.SET_MARK('CLOSE','C_GET_ASN','FM_STG_ASN_SEQ',L_key);
                  close C_GET_ASN;
                  ---
                  for L_index3 in L_item_tab(L_index1).first..L_item_tab(L_index1).last
                  loop                  
                     SQL_LIB.SET_MARK('OPEN','C_GET_CARTON','FM_STG_CTN_SEQ',L_key);
                     open  C_GET_CARTON;
                  ---
                     SQL_LIB.SET_MARK('FETCH','C_GET_CARTON','FM_STG_CTN_SEQ',L_key);
                     fetch C_GET_CARTON  INTO L_carton_tab(L_index2).carton_no;
                     L_carton_tab(L_index2).item := L_item_tab(L_index1)(L_index3);
                     L_carton_tab(L_index2).asn_no := L_asn_no;                     

                     SQL_LIB.SET_MARK('CLOSE','C_GET_CARTON','FM_STG_CTN_SEQ',L_key);
                     close C_GET_CARTON;
                     L_index2 := L_index2 + 1;
                  end loop;
                  ---
               elsif L_document_type in ('A','T','D') and L_requisition_type != C_REP_TYPE then
                  for L_index3 in L_item_tab(L_index1).first..L_item_tab(L_index1).last
                  loop
                     ---
                     SQL_LIB.SET_MARK('OPEN','C_GET_TSF_CARTON','SHIPSKU',L_key);
                     open  C_GET_TSF_CARTON(L_requisition_tab(L_index1),L_item_tab(L_index1)(L_index3));
                     ---
                     SQL_LIB.SET_MARK('FETCH','C_GET_TSF_CARTON','SHIPSKU',L_key);
                     fetch C_GET_TSF_CARTON into L_carton_tab(L_index2);
                     exit when C_GET_TSF_CARTON%NOTFOUND;
                     L_index2 := L_index2 + 1;
                     ---
                     SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_CARTON','SHIPSKU',L_key);
                     close C_GET_TSF_CARTON;
                  end loop;
               end if;
               for L_index2 in L_carton_tab.first..L_carton_tab.last
               loop
                  update (select frd.asn_nbr asn_nbr, frd.container_id container_id, frd.item item
                            from fm_receiving_detail frd, fm_receiving_header frh
                           where frd.header_seq_no = frh.seq_no
                             and frd.requisition_no = L_requisition_tab(L_index1)
                             and frd.item = L_carton_tab(L_index2).item
                             and frh.recv_no = I_schedule_no) tab
                        set tab.asn_nbr = L_carton_tab(L_index2).asn_no,
                            tab.container_id = L_carton_tab(L_index2).carton_no
                      where tab.item = L_carton_tab(L_index2).item;

               end loop;
            end loop;
         end if;
      end if;
      ---
      if L_tab_rib_recv_hdr.first is NOT NULL
         and L_tab_rib_recv_dtl.first is NOT NULL then
         FORALL i IN L_tab_rib_recv_hdr.first..L_tab_rib_recv_hdr.last
            insert into fm_rib_stg_receiving_header values L_tab_rib_recv_hdr(i);
         ---
         FORALL i IN L_tab_rib_recv_dtl.first..L_tab_rib_recv_dtl.last
            insert into fm_rib_stg_receiving_detail values L_tab_rib_recv_dtl(i);
         ---
         for I in L_tab_rib_recv_hdr.first..L_tab_rib_recv_hdr.last
         loop
            ---
            if L_tab_rib_recv_hdr(I).location_type = 'W' and L_document_type = 'P' then
               if FM_SCHED_SUBMIT.ADDTOQ(O_error_message,
                                         I_schedule_no) = FALSE then
                  return FALSE;
               end if;
            else
               if L_document_type = 'P' or L_requisition_type = C_REP_TYPE or (L_requisition_type in (C_TSF_TYPE, C_IC_TYPE) and L_count > 0)then
                  L_message_type := 'asnoutcre';
               elsif L_document_type in ('A','T','D') and L_requisition_type != C_REP_TYPE then
                     L_message_type := 'asnoutmod';
               end if;
               if L_document_type = 'P' then               
                  for L_index1 in L_requisition_tab.first..L_requisition_tab.last
                  loop
                     if FM_SECONDARY_ASNOUT.ADDTOQ (O_error_message,
                                                    I_schedule_no,
                                                    L_requisition_tab(L_index1),
                                                    L_message_type) = FALSE then
                        return FALSE;
                     end if;
                  end loop;
               else
                  if FM_SECONDARY_ASNOUT.ADDTOQ (O_error_message,
                                                 I_schedule_no,
                                                 NULL,
                                                 L_message_type) = FALSE then
                        return FALSE;
                  end if;
               end if;
            end if;
            ---
         end loop;
         ---
         L_tab_rib_recv_hdr.delete;
         L_tab_rib_recv_dtl.delete;
      end if;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1,254),
                                               L_program,
                                               to_char(SQLCODE));
         ---
         if C_GET_DOC_LINES%ISOPEN then
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_DOC_LINES','FM_FISCAL_DOC_HEADER',L_key);
            close C_GET_DOC_LINES;
         end if;
         ---
         if C_GET_PHY_WH%ISOPEN then
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_PHY_WH','WH',L_key);
            close C_GET_PHY_WH;
         end if;
         ---
         if C_GET_REQUISITION_NO%ISOPEN then
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_REQUISITION_NO','FM_RECEIVING_DETAIL',L_key);
            close C_GET_REQUISITION_NO;
         end if;
         ---
         if C_GET_CARTON%ISOPEN then
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_CARTON','FM_STG_CTN_SEQ',L_key);
            close C_GET_CARTON;
         end if;
         ---
         if C_GET_TSF_CARTON%ISOPEN then
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_CARTON','SHIPSKU',L_key);
            close C_GET_TSF_CARTON;
         end if;
         ---
         if C_GET_ASN%ISOPEN then
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_ASN','FM_STG_ASN_SEQ',L_key);
            close C_GET_ASN;
         end if;
         ---
         return FALSE;
   END POPULATE;
----------------------------------------------------------------------------------
   FUNCTION POPULATE_STG_TABLES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_status         IN OUT FM_SCHEDULE.STATUS%TYPE,
                                I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program     VARCHAR2(100) := 'FM_SUBMIT_SQL.POPULATE_STG_TABLES';
   ---
   L_status      FM_SCHEDULE.STATUS%TYPE;
   ---
   BEGIN
   ---
      L_status := O_status;
   ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                                L_program,
                                                NULL);
         return FALSE;
      end if;
      ---
      if FM_SUBMIT_SQL.CLEAR_SCHEDULE_ERRORS(O_error_message,
                                             I_schedule_no,
                                             L_program) = FALSE then
         return FALSE;
      end if;
      ---
      SAVEPOINT before_populate;
      ---
      if FM_SUBMIT_SQL.POPULATE(O_error_message,
                                I_schedule_no) = FALSE then
         L_status := C_ERROR_STATUS;
      else
         L_status := C_SUBMIT_STATUS;
      end if;
      ---
      if L_status = C_ERROR_STATUS then
         rollback to before_populate;
         ---
      --It is not possible to identify which document caused the error,
      -- because it will process all documents at the same time.
      --Therefore it will update the schedule and all documents to 'E'rror status.
      ---
         if WRITE_SCHEDULE_ERRORS(O_error_message,
                                  I_schedule_no,
                                  L_program) = FALSE then
            return FALSE;
         end if;
      ---
      end if;
   ---
      O_status := L_status;
      return TRUE;
   ---
   EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      ---
      rollback to before_populate;
      return FALSE;
   ---
   END POPULATE_STG_TABLES;
   -----------------------------------------------------------------------------
   FUNCTION SUBMIT_SCHED(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_status        IN OUT FM_SCHEDULE.STATUS%TYPE,
                         I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      ---
      L_program         VARCHAR2(50) := 'FM_SUBMIT_SQL.SUBMIT_SCHED';
      L_key             VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_submit_status   VARCHAR2(1)  := NULL;
      L_submit          VARCHAR2(1)  := 'Y';
      L_dummy           VARCHAR2(1);
      L_cnt_unexpc_item NUMBER;
      L_fiscal_doc_id   FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
      L_status_e          VARCHAR2(1) := 'E';
      L_status_w          VARCHAR2(1) := 'W';
      L_status_v          VARCHAR2(1) := 'V';
      L_unexpected_item   VARCHAR2(1)  := 'N';
      ---
      cursor C_FM_FISCAL_DOC_HEADER is
         select fdd.fiscal_doc_id, fdd.location_id, fdd.location_type
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_status_v;
      ---
      R_fiscal_doc_header  C_FM_FISCAL_DOC_HEADER%ROWTYPE;
      ---
      cursor C_EXISTS_ERROR is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_status_e;
      ---
      cursor C_NOT_ALL_FISCAL_DOC_MATCHED is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_status_w;
      ---
      cursor C_GET_DOC_ID is
         select fdd.fiscal_doc_id
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_status_v;
     ---
     cursor C_UNEXPC_ITEM_ONLY(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
         select count(1)
           from fm_fiscal_doc_header fdh,
                fm_fiscal_doc_detail fdd
          where fdh.fiscal_doc_id = fdd.fiscal_doc_id
            and fdh.status = L_status_v
            and fdd.unexpected_item = L_unexpected_item
            and fdh.fiscal_doc_id = P_fiscal_doc_id;
---
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_EXISTS_ERROR;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_EXISTS_ERROR into L_dummy;
      if C_EXISTS_ERROR%FOUND then
         O_status := 'E';
         SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
         close C_EXISTS_ERROR;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_EXISTS_ERROR;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_NOT_ALL_FISCAL_DOC_MATCHED',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_NOT_ALL_FISCAL_DOC_MATCHED;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_NOT_ALL_FISCAL_DOC_MATCHED',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_NOT_ALL_FISCAL_DOC_MATCHED into L_dummy;
      if C_NOT_ALL_FISCAL_DOC_MATCHED%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NOT_ALL_FISCAL_DOC_VM',
                                               NULL,
                                               NULL,
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE',
                       'C_NOT_ALL_FISCAL_DOC_MATCHED',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
         close C_NOT_ALL_FISCAL_DOC_MATCHED;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_NOT_ALL_FISCAL_DOC_MATCHED',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_NOT_ALL_FISCAL_DOC_MATCHED;
      --
      SQL_LIB.SET_MARK('OPEN', 'C_GET_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_GET_DOC_ID;
      ---
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_GET_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
         fetch C_GET_DOC_ID  into L_fiscal_doc_id;
         EXIT when C_GET_DOC_ID%NOTFOUND;
         ---
            ---
            SQL_LIB.SET_MARK('OPEN', 'C_UNEXPC_ITEM_ONLY', 'FM_FISCAL_DOC_HEADER', L_key);
            open C_UNEXPC_ITEM_ONLY(L_fiscal_doc_id);
            ---
            SQL_LIB.SET_MARK('FETCH', 'C_UNEXPC_ITEM_ONLY', 'FM_FISCAL_DOC_HEADER', L_key);
            fetch C_UNEXPC_ITEM_ONLY into L_cnt_unexpc_item;
               ---
               if L_cnt_unexpc_item = 0 then
                  O_error_message := SQL_LIB.CREATE_MSG('ORFM_SUB_UNEXPC_ITEM', 'I_schedule_no', I_schedule_no, NULL);
                  SQL_LIB.SET_MARK('CLOSE', 'C_UNEXPC_ITEM_ONLY', 'FM_FISCAL_DOC_HEADER', L_key);
                  close C_UNEXPC_ITEM_ONLY;
                  SQL_LIB.SET_MARK('CLOSE', 'C_GET_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
                  close C_GET_DOC_ID;

                  return FALSE;
               end if;
               ---
            SQL_LIB.SET_MARK('CLOSE', 'C_UNEXPC_ITEM_ONLY', 'FM_FISCAL_DOC_HEADER', L_key);
            close C_UNEXPC_ITEM_ONLY;
            ---
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_GET_DOC_ID;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FM_FISCAL_DOC_HEADER;
      ---
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_FM_FISCAL_DOC_HEADER',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
               fetch C_FM_FISCAL_DOC_HEADER
                into R_fiscal_doc_header ;
           EXIT when C_FM_FISCAL_DOC_HEADER%NOTFOUND;
         ---
         if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                         R_fiscal_doc_header .location_id,
                                                         R_fiscal_doc_header .location_type,
                                                         I_schedule_no,
                                                         R_fiscal_doc_header .fiscal_doc_id,
                                                         C_SUBMIT_STATUS) = FALSE then
            return FALSE;
         end if;
         ---
      END LOOP;
      ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_FM_FISCAL_DOC_HEADER',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         close C_FM_FISCAL_DOC_HEADER;
      ---
      --- Check is exists any NF with error status
      SQL_LIB.SET_MARK('OPEN',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_EXISTS_ERROR;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_EXISTS_ERROR into L_dummy;
      if C_EXISTS_ERROR%FOUND then
         O_status := C_ERROR_STATUS;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_EXISTS_ERROR;
      ---
      if GET_SUBMIT_STATUS(O_error_message,
                           L_submit_status,
                           I_schedule_no) = FALSE then
         return FALSE;
      end if;
      O_status := C_SUBMIT_STATUS;
      ---
      if L_submit_status = 'N' then
         if POPULATE_STG_TABLES(O_error_message,
                                O_status,
                                I_schedule_no) = FALSE then
            return FALSE;
         end if;
         if O_status = C_SUBMIT_STATUS then
            if UPD_SUBMIT_STATUS(O_error_message,
                                 L_submit,
                                 I_schedule_no) = FALSE then
               O_status := C_ERROR_STATUS;
               return FALSE;
            end if;
         else
            return FALSE;
         end if;
      end if;

      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_FM_FISCAL_DOC_HEADER%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FM_FISCAL_DOC_HEADER',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FM_FISCAL_DOC_HEADER;
         end if;
         ---
         if C_EXISTS_ERROR%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_ERROR',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_ERROR;
         end if;
         ---
         if C_NOT_ALL_FISCAL_DOC_MATCHED%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_NOT_ALL_FISCAL_DOC_MATCHED',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_NOT_ALL_FISCAL_DOC_MATCHED;
         end if;
         ---
         return FALSE;
   END SUBMIT_SCHED;
   -----------------------------------------------------------------------------
END FM_SUBMIT_SQL;
/