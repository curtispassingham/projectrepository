CREATE OR REPLACE PACKAGE BODY FM_RECEIVED_QTY_SQL is 
-----------------------------------------------------------------
FUNCTION CREATE_FOR_CORRECTION_DOC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'FM_RECEIVED_QTY_SQL.CREATE_FOR_CORRECTION_DOC';

   cursor C_V_FM_RECEIVED_QTY is
      select *
        from v_fm_received_qty
       where fiscal_doc_id = I_fiscal_doc_id
         and status = 'R';

   L_rec_recv_qty             V_FM_RECEIVED_QTY%ROWTYPE;
   L_return_nf_qty            FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_unacc_return_qty         FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_correction_qty           FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   ---
   L_description    FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_number_value   FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value   FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value     FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   ---
BEGIN
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_description,
                                         L_number_value,
                                         L_string_value,
                                         L_date_value,
                                         'CHAR',
                                         'QTY_RESOLUTION_RULE') = FALSE then
      return FALSE;
   end if;
   ---
   
   if (L_string_value = 'PO') then
      SQL_LIB.SET_MARK('OPEN',
                       'C_V_FM_RECEIVED_QTY',
                       'V_FM_RECEIVED_QTY',
                       ' I_fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id));
      open C_V_FM_RECEIVED_QTY;
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_V_FM_RECEIVED_QTY',
                          'V_FM_RECEIVED_QTY',
                          ' I_fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id));
         fetch C_V_FM_RECEIVED_QTY into L_rec_recv_qty;
         EXIT when C_V_FM_RECEIVED_QTY%NOTFOUND;
         
         if (L_rec_recv_qty.total_recv_qty > L_rec_recv_qty.appt_qty and
             L_rec_recv_qty.nf_qty > L_rec_recv_qty.appt_qty and
             ((L_rec_recv_qty.total_recv_qty - L_rec_recv_qty.appt_qty) = 
              (L_rec_recv_qty.nf_qty - L_rec_recv_qty.appt_qty) or
              (L_rec_recv_qty.total_recv_qty - L_rec_recv_qty.appt_qty) > 
              (L_rec_recv_qty.nf_qty - L_rec_recv_qty.appt_qty)
             )) then
            L_return_nf_qty := (L_rec_recv_qty.nf_qty - L_rec_recv_qty.appt_qty); 
            /*if create_correction_doc(O_error_message,
                                     L_rec_recv_qty,
                                     'RN',
                                     L_return_nf_qty) = FALSE then
               return FALSE;
            end if;*/
         end if;
         
         if (L_rec_recv_qty.total_recv_qty > L_rec_recv_qty.appt_qty and
             L_rec_recv_qty.nf_qty > L_rec_recv_qty.appt_qty and
             (L_rec_recv_qty.total_recv_qty - L_rec_recv_qty.appt_qty) < 
             (L_rec_recv_qty.nf_qty - L_rec_recv_qty.appt_qty)
            ) or
            (L_rec_recv_qty.total_recv_qty = L_rec_recv_qty.appt_qty and
             L_rec_recv_qty.nf_qty > L_rec_recv_qty.appt_qty and
             (L_rec_recv_qty.total_recv_qty - L_rec_recv_qty.appt_qty) < 
             (L_rec_recv_qty.nf_qty - L_rec_recv_qty.appt_qty)
            ) or
            (L_rec_recv_qty.total_recv_qty < L_rec_recv_qty.appt_qty and
             ((L_rec_recv_qty.nf_qty > L_rec_recv_qty.appt_qty) or
              (L_rec_recv_qty.nf_qty = L_rec_recv_qty.appt_qty)
             )
            ) then
            L_correction_qty := (L_rec_recv_qty.nf_qty - L_rec_recv_qty.total_recv_qty);
            /*if create_correction_doc(O_error_message,
                                     L_rec_recv_qty,
                                     'CLQ',
                                     L_correction_qty) = FALSE then
               return FALSE;
            end if;*/
         end if;
         
         if (L_rec_recv_qty.total_recv_qty > L_rec_recv_qty.appt_qty and
             L_rec_recv_qty.nf_qty > L_rec_recv_qty.appt_qty and
             ((L_rec_recv_qty.total_recv_qty - L_rec_recv_qty.appt_qty) < 
              (L_rec_recv_qty.nf_qty - L_rec_recv_qty.appt_qty)
             )
            ) then
            L_return_nf_qty := (L_rec_recv_qty.total_recv_qty - L_rec_recv_qty.appt_qty); 
            /*if create_correction_doc(O_error_message,
                                     L_rec_recv_qty,
                                     'RN',
                                     L_return_nf_qty) = FALSE then
               return FALSE;
            end if;*/
         end if;
         
         if (L_rec_recv_qty.total_recv_qty > L_rec_recv_qty.appt_qty and
             (
              (
               (L_rec_recv_qty.nf_qty > L_rec_recv_qty.appt_qty) and 
               ((L_rec_recv_qty.total_recv_qty - L_rec_recv_qty.appt_qty) > 
                (L_rec_recv_qty.nf_qty - L_rec_recv_qty.appt_qty)
               ) 
              ) or
              (
               (L_rec_recv_qty.nf_qty = L_rec_recv_qty.appt_qty)
              )
             )
            ) then 
            L_unacc_return_qty := (L_rec_recv_qty.total_recv_qty - L_rec_recv_qty.nf_qty);
            /*if create_correction_doc(O_error_message,
                                     L_rec_recv_qty,
                                     'MWN',
                                     L_unacc_return_qty) = FALSE then
               return FALSE;
            end if;*/
         end if;
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_V_FM_RECEIVED_QTY',
                       'V_FM_RECEIVED_QTY',
                       ' I_fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id));
      close C_V_FM_RECEIVED_QTY;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_V_FM_RECEIVED_QTY%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_V_FM_RECEIVED_QTY',
                          'V_FM_RECEIVED_QTY',
                          ' I_fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id));
         close C_V_FM_RECEIVED_QTY;
      end if;
      ---
      return FALSE;
END CREATE_FOR_CORRECTION_DOC;
--------------------------------------------------------------------------------------------------
/*
FUNCTION CREATE_CORRECTION_DOC(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_v_fm_received_qty  IN      V_FM_RECEIVED_QTY%ROWTYPE,
                               I_action_type        IN      FM_CORRECTION_DOC.ACTION_TYPE%TYPE,
                               I_action_value       IN      FM_CORRECTION_DOC.ACTION_VALUE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'FM_RECEIVED_QTY_SQL.CREATE_CORRECTION_DOC';

BEGIN
   if (I_ACTION_TYPE in ('CLQ','RN','MWN') ) then
      insert into FM_CORRECTION_DOC
                 (SEQ_NO,
                  SCHEDULE_NO,            
                  FISCAL_DOC_ID,          
                  REQUISITION_TYPE,       
                  REQUISITION_NO,         
                  ITEM,                   
                  ACTION_TYPE,            
                  ACTION_VALUE,           
                  STATUS,                 
                  CREATE_DATETIME,        
                  CREATE_ID,              
                  LAST_UPDATE_DATETIME,
                  LAST_UPDATE_ID         
                 )
         values  (
                  fm_correction_doc_seq.nextval,
                  I_v_fm_received_qty.SCHEDULE_NO,            
                  I_v_fm_received_qty.FISCAL_DOC_ID,          
                  I_v_fm_received_qty.REQUISITION_TYPE,       
                  I_v_fm_received_qty.REQUISITION_NO,         
                  I_v_fm_received_qty.ITEM,   
                  I_ACTION_TYPE,           --CLQ,RN,MWN     
                  I_ACTION_VALUE,
                  I_v_fm_received_qty.STATUS, 
                  SYSDATE,        
                  USER,              
                  SYSDATE,
                  USER
                 );     
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CREATE_CORRECTION_DOC;*/
------------------------------------------------------------------------------------------------
END FM_RECEIVED_QTY_SQL;
/