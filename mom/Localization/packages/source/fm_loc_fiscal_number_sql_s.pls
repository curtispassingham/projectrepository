CREATE OR REPLACE PACKAGE FM_LOC_FISCAL_NUMBER_SQL is
/*
   Purpose: Creates the packge specification that holds all functions
            related to the table FM_LOC_FISCAL_DOC_NUMBER_SQL.
*/
---------------------------------------------------------------------------------
-- Function Name: NEXT_NUMBER
-- Purpose: This function gets the next fiscal document number avaliable for
--          for a specific location.
----------------------------------------------------------------------------------
FUNCTION NEXT_NUMBER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_fiscal_doc_no      IN OUT FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE,
                     O_series_no          IN OUT FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE,
                     O_subseries_no       IN OUT FM_LOC_FISCAL_NUMBER.SUBSERIES_NO%TYPE,
                     I_location_type      IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                     I_location_id        IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                     I_issue_date         IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                     I_type_id            IN     FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CHK_LOC_FISCAL_NO(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists                    IN OUT BOOLEAN,
                           I_location_type             IN     FM_LOC_FISCAL_NUMBER.LOCATION_TYPE%TYPE,
                           I_location_id               IN     FM_LOC_FISCAL_NUMBER.LOCATION_ID%TYPE,
                           I_series_no                 IN     FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE,
                           I_type_id                   IN     FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------   
FUNCTION ROLLBACK_FISCAL_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_no  IN     FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE,
                            I_location_type  IN     FM_LOC_FISCAL_NUMBER.LOCATION_TYPE%TYPE,
                            I_location_id    IN     FM_LOC_FISCAL_NUMBER.LOCATION_ID%TYPE,
                            I_type_id        IN     FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE,
                            I_series_no      IN     FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
END FM_LOC_FISCAL_NUMBER_SQL;
/
