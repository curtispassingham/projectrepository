create or replace PACKAGE FM_SPED_SQL AS

-------------------------------------------------------------------------------
--Function Name: FM_LEGAL_MESSAGE
--Purpose:       This function get all the legal message associated for that NF
--               id and map it as a single column
-------------------------------------------------------------------------------

FUNCTION FM_LEGAL_MESSAGE( I_in IN fm_legal_msg_type)
 RETURN VARCHAR2;

-------------------------------------------------------------------------------
--Function Name: FM_SPED_HEADER
--Purpose:       This function fetches all the header informations from
--               fm_fiscal_doc_header and fm_schedule table. Based on the NF id
--               the total_value and tax_basis are calculated for each vat_code
--               All these values are inserted into fm_sped_fiscal_doc_header
--               table.
-------------------------------------------------------------------------------

FUNCTION FM_SPED_HEADER(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_last_update_datetime IN     VARCHAR2,
                        I_sped_last_run_date   IN     VARCHAR2)
    RETURN BOOLEAN;

-------------------------------------------------------------------------------
--Function Name: FM_SPED_DETAIL
--Purpose:       This function fetches all the detail informations from
--               fm_fiscal_doc_detail, fm_fiscal_doc_header and fm_schedule
--               table. Based on the NF id the total_value, tax_basis and
--               percentage_rate are calculated for each vat_code. All these
--               values are inserted into fm_sped_fiscal_doc_header table.
-------------------------------------------------------------------------------

FUNCTION FM_SPED_DETAIL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_last_update_datetime IN     VARCHAR2,
                        I_sped_last_run_date   IN     VARCHAR2)
    RETURN BOOLEAN;

-------------------------------------------------------------------------------
--Function Name: GET_SPED_INFO
--Purpose:       This function fetches sped last run date.It calls the function
--               FM_SPED_HEADER and FM_SPED_DETAIL with sped_last_run_date and
--               sysdate as input parameters. Once both the function are sucess
--               then it will update the sped last run date as sysdate.
-------------------------------------------------------------------------------

FUNCTION GET_SPED_INFO(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_last_update_datetime IN     VARCHAR2)
    RETURN BOOLEAN;

END FM_SPED_SQL;
/