create or replace PACKAGE BODY FM_ERROR_LOG_VAL_SQL is

   -----------------------------------------------------------------------------------
   FUNCTION LOV_FISCAL(O_error_message IN OUT VARCHAR2,
                       O_query         IN OUT VARCHAR2,
                       I_fiscal_doc_no IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                       I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                       I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(70) := 'ORFM_ERROR_LOG_SQL.LOV_FISCAL';
      ---
   BEGIN
      ---
      O_query := 'select DISTINCT e.fiscal_doc_id ' ||
                 'from fm_error_log e ';
      ---
      if I_fiscal_doc_no is NOT NULL then
         O_query := O_query || ' , fm_fiscal_doc_header h ' ||
                    ' where h.fiscal_doc_id = e.fiscal_doc_id ' ||
                    ' and fiscal_doc_no = ' || I_fiscal_doc_no||
                    ' and ';
      else
         O_query := O_query || ' where ';
      end if;
      ---
      O_query := O_query ||
                 ' ((exists (select 1
                               from fm_fiscal_doc_header a
                              where a.fiscal_doc_id = e.fiscal_doc_id
                                and a.location_id = '||I_location||
                 '              and a.location_type = '''||I_loc_type||''')) or
                    (exists (select 1
                               from fm_edi_doc_header a
                              where a.edi_doc_id = e.fiscal_doc_id
                                and a.location_id = '||I_location||
                 '              and a.location_type = '''||I_loc_type||''')))'||
                 ' order by 1';

      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END LOV_FISCAL;
   -----------------------------------------------------------------------------------
   FUNCTION LOV_PROGRAM(O_error_message IN OUT VARCHAR2,
                        O_query         IN OUT VARCHAR2) return BOOLEAN is
      ---
      L_program VARCHAR2(70) := 'ORFM_ERROR_LOG_SQL.LOV_PROGRAM';
      ---
   BEGIN
      ---
      O_query := 'select DISTINCT program ' ||
                 'from fm_error_log ' ||
                 'order by 1';
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END LOV_PROGRAM;
   -----------------------------------------------------------------------------------

   FUNCTION SET_WHERE_CLAUSE(O_error_message IN OUT VARCHAR2,
                             O_where         IN OUT VARCHAR2,
                             I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE,
                             I_err_type      IN FM_ERROR_LOG.ERR_TYPE%TYPE,
                             I_err_status    IN FM_ERROR_LOG.ERR_STATUS%TYPE,
                             I_err_date      IN FM_ERROR_LOG.ERR_DATE%TYPE,
                             I_fiscal_doc_no IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                             I_schedule_no   IN FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                             I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                             I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                             I_source_type   IN FM_ERROR_LOG.ERR_SOURCE_TYPE%TYPE)
      return BOOLEAN is

      L_program VARCHAR2(100) := 'ORFM_ERROR_LOG_SQL.SET_WHERE_CLAUSE';
      L_text    VARCHAR2(2000) := NULL;
   BEGIN
      if I_location is NULL or
         I_loc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               L_program,
                                               'I_location',
                                               'NULL');
         return FALSE;
      end if;
      if I_fiscal_doc_id is NOT NULL then
         L_text := L_text || ' fiscal_doc_id = ' || I_fiscal_doc_id ||
                   ' and ';
      end if;
      if I_err_type is NOT NULL then
         L_text := L_text || ' err_type = ''' || I_err_type || ''' and ';
      end if;
      if I_err_status is NOT NULL then
         L_text := L_text || ' err_status = ''' || I_err_status ||
                   ''' and ';
      end if;
      if I_err_date is NOT NULL then
         L_text := L_text || ' TRUNC(err_date) = ''' || I_err_date ||
                   ''' and ';
      end if;
      if I_source_type is NOT NULL then
         L_text := L_text || ' err_source_type = ''' || I_source_type ||
                   ''' and ';
      end if;
      if I_fiscal_doc_no is NOT NULL then
         L_text := L_text ||
                   ' exists (select 1
                               from fm_fiscal_doc_header h
                              where h.fiscal_doc_id = fm_error_log.fiscal_doc_id
                                and h.fiscal_doc_no = ' ||
                   I_fiscal_doc_no || ') and ';

      end if;
      if I_schedule_no is NOT NULL then
         L_text := L_text ||
                   ' exists (select 1
                               from fm_fiscal_doc_header h
                              where h.fiscal_doc_id = fm_error_log.fiscal_doc_id
                                and h.schedule_no = ' ||
                   I_schedule_no || ') and ';

      end if;
      ---
      L_text := L_text||
                 ' (
                   (exists (select 1
                              from fm_fiscal_doc_header a
                             where a.fiscal_doc_id = fm_error_log.fiscal_doc_id
                               and a.location_id   = '  ||I_location||
                             ' and a.location_type = '''||I_loc_type||''')) or
                   (exists (select 1
                              from fm_edi_doc_header a
                             where a.edi_doc_id = fm_error_log.fiscal_doc_id
                               and a.location_id   = '  ||I_location||
                             ' and a.location_type = '''||I_loc_type||''')) or
                   (exists (select 1
                              from fm_rma_head a
                             where a.rma_id = fm_error_log.fiscal_doc_id
                               and a.location   = '  ||I_location||
                             ' and a.loc_type = '''||I_loc_type||'''))
                   ) and ';

      ---
      if L_text is NOT NULL then
         L_text := SUBSTR(L_text, 1, LENGTH(L_text) - 5);
      end if;

      O_where := L_text;
      return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END SET_WHERE_CLAUSE;

   -----------------------------------------------------------------------------------
   FUNCTION SET_WHERE_CLAUSE(O_error_message IN OUT VARCHAR2,
                             O_where         IN OUT VARCHAR2,
                             I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                             I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      return BOOLEAN is
      
      L_program VARCHAR2(100) := 'ORFM_ERROR_LOG_SQL.SET_WHERE_CLAUSE';
      L_text    VARCHAR2(2000) := NULL;
   BEGIN
      if I_location is NULL or
         I_loc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               L_program,
                                               'I_location',
                                               'NULL');
         return FALSE;
      end if; 
      L_text := L_text || ' fiscal_doc_id in (select fiscal_doc_id from fm_fiscal_doc_header where location_type = ''' || I_loc_type || '''' || ' and location_id = '  ||  I_location || ')';
      O_where := L_text;
      return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END SET_WHERE_CLAUSE;    
   -----------------------------------------------------------------------------------------
      
      
   FUNCTION LOV_FISCAL_NO(O_error_message IN OUT VARCHAR2,
                          O_query         IN OUT VARCHAR2,
                          I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                          I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(70) := 'ORFM_ERROR_LOG_SQL.LOV_FISCAL_NO';
      ---
   BEGIN
      ---
      O_query := 'select DISTINCT fiscal_doc_no ' ||
                 '  from fm_fiscal_doc_header '||
                 ' where location_id = '||I_location||
                 '   and location_type = '''||I_loc_type||'''';
      ---
      if I_fiscal_doc_id is NOT NULL then
         O_query := O_query || ' and fiscal_doc_id = ' || I_fiscal_doc_id;
      end if;

      O_query := O_query || ' order by 1';

      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END LOV_FISCAL_NO;
   ----------------------------------------------------------------------
   FUNCTION GET_FISCAL_DOC_NO(O_error_message IN OUT VARCHAR2,
                              O_fiscal_doc_no IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                              O_fiscal_doc_type IN OUT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                              I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'ORFM_ERROR_LOG_SQL.GET_FISCAL_DOC_NO';
      ---
      cursor C_FISCAL is
         select a.fiscal_doc_no, a.requisition_type
           from fm_fiscal_doc_header a
          where a.fiscal_doc_id = I_fiscal_doc_id;
      ---
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      open C_FISCAL;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      fetch C_FISCAL into O_fiscal_doc_no, O_fiscal_doc_type;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      close C_FISCAL;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END GET_FISCAL_DOC_NO;
   -----------------------------------------------------------------------------------
   FUNCTION LOV_SCHEDULE_NO(O_error_message IN OUT VARCHAR2,
                            O_query         IN OUT VARCHAR2,
                            I_sched_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE,
                            I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                            I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      return BOOLEAN is
      L_program VARCHAR2(50) := 'ORFM_ERROR_LOG_SQL.LOV_SCHEDULE_NO';
   BEGIN
      ---
      if I_sched_no is NOT NULL then
         O_query := 'select DISTINCT s.schedule_no
                    from fm_schedule s
                       , fm_fiscal_doc_header h
                       , fm_error_log e
                   where s.schedule_no = h.schedule_no
                     and s.schedule_no = ' || I_sched_no || '
                     and e.fiscal_doc_id = h.fiscal_doc_id';
      else
         O_query := 'select DISTINCT s.schedule_no
                    from fm_schedule s
                       , fm_fiscal_doc_header h
                       , fm_error_log e
                   where s.schedule_no = h.schedule_no
                     and e.fiscal_doc_id = h.fiscal_doc_id';
      end if;
      O_query := O_query ||
                   ' and h.location_id   = '  ||I_location||
                   ' and h.location_type = '''||I_loc_type||''''||
                   ' order by s.schedule_no ';
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END LOV_SCHEDULE_NO;
   -------------------------------------------------------------------------
   FUNCTION GET_SCHEDULE_NO(O_error_message IN OUT VARCHAR2,
                            O_schedule_no   IN OUT FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                            I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'ORFM_ERROR_LOG_SQL.GET_SCHEDULE_NO';
      ---
      cursor C_SCHED is
         select a.schedule_no
           from fm_fiscal_doc_header a
          where a.fiscal_doc_id = I_fiscal_doc_id;
      ---
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_SCHED',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      open C_SCHED;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_SCHED',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      fetch C_SCHED
         into O_schedule_no;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SCHED',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      close C_SCHED;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END GET_SCHEDULE_NO;
   -----------------------------------------------------------------------------------
   FUNCTION EXIST_FISCAL_DOC_ID(O_error_message IN OUT VARCHAR2,
                                O_exist         IN OUT BOOLEAN,
                                I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'ORFM_ERROR_LOG_SQL.EXIST_FISCAL_DOC_ID';
      L_dummy   VARCHAR2(1);
      ---
      cursor C_FISCAL is
         select 'x'
           from fm_fiscal_doc_header a
          where a.fiscal_doc_id = I_fiscal_doc_id;
      ---
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      open C_FISCAL;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      fetch C_FISCAL
         into L_dummy;
      O_exist := C_FISCAL%FOUND;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id = ' || I_fiscal_doc_id);
      close C_FISCAL;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END EXIST_FISCAL_DOC_ID;
   -----------------------------------------------------------------------------------
end FM_ERROR_LOG_VAL_SQL;
/