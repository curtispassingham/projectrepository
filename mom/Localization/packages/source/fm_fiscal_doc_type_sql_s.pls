CREATE or REPLACE PACKAGE FM_FISCAL_DOC_TYPE_SQL as
----------------------------------------------------------------------------------
-- Function Name: EXISTS
-- Purpose:       This function will check if exists TYPE_ID on table.
----------------------------------------------------------------------------------
FUNCTION EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                O_exists         IN OUT BOOLEAN,
                O_type_desc      IN OUT FM_FISCAL_DOC_TYPE.TYPE_DESC%TYPE,
                I_type_id        IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------     
-- Function Name: EXIST_CHILD
-- Purpose:       This function will check if exists childs of table.
----------------------------------------------------------------------------------
FUNCTION EXIST_CHILD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists         IN OUT BOOLEAN,
                     I_type_id        IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------     
-- Function Name: EXIST_CHILD_HEADER
-- Purpose:       This function will check if exists childs on Fiscal_doc_header table.
----------------------------------------------------------------------------------
FUNCTION EXIST_CHILD_HEADER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         IN OUT BOOLEAN,
                            I_type_id        IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: GET_SUBSERIES_NO_IND
-- Purpose:       This function get subseries_no_ind information.
----------------------------------------------------------------------------------
FUNCTION GET_SUBSERIES_NO_IND(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_subseries_no_ind IN OUT FM_FISCAL_DOC_TYPE.SUBSERIES_NO_IND%TYPE,
                              I_type_id          IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------  
-- Function Name: GET_TYPE_DESC
-- Purpose:       This function get Type ID description.
----------------------------------------------------------------------------------
FUNCTION GET_TYPE_DESC(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_type_desc        IN OUT FM_FISCAL_DOC_TYPE.TYPE_DESC%TYPE,
                       I_type_id          IN     FM_FISCAL_DOC_TYPE.TYPE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------  
-- Function Name: LOV_TYPE_ID
-- Purpose:       List of values of Type Id
----------------------------------------------------------------------------------
FUNCTION LOV_TYPE_ID(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_query          IN OUT VARCHAR2)
return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_UTILIZATION
-- Purpose:       List of values of Utilizations
----------------------------------------------------------------------------------
FUNCTION LOV_UTILIZATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_query          IN OUT VARCHAR2)
return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for where clause of main block.
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where         IN OUT VARCHAR2,
                          I_type_id       IN NUMBER)
return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE_UTIL
-- Purpose:       This function returns the query for where clause of detail block.
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_UTIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_where              IN OUT VARCHAR2,
                               I_type_id            IN NUMBER)
return BOOLEAN;
----------------------------------------------------------------------------------
END FM_FISCAL_DOC_TYPE_SQL;
/

