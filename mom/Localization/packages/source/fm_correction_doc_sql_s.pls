CREATE or REPLACE PACKAGE FM_CORRECTION_DOC_SQL as
----------------------------------------------------------------------------------
-- Function Name: GEN_CORRECT_DOC_TAX
-- Purpose:       This function inserts a record into FM_CORRECTION_DOC and
--                FM_CORRECTION_TAX_DOC tables
----------------------------------------------------------------------------------
L_fetch_comp_taxes      BOOLEAN              := FALSE;
----------------------------------------------------------------------------------
FUNCTION GEN_CORRECT_DOC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_record             IN     FM_CORRECTION_DOC%ROWTYPE,
                         I_tax_code           IN     FM_CORRECTION_TAX_DOC.TAX_CODE%TYPE,
                         I_tax_basis          IN     FM_CORRECTION_TAX_DOC.TAX_BASIS%TYPE,
                         I_tax_amount         IN     FM_CORRECTION_TAX_DOC.TAX_AMOUNT%TYPE,
                         I_tax_rate           IN     FM_CORRECTION_TAX_DOC.TAX_RATE%TYPE,
                         I_modified_tax_basis IN     FM_CORRECTION_TAX_DOC.MODIFIED_TAX_BASIS%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: DEL_CORRECTION_DOC
-- Purpose:       This function deletes record from FM_CORRECTION_DOC and FM_CORRECTION_TAX_DOC table
----------------------------------------------------------------------------------
FUNCTION DEL_CORRECTION_DOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_line_id  IN     FM_CORRECTION_DOC.FISCAL_DOC_LINE_ID%TYPE,
                            I_action_type         IN     FM_CORRECTION_DOC.ACTION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CREATE_FOR_CORRECTION_DOC
-- Purpose:       This function inserts a record into FM_CORRECTION_DOC and
--                FM_CORRECTION_TAX_DOC tables
----------------------------------------------------------------------------------
FUNCTION CREATE_FOR_CORRECTION_DOC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists          IN OUT  BOOLEAN,
                                   I_fiscal_doc_id   IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                   I_schedule_no     IN      FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--    Name: CREATE_CORRECTION_DOC
-- Purpose: This function insert's correction record's into FM_CORRECTION_DOC .
------------------------------------------------------------------------------------------------
FUNCTION CREATE_CORRECTION_DOC(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_v_fm_received_qty      IN       V_FM_RECEIVED_QTY_CALC%ROWTYPE,
                               I_action_type            IN       FM_CORRECTION_DOC.ACTION_TYPE%TYPE,
                               I_action_value           IN       FM_CORRECTION_DOC.ACTION_VALUE%TYPE )
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION CREATE_FOR_CORRECTION_DOC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_schedule_no        IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------------------
END FM_CORRECTION_DOC_SQL;
/