CREATE OR REPLACE PACKAGE BODY L10N_BR_CNAE_SQL AS

------------------------------------------------------------------------------------
FUNCTION GET_CNAE_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cnae_desc       IN OUT   L10N_BR_CNAE_CODES.CNAE_DESC%TYPE,
                       I_cnae_code       IN       L10N_BR_CNAE_CODES.CNAE_CODE%TYPE)
RETURN BOOLEAN AS

   L_program     VARCHAR2(100) := 'L10N_BR_CNAE_SQL.GET_CNAE_DESC';
   L_cnae_desc   L10N_BR_CNAE_CODES.CNAE_DESC%TYPE;

  cursor C_CNAE_DESC is
      select cnae_desc description
        from l10n_br_cnae_codes
       where cnae_code = I_cnae_code;

BEGIN

   open C_CNAE_DESC;
   fetch C_CNAE_DESC into L_cnae_desc;
   close C_CNAE_DESC;

   if L_cnae_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CNAE_CODE', I_cnae_code);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_cnae_desc,
                                O_cnae_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CNAE_DESC;

------------------------------------------------------------------------------

FUNCTION CNAE_CODE_EXISTS(O_error_message  IN OUT   VARCHAR2,
                          O_exists         IN OUT   BOOLEAN,
                          I_entity         IN       L10N_BR_ENTITY_CNAE_CODES.KEY_VALUE_1%TYPE,
                          I_entity_2       IN       L10N_BR_ENTITY_CNAE_CODES.KEY_VALUE_2%TYPE,
                          I_entity_type    IN       L10N_BR_ENTITY_CNAE_CODES.MODULE%TYPE,
                          I_country        IN       L10N_BR_ENTITY_CNAE_CODES.COUNTRY_ID%TYPE,
                          I_cnae_code      IN       L10N_BR_ENTITY_CNAE_CODES.CNAE_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_BR_CNAE_SQL.CNAE_CODE_EXISTS';
   L_dummy   VARCHAR2(1)  := NULL;
   L_key     VARCHAR2(100) := 'entity: ' || I_entity ||
                              ',entity_2: ' || I_entity_2 ||
                              ',entity_type: ' || I_entity_type ||
                              ',country: ' || I_country ||
                              ',cnae_code: ' || I_cnae_code;

   cursor C_EXISTS is
      select 'x'
        from l10n_br_entity_cnae_codes
       where key_value_1  = I_entity
         and key_value_2  = I_entity_2
         and module       = I_entity_type
         and country_id   = I_country
         and cnae_code    = I_cnae_code;

BEGIN
   O_exists := FALSE;

   if I_entity is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_entity,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_entity_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_entity_type,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_country,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_cnae_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_cnae_code,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'l10n_br_entity_cnae_codes',
                    L_key);
   open C_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'l10n_br_entity_cnae_codes',
                    L_key);
   fetch C_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    'l10n_br_entity_cnae_codes',
                    L_key);
   close C_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CNAE_CODE_EXISTS;
------------------------------------------------------------------------------
FUNCTION GET_STORE_NAME( O_error_message IN OUT VARCHAR2,
                         I_store         IN     NUMBER,
                         O_store_name    IN OUT VARCHAR2)
      RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'L10N_BR_CNAE_SQL.GET_NAME';

   cursor C_STORE is
      select store_name
        from store
       where  store = I_store
    union all
      select store_name
        from store_add
       where store = I_store ;
BEGIN

      open C_STORE;
      fetch C_STORE into O_store_name;

      if C_STORE%NOTFOUND then
         close C_STORE;
         O_error_message := sql_lib.create_msg('INV_STORE'
                  ,null,null,null);
         RETURN FALSE;

      else
         close C_STORE;
         if LANGUAGE_SQL.TRANSLATE( O_store_name,
                  O_store_name,
                  O_error_message) = FALSE then

           return FALSE;
         end if;
         RETURN TRUE;
      end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
               SQLERRM,
               L_program,
               NULL);
      RETURN FALSE;

END GET_STORE_NAME;
------------------------------------------------------------------------------
END L10N_BR_CNAE_SQL;
/