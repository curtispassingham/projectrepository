CREATE or REPLACE PACKAGE FM_UTILIZATION_SQL is
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for where clause of block.
----------------------------------------------------------------------------------
FUNCTION P_SET_WHERE_CLAUSE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_where_clause       IN OUT VARCHAR2,
                            I_utilization        IN     VARCHAR2)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_UTILIZATION
-- Purpose:       List of values of utilization.
----------------------------------------------------------------------------------
FUNCTION LOV_UTILIZATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_query          IN OUT VARCHAR2,
                         I_where_clause   IN     VARCHAR2)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: P_SET_WHERE_CLAUSE_UTIL_REASON
-- Purpose:       This function returns the query for where clause of Util Reason block.
----------------------------------------------------------------------------------
FUNCTION P_SET_WHERE_CLAUSE_UTIL_REASON(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_where_clause       IN OUT VARCHAR2,
                                        I_utilization        IN     VARCHAR2)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: P_CREATE_FM_NOP
-- Purpose:       The function inserts the records into fm_nop table fetched from MTR
----------------------------------------------------------------------------------
FUNCTION P_CREATE_FM_NOP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: P_SET_WHERE_CLAUSE_DOC_PAR
-- Purpose: This function returns the query for where clause of Doc type
--          and Uitlization Parameter block.
----------------------------------------------------------------------------------
FUNCTION P_SET_WHERE_CLAUSE_DOC_PAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_where_clause       IN OUT VARCHAR2,
                                    I_utilization        IN     VARCHAR2)
return BOOLEAN;
----------------------------------------------------------------------------------
END FM_UTILIZATION_SQL;
/

