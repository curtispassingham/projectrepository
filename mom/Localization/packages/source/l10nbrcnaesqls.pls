CREATE OR REPLACE PACKAGE L10N_BR_CNAE_SQL AS

---------------------------------------------------------------------------------------------
-- Function Name: GET_CNAE_DESC
-- Purpose      : This function will get the cnae_desc from l10n_br_cnae_codes table
--                for the cnae_code parameter
---------------------------------------------------------------------------------------------
FUNCTION GET_CNAE_DESC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cnae_desc      IN OUT   L10N_BR_CNAE_CODES.CNAE_DESC%TYPE,
                       I_cnae_code      IN       L10N_BR_CNAE_CODES.CNAE_CODE%TYPE)
RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function Name: CNAE_CODE_EXISTS
-- Purpose      : Verify if tributary substitution exists.
---------------------------------------------------------------------------------------------
FUNCTION CNAE_CODE_EXISTS(O_error_message  IN OUT   VARCHAR2,
                          O_exists         IN OUT   BOOLEAN,
                          I_entity         IN       L10N_BR_ENTITY_CNAE_CODES.KEY_VALUE_1%TYPE,
                          I_entity_2       IN       L10N_BR_ENTITY_CNAE_CODES.KEY_VALUE_2%TYPE,
                          I_entity_type    IN       L10N_BR_ENTITY_CNAE_CODES.MODULE%TYPE,
                          I_country        IN       L10N_BR_ENTITY_CNAE_CODES.COUNTRY_ID%TYPE,
                          I_cnae_code      IN       L10N_BR_ENTITY_CNAE_CODES.CNAE_CODE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--- Function Name:  GET_STORE_NAME
--- Purpose:        Fetches the name of the store from store and store_add table.
--------------------------------------------------------------------
FUNCTION GET_STORE_NAME(O_error_message IN OUT VARCHAR2,
                        I_store         IN     NUMBER,
                        O_store_name    IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END L10N_BR_CNAE_SQL;
/