CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DOC_HEADER_SQL is
---------------------------------------------------------------------------------
FUNCTION CHECK_RMS_UPD_ERR(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT BOOLEAN,
                           I_fiscal_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_program     VARCHAR2(80) := 'FM_CORRECTION_DOC_SQL.CHECK_RMS_UPD_ERR';
   L_count       NUMBER(2)    := 0;
   L_key         VARCHAR2(80) := 'Fiscal Doc NO = '|| I_fiscal_doc_id;

   cursor C_CHECK_RMS_UPD_ERR is
     select count(*)
       from fm_fiscal_doc_header fdh
      where fdh.fiscal_doc_id = I_fiscal_doc_id
        and NVL(fdh.rms_upd_err_ind,'N') = 'Y';
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_RMS_UPD_ERR', 'FM_FISCAL_DOC_HEADER', L_key);
   OPEN C_CHECK_RMS_UPD_ERR;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_RMS_UPD_ERR', 'FM_FISCAL_DOC_HEADER', L_key);
   FETCH C_CHECK_RMS_UPD_ERR into L_count;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_RMS_UPD_ERR', 'FM_FISCAL_DOC_HEADER', L_key);
   CLOSE C_CHECK_RMS_UPD_ERR;

   if L_count = 0 then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
END CHECK_RMS_UPD_ERR;
--------------------------------------------------------------------------------
FUNCTION GET_NEXT_FISCAL_DOC_ID(O_error_message  IN OUT VARCHAR2,
                                O_fiscal_doc_id  IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_HEADER_SQL.GET_NEXT_FISCAL_DOC_ID';
   ---
   cursor C_SEQ_FISCAL_DOC_ID is
      select fm_fiscal_doc_id_seq.NEXTVAL seq_no
        from dual;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SEQ_FISCAL_DOC_ID', 'SEQ_FISCAL_DOC_ID', NULL);
   open C_SEQ_FISCAL_DOC_ID;
   SQL_LIB.SET_MARK('FETCH', 'C_SEQ_FISCAL_DOC_ID', 'SEQ_FISCAL_DOC_ID',NULL);
   fetch C_SEQ_FISCAL_DOC_ID into O_fiscal_doc_id;
   SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_FISCAL_DOC_ID', 'SEQ_FISCAL_DOC_ID',NULL);
   close C_SEQ_FISCAL_DOC_ID;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_SEQ_FISCAL_DOC_ID%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_FISCAL_DOC_ID', 'SEQ_FISCAL_DOC_ID',NULL);
         close C_SEQ_FISCAL_DOC_ID;
      end if;
      ---
      return FALSE;

END GET_NEXT_FISCAL_DOC_ID;
-----------------------------------------------------------------------------------
FUNCTION EXISTS_DETAIL(O_error_message  IN OUT VARCHAR2,
                       O_exist          IN OUT BOOLEAN,
                       I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER_SQL.EXISTS_DETAIL';
   L_cursor    VARCHAR2(100) := 'C_EXIST_DOC_DETAIL';
   L_table     VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL';
   L_key       VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_EXIST_DOC_DETAIL is
      select 'x'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_EXIST_DOC_DETAIL;
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_EXIST_DOC_DETAIL into L_dummy;
   ---
   O_exist := C_EXIST_DOC_DETAIL%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_EXIST_DOC_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_EXIST_DOC_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
         close C_EXIST_DOC_DETAIL;
      end if;
      ---
      return FALSE;
END EXISTS_DETAIL;
----------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message  IN OUT VARCHAR2,
                       I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                       I_status         IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE)
   return BOOLEAN is
   ---
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program      VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER';
   L_key          VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   ---
   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_LOCK_DOC_HEADER';
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_LOCK_DOC_HEADER;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_LOCK_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
   update fm_fiscal_doc_header fdh
      set fdh.status = I_status
     where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', L_table, L_key);
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', L_table, L_key);
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      return FALSE;
END UPDATE_STATUS;
----------------------------------------------------------------------------------
  FUNCTION EXISTS_FISCAL_NO(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                            I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                            I_fiscal_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                            I_fiscal_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
     return BOOLEAN is
     ---
     L_program   VARCHAR2(80) := 'FM_FISCAL_DOC_HEADER_SQL.EXISTS_FISCAL_NO';
     L_dummy     VARCHAR2(1);
     ---
     cursor C_FISCAL is
        select DISTINCT '1'
          from fm_fiscal_doc_header
         where fiscal_doc_no = I_fiscal_no
           and fiscal_doc_id = NVL(I_fiscal_id,fiscal_doc_id)
           and location_type = I_location_type
           and location_id   = I_location_id;
     ---
  BEGIN
     ---
     SQL_LIB.SET_MARK('OPEN','C_FISCAL','FM_FISCAL_DOC_HEADER','Fiscal_doc_no: '||I_fiscal_no);
     open C_FISCAL;
     ---
     SQL_LIB.SET_MARK('FETCH','C_FISCAL','FM_FISCAL_DOC_HEADER','Fiscal_doc_no: '||I_fiscal_no);
     fetch C_FISCAL into L_dummy;
     O_exists := C_FISCAL%FOUND;
     ---
     SQL_LIB.SET_MARK('CLOSE','C_FISCAL','FM_FISCAL_DOC_HEADER','Fiscal_doc_no: '||I_fiscal_no);
     close C_FISCAL;
     ---
     return TRUE;
     ---
  EXCEPTION
     when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
        return FALSE;

  END EXISTS_FISCAL_NO;
----------------------------------------------------------------------------------
FUNCTION EXISTS_PRINT_FISCAL_NO(O_error_message  IN OUT VARCHAR2,
                                O_exists         IN OUT BOOLEAN,
                                I_fiscal_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                I_sched_no       IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN is
     ---
     L_program   VARCHAR2(80) := 'FM_FISCAL_DOC_HEADER_SQL.EXISTS_PRINT_FISCAL_NO';
     L_dummy     VARCHAR2(1);
     ---
     cursor C_FISCAL is
        select '1'
          from fm_fiscal_doc_header
         where fiscal_doc_no = I_fiscal_no
           and schedule_no = I_sched_no;
     ---
  BEGIN
     ---
     SQL_LIB.SET_MARK('OPEN','C_FISCAL','FM_FISCAL_DOC_HEADER','Fiscal_doc_no: '||I_fiscal_no);
     open C_FISCAL;
     ---
     SQL_LIB.SET_MARK('FETCH','C_FISCAL','FM_FISCAL_DOC_HEADER','Fiscal_doc_no: '||I_fiscal_no);
     fetch C_FISCAL into L_dummy;
     O_exists := C_FISCAL%FOUND;
     ---
     SQL_LIB.SET_MARK('CLOSE','C_FISCAL','FM_FISCAL_DOC_HEADER','Fiscal_doc_no: '||I_fiscal_no);
     close C_FISCAL;
     ---
     return TRUE;
     ---
  EXCEPTION
     when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
        return FALSE;

  END EXISTS_PRINT_FISCAL_NO;
----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_HEADER_INFO(O_error_message     IN OUT VARCHAR2,
                                    O_fiscal_doc_header IN OUT FM_FISCAL_DOC_HEADER%ROWTYPE,
                                    I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER_SQL.GET_FISCAL_DOC_HEADER_INFO';
   L_cursor    VARCHAR2(100) := 'C_FM_FISCAL_DOC_HEADER';
   L_table     VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER';
   L_key       VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select *
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_FM_FISCAL_DOC_HEADER into O_fiscal_doc_header;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
         close C_FM_FISCAL_DOC_HEADER;
      end if;
      ---
      return FALSE;
END GET_FISCAL_DOC_HEADER_INFO;
----------------------------------------------------------------------------------
FUNCTION COPY_DOC_HEADER(O_error_message     IN OUT VARCHAR2,
                         O_new_fiscal_doc_id IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_new_schedule      IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                         I_location_id       IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                         I_location_type     IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                         I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_operation         IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER_SQL.COPY_DOC_HEADER';
   L_cursor    VARCHAR2(100) := 'C_FM_FISCAL_DOC_DETAIL';
   L_table     VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL';
   L_key       VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   L_dummy     VARCHAR2(1);

   C_DEFAULT_RURAL_PROD_DOC_TYPE  CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_RURAL_PROD_DOC_TYPE';
   C_DEFAULT_RURAL_PROD_UTIL      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_RURAL_PROD_UTILIZATION';
   ---
   C_INBOUND_TSF_DOC_TYPE         CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_INBOUND_TSF_DOC_TYPE';
   C_INBOUND_TSF_UTIL_ID          CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_INBOUND_TSF_UTIL_ID';
   C_INBOUND_IC_DOC_TYPE          CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_INBOUND_IC_DOC_TYPE';
   C_INBOUND_IC_UTIL_ID           CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_INBOUND_IC_UTIL_ID';
   C_INBOUND_REP_DOC_TYPE         CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_INBOUND_REP_DOC_TYPE';
   C_INBOUND_REP_UTIL_ID          CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_INBOUND_REP_UTIL_ID';

   ---
   C_NUMBER                       CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE := 'NUMBER';
   C_CHAR                         CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE := 'CHAR';
   C_TSF                          CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE :='TSF';

   ---
   L_requisition_type       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_req_type               FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_new_fiscal_doc_id      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_new_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_utilization            FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_type_id                FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_rural_prod_ind         V_BR_SUPS_FISCAL_CLASS.RURAL_PROD_IND%TYPE := NULL;
   L_ste                    FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE :='STE';
   L_found                  VARCHAR2(1);
   L_fiscal_doc_line_id     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;

   L_dummy_desc             FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_number           FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_dummy_string           FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_dummy_date             FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_mode_type              VARCHAR2(10) := 'EXIT';
   L_status                 VARCHAR2(1)  := 'A';
   L_module                 VARCHAR2(10)  := 'SUPP';
   L_exists                 BOOLEAN;
   ---
   cursor C_GET_REQUISITION_TYPE is
      select fdh.requisition_type
        from fm_fiscal_doc_header fdh,
             fm_schedule fs
       where fs.schedule_no = fdh.schedule_no
         and fs.mode_type = L_mode_type
         and fs.status in ('H','A')
         and fdh.fiscal_doc_id = I_fiscal_doc_id;

   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id,
             fdd.fiscal_doc_id,
             fdd.location_id,
             fdd.location_type,
             fdd.line_no,
             fdh.requisition_type,
             fdd.requisition_no,
             fdd.item,
             fdd.classification_id,
             fdd.quantity,
             fdd.unit_cost,
             fdd.total_cost,
             fdd.total_calc_cost,
             fdd.freight_cost,
             fdd.net_cost,
             fdd.create_datetime,
             fdd.create_id,
             fdd.last_update_datetime,
             fdd.last_update_id,
             fdd.fiscal_doc_id_ref,
             fdd.discount_type,
             fdd.discount_value,
             fdd.fiscal_doc_line_id_ref,
             fdd.unit_cost_with_disc,
             fdd.unexpected_item,
             fdd.entry_cfop,
             fdd.nf_cfop,
             fdd.other_expenses_cost,
             fdd.insurance_cost,
             fdd.qty_discrep_status,
             fdd.cost_discrep_status,
             fdd.tax_discrep_status,
             fdd.inconclusive_rules,
             fdd.appt_qty,
             fdd.recoverable_base,
             fdd.recoverable_value,
             fdd.base_cost,
             fdd.icms_cst,
             fdd.pis_cst,
             fdd.cofins_cst,
             fdd.pack_no,
             fdd.pack_ind
        from fm_fiscal_doc_detail fdd, fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id = I_fiscal_doc_id
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id;
   ---
   R_fiscal_detail  C_FM_FISCAL_DOC_DETAIL%ROWTYPE;
   ---
   cursor C_EXISTS_DOC_REF is
      select 'x'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = R_fiscal_detail.fiscal_doc_id_ref
         and fdh.schedule_no = (select fdh2.schedule_no
                                  from fm_fiscal_doc_header fdh2
                                 where fdh2.fiscal_doc_id = I_fiscal_doc_id);
   ---
   cursor C_EXISTS_DOC_LINE_REF is
      select 'x'
        from fm_fiscal_doc_detail fdd, fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_line_id = R_fiscal_detail.fiscal_doc_line_id_ref
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.schedule_no = (select fdh2.schedule_no
                                  from fm_fiscal_doc_header fdh2
                                 where fdh2.fiscal_doc_id = I_fiscal_doc_id);
   ---
   cursor C_CHK_RURAL_PROD_IND is
      select sfc.rural_prod_ind
       from  v_br_sups_fiscal_class sfc,
             fm_fiscal_doc_header fdh
      where fdh.fiscal_doc_id = I_fiscal_doc_id
        and fdh.module        = L_module
        and fdh.key_value_1   = sfc.supplier;

   cursor C_GET_TAX_DETAIL is
      select fdtd.fiscal_doc_line_id
        from fm_fiscal_doc_tax_detail fdtd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where fdh.fiscal_doc_id      = I_fiscal_doc_id
         and fdh.fiscal_doc_id      = fdd.fiscal_doc_id
         and fdd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
         and fdtd.vat_code          = L_ste
    order by fdtd.fiscal_doc_line_id;

   cursor C_GET_TAX_HEAD is
      select 'X'
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id
         and fdth.vat_code      = L_ste;

   cursor C_LOCK_DOC_TAX_HEAD is
      select 'X'
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;

   cursor C_LOCK_DOC_TAX_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_tax_detail fdtd
       where fdtd.fiscal_doc_line_id = P_fiscal_doc_line_id
         for update nowait;

BEGIN
   --- Get next fiscal document
   if FM_FISCAL_DOC_HEADER_SQL.GET_NEXT_FISCAL_DOC_ID(O_error_message,
                                                      L_new_fiscal_doc_id) = FALSE then
      return FALSE;
   end if;
   ---
   O_new_fiscal_doc_id := L_new_fiscal_doc_id;

   open C_CHK_RURAL_PROD_IND;
      fetch C_CHK_RURAL_PROD_IND into L_rural_prod_ind;
      close C_CHK_RURAL_PROD_IND;
      if NVL(L_rural_prod_ind,'N') = 'Y' then
         if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                               L_dummy_desc,
                                               L_dummy_number,
                                               L_utilization,
                                               L_dummy_date,
                                               C_CHAR,
                                               C_DEFAULT_RURAL_PROD_UTIL) = FALSE then
            return FALSE;
         end if;

         if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                               L_dummy_desc,
                                               L_type_id,
                                               L_dummy_string,
                                               L_dummy_date,
                                               C_NUMBER,
                                               C_DEFAULT_RURAL_PROD_DOC_TYPE) = FALSE then
            return FALSE;
         end if;
      end if;

   open C_GET_REQUISITION_TYPE;
   fetch C_GET_REQUISITION_TYPE into L_requisition_type;
   close C_GET_REQUISITION_TYPE;
   if L_requisition_type = 'TSF' then
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_utilization,
                                            L_dummy_date,
                                            C_CHAR,
                                            C_INBOUND_TSF_UTIL_ID) = FALSE then
            return FALSE;
      end if;
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_type_id,
                                            L_dummy_string,
                                            L_dummy_date,
                                            C_NUMBER,
                                            C_INBOUND_TSF_DOC_TYPE) = FALSE then
            return FALSE;
         end if;
   elsif L_requisition_type = 'IC' then
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_utilization,
                                            L_dummy_date,
                                            C_CHAR,
                                            C_INBOUND_IC_UTIL_ID) = FALSE then
            return FALSE;
      end if;
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_type_id,
                                            L_dummy_string,
                                            L_dummy_date,
                                            C_NUMBER,
                                            C_INBOUND_IC_DOC_TYPE) = FALSE then
            return FALSE;
         end if;

      if L_requisition_type in ('TSF','IC') then
         L_req_type := C_TSF;
      end if;
   elsif L_requisition_type = 'REP' then
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_utilization,
                                            L_dummy_date,
                                            C_CHAR,
                                            C_INBOUND_REP_UTIL_ID) = FALSE then
            return FALSE;
      end if;
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_type_id,
                                            L_dummy_string,
                                            L_dummy_date,
                                            C_NUMBER,
                                            C_INBOUND_REP_DOC_TYPE) = FALSE then
            return FALSE;
         end if;
   end if;
   if FM_FISCAL_DOC_TYPE_SQL.EXISTS(O_error_message,
                                    L_exists,
                                    L_dummy_desc,
                                    L_type_id) = FALSE then
      return FALSE;
   end if;
   --
   if NOT L_exists then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_DOC_TYPE',NULL,L_program,NULL);
      return FALSE;
   end if;
   --
   if FM_DOC_TYPE_UTILIZATION_SQL.EXISTS_UTILIZATION(O_error_message,
                                                     L_exists,
                                                     L_type_id,
                                                     L_utilization) = FALSE then
      return FALSE;
   end if;
   --
   if NOT L_exists then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_INV_UTIL_SETUP',L_type_id,L_utilization,L_program);
      return FALSE;
   end if;
   --
   if NVL(L_rural_prod_ind,'N') = 'Y' then
      L_requisition_type := 'RPO';
   end if;
   --
   if FM_FISCAL_UTILIZATION_SQL.EXISTS_UTILIZATION_REQ_TYPE(O_error_message,
                                                            L_exists,
                                                            L_requisition_type,
                                                            L_utilization) = FALSE then
       return FALSE;
   end if;

   if NOT L_exists then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_INV_UTIL',L_utilization,L_requisition_type,L_program);
      return FALSE;
   end if;
   --- Create the new fiscal document
   SQL_LIB.SET_MARK('INSERT', 'FM_FISCAL_DOC_HEADER', NULL, NULL);
   insert into fm_fiscal_doc_header (fiscal_doc_id,
                                     location_id,
                                     location_type,
                                     fiscal_doc_no,
                                     series_no,
                                     subseries_no,
                                     schedule_no,
                                     status,
                                     type_id,
                                     requisition_type,
                                     module,
                                     key_value_1,
                                     key_value_2,
                                     issue_date,
                                     entry_or_exit_date,
                                     exit_hour,
                                     partner_type,
                                     partner_id,
                                     quantity,
                                     unit_type,
                                     freight_type,
                                     net_weight,
                                     total_weight,
                                     total_serv_value,
                                     total_serv_calc_value,
                                     total_item_value,
                                     total_item_calc_value,
                                     total_doc_value,
                                     total_doc_calc_value,
                                     freight_cost,
                                     insurance_cost,
                                     other_expenses_cost,
                                     create_datetime,
                                     create_id,
                                     last_update_datetime,
                                     last_update_id,
                                     discount_type,
                                     total_discount_value,
                                     extra_costs_calc,
                                     total_doc_value_with_disc,
                                     vehicle_plate,
                                     plate_state,
                                     process_origin,
                                     process_value,
                                     utilization_id,
                                     entry_cfop,
                                     nf_cfop,
                                     nfe_accesskey,
                                     tax_discrep_status,
                                     ref_no_1,
                                     document_type)
                              select L_new_fiscal_doc_id, --- New sequence
                                     NVL(I_location_id,fdd.location_id),
                                     NVL(I_location_type,fdd.location_type),
                                     fdd.fiscal_doc_no,
                                     fdd.series_no,
                                     fdd.subseries_no,
                                     I_new_schedule,      --- New Schedule
                                     DECODE(NVL(L_rural_prod_ind,'N'),'Y','A','W'),-- Status Worksheet
                                     DECODE(NVL(L_req_type,fdd.requisition_type),'TSF',L_type_id,(DECODE(NVL(L_rural_prod_ind,'N'),'Y',L_type_id,fdd.type_id))),
                                     DECODE(NVL(L_rural_prod_ind,'N'),'Y','RPO',fdd.requisition_type),
                                     --- If operation is not Reverse and the requisition type is TSF set location and location_type from NF copied as new sender/receiver.
                                     DECODE(I_operation,'R',fdd.module,DECODE(NVL(L_req_type,fdd.requisition_type),'TSF','LOC',fdd.module)),
                                     DECODE(I_operation,'R',fdd.key_value_1,DECODE(NVL(L_req_type,fdd.requisition_type),'TSF',fdd.location_id,fdd.key_value_1)),
                                     DECODE(I_operation,'R',fdd.key_value_2,DECODE(NVL(L_req_type,fdd.requisition_type),'TSF',fdd.location_type,fdd.key_value_2)),
                                     fdd.issue_date,
                                     fdd.entry_or_exit_date,
                                     fdd.exit_hour,
                                     fdd.partner_type,
                                     fdd.partner_id,
                                     fdd.quantity,
                                     fdd.unit_type,
                                     fdd.freight_type,
                                     fdd.net_weight,
                                     fdd.total_weight,
                                     fdd.total_serv_value,
                                     fdd.total_serv_calc_value,
                                     fdd.total_item_value,
                                     fdd.total_item_calc_value,
                                     fdd.total_doc_value,
                                     fdd.total_doc_calc_value,
                                     fdd.freight_cost,
                                     fdd.insurance_cost,
                                     fdd.other_expenses_cost,
                                     SYSDATE,  --- Create datetime
                                     USER,     --- Create id
                                     SYSDATE,  --- Last Update Datetime,
                                     USER,     --- Last Update id
                                     fdd.discount_type,
                                     fdd.total_discount_value,
                                     fdd.extra_costs_calc,
                                     fdd.total_doc_value_with_disc,
                                     fdd.vehicle_plate,
                                     fdd.plate_state,
                                     fdd.process_origin,
                                     fdd.process_value,
                                     DECODE(NVL(L_req_type,fdd.requisition_type),'TSF',L_utilization,(DECODE(NVL(L_rural_prod_ind,'N'),'Y',L_utilization,fdd.utilization_id))),
                                     fdd.entry_cfop,
                                     fdd.nf_cfop,
                                     fdd.nfe_accesskey,
                                     DECODE(NVL(L_rural_prod_ind,'N'),'Y','M',fdd.tax_discrep_status),
                                     fdd.ref_no_1,
                                     fdd.document_type
                                from fm_fiscal_doc_header fdd, fm_schedule fs
                               where fdd.fiscal_doc_id = I_fiscal_doc_id
                                 and fs.schedule_no = fdd.schedule_no;

   --- Copy Fm_fiscal_doc_complement
   insert into fm_fiscal_doc_complement (compl_fiscal_doc_id,
                                         fiscal_doc_id,
                                         create_datetime,
                                         create_id,
                                         last_update_datetime,
                                         last_update_id)
                                  select L_new_fiscal_doc_id,   --- New fiscal document as complementary
                                         fdc.fiscal_doc_id,
                                         SYSDATE,               --- Create datetime
                                         USER,                  --- Create id
                                         SYSDATE,               --- Last update datetime
                                         USER                   --- Last update id
                                    from fm_fiscal_doc_complement fdc, fm_fiscal_doc_header fdh
                                   where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
                                     and fdh.fiscal_doc_id = fdc.fiscal_doc_id
                                     and fdh.schedule_no = (select fdh2.schedule_no
                                                              from fm_fiscal_doc_header fdh2
                                                             where fdh2.fiscal_doc_id = I_fiscal_doc_id);
if NVL(L_rural_prod_ind,'N') <> 'Y' then

   --- Copy Fm_fiscal_doc_tax_head
   insert into fm_fiscal_doc_tax_head (vat_code,
                                       fiscal_doc_id,
                                       tax_basis,
                                       modified_tax_basis,
                                       total_value,
                                       create_datetime,
                                       create_id,
                                       last_update_datetime,
                                       last_update_id)
                                select fdth.vat_code,
                                       L_new_fiscal_doc_id, -- New Fiscal Doc
                                       fdth.tax_basis,
                                       fdth.modified_tax_basis,
                                       fdth.total_value,
                                       SYSDATE,             -- Create datetime
                                       USER,                -- Create id
                                       SYSDATE,             -- Last update datetime
                                       USER                 -- Last update id
                                  from fm_fiscal_doc_tax_head fdth
                                 where fdth.fiscal_doc_id = I_fiscal_doc_id;
end if;

   --- Copy fm_fiscal_doc_payments
   insert into fm_fiscal_doc_payments (fiscal_doc_id,
                                       payment_date,
                                       value,
                                       create_datetime,
                                       create_id,
                                       last_update_datetime,
                                       last_update_id)
                                select L_new_fiscal_doc_id, -- New fiscal doc
                                       payment_date,
                                       value,
                                       SYSDATE,             -- Create datetime
                                       USER,                -- Create id
                                       SYSDATE,             -- Last update datetime
                                       USER                 -- Last update id
                                  from fm_fiscal_doc_payments fdp
                                 where fdp.fiscal_doc_id = I_fiscal_doc_id;

   --- Copy fm_fiscal_doc_detail
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   LOOP
      SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
      fetch C_FM_FISCAL_DOC_DETAIL into R_fiscal_detail;
      EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;

      --- Check if the referenced document belong to schedule that is being disapproved
      if R_fiscal_detail.fiscal_doc_id_ref is NOT NULL then
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_DOC_REF', L_table, 'Fiscal_doc_id: '||R_fiscal_detail.fiscal_doc_id_ref);
         open C_EXISTS_DOC_REF;
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_DOC_REF', L_table, 'Fiscal_doc_id: '||R_fiscal_detail.fiscal_doc_id_ref);
         fetch C_EXISTS_DOC_REF into L_dummy;
         if C_EXISTS_DOC_REF%FOUND then
            R_fiscal_detail.fiscal_doc_id_ref := NULL;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_DOC_REF', L_table, 'Fiscal_doc_id: '||R_fiscal_detail.fiscal_doc_id_ref);
         close C_EXISTS_DOC_REF;
         ---
      end if;

      --- Check if the referenced line document belong to schedule that is being disapproved
      if R_fiscal_detail.fiscal_doc_line_id_ref is NOT NULL then
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_DOC_LINE_REF', 'FM_FISCAL_DOC_DETAIL', 'Fiscal_doc_line_id: '||R_fiscal_detail.fiscal_doc_line_id_ref);
         open C_EXISTS_DOC_LINE_REF;
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_DOC_LINE_REF', L_table, 'Fiscal_doc_line_id: '||R_fiscal_detail.fiscal_doc_line_id_ref);
         fetch C_EXISTS_DOC_LINE_REF into L_dummy;
         if C_EXISTS_DOC_LINE_REF%FOUND then
            R_fiscal_detail.fiscal_doc_line_id_ref := NULL;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_DOC_LINE_REF', L_table, 'Fiscal_doc_line_id: '||R_fiscal_detail.fiscal_doc_line_id_ref);
         close C_EXISTS_DOC_LINE_REF;
         ---
      end if;
      ---

      if FM_FISCAL_DOC_DETAIL_SQL.GET_NEXT_FISCAL_DOC_LINE_ID(O_error_message,
                                                              L_new_fiscal_doc_line_id) = FALSE then
         return FALSE;
      end if;
      ---
      insert into fm_fiscal_doc_detail (fiscal_doc_line_id,
                                        fiscal_doc_id,
                                        location_id,
                                        location_type,
                                        line_no,
                                        requisition_no,
                                        item,
                                        classification_id,
                                        quantity,
                                        unit_cost,
                                        total_cost,
                                        total_calc_cost,
                                        freight_cost,
                                        net_cost,
                                        create_datetime,
                                        create_id,
                                        last_update_datetime,
                                        last_update_id,
                                        fiscal_doc_id_ref,
                                        discount_type,
                                        discount_value,
                                        fiscal_doc_line_id_ref,
                                        unit_cost_with_disc,
                                        unexpected_item,
                                        entry_cfop,
                                        nf_cfop,
                                        other_expenses_cost,
                                        insurance_cost,
                                        qty_discrep_status,
                                        cost_discrep_status,
                                        tax_discrep_status,
                                        inconclusive_rules,
                                        appt_qty,
                                        recoverable_base,
                                        recoverable_value,
                                        base_cost,
                                        icms_cst,
                                        pis_cst,
                                        cofins_cst,
                                        pack_no,
                                        pack_ind)
                                values (L_new_fiscal_doc_line_id,  --- New line id
                                        L_new_fiscal_doc_id,       --- New Fiscal doc,
                                        NVL(I_location_id,R_fiscal_detail.location_id),
                                        NVL(I_location_type,R_fiscal_detail.location_type),
                                        R_fiscal_detail.line_no,
                                        R_fiscal_detail.requisition_no,
                                        R_fiscal_detail.item,
                                        R_fiscal_detail.classification_id,
                                        R_fiscal_detail.quantity,
                                        R_fiscal_detail.unit_cost,
                                        R_fiscal_detail.total_cost,
                                        R_fiscal_detail.total_calc_cost,
                                        R_fiscal_detail.freight_cost,
                                        R_fiscal_detail.net_cost,
                                        SYSDATE,                   --- Create datetime
                                        USER,                      --- Create id
                                        SYSDATE,                   --- Last update datetime
                                        USER,                      --- Last update id
                                        DECODE(NVL(L_req_type,R_fiscal_detail.requisition_type),'TSF',NULL,DECODE(L_rural_prod_ind,'Y',R_fiscal_detail.fiscal_doc_id,R_fiscal_detail.fiscal_doc_id_ref)),
                                        R_fiscal_detail.discount_type,
                                        R_fiscal_detail.discount_value,
                                        DECODE(NVL(L_req_type,R_fiscal_detail.requisition_type),'TSF',NULL,DECODE(L_rural_prod_ind,'Y',R_fiscal_detail.fiscal_doc_line_id,R_fiscal_detail.fiscal_doc_line_id_ref)),
                                        R_fiscal_detail.unit_cost_with_disc,
                                        R_fiscal_detail.unexpected_item,
                                        R_fiscal_detail.entry_cfop,
                                        R_fiscal_detail.nf_cfop,
                                        R_fiscal_detail.other_expenses_cost,
                                        R_fiscal_detail.insurance_cost,
                                        DECODE(NVL(L_rural_prod_ind,'N'),'Y','M',R_fiscal_detail.qty_discrep_status),
                                        DECODE(NVL(L_rural_prod_ind,'N'),'Y','M',R_fiscal_detail.cost_discrep_status),
                                        DECODE(NVL(L_rural_prod_ind,'N'),'Y','M',R_fiscal_detail.tax_discrep_status),
                                        R_fiscal_detail.inconclusive_rules,
                                        DECODE(NVL(L_req_type,R_fiscal_detail.requisition_type),'TSF',R_fiscal_detail.quantity,R_fiscal_detail.appt_qty),
                                        R_fiscal_detail.recoverable_base,
                                        R_fiscal_detail.recoverable_value,
                                        R_fiscal_detail.base_cost,
                                        R_fiscal_detail.icms_cst,
                                        R_fiscal_detail.pis_cst,
                                        R_fiscal_detail.cofins_cst,
                                        R_fiscal_detail.pack_no,
                                        R_fiscal_detail.pack_ind);

   if NVL(L_rural_prod_ind,'N') <> 'Y' then

      --- Copy Fm_fiscal_doc_tax_detail
      insert into fm_fiscal_doc_tax_detail (vat_code,
                                            fiscal_doc_line_id,
                                            percentage_rate,
                                            total_value,
                                            create_datetime,
                                            create_id,
                                            last_update_datetime,
                                            last_update_id,
                                            legal_message_text,
                                            tax_basis,
                                            modified_tax_basis,
                                            unit_tax_amt,
                                            rec_value,
                                            unit_rec_value,
                                            tax_base_ind)
                                     select fdtd.vat_code,
                                            L_new_fiscal_doc_line_id,  --- New line id
                                            fdtd.percentage_rate,
                                            fdtd.total_value,
                                            SYSDATE,                   --- Create datetime
                                            USER,                      --- Create id
                                            SYSDATE,                   --- Last update datetime
                                            USER,                       --- Last update id
                                            fdtd.legal_message_text,
                                            fdtd.tax_basis,
                                            fdtd.modified_tax_basis,
                                            fdtd.unit_tax_amt,
                                            fdtd.rec_value,
                                            fdtd.unit_rec_value,
                                            fdtd.tax_base_ind
                                       from fm_fiscal_doc_tax_detail fdtd
                                      where fdtd.fiscal_doc_line_id = R_fiscal_detail.fiscal_doc_line_id;
      ---
   end if;

   END LOOP;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_FM_FISCAL_DOC_DETAIL;

   open C_GET_TAX_DETAIL;
   loop
   fetch C_GET_TAX_DETAIL into L_fiscal_doc_line_id;
   exit when C_GET_TAX_DETAIL%NOTFOUND;

      L_key   := 'fiscal_doc_line_id = '||TO_CHAR(L_fiscal_doc_line_id);
      SQL_LIB.SET_MARK('OPEN','C_LOCK_DOC_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
      open C_LOCK_DOC_TAX_DETAIL(L_fiscal_doc_line_id);

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_DOC_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
      close C_LOCK_DOC_TAX_DETAIL;
   ---
      SQL_LIB.SET_MARK('DELETE',NULL,'FM_FISCAL_DOC_TAX_DETAIL',L_key);

      delete fm_fiscal_doc_tax_detail
       where fiscal_doc_line_id = L_fiscal_doc_line_id
         and vat_code           = L_ste;

   end loop;
   close C_GET_TAX_DETAIL;
   ---
    open C_GET_TAX_HEAD;
   fetch C_GET_TAX_HEAD into L_found;
   if C_GET_TAX_HEAD%FOUND then

      L_key   := 'fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id);
      SQL_LIB.SET_MARK('OPEN','C_LOCK_DOC_TAX_HEAD','FM_FISCAL_DOC_TAX_HEAD',L_key);
      open C_LOCK_DOC_TAX_HEAD;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_DOC_TAX_HEAD','FM_FISCAL_DOC_TAX_HEAD',L_key);
      close C_LOCK_DOC_TAX_HEAD;
         ---
      SQL_LIB.SET_MARK('DELETE',NULL,'FM_FISCAL_DOC_TAX_HEAD',L_key);

      delete fm_fiscal_doc_tax_head
       where fiscal_doc_id = I_fiscal_doc_id
         and vat_code      = L_ste;

   end if;
   close C_GET_TAX_HEAD;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END COPY_DOC_HEADER;
----------------------------------------------------------------------------------
FUNCTION REVERSE_FISCAL_DOC(O_error_message  IN OUT VARCHAR2,
                            I_new_schedule   IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                            I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER_SQL.REVERSE_FISCAL_DOC';
   L_new_fiscal_doc_id      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   ---
BEGIN
   --- Copy fiscal document
   if FM_FISCAL_DOC_HEADER_SQL.COPY_DOC_HEADER(O_error_message,
                                               L_new_fiscal_doc_id,
                                               I_new_schedule,
                                               NULL,
                                               NULL,
                                               I_fiscal_doc_id,
                                               'R') = FALSE then
      return FALSE;
   end if;

   --- Change the NF status to Disapproved.
   if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                             I_fiscal_doc_id,
                                             'D') = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REVERSE_FISCAL_DOC;
----------------------------------------------------------------------------------
FUNCTION CHECK_DUP_DOC(O_error_message    IN OUT VARCHAR2,
                       O_exists           IN OUT BOOLEAN,
                       I_mode_type        IN     FM_SCHEDULE.MODE_TYPE%TYPE,
                       I_fiscal_doc_id    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_id%TYPE,
                       I_fiscal_doc_no    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                       I_series_no        IN     FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE,
                       I_module           IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                       I_key_value_1      IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                       I_key_value_2      IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                       I_issue_date       IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                       I_requisition_type IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_HEADER_SQL.CHECK_DUP_DOC';
   L_dummy     VARCHAR2(1);
   L_status_i  VARCHAR2(1) := 'I';
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select 'x'
        from fm_fiscal_doc_header d, fm_schedule fs
       where d.schedule_no = fs.schedule_no
         and fs.mode_type = I_mode_type 
         and d.schedule_no is NOT NULL
         and ((I_fiscal_doc_id is NOT NULL
         and d.fiscal_doc_id <> I_fiscal_doc_id)
          or I_fiscal_doc_id is NULL)
         and d.fiscal_doc_no = I_fiscal_doc_no
         and ((I_series_no is NULL
         and d.series_no is NULL)
          or (I_series_no is NOT NULL
         and d.series_no = I_series_no))
         and d.module = I_module
         and d.key_value_1 = I_key_value_1
         and d.key_value_2 = I_key_value_2
         and d.issue_date = I_issue_date
         and d.status NOT IN (L_status_i)
         and ((I_requisition_type is NOT NULL
         and d.requisition_type = I_requisition_type)
          or I_requisition_type is NULL)
    UNION ALL
      select 'x'
        from fm_fiscal_doc_header d   
       where I_mode_type = 'ENT'
         and d.schedule_no is NULL
         and ((I_fiscal_doc_id is NOT NULL
         and d.fiscal_doc_id <> I_fiscal_doc_id)
          or I_fiscal_doc_id is NULL)
         and d.fiscal_doc_no = I_fiscal_doc_no
         and ((I_series_no is NULL
         and d.series_no is NULL)
          or (I_series_no is NOT NULL
         and d.series_no = I_series_no))
         and d.module = I_module
         and d.key_value_1 = I_key_value_1
         and d.key_value_2 = I_key_value_2
         and d.issue_date = I_issue_date
         and d.status NOT IN (L_status_i)
         and ((I_requisition_type is NOT NULL
         and d.requisition_type = I_requisition_type)
          or I_requisition_type is NULL);
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_FM_FISCAL_DOC_HEADER',
                    'FM_FISCAL_DOC_HEADER, FM_SCHEDULE',
                    SUBSTR('Mode_type = '||I_mode_type ||
                    ' Fiscal_doc_no = ' || I_fiscal_doc_no ||
                    ' Series_no = '    || I_series_no ||
                    ' Module = '       || I_module ||
                    ' Key_value_1 = '  || I_key_value_1||
                    ' Key_value_2 = '  || I_key_value_2||
                    ' Issue_date = '   || TO_CHAR(I_issue_date), 1, 120));
   open C_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_FM_FISCAL_DOC_HEADER',
                    'FM_FISCAL_DOC_HEADER, FM_SCHEDULE',
                    SUBSTR('Mode_type = '||I_mode_type ||
                    ' Fiscal_doc_no = ' || I_fiscal_doc_no ||
                    ' Series_no = '    || I_series_no ||
                    ' Module = '       || I_module ||
                    ' Key_value_1 = '  || I_key_value_1||
                    ' Key_value_2 = '  || I_key_value_2||
                    ' Issue_date = '   || TO_CHAR(I_issue_date), 1, 120));
   fetch C_FM_FISCAL_DOC_HEADER into L_dummy;
   O_exists := C_FM_FISCAL_DOC_HEADER%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_FM_FISCAL_DOC_HEADER',
                    'FM_FISCAL_DOC_HEADER, FM_SCHEDULE',
                    SUBSTR('Mode_type = '||I_mode_type ||
                    ' Fiscal_doc_no = ' || I_fiscal_doc_no ||
                    ' Series_no = '    || I_series_no ||
                    ' Module = '       || I_module ||
                    ' Key_value_1 = '  || I_key_value_1||
                    ' Key_value_2 = '  || I_key_value_2||
                    ' Issue_date = '   || TO_CHAR(I_issue_date), 1, 120));
   close C_FM_FISCAL_DOC_HEADER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_DUP_DOC;
-----------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS_FOR_PENDING_DOC(O_error_message  IN OUT VARCHAR2,
                                       I_fiscal_doc_no  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                       I_series_no      IN     FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE,
                                       I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                                       I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                                       I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                                       I_issue_date     IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE)
   return BOOLEAN is
   ---
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program      VARCHAR2(60) := 'FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS_FOR_PENDING_DOC';
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER';
   L_key          VARCHAR2(120) := SUBSTR('Fiscal_doc_no = ' || I_fiscal_doc_no ||
                                          ' Series_no = '    || I_series_no ||
                                          ' Module = '       || I_module ||
                                          ' Key_value_1 = '  || I_key_value_1||
                                          ' Key_value_2 = '  || I_key_value_2||
                                          ' Issue_date = '   || TO_CHAR(I_issue_date), 1, 120);
   L_dummy        VARCHAR2(1);
   L_status       VARCHAR2(1)  := 'R';
   L_status_P       VARCHAR2(1)  := 'P';
   L_mode_type    VARCHAR2(5)  := 'EXIT';
   ---
   cursor C_LOCK_FM_FISCAL_DOC_HEADER is
      select 'x'
        from fm_fiscal_doc_header d
       where d.fiscal_doc_no = I_fiscal_doc_no
         and d.series_no = I_series_no
         and d.module = I_module
         and d.key_value_1 = I_key_value_1
         and d.key_value_2 = I_key_value_2
         and d.issue_date = I_issue_date
         and d.status = L_status
         and exists (select 1
                       from fm_schedule fs
                      where fs.schedule_no = d.schedule_no
                        and fs.mode_type = L_mode_type)
         for update nowait;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_FISCAL_DOC_HEADER',L_table,L_key);
   open C_LOCK_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_FISCAL_DOC_HEADER',L_table,L_key);
   close C_LOCK_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('UPDATE',L_table,NULL,NULL);
   update fm_fiscal_doc_header d
      set d.status = L_status_P
    where d.fiscal_doc_no = I_fiscal_doc_no
      and d.series_no = I_series_no
      and d.module = I_module
      and d.key_value_1 = I_key_value_1
      and d.key_value_2 = I_key_value_2
      and d.issue_date = I_issue_date
      and d.status = L_status
      and exists (select 1
                    from fm_schedule fs
                   where fs.schedule_no = d.schedule_no
                     and fs.mode_type = L_mode_type);
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FM_FISCAL_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_FM_FISCAL_DOC_HEADER', L_table, L_key);
         close C_LOCK_FM_FISCAL_DOC_HEADER;
      end if;
      ---
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_STATUS_FOR_PENDING_DOC;
----------------------------------------------------------------------------------
-- Function Name: CHECK_DISCREPANCY
-- Purpose:       This function identifies ,if there any type of discrepancy exists for a NF and returns the same.
----------------------------------------------------------------------------------
FUNCTION CHECK_DISCREPANCY(O_error_message  IN OUT VARCHAR2,
                           I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                           O_Cost_Disp      IN OUT VARCHAR2,
                           O_Qty_Disp       IN OUT VARCHAR2,
                           O_Tax_Disp       IN OUT VARCHAR2
                           )
   return BOOLEAN is

   L_program            VARCHAR2(80) := 'FM_FISCAL_DOC_HEADER_SQL.CHECK_DISCREPANCY';
   L_error_message      VARCHAR2(255);
   L_key                VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
   L_header_discrepant  VARCHAR2(1)  := 'N';
   L_discrep_status VARCHAR2(1)  := 'D';

   cursor C_GET_QTY_DIS is
     SELECT 'X'
     FROM   fm_fiscal_doc_detail
     WHERE  fiscal_doc_id   = I_fiscal_doc_id
       AND  Qty_discrep_status = L_discrep_status;

   cursor C_GET_COST_DIS is
     SELECT 'X'
     FROM   fm_fiscal_doc_detail
     WHERE  fiscal_doc_id   = I_fiscal_doc_id
       AND  Cost_discrep_status = L_discrep_status;

   cursor C_GET_HEADER_TAX_DIS is
     SELECT 'X'
     FROM   fm_fiscal_doc_header
     WHERE  fiscal_doc_id   = I_fiscal_doc_id
       AND  Tax_discrep_status = L_discrep_status;

   cursor C_GET_DETAIL_TAX_DIS is
     SELECT 'X'
     FROM   fm_fiscal_doc_detail
     WHERE  fiscal_doc_id   = I_fiscal_doc_id
       AND  Tax_discrep_status = L_discrep_status;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_QTY_DIS', 'fm_fiscal_doc_detail', L_key);
   FOR L_QTY_DIS IN C_GET_QTY_DIS
   LOOP
    O_Qty_Disp := 'Y';
    Exit;
   END LOOP;

   SQL_LIB.SET_MARK('OPEN','C_GET_COST_DIS', 'fm_fiscal_doc_detail', L_key);
   FOR L_COST_DIS IN C_GET_COST_DIS
   LOOP
    O_Cost_Disp := 'Y';
    Exit;
   END LOOP;

   SQL_LIB.SET_MARK('OPEN','C_GET_HEADER_TAX_DIS', 'fm_fiscal_doc_header', L_key);
   FOR L_HEADER_TAX_DIS IN C_GET_HEADER_TAX_DIS
   LOOP
    O_Tax_Disp := 'Y';
    L_header_discrepant := 'Y';
   END LOOP;

   if L_header_discrepant = 'N' then

      SQL_LIB.SET_MARK('OPEN','C_GET_DETAIL_TAX_DIS', 'fm_fiscal_doc_detail', L_key);
      FOR L_DETAIL_TAX_DIS IN C_GET_DETAIL_TAX_DIS
      LOOP
       O_Tax_Disp := 'Y';
       Exit;
      END LOOP;

   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));

      return FALSE;

END CHECK_DISCREPANCY;
-----------------------------------------------------------------------------------
-- Function Name: RESET_WORKSHEET
-- Purpose:       This function resets all the discrepancy flags,apportioned values and deletes the  resolution action , Tax coming from external systems.
----------------------------------------------------------------------------------
FUNCTION RESET_WORKSHEET(O_error_message IN OUT VARCHAR2,
                         I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN  is

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_appr_tax_count       Number;
   L_program              VARCHAR2(50) := 'FM_FISCAL_DOC_HEADER_SQL.RESET_WORKSHEET';
   L_key                  VARCHAR2(100) := 'fiscal_doc_id: '||I_fiscal_doc_id;
   L_comp_fiscal_id       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE := NULL;
   L_type_id              FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE;
   L_nfe_type_id          FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE;
   L_req_type             FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := NULL;
   L_rma                  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'RMA';
   L_dummy_string         FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_dummy_desc           FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date           FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   ---
   cursor C_LOCK_FM_FISCAL_HEADER is
    select 'X'
    from fm_fiscal_doc_header
    where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id)
    for update nowait;
   ---
   cursor C_LOCK_FM_FISCAL_DETAIL is
    select 'X'
    from fm_fiscal_doc_detail
    where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id)
    for update nowait;
   ---
   cursor C_LOCK_FM_FISCAL_TAX_HEAD is
    select 'X'
    from fm_fiscal_doc_tax_head
    where fiscal_doc_id in ( I_fiscal_doc_id)
    for update nowait;
   ---
   cursor C_LOCK_FM_FISCAL_TAX_DETAIL is
    select 'X'
    from fm_fiscal_doc_tax_detail
    where fiscal_doc_line_id in (select fiscal_doc_line_id from fm_fiscal_doc_detail  where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id) )
    for update nowait;
   ---
   cursor C_LOCK_FM_RESOLUTION is
    select 'X'
    from fm_resolution
    where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id)
    for update nowait;
   ---
   cursor C_FM_FISCAL_DOC_TAX_DETAIL is
      select count(1)
        from fm_fiscal_doc_detail fdd, fm_fiscal_doc_tax_detail fdtd
       where fdd.fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id)
         and fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         and fdtd.appr_tax_value is NULL;
   ---
   cursor C_GET_TYPE_ID is
      select type_id,
              requisition_type
        from fm_fiscal_doc_header
       where fiscal_doc_id = I_fiscal_doc_id;

BEGIN
-- Get the Type id
    SQL_LIB.SET_MARK('OPEN','C_GET_TYPE_ID','FM_FISCAL_DOC_HEADER',L_key);
    open C_GET_TYPE_ID;
    ---
    SQL_LIB.SET_MARK('FETCH','C_GET_TYPE_ID','FM_FISCAL_DOC_HEADER',L_key);
    fetch C_GET_TYPE_ID into L_type_id, L_req_type;
    ---
    SQL_LIB.SET_MARK('CLOSE','C_GET_TYPE_ID','FM_FISCAL_DOC_HEADER',L_key);
    close C_GET_TYPE_ID;

  -- get Nfe typeid from system options
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_nfe_type_id,
                                            L_dummy_string,
                                            L_dummy_date,
                                            'NUMBER',
                                            'DEFAULT_NFE_DOC_TYPE') = FALSE then
         return FALSE;
      end if;
-- check for complemantory NF
    if L_type_id <> L_nfe_type_id then
      if FM_FISCAL_DOC_HEADER_SQL.UPD_COMP_NF_STATUS(O_error_message,
                                                     L_comp_fiscal_id,
                                                     'W',
                                                     I_fiscal_doc_id) = FALSE then
          return FALSE;
     end if;
    end if;
 -- Acquire all the DB lockes before update
    ---
    SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_FISCAL_HEADER','FM_FISCAL_DOC_HEADER',L_key);
    open C_LOCK_FM_FISCAL_HEADER;
    SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_FISCAL_HEADER','FM_FISCAL_DOC_HEADER',L_key);
    close C_LOCK_FM_FISCAL_HEADER;
    ---
    SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_FISCAL_DETAIL','FM_FISCAL_DOC_DEATIL',L_key);
    open C_LOCK_FM_FISCAL_DETAIL;
    SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_FISCAL_DETAIL','FM_FISCAL_DOC_DETAIL',L_key);
    close C_LOCK_FM_FISCAL_DETAIL;
    ---
    SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_FISCAL_TAX_HEAD','FM_FISCAL_DOC_TAX_HEAD',L_key);
    open C_LOCK_FM_FISCAL_TAX_HEAD;
    SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_FISCAL_TAX_HEAD','FM_FISCAL_DOC_TAX_HEAD',L_key);
    close C_LOCK_FM_FISCAL_TAX_HEAD;
    ---
    SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_FISCAL_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
    open C_LOCK_FM_FISCAL_TAX_DETAIL;
    SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_FISCAL_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
    close C_LOCK_FM_FISCAL_TAX_DETAIL;
    ---
    SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_RESOLUTION','FM_RESOLUTION',L_key);
    open C_LOCK_FM_RESOLUTION;
    SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_RESOLUTION','FM_RESOLUTION',L_key);
    close C_LOCK_FM_RESOLUTION;
    ---

-- update flags in NF Header table
    SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_FISCAL_DOC_HEADER', L_key);
    ---
    update fm_fiscal_doc_header
      set tax_discrep_status = NULL,
          total_item_calc_value = NULL,
          total_serv_calc_value = NULL,
          extra_costs_calc = NULL,
          total_doc_value_with_disc = NULL,
          total_doc_calc_value = NULL,
          entry_cfop = NULL,
          variance_cost = NULL,
          last_update_datetime = SYSDATE,
          last_update_id = USER
    where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id);


-- update flags in NF Detail table

    SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_FISCAL_DOC_DETAIL', L_key);

    update fm_fiscal_doc_detail
         set qty_discrep_status = NULL,
             cost_discrep_status = NULL,
             tax_discrep_status = NULL,
             unit_item_disc = NULL,
             unit_header_disc = NULL,
             unit_cost_with_disc = NULL,
             total_calc_cost = NULL,
             freight_cost = NULL,
             insurance_cost = NULL,
             other_expenses_cost = NULL,
             last_update_datetime = SYSDATE,
             last_update_id = USER
       where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id);

-- update flags in NF Detail Tax table

    SQL_LIB.SET_MARK('OPEN','C_FM_FISCAL_DOC_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
    open C_FM_FISCAL_DOC_TAX_DETAIL;
    ---
    SQL_LIB.SET_MARK('FETCH','C_FM_FISCAL_DOC_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
    fetch C_FM_FISCAL_DOC_TAX_DETAIL into L_appr_tax_count ;
    ---
    SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DOC_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
    close C_FM_FISCAL_DOC_TAX_DETAIL;

    if L_type_id = L_nfe_type_id or L_req_type = L_rma then
      SQL_LIB.SET_MARK('DELETE', NULL, 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
      delete fm_fiscal_doc_tax_detail
      where fiscal_doc_line_id in (select fiscal_doc_line_id from fm_fiscal_doc_detail  where fiscal_doc_id in ( I_fiscal_doc_id) );
      ---
      SQL_LIB.SET_MARK('DELETE', NULL, 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
      delete fm_fiscal_doc_tax_head
      where fiscal_doc_id in (I_fiscal_doc_id);
    else
      if L_appr_tax_count = 0 then
         SQL_LIB.SET_MARK('DELETE', NULL, 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
         ---
         delete fm_fiscal_doc_tax_detail
         where fiscal_doc_line_id in (select fiscal_doc_line_id from fm_fiscal_doc_detail  where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id) );

      else
         SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
         ---
         update fm_fiscal_doc_tax_detail
         set unit_rec_value = NULL,
             rec_value = NULL,
             legal_message_text = NULL
         where fiscal_doc_line_id in (select fiscal_doc_line_id from fm_fiscal_doc_detail  where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id) );
      end if;
    end if;
-- flush resolution and tax tables

    delete fm_resolution where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id);

    delete fm_fiscal_doc_tax_rule_ext where fiscal_doc_line_id in (select fiscal_doc_line_id from fm_fiscal_doc_detail  where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id) );

    delete fm_fiscal_doc_tax_detail_ext where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id);

    delete fm_fiscal_doc_tax_head_ext where fiscal_doc_id in ( I_fiscal_doc_id , L_comp_fiscal_id);


 return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                             L_key,
                                             TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FM_FISCAL_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_HEADER;
      end if;

      if C_LOCK_FM_FISCAL_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_DETAIL;
      end if;

      if C_LOCK_FM_FISCAL_TAX_HEAD%ISOPEN then
         close C_LOCK_FM_FISCAL_TAX_HEAD;
      end if;

      if C_LOCK_FM_FISCAL_TAX_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_TAX_DETAIL;
      end if;

      if C_LOCK_FM_RESOLUTION%ISOPEN then
         close C_LOCK_FM_RESOLUTION;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      if C_FM_FISCAL_DOC_TAX_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
         close C_FM_FISCAL_DOC_TAX_DETAIL;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return FALSE;

END RESET_WORKSHEET;
-----------------------------------------------------------------------------------
-- Function Name: LOCK_FISCAL_HEADER
-- Purpose:       This function tries to acquire lock on fiscal header Form , when it is open in Edit mode.
-----------------------------------------------------------------------------------
FUNCTION LOCK_FISCAL_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_id   IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN  is

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_program              VARCHAR2(50) := 'FM_FISCAL_DOC_HEADER_SQL.LOCK_FISCAL_HEADER';
   L_key                  VARCHAR2(100) := 'fiscal_doc_id: '||I_fiscal_doc_id;
   ---
   cursor C_LOCK_FM_FISCAL_HEADER is
    select 'X'
    from fm_fiscal_doc_header
    where fiscal_doc_id = I_fiscal_doc_id
    for update nowait;
BEGIN
    SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_FISCAL_HEADER','FM_FISCAL_DOC_HEADER',L_key);
    open C_LOCK_FM_FISCAL_HEADER;

    SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_FISCAL_HEADER','FM_FISCAL_DOC_HEADER',L_key);
    close C_LOCK_FM_FISCAL_HEADER;
    ---
    return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            I_fiscal_doc_id,
                                            NULL);
      if C_LOCK_FM_FISCAL_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_HEADER;
      end if;
      return FALSE;
      ---

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_FISCAL_HEADER;
-------------------------------------------------------------------------
-- Function Name: UPD_SCHED_STATUS
-- Purpose:       This function sets the schedule status, based on the various NF status under that schedule.
-----------------------------------------------------------------------------------
FUNCTION UPD_SCHED_STATUS(O_error_message IN OUT VARCHAR2,
                          I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_FISCAL_DOC_HEADER_SQL.UPD_SCHED_STATUS';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_dummy   VARCHAR2(1);
      L_status  FM_SCHEDULE.STATUS%TYPE ;
      L_wrksht_status  FM_SCHEDULE.STATUS%TYPE := 'W';
      L_error_status   FM_SCHEDULE.STATUS%TYPE := 'E';
      L_disp_status    FM_SCHEDULE.STATUS%TYPE := 'D';
      L_utilization_id             FM_UTILIZATION_ATTRIBUTES.UTILIZATION_ID%TYPE;
      L_utilization_attributes     FM_UTILIZATION_ATTRIBUTES%ROWTYPE;
      L_fiscal_doc_id              FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;

      cursor C_EXISTS_WRKSHT is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_wrksht_status;
      ---
      cursor C_EXISTS_ERROR is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_error_status;
      ---
      cursor C_EXISTS_DISP is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_disp_status;
     ---
   BEGIN
      ---
    if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                                I_schedule_no,
                                                 NULL);
         return FALSE;
    end if;

    --- Check is exists any NF with Worksheet status

    SQL_LIB.SET_MARK('OPEN',
                     'C_EXISTS_WRKSHT',
                     'FM_FISCAL_DOC_HEADER',
                      L_key);
    open C_EXISTS_WRKSHT;
    ---
    SQL_LIB.SET_MARK('FETCH',
                     'C_EXISTS_WRKSHT',
                     'FM_FISCAL_DOC_HEADER',
                      L_key);
    fetch C_EXISTS_WRKSHT into L_dummy;
    if C_EXISTS_WRKSHT%FOUND then
        L_status := 'W';
    end if;
    ---
    SQL_LIB.SET_MARK('CLOSE',
                     'C_EXISTS_WRKSHT',
                     'FM_FISCAL_DOC_HEADER',
                      L_key);
    close C_EXISTS_WRKSHT;

    --- Check is exists any NF with error status
    if NVL(L_status,'N') <> 'W' then

        SQL_LIB.SET_MARK('OPEN',
                         'C_EXISTS_ERROR',
                         'FM_FISCAL_DOC_HEADER',
                          L_key);
        open C_EXISTS_ERROR;
        ---
        SQL_LIB.SET_MARK('FETCH',
                         'C_EXISTS_ERROR',
                         'FM_FISCAL_DOC_HEADER',
                          L_key);
        fetch C_EXISTS_ERROR into L_dummy;
        if C_EXISTS_ERROR%FOUND then
                L_status := 'E';
        end if;
        ---
        SQL_LIB.SET_MARK('CLOSE',
                         'C_EXISTS_ERROR',
                         'FM_FISCAL_DOC_HEADER',
                          L_key);
        close C_EXISTS_ERROR;

        --- Check is exists any NF with Discrepancy status
        if NVL(L_status,'N') <> 'E' then

                SQL_LIB.SET_MARK('OPEN',
                                 'C_EXISTS_DISP',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                open C_EXISTS_DISP;
                ---
                SQL_LIB.SET_MARK('FETCH',
                                 'C_EXISTS_DISP',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                fetch C_EXISTS_DISP into L_dummy;
                if C_EXISTS_DISP%FOUND then
                        L_status := 'D';
                end if;
                ---
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_EXISTS_DISP',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                close C_EXISTS_DISP;
        end if;
    end if;
    if FM_SCHEDULE_SQL.GET_UTIL_FISCAL_ID (O_error_message,
  	                                       L_utilization_id,
  	                                       L_fiscal_doc_id,
  	                                       I_schedule_no) = FALSE then
       return FALSE;
    end if;
    if FM_UTILIZATION_ATTRIBUTES_SQL.GET_UTILIZATION_ATTRIB_INFO(O_error_message,
		      	                                         L_utilization_attributes,
                                                                 L_utilization_id) = FALSE then

	 return FALSE;
    end if;
   ---
   --- if none of NF is in worksheet, error or disp
   if  NVL(L_status,'N') not in ('W','E','D') and L_utilization_attributes.comp_freight_nf_ind = 'N'  then
          L_status := 'V';
   elsif NVL(L_status,'N') not in ('W','E','D') and L_utilization_attributes.comp_freight_nf_ind = 'Y' then
         L_status := 'A';
   end if;
      ---
    -- update schedule
    if FM_SCHEDULE_VAL_SQL.UPDATE_SCHD_STATUS(O_error_message,
                                              I_schedule_no,
                                              L_status) = FALSE then
	return FALSE;
    end if;

    return TRUE;
    ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_EXISTS_WRKSHT%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_WRKSHT',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_WRKSHT;
         end if;
         ---
         if C_EXISTS_ERROR%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_ERROR',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_ERROR;
         end if;
         ---
         if C_EXISTS_DISP%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_DISP',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_DISP;
         end if;
         ---
         L_status := 'W';
         return FALSE;
   END UPD_SCHED_STATUS;
-----------------------------------------------------------------------------------

-------------------------------------------------------------------------
-- Function Name: UPD_SCHED_STATUS_RMA
-- Purpose:       This function sets the schedule status for rma, based on the various NF status under that schedule.
-----------------------------------------------------------------------------------
FUNCTION UPD_SCHED_STATUS_RMA(O_error_message IN OUT VARCHAR2,
                          I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_FISCAL_DOC_HEADER_SQL.UPD_SCHED_STATUS_RMA';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_dummy   VARCHAR2(1);
      L_status  FM_SCHEDULE.STATUS%TYPE ;
      L_wrksht_status  FM_SCHEDULE.STATUS%TYPE := 'W';
      L_err_status     FM_SCHEDULE.STATUS%TYPE := 'E';
      L_pend_status    FM_SCHEDULE.STATUS%TYPE := 'P';
      L_recv_status    FM_SCHEDULE.STATUS%TYPE := 'R';

      cursor C_EXISTS_WRKSHT is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_wrksht_status ;
      ---
      cursor C_EXISTS_ERROR is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_err_status ;
      ---
      cursor C_EXISTS_PEND is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_pend_status ;
      ---
      cursor C_EXISTS_RECV is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = L_recv_status ;
     ---
   BEGIN
      ---
    if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                                I_schedule_no,
                                                 NULL);
         return FALSE;
    end if;

    --- Check is exists any NF with Worksheet status

    SQL_LIB.SET_MARK('OPEN',
                     'C_EXISTS_WRKSHT',
                     'FM_FISCAL_DOC_HEADER',
                      L_key);
    open C_EXISTS_WRKSHT;
    ---
    SQL_LIB.SET_MARK('FETCH',
                     'C_EXISTS_WRKSHT',
                     'FM_FISCAL_DOC_HEADER',
                      L_key);
    fetch C_EXISTS_WRKSHT into L_dummy;
    if C_EXISTS_WRKSHT%FOUND then
        L_status := 'W';
    end if;
    ---
    SQL_LIB.SET_MARK('CLOSE',
                     'C_EXISTS_WRKSHT',
                     'FM_FISCAL_DOC_HEADER',
                      L_key);
    close C_EXISTS_WRKSHT;

    --- Check is exists any NF with error status
    if NVL(L_status,'N') <> 'W' then

        SQL_LIB.SET_MARK('OPEN',
                         'C_EXISTS_ERROR',
                         'FM_FISCAL_DOC_HEADER',
                          L_key);
        open C_EXISTS_ERROR;
        ---
        SQL_LIB.SET_MARK('FETCH',
                         'C_EXISTS_ERROR',
                         'FM_FISCAL_DOC_HEADER',
                          L_key);
        fetch C_EXISTS_ERROR into L_dummy;
        if C_EXISTS_ERROR%FOUND then
                L_status := 'E';
        end if;
        ---
        SQL_LIB.SET_MARK('CLOSE',
                         'C_EXISTS_ERROR',
                         'FM_FISCAL_DOC_HEADER',
                          L_key);
        close C_EXISTS_ERROR;

        --- Check is exists any NF with Discrepancy status
        if NVL(L_status,'N') <> 'E' then

                SQL_LIB.SET_MARK('OPEN',
                                 'C_EXISTS_PEND',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                open C_EXISTS_PEND;
                ---
                SQL_LIB.SET_MARK('FETCH',
                                 'C_EXISTS_PEND',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                fetch C_EXISTS_PEND into L_dummy;
                if C_EXISTS_PEND%FOUND then
                        L_status := 'V';
                end if;
                ---
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_EXISTS_PEND',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                close C_EXISTS_PEND;
                ---
		    if NVL(L_status,'N') <> 'R' then

                SQL_LIB.SET_MARK('OPEN',
                                 'C_EXISTS_RECV',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                open C_EXISTS_RECV;
                ---
                SQL_LIB.SET_MARK('FETCH',
                                 'C_EXISTS_RECV',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                fetch C_EXISTS_RECV into L_dummy;
                if C_EXISTS_RECV%FOUND then
                        L_status := 'A';
                end if;
                ---
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_EXISTS_RECV',
                                 'FM_FISCAL_DOC_HEADER',
                                  L_key);
                close C_EXISTS_RECV;
        end if;

        end if;
    end if;

    -- if none of NF is in worksheet, error or pending for receiving
    if  NVL(L_status,'N') not in ('W','E','V','A') then
         L_status := 'W';
    end if;
    ---
    -- update schedule
    if FM_SCHEDULE_VAL_SQL.UPDATE_SCHD_STATUS(O_error_message,
                                              I_schedule_no,
                                              L_status) = FALSE then
       return FALSE;
    end if;

    return TRUE;
    ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_EXISTS_WRKSHT%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_WRKSHT',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_WRKSHT;
         end if;
         ---
         if C_EXISTS_ERROR%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_ERROR',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_ERROR;
         end if;
         ---
         if C_EXISTS_PEND%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_PEND',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_PEND;
         end if;
         ---
         if C_EXISTS_RECV%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_RECV',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_RECV;
         end if;
         ---

         L_status := 'W';
         return FALSE;
   END UPD_SCHED_STATUS_RMA;
-----------------------------------------------------------------------------------
FUNCTION UPD_COMP_NF_STATUS(O_error_message IN OUT VARCHAR2,
                            O_fiscal_doc_id IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_status        IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program              VARCHAR2(50) := 'FM_FISCAL_DOC_HEADER_SQL.UPD_COMP_NF_STATUS';
   L_error_message        VARCHAR2(255) := NULL;
   L_nf_comp_ind          FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_exit                 BOOLEAN := FALSE;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_triangulation_ind    VARCHAR2(1) := 'Y';
   L_comp_nf_ind          VARCHAR2(1) := 'N';
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fua.comp_nf_ind
        from fm_fiscal_doc_detail   fdd,
             fm_fiscal_doc_header   fdh,
             fm_utilization_attributes  fua,
             ordhead                    ord
       where fdd.fiscal_doc_id        = I_fiscal_doc_id
         and fdd.fiscal_doc_id        = fdh.fiscal_doc_id
         and fdh.utilization_id       = fua.utilization_id
         and fdd.requisition_no       = ord.order_no
         and ord.triangulation_ind    = L_triangulation_ind;
   ---
   cursor C_FM_TRIANG_OTHER_DOC (P_comp_nf_ind    FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE) is
      select fdc.fiscal_doc_id other_doc_id
        from fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         and P_comp_nf_ind           = L_triangulation_ind
     UNION
      select fdc.compl_fiscal_doc_id other_doc_id
        from fm_fiscal_doc_complement fdc
       where fdc.fiscal_doc_id       = I_fiscal_doc_id
         and P_comp_nf_ind           = L_comp_nf_ind;
   ---
   cursor C_LOCK_FM_FISCAL_HEADER (P_fiscal_doc_id  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_header
       where fiscal_doc_id = P_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_DETAIL', 'fm_fiscal_doc_detail', 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_DETAIL', 'fm_fiscal_doc_detail', 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   fetch C_FM_FISCAL_DOC_DETAIL into L_nf_comp_ind;
   ---
   if C_FM_FISCAL_DOC_DETAIL%NOTFOUND then
    L_exit := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_DETAIL', 'fm_fiscal_doc_detail', 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   close C_FM_FISCAL_DOC_DETAIL;

   if L_exit then
      return TRUE;
   else
      SQL_LIB.SET_MARK('OPEN', 'C_FM_TRIANG_OTHER_DOC', 'fm_fiscal_doc_complement', 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
      open C_FM_TRIANG_OTHER_DOC (L_nf_comp_ind);
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_FM_TRIANG_OTHER_DOC', 'fm_fiscal_doc_complement', 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
      fetch C_FM_TRIANG_OTHER_DOC into O_fiscal_doc_id;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_TRIANG_OTHER_DOC', 'fm_fiscal_doc_complement', 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
      close C_FM_TRIANG_OTHER_DOC;
      ---
      SQL_LIB.SET_MARK('OPEN',
                        'C_LOCK_FM_FISCAL_HEADER',
                        'FM_FISCAL_DOC_HEADER',
                        'fiscal_doc_id: '||TO_CHAR(O_fiscal_doc_id));
      open C_LOCK_FM_FISCAL_HEADER(O_fiscal_doc_id);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_FM_FISCAL_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       'fiscal_doc_id: '||TO_CHAR(O_fiscal_doc_id));
      close C_LOCK_FM_FISCAL_HEADER;
       ---
       SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_FISCAL_DOC_HEADER', 'fiscal_doc_id: '||TO_CHAR(O_fiscal_doc_id));
       ---
       update fm_fiscal_doc_header
         set status              = I_status
       where fiscal_doc_id       = O_fiscal_doc_id;
       ---
    end if;
   ---

   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                             'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id),
                                             TO_CHAR(SQLCODE));
      --
      if C_LOCK_FM_FISCAL_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_HEADER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_DETAIL', 'fm_fiscal_doc_detail', 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      if C_FM_TRIANG_OTHER_DOC%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_TRIANG_OTHER_DOC', 'fm_fiscal_doc_complement', 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
         close C_FM_TRIANG_OTHER_DOC;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPD_COMP_NF_STATUS;
-----------------------------------------------------------------------------------
FUNCTION UPDATE_RMS_ERR_STATUS(O_error_message  IN OUT VARCHAR2,
                               I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                               I_status         IN     FM_FISCAL_DOC_HEADER.RMS_UPD_ERR_IND%TYPE DEFAULT 'N')
   return BOOLEAN is
   ---
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program      VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER_SQL.UPDATE_RMS_ERR_STATUS';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER';
   L_key          VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   ---
   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_LOCK_DOC_HEADER';
   ---
   SQL_LIB.SET_MARK('OPEN',L_cursor,L_table,L_key);
   open C_LOCK_DOC_HEADER;
   SQL_LIB.SET_MARK('CLOSE',L_cursor,L_table,L_key);
   close C_LOCK_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('UPDATE',NULL,L_table,L_key);
   update fm_fiscal_doc_header fdh
      set fdh.rms_upd_err_ind = I_status
    where fdh.fiscal_doc_id  = I_fiscal_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', L_table, L_key);
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', L_table, L_key);
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      return FALSE;
END UPDATE_RMS_ERR_STATUS;
-----------------------------------------------------------------------------------
FUNCTION DEL_FISCAL_DOC_TAX_DETAIL(O_error_message       IN OUT VARCHAR2,
                                   I_fiscal_doc_line_id  IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER_SQL.DEL_FISCAL_DOC_TAX_DETAIL';
   L_cursor    VARCHAR2(100) := 'C_FM_FISCAL_DOC_TAX_DETAIL';
   L_table     VARCHAR2(100) := 'FM_FISCAL_DOC_TAX_DETAIL';
   L_key       VARCHAR2(100) := 'Fiscal_doc_line_id: ' || TO_CHAR(I_fiscal_doc_line_id);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -64);
   ---
   cursor C_FM_FISCAL_DOC_TAX_DETAIL is
      select 1
      from fm_fiscal_doc_tax_detail f
      where f.fiscal_doc_line_id = I_fiscal_doc_line_id
      for update nowait;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_FM_FISCAL_DOC_TAX_DETAIL ;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_FM_FISCAL_DOC_TAX_DETAIL ;
   ---
   SQL_LIB.SET_MARK('DELETE', L_cursor, L_table, L_key);
   delete from fm_fiscal_doc_tax_detail f
   where f.fiscal_doc_line_id = I_fiscal_doc_line_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_TAX_DETAIL',
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_DOC_TAX_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_TAX_DETAIL', L_table, L_key);
         close C_FM_FISCAL_DOC_TAX_DETAIL;
      end if;
      ---
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DEL_FISCAL_DOC_TAX_DETAIL;
------------------------------------------------------------------------------------------------------------

END FM_FISCAL_DOC_HEADER_SQL;
/
