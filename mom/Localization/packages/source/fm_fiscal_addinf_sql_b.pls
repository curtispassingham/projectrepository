CREATE OR REPLACE PACKAGE BODY FM_FISCAL_ADDINF_SQL is
-----------------------------------------------------------------------------------
FUNCTION GET_QUERY_TYPE_TAXES(O_error_message    IN OUT VARCHAR2,
                              O_query            IN OUT VARCHAR2)

   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_ADDINF_SQL.GET_QUERY_TYPE_TAXES';
   ---
BEGIN
   ---
   O_query := 'select vfvc.vat_code, vfvc.vat_code '||
                'from vat_codes vfvc ';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_QUERY_TYPE_TAXES;
-----------------------------------------------------------------------------------
FUNCTION UNIQUE_COMP_DOC (O_error_message     IN OUT VARCHAR2,
                          O_unique            IN OUT BOOLEAN,
                          O_fiscal_doc_id     IN OUT FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE,
                          O_fiscal_doc_no     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_fiscal_doc_id     IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE,
                          I_complementary_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_FISCAL_ADDINF_SQL.UNIQUE_COMP_DOC';
   L_count     NUMBER;
   L_key       VARCHAR2(50);

   cursor C_COUNT_COMPL_DOC is
      select COUNT(*)
        from fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_COUNT_FISCAL_DOC is
      select COUNT(*)
        from fm_fiscal_doc_complement fdc
       where fdc.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_FISCAL_DOC is
      select fdh.fiscal_doc_no, fdc.fiscal_doc_id
        from fm_fiscal_doc_complement fdc, fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = fdc.fiscal_doc_id
         and fdc.compl_fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_DOC_COMPLEMENT is
      select fdh.fiscal_doc_no, fdc.compl_fiscal_doc_id
        from fm_fiscal_doc_complement fdc, fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = fdc.compl_fiscal_doc_id
         and fdc.fiscal_doc_id = I_fiscal_doc_id;
   ---
BEGIN
   ---
   if I_complementary_ind = 'Y' then
      L_key := 'compl_fiscal_doc_id = '||I_fiscal_doc_id;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_COMPL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      open C_COUNT_COMPL_DOC;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_COMPL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      fetch C_COUNT_COMPL_DOC into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_COMPL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      close C_COUNT_COMPL_DOC;
      ---
      if NVL(L_count,0) = 1 then
         O_unique := TRUE;
      else
         O_unique := FALSE;
      end if;
      ---
      O_fiscal_doc_no := NULL;
      O_fiscal_doc_id := NULL;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_FISCAL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      open C_GET_FISCAL_DOC;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_GET_FISCAL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      fetch C_GET_FISCAL_DOC into O_fiscal_doc_no, O_fiscal_doc_id;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      close C_GET_FISCAL_DOC;
      ---
   else
      ---
      L_key := 'fiscal_doc_id = '||I_fiscal_doc_id;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      open C_COUNT_FISCAL_DOC;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      fetch C_COUNT_FISCAL_DOC into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      close C_COUNT_FISCAL_DOC;
      ---
      if NVL(L_count,0) = 1 then
         O_unique := TRUE;
      else
         O_unique := FALSE;
      end if;
      ---
      O_fiscal_doc_no := NULL;
      O_fiscal_doc_id := NULL;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_DOC_COMPLEMENT', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      open C_GET_DOC_COMPLEMENT;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_GET_DOC_COMPLEMENT', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      fetch C_GET_DOC_COMPLEMENT into O_fiscal_doc_no, O_fiscal_doc_id;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_DOC_COMPLEMENT', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      close C_GET_DOC_COMPLEMENT;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END UNIQUE_COMP_DOC;
-----------------------------------------------------------------------------------
FUNCTION LOV_COMP_FISCAL_DOC_NO(O_error_message     IN OUT VARCHAR2,
                                O_query             IN OUT VARCHAR2,
                                I_fiscal_doc_no     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                I_complementary_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                                I_location          IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                I_loc_type          IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                I_requisition_type  IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_ADDINF_SQL.LOV_COMP_FISCAL_DOC_NO';
   ---
   L_pwh       BOOLEAN;
   ---
BEGIN
   ---
   if I_loc_type = 'S' or
      I_requisition_type <> 'PO' then
      -- 001 - Begin
         O_query := 'select fdh.fiscal_doc_id'||
                    ', fdh.fiscal_doc_no' ||
                    ', fdh.series_no' ||
                    ', decode(fdh.module,''SUPP'',(select NVL(s.sup_name_secondary, s.sup_name) from sups s where s.supplier = fdh.key_value_1)' ||
                       ',''PTNR'',(select NVL(p.partner_name_secondary, p.partner_desc) from partner p where p.partner_id = fdh.key_value_1 and p.partner_type = fdh.key_value_2)' ||
                       ',''OLOC'',(select NVL(o.outloc_name_secondary, o.outloc_desc) from outloc o where o.outloc_id = fdh.key_value_1)' ||
                       ',''CUS'',(select u.cust_name from customer u where u.cust_id = fdh.key_value_1)' ||
                       ',''LOC'',(decode(fdh.location_type,''W'',(select NVL(w.wh_name_secondary, w.wh_name) from wh w where w.wh = fdh.key_value_1)' ||
                           ',''S'',(select NVL(s.store_name_secondary, s.store_name) from store s where s.store = fdh.key_value_1)))) social_name' ||
                    ', cd1.code_desc module' ||
                    ', cd2.code_desc key_value_2' ||
                    ', fdh.key_value_1, fdh.issue_date '||
                      'from fm_fiscal_doc_header fdh, fm_utilization_attributes fu, code_detail cd1, code_detail cd2 '||
                     'where fdh.utilization_id = fu.utilization_id '||
                       'and cd1.code_type = ''FIMO'' '||
                       'and cd1.code = fdh.module '||
                       'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'') '||
                       'and cd2.code (+) = fdh.key_value_2 '||
                       'and fu.comp_nf_ind = '''|| I_complementary_ind ||''' '||
                       'and fdh.status != ''I'' '||
                       'and fdh.location_id = '|| I_location ||' '||
                       'and fdh.location_type = '''|| I_loc_type ||''' ';
      -- 001 - End
   elsif I_loc_type = 'W' Then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location) = FALSE then
         return FALSE;
      end if;
      ---
      if L_pwh then
         -- 001 - Begin
         O_query := 'select fdh.fiscal_doc_id' ||
                    ', fdh.fiscal_doc_no' ||
                    ', fdh.series_no' ||
                    ', decode(fdh.module,''SUPP'',(select NVL(s.sup_name_secondary, s.sup_name) from sups s where s.supplier = fdh.key_value_1)' ||
                       ',''PTNR'',(select NVL(p.partner_name_secondary, p.partner_desc) from partner p where p.partner_id = fdh.key_value_1 and p.partner_type = fdh.key_value_2)' ||
                       ',''OLOC'',(select NVL(o.outloc_name_secondary, o.outloc_desc) from outloc o where o.outloc_id = fdh.key_value_1)' ||
                       ',''CUS'',(select u.cust_name from customer u where u.cust_id = fdh.key_value_1)' ||
                       ',''LOC'',(decode(fdh.location_type,''W'',(select NVL(w.wh_name_secondary, w.wh_name) from wh w where w.wh = fdh.key_value_1)' ||
                           ',''S'',(select NVL(s.store_name_secondary, s.store_name) from store s where s.store = fdh.key_value_1)))) social_name' ||
                    ', cd1.code_desc module' ||
                    ', cd2.code_desc key_value_2' ||
                    ', fdh.key_value_1' ||
                    ', fdh.issue_date '||
                      'from fm_fiscal_doc_header fdh, fm_utilization_attributes fu, code_detail cd1, code_detail cd2 ' ||
                     'where fdh.utilization_id = fu.utilization_id '||
                       'and cd1.code_type = ''FIMO'' '||
                       'and cd1.code = fdh.module '||
                       'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'')' ||
                       'and cd2.code (+) = fdh.key_value_2 '||
                       'and fu.comp_nf_ind = '''|| I_complementary_ind ||''' '||
                       'and fdh.status != ''I'' '||
                       'and fdh.location_id IN (select wh.wh from wh where wh.physical_wh = '|| I_location ||') '||
                       'and fdh.location_type = '''|| I_loc_type ||''' ';
         -- 001 - End
      else
         -- 001 - Begin
         O_query := 'select fdh.fiscal_doc_id' ||
                    ', fdh.fiscal_doc_no' ||
                    ', fdh.series_no' ||
                    ', decode(fdh.module,''SUPP'',(select NVL(s.sup_name_secondary, s.sup_name) from sups s where s.supplier = fdh.key_value_1)' ||
                       ',''PTNR'',(select NVL(p.partner_name_secondary, p.partner_desc) from partner p where p.partner_id = fdh.key_value_1 and p.partner_type = fdh.key_value_2)' ||
                       ',''OLOC'',(select NVL(o.outloc_name_secondary, o.outloc_desc) from outloc o where o.outloc_id = fdh.key_value_1)' ||
                       ',''CUS'',(select u.cust_name from customer u where u.cust_id = fdh.key_value_1)' ||
                       ',''LOC'',(decode(fdh.location_type,''W'',(select NVL(w.wh_name_secondary, w.wh_name) from wh w where w.wh = fdh.key_value_1)' ||
                           ',''S'',(select NVL(s.store_name_secondary, s.store_name) from store s where s.store = fdh.key_value_1)))) social_name' ||
                    ', cd1.code_desc module' ||
                    ', cd2.code_desc key_value_2' ||
                    ', fdh.key_value_1' ||
                    ', fdh.issue_date '||
                      'from fm_fiscal_doc_header fdh, fm_utilization_attributes fu, code_detail cd1, code_detail cd2 ' ||
                     'where fdh.utilization_id = fu.utilization_id '||
                       'and cd1.code_type = ''FIMO'' '||
                       'and cd1.code = fdh.module '||
                       'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'') '||
                       'and cd2.code (+) = fdh.key_value_2 '||
                       'and fu.comp_nf_ind = '''|| I_complementary_ind ||''' '||
                       'and fdh.status != ''I'' '||
                       'and fdh.location_id = '|| I_location ||' '||
                       'and fdh.location_type = '''|| I_loc_type ||''' ';
         -- 001 -End
      end if;
   end if;
   if I_fiscal_doc_no is NOT NULL then
      O_query := O_query ||
                 'and fdh.fiscal_doc_no = '||I_fiscal_doc_no||' ';
   end if;
   O_query := O_query ||
               'order by 2';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_COMP_FISCAL_DOC_NO;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_COMP_FISCAL_DOC_NO(O_error_message      IN OUT VARCHAR2,
                                     O_exists             IN OUT BOOLEAN,
                                     O_comp_fiscal_doc_id IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                     I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                     I_complementary_ind  IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                                     I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                     I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                     I_requisition_type  IN      FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_ADDINF_SQL.VALIDATE_COMP_FISCAL_DOC_NO';
   ---
   L_key       VARCHAR2(100);
   L_count     NUMBER;
   L_comp_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_pwh       BOOLEAN;
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select fdh.fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_utilization fu , fm_utilization_attributes fa
       where fdh.utilization_id = fu.utilization_id
         and fu.utilization_id = fa.utilization_id
         and fa.comp_nf_ind = I_complementary_ind
         and ((O_comp_fiscal_doc_id is NOT NULL
         and fdh.fiscal_doc_id = O_comp_fiscal_doc_id)
          or (O_comp_fiscal_doc_id is NULL))
         and fdh.fiscal_doc_no = I_fiscal_doc_no
         and fdh.location_id = I_location
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D');
   ---
   cursor C_FM_FISCAL_DOC_HEADER_VWH is
      select fdh.fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_utilization fu, fm_utilization_attributes fa
       where fdh.utilization_id = fu.utilization_id
         and fu.utilization_id = fa.utilization_id
         and fa.comp_nf_ind = I_complementary_ind
         and ((O_comp_fiscal_doc_id is NOT NULL
         and fdh.fiscal_doc_id = O_comp_fiscal_doc_id)
          or (O_comp_fiscal_doc_id is NULL))
         and fdh.fiscal_doc_no = I_fiscal_doc_no
         and ((fdh.location_id = I_location
         and fdh.location_type = I_loc_type)
          or (fdh.location_id IN (select wh.wh
                                    from wh
                                   where wh.physical_wh = I_location)))
         and fdh.status NOT IN ('I','D');
   ---
   cursor C_COUNT_FISCAL_DOC_ID is
      select COUNT(*)
        from fm_fiscal_doc_header fdh, fm_fiscal_utilization fu, fm_utilization_attributes fa
       where fdh.utilization_id = fu.utilization_id
         and fu.utilization_id = fa.utilization_id
         and fa.comp_nf_ind = I_complementary_ind
         and ((O_comp_fiscal_doc_id is NOT NULL
         and fdh.fiscal_doc_id = O_comp_fiscal_doc_id)
          or (O_comp_fiscal_doc_id is NULL))
         and fdh.fiscal_doc_no = I_fiscal_doc_no
         and fdh.location_id = I_location
         and fdh.location_type = I_loc_type
         and fdh.status NOT IN ('I','D');
   ---
   cursor C_COUNT_FISCAL_DOC_ID_VWH is
      select COUNT(*)
        from fm_fiscal_doc_header fdh, fm_fiscal_utilization fu, fm_utilization_attributes fa
       where fdh.utilization_id = fu.utilization_id
         and fu.utilization_id = fa.utilization_id
         and fa.comp_nf_ind = I_complementary_ind
         and ((O_comp_fiscal_doc_id is NOT NULL
         and fdh.fiscal_doc_id = O_comp_fiscal_doc_id)
          or (O_comp_fiscal_doc_id is NULL))
         and fdh.fiscal_doc_no = I_fiscal_doc_no
         and ((fdh.location_id = I_location
         and fdh.location_type = I_loc_type)
          or (fdh.location_id IN (select wh.wh
                                    from wh
                                   where wh.physical_wh = I_location)))
         and fdh.status NOT IN ('I','D');
   ---
BEGIN
   ---
   if I_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_loc_type = 'S' or
      NOT L_pwh or
      I_requisition_type <> 'PO' then
      ---
      L_key := 'fiscal_doc_no = '||I_fiscal_doc_no ||
              ' location_id = '||TO_CHAR(I_location) ||
              ' location_type = '''||I_loc_type||'''';
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_FM_FISCAL_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_FM_FISCAL_DOC_HEADER into L_comp_fiscal_doc_id;
      if C_FM_FISCAL_DOC_HEADER%NOTFOUND then
         ---
         O_exists := FALSE;
         O_comp_fiscal_doc_id := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_DOC_HEADER;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ID;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ID into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ID;
      ---
   else
      ---
      L_key := 'fiscal_doc_no = '||I_fiscal_doc_no ||
              ' location_id = '||TO_CHAR(I_location) ||
              ' location_type = '''||I_loc_type||'''';
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_FM_FISCAL_DOC_HEADER_VWH;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_FM_FISCAL_DOC_HEADER_VWH into L_comp_fiscal_doc_id;
      if C_FM_FISCAL_DOC_HEADER_VWH%NOTFOUND then
         ---
         O_exists := FALSE;
         O_comp_fiscal_doc_id := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_DOC_HEADER_VWH;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_FM_FISCAL_DOC_HEADER_VWH;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ID_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ID_VWH;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ID_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ID_VWH into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ID_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ID_VWH;
      ---
   end if;
   ---
   if NVL(L_count,0) > 1 then
      O_comp_fiscal_doc_id := NULL;
   else
      O_comp_fiscal_doc_id := L_comp_fiscal_doc_id;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_COMP_FISCAL_DOC_NO;
-----------------------------------------------------------------------------------
/*
FUNCTION VALIDATE_LEGAL_MESSAGE(O_error_message      IN OUT VARCHAR2,
                                O_exists             IN OUT BOOLEAN,
                                O_message_id         IN OUT FM_LEGAL_MESSAGE.MESSAGE_ID%TYPE,
                                I_message_text       IN     FM_LEGAL_MESSAGE.MESSAGE_TEXT%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_ADDINF_SQL.VALIDATE_LEGAL_MESSAGE';
   ---
   cursor C_FM_LEGAL_MESSAGE is
      select lm.message_id
        from fm_legal_message lm
       where UPPER(lm.message_text) = UPPER(I_message_text)
         and lm.status <> 'I';
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_LEGAL_MESSAGE', 'FM_LEGAL_MESSAGE', NULL);
   open C_FM_LEGAL_MESSAGE;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_FM_LEGAL_MESSAGE', 'FM_LEGAL_MESSAGE', NULL);
   fetch C_FM_LEGAL_MESSAGE into O_message_id;
   O_exists := C_FM_LEGAL_MESSAGE%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_LEGAL_MESSAGE', 'FM_LEGAL_MESSAGE', NULL);
   close C_FM_LEGAL_MESSAGE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_LEGAL_MESSAGE;
-----------------------------------------------------------------------------------
FUNCTION GET_NEXT_MESSAGE_LINE(O_error_message IN OUT VARCHAR2,
                               O_message_line  IN OUT FM_FISCAL_DOC_LEGAL_MESSAGE.MESSAGE_LINE%TYPE,
                               I_fiscal_doc_id IN     FM_FISCAL_DOC_LEGAL_MESSAGE.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_ADDINF_SQL.GET_NEXT_MESSAGE_LINE';
   ---
   L_key       VARCHAR2(100);
   ---
   cursor C_FM_FISCAL_DOC_LEGAL_MESSAGE is
      select NVL(MAX(dlm.message_line),0) + 1
        from fm_fiscal_doc_legal_message dlm
       where dlm.fiscal_doc_id = I_fiscal_doc_id;
   ---
BEGIN
   ---
   L_key := 'fiscal_doc_id = '||I_fiscal_doc_id;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_LEGAL_MESSAGE', 'FM_FISCAL_DOC_LEGAL_MESSAGE', L_key);
   open C_FM_FISCAL_DOC_LEGAL_MESSAGE;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_LEGAL_MESSAGE', 'FM_FISCAL_DOC_LEGAL_MESSAGE', L_key);
   fetch C_FM_FISCAL_DOC_LEGAL_MESSAGE into O_message_line;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_LEGAL_MESSAGE', 'FM_FISCAL_DOC_LEGAL_MESSAGE', L_key);
   close C_FM_FISCAL_DOC_LEGAL_MESSAGE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_NEXT_MESSAGE_LINE;
-----------------------------------------------------------------------------------
FUNCTION LOV_LEGAL_MESSAGE(O_error_message  IN OUT VARCHAR2,
                           O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_ADDINF_SQL.LOV_LEGAL_MESSAGE';
   ---
BEGIN
   ---
   O_query := 'select message_text, message_id '||
                'from fm_legal_message '||
               'where status <> ''I'' '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_LEGAL_MESSAGE;
*/
-----------------------------------------------------------------------------------
FUNCTION CHANGE_COMPL_DOC(O_error_message       IN OUT VARCHAR2,
                          I_complementary_ind   IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                          I_compl_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE,
                          I_fiscal_doc_id       IN     FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program     VARCHAR2(50) := 'FM_FISCAL_ADDINF_SQL.CHANGE_COMPL_DOC';
   ---
   L_key         VARCHAR2(200);
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_COMPL_DOC_ID is
      select 'x'
        from fm_fiscal_doc_complement fdc
       where fdc.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
   cursor C_LOCK_FISCAL_DOC_ID is
      select 'x'
        from fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   if I_complementary_ind = 'N' then
      L_key := 'compl_fiscal_doc_id = '|| TO_CHAR(I_compl_fiscal_doc_id);
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COMPL_DOC_ID', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      open C_LOCK_COMPL_DOC_ID;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COMPL_DOC_ID', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      close C_LOCK_COMPL_DOC_ID;
      ---
      SQL_LIB.SET_MARK('DELETE', 'FM_FISCAL_DOC_COMPLEMENT', NULL, NULL);
      delete from fm_fiscal_doc_complement fdc
       where fdc.fiscal_doc_id = I_fiscal_doc_id;
      ---
   else
      L_key := 'fiscal_doc_id = '|| TO_CHAR(I_fiscal_doc_id);
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_FISCAL_DOC_ID', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      open C_LOCK_FISCAL_DOC_ID;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_FISCAL_DOC_ID', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
      close C_LOCK_FISCAL_DOC_ID;
      ---
      SQL_LIB.SET_MARK('DELETE', 'FM_FISCAL_DOC_COMPLEMENT', NULL, NULL);
      delete from fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id;
      ---
   end if;
   ---
   if I_compl_fiscal_doc_id is NOT NULL and I_fiscal_doc_id is NOT NULL then
      if I_complementary_ind = 'Y' then
         SQL_LIB.SET_MARK('INSERT', 'FM_FISCAL_DOC_COMPLEMENT', NULL, NULL);
         insert into fm_fiscal_doc_complement (compl_fiscal_doc_id,
                                               fiscal_doc_id,
                                               create_datetime,
                                               create_id,
                                               last_update_datetime,
                                               last_update_id)
                                        values(I_fiscal_doc_id,
                                               I_compl_fiscal_doc_id,
                                               SYSDATE,
                                               USER,
                                               SYSDATE,
                                               USER);
      else
         SQL_LIB.SET_MARK('INSERT', 'FM_FISCAL_DOC_COMPLEMENT', NULL, NULL);
         insert into fm_fiscal_doc_complement (compl_fiscal_doc_id,
                                               fiscal_doc_id,
                                               create_datetime,
                                               create_id,
                                               last_update_datetime,
                                               last_update_id)
                                        values(I_compl_fiscal_doc_id,
                                               I_fiscal_doc_id,
                                               SYSDATE,
                                               USER,
                                               SYSDATE,
                                               USER);
      end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_COMPLEMENT',
                                            'compl_fiscal_doc_id = '|| TO_CHAR(I_compl_fiscal_doc_id),
                                            'fiscal_doc_id = '|| TO_CHAR(I_fiscal_doc_id));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHANGE_COMPL_DOC;
-----------------------------------------------------------------------------------
FUNCTION LOV_PERC_VALUE(O_error_message  IN OUT VARCHAR2,
                        O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_ADDINF_SQL.LOV_PERC_VALUE';
   ---
BEGIN
   ---
   O_query := 'select code_desc name, code '||
                'from code_detail '||
               'where code_type = ''FMVP'' '||
               'order by code_seq';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_PERC_VALUE;
-----------------------------------------------------------------------------------
FUNCTION LOV_COMP_FREIGHT_FISCAL_DOC_NO(O_error_message     IN OUT VARCHAR2,
                                        O_query             IN OUT VARCHAR2,
                                        I_fiscal_doc_no     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                        I_location          IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                        I_loc_type          IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                        I_requisition_type  IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(200) := 'FM_FISCAL_ADDINF_SQL.LOV_COMP_FREIGHT_FISCAL_DOC_NO';
   ---
   L_pwh       BOOLEAN;
   ---
BEGIN
   ---
   if I_loc_type = 'S' or
      I_requisition_type <> 'PO' then
      -- 001 - Begin
         O_query := 'select fdh.fiscal_doc_id'||
                    ', fdh.fiscal_doc_no' ||
                    ', fdh.series_no' ||
                    ', decode(fdh.module,''SUPP'',(select NVL(s.sup_name_secondary, s.sup_name) from sups s where s.supplier = fdh.key_value_1)' ||
                       ',''PTNR'',(select NVL(p.partner_name_secondary, p.partner_desc) from partner p where p.partner_id = fdh.key_value_1 and p.partner_type = fdh.key_value_2)' ||
                       ',''OLOC'',(select NVL(o.outloc_name_secondary, o.outloc_desc) from outloc o where o.outloc_id = fdh.key_value_1)' ||
                       ',''CUS'',(select u.cust_name from customer u where u.cust_id = fdh.key_value_1)' ||
                       ',''LOC'',(decode(fdh.location_type,''W'',(select NVL(w.wh_name_secondary, w.wh_name) from wh w where w.wh = fdh.key_value_1)' ||
                           ',''S'',(select NVL(s.store_name_secondary, s.store_name) from store s where s.store = fdh.key_value_1)))) social_name' ||
                    ', cd1.code_desc module' ||
                    ', cd2.code_desc key_value_2' ||
                    ', fdh.key_value_1, fdh.issue_date '||
                      'from fm_fiscal_doc_header fdh, fm_utilization_attributes fu, code_detail cd1, code_detail cd2 '||
                     'where fdh.utilization_id = fu.utilization_id '||
                       'and cd1.code_type = ''FIMO'' '||
                       'and cd1.code = fdh.module '||
                       'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'') '||
                       'and cd2.code (+) = fdh.key_value_2 '||
                       'and fu.comp_freight_nf_ind = ''N'' '||
                       'and fdh.status != ''I'' '||
                       'and fdh.location_id = '|| I_location ||' '||
                       'and fdh.location_type = '''|| I_loc_type ||''' ';
      -- 001 - End
   elsif I_loc_type = 'W' Then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location) = FALSE then
         return FALSE;
      end if;
      ---
      if L_pwh then
         -- 001 - Begin
         O_query := 'select fdh.fiscal_doc_id' ||
                    ', fdh.fiscal_doc_no' ||
                    ', fdh.series_no' ||
                    ', decode(fdh.module,''SUPP'',(select NVL(s.sup_name_secondary, s.sup_name) from sups s where s.supplier = fdh.key_value_1)' ||
                       ',''PTNR'',(select NVL(p.partner_name_secondary, p.partner_desc) from partner p where p.partner_id = fdh.key_value_1 and p.partner_type = fdh.key_value_2)' ||
                       ',''OLOC'',(select NVL(o.outloc_name_secondary, o.outloc_desc) from outloc o where o.outloc_id = fdh.key_value_1)' ||
                       ',''CUS'',(select u.cust_name from customer u where u.cust_id = fdh.key_value_1)' ||
                       ',''LOC'',(decode(fdh.location_type,''W'',(select NVL(w.wh_name_secondary, w.wh_name) from wh w where w.wh = fdh.key_value_1)' ||
                           ',''S'',(select NVL(s.store_name_secondary, s.store_name) from store s where s.store = fdh.key_value_1)))) social_name' ||
                    ', cd1.code_desc module' ||
                    ', cd2.code_desc key_value_2' ||
                    ', fdh.key_value_1' ||
                    ', fdh.issue_date '||
                      'from fm_fiscal_doc_header fdh, fm_utilization_attributes fu, code_detail cd1, code_detail cd2 ' ||
                     'where fdh.utilization_id = fu.utilization_id '||
                       'and cd1.code_type = ''FIMO'' '||
                       'and cd1.code = fdh.module '||
                       'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'')' ||
                       'and cd2.code (+) = fdh.key_value_2 '||
                       'and fu.comp_freight_nf_ind = ''N'' '||
                       'and fdh.status != ''I'' '||
                       'and fdh.location_id IN (select wh.wh from wh where wh.physical_wh = '|| I_location ||') '||
                       'and fdh.location_type = '''|| I_loc_type ||''' ';
         -- 001 - End
      else
         -- 001 - Begin
         O_query := 'select fdh.fiscal_doc_id' ||
                    ', fdh.fiscal_doc_no' ||
                    ', fdh.series_no' ||
                    ', decode(fdh.module,''SUPP'',(select NVL(s.sup_name_secondary, s.sup_name) from sups s where s.supplier = fdh.key_value_1)' ||
                       ',''PTNR'',(select NVL(p.partner_name_secondary, p.partner_desc) from partner p where p.partner_id = fdh.key_value_1 and p.partner_type = fdh.key_value_2)' ||
                       ',''OLOC'',(select NVL(o.outloc_name_secondary, o.outloc_desc) from outloc o where o.outloc_id = fdh.key_value_1)' ||
                       ',''CUS'',(select u.cust_name from customer u where u.cust_id = fdh.key_value_1)' ||
                       ',''LOC'',(decode(fdh.location_type,''W'',(select NVL(w.wh_name_secondary, w.wh_name) from wh w where w.wh = fdh.key_value_1)' ||
                           ',''S'',(select NVL(s.store_name_secondary, s.store_name) from store s where s.store = fdh.key_value_1)))) social_name' ||
                    ', cd1.code_desc module' ||
                    ', cd2.code_desc key_value_2' ||
                    ', fdh.key_value_1' ||
                    ', fdh.issue_date '||
                      'from fm_fiscal_doc_header fdh, fm_utilization_attributes fu, code_detail cd1, code_detail cd2 ' ||
                     'where fdh.utilization_id = fu.utilization_id '||
                       'and cd1.code_type = ''FIMO'' '||
                       'and cd1.code = fdh.module '||
                       'and cd2.code_type (+) = decode(fdh.module,''SUPP'',''-1'',''PTNR'',''PTAL'',''LOC'',''LOC4'',''OLOC'',''LOCT'',''-1'') '||
                       'and cd2.code (+) = fdh.key_value_2 '||
                       'and fu.comp_freight_nf_ind = ''N'' '||
                       'and fdh.status != ''I'' '||
                       'and fdh.location_id = '|| I_location ||' '||
                       'and fdh.location_type = '''|| I_loc_type ||''' ';
         -- 001 -End
      end if;
   end if;
   if I_fiscal_doc_no is NOT NULL then
      O_query := O_query ||
                 'and fdh.fiscal_doc_no = '||I_fiscal_doc_no||' ';
   end if;
   O_query := O_query ||
               'order by 2';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_COMP_FREIGHT_FISCAL_DOC_NO;
-----------------------------------------------------------------------------------
END FM_FISCAL_ADDINF_SQL;
/