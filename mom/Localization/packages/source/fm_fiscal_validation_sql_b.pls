CREATE OR REPLACE PACKAGE BODY FM_FISCAL_VALIDATION_SQL is
-----------------------------------------------------------------------------------
FUNCTION PROCESS_VALIDATION(O_error_message    IN OUT VARCHAR2,
                            O_status           IN OUT INTEGER,
                            I_fiscal_doc_id    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program       VARCHAR2(100) := 'FM_FISCAL_VALIDATION_SQL.PROCESS_VALIDATION';
   O_line_tax          "RIB_TaxDetRBO_TBL" :=  "RIB_TaxDetRBO_TBL"();
   O_other_proc    BOOLEAN := FALSE;
   L_comp_nf_ind   FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_triang_ind    ORDHEAD.TRIANGULATION_IND%TYPE;
   L_main_nf       FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   L_appr          BOOLEAN := TRUE;
   L_number        NUMBER;
   ---
    cursor C_FM_TAX_DETAIL is
   SELECT 1
       FROM fm_fiscal_doc_detail ffdd
      WHERE ffdd.fiscal_doc_id = I_fiscal_doc_id
        AND EXISTS (SELECT 1
                      FROM fm_fiscal_doc_tax_detail ffdt
                     WHERE ffdd.fiscal_doc_line_id = ffdt.fiscal_doc_line_id)
        UNION
        SELECT 1
       FROM fm_fiscal_doc_detail ffdd,
       fm_fiscal_doc_complement ffdc
      WHERE  ffdc.compl_fiscal_doc_id = ffdd.fiscal_doc_id
        and  ffdc.fiscal_doc_id = I_fiscal_doc_id
        AND EXISTS (SELECT 1
                      FROM fm_fiscal_doc_tax_detail ffdt
                     WHERE ffdd.fiscal_doc_line_id = ffdt.fiscal_doc_line_id)
        UNION
        SELECT 1
       FROM fm_fiscal_doc_detail ffdd,
       fm_fiscal_doc_complement ffdc
      WHERE  ffdc.fiscal_doc_id = ffdd.fiscal_doc_id
      and ffdc.compl_fiscal_doc_id = I_fiscal_doc_id
        AND EXISTS (SELECT 1
                      FROM fm_fiscal_doc_tax_detail ffdt
                     WHERE ffdd.fiscal_doc_line_id = ffdt.fiscal_doc_line_id);
   ---
   cursor C_FM_TAX_HEADER is
     SELECT 1
        FROM fm_fiscal_doc_tax_head ffht
       WHERE ffht.fiscal_doc_id = I_fiscal_doc_id
       UNION
       SELECT 1
       FROM fm_fiscal_doc_tax_head ffht1,
       fm_fiscal_doc_complement ffdc
       where ffdc.compl_fiscal_doc_id = ffht1.fiscal_doc_id
       and ffdc.fiscal_doc_id = I_fiscal_doc_id
       UNION
       SELECT 1
       FROM fm_fiscal_doc_tax_head ffht1,
       fm_fiscal_doc_complement ffdc
       where ffdc.fiscal_doc_id = ffht1.fiscal_doc_id
       and ffdc.compl_fiscal_doc_id = I_fiscal_doc_id;
   ---
   ---
BEGIN
   O_status := 1;
   ---
   if (CHECK_DATA_INTEGRITY(O_error_message, O_status, O_other_proc, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   elsif O_status = 0 then
      return TRUE;
   end if;
   ---
   O_other_proc := FALSE;
   if (CALC_TOTAL_DISCOUNT(O_error_message, O_status, O_other_proc, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   elsif O_status = 0 then
      return TRUE;
   end if;
   ---
   O_other_proc := FALSE;
   if (CALC_UNIT_ITEM_COST_COMP(O_error_message, O_status, O_other_proc, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   elsif O_status = 0 then
      return TRUE;
   end if;
   ---
   if (GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;
   ---
   if (L_triang_ind = 'Y' and L_comp_nf_ind = 'Y') then
      ---
      if (GET_OTHER_NF(O_error_message, L_main_nf, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      L_fiscal_doc_id := L_main_nf;
   else
      L_fiscal_doc_id := I_fiscal_doc_id;
      ---
   end if;
   ---
   if (FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(O_line_tax, O_error_message, O_status, L_fiscal_doc_id, null, null, null)= FALSE) then
      return FALSE;
   elsif O_status = 0 then
      return TRUE;
   end if;
   ---
   O_other_proc := FALSE;
   if (UPDATE_CALC_VALUES(O_error_message, O_status, O_other_proc, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   elsif O_status = 0 then
      return TRUE;
   end if;
   ---
   --sid new
   open C_FM_TAX_HEADER;
   ---
   fetch C_FM_TAX_HEADER into L_number;
   ---
   if C_FM_TAX_HEADER%NOTFOUND then
      L_appr := FALSE;
   end if;
   ---
   if L_appr then
   ---
      close C_FM_TAX_HEADER;
      ---
      open C_FM_TAX_DETAIL;
      ---
      fetch C_FM_TAX_DETAIL into L_number;
      ---
      if C_FM_TAX_DETAIL%FOUND then
         L_appr := FALSE;
      end if;
      ---
      close C_FM_TAX_DETAIL;
   ---
   end if;
   ---
   if L_appr then
      ---
      O_other_proc := FALSE;
      if (FM_DISCREP_IDENTIFICATION_SQL.CALC_LINE_TAXES(O_error_message, O_status, O_other_proc, I_fiscal_doc_id) = FALSE) then
         O_status := 0;
         return (FALSE);
      end if;
      ---
   end if;
   ---
   --sid new
   O_other_proc := FALSE;
   if (CHECK_VALUE_INFORMED(O_error_message, O_status, O_other_proc, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   elsif O_status = 0 then
      return TRUE;
   end if;
   ---
   O_other_proc := FALSE;
   if (CHECK_VALUE_INFORMED_VS_CALC(O_error_message, O_status, O_other_proc, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   elsif O_status = 0 then
      return TRUE;
   end if;
   ---
   if L_triang_ind = 'N' or (L_triang_ind = 'Y' and L_comp_nf_ind = 'N') then
   ---
      if (FM_DISCREP_IDENTIFICATION_SQL.PROCESS_DISCREP_IDEN(O_error_message, O_status, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
   ---
   elsif (L_triang_ind = 'Y' and L_comp_nf_ind = 'Y') then
   ---
      if (GET_OTHER_NF(O_error_message, L_main_nf, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      if L_main_nf is NOT NULL then
          if (FM_DISCREP_IDENTIFICATION_SQL.PROCESS_DISCREP_IDEN(O_error_message, O_status, L_main_nf) = FALSE) then
             return FALSE;
          end if;
      end if;
      ---
      ---
   end if;
   ---
   return TRUE;
---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END PROCESS_VALIDATION;
--------------------------------------------------------------------------------
FUNCTION GET_TRIANG_IND(O_error_message IN OUT VARCHAR2,
                        O_comp_nf_ind      OUT VARCHAR2,
                        O_triang_ind       OUT VARCHAR2,
                        I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program              VARCHAR2(50) := 'FM_FISCAL_VALIDATION_SQL.GET_TRIANG_IND';
   L_table                VARCHAR2(30);
   L_key                  VARCHAR2(100);
   L_triang_ind           VARCHAR2(1);
   ---
   cursor C_GET_TRIANG_IND is
      select fua.comp_nf_ind, ord.triangulation_ind
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_utilization_attributes fua,
             ordhead ord
       where fdh.fiscal_doc_id     = fdd.fiscal_doc_id
         and fdd.requisition_no    = ord.order_no
         and fua.utilization_id    = fdh.utilization_id
         and fdh.fiscal_doc_id     = I_fiscal_doc_id;
   ---
BEGIN
   ---
   L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
   L_table := 'FM_FISCAL_DOC_HEADER';
   O_triang_ind := 'N';
   ---
   open C_GET_TRIANG_IND;
   ---
   LOOP
   ---
       fetch C_GET_TRIANG_IND into O_comp_nf_ind, L_triang_ind;
       ---
       EXIT when C_GET_TRIANG_IND%NOTFOUND;
       ---
       if L_triang_ind = 'Y' then
          O_triang_ind := 'Y';
       end if;
       ---
   ---
   END LOOP;
   ---
   close C_GET_TRIANG_IND;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      if C_GET_TRIANG_IND%ISOPEN then
         close C_GET_TRIANG_IND;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_TRIANG_IND;
--------------------------------------------------------------------------------
 FUNCTION GET_OTHER_NF (O_error_message IN OUT VARCHAR2,
                        O_other_nf_id      OUT VARCHAR2,
                        I_comp_nf_ind   IN     VARCHAR2,
                        I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program              VARCHAR2(50) := 'FM_FISCAL_VALIDATION_SQL.GET_OTHER_NF';
   L_table                VARCHAR2(30);
   L_key                  VARCHAR2(100);
   L_yes                  VARCHAR2(1) := 'Y';
   L_no                   VARCHAR2(1) := 'N';
   ---

   cursor C_GET_OTHER_NF_ID is
      select fdc.fiscal_doc_id other_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_header fdh1
       where fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
          and ord.triangulation_ind   = L_yes
         and I_comp_nf_ind           = L_yes
         and fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         and nvl(fdh.schedule_no,-999) = nvl(fdh1.schedule_no,-999)
         and fdh.fiscal_doc_id = fdc.fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdh1.fiscal_doc_id
         and fdc.fiscal_doc_id is NOT NULL
       UNION ALL
      select fdc.compl_fiscal_doc_id other_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_header fdh1,
             ordhead ord
       where fdc.compl_fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
       and ord.triangulation_ind   = L_yes
         and I_comp_nf_ind           = L_no
         and fdc.fiscal_doc_id       = I_fiscal_doc_id
         and nvl(fdh.schedule_no,-999) = nvl(fdh1.schedule_no,-999)
         and fdh.fiscal_doc_id = fdc.fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdh1.fiscal_doc_id
         and fdc.fiscal_doc_id is NOT NULL;
   ---
BEGIN
   ---
   L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
   L_table := 'FM_FISCAL_DOC_COMPLEMENT';
   ---
   open C_GET_OTHER_NF_ID;
   ---
   fetch C_GET_OTHER_NF_ID into O_other_nf_id;
   ---
   close C_GET_OTHER_NF_ID;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      if C_GET_OTHER_NF_ID%ISOPEN then
         close C_GET_OTHER_NF_ID;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_OTHER_NF;
--------------------------------------------------------------------------------
FUNCTION CHECK_DATA_INTEGRITY(O_error_message IN OUT VARCHAR2,
                              O_status        IN OUT INTEGER,
                              O_other_proc    IN OUT BOOLEAN,
                              I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                              I_form_ind      IN     VARCHAR2 DEFAULT 'Y')
   return BOOLEAN is
   ---
   L_program              VARCHAR2(50) := 'FM_FISCAL_VALIDATION_SQL.CHECK_DATA_INTEGRITY';
   L_table                VARCHAR2(30);
   L_key                  VARCHAR2(100);
   L_error_message        VARCHAR2(255) := NULL;
   L_dummy                VARCHAR2(1);
   L_exists               BOOLEAN;
   L_ref_exists           BOOLEAN;
   L_triang_po_exists     BOOLEAN := FALSE;
   L_utilization_desc     FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE;
   L_key_value_1_desc     VARCHAR2(250);
   L_count                NUMBER;
   L_cnt                NUMBER;
   L_detail_count         NUMBER;
   L_dum                  VARCHAR2(1) := 'A';
   L_yes                  VARCHAR2(1) := 'Y';
   L_vat_code             FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE;
   L_fiscal_doc_line_id   FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_ref_line_id          FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID_REF%TYPE;
   L_unit_cost            FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_total_cost           FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   L_payment_date         FM_FISCAL_DOC_PAYMENTS.PAYMENT_DATE%TYPE;
   L_nop                  FM_FISCAL_UTILIZATION.NOP%TYPE;
   L_requisition_type     FM_FISCAL_UTILIZATION.REQUISITION_TYPE%TYPE;
   L_status               FM_FISCAL_UTILIZATION.STATUS%TYPE;
   L_mode_type            FM_FISCAL_UTILIZATION.MODE_TYPE%TYPE;
   L_total_value_pay      FM_FISCAL_DOC_HEADER.TOTAL_DOC_VALUE%TYPE;
   L_item                 FM_FISCAL_DOC_DETAIL.ITEM%TYPE;
   L_subseries_no_ind     FM_FISCAL_DOC_TYPE.SUBSERIES_NO_IND%TYPE;
   L_req_no               FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
   L_pack_no              FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE;
   L_pack_ind             FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE;
   L_triang_ind           ORDHEAD.TRIANGULATION_IND%TYPE;
   L_del_supp             ORDHEAD.DELIVERY_SUPPLIER%TYPE;
   L_supplier             ORDHEAD.SUPPLIER%TYPE;
   L_other_fiscal_doc_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE;
   L_form_ind             NUMBER(1);

   L_head_total_value      FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
   L_detail_total_value	   FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE;
   L_tax_code 		   FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE;

   L_dif_perc                    NUMBER;
   L_description                 FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_number_value                FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value                FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value                  FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_div_value                   NUMBER;
   L_calc_tol_type    FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_calc_tol_val     FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select fdh.fiscal_doc_no,
             fdh.series_no,
             fdh.location_id,
             fdh.location_type,
             fdh.schedule_no,
             decode(nvl(fdh.schedule_no,-999),-999,'ENT',fs.mode_type) mode_type,
             fdh.module,
             fdh.requisition_type,
             fdh.key_value_1,
             fdh.key_value_2,
             fdh.total_serv_value,
             fdh.total_item_value,
             fdh.total_doc_value,
             fdh.type_id,
             fdh.utilization_id,
             fdh.issue_date,
             fdh.entry_or_exit_date,
             fdh.exit_hour,
             fdh.partner_type,
             fdh.partner_id,
             fdh.freight_type,
             fua.comp_nf_ind,
             fdh.subseries_no
        from fm_fiscal_doc_header fdh, fm_schedule fs, fm_utilization_attributes fua
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         and fs.schedule_no(+) = fdh.schedule_no
         and fdh.utilization_id = fua.utilization_id;
   ---
   R_fiscal_header C_FM_FISCAL_DOC_HEADER%ROWTYPE;
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id, fdd.unit_cost, fdd.total_cost, fdd.item
             , fdd.requisition_no, oh.triangulation_ind, oh.delivery_supplier, oh.supplier, fdd.pack_ind, fdd.pack_no
        from fm_fiscal_doc_detail fdd
             , ordhead oh
       where fdd.fiscal_doc_id  = I_fiscal_doc_id
         and fdd.requisition_no = oh.order_no;
   ---
   cursor C_FM_FISCAL_DETAIL_UNEXPECTED is
      select fdd.fiscal_doc_line_id, fdd.unit_cost, fdd.total_cost, fdd.item
             ,  fdd.pack_ind, fdd.pack_no
        from fm_fiscal_doc_detail fdd
         where fdd.fiscal_doc_id  = I_fiscal_doc_id
         and fdd.unexpected_item = 'Y' ;

   --triang
   cursor C_GET_REF_LINE_ID (P_fiscal_doc_id               FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                             P_req_no                      FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                             P_item                        FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                             P_pack_ind                    FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                             P_pack_no                     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE) is
      select b.fiscal_doc_line_id
        from fm_fiscal_doc_detail b
       where b.fiscal_doc_id   = P_fiscal_doc_id
         and b.requisition_no  = P_req_no
         and b.item            = P_item
         and b.pack_ind        = P_pack_ind
         and NVL(b.pack_no,L_dum)= NVL(P_pack_no,L_dum);

   cursor C_GET_REF_LINE_ID_UNEXPECTED (P_fiscal_doc_id               FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                             P_item                        FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                             P_pack_ind                    FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                             P_pack_no                     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE) is
      select b.fiscal_doc_line_id
        from fm_fiscal_doc_detail b
       where b.fiscal_doc_id   = P_fiscal_doc_id
         and b.item            = P_item
         and b.pack_ind        = P_pack_ind
         and NVL(b.pack_no,L_dum)= NVL(P_pack_no,L_dum);
   ---
   --triang
   cursor C_FM_FISCAL_DOC_COMPLEMENT is
      select 'X'
        from fm_fiscal_doc_complement fdc
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id;

        cursor C_FM_FISCAL_DOC_COMPLEMENT_CNT is
      select count(*)
        from fm_fiscal_doc_complement fdc
       where fdc.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_FM_COMP_FISCAL_ID (P_compl_fiscal_doc_id        FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE) is
      select fdc.fiscal_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.compl_fiscal_doc_id = P_compl_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and fdc.fiscal_doc_id is NOT NULL;

    cursor C_FM_COMP_FISCAL_ID_SCHED (P_fiscal_doc_id        FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE) is
    select 'X'
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord,
             fm_fiscal_doc_header ffdh,
             fm_fiscal_doc_header ffdh1
       where fdc.fiscal_doc_id = P_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = 'Y'
         and ffdh.schedule_no <> ffdh1.schedule_no
         and ffdh.schedule_no is not null
         and ffdh1.schedule_no is not null
         and ffdh.fiscal_doc_id = P_fiscal_doc_id
         and ffdh1.fiscal_doc_id = fdc.compl_fiscal_doc_id
         and fdc.compl_fiscal_doc_id is NOT NULL;

    cursor C_FM_FISCAL_ID_SCHED_STATUS (P_fiscal_doc_id        FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE) is
     select 'X'
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord,
             fm_fiscal_doc_header ffdh,
             fm_fiscal_doc_header ffdh1
       where fdc.fiscal_doc_id = ffdh.fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = 'Y'
         and ffdh.schedule_no = ffdh1.schedule_no
         and ffdh.schedule_no is not null
         and ffdh1.schedule_no is not null
         and ffdh.fiscal_doc_id = P_fiscal_doc_id
         and ffdh1.fiscal_doc_id = fdc.compl_fiscal_doc_id
         and ffdh.status <> ffdh1.status
         and fdc.compl_fiscal_doc_id is NOT NULL;

    cursor C_FM_FISCAL_ID_SCHED (P_fiscal_doc_id        FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE) is
    select fdc.compl_fiscal_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord,
             fm_fiscal_doc_header ffdh,
             fm_fiscal_doc_header ffdh1
       where fdc.fiscal_doc_id = P_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = 'Y'
         and ffdh.schedule_no = ffdh1.schedule_no
         and ffdh.schedule_no is not null
         and ffdh1.schedule_no is not null
         and ffdh.fiscal_doc_id = P_fiscal_doc_id
         and ffdh1.fiscal_doc_id = fdc.compl_fiscal_doc_id
         and fdc.compl_fiscal_doc_id is NOT NULL;
   ---
   cursor C_FM_COMP_FISCAL_ID_COMP (P_fiscal_doc_id        FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE) is
      select fdc.compl_fiscal_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.fiscal_doc_id       = P_fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and fdc.compl_fiscal_doc_id is NOT NULL;
   ---
   cursor C_MATCH_TRIANGULATION (P_fiscal_doc_id     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE) is
      select count(1)
        from fm_fiscal_doc_header aa, fm_fiscal_doc_header bb
       where aa.fiscal_doc_id   = I_fiscal_doc_id
         and bb.fiscal_doc_id   = P_fiscal_doc_id
         and nvl(aa.schedule_no,-999) = nvl(bb.schedule_no,-999)
         and (aa.total_serv_value <>   bb.total_serv_value or
              aa.total_item_value <>   bb.total_item_value or
              aa.total_doc_value  <>   bb.total_doc_value);
   ---
   cursor C_MATCH_LINE_VALUES (P_fiscal_doc_id      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                               P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select count(1)
        from fm_fiscal_doc_detail a, fm_fiscal_doc_detail b
       where a.fiscal_doc_id   = I_fiscal_doc_id
         and b.fiscal_doc_id   = P_fiscal_doc_id
         and a.fiscal_doc_line_id  = P_fiscal_doc_line_id
         and a.requisition_no = b.requisition_no
         and a.item = b.item
         and a.pack_ind = b.pack_ind
         and nvl(a.pack_no,1) = nvl(b.pack_no,1)
         and (a.quantity   <>   b.quantity   or
              a.unit_cost  <>   b.unit_cost  or
              a.total_calc_cost   <>   b.total_calc_cost);
   ---
   cursor C_FM_FISCAL_DOC_TAX_HEAD is
      select fdth.vat_code
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id
         and fdth.tax_basis < 0;
   ---
   cursor C_FM_FISCAL_DOC_TAX_DETAIL is
      select fdtd.vat_code, fdtd.fiscal_doc_line_id
        from fm_fiscal_doc_detail fdd, fm_fiscal_doc_tax_detail fdtd
       where fdd.fiscal_doc_id = I_fiscal_doc_id
         and fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         and fdtd.percentage_rate < 0;
   ---
  /* cursor C_FM_FISCAL_DOC_PAYMENTS is
      select fdp.payment_date
        from fm_fiscal_doc_payments fdp
       where fdp.fiscal_doc_id = I_fiscal_doc_id
         and fdp.payment_date < R_fiscal_header.issue_date;*/
   ---
   cursor C_TOTAL_FM_FISCAL_DOC_PAYMENTS is
      select SUM(fdp.value) total_value_pay
        from fm_fiscal_doc_payments fdp
       where fdp.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_EXIST_FM_FISCAL_DOC_DETAIL is
      select 'X'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_EXIST_TAX_HEAD is
      select 'x'
        from fm_fiscal_doc_tax_head fdt
       where fdt.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_LOCK_FM_FISCAL_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail
       where fiscal_doc_line_id = P_fiscal_doc_line_id
         for update nowait;

   cursor C_TAX_HEAD_DTL_MISMATCH is
  	 select 'x'
		 from fm_fiscal_doc_tax_head ffdth
  	 where ffdth.fiscal_doc_id = I_fiscal_doc_id
	  	 and ffdth.vat_code not in
  		 (select distinct(ffdtd.vat_code) from fm_fiscal_doc_tax_detail ffdtd , fm_fiscal_doc_detail fdd where fdd.fiscal_doc_line_id
	  	 = ffdtd.fiscal_doc_line_id and fdd.fiscal_doc_id =  I_fiscal_doc_id )
   	UNION ALL
  	 select 'x'
		 from fm_fiscal_doc_tax_detail ffdtd , fm_fiscal_doc_detail fdd
	where fdd.fiscal_doc_line_id  = ffdtd.fiscal_doc_line_id and fdd.fiscal_doc_id =  I_fiscal_doc_id
	   and ffdtd.vat_code not in (select vat_code from fm_fiscal_doc_tax_head ffdth
	   where ffdth.fiscal_doc_id = I_fiscal_doc_id);

  cursor C_GET_HEAD_DTL_TAX is
  	 select ffdth.total_value total_head_value ,
  		   sum(ffdtd.total_value) total_detail_value,
		   ffdth.vat_code tax_code
	   from   fm_fiscal_doc_tax_head ffdth,
		   fm_fiscal_doc_tax_detail ffdtd,
		   fm_fiscal_doc_detail ffdd
	   where ffdd.fiscal_doc_id = ffdth.fiscal_doc_id
		   and ffdd.fiscal_doc_line_id = ffdtd.fiscal_doc_line_id
		   and ffdth.vat_code = ffdtd.vat_code
	   and ffdth.fiscal_doc_id = I_fiscal_doc_id
     group by ffdth.vat_code,ffdth.total_value;

   ---
BEGIN
   ---
   if I_form_ind = 'Y' then
      L_form_ind := 1;
   else
      L_form_ind := 0;
   end if;


   --- Cleans up old error messages
   if FM_ERROR_LOG_SQL.CLEAR_ERROR(O_error_message,
                                   I_fiscal_doc_id,
                                   L_program) = FALSE then
      return FALSE;
   end if;
   ---
   L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
   L_table := 'FM_FISCAL_DOC_HEADER';
   ---
   --- Gets NF header informations.
   open C_FM_FISCAL_DOC_HEADER;
   ---
   fetch C_FM_FISCAL_DOC_HEADER into R_fiscal_header;
   ---
   close C_FM_FISCAL_DOC_HEADER;
   ---
   if R_fiscal_header.fiscal_doc_no is NULL then
      ---
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_DOC_NO_NOT_NULL',NULL,NULL,NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      O_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id),
                                      L_form_ind) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   if FM_FISCAL_UTILIZATION_SQL.GET_INFO(O_error_message,
                                         L_utilization_desc,
                                         L_status,
                                         L_mode_type,
                                         L_nop,
                                         L_requisition_type,
                                         R_fiscal_header.utilization_id) = FALSE then
      O_status := 0;
   end if;
   ---
   if L_mode_type <> R_fiscal_header.mode_type then
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_UTIL',NULL,NULL,NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      O_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id),
                                      L_form_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if FM_FISCAL_DOC_TYPE_SQL.GET_SUBSERIES_NO_IND(O_error_message,
                                                  L_subseries_no_ind,
                                                  R_fiscal_header.type_id) = FALSE then
      O_status := 0;
   end if;
   ---
   if NVL(L_subseries_no_ind,'N') = 'N' and
      R_fiscal_header.subseries_no is NOT NULL then
      ---
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_SUBSERIES_NULL',NULL,NULL,NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      O_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id),
                                      L_form_ind) = FALSE then
         return FALSE;
      end if;
   ---
   end if;
   ---
   if R_fiscal_header.requisition_type = 'RMA' then
      if FM_RMA_NF_CREATION_SQL.EXISTS_CUSTOMER(O_error_message,
                                                L_key_value_1_desc,
                                                R_fiscal_header.key_value_1) = FALSE then
         O_status := 0;
         O_error_message := SQL_LIB.CREATE_MSG(O_error_message,NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         O_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id)) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      if FM_FISCAL_HEADER_VAL_SQL.VALIDATE_ENTITY(O_error_message,
                                                  L_key_value_1_desc,
                                                  R_fiscal_header.module,
                                                  R_fiscal_header.key_value_1,
                                                  R_fiscal_header.key_value_2) = FALSE then
         O_status := 0;
         O_error_message := SQL_LIB.CREATE_MSG(O_error_message,NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         O_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id),
                                         L_form_ind) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   end if;
   ---
   if R_fiscal_header.entry_or_exit_date is NOT NULL then
      if R_fiscal_header.issue_date > R_fiscal_header.entry_or_exit_date then
         O_status := 0;
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_ISSUE_DATE',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         O_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id),
                                         L_form_ind) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if FM_FISCAL_HEADER_VAL_SQL.VALIDATE_ENTITY(O_error_message,
                                               L_key_value_1_desc,
                                               'PTNR',
                                               R_fiscal_header.partner_id,
                                               R_fiscal_header.partner_type) = FALSE then
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG(O_error_message,NULL,NULL,NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      O_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id),
                                      L_form_ind) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   if R_fiscal_header.fiscal_doc_no <> 0 then
      ---
      if FM_FISCAL_DOC_HEADER_SQL.CHECK_DUP_DOC(O_error_message,
                                                L_exists,
                                                R_fiscal_header.mode_type,
                                                I_fiscal_doc_id,
                                                R_fiscal_header.fiscal_doc_no,
                                                R_fiscal_header.series_no,
                                                R_fiscal_header.module,
                                                R_fiscal_header.key_value_1,
                                                R_fiscal_header.key_value_2,
                                                R_fiscal_header.issue_date,
                                                R_fiscal_header.requisition_type) = FALSE then
         O_status := 0;
      end if;
      ---
      if L_exists then
         O_status := 0;
         O_error_message := SQL_LIB.CREATE_MSG('DOC_EXISTS',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         O_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id),
                                         L_form_ind) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
   end if;
   ---
   if R_fiscal_header.fiscal_doc_no = 0 then
      --- Is not necessary set up the O_status because this message is just a warning.
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_AUTOMATIC_DOC_NO',NULL,NULL,NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'W',
                                      'VALIDATION_ERROR',
                                      O_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id),
                                      L_form_ind) = FALSE then
         return FALSE;
      end if;
   end if;

   if (GET_TRIANG_IND(O_error_message, R_fiscal_header.comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;

   if R_fiscal_header.mode_type = 'ENT' and R_fiscal_header.requisition_type <> 'RMA' then
   --- If complementary, checks if exist relationed documents
      if R_fiscal_header.comp_nf_ind = 'Y' then

         L_table:= 'FM_FISCAL_DOC_COMPLEMENT';
	   ---
         open C_FM_FISCAL_DOC_COMPLEMENT;
         ---
         fetch C_FM_FISCAL_DOC_COMPLEMENT into L_dummy;
         L_exists := C_FM_FISCAL_DOC_COMPLEMENT%FOUND;
         ---
         close C_FM_FISCAL_DOC_COMPLEMENT;
         ---
         if NOT L_exists then
            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_COMPL_DOC',NULL,NULL,NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'Compl_fiscal_doc_id: '||
                                            TO_CHAR(I_fiscal_doc_id)) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if;

      if (L_triang_ind = 'Y' and R_fiscal_header.comp_nf_ind = 'N') then

         open C_FM_FISCAL_DOC_COMPLEMENT_CNT;
         ---
         fetch C_FM_FISCAL_DOC_COMPLEMENT_CNT into L_cnt;
         ---
         close C_FM_FISCAL_DOC_COMPLEMENT_CNT;

                          ---
         if nvl(L_cnt,0) > 1 then

            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_COMPL_DOC_CNT',NULL,NULL,NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'Compl_fiscal_doc_id: '||
                                            TO_CHAR(I_fiscal_doc_id)) = FALSE then
               return FALSE;
            end if;
         end if;

         if R_fiscal_header.schedule_no is not null then
            L_table:= 'FM_FISCAL_DOC_COMPLEMENT';
            ---
            open C_FM_COMP_FISCAL_ID_SCHED(I_fiscal_doc_id);
            ---
            fetch C_FM_COMP_FISCAL_ID_SCHED into L_dummy;
            L_exists := C_FM_COMP_FISCAL_ID_SCHED%FOUND;
            ---
            close C_FM_COMP_FISCAL_ID_SCHED;
            ---
            if L_exists then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('ORFM_COMPL_DOC_SCHED',NULL,NULL,NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                               I_fiscal_doc_id,
                                               L_program,
                                               L_program,
                                               'E',
                                               'VALIDATION_ERROR',
                                               L_error_message,
                                               'Compl_fiscal_doc_id: '||
                                               TO_CHAR(I_fiscal_doc_id)) = FALSE then
                  return FALSE;
               end if;
            end if;


            open C_FM_FISCAL_ID_SCHED_STATUS(I_fiscal_doc_id);
            ---
            fetch C_FM_FISCAL_ID_SCHED_STATUS into L_dummy;
            L_exists := C_FM_FISCAL_ID_SCHED_STATUS%FOUND;
            ---
            close C_FM_FISCAL_ID_SCHED_STATUS;
            ---
            if L_exists then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('ORFM_TRIANG_SCHED_STATUS',NULL,NULL,NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                               I_fiscal_doc_id,
                                               L_program,
                                               L_program,
                                               'E',
                                               'VALIDATION_ERROR',
                                               L_error_message,
                                               'Compl_fiscal_doc_id: '||
                                               TO_CHAR(I_fiscal_doc_id)) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      end if;
   end if;
   --- Gets Detail informations.
   L_table := 'FM_FISCAL_DOC_DETAIL';
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   LOOP
      L_table := 'FM_FISCAL_DOC_DETAIL';
      fetch C_FM_FISCAL_DOC_DETAIL into L_fiscal_doc_line_id, L_unit_cost, L_total_cost, L_item
                                        , L_req_no, L_triang_ind, L_del_supp, L_supplier, L_pack_ind, L_pack_no;   ---new
      EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;
      --- Check if exists an unit cost
      if L_unit_cost is NULL then
         O_status := 0;
         O_error_message := SQL_LIB.CREATE_MSG('NO_UNIT_COST',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         O_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id)||
                                         ' Fiscal_doc_line_id: '||
                                         TO_CHAR(L_fiscal_doc_line_id),
                                         L_form_ind) = FALSE then
            return FALSE;
         end if;
      end if;
      --- Check if exists a total cost
      if L_total_cost is NULL then
         O_status := 0;
         O_error_message := SQL_LIB.CREATE_MSG('MUST_ENTER_TOTAL_COST',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         O_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id)||
                                         ' Fiscal_doc_line_id: '||
                                         TO_CHAR(L_fiscal_doc_line_id),
                                         L_form_ind) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_form_ind = 0 then
         return FALSE;
      end if;
      if L_triang_ind = 'Y' then
      ---
         L_triang_po_exists := TRUE;
      ---
      end if;
      ---
      if R_fiscal_header.mode_type = 'ENT' and R_fiscal_header.requisition_type = 'PO' and L_triang_ind = 'Y' then
      ---
         if R_fiscal_header.comp_nf_ind = 'Y' then
         ---
            open C_FM_COMP_FISCAL_ID (I_fiscal_doc_id);
            ---
            fetch C_FM_COMP_FISCAL_ID into L_other_fiscal_doc_id;
            L_exists := C_FM_COMP_FISCAL_ID%FOUND;
            ---
            close C_FM_COMP_FISCAL_ID;
            ---
            if NOT L_exists then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('NO_COMP_DOC',NULL,NULL,NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                              I_fiscal_doc_id,
                                              L_program,
                                              L_program,
                                              'E',
                                              'VALIDATION_ERROR',
                                              L_error_message,
                                              'fiscal_doc_id: '||
                                              TO_CHAR(I_fiscal_doc_id)) = FALSE then
                  return FALSE;
               end if;
            --end if;
            --triang
            else
               ---
               open C_GET_REF_LINE_ID (L_other_fiscal_doc_id, L_req_no, L_item, L_pack_ind, L_pack_no);
               ---
               fetch C_GET_REF_LINE_ID into L_ref_line_id;
               ---
               L_ref_exists := C_GET_REF_LINE_ID%FOUND;
               ---
               close C_GET_REF_LINE_ID;
               ---
               if L_ref_exists then
               ---
                  open C_LOCK_FM_FISCAL_DETAIL(L_other_fiscal_doc_id);
                  close C_LOCK_FM_FISCAL_DETAIL;
                  ---
                  ---
                  update fm_fiscal_doc_detail fdd
                     set fdd.fiscal_doc_id_ref      = L_other_fiscal_doc_id,
                        fdd.fiscal_doc_line_id_ref = L_ref_line_id
                  where fdd.fiscal_doc_id          = I_fiscal_doc_id
                    and fdd.fiscal_doc_line_id     = L_fiscal_doc_line_id;
                  ---
                  ---
               elsif not L_ref_exists then
                  ---
                  O_status := 0;
                  L_error_message := SQL_LIB.CREATE_MSG('NO_COMP_REF_LINE',NULL,NULL,NULL);
                  if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                                 I_fiscal_doc_id,
                                                 L_program,
                                                 L_program,
                                                 'E',
                                                 'VALIDATION_ERROR',
                                                 L_error_message,
                                                 'fiscal_doc_id: '||
                                                 TO_CHAR(I_fiscal_doc_id)||
                                         ' Fiscal_doc_line_id: '||
                                         TO_CHAR(L_fiscal_doc_line_id)) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;
            --triang
            open C_MATCH_LINE_VALUES (L_other_fiscal_doc_id, L_fiscal_doc_line_id);
            fetch C_MATCH_LINE_VALUES into L_detail_count;
            close C_MATCH_LINE_VALUES;
            ---
            if L_detail_count > 0 then
               ---
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('NO_TRIANG_LINE_MATCH',NULL,NULL,NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                              I_fiscal_doc_id,
                                              L_program,
                                              L_program,
                                              'E',
                                              'VALIDATION_ERROR',
                                              L_error_message,
                                              'fiscal_doc_id: '||
                                              TO_CHAR(I_fiscal_doc_id)||
                                         ' Fiscal_doc_line_id: '||
                                         TO_CHAR(L_fiscal_doc_line_id)) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         elsif R_fiscal_header.comp_nf_ind = 'N' then
            ---
            open C_FM_COMP_FISCAL_ID_COMP (I_fiscal_doc_id);
            ---
            fetch C_FM_COMP_FISCAL_ID_COMP into L_other_fiscal_doc_id;
            L_exists := C_FM_COMP_FISCAL_ID_COMP%FOUND;
            ---
            close C_FM_COMP_FISCAL_ID_COMP;
            ---
            if NOT L_exists then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('NO_COMP_DOC',NULL,NULL,NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                              I_fiscal_doc_id,
                                              L_program,
                                              L_program,
                                              'E',
                                              'VALIDATION_ERROR',
                                              L_error_message,
                                              'fiscal_doc_id: '||
                                              TO_CHAR(I_fiscal_doc_id)) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            open C_GET_REF_LINE_ID (L_other_fiscal_doc_id, L_req_no, L_item, L_pack_ind, L_pack_no);
            ---
            fetch C_GET_REF_LINE_ID into L_ref_line_id;
            ---
            L_ref_exists := C_GET_REF_LINE_ID%FOUND;
            ---
            close C_GET_REF_LINE_ID;
            ---
            if not L_ref_exists then
               ---
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('NO_COMP_REF_LINE',NULL,NULL,NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                              I_fiscal_doc_id,
                                              L_program,
                                              L_program,
                                              'E',
                                              'VALIDATION_ERROR',
                                              L_error_message,
                                              'fiscal_doc_id: '||
                                              TO_CHAR(I_fiscal_doc_id)||
                                      ' Fiscal_doc_line_id: '||
                                      TO_CHAR(L_fiscal_doc_line_id)) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            open C_MATCH_LINE_VALUES (L_other_fiscal_doc_id, L_fiscal_doc_line_id);
            fetch C_MATCH_LINE_VALUES into L_detail_count;
            close C_MATCH_LINE_VALUES;
            ---
            if L_detail_count > 0 then
               ---
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('NO_TRIANG_LINE_MATCH',NULL,NULL,NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                              I_fiscal_doc_id,
                                              L_program,
                                              L_program,
                                              'E',
                                              'VALIDATION_ERROR',
                                              L_error_message,
                                              'fiscal_doc_id: '||
                                              TO_CHAR(I_fiscal_doc_id)||
                                         ' Fiscal_doc_line_id: '||
                                         TO_CHAR(L_fiscal_doc_line_id)) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         end if;
         ---
      end if;
      ---
   END LOOP;
   ---
   L_table := 'FM_FISCAL_DOC_DETAIL';
   ---
   close C_FM_FISCAL_DOC_DETAIL;
   ---

    --- Gets Detail informations.
   L_table := 'FM_FISCAL_DOC_DETAIL';
   open C_FM_FISCAL_DETAIL_UNEXPECTED;
   ---
   LOOP
      L_table := 'FM_FISCAL_DOC_DETAIL';
      fetch C_FM_FISCAL_DETAIL_UNEXPECTED into L_fiscal_doc_line_id, L_unit_cost, L_total_cost, L_item
                                        ,  L_pack_ind, L_pack_no;   ---new
      EXIT when C_FM_FISCAL_DETAIL_UNEXPECTED%NOTFOUND;
      --- Check if exists an unit cost

      ---
      if L_form_ind = 0 then
         return FALSE;
      end if;
      if L_triang_ind = 'Y' then
      ---
         L_triang_po_exists := TRUE;
      ---
      end if;
      ---
      if R_fiscal_header.mode_type = 'ENT' and R_fiscal_header.requisition_type = 'PO' and L_triang_ind = 'Y' then
      ---
         if R_fiscal_header.comp_nf_ind = 'Y' then
         ---
            open C_FM_COMP_FISCAL_ID (I_fiscal_doc_id);
            ---
            fetch C_FM_COMP_FISCAL_ID into L_other_fiscal_doc_id;
            L_exists := C_FM_COMP_FISCAL_ID%FOUND;
            ---
            close C_FM_COMP_FISCAL_ID;
            ---
            if NOT L_exists then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('NO_COMP_DOC',NULL,NULL,NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                              I_fiscal_doc_id,
                                              L_program,
                                              L_program,
                                              'E',
                                              'VALIDATION_ERROR',
                                              L_error_message,
                                              'fiscal_doc_id: '||
                                              TO_CHAR(I_fiscal_doc_id)) = FALSE then
                  return FALSE;
               end if;
            --end if;
            --triang
            else
               ---
               open C_GET_REF_LINE_ID_UNEXPECTED (L_other_fiscal_doc_id, L_item, L_pack_ind, L_pack_no);
               ---
               fetch C_GET_REF_LINE_ID_UNEXPECTED into L_ref_line_id;
               ---
               L_ref_exists := C_GET_REF_LINE_ID_UNEXPECTED%FOUND;
               ---
               close C_GET_REF_LINE_ID_UNEXPECTED;
               ---
               if L_ref_exists then
               ---
                  open C_LOCK_FM_FISCAL_DETAIL(L_other_fiscal_doc_id);
                  close C_LOCK_FM_FISCAL_DETAIL;
                  ---
                  ---
                  update fm_fiscal_doc_detail fdd
                     set fdd.fiscal_doc_id_ref      = L_other_fiscal_doc_id,
                        fdd.fiscal_doc_line_id_ref = L_ref_line_id
                  where fdd.fiscal_doc_id          = I_fiscal_doc_id
                    and fdd.fiscal_doc_line_id     = L_fiscal_doc_line_id;
                  ---
                  ---
               elsif not L_ref_exists then
                  ---
                  O_status := 0;
                  L_error_message := SQL_LIB.CREATE_MSG('NO_COMP_REF_LINE',NULL,NULL,NULL);
                  if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                                 I_fiscal_doc_id,
                                                 L_program,
                                                 L_program,
                                                 'E',
                                                 'VALIDATION_ERROR',
                                                 L_error_message,
                                                 'fiscal_doc_id: '||
                                                 TO_CHAR(I_fiscal_doc_id)||
                                         ' Fiscal_doc_line_id: '||
                                         TO_CHAR(L_fiscal_doc_line_id)) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;
            --triang
            ---
         end if;
         ---
      end if;
      ---
   END LOOP;
   ---
   L_table := 'FM_FISCAL_DOC_DETAIL';
   ---
   close C_FM_FISCAL_DETAIL_UNEXPECTED;

   if L_triang_po_exists and L_exists then
   ---
      OPEN C_MATCH_TRIANGULATION (L_other_fiscal_doc_id);
      ---
      FETCH C_MATCH_TRIANGULATION INTO L_count;
      ---
      CLOSE C_MATCH_TRIANGULATION;
      ---
      if NVL(L_count,0) > 0 then
      ---
         O_status := 0;
         L_error_message := SQL_LIB.CREATE_MSG('NO_TRIANG_MATCH',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                        I_fiscal_doc_id,
                                        L_program,
                                        L_program,
                                        'E',
                                        'VALIDATION_ERROR',
                                        L_error_message,
                                        'fiscal_doc_id: '||
                                        TO_CHAR(I_fiscal_doc_id)) = FALSE then
            return FALSE;
         end if;
      ---
      end if;
   ---
   end if;
   ---
   if R_fiscal_header.mode_type = 'ENT' and R_fiscal_header.requisition_type <> 'RMA'  then
   --- Gets Tax header informations.
   L_table := 'FM_FISCAL_DOC_TAX_HEAD';
   open C_FM_FISCAL_DOC_TAX_HEAD;
   ---
   LOOP
      fetch C_FM_FISCAL_DOC_TAX_HEAD into L_vat_code;
      EXIT when C_FM_FISCAL_DOC_TAX_HEAD%NOTFOUND;
      ---
      O_status := 0;
      L_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_CALC_BASE',NULL,NULL,NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      L_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id)||
                                      ' Vat_code: '||L_vat_code) = FALSE then
         return FALSE;
      end if;
   END LOOP;
   ---
   close C_FM_FISCAL_DOC_TAX_HEAD;
   ---
   --- Gets Tax detail informations.
   L_table := 'FM_FISCAL_DOC_TAX_DETAIL';
   open C_FM_FISCAL_DOC_TAX_DETAIL;
   ---
   LOOP
      fetch C_FM_FISCAL_DOC_TAX_DETAIL into L_vat_code, L_fiscal_doc_line_id;
      EXIT when C_FM_FISCAL_DOC_TAX_DETAIL%NOTFOUND;
      ---
      O_status := 0;
      L_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_PERC_RATE',NULL,NULL,NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      L_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id)||
                                      ' Vat_code: '||L_vat_code||
                                      ' Fiscal_doc_line_id: '||
                                      TO_CHAR(L_fiscal_doc_line_id)) = FALSE then
         return FALSE;
      end if;
   END LOOP;
   ---
   close C_FM_FISCAL_DOC_TAX_DETAIL;
   ---
   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_ITEM_LEVEL_TAX_EXISTS(O_error_message
                                                                  ,L_count
                                                                  ,I_fiscal_doc_id) = FALSE then
         return FALSE;
      end if;
   --
   if L_count > 0 then

        if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
	                                         L_description,
	                                         L_number_value,
	                                         L_string_value,
	                                         L_date_value,
	                                         'CHAR',
	                                         'CALC_TOL_TYPE') = FALSE then
	      return FALSE;
	   end if;
	   ---
	   L_calc_tol_type := L_string_value;
	   ---
	   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
	                                         L_description,
	                                         L_number_value,
	                                         L_string_value,
	                                         L_date_value,
	                                         'NUMBER',
	                                         'CALC_TOL_VALUE') = FALSE then
	      return FALSE;
	   end if;
	   ---
   	L_calc_tol_val := L_number_value;


	 L_table:= 'FM_FISCAL_DOC_TAX_HEAD';
	 ---
	 open C_TAX_HEAD_DTL_MISMATCH;
	 ---
	 fetch C_TAX_HEAD_DTL_MISMATCH into L_dummy;
	 L_exists := C_TAX_HEAD_DTL_MISMATCH%FOUND;
	 ---
	 close C_TAX_HEAD_DTL_MISMATCH;
	 ---
	 if L_exists then
	    O_status := 0;
	    L_error_message := SQL_LIB.CREATE_MSG('ORFM_TAX_MISMATCH',NULL,NULL,NULL);
	    if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
					    I_fiscal_doc_id,
					    L_program,
					    L_program,
					    'E',
					    'VALIDATION_ERROR',
					    L_error_message,
					    'Compl_fiscal_doc_id: '||
					    TO_CHAR(I_fiscal_doc_id)) = FALSE then
	       return FALSE;
	    end if;
	end if;

	open C_GET_HEAD_DTL_TAX;
	   ---
	   LOOP
	      L_table := 'C_GET_HEAD_DTL_TAX';
	      fetch C_GET_HEAD_DTL_TAX into L_head_total_value, L_detail_total_value, L_tax_code;   ---new
	      EXIT when C_GET_HEAD_DTL_TAX%NOTFOUND;
	      --- Check if exists an unit cost



	      if L_calc_tol_type = C_DISC_PERC then
	      	            ---
		    if (NVL(L_head_total_value,0) = 0 and
			NVL(L_detail_total_value,0) <> 0) or
			(NVL(L_head_total_value,0) <> 0 and
			 NVL(L_detail_total_value,0) = 0) then
		       ---
		       L_dif_perc := 100;
		       ---
		    else
		       ---
		       L_dif_perc := ABS(((NVL(L_head_total_value,0) * 100) / NVL(L_detail_total_value,1)) - 100);
		       ---
		    end if;
	      	            ---
		    if L_dif_perc > NVL(L_calc_tol_val,0) then
		       O_status := 0;
		       L_error_message := SQL_LIB.CREATE_MSG('ORFM_DIFF_HEAD_DETAIL_TAX',L_calc_tol_type,NVL(L_calc_tol_val,0),L_tax_code);
		       if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
						       I_fiscal_doc_id,
						       L_program,
						       L_program,
						       'E',
						       'VALIDATION_ERROR',
						       L_error_message,
						       'Fiscal_doc_id: '||
						       TO_CHAR(I_fiscal_doc_id)||
						       ' Tolerance_type: '||L_calc_tol_type) = FALSE then
			  return FALSE;
		       end if;
		    end if;
		    ---
	     elsif L_calc_tol_type = C_DISC_VALUE then
	    ---
		    if ABS(NVL(L_head_total_value,0) - NVL(L_detail_total_value,0)) > NVL(L_calc_tol_val,0) then
		       O_status := 0;
		       L_error_message := SQL_LIB.CREATE_MSG('ORFM_DIFF_HEAD_DETAIL_TAX',L_calc_tol_type,NVL(L_calc_tol_val,0),L_tax_code);
		       if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
						       I_fiscal_doc_id,
						       L_program,
						       L_program,
						       'E',
						       'VALIDATION_ERROR',
						       L_error_message,
						       'Fiscal_doc_id: '||
						       TO_CHAR(I_fiscal_doc_id)||
						       ' Tolerance_type: '||L_calc_tol_type) = FALSE then
			  return FALSE;
		       end if;
			     end if;
	    end if;

	      ---
	   END LOOP;
	   ---
	   L_table := 'FM_FISCAL_DOC_DETAIL';
	   ---
    close C_GET_HEAD_DTL_TAX;

   end if;

   --- Gets Payments informations.
   --- Total Payments x Total document.
   L_table := 'FM_FISCAL_DOC_PAYMENTS';
   open C_TOTAL_FM_FISCAL_DOC_PAYMENTS;
   ---
   fetch C_TOTAL_FM_FISCAL_DOC_PAYMENTS into L_total_value_pay;
   ---
   close C_TOTAL_FM_FISCAL_DOC_PAYMENTS;
   ---
   if L_total_value_pay is NOT NULL and
      L_total_value_pay <> R_fiscal_header.total_doc_value then
      ---
      O_status := 0;
      L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOTAL_PAYMENTS',NULL,NULL,NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      L_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id)) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   --- Check if exists detail.
   if R_fiscal_header.comp_nf_ind <> 'Y' then
      L_table := 'FM_FISCAL_DOC_DETAIL';
      open C_EXIST_FM_FISCAL_DOC_DETAIL;
      ---
      fetch C_EXIST_FM_FISCAL_DOC_DETAIL into L_dummy;
      if C_EXIST_FM_FISCAL_DOC_DETAIL%NOTFOUND then
         O_status := 0;
         L_error_message := SQL_LIB.CREATE_MSG('ORFM_NOT_EXIST_DETAIL',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         L_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id)) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      close C_EXIST_FM_FISCAL_DOC_DETAIL;
      ---
   end if;
  end if;
   ---
   if L_triang_po_exists and L_exists then
   ---
     if not O_other_proc then
     ---
        O_other_proc := TRUE;
        if (CHECK_DATA_INTEGRITY(O_error_message, O_status, O_other_proc, L_other_fiscal_doc_id) = FALSE) then
           return FALSE;
        end if;
     ---
     end if;
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      if C_FM_FISCAL_DOC_HEADER%ISOPEN then
         close C_FM_FISCAL_DOC_HEADER;
      end if;
      ---
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      if C_FM_FISCAL_DOC_COMPLEMENT%ISOPEN then
         close C_FM_FISCAL_DOC_COMPLEMENT;
      end if;
      ---
      if C_FM_FISCAL_DOC_COMPLEMENT_CNT%ISOPEN then
         close C_FM_FISCAL_DOC_COMPLEMENT_CNT;
      end if;
      ---
       if C_FM_COMP_FISCAL_ID_SCHED%ISOPEN then
         close C_FM_COMP_FISCAL_ID_SCHED;
      end if;
      ---
      if C_FM_FISCAL_ID_SCHED_STATUS%ISOPEN then
         close C_FM_FISCAL_ID_SCHED_STATUS;
      end if;
      ---
        if C_FM_FISCAL_ID_SCHED%ISOPEN then
         close C_FM_FISCAL_ID_SCHED;
      end if;
      ---
      if C_FM_FISCAL_DOC_TAX_HEAD%ISOPEN then
         close C_FM_FISCAL_DOC_TAX_HEAD;
      end if;
      ---
      if C_FM_FISCAL_DOC_TAX_DETAIL%ISOPEN then
         close C_FM_FISCAL_DOC_TAX_DETAIL;
      end if;
      ---
      if C_TOTAL_FM_FISCAL_DOC_PAYMENTS%ISOPEN then
         close C_TOTAL_FM_FISCAL_DOC_PAYMENTS;
      end if;
      ---
      if C_EXIST_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_EXIST_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      if C_EXIST_TAX_HEAD%ISOPEN then
         close C_EXIST_TAX_HEAD;
      end if;
      ---
      if C_FM_COMP_FISCAL_ID%ISOPEN then
         close C_FM_COMP_FISCAL_ID;
      end if;
      ---
      if C_FM_COMP_FISCAL_ID_COMP%ISOPEN then
         close C_FM_COMP_FISCAL_ID_COMP;
      end if;
      ---
      if C_MATCH_TRIANGULATION%ISOPEN then
         close C_MATCH_TRIANGULATION;
      end if;
      ---
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
      ---
END CHECK_DATA_INTEGRITY;
-----------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_DISCOUNT(O_error_message IN OUT VARCHAR2,
                             O_status        IN OUT INTEGER,
                             O_other_proc    IN OUT BOOLEAN,
                             I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   TYPE po_item_disc IS RECORD
   (fiscal_doc_line_id    FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
    unit_cost             FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
    quantity              FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
    unit_item_disc        FM_FISCAL_DOC_DETAIL.unit_item_disc%TYPE,
    unit_header_disc      FM_FISCAL_DOC_DETAIL.unit_header_disc%TYPE);
   ---
   TYPE po_item_disc_tab IS TABLE OF po_item_disc INDEX BY BINARY_INTEGER;
   ---
   L_po_item_disc         PO_ITEM_DISC;
   L_po_item_disc_tab     PO_ITEM_DISC_TAB;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   L_comp_nf_ind                 FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind                  ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_fiscal_doc_id         FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   ---
   L_program              VARCHAR2(50) := 'FM_FISCAL_VALIDATION_SQL.CALC_TOTAL_DISCOUNT';
   L_table                VARCHAR2(30);
   L_key                  VARCHAR2(100);
   L_error_message        VARCHAR2(255) := NULL;
   L_po_item_disc_ind     NUMBER := 0;
   L_unit_item_disc       FM_FISCAL_DOC_DETAIL.UNIT_ITEM_DISC%TYPE;
   L_unit_header_disc     FM_FISCAL_DOC_DETAIL.UNIT_HEADER_DISC%TYPE;
   L_head_disc_type       FM_FISCAL_DOC_HEADER.DISCOUNT_TYPE%TYPE;
   L_head_disc_val        FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_total_unit_disc      FM_FISCAL_DOC_DETAIL.UNIT_ITEM_DISC%TYPE := 0;
   L_total_head_disc_val  FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_req_type             FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select fdh.discount_type,
             fdh.total_discount_value,
             fdh.requisition_type
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id, fdd.unit_cost, fdd.quantity, fdd.discount_type, fdd.discount_value
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_LOCK_FM_FISCAL_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail
       where fiscal_doc_line_id = P_fiscal_doc_line_id
         for update nowait;
   ---
   L_fiscal_doc_detail     C_FM_FISCAL_DOC_DETAIL%ROWTYPE;
   ---
BEGIN
   L_table := 'FM_FISCAL_DOC_HEADER';
   L_key := 'FISCAL_DOC_ID';
   open C_FM_FISCAL_DOC_HEADER;
   ---
   fetch C_FM_FISCAL_DOC_HEADER into L_head_disc_type, L_head_disc_val, L_req_type;
   ---
   close C_FM_FISCAL_DOC_HEADER;
   ---
   L_table := 'FM_FISCAL_DOC_DETAIL';
   L_key := 'FISCAL_DOC_LINE_ID';
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   LOOP
      L_table := 'FM_FISCAL_DOC_DETAIL';
      fetch C_FM_FISCAL_DOC_DETAIL into L_fiscal_doc_detail;
      EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;
      ---
      if L_fiscal_doc_detail.discount_type is NULL then
         L_unit_item_disc := NULL;
      elsif L_fiscal_doc_detail.discount_type = C_DISC_VALUE then
         L_unit_item_disc := L_fiscal_doc_detail.discount_value / L_fiscal_doc_detail.quantity;
      elsif L_fiscal_doc_detail.discount_type = C_DISC_PERC then
         L_unit_item_disc := (L_fiscal_doc_detail.unit_cost * L_fiscal_doc_detail.discount_value) / 100;
      end if;
      ---
      L_total_unit_disc := L_total_unit_disc + ((L_fiscal_doc_detail.unit_cost - NVL(L_unit_item_disc,0)) * L_fiscal_doc_detail.quantity);
      ---
      L_unit_header_disc := NULL;
      ---
      L_po_item_disc.fiscal_doc_line_id := L_fiscal_doc_detail.fiscal_doc_line_id;
      L_po_item_disc.unit_cost          := L_fiscal_doc_detail.unit_cost;
      L_po_item_disc.quantity           := L_fiscal_doc_detail.quantity;
      L_po_item_disc.unit_item_disc     := L_unit_item_disc;
      L_po_item_disc.unit_header_disc   := L_unit_header_disc;
      ---
      L_po_item_disc_tab(L_po_item_disc_ind) := L_po_item_disc;
      L_po_item_disc_ind                     := L_po_item_disc_ind + 1;
      ---
   END LOOP;
   close C_FM_FISCAL_DOC_DETAIL;
   ---
   FOR C_detail_ind in 0 .. L_po_item_disc_ind -1 LOOP
      L_total_head_disc_val := 0;
      if L_head_disc_type = C_DISC_PERC then
         L_total_head_disc_val := (L_total_unit_disc * L_head_disc_val) / 100;
      elsif L_head_disc_type = C_DISC_VALUE then
         L_total_head_disc_val := L_head_disc_val;
      end if;
      ---
      L_po_item_disc_tab(C_detail_ind).unit_header_disc := ((NVL(L_total_head_disc_val,0) / L_total_unit_disc) *
                                                           (L_po_item_disc_tab(C_detail_ind).unit_cost - NVL(L_po_item_disc_tab(C_detail_ind).unit_item_disc,0)) *
                                                            L_po_item_disc_tab(C_detail_ind).quantity) / L_po_item_disc_tab(C_detail_ind).quantity;
      ---
      open C_LOCK_FM_FISCAL_DETAIL(L_po_item_disc_tab(C_detail_ind).fiscal_doc_line_id);
      close C_LOCK_FM_FISCAL_DETAIL;
      ---
      ---
      update fm_fiscal_doc_detail fdd
         set fdd.unit_item_disc      = L_po_item_disc_tab(C_detail_ind).unit_item_disc,
             fdd.unit_header_disc    = L_po_item_disc_tab(C_detail_ind).unit_header_disc,
             fdd.unit_cost_with_disc = L_po_item_disc_tab(C_detail_ind).unit_cost - (NVL(L_po_item_disc_tab(C_detail_ind).unit_item_disc,0) + NVL(L_po_item_disc_tab(C_detail_ind).unit_header_disc,0)),
             fdd.total_calc_cost     = (L_po_item_disc_tab(C_detail_ind).unit_cost - (NVL(L_po_item_disc_tab(C_detail_ind).unit_item_disc,0))) * L_po_item_disc_tab(C_detail_ind).quantity,
             fdd.total_cost          = decode(L_req_type,'RPO',(L_po_item_disc_tab(C_detail_ind).unit_cost - (NVL(L_po_item_disc_tab(C_detail_ind).unit_item_disc,0))) * L_po_item_disc_tab(C_detail_ind).quantity,fdd.total_cost),
             last_update_datetime    = SYSDATE,
             last_update_id          = USER
       where fdd.fiscal_doc_line_id  = L_po_item_disc_tab(C_detail_ind).fiscal_doc_line_id;
      ---
   END LOOP;
   ---
   if (GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;
   ---
   if L_triang_ind = 'Y' then
   ---
      if (GET_OTHER_NF(O_error_message, L_other_fiscal_doc_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      if L_other_fiscal_doc_id is not NULL then
          if not O_other_proc then
          ---
             O_other_proc := TRUE;
             if (CALC_TOTAL_DISCOUNT(O_error_message, O_status, O_other_proc, L_other_fiscal_doc_id) = FALSE) then
                return FALSE;
             end if;
          ---
          end if;
      end if;
      ---
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_DETAIL',
                                             L_key,
                                             TO_CHAR(SQLCODE));
      --
      if C_LOCK_FM_FISCAL_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_DETAIL;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      if C_FM_FISCAL_DOC_HEADER%ISOPEN then
         close C_FM_FISCAL_DOC_HEADER;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := 0;
      return FALSE;
END CALC_TOTAL_DISCOUNT;
--------------------------------------------------------------------------------
FUNCTION CALC_UNIT_ITEM_COST_COMP(O_error_message IN OUT VARCHAR2,
                                  O_status        IN OUT INTEGER,
                                  O_other_proc    IN OUT BOOLEAN,
                                  I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   TYPE po_total_cost IS RECORD
   (order_no                ORDLOC.ORDER_NO%TYPE,
    order_cost              ORDLOC.UNIT_COST%TYPE,
    actual_order_freight    ORDLOC.UNIT_COST%TYPE,
    appr_order_freight      ORDLOC.UNIT_COST%TYPE,
    actual_order_insur      ORDLOC.UNIT_COST%TYPE,
    appr_order_insur        ORDLOC.UNIT_COST%TYPE,
    actual_order_other      ORDLOC.UNIT_COST%TYPE,
    appr_order_other        ORDLOC.UNIT_COST%TYPE);
   ---
   TYPE po_total_cost_tab IS TABLE OF po_total_cost INDEX BY BINARY_INTEGER;
   ---
   TYPE po_pack IS RECORD
   (order_no                FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
    pack_no                 FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE);
   ---
   TYPE po_pack_tab IS TABLE OF po_pack INDEX BY BINARY_INTEGER;
   ---
   L_po_total_cost          PO_TOTAL_COST;
   L_po_total_cost_tab      PO_TOTAL_COST_TAB;
   L_po_pack                PO_PACK;
   L_po_pack_tab            PO_PACK_TAB;
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   L_program                VARCHAR2(50) := 'FM_FISCAL_VALIDATION_SQL.CALC_UNIT_ITEM_COST_COMP';
   L_table                  VARCHAR2(30);
   L_key                    VARCHAR2(100);
   L_error_message          VARCHAR2(255) := NULL;
   L_yes                    VARCHAR2(1) := 'Y';
   L_no                     VARCHAR2(1) := 'N';
   L_head_freight_cost      FM_FISCAL_DOC_HEADER.FREIGHT_COST%TYPE;
   L_head_insur_cost        FM_FISCAL_DOC_HEADER.INSURANCE_COST%TYPE;
   L_head_other_cost        FM_FISCAL_DOC_HEADER.OTHER_EXPENSES_COST%TYPE;
   L_head_total_cost        FM_FISCAL_DOC_HEADER.TOTAL_DOC_VALUE%TYPE;
   L_disc_unit_cost         FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_location_id            FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE;
   --
   L_comp_nf_ind            FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind             ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_fiscal_doc_id    FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   ---
   L_freight_po_exists      BOOLEAN := FALSE;
   L_insur_po_exists        BOOLEAN := FALSE;
   L_other_po_exists        BOOLEAN := FALSE;
   ---
   L_freight_at_pack        NUMBER := 0;
   L_insur_at_pack          NUMBER := 0;
   L_other_at_pack          NUMBER := 0;
   L_pack_po                BOOLEAN;
   ---
   L_tot_freight_po         NUMBER(20,4) := 0;
   L_tot_insur_po           NUMBER(20,4) := 0;
   L_tot_other_po           NUMBER(20,4) := 0;
   ---
   L_appr_po_freight        NUMBER(20,4);
   L_appr_po_insur          NUMBER(20,4);
   L_appr_po_other          NUMBER(20,4);
   ---
   L_po_item_unit_freight   NUMBER(20,4) := NULL;
   L_po_item_unit_insur     NUMBER(20,4) := NULL;
   L_po_item_unit_other     NUMBER(20,4) := NULL;
   ---
   L_actual_freight_po_item FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_actual_insur_po_item   FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_actual_other_po_item   FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   ---
   L_total_po_freight       FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_total_po_insur         FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_total_po_other         FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   ---
   L_po_total_cost_ind      NUMBER := 0;
   L_po_pack_ind            NUMBER := 0;
   L_po_exists              BOOLEAN;
   L_total_nf_disc_cost     NUMBER(20,4) := 0;
   L_po_item_cost           FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_po_cost                FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_po_item_qty            ORDLOC.QTY_ORDERED%TYPE;
   L_comp_item_qty          PACKITEM.PACK_QTY%TYPE;
   ---
   C_freight_comp_id        ORDLOC_EXP.COMP_ID%TYPE := 'F';
   C_insur_comp_id          ORDLOC_EXP.COMP_ID%TYPE := 'I';
   C_other_comp_id          ORDLOC_EXP.COMP_ID%TYPE := 'M';
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select fdh.freight_cost, fdh.insurance_cost, fdh.other_expenses_cost, fdh.total_doc_value, fdh.location_id
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id, fdd.item, fdd.unit_cost, fdd.quantity,
             fdd.requisition_no, fdd.unit_cost_with_disc, fdd.location_id
             ,fdd.pack_no, fdd.pack_ind
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_COST_COMP_PO (P_req_no       ORDLOC_EXP.ORDER_NO%TYPE,
                          P_item         ORDLOC_EXP.ITEM%TYPE,
                          P_pack_no      FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                          P_pack_ind     FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                          P_comp_type    ELC_COMP.COMP_TYPE%TYPE,
                          P_location_id  ORDLOC_EXP.LOCATION%TYPE) is
      select sum(NVL(est_exp_value,0))
        from ordloc_exp
       where order_no = P_req_no and
                 ((P_pack_ind = L_yes and(item = P_item or pack_item = P_item)) or
                  (P_pack_ind = L_no and ((item = P_item and  P_pack_no is not null and pack_item = P_pack_no) or (P_pack_no is not null and item = P_pack_no) or
                  (item = P_item and P_pack_no is null)))) and
              comp_id in (select comp_id
                            from elc_comp
                           where exp_category  =  P_comp_type) and
             location in ((select wh loc
                             from wh
                            where physical_wh = P_location_id)
                          union
                          (select store loc
                             from store
                            where store = P_location_id));
   ---
   cursor C_GET_EXP_PACK (P_req_no       ORDLOC_EXP.ORDER_NO%TYPE,
                          P_item         ORDLOC_EXP.ITEM%TYPE,
                          P_pack_no      FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
			  P_comp_type    ELC_COMP.COMP_TYPE%TYPE,
                          P_location_id  ORDLOC_EXP.LOCATION%TYPE) is
   select count(1)
     from ordloc_exp
    where order_no = P_req_no and
              item = P_pack_no and
	  comp_id in (select comp_id
                            from elc_comp
                           where exp_category  =  P_comp_type) and
             location in ((select wh loc
                             from wh
                            where physical_wh = P_location_id)
                          union
                          (select store loc
                             from store
                            where store = P_location_id));
   ---
   cursor C_GET_COMP_APPR_CC (P_req_no       FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                              P_pack_no      FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                              P_item         FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                              P_appr_cc      ORDLOC_EXP.EST_EXP_VALUE%TYPE) is
   select ((P_appr_cc * (fdd.unit_cost_with_disc * fdd.quantity) / pack.total_cost) / fdd.quantity) appr_cc
     from fm_fiscal_doc_header fdh
         ,fm_fiscal_doc_detail fdd
         ,(select sum(fdd1.unit_cost_with_disc * fdd1.quantity) total_cost
             from fm_fiscal_doc_header fdh1
                 ,fm_fiscal_doc_detail fdd1
            where fdh1.fiscal_doc_id     = I_fiscal_doc_id
                     and fdh1.fiscal_doc_id     = fdd1.fiscal_doc_id
                     and fdd1.requisition_no    = P_req_no
                     and fdd1.pack_no           = P_pack_no) pack
    where fdh.fiscal_doc_id     = I_fiscal_doc_id
      and fdh.fiscal_doc_id     = fdd.fiscal_doc_id
      and fdd.requisition_no    = P_req_no
      and fdd.pack_no           = P_pack_no
      and fdd.item              = P_item;
   ---
   cursor C_PO_ITEM_QTY (P_req_no       ORDLOC.ORDER_NO%TYPE,
                         P_item         ORDLOC.ITEM%TYPE,
                         P_pack_no      PACKITEM.PACK_NO%TYPE,
                         P_location_id  ORDLOC.LOCATION%TYPE) is
      select NVL((qty_ordered - NVL(qty_received,0)),1)
        from ordloc
       where order_no = P_req_no and
                 item = decode(P_pack_no, NULL, P_item, P_pack_no) and
                 location in ((select wh loc
                                 from wh
                                where physical_wh = P_location_id)
                              union
                              (select store loc
                                 from store
                                where store = P_location_id));
   ---
   cursor C_COMP_ITEM_QTY (P_pack_no      PACKITEM.PACK_NO%TYPE,
                           P_item         PACKITEM.ITEM%TYPE) is
      select pack_qty
        from packitem
       where pack_no       = P_pack_no
         and item          = P_item;
   ---
   cursor C_LOCK_FM_FISCAL_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail
       where fiscal_doc_line_id = P_fiscal_doc_line_id
         for update nowait;
   ---
   L_fiscal_doc_detail     C_FM_FISCAL_DOC_DETAIL%ROWTYPE;
   ---
BEGIN
   ---
   L_table := 'FM_FISCAL_DOC_HEADER';
   L_key := 'FISCAL_DOC_ID';
   open C_FM_FISCAL_DOC_HEADER;
   ---
   fetch C_FM_FISCAL_DOC_HEADER into L_head_freight_cost, L_head_insur_cost, L_head_other_cost, L_head_total_cost, L_location_id;
   ---
   close C_FM_FISCAL_DOC_HEADER;
   --Checking if any cost component is defined at the NF header
   --If no cost component is defined at the NF header then no apportioning is
   --needed and the function is exited
   ---
   if (L_head_freight_cost is NULL or L_head_freight_cost = 0) and
      (L_head_insur_cost is NULL or L_head_insur_cost = 0) and
      (L_head_other_cost is NULL or L_head_other_cost = 0) then
      return TRUE;
   end if;
   ---
   L_table := 'FM_FISCAL_DOC_DETAIL';
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   LOOP      
      L_po_item_qty := 1;
      L_table := 'FM_FISCAL_DOC_DETAIL';
      fetch C_FM_FISCAL_DOC_DETAIL into L_fiscal_doc_detail;
      EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;
      ---
      if L_fiscal_doc_detail.unit_cost_with_disc > 0 then
        open C_PO_ITEM_QTY(L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.location_id);
        ---
        fetch C_PO_ITEM_QTY into L_po_item_qty;
        ---
        close C_PO_ITEM_QTY;
        ---
        if L_fiscal_doc_detail.pack_no is NOT NULL then
        ---
           open C_COMP_ITEM_QTY(L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.item);
           ---
           fetch C_COMP_ITEM_QTY into L_comp_item_qty;
           ---
           close C_COMP_ITEM_QTY;
           ---
           L_po_item_qty := L_po_item_qty * L_comp_item_qty;
           ---
        ---
        end if;
        ---
        if L_head_freight_cost is NOT NULL then
             --Since NF header has freight defined, checkis made if any PO in the NF
             --has freight mentioned.
             --If even a single PO in the NF has freight defined then the indicator
             --L_freight_po_exists is made 1 indicating that there is atleast one PO in the
             --NF for which the freight is defined.
             --If none  of the POs in the NF has freight defined, then the indicator
             --remains 0 indicating that there is no PO in the NF for which the
             --freight is defined
             --Similarly for insurance and other cost components
           L_freight_at_pack := NULL;
           open C_COST_COMP_PO (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.pack_ind, C_freight_comp_id, L_fiscal_doc_detail.location_id);
           ---
           fetch C_COST_COMP_PO into L_actual_freight_po_item;
           L_actual_freight_po_item := (L_actual_freight_po_item) * L_fiscal_doc_detail.quantity;
           if C_COST_COMP_PO%NOTFOUND then
              L_actual_freight_po_item := 0;
           end if;
           if L_actual_freight_po_item > 0 then
              L_freight_po_exists := TRUE;
           end if;
           ---
           close C_COST_COMP_PO;
           ---
           if L_actual_freight_po_item > 0 and L_fiscal_doc_detail.pack_no is not null then
                     ---
              open C_GET_EXP_PACK (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, C_freight_comp_id, L_fiscal_doc_detail.location_id);
              fetch C_GET_EXP_PACK into L_freight_at_pack;
              close C_GET_EXP_PACK;
              ---
              if L_freight_at_pack > 0 then
                 L_actual_freight_po_item := L_actual_freight_po_item / L_comp_item_qty;
              end if;
              ---
           end if;
        ---
        end if;
        ---
        if L_head_insur_cost is NOT NULL then
           ---checking if PO has insurance mentioned
           L_insur_at_pack := NULL;
           open C_COST_COMP_PO (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.pack_ind, C_insur_comp_id, L_fiscal_doc_detail.location_id);
           ---
           fetch C_COST_COMP_PO into L_actual_insur_po_item;
           L_actual_insur_po_item := (L_actual_insur_po_item) * L_fiscal_doc_detail.quantity;
           if C_COST_COMP_PO%NOTFOUND then
              L_actual_insur_po_item := 0;
           end if;
           if L_actual_insur_po_item > 0 then
              L_insur_po_exists := TRUE;
           end if;
           ---
           close C_COST_COMP_PO;
           ---
           if L_actual_insur_po_item > 0 and L_fiscal_doc_detail.pack_no is not null then
                     ---
              open C_GET_EXP_PACK (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, C_insur_comp_id, L_fiscal_doc_detail.location_id);
              fetch C_GET_EXP_PACK into L_insur_at_pack;
              close C_GET_EXP_PACK;
              ---
              if L_insur_at_pack > 0 then
                 L_actual_insur_po_item := L_actual_insur_po_item / L_comp_item_qty;
              end if;
              ---
           end if;
        ---
        end if;
        ---
        if L_head_other_cost is NOT NULL then
           ---checking if PO has other cost components mentioned
           L_other_at_pack := NULL;
  
           open C_COST_COMP_PO (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.pack_ind, C_other_comp_id, L_fiscal_doc_detail.location_id);
           ---
           fetch C_COST_COMP_PO into L_actual_other_po_item;
           L_actual_other_po_item := (L_actual_other_po_item) * L_fiscal_doc_detail.quantity;
           if C_COST_COMP_PO%NOTFOUND then
              L_actual_other_po_item := 0;
           end if;
           if L_actual_other_po_item > 0 then
              L_other_po_exists := TRUE;
           end if;
           ---
           close C_COST_COMP_PO;
           ---
           if L_actual_other_po_item > 0 and L_fiscal_doc_detail.pack_no is not null then
                     ---
              open C_GET_EXP_PACK (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, C_other_comp_id, L_fiscal_doc_detail.location_id);
              fetch C_GET_EXP_PACK into L_other_at_pack;
              close C_GET_EXP_PACK;
              ---
              if L_other_at_pack > 0 then
                 L_actual_other_po_item := L_actual_other_po_item / L_comp_item_qty;
              end if;
              ---
           end if;
        ---
        end if;
        ---
        L_disc_unit_cost := L_fiscal_doc_detail.unit_cost_with_disc;
        L_total_nf_disc_cost := L_total_nf_disc_cost + (L_disc_unit_cost * L_fiscal_doc_detail.quantity);
        ---
        L_po_exists := FALSE;
        ---
        L_pack_po := FALSE;
        ---
        if L_po_total_cost_tab.first is NOT NULL then
           FOR C_po_ind in 0 .. L_po_total_cost_ind - 1 LOOP
              if nvl(L_po_total_cost_tab(C_po_ind).order_no,0) = nvl(L_fiscal_doc_detail.requisition_no,0) then
                 L_po_total_cost_tab(C_po_ind).order_cost := L_po_total_cost_tab(C_po_ind).order_cost + (L_disc_unit_cost * L_fiscal_doc_detail.quantity);
                 ---
                 if L_head_freight_cost is NOT NULL then
                    ---
                    if (L_fiscal_doc_detail.pack_no is NULL or (L_fiscal_doc_detail.pack_no is NOT NULL and L_freight_at_pack = 0)) then
                       ---
                       L_po_total_cost_tab(C_po_ind).actual_order_freight := L_po_total_cost_tab(C_po_ind).actual_order_freight + NVL(L_actual_freight_po_item,0);
                       L_tot_freight_po := L_tot_freight_po + NVL(L_actual_freight_po_item,0);
                       ---
                    elsif (L_fiscal_doc_detail.pack_no is NOT NULL and L_freight_at_pack > 0) then
                       ---
                       FOR C_pack_ind in 0 .. L_po_pack_ind - 1 LOOP
                       ---
                          if (nvl(L_fiscal_doc_detail.requisition_no,0) = nvl(L_po_pack_tab(C_pack_ind).order_no,0) and L_fiscal_doc_detail.pack_no = NVL(L_po_pack_tab(C_pack_ind).pack_no,0)) then
                             ---
                             L_pack_po := TRUE;
                             ---
                          end if;
                          ---
                       END LOOP;
                       ---
                       if not L_pack_po then
                       ---
                          L_po_total_cost_tab(C_po_ind).actual_order_freight := L_po_total_cost_tab(C_po_ind).actual_order_freight + NVL(L_actual_freight_po_item,0);
                          L_tot_freight_po := L_tot_freight_po + NVL(L_actual_freight_po_item,0);
                          ---
                          L_po_pack.order_no := L_fiscal_doc_detail.requisition_no;
                          L_po_pack.pack_no := L_fiscal_doc_detail.pack_no;
                          ---
                          L_po_pack_tab(L_po_pack_ind) := L_po_pack;
                          L_po_pack_ind := L_po_pack_ind + 1;
                          ---
                       ---
                       end if;
                    end if;
                    ---
                 end if;
                 ---
                 if L_head_insur_cost is NOT NULL then
                    if (L_fiscal_doc_detail.pack_no is NULL or (L_fiscal_doc_detail.pack_no is NOT NULL and L_insur_at_pack = 0)) then
                       ---
                       L_po_total_cost_tab(C_po_ind).actual_order_insur := L_po_total_cost_tab(C_po_ind).actual_order_insur + NVL(L_actual_insur_po_item,0);
                       L_tot_insur_po := L_tot_insur_po + NVL(L_actual_insur_po_item,0);
                       ---
                    elsif (L_fiscal_doc_detail.pack_no is NOT NULL and L_insur_at_pack > 0) then
                       ---
                       FOR C_pack_ind in 0 .. L_po_pack_ind - 1 LOOP
                       ---
                          if (nvl(L_fiscal_doc_detail.requisition_no,0) = nvl(L_po_pack_tab(C_pack_ind).order_no,0) and L_fiscal_doc_detail.pack_no = NVL(L_po_pack_tab(C_pack_ind).pack_no,0)) then
                              ---
                              L_pack_po := TRUE;
                              ---
                           end if;
                           ---
                       END LOOP;
                       ---
                       if not L_pack_po then
                       ---
                          L_po_total_cost_tab(C_po_ind).actual_order_insur := L_po_total_cost_tab(C_po_ind).actual_order_insur + NVL(L_actual_insur_po_item,0);
                          L_tot_insur_po := L_tot_insur_po + NVL(L_actual_insur_po_item,0);
                          ---
                          L_po_pack.order_no := L_fiscal_doc_detail.requisition_no;
                          L_po_pack.pack_no := L_fiscal_doc_detail.pack_no;
                          ---
                          L_po_pack_tab(L_po_pack_ind) := L_po_pack;
                          L_po_pack_ind := L_po_pack_ind + 1;
                          ---
                       ---
                       end if;
                    end if;
                    ---
                 end if;
                 ---
                 if L_head_other_cost is NOT NULL then
                    if (L_fiscal_doc_detail.pack_no is NULL or (L_fiscal_doc_detail.pack_no is NOT NULL and L_other_at_pack = 0)) then
                       ---
                       L_po_total_cost_tab(C_po_ind).actual_order_other := L_po_total_cost_tab(C_po_ind).actual_order_other + NVL(L_actual_other_po_item,0);
                       L_tot_other_po := L_tot_other_po + NVL(L_actual_other_po_item,0);
                       ---
                    elsif (L_fiscal_doc_detail.pack_no is NOT NULL and L_other_at_pack > 0) then
                       ---
                       FOR C_pack_ind in 0 .. L_po_pack_ind - 1 LOOP
                       ---
                          if (nvl(L_fiscal_doc_detail.requisition_no,0) = nvl(L_po_pack_tab(C_pack_ind).order_no,0) and L_fiscal_doc_detail.pack_no = NVL(L_po_pack_tab(C_pack_ind).pack_no,0)) then
                             ---
                             L_pack_po := TRUE;
                             ---
                          end if;
                          ---
                       END LOOP;
                       ---
                       if not L_pack_po then
                       ---
                          L_po_total_cost_tab(C_po_ind).actual_order_other := L_po_total_cost_tab(C_po_ind).actual_order_other + NVL(L_actual_other_po_item,0);
                          L_tot_other_po := L_tot_other_po + NVL(L_actual_other_po_item,0);
                                 ---
                          L_po_pack.order_no := L_fiscal_doc_detail.requisition_no;
                          L_po_pack.pack_no := L_fiscal_doc_detail.pack_no;
                          ---
                          L_po_pack_tab(L_po_pack_ind) := L_po_pack;
                          L_po_pack_ind := L_po_pack_ind + 1;
                          ---
                       ---
                       end if;
                    end if;
                    ---
                 end if;
                 ---
                 L_po_exists := TRUE;
              end if;
           END LOOP;
        end if;
        ---
        if L_po_exists = FALSE then
           if L_head_freight_cost is NOT NULL then
              L_tot_freight_po := L_tot_freight_po + NVL(L_actual_freight_po_item,0);
              L_po_total_cost.actual_order_freight := NVL(L_actual_freight_po_item,0);
           end if;
           ---
           if L_head_insur_cost is NOT NULL then
              L_tot_insur_po := L_tot_insur_po + NVL(L_actual_insur_po_item,0);
              L_po_total_cost.actual_order_insur := NVL(L_actual_insur_po_item,0);
           end if;
           if L_head_other_cost is NOT NULL then
              L_tot_other_po := L_tot_other_po + NVL(L_actual_other_po_item,0);
              L_po_total_cost.actual_order_other := NVL(L_actual_other_po_item,0);
           end if;
           L_po_total_cost.order_no := L_fiscal_doc_detail.requisition_no;
           L_po_total_cost.order_cost := L_disc_unit_cost * L_fiscal_doc_detail.quantity;
           ---
           L_po_total_cost_tab(L_po_total_cost_ind) := L_po_total_cost;
           L_po_total_cost_ind := L_po_total_cost_ind + 1;
           ---
           L_po_pack.order_no := L_fiscal_doc_detail.requisition_no;
           L_po_pack.pack_no := L_fiscal_doc_detail.pack_no;
           ---
           L_po_pack_tab(L_po_pack_ind) := L_po_pack;
                  L_po_pack_ind := L_po_pack_ind + 1;
           ---
        end if;
      else
         O_status := 0;
         L_error_message := SQL_LIB.CREATE_MSG('ORFM_UNIT_COST_NOT_ZERO',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         L_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id)) = FALSE then
            return FALSE;
         end if;
         return TRUE;
      end if;  ---
   END LOOP;
   ---
   close C_FM_FISCAL_DOC_DETAIL;
   ---
   FOR C_po_ind in 0 .. L_po_total_cost_ind - 1 LOOP
      ---
      if L_head_freight_cost is NOT NULL then
         ---finding the apportioned freight cost applicable to PO on the NF
         if L_freight_po_exists = FALSE then
            L_po_total_cost_tab(C_po_ind).appr_order_freight := (L_po_total_cost_tab(C_po_ind).order_cost * L_head_freight_cost) / L_total_nf_disc_cost;
         elsif L_freight_po_exists = TRUE then
            L_po_total_cost_tab(C_po_ind).appr_order_freight := (L_head_freight_cost * NVL(L_po_total_cost_tab(C_po_ind).actual_order_freight,0)) / L_tot_freight_po;
         end if;
      end if;
      ---
      if L_head_insur_cost is NOT NULL then
         ---finding the apportioned insurance cost applicable to PO on the NF
         if L_insur_po_exists = FALSE then
            L_po_total_cost_tab(C_po_ind).appr_order_insur := (L_po_total_cost_tab(C_po_ind).order_cost * L_head_insur_cost) / L_total_nf_disc_cost;
         elsif L_insur_po_exists = TRUE then
            L_po_total_cost_tab(C_po_ind).appr_order_insur := (L_head_insur_cost * NVL(L_po_total_cost_tab(C_po_ind).actual_order_insur,0)) / L_tot_insur_po;
         end if;
      end if;
      ---
      if L_head_other_cost is NOT NULL then
         ---finding the apportioned misc cost applicable to PO on the NF
         if L_other_po_exists = FALSE then
            L_po_total_cost_tab(C_po_ind).appr_order_other := (L_po_total_cost_tab(C_po_ind).order_cost * L_head_other_cost) / L_total_nf_disc_cost;
         elsif L_other_po_exists = TRUE then
            L_po_total_cost_tab(C_po_ind).appr_order_other := (L_head_other_cost * NVL(L_po_total_cost_tab(C_po_ind).actual_order_other,0)) / L_tot_other_po;
         end if;
      end if;
      ---
   END LOOP;
   ---
   L_table := 'FM_FISCAL_DOC_DETAIL';
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   LOOP
      L_table := 'FM_FISCAL_DOC_DETAIL';
      fetch C_FM_FISCAL_DOC_DETAIL into L_fiscal_doc_detail;
      EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;
      ---
      open C_PO_ITEM_QTY(L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.location_id);
      ---
      fetch C_PO_ITEM_QTY into L_po_item_qty;
      ---
      close C_PO_ITEM_QTY;
      ---
      if L_fiscal_doc_detail.pack_no is NOT NULL then
      ---
         open C_COMP_ITEM_QTY(L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.item);
         ---
         fetch C_COMP_ITEM_QTY into L_comp_item_qty;
         ---
         close C_COMP_ITEM_QTY;
         ---
         L_po_item_qty := L_po_item_qty * L_comp_item_qty;
         ---
      ---
      end if;
      ---
      FOR C_po_ind in 0 .. L_po_total_cost_ind - 1 LOOP
         if nvl(L_po_total_cost_tab(C_po_ind).order_no,0) = nvl(L_fiscal_doc_detail.requisition_no,0) then
            L_po_cost := L_po_total_cost_tab(C_po_ind).order_cost;
            ---
            if L_head_freight_cost is NOT NULL then
               ---
               L_freight_at_pack := NULL;
               L_appr_po_freight  := L_po_total_cost_tab(C_po_ind).appr_order_freight;
               L_total_po_freight := L_po_total_cost_tab(C_po_ind).actual_order_freight;
               ---item level apportioned freight for items not having freight defined
               if L_freight_po_exists = FALSE then
                  L_po_item_cost         := L_fiscal_doc_detail.unit_cost_with_disc * L_fiscal_doc_detail.quantity;
                  L_po_item_unit_freight := ((L_appr_po_freight * L_po_item_cost) / L_po_cost) / L_fiscal_doc_detail.quantity;
               ---item level apportioned freight for items having freight defined
               elsif L_freight_po_exists = TRUE then
                  open C_COST_COMP_PO (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.pack_ind, C_freight_comp_id, L_fiscal_doc_detail.location_id);
                  ---
                  fetch C_COST_COMP_PO into L_actual_freight_po_item;
                  L_actual_freight_po_item := (L_actual_freight_po_item) * L_fiscal_doc_detail.quantity;
                  if C_COST_COMP_PO%NOTFOUND then
                     L_actual_freight_po_item := NULL;
                  end if;
                  ---
                  if L_actual_freight_po_item > 0 and L_fiscal_doc_detail.pack_no is not null then
                     ---
                     open C_GET_EXP_PACK (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, C_freight_comp_id, L_fiscal_doc_detail.location_id);
                     fetch C_GET_EXP_PACK into L_freight_at_pack;
                     close C_GET_EXP_PACK;
                     ---
                  end if;
                  ---
                  close C_COST_COMP_PO;
                  ---
                  if L_freight_at_pack > 0 then
                     L_actual_freight_po_item := L_actual_freight_po_item / L_comp_item_qty;
                  end if;
                  ---
                  if NVL(L_actual_freight_po_item,0) = 0 then
                     L_po_item_unit_freight := NULL;
                  else
                     ---
                     if L_actual_freight_po_item > 0 and L_fiscal_doc_detail.pack_no is not null and L_freight_at_pack > 0 then
                        ---
                        open C_GET_COMP_APPR_CC (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.item, ((L_appr_po_freight * L_actual_freight_po_item) / L_total_po_freight));
                        fetch C_GET_COMP_APPR_CC into L_po_item_unit_freight;
                        close C_GET_COMP_APPR_CC;
                        ---
                     elsif ((L_actual_freight_po_item > 0 and L_fiscal_doc_detail.pack_no is not null and L_freight_at_pack = 0) or
                           (L_actual_freight_po_item > 0 and L_fiscal_doc_detail.pack_no is null)) then
                        ---
                        L_po_item_unit_freight := ((L_appr_po_freight * L_actual_freight_po_item) / L_total_po_freight) / L_fiscal_doc_detail.quantity;
                        ---
                     end if;
                     ---
                  end if;
               end if;
               ---
            end if;
            ---
            if L_head_insur_cost is NOT NULL then
               ---
               L_insur_at_pack := NULL;
               L_appr_po_insur  := L_po_total_cost_tab(C_po_ind).appr_order_insur;
               L_total_po_insur := L_po_total_cost_tab(C_po_ind).actual_order_insur;
               ---item level apportioned insurance for items not having insurance defined
               if L_insur_po_exists = FALSE then
                  L_po_item_cost         := L_fiscal_doc_detail.unit_cost_with_disc * L_fiscal_doc_detail.quantity;
                  L_po_item_unit_insur := ((L_appr_po_insur * L_po_item_cost) / L_po_cost) / L_fiscal_doc_detail.quantity;
               ---item level apportioned insurance for items having insurance defined
               elsif L_insur_po_exists = TRUE then
                  open C_COST_COMP_PO (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.pack_ind, C_insur_comp_id, L_fiscal_doc_detail.location_id);
                  ---
                  fetch C_COST_COMP_PO into L_actual_insur_po_item;
                  L_actual_insur_po_item := (L_actual_insur_po_item) * L_fiscal_doc_detail.quantity;
                  if C_COST_COMP_PO%NOTFOUND then
                     L_actual_insur_po_item := NULL;
                  end if;
                  ---
                  if L_actual_insur_po_item > 0 and L_fiscal_doc_detail.pack_no is not null then
                     ---
                     open C_GET_EXP_PACK (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, C_insur_comp_id, L_fiscal_doc_detail.location_id);
                     fetch C_GET_EXP_PACK into L_insur_at_pack;
                     close C_GET_EXP_PACK;
                     ---
                  end if;
                  ---
                  close C_COST_COMP_PO;
                  ---
                  if L_insur_at_pack > 0 then
                     L_actual_insur_po_item := L_actual_insur_po_item / L_comp_item_qty;
                  end if;
                  ---
                  if NVL(L_actual_insur_po_item,0) = 0 then
                     L_po_item_unit_insur := NULL;
                  else
                     ---
                     if L_actual_insur_po_item > 0 and L_fiscal_doc_detail.pack_no is not null and L_insur_at_pack > 0 then
                        ---
                        open C_GET_COMP_APPR_CC (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.item, ((L_appr_po_insur * L_actual_insur_po_item) / L_total_po_insur));
                        fetch C_GET_COMP_APPR_CC into L_po_item_unit_insur;
                        close C_GET_COMP_APPR_CC;
                        ---
                     elsif ((L_actual_insur_po_item > 0 and L_fiscal_doc_detail.pack_no is not null and L_insur_at_pack = 0) or
                           (L_actual_insur_po_item > 0 and L_fiscal_doc_detail.pack_no is null)) then
                        ---
                        L_po_item_unit_insur := ((L_appr_po_insur * L_actual_insur_po_item) / L_total_po_insur) / L_fiscal_doc_detail.quantity;
                        ---
                     end if;
                     ---
                  end if;
               end if;
               ---
            end if;
            ---
            if L_head_other_cost is NOT NULL then
               ---
               L_other_at_pack := NULL;
               L_appr_po_other  := L_po_total_cost_tab(C_po_ind).appr_order_other;
               L_total_po_other := L_po_total_cost_tab(C_po_ind).actual_order_other;
               ---item level apportioned other costs for items not having misc costs defined
               if L_other_po_exists = FALSE then
                  L_po_item_cost         := L_fiscal_doc_detail.unit_cost_with_disc * L_fiscal_doc_detail.quantity;
                  L_po_item_unit_other := ((L_appr_po_other * L_po_item_cost) / L_po_cost) / L_fiscal_doc_detail.quantity;
               ---item level apportioned insurance for items having insurance defined
               elsif L_other_po_exists = TRUE then
                  open C_COST_COMP_PO (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.pack_ind, C_other_comp_id, L_fiscal_doc_detail.location_id);
                  ---
                  fetch C_COST_COMP_PO into L_actual_other_po_item;
                  L_actual_other_po_item := (L_actual_other_po_item) * L_fiscal_doc_detail.quantity;
                  if C_COST_COMP_PO%NOTFOUND then
                     L_actual_other_po_item := NULL;
                  end if;
                  ---
                  if L_actual_other_po_item > 0 and L_fiscal_doc_detail.pack_no is not null then
                     ---
                     open C_GET_EXP_PACK (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.item, L_fiscal_doc_detail.pack_no, C_other_comp_id, L_fiscal_doc_detail.location_id);
                     fetch C_GET_EXP_PACK into L_other_at_pack;
                     close C_GET_EXP_PACK;
                     ---
                  end if;
                  ---
                  close C_COST_COMP_PO;
                  ---
                  if L_other_at_pack > 0 then
                     L_actual_other_po_item := L_actual_other_po_item / L_comp_item_qty;
                  end if;
                  ---
                  if NVL(L_actual_other_po_item,0) = 0 then
                     L_po_item_unit_other := NULL;
                  else
                     ---
                     if L_actual_other_po_item > 0 and L_fiscal_doc_detail.pack_no is not null and L_other_at_pack > 0 then
                        ---
                        open C_GET_COMP_APPR_CC (L_fiscal_doc_detail.requisition_no, L_fiscal_doc_detail.pack_no, L_fiscal_doc_detail.item, ((L_appr_po_other * L_actual_other_po_item) / L_total_po_other));
                        fetch C_GET_COMP_APPR_CC into L_po_item_unit_other;
                        close C_GET_COMP_APPR_CC;
                        ---
                     elsif ((L_actual_other_po_item > 0 and L_fiscal_doc_detail.pack_no is not null and L_other_at_pack = 0) or
                           (L_actual_other_po_item > 0 and L_fiscal_doc_detail.pack_no is null)) then
                        ---
                        L_po_item_unit_other := ((L_appr_po_other * L_actual_other_po_item) / L_total_po_other) / L_fiscal_doc_detail.quantity;
                        ---
                     end if;
                     ---
                  end if;
               end if;
               ---
            end if;
            ---
            open C_LOCK_FM_FISCAL_DETAIL(L_fiscal_doc_detail.fiscal_doc_line_id);
            close C_LOCK_FM_FISCAL_DETAIL;
            ---
            update fm_fiscal_doc_detail
               set freight_cost         = L_po_item_unit_freight,
                   insurance_cost       = L_po_item_unit_insur,
                   other_expenses_cost  = L_po_item_unit_other,
                   last_update_datetime = SYSDATE,
                   last_update_id       = USER
             where fiscal_doc_line_id   = L_fiscal_doc_detail.fiscal_doc_line_id;
               ---
            EXIT;
            ---
         end if;
         ---
      END LOOP;
      ---
   END LOOP;
   ---
   close C_FM_FISCAL_DOC_DETAIL;
   ---
   if (GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;
   ---
   if L_triang_ind = 'Y' then
   ---
      if (GET_OTHER_NF(O_error_message, L_other_fiscal_doc_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      if L_other_fiscal_doc_id is NOT NULL then
          if not O_other_proc then
          ---
             O_other_proc := TRUE;
             if (CALC_UNIT_ITEM_COST_COMP(O_error_message, O_status, O_other_proc, L_other_fiscal_doc_id) = FALSE) then
                return FALSE;
             end if;
          ---
          end if;
      end if;
      ---
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_DETAIL',
                                             'fiscal_doc_line_id',
                                             TO_CHAR(SQLCODE));
      --
      if C_LOCK_FM_FISCAL_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_DETAIL;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      if C_FM_FISCAL_DOC_HEADER%ISOPEN then
         close C_FM_FISCAL_DOC_HEADER;
      end if;
      ---
      if C_COST_COMP_PO%ISOPEN then
         close C_COST_COMP_PO;
      end if;
      ---
      if C_PO_ITEM_QTY%ISOPEN then
         close C_PO_ITEM_QTY;
      end if;
      ---
      if C_COMP_ITEM_QTY%ISOPEN then
         close C_COMP_ITEM_QTY;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := 0;
      return FALSE;
END CALC_UNIT_ITEM_COST_COMP;
--------------------------------------------------------------------------------
FUNCTION UPDATE_CALC_VALUES(O_error_message IN OUT VARCHAR2,
                            O_status        IN OUT INTEGER,
                            O_other_proc    IN OUT BOOLEAN,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program                     VARCHAR2(50) := 'FM_TAXES_SQL.UPDATE_CALC_VALUES';
   L_table                       VARCHAR2(30);
   L_key                         VARCHAR2(100);
   L_error_message               VARCHAR2(255) := NULL;
   L_no                          VARCHAR2(1) := 'N';
   L_yes                         VARCHAR2(1) := 'Y';
   L_comp_nf_ind                 FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind                  ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_fiscal_doc_id         FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   L_fiscal_doc_id               FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE;
   ---
   C_ipi_code                    FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE  := 'IPI';
   C_icmsst_code                 FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE  := 'ICMSST';
   RECORD_LOCKED                 EXCEPTION;
   PRAGMA                        EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   L_total_expenses              FM_FISCAL_DOC_HEADER.FREIGHT_COST%TYPE := 0;
   --L_total_ipi_cost_calc         FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_VALUE%TYPE := 0;
   L_total_cost_calc_nic         FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_VALUE%TYPE := 0;
   L_total_doc_extra_costs       FM_FISCAL_DOC_HEADER.EXTRA_COSTS_CALC%TYPE;
   L_total_merch_cost            FM_FISCAL_DOC_HEADER.TOTAL_ITEM_VALUE%TYPE;
   L_total_service_cost          FM_FISCAL_DOC_HEADER.TOTAL_SERV_VALUE%TYPE;
   L_total_doc_cost              FM_FISCAL_DOC_HEADER.TOTAL_DOC_VALUE%TYPE;
   L_total_item_rate             FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_total_discounts             FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_total_item_cost_calc        FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   L_total_service_rate          FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_total_service_cost_calc     FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   L_total_cost_calc_disc        FM_FISCAL_DOC_HEADER.TOTAL_DOC_VALUE_WITH_DISC%TYPE;


   ---
   cursor C_TOTAL_EXPENSES is
      select NVL(fdh.freight_cost, 0) +NVL(fdh.insurance_cost, 0) +NVL(fdh.other_expenses_cost, 0)
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_TOTAL_COST_CALC_NIC (P_fiscal_doc_id   FM_FISCAL_DOC_TAX_HEAD.FISCAL_DOC_ID%TYPE) is
      select SUM(NVL(fthx.total_value,0))
        from fm_fiscal_doc_tax_head fthx
       where fthx.fiscal_doc_id in (I_fiscal_doc_id, NVL(P_fiscal_doc_id, I_fiscal_doc_id)) and
             fthx.vat_code in (select vc.vat_code
                                 from vat_codes vc,
                                      fm_tax_codes ftc
                                where vc.vat_Code  = ftc.tax_code
                                  and vc.incl_nic_ind = L_no
                                  and ftc.matching_ind = L_yes);
   ---
   cursor C_TOTAL_MERCH_COST is
      select DECODE(SUM(merch_total.item_total),0,0,
                                                  (SUM(merch_total.item_total)))
        from (select DECODE(SUM(fdd.total_cost), 0, 0,
                                        (SUM(DECODE(via.service_ind, L_no, fdd.total_cost, 0)))) item_total
                from fm_fiscal_doc_detail fdd,
                     v_br_item_fiscal_attrib via
               where via.item = fdd.item
                 and fdd.fiscal_doc_id = I_fiscal_doc_id
                 and fdd.pack_ind = L_no
               union all
               select DECODE(SUM(fdd.total_cost), 0, 0,
                                                ( SUM(fdd.total_cost))) item_total
                from fm_fiscal_doc_detail fdd
               where fdd.fiscal_doc_id = I_fiscal_doc_id
                 and fdd.pack_ind = L_yes) merch_total;
   ---
   cursor C_TOTAL_SERVICE_COST is
      select DECODE(SUM(fdd.total_cost), 0, 0,
                                        (SUM(DECODE(via.service_ind, L_yes, fdd.total_cost, 0))))
        from fm_fiscal_doc_detail fdd,
             v_br_item_fiscal_attrib via
       where via.item = fdd.item
         and fdd.fiscal_doc_id = I_fiscal_doc_id
         and fdd.pack_ind = L_no;
   ---
   cursor C_DOC_TOTAL is
      select SUM(NVL(fdd.total_cost,0))
        from fm_fiscal_doc_detail fdd
       where fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_TOTAL_DISCOUNTS is
      select DECODE(fdh.discount_type, C_DISC_VALUE, fdh.total_discount_value,
                                       C_DISC_PERC, (fdh.total_discount_value * (NVL((select SUM(fdd.total_cost)
                                                                                from fm_fiscal_doc_detail fdd
                                                                               where fdd.fiscal_doc_id = fdh.fiscal_doc_id),0)/100)))
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_LOCK_FM_FISCAL_HEADER is
      select 'X'
        from fm_fiscal_doc_header
       where fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
   L_table := 'FM_FISCAL_DOC_HEADER';
   ---
   open C_TOTAL_EXPENSES;
   ---
   fetch C_TOTAL_EXPENSES into L_total_expenses;
   ---
   close C_TOTAL_EXPENSES;
   ---
   if (GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;
   ---
   if L_triang_ind = 'Y' then
   ---
      if (GET_OTHER_NF(O_error_message, L_other_fiscal_doc_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      L_fiscal_doc_id := L_other_fiscal_doc_id;
   ---
   else
   ---
      L_fiscal_doc_id := NULL;
   ---
   end if;
   ---
   open C_TOTAL_COST_CALC_NIC (L_fiscal_doc_id);
   ---
   fetch C_TOTAL_COST_CALC_NIC into L_total_cost_calc_nic;
   ---
   close C_TOTAL_COST_CALC_NIC;
   ---
   L_table := 'FM_FISCAL_DOC_DETAIL';
   ---
   open C_TOTAL_MERCH_COST;
   ---
   fetch C_TOTAL_MERCH_COST into L_total_merch_cost;
   ---
   close C_TOTAL_MERCH_COST;
   ---
   open C_TOTAL_SERVICE_COST;
   ---
   fetch C_TOTAL_SERVICE_COST into L_total_service_cost;
   ---
   close C_TOTAL_SERVICE_COST;
   ---
   open C_DOC_TOTAL;
   ---
   fetch C_DOC_TOTAL into L_total_doc_cost;
   ---
   close C_DOC_TOTAL;
   ---
   L_total_item_rate       := L_total_merch_cost / L_total_doc_cost * 100;
   L_total_service_rate    := L_total_service_cost / L_total_doc_cost * 100;
   ---
   L_table := 'FM_FISCAL_DOC_HEADER';
   ---
   open C_TOTAL_DISCOUNTS;
   ---
   fetch C_TOTAL_DISCOUNTS into L_total_discounts;
   ---
   close C_TOTAL_DISCOUNTS;
   ---
   L_table := 'FM_FISCAL_DOC_DETAIL';
   ---
   L_total_item_cost_calc      := (L_total_merch_cost - (L_total_item_rate / 100 * NVL(L_total_discounts,0)));
   L_total_service_cost_calc   := (L_total_service_cost - (L_total_service_rate / 100 * NVL(L_total_discounts,0)));
   ---
   L_table := 'FM_FISCAL_DOC_HEADER';
   ---
   L_total_doc_extra_costs := NVL(L_total_expenses,0) + NVL(L_total_cost_calc_nic,0);
   L_total_cost_calc_disc  := NVL(L_total_item_cost_calc,0) + NVL(L_total_service_cost_calc,0);
   ---
   open C_LOCK_FM_FISCAL_HEADER;
   close C_LOCK_FM_FISCAL_HEADER;
   ---
   ---
   update fm_fiscal_doc_header fdh
      set fdh.extra_costs_calc          = L_total_doc_extra_costs,
          fdh.total_doc_value_with_disc = L_total_cost_calc_disc,
          last_update_datetime          = SYSDATE,
          last_update_id                = USER
    where fdh.fiscal_doc_id             = I_fiscal_doc_id;
   ---
   L_table := 'FM_FISCAL_DOC_DETAIL';
   ---
   if L_triang_ind = 'Y' then
   ---
      if (GET_OTHER_NF(O_error_message, L_other_fiscal_doc_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      if L_other_fiscal_doc_id is NOT NULL then
          if not O_other_proc then
          ---
             O_other_proc := TRUE;
             if (UPDATE_CALC_VALUES(O_error_message, O_status, O_other_proc, L_other_fiscal_doc_id) = FALSE) then
                return FALSE;
             end if;
          ---
          end if;
      end if;
      ---
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'C_LOCK_FM_FISCAL_HEADER',
                                             L_key,
                                             TO_CHAR(SQLCODE));
      --
      if C_LOCK_FM_FISCAL_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_HEADER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      if C_TOTAL_EXPENSES%ISOPEN then
         close C_TOTAL_EXPENSES;
      end if;
      ---
      if C_TOTAL_COST_CALC_NIC%ISOPEN then
         close C_TOTAL_COST_CALC_NIC;
      end if;
      ---
      if C_TOTAL_DISCOUNTS%ISOPEN then
         close C_TOTAL_DISCOUNTS;
      end if;
      ---
      if C_TOTAL_MERCH_COST%ISOPEN then
         close C_TOTAL_MERCH_COST;
      end if;
      ---
      if C_TOTAL_SERVICE_COST%ISOPEN then
         close C_TOTAL_SERVICE_COST;
      end if;
      ---
      if C_DOC_TOTAL%ISOPEN then
         close C_DOC_TOTAL;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := 0;
      return FALSE;
END UPDATE_CALC_VALUES;
---------------------------------------------------------------------------------
FUNCTION CHECK_VALUE_INFORMED(O_error_message IN OUT VARCHAR2,
                              O_status        IN OUT INTEGER,
                              O_other_proc    IN OUT BOOLEAN,
                              I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program        VARCHAR2(50) := 'FM_FISCAL_VALIDATION_SQL.CHECK_VALUE_INFORMED';
   L_table          VARCHAR2(30);
   L_key            VARCHAR2(100);
   L_error_message  VARCHAR2(255) := NULL;
   L_yes            VARCHAR2(1) := 'Y';
   L_no             VARCHAR2(1) := 'N';
   ---
   ---
   L_dif_perc                    NUMBER;
   L_description                 FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_number_value                FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value                FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value                  FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_div_value                   NUMBER;
   L_calc_tol_type               FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_calc_tol_val                FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_comp_nf_ind                 FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind                  ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_fiscal_doc_id         FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   L_variance_value              FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE;
   ---

   L_total_detail_expenses              FM_FISCAL_DOC_HEADER.FREIGHT_COST%TYPE := 0;
   L_total_detail_cost_calc_nic         FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_VALUE%TYPE := 0;
   L_total_detail_doc_extra_costs       FM_FISCAL_DOC_HEADER.EXTRA_COSTS_CALC%TYPE;

   cursor C_FM_FISCAL_DOC_HEADER is
      select fdh.location_id,
             fdh.location_type,
             fdh.module,
             fdh.key_value_1,
             fdh.key_value_2,
             fdh.total_serv_value,
             fdh.total_item_value,
             fdh.total_doc_value,
             fdh.discount_type,
             fdh.total_discount_value,
             fdh.extra_costs_calc,
             fdh.total_doc_value_with_disc,
             fdh.total_item_value + fdh.total_serv_value - fdh.total_doc_value_with_disc total_doc_discount
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   R_fiscal_header C_FM_FISCAL_DOC_HEADER%ROWTYPE;
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id,
             fdd.quantity,
             fdd.unit_cost,
             (fdd.unit_cost - NVL(fdd.unit_item_disc,0)) * fdd.quantity total_cost_calc,
             fdd.total_cost,
             fdd.discount_type,
             fdd.discount_value
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id
         and (NVL((fdd.unit_cost - NVL(fdd.unit_item_disc,0)) * fdd.quantity,0) <> NVL(fdd.total_cost,0)
          or NVL(fdd.total_cost,0) <= 0);
   ---
   R_fiscal_detail C_FM_FISCAL_DOC_DETAIL%ROWTYPE;
   ---
   cursor C_FM_FISCAL_DOC_DETAIL_TOTAL is
      select SUM(detail_totals.total_services) total_services,
             SUM(detail_totals.total_merch) total_merch,
             SUM(detail_totals.total_doc) total_doc,
             SUM(detail_totals.total_doc_with_disc) total_doc_with_disc,
             SUM(detail_totals.num_lines) num_lines
        from (select SUM(DECODE(fia.service_ind,L_yes,NVL(fdd.total_cost,0),0)) total_services,
                     SUM(DECODE(fia.service_ind,L_no,NVL(fdd.total_cost,0),0)) total_merch,
                     SUM(NVL(fdd.total_cost,0)) total_doc,
                     SUM(NVL(fdd.unit_cost_with_disc,0)*fdd.quantity) total_doc_with_disc,
                     COUNT(fdd.fiscal_doc_line_id) num_lines
                from fm_fiscal_doc_detail fdd, v_br_item_fiscal_attrib fia
               where fdd.fiscal_doc_id = I_fiscal_doc_id
                 and fia.item = fdd.item
                 and fdd.pack_ind = L_no
              union all
              select 0 total_services,
                     SUM(NVL(fdd.total_cost,0)) total_merch,
                     SUM(NVL(fdd.total_cost,0)) total_doc,
                     SUM(NVL(fdd.unit_cost_with_disc,0)*fdd.quantity) total_doc_with_disc,
                     COUNT(fdd.fiscal_doc_line_id) num_lines
                from fm_fiscal_doc_detail fdd
               where fdd.fiscal_doc_id = I_fiscal_doc_id
                 and fdd.pack_ind = L_yes) detail_totals
          ;
   ---
   cursor C_LOCK_FM_FISCAL_HEADER is
      select 'X'
        from fm_fiscal_doc_header
       where fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
   R_fiscal_detail_total  C_FM_FISCAL_DOC_DETAIL_TOTAL%ROWTYPE;
   ---

   cursor C_TOTAL_DETAIL_EXPENSES is
      select SUM(NVL(fdd.freight_cost*fdd.quantity, 0)) + SUM(NVL(fdd.insurance_cost*fdd.quantity, 0)) +SUM(NVL(fdd.other_expenses_cost*fdd.quantity, 0))
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_TOTAL_DETAIL_COST_CALC_NIC  is
       select decode(SUM(NVL(ftdx.appr_tax_value,0)),0,SUM(NVL(ftdx.total_value,0)),SUM(NVL(ftdx.appr_tax_value,0)))
        from fm_fiscal_doc_tax_detail ftdx, fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_line_id = ftdx.fiscal_doc_line_id and
             fdd.fiscal_doc_id = I_fiscal_doc_id and
             ftdx.vat_code in (select vc.vat_code
                                 from vat_codes vc,
                                      fm_tax_codes ftc
                                where vc.vat_Code  = ftc.tax_code
                                  and vc.incl_nic_ind = L_no
                                  and ftc.matching_ind = L_yes);
BEGIN
   ---
   if FM_ERROR_LOG_SQL.CLEAR_ERROR(O_error_message,
                                   I_fiscal_doc_id,
                                   L_program) = FALSE then
      return FALSE;
   end if;
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_description,
                                         L_number_value,
                                         L_string_value,
                                         L_date_value,
                                         'CHAR',
                                         'CALC_TOL_TYPE') = FALSE then
      return FALSE;
   end if;
   ---
   L_calc_tol_type := L_string_value;
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_description,
                                         L_number_value,
                                         L_string_value,
                                         L_date_value,
                                         'NUMBER',
                                         'CALC_TOL_VALUE') = FALSE then
      return FALSE;
   end if;
   ---
   L_calc_tol_val := L_number_value;
   ---
   L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
   L_table := 'FM_FISCAL_DOC_HEADER';
   ---
   --- Gets NF header informations.
   open C_FM_FISCAL_DOC_HEADER;
   ---
   fetch C_FM_FISCAL_DOC_HEADER into R_fiscal_header;
   ---
   close C_FM_FISCAL_DOC_HEADER;
   ---

   --- Checks tolerance at LINE level.
   L_table := 'FM_FISCAL_DOC_DETAIL';
   ---
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   LOOP
      ---
      fetch C_FM_FISCAL_DOC_DETAIL into R_fiscal_detail;
      EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;
      ---
      if NVL(R_fiscal_detail.total_cost,0) <= 0 then
         ---
         O_status := 0;
         L_error_message := SQL_LIB.CREATE_MSG('TOTAL_COST_MIN_VALUE',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         L_error_message,
                                         'Fiscal_doc_line_id: '||
                                         TO_CHAR(R_fiscal_detail.fiscal_doc_line_id)) = FALSE then
            return FALSE;
         end if;
         ---
      else
         ---
         if L_calc_tol_type = C_DISC_PERC then
            ---
            if (NVL(R_fiscal_detail.total_cost,0) = 0 and
                NVL(R_fiscal_detail.total_cost_calc,0) <> 0) or
                (NVL(R_fiscal_detail.total_cost,0) <> 0 and
                 NVL(R_fiscal_detail.total_cost_calc,0) = 0) then
               ---
               L_dif_perc := 100;
               ---
            else
               ---
               L_dif_perc := ABS(((NVL(R_fiscal_detail.total_cost,0) * 100) / NVL(R_fiscal_detail.total_cost_calc,1)) - 100);
               ---
            end if;
            ---
            if L_dif_perc > NVL(L_calc_tol_val,0) then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('ORFM_LINE_TOLERANCE_INF',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                               I_fiscal_doc_id,
                                               L_program,
                                               L_program,
                                               'E',
                                               'VALIDATION_ERROR',
                                               L_error_message,
                                               'Fiscal_doc_line_id: '||
                                               TO_CHAR(R_fiscal_detail.fiscal_doc_line_id)||
                                               ' Tolerance_type: '||L_calc_tol_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         elsif L_calc_tol_type = C_DISC_VALUE then
            ---
            if ABS(NVL(R_fiscal_detail.total_cost,0) - NVL(R_fiscal_detail.total_cost_calc,0)) > NVL(L_calc_tol_val,0) then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('ORFM_LINE_TOLERANCE_INF',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                               I_fiscal_doc_id,
                                               L_program,
                                               L_program,
                                               'E',
                                               'VALIDATION_ERROR',
                                               L_error_message,
                                               'Fiscal_doc_line_id: '||
                                               TO_CHAR(R_fiscal_detail.fiscal_doc_line_id)||
                                               ' Tolerance_type: '||L_calc_tol_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         else
            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_LINE_VALUE_INF',NULL,NULL,NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'Fiscal_doc_line_id: '||
                                            TO_CHAR(R_fiscal_detail.fiscal_doc_line_id)||
                                            ' No Tolerances') = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if;
      ---
   END LOOP;
   ---

   close C_FM_FISCAL_DOC_DETAIL;
   ---
   --- Get the total informed by type (merch., serv. and doc.)
   open C_FM_FISCAL_DOC_DETAIL_TOTAL;
   ---
   fetch C_FM_FISCAL_DOC_DETAIL_TOTAL into R_fiscal_detail_total;
   ---
   close C_FM_FISCAL_DOC_DETAIL_TOTAL;

   --- Will validate only if exists details on NFs
   if NVL(R_fiscal_detail_total.num_lines,0) > 0 then

      --- Checks tolerance at TOTAL level for total merchandise calculated.
      ---
      if NVL(R_fiscal_header.total_item_value,0) <> NVL(R_fiscal_detail_total.total_merch,0) then
         ---
         if L_calc_tol_type = C_DISC_PERC then
            ---
            if (NVL(R_fiscal_header.total_item_value,0) = 0 and
                NVL(R_fiscal_detail_total.total_merch,0) <> 0) or
               (NVL(R_fiscal_header.total_item_value,0) <> 0 and
                NVL(R_fiscal_detail_total.total_merch,0) = 0) then
               ---
               L_dif_perc := 100;
               ---
            else
               ---
               L_dif_perc := ABS(((NVL(R_fiscal_header.total_item_value,0) * 100) / NVL(R_fiscal_detail_total.total_merch,1)) - 100);
               ---
            end if;
            ---
            if L_dif_perc > NVL(L_calc_tol_val,0) then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOT_MERCH_TOLERA_INF',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                               I_fiscal_doc_id,
                                               L_program,
                                               L_program,
                                               'E',
                                               'VALIDATION_ERROR',
                                               L_error_message,
                                               'Tolerance_type: '||L_calc_tol_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         elsif L_calc_tol_type = C_DISC_VALUE then
            ---
            if ABS(NVL(R_fiscal_header.total_item_value,0) - NVL(R_fiscal_detail_total.total_merch,0)) > NVL(L_calc_tol_val,0) then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOT_MERCH_TOLERA_INF',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                               I_fiscal_doc_id,
                                               L_program,
                                               L_program,
                                               'E',
                                               'VALIDATION_ERROR',
                                               L_error_message,
                                               'Tolerance_type: '||L_calc_tol_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         else
            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_TOT_MERC_VAL_INF',NULL,NULL,NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'No Tolerances') = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if;

--- Checks tolerance at TOTAL level for total sercices calculated.
      if NVL(R_fiscal_header.total_serv_value,0) <> NVL(R_fiscal_detail_total.total_services,0) then
         ---
         if L_calc_tol_type = C_DISC_PERC then
            ---
            if (NVL(R_fiscal_header.total_serv_value,0) = 0 and
                NVL(R_fiscal_detail_total.total_services,0) <> 0) or
               (NVL(R_fiscal_header.total_serv_value,0) <> 0 and
                NVL(R_fiscal_detail_total.total_services,0) = 0) then
               ---
               L_dif_perc := 100;
               ---
            else
               ---
               L_dif_perc := ABS(((NVL(R_fiscal_header.total_serv_value,0) * 100) / NVL(R_fiscal_detail_total.total_services,1)) - 100);
               ---
            end if;
            ---
            if L_dif_perc > NVL(L_calc_tol_val,0) then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOT_SERV_TOLERA_INF',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                               I_fiscal_doc_id,
                                               L_program,
                                               L_program,
                                               'E',
                                               'VALIDATION_ERROR',
                                               L_error_message,
                                               'Tolerance_type: '||L_calc_tol_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         elsif L_calc_tol_type = C_DISC_VALUE then
            ---
            if ABS(NVL(R_fiscal_header.total_serv_value,0) - NVL(R_fiscal_detail_total.total_services,0)) > NVL(L_calc_tol_val,0) then
               O_status := 0;
               L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOT_SERV_TOLERA_INF',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
               if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                               I_fiscal_doc_id,
                                               L_program,
                                               L_program,
                                               'E',
                                               'VALIDATION_ERROR',
                                               L_error_message,
                                               'Tolerance_type: '||L_calc_tol_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         else
            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_TOT_SERV_VAL_INF',NULL,NULL,NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'No Tolerances') = FALSE then
               return FALSE;
            end if;
         end if;
      end if;



   if (GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;

   if L_triang_ind = 'N' or (L_triang_ind = 'Y' and L_comp_nf_ind = 'N') then
       L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
       L_table := 'FM_FISCAL_DOC_HEADER';
       ---
       open C_TOTAL_DETAIL_EXPENSES;
       ---
       fetch C_TOTAL_DETAIL_EXPENSES into L_total_detail_expenses;
       ---
       close C_TOTAL_DETAIL_EXPENSES;

       open C_TOTAL_DETAIL_COST_CALC_NIC;
       ---
       fetch C_TOTAL_DETAIL_COST_CALC_NIC into L_total_detail_cost_calc_nic;
       ---
       close C_TOTAL_DETAIL_COST_CALC_NIC;

       L_total_detail_doc_extra_costs := NVL(L_total_detail_expenses,0) + NVL(L_total_detail_cost_calc_nic,0);

    --- Checks tolerance at TOTAL level for total document calculated.
          if NVL(R_fiscal_header.total_doc_value,0) <> (NVL(R_fiscal_detail_total.Total_Doc_With_Disc,0) + L_total_detail_doc_extra_costs) or
             (NVL(R_fiscal_header.total_doc_value,0) <>
                (NVL(R_fiscal_detail_total.total_merch,0) +
                 NVL(R_fiscal_detail_total.total_services,0) +
                 L_total_detail_doc_extra_costs -
                 NVL(R_fiscal_header.Total_Doc_Discount,0))) then
             ---
             if L_calc_tol_type = C_DISC_PERC then
                ---
                if (NVL(R_fiscal_header.total_doc_value,0) = 0 and
                    NVL(R_fiscal_detail_total.total_doc,0) <> 0) or
                   (NVL(R_fiscal_header.total_doc_value,0) <> 0 and
                    NVL(R_fiscal_detail_total.total_doc,0) = 0) then
                   ---
                   L_dif_perc := 100;
                   ---
                else
                   ---
                   L_div_value := NVL(R_fiscal_detail_total.Total_Doc_With_Disc,0) + L_total_detail_doc_extra_costs;
                   if L_div_value = 0 then
                      L_div_value := 1;
                   end if;
                   L_dif_perc := ABS(((NVL(R_fiscal_header.total_doc_value,0) * 100) / (L_div_value)) - 100);
                   ---
                end if;
                ---
                if L_dif_perc > NVL(L_calc_tol_val,0) then
                   O_status := 0;
                   L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOTAL_TOLERA_INF',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
                   if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                                   I_fiscal_doc_id,
                                                   L_program,
                                                   L_program,
                                                   'E',
                                                   'VALIDATION_ERROR',
                                                   L_error_message,
                                                   'Tolerance_type: '||L_calc_tol_type) = FALSE then
                      return FALSE;
                   end if;
                else
                   L_variance_value   := NVL(R_fiscal_header.total_doc_value,0) - L_div_value;
                end if;
                ---
                open C_LOCK_FM_FISCAL_HEADER;
                close C_LOCK_FM_FISCAL_HEADER;
                ---
                ---
                update fm_fiscal_doc_header
                   set variance_cost  =  L_variance_value
                 where fiscal_doc_id  =  I_fiscal_doc_id;
                ---
             elsif L_calc_tol_type = C_DISC_VALUE then
                ---
                if ABS(NVL(R_fiscal_header.total_doc_value,0) - (NVL(R_fiscal_detail_total.Total_Doc_With_Disc,0) + L_total_detail_doc_extra_costs)) > NVL(L_calc_tol_val,0) then
                   O_status := 0;
                   L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOTAL_TOLERA_INF',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
                   if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                                   I_fiscal_doc_id,
                                                   L_program,
                                                   L_program,
                                                   'E',
                                                   'VALIDATION_ERROR',
                                                   L_error_message,
                                                   'Tolerance_type: '||L_calc_tol_type) = FALSE then
                      return FALSE;
                   end if;
                else
                   L_variance_value   := NVL(R_fiscal_header.total_doc_value,0) - (NVL(R_fiscal_detail_total.Total_Doc_With_Disc,0) + L_total_detail_doc_extra_costs);
                end if;
                ---
                open C_LOCK_FM_FISCAL_HEADER;
                close C_LOCK_FM_FISCAL_HEADER;
                ---
                ---
                update fm_fiscal_doc_header
                   set variance_cost  =  L_variance_value
                 where fiscal_doc_id  =  I_fiscal_doc_id;
                ---
             else
                O_status := 0;
                L_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_TOTAL_VAL_INF',NULL,NULL,NULL);
                if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                                I_fiscal_doc_id,
                                                L_program,
                                                L_program,
                                                'E',
                                                'VALIDATION_ERROR',
                                                L_error_message,
                                                'No Tolerances') = FALSE then
                   return FALSE;
                end if;
             end if;
          end if;
       end if; --- if exists details on NF
   ---
   end if;
   if (GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;
   ---

   if L_triang_ind = 'Y' then
   ---
      if (GET_OTHER_NF(O_error_message, L_other_fiscal_doc_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---

      if L_other_fiscal_doc_id is NOT NULL then
          if not O_other_proc then
          ---
             O_other_proc := TRUE;
             if (CHECK_VALUE_INFORMED(O_error_message, O_status, O_other_proc, L_other_fiscal_doc_id) = FALSE) then
                return FALSE;
             end if;
          ---
          end if;
      end if;
      ---
   ---
   end if;
   ---

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      if C_FM_FISCAL_DOC_HEADER%ISOPEN then
         close C_FM_FISCAL_DOC_HEADER;
      end if;
      ---
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      if C_LOCK_FM_FISCAL_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_HEADER;
      end if;
      ---
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_VALUE_INFORMED;

--------------------------------------------------------------------------------
FUNCTION CHECK_VALUE_INFORMED_VS_CALC(O_error_message IN OUT VARCHAR2,
                                      O_status        IN OUT INTEGER,
                                      O_other_proc    IN OUT BOOLEAN,
                                      I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)

   return BOOLEAN is
   ---
   L_program        VARCHAR2(100) := 'FM_FISCAL_VALIDATION_SQL.CHECK_VALUE_INFORMED_VS_CALC';
   L_table          VARCHAR2(30);
   L_key            VARCHAR2(100);
   ---
   L_error_message               VARCHAR2(255) := NULL;
   L_dif_perc                    NUMBER;
   L_description                 FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_number_value                FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value                FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value                  FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_calc_tol_type               FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_calc_tol_val                FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_comp_nf_ind                 FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind                  ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_fiscal_doc_id         FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id, fdd.total_calc_cost, fdd.total_cost
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id
         and NVL(fdd.total_calc_cost,0) <> NVL(fdd.total_cost,0);
   ---
   R_fiscal_detail C_FM_FISCAL_DOC_DETAIL%ROWTYPE;
   ---
   ---
BEGIN
   ---
   --- Cleans up old error messages
   if FM_ERROR_LOG_SQL.CLEAR_ERROR(O_error_message,
                                   I_fiscal_doc_id,
                                   L_program) = FALSE then
      return FALSE;
   end if;
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_description,
                                         L_number_value,
                                         L_string_value,
                                         L_date_value,
                                         'CHAR',
                                         'CALC_TOL_TYPE') = FALSE then
      return FALSE;
   end if;
   ---
   L_calc_tol_type := L_string_value;
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_description,
                                         L_number_value,
                                         L_string_value,
                                         L_date_value,
                                         'NUMBER',
                                         'CALC_TOL_VALUE') = FALSE then
      return FALSE;
   end if;
   ---
   L_calc_tol_val := L_number_value;
   ---
   L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
   L_table := 'FM_FISCAL_DOC_DETAIL';
   ---
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   LOOP
      ---
      fetch C_FM_FISCAL_DOC_DETAIL into R_fiscal_detail;
      EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;
      ---
      if L_calc_tol_type = C_DISC_PERC then
         ---
         if (NVL(R_fiscal_detail.total_calc_cost,0) = 0 and
             NVL(R_fiscal_detail.total_cost,0) <> 0) or
            (NVL(R_fiscal_detail.total_calc_cost,0) <> 0 and
             NVL(R_fiscal_detail.total_cost,0) = 0) then
            ---
            L_dif_perc := 100;
            ---
         else
            ---
            L_dif_perc := ABS(((NVL(R_fiscal_detail.total_calc_cost,0) * 100) / NVL(R_fiscal_detail.total_cost,1)) - 100);
            ---
         end if;
         ---
         if L_dif_perc > NVL(L_calc_tol_val,0) then
            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_LINE_TOLERANCE_CALC',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'Fiscal_doc_line_id: '||
                                            TO_CHAR(R_fiscal_detail.fiscal_doc_line_id)||
                                            ' Tolerance_type: '||L_calc_tol_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      elsif L_calc_tol_type = C_DISC_VALUE then
         ---
         if ABS(NVL(R_fiscal_detail.total_calc_cost,0) - NVL(R_fiscal_detail.total_cost,0)) > NVL(L_calc_tol_val,0) then
            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_LINE_TOLERANCE_CALC',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'Fiscal_doc_line_id: '||
                                            TO_CHAR(R_fiscal_detail.fiscal_doc_line_id)||
                                            ' Tolerance_type: '||L_calc_tol_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      else
         O_status := 0;
         L_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_LINE_VALUE_CALC',NULL,NULL,NULL);
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         L_error_message,
                                         'Fiscal_doc_line_id: '||
                                         TO_CHAR(R_fiscal_detail.fiscal_doc_line_id)||
                                         ' No Tolerances') = FALSE then
            return FALSE;
         end if;
      end if;
      ---
   END LOOP;
   ---
   close C_FM_FISCAL_DOC_DETAIL;
   ---
   if (GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;
   ---
   if L_triang_ind = 'Y' then
   ---
      if (GET_OTHER_NF(O_error_message, L_other_fiscal_doc_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      if L_other_fiscal_doc_id is NOT NULL then
          if not O_other_proc then
          ---
             O_other_proc := TRUE;
             if (CHECK_VALUE_INFORMED_VS_CALC(O_error_message, O_status, O_other_proc, L_other_fiscal_doc_id) = FALSE) then
                return FALSE;
             end if;
          ---
          end if;
      end if;
      ---
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_VALUE_INFORMED_VS_CALC;
--------------------------------------------------------------------------------
FUNCTION CALC_SECOND_LEG_TAXES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_status        IN OUT INTEGER,
                               O_other_proc    IN OUT BOOLEAN,
                               I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program                          VARCHAR2(50) := 'FM_FISCAL_VALIDATION_SQL.CALC_SECOND_LEG_TAXES';
   L_table                            VARCHAR2(30);
   L_key                              VARCHAR2(100);
   L_error_message                    VARCHAR2(255) := NULL;
   L_head_tax_code                    FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE;
   L_sys_tax_po_item                  FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE;
   L_sys_base_po_item                 FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE;
   L_sys_modified_base_po_item        FM_FISCAL_DOC_TAX_DETAIL_EXT.MODIFIED_TAX_BASIS%TYPE;
   L_total_tax_value                  FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE;
   L_total_base_value                 FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE;
   L_total_modified_base_value        FM_FISCAL_DOC_TAX_DETAIL_EXT.MODIFIED_TAX_BASIS%TYPE;

   L_head_tax_value                   FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
   L_head_base_value                  FM_FISCAL_DOC_TAX_HEAD.TAX_BASIS%TYPE;
   L_appr_tax_po_item                 FM_FISCAL_DOC_TAX_DETAIL.APPR_TAX_VALUE%TYPE;
   L_appr_base_po_item                FM_FISCAL_DOC_TAX_DETAIL.APPR_BASE_VALUE%TYPE;
   L_appr_modified_base_po_item       FM_FISCAL_DOC_TAX_DETAIL.APPR_MODIFIED_BASE_VALUE%TYPE;
   L_appr_rate_po_item                FM_FISCAL_DOC_TAX_DETAIL.APPR_TAX_RATE%TYPE;
   L_unit_tax_amt                     FM_FISCAL_DOC_TAX_DETAIL.UNIT_TAX_AMT%TYPE;
   L_dummy                            NUMBER;
   L_total_doc_value                  FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   C_ipi_code                         FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE :='IPI';
   C_icmsst_code                      FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE  :='ICMSST';

   L_create_datetime                  FM_FISCAL_DOC_HEADER.CREATE_DATETIME%TYPE := SYSDATE;
   L_create_id                        FM_FISCAL_DOC_HEADER.CREATE_ID%TYPE := USER;
   L_total_serv_calc_cost             FM_FISCAL_DOC_HEADER.TOTAL_SERV_CALC_VALUE%TYPE :=0;
   L_total_item_calc_cost             FM_FISCAL_DOC_HEADER.TOTAL_ITEM_CALC_VALUE%TYPE :=0;
   L_total_doc_extra_costs            FM_FISCAL_DOC_HEADER.EXTRA_COSTS_CALC%TYPE :=0;
   L_unit_cost_with_disc              FM_FISCAL_DOC_DETAIL.UNIT_COST_WITH_DISC%TYPE :=0;
   L_total_calc_cost                  FM_FISCAL_DOC_DETAIL.TOTAL_CALC_COST%TYPE :=0;
   L_total_ipi_cost_calc              FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE :=0;
   L_total_icmsst_cost_calc           FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE :=0;
   L_total_doc_cost                   FM_FISCAL_DOC_HEADER.TOTAL_DOC_CALC_VALUE%TYPE :=0;
   L_total_cost                       FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE :=0;
   L_total_expenses                   FM_FISCAL_DOC_HEADER.FREIGHT_COST%TYPE := 0;
   L_total_cost_calc_disc             FM_FISCAL_DOC_HEADER.TOTAL_DOC_VALUE_WITH_DISC%TYPE;
   L_cursor                           VARCHAR2(100);
   L_exit                             VARCHAR2(4) := 'EXIT';
   L_approved                         VARCHAR2(1) := 'A';
   L_completed                        VARCHAR2(1) := 'C';
   L_no                               VARCHAR2(1) := 'N';
   L_yes                              VARCHAR2(1) := 'Y';


   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);

   ---
   L_fiscal_doc_id          FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   ---
   cursor C_FM_HEAD_TAX_CODE is
      select fdth.vat_code, fdth.total_value,
        nvl(fdth.modified_tax_basis,fdth.tax_basis)
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_FM_HEAD_EXT_TAX_CODE (P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                  P_tax_code      FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE) is
      select 1
        from fm_fiscal_doc_tax_head_ext
       where fiscal_doc_id = P_fiscal_doc_id
         and tax_code      = P_tax_code;
   ---
   cursor C_GET_TOTAL_DOC_VALUE is
      select sum(unit_cost_with_disc * quantity) total_value
        from fm_fiscal_doc_detail
       where fiscal_doc_id = I_fiscal_doc_id;

   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id, fdd.item, fdd.requisition_no, fdd.quantity
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id;

   L_fiscal_doc_detail  C_FM_FISCAL_DOC_DETAIL%ROWTYPE;

   ---
   cursor C_SYS_TAX_TOTAL (P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                           P_tax_code      FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE) is
      select decode(SUM(NVL(tax_value,0)),0,1,SUM(NVL(tax_value,0))), decode(SUM(NVL(tax_basis,0)),0,1,SUM(NVL(tax_basis,0))),
        decode(SUM(NVL(modified_tax_basis,0)),0,1,SUM(NVL(modified_tax_basis,0)))
        from fm_fiscal_doc_tax_detail_ext
       where fiscal_doc_id = P_fiscal_doc_id and
             tax_code = P_tax_code;
   ---
   cursor C_SYS_TAX_PO_ITEM (P_fiscal_doc_line_id    FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                             P_tax_code              FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE) is
      select NVL(tax_value,0), NVL(tax_basis,0),NVL(modified_tax_basis,0)
        from fm_fiscal_doc_tax_detail_ext
       where fiscal_doc_line_id = P_fiscal_doc_line_id and
             tax_code = P_tax_code;
   ---
   cursor C_GET_LEG_1_FISCAL_ID is
      select fdh.fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_schedule fs
       where fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.schedule_no = fs.schedule_no
         and fs.mode_type = L_exit
         and fs.status in (L_approved,L_completed)
         and fdd.requisition_no in (select fdd1.requisition_no
                                      from fm_fiscal_doc_header fdh1, fm_schedule fs1, fm_fiscal_doc_detail fdd1
                                     where fdh1.fiscal_doc_id = I_fiscal_doc_id
                                       and fdh1.schedule_no = fs1.schedule_no
                                       and fdh1.fiscal_doc_id = fdd1.fiscal_doc_id)
      UNION
      select fdh.fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_schedule fs
       where fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.schedule_no = fs.schedule_no
         and fs.mode_type = L_exit
         and fs.status in (L_approved,L_completed)
         and fdd.requisition_no in (select tsf.tsf_parent_no
                                      from fm_fiscal_doc_header fdh1, fm_schedule fs1, fm_fiscal_doc_detail fdd1, tsfhead tsf
                                     where fdh1.fiscal_doc_id = I_fiscal_doc_id
                                       and fdh1.schedule_no = fs1.schedule_no
                                       and fdh1.fiscal_doc_id = fdd1.fiscal_doc_id
                                       and tsf.tsf_no = fdd1.requisition_no);
   ---
   cursor C_GET_LEG_1_FISCAL_DETAIL (P_fiscal_doc_line_id    FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                                     P_item                  FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                                     P_requisition_no        FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select fdd.item, fdd.fiscal_doc_line_id
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_schedule fs
       where fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.schedule_no = fs.schedule_no
         and fs.mode_type = L_exit
         and fs.status in (L_approved,L_completed)
         and fdd.requisition_no in (select fdd1.requisition_no
                                      from fm_fiscal_doc_header fdh1, fm_schedule fs1, fm_fiscal_doc_detail fdd1
                                     where fdd1.fiscal_doc_line_id = P_fiscal_doc_line_id
                                       and fdh1.schedule_no = fs1.schedule_no
                                       and fdh1.fiscal_doc_id = fdd1.fiscal_doc_id
                                       and fdd1.item = P_item
                                       and fdd1.requisition_no = P_requisition_no)
      UNION
      select fdd.item, fdd.fiscal_doc_line_id
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, fm_schedule fs
       where fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.schedule_no = fs.schedule_no
         and fs.mode_type = L_exit
         and fs.status in (L_approved,L_completed)
         and fdd.requisition_no in (select tsf.tsf_parent_no
                                      from fm_fiscal_doc_header fdh1, fm_schedule fs1, fm_fiscal_doc_detail fdd1, tsfhead tsf
                                     where fdd1.fiscal_doc_line_id = P_fiscal_doc_line_id
                                       and fdh1.schedule_no = fs1.schedule_no
                                       and fdh1.fiscal_doc_id = fdd1.fiscal_doc_id
                                       and fdd1.item = P_item
                                       and tsf.tsf_no = fdd1.requisition_no
                                       and fdd1.requisition_no = P_requisition_no);

   L_leg_1_fiscal_detail  C_GET_LEG_1_FISCAL_DETAIL%ROWTYPE;
   ---
   cursor C_TOTAL_SERV_CALC_VALUE is
      select NVL(DECODE(SUM(fdd.total_calc_cost), 0, 0,
                                        (SUM(DECODE(via.service_ind, L_yes, fdd.total_calc_cost, 0)))),0)
        from fm_fiscal_doc_detail fdd,
             v_br_item_fiscal_attrib via
       where via.item          = fdd.item
         and fdd.fiscal_doc_id = I_fiscal_doc_id
         and fdd.pack_ind      = L_no;

   cursor C_TOTAL_ITEM_CALC_VALUE is
      select DECODE(SUM(merch_total.item_total),0,0,(SUM(merch_total.item_total)))
        from
             (select NVL(DECODE(SUM(fdd.total_calc_cost), 0, 0, (SUM(DECODE(via.service_ind, L_no, fdd.total_calc_cost, 0)))),0) item_total
                from fm_fiscal_doc_detail fdd,
                     v_br_item_fiscal_attrib via
               where via.item          = fdd.item
                 and fdd.fiscal_doc_id = I_fiscal_doc_id
                 and fdd.pack_ind      = L_no
              union all
              select NVL(DECODE(SUM(fdd.total_calc_cost), 0, 0, ( SUM(fdd.total_calc_cost))),0) item_total
                from fm_fiscal_doc_detail fdd
               where fdd.fiscal_doc_id = I_fiscal_doc_id
                 and fdd.pack_ind      = L_yes
             ) merch_total;

   cursor C_TOTAL_IPI_COST_CALC is
      select NVL(fdth.total_value,0)
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id
         and fdth.vat_code      = C_ipi_code;

     cursor C_TOTAL_ICMSST_COST_CALC is
      select NVL(fdth.total_value,0)
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id and
             fdth.vat_code      = C_icmsst_code;

   cursor C_GET_DOC_DETAIL is
      select fdd.fiscal_doc_line_id,
             fdd.unit_cost,
             fdd.quantity,
             fdd.fiscal_doc_line_id_ref
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id=I_fiscal_doc_id;

   R_get_doc_detail C_GET_DOC_DETAIL%ROWTYPE;

   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;

   cursor C_LOCK_DOC_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = P_fiscal_doc_line_id
         for update nowait;


   cursor C_GET_COST_COMPONENTS is
      select sum(fdd.freight_cost * fdd.quantity ) freight_cost
            ,sum(fdd.insurance_cost * fdd.quantity) insurance_cost
            ,sum(fdd.other_expenses_cost * fdd.quantity) other_expenses_cost
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.fiscal_doc_id = I_fiscal_doc_id
    group by fdd.fiscal_doc_id;

   R_get_cost_components C_GET_COST_COMPONENTS%ROWTYPE;
   ---
   cursor C_FM_TAX_DETAIL is
      SELECT 1
        FROM fm_fiscal_doc_detail ffdd
       WHERE ffdd.fiscal_doc_id = I_fiscal_doc_id
         AND EXISTS (SELECT 1
                       FROM fm_fiscal_doc_tax_detail ffdt
                      WHERE ffdd.fiscal_doc_line_id = ffdt.fiscal_doc_line_id);

   L_appr                 BOOLEAN := TRUE;
   L_number               NUMBER;
   ---
BEGIN
   ---
   L_key := 'I_FISCAL_DOC_ID';
   open C_FM_TAX_DETAIL;
   ---
   fetch C_FM_TAX_DETAIL into L_number;
   ---
   if C_FM_TAX_DETAIL%FOUND then
      L_appr := FALSE;
   end if;
   ---
   close C_FM_TAX_DETAIL;
   ---
   if L_appr then
      open C_FM_HEAD_TAX_CODE;
   ---
      LOOP
      ---
         fetch C_FM_HEAD_TAX_CODE into L_head_tax_code, L_head_tax_value, L_head_base_value;
         EXIT when C_FM_HEAD_TAX_CODE%NOTFOUND;
      ---
         open C_GET_LEG_1_FISCAL_ID;

         fetch C_GET_LEG_1_FISCAL_ID into L_fiscal_doc_id;

         close C_GET_LEG_1_FISCAL_ID;

      ---
         open C_FM_HEAD_EXT_TAX_CODE (L_fiscal_doc_id,L_head_tax_code);
      ---
         fetch C_FM_HEAD_EXT_TAX_CODE into L_dummy;
      ---
         if C_FM_HEAD_EXT_TAX_CODE%NOTFOUND then
         ---
            open C_GET_TOTAL_DOC_VALUE;
         ---
            fetch C_GET_TOTAL_DOC_VALUE into L_total_doc_value;
         ---
            close C_GET_TOTAL_DOC_VALUE;
         ---
            insert into fm_fiscal_doc_tax_detail (vat_code,
                                                  fiscal_doc_line_id,
                                                  percentage_rate,
                                                  total_value,
                                                  appr_base_value,
                                                  appr_modified_base_value,
                                                  appr_tax_value,
                                                  appr_tax_rate,
                                                  create_datetime,
                                                  create_id,
                                                  last_update_datetime,
                                                  last_update_id,
                                                  tax_basis,
                                                  modified_tax_basis,
                                                  unit_tax_amt)
                                           select fdth.vat_code,
                                                  fdd.fiscal_doc_line_id,
                                                  0,
                                                  0,
                                                  ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.tax_basis,
                                                  ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.modified_tax_basis,
                                                  ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.total_value,
                                                  fdth.total_value / nvl(fdth.modified_tax_basis,fdth.tax_basis) * 100,
                                                  sysdate,
                                                  user,
                                                  sysdate,
                                                  user,
                                                  null,
                                                  null,
                                                  ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.total_value / fdd.quantity
                                             from fm_fiscal_doc_tax_head fdth,
                                                  fm_fiscal_doc_detail fdd
                                            where fdth.fiscal_doc_id = fdd.fiscal_doc_id
                                              and fdth.fiscal_doc_id = I_fiscal_doc_id
                                              and fdth.vat_code      = L_head_tax_code;
         ---
         else
         ---
            L_total_tax_value := 0;
            L_total_base_value := 0;
         ---
            open C_SYS_TAX_TOTAL(L_fiscal_doc_id, L_head_tax_code);
         ---
            fetch C_SYS_TAX_TOTAL into L_total_tax_value, L_total_base_value,L_total_modified_base_value;
         ---
            close C_SYS_TAX_TOTAL;
         ---
            open C_FM_FISCAL_DOC_DETAIL;
         ---
            LOOP
               fetch C_FM_FISCAL_DOC_DETAIL into L_fiscal_doc_detail;
               EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;
            ---
               open C_GET_LEG_1_FISCAL_DETAIL(L_fiscal_doc_detail.fiscal_doc_line_id, L_fiscal_doc_detail.item, L_fiscal_doc_detail.requisition_no);

               fetch C_GET_LEG_1_FISCAL_DETAIL into L_leg_1_fiscal_detail;

               close C_GET_LEG_1_FISCAL_DETAIL;
            ---
               open C_SYS_TAX_PO_ITEM(L_leg_1_fiscal_detail.fiscal_doc_line_id, L_head_tax_code);
            ---
               fetch C_SYS_TAX_PO_ITEM into L_sys_tax_po_item, L_sys_base_po_item, L_sys_modified_base_po_item ;
            ---
               if C_SYS_TAX_PO_ITEM%NOTFOUND then
               ---
                  L_appr_tax_po_item := 0;
                  L_appr_base_po_item := 0;
                  L_appr_modified_base_po_item := 0;
                  L_appr_rate_po_item := 0;
                  L_unit_tax_amt := 0;
               ---
               elsif C_SYS_TAX_PO_ITEM%FOUND then
                  if L_total_tax_value = 0 then
                     L_appr_tax_po_item := 0;
                  else
                     L_appr_tax_po_item := (L_sys_tax_po_item / L_total_tax_value) * L_head_tax_value;
                  end if;
               ---
                  if L_total_base_value = 1 and L_total_modified_base_value = 1 then
                     L_appr_base_po_item := 0;
                     L_appr_modified_base_po_item := 0;
                  else
                     L_appr_base_po_item := (L_sys_base_po_item / L_total_base_value) * L_head_base_value;
                     L_appr_modified_base_po_item := (L_sys_modified_base_po_item / L_total_modified_base_value) * L_head_base_value;
                  end if;
               ---
                  if L_appr_base_po_item = 0 and L_appr_modified_base_po_item = 0 then
                     L_appr_rate_po_item := 0;
                  else
                     L_appr_rate_po_item := L_appr_tax_po_item / nvl(L_appr_modified_base_po_item,L_appr_base_po_item)* 100;
                  end if;
               ---
                  L_unit_tax_amt := L_appr_tax_po_item / L_fiscal_doc_detail.quantity;
               ---
               end if;
            ---
               insert into fm_fiscal_doc_tax_detail(vat_code,
                                                    fiscal_doc_line_id,
                                                    appr_tax_value,
                                                    appr_base_value,
                                                    appr_modified_base_value,
                                                    appr_tax_rate,
                                                    unit_tax_amt,
                                                    percentage_rate,
                                                    total_value,
                                                    create_datetime,
                                                    create_id,
                                                    last_update_datetime,
                                                    last_update_id)
                                            values (L_head_tax_code,
                                                    L_fiscal_doc_detail.fiscal_doc_line_id,
                                                    L_appr_tax_po_item,
                                                    L_appr_base_po_item,
                                                    L_appr_modified_base_po_item,
                                                    L_appr_rate_po_item,
                                                    L_unit_tax_amt,
                                                    0,
                                                    0,
                                                    sysdate,
                                                    user,
                                                    sysdate,
                                                    user);

            ---
               close C_SYS_TAX_PO_ITEM;
            ---
            END LOOP;
         ---
            close C_FM_FISCAL_DOC_DETAIL;
         ---
         end if;
      ---
         close C_FM_HEAD_EXT_TAX_CODE;
      ---
      END LOOP;
   ---
      close C_FM_HEAD_TAX_CODE;
   ---
      open C_GET_DOC_DETAIL;
         loop
            fetch C_GET_DOC_DETAIL into R_get_doc_detail;
            exit when C_GET_DOC_DETAIL%NOTFOUND;
               L_total_calc_cost := (R_get_doc_detail.unit_cost)*(R_get_doc_detail.quantity);
               L_total_cost      := L_total_calc_cost;

               L_cursor := 'C_LOCK_DOC_DETAIL';
               L_table  := 'FM_FISCAL_DOC_DETAIL';
               L_key    := 'Fiscal_doc_line_id: ' || TO_CHAR(R_get_doc_detail.fiscal_doc_line_id);
   ---
               open C_LOCK_DOC_DETAIL(R_get_doc_detail.fiscal_doc_line_id);
               close C_LOCK_DOC_DETAIL;
   ---
               update fm_fiscal_doc_detail
                  set total_calc_cost     = L_total_calc_cost
                     ,total_cost          = L_total_cost
               where fiscal_doc_line_id   = R_get_doc_detail.fiscal_doc_line_id;
         end loop;
      close C_GET_DOC_DETAIL;
  ---
      open C_TOTAL_SERV_CALC_VALUE;
      fetch C_TOTAL_SERV_CALC_VALUE into L_total_serv_calc_cost;
      close C_TOTAL_SERV_CALC_VALUE;
     ---
      open C_TOTAL_ITEM_CALC_VALUE;
      fetch C_TOTAL_ITEM_CALC_VALUE into L_total_item_calc_cost;
      close C_TOTAL_ITEM_CALC_VALUE;
   ---
      open C_TOTAL_IPI_COST_CALC;
      fetch C_TOTAL_IPI_COST_CALC into L_total_ipi_cost_calc;
      close C_TOTAL_IPI_COST_CALC;
   ---
      open C_TOTAL_ICMSST_COST_CALC;
      fetch C_TOTAL_ICMSST_COST_CALC into L_total_icmsst_cost_calc;
      close C_TOTAL_ICMSST_COST_CALC;
   ---
      open C_GET_COST_COMPONENTS;
      fetch C_GET_COST_COMPONENTS into R_get_cost_components;
      close C_GET_COST_COMPONENTS;
     ---
      L_total_expenses        := NVL(R_get_cost_components.freight_cost,0) + NVL(R_get_cost_components.insurance_cost,0) + NVL(R_get_cost_components.other_expenses_cost,0);
      L_total_doc_extra_costs := L_total_expenses + NVL(L_total_ipi_cost_calc,0) + NVL(L_total_icmsst_cost_calc,0);
      L_total_doc_cost        := NVL(L_total_serv_calc_cost,0) + NVL(L_total_item_calc_cost,0) + NVL(L_total_doc_extra_costs,0);
      L_total_cost_calc_disc  := NVL(L_total_item_calc_cost,0) + NVL(L_total_serv_calc_cost,0);

      L_cursor := 'C_LOCK_DOC_HEADER';
      L_table  := 'FM_FISCAL_DOC_HEADER';
      L_key    := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   ---
      open C_LOCK_DOC_HEADER;
      close C_LOCK_DOC_HEADER;
   ---
   ---
      update fm_fiscal_doc_header
         set freight_cost               = NVL(R_get_cost_components.freight_cost,0)
            ,insurance_cost             = NVL(R_get_cost_components.insurance_cost,0)
            ,other_expenses_cost        = NVL(R_get_cost_components.other_expenses_cost,0)
            ,total_serv_calc_value      = L_total_serv_calc_cost
            ,total_item_calc_value      = L_total_item_calc_cost
            ,extra_costs_calc           = L_total_doc_extra_costs
            ,total_doc_calc_value       = L_total_doc_cost
            ,total_doc_value_with_disc  = L_total_cost_calc_disc
       where fiscal_doc_id              = I_fiscal_doc_id;
   ---
   end if;
   return TRUE;
   ---
EXCEPTION

  when RECORD_LOCKED then
     O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                           'C_LOCK_DOC_HEADER',
                                            L_key,
                                            TO_CHAR(SQLCODE));
      --
      if C_LOCK_DOC_HEADER%ISOPEN then
         close C_LOCK_DOC_HEADER;
      end if;
     O_status := 0;
   return FALSE;


   when OTHERS then
      if C_FM_TAX_DETAIL%ISOPEN then
         close C_FM_TAX_DETAIL;
      end if;
      ---
      if C_FM_HEAD_EXT_TAX_CODE%ISOPEN then
         close C_FM_HEAD_EXT_TAX_CODE;
      end if;
      ---
      if C_GET_TOTAL_DOC_VALUE%ISOPEN then
         close C_GET_TOTAL_DOC_VALUE;
      end if;
      ---
      if C_FM_HEAD_TAX_CODE%ISOPEN then
         close C_FM_HEAD_TAX_CODE;
      end if;
      ---
      if C_SYS_TAX_TOTAL%ISOPEN then
         close C_SYS_TAX_TOTAL;
      end if;
      ---
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      if C_SYS_TAX_PO_ITEM%ISOPEN then
         close C_SYS_TAX_PO_ITEM;
      end if;
      ---
      if C_GET_LEG_1_FISCAL_ID%ISOPEN then
         close C_GET_LEG_1_FISCAL_ID;
      end if;
      ---
      if C_GET_LEG_1_FISCAL_DETAIL%ISOPEN then
            close C_GET_LEG_1_FISCAL_DETAIL;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := 0;
      return FALSE;
END CALC_SECOND_LEG_TAXES;
--------------------------------------------------------------------------------
FUNCTION COMPL_DATA_VALIDATION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_status        IN OUT INTEGER,
                               I_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program          VARCHAR2(100) := 'FM_FISCAL_VALIDATION_SQL.COMPL_DATA_VALIDATION';
   L_table            VARCHAR2(30);
   L_key              VARCHAR2(100);
   L_nic_ind          VARCHAR2(1) := 'N';
   L_error_message    RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_dif_perc         NUMBER;
   L_description      FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_number_value     FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value     FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value       FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_calc_tol_type    FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_calc_tol_val     FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;

   ---
   L_total_value      FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
   ---
   cursor C_GET_FM_FISCAL_HEADER is
     SELECT (NVL(freight_cost,0) + NVL(insurance_cost, 0) + NVL(other_expenses_cost, 0)) non_merch_cost,
            NVL(total_doc_value,0) total_doc_value
       FROM fm_fiscal_doc_header fdh
      WHERE fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_TAX_HEAD is
      select sum(NVL(fdth.total_value,0)) total_value
        from fm_fiscal_doc_tax_head fdth
             ,vat_codes vc
       where fdth.fiscal_doc_id = I_fiscal_doc_id
         and fdth.vat_code = vc.vat_code
         and vc.incl_nic_ind = L_nic_ind;
   ---
   L_fiscal_doc_header   C_GET_FM_FISCAL_HEADER%ROWTYPE;
   ---
BEGIN
   O_status := 1;
   --- Cleans up old error messages
   if FM_ERROR_LOG_SQL.CLEAR_ERROR(O_error_message,
                                   I_fiscal_doc_id,
                                   L_program) = FALSE then
      return FALSE;
   end if;
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_description,
                                         L_number_value,
                                         L_string_value,
                                         L_date_value,
                                         'CHAR',
                                         'CALC_TOL_TYPE') = FALSE then
      return FALSE;
   end if;
   ---
   L_calc_tol_type := L_string_value;
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_description,
                                         L_number_value,
                                         L_string_value,
                                         L_date_value,
                                         'NUMBER',
                                         'CALC_TOL_VALUE') = FALSE then
      return FALSE;
   end if;
   ---
   L_calc_tol_val := L_number_value;
   ---
   L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
   L_table := 'FM_FISCAL_DOC_COMPLEMENT';
   ---
   open C_GET_FM_FISCAL_HEADER;
   ---
   fetch C_GET_FM_FISCAL_HEADER into L_fiscal_doc_header;
   ---
   close C_GET_FM_FISCAL_HEADER;
   ---
   open C_GET_TAX_HEAD;
   ---
   fetch C_GET_TAX_HEAD into L_total_value;
    ---
   close C_GET_TAX_HEAD;
   ---
   if NVL(L_fiscal_doc_header.non_merch_cost,0) <= 0 then
      O_status := 0;
      L_error_message := SQL_LIB.CREATE_MSG('ORFM_NON_MER_GREAT_ZERO',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      L_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id)) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if NVL(L_fiscal_doc_header.total_doc_value,0) <= 0 then
      O_status := 0;
      L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOTAL_DOC_GREAT_ZERO',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
      if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'VALIDATION_ERROR',
                                      L_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id)) = FALSE then
         return FALSE;
      end if;
   end if;
   ---


   if ((nvl(L_fiscal_doc_header.non_merch_cost,0) + nvl(L_total_value,0)) <> nvl(L_fiscal_doc_header.total_doc_value,0)) then
      ---
      if L_calc_tol_type = C_DISC_PERC then
         ---
         if ((NVL(L_fiscal_doc_header.non_merch_cost,0) + NVL(L_total_value,0)) = 0 and
             NVL(L_fiscal_doc_header.total_doc_value,0) <> 0) or
            ((NVL(L_fiscal_doc_header.non_merch_cost,0) + NVL(L_total_value,0)) <> 0 and
             NVL(L_fiscal_doc_header.total_doc_value,0) = 0) then
            ---
            L_dif_perc := 100;
            ---
         else
            ---
            L_dif_perc := ABS((((NVL(L_fiscal_doc_header.non_merch_cost,0) + NVL(L_total_value,0)) * 100) / NVL(L_fiscal_doc_header.total_doc_value,1)) - 100);
            ---
         end if;
         ---
         if L_dif_perc > NVL(L_calc_tol_val,0) then
            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_COMPL_TOLERANCE_CALC',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'Fiscal_doc_id: '||
                                            TO_CHAR(I_fiscal_doc_id)||
                                            ' Tolerance_type: '||L_calc_tol_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      elsif L_calc_tol_type = C_DISC_VALUE then
         ---
         if ABS((nvl(L_fiscal_doc_header.non_merch_cost,0) + nvl(L_total_value,0)) - nvl(L_fiscal_doc_header.total_doc_value,0)) > NVL(L_calc_tol_val,0) then
            O_status := 0;
            L_error_message := SQL_LIB.CREATE_MSG('ORFM_COMPL_TOLERANCE_CALC',L_calc_tol_type,NVL(L_calc_tol_val,0),NULL);
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            I_fiscal_doc_id,
                                            L_program,
                                            L_program,
                                            'E',
                                            'VALIDATION_ERROR',
                                            L_error_message,
                                            'Fiscal_doc_id: '||
                                            TO_CHAR(I_fiscal_doc_id)||
                                            ' Tolerance_type: '||L_calc_tol_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      else
         ---
         O_status := 0;
         L_error_message := SQL_LIB.CREATE_MSG('ORFM_TOTAL_DOC_COST_COMPO',NULL,NULL,NULL);
         ---
         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                         'E',
                                         'VALIDATION_ERROR',
                                         L_error_message,
                                         'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id)||
                                         ' No Tolerances') = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   end if;
   ---

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      ---
      if C_GET_FM_FISCAL_HEADER%ISOPEN then
         close C_GET_FM_FISCAL_HEADER;
      end if;
      ---
      if C_GET_TAX_HEAD%ISOPEN then
         close C_GET_TAX_HEAD;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END COMPL_DATA_VALIDATION;
-----------------------------------------------------------------------------------
FUNCTION PROCESS_EDI_VALIDATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_edi_doc_id       IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program                 VARCHAR2(100) := 'FM_FISCAL_VALIDATION_SQL.PROCESS_EDI_VALIDATION';
   L_other_proc              BOOLEAN := FALSE;
   L_fiscal_doc_id           FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_req_type                FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_utilization_id          FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE;
   L_loc                     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE;
   L_loc_type                FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
   L_status                  FM_FISCAL_DOC_HEADER.STATUS%TYPE  := NULL;
   L_status_process          NUMBER := 2;
   L_comp_freght_nf_ind      FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE;
   L_match_opr_type          V_FISCAL_ATTRIBUTES.MATCH_OPR_TYPE%TYPE;
   L_comp_nf_ind             VARCHAR2(1) := 'N';
   L_triang_ind              VARCHAR2(1) := 'N';
   L_main_fiscal_doc_id      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_main_cost_disp          FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE;
   L_main_qty_disp           FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE;
   L_main_tax_disp           FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE;
   L_comp_cost_disp          FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE;
   L_comp_qty_disp           FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE;
   L_comp_tax_disp           FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE;
   L_comp_fiscal_doc_id      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_cost_disp               FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE;
   L_qty_disp                FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE;
   L_tax_disp                FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE;
   ---
   cursor C_FM_EDI_HEADER is
     SELECT fedh.fiscal_doc_id,
            fedh.requisition_type,
            fedh.utilization_id,
            fedh.location_id,
            fedh.location_type,
            fua.comp_freight_nf_ind
       FROM fm_edi_doc_header fedh,
            fm_utilization_attributes fua
      WHERE fedh.utilization_id = fua.utilization_id
        AND fedh.edi_doc_id     = I_edi_doc_id;
   ---

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_EDI_HEADER','FM_EDI_DOC_HEADER','EDI doc id: '||I_edi_doc_id);
   open C_FM_EDI_HEADER;

   SQL_LIB.SET_MARK('FETCH','C_FM_EDI_HEADER','FM_EDI_DOC_HEADER','EDI doc id: '||I_edi_doc_id);
   fetch C_FM_EDI_HEADER into L_fiscal_doc_id,
                              L_req_type,
                              L_utilization_id,
                              L_loc,
                              L_loc_type,
                              L_comp_freght_nf_ind;

   SQL_LIB.SET_MARK('CLOSE','C_FM_EDI_HEADER','FM_EDI_DOC_HEADER','EDI doc id: '||I_edi_doc_id);
   close C_FM_EDI_HEADER;
   ---

   if L_comp_freght_nf_ind <> 'Y' then
      if L_req_type NOT IN ('TSF', 'REP') then
         if PROCESS_VALIDATION(O_error_message,
                               L_status_process,
                               L_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;
      end if;

      if L_status_process = 1 then
         -- check if location is centralize or decentralize
         if L_req_type NOT IN ('TSF', 'REP', 'IC') then
            if FM_DIS_RESOLUTION_SQL.GET_FM_LOC_OPR(O_error_message,
                                                    L_loc,
                                                    L_loc_type,
                                                    L_match_opr_type)= FALSE then
               return FALSE;
  	    end if;

            -- check for tringulation
            if GET_TRIANG_IND(O_error_message,
                              L_comp_nf_ind,
                              L_triang_ind,
                              L_fiscal_doc_id)= FALSE then
               return FALSE;
            end if;
            if L_triang_ind = 'N' then
               -- check if discrepant
               if FM_FISCAL_DOC_HEADER_SQL.CHECK_DISCREPANCY(O_error_message,
                                                             L_fiscal_doc_id,
                                                             L_cost_disp,
                                                             L_qty_disp,
                                                             L_tax_disp) = FALSE then

                  return FALSE;
               end if;

               if L_match_opr_type = 'D' then -- de-centralize location
                  if L_cost_disp = 'Y' or L_qty_disp= 'Y' or L_tax_disp= 'Y' then
                     L_status := 'D';
                  else
                     L_status := 'V';
                  end if;
               else -- centralize location
                  if L_qty_disp= 'Y'  then
                     L_status := 'D';
                  else
                     L_status := 'V';
                  end if;
               end if;
            else
               -- fetch the main and comp NF id
               if L_comp_nf_ind = 'Y' then
                  if GET_OTHER_NF(O_error_message,
                                  L_main_fiscal_doc_id,
                                  L_comp_nf_ind,
                                  L_fiscal_doc_id)= FALSE then
                     return FALSE;
                  end if;
                  L_comp_fiscal_doc_id := L_fiscal_doc_id;
               else
                  if GET_OTHER_NF(O_error_message,
                                  L_comp_fiscal_doc_id,
                                  L_comp_nf_ind,
                                  L_fiscal_doc_id)= FALSE then
                     return FALSE;
                  end if;
                  L_main_fiscal_doc_id := L_fiscal_doc_id;
               end if;

               -- check discrepancy for main  NF
               if FM_FISCAL_DOC_HEADER_SQL.CHECK_DISCREPANCY(O_error_message,
                                                             L_main_fiscal_doc_id,
                                                             L_main_cost_disp,
                                                             L_main_qty_disp,
                                                             L_main_tax_disp) = FALSE then
                  return FALSE;
               end if;

               -- check discrepancy for  comp NF
               if FM_FISCAL_DOC_HEADER_SQL.CHECK_DISCREPANCY(O_error_message,
                                                             L_comp_fiscal_doc_id,
                                                             L_comp_cost_disp,
                                                             L_comp_qty_disp,
                                                             L_comp_tax_disp) = FALSE then
                  return FALSE;
               end if;

               if L_match_opr_type = 'D' then -- de-centralize location
                  -- update main NF status
                  if L_main_cost_disp = 'Y' or L_main_qty_disp = 'Y' or L_main_tax_disp = 'Y' then
                     if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                               L_main_fiscal_doc_id,
                                                               'D') = FALSE then
                        return FALSE;
                     end if ;
                     if L_fiscal_doc_id = L_main_fiscal_doc_id then
                        L_status := 'D';
                     end if;
                  else
                     if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                               L_main_fiscal_doc_id,
                                                               'V') = FALSE then
                        return FALSE;
                     end if ;
                     if L_fiscal_doc_id = L_main_fiscal_doc_id then
                        L_status := 'V';
                     end if;
                  end if;
                  -- update comp NF status
                  if L_main_cost_disp = 'Y' or L_main_qty_disp = 'Y' or L_comp_tax_disp = 'Y' then
                     if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                               L_comp_fiscal_doc_id,
                                                               'D') = FALSE then
                        return FALSE;
                     end if ;
                     if L_fiscal_doc_id = L_comp_fiscal_doc_id then
                        L_status := 'D';
                     end if;
                  else
                     if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                               L_comp_fiscal_doc_id,
                                                               'V') = FALSE then
                        return FALSE;
                     end if ;
                     if L_fiscal_doc_id = L_comp_fiscal_doc_id then
                        L_status := 'V';
                     end if;
                  end if;
               else -- centralize location

                  if  L_main_qty_disp = 'Y'  then
                     if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                               L_main_fiscal_doc_id,
                                                               'D') = FALSE then
                        return FALSE;
                     end if ;
                     if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                               L_comp_fiscal_doc_id,
                                                               'D') = FALSE then
                        return FALSE;
                     end if ;

                     L_status := 'D';
                  else
                     if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                               L_main_fiscal_doc_id,
                                                               'V') = FALSE then
                        return FALSE;
                     end if ;
                     if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                               L_comp_fiscal_doc_id,
                                                               'V') = FALSE then
                        return FALSE;
                     end if ;
                     L_status := 'V';
                  end if;
               end if;
            end if;
         end if;
      elsif L_status_process = 0 then 	 --L_status_process = 0

         -- check for tringulation
         if GET_TRIANG_IND(O_error_message,
                           L_comp_nf_ind,
                           L_triang_ind,
                           L_fiscal_doc_id)= FALSE then
            return FALSE;
         end if;
         if L_triang_ind = 'Y' then
            -- fetch the main and comp NF id
            if L_comp_nf_ind = 'Y' then
               if GET_OTHER_NF(O_error_message,
                               L_main_fiscal_doc_id,
                               L_comp_nf_ind,
                               L_fiscal_doc_id)= FALSE then
                  return FALSE;
               end if;
               L_comp_fiscal_doc_id := L_fiscal_doc_id;
            else
               if GET_OTHER_NF(O_error_message,
                               L_comp_fiscal_doc_id,
                               L_comp_nf_ind,
                               L_fiscal_doc_id)= FALSE then
                  return FALSE;
               end if;
               L_main_fiscal_doc_id := L_fiscal_doc_id;
            end if;
            if (L_main_fiscal_doc_id is not NULL) then
               if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                         L_main_fiscal_doc_id,
                                                         'E') = FALSE then
                  return FALSE;
               end if ;
            end if;

            if ( L_comp_fiscal_doc_id is not NULL) then
               if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                         L_comp_fiscal_doc_id,
                                                         'E') = FALSE then
                  return FALSE;
               end if ;
            end if;
         else
            L_status := 'E';
         end if;
         L_status := 'E';
      end if;

      -- update status of fiscal doc
      if L_status is not NULL then
         if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                   L_fiscal_doc_id,
                                                   L_status) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_FM_EDI_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_FM_EDI_HEADER','FM_EDI_DOC_HEADER','EDI doc id: '||I_edi_doc_id);
         close C_FM_EDI_HEADER;
      end if;

      return FALSE;
END PROCESS_EDI_VALIDATION;
--------------------------------------------------------------------------------

END FM_FISCAL_VALIDATION_SQL;
/