CREATE OR REPLACE PACKAGE BODY RMSSUB_XITEM_L10N_BR AS
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_L10N_XITEMCTRY(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item                    IN      ITEM_COUNTRY.ITEM%TYPE,
                                 I_country_id              IN      ITEM_COUNTRY.COUNTRY_ID%TYPE,
                                 I_br_of_xitemctrydesc_tbl IN      "RIB_BrXItemCtryDesc_TBL")
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'RMSSUB_XITEM_L10N_BR.VALIDATE_L10N_XITEMCTRY';

   L_exists   VARCHAR2(1);

   cursor C_SERVICE_IND_REQ is
      select 1
        from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
       where value(ic).service_ind is NULL; 

   cursor C_ORIGIN_CODE_REQ is
      select 1
        from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
       where value(ic).origin_code is NULL; 

   cursor C_ORIGIN_CODE is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).origin_code is NOT NULL
      and NOT EXISTS (select 'x' 
                        from l10n_code_detail_descs 
                       where value(ic).origin_code = l10n_code
                         and l10n_code_type = 'ORME'
                         and rownum = 1);
   
   cursor C_PHARMA_LIST_TYPE is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).pharma_list_type is NOT NULL
      and NOT EXISTS (select 'x' 
                        from l10n_code_detail_descs 
                       where value(ic).pharma_list_type = l10n_code
                         and l10n_code_type = 'PHRM'
                         and rownum = 1);                      

   cursor C_CLASSIFICATION_ID is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).classification_id is NOT NULL 
      and NOT EXISTS (select 'x'
                        from l10n_br_ncm_codes nc
                       where value(ic).classification_id = nc.ncm_code
                         and nc.country_id = 'BR'
                         and rownum = 1);

   cursor C_NCM_CHAR_CODE is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).ncm_char_code is NOT NULL
      and NOT EXISTS (select 'x'
                        from l10n_br_ncm_char_codes ncc
                       where value(ic).ncm_char_code = ncc.ncm_char_code
                         and value(ic).classification_id = ncc.ncm_code
                         and ncc.country_id = 'BR'
                         and rownum = 1);

   cursor C_EX_IPI is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).ex_ipi is NOT NULL
      and NOT EXISTS (select 'x'
                        from l10n_br_ncm_ipi_codes nic
                       where value(ic).ex_ipi = nic.ipi_code
                         and value(ic).classification_id = nic.ncm_code
                         and nic.country_id = 'BR'
                         and rownum = 1);

   cursor C_PAUTA_CODE is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).pauta_code is NOT NULL
      and NOT EXISTS (select 'x'
                        from l10n_br_ncm_pauta_codes npc
                       where value(ic).pauta_code = npc.ncm_pauta_code
                         and value(ic).ncm_char_code = npc.ncm_char_code
                         and value(ic).classification_id = npc.ncm_code
                         and npc.country_id = 'BR'
                         and rownum = 1);

   cursor C_SERVICE_CODE is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).service_code is NOT NULL
      and NOT EXISTS (select 'x'
                        from l10n_br_mastersaf_svc_codes msc
                       where value(ic).service_code = msc.mastersaf_service_code
                         and rownum = 1);

   cursor C_FEDERAL_SERVICE is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).federal_service is NOT NULL
      and NOT EXISTS (select 'x'
                        from l10n_br_federal_svc_codes fsc
                       where value(ic).federal_service = fsc.federal_service_code
                         and rownum = 1);
                         
   cursor C_STATE_OF_MANUFACTURE is
   select 1
     from TABLE(CAST(I_br_of_xitemctrydesc_tbl AS "RIB_BrXItemCtryDesc_TBL")) ic
    where value(ic).state_of_manufacture is NOT NULL
      and NOT EXISTS (select 'x'
                        from state
                       where value(ic).state_of_manufacture = state
                         and country_id = 'BR'
                         and rownum = 1);                      
                         

BEGIN

   if I_br_of_xitemctrydesc_tbl is NULL or I_br_of_xitemctrydesc_tbl.COUNT <= 0 then
      return TRUE;
   end if;

   -- check required fields
   L_exists := NULL;
   open C_SERVICE_IND_REQ;
   fetch C_SERVICE_IND_REQ into L_exists;
   close C_SERVICE_IND_REQ;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',  
                                            'SERVICE_IND',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_exists := NULL;
   open C_ORIGIN_CODE_REQ;
   fetch C_ORIGIN_CODE_REQ into L_exists;
   close C_ORIGIN_CODE_REQ;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',  
                                            'ORIGIN_CODE',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- check lowest/highest allowed value - n/a

   -- check l10n_code_type
   -- origin_code in 'ORME'
   L_exists := NULL;
   open C_ORIGIN_CODE;
   fetch C_ORIGIN_CODE into L_exists;
   close C_ORIGIN_CODE;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_ORIGIN_CODE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- pharma list type in 'PHRM'
   L_exists := NULL;
   open C_PHARMA_LIST_TYPE;
   fetch C_PHARMA_LIST_TYPE into L_exists;
   close C_PHARMA_LIST_TYPE;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_PHARMA_LIST_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- check l10n_rec_group
   -- CLASSIFICATION_ID
   L_exists := NULL;
   open C_CLASSIFICATION_ID;
   fetch C_CLASSIFICATION_ID into L_exists;
   close C_CLASSIFICATION_ID;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_CLASS_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- NCM_CHAR_CODE
   L_exists := NULL;
   open C_NCM_CHAR_CODE;
   fetch C_NCM_CHAR_CODE into L_exists;
   close C_NCM_CHAR_CODE;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_NCM_CHAR_CODE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- EX_IPI
   L_exists := NULL;
   open C_EX_IPI;
   fetch C_EX_IPI into L_exists;
   close C_EX_IPI;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_EX_IPI',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- PAUTA_CODE
   L_exists := NULL;
   open C_PAUTA_CODE;
   fetch C_PAUTA_CODE into L_exists;
   close C_PAUTA_CODE;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_PAUTA_CODE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- SERVICE_CODE
   L_exists := NULL;
   open C_SERVICE_CODE;
   fetch C_SERVICE_CODE into L_exists;
   close C_SERVICE_CODE;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_SERVICE_CODE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- FEDERAL_SERVICE
   L_exists := NULL;
   open C_FEDERAL_SERVICE;
   fetch C_FEDERAL_SERVICE into L_exists;
   close C_FEDERAL_SERVICE;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_FEDERAL_SERVICE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- STATE_OF_MANUFACTURE
   L_exists := NULL;
   open C_STATE_OF_MANUFACTURE;
   fetch C_STATE_OF_MANUFACTURE into L_exists;
   close C_STATE_OF_MANUFACTURE;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_STATE_OF_MANU',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- invoke validation code defined in the meta-data tables:
   for i in I_br_of_xitemctrydesc_tbl.FIRST .. I_br_of_xitemctrydesc_tbl.LAST loop
      -- 1) L10N_ATTRIB.VALIDATION_CODE - n/a
      -- 2) L10N_ATTRIB_GROUP.GROUP_VALIDATION_CODE -
      if L10N_BR_ITEM_SQL.CHK_FISCAL_ATTRIB_GRP(O_error_message, 
                                                I_br_of_xitemctrydesc_tbl(i).service_ind,        --:I_SERVICE_IND,
                                                I_br_of_xitemctrydesc_tbl(i).origin_code,        --:I_ORIGIN_CODE,
                                                I_br_of_xitemctrydesc_tbl(i).classification_id,  --:I_CLASSIFICATION_ID,
                                                I_br_of_xitemctrydesc_tbl(i).ncm_char_code,      --:I_NCM_CHAR_CODE,
                                                I_br_of_xitemctrydesc_tbl(i).ex_ipi,             --:I_EX_IPI,
                                                I_br_of_xitemctrydesc_tbl(i).pauta_code,         --:I_PAUTA_CODE,
                                                I_br_of_xitemctrydesc_tbl(i).service_code,       --:I_SERVICE_CODE,
                                                I_br_of_xitemctrydesc_tbl(i).federal_service,      --:I_FEDERAL_SERVICE
                                                I_br_of_xitemctrydesc_tbl(i).state_of_manufacture, --:I_STATE_OF_MANUFACTURE
                                                I_br_of_xitemctrydesc_tbl(i).pharma_list_type) = FALSE then --:I_PHARMA_LIST_TYPE
         return FALSE;
      end if; 

      -- 3) L10N_EXT_ENTITY_VAL.CROSS_GROUP_VALIDATION_CODE - 
      if L10N_BR_ITEM_SQL.CHK_ITEM_CTRY_ENT(O_error_message, 
                                            I_br_of_xitemctrydesc_tbl(i).service_ind,          --:I_SERVICE_IND,
                                            I_br_of_xitemctrydesc_tbl(i).origin_code,          --:I_ORIGIN_CODE,
                                            I_br_of_xitemctrydesc_tbl(i).classification_id,    --:I_CLASSIFICATION_ID,
                                            I_br_of_xitemctrydesc_tbl(i).ncm_char_code,        --:I_NCM_CHAR_CODE,
                                            I_br_of_xitemctrydesc_tbl(i).ex_ipi,               --:I_EX_IPI,
                                            I_br_of_xitemctrydesc_tbl(i).pauta_code,           --:I_PAUTA_CODE,
                                            I_br_of_xitemctrydesc_tbl(i).service_code,         --:I_SERVICE_CODE,
                                            I_br_of_xitemctrydesc_tbl(i).federal_service,      --:I_FEDERAL_SERVICE
                                            I_br_of_xitemctrydesc_tbl(i).state_of_manufacture, --:I_STATE_OF_MANUFACTURE
                                            I_br_of_xitemctrydesc_tbl(i).pharma_list_type) = FALSE then --:I_PHARMA_LIST_TYPE
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END VALIDATE_L10N_XITEMCTRY; 
---------------------------------------------------------------------------------------------------
FUNCTION STAGE_L10N_XITEMCTRY(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id              IN      NUMBER,
                              I_item                    IN      ITEM_COUNTRY.ITEM%TYPE,
                              I_country_id              IN      ITEM_COUNTRY.COUNTRY_ID%TYPE,
                              I_br_of_xitemctrydesc_tbl IN      "RIB_BrXItemCtryDesc_TBL")
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'RMSSUB_XITEM_L10N_BR.STAGE_L10N_XITEMCTRY';
   
   L_name                  VARCHAR2(30);
   L_value                 VARCHAR2(250);
   L_entity_name_value_tbl ENTITY_NAME_VALUE_PAIR_TBL := ENTITY_NAME_VALUE_PAIR_TBL();
   L_entity_name_value_rec ENTITY_NAME_VALUE_PAIR_REC := ENTITY_NAME_VALUE_PAIR_REC(NULL, NULL);
   L_key_col_rec           NAME_VALUE_PAIR_REC := NAME_VALUE_PAIR_REC(NULL, NULL);
   L_key_col_tbl           NAME_VALUE_PAIR_TBL := NAME_VALUE_PAIR_TBL();
   L_attrib_col_rec        NAME_VALUE_PAIR_REC := NAME_VALUE_PAIR_REC(NULL, NULL);   
   L_attrib_col_tbl        NAME_VALUE_PAIR_TBL := NAME_VALUE_PAIR_TBL();

BEGIN
   -- write to staging table
   FORALL x in I_br_of_xitemctrydesc_tbl.FIRST..I_br_of_xitemctrydesc_tbl.LAST
      insert into stg_item_country_l10n_ext_br(process_id,                            
                                               item,       -- key                            
                                               country_id, -- key
                                               service_ind,
                                               origin_code,
                                               classification_id,
                                               ncm_char_code, 
                                               ex_ipi, 
                                               pauta_code, 
                                               service_code, 
                                               federal_service,
                                               state_of_manufacture,
                                               pharma_list_type)
                                        values (I_process_id, 
                                               I_item,
                                               I_country_id,
                                               I_br_of_xitemctrydesc_tbl(x).service_ind,
                                               I_br_of_xitemctrydesc_tbl(x).origin_code,
                                               I_br_of_xitemctrydesc_tbl(x).classification_id,
                                               I_br_of_xitemctrydesc_tbl(x).ncm_char_code, 
                                               I_br_of_xitemctrydesc_tbl(x).ex_ipi, 
                                               I_br_of_xitemctrydesc_tbl(x).pauta_code, 
                                               I_br_of_xitemctrydesc_tbl(x).service_code, 
                                               I_br_of_xitemctrydesc_tbl(x).federal_service,
                                               I_br_of_xitemctrydesc_tbl(x).state_of_manufacture,
                                               I_br_of_xitemctrydesc_tbl(x).pharma_list_type);

   -- update custom attributes of name/value pair on the staging table
  for i in I_br_of_xitemctrydesc_tbl.FIRST..I_br_of_xitemctrydesc_tbl.LAST loop
      L_key_col_tbl.DELETE;
      L_attrib_col_tbl.DELETE;
      
      L_entity_name_value_tbl.delete;

      L_key_col_rec := NAME_VALUE_PAIR_REC('ITEM', I_item);
      L_key_col_tbl.EXTEND;
      L_key_col_tbl(1) := L_key_col_rec;
      
      L_key_col_rec := NAME_VALUE_PAIR_REC('COUNTRY_ID', I_country_id);
      L_key_col_tbl.EXTEND;
      L_key_col_tbl(2) := L_key_col_rec;
      
      if I_br_of_xitemctrydesc_tbl(i).NameValPairRBO_TBL is NOT NULL and I_br_of_xitemctrydesc_tbl(i).NameValPairRBO_TBL.COUNT > 0 then
         for j in I_br_of_xitemctrydesc_tbl(i).NameValPairRBO_TBL.FIRST .. I_br_of_xitemctrydesc_tbl(i).NameValPairRBO_TBL.LAST loop
            L_name := I_br_of_xitemctrydesc_tbl(i).NameValPairRBO_TBL(j).name;
            L_value := I_br_of_xitemctrydesc_tbl(i).NameValPairRBO_TBL(j).value;
         
            L_attrib_col_rec := NAME_VALUE_PAIR_REC(L_name,L_value);
            L_attrib_col_tbl.extend;
            L_attrib_col_tbl(j) := L_attrib_col_rec;
         end loop;

         L_entity_name_value_rec.key_col_tbl := L_key_col_tbl;
         L_entity_name_value_rec.attrib_col_tbl := L_attrib_col_tbl;
         L_entity_name_value_tbl.extend;
         L_entity_name_value_tbl(1) := L_entity_name_value_rec;
      
         if L10N_FLEX_API_SQL.UPDATE_STG_NAME_VALUE_PAIR(O_error_message,
                                                         I_country_id,
                                                         'ITEM_COUNTRY',
                                                         I_process_id,
                                                         L_entity_name_value_tbl) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END STAGE_L10N_XITEMCTRY; 
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_L10N_XITEMCTRY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_message           IN      "RIB_XItemDesc_REC",
                                I_message_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'RMSSUB_XITEM_L10N_BR.PERSIST_L10N_XITEMCTRY';

   L_xitemctrydesc_tbl        "RIB_XItemCtryDesc_TBL" := NULL;
   L_loc_of_xitemctrydesc_tbl "RIB_LocOfXItemCtryDesc_TBL";
   L_br_of_xitemctrydesc_tbl  "RIB_BrXItemCtryDesc_TBL";

   L_itemctry_process_id  NUMBER;

   cursor C_SEQ is
      select ITEM_COUNTRY_L10N_EXT_SEQ.nextval
        from dual;

BEGIN
   -- ITEM_COUNTRY is localized for Brazil
   L_xitemctrydesc_tbl := I_message.XItemCtryDesc_TBL;
   if L_xitemctrydesc_tbl is NULL or L_xitemctrydesc_tbl.COUNT <= 0 then
      return TRUE;
   end if;

   -- get the next sequence for item country process id
   open C_SEQ; 
   fetch C_SEQ into L_itemctry_process_id;
   close C_SEQ;

   -- get Brazil localization data from the RIB object and write to staging table.
   for i in L_xitemctrydesc_tbl.FIRST .. L_xitemctrydesc_tbl.LAST loop
      -- get the localization hook
      L_loc_of_xitemctrydesc_tbl := L_xitemctrydesc_tbl(i).LocOfXItemCtryDesc_TBL;
      if L_loc_of_xitemctrydesc_tbl is NOT NULL and L_loc_of_xitemctrydesc_tbl.COUNT > 0 then
         for j in L_loc_of_xitemctrydesc_tbl.FIRST .. L_loc_of_xitemctrydesc_tbl.LAST loop
            -- get the Brazil hook
            L_br_of_xitemctrydesc_tbl := L_loc_of_xitemctrydesc_tbl(j).BrXItemCtryDesc_TBL;
            if L_br_of_xitemctrydesc_tbl is NOT NULL and L_br_of_xitemctrydesc_tbl.COUNT > 0 then
               -- write to staging table
               if RMSSUB_XITEM_L10N_BR.STAGE_L10N_XITEMCTRY(O_error_message,
                                                            L_itemctry_process_id, 
                                                            I_message.item,
                                                            L_xitemctrydesc_tbl(i).country_id,                       
                                                            L_br_of_xitemctrydesc_tbl) = FALSE then
                  return FALSE;
               end if;
            end if;
         end loop;
      end if;       
   end loop;

   -- move data from the staging table to the L10N extension table.
   if L10N_FLEX_API_SQL.INSERT_L10N_EXT_TBL(O_error_message, 
                                            'BR',
                                            'ITEM_COUNTRY',
                                            L_itemctry_process_id) = FALSE then
      return FALSE;
   end if;

   -- delete data in the staging table
   delete from stg_item_country_l10n_ext_br where process_id = L_itemctry_process_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END PERSIST_L10N_XITEMCTRY; 
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_L10N_XITEMCTRY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_message           IN      "RIB_XItemDesc_REC",
                                 I_message_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'RMSSUB_XITEM_L10N_BR.VALIDATE_L10N_XITEMCTRY';

   L_xitemctrydesc_tbl        "RIB_XItemCtryDesc_TBL";
   L_loc_of_xitemctrydesc_tbl "RIB_LocOfXItemCtryDesc_TBL";
   L_br_of_xitemctrydesc_tbl  "RIB_BrXItemCtryDesc_TBL";

BEGIN
   -- ITEM_COUNTRY is localized for Brazil
   L_xitemctrydesc_tbl := I_message.XItemCtryDesc_TBL;
   if L_xitemctrydesc_tbl is NULL or L_xitemctrydesc_tbl.COUNT <= 0 then
      return TRUE;
   end if;

   -- get Brazil localization data from the RIB object and perform validation.
   for i in L_xitemctrydesc_tbl.FIRST .. L_xitemctrydesc_tbl.LAST loop
      -- get the localization hook
      L_loc_of_xitemctrydesc_tbl := L_xitemctrydesc_tbl(i).LocOfXItemCtryDesc_TBL;
      if L_loc_of_xitemctrydesc_tbl is NOT NULL and L_loc_of_xitemctrydesc_tbl.COUNT > 0 then
         for j in L_loc_of_xitemctrydesc_tbl.FIRST .. L_loc_of_xitemctrydesc_tbl.LAST loop
            -- get the Brazil hook
            L_br_of_xitemctrydesc_tbl := L_loc_of_xitemctrydesc_tbl(j).BrXItemCtryDesc_TBL;
            if L_br_of_xitemctrydesc_tbl is NOT NULL and L_br_of_xitemctrydesc_tbl.COUNT > 0 then
               -- validate the records
               if RMSSUB_XITEM_L10N_BR.VALIDATE_L10N_XITEMCTRY(O_error_message, 
                                                                I_message.item,
                                                                L_xitemctrydesc_tbl(i).country_id,                       
                                                                L_br_of_xitemctrydesc_tbl) = FALSE then
                  return FALSE;
               end if;
            end if;
         end loop;
      end if;       
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END VALIDATE_L10N_XITEMCTRY; 
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_L10N_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_message           IN      "RIB_XItemDesc_REC",
                              I_message_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'RMSSUB_XITEM_L10N_BR.VALIDATE_L10N_ATTRIB';

BEGIN
   -- item country
   if RMSSUB_XITEM_L10N_BR.VALIDATE_L10N_XITEMCTRY(O_error_message, 
                                                   I_message,
                                                   I_message_type) = FALSE then
      return FALSE;
   end if;

   -- add other Brazil localized entities here

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END VALIDATE_L10N_ATTRIB; 
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_L10N_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_message           IN      "RIB_XItemDesc_REC",
                             I_message_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'RMSSUB_XITEM_L10N_BR.PERSIST_L10N_ATTRIB';

BEGIN 
   -- item country
   if RMSSUB_XITEM_L10N_BR.PERSIST_L10N_XITEMCTRY(O_error_message, 
                                                  I_message,
                                                  I_message_type) = FALSE then
      return FALSE;
   end if;

   -- add other Brazil localized entities here

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END PERSIST_L10N_ATTRIB; 
---------------------------------------------------------------------------------------------------
END RMSSUB_XITEM_L10N_BR;
/