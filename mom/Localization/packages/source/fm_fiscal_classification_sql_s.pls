CREATE OR REPLACE PACKAGE FM_FISCAL_CLASSIFICATION_SQL is
  ---------------------------------------------
  -- Author  : ABC
  -- Created : 17/04/2007 15:37:32
  -- Purpose : Validate fields of fm_city table
  ---------------------------------------------

---------------------------------------------------------------------------------
-- Function Name: GET_DESC
-- Purpose:       This function gets the classification description.
----------------------------------------------------------------------------------
FUNCTION GET_DESC (O_error_message       IN OUT VARCHAR2,
                   O_classification_desc IN OUT V_FISCAL_CLASSIFICATION.classification_desc%TYPE,
                   I_classification_id   IN     V_FISCAL_CLASSIFICATION.classification_id%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: GET_COD_DESC
-- Purpose:       This function gets the classification description and classification id.
----------------------------------------------------------------------------------
FUNCTION GET_COD_DESC (O_error_message       IN OUT VARCHAR2,
                       O_classification_desc IN OUT VARCHAR2,
                       I_classification_id   IN     V_FISCAL_CLASSIFICATION.classification_id%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
   -- Function Name: EXISTS
   -- Purpose      : Checks whether a classification_id already exists
--------------------------------------------------------------------------------
 
FUNCTION EXISTS(O_error_message     IN OUT VARCHAR2,
                O_exists            IN OUT BOOLEAN,
                I_item              IN V_BR_ITEM_FISCAL_ATTRIB.ITEM%TYPE,
                I_classification_id IN V_BR_ITEM_FISCAL_ATTRIB.CLASSIFICATION_ID%TYPE)
   return BOOLEAN;     
--------------------------------------------------------------------------------
END FM_FISCAL_CLASSIFICATION_SQL;
/
 
