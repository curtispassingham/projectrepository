create or replace PACKAGE BODY FM_RETURN_NF_SQL as
----------------------------------------------------------------------------------
-- Function Name: RETURN_NF_PROCESS
-- Purpose:       This function calls the other relavant functions for creating Return NF .
----------------------------------------------------------------------------------
FUNCTION RETURN_NF_PROCESS( O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_program              VARCHAR2(80) := 'FM_RETURN_NF_SQL.RETURN_NF_PROCESS';
   L_error_message        VARCHAR2(250);
   L_exists_in_correc     BOOLEAN;
   L_triangulation        VARCHAR2(1);
   L_compl_fiscal_doc_id  FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
BEGIN

   if FM_RETURN_NF_SQL.CHK_EXISTS_IN_CORREC_DOC(O_error_message
                                               ,I_fiscal_doc_id
                                               ,L_exists_in_correc) = FALSE then
      SQL_LIB.SET_MARK('OPEN','CHECK_ITEM_LEVEL_TAX_EXISTS', NULL,'check_item_level_tax_exists');
      return FALSE;
   end if;
   if (L_exists_in_correc) then
--
      if FM_RETURN_NF_SQL.CREATE_RNF_SCHED(O_error_message  => O_error_message
                                          ,I_fiscal_doc_id  => I_fiscal_doc_id
                                          ) = FALSE then
         SQL_LIB.SET_MARK('OPEN','CREATE_RNF_SCHED', NULL,'create_rnf_sched');
         return FALSE;
      end if;
--

--

      if FM_RETURN_NF_SQL.CREATE_RNF_HEADER(O_error_message  => O_error_message
                                           ,I_fiscal_doc_id  => I_fiscal_doc_id
                                           ) = FALSE then
         SQL_LIB.SET_MARK('OPEN','CREATE_RNF_HEADER', NULL,'create_rnf_header');
         return FALSE;
      end if;
--

      if FM_RETURN_NF_SQL.CREATE_RNF_DETAIL(O_error_message  => O_error_message
                                           ,I_fiscal_doc_id  => I_fiscal_doc_id
                                           ) = FALSE then
         SQL_LIB.SET_MARK('OPEN','CREATE_RNF_DETAIL', NULL,'create_rnf_detail');
         return FALSE;
      end if;


--
      if FM_RETURN_NF_SQL.UPDATE_RNF_HEADER(O_error_message  => O_error_message
                                           ,I_fiscal_doc_id  => I_fiscal_doc_id
                                           ) = FALSE then
         SQL_LIB.SET_MARK('OPEN','UPDATE_RNF_HEADER', NULL,'update_rnf_header');
         return FALSE;
      end if;
--

      if FM_RETURN_NF_SQL.CREATE_RNF_TAX_HEAD(O_error_message  => O_error_message
                                             ,I_fiscal_doc_id  => I_fiscal_doc_id
                                             ) = FALSE then
         SQL_LIB.SET_MARK('OPEN','CREATE_RNF_TAX_HEAD', NULL,'create_rnf_tax_head');
         return FALSE;
      end if;
--


      if FM_RETURN_NF_SQL.CREATE_RNF_TAX_DETAIL(O_error_message  => O_error_message
                                               ,I_fiscal_doc_id  => I_fiscal_doc_id
                                               ) = FALSE then
         SQL_LIB.SET_MARK('OPEN','CREATE_RNF_TAX_DETAIL', NULL,'create_rnf_tax_detail');
         return FALSE;
      end if;
--


      if FM_RETURN_NF_SQL.UPDATE_RNF_TAX_HEADER(O_error_message  => O_error_message
                                               ,I_fiscal_doc_id  => I_fiscal_doc_id
                                               ) = FALSE then
         SQL_LIB.SET_MARK('OPEN','UPDATE_RNF_TAX_HEADER', NULL,'update_rnf_tax_header');
         return FALSE;
      end if;
--
----
   end if;
--
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
--
END RETURN_NF_PROCESS;
----------------------------------------------------------------------------------
-- Function Name: CHK_EXISTS_IN_CORREC_DOC
-- Purpose:       This function checks if the fiscal_doc_id exists in fm_correction_doc
--                table for generating Return NF .
----------------------------------------------------------------------------------
FUNCTION CHK_EXISTS_IN_CORREC_DOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                 ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                 ,O_exists         OUT BOOLEAN)
   return BOOLEAN is
   L_program            VARCHAR2(80) := 'FM_RETURN_NF_SQL.CHK_EXISTS_IN_CORREC_DOC';
   L_key                VARCHAR2(80) := 'fcd.fiscal_doc_id = '||I_fiscal_doc_id;
--
   L_mode_type          VARCHAR2(4) := 'EXIT';
   L_status             VARCHAR2(6):= 'A';
   L_count              NUMBER;
--
   cursor C_GET_CORRECTION is
      SELECT count(1)
        FROM FM_CORRECTION_DOC fcd
       WHERE fcd.fiscal_doc_id = I_fiscal_doc_id
         AND fcd.action_type = 'RNF';
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_CORRECTION', 'fm_correction_doc', L_key);
--
   OPEN C_GET_CORRECTION;
   SQL_LIB.SET_MARK('FETCH','C_GET_CORRECTION', 'fm_correction_doc', L_key);
   FETCH C_GET_CORRECTION INTO l_count;
   SQL_LIB.SET_MARK('CLOSE','C_GET_CORRECTION', 'fm_correction_doc', L_key);
   CLOSE C_GET_CORRECTION;
--
   if L_count > 0 then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_GET_CORRECTION%ISOPEN then
         close C_GET_CORRECTION;
      end if;
--
      return FALSE;
--
END CHK_EXISTS_IN_CORREC_DOC;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_SCHED
-- Purpose:       This function will create a new schedule for the return NF
--                and the status of the schedule will be Approved.
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_SCHED(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                         ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                          )
   return BOOLEAN is
   L_program            VARCHAR2(80) := 'FM_RETURN_NF_SQL.CREATE_RNF_SCHED';
   L_key                VARCHAR2(80) := 'fdh.fiscal_doc_id = '||I_fiscal_doc_id;
   L_mode_type          VARCHAR2(4) := 'EXIT';
   L_status             VARCHAR2(6):= 'A';
   CURSOR C_GET_SCHEDULE(I_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      SELECT fdh.location_id
            ,fdh.location_type
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.fiscal_doc_id = I_fiscal_doc_id;
--
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_SCHEDULE', 'fm_fiscal_doc_header', L_key);
--
   SELECT fm_schedule_seq.nextval
     INTO L_seq_schedule_no
     FROM dual;
--
   FOR L_GET_SCHEDULE IN C_GET_SCHEDULE(I_fiscal_doc_id)
   LOOP
--
      INSERT INTO fm_schedule(schedule_no
                             ,location_id
                             ,location_type
                             ,schedule_date
                             ,mode_type
                             ,status
                             ,accounting_date
                             ,create_datetime
                             ,create_id
                             ,last_update_datetime
                             ,last_update_id
                             ,submitted_ind
                             )
                       values
                             (L_seq_schedule_no
                             ,l_get_schedule.location_id
                             ,l_get_schedule.location_type
                             ,sysdate
                             ,L_mode_type
                             ,L_status
                             ,sysdate
                             ,sysdate
                             ,user
                             ,sysdate
                             ,user
                             ,'Y');
--
   END LOOP;
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_GET_SCHEDULE%ISOPEN then
         close C_GET_SCHEDULE;
      end if;
--
      return FALSE;
--
END CREATE_RNF_SCHED;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_HEADER
-- Purpose:       This function will populate header table for the return NF
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_HEADER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN is
   L_program            VARCHAR2(80) := 'FM_RETURN_NF_SQL.CREATE_RNF_HEADER';
   L_utilization_id     FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_requisition_type   FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'RNF';

   L_fiscal_doc_no            FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE;
   L_series_no                FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE;
   L_subseries_no             FM_FISCAL_DOC_HEADER.SUBSERIES_NO%TYPE;

   L_dummy_desc               FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date               FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_dummy_number             FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_dummy_string             FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;

   L_location_id              FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE;
   L_location_type            FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
   L_create_datetime          FM_FISCAL_DOC_HEADER.CREATE_DATETIME%TYPE := SYSDATE;
   L_type_id                  FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE;
   L_exit_hour                TIMESTAMP := NULL;
   L_dummy                    VARCHAR2(30);
--
   CURSOR C_UTILIZATION is
      SELECT fso.string_value
        FROM fm_system_options fso
       WHERE fso.variable = 'DEFAULT_RNF_UTILIZATION_ID';

CURSOR C_LOCATION is
      SELECT fdd.location_id,
             fdd.location_type
        FROM fm_fiscal_doc_header fdd
       WHERE fdd.fiscal_doc_id = I_fiscal_doc_id;

CURSOR C_DOC_TYPE is
      SELECT fdh.type_id 
        FROM fm_fiscal_doc_header fdh,
             fm_correction_doc fcd
       WHERE fdh.fiscal_doc_id = fcd.fiscal_doc_id
         AND fcd.fiscal_doc_id = I_fiscal_doc_id
         AND fcd.action_type   = L_requisition_type;

BEGIN
--
   SELECT fm_fiscal_doc_id_seq.nextval
   INTO L_seq_fiscal_doc_id
   FROM dual;
--
   OPEN C_UTILIZATION;
   FETCH c_utilization INTO L_utilization_id;
   CLOSE C_UTILIZATION;
     
   OPEN C_LOCATION;
   FETCH C_LOCATION INTO L_location_id,L_location_type;
   CLOSE C_LOCATION;
     
   OPEN C_DOC_TYPE;
   FETCH C_DOC_TYPE INTO L_type_id;
   CLOSE C_DOC_TYPE;
   ---
   if (fm_loc_fiscal_number_sql.next_number(O_error_message,
                                            L_fiscal_doc_no,
                                            L_series_no,
                                            L_subseries_no,
                                            L_location_type,
                                            L_location_id,
                                            L_create_datetime,
                                            L_type_id) = FALSE) then
      return FALSE;
   end if;
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_dummy_desc,
                                         L_dummy_number,
                                         L_dummy_string,
                                         L_dummy_date,
                                         'CHAR',
                                         'HOUR_FORMAT') = FALSE then
      return FALSE;
   end if;
   if L_dummy_string = 'HH24:MI' then
      L_exit_hour := TO_DATE('00:00','HH24:MI');
   elsif L_dummy_string = 'HH:MI AM' then
      L_exit_hour := TO_DATE('12:00 AM','HH:MI AM');
   end if;
   if L_exit_hour is NOT NULL then  
      L_dummy := substr(to_timestamp(L_exit_hour),11,22);
      L_exit_hour := to_char(sysdate,'DD-MON-YY') ||' '|| L_dummy;       
   end if;
   
   INSERT INTO fm_fiscal_doc_header (fiscal_doc_id
                                     ,location_id
                                     ,location_type
                                     ,fiscal_doc_no
                                     ,series_no
                                     ,subseries_no
                                     ,schedule_no
                                     ,status
                                     ,type_id
                                     ,requisition_type
                                     ,module
                                     ,key_value_1
                                     ,key_value_2
                                     ,issue_date
                                     ,entry_or_exit_date
                                     ,exit_hour
                                     ,partner_type
                                     ,partner_id
                                     ,quantity
                                     ,unit_type
                                     ,freight_type
                                     ,net_weight
                                     ,total_weight
                                     ,total_serv_value
                                     ,total_serv_calc_value
                                     ,total_item_value
                                     ,total_item_calc_value
                                     ,total_doc_value
                                     ,total_doc_calc_value
                                     ,freight_cost
                                     ,insurance_cost
                                     ,other_expenses_cost
                                     ,extra_costs_calc
                                     ,discount_type
                                     ,total_discount_value
                                     ,total_doc_value_with_disc
                                     ,create_datetime
                                     ,create_id
                                     ,last_update_datetime
                                     ,last_update_id
                                     ,vehicle_plate
                                     ,plate_state
                                     ,process_origin
                                     ,process_value
                                     ,utilization_id
                                     ,entry_cfop
                                     ,nf_cfop
                                     ,nfe_accesskey
                                     ,tax_discrep_status)
                                     (
                               SELECT distinct L_seq_fiscal_doc_id
                                     ,fdh.location_id
                                     ,fdh.location_type
                                     ,L_fiscal_doc_no--null --fiscal_doc_no
                                     ,L_series_no
                                     ,L_subseries_no
                                     ,L_seq_schedule_no --fs.schedule_no
                                     ,'A'  --status
                                     ,fdh.type_id
                                     ,L_requisition_type  --requisition_type
                                     ,fdh.module
                                     ,fdh.key_value_1
                                     ,fdh.key_value_2
                                     ,sysdate  --issue_date
                                     ,sysdate  --entry_or_exit_date
                                     ,L_exit_hour     --exit_hour
                                     ,fdh.partner_type
                                     ,fdh.partner_id
                                     ,NULL  --quantity
                                     ,fdh.unit_type
                                     ,fdh.freight_type
                                     ,null   -- net_weight
                                     ,null   -- total_wight
                                     ,0      -- TOTAL_SERV_VALUE
                                     ,null   --total_serv_calc_value
                                     ,0      --TOTAL_ITEM_VALUE
                                     ,null   --total_item_calc_value
                                     ,0      --TOTAL_DOC_VALUE
                                     ,null   --total_doc_calc_value
                                     ,NULL   --freight_cost
                                     ,NULL   --insurance_cost
                                     ,NULL   --other_expenses_cost
                                     ,null   --extra_costs_calc
                                     ,fdh.discount_type
                                     ,NULL    --total_discount_value     -- get from fdd.unit_header_disc * return qty
                                     ,null    --total_doc_value_with_disc
                                     ,sysdate --create_datetime
                                     ,user    --create_id
                                     ,sysdate --last_update_datetime
                                     ,user    --last_update_id
                                     ,null    --vehicle_plate
                                     ,null    --plate_state
                                     ,null    --process_origin
                                     ,null    --process_value
                                     ,L_utilization_id --utilization_id
                                     ,null    --entry_cfop
                                     ,null    --nf_cfop
                                     ,null    --nfe_accesskey
                                     ,null    --fdh.tax_discrep_status
                                 FROM fm_fiscal_doc_header fdh
                                     ,fm_correction_doc fcd
                                WHERE fdh.fiscal_doc_id   = fcd.fiscal_doc_id
                                  AND   fcd.fiscal_doc_id = I_fiscal_doc_id
                                  AND   fcd.action_type   = L_requisition_type);
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      return FALSE;
--
END CREATE_RNF_HEADER;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_DETAIL
-- Purpose:       This function will populate detail table for the return NF
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN is
   L_program            VARCHAR2(80) := 'FM_RETURN_NF_SQL.CREATE_RNF_DETAIL';
--
BEGIN
--
/*
   SELECT fm_fiscal_doc_line_id_seq.nextval
     INTO L_seq_fiscal_doc_line_id
     FROM dual;
*/
--
   INSERT INTO fm_fiscal_doc_detail(
                                   fiscal_doc_line_id
                                  ,fiscal_doc_id
                                  ,location_id
                                  ,location_type
                                  ,line_no
                                  ,requisition_no
                                  ,item
                                  ,classification_id
                                  ,quantity
                                  ,unit_cost
                                  ,total_cost
                                  ,total_calc_cost
                                  ,freight_cost
                                  ,net_cost
                                  ,fiscal_doc_line_id_ref
                                  ,fiscal_doc_id_ref
                                  ,discount_type
                                  ,discount_value
                                  ,unit_cost_with_disc
                                  ,create_datetime
                                  ,create_id
                                  ,last_update_datetime
                                  ,last_update_id
                                  ,unexpected_item
                                  ,entry_cfop
                                  ,nf_cfop
                                  ,other_expenses_cost
                                  ,insurance_cost
                                  ,qty_discrep_status
                                  ,cost_discrep_status
                                  ,tax_discrep_status
                                  ,inconclusive_rules
                                  ,appt_qty
                                  ,recoverable_base
                                  ,recoverable_value
                                  ,icms_cst
                                  ,unit_item_disc
                                  ,unit_header_disc
                                  ,pack_no
                                  ,pack_ind
                                  )
                          (SELECT fm_fiscal_doc_line_id_seq.nextval  --L_seq_fiscal_doc_line_id
                                 ,L_seq_fiscal_doc_id
                                 ,fdd.location_id
                                 ,fdd.location_type
                                 ,fdd.line_no
                                 ,fdd.requisition_no
                                 ,fdd.item
                                 ,fdd.classification_id
                                 ,fcd.action_value   --Quantity
                                 ,fdd.unit_cost
                                 ,((unit_cost - nvl(fdd.unit_item_disc,0)) * fcd.action_value) total_cost
                                 ,((fdd.total_calc_cost / fdd.quantity) * fcd.action_value)  total_calc_cost  --TOTAL_CALC_COST
                                 ,fdd.freight_cost  --((fdd.freight_cost / fdd.quantity) * fcd.action_value) freight_cost
                                 ,fdd.net_cost      --((fdd.net_cost / fdd.quantity) * fcd.action_value) net_cost
                                 ,fdd.fiscal_doc_line_id  --fiscal_doc_line_id_ref
                                 ,fdd.fiscal_doc_id       --fiscal_doc_id_ref
                                 ,fdd.discount_type
                                 ,decode(fdd.discount_type,'V',((fdd.discount_value / fdd.quantity) * fcd.action_value),fdd.discount_value ) discount_value
                                 ,fdd.unit_cost_with_disc                                                             --unit_cost_with_disc
                                 ,sysdate --create_datetime
                                 ,user    --create_id
                                 ,sysdate --last_update_datetime
                                 ,user    --last_update_id
                                 ,unexpected_item --null    --unexpected_item
                                 ,null    --entry_cfop
                                 ,null    --nf_cfop
                                 ,fdd.other_expenses_cost --((fdd.other_expenses_cost / fdd.quantity) * fcd.action_value) other_expenses_cost
                                 ,fdd.insurance_cost      --((fdd.insurance_cost / fdd.quantity) * fcd.action_value) insurance_cost
                                 ,null --fdd.qty_discrep_status
                                 ,null --fdd.cost_discrep_status
                                 ,null --fdd.tax_discrep_status
                                 ,null --fdd.inconclusive_rules
                                 ,appt_qty
                                 ,null  --recoverable_base
                                 ,null  --recoverable_value
                                 ,null  --icms_cst
                                 ,fdd.unit_item_disc
                                 ,fdd.unit_header_disc
                                 ,pack_no
                                 ,pack_ind
                             FROM fm_fiscal_doc_detail fdd
                                 ,fm_correction_doc fcd
                            WHERE fdd.fiscal_doc_id      = fcd.fiscal_doc_id
                              AND fdd.fiscal_doc_line_id = fcd.fiscal_doc_line_id
                              AND fcd.fiscal_doc_id      = I_fiscal_doc_id
                              AND fcd.action_type        = 'RNF');
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      return FALSE;
--
END CREATE_RNF_DETAIL;
--
----------------------------------------------------------------------------------
-- Function Name: UPDATE_RNF_HEADER
-- Purpose:       This function will UPDATE all the column in Header table wich needs to be calculated based on the
--                Return NF in the detail table.
----------------------------------------------------------------------------------
FUNCTION UPDATE_RNF_HEADER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id      IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_program              VARCHAR2(80) := 'FM_RETURN_NF_SQL.UPDATE_RNF_HEADER';
--
   L_new_fiscal_doc_id    FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_freight_cost         FM_FISCAL_DOC_HEADER.FREIGHT_COST%TYPE;
   L_insurance_cost       FM_FISCAL_DOC_HEADER.INSURANCE_COST%TYPE;
   L_other_expenses_cost  FM_FISCAL_DOC_HEADER.OTHER_EXPENSES_COST%TYPE;
   L_total_discount_value FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_final_total_discount_value FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_total_serv_value     FM_FISCAL_DOC_HEADER.TOTAL_SERV_VALUE%TYPE;
   L_total_item_value     FM_FISCAL_DOC_HEADER.TOTAL_ITEM_VALUE%TYPE;
   L_total_doc_value       FM_FISCAL_DOC_HEADER.TOTAL_DOC_VALUE%TYPE;
   L_head_total_serv_value FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_quantity              FM_FISCAL_DOC_HEADER.QUANTITY%TYPE;
   L_line_tax               "RIB_TaxDetRBO_TBL"     :=  "RIB_TaxDetRBO_TBL"();
   L_status                VARCHAR2(30);
 L_requisition_type   FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'RNF';

--
   L_key                  VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
   L_key1                 VARCHAR2(80) := 'fiscal_doc_id = '||L_new_fiscal_doc_id;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
--
  CURSOR C_COST_COMPONENT is
      SELECT sum(fdd.freight_cost * fdd.quantity ) freight_cost
            ,sum(fdd.insurance_cost * fdd.quantity) insurance_cost
            ,sum(fdd.other_expenses_cost * fdd.quantity) other_expenses_cost
            ,sum(fdd.unit_header_disc * fcd.action_value) final_total_discount_value
            ,decode(fdh.discount_type,'P',fdh1.total_discount_value,sum(fdd.unit_header_disc * fcd.action_value) ) total_discount_value
            ,fdd.fiscal_doc_id
        FROM fm_fiscal_doc_detail fdd
            ,fm_fiscal_doc_header fdh
            ,fm_correction_doc fcd
            ,fm_fiscal_doc_header fdh1
       WHERE fdd.fiscal_doc_id_ref      = fcd.fiscal_doc_id
         AND fdd.fiscal_doc_line_id_ref = fcd.fiscal_doc_line_id
         AND fcd.fiscal_doc_id          = I_fiscal_doc_id
         and fdd.fiscal_doc_id          = fdh.fiscal_doc_id
         AND fcd.action_type            = L_requisition_type
         AND fdh.requisition_type       = L_requisition_type
         AND fdh1.fiscal_doc_id = fcd.fiscal_doc_id
    GROUP BY fdd.fiscal_doc_id,
             fdh.total_discount_value,
             fdh.discount_type,
		 fdh1.total_discount_value;
--
   CURSOR C_TOTAL_SERV_VALUE is
      SELECT sum(fdd.discount_value)
            ,sum(fdh.total_serv_value)
            ,fdd.fiscal_doc_id
        FROM fm_fiscal_doc_detail fdd
            ,fm_fiscal_doc_header fdh
            ,fm_correction_doc fcd
            ,v_br_item_country_attributes ica
       WHERE fdd.fiscal_doc_id          = fdh.fiscal_doc_id
         AND fdd.fiscal_doc_id_ref      = fcd.fiscal_doc_id
         AND fdd.fiscal_doc_line_id_ref = fcd.fiscal_doc_line_id
         AND fdd.item                   = ica.item
         AND fcd.fiscal_doc_id          = I_fiscal_doc_id
         AND fcd.action_type            = 'RNF'
         AND fdh.requisition_type       = 'RNF'
         AND ica.service_ind            = 'Y'
         AND fdd.pack_ind               = 'N'
    GROUP BY fdd.fiscal_doc_id;
--
   CURSOR C_TOTAL_ITEM_VALUE is
      SELECT sum (NVL(total_item_val.total_cost,0)) total_cost
             ,total_item_val.fiscal_doc_id  fiscal_doc_id
        FROM (
                  SELECT sum(NVL(fdd.total_cost,0)) total_cost
                        ,fdd.fiscal_doc_id
                    FROM fm_fiscal_doc_detail fdd
                        ,fm_fiscal_doc_header fdh
                        ,fm_correction_doc fcd
                        ,v_br_item_country_attributes ica
                   WHERE fdd.fiscal_doc_id_ref      = fcd.fiscal_doc_id
                     AND fdd.fiscal_doc_line_id_ref = fcd.fiscal_doc_line_id
                     AND fdd.fiscal_doc_id          = fdh.fiscal_doc_id
                     AND fdd.item                   = ica.item
                     AND fcd.fiscal_doc_id          = I_fiscal_doc_id
                     AND fcd.action_type            = 'RNF'
                     AND fdh.requisition_type       = 'RNF'
                     AND ica.service_ind            = 'N'
                     AND fdd.pack_ind               = 'N'
                GROUP BY fdd.fiscal_doc_id
               UNION ALL
                  SELECT sum(NVL(fdd.total_cost,0)) total_cost
                        ,fdd.fiscal_doc_id
                    FROM fm_fiscal_doc_detail fdd
                        ,fm_fiscal_doc_header fdh
                        ,fm_correction_doc fcd
                   WHERE fdd.fiscal_doc_id_ref      = fcd.fiscal_doc_id
                     AND fdd.fiscal_doc_line_id_ref = fcd.fiscal_doc_line_id
                     AND fdd.fiscal_doc_id          = fdh.fiscal_doc_id
                     AND fcd.fiscal_doc_id          = I_fiscal_doc_id
                     AND fcd.action_type            = 'RNF'
                     AND fdh.requisition_type       = 'RNF'
                     AND fdd.pack_ind               = 'Y'
                GROUP BY fdd.fiscal_doc_id
              ) total_item_val
    GROUP BY fiscal_doc_id;
--
   CURSOR C_TOTAL_QUANTITY IS
      SELECT sum(fcd.action_value)
        FROM fm_fiscal_doc_header fdh
            ,fm_correction_doc fcd
       WHERE fdh.fiscal_doc_id   = fcd.fiscal_doc_id
         AND fcd.fiscal_doc_id = I_fiscal_doc_id
         AND fcd.action_type   = 'RNF';
--
   CURSOR C_LOCK_HEADER is
      SELECT 'X'
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.fiscal_doc_id = L_new_fiscal_doc_id;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_COST_COMPONENT', 'fm_fiscal_doc_detail', L_key);
--
   OPEN C_COST_COMPONENT;
   SQL_LIB.SET_MARK('FETCH','C_COST_COMPONENT', 'fm_fiscal_doc_detail', L_key);
   FETCH C_COST_COMPONENT INTO L_freight_cost,L_insurance_cost,L_other_expenses_cost,L_final_total_discount_value,L_total_discount_value,L_new_fiscal_doc_id;
   SQL_LIB.SET_MARK('CLOSE','C_COST_COMPONENT', 'fm_fiscal_doc_detail', L_key);
   CLOSE C_COST_COMPONENT;
--
   SQL_LIB.SET_MARK('OPEN','C_TOTAL_SERV_VALUE', 'fm_fiscal_doc_detail', L_key);
   OPEN C_TOTAL_SERV_VALUE;
   FETCH C_TOTAL_SERV_VALUE INTO L_total_serv_value,L_head_total_serv_value,L_new_fiscal_doc_id;
   CLOSE C_TOTAL_SERV_VALUE;
   SQL_LIB.SET_MARK('CLOSE','C_TOTAL_SERV_VALUE', 'fm_fiscal_doc_detail', L_key);
--
   SQL_LIB.SET_MARK('OPEN','C_TOTAL_ITEM_VALUE', 'fm_fiscal_doc_detail', L_key);
   OPEN C_TOTAL_ITEM_VALUE;
   FETCH C_TOTAL_ITEM_VALUE INTO L_total_item_value,L_new_fiscal_doc_id;
   CLOSE C_TOTAL_ITEM_VALUE;
   SQL_LIB.SET_MARK('CLOSE','C_TOTAL_ITEM_VALUE', 'fm_fiscal_doc_detail', L_key);
--
   L_total_doc_value := nvl(L_head_total_serv_value,0) + nvl(L_total_item_value,0) + nvl(L_freight_cost,0) + nvl(L_insurance_cost,0) + nvl(L_other_expenses_cost,0) - NVL(L_final_total_discount_value,0);
--
   SQL_LIB.SET_MARK('OPEN','C_TOTAL_QUANTITY', 'fm_fiscal_doc_detail', L_key);
   OPEN C_TOTAL_QUANTITY;
   FETCH C_TOTAL_QUANTITY INTO L_quantity;
   CLOSE C_TOTAL_QUANTITY;
   SQL_LIB.SET_MARK('CLOSE','C_TOTAL_QUANTITY', 'fm_fiscal_doc_detail', L_key);
--
   OPEN C_LOCK_HEADER;
   CLOSE C_LOCK_HEADER;
--
--To Update Entry Cfop and NF Cfop
    if fm_ext_taxes_sql.get_ext_tax_values(O_line_tax             => L_line_tax,
                                           O_error_message        => O_error_message,
                                           O_status               => L_status,
                                           I_fiscal_doc_id        => L_new_fiscal_doc_id,
                                           I_fiscal_doc_line_id   => null,
                                           I_quantity             => null,
                                           I_cost                 => null) = FALSE then
       return FALSE;
    end if;
--
   UPDATE fm_fiscal_doc_header
      SET freight_cost          = NVL(L_freight_cost,0)
         ,insurance_cost        = NVL(L_insurance_cost,0)
         ,other_expenses_cost   = NVL(L_other_expenses_cost,0)
         ,total_discount_value  = NVL(L_total_discount_value,0)
         ,total_serv_value      = NVL(L_total_serv_value,0)
         ,total_item_value      = NVL(L_total_item_value,0)
         ,total_item_calc_value = NVL(L_total_item_value,0)
         ,total_doc_value       = NVL(L_total_doc_value,0)
         ,quantity              = NVL(L_quantity,0)
         ,last_update_datetime  = sysdate
         ,last_update_id        = user
    WHERE fiscal_doc_id         = L_new_fiscal_doc_id;
--
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('FM_RETURN_NF_SQL.UPDATE_RNF_HEADER',
                                            'FM_FISCAL_DOC_HEADER',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
--
      if C_LOCK_HEADER%ISOPEN then
         close C_LOCK_HEADER;
      end if;
---
      return FALSE;
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_LOCK_HEADER%ISOPEN then
         close C_LOCK_HEADER;
      end if;
--
      return FALSE;
--
END UPDATE_RNF_HEADER;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_TAX_HEAD
-- Purpose:       This function will populate Tax header table for the return NF
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_TAX_HEAD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                            ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN is
   L_program            VARCHAR2(80) := 'FM_RETURN_NF_SQL.CREATE_RNF_TAX_HEAD';
--
BEGIN
--
   INSERT INTO FM_FISCAL_DOC_TAX_HEAD(
                                       vat_code
                                      ,fiscal_doc_id
                                      ,tax_basis
                                      ,total_value
                                      ,create_datetime
                                      ,create_id
                                      ,last_update_datetime
                                      ,last_update_id
                                      ,modified_tax_basis
                                      )
                                      (
                                SELECT distinct fdth.vat_code
                                      ,fdd.fiscal_doc_id
                                      ,0 --fdth.tax_basis
                                      ,0
                                      ,SYSDATE
                                      ,user
                                      ,sysdate
                                      ,user
                                      ,fdth.modified_tax_basis
                                  FROM fm_fiscal_doc_tax_head fdth
                                      ,fm_fiscal_doc_detail fdd
                                 WHERE fdth.fiscal_doc_id = fdd.fiscal_doc_id_ref
                                   AND fdd.fiscal_doc_id_ref = I_fiscal_doc_id
                                   AND fdd.fiscal_doc_id     = L_seq_fiscal_doc_id
                                       );
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      return FALSE;
--
END CREATE_RNF_TAX_HEAD;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_TAX_DETAIL
-- Purpose:       This function will populate Tax Detail table for the return NF
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_TAX_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                              )
   return BOOLEAN is
   L_program            VARCHAR2(80) := 'FM_RETURN_NF_SQL.CREATE_RNF_TAX_DETAIL';
--
BEGIN
--
   INSERT INTO FM_FISCAL_DOC_TAX_DETAIL(
                                        vat_code
                                       ,fiscal_doc_line_id
                                       ,percentage_rate
                                       ,total_value
                                       ,appr_base_value
                                       ,appr_tax_value
                                       ,appr_tax_rate
                                       ,res_base_value
                                       ,res_tax_value
                                       ,res_tax_rate
                                       ,unit_tax_amt
                                       ,unit_rec_value
                                       ,create_datetime
                                       ,create_id
                                       ,last_update_datetime
                                       ,last_update_id
                                       ,tax_basis
                                       ,rec_value
                                       ,modified_tax_basis
                                       ,res_modified_base_value
                                       ,appr_modified_base_value
                                      )
                                      (
                                SELECT fdtd.vat_code
                                      ,fdd.fiscal_doc_line_id
                                      ,fdtd.percentage_rate
                                      ,decode(fdtd.total_value,null,null,((fdtd.total_value / fdd1.quantity) * fcd.action_value)) total_value
                                      ,decode(fdtd.appr_base_value,null,null,((fdtd.appr_base_value / fdd1.quantity) * fcd.action_value)) appr_base_value
                                      ,decode(fdtd.appr_tax_value,null,null,((fdtd.appr_tax_value / fdd1.quantity) * fcd.action_value)) appr_tax_value
                                      ,appr_tax_rate     --appr_tax_rate
                                      ,decode(fdtd.res_base_value,null,null,((fdtd.res_base_value / fdd1.quantity) * fcd.action_value)) res_base_value
                                      ,decode(fdtd.res_tax_value,null,null,((fdtd.res_tax_value / fdd1.quantity) * fcd.action_value)) res_tax_value
                                      ,res_tax_rate     --res_tax_rate
                                      ,unit_tax_amt
                                      ,unit_rec_value
                                      ,sysdate  --create_datetime
                                      ,user     --create_id
                                      ,sysdate  --last_update_datetime
                                      ,user     --last_update_id
                                      ,decode(fdtd.tax_basis,null,null,((fdtd.tax_basis / fdd1.quantity) * fcd.action_value)) tax_basis
                                      ,decode(fdtd.rec_value,null,null,((fdtd.rec_value / fdd1.quantity) * fcd.action_value)) rec_value
                                      ,decode(fdtd.modified_tax_basis,null,null,((fdtd.modified_tax_basis / fdd1.quantity) * fcd.action_value)) modified_tax_basis
                                      ,decode(fdtd.res_modified_base_value,null,null,((fdtd.res_modified_base_value / fdd1.quantity) * fcd.action_value)) res_modified_base_value
                                      ,decode(fdtd.appr_modified_base_value,null,null,((fdtd.appr_modified_base_value / fdd1.quantity) * fcd.action_value)) appr_modified_base_value
                                  from fm_fiscal_doc_tax_detail fdtd
                                      ,fm_fiscal_doc_detail fdd
                                      ,fm_correction_doc fcd
                                      ,fm_fiscal_doc_detail fdd1
                                 where fdd1.fiscal_doc_id = fdd.fiscal_doc_id_ref
                                   and fdd.fiscal_doc_id       = L_seq_fiscal_doc_id
                                   and fdd1.fiscal_doc_line_id = fdd.fiscal_doc_line_id_ref
                                   and fdtd.fiscal_doc_line_id = fcd.fiscal_doc_line_id
                                   and fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id_ref
                                   and fdd.fiscal_doc_id_ref   = fcd.fiscal_doc_id
                                   and fcd.fiscal_doc_id       = I_fiscal_doc_id
                                   and fcd.action_type         = 'RNF'
                                       );
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      return FALSE;
--
END CREATE_RNF_TAX_DETAIL;
--
--
----------------------------------------------------------------------------------
-- Function Name: UPDATE_RNF_TAX_HEADER
-- Purpose:       This function will UPDATE all the column in Header table wich needs to be calculated based on the
--                Return NF in the detail table.
----------------------------------------------------------------------------------
FUNCTION UPDATE_RNF_TAX_HEADER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_fiscal_doc_id      IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_program              VARCHAR2(80) := 'FM_RETURN_NF_SQL.UPDATE_RNF_TAX_HEADER';
--
   L_total_value          FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
   L_appr_tax_value       FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
   L_res_tax_value        FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
   L_new_fiscal_doc_id    FM_FISCAL_DOC_TAX_HEAD.FISCAL_DOC_ID%TYPE;
   L_total_value_head    FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
--
   L_key                  VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
   L_key1                 VARCHAR2(80) := 'fiscal_doc_id = '||L_new_fiscal_doc_id;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
--
   CURSOR C_TOTAL_VALUE is
      SELECT sum(nvl(total_value,0))    total_value
            ,sum(appr_tax_value) appr_tax_value
            ,sum(res_tax_value)  res_tax_value
            ,sum(nvl(nvl(tax_basis,modified_tax_basis),0))  tax_basis
            ,sum(nvl(appr_base_value,appr_modified_base_value)) appr_base_value
            ,sum(nvl(res_base_value,res_modified_base_value))  res_base_value
            ,fdd.fiscal_doc_id
            ,fdtd.vat_code
        FROM fm_fiscal_doc_tax_detail fdtd
            ,fm_fiscal_doc_detail fdd
       WHERE fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         AND fdd.fiscal_doc_id_ref = I_fiscal_doc_id
    GROUP BY fdd.fiscal_doc_id
            ,fdtd.vat_code;
--
   CURSOR C_TOTAL_VALUE_HEAD is
      select sum(nvl(total_value,0)) total_value
        from fm_fiscal_doc_tax_head fdtd
       where fdtd.fiscal_doc_id = L_new_fiscal_doc_id
         AND fdtd.vat_code in ('ICMSST','IPI');
--
--
   CURSOR C_LOCK_HEADER is
      SELECT 'X'
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.fiscal_doc_id = L_new_fiscal_doc_id;
--
   CURSOR C_RNF_FISCAL_DOC_ID is
      SELECT fdd.fiscal_doc_id
        FROM fm_fiscal_doc_detail fdd
       WHERE fdd.fiscal_doc_id_ref = I_fiscal_doc_id;
--
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_TOTAL_VALUE', 'fm_fiscal_doc_tax_detail', L_key);
--
   FOR REC_TOTAL_VALUE in C_TOTAL_VALUE
   LOOP
      if (rec_total_value.total_value > 0 or rec_total_value.tax_basis > 0) then
         UPDATE fm_fiscal_doc_tax_head
            SET total_value          = rec_total_value.total_value
               ,tax_basis            = rec_total_value.tax_basis
               ,last_update_datetime = sysdate
               ,last_update_id       = user
          WHERE fiscal_doc_id        = rec_total_value.fiscal_doc_id
            AND vat_code             = rec_total_value.vat_code;
      else
         UPDATE fm_fiscal_doc_tax_head
            SET total_value          = nvl(rec_total_value.res_tax_value,rec_total_value.appr_tax_value)
               ,tax_basis            = nvl(rec_total_value.res_base_value,rec_total_value.appr_base_value)
               ,last_update_datetime = sysdate
               ,last_update_id       = user
          WHERE fiscal_doc_id        = rec_total_value.fiscal_doc_id
            AND vat_code             = rec_total_value.vat_code;
      end if;
   END LOOP;
--
   SQL_LIB.SET_MARK('OPEN','C_RNF_FISCAL_DOC_ID', 'fm_fiscal_doc_tax_detail', L_key);
   OPEN C_RNF_FISCAL_DOC_ID;
   SQL_LIB.SET_MARK('FETCH','C_RNF_FISCAL_DOC_ID', 'fm_fiscal_doc_tax_detail', L_key);
   FETCH C_RNF_FISCAL_DOC_ID INTO L_new_fiscal_doc_id;
   SQL_LIB.SET_MARK('CLOSE','C_RNF_FISCAL_DOC_ID', 'fm_fiscal_doc_tax_detail', L_key);
   CLOSE C_RNF_FISCAL_DOC_ID;
--
   SQL_LIB.SET_MARK('OPEN','C_TOTAL_VALUE_HEAD', 'fm_fiscal_doc_tax_head', L_key);
   OPEN C_TOTAL_VALUE_HEAD;
   SQL_LIB.SET_MARK('FETCH','C_TOTAL_VALUE_HEAD', 'fm_fiscal_doc_tax_head', L_key);
   FETCH C_TOTAL_VALUE_HEAD INTO L_total_value_head;
   SQL_LIB.SET_MARK('CLOSE','C_TOTAL_VALUE_HEAD', 'fm_fiscal_doc_tax_head', L_key);
   CLOSE C_TOTAL_VALUE_HEAD;
--
   SQL_LIB.SET_MARK('OPEN','C_LOCK_HEADER', 'fm_fiscal_doc_header', L_key);
   OPEN C_LOCK_HEADER;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_HEADER', 'fm_fiscal_doc_header', L_key);
   CLOSE C_LOCK_HEADER;
--
   UPDATE fm_fiscal_doc_header
      SET total_doc_value      = NVL(total_doc_value,0) + NVL(L_total_value_head,0)
         ,total_doc_calc_value = NVL(total_doc_value,0) + NVL(L_total_value_head,0)
         ,last_update_datetime = sysdate
         ,last_update_id       = user
    WHERE fiscal_doc_id        = L_new_fiscal_doc_id;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('FM_RETURN_NF_SQL.UPDATE_RNF_HEADER',
                                            'FM_FISCAL_DOC_HEADER',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
--
      if C_LOCK_HEADER%ISOPEN then
         close C_LOCK_HEADER;
      end if;
---
      return FALSE;
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_TOTAL_VALUE_HEAD%ISOPEN then
         close C_TOTAL_VALUE_HEAD;
         SQL_LIB.SET_MARK('CLOSE','C_TOTAL_VALUE_HEAD', 'fm_fiscal_doc_tax_head', L_key);
      end if;
--
      if C_RNF_FISCAL_DOC_ID%ISOPEN then
         close C_RNF_FISCAL_DOC_ID;
         SQL_LIB.SET_MARK('CLOSE','C_RNF_FISCAL_DOC_ID', 'fm_fiscal_doc_tax_detail', L_key);
      end if;
--
      if C_LOCK_HEADER%ISOPEN then
         close C_LOCK_HEADER;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_HEADER', 'fm_fiscal_doc_header', L_key);
      end if;
--
      return FALSE;
--
END UPDATE_RNF_TAX_HEADER;
--
END fm_return_nf_sql;
/
