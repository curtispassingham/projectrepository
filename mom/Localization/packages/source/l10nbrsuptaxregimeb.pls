CREATE OR REPLACE PACKAGE BODY L10N_BR_SUP_TAX_REGIME_SQL AS
---------------------------------------------------------------------
  -- Function: REGIME_EXISTS
  -- Purpose:  Function will determine if the passed in tax regime is already associated with the passed in supplier (site). 
--------------------------------------------------------------------
FUNCTION REGIME_EXISTS(O_error_message 	IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT     BOOLEAN,
                       I_supplier       IN      NUMBER,
                       I_tax_regime     IN      VARCHAR2)
RETURN BOOLEAN is

L_exists    VARCHAR2(1) := NULL;

cursor C_CHECK_REGIME is
   select 'Y'
     from l10n_br_sup_tax_regime
    where supplier = I_supplier
      and tax_regime = I_tax_regime ;
BEGIN
    O_exists := FALSE;
    
    if I_supplier is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'I_supplier',
                                             'NULL',
                                             'NOT NULL');
       return FALSE;
    end if;
    ---
    if I_tax_regime is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'I_tax_regime',
                                             'NULL',
                                             'NOT NULL');
       return FALSE;
    end if;

    sql_lib.set_mark('OPEN','C_CHECK_REGIME','L10N_BR_SUP_TAX_REGIME','SUPPLIER ID: '||I_supplier||', TAX REGIME: '||I_tax_regime);
    open C_CHECK_REGIME;
    sql_lib.set_mark('FETCH','C_CHECK_REGIME','L10N_BR_SUP_TAX_REGIME','SUPPLIER ID: '||I_supplier||',  TAX REGIME: '||I_tax_regime);
    fetch C_CHECK_REGIME into L_exists;
    sql_lib.set_mark('CLOSE','C_CHECK_REGIME','L10N_BR_SUP_TAX_REGIME','SUPPLIER ID: '||I_supplier||',  TAX REGIME: '||I_tax_regime);
    close C_CHECK_REGIME;
    
    if L_exists is not null then
       O_exists := TRUE;
    end if;
    return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_BR_SUP_TAX_REGIME_SQL.REGIME_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
  
END;
---------------------------------------------------------------------
END L10N_BR_SUP_TAX_REGIME_SQL;
/