CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DOC_COMPLEMENT_SQL is
-------------------------------------------------------------------
FUNCTION GET_DOC_HEADER_ATTR(O_error_message      IN OUT VARCHAR2,
                             O_exists             IN OUT BOOLEAN,
                             O_utilization_id     IN OUT FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                             O_utilization_desc   IN OUT FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                             O_nf_cfop            IN OUT FM_FISCAL_DOC_HEADER.NF_CFOP%TYPE,
                             O_module             IN OUT FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                             O_key_value_1        IN OUT FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                             O_issue_date         IN OUT FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                             O_entry_or_exit_date IN OUT FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                             O_status             IN OUT FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                             O_type_id            IN OUT FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                             IO_fiscal_doc_no     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                             IO_fiscal_doc_id     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                             I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                             I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                             I_complemtary_ind    IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_COMPLEMENT_SQL.GET_DOC_HEADER_ATTR';
   L_key       VARCHAR2(50)  := 'IO_fiscal_doc_id: '||IO_fiscal_doc_id;
   L_pwh       BOOLEAN := NULL;
   L_count     NUMBER;
   L_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   ---
   cursor C_GET_FISCAL_DOC_ATTR is
      select fh.utilization_id,
             fu.utilization_desc,
             fh.nf_cfop,
             fh.module,
             fh.key_value_1,
             fh.issue_date,
             fh.entry_or_exit_date,
             fh.status,
             fh.type_id,
             fh.fiscal_doc_no,
             fh.fiscal_doc_id
        from fm_fiscal_doc_header fh,
             fm_utilization_attributes fa, 
             fm_fiscal_utilization fu
       where fh.utilization_id = fa.utilization_id
         and fh.utilization_id = fu.utilization_id
         and fa.utilization_id = fu.utilization_id
         and fa.comp_nf_ind = NVL(I_complemtary_ind, fa.comp_nf_ind)
         and fh.fiscal_doc_id = NVL(IO_fiscal_doc_id,fh.fiscal_doc_id)
         and fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no)
         and fh.location_id = I_location
         and fh.location_type = I_loc_type
         and fh.status NOT IN ('I','E');
   ---
   cursor C_COUNT_FISCAL_DOC_ATTR is
      select count(*)
        from fm_fiscal_doc_header fh,
             fm_utilization_attributes fa
       where fh.utilization_id = fa.utilization_id
         and fa.comp_nf_ind = NVL(I_complemtary_ind, fa.comp_nf_ind)
         and fh.fiscal_doc_id = NVL(IO_fiscal_doc_id,fh.fiscal_doc_id)
         and fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no)
         and fh.location_id = I_location
         and fh.location_type = I_loc_type
         and fh.status NOT IN ('I','E');
   ---
   cursor C_GET_FISCAL_DOC_ATTR_VWH is
      select fh.utilization_id,
             fu.utilization_desc, 
             fh.nf_cfop,
             fh.module,
             fh.key_value_1,
             fh.issue_date,
             fh.entry_or_exit_date,
             fh.status,
             fh.type_id,
             fh.fiscal_doc_no,
             fh.fiscal_doc_id
        from fm_fiscal_doc_header fh,
             fm_utilization_attributes fa,
             fm_fiscal_utilization fu
       where fh.utilization_id = fa.utilization_id
         and fh.utilization_id = fu.utilization_id
         and fu.utilization_id = fa.utilization_id
         and fa.comp_nf_ind = NVL(I_complemtary_ind, fa.comp_nf_ind)
         and fh.fiscal_doc_id = NVL(IO_fiscal_doc_id,fh.fiscal_doc_id)
         and fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no)
         and fh.location_id IN (select wh.wh from wh where wh.physical_wh = I_location)
         and fh.location_type = I_loc_type
         and fh.status NOT IN ('I','E');
   ---
   cursor C_COUNT_FISCAL_DOC_ATTR_VWH is
      select count(*)
        from fm_fiscal_doc_header fh,
             fm_utilization_attributes fa
       where fh.utilization_id = fa.utilization_id
         and fa.comp_nf_ind = NVL(I_complemtary_ind, fa.comp_nf_ind)
         and fh.fiscal_doc_id = NVL(IO_fiscal_doc_id,fh.fiscal_doc_id)
         and fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no)
         and fh.location_id IN (select wh.wh from wh where wh.physical_wh = I_location)
         and fh.location_type = I_loc_type
         and fh.status NOT IN ('I','E');
   ---
BEGIN
   ---
   if I_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_loc_type = 'S' or
      NOT L_pwh then
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_GET_FISCAL_DOC_ATTR;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_GET_FISCAL_DOC_ATTR into O_utilization_id,
                                       O_utilization_desc,
                                       O_nf_cfop,
                                       O_module,
                                       O_key_value_1,
                                       O_issue_date,
                                       O_entry_or_exit_date,
                                       O_status,
                                       O_type_id,
                                       IO_fiscal_doc_no,
                                       L_fiscal_doc_id;
      ---
      if C_GET_FISCAL_DOC_ATTR%NOTFOUND then
         ---
         O_exists := FALSE;
         IO_fiscal_doc_id := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_GET_FISCAL_DOC_ATTR;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_GET_FISCAL_DOC_ATTR;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ATTR;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ATTR into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ATTR;
      ---
   else
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_GET_FISCAL_DOC_ATTR_VWH;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_GET_FISCAL_DOC_ATTR_VWH into O_utilization_id,
                                           O_utilization_desc,
                                           O_nf_cfop,
                                           O_module,
                                           O_key_value_1,
                                           O_issue_date,
                                           O_entry_or_exit_date,
                                           O_status,
                                           O_type_id,
                                           IO_fiscal_doc_no,
                                           L_fiscal_doc_id;
      ---
      if C_GET_FISCAL_DOC_ATTR_VWH%NOTFOUND then
         ---
         O_exists := FALSE;
         IO_fiscal_doc_id := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_GET_FISCAL_DOC_ATTR_VWH;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_GET_FISCAL_DOC_ATTR_VWH;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ATTR_VWH;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ATTR_VWH into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ATTR_VWH;
      ---
   end if;
   ---
   if NVL(L_count,0) > 1 then
      IO_fiscal_doc_id := NULL;
   else
      IO_fiscal_doc_id := L_fiscal_doc_id;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_GET_FISCAL_DOC_ATTR%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_GET_FISCAL_DOC_ATTR;
      end if;
      ---
      return FALSE;

END GET_DOC_HEADER_ATTR;
---

--------------------------------------------------------------------------------------
FUNCTION VERIFY_EXISTS_DOC_ID(O_error_message  IN OUT VARCHAR2,
                              O_exists         IN OUT BOOLEAN,
                              I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                              I_compl_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_COMPLEMENT_SQL.GET_DOC_HEADER_ATTR';
   L_key       VARCHAR2(50)  := 'I_fiscal_doc_id: '||I_fiscal_doc_id||',I_compl_doc_id: '||I_compl_doc_id;
   L_dummy     VARCHAR2(1);
   ---
   cursor C_DOC_ID_EXISTS is
      select 'X'
        from fm_fiscal_doc_complement fc
       where fc.compl_fiscal_doc_id = I_compl_doc_id
         and fc.fiscal_doc_id = I_fiscal_doc_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_DOC_ID_EXISTS', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
   open C_DOC_ID_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_DOC_ID_EXISTS', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
   fetch C_DOC_ID_EXISTS into L_dummy;
   ---
   if C_DOC_ID_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_DOC_ID_EXISTS', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
   close C_DOC_ID_EXISTS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_DOC_ID_EXISTS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_DOC_ID_EXISTS', 'FM_FISCAL_DOC_COMPLEMENT', L_key);
         close C_DOC_ID_EXISTS;
      end if;
      ---
      return FALSE;

END VERIFY_EXISTS_DOC_ID;
-----------------------------------------------------------------------------------
FUNCTION EXPLODE_MAIN_NF(O_error_message  IN OUT VARCHAR2,
                         I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_compl_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_COMPLEMENT_SQL.EXPLODE_MAIN_NF';
   L_key       VARCHAR2(50)  := 'I_fiscal_doc_id: '||I_fiscal_doc_id;
   L_dummy     VARCHAR2(1);
   ---
   /*cursor C_DETAIL_MAIN_NF is
      select *
        from fm_fiscal_doc_detail
       where fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_LOCK_DOC_DETAIL is
      select 'X'
        from fm_fiscal_doc_detail fd
       where fd.fiscal_doc_id = I_compl_doc_id;
   ---
   Rec_detail     C_DETAIL_MAIN_NF%ROWTYPE;
   L_line_no      NUMBER := 0;*/
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                            I_fiscal_doc_id,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_compl_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_compl_doc_id',
                                            I_compl_doc_id,
                                            NULL);
      return FALSE;
   end if;
   ---
   /*SQL_LIB.SET_MARK('OPEN', 'C_DETAIL_MAIN_NF', 'FM_FISCAL_DOC_DETAIL', L_key);
   open C_DETAIL_MAIN_NF;
   LOOP
      SQL_LIB.SET_MARK('FETCH', 'C_DETAIL_MAIN_NF', 'FM_FISCAL_DOC_DETAIL', L_key);
      fetch C_DETAIL_MAIN_NF
       into Rec_detail;
      ---
         EXIT when C_GET_ORDER_DETAIL%NOTFOUND;
      ---
      --L_line_no := L_line_no + 1;
      ----- INSERTING into fm_fiscal_doc_detail
Insert into fm_fiscal_doc_detail ("FISCAL_DOC_LINE_ID","FISCAL_DOC_ID","LOCATION_ID","LOCATION_TYPE","LINE_NO","REQUISITION_NO","UTILIZATION_CFOP","ITEM","CLASSIFICATION_ID","QUANTITY","UNIT_COST","TOTAL_COST","TOTAL_CALC_COST","OTHER_COST","FREIGHT_COST","NET_COST","FISCAL_DOC_LINE_ID_REF","FISCAL_DOC_ID_REF","DISCOUNT_TYPE","DISCOUNT_VALUE","UNIT_COST_WITH_DISC","CREATE_DATETIME","CREATE_ID","LAST_UPDATE_DATETIME","LAST_UPDATE_ID","UNEXPECTED_ITEM","RECONCILE_TAX","RECONCILE_COST","RECONCILE_QTY","ASN_QTY") values ('13681','8867','101684','W','1','1901','1','100004025','01.01','10','1','10',null,null,null,null,null,null,null,null,null,to_date('30-JUL-09','DD-MON-RR'),'SMAHANSA_RMS01',to_date('30-JUL-09','DD-MON-RR'),'SMAHANSA_RMS01','N',null,null,null,null);

      */
   insert into fm_fiscal_doc_detail
      (FISCAL_DOC_LINE_ID,
       FISCAL_DOC_ID,
       LOCATION_ID,
       LOCATION_TYPE,
       LINE_NO,
       REQUISITION_NO,
       ITEM,
       CLASSIFICATION_ID,
       QUANTITY,
       UNIT_COST,
       TOTAL_COST,
       TOTAL_CALC_COST,
       FREIGHT_COST,
       NET_COST,
       FISCAL_DOC_LINE_ID_REF,
       FISCAL_DOC_ID_REF,
       DISCOUNT_TYPE,
       DISCOUNT_VALUE,
       UNIT_COST_WITH_DISC,
       CREATE_DATETIME,
       CREATE_ID,
       LAST_UPDATE_DATETIME,
       LAST_UPDATE_ID,
       UNEXPECTED_ITEM,
       ENTRY_CFOP,
       NF_CFOP,
       OTHER_EXPENSES_COST,
       INSURANCE_COST,
       QTY_DISCREP_STATUS,
       COST_DISCREP_STATUS,
       TAX_DISCREP_STATUS,
       INCONCLUSIVE_RULES,
       APPT_QTY,
       RECOVERABLE_BASE,
       RECOVERABLE_VALUE)
   select FM_FISCAL_DOC_LINE_ID_SEQ.NEXTVAL,
       I_COMPL_DOC_ID,
       LOCATION_ID,
       LOCATION_TYPE,
       LINE_NO,
       REQUISITION_NO,
       ITEM,
       CLASSIFICATION_ID,
       QUANTITY,
       UNIT_COST,
       TOTAL_COST,
       TOTAL_CALC_COST,
       FREIGHT_COST,
       NET_COST,
       FISCAL_DOC_LINE_ID_REF,
       FISCAL_DOC_ID_REF,
       DISCOUNT_TYPE,
       DISCOUNT_VALUE,
       UNIT_COST_WITH_DISC,
       CREATE_DATETIME,
       CREATE_ID,
       LAST_UPDATE_DATETIME,
       LAST_UPDATE_ID,
       UNEXPECTED_ITEM,
       ENTRY_CFOP,
       NF_CFOP,
       OTHER_EXPENSES_COST,
       INSURANCE_COST,
       QTY_DISCREP_STATUS,
       COST_DISCREP_STATUS,
       TAX_DISCREP_STATUS,
       INCONCLUSIVE_RULES,
       APPT_QTY,
       RECOVERABLE_BASE,
       RECOVERABLE_VALUE
     from fm_fiscal_doc_detail
    where fiscal_doc_id = I_fiscal_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      return FALSE;
END EXPLODE_MAIN_NF;
--------------------------------------------------------------------------------
FUNCTION LOV_COMPLEMENT_FISCAL_DOC(O_error_message  IN OUT VARCHAR2,
                                   O_query          IN OUT VARCHAR2,
                                   I_fiscal_doc_id  IN     VARCHAR2,
                                   I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                   I_loc_type       IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                   I_complementary_ind IN  FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_COMPLEMENT_SQL.LOV_COMPLEMENT_FISCAL_DOC';
BEGIN
   ---
   O_query := 'Select t.fiscal_doc_id, t.fiscal_doc_no, t.series_no, t.module, t.key_value_1, t.issue_date
                from fm_fiscal_doc_header t,
                     fm_utilization_attributes fa
                 where t.utilization_id = fa.utilization_id
                 and fa.complementary_ind != '''||I_complementary_ind||'''
                              and t.fiscal_doc_id != '||I_fiscal_doc_id||'
                                and t.location_id = '||I_location_id||'
                                and t.location_type  = '''||I_loc_type||'''
                                and t.status NOT IN (''I'', ''D'') order by t.fiscal_doc_no';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOV_COMPLEMENT_FISCAL_DOC;
-----------------------------------------------------------------------------
FUNCTION GET_DOC_HEADER_ATTR_FREIGHT_NF(O_error_message      IN OUT VARCHAR2,
                                        O_exists             IN OUT BOOLEAN,
                                        O_utilization_id     IN OUT FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                                        O_utilization_desc   IN OUT FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                                        O_nf_cfop            IN OUT FM_FISCAL_DOC_HEADER.NF_CFOP%TYPE,
                                        O_module             IN OUT FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                                        O_key_value_1        IN OUT FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                        O_issue_date         IN OUT FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                                        O_entry_or_exit_date IN OUT FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                                        O_status             IN OUT FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                                        O_type_id            IN OUT FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                                        IO_fiscal_doc_no     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                        IO_fiscal_doc_id     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                        I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                        I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(200) := 'FM_FISCAL_DOC_COMPLEMENT_SQL.GET_DOC_HEADER_ATTR_FREIGHT_NF';
   L_key       VARCHAR2(50)  := 'IO_fiscal_doc_id: '||IO_fiscal_doc_id;
   L_pwh       BOOLEAN := NULL;
   L_count     NUMBER;
   L_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   ---
   cursor C_GET_FISCAL_DOC_ATTR is
      select fh.utilization_id,
             fu.utilization_desc,
             fh.nf_cfop,
             fh.module,
             fh.key_value_1,
             fh.issue_date,
             fh.entry_or_exit_date,
             fh.status,
             fh.type_id,
             fh.fiscal_doc_no,
             fh.fiscal_doc_id
        from fm_fiscal_doc_header fh,
             fm_utilization_attributes fa, 
             fm_fiscal_utilization fu
       where fh.utilization_id = fa.utilization_id
         and fh.utilization_id = fu.utilization_id
         and fa.utilization_id = fu.utilization_id
         and fa.comp_freight_nf_ind = 'N'
         and fh.fiscal_doc_id = NVL(IO_fiscal_doc_id,fh.fiscal_doc_id)
        -- and (fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no) or fh.fiscal_doc_no IS NULL)
         and fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no)
         and fh.location_id = I_location
         and fh.location_type = I_loc_type
         and fh.status NOT IN ('I','E');
   ---
   cursor C_COUNT_FISCAL_DOC_ATTR is
      select count(*)
        from fm_fiscal_doc_header fh,
             fm_utilization_attributes fa
       where fh.utilization_id = fa.utilization_id
         and fa.comp_freight_nf_ind = 'N'
         and fh.fiscal_doc_id = NVL(IO_fiscal_doc_id,fh.fiscal_doc_id)
        -- and (fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no) or fh.fiscal_doc_no IS NULL)
         and fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no)
         and fh.location_id = I_location
         and fh.location_type = I_loc_type
         and fh.status NOT IN ('I','E');
   ---
   cursor C_GET_FISCAL_DOC_ATTR_VWH is
      select fh.utilization_id,
             fu.utilization_desc, 
             fh.nf_cfop,
             fh.module,
             fh.key_value_1,
             fh.issue_date,
             fh.entry_or_exit_date,
             fh.status,
             fh.type_id,
             fh.fiscal_doc_no,
             fh.fiscal_doc_id
        from fm_fiscal_doc_header fh,
             fm_utilization_attributes fa,
             fm_fiscal_utilization fu
       where fh.utilization_id = fa.utilization_id
         and fh.utilization_id = fu.utilization_id
         and fu.utilization_id = fa.utilization_id
         and fa.comp_freight_nf_ind = 'N'
         and fh.fiscal_doc_id = NVL(IO_fiscal_doc_id,fh.fiscal_doc_id)
         --and (fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no) or fh.fiscal_doc_no IS NULL)
         and fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no)
         and fh.location_id IN (select wh.wh from wh where wh.physical_wh = I_location)
         and fh.location_type = I_loc_type
         and fh.status NOT IN ('I','E');
   ---
   cursor C_COUNT_FISCAL_DOC_ATTR_VWH is
      select count(*)
        from fm_fiscal_doc_header fh,
             fm_utilization_attributes fa
       where fh.utilization_id = fa.utilization_id
         and fa.comp_freight_nf_ind = 'N'
         and fh.fiscal_doc_id = NVL(IO_fiscal_doc_id,fh.fiscal_doc_id)
         and fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no)
        -- and (fh.fiscal_doc_no = NVL(IO_fiscal_doc_no, fh.fiscal_doc_no) or fh.fiscal_doc_no IS NULL)
         and fh.location_id IN (select wh.wh from wh where wh.physical_wh = I_location)
         and fh.location_type = I_loc_type
         and fh.status NOT IN ('I','E');
   ---
BEGIN
   ---
  
   if I_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_loc_type = 'S' or
      NOT L_pwh then
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_GET_FISCAL_DOC_ATTR;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_GET_FISCAL_DOC_ATTR into O_utilization_id,
                                       O_utilization_desc,
                                       O_nf_cfop,
                                       O_module,
                                       O_key_value_1,
                                       O_issue_date,
                                       O_entry_or_exit_date,
                                       O_status,
                                       O_type_id,
                                       IO_fiscal_doc_no,
                                       L_fiscal_doc_id;
      ---
      if C_GET_FISCAL_DOC_ATTR%NOTFOUND then
         ---
         O_exists := FALSE;
         IO_fiscal_doc_id := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_GET_FISCAL_DOC_ATTR;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_GET_FISCAL_DOC_ATTR;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ATTR;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ATTR into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ATTR;
      ---
   else
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_GET_FISCAL_DOC_ATTR_VWH;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_GET_FISCAL_DOC_ATTR_VWH into O_utilization_id,
                                           O_utilization_desc,
                                           O_nf_cfop,
                                           O_module,
                                           O_key_value_1,
                                           O_issue_date,
                                           O_entry_or_exit_date,
                                           O_status,
                                           O_type_id,
                                           IO_fiscal_doc_no,
                                           L_fiscal_doc_id;
      ---
      if C_GET_FISCAL_DOC_ATTR_VWH%NOTFOUND then
         ---
         O_exists := FALSE;
         IO_fiscal_doc_id := NULL;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_GET_FISCAL_DOC_ATTR_VWH;
         ---
         return TRUE;
         ---
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_GET_FISCAL_DOC_ATTR_VWH;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      open C_COUNT_FISCAL_DOC_ATTR_VWH;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_COUNT_FISCAL_DOC_ATTR_VWH into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_FISCAL_DOC_ATTR_VWH', 'FM_FISCAL_DOC_HEADER', L_key);
      close C_COUNT_FISCAL_DOC_ATTR_VWH;
      ---
   end if;
   ---
   if NVL(L_count,0) > 1 then
      IO_fiscal_doc_id := NULL;
   else
      IO_fiscal_doc_id := L_fiscal_doc_id;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_GET_FISCAL_DOC_ATTR%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ATTR', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_GET_FISCAL_DOC_ATTR;
      end if;
      ---
      return FALSE;

END GET_DOC_HEADER_ATTR_FREIGHT_NF;
-----------------------------------------------------------------------------
FUNCTION COMPLEMENT_NF_LINK_SCHEDULE(O_error_message             IN  OUT VARCHAR2,
                                     I_fiscal_doc_id             IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                     I_complement_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
return BOOLEAN is
   L_program       VARCHAR2(200) := 'FM_FISCAL_DOC_COMPLEMENT_SQL.COMPLEMENT_NF_LINK_SCHEDULE';
   L_count         NUMBER;
   L_trg_count     NUMBER;
   L_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_schedule_no   FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE;
   L_table         VARCHAR2(50);
   RECORD_LOCKED   EXCEPTION;

   cursor C_COUNT_TRIANG_IND is
      select COUNT(1) 
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_utilization_attributes fua,
             ordhead ord
       where fdh.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and fdh.requisition_type    = 'PO'
         and fua.utilization_id      = fdh.utilization_id
         and (fdh.fiscal_doc_id      = I_fiscal_doc_id
          or fdh.fiscal_doc_id       = I_complement_fiscal_doc_id)
         and fua.comp_freight_nf_ind = 'N'
         and ord.triangulation_ind   = 'Y';

   cursor C_COUNT_NOT_COMPL_NF is
      select COUNT(1)
        from fm_fiscal_doc_header fdh
       where (fdh.fiscal_doc_id = I_fiscal_doc_id
          or fdh.fiscal_doc_id =I_complement_fiscal_doc_id)
         and fdh.schedule_no is NULL;
   ---
   cursor C_GET_FISCAL_DOC is
      select fdh.fiscal_doc_id
        from fm_fiscal_doc_header fdh
       where (fdh.fiscal_doc_id = I_fiscal_doc_id
          or fdh.fiscal_doc_id =I_complement_fiscal_doc_id)
         and fdh.schedule_no is NULL;

   ---
   cursor C_GET_SCHEDULE_NO is
       select fdh.schedule_no
        from fm_fiscal_doc_header fdh
       where (fdh.fiscal_doc_id = I_fiscal_doc_id
          or fdh.fiscal_doc_id =I_complement_fiscal_doc_id)
         and fdh.schedule_no is NOT NULL
           for update nowait;

   ---
BEGIN
   ---
   open C_COUNT_TRIANG_IND;
   fetch C_COUNT_TRIANG_IND into L_trg_count;
   close C_COUNT_TRIANG_IND;
   ---
   if NVL(L_trg_count,0) = 2 then
   ---
      open C_COUNT_NOT_COMPL_NF;
      fetch C_COUNT_NOT_COMPL_NF into L_count;
      close C_COUNT_NOT_COMPL_NF;
      ---
      if NVL(L_count,0) = 1 then
   	   ---
   	   open C_GET_FISCAL_DOC;
   	   fetch C_GET_FISCAL_DOC into L_fiscal_doc_id;
   	   close C_GET_FISCAL_DOC;
   	   ---

   	   ---
  	   open C_GET_SCHEDULE_NO;
   	   fetch C_GET_SCHEDULE_NO into L_schedule_no;
   	   close C_GET_SCHEDULE_NO;
   	   ---   

         ---
         L_table := 'FM_FISCAL_DOC_HEADER';
         ---
         update fm_fiscal_doc_header  
            set schedule_no         = L_schedule_no  
           where fiscal_doc_id      = L_fiscal_doc_id; 
         ---
      end if;
      ---
   end if;
   ---
   commit;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('L_program',
                                             L_table,
                                             L_fiscal_doc_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if C_COUNT_TRIANG_IND%ISOPEN then
         close C_COUNT_TRIANG_IND;
      end if;
      if C_COUNT_NOT_COMPL_NF%ISOPEN then
         close C_COUNT_NOT_COMPL_NF;
      end if;
      if C_GET_FISCAL_DOC%ISOPEN then
         close C_GET_FISCAL_DOC;
      end if;
      if C_GET_SCHEDULE_NO%ISOPEN then
         close C_GET_SCHEDULE_NO;
      end if;
                                    
      return FALSE;

END COMPLEMENT_NF_LINK_SCHEDULE;
---

---------------------------------------------------------------------------------------------------------------
END FM_FISCAL_DOC_COMPLEMENT_SQL;
/
