CREATE OR REPLACE PACKAGE L10N_BR_TAX_SERVICE_MNGR_SQL AS
----------------------------------------------------------------------------------
RMS_TAX     CONSTANT  VARCHAR2(12) := 'RMS_TAX';
RFM_TAX     CONSTANT  VARCHAR2(12) := 'RFM_TAX';
FISCAL_FDN  CONSTANT  VARCHAR2(12) := 'FISCAL_FDN';
SEED_RETAIL CONSTANT  VARCHAR2(12) := 'SEED_RETAIL';
SEED_COST   CONSTANT  VARCHAR2(12) := 'SEED_COST';
REFRESH     CONSTANT  VARCHAR2(12) := 'REFRESH';
----------------------------------------------------------------------------------
FUNCTION EXECUTE_TAX_PROCESS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status          IN OUT VARCHAR2,
                             I_tax_service_id  IN     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                             I_tax_call_type   IN     VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION GET_TAX_REQUEST_DATA(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_status_code     IN OUT VARCHAR2,
                              O_businessObject  IN OUT "RIB_FiscDocColRBM_REC",
                              I_tax_service_id  IN     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE )
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION SET_TAX_RESPONSE_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_status_code     IN OUT  VARCHAR2,
                               I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                               I_businessObject  IN      "RIB_FiscDocTaxColRBM_REC" )
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_FDN_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status_code     IN OUT  VARCHAR2,
                             O_businessObject  IN OUT  "RIB_FiscalFDNQryRBM_REC",
                             I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION SET_FISCAL_FDN_DATA(O_error_message   IN  OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status_code     IN  OUT  VARCHAR2,
                             I_tax_service_id  IN       L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE,
                             I_businessObject  IN       "RIB_FiscalFDNColRBM_REC" )
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CLEAR_TABLES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tax_service_id  IN     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
END L10N_BR_TAX_SERVICE_MNGR_SQL;
/