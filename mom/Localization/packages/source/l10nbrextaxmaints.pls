create or replace PACKAGE L10N_BR_EXTAX_MAINT_SQL AS
--------------------------------------------------------
SEED_RUN_TYPE       CONSTANT   VARCHAR2(8) := 'SEED';
REFRESH_RUN_TYPE    CONSTANT   VARCHAR2(8) := 'REFRESH';
TAX_PROCESSING_SIZE CONSTANT   NUMBER(5)   := 9999;

ST_ENTITY            CONSTANT  VARCHAR2(2) := 'ST';
WH_ENTITY            CONSTANT  VARCHAR2(2) := 'WH';
PTNR_ENTITY          CONSTANT  VARCHAR2(4) := 'PTNR';
SUPP_ENTITY          CONSTANT  VARCHAR2(4) := 'SUPP';
ITEM_ENTITY          CONSTANT  VARCHAR2(4) := 'ITEM';
--------------------------------------------------------
TYPE TAX_RESULT_REC IS RECORD
(
   LOC_JURISDICTION_CODE           VARCHAR2(10),
   LOC_CNPJ                        VARCHAR2(250),
   LOC_CNAE_CODE                   VARCHAR2(250),
   LOC_ISS_CONTRIB_IND             VARCHAR2(250),
   LOC_IPI_IND                     VARCHAR2(250),
   LOC_ICMS_CONTRIB_IND            VARCHAR2(250),
   LOC_IS_INCOME_RANGE_ELIGIBLE    VARCHAR2(250),
   LOC_IS_DISTR_A_MANUFACTURER     VARCHAR2(250),
   LOC_ICMS_SIMPLES_RATE           VARCHAR2(250),
   LOC_TAX_REGIME_CONCAT           VARCHAR2(250),
   LOC_NAME_VALUE_PAIR_ID_CONCAT   VARCHAR2(250),
   --
   SUP_JURISDICTION_CODE           VARCHAR2(10),
   SUP_CNPJ                        VARCHAR2(250),
   SUP_CNAE_CODE                   VARCHAR2(250),
   SUP_ISS_CONTRIB_IND             VARCHAR2(250),
   SUP_SIMPLES_IND                 VARCHAR2(250),
   SUP_IPI_IND                     VARCHAR2(250),
   SUP_ICMS_CONTRIB_IND            VARCHAR2(250),
   SUP_ST_CONTRIB_IND              VARCHAR2(250),
   SUP_IS_INCOME_RANGE_ELIGIBLE    VARCHAR2(250),
   SUP_IS_DISTR_A_MANUFACTURER     VARCHAR2(250),
   SUP_ICMS_SIMPLES_RATE           VARCHAR2(250),
   SUP_TAX_REGIME_CONCAT           VARCHAR2(250),
   SRC_IS_RURAL_PRODUCER           VARCHAR2(250),
   SUP_NAME_VALUE_PAIR_ID_CONCAT   VARCHAR2(250),
   --
   ITEM_ORIGIN_CODE                VARCHAR2(250),
   ITEM_SERVICE_IND                VARCHAR2(250),
   ITEM_CLASSIFICATION_ID          VARCHAR2(250),
   ITEM_NCM_CHAR_CODE              VARCHAR2(250),
   ITEM_EX_IPI                     VARCHAR2(250),
   ITEM_PAUTA_CODE                 VARCHAR2(250),
   ITEM_SERVICE_CODE               VARCHAR2(250),
   ITEM_FEDERAL_SERVICE            VARCHAR2(250),
   UNIT_COST                       NUMBER(20,4),
   ITEM_DIM_OBJECT                 VARCHAR2(4000),
   ITEM_LENGTH                     NUMBER(12,4),
   ITEM_LWH_UOM                    VARCHAR2(4000),
   ITEM_WEIGHT                     NUMBER(12,4),
   ITEM_NET_WEIGHT                 NUMBER(12,4),
   ITEM_WEIGHT_UOM                 VARCHAR2(4000),
   ITEM_LIQUID_VOLUME              NUMBER(12,4),
   ITEM_LIQUID_VOLUME_UOM          VARCHAR2(4000),
   ITEM_XFORM_IND                  VARCHAR2(250),
   STATE_OF_MANUFACTURE            VARCHAR2(250),
   PHARMA_LIST_TYPE                VARCHAR2(250),
   ITEM_NAME_VALUE_PAIR_ID_CONCAT  VARCHAR2(250),
   PACK_NO                         VARCHAR2(250),
   ITEM                            VARCHAR2(250),
   --
   EFFECTIVE_DATE                  DATE,
   CUM_TAX_PCT                     NUMBER(20,10),
   CUM_TAX_VALUE                   NUMBER(20,10),
   TOTAL_TAX_AMOUNT                NUMBER(20,10),
   TOTAL_RECOVERABLE_AMOUNT        NUMBER(20,10),
   TOTAL_TAX_AMOUNT_NIC_INCL       NUMBER(20,10)
);
TYPE TAX_RESULT_TBL IS TABLE OF TAX_RESULT_REC INDEX BY BINARY_INTEGER;

---

TYPE TAX_RESULT_BREAKOUT_REC IS RECORD
(
   LOC_JURISDICTION_CODE          VARCHAR2(10),
   LOC_CNPJ                       VARCHAR2(250),
   LOC_CNAE_CODE                  VARCHAR2(250),
   LOC_ISS_CONTRIB_IND            VARCHAR2(250),
   LOC_IPI_IND                    VARCHAR2(250),
   LOC_ICMS_CONTRIB_IND           VARCHAR2(250),
   LOC_IS_INCOME_RANGE_ELIGIBLE   VARCHAR2(250),
   LOC_IS_DISTR_A_MANUFACTURER    VARCHAR2(250),
   LOC_ICMS_SIMPLES_RATE          VARCHAR2(250),
   LOC_TAX_REGIME_CONCAT          VARCHAR2(250),
   LOC_NAME_VALUE_PAIR_ID_CONCAT  VARCHAR2(250),
   --
   SUP_JURISDICTION_CODE          VARCHAR2(10),
   SUP_CNPJ                       VARCHAR2(250),
   SUP_CNAE_CODE                  VARCHAR2(250),
   SUP_ISS_CONTRIB_IND            VARCHAR2(250),
   SUP_SIMPLES_IND                VARCHAR2(250),
   SUP_IPI_IND                    VARCHAR2(250),
   SUP_ICMS_CONTRIB_IND           VARCHAR2(250),
   SUP_ST_CONTRIB_IND             VARCHAR2(250),
   SUP_IS_INCOME_RANGE_ELIGIBLE   VARCHAR2(250),
   SUP_IS_DISTR_A_MANUFACTURER    VARCHAR2(250),
   SUP_ICMS_SIMPLES_RATE          VARCHAR2(250),
   SUP_TAX_REGIME_CONCAT          VARCHAR2(250),
   SRC_IS_RURAL_PRODUCER          VARCHAR2(250),
   SUP_NAME_VALUE_PAIR_ID_CONCAT  VARCHAR2(250),
   --
   ITEM_ORIGIN_CODE               VARCHAR2(250),
   ITEM_SERVICE_IND               VARCHAR2(250),
   ITEM_CLASSIFICATION_ID         VARCHAR2(250),
   ITEM_NCM_CHAR_CODE             VARCHAR2(250),
   ITEM_EX_IPI                    VARCHAR2(250),
   ITEM_PAUTA_CODE                VARCHAR2(250),
   ITEM_SERVICE_CODE              VARCHAR2(250),
   ITEM_FEDERAL_SERVICE           VARCHAR2(250),
   UNIT_COST                      NUMBER(20,4),
   ITEM_DIM_OBJECT                VARCHAR2(4000),
   ITEM_LENGTH                    NUMBER(12,4),
   ITEM_LWH_UOM                   VARCHAR2(4000),
   ITEM_WEIGHT                    NUMBER(12,4),
   ITEM_NET_WEIGHT                NUMBER(12,4),
   ITEM_WEIGHT_UOM                VARCHAR2(4000),
   ITEM_LIQUID_VOLUME             NUMBER(12,4),
   ITEM_LIQUID_VOLUME_UOM         VARCHAR2(4000),
   ITEM_XFORM_IND                 VARCHAR2(250),
   STATE_OF_MANUFACTURE           VARCHAR2(250),
   PHARMA_LIST_TYPE               VARCHAR2(250),
   ITEM_NAME_VALUE_PAIR_ID_CONCAT VARCHAR2(250),
   PACK_NO                        VARCHAR2(250),
   ITEM                           VARCHAR2(250),
   --
   EFFECTIVE_DATE                 DATE,
   TAX_CODE                       VARCHAR2(4000),
   TAX_RATE                       NUMBER(20,10),
   TAX_BASIS_AMOUNT               NUMBER(20,10),
   TAX_AMOUNT                     NUMBER(20,10),
   RECOVERABLE_AMOUNT             NUMBER(20,10),
   CALCULATION_BASIS              VARCHAR2(2),
   MODIFIED_TAX_BASIS_AMOUNT      NUMBER(20,10)
);
TYPE TAX_RESULT_BREAKOUT_TBL IS TABLE OF TAX_RESULT_BREAKOUT_REC INDEX BY BINARY_INTEGER;

-----

TYPE NVP_HELPER_REC IS RECORD (COLUMN_NAME   L10N_BR_EXTAX_HELP_NV_PAIR.NAME%TYPE,
                               ENTITY_TYPE   L10N_BR_EXTAX_HELP_NV_PAIR.ENTITY_TYPE%TYPE);
TYPE NVP_HELPER_TBL IS TABLE OF NVP_HELPER_REC INDEX BY BINARY_INTEGER;

--------------------------------------------------------
FUNCTION SEED_EXTAX_SETUP(O_error_message    IN OUT VARCHAR2,
                         I_process_size     IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION SEED_EXTAX_FUTURE_SETUP(O_error_message    IN OUT VARCHAR2,
                                I_process_size     IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION REFRESH_EXTAX_SETUP(O_error_message    IN OUT VARCHAR2,
                            I_process_size     IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION FISCAL_ITM_RECLASS_EXTAX_SETUP(O_error_message    IN OUT VARCHAR2,
                                        I_process_size     IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION PROCESS(O_error_message  IN OUT VARCHAR2,
                 I_process_id     IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION SEED_EXTAX_FINISH(O_error_message  IN OUT VARCHAR2,
                           pos_mods_tax_ind IN VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION FISCAL_ITEM_RECLASS_FINISH(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION REFRESH_EXTAX_FINISH(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION SEED_GTAX_SETUP_COST(O_error_message    IN OUT VARCHAR2,
                              I_process_size     IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION PROCESS_COST(O_error_message  IN OUT VARCHAR2,
                      I_process_id     IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION SEED_GTAX_FINISH_COST(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION FUTURE_TAX_LAW_CHANGES_COST(O_error_message    IN OUT VARCHAR2,
                                     I_process_size     IN     NUMBER,
                                     I_run_type         IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------
FUNCTION FISCAL_ITEM_RECLASS_COST(O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
--------------------------------------------------------
FUNCTION GET_REQUEST_DATA_COST(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id         IN     NUMBER,
                               O_tax_data              OUT "RIB_FiscDocColRBM_REC")
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION GET_REQUEST_DATA_RETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id         IN     NUMBER,
                                 O_tax_data              OUT "RIB_FiscDocColRBM_REC")
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION SET_TAX_RESPONSE_DATA_RETAIL(O_error_message    IN OUT VARCHAR2,
                                      I_TaxData          IN     "RIB_FiscDocTaxColRBM_REC",
                                      I_process_id       IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION SET_TAX_RESPONSE_DATA_COST(O_error_message    IN OUT VARCHAR2,
                                    I_TaxData          IN     "RIB_FiscDocTaxColRBM_REC",
                                    I_process_id       IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION GET_SEQ(I_next IN varchar2)
return NUMBER;
--------------------------------------------------------
--------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
   FUNCTION EXPLODE_TAX_ATTRIBS(O_error_message    IN OUT VARCHAR2,
                                I_process_size     IN     NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION SPLIT_BY_PROCESS_SIZE(O_error_message  IN OUT VARCHAR2,
                                  I_process_size   IN     NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION SETUP_TAX_CALL_ROUTING_RETAIL(O_error_message    IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION MERGE_RESULTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION RESULT_TO_IL(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION RECLASS_ITEM_RESULT_TO_IL(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION FISCAL_ITEM_RECLASS_UPDATE(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION RESULT_TO_ISCL(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION UPD_ITEM_SUPP_COUNTRY_LOC(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION UPD_ITEM_SUPP_COUNTRY(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION INS_ITEM_COST_HEAD(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION INS_ITEM_COST_DETAIL(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION UPD_FUTURE_COST_CURRENT(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   --------------------------------------------------------
   --------------------------------------------------------
   FUNCTION EXPLODE_TAX_ATTRIBS_COST(O_error_message    IN OUT VARCHAR2,
                                     I_process_size     IN     NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION SETUP_TAX_CALL_ROUTING_COST(O_error_message    IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
   FUNCTION CREATE_COST_CHANGES(O_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN;
   --------------------------------------------------------
$end
--------------------------------------------------------
END L10N_BR_EXTAX_MAINT_SQL;
/
