CREATE OR REPLACE PACKAGE FM_AUTO_SHIPPING_SQL IS
---------------------------------------------------------------------------------------------
-- Function Name: CONSUME
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind       IN OUT   BOOLEAN,
                 I_schedule_no     IN       FM_RECEIVING_HEADER.RECV_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END FM_AUTO_SHIPPING_SQL;
/


