create or replace PACKAGE fm_trandata_posting_sql as
----------------------------------------------------------------------------------
-- Function Name: CALC_BC
-- Purpose:       This function Calculates the BC after subtracting all the tax from nf cost.
----------------------------------------------------------------------------------
FUNCTION CALC_BC (O_error_message          IN OUT VARCHAR2,
                  O_base_cost              OUT FM_FISCAL_DOC_DETAIL.BASE_COST%TYPE,
                  I_nic_cost               IN  FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                  I_nic_cost_factor        IN  FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                  I_fiscal_doc_line_id     IN  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                )
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: INSERT_TRAN_DATA
-- Purpose:       This function Inserts into Tran Data based on the Tran Code
----------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                         ,I_tran_code      IN FM_TRAN_DATA.TRAN_CODE%TYPE
                         ,I_fiscal_doc_id  IN FM_TRAN_DATA.FISCAL_DOC_ID%TYPE
                         ,I_fiscal_doc_no  IN FM_TRAN_DATA.FISCAL_DOC_NO%TYPE
                         ,I_supplier       IN FM_TRAN_DATA.SUPPLIER%TYPE
                         ,I_dept           IN FM_TRAN_DATA.DEPT%TYPE
                         ,I_class          IN FM_TRAN_DATA.CLASS%TYPE
                         ,I_subclass       IN FM_TRAN_DATA.SUBCLASS%TYPE
                         ,I_item           IN FM_TRAN_DATA.ITEM%TYPE 
                         ,I_location_type  IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                         ,I_location       IN FM_TRAN_DATA.LOCATION%TYPE
                         ,I_total_cost     IN FM_TRAN_DATA.TOTAL_COST%TYPE
                         ,I_ref_no_1       IN FM_TRAN_DATA.REF_NO_1%TYPE
                         ,I_ref_no_2       IN FM_TRAN_DATA.REF_NO_2%TYPE 
                         ,I_ref_no_3       IN FM_TRAN_DATA.REF_NO_3%TYPE
                         ,I_ref_no_4       IN FM_TRAN_DATA.REF_NO_4%TYPE
                         ,I_ref_no_5       IN FM_TRAN_DATA.REF_NO_5%TYPE
                         ,I_tran_date      IN FM_TRAN_DATA.TRAN_DATE%TYPE ) 
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_REF_VALUES
-- Purpose:       This function gets the REF values based on the Tran codes
----------------------------------------------------------------------------------
FUNCTION GET_REF_VALUES(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                       ,I_tran_code           IN FM_TRAN_DATA.TRAN_CODE%TYPE
                       ,I_fiscal_doc_id       IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                       ,I_fiscal_doc_line_id  IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%type
                       ,I_vat_code            IN FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE
                       ,O_ref_no_1           OUT FM_TRAN_CODES.REF_NO_1%TYPE
                       ,O_ref_no_2           OUT FM_TRAN_CODES.REF_NO_2%TYPE
                       ,O_ref_no_3           OUT FM_TRAN_CODES.REF_NO_3%TYPE
                       ,O_ref_no_4           OUT FM_TRAN_CODES.REF_NO_4%TYPE
                       ,O_ref_no_5           OUT FM_TRAN_CODES.REF_NO_5%TYPE) 
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: PO_WITH_VARIANCE
-- Purpose:       This function This function Process Tran Data when the PO has Variance cost
----------------------------------------------------------------------------------
FUNCTION PO_WITH_VARIANCE( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                          ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                          ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                          ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                          ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                          ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                          ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                          ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                          ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                          ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                          ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                          ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                          ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                          ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                          ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                          ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                          ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                          ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                          ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                          ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                          ,I_triangulation_ind    IN VARCHAR2
                          ,I_requisition_no       IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                          ,I_comp_fiscal_doc_id   IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE) 
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: PROCESS_TRAN_DATA
-- Purpose:       This function Process Tran Data Posting
----------------------------------------------------------------------------------
FUNCTION PROCESS_TRAN_DATA(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id    IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_RNF
-- Purpose:       This function Post trandata when the requisition type is RNF
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_RNF( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                           ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                           ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                           ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                           ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                           ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                           ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                           ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                           ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                           ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                           ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                           ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                           ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                           ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                           ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                           ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                           ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                           ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                           ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                           ,I_triangulation_ind    IN VARCHAR2
                           ,I_requisition_no       IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                           ,I_comp_fiscal_doc_id   IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_RTV
-- Purpose:       This function Post trandata when the requisition type is RTV
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_RTV( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                           ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                           ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                           ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                           ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                           ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                           ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                           ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                           ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                           ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                           ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                           ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                           ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                           ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                           ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                           ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                           ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                           ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                           ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                           ,I_triangulation_ind    IN VARCHAR2
                           ,I_requisition_no       IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                           ,I_comp_fiscal_doc_id   IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_TRANSFERS
-- Purpose:       This function Post trandata when the requisition type is TSF,IC
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_TRANSFERS( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                 ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                 ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                                 ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                                 ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                                 ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                                 ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                                 ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                                 ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                                 ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                                 ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                                 ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                                 ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                                 ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                                 ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                                 ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                                 ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                                 ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                                 ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                                 ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                                 ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                                 ,I_triangulation_ind    IN VARCHAR2
                                 ,I_requisition_no       IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                                 ,I_comp_fiscal_doc_id   IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_CORRECTION
-- Purpose:       This function Process Tran Data Posting for Correction Letter
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_CORRECTION( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                  ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                                  ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                                  ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                                  ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                                  ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                                  ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                                  ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                                  ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                                  ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                                  ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                                  ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                                  ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                                  ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                                  ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                                  ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                                  ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                                  ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                                  ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                                  ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE) 
   return BOOLEAN;
   
   -------------------------------------------------------------------------------------------------
-- Function Name: post_trandata_corr_tran
-- Purpose:       This function Process Tran Data Posting for Correction Letter
----------------------------------------------------------------------------------
FUNCTION post_trandata_corr_tran( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                  ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                                  ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                                  ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                                  ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                                  ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                                  ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                                  ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                                  ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                                  ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                                  ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                                  ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                                  ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                                  ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                                  ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                                  ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                                  ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                                  ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                                  ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                                  ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                                  ,I_comp_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                  ,I_compl_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) 
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_NF_STATUS_C
-- Purpose:       This function will update the NF status to C once all the Posting is done.
----------------------------------------------------------------------------------
FUNCTION UPDATE_NF_STATUS_C( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                            ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_CAL_VARIANCE
-- Purpose:       This function Process Tran Data Posting for Correction Letter
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_CAL_VARIANCE( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                    ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                    ,I_variance             IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE) 
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: PROCESS_TRAN_DATA_TRAN
-- Purpose:       This function Process Tran Data Posting for PO when the triangulation is Y
----------------------------------------------------------------------------------
FUNCTION PROCESS_TRAN_DATA_TRAN( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                               ,I_fiscal_doc_id         IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                               ,I_fiscal_doc_no         IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                               ,I_supplier              IN FM_TRAN_DATA.SUPPLIER%TYPE
                               ,I_item                  IN FM_TRAN_DATA.ITEM%TYPE 
                               ,I_location_type         IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                               ,I_location_id           IN FM_TRAN_DATA.LOCATION%TYPE
                               ,I_unit_cost             IN FM_TRAN_DATA.TOTAL_COST%TYPE
                               ,I_fiscal_doc_line_id    IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                               ,I_variance_cost         IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                               ,I_tolerance_value_cost  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                               ,I_tolerance_value_qty   IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                               ,I_utilization_id        IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                               ,I_requisition_type      IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                               ,I_dept                  IN FM_TRAN_DATA.DEPT%TYPE
                               ,I_class                 IN FM_TRAN_DATA.CLASS%TYPE
                               ,I_subclass              IN FM_TRAN_DATA.SUBCLASS%TYPE
                               ,I_quantity              IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                               ,I_freight_cost          IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                               ,I_insurance_cost        IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                               ,I_other_expenses_cost   IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                               ,I_requisition_no        IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                               ,I_comp_fiscal_doc_id    IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: INSERT_TRAN_DATA
-- Purpose:       This function Inserts Tran data from staging table to Base table.
--                This function check if there are any tax tran code available. If not this will sum all the 
--                tax values and insert into base tran data table
----------------------------------------------------------------------------------
FUNCTION MOVE_TRAN_STG_BASE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id  IN FM_TRAN_DATA.FISCAL_DOC_ID%TYPE) 
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_COMPL_FREIGHT
-- Purpose:       This function Process Tran Data Posting for Complemetary NF 
--                for Non Merchandise cost
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_COMPL_FREIGHT( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                      ,I_fiscal_doc_id       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
END fm_trandata_posting_sql;
/