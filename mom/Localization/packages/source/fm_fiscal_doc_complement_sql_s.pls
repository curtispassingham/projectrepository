CREATE OR REPLACE PACKAGE FM_FISCAL_DOC_COMPLEMENT_SQL is
---------------------------------------------------------------------------------
-- CREATE DATE           - 19-04-2007
-- CREATE USER           ? JLD
-- PROJECT / PT / TRD    - 10481 / /001
-- DESCRIPTION           ? This package manipulate the form orfm_recv_loc_login.fmb
---------------------------------------------------------------------------------
-- Function Name: GET_DOC_HEADER_ATTR
-- Purpose:       This function returns the query for record_grup REC_STORE.
----------------------------------------------------------------------------------
FUNCTION GET_DOC_HEADER_ATTR(O_error_message      IN OUT VARCHAR2,
                             O_exists             IN OUT BOOLEAN,
                             O_utilization_id     IN OUT FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                             O_utilization_desc   IN OUT FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                             O_nf_cfop            IN OUT FM_FISCAL_DOC_HEADER.NF_CFOP%TYPE,
                             O_module             IN OUT FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                             O_key_value_1        IN OUT FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                             O_issue_date         IN OUT FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                             O_entry_or_exit_date IN OUT FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                             O_status             IN OUT FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                             O_type_id            IN OUT FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                             IO_fiscal_doc_no     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                             IO_fiscal_doc_id     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                             I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                             I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                             I_complemtary_ind    IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)

return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VERIFY_EXISTS_DOC_ID
-- Purpose:       This function returns the query for record_grup REC_STORE.
----------------------------------------------------------------------------------
FUNCTION VERIFY_EXISTS_DOC_ID(O_error_message  IN OUT VARCHAR2,
                              O_exists         IN OUT BOOLEAN,
                              I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                              I_compl_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Function Name: EXPLODE_MAIN_NF
-- Purpose:       This function inserts into the fiscal detail tables the details of the NFs that are complemented
----------------------------------------------------------------------------------
FUNCTION EXPLODE_MAIN_NF(O_error_message  IN OUT VARCHAR2,
                         I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_compl_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_COMPLEMENT_FISCAL_DOC
-- Purpose:       This function returns the query for record_grup REC_STORE.
----------------------------------------------------------------------------------
FUNCTION LOV_COMPLEMENT_FISCAL_DOC(O_error_message  IN OUT VARCHAR2,
                                   O_query          IN OUT VARCHAR2,
                                   I_fiscal_doc_id  IN     VARCHAR2,
                                   I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                   I_loc_type       IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                   I_complementary_ind IN  FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)

return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_DOC_HEADER_ATTR_FREIGHT_NF
-- Purpose:       This function returns the query for record_grup REC_STORE.
----------------------------------------------------------------------------------
FUNCTION GET_DOC_HEADER_ATTR_FREIGHT_NF(O_error_message      IN OUT VARCHAR2,
                                        O_exists             IN OUT BOOLEAN,
                                        O_utilization_id     IN OUT FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                                        O_utilization_desc   IN OUT FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                                        O_nf_cfop            IN OUT FM_FISCAL_DOC_HEADER.NF_CFOP%TYPE,
                                        O_module             IN OUT FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                                        O_key_value_1        IN OUT FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                        O_issue_date         IN OUT FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                                        O_entry_or_exit_date IN OUT FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                                        O_status             IN OUT FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                                        O_type_id            IN OUT FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                                        IO_fiscal_doc_no     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                        IO_fiscal_doc_id     IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                        I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                        I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)

return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: COMPLEMENT_NF_LINK_SCHEDULE
-- Purpose:       This function links the unlinked complementary NF with SCHEDULE.
----------------------------------------------------------------------------------
FUNCTION COMPLEMENT_NF_LINK_SCHEDULE(O_error_message           IN OUT VARCHAR2,
                                   I_fiscal_doc_id             IN  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                   I_complement_fiscal_doc_id  IN  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)

return BOOLEAN;


END FM_FISCAL_DOC_COMPLEMENT_SQL;
/