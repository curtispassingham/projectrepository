CREATE OR REPLACE PACKAGE RMSSUB_XITEM_L10N_BR AS
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
-- Function name:  VALIDATE_L10N_ATTRIB
-- Purpose      :  This function contains all validation for XItemDesc message for Brazil l10n
--                 attributes. Validation includes simple validations (required attribute, min/max
--                 values, code types) and complex validations (validation functions defined in
--                 the meta-data tables L10N_EXT_ENTITY_VAL.CROSS_GROUP_VALIDATION_CODE,
--                 L10N_ATTRIB_GROUP.GROUP_VALIDATION_CODE and L10N_ATTRIB.VALIDATION_CODE).
-- Inputs       :  
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_L10N_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_message           IN      "RIB_XItemDesc_REC",
                              I_message_type      IN      VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  PERSIST_L10N_ATTRIB
-- Purpose      :  This function inserts Brazil localization attributes defined in the RIB object 
--                 to item-related l10n extension tables (e.g. item_country_l10n_ext).
-- Inputs       :  
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_L10N_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_message           IN      "RIB_XItemDesc_REC",
                             I_message_type      IN      VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END RMSSUB_XITEM_L10N_BR;
/