CREATE OR REPLACE PACKAGE L10N_BR_DATA_MIGRATION_SQL AS
   G_process_no NUMBER:=NULL;
-----------------------------------------------------------------------------------------
-- Function Name: ADDR
-- Purpose      : This function will populate the jurisdiction_code column of 
--                the ADDR table
-----------------------------------------------------------------------------------------
FUNCTION ADDRESS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_addr_key            IN       INVC_HEAD.ADDR_KEY%TYPE,
                 I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: COMPHEAD
-- Purpose      : This function will populate the jurisdiction_code column of 
--                the COMPHEAD table
-----------------------------------------------------------------------------------------
FUNCTION COMPHEAD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_company             IN       STORE_HIERARCHY.COMPANY%TYPE,
                  I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: RTV_HEAD
-- Purpose      : This function will populate the jurisdiction_code column of 
--                the RTV_HEAD table
-----------------------------------------------------------------------------------------
FUNCTION RTV_HEAD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_rtv_order_no        IN       RTV_DETAIL.RTV_ORDER_NO%TYPE,
                  I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: COMP_STORE
-- Purpose      : This function will populate the jurisdiction_code column of 
--                the COMP_STORE table
-----------------------------------------------------------------------------------------
FUNCTION COMP_STORE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_store               IN       STORE.STORE%TYPE,
                    I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: COMPETITOR
-- Purpose      : This function will populate the jurisdiction_code column of 
--                the COMPETITOR table
-----------------------------------------------------------------------------------------
FUNCTION COMPETITOR(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_competitor          IN       COMP_SHOP_LIST.COMPETITOR%TYPE,
                    I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: CUSTOMER
-- Purpose      : This function will populate the jurisdiction_code column of 
--                the CUSTOMER table
-----------------------------------------------------------------------------------------
FUNCTION CUSTOMER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cust_id             IN       ORDCUST.CUST_ID%TYPE,
                  I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: ORD_CUST
-- Purpose      : This function will populate the jurisdiction_code column of 
--                the ORDCUST table
-----------------------------------------------------------------------------------------
FUNCTION ORD_CUST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cust_id             IN       ORDCUST.CUST_ID%TYPE,
                  I_ordcust_seq_no      IN       ORDCUST.ORDCUST_SEQ_NO%TYPE,
                  I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: OUTLOC
-- Purpose      : This function will populate the jurisdiction_code column of 
--                the OUTLOC table
-----------------------------------------------------------------------------------------
FUNCTION OUTLOC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_outloc_id           IN       VARCHAR2,
                I_outloc_type         IN       VARCHAR2,
                I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: VAT_CODES
-- Purpose      : This function will insert records into VAT_CODES table
-----------------------------------------------------------------------------------------
FUNCTION VAT_CODES(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_vat_code             IN       VARCHAR2,
                   I_vat_code_desc        IN       VARCHAR2,
                   I_incl_nic_ind         IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: L10N_BR_ENTITY_CNAE_CODES
-- Purpose      : This function will insert records into L10N_BR_ENTITY_CNAE_CODES  table.
-----------------------------------------------------------------------------------------
FUNCTION INS_L10N_BR_ENTITY_CNAE_CODES(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: PURGE_STAGING_TBL
-- Purpose      : This function will delete the records from the staging table.
-----------------------------------------------------------------------------------------
FUNCTION PURGE_STAGING_TBL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_base_table          IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: ITEM_COST
-- Purpose      : This function will insert records into ITEM_COST_HEAD and ITEM_COST_DETAIL
--                tables, also into GTAX_ITEM for default_tax_type 'GTAX'. And will update
--                ITEM_SUPP_COUNTRY/ITEM_SUPP_COUNTRY_LOC tables.
-----------------------------------------------------------------------------------------
FUNCTION ITEM_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: COUNTRY_ATTRIB
-- Purpose      : This function will populate item_cost_tax_incl_ind,default_po_cost,default_deal_cost
--                default_cost_comp_cost and localized_ind columns of COUNTRY_ATTRIB table for country_id BR. 
-----------------------------------------------------------------------------------------
FUNCTION COUNTRY_ATTRIBUTES(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_default_loc             IN       COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                            I_default_loc_type        IN       COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                            I_check_ind               IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: FUTURE_COST
-- Purpose      : This will function will recalculate the UNIT_COST, NET_COST, NET_NET_COST,
--                DEAD_NET_NET_COST, etc. based on the NEGOTIATED_ITEM_COST
-----------------------------------------------------------------------------------------
FUNCTION FUTURE_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: COST_SUSP_SUP_DETAIL_LOC
-- Purpose      : This function will update the delivery_country_id field of the
--                cost_susp_sup_detail_loc table.
-----------------------------------------------------------------------------------------
FUNCTION COST_SUSP_SUP_DETAIL_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: COST_SUSP_SUP_DETAIL
-- Purpose      : This function will update the delivery_country_id field of the 
--                cost_susp_sup_detail table. This will also insert new records to the 
--                table for multiple delivery countries attached to an item.
-----------------------------------------------------------------------------------------
FUNCTION COST_SUSP_SUP_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: SYSTEM_OPTIONS
-- Purpose      : This function will update the default_tax_type
--                in the SYSTEM_OPTIONS table
-----------------------------------------------------------------------------------------
FUNCTION SYS_OPTIONS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GET_PROCESS_SEQ_VAL
-- Purpose      : This function will get the  sequence value from the corresponding sequence
--                 depending on the I_ext
-----------------------------------------------------------------------------------------

FUNCTION GET_PROCESS_SEQ_VAL(I_ext IN  VARCHAR2)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
-- Function Name: PURGE_STG_L10N_EXT
-- Purpose      : This function will delete the records from the staging table for the 
--                process_id
-----------------------------------------------------------------------------------------
FUNCTION PURGE_STG_L10N_EXT   (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_base_table           IN       VARCHAR2,
                               I_process_id           IN       NUMBER)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: ORD_TAX_BREAKUP
-- Purpose      : This function will fill up the tax breakup table with open purchase 
--                orders.
-----------------------------------------------------------------------------------------
FUNCTION ORD_TAX_BREAKUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: MRT_L10N_EXT
-- Purpose      : This function will fill up the mrt_l10n_ext table 
-----------------------------------------------------------------------------------------
FUNCTION MRT_L10N_EXT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: ORDHEAD_L10N_EXT
-- Purpose      : This function will fill up the ordhead_l10n_ext table
-----------------------------------------------------------------------------------------
FUNCTION ORDHEAD_L10N_EXT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: TSFHEAD_L10N_EXT
-- Purpose      : This function will fill up the tsfhead_l10n_ext table
-----------------------------------------------------------------------------------------
FUNCTION TSFHEAD_L10N_EXT  (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GTAX_ITEM_ROLLUP
-- Purpose      : This function will fill up the gtax_item_rollup table
-----------------------------------------------------------------------------------------
FUNCTION GTAX_ITEM_ROLLUP   (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: UPD_NIC_COST_TYPE
-- Purpose      : This function will update the unit cost to NIC and update the cost type to NIC
-----------------------------------------------------------------------------------------
FUNCTION UPD_NIC_COST_TYPE   (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function : DEFAULT_FISCAL_ATTRIB
-- Purpose  : This function will default fiscal attribute values for virtual warehouses.
------------------------------------------------------------------------------
FUNCTION DEFAULT_FISCAL_ATTRIB(O_error_message  IN OUT   VARCHAR2)
RETURN BOOLEAN;
END L10N_BR_DATA_MIGRATION_SQL;

/