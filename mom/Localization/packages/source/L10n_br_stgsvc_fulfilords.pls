CREATE OR REPLACE PACKAGE L10N_BR_STGSVC_FULFILORD AS
-----------------------------------------------------------------------------------------------
-- Function Name: STAGE_SVC_FULFILORD_L10N
-- Purpose      : This function stages Br customer order fulfillment create requests
--                in the RIB collection object to interface staging tables.
-----------------------------------------------------------------------------------------------
FUNCTION STAGE_SVC_FULFILORD_L10N(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_l10n_obj                  IN OUT   L10N_OBJ)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Function Name: CLEAR_SVC_FULFILORD_L10N
-- Purpose      : This is the public function that deletes staged br customer order
--                fulfillment create.
-----------------------------------------------------------------------------------------------
FUNCTION CLEAR_SVC_FULFILORD_L10N(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_l10n_obj       IN OUT   L10N_OBJ)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END L10N_BR_STGSVC_FULFILORD;
/