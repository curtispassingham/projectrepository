CREATE OR REPLACE PACKAGE FM_SYSTEM_OPTIONS_SQL is
---------------------------------------------------------------------------------
-- Function Name: GET_VARIABLE
-- Purpose:       This function will return data from fm_system_options.
---------------------------------------------------------------------------------
FUNCTION GET_VARIABLE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_description       IN OUT FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE,
                      O_number_value      IN OUT FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE,
                      O_string_value      IN OUT FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE,
                      O_date_value        IN OUT FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE,
                      I_variable_type     IN     FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE,
                      I_variable          IN     FM_SYSTEM_OPTIONS.VARIABLE%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_DESC
-- Purpose:       This function will return description from fm_system_options.
---------------------------------------------------------------------------------
FUNCTION GET_DESC (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_description     IN OUT FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE,
                   I_variable_type   IN     FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE,
                   I_variable        IN     FM_SYSTEM_OPTIONS.VARIABLE%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXISTS
-- Purpose:       This function will variable exists in fm_system_options table.
---------------------------------------------------------------------------------
FUNCTION EXISTS (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists            IN OUT BOOLEAN,
                 I_variable          IN FM_SYSTEM_OPTIONS.VARIABLE%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: P_SET_WHERE_CLAUSE
-- Purpose:       This function will set the where clause.
---------------------------------------------------------------------------------
FUNCTION P_SET_WHERE_CLAUSE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_where_clause       IN OUT VARCHAR2,
                            I_variable_type      IN     VARCHAR2,
                            I_variable           IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: LOV_VARIABLE
-- Purpose:       This function will get the variable query.
----------------------------------------------------------------------------------
FUNCTION LOV_VARIABLE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_where_clause   IN     VARCHAR2)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: RESOLUTION_RULE_EXISTS 
-- Purpose:       This function will check the resolution rules are SYS/NF/RECONCILE.
----------------------------------------------------------------------------------
FUNCTION RESOLUTION_RULE_EXISTS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT BOOLEAN,
                                I_variable_value   IN     FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
END FM_SYSTEM_OPTIONS_SQL;
/

