CREATE OR REPLACE PACKAGE BODY L10N_BR_ITEM_SQL AS
------------------------------------------------------------------------
FUNCTION CLASS_ID_EXISTS(O_error_message       IN OUT VARCHAR2,
                         O_exists              IN OUT BOOLEAN,
                         I_country             IN     L10N_BR_NCM_CODES.COUNTRY_ID%TYPE,
                         I_classification      IN     L10N_BR_NCM_CODES.NCM_CODE%TYPE,
                         I_classification_type IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64) := 'L10N_BR_ITEM_SQL.CLASS_ID_EXISTS';
   L_fiscal_class   VARCHAR2(1)  := NULL;
   L_key            VARCHAR2(100):= ',country: ' || I_country ||
                                    ',classification: ' || I_classification ||
                                    ',classification_type: ' || I_classification_type;

   cursor C_EXISTS is
      select 'X'
        from l10n_br_ncm_codes
       where ncm_code             = I_classification
         and country_id           = I_country
         and 'I'                  = NVL(I_classification_type,'I')
      union
      select 'X'
        from l10n_br_federal_svc_codes
       where federal_service_code = I_classification
         and 'BR'                 = I_country
         and 'S'                  = NVL(I_classification_type,'S');

BEGIN
   O_exists := FALSE;

   if I_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_classification is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'L10N_BR_NCM_CODES'||' AND '||'L10N_BR_FEDERAL_SVC_CODES',
                    L_key);
   open C_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'L10N_BR_NCM_CODES'||' AND '||'L10N_BR_FEDERAL_SVC_CODES',
                    L_key);
   fetch C_EXISTS into L_fiscal_class;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    'L10N_BR_NCM_CODES'||' AND '||'L10N_BR_FEDERAL_SVC_CODES',
                    L_key);
   close C_EXISTS;

   if L_fiscal_class is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CLASS_ID_EXISTS;
--------------------------------------------------------------------------------------------
FUNCTION  GET_FISCAL_CLASS_INFO(O_error_message       IN OUT VARCHAR2,
                                O_classification_desc IN OUT L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                                O_classification_type IN OUT VARCHAR2,
                                I_country_id          IN     L10N_BR_NCM_CODES.COUNTRY_ID%TYPE,
                                I_classification_id   IN     L10N_BR_NCM_CODES.NCM_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_BR_ITEM_SQL.GET_FISCAL_CLASS_INFO ';

   cursor C_GET_INFO is
      select ncm_desc,
             'I'
        from l10n_br_ncm_codes
       where ncm_code             = I_classification_id
         and country_id           = NVL(I_country_id,country_id)
      union
      select federal_service_desc,
             'S'
        from l10n_br_federal_svc_codes
       where federal_service_code = I_classification_id
         and 'BR'                 = NVL(I_country_id,'BR');
BEGIN

   if I_classification_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   ---
   open C_GET_INFO;
   fetch C_GET_INFO into O_classification_desc,O_classification_type;
   close C_GET_INFO;

   if O_classification_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FISCAL_CLASS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_FISCAL_CLASS_INFO;
-------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_ATTRIB(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_l10n_obj     IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'L10N_BR_ITEM_SQL.COPY_DOWN_ATTRIB';
   L_child_item      ITEM_MASTER.ITEM%TYPE;

   cursor C_GET_CHILD_ITEMS is
      select item
        from item_master
       where (item_parent = IO_l10n_obj.item or
              item_grandparent = IO_l10n_obj.item)
         and item_level >= tran_level
    order by item_level;

BEGIN

   if IO_l10n_obj.country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'IO_l10n_obj.country_id',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if IO_l10n_obj.item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'IO_l10n_obj.item',
                                           L_program,
                                           NULL);
      return FALSE;
   else
     FOR child_item IN C_GET_CHILD_ITEMS LOOP
         L_child_item := child_item.item;

         MERGE INTO item_country_l10n_ext icl10ne
           USING (select *
                    from item_country_l10n_ext
                   where item = IO_l10n_obj.item
                     and country_id = IO_l10n_obj.country_id) d
           ON (icl10ne.item = child_item.item
           and icl10ne.country_id = IO_l10n_obj.country_id
           and icl10ne.group_id = d.group_id)
         WHEN MATCHED THEN
           UPDATE SET
              icl10ne.varchar2_1 = d.varchar2_1,
              icl10ne.varchar2_2 = d.varchar2_2,
              icl10ne.varchar2_3 = d.varchar2_3,
              icl10ne.varchar2_4 = d.varchar2_4,
              icl10ne.varchar2_5 = d.varchar2_5,
              icl10ne.varchar2_6 = d.varchar2_6,
              icl10ne.varchar2_7 = d.varchar2_7,
              icl10ne.varchar2_8 = d.varchar2_8,
              icl10ne.varchar2_9 = d.varchar2_9,
              icl10ne.varchar2_10 = d.varchar2_10,
              icl10ne.number_11 = d.number_11,
              icl10ne.number_12 = d.number_12,
              icl10ne.number_13 = d.number_13,
              icl10ne.number_14 = d.number_14,
              icl10ne.number_15 = d.number_15,
              icl10ne.number_16 = d.number_16,
              icl10ne.number_17 = d.number_17,
              icl10ne.number_18 = d.number_18,
              icl10ne.number_19 = d.number_19,
              icl10ne.number_20 = d.number_20,
              icl10ne.date_21 = d.date_21,
              icl10ne.date_22 = d.date_22
         WHEN NOT MATCHED THEN
           INSERT (item,
                   country_id,
                   l10n_country_id,
                   group_id,
                   varchar2_1,
                   varchar2_2,
                   varchar2_3,
                   varchar2_4,
                   varchar2_5,
                   varchar2_6,
                   varchar2_7,
                   varchar2_8,
                   varchar2_9,
                   varchar2_10,
                   number_11,
                   number_12,
                   number_13,
                   number_14,
                   number_15,
                   number_16,
                   number_17,
                   number_18,
                   number_19,
                   number_20,
                   date_21,
                   date_22)
            VALUES(L_child_item,
                   d.country_id,
                   d.l10n_country_id,
                   d.group_id,
                   d.varchar2_1,
                   d.varchar2_2,
                   d.varchar2_3,
                   d.varchar2_4,
                   d.varchar2_5,
                   d.varchar2_6,
                   d.varchar2_7,
                   d.varchar2_8,
                   d.varchar2_9,
                   d.varchar2_10,
                   d.number_11,
                   d.number_12,
                   d.number_13,
                   d.number_14,
                   d.number_15,
                   d.number_16,
                   d.number_17,
                   d.number_18,
                   d.number_19,
                   d.number_20,
                   d.date_21,
                   d.date_22);
     END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END COPY_DOWN_ATTRIB;
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
-- LFAS Group Validation functions
-------------------------------------------------------------------------------------
FUNCTION CHK_FISCAL_ATTRIB_GRP(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_service_ind          IN     VARCHAR2,
                               I_origin_code          IN     VARCHAR2,
                               I_classification_id    IN     VARCHAR2,
                               I_ncm_char_code        IN     VARCHAR2,
                               I_ex_ipi               IN     VARCHAR2,
                               I_pauta_code           IN     VARCHAR2,
                               I_service_code         IN     VARCHAR2,
                               I_federal_service      IN     VARCHAR2,
                               I_state_of_manufacture IN     VARCHAR2,
                               I_pharma_list_type     IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'L10N_BR_ITEM_SQL.CHK_FISCAL_ATTRIB_GRP';

BEGIN

   if I_service_ind = 'Y' then
      if I_service_code is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SERV_CODE_REQ');
         return FALSE;
      end if;
      if I_federal_service is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FED_SERV_REQ');
         return FALSE;
      end if;
      if I_classification_id    is NOT NULL or 
         I_ncm_char_code        is NOT NULL or 
         I_ex_ipi               is NOT NULL or 
         I_pauta_code           is NOT NULL or
         I_state_of_manufacture is NOT NULL or 
         I_pharma_list_type     is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SERVICE_IND_CHECK', NULL, NULL, NULL);
         return FALSE;
      end if;
   elsif I_service_ind = 'N' then
      if I_classification_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NCM_REQ');
         return FALSE;
      end if;
      if I_service_code    is NOT NULL or 
         I_federal_service is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SERVICE_IND_UNCHECK',NULL, NULL, NULL);
         return FALSE;
      end if;
      
      --NCM_CHAR_CODE and PAUTA_CODE have a hierarchical relationship -
      --If PAUTA_CODE is defined, NCM_CHAR_CODE must be defined.
      if I_ncm_char_code is NULL and
         I_pauta_code is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NCM_CHAR_PAUTA',NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_FISCAL_ATTRIB_GRP;
-------------------------------------------------------------------------------------

--LFAS Entity Validation Function(s)
-------------------------------------------------------------------------------------
FUNCTION CHK_ITEM_CTRY_ENT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_service_ind          IN     VARCHAR2,
                           I_origin_code          IN     VARCHAR2,
                           I_classification_id    IN     VARCHAR2,
                           I_ncm_char_code        IN     VARCHAR2,
                           I_ex_ipi               IN     VARCHAR2,
                           I_pauta_code           IN     VARCHAR2,
                           I_service_code         IN     VARCHAR2,
                           I_federal_service      IN     VARCHAR2,
                           I_state_of_manufacture IN     VARCHAR2,
                           I_pharma_list_type     IN     VARCHAR2)

RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'L10N_BR_ITEM_SQL.CHK_ITEM_CTRY_ENT';

BEGIN

   if I_service_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('SERVICE_IND_REQ',NULL,NULL,NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHK_ITEM_CTRY_ENT;
-------------------------------------------------------------------------------------
FUNCTION GET_FISCAL_RECLASS_SEQ(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_fiscal_reclass_seq   IN OUT   NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'L10N_BR_ITEM_SQL.GET_FISCAL_RECLASS_SEQ';
   L_sequence_name   VARCHAR2(28) := 'L10N_BR_FISCAL_RECLASS_SEQ'; 

   NO_SEQ_NO_AVAIL   EXCEPTION;

   cursor C_GET_RECLASS_SEQ is
      select l10n_br_fiscal_reclass_seq.nextval
        from dual;

BEGIN
   open C_GET_RECLASS_SEQ;
   fetch C_GET_RECLASS_SEQ into O_fiscal_reclass_seq;
   close C_GET_RECLASS_SEQ;

   return TRUE;

EXCEPTION
   WHEN NO_SEQ_NO_AVAIL THEN
      O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                            L_sequence_name,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_FISCAL_RECLASS_SEQ;
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_FRECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_item_desc       IN OUT   ITEM_MASTER.ITEM_DESC%TYPE,
                                O_item_level      IN OUT   ITEM_MASTER.ITEM_LEVEL%TYPE,
                                I_item            IN       ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'L10N_BR_ITEM_SQL.VALIDATE_ITEM_FRECLASS';
   L_status          ITEM_MASTER.STATUS%TYPE;
   L_tran_level      ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_pack_ind        ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind    ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind   ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type       ITEM_MASTER.PACK_TYPE%TYPE;
   L_fiscal_check    VARCHAR2(1) := 'N';
   L_exists          BOOLEAN := FALSE;

   cursor C_FISCAL_ATTR is
      select 'Y'
        from v_br_item_fiscal_attrib
       where item = I_item;
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ITEM',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_FISCAL_ATTR;
   fetch C_FISCAL_ATTR into L_fiscal_check;
   close C_FISCAL_ATTR;
   if L_fiscal_check = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_FISCAL_ATTRIB_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                               O_item_desc,
                               L_status,
                               O_item_level,
                               L_tran_level,
                               I_item) = FALSE then
      return FALSE;
   end if;

   if L_status <> 'A' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_NOT_APPROVED',
                                            I_item,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if O_item_level != 1 then
      O_error_message := SQL_LIB.CREATE_MSG('RECLASS_ITEM_LEVEL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                   L_exists,
                                   I_item,
                                   'ITEM_MASTER') = FALSE then
      return FALSE;
   end if;

   if L_exists = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('POS_DUP_DEL_PARENT',
                                            I_item,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;

   if L_pack_ind = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PACK_ALLOWED',
                                            I_item,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ITEM_FRECLASS;
-------------------------------------------------------------------------------------
FUNCTION GET_CLASSIFICATION(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_classification_id      IN OUT   V_BR_ITEM_FISCAL_ATTRIB.CLASSIFICATION_ID%TYPE,
                            O_service_ind            IN OUT   V_BR_ITEM_FISCAL_ATTRIB.SERVICE_IND%TYPE,
                            O_origin_code            IN OUT   V_BR_ITEM_FISCAL_ATTRIB.ORIGIN_CODE%TYPE,
                            O_service_code           IN OUT   V_BR_ITEM_FISCAL_ATTRIB.SERVICE_CODE%TYPE,
                            O_federal_service_code   IN OUT   V_BR_ITEM_FISCAL_ATTRIB.FEDERAL_SERVICE%TYPE,
                            O_country_id             IN OUT   V_BR_ITEM_FISCAL_ATTRIB.COUNTRY_ID%TYPE, 
                            O_ncm_char_code          IN OUT   V_BR_ITEM_FISCAL_ATTRIB.NCM_CHAR_CODE%TYPE,
                            O_ex_ipi_code            IN OUT   V_BR_ITEM_FISCAL_ATTRIB.EX_IPI%TYPE,
                            O_pauta_code             IN OUT   V_BR_ITEM_FISCAL_ATTRIB.PAUTA_CODE%TYPE,
                            O_state_of_manufacture   IN OUT   V_BR_ITEM_FISCAL_ATTRIB.STATE_OF_MANUFACTURE%TYPE,
                            O_pharma_list_type       IN OUT   V_BR_ITEM_FISCAL_ATTRIB.PHARMA_LIST_TYPE%TYPE,
                            I_item                   IN       V_BR_ITEM_FISCAL_ATTRIB.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'L10N_BR_ITEM_SQL.GET_CLASSIFICATION';

   cursor C_GET_CLASSIFICATION is
      select country_id,
             classification_id,
             ncm_char_code,
             ex_ipi,
             pauta_code,
             service_code,
             federal_service,
             service_ind,
             origin_code,
             state_of_manufacture,
             pharma_list_type
        from v_br_item_fiscal_attrib
       where item = I_item;
       
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'ITEM',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_CLASSIFICATION;
   
   fetch C_GET_CLASSIFICATION into O_country_id,
                                   O_classification_id,
                                   O_ncm_char_code,
                                   O_ex_ipi_code,
                                   O_pauta_code,
                                   O_service_code,
                                   O_federal_service_code,
                                   O_service_ind,
                                   O_origin_code,
                                   O_state_of_manufacture,
                                   O_pharma_list_type;

   close C_GET_CLASSIFICATION;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_CLASSIFICATION;
-------------------------------------------------------------------------------------
FUNCTION FRECLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_item            IN       L10N_BR_FISCAL_RECLASS.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'L10N_BR_ITEM_SQL.FRECLASS_EXISTS';
   L_exists    VARCHAR2(1) := NULL;

   cursor C_FRECLASS_EXISTS is
      select 'x'
        from l10n_br_fiscal_reclass
       where item = I_item
         and processed_date is NULL;

BEGIN
   open C_FRECLASS_EXISTS;
   fetch C_FRECLASS_EXISTS into L_exists;
   close C_FRECLASS_EXISTS;

   if L_exists is not NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END FRECLASS_EXISTS;
-------------------------------------------------------
FUNCTION GET_SERVICE_CODE_DESC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_mtr_service_code_desc   IN OUT   L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_DESC%TYPE,
                               O_fed_service_code_desc   IN OUT   L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_DESC%TYPE,
                               I_mtr_service_code        IN       L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_CODE%TYPE,
                               I_fed_service_code        IN       L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_CODE%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'L10N_BR_ITEM_SQL.GET_SERVICE_CODE_DESC';
   L_null_code   VARCHAR2(25) := NULL;

   cursor C_GET_MTR_DESC is
      select mastersaf_service_desc
        from l10n_br_mastersaf_svc_codes
       where mastersaf_service_code = I_mtr_service_code;

   cursor C_GET_FED_DESC is
      select federal_service_desc
        from l10n_br_federal_svc_codes
       where federal_service_code = I_fed_service_code;
BEGIN
   if I_mtr_service_code is NULL and I_fed_service_code is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'SERVICE CODE',
                                             L_program,
                                             NULL);
       return FALSE;
   end if;
   
   if I_mtr_service_code is NOT NULL then
      -- get mtr service code desc
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_MTR_DESC',
                       'L10N_BR_MASTERSAF_SVC_CODES',
                       'MASTERSAF_SERVICE_CODE: ' || I_mtr_service_code);
      open C_GET_MTR_DESC;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_MTR_DESC',
                       'L10N_BR_MASTERSAF_SVC_CODES',
                       'MASTERSAF_SERVICE_CODE: ' || I_mtr_service_code);
      fetch C_GET_MTR_DESC into O_mtr_service_code_desc;
     
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_MTR_DESC',
                       'L10N_BR_MASTERSAF_SVC_CODES',
                       'MASTERSAF_SERVICE_CODE: ' || I_mtr_service_code);
      close C_GET_MTR_DESC;
     
      if O_mtr_service_code_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_MTR_SRVC_CODE');
         return FALSE;
      end if;
   end if;

   if I_fed_service_code is NOT NULL then
      -- get federal service code desc
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FED_DESC',
                       'L10N_BR_FEDERAL_SVC_CODES',
                       'FEDERAL_SERVICE_CODE: ' || I_fed_service_code);
      open C_GET_FED_DESC;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_FED_DESC',
                       'L10N_BR_FEDERAL_SVC_CODES',
                       'FEDERAL_SERVICE_CODE: ' || I_fed_service_code);
      fetch C_GET_FED_DESC into O_fed_service_code_desc;
     
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_FED_DESC',
                       'L10N_BR_FEDERAL_SVC_CODES',
                       'FEDERAL_SERVICE_CODE: ' || I_fed_service_code);
      close C_GET_FED_DESC;

      if O_fed_service_code_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_FED_SRVC_CODE');
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SERVICE_CODE_DESC;
-------------------------------------------------------------------------------------
FUNCTION GET_NCM_ATTR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_ncm_char_code   IN OUT   L10N_BR_NCM_CHAR_CODES.NCM_CHAR_CODE%TYPE,
                      O_ipi_code        IN OUT   L10N_BR_NCM_IPI_CODES.IPI_CODE%TYPE,
                      O_pauta_code      IN OUT   L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_CODE%TYPE,
                      I_ncm_code        IN       L10N_BR_NCM_CHAR_CODES.NCM_CODE%TYPE,
                      I_country_id      IN       L10N_BR_NCM_CHAR_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'L10N_BR_ITEM_SQL.GET_NCM_ATTR';
   
   cursor C_GET_CHAR is
      select ncm_char_code
        from l10n_br_ncm_char_codes
       where ncm_code = I_ncm_code
         and country_id = I_country_id;

   cursor C_GET_IPI is
      select ipi_code
        from l10n_br_ncm_ipi_codes
       where ncm_code = I_ncm_code
         and country_id = I_country_id;

   cursor C_GET_PAUTA is
      select ncm_pauta_code
        from l10n_br_ncm_pauta_codes
       where ncm_code = I_ncm_code
         and country_id = I_country_id;

BEGIN
   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_COUNTRY_ID',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_ncm_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_NCM_CODE',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_CHAR;
   if C_GET_CHAR%ROWCOUNT = 1 then
      fetch C_GET_CHAR into O_ncm_char_code;
   else
      O_ncm_char_code := NULL;
   end if;
   close C_GET_CHAR;
   ---
   open C_GET_IPI;
   
   if C_GET_IPI%ROWCOUNT = 1 then
      fetch C_GET_IPI into O_ipi_code;
   else
      O_ipi_code := NULL;
   end if;
   
   close C_GET_IPI;
   ---
   open C_GET_PAUTA;
   
   if C_GET_PAUTA%ROWCOUNT = 1 then
      fetch C_GET_PAUTA into O_pauta_code;
   else
      O_pauta_code := NULL;
   end if;
   
   close C_GET_PAUTA;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NCM_ATTR;
-------------------------------------------------------------------------------------
FUNCTION GET_NCM_ATTR_DESC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_ncm_char_desc    IN OUT   L10N_BR_NCM_CHAR_CODES.NCM_CHAR_DESC%TYPE,
                           O_ipi_desc         IN OUT   L10N_BR_NCM_IPI_CODES.IPI_DESC%TYPE,
                           O_ncm_pauta_desc   IN OUT   L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_DESC%TYPE,
                           I_ncm_char_code    IN       L10N_BR_NCM_CHAR_CODES.NCM_CHAR_CODE%TYPE,
                           I_ipi_code         IN       L10N_BR_NCM_IPI_CODES.IPI_CODE%TYPE,
                           I_pauta_code       IN       L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_CODE%TYPE,
                           I_ncm_code         IN       L10N_BR_NCM_CHAR_CODES.NCM_CODE%TYPE,
                           I_country_id       IN       L10N_BR_NCM_CHAR_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'L10N_BR_ITEM_SQL.GET_NCM_ATTR_DESC';
   
   cursor C_GET_NCM_CHAR_DESC is
      select ncm_char_desc
        from l10n_br_ncm_char_codes
       where ncm_char_code = I_ncm_char_code
         and ncm_code = I_ncm_code
         and country_id = I_country_id;

   cursor C_GET_IPI_DESC is
      select ipi_desc
        from l10n_br_ncm_ipi_codes
       where ipi_code = I_ipi_code
         and ncm_code = I_ncm_code
         and country_id = I_country_id;
         
   cursor C_GET_PAUTA_DESC is
      select ncm_pauta_desc
        from l10n_br_ncm_pauta_codes
       where ncm_char_code = I_ncm_char_code
         and ncm_pauta_code = I_pauta_code
         and ncm_code = I_ncm_code
         and country_id = I_country_id;

BEGIN

   if I_ncm_char_code is NOT NULL and I_country_id is NOT NULL then
      open C_GET_NCM_CHAR_DESC;
      fetch C_GET_NCM_CHAR_DESC into O_ncm_char_desc;
      close C_GET_NCM_CHAR_DESC;

      if O_ncm_char_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_NCMCHAR_CODE',
                                                I_ncm_char_code,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
   end if;
   ---
   if I_ipi_code is NOT NULL and I_country_id is NOT NULL then
      open C_GET_IPI_DESC;
      fetch C_GET_IPI_DESC into O_ipi_desc;
      close C_GET_IPI_DESC;

      if O_ipi_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_IPI_CODE',
                                                I_ipi_code,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
   end if;
   ---
   if I_pauta_code is NOT NULL and I_country_id is NOT NULL then
      open C_GET_PAUTA_DESC;
      fetch C_GET_PAUTA_DESC into O_ncm_pauta_desc;
      close C_GET_PAUTA_DESC;

      if O_ncm_pauta_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PAUTA_CODE',
                                                I_pauta_code,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NCM_ATTR_DESC;
-------------------------------------------------------------------------------------
FUNCTION FRECLASS_ITEM_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_item            IN       L10N_BR_FISCAL_RECLASS.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'L10N_BR_ITEM_SQL.FRECLASS_ITEM_EXISTS';
   L_exists    VARCHAR2(1) := NULL;

   cursor C_FRECLASS_ITEM_EXISTS is
      select 'x'
        from l10n_br_fiscal_reclass
       where item = I_item
         and rownum = 1;

BEGIN

   open C_FRECLASS_ITEM_EXISTS;
   fetch C_FRECLASS_ITEM_EXISTS into L_exists;
   close C_FRECLASS_ITEM_EXISTS;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END FRECLASS_ITEM_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION GET_OLD_VALUE(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item                IN     ITEM_MASTER.ITEM%TYPE,
                       I_name                IN     L10N_BR_FISCAL_RECLASS_NV.NAME%TYPE,
                       O_old_value           OUT    L10N_BR_FISCAL_RECLASS_NV.OLD_VALUE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(62) := 'L10N_BR_ITEM_SQL.GET_OLD_VALUE';
   L_select_string   VARCHAR2(4000); 

BEGIN

   L_select_string := ' select '|| I_name || ' from V_BR_ITEM_COUNTRY_ATTRIBUTES where item = ' || '''' || I_item || '''';
   EXECUTE IMMEDIATE L_select_string into  O_old_value;
    
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;    
 
END GET_OLD_VALUE;
------------------------------------------------------------------------------------------
FUNCTION DELETE_FRECLASS_CUSTOM_ATTRIB(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_reclass_id          IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE,
                                       I_name                IN     L10N_BR_FISCAL_RECLASS_NV.NAME%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
 
   L_program  VARCHAR2(62) := 'L10N_BR_ITEM_SQL.DELETE_FRECLASS_CUSTOM_ATTRIB';
   
   cursor C_LOCK_L10N_RECLASS_NV is
      select 'x'
        from l10n_br_fiscal_reclass_nv
       where reclassification_id = I_reclass_id
         and name = NVL(I_name,name)
         for update nowait;
      
BEGIN
   
   open C_LOCK_L10N_RECLASS_NV;
   close C_LOCK_L10N_RECLASS_NV;
   
   delete from l10n_br_fiscal_reclass_nv    
    where reclassification_id = I_reclass_id
      and name = NVL(I_name,name);
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;         
     
END DELETE_FRECLASS_CUSTOM_ATTRIB ;
------------------------------------------------------------------------------------------------
FUNCTION CUSTOM_ATTRIB_EXISTS(O_error_message        IN OUT VARCHAR2,
                              O_exists               IN OUT BOOLEAN,
                              I_reclassification_id  IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE,
                              I_attrib_name          IN     L10N_BR_FISCAL_RECLASS_NV.NAME%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_BR_ITEM_SQL.CUSTOM_ATTRIB_EXISTS';
   L_dummy   VARCHAR2(1)  := NULL;


   cursor C_EXISTS is
      select 'x'
        from l10n_br_fiscal_reclass_nv
       where reclassification_id = I_reclassification_id
         and name                = I_attrib_name
         and rownum              = 1 ;

BEGIN
   O_exists := FALSE;

   if I_reclassification_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_reclassification_id,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_attrib_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_attrib_name,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_EXISTS;
   fetch C_EXISTS into L_dummy;
   close C_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CUSTOM_ATTRIB_EXISTS;
------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQ_CUSTOM_ATTRIB(O_error_message        IN OUT VARCHAR2,
                                 O_exists               IN OUT BOOLEAN,
                                 I_reclassification_id  IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE,
                                 I_base_rms_table       IN     EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_BR_ITEM_SQL.CHECK_REQ_CUSTOM_ATTRIB';
   L_dummy   VARCHAR2(1)  := NULL;


   cursor C_EXISTS is
      select 'x'
        from l10n_attrib att,
             l10n_attrib_group atg ,
             ext_entity ext,
             l10n_br_fiscal_reclass_nv nv 
       where ext.base_rms_table = I_base_rms_table
         and atg.ext_entity_id = ext.ext_entity_id
         and att.group_id = atg.group_id
         and atg.base_ind = 'N'
         and att.value_req = 'Y'
         and nv.name = att.view_col_name
         and nv.reclassification_id = I_reclassification_id
         and nv.new_value is null
         and rownum = 1;

BEGIN

   O_exists := FALSE;

   if I_reclassification_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_reclassification_id,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_EXISTS;
   fetch C_EXISTS into L_dummy;
   close C_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_REQ_CUSTOM_ATTRIB;
------------------------------------------------------------------------------------------
FUNCTION DELETE_FRECLASS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_reclass_id          IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE)
RETURN BOOLEAN IS
 
   L_program  VARCHAR2(62) := 'L10N_BR_ITEM_SQL.DELETE_FRECLASS';
   
   cursor C_LOCK_L10N_BR_FRECLASS_NV is
      select 'x'
        from l10n_br_fiscal_reclass_nv
       where reclassification_id = I_reclass_id
         for update nowait;
   
   cursor C_LOCK_L10N_BR_FISCAL_RECLASS is
      select 'x'
        from l10n_br_fiscal_reclass
       where reclassification_id = I_reclass_id
         for update nowait;   
 
BEGIN
   
   open C_LOCK_L10N_BR_FRECLASS_NV;
   close C_LOCK_L10N_BR_FRECLASS_NV;
   
   delete from l10n_br_fiscal_reclass_nv    
      where reclassification_id = I_reclass_id;

   open C_LOCK_L10N_BR_FISCAL_RECLASS;
   close C_LOCK_L10N_BR_FISCAL_RECLASS;
   
   delete from l10n_br_fiscal_reclass  
    where reclassification_id = I_reclass_id;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;         
     

END DELETE_FRECLASS;
------------------------------------------------------------------------------------------------
FUNCTION CHECK_FRECLASS_EXISTS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists              IN OUT BOOLEAN,
                               I_reclass_id          IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_BR_ITEM_SQL.CHECK_FRECLASS_EXISTS';
   L_dummy   VARCHAR2(1)  := NULL;


   cursor C_EXISTS is
      select 'x'
        from l10n_br_fiscal_reclass 
       where reclassification_id = I_reclass_id;

BEGIN
   O_exists := FALSE;

   if I_reclass_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_reclass_id,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_EXISTS;
   fetch C_EXISTS into L_dummy;
   close C_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_FRECLASS_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION MERGE_FRECLASS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_return_field_tbl    IN     L10N_FLEX_ATTRIB_VAL_SQL.TBL_attrib_val_tbl,
                        I_reclass_id          IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE,
                        I_item                IN     L10N_BR_FISCAL_RECLASS.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64) := 'L10N_BR_ITEM_SQL.MERGE_FRECLASS';
   L_old_value   L10N_BR_FISCAL_RECLASS_NV.OLD_VALUE%TYPE;
   L_exists      BOOLEAN;

BEGIN
   if I_reclass_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_reclass_id,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_return_field_tbl.count > 0 then
      for i in I_return_field_tbl.FIRST .. I_return_field_tbl.LAST loop
         if L10N_BR_ITEM_SQL.CUSTOM_ATTRIB_EXISTS(O_error_message,
      	                                      L_exists,
  	                                            I_reclass_id,
  	                                            I_return_field_tbl(i).view_col_name) = FALSE THEN
            return FALSE;
         end if; 	    	                                                    
         if L_exists then
            update L10N_BR_FISCAL_RECLASS_NV
               set new_value = I_return_field_tbl(i).field_value 
             where name = I_return_field_tbl(i).view_col_name
               and reclassification_id = I_reclass_id;
         else
            if L10N_BR_ITEM_SQL.GET_OLD_VALUE(O_error_message,
                                              I_item,
                                              I_return_field_tbl(i).view_col_name,
                                              L_old_value) = false then
               return FALSE;                                     
            end if;
            --
            insert into L10N_BR_FISCAL_RECLASS_NV
               (reclassification_id,
                name,
                old_value,
                new_value) 
            values 
               (I_reclass_id,
                I_return_field_tbl(i).view_col_name,
                L_old_value,
                I_return_field_tbl(i).field_value);
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MERGE_FRECLASS;
---------------------------------------------------------------------------------------------
END L10N_BR_ITEM_SQL;
/

