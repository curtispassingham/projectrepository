CREATE OR REPLACE PACKAGE BODY FM_RECEIVING_CONSUME_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind       IN OUT   BOOLEAN,
                 I_schedule_no     IN       FM_RECEIVING_HEADER.RECV_NO%TYPE)

RETURN BOOLEAN is

   L_program         VARCHAR2(100) := 'FM_RECEIVING_CONSUME_SQL.CONSUME';
   
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   L_status_code       VARCHAR2(1);
   L_message_type      VARCHAR(20) := 'ReceiptCre';
   L_rib_otbdesc_rec   "RIB_OTBDesc_REC";
   L_rib_error_tbl     RIB_ERROR_TBL;
   L_error_ind         BOOLEAN;
   L_rms_upd_err_ind   FM_FISCAL_DOC_HEADER.RMS_UPD_ERR_IND%TYPE;

   TYPE "RIB_ReceiptDesc_TBL" IS TABLE OF "RIB_ReceiptDesc_REC";

   L_receivcre_message_tbl   "RIB_ReceiptDesc_TBL"   := NULL;

   cursor C_GET_RCPT is
      select "RIB_ReceiptDesc_REC" (0,  -- rib_oid,
                                    fh.recv_no,    -- schedule_nbr
                                    fh.appt_nbr,   -- appt nbr
                                    CAST(MULTISET(select "RIB_Receipt_REC"(0,                   -- rib_oid
                                                                           fh.location_id,      -- dc_dest_id
                                                                           fh.requisition_no,   -- po_nbr
                                                                           fh.document_type,    -- document_type
                                                                           fh.fiscal_doc_id,    -- ref_doc_no
                                                                           fh.asn_nbr,          -- asn_nbr
                                                                           CAST(MULTISET(select "RIB_ReceiptDtl_REC"(0,                        -- rib_oid,
                                                                                                                     frd.item,                 -- item_id
                                                                                                                     frd.quantity,             -- unit_qty
                                                                                                                     frd.receipt_xactn_type,   -- receipt_xactn_type
                                                                                                                     frd.receipt_date,         -- receipt_date
                                                                                                                     frd.receipt_nbr,          -- receipt_nbr
                                                                                                                     fh.location_id,           -- dest_id
                                                                                                                     frd.container_id,         -- container_id
                                                                                                                     frd.requisition_no,       -- distro_nbr
                                                                                                                     fh.document_type,         -- distro_doc_type
                                                                                                                     frd.to_disposition,       -- to_disposition
                                                                                                                     frd.from_disposition,     -- from_disposition
                                                                                                                     frd.to_wip,               -- to_wip
                                                                                                                     frd.from_wip,             -- from_wip
                                                                                                                     frd.to_trouble,           -- to_trouble
                                                                                                                     frd.from_trouble,         -- from_trouble
                                                                                                                     NULL,                     -- user_id
                                                                                                                     frd.dummy_carton_ind,     -- dummy_carton_ind
                                                                                                                     frd.tampered_carton_ind,  -- tampered_carton_ind
                                                                                                                     frd.ebc_cost,             -- unit_cost
                                                                                                                     frd.quantity,             -- shipped_qty
                                                                                                                     frd.weight,               -- weight
                                                                                                                     frd.weight_uom,           -- weight_uom
                                                                                                                     frd.nic_cost)             -- gross_cost
                                                                                           from fm_receiving_detail frd,
                                                                                                fm_receiving_header frh
                                                                                          where frd.header_seq_no = frh.seq_no
                                                                                            and frd.requisition_no = fh.requisition_no
                                                                                            and frh.recv_no = fh.recv_no
                                                                                            and frh.fiscal_doc_id = fh.fiscal_doc_id
                                                                                            and (frd.quantity is NOT NULL and frd.quantity > 0)) as "RIB_ReceiptDtl_TBL"),  -- ReceiptDtl_TBL
                                                                           NULL,                -- ReceiptCartonDtl_TBL
                                                                           fh.receipt_type,     -- receipt_type
                                                                           fh.from_loc,         -- from_loc
                                                                           fh.from_loc_type)    -- from_loc_type
                                                    from dual) as "RIB_Receipt_TBL"), -- Receipt_TBL
                                    NULL) -- ReceiptOverage_TBL
        from (select DISTINCT frd.requisition_no,
                              frh.fiscal_doc_id,
                              frh.location_id,
                              frh.document_type,
                              frd.asn_nbr,
                              frh.recv_no,
                              frh.appt_nbr,
                              frd.receipt_type,
                              frd.from_loc,
                              frd.from_loc_type
                         from fm_receiving_detail frd,
                              fm_receiving_header frh
                        where frd.header_seq_no = frh.seq_no
                          and frh.status in ('P', 'H')
                          and frd.rms_upd_status = 'R'                          
                          and (frd.quantity is NOT NULL and frd.quantity > 0 )
                          and frh.recv_no= I_schedule_no) fh;

   cursor C_GET_RMS_UPD_ERR_IND(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select rms_upd_err_ind
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = P_fiscal_doc_id;
       
   cursor C_LOCK_RECV_UPDATE is
      select 'X'
        from fm_receiving_detail frd, fm_receiving_header frh
       where frh.seq_no = frd.header_seq_no
         and frh.status in ('P', 'H')
         and frd.rms_upd_status = 'R'                          
         and (frd.quantity is NOT NULL and frd.quantity > 0 )
         and frh.recv_no= I_schedule_no          
         for update nowait;

BEGIN

   -- Populate Header
   open C_GET_RCPT;
   fetch C_GET_RCPT BULK COLLECT into L_receivcre_message_tbl;
   close C_GET_RCPT;

   if L_receivcre_message_tbl is NOT NULL and L_receivcre_message_tbl.COUNT > 0 then
      FOR a in L_receivcre_message_tbl.FIRST..L_receivcre_message_tbl.LAST LOOP

         L_status_code   := NULL;
         O_error_message := NULL;
         -- Call procedure Consume

         RMSSUB_RECEIVING.CONSUME_RECEIPT(L_status_code,
                                          O_error_message,
                                          L_receivcre_message_tbl(a),
                                          L_message_type,
                                          'N',
                                          L_rib_otbdesc_rec,
                                          L_rib_error_tbl);

        if L_status_code = API_CODES.UNHANDLED_ERROR or
           O_error_message is NOT NULL or
           O_error_ind = TRUE then
           O_error_ind := TRUE;
        else
           O_error_ind := FALSE;
        end if;

        if L_receivcre_message_tbl(a).Receipt_TBL(1).document_type = 'P' then
         -- Call function to consume OTB info
           RMSSUB_OTBMOD.CONSUME(L_status_code,
                                 O_error_message,
                                 L_rib_otbdesc_rec,
                                 NULL);

           if O_error_ind = FALSE then
              if L_status_code = API_CODES.UNHANDLED_ERROR or
                 O_error_message is NOT NULL then
                 O_error_ind := TRUE;
              end if;
           end if;
        end if;

        --check for rms update err
        if O_error_ind = TRUE then
           if FM_FISCAL_DOC_HEADER_SQL.UPDATE_RMS_ERR_STATUS(O_error_message,
                                                             L_receivcre_message_tbl(a).Receipt_TBL(1).ref_doc_no,
                                                             'Y') = FALSE then
              return FALSE;
           end if;

           if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                           L_receivcre_message_tbl(a).Receipt_TBL(1).ref_doc_no,
                                           L_program,
                                           L_program,
                                           'E',
                                           'RMS_UPDATION_ERROR',
                                            O_error_message,
                                           'Fiscal_doc_id: '||
                                           TO_CHAR(L_receivcre_message_tbl(a).Receipt_TBL(1).ref_doc_no)) = FALSE then
                return FALSE;
           end if;

           return FALSE;
        else
           open C_GET_RMS_UPD_ERR_IND(L_receivcre_message_tbl(a).Receipt_TBL(1).ref_doc_no);
           fetch C_GET_RMS_UPD_ERR_IND into L_rms_upd_err_ind;
           close C_GET_RMS_UPD_ERR_IND;
           --
           if L_rms_upd_err_ind = 'Y' then
              if FM_FISCAL_DOC_HEADER_SQL.UPDATE_RMS_ERR_STATUS(O_error_message,
                                                                L_receivcre_message_tbl(a).Receipt_TBL(1).ref_doc_no) = FALSE then
                 return FALSE;
              end if;
           end if;
           open C_LOCK_RECV_UPDATE;
           close C_LOCK_RECV_UPDATE;
           update (select frd.rms_upd_status
                     from fm_receiving_detail frd, fm_receiving_header frh
                    where frh.seq_no = frd.header_seq_no
                      and frh.status in ('P', 'H')
                      and frd.rms_upd_status = 'R'                          
                      and (frd.quantity is NOT NULL and frd.quantity > 0 )
                      and frh.recv_no= I_schedule_no) recv_table
              set recv_table.rms_upd_status = 'P';                                            
        end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_RECEIVING_HEADER, FM_RECEIVING_DETAIL',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_RECV_UPDATE%ISOPEN then
         close C_LOCK_RECV_UPDATE;
      end if;
      ---
      return FALSE;
      
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_RECV_UPDATE%ISOPEN then
         close C_LOCK_RECV_UPDATE;
      end if;
      ---
      return FALSE;
END CONSUME;
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_transaction_type   IN       FM_RECEIVING_HEADER.REQUISITION_TYPE%TYPE,
                              I_thread_no          IN       NUMBER,
                              I_num_threads        IN       NUMBER)
RETURN BOOLEAN IS

   L_program               VARCHAR2(100)      := 'FM_RECEIVING_CONSUME_SQL.BATCH_CONSUME_THREAD';
   L_table                 VARCHAR2(50)       := NULL;
   L_rowid_tbl             ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_dh_tbl          ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_sch_tbl         ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_recv_no_tbl           NUMBER_TBL         := NUMBER_TBL();
   L_fd_id_tbl             NUMBER_TBL         := NUMBER_TBL();
   L_error_ind             BOOLEAN;
   L_compl_fiscal_doc_id   FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   L_yes                   VARCHAR2(1) := 'Y';
   L_triangulation         ORDHEAD.TRIANGULATION_IND%TYPE;
   L_schedule              FM_SCHEDULE.SCHEDULE_NO%TYPE := 0;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   -- Staging Records
   cursor C_GET_STG_RCPT is
      select recv_no,
             fiscal_doc_id
        from fm_receiving_header
       where ((I_transaction_type = 'PO' and requisition_type = 'PO') or
              (I_transaction_type = 'TSF' and requisition_type in ('TSF', 'IC', 'REP')))
         and status = 'H'
         and mod(location_id, I_num_threads )+1 = I_thread_no
       order by last_update_datetime;

   -- Locking cursors
   cursor C_LOCK_STG_RCPT is
      select ROWIDTOCHAR(rowid)
        from fm_receiving_header
       where ((I_transaction_type = 'PO' and requisition_type = 'PO') or
              (I_transaction_type = 'TSF' and requisition_type in ('TSF', 'IC', 'REP')))
         and status = 'H'
         and mod(location_id, I_num_threads )+1 = I_thread_no
       order by last_update_datetime
         for update nowait;

   cursor C_LOCK_FISCAL_DOC_HEADER is
      select ROWIDTOCHAR(rowid),dh.fiscal_doc_id
        from fm_fiscal_doc_header dh
       where dh.status = 'H'
         and exists (select 'x'
                       from TABLE(CAST(L_fd_id_tbl as NUMBER_TBL)) fd
                      where value(fd) = dh.fiscal_doc_id
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_SCHEDULE is
      select ROWIDTOCHAR(rowid)
        from fm_schedule s
       where s.status = 'H'
         and not exists (select 'x'
                           from fm_fiscal_doc_header dh
                          where dh.status != 'A'
                            and dh.schedule_no = s.schedule_no
                            and exists (select 'x'
                                          from TABLE(CAST(L_fd_id_tbl as NUMBER_TBL)) fd
                                         where value(fd) = dh.fiscal_doc_id
                                           and rownum = 1)
                            and rownum = 1)
         for update nowait;

    -- Cost Adjustment
    cursor C_GET_REQUISITION_NO is
      SELECT DISTINCT requisition_no
        FROM fm_fiscal_doc_detail dh
       WHERE exists (select 'x'
                       from TABLE(CAST(L_fd_id_tbl as NUMBER_TBL)) fd
                      where value(fd) = dh.fiscal_doc_id);

    --Getting the Triangulation indication and complement fiscal_doc_id
     cursor C_CHECK_FOR_TRIAG(P_fiscal_doc_id in fm_fiscal_doc_header.fiscal_doc_id%type) is
        select fdc.compl_fiscal_doc_id,ord.triangulation_ind
          from fm_fiscal_doc_header fdh
              ,fm_fiscal_doc_detail fdd
              ,fm_fiscal_doc_complement fdc
              ,ordhead ord
         where fdh.fiscal_doc_id     = fdd.fiscal_doc_id
           and fdh.fiscal_doc_id     = fdc.fiscal_doc_id
           and fdd.requisition_no    = ord.order_no
           and ord.triangulation_ind = L_yes
           and fdh.fiscal_doc_id     = P_fiscal_doc_id
       UNION
        select fdc.fiscal_doc_id,ord.triangulation_ind
          from fm_fiscal_doc_header fdh
              ,fm_fiscal_doc_detail fdd
              ,fm_fiscal_doc_complement fdc
              ,ordhead ord
         where fdh.fiscal_doc_id     = fdd.fiscal_doc_id
           and fdh.fiscal_doc_id     = fdc.compl_fiscal_doc_id
           and fdd.requisition_no    = ord.order_no
           and ord.triangulation_ind = L_yes
           and fdh.fiscal_doc_id     = P_fiscal_doc_id  ;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STG_RCPT',
                    'fm_receiving_header',
                    NULL);
   open C_GET_STG_RCPT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STG_RCPT',
                    'fm_receiving_header',
                    NULL);
   fetch C_GET_STG_RCPT BULK COLLECT into L_recv_no_tbl,
                                          L_fd_id_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STG_RCPT',
                    'fm_receiving_header',
                    NULL);
   close C_GET_STG_RCPT;

   if L_recv_no_tbl is NULL or L_recv_no_tbl.COUNT <= 0 then
      return TRUE;
   end if;

   -- Loop thru all the sequence numbers
   FOR rec in L_recv_no_tbl.FIRST..L_recv_no_tbl.LAST LOOP
      if L_schedule != L_recv_no_tbl(rec) then
         if FM_RECEIVING_CONSUME_SQL.CONSUME(O_error_message,
                                             L_error_ind,
                                             L_recv_no_tbl(rec)) = FALSE then
            return FALSE;
         end if;
      end if;

      L_schedule := L_recv_no_tbl(rec);
   END LOOP;

   -- Update FM_RECEIVING_HEADER status to 'A'
   L_table := 'FM_RECEIVING_HEADER';
   open C_LOCK_STG_RCPT;
   fetch C_LOCK_STG_RCPT BULK COLLECT into L_rowid_tbl;
   close C_LOCK_STG_RCPT;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'fm_receiving_header',
                    NULL);
   update fm_receiving_header sr
      set sr.status = 'P'
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = sr.rowid);

   -- Update status of fiscal doc to 'A'pproved
   L_table := 'FM_FISCAL_DOC_HEADER';
   open C_LOCK_FISCAL_DOC_HEADER;
   fetch C_LOCK_FISCAL_DOC_HEADER BULK COLLECT into L_rowid_dh_tbl,L_fd_id_tbl;
   close C_LOCK_FISCAL_DOC_HEADER;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'fm_fiscal_doc_header',
                    NULL);
   update fm_fiscal_doc_header dh
      set dh.status = 'A'
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_dh_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = dh.rowid);

    if L_fd_id_tbl is NOT NULL and L_fd_id_tbl.COUNT > 0 then
       FOR rec in L_fd_id_tbl.FIRST..L_fd_id_tbl.LAST LOOP
          open C_CHECK_FOR_TRIAG(L_fd_id_tbl(rec));
          fetch C_CHECK_FOR_TRIAG into L_compl_fiscal_doc_id,L_triangulation;
          close C_CHECK_FOR_TRIAG;
          if NVL(L_triangulation,'N') = 'Y' then
             update fm_fiscal_doc_header dh
                set dh.status = 'A'
              where fiscal_doc_id = L_compl_fiscal_doc_id;
          end if;
       END LOOP;
    end if;

   -- Cost Adjustment for Receipt
   FOR rec_requisition_no in C_GET_REQUISITION_NO LOOP
      if REC_COST_ADJ_SQL.FM_RECEIPT_MATCHED(O_error_message,
                                             rec_requisition_no.requisition_no,
                                             NULL) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   -- Update status of schedule to 'A'pproved
   L_table := 'FM_SCHEDULE';
   open C_LOCK_SCHEDULE;
   fetch C_LOCK_SCHEDULE BULK COLLECT into L_rowid_sch_tbl;
   close C_LOCK_SCHEDULE;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'fm_schedule',
                    NULL);
   update fm_schedule s
      set s.status = 'A'
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_sch_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = s.rowid);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            SQLERRM,
                                            L_table,
                                            TO_CHAR(SQLCODE));

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BATCH_CONSUME_THREAD;
---------------------------------------------------------------------------------------------
END FM_RECEIVING_CONSUME_SQL;
/