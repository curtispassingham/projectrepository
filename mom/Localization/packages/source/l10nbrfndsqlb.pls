CREATE OR REPLACE PACKAGE BODY L10N_BR_FND_SQL AS
---------------------------------------------------------------------------------------------------------
FUNCTION TRIB_SUBS_EXISTS(O_error_message IN OUT VARCHAR2,
                          O_exists        IN OUT BOOLEAN,
                          I_entity        IN     L10N_BR_ENTITY_TRIB_SUBS.ENTITY%TYPE,
                          I_entity_type   IN     L10N_BR_ENTITY_TRIB_SUBS.ENTITY_TYPE%TYPE,
                          I_country       IN     L10N_BR_ENTITY_TRIB_SUBS.COUNTRY_ID%TYPE,
                          I_state         IN     L10N_BR_ENTITY_TRIB_SUBS.STATE%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_BR_FND_SQL.TRIB_SUBS_EXISTS';
   L_dummy   VARCHAR2(1)  := NULL;
   L_key     VARCHAR2(100) := 'entity: ' || I_entity ||
                              ',entity_type: ' || I_entity_type ||
                              ',country: ' || I_country ||
                              ',state: ' || I_state;

   cursor C_EXISTS is
      select 'x'
        from l10n_br_entity_trib_subs
       where entity      = I_entity
         and entity_type = I_entity_type
         and country_id  = I_country
         and state       = I_state;

BEGIN
   O_exists := FALSE;

   if I_entity is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_entity,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_entity_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_entity_type,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_country,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_state is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_state,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'l10n_br_entity_trib_subs',
                    L_key);
   open C_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'l10n_br_entity_trib_subs',
                    L_key);
   fetch C_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    'l10n_br_entity_trib_subs',
                    L_key);
   close C_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END TRIB_SUBS_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION TRIB_SUBS_EXISTS(O_error_message IN OUT VARCHAR2,
                          O_exists        IN OUT BOOLEAN,
                          I_entity        IN     L10N_BR_ENTITY_TRIB_SUBS.ENTITY%TYPE,
                          I_entity_type   IN     L10N_BR_ENTITY_TRIB_SUBS.ENTITY_TYPE%TYPE,
                          I_country       IN     L10N_BR_ENTITY_TRIB_SUBS.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_BR_FND_SQL.TRIB_SUBS_EXISTS';
   L_dummy   VARCHAR2(1)  := NULL;


   cursor C_EXISTS is
      select 'x'
        from l10n_br_entity_trib_subs
       where entity      = to_char(I_entity)
         and entity_type = I_entity_type
         and country_id  = I_country
         and rownum = 1 ;

BEGIN
   O_exists := FALSE;

   if I_entity is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_entity,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_entity_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_entity_type,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_country,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;


   open C_EXISTS;
   fetch C_EXISTS into L_dummy;
   close C_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END TRIB_SUBS_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION CNAE_CODES_EXISTS(O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           I_entity        IN     L10N_BR_ENTITY_CNAE_CODES.KEY_VALUE_1%TYPE,
                           I_entity_type   IN     L10N_BR_ENTITY_CNAE_CODES.KEY_VALUE_2%TYPE,
                           I_module        IN     L10N_BR_ENTITY_CNAE_CODES.MODULE%TYPE,
                           I_country       IN     L10N_BR_ENTITY_CNAE_CODES.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_BR_FND_SQL.CNAE_CODES_EXISTS';
   L_dummy   VARCHAR2(1)  := NULL;


   cursor C_EXISTS is
      select 'x'
        from l10n_br_entity_cnae_codes
       where key_value_1      = to_char(I_entity)
         and key_value_2      = I_entity_type
         and module           = I_module
         and country_id       = I_country
         and rownum           = 1 ;

BEGIN
   O_exists := FALSE;

   if I_entity is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_entity,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_entity_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_entity_type,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_module,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_country,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;


   open C_EXISTS;
   fetch C_EXISTS into L_dummy;
   close C_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CNAE_CODES_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION FISCAL_ATTRIB_EXISTS(O_error_message   IN OUT   VARCHAR2,
                              IO_l10n_obj       IN OUT   L10N_OBJ)
   return BOOLEAN is

   L_program   VARCHAR2(64) := 'L10N_BR_FND_SQL.FISCAL_ATTRIB_EXISTS';
   L_exists    VARCHAR2(1)  := 'N';

   --Declaring an object of the subtype. No need to initialize the object using the constructor function.
   L_l10n_obj_sub   L10N_EXISTS_REC;

   cursor C_FISCAL_TSFENTITY is
      select 'Y'
        from tsf_entity_l10n_ext
       where tsf_entity_id = L_l10n_obj_sub.source_id
         and l10n_country_id = L_l10n_obj_sub.country_id;

   cursor C_FISCAL_SOB is
      select 'Y'
        from fif_gl_setup_l10n_ext
       where set_of_books_id = L_l10n_obj_sub.source_id
         and l10n_country_id = L_l10n_obj_sub.country_id;

   cursor C_FISCAL_STORE_WH is
      select 'Y'
        from store_l10n_ext
       where store = L_l10n_obj_sub.source_id
         and (l10n_country_id = L_l10n_obj_sub.country_id or L_l10n_obj_sub.country_id is NULL)
       UNION ALL
      select 'Y'
        from store_add_l10n_ext
       where store = L_l10n_obj_sub.source_id
         and (l10n_country_id = L_l10n_obj_sub.country_id or L_l10n_obj_sub.country_id is NULL)
       UNION ALL
      select 'Y'
        from wh_l10n_ext
       where wh = L_l10n_obj_sub.source_id
         and (l10n_country_id = L_l10n_obj_sub.country_id or L_l10n_obj_sub.country_id is NULL);

   cursor C_FISCAL_SUPS is
      select 'Y'
        from sups_l10n_ext
       where supplier = L_l10n_obj_sub.source_id
         and (l10n_country_id = L_l10n_obj_sub.country_id or L_l10n_obj_sub.country_id is NULL);

   cursor C_FISCAL_OUTLOC is
      select 'Y'
        from outloc_l10n_ext
       where outloc_id = L_l10n_obj_sub.source_id
         and outloc_type = L_l10n_obj_sub.source_type
         and l10n_country_id = L_l10n_obj_sub.country_id;

   cursor C_FISCAL_PARTNER is
      select 'Y'
        from partner_l10n_ext
       where partner_id = L_l10n_obj_sub.source_id
         and partner_type = L_l10n_obj_sub.source_type
         and (l10n_country_id = L_l10n_obj_sub.country_id or L_l10n_obj_sub.country_id is NULL);

BEGIN

   -- IO_l10n_obj to subtype object, so that IO_l10n_obj.exists_ind can be used in the program
   L_l10n_obj_sub := treat(IO_l10n_obj AS L10N_EXISTS_REC);

   if L_l10n_obj_sub.source_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_key_value_1',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if L_l10n_obj_sub.source_entity = 'TSFE' then

      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_TSFENTITY',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      OPEN c_fiscal_tsfentity;

      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_TSFENTITY',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      fetch c_fiscal_tsfentity
       into L_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_TSFENTITY',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      close c_fiscal_tsfentity;
   elsif L_l10n_obj_sub.source_entity = 'SOB' then

      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_SOB',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      OPEN c_fiscal_sob;

      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_SOB',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      fetch c_fiscal_sob
       into L_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_SOB',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      close c_fiscal_sob;

   elsif L_l10n_obj_sub.source_entity = 'LOC' then

      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_STORE_WH',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      open C_FISCAL_STORE_WH;

      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_STORE_WH',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      fetch C_FISCAL_STORE_WH
       into L_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_STORE_WH',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      close C_FISCAL_STORE_WH;

   elsif L_l10n_obj_sub.source_entity = 'SUPP' then

      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_SUPS',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      open C_FISCAL_SUPS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_SUPS',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      fetch C_FISCAL_SUPS
       into L_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_SUPS',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      close C_FISCAL_SUPS;

   elsif L_l10n_obj_sub.source_entity = 'OLOC' then

      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_OUTLOC',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      open C_FISCAL_OUTLOC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_OUTLOC',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      fetch C_FISCAL_OUTLOC
       into L_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_OUTLOC',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      close C_FISCAL_OUTLOC;

   elsif L_l10n_obj_sub.source_entity = 'PTNR' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_PARTNER',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      open C_FISCAL_PARTNER;

      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_PARTNER',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      fetch C_FISCAL_PARTNER
       into L_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_PARTNER',
                       'l10n_br_fnd_sql',
                       'EntityType: ' || L_l10n_obj_sub.source_entity || ', source_id: ' ||
                       L_l10n_obj_sub.source_id);
      close C_FISCAL_PARTNER;

   end if;

   --Assign the out variable to L_l10n_obj_sub.exists_ind and L_l10n_obj_sub to IO_l10n_obj
   L_l10n_obj_sub.exists_ind := L_exists;
   IO_l10n_obj               := L_l10n_obj_sub;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FISCAL_ATTRIB_EXISTS;
------------------------------------------------------------------------------------------------------
FUNCTION DEL_ENT_TRIB_SUBS(O_error_message   IN OUT   VARCHAR2,
                           IO_l10n_obj       IN OUT   L10N_OBJ)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'L10N_BR_FND_SQL.DEL_ENT_TRIB_SUBS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_entity_type   VARCHAR2(6) := IO_l10n_obj.source_entity;

   cursor C_LOCK_ENTITY_TRIB_SUBS is
      select 'x'
        from l10n_br_entity_trib_subs
       where entity = IO_l10n_obj.source_id
         and entity_type = L_entity_type
         for update nowait;

   cursor C_CHECK_LOCATION_TYPE is
      select 'S'
        from store
       where store = IO_l10n_obj.source_id
         and rownum = 1
       UNION ALL
      select 'W'
        from wh
       where wh = IO_l10n_obj.source_id
         and rownum = 1;

BEGIN

   if L_entity_type = 'LOC' then

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_LOCATION_TYPE',
                       'STORE, WH',
                       'Location: ' || IO_l10n_obj.source_id);
      open C_CHECK_LOCATION_TYPE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_LOCATION_TYPE',
                       'STORE, WH',
                       'Location: ' || IO_l10n_obj.source_id);
      fetch C_CHECK_LOCATION_TYPE into L_entity_type;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_LOCATION_TYPE',
                       'STORE, WH',
                       'Location: ' || IO_l10n_obj.source_id);
      close C_CHECK_LOCATION_TYPE;

   end if;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ENTITY_TRIB_SUBS',
                    'L10N_BR_ENTITY_TRIB_SUBS',
                    'Source id: '||IO_l10n_obj.source_id||',
                    Source Entity: '|| L_entity_type);
   open C_LOCK_ENTITY_TRIB_SUBS;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ENTITY_TRIB_SUBS',
                    'L10N_BR_ENTITY_TRIB_SUBS',
                    'Source id: '||IO_l10n_obj.source_id||',
                    Source Entity: '|| L_entity_type);
   close C_LOCK_ENTITY_TRIB_SUBS;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'L10N_BR_ENTITY_TRIB_SUBS',
                    'Source id: '||IO_l10n_obj.source_id||',
                    Source Entity: '|| L_entity_type);

   delete from l10n_br_entity_trib_subs
         where entity = IO_l10n_obj.source_id
           and entity_type = L_entity_type;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'L10N_BR_ENTITY_TRIB_SUBS',
                                            'Source id: '||IO_l10n_obj.source_id,
                                            'Source entity: '|| L_entity_type);
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END DEL_ENT_TRIB_SUBS;
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DESC (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_desc               IN OUT   VARCHAR2,
                        I_key_value          IN       VARCHAR2,
                        I_key_type           IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program  VARCHAR2(62) := 'L10N_BR_FND_SQL.GET_ITEM_DESC';

BEGIN
   ---
   if NOT ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                   O_desc,
                                   I_key_value) then
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ITEM_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_DESC (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_desc           IN OUT   VARCHAR2,
                            I_key_value      IN       VARCHAR2,
                            I_key_type       IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program	  VARCHAR2(62) := 'L10N_BR_FND_SQL.GET_SUPPLIER_DESC';

BEGIN
   ---
   if NOT SUPP_ATTRIB_SQL.GET_SUPP_DESC(O_error_message,
                                        I_key_value,
                                        O_desc) then
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_SUPPLIER_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_JURISDICTION_DESC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_desc            IN OUT   VARCHAR2,
                                I_key_value       IN       VARCHAR2,
                                I_key_type        IN       VARCHAR2,
                                I_key_value_1     IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program   VARCHAR2(62) := 'L10N_BR_FND_SQL.GET_JURISDICTION_DESC';
   L_exists    BOOLEAN;

BEGIN
   ---
   if NOT COUNTRY_TAX_JURS_SQL.GET_JURS_DESC(O_error_message,
                                             L_exists,
                                             O_desc,
                                             I_key_value,
                                             I_key_type,
                                             I_key_value_1) then
      return FALSE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_JURISDICTION_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_DESC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_desc            IN OUT   VARCHAR2,
                           I_key_value       IN       VARCHAR2,
                           I_key_type        IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program   VARCHAR2(62) := 'L10N_BR_FND_SQL.GET_COUNTRY_DESC';

BEGIN
   ---
   if NOT COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                        I_key_value,
                                        O_desc) then
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_COUNTRY_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_STORE_DESC (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_desc              IN OUT   VARCHAR2,
                         I_key_value         IN       VARCHAR2,
                         I_key_type          IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'L10N_BR_FND_SQL.GET_STORE_DESC';

   cursor C_GET_STORE_NAME is
      select store_name
        from store_add
       where store = I_key_value;

BEGIN
   open C_GET_STORE_NAME;
   fetch C_GET_STORE_NAME into O_desc;
   close C_GET_STORE_NAME;
   ---
   if O_desc is NULL then
      if NOT STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                       I_key_value,
                                       O_desc) then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STORE_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_WH_DESC (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_desc                 IN OUT   VARCHAR2,
                      I_key_value            IN       VARCHAR2,
                      I_key_type             IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program  VARCHAR2(62) := 'L10N_BR_FND_SQL.GET_WH_DESC';

BEGIN
   ---
   if NOT WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                 I_key_value,
                                 O_desc) then
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_WH_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_PARTNER_DESC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_desc            IN OUT   VARCHAR2,
                           I_key_value       IN       VARCHAR2,
                           I_key_type        IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program        VARCHAR2(62)                := 'L10N_BR_FND_SQL.GET_PARTNER_DESC';
   L_key_type       PARTNER.PARTNER_TYPE%TYPE   := I_key_type;

   cursor C_GET_PARTNER_TYPE is
      select partner_type
        from partner
       where partner_id = I_key_value;

BEGIN
   ---
   open C_GET_PARTNER_TYPE;
   fetch C_GET_PARTNER_TYPE into L_key_type;
   close C_GET_PARTNER_TYPE;
   ---
   if NOT PARTNER_SQL.GET_DESC(O_error_message,
                               O_desc,
                               I_key_value,
                               L_key_type) then
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PARTNER_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_OUTLOC_DESC (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_desc                 IN OUT   VARCHAR2,
                          I_key_value            IN       VARCHAR2,
                          I_key_type             IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program         VARCHAR2(62)              := 'L10N_BR_FND_SQL.GET_OUTLOC_DESC';
   L_key_type        OUTLOC.OUTLOC_TYPE%TYPE   := I_key_type;

   cursor C_GET_OUTLOC_TYPE is
      select outloc_type
        from outloc
       where outloc_id = I_key_value;

BEGIN
   ---
   open C_GET_OUTLOC_TYPE;
   fetch C_GET_OUTLOC_TYPE into L_key_type;
   close C_GET_OUTLOC_TYPE;
   ---
   if NOT OUTSIDE_LOCATION_SQL.GET_DESC(O_error_message,
                                        O_desc,
                                        I_key_value,
                                        L_key_type) then
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_OUTLOC_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ENTITY_DESC (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_desc                 IN OUT   VARCHAR2,
                              I_key_value            IN       VARCHAR2,
                              I_key_type             IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program                  VARCHAR2(62) := 'L10N_BR_FND_SQL.GET_TSF_ENTITY_DESC';
   L_exists                   BOOLEAN;
   L_tsf_entity_row           TSF_ENTITY%ROWTYPE;

BEGIN
   ---
   if NOT TSF_ENTITY_SQL.GET_ROW(O_error_message,
                                 L_exists,
                                 L_tsf_entity_row,
                                 I_key_value) then
    return FALSE;
   end if;

   if L_exists then
      O_desc := L_tsf_entity_row.tsf_entity_desc;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TSF_ENTITY_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_SOB_DESC (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_desc             IN OUT   VARCHAR2,
                       I_key_value        IN       VARCHAR2,
                       I_key_type         IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program                  VARCHAR2(62) := 'L10N_BR_FND_SQL.GET_SOB_DESC';
BEGIN
   ---
   if NOT SET_OF_BOOKS_SQL.GET_DESC (O_error_message,
                                     O_desc,
                                     I_key_value) then
      return FALSE;
   end if;
   ---
   return TRUE;
   ---

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SOB_DESC;
---------------------------------------------------------------------------------------------
--LFAS Field Validation Functions
---------------------------------------------------------------------------------------------
FUNCTION GET_NCM_DESC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_ncm_desc       IN OUT   L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                      I_ncm_code       IN       L10N_BR_NCM_CODES.NCM_CODE%TYPE,
                      I_country_id     IN       L10N_BR_NCM_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN AS

   L_program    VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_NCM_DESC';
   L_ncm_desc   L10N_BR_NCM_CODES.NCM_DESC%TYPE;

   cursor C_NCM_DESC is
      select ncm_desc description
        from l10n_br_ncm_codes
       where ncm_code = I_ncm_code
         and country_id = I_country_id;

BEGIN

   open C_NCM_DESC;
   fetch C_NCM_DESC into L_ncm_desc;
   close C_NCM_DESC;

   if L_ncm_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_NCM_CODE', I_ncm_code);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_ncm_desc,
                                O_ncm_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NCM_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_NCMCHAR_DESC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_ncmchar_desc    IN OUT  L10N_BR_NCM_CHAR_CODES.NCM_CHAR_DESC%TYPE,
                          O_ncm_desc        IN OUT  L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                          I_ncmchar_code    IN      L10N_BR_NCM_CHAR_CODES.NCM_CHAR_CODE%TYPE,
                          IO_ncm_code       IN OUT  L10N_BR_NCM_CHAR_CODES.NCM_CODE%TYPE,
                          I_country_id      IN      L10N_BR_NCM_CHAR_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN AS

   L_program        VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_NCMCHAR_DESC';
   L_ncm_code       L10N_BR_NCM_CHAR_CODES.NCM_CODE%TYPE;
   L_ncm_desc       L10N_BR_NCM_CODES.NCM_DESC%TYPE;
   L_ncmchar_desc   L10N_BR_NCM_CHAR_CODES.NCM_CHAR_DESC%TYPE;

   cursor C_NCM_CODE_CHAR_DESC is
      select ncc.ncm_code,
             nc.ncm_desc,
             ncc.ncm_char_desc description
        from l10n_br_ncm_char_codes ncc,
             l10n_br_ncm_codes nc
       where ncc.ncm_char_code = I_ncmchar_code
         and ncc.ncm_code      = nvl(IO_ncm_code,ncc.ncm_code)
         and ncc.country_id    = I_country_id
         and ncc.ncm_code  = nc.ncm_code
         and ncc.country_id  = nc.country_id;

BEGIN

   open C_NCM_CODE_CHAR_DESC;
   fetch C_NCM_CODE_CHAR_DESC into IO_ncm_code,L_ncm_desc,L_ncmchar_desc;
   close C_NCM_CODE_CHAR_DESC;

   if L_ncmchar_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_NCMCHAR_CODE', I_ncmchar_code);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_ncm_desc,
                                O_ncm_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
      if LANGUAGE_SQL.TRANSLATE(L_ncmchar_desc,
                                O_ncmchar_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NCMCHAR_DESC;
-------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_NCM_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'L10N_BR_FND_SQL.MERG_INTO_NCM_CODES';
   L_tax_service_id      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE := NULL;

BEGIN

   if L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE(O_error_message,
                                                     L_tax_service_id,
                                                     L10N_BR_FND_SQL.LP_NCM) = FALSE then
      return FALSE;
   end if;

   merge into l10n_br_ncm_codes ncmc
      using (select lbt.fiscal_code,
                    'BR' country_id,
                    lbt.fiscal_code_description
               from l10n_br_tax_call_res_fsc_fnd lbt
              where lbt.tax_service_id = L_tax_service_id
                and (NOT EXISTS (select 1
                                   from l10n_br_ncm_codes b
                                  where lbt.fiscal_code = b.ncm_code
                                    and 'BR' = b.country_id)
                 or EXISTS (select 1
                              from l10n_br_ncm_codes b
                             where lbt.fiscal_code = b.ncm_code
                               and 'BR' = b.country_id
                               and lbt.fiscal_code_description <> b.ncm_desc))) ncmc1
    on (ncmc.ncm_code = ncmc1.fiscal_code)
    when matched then
       update set ncmc.ncm_desc = ncmc1.fiscal_code_description
    when NOT matched then
        insert(ncm_code,
               country_id,
               ncm_desc)
        values(ncmc1.fiscal_code,
               ncmc1.country_id,
               ncmc1.fiscal_code_description);

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERG_INTO_NCM_CODES;
------------------------------------------------------------------------------------
FUNCTION MERG_INTO_NCM_CHAR_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'L10N_BR_FND_SQL.MERG_INTO_NCM_CHAR_CODES';
   L_tax_service_id      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE := NULL;

BEGIN

   if L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE(O_error_message,
                                                     L_tax_service_id,
                                                     L10N_BR_FND_SQL.LP_NCM_CARAC) = FALSE then
      return FALSE;
   end if;

   merge into l10n_br_ncm_char_codes ncmcc
      using (select fsl.fiscal_code,
                    fsl.fiscal_code_description,
                    fsl.fiscal_parent_code,
                    'BR' country_id
               from l10n_br_tax_call_res_fsc_fnd fsl
             where fsl.tax_service_id = L_tax_service_id
               and (NOT EXISTS (select 1
                                 from l10n_br_ncm_char_codes b
                                where fsl.fiscal_code = b.ncm_char_code
                                  and fsl.fiscal_parent_code = b.ncm_code
                                  and 'BR' = b.country_id)
                or EXISTS (select 1
                             from l10n_br_ncm_char_codes b
                            where fsl.fiscal_code = b.ncm_char_code
                              and fsl.fiscal_parent_code = b.ncm_code
                              and 'BR' = b.country_id
                              and fsl.fiscal_parent_code <> b.ncm_char_desc))) ncmcc1
   on (ncmcc.ncm_char_code = ncmcc1.fiscal_code
       and ncmcc.ncm_code = ncmcc1.fiscal_parent_code
       and ncmcc.country_id = ncmcc1.country_id)
   when matched then
      update set ncmcc.ncm_char_desc = ncmcc1.fiscal_code_description
   when not matched then
      insert(ncm_char_code,
             ncm_char_desc,
             ncm_code,
             country_id)
      values(ncmcc1.fiscal_code,
             ncmcc1.fiscal_code_description,
             ncmcc1.fiscal_parent_code,
             ncmcc1.country_id);

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERG_INTO_NCM_CHAR_CODES;
------------------------------------------------------------------------------------
FUNCTION GET_IPI_DESC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_ipi_desc        IN OUT  L10N_BR_NCM_IPI_CODES.IPI_DESC%TYPE,
                      O_ncm_desc        IN OUT  L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                      I_ipi_code        IN      L10N_BR_NCM_IPI_CODES.IPI_CODE%TYPE,
                      IO_ncm_code       IN OUT  L10N_BR_NCM_IPI_CODES.NCM_CODE%TYPE,
                      I_country_id      IN      L10N_BR_NCM_IPI_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN AS

   L_program        VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_IPI_DESC';
   L_ipi_desc       L10N_BR_NCM_IPI_CODES.IPI_DESC%TYPE;
   L_ncm_desc       L10N_BR_NCM_CODES.NCM_DESC%TYPE;

   cursor C_NCM_IPI_DESC is
      select nic.ipi_desc description,
             nic.ncm_code,
             nc.ncm_desc
        from l10n_br_ncm_ipi_codes nic,
             l10n_br_ncm_codes nc
       where nic.ipi_code = I_ipi_code
         and nic.ncm_code      = nvl(IO_ncm_code,nic.ncm_code)
         and nic.country_id    = I_country_id
         and nic.ncm_code = nc.ncm_code
         and nic.country_id = nc.country_id;

BEGIN


   open C_NCM_IPI_DESC;
   fetch C_NCM_IPI_DESC into L_ipi_desc,IO_ncm_code,L_ncm_desc;
   close C_NCM_IPI_DESC;

   if L_ipi_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_IPI_CODE', I_ipi_code);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_ipi_desc,
                                O_ipi_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
      if LANGUAGE_SQL.TRANSLATE(L_ncm_desc,
                                O_ncm_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_IPI_DESC;
-------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_NCM_IPI_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'L10N_BR_FND_SQL.MERG_INTO_NCM_IPI_CODES';
   L_tax_service_id      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE := NULL;

BEGIN

   if L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE(O_error_message,
                                                     L_tax_service_id,
                                                     L10N_BR_FND_SQL.LP_NCM_IPI) = FALSE then
      return FALSE;
   end if;

   merge into l10n_br_ncm_ipi_codes ncmip
      using (select fsl.fiscal_code,
                    fsl.fiscal_code_description,
                    fsl.fiscal_parent_code,
                    'BR' country_id
               from l10n_br_tax_call_res_fsc_fnd fsl
              where fsl.tax_service_id = L_tax_service_id
                and (NOT EXISTS (select 1
                                  from l10n_br_ncm_ipi_codes b
                                 where fsl.fiscal_code = b.ipi_code
                                   and fsl.fiscal_parent_code = b.ncm_code
                                   and 'BR' = b.country_id)
                 or EXISTS (select 1
                              from l10n_br_ncm_ipi_codes b
                             where fsl.fiscal_code = b.ipi_code
                               and fsl.fiscal_parent_code = b.ncm_code
                               and 'BR' = b.country_id
                               and fsl.fiscal_code_description <> b.ipi_desc))) ncmip1
   on (ncmip.ipi_code = ncmip1.fiscal_code
       and ncmip.ncm_code = ncmip1.fiscal_parent_code
       and ncmip.country_id = ncmip1.country_id)
   when matched then
      update set ncmip.ipi_desc = ncmip1.fiscal_code_description
   when not matched then
      insert(ipi_code,
             ipi_desc,
             ncm_code,
             country_id)
      values(ncmip1.fiscal_code,
             ncmip1.fiscal_code_description,
             ncmip1.fiscal_parent_code,
             ncmip1.country_id);

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERG_INTO_NCM_IPI_CODES;
------------------------------------------------------------------------------------
FUNCTION GET_PAUTA_DESC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_pauta_desc      IN OUT  L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_DESC%TYPE,
                        O_ncmchar_desc    IN OUT  L10N_BR_NCM_CHAR_CODES.NCM_CHAR_DESC%TYPE,
                        O_ncm_desc        IN OUT  L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                        I_pauta_code      IN      L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_CODE%TYPE,
                        IO_ncm_code       IN OUT  L10N_BR_NCM_PAUTA_CODES.NCM_CODE%TYPE,
                        IO_ncmchar_code   IN OUT  L10N_BR_NCM_PAUTA_CODES.NCM_CHAR_CODE%TYPE,
                        I_country_id      IN      L10N_BR_NCM_PAUTA_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_PAUTA_DESC';
   L_pauta_desc   L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_DESC%TYPE;
   L_ncm_desc     L10N_BR_NCM_CODES.NCM_DESC%TYPE;
   L_ncmchar_desc L10N_BR_NCM_CHAR_CODES.NCM_CHAR_DESC%TYPE;

   cursor C_NCM_PAUTA_DESC is
      select npc.ncm_pauta_desc description,
             npc.ncm_code,
             nc.ncm_desc,
             npc.ncm_char_code,
             ncc.ncm_char_desc
        from l10n_br_ncm_pauta_codes npc,
             l10n_br_ncm_char_codes ncc,
             l10n_br_ncm_codes nc
       where npc.ncm_pauta_code = I_pauta_code
         and npc.ncm_code       = nvl(IO_ncm_code,npc.ncm_code)
         and npc.ncm_char_code  = nvl(IO_ncmchar_code ,npc.ncm_char_code)
         and npc.country_id     = I_country_id
         and npc.ncm_code = nc.ncm_code
         and npc.country_id = nc.country_id
         and nc.ncm_code = ncc.ncm_code
         and nc.country_id = ncc.country_id;


BEGIN

   open C_NCM_PAUTA_DESC;
   fetch C_NCM_PAUTA_DESC into L_pauta_desc,IO_ncm_code,L_ncm_desc,IO_ncmchar_code,L_ncmchar_desc;
   close C_NCM_PAUTA_DESC;

   if L_pauta_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PAUTA_CODE', I_pauta_code);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_pauta_desc,
                                O_pauta_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
      if LANGUAGE_SQL.TRANSLATE(L_ncm_desc,
                                O_ncm_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
      if LANGUAGE_SQL.TRANSLATE(L_ncmchar_desc,
                                O_ncmchar_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PAUTA_DESC;
-------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_NCM_PAUTA_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'L10N_BR_FND_SQL.MERG_INTO_NCM_PAUTA_CODES';
   L_tax_service_id      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE := NULL;

BEGIN

   if L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE(O_error_message,
                                                     L_tax_service_id,
                                                     L10N_BR_FND_SQL.LP_NCM_PAUTA) = FALSE then
      return FALSE;
   end if;

   merge into l10n_br_ncm_pauta_codes npc
      using (select fsl.fiscal_code,
                    fsl.fiscal_code_description,
                    fsl.fiscal_parent_code,
                    fsl.fiscal_extended_parent_code,
                    'BR' country_id
               from l10n_br_tax_call_res_fsc_fnd fsl
              where fsl.tax_service_id = L_tax_service_id
                and (NOT EXISTS (select 1
                                  from l10n_br_ncm_pauta_codes b
                                 where fsl.fiscal_code = b.ncm_pauta_code
                                   and fsl.fiscal_parent_code = b.ncm_char_code
                                   and fsl.fiscal_extended_parent_code = b.ncm_code
                                   and 'BR' = b.country_id)
                 or EXISTS (select 1
                              from l10n_br_ncm_pauta_codes b
                             where fsl.fiscal_code = b.ncm_pauta_code
                               and fsl.fiscal_parent_code = b.ncm_char_code
                               and fsl.fiscal_extended_parent_code = b.ncm_code
                               and 'BR' = b.country_id
                               and fsl.fiscal_code_description <> b.ncm_pauta_desc))) npc1
   on (npc.ncm_pauta_code = npc1.fiscal_code
       and npc.ncm_char_code = npc1.fiscal_parent_code
       and npc.ncm_code = npc1.fiscal_extended_parent_code
       and npc.country_id = npc1.country_id)
   when matched then
      update set npc.ncm_pauta_desc = npc1.fiscal_code_description
   when not matched then
      insert(ncm_pauta_code,
             ncm_pauta_desc,
             ncm_char_code,
             country_id,
             ncm_code)
   values(npc1.fiscal_code,
          npc1.fiscal_code_description,
          npc1.fiscal_parent_code,
          npc1.country_id,
          npc1.fiscal_extended_parent_code);

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERG_INTO_NCM_PAUTA_CODES;
------------------------------------------------------------------------------------
FUNCTION GET_SERVCODE_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_service_desc    IN OUT   L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_DESC%TYPE,
                           I_service_code    IN       L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_CODE%TYPE)
RETURN BOOLEAN AS

   L_program        VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_SERVCODE_DESC';
   L_service_desc   L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_DESC%TYPE;

   cursor C_SERV_DESC is
      select mastersaf_service_desc description
        from l10n_br_mastersaf_svc_codes
       where mastersaf_service_code = I_service_code;

BEGIN

   open C_SERV_DESC;
   fetch C_SERV_DESC into L_service_desc;
   close C_SERV_DESC;

   if L_service_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SERVICE_CODE', I_service_code);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_service_desc,
                                O_service_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SERVCODE_DESC;
-------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_MASSERV(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'L10N_BR_FND_SQL.MERG_INTO_MASSERV';
   L_tax_service_id      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE := NULL;

BEGIN

   if L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE(O_error_message,
                                                     L_tax_service_id,
                                                     L10N_BR_FND_SQL.LP_MASSERV) = FALSE then
      return FALSE;
   end if;

   merge into l10n_br_mastersaf_svc_codes mssc
      using (select fsl.fiscal_code ,
                    fsl.fiscal_code_description
               from l10n_br_tax_call_res_fsc_fnd fsl
              where fsl.tax_service_id = L_tax_service_id
                and (NOT EXISTS (select 1
                                  from l10n_br_mastersaf_svc_codes b
                                 where fsl.fiscal_code = b.mastersaf_service_code)
                 or EXISTS (select 1
                              from l10n_br_mastersaf_svc_codes b
                             where fsl.fiscal_code = b.mastersaf_service_code
                               and fsl.fiscal_code_description <> b.mastersaf_service_desc))) mssc1
   on (mssc.mastersaf_service_code = mssc1.fiscal_code)
   when matched then
      update set mssc.mastersaf_service_desc = mssc1.fiscal_code_description
   when not matched then
      insert(mastersaf_service_code,
             mastersaf_service_desc)
      values(mssc1.fiscal_code,
             mssc1.fiscal_code_description);

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERG_INTO_MASSERV;
------------------------------------------------------------------------------------
FUNCTION GET_CNAE_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cnae_desc       IN OUT   L10N_BR_CNAE_CODES.CNAE_DESC%TYPE,
                       I_cnae_code       IN       L10N_BR_CNAE_CODES.CNAE_CODE%TYPE)
RETURN BOOLEAN AS

   L_program     VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_CNAE_DESC';
   L_cnae_desc   L10N_BR_CNAE_CODES.CNAE_DESC%TYPE;

  cursor C_CNAE_DESC is
      select cnae_desc description
        from l10n_br_cnae_codes
       where cnae_code = I_cnae_code;

BEGIN

   open C_CNAE_DESC;
   fetch C_CNAE_DESC into L_cnae_desc;
   close C_CNAE_DESC;

   if L_cnae_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CNAE_CODE', I_cnae_code);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_cnae_desc,
                                O_cnae_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CNAE_DESC;
-------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_CNAE_CODES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'L10N_BR_FND_SQL.MERG_INTO_CNAE_CODES';
   L_tax_service_id      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE := NULL;

BEGIN

   if L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE(O_error_message,
                                                     L_tax_service_id,
                                                     L10N_BR_FND_SQL.LP_CNAE) = FALSE then
      return FALSE;
   end if;

   merge into l10n_br_cnae_codes cnac
      using (select fsl.fiscal_code,
                    fsl.fiscal_code_description
               from l10n_br_tax_call_res_fsc_fnd fsl
              where fsl.tax_service_id = L_tax_service_id
                and (NOT EXISTS (select 1
                                   from l10n_br_cnae_codes b
                                  where fsl.fiscal_code = b.cnae_code)
                 or EXISTS (select 1
                              from l10n_br_cnae_codes b
                             where fsl.fiscal_code = b.cnae_code
                               and fsl.fiscal_code_description <> b.cnae_desc))) cnac1
   on (cnac.cnae_code = cnac1.fiscal_code)
   when matched then
      update set cnac.cnae_desc = cnac1.fiscal_code_description
   when not matched then
      insert(cnae_code,
             cnae_desc)
      values(cnac1.fiscal_code,
             cnac1.fiscal_code_description);

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERG_INTO_CNAE_CODES;
------------------------------------------------------------------------------------
FUNCTION GET_FEDSERV_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_fedserv_desc    IN OUT   L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_DESC%TYPE,
                          I_fedserv_code    IN       L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_CODE%TYPE)
RETURN BOOLEAN AS

   L_program        VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_FEDSERV_DESC';
   L_fedserv_desc   L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_DESC%TYPE;

   cursor C_FEDSERV_DESC is
      select federal_service_desc description
        from l10n_br_federal_svc_codes
       where federal_service_code = I_fedserv_code;

BEGIN

   open C_FEDSERV_DESC;
   fetch C_FEDSERV_DESC into L_fedserv_desc;
   close C_FEDSERV_DESC;

   if L_fedserv_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FEDSERV_CODE', I_fedserv_code);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_fedserv_desc,
                                O_fedserv_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_FEDSERV_DESC;
-------------------------------------------------------------------------------------------
FUNCTION MERG_INTO_FEDSERV(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'L10N_BR_FND_SQL.MERG_INTO_FEDSERV';
   L_tax_service_id      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE := NULL;

BEGIN

   if L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE(O_error_message,
                                                     L_tax_service_id,
                                                     L10N_BR_FND_SQL.LP_FEDSERV) = FALSE then
      return FALSE;
   end if;

   merge into l10n_br_federal_svc_codes fsc
      using (select fsl.fiscal_code,
                    fsl.fiscal_code_description
               from l10n_br_tax_call_res_fsc_fnd fsl
              where fsl.tax_service_id = L_tax_service_id
                and (NOT EXISTS (select 1
                                  FROM l10n_br_federal_svc_codes b
                                 WHERE fsl.fiscal_code = b.federal_service_code)
                 or EXISTS (select 1
                              from l10n_br_federal_svc_codes b
                             where fsl.fiscal_code = b.federal_service_code
                               and fsl.fiscal_code_description <> b.federal_service_desc))) fsc1
   on (fsc.federal_service_code = fsc1.fiscal_code)
   when matched then
      update set fsc.federal_service_desc = fsc1.fiscal_code_description
   when not matched then
      insert(federal_service_code,
             federal_service_desc)
      values(fsc1.fiscal_code,
             fsc1.fiscal_code_description);

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERG_INTO_FEDSERV;
------------------------------------------------------------------------------------
FUNCTION VALIDATE_STATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_state_desc      IN OUT   STATE.DESCRIPTION%TYPE,
                        O_country_desc    IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                        IO_country        IN OUT   STATE.COUNTRY_ID%TYPE,
                        I_jurisdiction    IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                        I_state           IN       STATE.STATE%TYPE)
RETURN BOOLEAN AS

   L_program        VARCHAR2(100) := 'L10N_BR_FND_SQL.VALIDATE_STATE';
   L_state_desc     STATE.DESCRIPTION%TYPE;
   L_country_desc   COUNTRY.COUNTRY_DESC%TYPE;

   cursor C_STATE_INFO is
      select st.description,
             st.country_id,
             cy.country_desc
        from state st,
             country cy,
             country_tax_jurisdiction ctj
       where st.state = I_state
         and st.country_id = cy.country_id
         and ctj.state = st.state
         and ctj.jurisdiction_code = NVL(I_jurisdiction,ctj.jurisdiction_code)
         and ctj.country_id = cy.country_id
         and cy.country_id = NVL(IO_country,cy.country_id);


BEGIN

   if I_state is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','State');
      return FALSE;
   end if;

   open C_STATE_INFO;
   fetch C_STATE_INFO into L_state_desc,
                           IO_country,
                           L_country_desc;
   close C_STATE_INFO;

   if (L_state_desc is NULL and IO_country is NOT NULL and I_jurisdiction is NOT NULL) or
      (L_state_desc is NULL and I_jurisdiction is NOT NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('NO_CNTRY_ST_JURIS', NULL);
      return FALSE;

   elsif L_state_desc is NULL and IO_country is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_COUNTRY_STATE', NULL);
      return FALSE;
      --

   elsif L_state_desc is NULL and (IO_country is NULL or I_jurisdiction is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STATE', I_state);
      return FALSE;
      --

   else
      if LANGUAGE_SQL.TRANSLATE(L_state_desc,
                                O_state_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
      if LANGUAGE_SQL.TRANSLATE(L_country_desc,
                                O_country_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_STATE;
-------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
-- LFAS Group Validation functions
---------------------------------------------------------------------------------------------
FUNCTION CHK_REG_NUM_GRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_taxpayer_type   IN       VARCHAR2,
                         I_cnpj            IN       VARCHAR2,
                         I_ie              IN       VARCHAR2,
                         I_im              IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_REG_NUM_GRP';

BEGIN

   if I_taxpayer_type ='J' then
      if I_cnpj is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CNPJ');
         return FALSE;
      end if;
      ---
      if I_ie is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','State Incription');
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_REG_NUM_GRP;
---------------------------------------------------------------------------------------------
FUNCTION CHK_SUPP_REG_NUM_GRP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_taxpayer_type     IN       VARCHAR2,
                              I_cnpj              IN       VARCHAR2,
                              I_ie                IN       VARCHAR2,
                              I_im                IN       VARCHAR2,
                              I_suframa           IN       VARCHAR2,
                              I_cpf               IN       VARCHAR2,
                              I_nit               IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_SUPP_REG_NUM_GRP';

BEGIN

   if I_taxpayer_type = 'J' then
      if I_cnpj is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CNPJ');
         return FALSE;
      end if;
      ---
      if I_ie is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','State Inscription');
         return FALSE;
      end if;
      ---
      if I_suframa is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Suframa');
         return FALSE;
      end if;
      ---
   elsif I_taxpayer_type = 'F' then
      if I_cpf is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CPF');
         return FALSE;
      end if;
      ---
      if I_nit is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','NIT');
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_SUPP_REG_NUM_GRP;
---------------------------------------------------------------------------------------------
FUNCTION CHK_PTNR_REG_NUM_GRP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_taxpayer_type     IN       VARCHAR2,
                              I_cnpj              IN       VARCHAR2,
                              I_ie                IN       VARCHAR2,
                              I_im                IN       VARCHAR2,
                              I_cpf               IN       VARCHAR2,
                              I_nit               IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_PTNR_REG_NUM_GRP';

BEGIN

   if I_taxpayer_type = 'J' then
      if I_cnpj is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CNPJ');
         return FALSE;
      end if;
      ---
      if I_ie is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','State Incription');
         return FALSE;
      end if;
   end if;

   if I_taxpayer_type = 'F' then
      if I_cpf is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CPF');
         return FALSE;
      end if;
      ---
      if I_nit is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','NIT');
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_PTNR_REG_NUM_GRP;
---------------------------------------------------------------------------------------------
FUNCTION CHK_FISCAL_CLASS_GRP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_taxpayer_type     IN       VARCHAR2,
                              I_ipi_ind           IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_FISCAL_CLASS_GRP';

BEGIN

   if I_taxpayer_type ='J' and
      I_ipi_ind is NULL then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','IPI Indicator');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_FISCAL_CLASS_GRP;
---------------------------------------------------------------------------------------------
FUNCTION CHK_SUPP_FISCAL_CLASS_GRP(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_taxpayer_type    IN       VARCHAR2,
                                   I_ipi_ind          IN       VARCHAR2,
                                   I_iss_contrib_ind  IN       VARCHAR2,
                                   I_rural_prod_ind   IN       VARCHAR2,
                                   I_simples_ind      IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_SUPP_FISCAL_CLASS_GRP';

BEGIN

   if I_taxpayer_type = 'J' then
      if I_ipi_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','IPI Indicator');
         return FALSE;
      end if;
      ---
      if I_iss_contrib_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','ISS Contributor');
         return FALSE;
      end if;
      ---
      if I_simples_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','SIMPLES Contributor');
         return FALSE;
      end if;

   elsif I_taxpayer_type = 'F' then
      if I_rural_prod_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Rural Produce');
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_SUPP_FISCAL_CLASS_GRP;
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
-- LFAS Entity Validations
---------------------------------------------------------------------------------------------
FUNCTION CHK_STORE_ENT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_taxpayer_type  IN       VARCHAR2,
                       I_cnpj           IN       VARCHAR2,
                       I_ie             IN       VARCHAR2,
                       I_im             IN       VARCHAR2,
                       I_ipi_ind        IN       VARCHAR2,
                       I_country        IN       VARCHAR2,
                       I_store          IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_STORE_ENT';
   L_exists      BOOLEAN := TRUE;

BEGIN


   if I_taxpayer_type = 'J' then

      if TRIB_SUBS_EXISTS(O_error_message,
                          L_exists,
                          I_store,
                          'S',
                          I_country) = FALSE then
         return FALSE;
      end if;

      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('TRIB_SUBS_REQD');
         return FALSE;
      end if;
      
      if CNAE_CODES_EXISTS(O_error_message,
                                 L_exists,
                                 I_store,
                                 'S',
                                 'LOC',
                                 I_country) = FALSE then
               return FALSE;
      end if;
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('CNAE_CODES_REQD');
         return FALSE;
      end if;
      
      if I_cnpj is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CNPJ');
         return FALSE;
      end if;

      if I_ie is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','State Inscription');
         return FALSE;
      end if;

      if I_ipi_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','IPI Indicator');
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_STORE_ENT;
---------------------------------------------------------------------------------------------
FUNCTION CHK_SOB_TSFE_STR_OUTL_WH_ENT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_taxpayer_type  IN       VARCHAR2,
                                      I_cnpj           IN       VARCHAR2,
                                      I_ie             IN       VARCHAR2,
                                      I_im             IN       VARCHAR2,
                                      I_ipi_ind        IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_TSFE_STR_OUTL_WH_ENT';

BEGIN

   if I_taxpayer_type = 'J' then

      if I_cnpj is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CNPJ');
         return FALSE;
      end if;

      if I_ie is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','State Inscription');
         return FALSE;
      end if;

      if I_ipi_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','IPI Indicator');
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_SOB_TSFE_STR_OUTL_WH_ENT;
------------------------------------------------------------------------------
FUNCTION CHK_OUTL_ENT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_taxpayer_type      IN       VARCHAR2,
                      I_cnpj               IN       VARCHAR2,
                      I_ie                 IN       VARCHAR2,
                      I_im                 IN       VARCHAR2,
                      I_ipi_ind            IN       VARCHAR2,
                      I_outloc_type        IN       VARCHAR2,
                      I_outloc_id          IN       VARCHAR2,
                      I_country            IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_OUTL_ENT';
   L_exists      BOOLEAN := TRUE;

BEGIN

   if I_taxpayer_type = 'J' then
      if CNAE_CODES_EXISTS(O_error_message,
                           L_exists,
                           I_outloc_id,
                           I_outloc_type,
                           'OLOC',
                           I_country) = FALSE then
               return FALSE;
      end if;
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('CNAE_CODES_REQD','CNAE Codes');
         return FALSE;
      end if;
      if CHK_SOB_TSFE_STR_OUTL_WH_ENT(O_error_message,
                                      I_taxpayer_type,
                                      I_cnpj,
                                      I_ie,
                                      I_im,
                                      I_ipi_ind)= FALSE then
                                           
          return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_OUTL_ENT;
---------------------------------------------------------------------------------------------
FUNCTION CHK_WH_ENT_POP_VWH(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wh                IN       VARCHAR2,
                            I_country_id        IN       VARCHAR2,
                            I_taxpayer_type     IN       VARCHAR2,
                            I_cnpj              IN       VARCHAR2,
                            I_ie                IN       VARCHAR2,
                            I_im                IN       VARCHAR2,
                            I_ipi_ind           IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_WH_ENT_POP_VWH';
   L_exists      BOOLEAN := TRUE;

   cursor C_GET_VWH is
      select wh
        from wh
       where physical_wh = I_wh
         and physical_wh <> wh;

BEGIN

   if I_taxpayer_type = 'J' then
      if TRIB_SUBS_EXISTS(O_error_message,
                          L_exists,
                          I_wh,
                          'W',
                          I_country_id) = FALSE then
         return FALSE;
      end if;
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('TRIB_SUBS_REQD');
         return FALSE;
      end if;
      
      if CNAE_CODES_EXISTS(O_error_message,
                        L_exists,
                        I_wh,
                        'W',
                        'LOC',
                        I_country_id) = FALSE then
      return FALSE;
      end if;
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('CNAE_CODES_REQD');
         return FALSE;
      end if; 
   end if;
   
   if CHK_SOB_TSFE_STR_OUTL_WH_ENT(O_error_message,
                                   I_taxpayer_type,
                                   I_cnpj,
                                   I_ie,
                                   I_im,
                                   I_ipi_ind) = FALSE then
      return FALSE;
   end if;


-- Copying the Fiscal Attributes for virtual WH from WH
   FOR v_wh IN C_GET_VWH LOOP
      MERGE INTO wh_l10n_ext whl10ne
         USING (select *
                  from wh_l10n_ext
                   where wh = I_wh
                     and l10n_country_id = I_country_id) d
           ON (whl10ne.wh = v_wh.wh
           and whl10ne.l10n_country_id = I_country_id
           and whl10ne.group_id = d.group_id)
         WHEN MATCHED THEN
           UPDATE SET
              whl10ne.varchar2_1 = d.varchar2_1,
              whl10ne.varchar2_2 = d.varchar2_2,
              whl10ne.varchar2_3 = d.varchar2_3,
              whl10ne.varchar2_4 = d.varchar2_4,
              whl10ne.varchar2_5 = d.varchar2_5,
              whl10ne.varchar2_6 = d.varchar2_6,
              whl10ne.varchar2_7 = d.varchar2_7,
              whl10ne.varchar2_8 = d.varchar2_8,
              whl10ne.varchar2_9 = d.varchar2_9,
              whl10ne.varchar2_10 = d.varchar2_10,
              whl10ne.number_11 = d.number_11,
              whl10ne.number_12 = d.number_12,
              whl10ne.number_13 = d.number_13,
              whl10ne.number_14 = d.number_14,
              whl10ne.number_15 = d.number_15,
              whl10ne.number_16 = d.number_16,
              whl10ne.number_17 = d.number_17,
              whl10ne.number_18 = d.number_18,
              whl10ne.number_19 = d.number_19,
              whl10ne.number_20 = d.number_20,
              whl10ne.date_21 = d.date_21,
              whl10ne.date_22 = d.date_22
         WHEN NOT MATCHED THEN
           INSERT (wh,
                   l10n_country_id,
                   group_id,
                   varchar2_1,
                   varchar2_2,
                   varchar2_3,
                   varchar2_4,
                   varchar2_5,
                   varchar2_6,
                   varchar2_7,
                   varchar2_8,
                   varchar2_9,
                   varchar2_10,
                   number_11,
                   number_12,
                   number_13,
                   number_14,
                   number_15,
                   number_16,
                   number_17,
                   number_18,
                   number_19,
                   number_20,
                   date_21,
                   date_22)
            VALUES(v_wh.wh,
                   d.l10n_country_id,
                   d.group_id,
                   d.varchar2_1,
                   d.varchar2_2,
                   d.varchar2_3,
                   d.varchar2_4,
                   d.varchar2_5,
                   d.varchar2_6,
                   d.varchar2_7,
                   d.varchar2_8,
                   d.varchar2_9,
                   d.varchar2_10,
                   d.number_11,
                   d.number_12,
                   d.number_13,
                   d.number_14,
                   d.number_15,
                   d.number_16,
                   d.number_17,
                   d.number_18,
                   d.number_19,
                   d.number_20,
                   d.date_21,
                   d.date_22);
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_WH_ENT_POP_VWH;
------------------------------------------------------------------------------
FUNCTION CHK_PTNR_ENT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_taxpayer_type  IN     VARCHAR2,
                      I_cnpj           IN     VARCHAR2,
                      I_ie             IN     VARCHAR2,
                      I_im             IN     VARCHAR2,
                      I_ipi_ind        IN     VARCHAR2,
                      I_cpf            IN     VARCHAR2,
                      I_nit            IN     VARCHAR2,
                      I_partner_type   IN     VARCHAR2,
                      I_partner_id     IN     VARCHAR2,
                      I_country        IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_PTNR_ENT';
   L_exists      BOOLEAN := TRUE;

BEGIN

   if I_taxpayer_type = 'J' then
      if CNAE_CODES_EXISTS(O_error_message,
                           L_exists,
                           I_partner_id,
                           I_partner_type,
                           'PTNR',
                           I_country) = FALSE then
         return FALSE;
      end if;
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('CNAE_CODES_REQD');
         return FALSE;
      end if; 
      
      if I_cnpj is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CNPJ');
         return FALSE;
      end if;

      if I_ie is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','State Inscription');
         return FALSE;
      end if;

      if I_ipi_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','IPI Indicator');
         return FALSE;
      end if;

   elsif I_taxpayer_type = 'F' then

      if I_cpf is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CPF');
         return FALSE;
      end if;

      if I_nit is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','NIT');
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHK_PTNR_ENT;
------------------------------------------------------------------------------
FUNCTION CHK_SUPS_ENT(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_taxpayer_type    IN     VARCHAR2,
                      I_cnpj             IN     VARCHAR2,
                      I_ie               IN     VARCHAR2,
                      I_im               IN     VARCHAR2,
                      I_suframa          IN     VARCHAR2,
                      I_ipi_ind          IN     VARCHAR2,
                      I_iss_contrib_ind  IN     VARCHAR2,
                      I_cpf              IN     VARCHAR2,
                      I_nit              IN     VARCHAR2,
                      I_rural_prod_ind   IN     VARCHAR2,
                      I_simples_ind      IN     VARCHAR2,
                      I_country          IN     VARCHAR2,
                      I_supplier         IN     VARCHAR2)

RETURN BOOLEAN AS

   L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.CHK_SUPS_ENT';
   L_exists      BOOLEAN := TRUE;

BEGIN

   if I_taxpayer_type = 'J' then

      if TRIB_SUBS_EXISTS(O_error_message,
                          L_exists,
                          I_supplier,
                          'SUPP',
                          I_country) = FALSE then
         return FALSE;
      end if;
      if L_exists = FALSE then
               O_error_message := SQL_LIB.CREATE_MSG('TRIB_SUBS_REQD');
               return FALSE;
      end if;

      if CNAE_CODES_EXISTS(O_error_message,
                           L_exists,
                           I_supplier,
                           'S',
                           'SUPP',
                           I_country) = FALSE then
         return FALSE;
      end if;
      if L_exists = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG('CNAE_CODES_REQD');
      return FALSE;
      end if; 

      

      if I_cnpj is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CNPJ');
         return FALSE;
      end if;

      if I_ie is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','State Inscription');
         return FALSE;
      end if;

      if I_suframa is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Suframa');
         return FALSE;
      end if;

      if I_ipi_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','IPI Indicator');
         return FALSE;
      end if;

      if I_iss_contrib_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','ISS Contributor');
         return FALSE;
      end if;


      if I_simples_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','SIMPLES Contributor');
         return FALSE;
      end if;
   end if;

   if I_taxpayer_type = 'F' then

      if I_cpf is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','CPF');
         return FALSE;
      end if;

      if I_nit is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','NIT');
         return FALSE;
      end if;

      if I_rural_prod_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Rural Produce');
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_SUPS_ENT;
------------------------------------------------------------------------------
FUNCTION DEFAULT_FISCAL_ATTRIB(O_error_message  IN OUT   VARCHAR2,
                               IO_l10n_obj      IN OUT   L10N_OBJ)

RETURN BOOLEAN AS

   L_program           VARCHAR2(50) := 'L10N_BR_FND_SQL.DEFAULT_FISCAL_ATTRIB';
   L_fiscal_attr_tbl   TYP_l10n_fisc_attrib_tbl;
   L_vwh_tbl           LOC_TBL      := LOC_TBL();
   L_fiscal_cnt        NUMBER       := 0;

   cursor C_GET_FISCAL_ATTRIB_VWH is
      select vwh.wh,
             ext.l10n_country_id,
             ext.group_id,
             ext.varchar2_1,
             ext.varchar2_2,
             ext.varchar2_3,
             ext.varchar2_4,
             ext.varchar2_5,
             ext.varchar2_6,
             ext.varchar2_7,
             ext.varchar2_8,
             ext.varchar2_9,
             ext.varchar2_10,
             ext.number_11,
             ext.number_12,
             ext.number_13,
             ext.number_14,
             ext.number_15,
             ext.number_16,
             ext.number_17,
             ext.number_18,
             ext.number_19,
             ext.number_20,
             ext.date_21,
             ext.date_22
        from wh_l10n_ext ext,
             wh vwh
       where vwh.physical_wh = ext.wh
         and vwh.physical_wh = IO_l10n_obj.source_id
         and vwh.physical_wh != vwh.wh
         and not exists (select 1
                           from wh_l10n_ext w
                          where w.wh = vwh.wh
                            and w.l10n_country_id = ext.l10n_country_id
                            and w.group_id = ext.group_id
                            and rownum = 1)
       order by vwh.wh, ext.group_id;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_FISCAL_ATTRIB_VWH',
                    'v_wh, wh_l10n_ext ',
                    'Physical Warehouse: ' || IO_l10n_obj.source_id);

   open C_GET_FISCAL_ATTRIB_VWH;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_FISCAL_ATTRIB_VWH',
                    'v_wh, wh_l10n_ext ',
                    'Physical Warehouse: ' || IO_l10n_obj.source_id);

   fetch C_GET_FISCAL_ATTRIB_VWH BULK COLLECT INTO L_fiscal_attr_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_FISCAL_ATTRIB_VWH',
                    'v_wh, wh_l10n_ext ',
                    'Physical Warehouse: ' || IO_l10n_obj.source_id);
   close C_GET_FISCAL_ATTRIB_VWH;

   -- Exit if no fiscal attributes exists for the pwh
   if L_fiscal_attr_tbl.count = 0 then
      return TRUE;
   end if;

   --- Insert fiscal attributes for vwh
   for i in L_fiscal_attr_tbl.first..L_fiscal_attr_tbl.last loop
      insert into wh_l10n_ext
      values (L_fiscal_attr_tbl(i).wh,
              L_fiscal_attr_tbl(i).l10n_country_id,
              L_fiscal_attr_tbl(i).group_id,
              L_fiscal_attr_tbl(i).varchar2_1,
              L_fiscal_attr_tbl(i).varchar2_2,
              L_fiscal_attr_tbl(i).varchar2_3,
              L_fiscal_attr_tbl(i).varchar2_4,
              L_fiscal_attr_tbl(i).varchar2_5,
              L_fiscal_attr_tbl(i).varchar2_6,
              L_fiscal_attr_tbl(i).varchar2_7,
              L_fiscal_attr_tbl(i).varchar2_8,
              L_fiscal_attr_tbl(i).varchar2_9,
              L_fiscal_attr_tbl(i).varchar2_10,
              L_fiscal_attr_tbl(i).number_11,
              L_fiscal_attr_tbl(i).number_12,
              L_fiscal_attr_tbl(i).number_13,
              L_fiscal_attr_tbl(i).number_14,
              L_fiscal_attr_tbl(i).number_15,
              L_fiscal_attr_tbl(i).number_16,
              L_fiscal_attr_tbl(i).number_17,
              L_fiscal_attr_tbl(i).number_18,
              L_fiscal_attr_tbl(i).number_19,
              L_fiscal_attr_tbl(i).number_20,
              L_fiscal_attr_tbl(i).date_21,
              L_fiscal_attr_tbl(i).date_22);
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_FISCAL_ATTRIB;
------------------------------------------------------------------------------
FUNCTION VALIDATE_JURISDICTION (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                O_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                                O_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                                IO_state              IN OUT   STATE.STATE%TYPE,
                                IO_country            IN OUT   STATE.COUNTRY_ID%TYPE,
                                I_jurisdiction        IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)

RETURN BOOLEAN AS

   L_program             VARCHAR2(100) := 'L10N_BR_FND_SQL.VALIDATE_JURISDICTION';
   L_jurisdiction_desc   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   L_state_desc          STATE.DESCRIPTION%TYPE;
   L_country_desc        COUNTRY.COUNTRY_DESC%TYPE;

   cursor C_JURIS_INFO is
      select ctj.jurisdiction_desc,
             st.description,
             cy.country_desc,
             ctj.state,
             ctj.country_id
        from country_tax_jurisdiction ctj,
             country cy,
             state st
       where ctj.jurisdiction_code = I_jurisdiction
         and ctj.state = st.state
         and st.country_id = cy.country_id
         and ctj.country_id = cy.country_id
         and st.state = NVL(IO_state, st.state)
         and cy.country_id = NVL(IO_country, cy.country_id) ;

BEGIN

   if I_jurisdiction is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'Jurisdiction', L_program);
      return FALSE;
   end if;

   open C_JURIS_INFO;
   fetch C_JURIS_INFO into L_jurisdiction_desc,
                           L_state_desc,
                           L_country_desc,
                           IO_state,
                           IO_country;
   close C_JURIS_INFO;

   if L_jurisdiction_desc is NULL and (IO_state is NOT NULL or IO_country is NOT NULL )then
      O_error_message := SQL_LIB.CREATE_MSG('NO_CNTRY_ST_JURIS', NULL);
      return FALSE;
      --

   elsif L_jurisdiction_desc is NULL and (IO_country is NULL or IO_state is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TAX_JURIS',NULL);
      return FALSE;

   else
      if LANGUAGE_SQL.TRANSLATE(L_jurisdiction_desc,
                                O_jurisdiction_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
      if LANGUAGE_SQL.TRANSLATE(L_state_desc,
                                O_state_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
      if LANGUAGE_SQL.TRANSLATE(L_country_desc,
                                O_country_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_JURISDICTION;
---------------------------------------------------------------------
FUNCTION VALIDATE_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_country_desc    IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                          I_country         IN       COUNTRY.COUNTRY_ID%TYPE,
                          I_jurisdiction    IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                          I_state           IN       STATE.STATE%TYPE)
RETURN BOOLEAN AS

   L_program        VARCHAR2(100) := 'L10N_BR_FND_SQL.VALIDATE_COUNTRY';
   L_country_desc   COUNTRY.COUNTRY_DESC%TYPE;

   cursor C_COUNTRY_DESC is
      select cy.country_desc
        from country cy,
             state st,
             country_tax_jurisdiction ctj
       where cy.country_id = I_country
         and ctj.jurisdiction_code = NVL(I_jurisdiction, ctj.jurisdiction_code)
         and ctj.state = st.state
         and st.country_id = cy.country_id
         and ctj.country_id = cy.country_id
         and st.state = NVL(I_state, st.state);

BEGIN

   if I_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Country');
      return FALSE;
   end if;

   open C_COUNTRY_DESC;
   fetch C_COUNTRY_DESC into L_country_desc;
   close C_COUNTRY_DESC;


   if (L_country_desc is NULL and I_state is NOT NULL and I_jurisdiction is NOT NULL) or
      (L_country_desc is NULL and I_jurisdiction is NOT NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('NO_CNTRY_ST_JURIS', NULL);
      return FALSE;

   elsif L_country_desc is NULL and I_state is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_COUNTRY_STATE', NULL);
      return FALSE;
      --

   elsif L_country_desc is NULL and (I_state is NULL or I_jurisdiction is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_COUNTRY', NULL);
      return FALSE;
      --

   else
      if LANGUAGE_SQL.TRANSLATE(L_country_desc,
                                O_country_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_COUNTRY;
---------------------------------------------------------------------
FUNCTION DEFAULT_ADDRESS_TO_ATTRIB(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_add_1              IN OUT ADDR.ADD_1%TYPE,
                                   IO_add_2              IN OUT ADDR.ADD_2%TYPE,
                                   IO_add_3              IN OUT ADDR.ADD_3%TYPE,
                                   IO_state              IN OUT ADDR.STATE%TYPE,
                                   IO_state_desc         IN OUT STATE.DESCRIPTION%TYPE,
                                   IO_country_id         IN OUT ADDR.COUNTRY_ID%TYPE,
                                   IO_country_desc       IN OUT COUNTRY.COUNTRY_DESC%TYPE,
                                   IO_post               IN OUT ADDR.POST%TYPE,
                                   IO_jurisdiction_code  IN OUT ADDR.JURISDICTION_CODE%TYPE,
                                   IO_jurisdiction_desc  IN OUT COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                   I_key_value_1         IN     ADDR.KEY_VALUE_1%TYPE,
                                   I_key_value_2         IN     ADDR.KEY_VALUE_2%TYPE,
                                   I_module              IN     ADDR.MODULE%TYPE)
RETURN BOOLEAN AS

L_program     VARCHAR2(50) := 'L10N_BR_FND_SQL.DEFAULT_ADDRESS_TO_ATTRIB';
L_exists      BOOLEAN;
L_city        ADDR.CITY%TYPE;
L_module_id   VARCHAR2(10);

   cursor C_GET_MODULE_ID is
     select decode(store_type,'C','ST','WFST') loc_type
       from store
      where store = I_key_value_1
      UNION ALL
     select decode(store_type,'C','ST','WFST') loc_type
       from store_add
      where store = I_key_value_1;

BEGIN
   L_module_id := I_module;

   -- Get the module id
   if I_module = 'ST' then

      open C_GET_MODULE_ID;
      fetch C_GET_MODULE_ID  into L_module_id;
      close C_GET_MODULE_ID;

   end if;

   if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                IO_add_1,
                                IO_add_2,
                                IO_add_3,
                                L_city,
                                IO_state,
                                IO_country_id,
                                IO_post,
                                IO_jurisdiction_code,
                                L_module_id,
                                I_key_value_1,
                                I_key_value_2)   then
      if COUNTRY_TAX_JURS_SQL.GET_JURS_DESC(O_error_message,
          	                                L_exists,
                                            IO_jurisdiction_desc,
                                            IO_country_id,
                                            IO_state,
                                            IO_jurisdiction_code) = FALSE  then
         return FALSE;
      end if;

      if GEOGRAPHY_SQL.COUNTRY_DESC(O_error_message,
			                  IO_country_id,
                             		IO_country_desc) = FALSE then
         return FALSE;
      end if;

      if GEOGRAPHY_SQL.STATE_DESC(O_error_message,
                                  IO_state_desc,
                                  IO_state,
                                  IO_country_id) = FALSE then
         return FALSE;
      end if;

      return TRUE;
   else
      return FALSE;
    end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_ADDRESS_TO_ATTRIB;
---------------------------------------------------------------------
FUNCTION CHK_ADDR_UPDATE(O_error_msg         IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_exists           IN  OUT VARCHAR2,
                         I_group_id          IN      L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                         I_addr_1            IN      ADDR.ADD_1%TYPE,
                         I_addr_2            IN      ADDR.ADD_2%TYPE,
                         I_addr_3            IN      ADDR.ADD_3%TYPE,
                         I_jurisdiction_code IN      ADDR.JURISDICTION_CODE%TYPE,
                         I_state             IN      ADDR.STATE%TYPE,
                         I_country           IN      ADDR.COUNTRY_ID%TYPE,
                         I_postal_code       IN      ADDR.POST%TYPE,
                         I_where_string      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'L10N_BR_FND_SQL.CHK_ADDR_UPDATE';
   L_group_view_name  L10N_ATTRIB_GROUP.GROUP_VIEW_NAME%TYPE;
   L_select_string    VARCHAR2(4000) := NULL;

   cursor C_GET_ADDR_VIEW is
      select group_view_name
       from l10n_attrib_group
      where group_id = I_group_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ADDR_VIEW', 'l10n_attrib_group', I_group_id);
   open C_GET_ADDR_VIEW;
   --
   SQL_LIB.SET_MARK('FETCH','C_GET_ADDR_VIEW','l10n_attrib_group', I_group_id);
   fetch C_GET_ADDR_VIEW into L_group_view_name;
   --
   SQL_LIB.SET_MARK('CLOSE','C_GET_ADDR_VIEW','l10n_attrib_group', I_group_id);
   close C_GET_ADDR_VIEW;
   --
   L_select_string := ' select ' || '''' || 'Y' || '''' ||
                      ' from '|| L_group_view_name ||
                      ' where ' || I_where_string ||
                      ' and ( addr_1 <> ' || '''' || I_addr_1 || '''' ||
                            ' or (nvl(addr_2,' ||-99 ||') <> nvl(' || ''''|| I_addr_2 || '''' || ',' || '-99' ||') )' ||
                            ' or (nvl(addr_3,'||-99 || ') <> nvl(' || ''''|| I_addr_3 || ''''|| ',' || '-99' ||') )' ||
                            ' or  city <> ' || ''''|| I_jurisdiction_code || '''' ||
                            ' or  state <> ' || ''''|| I_state || '''' ||
                            ' or  nvl(postal_code, ' || -99 || ') <> nvl(' || ''''|| I_postal_code || '''' ||',' || '-99'|| ') )';

   EXECUTE IMMEDIATE l_select_string into IO_exists ;

   return TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      IO_exists := 'N';
      return TRUE;
   when OTHERS then
      o_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
      return FALSE;
END CHK_ADDR_UPDATE;
-------------------------------------------------------------------------------------------------------
FUNCTION ADDR_UPDATE_L10N_EXT(O_error_msg    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_l10n_obj    IN OUT   L10N_OBJ)
RETURN BOOLEAN IS
   L_program             VARCHAR2(60)   := 'L10N_BR_FND_SQL.ADDR_UPDATE_L10N_EXT';

   L_l10n_ext_table      EXT_ENTITY.L10N_EXT_TABLE%TYPE;
   L_update_string       VARCHAR2(4000);
   L_base_table          VARCHAR2(20);
   L_group_id            L10N_ATTRIB_GROUP.GROUP_ID%TYPE;
   L_addr_1              ADDR.ADD_1%TYPE;
   L_addr_2              ADDR.ADD_2%TYPE;
   L_addr_3              ADDR.ADD_3%TYPE;
   L_jurisdiction_code   ADDR.JURISDICTION_CODE%TYPE;
   L_state               ADDR.STATE%TYPE;
   L_country             ADDR.COUNTRY_ID%TYPE;
   L_postal_code         ADDR.POST%TYPE;
   L_city                ADDR.CITY%TYPE;
   L_where_string        VARCHAR2(4000);
   L_module_id           VARCHAR2(10);
   L_store_add_ind       VARCHAR2(1)     := 'N';
   L_exists              VARCHAR2(1)     := 'N';
   L_source_id           ADDR.KEY_VALUE_1%TYPE ;
   L_source_type         ADDR.KEY_VALUE_2%TYPE ;



   --Declaring an object of the subtype. No need to initialize the object using the constructor function.
   L_l10n_obj_sub   L10N_EXISTS_REC;


   TYPE COLUMN_TBL IS TABLE OF ALL_TAB_COLUMNS.COLUMN_NAME%TYPE INDEX BY BINARY_INTEGER;
   L_key_col COLUMN_TBL;
   TYPE view_col_rec IS RECORD
   (
     view_col_names    COLUMN_TBL,
     group_ids         ID_TBL,
     tab_col_names     COLUMN_TBL
   );
   L_view_cols   view_col_rec;

   cursor C_GET_MODULE_ID is
     select decode(store_type,'C','ST','WFST') loc_type
       from store
      where store = L_l10n_obj_sub.source_id
      UNION ALL
     select decode(store_type,'C','ST','WFST') loc_type
       from store_add
      where store = L_l10n_obj_sub.source_id
      UNION ALL
     select 'WH' loc_type
       from wh
      where wh = L_l10n_obj_sub.source_id;

   cursor C_GET_BASE_TABLE is
     select decode(L_l10n_obj_sub.source_entity, 'SUPP','SUPS',
                                                 'ST' ,'STORE',
                                                 'WH'  ,'WH',
                                                 'PTNR','PARTNER',
                                                 'OLOC','OUTLOC',
                                                 'WFST','STORE')
       from dual;

   cursor C_GET_STORE_BASE_TAB is
     select 'Y'
       from store_add
      where store = to_number(L_l10n_obj_sub.source_id);

   cursor C_GET_GROUP_ID is
      select grp.group_id
        from ext_entity ent,
             l10n_attrib_group grp,
             l10n_attrib attr
       where ent.ext_entity_id = grp.ext_entity_id
         and grp.group_id = attr.group_id
         and attr.view_col_name = 'ADDR_1'
         and ent.base_rms_table = L_base_table;

  cursor C_VIEW_COLS is
      select view_col_name,
             group_id,
             attrib_storage_col tab_col_name
        from l10n_attrib
       where group_id = L_group_id
         and view_col_name in ('ADDR_1','ADDR_2','ADDR_3','JURISDICTION_CODE','STATE','COUNTRY','POSTAL_CODE')
       order by attrib_id;

   cursor C_GET_KEY_COL is
      select key_col
        from ext_entity_key
       where base_rms_table = L_base_table
        order by key_number desc;

BEGIN
   -- IO_l10n_obj to subtype object, so that IO_l10n_obj.exists_ind can be used in the program
   L_l10n_obj_sub := treat(IO_l10n_obj AS L10N_EXISTS_REC);

   -- Get the module id
   if L_l10n_obj_sub.source_entity = 'LOC' then

      SQL_LIB.SET_MARK('OPEN','C_GET_MODULE_ID','STORE', L_l10n_obj_sub.source_id);
      open C_GET_MODULE_ID;
      --
      SQL_LIB.SET_MARK('FETCH','C_GET_MODULE_ID','STORE', L_l10n_obj_sub.source_id);
      fetch C_GET_MODULE_ID  into L_module_id;
      --
      SQL_LIB.SET_MARK('CLOSE','C_GET_MODULE_ID','STORE', L_l10n_obj_sub.source_id);
      close C_GET_MODULE_ID;

   end if;
   --
   if L_module_id is not null then
      L_l10n_obj_sub.source_entity := L_module_id;
   end if;

   -- Get the Base Table
   SQL_LIB.SET_MARK('OPEN','C_GET_BASE_TABLE','DUAL', L_l10n_obj_sub.source_entity);
   open C_GET_BASE_TABLE;
   --
   SQL_LIB.SET_MARK('FETCH','C_GET_BASE_TABLE','DUAL', L_l10n_obj_sub.source_entity);
   fetch C_GET_BASE_TABLE into L_base_table;
   --
   SQL_LIB.SET_MARK('CLOSE','C_GET_BASE_TABLE','DUAL', L_l10n_obj_sub.source_entity);
   close C_GET_BASE_TABLE;

   -- Get the Base Table if module is STORE or WFST

   if L_l10n_obj_sub.source_entity in ('ST','WFST') then

      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_BASE_TAB', 'store_add', L_l10n_obj_sub.source_id);
      open C_GET_STORE_BASE_TAB;
      --
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_BASE_TAB','store_add', L_l10n_obj_sub.source_id);
      fetch C_GET_STORE_BASE_TAB into L_store_add_ind;
      --
      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_BASE_TAB','store_add', L_l10n_obj_sub.source_id);
      close C_GET_STORE_BASE_TAB;

      if L_store_add_ind = 'Y' then
         L_base_table := 'STORE_ADD';
      end if;

   end if;

   --Set the extension table.
   L_l10n_ext_table := L_base_table||'_L10N_EXT';

   L_update_string := ' update '|| L_l10n_ext_table || ' set ';

   -- Get the Group Id.
   SQL_LIB.SET_MARK('OPEN','C_GET_GROUP_ID', 'ext_entity', L_base_table);
   open C_GET_GROUP_ID;
   --
   SQL_LIB.SET_MARK('FETCH','C_GET_GROUP_ID','ext_entity', L_base_table);
   fetch C_GET_GROUP_ID into L_group_id;
   --
   SQL_LIB.SET_MARK('CLOSE','C_GET_GROUP_ID','ext_entity', L_base_table);
   close C_GET_GROUP_ID;

   -- Get the ADDRESS details
   if L_l10n_obj_sub.source_entity = 'PTNR' then
      L_source_id   := L_l10n_obj_sub.source_type;
      L_source_type := L_l10n_obj_sub.source_id;
   else
      L_source_id   := L_l10n_obj_sub.source_id;
      L_source_type := L_l10n_obj_sub.source_type;
   end if;
   if ADDRESS_SQL.GET_PRIM_ADDR(O_error_msg,
                                L_addr_1,
                                L_addr_2,
                                L_addr_3,
                                L_city,
                                L_state,
                                L_country,
                                L_postal_code,
                                L_jurisdiction_code,
                                L_l10n_obj_sub.source_entity,
                                L_source_id,
                                L_source_type
                               ) = FALSE then
       return FALSE;
    end if;

   -- Get the Key columns
   SQL_LIB.SET_MARK('OPEN','C_GET_KEY_COL', 'ext_entity_key', L_base_table);
   open C_GET_KEY_COL;
   --
   SQL_LIB.SET_MARK('FETCH', 'C_GET_KEY_COL', 'ext_entity_key', L_base_table);
   fetch C_GET_KEY_COL bulk collect  into L_key_col;
   --
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_KEY_COL', 'ext_entity_key', L_base_table);
   close C_GET_KEY_COL;

   for i in L_key_col.FIRST .. L_key_col.LAST loop
      if i = 1 then
         L_where_string :=' to_char('||L_key_col(i)||') = to_char('||''''||L_l10n_obj_sub.source_id||''''||')';
      elsif i = 2 then
         L_where_string := L_where_string ||
    	                     ' and nvl ('|| L_key_col(i)||','||'-99'||')=nvl('||''''||L_l10n_obj_sub.source_type||''''||','||'-99'||')';
      end if;
   end loop;

   --Check whther to update the address in the respective fiscal table or not
   if CHK_ADDR_UPDATE(O_error_msg,
                      L_exists,
                      L_group_id,
                      L_addr_1,
                      L_addr_2,
                      L_addr_3,
                      L_jurisdiction_code,
                      L_state,
                      L_country,
                      L_postal_code,
                      L_where_string
                      ) = FALSE then
      return FALSE;
   end if;

   if L_exists = 'Y' then

      --Set the SET clause of Update Statement
      SQL_LIB.SET_MARK('OPEN', 'C_VIEW_COLS', 'l10n_attrib', L_group_id);
      open C_VIEW_COLS;
      --
      SQL_LIB.SET_MARK('FETCH', 'C_VIEW_COLS', 'l10n_attrib', L_group_id);
      fetch C_VIEW_COLS bulk collect into L_view_cols.view_col_names,
                                          L_view_cols.group_ids,
                                          L_view_cols.tab_col_names;
      --
      SQL_LIB.SET_MARK('CLOSE','C_VIEW_COLS','l10n_attrib', L_group_id);
      close C_VIEW_COLS;

      for i in L_view_cols.tab_col_names.FIRST .. L_view_cols.tab_col_names.LAST loop
         if i=1 then
            L_update_string := L_update_string || L_view_cols.tab_col_names(i) || '=' ||''''|| L_addr_1 ||''''||',';
         elsif i=2 then
            L_update_string := L_update_string || L_view_cols.tab_col_names(i) || '='||''''|| L_addr_2 ||''''||',';
         elsif i=3 then
            L_update_string := L_update_string || L_view_cols.tab_col_names(i) || '=' ||''''|| L_addr_3 ||''''||',';
         elsif i=4 then
            L_update_string := L_update_string || L_view_cols.tab_col_names(i) || '=' ||''''|| L_jurisdiction_code ||''''||',';
         elsif i=5 then
            L_update_string := L_update_string || L_view_cols.tab_col_names(i) || '=' ||''''|| L_state    ||''''||',';
         elsif i=6 then
            L_update_string := L_update_string || L_view_cols.tab_col_names(i) || '=' ||''''|| L_country  ||''''||',';
         elsif i=7 then
            L_update_string := L_update_string || L_view_cols.tab_col_names(i) || '=' || ''''||L_postal_code ||'''';
         end if;
      end loop;

      L_update_string := L_update_string ||
    	                   ' where  group_id = ' || L_group_id ||
    	                   ' and ' || L_where_string;

      EXECUTE IMMEDIATE L_update_string;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      o_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
      return FALSE;
END ADDR_UPDATE_L10N_EXT;
-----------------------------------------------------------------------------------------------------
END L10N_BR_FND_SQL;
/