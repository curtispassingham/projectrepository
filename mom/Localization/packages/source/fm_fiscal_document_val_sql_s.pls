CREATE OR REPLACE PACKAGE FM_FISCAL_DOCUMENT_VAL_SQL is
----------------------------------------------------------------------------------
-- Function Name: GET_ENTITY_INFO
-- Purpose:       This function gets the entity informations.
----------------------------------------------------------------------------------
FUNCTION GET_ENTITY_INFO(O_error_message  IN OUT VARCHAR2,
                         O_cnpjcfp_type   IN OUT V_FISCAL_ATTRIBUTES.TYPE%TYPE,
                         O_cnpjcfp        IN OUT V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
                         O_addr_1         IN OUT V_FISCAL_ATTRIBUTES.ADDR_1%TYPE,
                         O_addr_2         IN OUT V_FISCAL_ATTRIBUTES.ADDR_2%TYPE,
                         O_addr_3         IN OUT V_FISCAL_ATTRIBUTES.ADDR_3%TYPE,
                         O_postal_code    IN OUT V_FISCAL_ATTRIBUTES.POSTAL_CODE%TYPE,
                         O_city           IN OUT V_FISCAL_ATTRIBUTES.CITY%TYPE,
                         O_district       IN OUT V_FISCAL_ATTRIBUTES.NEIGHBORHOOD%TYPE,
                         O_state          IN OUT V_FISCAL_ATTRIBUTES.STATE%TYPE,
                         O_ie             IN OUT V_FISCAL_ATTRIBUTES.IE%TYPE,
                       --  O_ie_subs        IN OUT V_FISCAL_ATTRIBUTES.IE_SUBS%TYPE,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_CITY_DESC
-- Purpose:       This function gets the entity informations.
----------------------------------------------------------------------------------
FUNCTION GET_CITY_DESC(O_error_message  IN OUT VARCHAR2,
                       O_city_desc      IN OUT COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                       I_city           IN     COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_SUPP
-- Purpose:       This function gets the Supplier name.
----------------------------------------------------------------------------------
FUNCTION GET_SUPP(O_error_message  IN OUT VARCHAR2,
                  O_supp_desc      IN OUT SUPS.SUP_NAME%TYPE,
                  I_supp           IN     SUPS.SUPPLIER%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_CUSTOMER
-- Purpose:       This function gets the customer name.
----------------------------------------------------------------------------------

FUNCTION GET_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                      O_cust_desc      IN OUT CUSTOMER.CUST_NAME%TYPE,
                      I_customer       IN     CUSTOMER.CUST_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_PARTNER
-- Purpose:       This function gets the Partner name.
----------------------------------------------------------------------------------

FUNCTION GET_PARTNER(O_error_message  IN OUT VARCHAR2,
                     O_part_desc      IN OUT PARTNER.PARTNER_DESC%TYPE,
                     I_partner        IN     PARTNER.PARTNER_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_LOC
-- Purpose:       This function gets the location name.
----------------------------------------------------------------------------------

FUNCTION GET_LOC(O_error_message  IN OUT VARCHAR2,
                 O_loc_desc       IN OUT STORE.STORE_NAME%TYPE,
                 I_loc            IN     STORE.STORE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_OUTLOC
-- Purpose:       This function gets the out location name.
----------------------------------------------------------------------------------

FUNCTION GET_OUTLOC(O_error_message  IN OUT VARCHAR2,
                    O_outloc_desc    IN OUT OUTLOC.OUTLOC_DESC%TYPE,
                    I_outloc         IN     OUTLOC.OUTLOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_COMP
-- Purpose:       This function gets the company name.
----------------------------------------------------------------------------------

FUNCTION GET_COMP(O_error_message  IN OUT VARCHAR2,
                  O_comp_desc      IN OUT COMPHEAD.CO_NAME%TYPE,
                  I_comp           IN     COMPHEAD.COMPANY%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_MESSAGE
-- Purpose:       This function gets the messages of the fiscal document.
----------------------------------------------------------------------------------
END FM_FISCAL_DOCUMENT_VAL_SQL;
/
 