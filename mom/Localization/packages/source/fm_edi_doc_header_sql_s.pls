CREATE OR REPLACE PACKAGE FM_EDI_DOC_HEADER_SQL is
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SCHEDULE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,O_rma_exists     IN OUT BOOLEAN
                          ,O_edi_exists     IN OUT BOOLEAN
                          ,O_status         IN OUT FM_SCHEDULE.STATUS%TYPE
                          ,I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION CHECK_EDI_RMA_LOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,O_rma_exists     IN OUT BOOLEAN
                          ,O_edi_exists     IN OUT BOOLEAN
                          ,I_loc_type       IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE
                          ,I_location       IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------   
-- Verify if Edi Doc already Exists
FUNCTION EXISTS_FISCAL_DOC_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists         IN OUT BOOLEAN,
                              I_fiscal_doc_no  IN     FM_EDI_DOC_HEADER.FISCAL_DOC_NO%TYPE)
   return BOOLEAN;

--------------------------------------------------------------------------------
-- Update Process Status
-- Various values for I_nf_type_ind is M (Main NF), C (Complementary NF), B (Both Main and compl NF)
FUNCTION UPDATE_STATUS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                       I_status         IN     FM_EDI_DOC_HEADER.PROCESS_STATUS%TYPE,
                       I_nf_type_ind    IN     VARCHAR2 DEFAULT 'M')

   return BOOLEAN;
--------------------------------------------------------------------------------
-- Update Process Status
FUNCTION UPDATE_FISCAL_DOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                           I_fiscal_doc_id  IN     FM_EDI_DOC_HEADER.FISCAL_DOC_ID%TYPE)

   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXISTS_LOC_UTIL_NFE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists         IN OUT BOOLEAN,
                             I_location       IN     FM_NFE_CONFIG_LOC.LOCATION%TYPE,
                             I_utilization    IN     FM_NFE_CONFIG_LOC.UTILIZATION_ID%TYPE)
   return BOOLEAN;

--------------------------------------------------------------------------------
END FM_EDI_DOC_HEADER_SQL;
/
 
