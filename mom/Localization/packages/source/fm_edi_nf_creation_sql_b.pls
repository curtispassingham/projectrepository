CREATE OR REPLACE PACKAGE BODY FM_EDI_NF_CREATION_SQL is
-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message  IN OUT VARCHAR2,
                          O_query          IN OUT VARCHAR2,
                          I_supplier       IN FM_EDI_DOC_HEADER.KEY_VALUE_1%TYPE,
                          I_fiscal_doc_no  IN FM_EDI_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_nfe_access_key IN FM_EDI_DOC_HEADER.NFE_ACCESS_KEY%TYPE,
                          I_location_type  IN FM_EDI_DOC_HEADER.LOCATION_TYPE%TYPE,
                          I_location_id    IN FM_EDI_DOC_HEADER.LOCATION_ID%TYPE)
 return BOOLEAN is
   L_program VARCHAR2(50) := 'FM_EDI_NF_CREATION_SQL.SET_WHERE_CLAUSE';

BEGIN
   O_query := ' process_status=''U'' ' ||
              ' and requisition_type=''PO'' ' ||
              ' and module =''SUPP'' ' ||
              ' and location_type = '''||I_location_type||''' '||
              ' and location_id = '||I_location_id;

   if I_supplier is NOT NULL then
      O_query := O_query || ' and key_value_1 = '|| I_supplier ;
   end if;

   if I_fiscal_doc_no is NOT NULL  then
      O_query := O_query || ' and fiscal_doc_no = '||I_fiscal_doc_no;
   end if;

   if I_nfe_access_key is NOT NULL  then
      O_query := O_query || ' and nfe_Access_key= '||I_nfe_access_key;
   end if;

   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1, 254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE;

-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO(O_error_message  IN OUT VARCHAR2,
                           O_query          IN OUT VARCHAR2,
                           I_location_type  IN     FM_EDI_DOC_HEADER.LOCATION_TYPE%TYPE,
                           I_location_id    IN     FM_EDI_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_EDI_NF_CREATION_SQL.LOV_FISCAL_DOC_NO';
   ---
BEGIN
   ---
   O_query := 'select distinct edh.fiscal_doc_no fiscal_doc_no'||
                ' from fm_edi_doc_header edh '||
               'where edh.module = ''SUPP'' '||
               ' and edh.location_type = '''||I_location_type||''' '||
                 ' and edh.location_id = '||I_location_id ||
                 ' and edh.requisition_type = ''PO'' '||
               ' order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_FISCAL_DOC_NO;

-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOV_NFE_ACCESS_KEY(O_error_message  IN OUT VARCHAR2,
                        O_query          IN OUT VARCHAR2,
                        I_location_type  IN     FM_EDI_DOC_HEADER.LOCATION_TYPE%TYPE,
                        I_location_id    IN     FM_EDI_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_EDI_NF_CREATION_SQL.LOV_NFE_ACCESS_KEY';
   ---
BEGIN
   ---
   O_query := 'select nfe_access_key'||
                ' from fm_edi_doc_header edh '||
               'where edh.module = ''SUPP'' '||
               ' and edh.location_type = '''||I_location_type||''' '||
                 ' and edh.location_id = '||I_location_id ||
                 ' and edh.requisition_type = ''PO'' '||
		 ' and edh.nfe_access_key is NOT NULL '||
               ' order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_NFE_ACCESS_KEY;
-------------------------------------------------------------------------------------------------------------------------------------------------------
 FUNCTION EXISTS_NFE_ACCESS_KEY(O_error_message  IN OUT VARCHAR2,
                                O_exists         IN OUT BOOLEAN,
                                I_nfe_access_key IN     FM_EDI_DOC_HEADER.NFE_ACCESS_KEY%TYPE)
     return BOOLEAN is
     ---
     L_program   VARCHAR2(50) := 'FM_EDI_NF_CREATION_SQL.EXISTS_NFE_ACCESS_KEY';
     L_dummy     VARCHAR2(1);
     ---
     cursor C_FM_EDI is
        select '1'
          from fm_edi_doc_header
         where nfe_access_key = I_nfe_access_key;
     ---
  BEGIN
     ---
     SQL_LIB.SET_MARK('OPEN','C_FM_EDI','FM_EDI_DOC_HEADER','NFE ACCESS KEY: '||I_nfe_access_key);
     open C_FM_EDI;
     ---
     SQL_LIB.SET_MARK('FETCH','C_FM_EDI','FM_EDI_DOC_HEADER','NFE ACCESS KEY: '||I_nfe_access_key);
     fetch C_FM_EDI into L_dummy;
     O_exists := C_FM_EDI%FOUND;
     ---
     SQL_LIB.SET_MARK('CLOSE','C_FM_EDI','FM_EDI_DOC_HEADER','NFE ACCESS KEY: '||I_nfe_access_key);
     close C_FM_EDI;
     ---
     return TRUE;
     ---
  EXCEPTION
     when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
        return FALSE;

  END EXISTS_NFE_ACCESS_KEY;
----------------------------------------------------------------------------------

end FM_EDI_NF_CREATION_SQL;
/