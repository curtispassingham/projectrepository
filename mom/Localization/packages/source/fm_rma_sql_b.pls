CREATE OR REPLACE PACKAGE BODY FM_RMA_SQL AS

PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2);

---------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
--------------------------------------------------------------------------------
PROCEDURE POPULATE_RMA(O_error_message      IN OUT VARCHAR2,
                       O_status_code        IN OUT VARCHAR2,
                       I_rmaitemdesc_rec    IN     "OBJ_RMAITEMDesc_REC") 
IS
L_module  VARCHAR2(100)  := 'FM_RMA_SQL.POPULATE_RMA';
BEGIN
--- Populate RMA Header
      savepoint before_insert;
      insert into fm_rma_head(rma_id,
                              pro_nbr,
                              loc_type,
                              location,
                              cust_id,
                              cust_name,
                              rma_date,
                              auth_by,
                              rma_status,
                              close_date,
                              cnpj,
                              cpf,
                              addr_1,
                              addr_2,
                              addr_3,
                              neighborhood,
                              city,
                              state,
                              postal_code,
                              country_id,
                              suframa,
                              ie,
                              create_datetime,
                              create_id,
                              last_update_datetime,
                              last_update_id)
                       values(I_rmaitemdesc_rec.rma_id,
                              I_rmaitemdesc_rec.pro_nbr,
                              I_rmaitemdesc_rec.loc_type,
                              I_rmaitemdesc_rec.location,
                              I_rmaitemdesc_rec.cust_id,
                              I_rmaitemdesc_rec.cust_name,
                              I_rmaitemdesc_rec.rma_date,
                              I_rmaitemdesc_rec.auth_by,
                              I_rmaitemdesc_rec.rma_status,
                              I_rmaitemdesc_rec.close_date,
                              I_rmaitemdesc_rec.cnpj,
                              I_rmaitemdesc_rec.cpf,
                              I_rmaitemdesc_rec.addr_1,
                              I_rmaitemdesc_rec.addr_2,
                              I_rmaitemdesc_rec.addr_3,
                              I_rmaitemdesc_rec.neighborhood,
                              I_rmaitemdesc_rec.city,
                              I_rmaitemdesc_rec.state,
                              I_rmaitemdesc_rec.postal_code,
                              I_rmaitemdesc_rec.country_id,
                              I_rmaitemdesc_rec.suframa,
                              I_rmaitemdesc_rec.ie,
                              SYSDATE,
                              USER,
                              SYSDATE,
                              USER);
                              
--- Populate RMA detail            
      for i in I_rmaitemdesc_rec.RMADtl_TBL.first .. I_rmaitemdesc_rec.RMADtl_TBL.last
         loop
            insert into fm_rma_detail(rma_id,
                                      rma_detail_id,
                                      qty,
                                      item,
                                      rma_reason,
                                      origin_document,
                                      from_disposition,
                                      to_disposition,
                                      unit_cost,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
                               values(I_rmaitemdesc_rec.RMADtl_TBL(i).rma_id,
                                      I_rmaitemdesc_rec.RMADtl_TBL(i).rma_detail_id,
                                      I_rmaitemdesc_rec.RMADtl_TBL(i).qty,
                                      I_rmaitemdesc_rec.RMADtl_TBL(i).item,
                                      I_rmaitemdesc_rec.RMADtl_TBL(i).rma_reason,
                                      I_rmaitemdesc_rec.RMADtl_TBL(i).origin_document,
                                      I_rmaitemdesc_rec.RMADtl_TBL(i).from_disposition,
                                      I_rmaitemdesc_rec.RMADtl_TBL(i).to_disposition,
                                      I_rmaitemdesc_rec.RMADtl_TBL(i).unit_cost,
                                      SYSDATE,
                                      USER,
                                      SYSDATE,
                                      USER);
          end loop;
EXCEPTION
   when OTHERS then
      rollback to before_insert;
      O_status_code := 'E';
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);          
END POPULATE_RMA;
--------------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code          IN OUT  VARCHAR2,
                   O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_rmadesc_rec          IN      "OBJ_RMAITEMDesc_REC",
                   I_message_type         IN      VARCHAR2)

IS
   L_rmaitemdesc_rec      "OBJ_RMAITEMDesc_REC" := I_rmadesc_rec;
   L_rmaitem_rec          "OBJ_RMAItem_REC"     := NULL;
   L_rmaitem_tbl          "OBJ_RMAItem_TBL"     := NULL;
   L_module               VARCHAR2(64)          := 'FM_RMA_SQL.CONSUME';
   L_fiscal_id            FM_RMA_HEAD.FISCAL_DOC_ID%TYPE;
   L_wh_name              WH.WH_NAME%TYPE;
   L_count                NUMBER(10);
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100);
   L_key          VARCHAR2(100);
   L_dummy     VARCHAR2(1);

   cursor c_fiscal_id(P_rma_id FM_RMA_HEAD.RMA_ID%TYPE)  is
      select fiscal_doc_id
        from fm_rma_head
       where rma_id = P_rma_id;

   cursor C_EXISTS_SCHED  is
       select 'X'
        from fm_schedule fs, fm_fiscal_doc_header fdh
       where fs.schedule_no in (select schedule_no
                               from fm_fiscal_doc_header fdh
                              where fdh.fiscal_doc_id = L_fiscal_id)  
                                and fdh.status  in ('W','E','V','P')
                                and fdh.schedule_no=fs.schedule_no;

    cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = L_fiscal_id
         for update nowait;

    cursor C_LOCK_RMA(P_rma_id FM_RMA_HEAD.RMA_ID%TYPE)  is
      select 'X'
        from fm_rma_head frh
       where frh.rma_id = P_rma_id
         for update nowait;

    cursor C_LOCK_SCHEDULE  is
      select 'X'
        from fm_schedule fs
       where schedule_no in (select schedule_no
                               from fm_fiscal_doc_header fdh
                              where fdh.fiscal_doc_id = L_fiscal_id)
         for update nowait;

BEGIN
   L_rmaitemdesc_rec := "OBJ_RMAITEMDesc_REC"(NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL);
   L_rmaitem_rec := "OBJ_RMAItem_REC"(NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL);
   L_rmaitem_tbl        := "OBJ_RMAItem_TBL"();
   L_rmaitemdesc_rec    := I_rmadesc_rec;

  if L_rmaitemdesc_rec.rma_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'rma_id',
                                             L_module,
                                             NULL);
       raise PROGRAM_ERROR;
  end if;

  if L_rmaitemdesc_rec.rma_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'status',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

 if I_message_type = LP_cre_type then
   if L_rmaitemdesc_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'L_rmaitemdesc_rec',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   if L_rmaitemdesc_rec.loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'loc_type',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_rmaitemdesc_rec.location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'location',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   if L_rmaitemdesc_rec.cust_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'cust_id',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   if L_rmaitemdesc_rec.rma_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'rma_date',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_rmaitemdesc_rec.pro_nbr is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'pro_nbr',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_rmaitemdesc_rec.addr_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'addr_1',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   if L_rmaitemdesc_rec.neighborhood is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'neighborhood',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_rmaitemdesc_rec.city is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'city',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   if L_rmaitemdesc_rec.state is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'state',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   if L_rmaitemdesc_rec.postal_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'postal_code',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_rmaitemdesc_rec.country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'country_id',
                                            L_module,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                             L_rmaitemdesc_rec.location,
                             L_wh_name) = FALSE then

      O_error_message := SQL_LIB.CREATE_MSG('INV_WH',
                                            NULL,
                                            L_module,
                                            NULL);
   end if;
      for i in L_rmaitemdesc_rec.RMADtl_TBL.first .. L_rmaitemdesc_rec.RMADtl_TBL.last
         loop
         
            if L_rmaitemdesc_rec.RMADtl_TBL(i).rma_detail_id is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                     'rma_detail_id',
                                                     L_module,
                                                     NULL);
                    raise PROGRAM_ERROR;
            end if;
            if L_rmaitemdesc_rec.RMADtl_TBL(i).qty is NULL then
                O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                      'Quantity',
                                                       L_module,
                                                       NULL);
                    raise PROGRAM_ERROR;
            end if;
            if L_rmaitemdesc_rec.RMADtl_TBL(i).item is NULL then
                O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                      'item',
                                                       L_module,
                                                       NULL);
                    raise PROGRAM_ERROR;
            end if;
            if L_rmaitemdesc_rec.RMADtl_TBL(i).rma_reason is NULL then
                O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                      'rma_reason',
                                                       L_module,
                                                       NULL);
                    raise PROGRAM_ERROR;
            end if;
            if L_rmaitemdesc_rec.RMADtl_TBL(i).origin_document is NULL then
                O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                      'origin_document',
                                                       L_module,
                                                       NULL);
                    raise PROGRAM_ERROR;
            end if;           
          end loop;
          
        POPULATE_RMA(O_error_message,
                      O_status_code,
                      L_rmaitemdesc_rec);
        if O_status_code = 'E' then
            return;
        end if;
   else
     L_cursor := 'C_LOCK_RMA';
     L_table  := 'FM_RMA_HEAD';
     L_key    := 'RMA_ID: ' || TO_CHAR(L_rmaitemdesc_rec.rma_id);
     ---
     SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
     open C_LOCK_RMA(L_rmaitemdesc_rec.rma_id);
     SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
     close C_LOCK_RMA;
     ---
     SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
     --- Update rma head table
      update fm_rma_head
         set rma_status = L_rmaitemdesc_rec.rma_status,
             close_date = L_rmaitemdesc_rec.close_date
       where rma_id = L_rmaitemdesc_rec.rma_id
         and rma_status = 'R';

      open c_fiscal_id(L_rmaitemdesc_rec.rma_id);
      fetch c_fiscal_id into L_fiscal_id;
      if c_fiscal_id%NOTFOUND then
         close c_fiscal_id;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close c_fiscal_id;
      if L_rmaitemdesc_rec.rma_status = 'P' then
         --update fm_fiscal_Doc_header with new status
         L_cursor := 'C_LOCK_DOC_HEADER';
         L_table  := 'FM_FISCAL_DOC_HEADER';
         L_key    := 'FISCAL_DOC_ID: ' || TO_CHAR(L_fiscal_id);
         ---
         SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
         open C_LOCK_DOC_HEADER;
         SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
         close C_LOCK_DOC_HEADER;
         ---
         SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
         update fm_fiscal_doc_header
            set status = 'R'
          where fiscal_doc_id = L_fiscal_id;
         ---update fm_Schedule if all nfs are in P status
          open C_EXISTS_SCHED;
          ---
          fetch C_EXISTS_SCHED into L_dummy;
          if C_EXISTS_SCHED%NOTFOUND then
           L_cursor := 'C_LOCK_SCHEDULE';
           L_table  := 'FM_SCHEDULE';
           L_key    := 'FISCAL_DOC_ID: ' || TO_CHAR(L_fiscal_id);
           ---
           SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
           open C_LOCK_SCHEDULE;
           SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
           close C_LOCK_SCHEDULE;
           ---
           SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);

            update fm_schedule
               set status = 'A'
             where schedule_no = (select schedule_no
                                    from fm_fiscal_doc_header
                                    where fiscal_doc_id = L_fiscal_id);
          end if;
          ---
          close C_EXISTS_SCHED;

      end if;
   end if;
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);

END CONSUME;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2)
IS
   L_module  VARCHAR2(100)  := 'FM_RMA_SQL.HANDLE_ERRORS';

BEGIN
   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_module,
                                             To_Char(SQLCODE));
      ---
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_module);

END HANDLE_ERRORS;
----------------------------------------------------------------------------------------
END FM_RMA_SQL;
/
 
