CREATE OR REPLACE PACKAGE BODY FM_MATCH_DOC_SQL is
-----------------------------------------------------------------------------------
FUNCTION LOV_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                O_query         IN OUT VARCHAR2,
                I_order_no      IN ORDHEAD.ORDER_NO%TYPE,
                I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                I_supplier      IN FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                I_location_type IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                I_location_id   IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN is
   L_program             VARCHAR2(50) := 'FM_MATCH_DOC_SQL.LOV_PO';
   L_pwh                 BOOLEAN := FALSE;
   L_supplier_sites_ind  SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE; 
   L_comp_nf_ind         FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   ---
   cursor C_GET_SUPS_SITE_IND is
      select supplier_sites_ind
        from system_options;
   ---
   CURSOR C_GET_UTIL_COMP_NF_IND IS
      select comp_nf_ind
        from fm_utilization_attributes fua,
             fm_fiscal_doc_header fdh
       where fdh.utilization_id = fua.utilization_id 
         and fdh.fiscal_doc_id  =  I_fiscal_doc_id;
   ---
BEGIN
   --
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SUPS_SITE_IND',
                    'SYSTEM_OPTIONS',
                    ' Supplier = '||TO_CHAR(I_supplier));
   open C_GET_SUPS_SITE_IND;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SUPS_SITE_IND',
                    'SYSTEM_OPTIONS',
                    ' Supplier = '||TO_CHAR(I_supplier));
   fetch C_GET_SUPS_SITE_IND
      into L_supplier_sites_ind;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SUPS_SITE_IND',
                    'SYSTEM_OPTIONS',
                    ' Supplier = '||TO_CHAR(I_supplier));
   close C_GET_SUPS_SITE_IND;
   --
   ---
   open C_GET_UTIL_COMP_NF_IND;
   fetch C_GET_UTIL_COMP_NF_IND into L_comp_nf_ind;
   close C_GET_UTIL_COMP_NF_IND;
   ---
   if I_location_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_location_type = 'S' or
      NOT L_pwh then
      if I_order_no is NOT NULL then
         if L_comp_nf_ind = 'Y' then
            O_query := 'Select o.doc_no
                       from v_fm_requisition_docs o
                      where UPPER(o.doc_no) like UPPER(''%' ||I_order_no || '%'')
                        and delivery_supplier = ' || I_supplier || '
                        and o.status = ''A'' '||'
                        and o.requisition_type = ''PO'' '||
                       'and exists (select ''X'' '||
                                     'from v_ordloc loc '||
                                    'where loc.order_no = o.doc_no '||
                                      'and loc.location = '||I_location_id||' '||
                                      'and loc.loc_type = '''||I_location_type||''' '||
                                      'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                        'and not exists (select ''X''
                                          from fm_fiscal_doc_detail fdd
                                         where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                           and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
         else
            O_query := 'Select o.doc_no
                       from v_fm_requisition_docs o
                      where UPPER(o.doc_no) like UPPER(''%' ||I_order_no || '%'')
                        and supplier = ' || I_supplier || '
                        and o.status = ''A'' '||'
                        and o.requisition_type = ''PO'' '||
                       'and exists (select ''X'' '||
                                     'from v_ordloc loc '||
                                    'where loc.order_no = o.doc_no '||
                                      'and loc.location = '||I_location_id||' '||
                                      'and loc.loc_type = '''||I_location_type||''' '||
                                      'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                        'and not exists (select ''X''
                                          from fm_fiscal_doc_detail fdd
                                         where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                           and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
         end if;
      else
         if L_supplier_sites_ind = 'Y' then
            if L_comp_nf_ind = 'Y' then 
               O_query := 'Select o.doc_no
                          from v_fm_requisition_docs o
                         where
                            ( o.delivery_supplier in
                                          ( select s.supplier
                                               from sups s
                                              where s.supplier_parent = ' ||I_supplier|| ' ) or o.delivery_supplier = '||I_supplier||' )
                           and o.status = ''A''' || '
                           and o.requisition_type = ''PO'' '||
                          'and exists (select ''X'' '||
                                        'from v_ordloc loc '||
                                       'where loc.order_no = o.doc_no '||
                                         'and loc.location = '||I_location_id||' '||
                                         'and loc.loc_type = '''||I_location_type||''' '||
                                         'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                          'and not exists (select ''X''
                                             from fm_fiscal_doc_detail fdd
                                            where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                              and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
            else
               O_query := 'Select o.doc_no
                          from v_fm_requisition_docs o
                         where
                            ( o.supplier in
                                          ( select s.supplier
                                               from sups s
                                              where s.supplier_parent = ' ||I_supplier|| ' ) or o.supplier = '||I_supplier||' )
                           and o.status = ''A''' || '
                           and o.requisition_type = ''PO'' '||
                          'and exists (select ''X'' '||
                                        'from v_ordloc loc '||
                                       'where loc.order_no = o.doc_no '||
                                         'and loc.location = '||I_location_id||' '||
                                         'and loc.loc_type = '''||I_location_type||''' '||
                                         'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                          'and not exists (select ''X''
                                             from fm_fiscal_doc_detail fdd
                                            where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                              and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
            end if;
         else
            if L_comp_nf_ind = 'Y' then
               O_query := 'Select o.doc_no
                          from v_fm_requisition_docs o
                         where delivery_supplier = ' || I_supplier || '
                           and o.status = ''A''' || '
                           and o.requisition_type = ''PO'' '||
                          'and exists (select ''X'' '||
                                        'from v_ordloc loc '||
                                       'where loc.order_no = o.doc_no '||
                                         'and loc.location = '||I_location_id||' '||
                                         'and loc.loc_type = '''||I_location_type||''' '||
                                         'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                          'and not exists (select ''X''
                                             from fm_fiscal_doc_detail fdd
                                            where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                              and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
            else
               O_query := 'Select o.doc_no
                          from v_fm_requisition_docs o
                         where supplier = ' || I_supplier || '
                           and o.status = ''A''' || '
                           and o.requisition_type = ''PO'' '||
                          'and exists (select ''X'' '||
                                        'from v_ordloc loc '||
                                       'where loc.order_no = o.doc_no '||
                                         'and loc.location = '||I_location_id||' '||
                                         'and loc.loc_type = '''||I_location_type||''' '||
                                         'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                          'and not exists (select ''X''
                                             from fm_fiscal_doc_detail fdd
                                            where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                              and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
            end if;
         end if;
      end if;
      ---
   else
      ---
      if I_order_no is NOT NULL then
         if L_comp_nf_ind = 'Y' then
            O_query := 'Select o.doc_no
                       from v_fm_requisition_docs o
                      where UPPER(o.doc_no) like UPPER(''%' ||I_order_no || '%'')
                        and delivery_supplier = ' || I_supplier || '
                        and o.status = ''A'''|| '
                        and o.requisition_type = ''PO'' '||
                       'and exists (select ''X'' '||
                                     'from v_ordloc loc '||
                                    'where loc.order_no = o.doc_no '||
                                      'and loc.location IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id ||') '||
                                      'and loc.loc_type = '''||I_location_type||''' '||
                                      'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                        'and not exists (select ''X''
                                          from fm_fiscal_doc_detail fdd
                                         where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                           and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
         else
            O_query := 'Select o.doc_no
                       from v_fm_requisition_docs o
                      where UPPER(o.doc_no) like UPPER(''%' ||I_order_no || '%'')
                        and supplier = ' || I_supplier || '
                        and o.status = ''A'''|| '
                        and o.requisition_type = ''PO'' '||
                       'and exists (select ''X'' '||
                                     'from v_ordloc loc '||
                                    'where loc.order_no = o.doc_no '||
                                      'and loc.location IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id ||') '||
                                      'and loc.loc_type = '''||I_location_type||''' '||
                                      'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                        'and not exists (select ''X''
                                          from fm_fiscal_doc_detail fdd
                                         where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                           and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
         end if;
      else
         ---
         if L_supplier_sites_ind = 'Y' then
            if L_comp_nf_ind = 'Y' then
               O_query := 'Select o.doc_no
                          from v_fm_requisition_docs o
                         where
                            ( o.delivery_supplier in
                                          ( select s.supplier
                                               from sups s
                                              where s.supplier_parent = ' ||I_supplier|| ' ) or o.delivery_supplier = '||I_supplier||' )
                           and o.status = ''A''' || '
                           and o.requisition_type = ''PO'' '||
                          'and exists (select ''X'' '||
                                        'from v_ordloc loc '||
                                       'where loc.order_no = o.doc_no '||
                                         'and loc.location IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id ||') '||
                                         'and loc.loc_type = '''||I_location_type||''' '||
                                         'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                          'and not exists (select ''X''
                                             from fm_fiscal_doc_detail fdd
                                            where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                              and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
            else
               O_query := 'Select o.doc_no
                          from v_fm_requisition_docs o
                         where
                            ( o.supplier in
                                          ( select s.supplier
                                               from sups s
                                              where s.supplier_parent = ' ||I_supplier|| ' ) or o.supplier = '||I_supplier||' )
                           and o.status = ''A''' || '
                           and o.requisition_type = ''PO'' '||
                          'and exists (select ''X'' '||
                                        'from v_ordloc loc '||
                                       'where loc.order_no = o.doc_no '||
                                         'and loc.location IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id ||') '||
                                         'and loc.loc_type = '''||I_location_type||''' '||
                                         'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                          'and not exists (select ''X''
                                             from fm_fiscal_doc_detail fdd
                                            where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                              and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
           end if;
         else
         ---
            if L_comp_nf_ind = 'Y' then
               O_query := 'Select o.doc_no
                          from v_fm_requisition_docs o
                         where delivery_supplier = ' || I_supplier || '
                           and o.status = ''A''' || '
                           and o.requisition_type = ''PO'' '||
                          'and exists (select ''X'' '||
                                        'from v_ordloc loc '||
                                       'where loc.order_no = o.doc_no '||
                                         'and loc.location IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id ||') '||
                                         'and loc.loc_type = '''||I_location_type||''' '||
                                         'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                          'and not exists (select ''X''
                                             from fm_fiscal_doc_detail fdd
                                            where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                              and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
            else
               O_query := 'Select o.doc_no
                          from v_fm_requisition_docs o
                         where supplier = ' || I_supplier || '
                           and o.status = ''A''' || '
                           and o.requisition_type = ''PO'' '||
                          'and exists (select ''X'' '||
                                        'from v_ordloc loc '||
                                       'where loc.order_no = o.doc_no '||
                                         'and loc.location IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id ||') '||
                                         'and loc.loc_type = '''||I_location_type||''' '||
                                         'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) '||
                          'and not exists (select ''X''
                                             from fm_fiscal_doc_detail fdd
                                            where fdd.fiscal_doc_id = ' ||I_fiscal_doc_id || '
                                              and fdd.requisition_no = o.doc_no) order by o.doc_no desc';
            end if;
         end if;
      end if;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1, 254),
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_GET_SUPS_SITE_IND%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_SUPS_SITE_IND',
                          'SYSTEM_OPTIONS',
                          ' Supplier = '||TO_CHAR(I_supplier));
         close C_GET_SUPS_SITE_IND;
      end if;
      ---
      if C_GET_UTIL_COMP_NF_IND%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_UTIL_COMP_NF_IND',
                          'FM_FISCAL_DOC_HEADER',
                          ' I_fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id));
         close C_GET_UTIL_COMP_NF_IND;
      end if;
      ---
      return FALSE;
END LOV_PO;
-----------------------------------------------------------------------------------
FUNCTION EXPLODE_ORDER_DETAIL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_pre_load      IN OUT BOOLEAN,
                              I_fiscal_doc_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                              I_order_no      IN ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN is
   L_program VARCHAR2(50) := 'FM_MATCH_DOC_SQL.EXPLODE_ORDER_DETAIL';
   L_key     VARCHAR2(50) := 'Doc ID: ' || I_fiscal_doc_id ||
                             ', Order No: ' || I_order_no;
   ---
   cursor C_GET_ORDER_DETAIL is
      select distinct '1' line_type,
             vo.order_no,
             vo.item, -- sinlge item or complex item
             fdh.location_id,
             vo.loc_type,
             sum(vo.qty_ordered) qty_ordered,
             sum(vo.qty_prescaled) qty_prescaled,
             sum(vo.qty_received) qty_received,
             vo.unit_cost,
             va.classification_id,
             NULL complex_item_code,
             'N' pack_ind
        from ordloc           vo,
             v_br_item_fiscal_attrib va,
             item_master          vim,
             fm_fiscal_doc_header fdh
       where va.item = vo.item
         and vo.order_no = I_order_no
         and va.item = vim.item
         and vim.pack_ind = 'N'
         and vim.status = 'A'
         and vim.merchandise_ind = 'Y'
         and vim.item_level >= tran_level
         and fdh.location_id = decode(fdh.location_type,'W',(select physical_wh from wh where wh = vo.location),vo.location) 
         and fdh.fiscal_doc_id = I_fiscal_doc_id
       group by vo.order_no,vo.item,fdh.location_id,vo.loc_type,vo.unit_cost,va.classification_id
       union
      select distinct '2' line_type,
             vol.order_no,
             vol.item,  -- pack item
             fdh.location_id,
             vol.loc_type,
             sum(vol.qty_ordered), 
             sum(vol.qty_prescaled),
             vol.qty_received,
             vol.unit_cost unit_cost,
             NULL classification_id,
             NULL complex_item_code,
             'Y' pack_ind
        from ordloc vol,
             ordhead vo,
             ordsku vk,
             item_master vim,
             item_supp_country_loc iscl ,
             fm_fiscal_doc_header fdh
       where vol.order_no = I_order_no 
         and vol.order_no = vo.order_no
         and vol.order_no = vk.order_no
         and vol.item = vk.item
         and vol.item = vim.item
         and vim.pack_ind = 'Y'
         and vo.supplier = iscl.supplier
         and vk.origin_country_id = iscl.origin_country_id
         and vol.location = iscl.loc
         and vol.loc_type = iscl.loc_type
         and iscl.item = vim.item
         and vim.status = 'A'
         and vim.merchandise_ind = 'Y'
         and vim.item_level >= tran_level
         and fdh.location_id = decode(fdh.location_type,'W',(select physical_wh from wh where wh = vol.location),vol.location) 
         and fdh.fiscal_doc_id = I_fiscal_doc_id
       group by vol.order_no,vol.item,fdh.location_id,vol.loc_type,vol.qty_received,vol.unit_cost
       order by 1;
   ---
   cursor C_GET_DOC_HEADER is
      select fiscal_doc_id,
             location_id,
             location_type,
             fiscal_doc_no,
             utilization_id,
             nf_cfop
        from fm_fiscal_doc_header fh
       where fh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_LOCK_DOC_DETAIL(P_fiscal_doc_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE, P_location FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE, P_loc_type FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE, P_order_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE, P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fd
       where fd.fiscal_doc_id = P_fiscal_doc_id
         and fd.location_id = P_location
         and fd.location_type = P_loc_type
         and fd.requisition_no = P_order_no
         and fd.item = P_item;
   ---
   cursor C_GET_COMPLEX_LINE_ID(P_fiscal_doc_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE, P_location FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE, P_loc_type FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE, P_order_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE, P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select fd.fiscal_doc_line_id
        from fm_fiscal_doc_detail fd
       where fd.fiscal_doc_id = P_fiscal_doc_id
         and fd.location_id = P_location
         and fd.location_type = P_loc_type
         and fd.requisition_no = P_order_no
         and fd.item = P_item;
   ---
   cursor C_GET_NEXT_DOC_LINE_ID_SEQ is
      select FM_FISCAL_DOC_LINE_ID_SEQ.NEXTVAL
        from dual;
   ---
   Rec_ordloc     C_GET_ORDER_DETAIL%ROWTYPE;
   Rec_doc_header C_GET_DOC_HEADER%ROWTYPE;
   L_line_no      NUMBER := 0;
   L_dummy        VARCHAR2(1);
   L_quantity     ORDLOC.QTY_ORDERED%TYPE;
   L_unit_cost    ORDLOC.UNIT_COST%TYPE;
   L_total_cost   ORDLOC.UNIT_RETAIL%TYPE;
   L_complex_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_seq_no  NUMBER;
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                            I_fiscal_doc_id,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            I_order_no,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DOC_HEADER',
                    'FM_FISCAL_DOC_HEADER',
                    L_key);
   open C_GET_DOC_HEADER;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DOC_HEADER',
                    'FM_FISCAL_DOC_HEADER',
                    L_key);
   fetch C_GET_DOC_HEADER
      into Rec_doc_header;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DOC_HEADER',
                    'FM_FISCAL_DOC_HEADER',
                    L_key);
   close C_GET_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ORDER_DETAIL', 'ORDLOC', L_key);
   open C_GET_ORDER_DETAIL;
   LOOP
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_ORDER_DETAIL',
                       'ORDLOC',
                       L_key);
      fetch C_GET_ORDER_DETAIL
         into Rec_ordloc;
      ---
      EXIT when C_GET_ORDER_DETAIL%NOTFOUND;
      ---
      L_line_no := L_line_no + 1;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_DOC_DETAIL',
                       'FM_FISCAL_DOC_DETAIL',
                       L_key);
      open C_LOCK_DOC_DETAIL(I_fiscal_doc_id,
                             Rec_doc_header.Location_Id,
                             Rec_doc_header.Location_Type,
                             I_order_no,
                             Rec_ordloc.Item);

      SQL_LIB.SET_MARK('FETCH',
                       'C_LOCK_DOC_DETAIL',
                       'FM_FISCAL_DOC_DETAIL',
                       L_key);
      fetch C_LOCK_DOC_DETAIL
         into L_dummy;
      ---
      if I_pre_load = TRUE then
         L_quantity   := Rec_ordloc.qty_ordered - NVL(Rec_ordloc.qty_received,0);
         L_unit_cost  := Rec_ordloc.unit_cost;
         L_total_cost := Rec_ordloc.unit_cost *
                         (Rec_ordloc.qty_ordered - NVL(Rec_ordloc.qty_received,0));
      else
         L_quantity   := 0;
         L_unit_cost  := 0;
         L_total_cost := 0;
      end if;
      ---
      if Rec_ordloc.complex_item_code is NOT NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_COMPLEX_LINE_ID',
                          'FM_FISCAL_DOC_DETAIL',
                          L_key);
         open C_GET_COMPLEX_LINE_ID(I_fiscal_doc_id,
                                    Rec_doc_header.Location_Id,
                                    Rec_doc_header.Location_Type,
                                    I_order_no,
                                    Rec_ordloc.complex_item_code);
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_COMPLEX_LINE_ID',
                          'FM_FISCAL_DOC_DETAIL',
                          L_key);
         fetch C_GET_COMPLEX_LINE_ID
            into L_complex_fiscal_doc_line_id;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_COMPLEX_LINE_ID',
                          'FM_FISCAL_DOC_DETAIL',
                          L_key);
         close C_GET_COMPLEX_LINE_ID;
      end if;
      ---
       SQL_LIB.SET_MARK('OPEN', 'C_GET_NEXT_DOC_LINE_ID_SEQ', 'FM_FISCAL_DOC_LINE_ID_SEQ',L_key);
       open C_GET_NEXT_DOC_LINE_ID_SEQ;
       ---
       SQL_LIB.SET_MARK('FETCH', 'C_GET_NEXT_DOC_LINE_ID_SEQ', 'FM_FISCAL_DOC_LINE_ID_SEQ', L_key);
       fetch C_GET_NEXT_DOC_LINE_ID_SEQ into L_seq_no;
       ---
       SQL_LIB.SET_MARK('CLOSE', 'C_GET_NEXT_DOC_LINE_ID_SEQ', 'FM_FISCAL_DOC_LINE_ID_SEQ', L_key);
       close C_GET_NEXT_DOC_LINE_ID_SEQ;
       ---
      if Rec_ordloc.qty_ordered - NVL(Rec_ordloc.qty_received,0) > 0 then
         SQL_LIB.SET_MARK('INSERT', NULL, 'FM_FISCAL_DOC_DETAIL', L_key);
         insert into fm_fiscal_doc_detail
            (fiscal_doc_line_id,
             fiscal_doc_id,
             location_id,
             location_type,
             line_no,
             requisition_no,
             item,
             quantity,
             unit_cost,
             total_cost,
             total_calc_cost,
             other_expenses_cost,
             freight_cost,
             net_cost,
             create_datetime,
             create_id,
             last_update_datetime,
             last_update_id,
             classification_id,
             fiscal_doc_id_ref,
             fiscal_doc_line_id_ref,
             nf_cfop,
             appt_qty,
             pack_ind)
         values
            (L_seq_no,
             Rec_doc_header.fiscal_doc_id,
             Rec_doc_header.location_id,
             Rec_doc_header.location_type,
             L_line_no,
             I_order_no,
             Rec_ordloc.item,
             L_quantity,
             DECODE(Rec_ordloc.complex_item_code,NULL,L_unit_cost,0),
             DECODE(Rec_ordloc.complex_item_code,NULL,L_total_cost,0),
             NULL,
             NULL,
             NULL,
             NULL,
             SYSDATE,
             USER,
             SYSDATE,
             USER,
             Rec_ordloc.Classification_Id,
             DECODE(Rec_ordloc.complex_item_code,NULL,NULL,Rec_doc_header.fiscal_doc_id),
             DECODE(Rec_ordloc.complex_item_code,NULL,NULL,L_complex_fiscal_doc_line_id),
             Rec_doc_header.nf_cfop,
             L_quantity,
             Rec_ordloc.pack_ind);
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_DOC_DETAIL',
                       'FM_FISCAL_DOC_DETAIL',
                       L_key);
      close C_LOCK_DOC_DETAIL;
      ---
   END LOOP;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORDER_DETAIL', 'ORDLOC', L_key);
   close C_GET_ORDER_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1, 254),
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_GET_ORDER_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_ORDER_DETAIL',
                          'ORDLOC',
                          L_key);
         close C_GET_ORDER_DETAIL;
      end if;
      ---
      if C_LOCK_DOC_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_DOC_DETAIL',
                          'FM_FISCAL_DOC_DETAIL',
                          L_key);
         close C_LOCK_DOC_DETAIL;
      end if;
      ---
      if C_GET_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_DOC_HEADER',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         close C_GET_DOC_HEADER;
      end if;
      ---
      if C_GET_NEXT_DOC_LINE_ID_SEQ%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_NEXT_DOC_LINE_ID_SEQ', 'FM_FISCAL_DOC_LINE_ID_SEQ', L_key);
         close C_GET_NEXT_DOC_LINE_ID_SEQ;
      end if;
      ---
      return FALSE;

END EXPLODE_ORDER_DETAIL;
-----------------------------------------------------------------------------------
FUNCTION EXISTS_PO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists         IN OUT BOOLEAN,
                   I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                   I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                   I_supplier       IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                   I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                   I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
      return BOOLEAN is
      ---
   L_program              VARCHAR2(50) := 'FM_MATCH_DOC_SQL.EXISTS_PO';
   L_dummy                VARCHAR2(1);
   L_supplier_sites_ind   SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE; 
   L_comp_nf_ind          FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   ---
   cursor C_EXISTS_PO is
      Select 'X'
        from v_fm_requisition_docs o
       where o.doc_no = I_order_no
         and supplier = I_supplier
         and o.status = 'A'
         and o.requisition_type = 'PO'
         and exists (select 'X'
                       from v_ordloc loc
                      where loc.order_no = o.doc_no
                        and loc.location = I_location_id
                        and loc.loc_type = I_location_type
                        and (loc.qty_ordered - NVL(loc.qty_received,0)) > 0)
         and not exists (select 'X'
                           from fm_fiscal_doc_detail fdd
                          where fdd.fiscal_doc_id = I_fiscal_doc_id
                            and fdd.requisition_no = o.doc_no) order by o.doc_no;
   ---
   cursor C_EXISTS_DEL_SUP_PO is
      Select 'X'
        from v_fm_requisition_docs o
       where o.doc_no = I_order_no
         and delivery_supplier = I_supplier
         and o.status = 'A'
         and o.requisition_type = 'PO'
         and exists (select 'X'
                       from v_ordloc loc
                      where loc.order_no = o.doc_no
                        and loc.location = I_location_id
                        and loc.loc_type = I_location_type
                        and (loc.qty_ordered - NVL(loc.qty_received,0)) > 0)
         and not exists (select 'X'
                           from fm_fiscal_doc_detail fdd
                          where fdd.fiscal_doc_id = I_fiscal_doc_id
                            and fdd.requisition_no = o.doc_no) order by o.doc_no;
   ---
   cursor C_EXISTS_PO_SUPS_SITE is
      Select 'X'
        from v_fm_requisition_docs o
       where o.doc_no = I_order_no
         and ( o.supplier in
                       ( select s.supplier
                            from sups s
                           where s.supplier_parent = I_supplier ) or o.supplier = I_supplier )
         and o.status = 'A'
         and o.requisition_type = 'PO'
         and exists (select 'X'
                       from v_ordloc loc
                      where loc.order_no = o.doc_no
                        and loc.location = I_location_id
                        and loc.loc_type = I_location_type
                        and (loc.qty_ordered - NVL(loc.qty_received,0)) > 0)
         and not exists (select 'X'
                           from fm_fiscal_doc_detail fdd
                          where fdd.fiscal_doc_id = I_fiscal_doc_id
                            and fdd.requisition_no = o.doc_no) order by o.doc_no;
   ---
   cursor C_EXISTS_PO_DEL_SUPS_SITE is
      Select 'X'
        from v_fm_requisition_docs o
       where o.doc_no = I_order_no
         and ( o.delivery_supplier in
                       ( select s.supplier
                            from sups s
                           where s.supplier_parent = I_supplier ) or o.delivery_supplier = I_supplier )
         and o.status = 'A'
         and o.requisition_type = 'PO'
         and exists (select 'X'
                       from v_ordloc loc
                      where loc.order_no = o.doc_no
                        and loc.location = I_location_id
                        and loc.loc_type = I_location_type
                        and (loc.qty_ordered - NVL(loc.qty_received,0)) > 0)
         and not exists (select 'X'
                           from fm_fiscal_doc_detail fdd
                          where fdd.fiscal_doc_id = I_fiscal_doc_id
                            and fdd.requisition_no = o.doc_no) order by o.doc_no;
   ---
   cursor C_EXISTS_PO_VWH is
      Select 'X'
        from v_fm_requisition_docs o
       where o.doc_no = I_order_no
         and supplier = I_supplier
         and o.status = 'A'
         and o.requisition_type = 'PO'
         and exists (select 'X'
                       from v_ordloc loc
                      where loc.order_no = o.doc_no
                        and loc.location IN (select wh.wh from wh where wh.physical_wh = I_location_id)
                        and loc.loc_type = I_location_type
                        and (loc.qty_ordered - NVL(loc.qty_received,0)) > 0)
         and not exists (select 'X'
                           from fm_fiscal_doc_detail fdd
                          where fdd.fiscal_doc_id = I_fiscal_doc_id
                            and fdd.requisition_no = o.doc_no) order by o.doc_no;
   ---
   cursor C_EXISTS_PO_VWH_DEL_SUPS is
      Select 'X'
        from v_fm_requisition_docs o
       where o.doc_no = I_order_no
         and delivery_supplier = I_supplier
         and o.status = 'A'
         and o.requisition_type = 'PO'
         and exists (select 'X'
                       from v_ordloc loc
                      where loc.order_no = o.doc_no
                        and loc.location IN (select wh.wh from wh where wh.physical_wh = I_location_id)
                        and loc.loc_type = I_location_type
                        and (loc.qty_ordered - NVL(loc.qty_received,0)) > 0)
         and not exists (select 'X'
                           from fm_fiscal_doc_detail fdd
                          where fdd.fiscal_doc_id = I_fiscal_doc_id
                            and fdd.requisition_no = o.doc_no) order by o.doc_no;
   ---
   cursor C_EXISTS_PO_VWH_SUPS_SITE is
      Select 'X'
        from v_fm_requisition_docs o
       where o.doc_no = I_order_no
         and ( o.supplier in
                       ( select s.supplier
                            from sups s
                           where s.supplier_parent = I_supplier ) or o.supplier = I_supplier )
         and o.status = 'A'
         and o.requisition_type = 'PO'
         and exists (select 'X'
                       from v_ordloc loc
                      where loc.order_no = o.doc_no
                        and loc.location IN (select wh.wh from wh where wh.physical_wh = I_location_id)
                        and loc.loc_type = I_location_type
                        and (loc.qty_ordered - NVL(loc.qty_received,0)) > 0)
         and not exists (select 'X'
                           from fm_fiscal_doc_detail fdd
                          where fdd.fiscal_doc_id = I_fiscal_doc_id
                            and fdd.requisition_no = o.doc_no) order by o.doc_no;
   ---
   cursor C_EXISTS_PO_VWH_DEL_SUPS_SITE is
      Select 'X'
        from v_fm_requisition_docs o
       where o.doc_no = I_order_no
         and ( o.delivery_supplier in
                       ( select s.supplier
                            from sups s
                           where s.supplier_parent = I_supplier ) or o.delivery_supplier = I_supplier )
         and o.status = 'A'
         and o.requisition_type = 'PO'
         and exists (select 'X'
                       from v_ordloc loc
                      where loc.order_no = o.doc_no
                        and loc.location IN (select wh.wh from wh where wh.physical_wh = I_location_id)
                        and loc.loc_type = I_location_type
                        and (loc.qty_ordered - NVL(loc.qty_received,0)) > 0)
         and not exists (select 'X'
                           from fm_fiscal_doc_detail fdd
                          where fdd.fiscal_doc_id = I_fiscal_doc_id
                            and fdd.requisition_no = o.doc_no) order by o.doc_no;
   ---
   cursor C_GET_SUPS_SITE_IND is
      select supplier_sites_ind
        from system_options;
   ---
   CURSOR C_GET_UTIL_COMP_NF_IND IS
      select comp_nf_ind
        from fm_utilization_attributes fua,
             fm_fiscal_doc_header fdh
       where fdh.utilization_id = fua.utilization_id 
         and fdh.fiscal_doc_id  =  I_fiscal_doc_id;
   ---
   L_pwh     BOOLEAN := FALSE;
   ---
BEGIN
   
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SUPS_SITE_IND',
                    'SYSTEM_OPTIONS',
                    ' Supplier = '||TO_CHAR(I_supplier));
   open C_GET_SUPS_SITE_IND;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SUPS_SITE_IND',
                    'SYSTEM_OPTIONS',
                    ' Supplier = '||TO_CHAR(I_supplier));
   fetch C_GET_SUPS_SITE_IND
      into L_supplier_sites_ind;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SUPS_SITE_IND',
                    'SYSTEM_OPTIONS',
                    ' Supplier = '||TO_CHAR(I_supplier));
   close C_GET_SUPS_SITE_IND;
   ---
   ---
   open C_GET_UTIL_COMP_NF_IND;
   fetch C_GET_UTIL_COMP_NF_IND into L_comp_nf_ind;
   close C_GET_UTIL_COMP_NF_IND;
   ---
   if I_location_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_location_type = 'S' or
      NOT L_pwh then
      if L_supplier_sites_ind = 'Y' then
         ---
         if L_comp_nf_ind = 'Y' then
            ---
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXISTS_PO_DEL_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            open C_EXISTS_PO_DEL_SUPS_SITE;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXISTS_PO_DEL_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            fetch C_EXISTS_PO_DEL_SUPS_SITE into L_dummy;
            ---
            O_exists := C_EXISTS_PO_DEL_SUPS_SITE%FOUND;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_PO_DEL_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            close C_EXISTS_PO_DEL_SUPS_SITE;
            ---
         else
            ---
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXISTS_PO_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            open C_EXISTS_PO_SUPS_SITE;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXISTS_PO_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            fetch C_EXISTS_PO_SUPS_SITE into L_dummy;
            ---
            O_exists := C_EXISTS_PO_SUPS_SITE%FOUND;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_PO_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));

            close C_EXISTS_PO_SUPS_SITE;
            ---
        end if;
      else
         ---
         if L_comp_nf_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXISTS_DEL_SUP_PO',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            open C_EXISTS_DEL_SUP_PO;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXISTS_DEL_SUP_PO',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            fetch C_EXISTS_DEL_SUP_PO into L_dummy;
            ---
            O_exists := C_EXISTS_DEL_SUP_PO%FOUND;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_DEL_SUP_PO',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            close C_EXISTS_DEL_SUP_PO;
         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXISTS_PO',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            open C_EXISTS_PO;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXISTS_PO',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            fetch C_EXISTS_PO into L_dummy;
            ---
            O_exists := C_EXISTS_PO%FOUND;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_PO',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            close C_EXISTS_PO;
         ---
         end if;
      end if;
   else
      if L_supplier_sites_ind = 'Y' then
         if L_comp_nf_ind = 'Y' then
            ---
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXISTS_PO_VWH_DEL_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            open C_EXISTS_PO_VWH_DEL_SUPS_SITE;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXISTS_PO_VWH_DEL_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            fetch C_EXISTS_PO_VWH_DEL_SUPS_SITE into L_dummy;
            ---
            O_exists := C_EXISTS_PO_VWH_DEL_SUPS_SITE%FOUND;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_PO_VWH_DEL_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            close C_EXISTS_PO_VWH_DEL_SUPS_SITE;
            ---
         else
            ---
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXISTS_PO_VWH_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            open C_EXISTS_PO_VWH_SUPS_SITE;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXISTS_PO_VWH_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            fetch C_EXISTS_PO_VWH_SUPS_SITE into L_dummy;
            ---
            O_exists := C_EXISTS_PO_VWH_SUPS_SITE%FOUND;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_PO_VWH_SUPS_SITE',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));

            close C_EXISTS_PO_VWH_SUPS_SITE;
            ---
        end if;
      else
         ---
         if L_comp_nf_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXISTS_PO_VWH_DEL_SUPS',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            open C_EXISTS_PO_VWH_DEL_SUPS;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXISTS_PO_VWH_DEL_SUPS',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            fetch C_EXISTS_PO_VWH_DEL_SUPS into L_dummy;
            ---
            O_exists := C_EXISTS_PO_VWH_DEL_SUPS%FOUND;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_PO_VWH_DEL_SUPS',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            close C_EXISTS_PO_VWH_DEL_SUPS;
         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXISTS_PO_VWH',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            open C_EXISTS_PO_VWH;
            ---
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXISTS_PO_VWH',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            fetch C_EXISTS_PO_VWH into L_dummy;
            ---
            O_exists := C_EXISTS_PO_VWH%FOUND;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_PO_VWH',
                             'V_FM_REQUISITION_DOCS',
                             'Order_no = '||TO_CHAR(I_order_no)||
                             ' Supplier = '||TO_CHAR(I_supplier));
            close C_EXISTS_PO_VWH;
         end if;
      end if;
      ---
   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1, 254),
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_GET_SUPS_SITE_IND%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_SUPS_SITE_IND',
                          'SYSTEM_OPTIONS',
                          ' Supplier = '||TO_CHAR(I_supplier));
         close C_GET_SUPS_SITE_IND;
      end if;
      ---
      if C_EXISTS_PO_SUPS_SITE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_PO_SUPS_SITE',
                          'V_FM_REQUISITION_DOCS',
                          'Order_no = '||TO_CHAR(I_order_no)||
                          ' Supplier = '||TO_CHAR(I_supplier));

         close C_EXISTS_PO_SUPS_SITE;
      end if;
      ---
      if C_EXISTS_PO%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_PO',
                          'V_FM_REQUISITION_DOCS',
                          'Order_no = '||TO_CHAR(I_order_no)||
                          ' Supplier = '||TO_CHAR(I_supplier));
         close C_EXISTS_PO;
      end if;
      ---
      if C_EXISTS_PO_VWH_SUPS_SITE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_PO_VWH_SUPS_SITE',
                          'V_FM_REQUISITION_DOCS',
                          'Order_no = '||TO_CHAR(I_order_no)||
                          ' Supplier = '||TO_CHAR(I_supplier));

         close C_EXISTS_PO_VWH_SUPS_SITE;
      end if;
      ---
      if C_EXISTS_PO_VWH%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_PO_VWH',
                          'V_FM_REQUISITION_DOCS',
                          'Order_no = '||TO_CHAR(I_order_no)||
                          ' Supplier = '||TO_CHAR(I_supplier));
         close C_EXISTS_PO_VWH;
      end if;
      ---
      if C_EXISTS_PO_VWH_DEL_SUPS_SITE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_PO_VWH_DEL_SUPS_SITE',
                          'V_FM_REQUISITION_DOCS',
                          'Order_no = '||TO_CHAR(I_order_no)||
                          ' Supplier = '||TO_CHAR(I_supplier));
         close C_EXISTS_PO_VWH_DEL_SUPS_SITE;
      end if;
      ---
      if C_EXISTS_PO_DEL_SUPS_SITE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_PO_DEL_SUPS_SITE',
                          'V_FM_REQUISITION_DOCS',
                          'Order_no = '||TO_CHAR(I_order_no)||
                          ' Supplier = '||TO_CHAR(I_supplier));
         close C_EXISTS_PO_DEL_SUPS_SITE;
      end if;
      ---
      if C_EXISTS_DEL_SUP_PO%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_DEL_SUP_PO',
                          'V_FM_REQUISITION_DOCS',
                          'Order_no = '||TO_CHAR(I_order_no)||
                          ' Supplier = '||TO_CHAR(I_supplier));
         close C_EXISTS_DEL_SUP_PO;
      end if;
      ---
      if C_EXISTS_PO_VWH_DEL_SUPS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_DEL_SUP_PO',
                          'V_FM_REQUISITION_DOCS',
                          'Order_no = '||TO_CHAR(I_order_no)||
                          ' Supplier = '||TO_CHAR(I_supplier));
         close C_EXISTS_PO_VWH_DEL_SUPS;
      end if;
      ---
      return FALSE;
END EXISTS_PO;
-----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where            IN OUT VARCHAR2,
                          I_supplier         IN     V_FM_REQUISITION_DOCS.SUPPLIER%TYPE,
                          I_location_type    IN     V_ORDLOC.LOC_TYPE%TYPE,
                          I_location_id      IN     V_ORDLOC.LOCATION%TYPE,
                          I_fiscal_doc_id    IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                          I_doc_no           IN     V_FM_REQUISITION_DOCS.DOC_NO%TYPE,
                          I_requisition_type IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
return BOOLEAN is
---
L_program             VARCHAR2(100)  := 'FM_MATCH_DOC_SQL.SET_WHERE_CLAUSE';
L_pwh                 BOOLEAN := FALSE;
L_supplier_sites_ind  SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE; 
L_comp_nf_ind         FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
---
CURSOR C_GET_UTIL_COMP_NF_IND IS
   select comp_nf_ind
     from fm_utilization_attributes fua,
          fm_fiscal_doc_header fdh
    where fdh.utilization_id = fua.utilization_id 
      and fdh.fiscal_doc_id  =  I_fiscal_doc_id;
---
BEGIN
   ---
   if I_location_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh,
                                 I_location_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   open C_GET_UTIL_COMP_NF_IND;
   fetch C_GET_UTIL_COMP_NF_IND into L_comp_nf_ind;
   close C_GET_UTIL_COMP_NF_IND;
   ---
   if I_location_type = 'S' or
      NOT L_pwh then
      ---
      if I_requisition_type = 'PO' then
         if (L_comp_nf_ind = 'Y') then
            if I_supplier is NOT NULL then
               O_where := O_where || ' o.delivery_supplier = '||I_supplier||' and ';
            end if;
         else
            if I_supplier is NOT NULL then
               O_where := O_where || ' o.supplier = '||I_supplier||' and ';
            end if;
         end if;
         ---
         if I_location_id is NOT NULL and I_location_type is NOT NULL then
            O_where := O_where || ' o.status = ''A'' and '||
                                  ' o.requisition_type = ''PO'' and '||
                                  ' exists (select ''X'' '||
                                             'from v_ordloc loc '||
                                            'where loc.order_no = o.doc_no '||
                                              'and loc.location = '||I_location_id||' '||
                                              'and loc.loc_type = '''||I_location_type||''' '||
                                              'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) and ';
         end if;
         ---
         if I_fiscal_doc_id is NOT NULL then
            O_where := O_where || ' not exists (select ''X'' '||
                                                 'from fm_fiscal_doc_detail fdd '||
                                                'where fdd.fiscal_doc_id = '||I_fiscal_doc_id||' '||
                                                  'and fdd.requisition_no = o.doc_no) and ';
         end if;
         ---
         if I_doc_no is NOT NULL then
            O_where := O_where ||  ' o.doc_no = '||I_doc_no||' and ';
         end if;
         ---
      elsif I_requisition_type = 'RMA' then
         ---
         if I_location_id is NOT NULL and I_location_type is NOT NULL then
            O_where := O_where || ' o.status = ''A'' and '||
                                  ' o.requisition_type = ''RMA'' and '||
                                  ' exists (select ''X'' '||
                                             'from fm_rma_head r '||
                                            'where r.rma_id = o.doc_no '||
                                              'and r.location = '||I_location_id||' '||
                                              'and r.loc_type = '''||I_location_type||''') and ';
         end if;
         ---
         if I_fiscal_doc_id is NOT NULL then
            O_where := O_where || ' not exists (select ''X'' '||
                                                 'from fm_fiscal_doc_detail fdd '||
                                                'where fdd.fiscal_doc_id = '||I_fiscal_doc_id||' '||
                                                  'and fdd.requisition_no = o.doc_no) and ';
         end if;
         ---
         if I_doc_no is NOT NULL then
            O_where := O_where ||  ' o.doc_no = '||I_doc_no||' and ';
         end if;
         ---
      end if;
      ---
   else
      if I_requisition_type = 'PO' then
         if (L_comp_nf_ind = 'Y') then
            if I_supplier is NOT NULL then
               O_where := O_where || ' o.delivery_supplier = '||I_supplier||' and ';
            end if;
         else
            if I_supplier is NOT NULL then
               O_where := O_where || ' o.supplier = '||I_supplier||' and ';
            end if;
         end if;
         ---
         if I_location_id is NOT NULL and I_location_type is NOT NULL then
            O_where := O_where || ' o.status = ''A'' and '||
                                  ' o.requisition_type = ''PO'' and '||
                                  ' exists (select ''X'' '||
                                             'from v_ordloc loc '||
                                            'where loc.order_no = o.doc_no '||
                                              'and loc.location IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id ||') '||
                                              'and loc.loc_type = '''||I_location_type||''' '||
                                              'and (loc.qty_ordered - NVL(loc.qty_received,0) > 0)) and ';
         end if;
         ---
         if I_fiscal_doc_id is NOT NULL then
            O_where := O_where || ' not exists (select ''X'' '||
                                                 'from fm_fiscal_doc_detail fdd '||
                                                'where fdd.fiscal_doc_id = '||I_fiscal_doc_id||' '||
                                                  'and fdd.requisition_no = o.doc_no) and ';
         end if;
         ---
         if I_doc_no is NOT NULL then
            O_where := O_where ||  ' o.doc_no = '||I_doc_no||' and ';
         end if;
         ---
      elsif I_requisition_type = 'RMA' then
         ---
         if I_location_id is NOT NULL and I_location_type is NOT NULL then
            O_where := O_where || ' o.status = ''A'' and '||
                                  ' o.requisition_type = ''RMA'' and '||
                                  ' exists (select ''X'' '||
                                             'from fm_rma_head r '||
                                            'where r.rma_id = o.doc_no '||
                                              'and r.location IN (select wh.wh from wh where wh.physical_wh = '|| I_location_id ||') '||
                                              'and r.loc_type = '''||I_location_type||''') and ';
         end if;
         ---
         if I_fiscal_doc_id is NOT NULL then
            O_where := O_where || ' not exists (select ''X'' '||
                                                 'from fm_fiscal_doc_detail fdd '||
                                                'where fdd.fiscal_doc_id = '||I_fiscal_doc_id||' '||
                                                  'and fdd.requisition_no = o.doc_no) and ';
         end if;
         ---
         if I_doc_no is NOT NULL then
            O_where := O_where ||  ' o.doc_no = '||I_doc_no||' and ';
         end if;
         ---
      end if;
      ---
   end if;
   O_where := SUBSTR(O_where,1,LENGTH(O_where)-4);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE;
-----------------------------------------------------------------------------------
FUNCTION GET_PARENT_SUPS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_sup_name       OUT    SUPS.SUP_NAME%TYPE,
                         O_supplier       OUT    SUPS.SUPPLIER_PARENT%TYPE,
                         I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE
                         )
return BOOLEAN is

   L_program VARCHAR2(50) := 'FM_MATCH_DOC_SQL.GET_PARENT_SUPS';
   L_key     VARCHAR2(50) := ' I_key_value_1: ' || I_key_value_1;

   cursor C_GET_PARENT_SUPS is
      select distinct s.sup_name,
             s.supplier
        from v_sups s,
             sups
       where sups.supplier = I_key_value_1
         and s.supplier_parent is NULL
         and s.sup_status = 'A'
         and sups.supplier_parent = s.supplier
       order by 1;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PARENT_SUPS',
                    'SUPS',
                    L_key);
   open C_GET_PARENT_SUPS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PARENT_SUPS',
                    'SUPS',
                    L_key);
   fetch C_GET_PARENT_SUPS into O_sup_name,O_supplier;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PARENT_SUPS',
                    'SUPS',
                    L_key);
   close C_GET_PARENT_SUPS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1, 254),
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_GET_PARENT_SUPS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_PARENT_SUPS',
                          'SUPS',
                          L_key);
         close C_GET_PARENT_SUPS;
      end if;
      ---
      return FALSE;
END GET_PARENT_SUPS;
-----------------------------------------------------------------------------------
END FM_MATCH_DOC_SQL;
/