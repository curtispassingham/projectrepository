CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DOC_DETAIL_SQL is
-------------------------------------------------------------------
FUNCTION GET_NEXT_FISCAL_DOC_LINE_ID(O_error_message       IN OUT VARCHAR2,
                                     O_fiscal_doc_line_id  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL_SQL.GET_NEXT_FISCAL_DOC_LINE_ID';
   ---
   cursor C_SEQ_FISCAL_DOC_LINE_ID is
      select fm_fiscal_doc_line_id_seq.NEXTVAL seq_no
        from dual;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SEQ_FISCAL_DOC_LINE_ID', 'SEQ_FISCAL_DOC_LINE_ID', NULL);
   open C_SEQ_FISCAL_DOC_LINE_ID;
   SQL_LIB.SET_MARK('FETCH', 'C_SEQ_FISCAL_DOC_LINE_ID', 'SEQ_FISCAL_DOC_LINE_ID',NULL);
   fetch C_SEQ_FISCAL_DOC_LINE_ID into O_fiscal_doc_line_id;
   SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_FISCAL_DOC_LINE_ID', 'SEQ_FISCAL_DOC_LINE_ID',NULL);
   close C_SEQ_FISCAL_DOC_LINE_ID;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_SEQ_FISCAL_DOC_LINE_ID%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_FISCAL_DOC_LINE_ID', 'SEQ_FISCAL_DOC_LINE_ID',NULL);
         close C_SEQ_FISCAL_DOC_LINE_ID;
      end if;
      ---
      return FALSE;

END GET_NEXT_FISCAL_DOC_LINE_ID;
-------------------------------------------------------------------
FUNCTION GET_NEXT_LINE_NO(O_error_message IN OUT VARCHAR2,
                          O_line_no       IN OUT FM_FISCAL_DOC_DETAIL.LINE_NO%TYPE,
                          I_fiscal_doc_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_DETAIL_SQL.GET_NEXT_LINE_NO';
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select NVL(MAX(line_no),0) + 1
        from fm_fiscal_doc_detail
       where fiscal_doc_id = I_fiscal_doc_id;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', 'Fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   open C_FM_FISCAL_DOC_DETAIL;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', 'Fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   fetch C_FM_FISCAL_DOC_DETAIL into O_line_no;
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', 'Fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   close C_FM_FISCAL_DOC_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_NEXT_LINE_NO;
-----------------------------------------------------------------------------------
FUNCTION EXISTS_DETAIL(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_fiscal_doc_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_DETAIL_SQL.EXISTS_DETAIL';
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select 'x'
        from fm_fiscal_doc_detail
       where fiscal_doc_id = I_fiscal_doc_id;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', 'Fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', 'Fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   fetch C_FM_FISCAL_DOC_DETAIL into L_dummy;
   O_exists := C_FM_FISCAL_DOC_DETAIL%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', 'Fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id));
   close C_FM_FISCAL_DOC_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXISTS_DETAIL;
-----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_DETAIL_INFO(O_error_message      IN OUT VARCHAR2,
                                    O_fiscal_doc_detail  IN OUT FM_FISCAL_DOC_DETAIL%ROWTYPE,
                                    I_fiscal_doc_line_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL_SQL.GET_FISCAL_DOC_DETAIL_INFO';
   L_cursor    VARCHAR2(100) := 'C_FM_FISCAL_DOC_DETAIL';
   L_table     VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL';
   L_key       VARCHAR2(100) := 'Fiscal_doc_line_id: ' || TO_CHAR(I_fiscal_doc_line_id);
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select *
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_line_id = I_fiscal_doc_line_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_FM_FISCAL_DOC_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_FM_FISCAL_DOC_DETAIL into O_fiscal_doc_detail;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_FM_FISCAL_DOC_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      return FALSE;
END GET_FISCAL_DOC_DETAIL_INFO;
----------------------------------------------------------------------------------

END FM_FISCAL_DOC_DETAIL_SQL;
/
 