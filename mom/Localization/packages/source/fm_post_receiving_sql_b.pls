CREATE OR REPLACE PACKAGE BODY FM_POST_RECEIVING_SQL AS
--------------------------------------------------------------------------------
FUNCTION UPDATE_ST_HISTORY (O_error_message  IN OUT  VARCHAR2,
                            I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
RETURN BOOLEAN IS
   PRAGMA AUTONOMOUS_TRANSACTION;

   L_fiscal_doc_id      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_rec_st_status      VARCHAR2(1) := 'R';
  
   cursor C_GET_FISCAL_DOC_LINE_ID is
      select fdd.fiscal_doc_line_id
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id  = I_fiscal_doc_id
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id;

BEGIN

   L_fiscal_doc_id   := I_fiscal_doc_id;
   open C_GET_FISCAL_DOC_LINE_ID;
   loop
   fetch C_GET_FISCAL_DOC_LINE_ID into L_fiscal_doc_line_id;
   exit when C_GET_FISCAL_DOC_LINE_ID%NOTFOUND;

   insert into fm_stg_st_rec_hist(fiscal_doc_id,
                                  fiscal_doc_line_id,
                                  status,
                                  create_datetime,
                                  create_id,
                                  last_update_datetime,
                                  last_update_id)
                           values(L_fiscal_doc_id,
                                  L_fiscal_doc_line_id,
                                  L_rec_st_status,
                                  SYSDATE,
                                  USER,
                                  SYSDATE,
                                  USER);
   end loop;
   close C_GET_FISCAL_DOC_LINE_ID;
   commit;
   return TRUE;

EXCEPTION
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_POST_RECEIVING_SQL.UPDATE_ST_HISTORY',
                                             TO_CHAR(SQLCODE));
     return FALSE;
END UPDATE_ST_HISTORY;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS (O_error_message   IN OUT   VARCHAR2,
                  I_schedule        IN       FM_SCHEDULE.SCHEDULE_NO%TYPE)
RETURN BOOLEAN IS

   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   L_program_name   VARCHAR2(60) := 'FM_POST_RECEIVING_SQL.PROCESS';

   L_schedule             FM_SCHEDULE.SCHEDULE_NO%TYPE := 0;
   L_error_ind            BOOLEAN := FALSE;
   L_document_type        FM_RECEIVING_HEADER.DOCUMENT_TYPE%TYPE;
   L_rec_st_ind           V_BR_WH_CONTROL_FLAGS.CONTROL_REC_ST_IND%TYPE := NULL;
   L_utilization_st_ind   FM_UTILIZATION_ATTRIBUTES.ICMSST_RECOVERY_IND%TYPE := NULL;
   L_count                INTEGER :=0;
   L_status               INTEGER;
   L_batch_running_ind    RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE;
   L_nf_status            FM_FISCAL_DOC_HEADER.STATUS%TYPE := NULL;
   L_table                VARCHAR2(100) := 'FM_RECEIVING_HEADER';

   C_APPROVED_STATUS    CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'A';--Approved
   C_HOLD_STATUS        CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'H';-- Hold
   C_PROCESSED_STATUS   CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'P';-- Processed
   C_TSF_TYPE           CONSTANT VARCHAR2(6) := 'TSF';
   C_IC_TYPE            CONSTANT VARCHAR2(6) := 'IC';
   C_REP_TYPE           CONSTANT VARCHAR2(6)  := 'REP';

   cursor C_DOCUMENT_TYPE(P_schedule NUMBER) is
    select DISTINCT frh.document_type
      from fm_receiving_header frh
     where frh.recv_no = P_schedule;

   cursor C_PROCESSED_NF(P_schedule NUMBER) is
    select frh.fiscal_doc_id
      from fm_receiving_header frh
     where frh.recv_no = P_schedule
       and frh.status in (C_PROCESSED_STATUS, C_HOLD_STATUS);

   cursor C_GET_REQUISITION_INFO(P_schedule NUMBER) is
    select DISTINCT frh.requisition_type, frd.requisition_no, frh.fiscal_doc_id
      from fm_receiving_header frh,
           fm_receiving_detail frd
     where frh.seq_no = frd.header_seq_no
       and frh.recv_no = P_schedule;

   L_requisition_info  C_GET_REQUISITION_INFO%ROWTYPE;

   cursor C_LEG_COUNT(P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select count(1)
        from tsfhead t
       where (t.tsf_parent_no = P_requisition_no
        and from_loc_type = 'E'       
          or (tsf_no = P_requisition_no
         and tsf_parent_no is not null
         and from_loc_type = 'E'))
         and NVL(context_type,'X') != 'REPAIR';

   cursor C_GET_RMS_UPD_ERR_IND(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
     select rms_upd_err_ind
       from fm_fiscal_doc_header fdh
      where fdh.fiscal_doc_id = P_fiscal_doc_id;

   cursor C_GET_REC_ST_IND(P_schedule NUMBER) is
      select distinct DECODE(fs.location_type,'S',scf.control_rec_st_ind,'W',wcf.control_rec_st_ind) as control_ind,
             fa.icmsst_recovery_ind
       from v_br_store_control_flags scf,
            v_br_wh_control_flags wcf,
            fm_schedule fs,
            fm_fiscal_doc_header fdh,
            fm_fiscal_utilization fu,
            fm_utilization_attributes fa
      where fs.schedule_no     = P_schedule
        and ((fs.location_type = 'S'
        and fs.location_id     = scf.store)
         or (fs.location_type  = 'W'
        and fs.location_id     = wcf.wh))
        and fdh.schedule_no    = fs.schedule_no
        and fu.status          = 'A'
        and fa.utilization_id  = fu.utilization_id
        and fa.utilization_id  = fdh.utilization_id;

   cursor C_LOCK_STG_RECV_DESC is
      select 1
        from fm_receiving_header frh
       where frh.recv_no = L_schedule
         for update nowait;

   TYPE T_nf_tab IS TABLE OF FM_RECEIVING_HEADER.FISCAL_DOC_ID%TYPE index by binary_integer;
   L_nf_tab   T_nf_tab;
   k    INTEGER := 0;

   L_rms_upd_err_ind  FM_FISCAL_DOC_HEADER.RMS_UPD_ERR_IND%TYPE;

BEGIN

   if I_schedule is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_schedule',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   L_schedule := I_schedule;

   open C_DOCUMENT_TYPE(L_schedule);
   fetch C_DOCUMENT_TYPE into L_document_type;
   close C_DOCUMENT_TYPE;

   if L_document_type in ('T','D','A') then
      open C_GET_REQUISITION_INFO(L_schedule);
      LOOP
         fetch C_GET_REQUISITION_INFO into L_requisition_info;
         EXIT when C_GET_REQUISITION_INFO%NOTFOUND;

         open C_LEG_COUNT(L_requisition_info.requisition_no);
         fetch C_LEG_COUNT into L_count;
         close C_LEG_COUNT;

         if L_requisition_info.requisition_type = C_REP_TYPE or (L_requisition_info.requisition_type in (C_TSF_TYPE, C_IC_TYPE) and L_count > 0) then
            if FM_AUTO_SHIPPING_SQL.CONSUME(O_error_message,
                                            L_error_ind,
                                            L_schedule) = FALSE then
               return FALSE;
            end if;
         end if;
         if L_error_ind = TRUE then
            if FM_FISCAL_DOC_HEADER_SQL.UPDATE_RMS_ERR_STATUS(O_error_message,
                                                              L_requisition_info.fiscal_doc_id,
                                                             'Y') = FALSE then
               return FALSE;
            end if;
            if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                            L_requisition_info.fiscal_doc_id,
                                           'FM_AUTO_SHIPPING_SQL.CONSUME',
                                           'FM_AUTO_SHIPPING_SQL.CONSUME',
                                           'E',
                                           'RMS_UPDATION_ERROR',
                                            O_error_message,
                                           'Fiscal_doc_id: '||
                                            TO_CHAR(L_requisition_info.fiscal_doc_id)) = FALSE then
               return FALSE;
            end if;
         else
            open C_GET_RMS_UPD_ERR_IND(L_requisition_info.fiscal_doc_id);
            fetch C_GET_RMS_UPD_ERR_IND into L_rms_upd_err_ind;
            close C_GET_RMS_UPD_ERR_IND;
            --
            if L_rms_upd_err_ind = 'Y' then
               if FM_FISCAL_DOC_HEADER_SQL.UPDATE_RMS_ERR_STATUS(O_error_message,
                                                                 L_requisition_info.fiscal_doc_id) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      END LOOP;
      close C_GET_REQUISITION_INFO;
   end if;

   if FM_RECEIVING_SQL.CALC_FINAL_EBC(O_error_message,
                                      L_schedule) = FALSE then
      return FALSE;
   end if;

   if RMS_BATCH_STATUS_SQL.GET_BATCH_RUNNING_IND(O_error_message,
                                                 L_batch_running_ind) = FALSE then
      return FALSE;
   end if;

   if L_batch_running_ind = 'Y' then
      -- Update FM_RECEIVING_HEADER to Hold status
      open C_LOCK_STG_RECV_DESC;
      close C_LOCK_STG_RECV_DESC;
      ---
      update fm_receiving_header
         set status = C_HOLD_STATUS,
             last_update_id = USER,
             last_update_datetime = SYSDATE
       where recv_no = L_schedule;
   else
      if FM_RECEIVING_CONSUME_SQL.CONSUME(O_error_message,
                                          L_error_ind,
                                          L_schedule) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_error_ind = FALSE then
      if L_document_type = 'P' then
         if FM_CORRECTION_DOC_SQL.CREATE_FOR_CORRECTION_DOC(O_error_message,
                                                            L_schedule) = FALSE then
            return FALSE;
         end if;
      elsif L_document_type in ('T','D','A') then

         open C_PROCESSED_NF(L_schedule);
         fetch C_PROCESSED_NF BULK COLLECT into L_nf_tab;
         close C_PROCESSED_NF;

         if L_batch_running_ind = 'Y' then
            L_nf_status := C_HOLD_STATUS;
         else
            L_nf_status := C_APPROVED_STATUS;
         end if;

         FOR k in L_nf_tab.FIRST..L_nf_tab.LAST LOOP
            if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                      L_nf_tab(k),
                                                      L_nf_status) = FALSE then
               return FALSE;
            end if;
         END LOOP;

         if FM_SCHEDULE_VAL_SQL.UPDATE_SCHD_STATUS(O_error_message,
                                                   L_schedule,
                                                   L_nf_status) = FALSE then
            return FALSE;
         end if;

         open C_GET_REC_ST_IND(L_schedule);
         fetch C_GET_REC_ST_IND into L_rec_st_ind,L_utilization_st_ind;
         close C_GET_REC_ST_IND;

         FOR k in L_nf_tab.FIRST..L_nf_tab.LAST LOOP
            if NVL(L_rec_st_ind,'N') ='Y' and NVL(L_utilization_st_ind,'N') = 'Y' then
               if UPDATE_ST_HISTORY(O_error_message,
                                    L_nf_tab(k)) = FALSE then
                  return FALSE;
               end if;
               if FM_EXT_TAXES_SQL.UPDATE_HIST_TABLES(O_error_message,
                                                      L_status,
                                                      L_nf_tab(k)) = FALSE then
                  return FALSE;
               end if;
            end if;
         END LOOP;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            SQLERRM,
                                            L_table,
                                            TO_CHAR(SQLCODE));
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------
FUNCTION RECEIVING_STATUS(O_error_message   IN OUT   VARCHAR2,
                          O_status          IN OUT   FM_RECEIVING_HEADER.STATUS%TYPE,
                          I_fiscal_doc_id   IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program_name   VARCHAR2(60) := 'FM_POST_RECEIVING_SQL.RECEIVING_STATUS';

   cursor C_STATUS_RECEIVING is
      select status
        from fm_receiving_header
       where fiscal_doc_id = I_fiscal_doc_id;

BEGIN

   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   open C_STATUS_RECEIVING;
   fetch C_STATUS_RECEIVING into O_status;
   close C_STATUS_RECEIVING;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RECEIVING_STATUS; 
----------------------------------------------------------------------------------------------
END FM_POST_RECEIVING_SQL;
/