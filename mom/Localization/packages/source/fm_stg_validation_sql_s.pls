create or replace PACKAGE FM_STG_VALIDATION_SQL is
--------------------------------------------------------------------------------
FUNCTION PROCESS_VALIDATION(O_error_message    IN OUT VARCHAR2,
                            I_message          IN      RIB_OBJECT,
                            I_message_type     IN      VARCHAR2,
                            I_requisition_type IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
end FM_STG_VALIDATION_SQL;
/