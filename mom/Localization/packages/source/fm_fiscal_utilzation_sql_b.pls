CREATE OR REPLACE PACKAGE BODY FM_FISCAL_UTILIZATION_SQL is
------------------------------------------------------------------------------------------------------
FUNCTION GET_DESC (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_util_desc        IN OUT FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                   I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80)   := 'FM_FISCAL_UTILIZATION_SQL.GET_DESC';
   
   cursor C_UTILIZATION is
      select utilization_desc
        from fm_fiscal_utilization
       where utilization_id = I_utilization
         and status = 'A';

BEGIN
   open C_UTILIZATION;
   --
   fetch C_UTILIZATION into O_util_desc;
   --
   if C_UTILIZATION%NOTFOUND then
      close C_UTILIZATION;
      O_error_message := SQL_LIB.create_msg('ORFM_INV_UTIL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      close C_UTILIZATION;
         if LANGUAGE_SQL.TRANSLATE( O_util_desc,
                                    O_util_desc,
                                    O_error_message) = FALSE then
            return FALSE;
         end if;
      return TRUE;
   end if;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_DESC;
------------------------------------------------------------------------------------------------------
FUNCTION EXISTS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists           IN OUT BOOLEAN,
                 I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_FISCAL_UTILIZATION_SQL.EXISTS';
   L_dummy     VARCHAR2(1);
   cursor C_UTILIZATION is
      select '1'
        from fm_fiscal_utilization
       where utilization_id = I_utilization;

BEGIN
   open C_UTILIZATION;
   --
   fetch C_UTILIZATION into L_dummy;
   --
   if C_UTILIZATION%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   --
   close C_UTILIZATION;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END EXISTS;
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program     VARCHAR2(80) := 'FM_FISCAL_UTILIZATION_SQL.UPDATE_STATUS';
   L_util_cfop   VARCHAR2(10);
   L_dummy       VARCHAR2(1);
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DOC_TYPE is
      select 'x'
         from fm_doc_type_utilization fdtu
        where fdtu.utilization_id   = I_utilization
          for update nowait;

BEGIN
   --
   open C_LOCK_DOC_TYPE;
   close C_LOCK_DOC_TYPE;
   update fm_doc_type_utilization fdtu
      set fdtu.status = 'I',
          fdtu.last_update_datetime = SYSDATE,
          fdtu.last_update_id = USER
    where fdtu.utilization_id = I_utilization;
   --

   return TRUE;
EXCEPTION
  when RECORD_LOCKED then
     O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                           L_program,
                                           'utilization_id = '||I_utilization,
                                           NULL);
     return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END UPDATE_STATUS;
------------------------------------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_util_desc         IN OUT FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                   O_status            IN OUT FM_FISCAL_UTILIZATION.STATUS%TYPE,
                   O_mode_type         IN OUT FM_FISCAL_UTILIZATION.MODE_TYPE%TYPE,
                   O_nop               IN OUT FM_FISCAL_UTILIZATION.NOP%TYPE,
                   O_requisition_type  IN OUT FM_FISCAL_UTILIZATION.REQUISITION_TYPE%TYPE,
                   I_utilization       IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80)   := 'FM_FISCAL_UTILIZATION_SQL.GET_INFO';
  
   cursor C_UTILIZATION is
      select utilization_desc,
             status,
             mode_type,
             nop,
             requisition_type
        from fm_fiscal_utilization
       where utilization_id = I_utilization
         and status = 'A';

BEGIN
   open C_UTILIZATION;
   --
   fetch C_UTILIZATION into O_util_desc,
                            O_status,
                            O_mode_type,
                            O_nop,
                            O_requisition_type;
   --
   if C_UTILIZATION%NOTFOUND then
      close C_UTILIZATION;
      --
      O_error_message := SQL_LIB.create_msg('INV_UTIL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      close C_UTILIZATION;
      --
         if LANGUAGE_SQL.TRANSLATE( O_util_desc,
                                    O_util_desc,
                                    O_error_message) = FALSE then
            return FALSE;
         end if;
      return TRUE;
      --
   end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_INFO;

------------------------------------------------------------------------------------------------------
FUNCTION EXISTS_UTIL_REC (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT BOOLEAN,
                          I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_UTILIZATION_CFOP_SQL.EXISTS_UTIL_REC';
   L_fiscal_doc_id  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;

   cursor C_NF_EXISTS is
      select fiscal_doc_id
        from fm_fiscal_doc_header fdh, fm_fiscal_utilization fu
       where fdh.utilization_id = fu.utilization_id
         and fdh.requisition_type = fu.requisition_type
         and fdh.requisition_type = 'STOCK'
         and fu.status = 'A'
         and fdh.status not in ('E')
         and fdh.utilization_id = I_utilization;
BEGIN
   open C_NF_EXISTS;
   ---
   fetch C_NF_EXISTS into L_fiscal_doc_id;
   ---
   if C_NF_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   close C_NF_EXISTS;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END EXISTS_UTIL_REC;
------------------------------------------------------------------------------------------------------
FUNCTION UTIL_REASON_EXISTS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists_reason    IN OUT BOOLEAN,
                             I_utilization_id   IN     FM_UTIL_INVADJ_REASON.UTILIZATION_ID%TYPE,
                             I_reason           IN     FM_UTIL_INVADJ_REASON.REASON%TYPE )
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_UTILIZATION_CFOP_SQL.UTIL_REASON_EXISTS';
   L_dummy varchar2(4);

   cursor C_UTIL_REASON is
       select utilization_id
         from fm_util_invadj_reason
        where utilization_id = I_utilization_id
          and reason = I_reason;
BEGIN

   open C_UTIL_REASON;
   ---
   fetch C_UTIL_REASON into L_dummy;
   ---
   if C_UTIL_REASON%NOTFOUND then
      O_exists_reason := FALSE;
   else
      O_exists_reason := TRUE;
   end if;
   ---
   close C_UTIL_REASON;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END UTIL_REASON_EXISTS;
------------------------------------------------------------------------------------------------------
FUNCTION EXISTS_DOC (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists           IN OUT BOOLEAN,
                     I_utilization_id   IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_FISCAL_UTILIZATION_SQL.EXISTS_DOC';
   L_dummy     VARCHAR2(1);
   
   cursor C_UTILIZATION is
      select '1'
        from fm_fiscal_doc_header fdh, fm_fiscal_utilization fu
       where fu.utilization_id = fdh.utilization_id
         and fdh.utilization_id = I_utilization_id;

BEGIN
   open C_UTILIZATION;
   ---
   fetch C_UTILIZATION into L_dummy;
   ---
   if C_UTILIZATION%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   close C_UTILIZATION;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END EXISTS_DOC;
------------------------------------------------------------------------------------------------------
FUNCTION UTIL_REASON_DUP (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists_reason    IN OUT BOOLEAN,
                          I_reason           IN     FM_UTIL_INVADJ_REASON.REASON%TYPE )
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_UTILIZATION_CFOP_SQL.UTIL_REASON_DUP';
   L_dummy varchar2(1);

   cursor C_UTIL_REASON_DUP is
      select 'X'
        from inv_adj_reason
       where reason in (
                        select reason
                          from fm_util_invadj_reason fuiv,
                               fm_fiscal_utilization fu
                         where fu.utilization_id = fuiv.utilization_id
                        )
         and reason = I_reason;
BEGIN
   open C_UTIL_REASON_DUP;
   ---
   fetch C_UTIL_REASON_DUP into L_dummy;
   ---
   if C_UTIL_REASON_DUP%NOTFOUND then
      O_exists_reason := FALSE;
   else
      O_exists_reason := TRUE;
   end if;
   close C_UTIL_REASON_DUP;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END UTIL_REASON_DUP;
------------------------------------------------------------------------------------------------------
FUNCTION EXISTS_UTILIZATION_REQ_TYPE(O_error_message  IN OUT VARCHAR2,
                                     O_exists         IN OUT BOOLEAN,
                                     I_requisition_type  IN  FM_FISCAL_UTILIZATION.REQUISITION_TYPE%TYPE,
                                     I_utilization_id IN     FM_DOC_TYPE_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(1000) := 'FM_DOC_TYPE_UTILIZATION_SQL.EXISTS_UTILIZATION_REQ_TYPE';
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_UTILIZATION_REQ_TYPE is
      select 'X'
        from fm_fiscal_utilization
       where requisition_type = I_requisition_type
         and utilization_id = I_utilization_id;
   ---
BEGIN
   open C_FM_UTILIZATION_REQ_TYPE;
   ---
   fetch C_FM_UTILIZATION_REQ_TYPE into L_dummy;
   O_exists := C_FM_UTILIZATION_REQ_TYPE%FOUND;
   ---
   close C_FM_UTILIZATION_REQ_TYPE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXISTS_UTILIZATION_REQ_TYPE;
------------------------------------------------------------------------------------------------------
FUNCTION RMS_UTIL_EXISTS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT BOOLEAN,
                          I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_FISCAL_UTILIZATION_SQL.RMS_UTIL_EXISTS';
   L_dummy     VARCHAR2(1);
   
   cursor C_UTILIZATION_EXIST is
      select 'X'
        from V_BR_ORD_UTIL_CODE
       where util_code = i_utilization
       UNION
      select 'X'
        from V_BR_TSF_UTIL_CODE
       where util_code = i_utilization
       UNION
      select 'X'
        from V_BR_MRT_UTIL_CODE
       where util_code = i_utilization;

BEGIN
   open C_UTILIZATION_EXIST;
   --
   fetch C_UTILIZATION_EXIST into L_dummy;
   --
   if C_UTILIZATION_EXIST%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   --
   close C_UTILIZATION_EXIST;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      ---
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      ---
      if C_UTILIZATION_EXIST%ISOPEN then
         --
         close C_UTILIZATION_EXIST;
         --
      end if;
      ---
      return FALSE;
END RMS_UTIL_EXISTS;
------------------------------------------------------------------------------------------------------
FUNCTION DELETE_UTIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_utilization_id IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program     VARCHAR2(50) := 'FM_FISCAL_UTILIZATION_SQL.DELETE_UTIL';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_UTIL_DOC_TYPE IS
      select 'x'
        from fm_doc_type_utilization
       where utilization_id  = I_utilization_id
         for update nowait;

   cursor C_UTIL_REASON IS
      select 'x'
        from fm_util_invadj_reason
       where utilization_id  = I_utilization_id
         for update nowait;

   cursor C_UTIL_ATTRIB IS
      select 'x'
        from fm_utilization_attributes
       where utilization_id  = I_utilization_id
         for update nowait;
BEGIN
   ---
   open C_UTIL_DOC_TYPE;
   close C_UTIL_DOC_TYPE;
   ---
   delete fm_doc_type_utilization
    where utilization_id  = I_utilization_id;
   ---
   open C_UTIL_REASON;
   close C_UTIL_REASON;
   ---
   delete fm_util_invadj_reason
    where utilization_id  = I_utilization_id;
   ---
   open C_UTIL_ATTRIB;
   close C_UTIL_ATTRIB;
   ---
   delete fm_utilization_attributes
    where utilization_id  = I_utilization_id;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_DOC_TYPE_UTILIZATION',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
   when OTHERS then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_UTIL_DOC_TYPE%ISOPEN then
         close C_UTIL_DOC_TYPE;
      end if;
      ---
      if C_UTIL_REASON%ISOPEN then
         close C_UTIL_REASON;
      end if;
      ---
      if C_UTIL_ATTRIB%ISOPEN then
         close C_UTIL_ATTRIB;
      end if;
      ---
      return FALSE;
END DELETE_UTIL;
------------------------------------------------------------------------------------------------------
FUNCTION NFE_UTIL_EXISTS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT BOOLEAN,
                          I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_FISCAL_UTILIZATION_SQL.NFE_UTIL_EXISTS';
   L_dummy     VARCHAR2(1);
   
   cursor C_UTIL_NFE_EXIST is
      select 'X'
        from fm_nfe_config_loc
       where utilization_id = I_utilization;

BEGIN
   open C_UTIL_NFE_EXIST;
   fetch C_UTIL_NFE_EXIST into L_dummy;
   --
   if C_UTIL_NFE_EXIST%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   --
   close C_UTIL_NFE_EXIST;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      ---
      if C_UTIL_NFE_EXIST%ISOPEN then
         close C_UTIL_NFE_EXIST;
      end if;
      ---
      return FALSE;
END NFE_UTIL_EXISTS;
------------------------------------------------------------------------------------------------------
END FM_FISCAL_UTILIZATION_SQL;
/