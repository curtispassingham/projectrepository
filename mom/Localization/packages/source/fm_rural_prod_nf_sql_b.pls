create or replace PACKAGE BODY FM_RURAL_PROD_NF_SQL is
----------------------------------------------------------------------------------
FUNCTION GET_CORRECTED_VALUES(O_error_message     IN OUT VARCHAR2,
                              I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
 return BOOLEAN is
   --
   L_program   VARCHAR2(100) := 'FM_RURAL_PROD_NF_SQL.GET_CORRECTED_VALUES';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   --
   L_recv_qty               FM_RECEIVING_DETAIL.QUANTITY%TYPE;
   L_overage_qty            FM_RECEIVING_DETAIL.QUANTITY%TYPE;
   L_nf_qty                 FM_RECEIVING_DETAIL.QUANTITY%TYPE;
   L_qty_discrep_status     FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE;
   L_cost_discrep_status    FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE;
   L_item                   FM_FISCAL_DOC_DETAIL.ITEM%TYPE;
   L_fiscal_doc_line_id     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_corrected_qty          FM_CORRECTION_DOC.ACTION_VALUE%TYPE;
   L_corrected_cost         FM_CORRECTION_DOC.ACTION_VALUE%TYPE;
   L_resolution_type        FM_RESOLUTION.RESOLUTION_TYPE%TYPE := NULL;
   L_fiscal_doc_id_ref      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE;
   L_fiscal_doc_line_id_ref FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_create_date            FM_FISCAL_DOC_DETAIL.CREATE_DATETIME%TYPE := SYSDATE;
   L_user                   FM_FISCAL_DOC_DETAIL.CREATE_ID%TYPE := USER;
   L_other_proc             BOOLEAN := TRUE;
   --
   L_table        VARCHAR2(100);
   L_key          VARCHAR2(100);
   L_cursor       VARCHAR2(100);
   --
   L_status             INTEGER := NULL;
   L_qty_discrep_count  NUMBER  := 0;
   L_cost_discrep_count NUMBER  := 0;
   --
   CURSOR C_GET_DOC_ID_REF is
      select fdd.fiscal_doc_line_id,
             fdd.fiscal_doc_id_ref,
             fdd.fiscal_doc_line_id_ref
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id
    order by fdd.fiscal_doc_line_id;
   --
   CURSOR C_CHK_DISCREP(P_fiscal_doc_line_id_ref IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdd.qty_discrep_status,
             fdd.cost_discrep_status,
             fdd.item
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id      = fdh.fiscal_doc_id
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id_ref;
   --
   CURSOR C_GET_QTY(P_fiscal_doc_id_ref      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                    P_item                   FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                    P_fiscal_doc_line_id_ref FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select SUM(NVL(frd.quantity,0)) as recv_qty,
             SUM(NVL(frd.overage_qty,0)) as overage_qty,
             fdd.quantity as NF_qty
        from fm_receiving_header frh,
             fm_receiving_detail frd,
             fm_fiscal_doc_detail fdd
       where frh.fiscal_doc_id = P_fiscal_doc_id_ref
         and fdd.fiscal_doc_id = frh.fiscal_doc_id
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id_ref
         and frd.header_seq_no = frh.seq_no
         and fdd.item          = P_item
         and frd.item          = fdd.item
    group by fdd.quantity;
   --
   CURSOR C_GET_CORRECTED_VAL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                              P_discrep_type  varchar2) is
      select fcd.action_value,
             fr.resolution_type
        from fm_correction_doc fcd,
             fm_resolution fr
       where fcd.fiscal_doc_line_id = P_fiscal_doc_line_id
         and fr.fiscal_doc_line_id  = fcd.fiscal_doc_line_id
         and fr.discrep_type        = P_discrep_type
         and fcd.action_type        = (case P_discrep_type when 'Q' then 'CLQ'
                                       else 'CLC'
                                       end);
   --
   CURSOR C_QTY_DISCREP_COUNT(P_fiscal_doc_id_ref FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE) is
      select count(fdd.qty_discrep_status)
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = P_fiscal_doc_id_ref
         and qty_discrep_status='R';
   --
   CURSOR C_COST_DISCREP_COUNT(P_fiscal_doc_id_ref FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE) is
      select count(fdd.cost_discrep_status)
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = P_fiscal_doc_id_ref
         and cost_discrep_status='R';
   --
   CURSOR C_GET_CORRECTED_TAXES (P_fiscal_doc_line_id_ref IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      (select fctd.tax_code as vat_code,
              NVL(fctd.tax_basis,0) as tax_basis,
              NVL(fctd.modified_tax_basis,0) as modified_tax_basis,
              NVL(fctd.tax_amount,0) as total_value,
              NVL(fctd.tax_rate,0) as tax_rate
         from fm_correction_tax_doc fctd,
              fm_correction_doc fcd,
              fm_fiscal_doc_tax_detail fdtd
        where fctd.header_seq_no     = fcd.seq_no
          and fcd.fiscal_doc_line_id = P_fiscal_doc_line_id_ref
          and fcd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
          and fctd.tax_code          = fdtd.vat_code
          and fcd.action_type        ='CLT')
       union
       (select fdtd.vat_code,
               decode(NVL(fdtd.tax_basis,0),0,fdtd.appr_base_value,fdtd.tax_basis) as tax_basis,
               decode(NVL(fdtd.modified_tax_basis,0),0,fdtd.appr_modified_base_value,fdtd.modified_tax_basis) as modified_tax_basis,
               decode(NVL(fdtd.total_value,0),0,fdtd.appr_tax_value,fdtd.total_value) as total_value,
               decode(NVL(fdtd.percentage_rate,0),0,fdtd.appr_tax_rate,fdtd.percentage_rate) as tax_rate
          from fm_fiscal_doc_tax_detail fdtd
         where fdtd.fiscal_doc_line_id=P_fiscal_doc_line_id_ref
           and fdtd.vat_code not in (select fctd.tax_code as vat_code
                                       from fm_correction_tax_doc fctd,
                                            fm_correction_doc fcd
                                      where fctd.header_seq_no= fcd.seq_no
                                        and fcd.fiscal_doc_line_id = P_fiscal_doc_line_id_ref
                                        and fcd.action_type        ='CLT'));
   --
   R_get_corrected_taxes C_GET_CORRECTED_TAXES%ROWTYPE;
   --
   CURSOR C_GET_RPNF_QTY(P_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdd.quantity as correct_qty
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id= fdh.fiscal_doc_id
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id;
   --
R_get_rpnf_qty C_GET_RPNF_QTY%ROWTYPE;
   --
   CURSOR C_LOCK_DOC_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_line_id = P_fiscal_doc_line_id
       for update nowait;
   --
   CURSOR C_LOCK_DOC_HEADER(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = P_fiscal_doc_id
       for update nowait;
   --
   BEGIN
   open C_GET_DOC_ID_REF;
   loop
   fetch C_GET_DOC_ID_REF into L_fiscal_doc_line_id,L_fiscal_doc_id_ref,L_fiscal_doc_line_id_ref;
   exit when C_GET_DOC_ID_REF%NOTFOUND;

      open C_CHK_DISCREP(L_fiscal_doc_line_id_ref);
     fetch C_CHK_DISCREP into L_qty_discrep_status,L_cost_discrep_status,L_item;

      if NVL(L_qty_discrep_status,'M') ='R' then
         open C_GET_QTY(L_fiscal_doc_id_ref,L_item,L_fiscal_doc_line_id_ref);
         fetch C_GET_QTY into L_recv_qty,L_overage_qty,L_nf_qty;

            if L_nf_qty > (L_recv_qty + L_overage_qty) then
               open C_GET_CORRECTED_VAL(L_fiscal_doc_line_id_ref,'Q');
               fetch C_GET_CORRECTED_VAL into L_corrected_qty,L_resolution_type;

               L_cursor := 'C_LOCK_DOC_DETAIL';
               L_table  := 'FM_FISCAL_DOC_DETAIL';
               L_key    := 'Fiscal_doc_line_id: ' || TO_CHAR(L_fiscal_doc_line_id);

               SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
               open C_LOCK_DOC_DETAIL(L_fiscal_doc_line_id);

               SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
               close C_LOCK_DOC_DETAIL;
          ---
               SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);

               update fm_fiscal_doc_detail fdd
                  set fdd.quantity = L_corrected_qty
                where fdd.fiscal_doc_line_id = L_fiscal_doc_line_id;

               close C_GET_CORRECTED_VAL;
            end if;
         close C_GET_QTY;

      L_recv_qty    :=0;
      L_overage_qty :=0;
      L_nf_qty      :=0;

      end if;

      if NVL(L_cost_discrep_status,'M')='R' then
         open C_GET_CORRECTED_VAL(L_fiscal_doc_line_id_ref,'C');
        fetch C_GET_CORRECTED_VAL into L_corrected_cost,L_resolution_type;

           if L_resolution_type= 'SYS' then
              L_cursor := 'C_LOCK_DOC_DETAIL';
              L_table  := 'FM_FISCAL_DOC_DETAIL';
              L_key    := 'Fiscal_doc_line_id: ' || TO_CHAR(L_fiscal_doc_line_id);

              SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
              open C_LOCK_DOC_DETAIL(L_fiscal_doc_line_id);

              SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
              close C_LOCK_DOC_DETAIL;
              ---
              SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);

              update fm_fiscal_doc_detail fdd
                 set fdd.unit_cost = L_corrected_cost
               where fdd.fiscal_doc_line_id = L_fiscal_doc_line_id;
               ---
           end if;
        close C_GET_CORRECTED_VAL;

        update fm_fiscal_doc_detail fdd
           set fdd.total_cost = fdd.unit_cost * fdd.quantity
         where fiscal_doc_line_id = L_fiscal_doc_line_id;
      end if;

      L_resolution_type := null;
      L_corrected_cost  := 0;

      close C_CHK_DISCREP;

      open C_GET_RPNF_QTY(L_fiscal_doc_line_id);
     fetch C_GET_RPNF_QTY into R_get_rpnf_qty;
     close C_GET_RPNF_QTY;

      open C_GET_CORRECTED_TAXES(L_fiscal_doc_line_id_ref);
      loop
         fetch C_GET_CORRECTED_TAXES into R_get_corrected_taxes;
         exit when C_GET_CORRECTED_TAXES%NOTFOUND;
         insert into fm_fiscal_doc_tax_detail(vat_code,
                                              fiscal_doc_line_id,
                                              percentage_rate,
                                              total_value,
                                              create_datetime,
                                              create_id,
                                              last_update_datetime,
                                              last_update_id,
                                              tax_basis,
                                              modified_tax_basis,
                                              unit_tax_amt)
                                       values(R_get_corrected_taxes.vat_code,
                                              L_fiscal_doc_line_id,
                                              R_get_corrected_taxes.tax_rate,
                                              R_get_corrected_taxes.total_value,
                                              L_create_date,
                                              L_user,
                                              L_create_date,
                                              L_user,
                                              R_get_corrected_taxes.tax_basis,
                                              R_get_corrected_taxes.modified_tax_basis,
                                             (R_get_corrected_taxes.total_value)/(R_get_rpnf_qty.correct_qty));
      end loop;
      close C_GET_CORRECTED_TAXES;
   end loop;

   close C_GET_DOC_ID_REF;

   if FM_CALC_TOTALS_SQL.POPULATE_TAX_HEAD(O_error_message,
                                           L_status,
                                           I_fiscal_doc_id)= FALSE then
      return FALSE;
   end if;

    open C_QTY_DISCREP_COUNT(L_fiscal_doc_id_ref);
   fetch C_QTY_DISCREP_COUNT into L_qty_discrep_count;
   close C_QTY_DISCREP_COUNT;

    open C_COST_DISCREP_COUNT(L_fiscal_doc_id_ref);
   fetch C_COST_DISCREP_COUNT into L_cost_discrep_count;
   close C_COST_DISCREP_COUNT;

   if L_qty_discrep_count !=0 then
      L_cursor := 'C_LOCK_DOC_HEADER';
      L_table  := 'FM_FISCAL_DOC_HEADER';
      L_key    := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);

      SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
      open C_LOCK_DOC_HEADER(I_fiscal_doc_id);

      SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
      close C_LOCK_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);

      update fm_fiscal_doc_header fdh
         set fdh.quantity = (select sum(fdd.quantity)
                               from fm_fiscal_doc_detail fdd
                              where fiscal_doc_id = I_fiscal_doc_id)
         where fiscal_doc_id = I_fiscal_doc_id;

   end if;

    if L_qty_discrep_count != 0 or L_cost_discrep_count != 0 then
          if (FM_FISCAL_VALIDATION_SQL.CALC_TOTAL_DISCOUNT(O_error_message,
                                                           L_status,
                                                           L_other_proc,
                                                           I_fiscal_doc_id) = FALSE) then
             return FALSE;
          end if;

          if(FM_CALC_TOTALS_SQL.UPDATE_TOTALS(O_error_message,
                                              L_status,
                                              I_fiscal_doc_id)=FALSE) then
            return FALSE;
          end if;

         if L_status != 0 then
            L_cursor := 'C_LOCK_DOC_HEADER';
            L_table  := 'FM_FISCAL_DOC_HEADER';
            L_key    := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);

           SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
           open C_LOCK_DOC_HEADER(I_fiscal_doc_id);

           SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
           close C_LOCK_DOC_HEADER;

          SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);

          update fm_fiscal_doc_header fdh
             set fdh.total_item_value     = fdh.total_item_calc_value,
                 fdh.total_serv_value     = fdh.total_serv_calc_value,
                 fdh.total_doc_value      = fdh.total_doc_calc_value
           where fdh.fiscal_doc_id        = I_fiscal_doc_id;
         end if;
    end if;

return true;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_DETAIL', L_table, L_key);
         close C_LOCK_DOC_DETAIL;
      end if;

      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', L_table, L_key);
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;

END GET_CORRECTED_VALUES;
----------------------------------------------------------------------------------
FUNCTION CREATE_NEW_NF(O_error_message  IN OUT VARCHAR2,
                       I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                       I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
 return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_RURAL_PROD_NF_SQL.CREATE_NEW_NF';

   L_new_Fiscal_doc_id  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE := NULL;
   ---
   BEGIN

   if FM_FISCAL_DOC_HEADER_SQL.COPY_DOC_HEADER(O_error_message,
                                               L_new_fiscal_doc_id,
                                               I_schedule_no,
                                               null,
                                               null,
                                               I_fiscal_doc_id,
                                               'C') = FALSE then
      return FALSE;
   end if;

   if GET_CORRECTED_VALUES(O_error_message,
                           L_new_fiscal_doc_id)= FALSE then
      return FALSE;
   end if;

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;

END CREATE_NEW_NF;

----------------------------------------------------------------------------------
END FM_RURAL_PROD_NF_SQL;
/