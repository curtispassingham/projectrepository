create or replace PACKAGE TaxDataStageServiceProviderImp AS

/******************************************************************************
 *
 * Operation       : getTaxRequestData
 * Description     : Get staged tax data to send to thirdparty tax provider.
 *
 * Input           : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Output          : "RIB_FiscDocColRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscDocColRBM/v1
 * Description     : Tax data to send to thirdparty tax provider.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "soap:Server"
					side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE getTaxRequestData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                            I_businessObject          IN  "RIB_ChannelRef_REC",
                            O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                            O_businessObject          OUT "RIB_FiscDocColRBM_REC");
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : setTaxResponseData
 * Description     : Save tax data from thirdparty tax provider.

 *
 * Input           : "RIB_FiscDocTaxColRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscDocTaxColRBM/v1
 * Description     : Tax data from thirdparty provider.
 *
 * Output          : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "soap:Server"
                    		side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE setTaxResponseData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                             I_businessObject          IN  "RIB_FiscDocTaxColRBM_REC",
                             O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                             O_businessObject          OUT "RIB_ChannelRef_REC");
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : setFiscalFdnData
 * Description     : Get staged tax data to send to thirdparty tax provider.
 *
 * Input           : "RIB_FiscalFDNColRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscalFDNColRBM/v1
 * Description     : Fiscal Foundation data from thirdparty provider.
 *
 * Output          : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "soap:Server"
					side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE setFiscalFdnData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                           I_businessObject          IN  "RIB_FiscalFDNColRBM_REC",
                           O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                           O_businessObject          OUT "RIB_ChannelRef_REC");
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : getFiscalFdnData
 * Description     : Get fiscal foundation data from thirdparty tax provider.
 *
 * Input           : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Output          : "RIB_FiscalFDNQryRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscalFDNQryRBM/v1
 * Description     : parameters like fiscal code,cut off date etc..send to thirdparty tax provider.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "soap:Server"
					side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE getFiscalFdnData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                           I_businessObject          IN  "RIB_ChannelRef_REC",
                           O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                           O_businessObject          OUT "RIB_FiscalFDNQryRBM_REC");
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : cancelTaxTransactionData
 * Description     : delete the tax data from the thirdparty tax provider in case of transaction failure.
 *
 * Input           : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Output          : "RIB_FiscDocColRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscDocColRBM/v1
 * Description     : details send to the thirdparty tax provider to delete data .
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "soap:Server"
					side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
  ******************************************************************************/
PROCEDURE cancelTaxTransactionData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                                   I_businessObject          IN  "RIB_ChannelRef_REC",
                                   O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                                   O_businessObject          OUT "RIB_FiscDocColRBM_REC");

/******************************************************************************/


END TaxDataStageServiceProviderImp;
/