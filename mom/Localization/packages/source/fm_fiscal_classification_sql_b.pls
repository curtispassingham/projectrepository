CREATE OR REPLACE PACKAGE BODY FM_FISCAL_CLASSIFICATION_SQL is
----------------------------------------------------------------
FUNCTION GET_DESC (O_error_message       IN OUT VARCHAR2,
                   O_classification_desc IN OUT V_FISCAL_CLASSIFICATION.classification_desc%TYPE,
                   I_classification_id   IN     V_FISCAL_CLASSIFICATION.classification_id%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80)   := 'FM_FISCAL_CLASSIFICATION_SQL.GET_DESC';
   L_key       VARCHAR2(80);
   cursor C_CLASSIF is
      select classification_desc
        from v_fiscal_classification
       where classification_id = I_classification_id;

BEGIN

   L_key := 'classification_id = '||I_classification_id;
   SQL_LIB.SET_MARK('OPEN', 'C_CLASSIF', 'V_FISCAL_CLASSIFICATION', L_key);
   open C_CLASSIF;
   --
   SQL_LIB.SET_MARK('FETCH', 'C_CLASSIF', 'V_FISCAL_CLASSIFICATION', L_key);
   fetch C_CLASSIF into O_classification_desc;
   --
   if C_CLASSIF%NOTFOUND then
       SQL_LIB.SET_MARK('CLOSE', 'C_CLASSIF', 'V_FISCAL_CLASSIFICATION', L_key);
       close C_CLASSIF;
       O_error_message := SQL_LIB.create_msg('ORFM_INV_CLASSIF',
                                             NULL,
                                             NULL,
                                             NULL);
       return FALSE;
   else
       SQL_LIB.SET_MARK('CLOSE', 'C_CLASSIF', 'V_FISCAL_CLASSIFICATION', L_key);
       close C_CLASSIF;
       if LANGUAGE_SQL.TRANSLATE( O_classification_desc,
                                  O_classification_desc,
                                  O_error_message) = FALSE then
          return FALSE;
       end if;
       return TRUE;
   end if;
   --
EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
       return FALSE;
END GET_DESC;
----------------------------------------------------------------
FUNCTION GET_COD_DESC (O_error_message       IN OUT VARCHAR2,
                       O_classification_desc IN OUT VARCHAR2,
                       I_classification_id   IN     V_FISCAL_CLASSIFICATION.classification_id%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80)   := 'FM_FISCAL_CLASSIFICATION_SQL.GET_COD_DESC';
   L_key       VARCHAR2(50);
   cursor C_CLASSIF is
      select classification_id||' - '||classification_desc
        from v_fiscal_classification
       where classification_id = I_classification_id
       order by 1;

BEGIN
   L_key := 'classification_id = '||I_classification_id;
   SQL_LIB.SET_MARK('OPEN', 'C_CLASSIF', 'V_FISCAL_CLASSIFICATION', L_key);
   open C_CLASSIF;
   --
   SQL_LIB.SET_MARK('FETCH', 'C_CLASSIF', 'V_FISCAL_CLASSIFICATION', L_key);
   fetch C_CLASSIF into O_classification_desc;
   --
   if C_CLASSIF%NOTFOUND then
       SQL_LIB.SET_MARK('CLOSE', 'C_CLASSIF', 'V_FISCAL_CLASSIFICATION', L_key);
       close C_CLASSIF;
       O_error_message := SQL_LIB.create_msg('ORFM_INV_CLASSIF',
                                             NULL,
                                             NULL,
                                             NULL);
       return FALSE;
   else
       SQL_LIB.SET_MARK('CLOSE', 'C_CLASSIF', 'V_FISCAL_CLASSIFICATION', L_key);
       close C_CLASSIF;
       if LANGUAGE_SQL.TRANSLATE( O_classification_desc,
                                  O_classification_desc,
                                  O_error_message) = FALSE then
          return FALSE;
       end if;
       return TRUE;
   end if;
   --
EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
       return FALSE;
END GET_COD_DESC;
---------------------------------------------------------------------------------------------
FUNCTION EXISTS(O_error_message     IN OUT VARCHAR2,
                O_exists            IN OUT BOOLEAN,
                I_item              IN V_BR_ITEM_FISCAL_ATTRIB.ITEM%TYPE,
                I_classification_id IN V_BR_ITEM_FISCAL_ATTRIB.CLASSIFICATION_ID%TYPE)
   return BOOLEAN is
      L_program VARCHAR2(64) := 'FM_FISCAL_CLASSIFICATION_SQL.EXISTS';
      L_key     VARCHAR2(50);
      L_table   VARCHAR2(50);
      L_dummy   VARCHAR2(1);
      cursor C_EXISTS is
         select '1'
           from v_br_item_fiscal_attrib
          where classification_id = I_classification_id
                and item = I_item ;
   BEGIN
      ---
      if I_classification_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_classification_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
      ---
      L_key   := 'classification_id: ' || I_classification_id;
      L_table := 'v_br_item_fiscal_attrib';
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', L_table, L_key);
      open C_EXISTS;
      SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', L_table, L_key);
      fetch C_EXISTS
         into L_dummy;
      if C_EXISTS%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', L_table, L_key);
      close C_EXISTS;
      ---
      return TRUE;
   EXCEPTION
      when OTHERS then
         if C_EXISTS%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', L_table, L_key);
            close C_EXISTS;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
END EXISTS;
-----------------------------------------------------------------
END FM_FISCAL_CLASSIFICATION_SQL;

 
/
 
