CREATE OR REPLACE PACKAGE FM_TOLERANCES_SQL is
----------------------------------------------------------------------------------------------------
-- Name:       EXIST
-- Purpose:    Determines if a given Supplier exists.
----------------------------------------------------------------------------------------------------
FUNCTION EXIST_SUPPLIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exist	         IN OUT BOOLEAN,
                        I_supplier       IN     VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Name:       GET_SUPPLIER_DESC
-- Purpose:    Returns a Supplier description if a given Supplier exists.
----------------------------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_supplier_desc   IN OUT   SUPS.SUP_NAME%TYPE,
                           I_supplier        IN       SUPS.SUPPLIER%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Name:       CHECK_TOLERANCES
--Purpose:     This function checks to see if the tolerance overlaps with any other
--             tolerances already found on the FM_TOLERANCES table.
----------------------------------------------------------------------------------------------------
FUNCTION CHECK_TOLERANCES (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_lower_overlap     IN OUT  BOOLEAN,
                           O_upper_overlap     IN OUT  BOOLEAN,
                           I_tolerance_id      IN      FM_TOLERANCES.TOLERANCE_ID%TYPE,
                           I_module            IN      FM_TOLERANCES.MODULE%TYPE,
                           I_key_value_1       IN      FM_TOLERANCES.KEY_VALUE_1%TYPE,
                           I_lower_limit       IN      FM_TOLERANCES.LOWER_LIMIT%TYPE,
                           I_upper_limit       IN      FM_TOLERANCES.UPPER_LIMIT%TYPE,
                           I_tolerance_level   IN      FM_TOLERANCES.TOLERANCE_LEVEL%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Name:       EXIST_TOLERANCES
-- Purpose:    Determines if a given key_value_1 for that module exists.
----------------------------------------------------------------------------------------------------
FUNCTION EXIST_TOLERANCES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exist	         IN OUT BOOLEAN,
                          I_module         IN     FM_TOLERANCES.MODULE%TYPE,
                          I_key_value_1    IN     FM_TOLERANCES.KEY_VALUE_1%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Name:       EXIST_PV
-- Purpose:    Determines if a given percent value exists.
----------------------------------------------------------------------------------------------------
FUNCTION EXIST_PV (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists           IN OUT BOOLEAN,
                   I_pv               IN     FM_TOLERANCES.TOLERANCE_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
  -- Function Name: GET_NEXT_TOLERANCE_ID
  -- Purpose : This function gets the next tolerance_id number.
----------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TOLERANCE_ID(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_tolerance_id   IN OUT FM_TOLERANCES.TOLERANCE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
  -- Function Name: LOV_COMPANY
  -- Purpose : This function returns the query for record_group REC_COMPANY.
----------------------------------------------------------------------------------------------------
FUNCTION LOV_COMPANY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_query          IN OUT VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
  -- Function Name: LOV_SUPPLIER
  -- Purpose : This function returns the query for record_group REC_SUPPLIER.
----------------------------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
  -- Function Name: LOV_PERC_VALUE
  -- Purpose : This function returns the query for record_group REC_TOLERANCE_VALUE.
----------------------------------------------------------------------------------------------------
FUNCTION LOV_PERC_VALUE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_query          IN OUT VARCHAR2) 
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
  -- Function Name: SET_WHERE_CLAUSE
  -- Purpose : This function returns the query requested to WHERE_CLAUSE
  --           for form FM_TOLERANCES.
----------------------------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where_clause     IN OUT VARCHAR2,
                          I_module           IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                          I_key_value_1      IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                          I_key_value_2      IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
  -- Function Name: VALIDATE_FISCAL_ENTITY
  -- Purpose : This function validates if a entity compnay/supplier exists and returns the query
  --           for form FM_TOLERANCES.
----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FISCAL_ENTITY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_description    IN OUT VARCHAR2,
                                I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                                I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                                I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------------
END FM_TOLERANCES_SQL;
/
