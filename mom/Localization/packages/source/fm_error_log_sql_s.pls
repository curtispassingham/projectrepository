CREATE OR REPLACE PACKAGE FM_ERROR_LOG_SQL is
-----------------------------------------------------------------------------------------
 FUNCTION WRITE_ERROR(O_error_message IN OUT VARCHAR2
                     ,I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE
                     ,I_program       IN FM_ERROR_LOG.PROGRAM%TYPE
                     ,I_function      IN FM_ERROR_LOG.FUNCTION%TYPE
                     ,I_err_type      IN FM_ERROR_LOG.ERR_TYPE%TYPE
                     ,I_err_code      IN FM_ERROR_LOG.ERR_CODE%TYPE
                     ,I_err_msg       IN FM_ERROR_LOG.ERR_MSG%TYPE
                     ,I_err_data      IN FM_ERROR_LOG.ERR_DATA%TYPE
                     ,I_form_ind      IN NUMBER DEFAULT 1)  return BOOLEAN;
-----------------------------------------------------------------------------------------
 FUNCTION WRITE_ERROR(O_error_message IN OUT VARCHAR2
                     ,I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE
                     ,I_program       IN FM_ERROR_LOG.PROGRAM%TYPE
                     ,I_function      IN FM_ERROR_LOG.FUNCTION%TYPE
                     ,I_err_type      IN FM_ERROR_LOG.ERR_TYPE%TYPE
                     ,I_err_code      IN FM_ERROR_LOG.ERR_CODE%TYPE
                     ,I_err_msg       IN FM_ERROR_LOG.ERR_MSG%TYPE
                     ,I_err_data      IN FM_ERROR_LOG.ERR_DATA%TYPE
                     ,I_err_source    IN FM_ERROR_LOG.ERR_SOURCE_TYPE%TYPE)  return BOOLEAN;
-----------------------------------------------------------------------------------------
 FUNCTION CLEAR_ERROR(O_error_message IN OUT VARCHAR2
                     ,I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE
                     ,I_program       IN FM_ERROR_LOG.PROGRAM%TYPE) return BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION EXISTS_FISCAL(O_error_message  IN OUT VARCHAR2,
                       O_exists         IN OUT BOOLEAN,
                       I_fiscal_id      IN     FM_ERROR_LOG.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION EXISTS_PROGRAM(O_error_message  IN OUT VARCHAR2,
                        O_exists         IN OUT BOOLEAN,
                        I_program        IN     FM_ERROR_LOG.PROGRAM%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------
END FM_ERROR_LOG_SQL;
/