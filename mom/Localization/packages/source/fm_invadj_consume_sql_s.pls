create or replace PACKAGE FM_INVADJ_CONSUME_SQL as
---------------------------------------------------------------------------------------------
-- Function Name: CONSUME
-- Purpose      : Consume Message from fm_stg_adjust_desc and fm_stg_adjust_dtl tables
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind            IN OUT  BOOLEAN,
                 I_seq_no               IN      FM_STG_ADJUST_DESC.SEQ_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: BATCH_CONSUME_THREAD
-- Purpose      : Handles multi-threading when RMS invadjust consume is processed in batch.
--                It is invoked by the batch shell script fm_batch_consume_invadjust.ksh.
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_no     IN     NUMBER,
                              I_num_threads   IN     NUMBER)
return BOOLEAN;
---------------------------------------------------------------------------------------------
END FM_INVADJ_CONSUME_SQL;
/