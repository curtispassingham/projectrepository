CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DOC_TAX_HEAD_SQL is
-------------------------------------------------------------------
FUNCTION EXISTS(O_error_message  IN OUT VARCHAR2,
                O_exists         IN OUT BOOLEAN,
                I_vat_code       IN     FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE,
                I_fiscal_doc_id  IN     FM_FISCAL_DOC_TAX_HEAD.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_TAX_HEAD_SQL.EXISTS';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_FISCAL_DOC_TAX_HEAD is
      select 'x'
        from fm_fiscal_doc_tax_head fdth
       where fdth.vat_code = I_vat_code
         and fdth.fiscal_doc_id = I_fiscal_doc_id;
BEGIN
   ---
   L_key := 'vat_code = '||I_vat_code||' fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id);
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_TAX_HEAD', 'FM_FISCAL_DOC_TAX_HEAD', L_key);
   open C_FM_FISCAL_DOC_TAX_HEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_TAX_HEAD', 'FM_FISCAL_DOC_TAX_HEAD',L_key);
   fetch C_FM_FISCAL_DOC_TAX_HEAD into L_dummy;
   O_exists := C_FM_FISCAL_DOC_TAX_HEAD%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_TAX_HEAD', 'FM_FISCAL_DOC_TAX_HEAD',L_key);
   close C_FM_FISCAL_DOC_TAX_HEAD;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXISTS;
-----------------------------------------------------------------------------------

FUNCTION DELETE_TAX_HEAD(O_error_message       IN OUT VARCHAR2,
                         I_fiscal_doc_id       IN     FM_FISCAL_DOC_TAX_HEAD.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_FISCAL_DOC_TAX_HEAD_SQL.DELETE_TAX_HEAD';
   L_key       VARCHAR2(100);
   ---
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_FM_FISCAL_DOC_TAX_HEAD is
      select 'x'
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   L_key := 'fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id);
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_FM_FISCAL_DOC_TAX_HEAD', 'FM_FISCAL_DOC_TAX_HEAD', L_key);
   open C_LOCK_FM_FISCAL_DOC_TAX_HEAD;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_FM_FISCAL_DOC_TAX_HEAD', 'FM_FISCAL_DOC_TAX_HEAD',L_key);
   close C_LOCK_FM_FISCAL_DOC_TAX_HEAD;
   ---
   SQL_LIB.SET_MARK('DELETE', 'C_LOCK_FM_FISCAL_DOC_TAX_HEAD', 'FM_FISCAL_DOC_TAX_HEAD',L_key);
   delete from fm_fiscal_doc_tax_head fdth
    where fdth.fiscal_doc_id = I_fiscal_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_TAX_HEAD',
                                            L_key,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_TAX_HEAD;
-----------------------------------------------------------------------------------


END FM_FISCAL_DOC_TAX_HEAD_SQL;
/
 