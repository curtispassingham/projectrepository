CREATE OR REPLACE PACKAGE BODY FM_WASTE_ADJ_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_seq_no          IN OUT   FM_STG_ADJUST_DESC.SEQ_NO%TYPE,
                 I_message         IN OUT   FM_STG_ADJUST_DTL%ROWTYPE,
                 I_location        IN       FM_STG_ADJUST_DESC.DC_DEST_ID%TYPE,
                 I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64) := 'FM_WASTE_ADJ_SQL.CONSUME';

   L_key        VARCHAR2(50) := NULL;
   L_table      VARCHAR2(50) := NULL;
   L_cursor     VARCHAR2(50) := NULL;
   L_util_exist varchar2(1)  := NULL;

   cursor C_GET_NEXT_SEQ_DESC is
      select fm_stg_adjust_desc_seq.nextval 
        from dual;
   
   cursor C_GET_NEXT_SEQ_DTL is
      select fm_stg_adjust_dtl_seq.nextval 
        from dual;
   
   cursor C_REASON_UTIL_EXISTS is
      select 'X'
        from fm_util_invadj_reason fuir
       where fuir.reason = I_message.adjustment_reason_code
         and rownum = 1;

BEGIN
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_message.unit_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('QTY_LESS_THAN_ZERO',
                                            NULL,
                                            NULL,
                                            I_message.unit_qty);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_REASON_UTIL_EXISTS';
   L_table  := 'FM_UTIL_INVADJ_REASON';
   L_key    := 'adjustment_reason_code: '||I_message.adjustment_reason_code;
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_REASON_UTIL_EXISTS;
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_REASON_UTIL_EXISTS into L_util_exist;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_REASON_UTIL_EXISTS;
   ---
   if L_util_exist ='X' and I_message.unit_qty < 0 then
      if I_message.desc_seq_no is NULL then
         open C_GET_NEXT_SEQ_DESC;
         fetch C_GET_NEXT_SEQ_DESC into I_message.desc_seq_no;
         close C_GET_NEXT_SEQ_DESC;
      end if;
      ---
      L_key    := 'desc_seq_no: ' || I_message.desc_seq_no;
      L_table  := 'fm_stg_adjust_desc';
      L_cursor := NULL;
      ---
      SQL_LIB.SET_MARK('INSERT', L_cursor, L_table, L_key);
      insert into fm_stg_adjust_desc (SEQ_NO,
                                      DC_DEST_ID,
                                      STATUS,
                                      FISCAL_DOC_ID,
                                      CREATE_DATETIME,
                                      CREATE_ID,
                                      LAST_UPDATE_DATETIME,
                                      LAST_UPDATE_ID)
                              values (I_message.desc_seq_no,
                                      I_location,
                                      'U',
                                      NULL, -- FISCAL_DOC_ID
                                      SYSDATE,
                                      USER,
                                      SYSDATE,
                                      USER);
      ---
      open C_GET_NEXT_SEQ_DTL;
      fetch C_GET_NEXT_SEQ_DTL into I_message.seq_no;
      close C_GET_NEXT_SEQ_DTL;
      ---
      if I_message.unit_qty < 0 then
         -- Outbound
         if NVL(I_inv_status, 0) = 1 then
            -- Merchandise with trouble
            I_message.from_disposition := 'TRBL';
            I_message.to_disposition   := NULL;
         else
            I_message.from_disposition := 'ATS';
            I_message.to_disposition   := NULL;
         end if;
         ---
         I_message.unit_qty := I_message.unit_qty * (-1); 
      end if;
      ---
      L_key    := 'seq_no: ' || I_message.seq_no;
      L_table  := 'fm_stg_adjust_dtl';
      L_cursor := NULL;
      ---
      SQL_LIB.SET_MARK('INSERT', L_cursor, L_table, L_key);
      insert into fm_stg_adjust_dtl (seq_no,
                                     desc_seq_no,
                                     item_id,
                                     adjustment_reason_code,
                                     unit_qty,
                                     transshipment_nbr,
                                     from_disposition,
                                     to_disposition,
                                     from_trouble_code,
                                     to_trouble_code,
                                     from_wip_code,
                                     to_wip_code,
                                     transaction_code,
                                     user_id,
                                     create_date,
                                     po_nbr,
                                     doc_type,
                                     aux_reason_code,
                                     weight,
                                     weight_uom,
                                     ebc_cost,
                                     create_datetime,
                                     create_id,
                                     last_update_datetime,
                                     last_update_id)
                             values (I_message.seq_no,
                                     I_message.desc_seq_no,
                                     I_message.item_id,
                                     I_message.adjustment_reason_code,
                                     I_message.unit_qty,
                                     I_message.transshipment_nbr,
                                     I_message.from_disposition,
                                     I_message.to_disposition,
                                     I_message.from_trouble_code,
                                     I_message.to_trouble_code,
                                     I_message.from_wip_code,
                                     I_message.to_wip_code,
                                     I_message.transaction_code,
                                     USER,
                                     SYSDATE,
                                     I_message.po_nbr,
                                     I_message.doc_type,
                                     I_message.aux_reason_code,
                                     I_message.weight,
                                     I_message.weight_uom,
                                     I_message.ebc_cost,
                                     SYSDATE,
                                     USER,
                                     SYSDATE,
                                     USER);
      ---
      O_seq_no := I_message.desc_seq_no;
   end if;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CONSUME;
---------------------------------------------------------------------------------------------
END FM_WASTE_ADJ_SQL;
/
