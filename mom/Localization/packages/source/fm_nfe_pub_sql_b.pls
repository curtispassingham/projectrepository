CREATE OR REPLACE PACKAGE BODY FM_NFE_PUB_SQL IS
--------------------------------------------------------------------------------
FUNCTION  GET_NFE_INFO(O_status_code    IN OUT  NUMBER,
                       O_error_message  IN OUT  VARCHAR2,
                       O_message        IN OUT  "OBJ_FM_NFE_DOCHDR_REC",
                       I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
return BOOLEAN is
   ---
   L_program            VARCHAR2(100) := 'FM_NFE_PUB_SQL.GET_NFE_INFO';
   L_count              NUMBER;
   L_supp               FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE;
   L_ptnr               FM_FISCAL_DOC_HEADER.PARTNER_ID%TYPE;
   L_loc                FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE;
   L_error_message      VARCHAR2(255);
   L_error_ind          BOOLEAN:=FALSE;
   L_legal_msg          FM_FISCAL_DOC_TAX_DETAIL.LEGAL_MESSAGE_TEXT%TYPE;
   L_cod_ean            ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE;
   L_ind_pgto           NUMBER(1);
   L_perc_mva           FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_PERC_MVA%TYPE;
   L_tax_perc_redu_base FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_PERC_REDU_BASE%TYPE;
   L_tax_val_mva        NUMBER(1);
   L_ie_subs            L10N_BR_ENTITY_TRIB_SUBS.IE_SUBS%TYPE;
   ---
   cursor C_GET_PTNR_INFO(P_partner_id FM_FISCAL_DOC_HEADER.PARTNER_ID%TYPE,
                          P_partner_type FM_FISCAL_DOC_HEADER.PARTNER_TYPE%TYPE) is
     select vprn.partner_id cod_pfj,
             p.partner_desc razao_social,
             vpfa.addr_1 endereco,
             vprn.cnpj CNPJ,
             vprn.cpf CPF,
             vprn.ie inscr_estadual,
             DECODE(vpfa.country,'BR','1058',vpfa.country) COD_PAIS,
             vpfa.city COD_MUN,
             vprn.suframa SUFRAMA,
             vpfa.addr_2 NUM,
             vpfa.addr_3 COMPLEMENTO,
             c.jurisdiction_desc CIDADE,
             SUBSTR(vpfa.city,1,2) IBG_ESTADO,
             vpfa.state ESTADO,
             vpfa.postal_code CEP,
             vpfa.neighborhood BAIRRO,
             p.contact_phone FONE,
             p.contact_email EMAIL,
             co.country_desc SISCOMEX_ID,
             regexp_replace(ecc.cnae_code, '+[^[:digit:]]+')  COD_CNAE,
             3 COD_REG_TRIB
       from  v_br_partner_fiscal_addr vpfa,
             v_br_partner_reg_num vprn,
             partner p,
             country_tax_jurisdiction c,
             country co,
             l10n_br_entity_cnae_codes ecc
       where vpfa.partner_id = vprn.partner_id
         and p.partner_id = vprn.partner_id
         and vpfa.city = c.jurisdiction_code
         and vpfa.country = c.country_id
         and vpfa.state  = c.state
         and vpfa.country = co.country_id
         and co.country_id = 'BR'
         and p.partner_id = P_partner_id
         and p.partner_type=P_partner_type
         and ecc.key_value_1 = P_partner_id
         and ecc.key_value_2 = P_partner_type
         and ecc.module = 'PTNR'
         and ecc.country_id = co.country_id
         and ecc.primary_ind = 'Y';
   R_get_ptnr_info C_GET_PTNR_INFO%ROWTYPE;
   cursor C_GET_CUST_INFO(P_cust_id FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE) is
      select frh.cust_id cod_pfj,
             frh.cust_name razao_social,
             frh.addr_1 endereco,
             frh.cnpj CNPJ,
             frh.cpf CPF,
             frh.ie inscr_estadual,
             DECODE(frh.country_id,'BR','1058',frh.country_id) COD_PAIS,
             frh.city COD_MUN,
             frh.suframa SUFRAMA,
             frh.addr_2 NUM,
             frh.addr_3 COMPLEMENTO,
             c.jurisdiction_desc CIDADE,
             SUBSTR(frh.city,1,2) IBG_ESTADO,
             frh.state ESTADO,
             frh.postal_code CEP,
             frh.neighborhood BAIRRO
       from  fm_rma_head frh,
             country_tax_jurisdiction c
       where frh.fiscal_doc_id = I_fiscal_doc_id
         and frh.city = c.jurisdiction_code
         and frh.country_id = c.country_id
         and frh.state  = c.state
         and frh.cust_id = P_cust_id;
   R_get_cust_info C_GET_CUST_INFO%ROWTYPE;
   cursor C_GET_SUPP_INFO(P_supplier FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE) is
     select vsfa.supplier COD_PFJ,
            s.sup_name RAZAO_SOCIAL,
            vsfa.addr_1 ENDERECO,
            vsfa.neighborhood BAIRRO,
            DECODE(vsfa.country,'BR','1058',vsfa.country) COD_PAIS,
            vsrn.cnpj CNPJ,
            vsrn.cpf CPF,
            vsrn.ie INSCR_ESTADUAL,
            vsfa.city COD_MUN,
            vsrn.suframa SUFRAMA,
            vsfa.addr_2 NUM,
            vsfa.addr_3 COMPLEMENTO,
            SUBSTR(vsfa.city,1,2) IBG_ESTADO,
            vsfa.state ESTADO,
            vsfa.postal_code CEP,
            c.jurisdiction_desc CIDADE,
            s.contact_phone FONE,
            s.contact_email EMAIL,
            co.country_desc SISCOMEX_ID,
            decode(vsrn.simples_ind,'Y',1,'N',3) COD_REG_TRIB
       from v_br_sups_fiscal_addr vsfa,
            v_br_sups vsrn,
            sups s,
            country_tax_jurisdiction c,
            country co
      where vsfa.supplier=vsrn.supplier
        and s.supplier=vsrn.supplier
        and vsfa.city = c.jurisdiction_code
        and vsfa.country = c.country_id
        and vsfa.state  = c.state
        and vsfa.country = co.country_id
        and co.country_id = 'BR'
        and s.supplier = P_supplier;

   R_get_supp_info C_GET_SUPP_INFO%ROWTYPE;

   cursor C_GET_LOC_INFO(P_loc_type    FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                         P_location_id FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE) is
        select  vfa.key_value_1 COD_PFJ,
                decode(P_loc_type,'W',(select wh_name from wh where wh=P_location_id ),(select store_name from store where store=P_location_id))RAZAO_SOCIAL,
                vfa.ie INSCR_ESTADUAL,
                vfa.im INSCR_MUNICIPAL,
                vfa.cnpj CNPJ,
                vfa.cpf CPF,
                vfa.neighborhood BAIRRO,
                SUBSTR(vfa.city,1,2) IBG_ESTADO,
                vfa.state ESTADO,
                vfa.postal_code CEP,
                DECODE(vfa.country_id,'BR','1058',vfa.country_id) COD_PAIS,
                vfa.city COD_MUN,
                vfa.suframa SUFRAMA,
                decode(P_loc_type,'W',(select wh_name_secondary from wh where wh=P_location_id ),(select store_name_secondary from store where store=P_location_id)) FANTASIA,
                vfa.addr_1 ENDERECO,
                vfa.addr_2 NUM,
                vfa.addr_3 COMPLEMENTO,
                decode(P_loc_type,'W',NULL,(select phone_number from store where store=P_location_id)) FONE,
                decode(P_loc_type,'W',(select email from wh where wh=P_location_id),(select email from store where store=P_location_id)) EMAIL,
                c.jurisdiction_desc CIDADE,
                co.country_desc SISCOMEX_ID,
                regexp_replace(ecc.cnae_code, '+[^[:digit:]]+')  COD_CNAE,
                decode(vfa.simples_ind,'Y',1,'N',3) COD_REG_TRIB
           FROM v_fiscal_attributes vfa,
                country_tax_jurisdiction c,
                country co,
                l10n_br_entity_cnae_codes ecc
          WHERE vfa.module='LOC'
            AND c.jurisdiction_code=vfa.city
            AND co.country_id=vfa.country_id
            and vfa.country_id = c.country_id
            and vfa.state  = c.state
            AND co.country_id = 'BR'
            AND vfa.key_value_1 = P_location_id
            and ecc.key_value_1 = P_location_id
            and ecc.key_value_2 = P_loc_type
            and ecc.module = 'LOC'
            and ecc.country_id = co.country_id
            and ecc.primary_ind = 'Y';
   R_get_loc_info C_GET_LOC_INFO%ROWTYPE;
   cursor C_GET_NFE_DETAIL is
      SELECT  ffdd.fiscal_doc_line_id,
              ffdd.fiscal_doc_id_ref,
              ffdd.line_no LINE_NUMBER,
              ffdd.item COD_PRODUTO,
              ffdd.quantity QUANTIDADE,
              ffdd.unit_cost PRECO_UNITARIO,
              ffdd.total_cost VLR_TOTAL_ITEM,
              im.standard_uom COD_MEDIDA,
              ffdd.nf_cfop COD_CFOP,
              SUBSTR(ffdd.classification_id,1,8) COD_NCM,
              vifa.origin_code TP_ORIGEM_ITEM,
              SUBSTR(ffdd.icms_cst,LENGTH(ffdd.icms_cst)-1,2) COD_SIT_TRIB_EST,
              (ffdd.quantity * ffdd.freight_cost) VLR_FRETE,
              (ffdd.quantity * ffdd.insurance_cost) VLR_SEGURO,
              (ffdd.quantity * ffdd.other_expenses_cost) VLR_OUTRAS,
              (ffdd.quantity*ffdd.unit_item_disc) VLR_DESCONTO,
              ffdd.unit_header_disc,
              ffdd.pis_cst CST_PIS,
              ffdd.cofins_cst CST_COFINS,
              im.item_desc DESCRICAO,
              vifa.service_code COD_LST,
              1 IND_TOT,
               vifa.ex_ipi EX_IPI,
              ffdd.requisition_no NUM_PEDIDO,
              vifa.service_ind
         from fm_fiscal_doc_detail ffdd,
              fm_fiscal_doc_header ffdh,
              fm_schedule fs,
              item_master im,
              fm_fiscal_utilization ffu,
              v_br_item_fiscal_attrib vifa
        where ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
          and ffdd.item = im.item
          and im.item=vifa.item
          and ffdh.schedule_no = fs.schedule_no
          and (fs.mode_type = 'EXIT' or ffdh.requisition_type in ('RMA'))
          and ffdh.status not in ('I')
          and ffu.utilization_id = ffdh.utilization_id
          and ffdh.fiscal_doc_id = I_fiscal_doc_id;
   R_get_nfe_dtl C_GET_NFE_DETAIL%ROWTYPE;
   cursor C_GET_NFE_HEADER is
     SELECT  TO_CHAR(ffdh.fiscal_doc_no) NUM_DOCFIS,
                  ffdh.issue_date DATA_EMISSAO,
                  ffdh.entry_or_exit_date DT_RECEBIMENTO,
                  TO_CHAR(ffdh.freight_cost) VLR_FRETE,
                  TO_CHAR(ffdh.insurance_cost) VLR_SEGURO,
                  TO_CHAR(ffdh.other_expenses_cost) VLR_OUTRAS,
                  ffdh.vehicle_plate PLACA_VEICULO,
                  TO_CHAR(DECODE(ffdh.discount_type, 'P',
                                 DECODE(NVL(ffdh.total_discount_value,0),
                                             0,
                                             NULL,
                                             (ffdh.total_doc_value * ffdh.total_discount_value / 100),
                                             ffdh.total_discount_value))) VLR_DESCONTO,
                  TO_CHAR(ffdh.total_doc_value) VLR_TOT_NOTA,
                  fn.nop_desc COD_NATUREZA_OP,
                  ffdh.quantity   QTD_VOL,
                  ffdh.total_weight PESO_BRT,
                  ffdh.net_weight PESO_LIQ,
                  ffdh.total_item_value VLR_PRODUTO,
                  ffdh.total_serv_value VL_SERV,
                  ffdh.process_origin IND_PROC,
                  ffdh.process_value NUM_PROC,
                  ffdh.exit_hour HORA_ENT_SAI,
                  1 TIP_EMISS_NFE,
                  1 FIN_NFE,
                  0 PROC_EMISS_NFE,
                  DECODE (ffdh.freight_type,'CIF',1,'FOB',2) ind_frt,
                  ffdh.requisition_type TIPO_PEDIDO,
                  ffdh.unit_type COD_MEDIDA,
                  1 UTILIZATION_MODE,
                  fm_aleatory_seq.nextval ALEATORY_NUMBER,
                  55 NFE_DOC_TYPE,
                  ffdh.series_no SERIE_DOCFIS,
                  NULL CHAVE_RURAL_PROD_REFER
             from fm_fiscal_doc_header ffdh,
                  fm_schedule fs,
                  fm_fiscal_utilization ffu,
                  fm_nop fn
            where ffdh.schedule_no = fs.schedule_no
              and (fs.mode_type = 'EXIT' or ffdh.requisition_type in ('RMA','RPO'))
              and ffdh.status not in ('I')
              and ffu.utilization_id = ffdh.utilization_id
              and ffu.nop=fn.nop
              and ffdh.fiscal_doc_id = I_fiscal_doc_id;
   R_get_nfe_hdr C_GET_NFE_HEADER%ROWTYPE;
   cursor C_GET_LOC_SUPP_PTNR is
      select location_type,
             location_id,
             key_value_1,
             key_value_2,
             partner_id,
             partner_type,
             module,
             requisition_type
        from fm_fiscal_doc_header
       where fiscal_doc_id = I_fiscal_doc_id;
   R_get_loc_supp_ptnr C_GET_LOC_SUPP_PTNR%ROWTYPE;
   cursor C_GET_HDR_TAXES is
      select vat_code,
             nvl(modified_tax_basis,tax_basis) tax_basis,
             total_value
        from fm_fiscal_doc_tax_head
       where fiscal_doc_id = I_fiscal_doc_id;
   R_get_hdr_taxes C_GET_HDR_TAXES%ROWTYPE;
   cursor C_GET_DTL_TAXES(P_fiscal_doc_line_id FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select vat_code,
             nvl(modified_tax_basis,tax_basis) tax_basis,
             total_value,
             percentage_rate
        from fm_fiscal_doc_tax_detail
       where fiscal_doc_line_id = P_fiscal_doc_line_id;
   R_get_dtl_taxes C_GET_DTL_TAXES%ROWTYPE;
   cursor C_GET_LEGAL_MSG is
    select FM_SPED_SQL.fm_legal_message(
                  CAST(
                   MULTISET(select distinct legal_message_text
                                from fm_fiscal_doc_tax_detail fdtd,
                                     fm_fiscal_doc_detail ffdd
                               WHERE ffdd.fiscal_doc_id=I_fiscal_doc_id
                                 and fdtd.fiscal_doc_line_id = ffdd.fiscal_doc_line_id
                           ) AS fm_legal_msg_type))
             FROM dual;
   cursor C_GET_ITEM_NUMBER_TYPE(P_item ITEM_MASTER.ITEM%TYPE) is
    select item_number_type
      from item_master
     where item=P_item
       and item_level=3;
   cursor C_GET_PAYMENT_IND(P_issue_date FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE) is
     WITH count_payment_date AS
      (SELECT COUNT(ffdp.payment_date) cnt_payment_date
         FROM fm_fiscal_doc_payments ffdp
        WHERE NVL(ffdp.fiscal_doc_id,I_fiscal_doc_id)=I_fiscal_doc_id)
       SELECT DECODE(cnt_payment_date,0,2,1,(SELECT
                                                    CASE
                                                      WHEN P_issue_date = payment_date
                                                      THEN 0
                                                      WHEN payment_date > P_issue_date
                                                      THEN 1
                                                      WHEN payment_date IS NULL
                                                      THEN 2
                                                    END
                                                  FROM fm_fiscal_doc_payments
                                                  WHERE fiscal_doc_id = I_fiscal_doc_id
                                                  ),1)
        FROM count_payment_date;
   ---
   cursor C_GET_PERC_MVA(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                         P_tax_code           FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE) is
    select tax_perc_mva,
           tax_val_mva,
           tax_perc_redu_base
      from fm_fiscal_doc_tax_detail_ext
     where fiscal_doc_line_id = P_fiscal_doc_line_id
       and tax_code = P_tax_code;
   ---
   cursor C_GET_NFE_REFER_INFO(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
    select ffdh.nfe_accesskey,
           SUBSTR(vsfa.city,1,2) IBG_ESTADO_REFER,
           ffdh.issue_date DATA_EMISSAO_REFER,
           vsrn.cnpj CNPJ_REFER,
           vsrn.cpf CPF_REFER,
           vsrn.ie INSCR_ESTADUAL_REFER,
           ffdh.type_id NFE_DOC_TYPE_REFER,
           ffdh.fiscal_doc_no NUM_DOCFIS_REFER,
           ffdh.series_no SERIE_DOCFIS_REFER
      from fm_fiscal_doc_header ffdh,
           v_br_sups_fiscal_addr vsfa,
           v_br_sups_reg_num vsrn
     where ffdh.fiscal_doc_id = P_fiscal_doc_id
       and vsfa.supplier = vsrn.supplier
       and vsfa.supplier = ffdh.key_value_1
       and ffdh.requisition_type = 'PO'
       and ffdh.status = 'A';
   R_get_nfe_refer_info   C_GET_NFE_REFER_INFO%ROWTYPE;
   ---
   cursor C_GET_TRIB_SUBS_ST(P_source_loc  FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                             P_source_type FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                             P_state       FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE) is
     select ie_subs
       from l10n_br_entity_trib_subs
      where entity      = P_source_loc
        and entity_type = P_source_type
        and state       = P_state;
   ---
   L_nfe_hdr_msg       "OBJ_FM_NFE_DOCHDR_REC";
   L_nfe_issuer_msg    "OBJ_FM_NFE_ENTITY_REC";
   L_nfe_addressee_msg "OBJ_FM_NFE_ENTITY_REC";
   L_nfe_trans_msg     "OBJ_FM_NFE_ENTITY_REC";
BEGIN
   ---
   open C_GET_LOC_SUPP_PTNR;
   fetch C_GET_LOC_SUPP_PTNR into R_get_loc_supp_ptnr;
   close C_GET_LOC_SUPP_PTNR;
   ---populate header information
   open C_GET_NFE_HEADER;
   fetch C_GET_NFE_HEADER into R_get_nfe_hdr;
   L_nfe_hdr_msg                    := "OBJ_FM_NFE_DOCHDR_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
   L_nfe_hdr_msg.num_docfis         := R_get_nfe_hdr.num_docfis;
   L_nfe_hdr_msg.data_emissao       := R_get_nfe_hdr.data_emissao;
   L_nfe_hdr_msg.dt_recebimento     := R_get_nfe_hdr.dt_recebimento;
   L_nfe_hdr_msg.vlr_frete          := R_get_nfe_hdr.vlr_frete;
   L_nfe_hdr_msg.vlr_seguro         := R_get_nfe_hdr.vlr_seguro;
   L_nfe_hdr_msg.vlr_outras         := R_get_nfe_hdr.vlr_outras;
   L_nfe_hdr_msg.placa_veiculo      := R_get_nfe_hdr.placa_veiculo;
   L_nfe_hdr_msg.vlr_desconto       := 0;
   L_nfe_hdr_msg.vlr_tot_nota       := R_get_nfe_hdr.vlr_tot_nota;
   L_nfe_hdr_msg.cod_natureza_op    := R_get_nfe_hdr.cod_natureza_op;
   L_nfe_hdr_msg.qtd_vol            := R_get_nfe_hdr.qtd_vol;
   L_nfe_hdr_msg.peso_brt           := R_get_nfe_hdr.peso_brt;
   L_nfe_hdr_msg.peso_liq           := R_get_nfe_hdr.peso_liq;
   L_nfe_hdr_msg.vlr_produto        := R_get_nfe_hdr.vlr_produto;
   L_nfe_hdr_msg.vl_serv            := R_get_nfe_hdr.vl_serv;
   L_nfe_hdr_msg.ind_proc           := R_get_nfe_hdr.ind_proc;
   L_nfe_hdr_msg.num_proc           := R_get_nfe_hdr.num_proc;
   L_nfe_hdr_msg.hora_ent_sai       := R_get_nfe_hdr.hora_ent_sai;
   L_nfe_hdr_msg.tip_emiss_nfe      := R_get_nfe_hdr.tip_emiss_nfe;
   L_nfe_hdr_msg.fin_nfe            := R_get_nfe_hdr.fin_nfe;
   L_nfe_hdr_msg.proc_emiss_nfe     := R_get_nfe_hdr.proc_emiss_nfe;
   L_nfe_hdr_msg.ind_frt            := R_get_nfe_hdr.ind_frt;
   L_nfe_hdr_msg.tipo_pedido        := R_get_nfe_hdr.tipo_pedido;
   L_nfe_hdr_msg.cod_medida         := R_get_nfe_hdr.cod_medida;
   L_nfe_hdr_msg.utilization_mode   := R_get_nfe_hdr.utilization_mode;
   L_nfe_hdr_msg.aleatory_number    := R_get_nfe_hdr.aleatory_number;
   L_nfe_hdr_msg.nfe_doc_type       := R_get_nfe_hdr.nfe_doc_type;
   L_nfe_hdr_msg.serie_docfis       := R_get_nfe_hdr.serie_docfis;
   ---
   open C_GET_HDR_TAXES;
   LOOP
      fetch C_GET_HDR_TAXES into R_get_hdr_taxes;
      EXIT when C_GET_HDR_TAXES%NOTFOUND;
      if R_get_hdr_taxes.vat_code = 'PIS' then
         L_nfe_hdr_msg.vlr_total_pis  :=  R_get_hdr_taxes.total_value;
      elsif R_get_hdr_taxes.vat_code = 'COFINS' then
         L_nfe_hdr_msg.vlr_total_cofins  :=  R_get_hdr_taxes.total_value;
      elsif R_get_hdr_taxes.vat_code = 'ISS' then
         L_nfe_hdr_msg.vlr_base_iss   :=  R_get_hdr_taxes.tax_basis;
         L_nfe_hdr_msg.vlr_total_iss  :=  R_get_hdr_taxes.total_value;
      elsif R_get_hdr_taxes.vat_code = 'ICMS' then
         L_nfe_hdr_msg.vlr_base_icms  :=  R_get_hdr_taxes.tax_basis;
         L_nfe_hdr_msg.vlr_total_icms :=  R_get_hdr_taxes.total_value;
      elsif R_get_hdr_taxes.vat_code = 'ICMSST' then
         L_nfe_hdr_msg.vlr_base_icms_st   :=  R_get_hdr_taxes.tax_basis;
         L_nfe_hdr_msg.vlr_total_icms_st  :=  R_get_hdr_taxes.total_value;
      end if;
   END LOOP;
   close C_GET_HDR_TAXES;
   ---
   open C_GET_LEGAL_MSG;
   fetch C_GET_LEGAL_MSG into L_legal_msg;
   close C_GET_LEGAL_MSG;
   L_nfe_hdr_msg.txt            := L_legal_msg;
   --
   open C_GET_PAYMENT_IND(R_get_nfe_hdr.data_emissao);
   fetch C_GET_PAYMENT_IND into L_ind_pgto;
   close C_GET_PAYMENT_IND;
   ---
   L_nfe_hdr_msg.ind_pgto           := L_ind_pgto;
   ---
   close C_GET_NFE_HEADER;
   --L_nfe_hdr_msg.nfe_issuer        := "OBJ_FM_NFE_LOC_TBL"();
   if R_get_loc_supp_ptnr.requisition_type in ('RTV','TSF','STOCK','IC','REP') then
   -- Populate Location details
     open C_GET_LOC_INFO(R_get_loc_supp_ptnr.location_type,R_get_loc_supp_ptnr.location_id);
     fetch C_GET_LOC_INFO into R_get_loc_info;
     L_nfe_issuer_msg                    := "OBJ_FM_NFE_ENTITY_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
     L_nfe_issuer_msg.cod_pfj            := R_get_loc_info.cod_pfj;
     L_nfe_issuer_msg.razao_social       := R_get_loc_info.razao_social;
     L_nfe_issuer_msg.inscr_estadual     := R_get_loc_info.inscr_estadual;
     L_nfe_issuer_msg.inscr_municipal    := R_get_loc_info.inscr_municipal;
     L_nfe_issuer_msg.cnpj               := R_get_loc_info.cnpj;
     L_nfe_issuer_msg.cpf                := R_get_loc_info.cpf;
     L_nfe_issuer_msg.bairro             := R_get_loc_info.bairro;
     L_nfe_issuer_msg.ibg_estado         := R_get_loc_info.ibg_estado;
     L_nfe_issuer_msg.estado             := R_get_loc_info.estado;
     L_nfe_issuer_msg.cep                := R_get_loc_info.cep;
     L_nfe_issuer_msg.cod_pais           := R_get_loc_info.cod_pais;
     L_nfe_issuer_msg.cod_mun            := R_get_loc_info.cod_mun;
     L_nfe_issuer_msg.suframa            := R_get_loc_info.suframa;
     L_nfe_issuer_msg.fantasia           := R_get_loc_info.fantasia;
     L_nfe_issuer_msg.endereco           := R_get_loc_info.endereco;
     L_nfe_issuer_msg.num                := R_get_loc_info.num;
     L_nfe_issuer_msg.complemento        := R_get_loc_info.complemento;
     L_nfe_issuer_msg.fone               := R_get_loc_info.fone;
     L_nfe_issuer_msg.email              := R_get_loc_info.email;
     L_nfe_issuer_msg.cidade             := R_get_loc_info.cidade;
     L_nfe_issuer_msg.siscomex_id        := R_get_loc_info.siscomex_id;
     L_nfe_issuer_msg.cod_cnae           := R_get_loc_info.cod_cnae;
     L_nfe_issuer_msg.cod_reg_trib       := R_get_loc_info.cod_reg_trib;
     --L_nfe_hdr_msg.nfe_loc_tbl.extend;
     L_nfe_hdr_msg.nfe_issuer_rec     := L_nfe_issuer_msg;
     close C_GET_LOC_INFO;
   elsif R_get_loc_supp_ptnr.requisition_type ='RPO' then
     open C_GET_SUPP_INFO(R_get_loc_supp_ptnr.key_value_1);
     fetch C_GET_SUPP_INFO into R_get_supp_info;
     L_nfe_issuer_msg                   := "OBJ_FM_NFE_ENTITY_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
     L_nfe_issuer_msg.cod_pfj           := R_get_supp_info.cod_pfj;
     L_nfe_issuer_msg.razao_social      := R_get_supp_info.razao_social;
     L_nfe_issuer_msg.endereco          := R_get_supp_info.endereco;
     L_nfe_issuer_msg.bairro            := R_get_supp_info.bairro;
     L_nfe_issuer_msg.cod_pais          := R_get_supp_info.cod_pais;
     L_nfe_issuer_msg.cnpj              := R_get_supp_info.cnpj;
     L_nfe_issuer_msg.cpf               := R_get_supp_info.cpf;
     L_nfe_issuer_msg.inscr_estadual    := R_get_supp_info.inscr_estadual;
     L_nfe_issuer_msg.cod_mun           := R_get_supp_info.cod_mun;
     L_nfe_issuer_msg.num               := R_get_supp_info.num;
     L_nfe_issuer_msg.complemento       := R_get_supp_info.complemento;
     L_nfe_issuer_msg.estado            := R_get_supp_info.estado;
     L_nfe_issuer_msg.ibg_estado        := R_get_supp_info.ibg_estado;
     L_nfe_issuer_msg.cep               := R_get_supp_info.cep;
     L_nfe_issuer_msg.suframa           := R_get_supp_info.suframa;
     L_nfe_issuer_msg.cidade            := R_get_supp_info.cidade;
     L_nfe_issuer_msg.fone              := R_get_supp_info.fone;
     L_nfe_issuer_msg.email             := R_get_supp_info.email;
     L_nfe_issuer_msg.siscomex_id       := R_get_supp_info.siscomex_id;
     L_nfe_issuer_msg.cod_reg_trib       := R_get_supp_info.cod_reg_trib;
     ---
     L_nfe_hdr_msg.nfe_issuer_rec     := L_nfe_issuer_msg;
     close C_GET_SUPP_INFO;
   elsif R_get_loc_supp_ptnr.requisition_type ='RMA' then
    -- put RMA ISSUER information - Customer Information
     open C_GET_CUST_INFO(R_get_loc_supp_ptnr.key_value_1);
     fetch C_GET_CUST_INFO into R_get_cust_info;
     L_nfe_issuer_msg                  := "OBJ_FM_NFE_ENTITY_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
     L_nfe_issuer_msg.cod_pfj          := R_get_cust_info.cod_pfj;
     L_nfe_issuer_msg.razao_social     := R_get_cust_info.razao_social;
     L_nfe_issuer_msg.endereco         := R_get_cust_info.endereco;
     L_nfe_issuer_msg.bairro           := R_get_cust_info.bairro;
     L_nfe_issuer_msg.cod_pais         := R_get_cust_info.cod_pais;
     L_nfe_issuer_msg.cnpj             := R_get_cust_info.cnpj;
     L_nfe_issuer_msg.cpf              := R_get_cust_info.cpf;
     L_nfe_issuer_msg.inscr_estadual   := R_get_cust_info.inscr_estadual;
     L_nfe_issuer_msg.cod_mun          := R_get_cust_info.cod_mun;
     L_nfe_issuer_msg.num              := R_get_cust_info.num;
     L_nfe_issuer_msg.complemento      := R_get_cust_info.complemento;
     L_nfe_issuer_msg.estado           := R_get_cust_info.estado;
     L_nfe_issuer_msg.ibg_estado       := R_get_cust_info.ibg_estado;
     L_nfe_issuer_msg.cep              := R_get_cust_info.cep;
     L_nfe_issuer_msg.suframa          := R_get_cust_info.suframa;
     L_nfe_issuer_msg.cidade           := R_get_cust_info.cidade;
     L_nfe_hdr_msg.nfe_issuer_rec    := L_nfe_issuer_msg;
     close C_GET_CUST_INFO;
   end if;
   --- Get the addresse Details
   if R_get_loc_supp_ptnr.requisition_type in ('STOCK','IC','RMA','RPO','TSF','REP') then
     if R_get_loc_supp_ptnr.key_value_2 in ('S','W')  or R_get_loc_supp_ptnr.requisition_type in ('RMA','RPO') then
       if R_get_loc_supp_ptnr.requisition_type in ('RMA','RPO') then
         open C_GET_LOC_INFO(R_get_loc_supp_ptnr.location_type,R_get_loc_supp_ptnr.location_id);
       else
         open C_GET_LOC_INFO(R_get_loc_supp_ptnr.key_value_2,R_get_loc_supp_ptnr.key_value_1);
       end if;
       fetch C_GET_LOC_INFO into R_get_loc_info;
       L_nfe_addressee_msg                    := "OBJ_FM_NFE_ENTITY_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
       L_nfe_addressee_msg.cod_pfj            := R_get_loc_info.cod_pfj;
       L_nfe_addressee_msg.razao_social       := R_get_loc_info.razao_social;
       L_nfe_addressee_msg.inscr_estadual     := R_get_loc_info.inscr_estadual;
       L_nfe_addressee_msg.inscr_municipal    := R_get_loc_info.inscr_municipal;
       L_nfe_addressee_msg.cnpj               := R_get_loc_info.cnpj;
       L_nfe_addressee_msg.cpf                := R_get_loc_info.cpf;
       L_nfe_addressee_msg.bairro             := R_get_loc_info.bairro;
       L_nfe_addressee_msg.estado             := R_get_loc_info.estado;
       L_nfe_addressee_msg.ibg_estado         := R_get_loc_info.ibg_estado;
       L_nfe_addressee_msg.cep                := R_get_loc_info.cep;
       L_nfe_addressee_msg.cod_pais           := R_get_loc_info.cod_pais;
       L_nfe_addressee_msg.cod_mun            := R_get_loc_info.cod_mun;
       L_nfe_addressee_msg.suframa            := R_get_loc_info.suframa;
       L_nfe_addressee_msg.fantasia           := R_get_loc_info.fantasia;
       L_nfe_addressee_msg.endereco           := R_get_loc_info.endereco;
       L_nfe_addressee_msg.num                := R_get_loc_info.num;
       L_nfe_addressee_msg.complemento        := R_get_loc_info.complemento;
       L_nfe_addressee_msg.fone               := R_get_loc_info.fone;
       L_nfe_addressee_msg.email              := R_get_loc_info.email;
       L_nfe_addressee_msg.cidade             := R_get_loc_info.cidade;
       L_nfe_addressee_msg.siscomex_id        := R_get_loc_info.siscomex_id;
       L_nfe_addressee_msg.cod_cnae           := R_get_loc_info.cod_cnae;
       L_nfe_addressee_msg.cod_reg_trib       := R_get_loc_info.cod_reg_trib;
       ---
       --L_nfe_hdr_msg.nfe_loc_tbl.extend;
       L_nfe_hdr_msg.nfe_addressee_rec   := L_nfe_addressee_msg;
       ---
       --- GET the IE_SUBS for the source entity using the destination state
       if R_get_loc_supp_ptnr.requisition_type in ('RMA','RPO') then
         OPEN C_GET_TRIB_SUBS_ST(R_get_loc_supp_ptnr.key_value_1,R_get_loc_supp_ptnr.key_value_2,R_get_loc_info.estado);
       else
         OPEN C_GET_TRIB_SUBS_ST(R_get_loc_supp_ptnr.location_id,R_get_loc_supp_ptnr.location_type,R_get_loc_info.estado);
       end if;
       FETCH C_GET_TRIB_SUBS_ST into L_ie_subs;
       L_nfe_issuer_msg.inscr_estadual_st  := L_ie_subs;
       CLOSE C_GET_TRIB_SUBS_ST;
       ---
       close C_GET_LOC_INFO;
     elsif R_get_loc_supp_ptnr.key_value_2 = 'E' then
     -- Populate Partner details
       open C_GET_PTNR_INFO(R_get_loc_supp_ptnr.key_value_1,R_get_loc_supp_ptnr.key_value_2);
       fetch C_GET_PTNR_INFO into R_get_ptnr_info;
       L_nfe_addressee_msg                  := "OBJ_FM_NFE_ENTITY_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
       L_nfe_addressee_msg.cod_pfj          := R_get_ptnr_info.cod_pfj;
       L_nfe_addressee_msg.razao_social     := R_get_ptnr_info.razao_social;
       L_nfe_addressee_msg.endereco         := R_get_ptnr_info.endereco;
       L_nfe_addressee_msg.bairro           := R_get_ptnr_info.bairro;
       L_nfe_addressee_msg.cod_pais         := R_get_ptnr_info.cod_pais;
       L_nfe_addressee_msg.cnpj             := R_get_ptnr_info.cnpj;
       L_nfe_addressee_msg.cpf              := R_get_ptnr_info.cpf;
       L_nfe_addressee_msg.inscr_estadual   := R_get_ptnr_info.inscr_estadual;
       L_nfe_addressee_msg.cod_mun          := R_get_ptnr_info.cod_mun;
       L_nfe_addressee_msg.num              := R_get_ptnr_info.num;
       L_nfe_addressee_msg.complemento      := R_get_ptnr_info.complemento;
       L_nfe_addressee_msg.estado           := R_get_ptnr_info.estado;
       L_nfe_addressee_msg.ibg_estado       := R_get_ptnr_info.ibg_estado;
       L_nfe_addressee_msg.cep              := R_get_ptnr_info.cep;
       L_nfe_addressee_msg.suframa          := R_get_ptnr_info.suframa;
       L_nfe_addressee_msg.cidade           := R_get_ptnr_info.cidade;
       L_nfe_addressee_msg.fone             := R_get_ptnr_info.fone;
       L_nfe_addressee_msg.email            := R_get_ptnr_info.email;
       L_nfe_addressee_msg.siscomex_id      := R_get_ptnr_info.siscomex_id;
       L_nfe_addressee_msg.cod_cnae         := R_get_ptnr_info.cod_cnae;
       L_nfe_addressee_msg.cod_reg_trib     := R_get_ptnr_info.cod_reg_trib;
       L_nfe_hdr_msg.nfe_addressee_rec      := L_nfe_addressee_msg;
       close C_GET_PTNR_INFO;
     end if;
   elsif R_get_loc_supp_ptnr.requisition_type ='RTV' then
     open C_GET_SUPP_INFO(R_get_loc_supp_ptnr.key_value_1);
     fetch C_GET_SUPP_INFO into R_get_supp_info;
     L_nfe_addressee_msg                   := "OBJ_FM_NFE_ENTITY_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
     L_nfe_addressee_msg.cod_pfj           := R_get_supp_info.cod_pfj;
     L_nfe_addressee_msg.razao_social      := R_get_supp_info.razao_social;
     L_nfe_addressee_msg.endereco          := R_get_supp_info.endereco;
     L_nfe_addressee_msg.bairro            := R_get_supp_info.bairro;
     L_nfe_addressee_msg.cod_pais          := R_get_supp_info.cod_pais;
     L_nfe_addressee_msg.cnpj              := R_get_supp_info.cnpj;
     L_nfe_addressee_msg.cpf               := R_get_supp_info.cpf;
     L_nfe_addressee_msg.inscr_estadual    := R_get_supp_info.inscr_estadual;
     L_nfe_addressee_msg.cod_mun           := R_get_supp_info.cod_mun;
     L_nfe_addressee_msg.num               := R_get_supp_info.num;
     L_nfe_addressee_msg.complemento       := R_get_supp_info.complemento;
     L_nfe_addressee_msg.estado            := R_get_supp_info.estado;
     L_nfe_addressee_msg.ibg_estado        := R_get_supp_info.ibg_estado;
     L_nfe_addressee_msg.cep               := R_get_supp_info.cep;
     L_nfe_addressee_msg.suframa           := R_get_supp_info.suframa;
     L_nfe_addressee_msg.cidade            := R_get_supp_info.cidade;
     L_nfe_addressee_msg.fone              := R_get_supp_info.fone;
     L_nfe_addressee_msg.email             := R_get_supp_info.email;
     L_nfe_addressee_msg.siscomex_id       := R_get_supp_info.siscomex_id;
     ---
     L_nfe_hdr_msg.nfe_addressee_rec  := L_nfe_addressee_msg;
     --- GET the IE_SUBS for the source entity using the destination state
     OPEN C_GET_TRIB_SUBS_ST(R_get_loc_supp_ptnr.location_id,R_get_loc_supp_ptnr.location_type,R_get_supp_info.estado);
     FETCH C_GET_TRIB_SUBS_ST into L_ie_subs;
     L_nfe_issuer_msg.inscr_estadual_st  := L_ie_subs;
     CLOSE C_GET_TRIB_SUBS_ST;
     ---
     close C_GET_SUPP_INFO;
   end if;
   ----Transportation details.
   open C_GET_PTNR_INFO(R_get_loc_supp_ptnr.partner_id,R_get_loc_supp_ptnr.partner_type);
   fetch C_GET_PTNR_INFO into R_get_ptnr_info;
   L_nfe_trans_msg                  := "OBJ_FM_NFE_ENTITY_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
   L_nfe_trans_msg.cod_pfj          := R_get_ptnr_info.cod_pfj;
   L_nfe_trans_msg.razao_social     := R_get_ptnr_info.razao_social;
   L_nfe_trans_msg.endereco         := R_get_ptnr_info.endereco;
   L_nfe_trans_msg.bairro           := R_get_ptnr_info.bairro;
   L_nfe_trans_msg.cod_pais         := R_get_ptnr_info.cod_pais;
   L_nfe_trans_msg.cnpj             := R_get_ptnr_info.cnpj;
   L_nfe_trans_msg.cpf              := R_get_ptnr_info.cpf;
   L_nfe_trans_msg.inscr_estadual   := R_get_ptnr_info.inscr_estadual;
   L_nfe_trans_msg.cod_mun          := R_get_ptnr_info.cod_mun;
   L_nfe_trans_msg.num              := R_get_ptnr_info.num;
   L_nfe_trans_msg.complemento      := R_get_ptnr_info.complemento;
   L_nfe_trans_msg.estado           := R_get_ptnr_info.estado;
   L_nfe_trans_msg.ibg_estado       := R_get_ptnr_info.ibg_estado;
   L_nfe_trans_msg.cep              := R_get_ptnr_info.cep;
   L_nfe_trans_msg.suframa          := R_get_ptnr_info.suframa;
   L_nfe_trans_msg.cidade           := R_get_ptnr_info.cidade;
   L_nfe_trans_msg.fone             := R_get_ptnr_info.fone;
   L_nfe_trans_msg.email            := R_get_ptnr_info.email;
   L_nfe_trans_msg.siscomex_id      := R_get_ptnr_info.siscomex_id;
   L_nfe_trans_msg.cod_cnae         := R_get_ptnr_info.cod_cnae;
   L_nfe_trans_msg.cod_reg_trib     := R_get_ptnr_info.cod_reg_trib;
   close C_GET_PTNR_INFO;
    ---
   L_nfe_hdr_msg.nfe_transporter_rec  := L_nfe_trans_msg;
   L_nfe_hdr_msg.nfe_detail_tbl   := "OBJ_FM_NFE_DOCDTL_TBL"();
   open C_GET_NFE_DETAIL;
   LOOP
      fetch C_GET_NFE_DETAIL into R_get_nfe_dtl;
      EXIT when C_GET_NFE_DETAIL%NOTFOUND;
      L_nfe_hdr_msg.nfe_detail_tbl.extend;
      L_count := L_nfe_hdr_msg.nfe_detail_tbl.last;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count)                         := "OBJ_FM_NFE_DOCDTL_REC"(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).line_number             := R_get_nfe_dtl.line_number;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cod_produto             := R_get_nfe_dtl.cod_produto;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).descricao               := R_get_nfe_dtl.descricao;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).quantidade              := R_get_nfe_dtl.quantidade;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).preco_unitario          := R_get_nfe_dtl.preco_unitario;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_total_item          := R_get_nfe_dtl.vlr_total_item;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cod_cfop                := R_get_nfe_dtl.cod_cfop;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cod_ncm                 := R_get_nfe_dtl.cod_ncm;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).tp_origem_item          := R_get_nfe_dtl.tp_origem_item;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cod_sit_trib_est        := R_get_nfe_dtl.cod_sit_trib_est;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_frete               := R_get_nfe_dtl.vlr_frete;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_seguro              := R_get_nfe_dtl.vlr_seguro;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_outras              := R_get_nfe_dtl.vlr_outras;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_desconto            := R_get_nfe_dtl.vlr_desconto;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).ind_tot                 := R_get_nfe_dtl.ind_tot;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).ex_ipi                  := R_get_nfe_dtl.ex_ipi;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).num_pedido              := R_get_nfe_dtl.num_pedido;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cod_medida              := R_get_nfe_dtl.cod_medida;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cst_pis                 := R_get_nfe_dtl.cst_pis;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cst_cofins              := R_get_nfe_dtl.cst_cofins;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cod_lst                 := R_get_nfe_dtl.cod_lst;
      ---fetch the header level discounts and add it to the header object.
       L_nfe_hdr_msg.vlr_desconto       := L_nfe_hdr_msg.vlr_desconto + (R_get_nfe_dtl.unit_header_disc * R_get_nfe_dtl.quantidade);
      ---fetch the Detail level taxes
      open C_GET_DTL_TAXES(R_get_nfe_dtl.fiscal_doc_line_id);
       LOOP
          fetch C_GET_DTL_TAXES into R_get_dtl_taxes;
          EXIT when C_GET_DTL_TAXES%NOTFOUND;
          if R_get_dtl_taxes.vat_code = 'COFINS' then
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_base_cofins   :=  R_get_dtl_taxes.tax_basis;
             if R_get_nfe_dtl.service_ind  = 'N' then
                L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_total_cofins  :=  R_get_dtl_taxes.total_value;
             else
                L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_total_cofins_serv  :=  R_get_dtl_taxes.total_value;
             end if;
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_aliq_cofins   :=  R_get_dtl_taxes.percentage_rate;
          elsif R_get_dtl_taxes.vat_code = 'PIS' then
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_base_pis   :=  R_get_dtl_taxes.tax_basis;
             if R_get_nfe_dtl.service_ind  = 'N' then
                L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_total_pis  :=  R_get_dtl_taxes.total_value;
             else
                L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_total_pis_serv  :=  R_get_dtl_taxes.total_value;
             end if;
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_aliq_pis   :=  R_get_dtl_taxes.percentage_rate;
          elsif R_get_dtl_taxes.vat_code = 'ISS' then
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_base_iss   :=  R_get_dtl_taxes.tax_basis;
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_total_iss  :=  R_get_dtl_taxes.total_value;
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_aliq_iss   :=  R_get_dtl_taxes.percentage_rate;
          elsif R_get_dtl_taxes.vat_code = 'ICMS' then
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_base_icms           :=  R_get_dtl_taxes.tax_basis;
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_total_icms          :=  R_get_dtl_taxes.total_value;
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_aliq_icms           :=  R_get_dtl_taxes.percentage_rate;
          elsif R_get_dtl_taxes.vat_code = 'ICMSST' then
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_base_icms_st         :=  R_get_dtl_taxes.tax_basis;
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_total_icms_st        :=  R_get_dtl_taxes.total_value;
             L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_aliq_icms_st         :=  R_get_dtl_taxes.percentage_rate;
          end if;
       END LOOP;
       close C_GET_DTL_TAXES;
      open C_GET_ITEM_NUMBER_TYPE(R_get_nfe_dtl.cod_produto);
      fetch C_GET_ITEM_NUMBER_TYPE into L_cod_ean;
      close C_GET_ITEM_NUMBER_TYPE;
      L_nfe_hdr_msg.nfe_detail_tbl(L_count).cod_ean                 := L_cod_ean;
      open C_GET_PERC_MVA (R_get_nfe_dtl.fiscal_doc_line_id,'ICMSST');
      fetch C_GET_PERC_MVA into L_perc_mva,L_tax_val_mva,L_tax_perc_redu_base;
      if C_GET_PERC_MVA%FOUND then
        L_nfe_hdr_msg.nfe_detail_tbl(L_count).perc_mva                := L_perc_mva;
        L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_base_redu_icms_st   :=  L_tax_perc_redu_base;
        if L_perc_mva is NULL or L_perc_mva = 0 then
            if L_tax_val_mva is NOT NULL or L_tax_val_mva <> 0 then
               L_nfe_hdr_msg.nfe_detail_tbl(L_count).mod_bc_icms_st     := 5;
            end if;
        else
           L_nfe_hdr_msg.nfe_detail_tbl(L_count).mod_bc_icms     := 4;
        end if;
      end if;
      close C_GET_PERC_MVA;
      ---
      open C_GET_PERC_MVA (R_get_nfe_dtl.fiscal_doc_line_id,'ICMS');
      fetch C_GET_PERC_MVA into L_perc_mva,L_tax_val_mva,L_tax_perc_redu_base;
      if C_GET_PERC_MVA%FOUND then
--        L_nfe_hdr_msg.nfe_detail_tbl(L_count).perc_mva                := L_perc_mva;
        L_nfe_hdr_msg.nfe_detail_tbl(L_count).vlr_base_redu_icms    :=  L_tax_perc_redu_base;
        if L_perc_mva is NULL or L_perc_mva = 0 then
            if L_tax_val_mva is NULL or L_tax_val_mva = 0 then
               L_nfe_hdr_msg.nfe_detail_tbl(L_count).mod_bc_icms     := 3;
            else
               L_nfe_hdr_msg.nfe_detail_tbl(L_count).mod_bc_icms     := 1;
            end if;
        else
           L_nfe_hdr_msg.nfe_detail_tbl(L_count).mod_bc_icms     := 0;
        end if;
      end if;
      close C_GET_PERC_MVA;
      ---
       if R_get_nfe_hdr.tipo_pedido in ('RTV','RPO') then
         open C_GET_NFE_REFER_INFO(R_get_nfe_dtl.fiscal_doc_id_ref);
         fetch C_GET_NFE_REFER_INFO into R_get_nfe_refer_info;
          L_nfe_hdr_msg.nfe_detail_tbl(L_count).num_docfis_refer        := R_get_nfe_refer_info.num_docfis_refer;
          L_nfe_hdr_msg.nfe_detail_tbl(L_count).ibg_estado_refer        := R_get_nfe_refer_info.ibg_estado_refer;
          L_nfe_hdr_msg.nfe_detail_tbl(L_count).data_emissao_refer      := R_get_nfe_refer_info.data_emissao_refer;
          L_nfe_hdr_msg.nfe_detail_tbl(L_count).cnpj_refer              := R_get_nfe_refer_info.cnpj_refer;
          L_nfe_hdr_msg.nfe_detail_tbl(L_count).cpf_refer               := R_get_nfe_refer_info.cpf_refer;
          L_nfe_hdr_msg.nfe_detail_tbl(L_count).nfe_doc_type_refer      := R_get_nfe_refer_info.nfe_doc_type_refer;
          L_nfe_hdr_msg.nfe_detail_tbl(L_count).serie_docfis_refer      := R_get_nfe_refer_info.serie_docfis_refer;
          if R_get_nfe_hdr.tipo_pedido = 'RTV' then
            L_nfe_hdr_msg.nfe_detail_tbl(L_count).chave_acesso_nfe_refer  := R_get_nfe_refer_info.nfe_accesskey;
          else
            L_nfe_hdr_msg.nfe_detail_tbl(L_count).chave_rural_prod_refer  := R_get_nfe_refer_info.num_docfis_refer;
            L_nfe_hdr_msg.nfe_detail_tbl(L_count).inscr_estadual_refer    := R_get_nfe_refer_info.inscr_estadual_refer;
          end if;
         close C_GET_NFE_REFER_INFO;
       end if;
      ---
   END LOOP;
   close C_GET_NFE_DETAIL;
   O_message :=  L_nfe_hdr_msg;
   O_status_code := 1;
--
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_LOC_INFO%ISOPEN then
         close C_GET_LOC_INFO;
      end if;
      if C_GET_NFE_HEADER%ISOPEN then
         close C_GET_NFE_HEADER;
      end if;
      if C_GET_PTNR_INFO%ISOPEN then
         close C_GET_PTNR_INFO;
      end if;
      if C_GET_SUPP_INFO%ISOPEN then
         close C_GET_SUPP_INFO;
      end if;
      if C_GET_NFE_DETAIL%ISOPEN then
         close C_GET_NFE_DETAIL;
      end if;
      if C_GET_LOC_SUPP_PTNR%ISOPEN then
         close C_GET_LOC_SUPP_PTNR;
      end if;
      if C_GET_HDR_TAXES%ISOPEN then
         close C_GET_HDR_TAXES;
      end if;
      if C_GET_DTL_TAXES%ISOPEN then
         close C_GET_DTL_TAXES;
      end if;
      if C_GET_LEGAL_MSG%ISOPEN then
         close C_GET_LEGAL_MSG;
      end if;
      if C_GET_ITEM_NUMBER_TYPE%ISOPEN then
         close C_GET_ITEM_NUMBER_TYPE;
      end if;
      if C_GET_PAYMENT_IND%ISOPEN then
         close C_GET_PAYMENT_IND;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_NFE_INFO;
--------------------------------------------------------------------------------
PROCEDURE GET_NFE_INFO(O_status_code    IN OUT  NUMBER,
                       O_error_message  IN OUT  VARCHAR2,
                       O_message        IN OUT  "OBJ_FM_NFE_DOCHDR_REC",
                       I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
is
---
L_program            VARCHAR2(61)  := 'FM_NFE_PUB_SQL.GET_NFE_INFO';
BEGIN
    ---
    if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'fiscal_doc_id',
                                             L_program,
                                             NULL);
      O_status_code := 0;
      return;
    end if;
   ---
    if GET_NFE_INFO(O_status_code,
                    O_error_message,
                    O_message,
                    I_fiscal_doc_id) = FALSE then
        O_status_code := 0;
    end if;
    ---
EXCEPTION
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
        O_status_code := 0;
END GET_NFE_INFO;
--------------------------------------------------------------------------------
END FM_NFE_PUB_SQL;
/