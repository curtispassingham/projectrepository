CREATE OR REPLACE PACKAGE BODY FM_SYSTEM_OPTIONS_SQL is
-----------------------------------------------------------------
FUNCTION GET_VARIABLE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_description       IN OUT FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE,
                      O_number_value      IN OUT FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE,
                      O_string_value      IN OUT FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE,
                      O_date_value        IN OUT FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE,
                      I_variable_type     IN     FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE,
                      I_variable          IN     FM_SYSTEM_OPTIONS.VARIABLE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE';
   ---
   cursor C_FM_SYSTEM_OPTIONS is
      select description,
             number_value,
             string_value,
             date_value
        from fm_system_options
       where variable_type = I_variable_type
         and variable = I_variable;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_SYSTEM_OPTIONS','FM_SYSTEM_OPTIONS','Variable_type: '||I_variable_type||' Variable: '||I_variable);
   open C_FM_SYSTEM_OPTIONS;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_SYSTEM_OPTIONS','FM_SYSTEM_OPTIONS','Variable_type: '||I_variable_type||' Variable: '||I_variable);
   fetch C_FM_SYSTEM_OPTIONS into O_description, O_number_value, O_string_value, O_date_value;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_SYSTEM_OPTIONS','FM_SYSTEM_OPTIONS','Variable_type: '||I_variable_type||' Variable: '||I_variable);
   close C_FM_SYSTEM_OPTIONS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_VARIABLE;
-----------------------------------------------------------------
FUNCTION GET_DESC (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_description     IN OUT FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE,
                   I_variable_type   IN     FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE,
                   I_variable        IN     FM_SYSTEM_OPTIONS.VARIABLE%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80)   := 'FM_SYSTEM_OPTIONS_SQL.GET_DESC';
   L_key       VARCHAR2(500);

   cursor C_OPTION is
      select description
        from fm_system_options
       where variable_type = nvl(I_variable_type,variable_type)
         and variable      = I_variable;

BEGIN
   L_key := 'variable_type = '||I_variable_type||
            ',variable = '||I_variable;
   SQL_LIB.SET_MARK('OPEN','C_OPTION','FM_SYSTEM_OPTIONS',L_key);
   open C_OPTION;
   SQL_LIB.SET_MARK('FETCH','C_OPTION','FM_SYSTEM_OPTIONS',L_key);
   fetch C_OPTION into O_description;
   if C_OPTION%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_OPTION','FM_SYSTEM_OPTIONS',L_key);
      close C_OPTION;
      O_error_message := SQL_LIB.create_msg('ORFM_INV_VARIABLE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE','C_OPTION','FM_SYSTEM_OPTIONS',L_key);
      close C_OPTION;
         if LANGUAGE_SQL.TRANSLATE( O_description,
                                    O_description,
                                    O_error_message) = FALSE then
            return FALSE;
         end if;
      return TRUE;
   end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_DESC;
-----------------------------------------------------------------
FUNCTION EXISTS (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists            IN OUT BOOLEAN,
                 I_variable          IN FM_SYSTEM_OPTIONS.VARIABLE%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_SYSTEM_OPTIONS_SQL.EXISTS';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(50);

   cursor C_SYSTEM is
      select '1'
        from fm_system_options
       where variable      = I_variable;

BEGIN
   L_key := 'variable = '||I_variable;
   SQL_LIB.SET_MARK('OPEN','C_SYSTEM','FM_SYSTEM_OPTIONS',L_key);
   open C_SYSTEM;
   SQL_LIB.SET_MARK('FETCH','C_SYSTEM','FM_SYSTEM_OPTIONS',L_key);
   fetch C_SYSTEM into L_dummy;
   if C_SYSTEM%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_SYSTEM','FM_SYSTEM_OPTIONS',L_key);
   close C_SYSTEM;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END EXISTS;
-----------------------------------------------------------------
FUNCTION P_SET_WHERE_CLAUSE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_where_clause       IN OUT VARCHAR2,
                            I_variable_type      IN     VARCHAR2,
                            I_variable           IN     VARCHAR2)
return BOOLEAN is

      L_program   VARCHAR2(80)   := 'ORFM_SYSTEM_OPTIONS_SQL.P_SET_WHERE_CLAUSE';
      L_text      VARCHAR2(2000) := NULL;
   BEGIN
      if I_variable_type is NOT NULL and
         I_variable      is NOT NULL then
         L_text := L_text || ' variable_type = '''||I_variable_type||''' and ';
         L_text := L_text || ' variable = '''||I_variable||''' and ';
      elsif I_variable_type is NOT NULL and
            I_variable      is NULL then
         L_text := L_text || ' variable_type = '''||I_variable_type||''' and ';
      elsif I_variable_type is NULL and
            I_variable      is NOT NULL then
         L_text := L_text || ' variable = '''||I_variable||''' and ';
      end if;

      if L_text is NOT NULL then
         L_text := SUBSTR(L_text,1,LENGTH(L_text)-5);
      end if;

      O_where_clause := L_text;

      return TRUE;

     EXCEPTION
        when OTHERS then
           O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                                 SQLERRM,
                                                 L_program,
                                                 NULL);
           return FALSE;
  END P_SET_WHERE_CLAUSE;
-----------------------------------------------------------------
FUNCTION LOV_VARIABLE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_where_clause   IN     VARCHAR2)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'ORFM_SYSTEM_OPTIONS_SQL.LOV_VARIABLE';
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'Select description,variable from fm_system_options where variable_type like '''||upper(I_where_clause)||'%'' order by description';
   else
      O_query := 'Select description,variable from fm_system_options order by description';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOV_VARIABLE;
-----------------------------------------------------------------
FUNCTION RESOLUTION_RULE_EXISTS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT BOOLEAN,
                                I_variable_value   IN     FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_SYSTEM_OPTIONS_SQL.RESOLUTION_RULE_EXISTS';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(50);

   cursor C_RESOLUTION_RULE_EXISTS is
      select 'X'
        from fm_system_options
       where I_variable_value in ('NF','SYS','RECONCILE');
BEGIN
   L_key := 'variable value = '||I_variable_value;
   SQL_LIB.SET_MARK('OPEN', 'C_RESOLUTION_RULE_EXISTS', 'FM_SYSTEM_OPTIONS', L_key);
   open C_RESOLUTION_RULE_EXISTS;
   --
   SQL_LIB.SET_MARK('FETCH', 'C_RESOLUTION_RULE_EXISTS', 'FM_SYSTEM_OPTIONS', L_key);
   fetch C_RESOLUTION_RULE_EXISTS into L_dummy;
   --
   if C_RESOLUTION_RULE_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   --
   SQL_LIB.SET_MARK('CLOSE', 'C_RESOLUTION_RULE_EXISTS', 'FM_SYSTEM_OPTIONS', L_key);
   close C_RESOLUTION_RULE_EXISTS;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      ---
      if C_RESOLUTION_RULE_EXISTS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_RESOLUTION_RULE_EXISTS', 'FM_SYSTEM_OPTIONS', L_key);
         close C_RESOLUTION_RULE_EXISTS;
      end if;
      ---
      return FALSE;
END RESOLUTION_RULE_EXISTS;
-----------------------------------------------------------------
END FM_SYSTEM_OPTIONS_SQL;
/
