CREATE OR REPLACE PACKAGE FM_FISCAL_DOC_TAX_HEAD_SQL as
---------------------------------------------------------------------------------
-- CREATE DATE - 03-05-2007
-- CREATE USER ? Sigma.EPP
-- PROJECT / FRD - 10481 / 10481_ORFMI_FRD_001
-- DESCRIPTION ? Package associated to table FM_FISCAL_DOC_TAX_HEAD.
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Function Name: EXISTS
-- Purpose:       This function checks if already exists.
----------------------------------------------------------------------------------
FUNCTION EXISTS(O_error_message  IN OUT VARCHAR2,
                O_exists         IN OUT BOOLEAN,
                I_vat_code       IN     FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE,
                I_fiscal_doc_id  IN     FM_FISCAL_DOC_TAX_HEAD.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: DELETE_TAX_HEAD
-- Purpose:       This function deletes all taxes heads from an especific item of NF.
----------------------------------------------------------------------------------
FUNCTION DELETE_TAX_HEAD(O_error_message       IN OUT VARCHAR2,
                         I_fiscal_doc_id       IN     FM_FISCAL_DOC_TAX_HEAD.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------

END FM_FISCAL_DOC_TAX_HEAD_SQL;
/
 