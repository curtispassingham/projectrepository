CREATE OR REPLACE PACKAGE FM_SECONDARY_ASNOUT AS
---
L_FAMILY      CONSTANT VARCHAR2(25)   := 'asnout';
---
-------------------------------------------------------------------------------
-- This function adds the schedule message to the FM_RIB_RECEIVING_MFQUEUE table
-- in 'U'npublished status.
-------------------------------------------------------------------------------
FUNCTION ADDTOQ (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_recv_no         IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                 I_po_number       IN       FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE,                 
                 I_msg_type        IN       VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- This procedure is called from the RIB to get the next unpublished schedule number
-- on FM_RIB_RECEIVING_MFQUEUE table for publishing. Based on the schedule number on
-- FM_RIB_RECEIVING_MFQUEUE, it calls the PROCESS_QUEUE_RECORD procedure.
-------------------------------------------------------------------------------
PROCEDURE GETNXT (O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_message_type    IN OUT   VARCHAR2,
                  O_message         IN OUT   RIB_OBJECT,
                  O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                  O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                  I_num_threads     IN       NUMBER DEFAULT 1,
                  I_thread_val      IN       NUMBER DEFAULT 1);

-------------------------------------------------------------------------------
-- This function re-processes the records in the FM_RIB_RECEIVING_MFQUEUE table
-- with pub_status equal to 'H'.
-------------------------------------------------------------------------------
PROCEDURE PUB_RETRY (O_status_code     IN OUT   VARCHAR2,
                     O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_message_type    IN OUT   VARCHAR2,
                     O_message         IN OUT   RIB_OBJECT,
                     O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                     O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                     I_ref_object      IN       RIB_OBJECT);

-------------------------------------------------------------------------------

END FM_SECONDARY_ASNOUT;
/
