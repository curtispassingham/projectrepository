create or replace PACKAGE FM_FISCAL_DETAIL_VAL_SQL as
----------------------------------------------------------------------------------
-- Function Name: GET_QUERY_TYPE_TAXES
-- Purpose:       This function gets the query for type of taxes to NF.
----------------------------------------------------------------------------------
FUNCTION GET_QUERY_TYPE_TAXES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_query             IN OUT VARCHAR2)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_ITEM_CLASSIFICATION
-- Purpose:       This function gets the item claissification.
----------------------------------------------------------------------------------
FUNCTION GET_ITEM_CLASSIFICATION(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_classification      IN OUT V_FISCAL_CLASSIFICATION.CLASSIFICATION_ID%TYPE,
                                 O_classification_desc IN OUT V_FISCAL_CLASSIFICATION.CLASSIFICATION_DESC%TYPE,
                                 I_item                IN     V_BR_ITEM_FISCAL_ATTRIB.ITEM%TYPE,
                                 I_key_value_1         IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                 I_location            IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                 I_loc_type            IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                 I_issue_date          IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_ORDER_NO
-- Purpose:       This function returns the query for record_grup REC_ORDER_NO.
----------------------------------------------------------------------------------
FUNCTION LOV_ORDER_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                      I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                      I_item           IN     ORDLOC.ITEM%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_ITEM_ORDER
-- Purpose:       This function returns the query for record_grup REC_ITEM.
----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_ORDER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_query          IN OUT VARCHAR2,
                        I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                        I_order_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                        I_location       IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                        I_loc_type       IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                        I_unexpected_Item IN    FM_FISCAL_DOC_DETAIL.UNEXPECTED_ITEM%TYPE,
                        I_Module         IN     FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                        I_Key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_ITEM
-- Purpose:       This function returns the query for record_grup REC_ITEM.
----------------------------------------------------------------------------------
FUNCTION LOV_ITEM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_query          IN OUT VARCHAR2,
                  I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for where clause of detail block.
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where_clause       IN OUT VARCHAR2,
                          I_location_id        IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                          I_location_type      IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                          I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_requisition_no     IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                          I_item               IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_ITEM_ATTRIBUTES
-- Purpose:       This function gets the item attributes.
----------------------------------------------------------------------------------
FUNCTION GET_ITEM_ATTRIBUTES(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_classification      IN OUT V_BR_ITEM_FISCAL_ATTRIB.CLASSIFICATION_ID%TYPE,
                             O_origin_code         IN OUT V_BR_ITEM_FISCAL_ATTRIB.ORIGIN_CODE%TYPE,
                             O_service_ind         IN OUT V_BR_ITEM_FISCAL_ATTRIB.SERVICE_IND%TYPE,
                             O_embedded_item       IN OUT VARCHAR2,
                             I_item                IN     V_BR_ITEM_FISCAL_ATTRIB.ITEM%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_TSF_NO
-- Purpose:       This function returns the query for record_grup REC_TSF_NO.
----------------------------------------------------------------------------------
FUNCTION LOV_TSF_NO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_query           IN OUT VARCHAR2,
                    I_location_id     IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type   IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                    I_doc_fiscal_type IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                    I_key_value_1     IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_EDI_NO
-- Purpose:       This function returns the query for record_grup REC_RTV_NO.
----------------------------------------------------------------------------------
---FUNCTION LOV_RTV_NO(O_error_message  IN OUT VARCHAR2,
---                    O_query          IN OUT VARCHAR2,
---                    I_location_id    IN     FM_EDI_DOC_HEADER.LOCATION_ID%TYPE,
---                    I_location_type  IN     FM_EDI_DOC_HEADER.LOCATION_TYPE%TYPE)
FUNCTION LOV_EDI_NO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_query           IN OUT VARCHAR2,
                    I_location_id     IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type   IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                    I_doc_fiscal_type IN     VARCHAR2,
                    I_fiscal_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--- 004 - End
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEM_ORDER
-- Purpose:       This function checks if the item is valid on order.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_ORDER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists         IN OUT BOOLEAN,
                             O_unique         IN OUT BOOLEAN,                             
                             I_order_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                             I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                             I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                             I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_PERC_VALUE
-- Purpose:       This function returns the query for record_group REC_DISCOUNT_TYPE.
----------------------------------------------------------------------------------
FUNCTION LOV_PERC_VALUE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_query          IN OUT VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_FISCAL_DOC_NO_REF_PO
-- Purpose:       This function returns the query for record_group REC_FISCAL_DOC_NO_REF_PO.
----------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO_REF_PO(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_query              IN OUT VARCHAR2,
                                  I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                  I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                  I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                  I_fiscal_doc_line_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                  I_complementary_ind  IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                                  I_main_item          IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_FISCAL_DOC_NO_REF_PO
-- Purpose:       This function validates the doc number reference.
----------------------------------------------------------------------------------

 -- code commented for code drop 1
/*
FUNCTION VALIDATE_FISCAL_DOC_NO_REF_PO(O_error_message      IN OUT VARCHAR2,
                                       O_exists             IN OUT BOOLEAN,
                                       O_fiscal_doc_id_ref  IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                       I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                       I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                       I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                       I_main_item          IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                                       I_complementary_ind  IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEM_REF
-- Purpose:       This function validates the item reference.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_REF(O_error_message          IN OUT VARCHAR2,
                           O_exists                 IN OUT BOOLEAN,
                           O_fiscal_doc_line_id_ref IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                           I_fiscal_doc_id          IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                           I_item                   IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           I_location               IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                           I_loc_type               IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                           I_main_item              IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           I_complementary_ind      IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_ITEM_REF
-- Purpose:       This function returns the query for record_grup REC_ITEM_REF.
----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_REF(O_error_message      IN OUT VARCHAR2,
                      O_query              IN OUT VARCHAR2,
                      I_fiscal_doc_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                      I_item               IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                      I_item_desc          IN     ITEM_MASTER.ITEM_DESC%TYPE,
                      I_fiscal_doc_line_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                      I_complementary_ind  IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE,
                      I_main_item          IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE)
   return BOOLEAN;*/
----------------------------------------------------------------------------------
-- Function Name: LOV_ITEM_TSF
-- Purpose:       This function returns the query for record_grup REC_ITEM_TSF.
----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                      I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_TSF
-- Purpose:       This function validates the TSF.
----------------------------------------------------------------------------------
--- 004 - Begin
---FUNCTION VALIDATE_TSF(O_error_message  IN OUT VARCHAR2,
---                      O_exists         IN OUT BOOLEAN,
---                      I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
---                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
---                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
---   return BOOLEAN;
--- 004 - End
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEM_TSF
-- Purpose:       This function validates the item of TSF.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           O_unique         IN OUT BOOLEAN,
                           I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                           I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_FISCAL_DOC_NO_REF_TSF
-- Purpose:       This function returns the query for record_group REC_FISCAL_DOC_NO_REF_TSF.
----------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO_REF(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_query              IN OUT VARCHAR2,
                               I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                               I_location           IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                               I_loc_type           IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                               I_item               IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                               I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                               I_supplier           IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_FISCAL_DOC_NO_REF
-- Purpose:       This function validates the doc number reference.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_FISCAL_DOC_NO_REF(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists                  IN OUT BOOLEAN,
                                    O_fiscal_doc_line_id_ref  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                    O_fiscal_doc_id_ref       IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                    I_fiscal_doc_no           IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                    I_location                IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                    I_loc_type                IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                    I_supplier                IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                    I_item                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                                    I_fiscal_doc_line_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                    I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_FISCAL_DOC_NO_REF
-- Purpose:       Gets the last document using the same item.
----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_NO_REF(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists                  IN OUT BOOLEAN,
                               O_fiscal_doc_line_id_ref  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                               O_fiscal_doc_id_ref       IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                               I_location                IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                               I_loc_type                IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                               I_item                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                               I_fiscal_doc_line_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_FISCAL_DOC_NO_REF_RMA
-- Purpose:       Gets the last document using the same item for RMA.
----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_NO_REF_RMA(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists                  IN OUT BOOLEAN,
                               O_fiscal_doc_line_id_ref  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                               O_fiscal_doc_id_ref       IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                               I_location                IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                               I_loc_type                IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                               I_item                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                               I_fiscal_doc_line_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists          IN OUT BOOLEAN,
                      I_requisition_no IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id     IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                      I_location_type   IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                      I_doc_fiscal_type IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                      I_key_value_1     IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_RTV
-- Purpose:       This function validates the RTV.
----------------------------------------------------------------------------------
--- 004 - Begin
FUNCTION VALIDATE_EDI(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists          IN OUT BOOLEAN,
                      I_edi_order_no    IN     FM_EDI_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id     IN     FM_EDI_DOC_DETAIL.LOCATION_ID%TYPE,
                      I_location_type   IN     FM_EDI_DOC_DETAIL.LOCATION_TYPE%TYPE,
                      I_doc_fiscal_type IN     VARCHAR2,
                      I_edi_doc_id      IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
---FUNCTION VALIDATE_RTV(O_error_message  IN OUT VARCHAR2,
---                      O_exists         IN OUT BOOLEAN,
---                      I_rtv_order_no   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
---                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
---                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
   return BOOLEAN;
--- 004 - End
----------------------------------------------------------------------------------
-- Function Name: LOV_ITEM_RTV
-- Purpose:       This function returns the query for record_grup REC_ITEM_RTV.
----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_RTV(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                      I_rtv_order_no   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEM_TSF
-- Purpose:       This function validates the item of TSF.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_RTV(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           I_rtv_order_no   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                           I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                           I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_STOCK
-- Purpose:       This function validates the STOCK.
----------------------------------------------------------------------------------
--- 004 - Begin
---FUNCTION VALIDATE_STOCK(O_error_message  IN OUT VARCHAR2,
---                        O_exists         IN OUT BOOLEAN,
---                        I_stock_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
---                        I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
---                        I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
---   return BOOLEAN;
--- 004 - End
----------------------------------------------------------------------------------
-- Function Name: LOV_ITEM_STOCK
-- Purpose:       This function returns the query for record_grup REC_ITEM_STOCK.
----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_STOCK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_query          IN OUT VARCHAR2,
                        I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                        I_stock_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                        I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                        I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEM_STOCK
-- Purpose:       This function validates the item of STOCK.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_STOCK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists         IN OUT BOOLEAN,
                             I_stock_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                             I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                             I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                             I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_STOCK_NO
-- Purpose:       This function returns the query for record_grup REC_STOCK_NO.
----------------------------------------------------------------------------------
--- 004 - Begin
---FUNCTION LOV_STOCK_NO(O_error_message  IN OUT VARCHAR2,
---                      O_query          IN OUT VARCHAR2,
---                      I_location_id    IN     FM_EDI_DOC_HEADER.LOCATION_ID%TYPE,
---                      I_location_type  IN     FM_EDI_DOC_HEADER.LOCATION_TYPE%TYPE)
---   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_RMA_NO
-- Purpose:       This function returns the query for record_grup REC_RMA_NO.
----------------------------------------------------------------------------------
FUNCTION LOV_RMA_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_query          IN OUT VARCHAR2,
                    I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_RMA
-- Purpose:       This function validates the RMA.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_RMA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists         IN OUT BOOLEAN,
                      I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_ITEM_RMA
-- Purpose:       This function returns the query for record_grup REC_ITEM_RMA.
----------------------------------------------------------------------------------
FUNCTION LOV_ITEM_RMA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2,
                      I_item_desc      IN     ITEM_MASTER.ITEM_DESC%TYPE,
                      I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEM_RMA
-- Purpose:       This function checks if the item is valid on RMA.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_RMA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                           I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_FISCAL_DOC_NO_COMP_REF
-- Purpose:       This function returns the query for record_group REC_FISCAL_DOC_NO_REF_PO.
----------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO_COMP_REF(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_query               IN OUT VARCHAR2,
                                    I_compl_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE,
                                    I_fiscal_doc_no       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                    I_location            IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                    I_loc_type            IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VAL_FISCAL_DOC_NO_COMP_REF
-- Purpose:       This function validates the doc number reference.
----------------------------------------------------------------------------------
FUNCTION VAL_FISCAL_DOC_NO_COMP_REF(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists              IN OUT BOOLEAN,
                                    O_fiscal_doc_id_ref   IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                    I_compl_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE,
                                    I_fiscal_doc_no       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                    I_location            IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                    I_loc_type            IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_DELIVERY_SUPPLIER
-- Purpose:       This function will return the delivery supplier of PO
----------------------------------------------------------------------------------
FUNCTION GET_DELIVERY_SUPPLIER(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_delivery_supplier IN OUT ORDHEAD.DELIVERY_SUPPLIER%TYPE,
                               I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: INSERT_NON_EXIST_DOCS
-- Purpose:       This function insert non exixtent documents on fm_printer_queue table.
----------------------------------------------------------------------------------
FUNCTION INSERT_NON_EXIST_DOCS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_ITEM_ORIGIN_CST
-- Purpose:       This function will return the origin CST
---- CST will be moved to NF line level so this Function call is no longer required
----------------------------------------------------------------------------------
/*FUNCTION GET_ITEM_ORIGIN_CST(O_error_message    IN OUT VARCHAR2,
                             O_origin_code      IN OUT FM_VAT_CLASSIFICATION.ORIGIN_CODE%TYPE,
                             O_cst              IN OUT FM_VAT_CLASSIFICATION.CST%TYPE,
                             I_fiscal_doc_id    IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                             I_classification   IN     FM_VAT_CLASSIFICATION.CLASSIFICATION_ID%TYPE,
                             I_utilization_cfop IN     FM_FISCAL_DOC_DETAIL.UTILIZATION_CFOP%TYPE)
   return BOOLEAN;
   */
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ORDLOC
-- Purpose:       This function validates the Order Item Location.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDLOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists         IN OUT BOOLEAN,
                         I_order_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                         I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                         I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                         I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_UNIQUE_DETAIL
-- Purpose:       This function returns item detail if it is unique on document.
----------------------------------------------------------------------------------
FUNCTION GET_UNIQUE_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_fiscal_doc_line_id IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                           O_item               IN OUT FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           I_fiscal_doc_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--- 002 - Begin
----------------------------------------------------------------------------------
-- Function Name: GET_QTY_ITEM_ORDER
-- Purpose:       Fun??o  para buscar quantidade e custo unit?rio do item do
--                documento fiscal (ORDER).
----------------------------------------------------------------------------------
FUNCTION GET_QTY_ITEM_ORDER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_quantity       IN OUT NUMBER,
                            O_unit_cost      IN OUT NUMBER,
                            O_pack_ind       IN OUT FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                            O_pack_no        IN OUT FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,                            
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_order_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                            I_location       IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                            I_loc_type       IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_QTY_ITEM_RMA
-- Purpose:       Fun??o  para buscar quantidade e custo unit?rio do item do
--                documento fiscal (RMA).
----------------------------------------------------------------------------------
FUNCTION GET_QTY_ITEM_RMA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_quantity       IN OUT NUMBER,
                          O_unit_cost      IN OUT NUMBER,
                          I_item           IN     ITEM_MASTER.ITEM%TYPE,
                          I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_QTY_EDI
-- Purpose:       Fun  para buscar quantidade e custo unit?rio do item do
--                documento fiscal (RTV,STOCK,TSF).
----------------------------------------------------------------------------------
FUNCTION GET_QTY_EDI(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_quantity        IN OUT NUMBER,
                     O_unit_cost       IN OUT NUMBER,
                     I_item            IN     ITEM_MASTER.ITEM%TYPE,
                     I_rtv_stock_tsf   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                     I_doc_fiscal_type IN     VARCHAR2,
                     I_location_id     IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                     I_location_type   IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                     I_pack_ind        IN     FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE DEFAULT NULL,
                     I_pack_no         IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE DEFAULT NULL)
   return BOOLEAN;
----------------------------------------------------------------------------------
--- 002 - End
----------------------------------------------------------------------------------
-- Function Name: GET_FISCAL_DOC_NO_REF_REP
-- Purpose:       Gets the last document using the same item.
----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_NO_REF_REP(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists                  IN OUT BOOLEAN,
                                   O_fiscal_doc_line_id_ref  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                   O_fiscal_doc_id_ref       IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                   I_location                IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                   I_loc_type                IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                                   I_item                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                                   I_fiscal_doc_line_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
FUNCTION EXPLODE_PACK(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN ;
----------------------------------------------------------------------------------
FUNCTION EXPLODE_ALL_PACK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN ;
----------------------------------------------------------------------------------
FUNCTION GET_COMP_COST_QTY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                           I_comp_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                           O_Comp_qty            OUT    FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                           O_Comp_unit_cost      OUT    FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE)
   return BOOLEAN ;
----------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_EXPLODE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN ;
----------------------------------------------------------------------------------
-- Function Name: CHECK_PACK_ITEM
-- Purpose:       This function checks the if there any pack item on  NF.
----------------------------------------------------------------------------------
FUNCTION CHECK_PACK_ITEM (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists         OUT    BOOLEAN,
                          I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_TRIANGULATION_IND
-- Purpose:       This function gets the Triangulation Ind.
----------------------------------------------------------------------------------
FUNCTION GET_TRIANGULATION_IND (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_triangulation_ind OUT   VARCHAR2,
                                I_order_no          IN   ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_PACK_INFO
-- Purpose:       This function finds the pack information from fiscal detail table
----------------------------------------------------------------------------------
FUNCTION GET_PACK_INFO (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_pack_no        IN OUT FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                        O_pack_ind       IN OUT FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE,
                        I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                        I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                        I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: CHECK_PAYMENT_DATE
-- Purpose:       This function check the Payment date from fiscal payment table
----------------------------------------------------------------------------------
FUNCTION CHECK_PAYMENT_DATE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         OUT    BOOLEAN,
                            I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_COMP_REF_DOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists        IN OUT BOOLEAN,
                                    I_fiscal_doc_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
END FM_FISCAL_DETAIL_VAL_SQL;
/