CREATE OR REPLACE PACKAGE BODY FISCAL_CLASSIFICATION_SQL AS
-----------------------------------------------------------------------------------------------
PROCEDURE P_GET_FISCAL_CLASSIFICATION(O_FISCLASS_TABLE               IN OUT   FISCAL_CLASS_TBL,
                                      I_country_id                   IN       L10N_BR_NCM_CODES.COUNTRY_ID%TYPE,
                                      I_fiscal_classification_id     IN       L10N_BR_NCM_CODES.NCM_CODE%TYPE,
                                      I_fiscal_classification_type   IN       CODE_DETAIL.CODE%TYPE)
IS
   L_program VARCHAR2(100) := 'FISCAL_CLASSIFICATION_SQL.P_GET_FISCAL_CLASSIFICATION';

   cursor C_GET_FISC_CLASSIFICATION_PROD is
   select country_id,
          ncm_code,
          ncm_desc,
          'I',
          NULL,
          'TRUE'
     from l10n_br_ncm_codes
    where country_id = NVL(I_country_id,country_id)
      and ncm_code   = NVL(I_fiscal_classification_id,ncm_code);

   cursor C_GET_FISC_CLASSIFICATION_SERV is
   select 'BR',
          federal_service_code,
          federal_service_desc,
          'S',
          NULL,
          'TRUE'
     from l10n_br_federal_svc_codes
    where 'BR'                 = NVL(I_country_id,'BR')
      and federal_service_code = NVL(I_fiscal_classification_id,federal_service_code);

   cursor C_GET_FISC_CLASSIFICATION_ALL is
   select country_id,
          ncm_code,
          ncm_desc,
          'I',
          NULL,
          'TRUE'
     from l10n_br_ncm_codes
    where country_id = NVL(I_country_id,country_id)
      and ncm_code   = NVL(I_fiscal_classification_id,ncm_code)
   union
   select 'BR',
          federal_service_code,
          federal_service_desc,
          'S',
          NULL,
          'TRUE'
     from l10n_br_federal_svc_codes
    where 'BR'                 = NVL(I_country_id,'BR')
      and federal_service_code = NVL(I_fiscal_classification_id,federal_service_code);
BEGIN
   if I_fiscal_classification_type = 'I' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FISC_CLASSIFICATION_PROD',
                       'L10N_BR_NCM_CODES',
                       NULL);
      open C_GET_FISC_CLASSIFICATION_PROD;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_FISC_CLASSIFICATION_PROD',
                       'L10N_BR_NCM_CODES',
                       NULL);
      fetch C_GET_FISC_CLASSIFICATION_PROD BULK COLLECT INTO O_fisclass_table;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_FISC_CLASSIFICATION_PROD',
                       'L10N_BR_NCM_CODES',
                       NULL);
      close C_GET_FISC_CLASSIFICATION_PROD;
   elsif I_fiscal_classification_type = 'S' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FISC_CLASSIFICATION_SERV',
                       'L10N_BR_FEDERAL_SVC_CODES',
                       NULL);
      open C_GET_FISC_CLASSIFICATION_SERV;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_FISC_CLASSIFICATION_SERV',
                       'L10N_BR_FEDERAL_SVC_CODES',
                       NULL);
      fetch C_GET_FISC_CLASSIFICATION_SERV BULK COLLECT INTO O_fisclass_table;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_FISC_CLASSIFICATION_SERV',
                       'L10N_BR_FEDERAL_SVC_CODES',
                       NULL);
      close C_GET_FISC_CLASSIFICATION_SERV;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FISC_CLASSIFICATION_ALL',
                       'L10N_BR_NCM_CODES'||' AND '||'L10N_BR_FEDERAL_SVC_CODES',
                       NULL);
      open C_GET_FISC_CLASSIFICATION_ALL;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_FISC_CLASSIFICATION_ALL',
                       'L10N_BR_NCM_CODES'||' AND '||'L10N_BR_FEDERAL_SVC_CODES',
                       NULL);
      fetch C_GET_FISC_CLASSIFICATION_ALL BULK COLLECT INTO O_fisclass_table;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_FISC_CLASSIFICATION_ALL',
                       'L10N_BR_NCM_CODES'||' AND '||'L10N_BR_FEDERAL_SVC_CODES',
                       NULL);
      close C_GET_FISC_CLASSIFICATION_ALL;
   end if;
EXCEPTION
   when OTHERS then
      O_fisclass_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     to_char(SQLCODE));
      O_fisclass_table(1).return_code   := 'FALSE';
      return;
END P_GET_FISCAL_CLASSIFICATION;
-----------------------------------------------------------------------------------------------
PROCEDURE P_UPD_FISCAL_CLASSIFICATION(I_fisclass_table           IN   FISCAL_CLASS_TBL)
IS
   L_program VARCHAR2(100) := 'FISCAL_CLASSIFICATION_SQL.P_UPD_FISCAL_CLASSIFICATION';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_first                   NUMBER(10);
   L_last                    NUMBER(10);

BEGIN
   if I_fisclass_table is NULL or I_fisclass_table.COUNT = 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_country_id',
                                            L_program,
                                            NULL);
      raise_application_error(ERRNUM_INVALID_PARAM,L_error_message);
   end if;

   L_first               := I_fisclass_table.FIRST;
   L_last                := I_fisclass_table.LAST;

   for i in L_first..L_last LOOP

      if I_fisclass_table(i).fiscal_classification_type = 'I' then
         update l10n_br_ncm_codes
            set ncm_desc   = I_fisclass_table(i).fiscal_classification_desc
          where ncm_code   = I_fisclass_table(i).fiscal_classification_id
            and country_id = I_fisclass_table(i).country_id;

      elsif I_fisclass_table(i).fiscal_classification_type = 'S' then
         update l10n_br_federal_svc_codes
            set federal_service_desc = I_fisclass_table(i).fiscal_classification_desc
          where federal_service_code = I_fisclass_table(i).fiscal_classification_id
            and 'BR'                 = I_fisclass_table(i).country_id;

      end if;

   end LOOP;

EXCEPTION
   when ERROR_PACKAGE_CALL then
      raise;
   when ERROR_INVALID_PARAM then
      raise;
   when ERROR_OTHERS then
      raise;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      raise_application_error(ERRNUM_OTHERS,L_error_message);
END P_UPD_FISCAL_CLASSIFICATION;
-----------------------------------------------------------------------------------------------
PROCEDURE P_LOCK_FISCAL_CLASS(I_fisclass_table           IN   FISCAL_CLASS_TBL)
IS
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_exists   VARCHAR2(1);
BEGIN

   FOR i in 1..I_fisclass_table.COUNT LOOP
      if I_fisclass_table(i).fiscal_classification_type = 'I' then
         select 'x'
           into L_exists
           from l10n_br_ncm_codes
          where ncm_code   = I_fisclass_table(i).fiscal_classification_id
            and country_id = I_fisclass_table(i).country_id
            for update nowait;
      elsif I_fisclass_table(i).fiscal_classification_type = 'S' then
         select 'x'
           into L_exists
           from l10n_br_federal_svc_codes
          where federal_service_code = I_fisclass_table(i).fiscal_classification_id
            and 'BR'                 = I_fisclass_table(i).country_id
            for update nowait;
      end if;
   END LOOP;

EXCEPTION
   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'L10N_BR_NCM_CODES'||' OR '||'L10N_BR_FEDERAL_SVC_CODES',
                                            NULL,
                                            NULL);
      raise_application_error(ERRNUM_OTHERS,L_error_message);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FISCAL_CLASSIFICATION_SQL.P_LOCK_FISCAL_CLASS',
                                            to_char(SQLCODE));
      raise_application_error(ERRNUM_OTHERS,L_error_message);
END P_LOCK_FISCAL_CLASS;
------------------------------------------------------------------------------------------------
PROCEDURE P_INSRT_FISCAL_CLASS(I_fisclass_table           IN   FISCAL_CLASS_TBL)
IS
   L_program VARCHAR2(100) := 'FISCAL_CLASSIFICATION_SQL.P_INSRT_FISCAL_CLASS';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_first                   NUMBER(10);
   L_last                    NUMBER(10);

BEGIN
   if I_fisclass_table is NULL or I_fisclass_table.COUNT = 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_country_id',
                                            L_program,
                                            NULL);
      raise_application_error(ERRNUM_INVALID_PARAM,L_error_message);
   end if;

   L_first               := I_fisclass_table.FIRST;
   L_last                := I_fisclass_table.LAST;

   for i in L_first..L_last LOOP

      if I_fisclass_table(i).fiscal_classification_type = 'I' then
         insert into l10n_br_ncm_codes values(I_fisclass_table(i).fiscal_classification_id,
                                              I_fisclass_table(i).country_id,
                                              I_fisclass_table(i).fiscal_classification_desc);

      elsif I_fisclass_table(i).fiscal_classification_type = 'S' then
         insert into l10n_br_federal_svc_codes values(I_fisclass_table(i).fiscal_classification_id,
                                                      I_fisclass_table(i).fiscal_classification_desc);

      end if;

   end LOOP;

EXCEPTION
   when ERROR_PACKAGE_CALL then
      raise;
   when ERROR_INVALID_PARAM then
      raise;
   when ERROR_OTHERS then
      raise;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      raise_application_error(ERRNUM_OTHERS,L_error_message);
END P_INSRT_FISCAL_CLASS;
-----------------------------------------------------------------------------------------------
END FISCAL_CLASSIFICATION_SQL;
/