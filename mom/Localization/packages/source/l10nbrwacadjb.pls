CREATE OR REPLACE PACKAGE BODY L10N_BR_WACADJ_SQL AS

FUNCTION WAC_UPDATE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_wac_update_tbl   IN OUT   NOCOPY   WAC_UPDATE_TBL,
                    I_action_type      IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program                           VARCHAR2(62)   := 'L10N_BR_WACADJ_SQL.WAC_UPDATE';
   RECORD_LOCKED                       EXCEPTION;
   PRAGMA                              EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_wac_update_tbl                    WAC_UPDATE_TBL := WAC_UPDATE_TBL();
   L_wac_update_rec                    WAC_UPDATE_REC :=NULL;


   L_tran_code       TRAN_DATA.TRAN_CODE%TYPE;
   L_adj_code        TRAN_DATA.ADJ_CODE%TYPE;
   L_total_cost      TRAN_DATA.TOTAL_COST%TYPE;
   L_tran_date       TRAN_DATA.TRAN_DATE%TYPE:= get_vdate;


   cursor C_WAC_UPDATE_TBL_VWH is
      select w.item item,
             wh.wh location,
             w.loc_type loc_type,
             w.icms_unit_amt icms_unit_amt,
             w.icmsst_unit_amt icmsst_unit_amt,
             w.icmsste_unit_amt icmsste_unit_amt,
             w.new_av_cost new_av_cost
        from TABLE(CAST(I_wac_update_tbl as WAC_UPDATE_TBL)) w,
             wh wh,
             item_loc_soh il
       where w.location = wh.physical_wh
         and il.loc = wh.wh
         and w.item= il.item
         and w.loc_type = il.loc_type
         and w.loc_type ='W'
      union all
      select w.item item,
             w.location location,
             w.loc_type loc_type,
             w.icms_unit_amt icms_unit_amt,
             w.icmsst_unit_amt icmsst_unit_amt,
             w.icmsste_unit_amt icmsste_unit_amt,
             w.new_av_cost new_av_cost
        from TABLE(CAST(I_wac_update_tbl as WAC_UPDATE_TBL)) w
       where w.loc_type = 'S';

   TYPE wac_update_vwh_tbl IS TABLE OF C_WAC_UPDATE_TBL_VWH%ROWTYPE;
   L_wac_update_vwh_tbl wac_update_vwh_tbl;

   cursor C_GET_WAC_DETAILS is
       select ils.rowid rowid1,
              im.item item,
              im.dept dept,
              im.class class,
              im.subclass subclass,
              ils.loc loc,
              ils.loc_type loc_type,
              (ils.stock_on_hand +
              ils.in_transit_qty +
              ils.pack_comp_intran +
              ils.pack_comp_soh ) units,
              ils.av_cost avg_cost_old,
              case
               when w.new_av_cost is NOT NULL then
                  w.new_av_cost
               else
                  decode(I_action_type,'insert',NVL(ils.av_cost,0)+(NVL(w.icms_unit_amt,0)+NVL(w.icmsst_unit_amt,0)+NVL(w.icmsste_unit_amt,0)),
                                       'delete',NVL(ils.av_cost,0)-(NVL(w.icms_unit_amt,0)+NVL(w.icmsst_unit_amt,0)+NVL(w.icmsste_unit_amt,0)),
                                       NVL(ils.av_cost,0))
              end avg_cost_new
         from TABLE(CAST(L_wac_update_tbl as wac_update_tbl)) w,
              item_loc_soh ils,
              item_master im
        where w.item = ils.item
          and w.location = ils.loc
          and im.item = ils.item
          for update of ils.item,ils.loc nowait;

   TYPE wac_dtl_tbl IS TABLE OF C_GET_WAC_DETAILS%ROWTYPE;
   L_wac_dtl_tbl wac_dtl_tbl;


BEGIN

   open C_WAC_UPDATE_TBL_VWH;
   fetch C_WAC_UPDATE_TBL_VWH BULK COLLECT INTO L_wac_update_vwh_tbl;
   close C_WAC_UPDATE_TBL_VWH;
  

   --- Logic for populating the VWH for the Physical WH
   FOR i IN L_wac_update_vwh_tbl.FIRST..L_wac_update_vwh_tbl.LAST LOOP
      L_wac_update_rec := WAC_UPDATE_REC (L_wac_update_vwh_tbl(i).item,
                                          L_wac_update_vwh_tbl(i).location,
                                          L_wac_update_vwh_tbl(i).loc_type,
                                          L_wac_update_vwh_tbl(i).icms_unit_amt,
                                          L_wac_update_vwh_tbl(i).icmsst_unit_amt,
                                          L_wac_update_vwh_tbl(i).icmsste_unit_amt,
                                          L_wac_update_vwh_tbl(i).new_av_cost
                                         );
      L_wac_update_tbl.extend();
      L_wac_update_tbl(L_wac_update_tbl.count) := L_wac_update_rec;
   END LOOP;

   open C_GET_WAC_DETAILS;
   fetch C_GET_WAC_DETAILS BULK COLLECT INTO L_wac_dtl_tbl;
   close C_GET_WAC_DETAILS;

   if L_wac_dtl_tbl is NOT NULL and L_wac_dtl_tbl.COUNT > 0 then
      FORALL i in 1..L_wac_dtl_tbl.COUNT
         UPDATE item_loc_soh e
            SET e.av_cost = L_wac_dtl_tbl(i).avg_cost_new,
                last_update_datetime = sysdate,
                last_update_id       = user
          WHERE rowid = L_wac_dtl_tbl(i).rowid1;
   end if;

   if L_wac_dtl_tbl is NOT NULL and L_wac_dtl_tbl.COUNT>0 then
      FOR i IN L_wac_dtl_tbl.FIRST..L_wac_dtl_tbl.LAST LOOP
         L_tran_code := 70;
         L_adj_code  := 'C';
         L_total_cost := ( NVL((L_wac_dtl_tbl(i).avg_cost_old),0)- NVL(L_wac_dtl_tbl(i).avg_cost_new,0))* L_wac_dtl_tbl(i).units;

         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                L_wac_dtl_tbl(i).item,                        -- I_item
                                                L_wac_dtl_tbl(i).dept,                        -- I_dept
                                                L_wac_dtl_tbl(i).class,                       -- I_class
                                                L_wac_dtl_tbl(i).subclass,                    -- I_subclass
                                                L_wac_dtl_tbl(i).loc,                         -- I_loc
                                                L_wac_dtl_tbl(i).loc_type,                    -- I_loc_type
                                                L_tran_date,                   -- I_tran_date
                                                L_tran_code,                   -- IO_tran_code
                                                L_adj_code,                       -- I_adj_code
                                                L_wac_dtl_tbl(i).units,                 -- unit
                                                L_total_cost,                  -- IO_total_cost
                                                NULL,                          -- I_total_retail
                                                NULL,                          -- I_ref_no_1
                                                NULL,                          -- I_ref_no_2
                                                NULL,                          -- I_tsf_souce_store
                                                NULL,                          -- I_tsf_souce_wh
                                                NULL,                          -- I_old_unit_retail
                                                NULL,                          -- I_new_unit_retail
                                                NULL,                          -- I_source_dept
                                                NULL,                          -- I_source_class
                                                NULL,                          -- I_souce_subclass
                                                L_program,                    -- I_pgm_name
                                                NULL) = FALSE then             -- I_gl_ref_no
            return FALSE;
         end if;
      END LOOP;
   end if;

   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   RETURN TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_SOH_LOCKED',
                                             'ITEM_LOC_SOH',
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
   O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END WAC_UPDATE;

END L10N_BR_WACADJ_SQL;
/