CREATE OR REPLACE PACKAGE BODY L10N_BR_FULFILORD_VALIDATE AS

   LP_user         SVCPROV_CONTEXT.USER_NAME%TYPE   := user;
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_FULFILORD_CUST_ATTRIB
-- Purpose: This is a private function that performs basic validation of staged header fulfillment
--          records on SVC_BRFULFILORD. It should process all messages on the staging table for a
--          given I_process_id,I_chunk_id and 'N' processed status.
FUNCTION CHECK_FULFILORD_CUST_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN       SVC_BRFULFILORD.PROCESS_ID%TYPE,
                                     I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_FULFILORD_PAYMENT
-- Purpose: This is a private function that performs basic validation of staged header fulfillment
--          records on SVC_BRFULFILORD_PMT. It should process all messages on the staging table for a
--          given I_process_id,I_chunk_id and 'N' processed status.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORD_PAYMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN       SVC_BRFULFILORD_PMT.PROCESS_ID%TYPE,
                                 I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SVC_FULFILORD_L10N(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'L10N_BR_FULFILORD_VALIDATE.VALIDATE_SVC_FULFILORD_L10N';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_process_id      SVC_BRFULFILORD.PROCESS_ID%TYPE;
   L_chunk_id        SVC_FULFILORD.CHUNK_ID%TYPE;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_BRFULFILORD is
      select 'x'
        from svc_brfulfilord
       where process_id = L_process_id
         and process_status <> CORESVC_FULFILORD.PROCESS_STATUS_ERROR
         for update nowait;

BEGIN

   L_process_id :=IO_l10n_obj.process_id;
   L_chunk_id :=IO_l10n_obj.chunk_id;   
   if L_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'L_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if CHECK_FULFILORD_CUST_ATTRIB(O_error_message,
                                  L_process_id,
                                  L_chunk_id)= FALSE then
      return FALSE;
   end if;
   
   if CHECK_FULFILORD_PAYMENT(O_error_message,
                              L_process_id,
                              L_chunk_id)= FALSE then
      return FALSE;
   end if;
   L_table := 'SVC_BRFULFILORD';
   open C_LOCK_SVC_BRFULFILORD;
   close C_LOCK_SVC_BRFULFILORD;

   update svc_brfulfilord
      set process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED,
          last_update_id = LP_user,
          last_update_datetime = sysdate
    where process_id = L_process_id
      and process_status <> CORESVC_FULFILORD.PROCESS_STATUS_ERROR;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_program,
                                            to_char(L_process_id));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_SVC_FULFILORD_L10N;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORD_CUST_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN       SVC_BRFULFILORD.PROCESS_ID%TYPE,
                                     I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE
                                     )
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'L10N_BR_FULFILORD_VALIDATE.CHECK_FULFILORD_CUST_ATTRIB';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   
   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord sf
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_brfulfilord sd
                      where sd.error_msg is NOT NULL
                        and sd.process_id = sf.process_id
                        and sd.fulfilord_id = sf.fulfilord_id
                        and rownum = 1)
         for update nowait;   

BEGIN      
   merge into svc_brfulfilord sbrf
   using (select *
            from (select brf.process_id,
                         brf.fulfilord_id,
                         brf.brfulfilord_id,
                         -- validate whether more than one BR record exists against a process_id
                         case
                            when exists (select 'x' 
                                    from svc_brfulfilord sro
                                   where sro.process_id = I_process_id
                                     and sro.fulfilord_id = brf.fulfilord_id
                                     and sro.process_id = brf.process_id
                                     group by sro.fulfilord_id
                                     having count(sro.brfulfilord_id)>1) then
                                 SQL_LIB.GET_MESSAGE_TEXT('EXCESS_BR_RECS_EXISTS', brf.fulfilord_id,NULL,NULL)||';'   
                         end ||
                         -- validate that Taxpayer_Type is 'J' (Corporate Taxpayer Type) or 'F' (Individual Taxpayer Type)
                         case
                            when brf.taxpayer_type is NULL or brf.taxpayer_type NOT in ('J', 'F') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_TAXPAYER_TYPE', NVL(brf.taxpayer_type,'X'), brf.brfulfilord_id, brf.fulfilord_id )||';'
                         end ||
                         -- validate required fields - corporate_taxpayer_id
                         -- if taxpayer_type is 'J'  svc_brfulfilord                         
                         case
                            when brf.taxpayer_type ='J' and
                               ( brf.corporate_taxpayer_id is NULL)then
                               SQL_LIB.GET_MESSAGE_TEXT('CORP_TAXPAYER_NOT_NULL', brf.brfulfilord_id, brf.fulfilord_id, NULL)||';'
                         end ||
                         -- validate ie_exempt                         
                         case
                            when brf.ie_exempt is NOT NULL
                               and ( brf.ie_exempt NOT in('Y','N'))then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_IE_EXEMPT_TYPE', brf.ie_exempt, brf.brfulfilord_id, brf.fulfilord_id)||';'
                         end ||                         
                         -- validate state_inscription_id when taxpayer_type is 'J' in svc_brfulfilord
                         case
                            when brf.taxpayer_type ='J' and NVL(brf.ie_exempt,'X') <> 'Y'
                               and ( brf.state_inscription_id is NULL)then
                               SQL_LIB.GET_MESSAGE_TEXT('STATE_INSCRIP_NOT_NULL', brf.brfulfilord_id, brf.fulfilord_id, NULL)||';'
                         end ||                         
                         -- validate the contributor_type
                         case
                            when brf.contributor_type is NOT NULL and 
                                 NOT exists ( select 'x'
                                                from l10n_code_head lch,
                                                     l10n_code_detail_descs lcd
                                               where lch.l10n_code_type = 'COTY'
                                                 and lch.l10n_code_type = lcd.l10n_code_type
                                                 and brf.contributor_type = lcd.l10n_code
                                                 and rownum =1) then
                                  SQL_LIB.GET_MESSAGE_TEXT('INV_CONTRIBUTOR_TYPE', brf.contributor_type,brf.brfulfilord_id, brf.fulfilord_id)||';'
                         end ||
                         -- validate the contributor_type
                         case
                            when brf.tax_exception_type is NOT NULL and 
                                 NOT exists ( select 'x'
                                                from l10n_code_head lch,
                                                     l10n_code_detail_descs lcd
                                               where lch.l10n_code_type = 'TEXC'
                                                 and lch.l10n_code_type = lcd.l10n_code_type
                                                 and brf.tax_exception_type = lcd.l10n_code
                                                 and rownum =1) then
                                  SQL_LIB.GET_MESSAGE_TEXT('INV_TAX_EXCPT_TYPE', brf.tax_exception_type,brf.brfulfilord_id, brf.fulfilord_id)||';'
                         end ||
                         -- validate individual_taxpayer_id,cust_payment_method,cust_payment_amount
                         -- if taxpayer_type is 'F'  SVC_BRFULFILORD
                         case
                            when brf.taxpayer_type ='F' and 
                               ( brf.individual_taxpayer_id is NULL ) then
                               SQL_LIB.GET_MESSAGE_TEXT('IND_TAXPAYER_ID_NOT_NULL', brf.brfulfilord_id, brf.fulfilord_id, NULL)||';'
                         end ||
                         -- if taxpayer_type is 'F'  in SVC_BRFULFILORD then presence_ind is mandatory.
                         case
                            when brf.taxpayer_type ='F' and 
                               ( brf.presence_ind is NULL ) then
                               SQL_LIB.GET_MESSAGE_TEXT('PRESENCE_IND_NOT_NULL', brf.brfulfilord_id, brf.fulfilord_id, NULL)||';'
                         end ||
                         -- if taxpayer_type is 'F'  in SVC_BRFULFILORD then presence_ind
                         -- should one among these (0, 1, 2, 3, 4 and 9).
                         case
                            when brf.taxpayer_type ='F' and 
                               ( brf.presence_ind NOT in(0,1,2,3,4,9) ) then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_PRESENCE_IND', brf.presence_ind, brf.brfulfilord_id, brf.fulfilord_id)||';'
                         end ||
                         -- validate whether payment method exists
                         case
                            when brf.taxpayer_type ='F' and brf.individual_taxpayer_id is not NULL and
                                 NOT exists( select 'x'
                                               from svc_brfulfilord_pmt
                                              where cust_payment_method is NOT NULL
                                                and fulfilord_id = brf.fulfilord_id
                                                and process_id = brf.process_id
                                                and rownum =1) then
                               SQL_LIB.GET_MESSAGE_TEXT('PAYMENT_METHOD_NOT_NULL', brf.brfulfilord_id, brf.fulfilord_id, NULL)||';'
                         end ||
                         -- validate whether payment amount is -ve, 0 or null
                         case
                            when brf.taxpayer_type ='F' and brf.individual_taxpayer_id is not NULL and
                                 NOT exists( select SUM(cust_payment_amount)
                                               from svc_brfulfilord_pmt
                                              where cust_payment_method is NOT NULL
                                                and fulfilord_id = brf.fulfilord_id
                                                and process_id = brf.process_id
                                                and process_id = I_process_id
                                                having SUM(cust_payment_amount)> 0) then
                               SQL_LIB.GET_MESSAGE_TEXT('PAYMT_AMT_CANOT_NEG_ZERO', brf.brfulfilord_id, brf.fulfilord_id, NULL)||';'
                         end error_msg 

                    from svc_brfulfilord brf
                   where brf.process_id = I_process_id
                     and brf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW)
           where error_msg is NOT NULL) iner
   on (sbrf.process_id = iner.process_id
       and sbrf.fulfilord_id = iner.fulfilord_id
       and sbrf.brfulfilord_id =iner.brfulfilord_id)
   when matched then
      update set sbrf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
                 sbrf.error_msg = substr(sbrf.error_msg || iner.error_msg, 1, 2000),
                 sbrf.last_update_id = LP_user,
                 sbrf.last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      L_table := 'SVC_FULFILORD';
      open C_LOCK_SVC_FULFILORD;
      close C_LOCK_SVC_FULFILORD;

      update svc_fulfilord sf
         set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sf.error_msg = substr(sf.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_BRFULFIL', sf.customer_order_no, sf.fulfill_order_no, NULL)||';', 1, 2000),
             sf.last_update_id = LP_user,
             sf.last_update_datetime = sysdate
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_brfulfilord sd
                      where sd.error_msg is NOT NULL
                        and sd.process_id = sf.process_id
                        and sd.fulfilord_id = sf.fulfilord_id
                        and rownum = 1);   
      select substr(substr(error_msg, 1, instr(error_msg, ';')-1), 1, 255)
        into L_error_message
        from svc_brfulfilord
       where process_id = I_process_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FULFILORD_CUST_ATTRIB;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORD_PAYMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN       SVC_BRFULFILORD_PMT.PROCESS_ID%TYPE,
                                 I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'L10N_BR_FULFILORD_VALIDATE.CHECK_FULFILORD_PAYMENT';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord sbr
       where sbr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sbr.process_id = I_process_id
         and sbr.chunk_id =I_chunk_id
         and exists (select 'x'
                       from svc_brfulfilord_pmt sp
                      where sp.error_msg is NOT NULL
                        and sp.process_id = sbr.process_id
                        and sp.fulfilord_id = sbr.fulfilord_id
                        and rownum = 1)
         for update nowait;   

BEGIN      
  merge into svc_brfulfilord_pmt spmt
   using (select *
            from (select pmt.process_id,
                         pmt.fulfilord_id,
                         pmt.brfulfilord_pmt_id,
                         -- validate the cust_payment_method
                         case
                            when pmt.cust_payment_method is NOT NULL and 
                                 NOT exists ( select 'x'
                                                from l10n_code_head lch,
                                                     l10n_code_detail_descs lcd
                                               where lch.l10n_code_type = 'PYMT'
                                                 and lch.l10n_code_type = lcd.l10n_code_type
                                                 and pmt.cust_payment_method = lcd.l10n_code
                                                 and rownum =1) then
                                  SQL_LIB.GET_MESSAGE_TEXT('INV_PAYMENT_METHOD', pmt.cust_payment_method, pmt.brfulfilord_pmt_id, pmt.fulfilord_id)||';'
                         end ||
                          --validate payment_amount should not be blank,zero or negative
                         case 
                            when pmt.cust_payment_method is NOT NULL and
                               ( pmt.cust_payment_amount is NULL or
                                pmt.cust_payment_amount <=0) then
                               SQL_LIB.GET_MESSAGE_TEXT('AMOUNT_CANNOT_NEG_ZERO', pmt.cust_payment_method, pmt.brfulfilord_pmt_id, pmt.fulfilord_id)||';'
                         end ||
                          -- validate the payment method
                         case 
                            when pmt.cust_payment_amount is NOT NULL and
                               ( pmt.cust_payment_method is NULL) then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_PAYMENT_METHOD', pmt.cust_payment_method, pmt.brfulfilord_pmt_id,pmt.fulfilord_id)||';'                                                    
                         end error_msg 
                    from svc_brfulfilord_pmt pmt
                   where pmt.process_id = I_process_id
                     and pmt.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW)
           where error_msg is NOT NULL) iner
   on (spmt.process_id = iner.process_id
       and spmt.fulfilord_id = iner.fulfilord_id
       and spmt.brfulfilord_pmt_id = iner.brfulfilord_pmt_id)
   when matched then
      update set spmt.error_msg = substr(spmt.error_msg || iner.error_msg, 1, 2000),
                 spmt.last_update_id = LP_user,
                 spmt.last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      L_table := 'SVC_FULFILORD';
      open C_LOCK_SVC_FULFILORD;
      close C_LOCK_SVC_FULFILORD;
      
      update svc_fulfilord sbr
         set sbr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sbr.error_msg = substr(sbr.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_PAYMENT', sbr.customer_order_no, fulfill_order_no, NULL)||';', 1, 2000),
             sbr.last_update_id = LP_user,             
             sbr.last_update_datetime = sysdate
       where sbr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sbr.process_id = I_process_id
         and sbr.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_brfulfilord_pmt pmt
                      where pmt.error_msg is NOT NULL
                        and pmt.process_id = sbr.process_id
                        and pmt.fulfilord_id = sbr.fulfilord_id
                        and rownum = 1);
                        
      select substr(substr(error_msg, 1, instr(error_msg, ';')-1), 1, 255)
        into L_error_message
        from svc_brfulfilord_pmt
       where process_id = I_process_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;
      
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FULFILORD_PAYMENT;
-------------------------------------------------------------------------------------------------------
END L10N_BR_FULFILORD_VALIDATE;
/