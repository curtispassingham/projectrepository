CREATE OR REPLACE PACKAGE BODY L10N_BR_DATA_MIGRATION_SQL AS
----------------------------------------------------------------------------------------
FUNCTION ADDRESS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_addr_key            IN       INVC_HEAD.ADDR_KEY%TYPE,
                 I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.ADDR';
   L_dummy               VARCHAR2(1);
   
   cursor C_GET_COUNTRY_TAX_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction_code
         and rownum = 1;

BEGIN
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                       
   open C_GET_COUNTRY_TAX_JURISDICTION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                    
   fetch C_GET_COUNTRY_TAX_JURISDICTION into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                          
   close C_GET_COUNTRY_TAX_JURISDICTION;
   
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_JURISDICTION_CODE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   update addr
      set jurisdiction_code = I_jurisdiction_code
    where addr_key = I_addr_key;

   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ADDRESS;
----------------------------------------------------------------------------------------
FUNCTION COMPHEAD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_company             IN       STORE_HIERARCHY.COMPANY%TYPE,
                  I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.COMPHEAD';
   L_dummy               VARCHAR2(1);
   
   cursor C_GET_COUNTRY_TAX_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction_code
         and rownum = 1;
BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                       
   open C_GET_COUNTRY_TAX_JURISDICTION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                    
   fetch C_GET_COUNTRY_TAX_JURISDICTION into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                          
   close C_GET_COUNTRY_TAX_JURISDICTION;
   
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_JURISDICTION_CODE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   update comphead
      set co_jurisdiction_code = I_jurisdiction_code
    where company = I_company;

   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END COMPHEAD;
----------------------------------------------------------------------------------------
FUNCTION RTV_HEAD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_rtv_order_no        IN       RTV_DETAIL.RTV_ORDER_NO%TYPE,
                  I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.RTV_HEAD';
   L_dummy               VARCHAR2(1);
   
   cursor C_GET_COUNTRY_TAX_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction_code
         and rownum = 1;
         
BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                       
   open C_GET_COUNTRY_TAX_JURISDICTION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                    
   fetch C_GET_COUNTRY_TAX_JURISDICTION into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                          
   close C_GET_COUNTRY_TAX_JURISDICTION;
   
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_JURISDICTION_CODE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   update rtv_head
      set ship_to_jurisdiction_code = I_jurisdiction_code
    where rtv_order_no = I_rtv_order_no;

   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END RTV_HEAD;
----------------------------------------------------------------------------------------
FUNCTION COMP_STORE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_store               IN       STORE.STORE%TYPE,
                    I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.COMP_STORE';
   L_dummy               VARCHAR2(1);
   
   cursor C_GET_COUNTRY_TAX_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction_code
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                       
   open C_GET_COUNTRY_TAX_JURISDICTION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                    
   fetch C_GET_COUNTRY_TAX_JURISDICTION into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                          
   close C_GET_COUNTRY_TAX_JURISDICTION;
   
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_JURISDICTION_CODE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   update comp_store
      set jurisdiction_code = I_jurisdiction_code
    where store = I_store;

   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END COMP_STORE;
----------------------------------------------------------------------------------------
FUNCTION COMPETITOR(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_competitor          IN       COMP_SHOP_LIST.COMPETITOR%TYPE,
                    I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.COMPETITOR';
   L_dummy               VARCHAR2(1);
   
   cursor C_GET_COUNTRY_TAX_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction_code
         and rownum = 1;
         
BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                       
   open C_GET_COUNTRY_TAX_JURISDICTION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                    
   fetch C_GET_COUNTRY_TAX_JURISDICTION into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                          
   close C_GET_COUNTRY_TAX_JURISDICTION;
   
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_JURISDICTION_CODE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   update competitor
      set jurisdiction_code = I_jurisdiction_code
    where competitor = I_competitor;

   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END COMPETITOR;
----------------------------------------------------------------------------------------
FUNCTION CUSTOMER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cust_id             IN       ORDCUST.CUST_ID%TYPE,
                  I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.CUSTOMER';
   L_dummy               VARCHAR2(1);
   
   cursor C_GET_COUNTRY_TAX_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction_code
         and rownum = 1;
BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                       
   open C_GET_COUNTRY_TAX_JURISDICTION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                    
   fetch C_GET_COUNTRY_TAX_JURISDICTION into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                          
   close C_GET_COUNTRY_TAX_JURISDICTION;
   
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_JURISDICTION_CODE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   update customer
      set cust_jurisdiction_code = I_jurisdiction_code
    where cust_id = I_cust_id;

   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CUSTOMER;
----------------------------------------------------------------------------------------
FUNCTION ORD_CUST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cust_id             IN       ORDCUST.CUST_ID%TYPE,
                  I_ordcust_seq_no      IN       ORDCUST.ORDCUST_SEQ_NO%TYPE,
                  I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.ORD_CUST';
   L_dummy               VARCHAR2(1);
   
   cursor C_GET_COUNTRY_TAX_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction_code
         and rownum = 1;
BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                       
   open C_GET_COUNTRY_TAX_JURISDICTION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                    
   fetch C_GET_COUNTRY_TAX_JURISDICTION into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                          
   close C_GET_COUNTRY_TAX_JURISDICTION;
   
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_JURISDICTION_CODE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   update ordcust
      set deliver_jurisdiction_code = I_jurisdiction_code
    where cust_id = I_cust_id
      and ordcust_seq_no = I_ordcust_seq_no;

   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ORD_CUST;
----------------------------------------------------------------------------------------
FUNCTION OUTLOC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_outloc_id           IN       VARCHAR2,
                I_outloc_type         IN       VARCHAR2,
                I_jurisdiction_code   IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.OUTLOC';
   L_dummy               VARCHAR2(1);
   
   cursor C_GET_COUNTRY_TAX_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction_code
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                       
   open C_GET_COUNTRY_TAX_JURISDICTION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                    
   fetch C_GET_COUNTRY_TAX_JURISDICTION into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_TAX_JURISDICTION',
                    'COUNTRY_TAX_JURISDICTION',
                    NULL);
                          
   close C_GET_COUNTRY_TAX_JURISDICTION;
   
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_JURISDICTION_CODE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   update outloc
      set outloc_jurisdiction_code = I_jurisdiction_code
    where outloc_id = I_outloc_id
      and outloc_type = I_outloc_type;

   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END OUTLOC;
----------------------------------------------------------------------------------------
 FUNCTION VAT_CODES(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_vat_code             IN       VARCHAR2,
                    I_vat_code_desc        IN       VARCHAR2,
                    I_incl_nic_ind         IN       VARCHAR2)

RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.VAT_CODES';

BEGIN
   ---
   insert into vat_codes
       values (I_vat_code,
               I_vat_code_desc,
               I_incl_nic_ind);
   
   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VAT_CODES;
----------------------------------------------------------------------------------------
FUNCTION INS_L10N_BR_ENTITY_CNAE_CODES(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'L10N_BR_DATA_MIGRATION_SQL.INS_L10N_BR_ENTITY_CNAE_CODES';

BEGIN
   ---
   insert into l10n_br_entity_cnae_codes
             ( key_value_1,
              key_value_2,
              module,
              country_id,
              cnae_code,
              primary_ind)
        select key_value_1,
               key_value_2,
               module,
               country_id,
               cnae_code,
               primary_ind
          from upg_l10n_br_entity_cnae_codes;

 --For inserting the CNAE CODES for all the virtual wh whose physical wh are already present in the l10n_br_entity_cnae_codes table.
   insert into l10n_br_entity_cnae_codes
        select *
          from (select wh key_value_1,
                       key_value_2,
                       module,
                       country_id,
                       cnae_code,
                       primary_ind
                  from l10n_br_entity_cnae_codes a,
                       wh b
                 where a.key_value_1 = b.physical_wh
                   and a.key_value_2 ='W'
                   and a.module = 'LOC'
                   and b.wh<>b.physical_wh
                   and not exists (select 'X'
                                     from l10n_br_entity_cnae_codes c
                                    where b.wh = c.key_value_1
                                      and a.key_value_2 = c.key_value_2
                                      and a.module = c.module));


   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END INS_L10N_BR_ENTITY_CNAE_CODES;
----------------------------------------------------------------------------------------
FUNCTION PURGE_STAGING_TBL (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_base_table          IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50)  := 'L10N_BR_DATA_MIGRATION_SQL.PURGE_STAGING_TBL';
   L_STG_TBL     VARCHAR2(30)  := NULL;
   L_DELETE_STR  VARCHAR2(100) := NULL;
BEGIN

   L_STG_TBL := 'UPG_' || I_base_table ;

   L_DELETE_STR := 'delete from ' ||
                   L_STG_TBL ;

   EXECUTE IMMEDIATE L_DELETE_STR;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PURGE_STAGING_TBL;
----------------------------------------------------------------------------------------
FUNCTION ITEM_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS

   L_program                    VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.ITEM_COST';
   L_country_attrib_row         COUNTRY_ATTRIB%ROWTYPE;
   L_system_options_row         SYSTEM_OPTIONS%ROWTYPE;
   L_item_cost_head_row         ITEM_COST_HEAD%ROWTYPE;
   L_base_cost                  ITEM_COST_HEAD.BASE_COST%TYPE;
   L_extended_base_cost         ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost             ITEM_COST_HEAD.INCLUSIVE_COST%TYPE;
   L_negotiated_item_cost       ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE;
   L_default_loc                COUNTRY_ATTRIB.DEFAULT_LOC%TYPE;
   L_default_loc_type           COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE;
   L_item                       ITEM_COUNTRY.ITEM%TYPE;
   L_prim_dlvy_ctry_ind         ITEM_COST_HEAD.PRIM_DLVY_CTRY_IND%TYPE;
   L_prim_delivery_country_id   ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_add_1                      ADDR.ADD_1%TYPE;
   L_add_2                      ADDR.ADD_2%TYPE;
   L_add_3                      ADDR.ADD_3%TYPE;
   L_city                       ADDR.CITY%TYPE;
   L_state                      ADDR.STATE%TYPE;
   L_country_id                 ADDR.COUNTRY_ID%TYPE;
   L_post                       ADDR.POST%TYPE;
   L_module                     ADDR.MODULE%TYPE;
   L_key_value_1                ADDR.KEY_VALUE_1%TYPE;
   L_key_value_2                ADDR.KEY_VALUE_2%TYPE;
   L_country                    COUNTRY_ATTRIB.COUNTRY_ID%TYPE;
   L_item_cost_tax_incl_ind     COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE;
   
   cursor C_GET_ITEM_SUPP_COUNTRY is
      select item,
             supplier,
             origin_country_id,
             unit_cost
        from item_supp_country;

   cursor C_GET_ITEM_SUPP_COUNTRY_LOC is
      select item,
             supplier,
             origin_country_id,
             loc,
             loc_type
        from item_supp_country_loc;
        
   cursor C_GET_COUNTRY is
      select country_id
        from item_country
       where item = L_item;

   cursor C_ITEM_COST_TAX_INCL_IND is
      select item_cost_tax_incl_ind
        from country_attrib
       where country_id = L_country;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   FOR i IN C_GET_ITEM_SUPP_COUNTRY LOOP
      L_item               := i.item;
      L_country_attrib_row := NULL;
      L_prim_dlvy_ctry_ind := 'N';
      FOR j in C_GET_COUNTRY LOOP
         L_country := j.country_id;

         open C_ITEM_COST_TAX_INCL_IND;
         fetch C_ITEM_COST_TAX_INCL_IND into L_item_cost_tax_incl_ind;
         close C_ITEM_COST_TAX_INCL_IND;

         if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                    L_country_attrib_row,
                                                    NULL,
                                                    j.country_id) = FALSE then
            return FALSE;
         end if;
         ---
         if L_country_attrib_row.item_cost_tax_incl_ind = 'Y' then
            L_negotiated_item_cost := i.unit_cost;
            L_base_cost            := NULL;
         else
            L_base_cost            := i.unit_cost;
            L_negotiated_item_cost := NULL;
         end if;
            
         if L_system_options_row.default_tax_type = 'GTAX' then
            L_default_loc        := L_country_attrib_row.default_loc;
            L_default_loc_type   := L_country_attrib_row.default_loc_type;
         elsif L_system_options_row.default_tax_type = 'SVAT' then
            L_default_loc        := NULL;
            L_default_loc_type   := NULL;
         end if;
         ---
         if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                             L_base_cost,
                                             L_extended_base_cost,
                                             L_inclusive_cost,
                                             L_negotiated_item_cost,
                                             i.item,
                                             'N',
                                             i.supplier,
                                             L_default_loc,
                                             L_default_loc_type,
                                             'ITEMSUPPCTRY') = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_COST_SQL.PRIMARY_DLVY_CTRY_EXISTS(O_error_message,
                                                   L_prim_delivery_country_id,
                                                   i.item,
                                                   i.supplier,
                                                   i.origin_country_id) = FALSE then
            return FALSE;
         end if;
         ---
         if L_prim_delivery_country_id is NULL or
            L_system_options_row.base_country_id = j.country_id then
            L_prim_dlvy_ctry_ind := 'Y';
         else
            L_prim_dlvy_ctry_ind := 'N';
         end if;
         ---
         if L_prim_delivery_country_id is NOT NULL then
            if ITEM_COST_SQL.CHANGE_PRIM_DLVY_CTRY(O_error_message,
                                                   i.item,
                                                   i.supplier,
                                                   i.origin_country_id) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message=> O_error_message,
                                             O_base_cost=> L_base_cost,
                                             O_extended_base_cost=> L_extended_base_cost,
                                             O_inclusive_cost=> L_inclusive_cost,
                                             O_negotiated_item_cost=> L_negotiated_item_cost,
                                             I_item=> i.item,
                                             I_nic_static_ind=> 'N',
                                             I_supplier=> i.supplier,
                                             I_location=> L_default_loc,
                                             I_loc_type=> L_default_loc_type,
                                             I_origin_country_id=> i.origin_country_id,
                                             I_delivery_country_id=> j.country_id,
                                             I_prim_dlvy_ctry_ind=> L_prim_dlvy_ctry_ind,
                                             I_update_itemcost_ind=>'Y',
                                             I_update_itemcost_child_ind =>'Y',
                                             I_item_cost_tax_incl_ind=> L_item_cost_tax_incl_ind) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST (O_error_message,
                                                      i.item,
                                                      i.supplier,
                                                      i.origin_country_id,
                                                      NULL,
                                                      L_base_cost,
                                                      L_extended_base_cost,
                                                      L_negotiated_item_cost,
                                                      L_inclusive_cost,
                                                      'ITEMSUPPCTRY') = FALSE then
            return FALSE;
         end if;
      END LOOP;
   END LOOP;
   
   FOR i IN C_GET_ITEM_SUPP_COUNTRY_LOC LOOP
      if i.loc_type = 'W' then
         L_module := 'WH';
         L_key_value_1 := i.lOC;
      end if;

      if i.loc_type = 'S' then
         L_module := 'ST';
         L_key_value_1 := i.loc;
      end if;

      if i.loc_type = 'E' then
         L_module := 'PTNR';
         L_key_value_1 := i.loc_type;
         L_key_value_2 := i.loc;
      end if;

      if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                   L_add_1,
                                   L_add_2,
                                   L_add_3,
                                   L_city,
                                   L_state,
                                   L_country_id,
                                   L_post,
                                   L_module,
                                   L_key_value_1,
                                   L_key_value_2) = FALSE then
         return FALSE;
      end if;
      ---
      if ITEM_COST_SQL.GET_ITEM_COST_HEAD(O_error_message,
                                          L_item_cost_head_row,
                                          i.item,
                                          i.supplier,
                                          i.origin_country_id,
                                          L_country_id) = FALSE then
         return FALSE;
      end if;
      ---
      if NVL(L_item_cost_head_row.nic_static_ind,'N') = 'Y' then
         L_negotiated_item_cost := L_item_cost_head_row.negotiated_item_cost;
         L_base_cost            := NULL;
      else
         L_base_cost            := L_item_cost_head_row.base_cost;
         L_negotiated_item_cost := NULL;
      end if;
      ---
      if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                          L_base_cost,
                                          L_extended_base_cost,
                                          L_inclusive_cost,
                                          L_negotiated_item_cost,
                                          i.item,
                                          'N',
                                          i.supplier,
                                          i.loc,
                                          i.loc_type,
                                          'ITEMSUPPCTRYLOC') = FALSE then
         return FALSE;
      end if;
      ---
      if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST (O_error_message,
                                                   i.item,
                                                   i.supplier,
                                                   i.origin_country_id,
                                                   i.loc,
                                                   L_base_cost,
                                                   L_extended_base_cost,
                                                   L_negotiated_item_cost,
                                                   L_inclusive_cost,
                                                   'ITEMSUPPCTRYLOC') = FALSE then
         return FALSE;
      end if;
   END LOOP;
   
   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_COST;
----------------------------------------------------------------------------------------
FUNCTION COUNTRY_ATTRIBUTES(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_default_loc             IN       COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                            I_default_loc_type        IN       COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                            I_check_ind               IN       VARCHAR2)
RETURN BOOLEAN IS
   
   L_program    VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.COUNTRY_ATTRIB';

BEGIN
   
   if I_check_ind ='Y' then
      update country_attrib
         set default_loc = I_default_loc,
             default_loc_type = I_default_loc_type,
             localized_ind = 'Y'
       where country_id = 'BR';
   else
      update country_attrib
         set item_cost_tax_incl_ind = 'Y',
             default_po_cost = 'NIC',
             default_deal_cost = 'NIC',
             default_cost_comp_cost = 'NIC'
       where country_id = 'BR';
   end if;
    
    return TRUE;
    ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END COUNTRY_ATTRIBUTES;
----------------------------------------------------------------------------------------
FUNCTION FUTURE_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.FUTURE_COST';
   L_item                    ITEM_MASTER.ITEM%TYPE;
   L_obj_itemloc_tbl         OBJ_ITEMLOC_TBL;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_default_tax_type        SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;
   L_count                   NUMBER;
   L_event_run_type          COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE;

   cursor C_FUTURE_COST_COUNT is
      select count(*)
        from future_cost;

   cursor C_GET_ITEM_LOC is
      select distinct item,loc
        from item_loc;

   cursor C_GET_EVENT_RUN_TYPE is
      select event_run_type
        from cost_event_run_type_config
       where event_type='NIL';

BEGIN

   OPEN C_FUTURE_COST_COUNT;
   FETCH C_FUTURE_COST_COUNT into L_count;
   CLOSE C_FUTURE_COST_COUNT;


   if L_count = 0 then
      L_obj_itemloc_tbl := OBJ_ITEMLOC_TBL(NULL);
      
      OPEN C_GET_EVENT_RUN_TYPE;
      FETCH C_GET_EVENT_RUN_TYPE into L_event_run_type;
      CLOSE C_GET_EVENT_RUN_TYPE;

      update cost_event_run_type_config
         set event_run_type ='SYNC'
       where event_type='NIL';

      update country_attrib
         set localized_ind = 'N'
       where  country_id='BR';

      update system_options
         set default_tax_type    = NULL,
             vat_ind             = 'N',
             default_vat_region  = NULL,
             stkldgr_vat_incl_retl_ind = NULL;

      for rec in C_GET_ITEM_LOC LOOP
         L_obj_itemloc_tbl(L_obj_itemloc_tbl.count) := OBJ_ITEMLOC_REC(rec.item,
                                                                       rec.loc);

         if FUTURE_COST_EVENT_SQL.ADD_NIL(O_error_message,
                                          L_cost_event_process_id,
                                          L_obj_itemloc_tbl,
                                          USER) = FALSE then
            return FALSE;
         end if;
      end LOOP;

      update country_attrib
         set localized_ind = 'Y'
       where  country_id='BR';

     update system_options
        set default_tax_type    = 'GTAX',
            vat_ind             = 'Y',
            default_vat_region  = 1000,
            stkldgr_vat_incl_retl_ind = 'Y';

       update cost_event_run_type_config
         set event_run_type = L_event_run_type
       where event_type='NIL';

   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END FUTURE_COST;
----------------------------------------------------------------------------------------
FUNCTION COST_SUSP_SUP_DETAIL_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'DATA_MIGRATION_SQL.COST_SUSP_SUP_DETAIL_LOC';
   L_add_1              ADDR.ADD_1%TYPE;
   L_add_2              ADDR.ADD_2%TYPE;
   L_add_3              ADDR.ADD_3%TYPE;
   L_city               ADDR.CITY%TYPE;
   L_state              ADDR.STATE%TYPE;
   L_country_id         ADDR.COUNTRY_ID%TYPE;
   L_post               ADDR.POST%TYPE;
   L_module             ADDR.MODULE%TYPE;
   L_key_value_1        ADDR.KEY_VALUE_1%TYPE;
   L_key_value_2        ADDR.KEY_VALUE_2%TYPE;
   L_default_tax_type   VARCHAR2(6);

   cursor C_GET_COST_SUSP_SUP_DETAIL_LOC is
      select rowid,
             loc_type,
             loc
        from cost_susp_sup_detail_loc;

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                              L_default_tax_type) = FALSE then
      return FALSE;
   end if;

   if L_default_tax_type = 'GTAX' then
      for rec in C_GET_COST_SUSP_SUP_DETAIL_LOC LOOP
         if rec.loc_type = 'W' then
            L_module := 'WH';
            L_key_value_1 := rec.loc;
         end if;
   
         if rec.loc_type = 'S' then
            L_module := 'ST';
            L_key_value_1 := rec.loc;
         end if;
   
         if rec.loc_type = 'E' then
            L_module := 'PTNR';
            L_key_value_1 := rec.loc_type;
            L_key_value_2 := rec.loc;
         end if;
   
         if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                      L_add_1,
                                      L_add_2,
                                      L_add_3,
                                      L_city,
                                      L_state,
                                      L_country_id,
                                      L_post,
                                      L_module,
                                      L_key_value_1,
                                      L_key_value_2) = FALSE then
            return FALSE;
         end if;
   
         update cost_susp_sup_detail_loc
            set delivery_country_id = L_country_id
          where rowid = rec.rowid;
   
      end LOOP;
   end if;
   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END COST_SUSP_SUP_DETAIL_LOC;
----------------------------------------------------------------------------------------
FUNCTION COST_SUSP_SUP_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(50) := 'DATA_MIGRATION_SQL.COST_SUSP_SUP_DETAIL';
   L_item               ITEM_MASTER.ITEM%TYPE := NULL;
   L_default_tax_type   VARCHAR2(6);

   cursor C_GET_COST_SUSP_SUP_DETAIL is
      select rowid,
             cost_change,
             supplier,
             origin_country_id,
             item,
             bracket_value1,
             bracket_uom1,
             bracket_value2,
             unit_cost,
             cost_change_type,
             cost_change_value,
             recalc_ord_ind,
             default_bracket_ind,
             dept,
             sup_dept_seq_no,
             delivery_country_id
        from cost_susp_sup_detail;

   cursor C_GET_ITEM_COUNTRY is
      select item,
             country_id
        from item_country
       where item = L_item;

   TYPE cost_susp_sup_detail_tbl IS TABLE of C_GET_COST_SUSP_SUP_DETAIL%ROWTYPE index by binary_integer;
   TYPE item_country_tbl IS TABLE of C_GET_ITEM_COUNTRY%ROWTYPE index by binary_integer;

   L_cost_susp_sup_detail_tbl   cost_susp_sup_detail_tbl;
   L_item_country_tbl           item_country_tbl;

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                              L_default_tax_type) = FALSE then
      return FALSE;
   end if;

   if L_default_tax_type = 'GTAX' then
      open C_GET_COST_SUSP_SUP_DETAIL;

      fetch C_GET_COST_SUSP_SUP_DETAIL BULK COLLECT into L_cost_susp_sup_detail_tbl;
      
      for rec in L_cost_susp_sup_detail_tbl.first..L_cost_susp_sup_detail_tbl.last LOOP
         L_item := L_cost_susp_sup_detail_tbl(rec).item;
         for rec2 in C_GET_ITEM_COUNTRY LOOP
            if C_GET_ITEM_COUNTRY%ROWCOUNT > 1 then
               insert into cost_susp_sup_detail (cost_change,
                                                 supplier,
                                                 origin_country_id,
                                                 item,
                                                 bracket_value1,
                                                 bracket_uom1,
                                                 bracket_value2,
                                                 unit_cost,
                                                 cost_change_type,
                                                 cost_change_value,
                                                 recalc_ord_ind,
                                                 default_bracket_ind,
                                                 dept,
                                                 sup_dept_seq_no,
                                                 delivery_country_id)
                                         values (L_cost_susp_sup_detail_tbl(rec).cost_change,
                                                 L_cost_susp_sup_detail_tbl(rec).supplier,
                                                 L_cost_susp_sup_detail_tbl(rec).origin_country_id,
                                                 L_cost_susp_sup_detail_tbl(rec).item,
                                                 L_cost_susp_sup_detail_tbl(rec).bracket_value1,
                                                 L_cost_susp_sup_detail_tbl(rec).bracket_uom1,
                                                 L_cost_susp_sup_detail_tbl(rec).bracket_value2,
                                                 L_cost_susp_sup_detail_tbl(rec).unit_cost,
                                                 L_cost_susp_sup_detail_tbl(rec).cost_change_type,
                                                 L_cost_susp_sup_detail_tbl(rec).cost_change_value,
                                                 L_cost_susp_sup_detail_tbl(rec).recalc_ord_ind,
                                                 L_cost_susp_sup_detail_tbl(rec).default_bracket_ind,
                                                 L_cost_susp_sup_detail_tbl(rec).dept,
                                                 L_cost_susp_sup_detail_tbl(rec).sup_dept_seq_no,
                                                 rec2.country_id);
            else
               update cost_susp_sup_detail
                  set delivery_country_id = rec2.country_id
                where rowid = L_cost_susp_sup_detail_tbl(rec).rowid;
            end if;
         end LOOP;
      end LOOP;
      close C_GET_COST_SUSP_SUP_DETAIL;
   end if;
      
   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END COST_SUSP_SUP_DETAIL;
----------------------------------------------------------------------------------------
FUNCTION SYS_OPTIONS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.SYS_OPTIONS';

BEGIN

   update system_options
      set default_tax_type = 'GTAX',
          vat_ind          = 'Y',
          default_vat_region = 1000,
          stkldgr_vat_incl_retl_ind = 'Y';

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END SYS_OPTIONS;
----------------------------------------------------------------------------------------
FUNCTION GET_PROCESS_SEQ_VAL(I_ext IN  VARCHAR2)
RETURN NUMBER
IS
   L_program          VARCHAR2(50)  := 'L10N_BR_DATA_MIGRATION_SQL.GET_PROCESS_SEQ_VAL';
   L_SELECT_NEXT_STR  VARCHAR2(1000) := NULL;
   L_SEQ_NEXT_STR     VARCHAR2(60)  := NULL;
   l_process_no       NUMBER;

BEGIN

   L_SEQ_NEXT_STR := I_ext || '_SEQ.NEXTVAL';

   if G_process_no is null then
     L_SELECT_NEXT_STR := 'select ' ||L_SEQ_NEXT_STR ||' from dual';
     EXECUTE IMMEDIATE L_SELECT_NEXT_STR into G_process_no;
     return G_process_no;
   else
     return G_process_no;
   end if;

 EXCEPTION
    WHEN OTHERS THEN
         return 0;
END GET_PROCESS_SEQ_VAL;


----------------------------------------------------------------------------------------
FUNCTION PURGE_STG_L10N_EXT   (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_base_table           IN       VARCHAR2,
                               I_process_id           IN       NUMBER)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50)  := 'L10N_BR_DATA_MIGRATION_SQL.PURGE_STG_L10N_EXT';
   L_STG_TBL     VARCHAR2(30)  := NULL;
   L_DELETE_STR  VARCHAR2(100) := NULL;
BEGIN
   
   L_STG_TBL := 'STG_' || I_base_table || '_L10N_EXT_BR';

   L_DELETE_STR := 'delete from ' ||
                   L_STG_TBL ||
                   ' where process_id = ' || I_process_id;

   EXECUTE IMMEDIATE L_DELETE_STR;

   return TRUE;
   
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PURGE_STG_L10N_EXT;
----------------------------------------------------------------------------------------
FUNCTION ORD_TAX_BREAKUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'L10N_BR_DATA_MIGRATION_SQL.ORD_TAX_BREAKUP';
   L_array_size   NUMBER := 10000;

   cursor C_GET_OPEN_ORDER is
      select ord.order_no
        from ordhead ord
       where not exists (select order_no
                           from shipment
                          where order_no = ord.order_no)
         and ord.status = 'A';

   TYPE open_order_tbl IS TABLE of C_GET_OPEN_ORDER%ROWTYPE index by binary_integer;
   L_open_order_tbl   open_order_tbl;
   
BEGIN
   open C_GET_OPEN_ORDER;

   fetch C_GET_OPEN_ORDER BULK COLLECT into L_open_order_tbl LIMIT L_array_size;

   for rec in L_open_order_tbl.first..L_open_order_tbl.last LOOP
      if ORDER_STATUS_SQL.INSERT_ORD_TAX_BREAKUP(O_error_message,
                                                 L_open_order_tbl(rec).order_no) = FALSE then
         return FALSE;
      end if;
   end LOOP;

   close C_GET_OPEN_ORDER;

   return TRUE;
   
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ORD_TAX_BREAKUP;
----------------------------------------------------------------------------------------
FUNCTION MRT_L10N_EXT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'L10N_BR_DATA_MIGRATION_SQL.MRT_L10N_EXT ';
   L_tsf_util_id  FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;

   cursor C_GET_TSF_UTIL_ID is
      select string_value
        from fm_system_options
       where variable = 'DEFAULT_OUTBOUND_TSF_UTIL_ID';

BEGIN

   open c_get_tsf_util_id;
   fetch c_get_tsf_util_id into L_tsf_util_id;
   close c_get_tsf_util_id;

   insert into mrt_l10n_ext
               ( mrt_no,
                 l10n_country_id,
                 group_id,
                 varchar2_1)
        select mrt_no,
               'BR',
               26,
               L_tsf_util_id
          from mrt
         where mrt_status = 'A';

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END MRT_L10N_EXT ;
----------------------------------------------------------------------------------------
FUNCTION ORDHEAD_L10N_EXT  (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)  := 'L10N_BR_DATA_MIGRATION_SQL.ORDHEAD_L10N_EXT ';
   L_default_po_type    FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;

   cursor C_GET_PO_TYPE is
      select string_value
        from fm_system_options
       where variable = 'DEFAULT_PO_TYPE';

BEGIN

   open c_get_po_type;
   fetch c_get_po_type into L_default_po_type;
   close c_get_po_type;

   insert into ordhead_l10n_ext
               (order_no,
                l10n_country_id,
                group_id,
                varchar2_1)
        select order_no,
               'BR',
               25,
               L_default_po_type
          from ordhead
         where status = 'A';

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ORDHEAD_L10N_EXT ;
----------------------------------------------------------------------------------------
FUNCTION TSFHEAD_L10N_EXT  (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'L10N_BR_DATA_MIGRATION_SQL.TSFHEAD_L10N_EXT ';

   L_tsf_util_id  FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_ic_util_id   FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;

   cursor C_GET_TSF_UTIL_ID is
      select string_value
        from fm_system_options
       where variable = 'DEFAULT_OUTBOUND_TSF_UTIL_ID';

   cursor C_GET_IC_UTIL_ID is
      select string_value
        from fm_system_options
       where variable = 'DEFAULT_OUTBOUND_IC_UTIL_ID';

BEGIN

   open c_get_tsf_util_id;
   fetch c_get_tsf_util_id into L_tsf_util_id;
   close c_get_tsf_util_id;

   open c_get_ic_util_id;
   fetch c_get_ic_util_id into L_ic_util_id;
   close c_get_ic_util_id;

   insert into tsfhead_l10n_ext
               (tsf_no,
                l10n_country_id,
                group_id,
                varchar2_1)
        select tsf_no,
               'BR',
               24,
               L_tsf_util_id
          from tsfhead
         where status = 'A'
           and tsf_type != 'IC';

   insert into tsfhead_l10n_ext
              (tsf_no,
               l10n_country_id,
               group_id,
               varchar2_1)
        select tsf_no,
               'BR',
               24,
               L_ic_util_id
          from tsfhead
         where status = 'A'
           and tsf_type = 'IC';


   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END TSFHEAD_L10N_EXT ;
----------------------------------------------------------------------------------------
FUNCTION GTAX_ITEM_ROLLUP  (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'L10N_BR_DATA_MIGRATION_SQL.GTAX_ITEM_ROLLUP ';

   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
   L_exists                BOOLEAN;
   L_loc_currency_code     WH.CURRENCY_CODE%TYPE := NULL;
   L_tax_calc_rec          OBJ_TAX_CALC_REC;
   L_tax_calc_tbl          OBJ_TAX_CALC_TBL;
   L_tax_loc_type          VARCHAR2(2) := NULL;
   L_get_vdate             DATE := GET_VDATE;

   lp_call_type_nil         VARCHAR2(20)             := 'NIL';-- new item loc

   cursor C_ITEM_LOC is
      select im.item item,
             im.pack_ind pack_ind,
             il.loc loc,
             il.loc_type loc_type,
             il.unit_retail unit_retail
        from item_loc il,
             item_master im
       where il.loc_type in ('S','W')
         and im.sellable_ind ='Y'
         and il.item = im.item;

   TYPE TYP_item_loc IS TABLE OF C_ITEM_LOC%ROWTYPE INDEX BY BINARY_INTEGER;
   TBL_TYP_item_loc        TYP_item_loc ;

BEGIN

   open C_ITEM_LOC;
   fetch C_ITEM_LOC bulk collect into TBL_TYP_item_loc;
   close C_ITEM_LOC;

   FOR i IN TBL_TYP_item_loc.FIRST .. TBL_TYP_item_loc.LAST LOOP

      if LOCATION_ATTRIB_SQL.GET_LOCATION_CURRENCY(L_error_message,
                                                   L_exists,
                                                   L_loc_currency_code,
                                                   TBL_TYP_item_loc(i).loc_type,
                                                   TBL_TYP_item_loc(i).loc) = FALSE then
         return FALSE;
      end if;
      --
      if TBL_TYP_item_loc(i).loc_type = 'W' then
         L_tax_loc_type := 'WH';
      else
         L_tax_loc_type := 'ST';
      end if;

      L_tax_calc_tbl := new OBJ_TAX_CALC_TBL();
      L_tax_calc_rec := new OBJ_TAX_CALC_REC(TBL_TYP_item_loc(i).item,         --I_item
                                             TBL_TYP_item_loc(i).pack_ind,         --I_pack_ind
                                             TBL_TYP_item_loc(i).loc,         --I_from_entity
                                             L_tax_loc_type,     --I_from_entity_type
                                             NULL,               --I_to_entity
                                             NULL,               --I_to_entity_type
                                             L_get_vdate,          --I_effective_from_date
                                             TBL_TYP_item_loc(i).unit_retail,  --I_amount
                                             L_loc_currency_code,--I_amount_curr
                                             NULL,               --O_cum_tax_pct
                                             NULL,               --O_cum_tax_value
                                             NULL,               --O_total_tax_amount
                                             NULL,               --O_total_tax_amount_curr
                                             NULL,               --O_total_recover_amount
                                             NULL,               --O_total_recover_amount_curr
                                             NULL);              --OBJ_TAX_DETAIL_TBL
      L_tax_calc_tbl.extend();
      L_tax_calc_tbl(L_tax_calc_tbl.count) := L_tax_calc_rec;
      ---
      if TAX_SQL.CALC_RETAIL_TAX(L_error_message,
                                 L_tax_calc_tbl,
                                 lp_call_type_nil) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GTAX_ITEM_ROLLUP ;
----------------------------------------------------------------------------------------
FUNCTION UPD_NIC_COST_TYPE(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.UPD_NIC_COST_TYPE';

BEGIN
   ---------

   update item_supp_country_loc
      set unit_cost= negotiated_item_cost;
   ---------
    update item_supp_country
       set unit_cost= negotiated_item_cost;
   ---------
   update future_cost
      set default_costing_type ='NIC';
   --------
    return TRUE;
    ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR (SQLCODE));
      return FALSE;

END UPD_NIC_COST_TYPE;
----------------------------------------------------------------------------------------
FUNCTION DEFAULT_FISCAL_ATTRIB(O_error_message  IN OUT   VARCHAR2)

RETURN BOOLEAN AS

   L_program VARCHAR2(50) := 'L10N_BR_DATA_MIGRATION_SQL.DEFAULT_FISCAL_ATTRIB';   

BEGIN

   --- Insert fiscal attributes for vwh
   insert into wh_l10n_ext
              (wh,
               l10n_country_id,        
               group_id,               
               varchar2_1,             
               varchar2_2,             
               varchar2_3,             
               varchar2_4,             
               varchar2_5,             
               varchar2_6,             
               varchar2_7,             
               varchar2_8,             
               varchar2_9,             
               varchar2_10,            
               number_11,              
               number_12,              
               number_13,              
               number_14,              
               number_15,              
               number_16,              
               number_17,              
               number_18,              
               number_19,              
               number_20,              
               date_21,                
               date_22)  
        select vwh.wh,
               ext.l10n_country_id,
               ext.group_id,
               ext.varchar2_1,
               ext.varchar2_2,
               ext.varchar2_3,
               ext.varchar2_4,
               ext.varchar2_5,
               ext.varchar2_6,
               ext.varchar2_7,
               ext.varchar2_8,
               ext.varchar2_9,
               ext.varchar2_10,
               ext.number_11,
               ext.number_12,
               ext.number_13,
               ext.number_14,
               ext.number_15,
               ext.number_16,
               ext.number_17,
               ext.number_18,
               ext.number_19,
               ext.number_20,
               ext.date_21,
               ext.date_22
          from wh_l10n_ext ext,
               wh vwh
         where vwh.physical_wh = ext.wh
           and vwh.physical_wh != vwh.wh
           and not exists (select 1
                             from wh_l10n_ext w
                            where w.wh = vwh.wh
                              and w.l10n_country_id = ext.l10n_country_id
                              and w.group_id = ext.group_id
                              and rownum = 1)
         order by vwh.wh, ext.group_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_FISCAL_ATTRIB;
----------------------------------------------------------------------------------------
END L10N_BR_DATA_MIGRATION_SQL;
/
