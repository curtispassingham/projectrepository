CREATE OR REPLACE PACKAGE FM_FISCAL_DOC_HEADER_SQL is
--------------------------------------------------------------------------------
-- Function Name: CHECK_RMA_UPD_ERR
-- Purpose:       This function will check whether there are any rms udpation errors for the given fiscal number.
----------------------------------------------------------------------------------
FUNCTION CHECK_RMS_UPD_ERR(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT BOOLEAN,
                           I_fiscal_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_NEXT_FISCAL_DOC_ID
-- Purpose:       This function gets the next foscal_doc_id number.
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_FISCAL_DOC_ID(O_error_message  IN OUT VARCHAR2,
                                O_fiscal_doc_id  IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: EXISTS_DETAIL
-- Purpose:       This function checks if a Fiscal document header has details.
----------------------------------------------------------------------------------
FUNCTION EXISTS_DETAIL(O_error_message  IN OUT VARCHAR2,
                      O_exist          IN OUT BOOLEAN,
                      I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: UPDATE_STATUS
-- Purpose:       This function updates the Fiscal Doc Header Status.
----------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message  IN OUT VARCHAR2,
                       I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                       I_status         IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: EXISTS_FISCAL_NO
-- Purpose:       This function checks if Fiscal Doc No exists.
----------------------------------------------------------------------------------
FUNCTION EXISTS_FISCAL_NO(O_error_message  IN OUT VARCHAR2,
                          O_exists         IN OUT BOOLEAN,
                          I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                          I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                          I_fiscal_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_fiscal_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
FUNCTION EXISTS_PRINT_FISCAL_NO(O_error_message  IN OUT VARCHAR2,
                                O_exists         IN OUT BOOLEAN,
                                I_fiscal_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                I_sched_no       IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: GET_FISCAL_DOC_HEADER_INFO
-- Purpose:       This function gets fiscal_doc_header informations.
----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_HEADER_INFO(O_error_message     IN OUT VARCHAR2,
                                    O_fiscal_doc_header IN OUT FM_FISCAL_DOC_HEADER%ROWTYPE,
                                    I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: COPY_DOC_HEADER
-- Purpose:       This function will create a new document from an existing.
----------------------------------------------------------------------------------
FUNCTION COPY_DOC_HEADER(O_error_message     IN OUT VARCHAR2,
                         O_new_fiscal_doc_id IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_new_schedule      IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                         I_location_id       IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                         I_location_type     IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                         I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_operation         IN     VARCHAR2)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: REVERSE_DOC_HEADER
-- Purpose:       This function will proccess the reversion of NF.
----------------------------------------------------------------------------------
FUNCTION REVERSE_FISCAL_DOC(O_error_message  IN OUT VARCHAR2,
                            I_new_schedule   IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                            I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: CHECK_DUP_DOC
-- Purpose:       This function checks if exists duplicated document.
----------------------------------------------------------------------------------
FUNCTION CHECK_DUP_DOC(O_error_message    IN OUT VARCHAR2,
                       O_exists           IN OUT BOOLEAN,
                       I_mode_type        IN     FM_SCHEDULE.MODE_TYPE%TYPE,
                       I_fiscal_doc_id    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_id%TYPE,
                       I_fiscal_doc_no    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                       I_series_no        IN     FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE,
                       I_module           IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                       I_key_value_1      IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                       I_key_value_2      IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                       I_issue_date       IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                       I_requisition_type IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: CHECK_FOR_PENDING_DOC
-- Purpose:       This function checks if exists duplicated document.
----------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS_FOR_PENDING_DOC(O_error_message  IN OUT VARCHAR2,
                                       I_fiscal_doc_no  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                       I_series_no      IN     FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE,
                                       I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                                       I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                                       I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                                       I_issue_date     IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: CHECK_DISCREPANCY
-- Purpose:       This function identifies ,if there any type of discrepancy exists for a NF and returns the same.
----------------------------------------------------------------------------------
FUNCTION CHECK_DISCREPANCY(O_error_message  IN OUT VARCHAR2,
                           I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                           O_Cost_Disp      IN OUT VARCHAR2,
                           O_Qty_Disp       IN OUT VARCHAR2,
                           O_Tax_Disp       IN OUT VARCHAR2
                           )
   return BOOLEAN ;
----------------------------------------------------------------------------------
-- Function Name: RESET_WORKSHEET
-- Purpose:       This function resets all the discrepancy flags,apportioned values and deletes the  resolution action , Tax coming from external systems.
----------------------------------------------------------------------------------
FUNCTION RESET_WORKSHEET(O_error_message IN OUT VARCHAR2,
                         I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN ;
----------------------------------------------------------------------------------
-- Function Name: LOCK_FISCAL_HEADER
-- Purpose:       This function tries to acquire lock on fiscal header Form , when it is open in Edit mode.
-----------------------------------------------------------------------------------
FUNCTION LOCK_FISCAL_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_id   IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: UPD_SCHED_STATUS
-- Purpose:       This function sets the schedule status, based on the various NF status under that schedule.
-----------------------------------------------------------------------------------
FUNCTION UPD_SCHED_STATUS(O_error_message IN OUT VARCHAR2,
                          I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: UPD_SCHED_STATUS_RMA
-- Purpose:       This function sets the schedule status for rma, based on the various NF status under that schedule.
-----------------------------------------------------------------------------------
FUNCTION UPD_SCHED_STATUS_RMA(O_error_message IN OUT VARCHAR2,
                              I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: UPD_COMP_NF_STATUS
-- Purpose:       This function updates the status of comp NF/Main NF as the main NF/comp NF.
-----------------------------------------------------------------------------------
FUNCTION UPD_COMP_NF_STATUS(O_error_message IN OUT VARCHAR2,
                            O_fiscal_doc_id IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_status        IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
      return BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION UPDATE_RMS_ERR_STATUS(O_error_message  IN OUT VARCHAR2,
                               I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                               I_status         IN     FM_FISCAL_DOC_HEADER.RMS_UPD_ERR_IND%TYPE DEFAULT 'N')
   return BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: DEL_FISCAL_DOC_TAX_DETAIL
-- Purpose:       This function deletes the records of fiscal doc tax detail based on fiscal doc detail.
---------------------------------------------------------------------------------------
FUNCTION DEL_FISCAL_DOC_TAX_DETAIL(O_error_message       IN OUT VARCHAR2,
                                   I_fiscal_doc_line_id  IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
      return BOOLEAN;
---------------------------------------------------------------------------------------
END FM_FISCAL_DOC_HEADER_SQL;
/