CREATE OR REPLACE PACKAGE BODY FM_RTV_SQL IS
--------------------------------------------------------------------------------
FUNCTION PARSE_RTVDTL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_seq_no          IN       NUMBER,               
                      I_rtvDtl_tbl      IN       "RIB_RTVDtl_TBL")     
RETURN BOOLEAN IS                                                   

    L_program       VARCHAR2(61) := 'FM_RTV_SQL.PARSE_RTVDTL';
    L_rtvDtl_rec    "RIB_RTVDtl_REC" := NULL;

BEGIN

    for i in I_rtvDtl_tbl.FIRST..I_rtvDtl_tbl.LAST loop

        L_rtvDtl_rec := I_rtvDtl_tbl(i);

            insert into fm_stg_rtv_dtl (seq_no,
                                        desc_seq_no,
                                        item_id,
                                        unit_qty,
                                        container_qty,
                                        from_disposition,
                                        to_disposition,
                                        ebc_cost,
                                        reason,
                                        weight,
                                        weight_uom,
                                        nic_cost,
                                        create_datetime,
                                        create_id,
                                        last_update_datetime,
                                        last_update_id)
                                values (fm_stg_rtv_dtl_seq.nextval,
                                        I_seq_no,
                                        L_rtvDtl_rec.item_id,
                                        L_rtvDtl_rec.unit_qty,
                                        L_rtvDtl_rec.container_qty,
                                        L_rtvDtl_rec.from_disposition,
                                        L_rtvDtl_rec.to_disposition,
                                        L_rtvDtl_rec.unit_cost,
                                        L_rtvDtl_rec.reason,
                                        L_rtvDtl_rec.weight,
                                        L_rtvDtl_rec.weight_uom,
                                        L_rtvDtl_rec.gross_cost,
                                        sysdate,
                                        user,
                                        sysdate,
                                        user);
    end loop;

    return TRUE;

EXCEPTION
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
        return FALSE;
END PARSE_RTVDTL;
--------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,           
                 O_seq_no          IN OUT  FM_STG_RTV_DESC.SEQ_NO%TYPE,        
                 I_message         IN      RIB_OBJECT,                         
                 I_message_type    IN      VARCHAR2)                           
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'FM_RTV_SQL.CONSUME';

   L_rtvDesc_rec         "RIB_RTVDesc_REC" := NULL;
   L_batch_running_ind   RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE;
   L_seq_no              FM_STG_RTV_DESC.SEQ_NO%TYPE := NULL;
   L_status_code         VARCHAR2(1) := NULL;
 
   cursor C_SEQ_NO is                                
      select fm_stg_rtv_desc_seq.nextval             
        from dual;

BEGIN
    ---
    L_rtvDesc_rec := treat(I_message as "RIB_RTVDesc_REC");
    ---
    if RMS_BATCH_STATUS_SQL.GET_BATCH_RUNNING_IND(O_error_message,
                                                  L_batch_running_ind) = FALSE then
       return FALSE;
    end if;
    ---
    if L_rtvDesc_rec.status_ind = 'A' then
       -- Call procedure consume for Approved RTV during the day
       if L_batch_running_ind = 'N' then
          RMSSUB_RTV.CONSUME_RTV(L_status_code,
                                 O_error_message,
                                 L_rtvDesc_rec,
                                 I_message_type,
                                 'N');
          if L_status_code != API_CODES.SUCCESS then
             return FALSE;
          else
             -- Approved RTVs do NOT go through any RFM processing
             return TRUE;
          end if;
       end if;
    end if;
   
    if NVL(L_rtvDesc_rec.status_ind, 'S') = 'S' or                          --'S'hipped RTV, NULL status is treated as 'S'hipped
       L_rtvDesc_rec.status_ind = 'A' and L_batch_running_ind = 'Y' then    --'A'pproved RTV, night run
       -- perform basic validation of the message before staging
       if FM_STG_VALIDATION_SQL.PROCESS_VALIDATION(O_error_message,
                                                   I_message,
                                                   I_message_type,
                                                   'RTV') = FALSE then  -- requisition_type
          return FALSE;
       end if;
       ---
       open C_SEQ_NO;                          
       fetch C_SEQ_NO into L_seq_no;
       close C_SEQ_NO;

       insert into fm_stg_rtv_desc (seq_no,
                                    dc_dest_id,
                                    rtv_id,
                                    rtn_auth_nbr,
                                    vendor_nbr,
                                    ship_address1,
                                    ship_address2,
                                    ship_address3,
                                    state,
                                    city,
                                    shipto_zip,
                                    country,
                                    creation_ts,
                                    comments,
                                    rtv_order_no,
                                    status,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                            values (L_seq_no,
                                    L_rtvDesc_rec.dc_dest_id,
                                    L_rtvDesc_rec.rtv_id,
                                    L_rtvDesc_rec.rtn_auth_nbr,
                                    L_rtvDesc_rec.vendor_nbr,
                                    L_rtvDesc_rec.ship_address1,
                                    L_rtvDesc_rec.ship_address2,
                                    L_rtvDesc_rec.ship_address3,
                                    L_rtvDesc_rec.state,
                                    L_rtvDesc_rec.city,
                                    L_rtvDesc_rec.shipto_zip,
                                    L_rtvDesc_rec.country,
                                    L_rtvDesc_rec.creation_ts,
                                    L_rtvDesc_rec.comments,
                                    L_rtvDesc_rec.rtv_order_no,
                                    --Write stg table with a status of 'U'nprocessed for 'S'hipped RTV. Treat NULL status_ind on msg as 'S'hipped RTV.
                                    --Write stg table with a status of 'A' for 'A'pproved RTV.
                                    decode(NVL(L_rtvDesc_rec.status_ind, 'S'), 'S', 'U', 'A'),
                                    sysdate,
                                    user,
                                    sysdate,
                                    user);

       if PARSE_RTVDTL(O_error_message, 
                       L_seq_no, 
                       L_rtvDesc_rec.RTVDtl_TBL) = FALSE then
          return FALSE;
       end if;

       -- only return O_seq_no for 'S'hipped RTVs
       if NVL(L_rtvDesc_rec.status_ind, 'S') = 'S' then 
          O_seq_no := L_seq_no;
       end if;
    end if;

    return TRUE;

EXCEPTION
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'FM_RTV_SQL.CONSUME',
                                               to_char(SQLCODE));
        return FALSE;
END CONSUME;
--------------------------------------------------------------------------------
END FM_RTV_SQL;
/