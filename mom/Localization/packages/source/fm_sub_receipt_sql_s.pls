CREATE OR REPLACE PACKAGE FM_SUB_RECEIPT_SQL AS
--------------------------------------------------------------------------------
-- DESCRIPTION  This function will be used for receiving in RFM.
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION CONSUME (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_schedule_no     IN OUT   FM_SCHEDULE.SCHEDULE_NO%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END FM_SUB_RECEIPT_SQL;
/
