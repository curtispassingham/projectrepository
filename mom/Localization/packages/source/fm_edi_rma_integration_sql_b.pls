CREATE OR REPLACE PACKAGE BODY FM_EDI_RMA_INTEGRATION_SQL is

C_CHAR_TYPE        CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE := 'CHAR';
C_RMA_UTIL_ID      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_RMA_UTIL_ID';
C_TYPE_ID          CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_RMA_DOC_TYPE';
C_INITIAL_STATUS   CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'W'; --Worksheet
C_RMA_ID           CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'RMA';
C_MODULE           CONSTANT FM_FISCAL_DOC_HEADER.MODULE%TYPE := 'CUST';
C_KEY_VALUE_2      CONSTANT FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE := 'C';
C_FREIGHT_TYPE     CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_FREIGHT_TYPE';
C_NUMBER_TYPE      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE := 'NUMBER';
--------------------------------------------------------------------------------
FUNCTION PROCESS_EDI_HEADER(O_error_message IN OUT VARCHAR2,
                            O_fiscal_doc_no IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                            I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_EDI_HEADER';
   ---
   cursor C_GET_EDI_HEADER is
      select I_fiscal_doc_id fiscal_doc_id
           , location_id
           , location_type
           , fiscal_doc_no
           , series_no
           , subseries_no
           , 'W' status
           , type_id
           , requisition_type
           , module
           , key_value_1
           , key_value_2
           , issue_date
           , entry_or_exit_date
           , exit_hour
           , partner_type
           , partner_id
           , quantity
           , unit_type
           , freight_type
           , net_weight
           , total_weight
           , total_serv_value
           , total_item_value
           , total_doc_value
           , freight_cost
           , insurance_cost
           , other_expenses_cost
           , discount_type
           , total_discount_value
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
           , process_status
           , import_date
           , process_date
           , fiscal_doc_id fiscal_doc_id_ref
           , attribute1
           , attribute2
           , attribute3
           , attribute4
           , attribute5
           , attribute6
           , attribute7
           , attribute8
           , attribute9
           , attribute10
           , attribute11
           , attribute12
           , attribute13
           , attribute14
           , attribute15
           , utilization_id
           , nf_cfop
        from fm_edi_doc_header h
       where h.edi_doc_id = I_edi_doc_id;

   L_edi_header C_GET_EDI_HEADER%ROWTYPE;
   ---

   L_dummy_desc       FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date       FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_dummy_number     FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;

   ---
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_EDI_HEADER','FM_EDI_DOC_HEADER','EDI doc id: '||I_edi_doc_id);
   open C_GET_EDI_HEADER;
   SQL_LIB.SET_MARK('FETCH','C_GET_EDI_HEADER','FM_EDI_DOC_HEADER','EDI doc id: '||I_edi_doc_id);
   fetch C_GET_EDI_HEADER into L_edi_header;
   ---
   if L_edi_header.partner_type is NULL or L_edi_header.partner_id is NULL then
      -- Get the Default Partner Type
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_edi_header.partner_type,
                                            L_dummy_date,
                                            'CHAR',
                                            'EDI_DEF_PTNR_TYPE') = FALSE then
         return FALSE;
      end if;
      -- Get the Default Partner ID
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_edi_header.partner_id,
                                            L_dummy_date,
                                            'CHAR',
                                            'EDI_DEF_PTNR_ID') = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_fiscal_doc_no := L_edi_header.fiscal_doc_no;
   ---
   insert into fm_fiscal_doc_header( fiscal_doc_id
                                   , location_id
                                   , location_type
                                   , fiscal_doc_no
                                   , series_no
                                   , subseries_no
                                   , schedule_no
                                   , status
                                   , type_id
                                   , requisition_type
                                   , module
                                   , key_value_1
                                   , key_value_2
                                   , issue_date
                                   , entry_or_exit_date
                                   , exit_hour
                                   , partner_type
                                   , partner_id
                                   , quantity
                                   , unit_type
                                   , freight_type
                                   , net_weight
                                   , total_weight
                                   , total_serv_value
                                   , total_serv_calc_value
                                   , total_item_value
                                   , total_item_calc_value
                                   , total_doc_value
                                   , total_doc_calc_value
                                   , freight_cost
                                   , insurance_cost
                                   , other_expenses_cost
                                   , extra_costs_calc
                                   , discount_type
                                   , total_discount_value
                                   , total_doc_value_with_disc
                                   , create_datetime
                                   , create_id
                                   , last_update_datetime
                                   , last_update_id
                                   , utilization_id
                                   , nf_cfop
                                   , edi_nf_ind)
                              values
                                   ( L_edi_header.fiscal_doc_id
                                   , L_edi_header.location_id
                                   , L_edi_header.location_type
                                   , L_edi_header.fiscal_doc_no
                                   , L_edi_header.series_no
                                   , L_edi_header.subseries_no
                                   , NULL
                                   , L_edi_header.status
                                   , L_edi_header.type_id
                                   , L_edi_header.requisition_type
                                   , L_edi_header.module
                                   , L_edi_header.key_value_1
                                   , L_edi_header.key_value_2
                                   , L_edi_header.issue_date
                                   , L_edi_header.entry_or_exit_date
                                   , L_edi_header.exit_hour
                                   , L_edi_header.partner_type
                                   , L_edi_header.partner_id
                                   , L_edi_header.quantity
                                   , L_edi_header.unit_type
                                   , L_edi_header.freight_type
                                   , L_edi_header.net_weight
                                   , L_edi_header.total_weight
                                   , L_edi_header.total_serv_value
                                   , NULL --total_serv_calc_value
                                   , L_edi_header.total_item_value
                                   , NULL --total_item_calc_value
                                   , L_edi_header.total_doc_value
                                   , NULL --total_doc_calc_value
                                   , L_edi_header.freight_cost
                                   , L_edi_header.insurance_cost
                                   , L_edi_header.other_expenses_cost
                                   , NULL -- extra_costs_calc
                                   , L_edi_header.discount_type
                                   , L_edi_header.total_discount_value
                                   , NULL -- total_doc_value_with_disc
                                   , L_edi_header.create_datetime
                                   , L_edi_header.create_id
                                   , L_edi_header.last_update_datetime
                                   , L_edi_header.last_update_id
                                   , L_edi_header.utilization_id
                                   , L_edi_header.nf_cfop
                                   , 'Y');
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_EDI_HEADER','FM_EDI_DOC_HEADER','EDI doc id: '||I_edi_doc_id);
   close C_GET_EDI_HEADER;

   return(true);
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_GET_EDI_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_GET_EDI_HEADER','FM_EDI_DOC_HEADER','EDI doc id: '||I_edi_doc_id);
         close C_GET_EDI_HEADER;
      end if;
      
      return FALSE;
END PROCESS_EDI_HEADER;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_EDI_DETAILS(O_error_message IN OUT VARCHAR2,
                            I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_EDI_DETAILS';
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_DETAIL';
   L_key          VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   ---
   cursor C_GET_EDI_DETAILS is
      select fm_fiscal_doc_line_id_seq.nextval fiscal_doc_line_id
           , I_fiscal_doc_id fiscal_doc_id
           , location_id
           , location_type
           , line_no
           , requisition_no
           , item
           , classification_id
           , quantity
           , unit_cost
           , total_cost
           , freight_cost
           , net_cost
           , discount_type
           , discount_value
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
           , edi_doc_line_id
           , pack_ind
           , pack_no
           , recoverable_base
           , recoverable_value
           , icms_cst
           , pis_cst
           , cofins_cst
           , nf_cfop
        from fm_edi_doc_detail d
       where d.edi_doc_id = I_edi_doc_id;

   TYPE SEL_EDI_DETAIL_TYPE IS TABLE OF C_GET_EDI_DETAILS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_edi_detail SEL_EDI_DETAIL_TYPE;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_DETAILS', 'FM_EDI_DOC_DETAIL', I_edi_doc_id);
   open C_GET_EDI_DETAILS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_DETAILS', 'FM_EDI_DOC_DETAIL', I_edi_doc_id);
   fetch C_GET_EDI_DETAILS BULK COLLECT into L_edi_detail;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_DETAILS', 'FM_EDI_DOC_DETAIL', I_edi_doc_id);
   close C_GET_EDI_DETAILS;
   ---

   SQL_LIB.SET_MARK('INSERT', 'NULL', 'FM_FISCAL_DOC_DETAIL', NULL);
   FORALL i IN L_edi_detail.FIRST..L_edi_detail.LAST
      insert into fm_fiscal_doc_detail( fiscal_doc_line_id
                                      , fiscal_doc_id
                                      , location_id
                                      , location_type
                                      , line_no
                                      , requisition_no
                                      , item
                                      , classification_id
                                      , quantity
                                      , unit_cost
                                      , total_cost
                                      , total_calc_cost
                                      , freight_cost
                                      , net_cost
                                      , discount_type
                                      , discount_value
                                      , create_datetime
                                      , create_id
                                      , last_update_datetime
                                      , last_update_id
                                      , pack_ind
                                      , pack_no
                                      , recoverable_base
                                      , recoverable_value
                                      , icms_cst
                                      , pis_cst
                                      , cofins_cst
                                      , nf_cfop
                                      , appt_qty)
                               values
                                      ( L_edi_detail(i).fiscal_doc_line_id
                                      , L_edi_detail(i).fiscal_doc_id
                                      , L_edi_detail(i).location_id
                                      , L_edi_detail(i).location_type
                                      , L_edi_detail(i).line_no
                                      , L_edi_detail(i).requisition_no
                                      , L_edi_detail(i).item
                                      , L_edi_detail(i).classification_id
                                      , L_edi_detail(i).quantity
                                      , L_edi_detail(i).unit_cost
                                      , L_edi_detail(i).total_cost
                                      , NULL                                --total_calc_cost
                                      , L_edi_detail(i).freight_cost
                                      , L_edi_detail(i).net_cost
                                      , L_edi_detail(i).discount_type
                                      , L_edi_detail(i).discount_value
                                      , L_edi_detail(i).create_datetime
                                      , L_edi_detail(i).create_id
                                      , L_edi_detail(i).last_update_datetime
                                      , L_edi_detail(i).last_update_id
                                      , NVL(L_edi_detail(i).pack_ind,'N')
                                      , L_edi_detail(i).pack_no
                                      , L_edi_detail(i).recoverable_base
                                      , L_edi_detail(i).recoverable_value
                                      , L_edi_detail(i).icms_cst
                                      , L_edi_detail(i).pis_cst
                                      , L_edi_detail(i).cofins_cst
                                      , L_edi_detail(i).nf_cfop
                                      , L_edi_detail(i).quantity);

   --- This will update the items on NF as unexpected items,
   --- which are not associated to any requisition number.
      
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);

   update fm_fiscal_doc_detail
      set unexpected_item = 'Y'
    where fiscal_doc_id = I_fiscal_doc_id
      and requisition_no is null;

   --- This will update the nf_cfop on fm_fiscal_doc_detail table with nf_cfop
   --- from fm_fiscal_doc_header table where nf_cfop is null in detail table.
      
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
   update fm_fiscal_doc_detail
      set nf_cfop = (select nvl(nf_cfop,-1)
                       from fm_fiscal_doc_header
                      where fiscal_doc_id = I_fiscal_doc_id)
    where fiscal_doc_id = I_fiscal_doc_id
      and nf_cfop is null;

   return(true);
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_GET_EDI_DETAILS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_DETAILS', 'FM_EDI_DOC_DETAIL', I_edi_doc_id);
         close C_GET_EDI_DETAILS;
      end if;
      
      return FALSE;
END PROCESS_EDI_DETAILS;
--------------------------------------------------------------------------------
FUNCTION PROCESS_RMA_HEADER(O_error_message IN OUT VARCHAR2,
                            I_schedule_no   IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                            O_fiscal_doc_no IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                            I_rma_id        IN     FM_RMA_HEAD.RMA_ID%TYPE,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   L_program          VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_RMA_HEADER';
   L_utilization      FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE;
   L_type_id          FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE;
   L_freight_type     FM_FISCAL_DOC_HEADER.FREIGHT_TYPE%TYPE;
   L_dummy_desc       FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date       FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_dummy_number     FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_dummy_string     FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_create_datetime  FM_EDI_DOC_HEADER.CREATE_DATETIME%TYPE := SYSDATE;
   L_create_id        FM_EDI_DOC_HEADER.CREATE_ID%TYPE := USER;
   L_fiscal_doc_no    FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE;
   L_series_no        FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE;
   L_subseries_no     FM_FISCAL_DOC_HEADER.SUBSERIES_NO%TYPE;
   L_exists           BOOLEAN;
   ---
   cursor C_GET_RMA_HEADER is
      select I_fiscal_doc_id fiscal_doc_id
           , h.location
           , h.loc_type
           , SUM(d.qty) total_qty
           , h.cust_id
           , h.rma_date
        from fm_rma_head h,
             fm_rma_detail d
       where h.rma_id = I_rma_id
         and h.rma_id = d.rma_id
    group by fiscal_doc_id
           , h.location
           , h.loc_type
           , h.cust_id
           , h.rma_date;
   R_header C_GET_RMA_HEADER%ROWTYPE;
   ---

BEGIN
   open C_GET_RMA_HEADER;
   fetch C_GET_RMA_HEADER into R_header;
   ---
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_dummy_desc,
                                         L_dummy_number,
                                         L_utilization,
                                         L_dummy_date,
                                         C_CHAR_TYPE,
                                         C_RMA_UTIL_ID) = FALSE then
      return FALSE;
   end if;
   ---Get the default freight type
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_dummy_desc,
                                         L_dummy_number,
                                         L_freight_type,
                                         L_dummy_date,
                                         C_CHAR_TYPE,
                                         C_FREIGHT_TYPE) = FALSE then
      return FALSE;
   end if;

   --Get the default document type
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_dummy_desc,
                                         L_type_id,
                                         L_dummy_string,
                                         L_dummy_date,
                                         C_NUMBER_TYPE,
                                         C_TYPE_ID) = FALSE then
      return FALSE;
   end if;
   -- 
   if FM_FISCAL_DOC_TYPE_SQL.EXISTS(O_error_message,
                                    L_exists,
                                    L_dummy_desc,
                                    L_type_id) = FALSE then
      return FALSE;
   end if;       
   ---
   if NOT L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DOC_TYPE',SQLERRM,L_program,NULL);
      return FALSE; 
   end if;
   -- 
   if FM_DOC_TYPE_UTILIZATION_SQL.EXISTS_UTILIZATION(O_error_message,
                                                     L_exists,
                                                     L_type_id,
                                                     L_utilization) = FALSE then
      return FALSE;
   end if;
   --
   if NOT L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_UTIL_DOC_TYPE',SQLERRM,L_program,NULL);
      return FALSE; 
   end if;
   --
   if FM_FISCAL_UTILIZATION_SQL.EXISTS_UTILIZATION_REQ_TYPE(O_error_message,
                                                            L_exists,
                                                            C_RMA_ID,
                                                            L_utilization) = FALSE then
      return FALSE;
   end if;
     
   if NOT L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_UTIL',SQLERRM,L_program,NULL);
      return FALSE; 
   end if;
   ---
   if (fm_loc_fiscal_number_sql.next_number(O_error_message,
                                            L_fiscal_doc_no,
                                            L_series_no,
                                            L_subseries_no,
                                            R_header.loc_type,
                                            R_header.location,
                                            L_create_datetime,
                                            L_type_id) = FALSE) then
      return FALSE;
   end if;

   insert into fm_fiscal_doc_header(fiscal_doc_id,
                                    location_id,
                                    location_type,
                                    fiscal_doc_no,
                                    series_no,
                                    subseries_no,
                                    schedule_no,
                                    status,
                                    type_id,
                                    utilization_id,
                                    requisition_type,
                                    module,
                                    key_value_1,
                                    key_value_2,
                                    issue_date,
                                    entry_or_exit_date,
                                    exit_hour,
                                    partner_type,
                                    partner_id,
                                    quantity,
                                    unit_type,
                                    freight_type,
                                    net_weight,
                                    total_weight,
                                    total_serv_value,
                                    total_item_value,
                                    total_doc_value,
                                    freight_cost,
                                    insurance_cost,
                                    other_expenses_cost,
                                    discount_type,
                                    total_discount_value,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                            values( R_header.fiscal_doc_id,
                                    R_header.location,   -- location_id
                                    R_header.loc_type,             -- location_type
                                    L_fiscal_doc_no,                   -- fiscal_doc_no
                                    L_series_no,                   -- series_no
                                    L_subseries_no,                   -- subseries_no
                                    I_schedule_no,                   -- schedule_no
                                    C_INITIAL_STATUS,       -- status
                                    L_type_id,              -- type_id (system_options)
                                    L_utilization,         -- utilization_id
                                    C_RMA_ID,               -- requisition_type
                                    C_MODULE,               -- module
                                    R_header.cust_id,       -- key_value_1
                                    C_KEY_VALUE_2,          -- key_value_2
                                    L_create_datetime,      -- issue_date
                                    NULL,      -- entry_or_exit_date
                                    NULL,                   -- exit_hour
                                    NULL,                   -- partner_type
                                    NULL,                   -- partner_id
                                    R_header.total_qty,    -- quantity
                                    null,   -- unit_type
                                    L_freight_type,         -- freight_type (system_options)
                                    NULL,                   -- net_weight
                                    null, -- total_weight
                                    0,                      -- total_serv_value
                                    0,                      -- total_item_value
                                    0,                      -- total_doc_value
                                    0,                      -- freight_cost
                                    0,                      -- insurance_cost
                                    0,                      -- other_expenses_cost
                                    NULL,                   -- discount_type
                                    NULL,                   -- total_discount_value
                                    L_create_datetime,      -- create_datetime
                                    L_create_id,            -- create_id
                                    L_create_datetime,      -- last_update_datetime
                                    L_create_id);
   ---
   close C_GET_RMA_HEADER;

   return(true);
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END PROCESS_RMA_HEADER;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_RMA_DETAILS(O_error_message IN OUT VARCHAR2,
                            I_rma_id         IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                            I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                            I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_DETAILS';
   L_classif_id       FM_FISCAL_DOC_DETAIL.CLASSIFICATION_ID%TYPE;
   L_classif_desc     V_FISCAL_CLASSIFICATION.classification_desc%TYPE;
   L_create_datetime  FM_FISCAL_DOC_HEADER.CREATE_DATETIME%TYPE := SYSDATE;
   L_create_id        FM_FISCAL_DOC_HEADER.CREATE_ID%TYPE := USER;
   L_new_status       VARCHAR2(1);
   L_line_no          FM_FISCAL_DOC_DETAIL.LINE_NO%TYPE;
     -- L_pack_ind         FM_FISCAL_DOC_HEADER.PACK_IND%TYPE;
     -- L_pack_no          FM_FISCAL_DOC_HEADER.PACK_NO%TYPE;
      ---
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL';
   L_key          VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   L_doc_value    FM_FISCAL_DOC_HEADER.TOTAL_DOC_VALUE%TYPE := NULL;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_CHECK_PACK_IND(P_item FM_RMA_DETAIL.ITEM%TYPE) is 
      select pack_ind
        from item_master
       where item = P_item;
         
    cursor C_GET_RMA_DETAILS is
        select item
             , qty
             , unit_cost
             , qty * unit_cost total_cost
             , origin_document
             , 'N' pack_ind
             , null pack_no
        from fm_rma_detail dtl
       where dtl.rma_id = I_rma_id
         and NOT EXISTS (select 0
                           from item_master im
                          where im.item = dtl.item
                            and im.pack_ind = 'Y')
      UNION ALL
       select pi.item item
            , (pi.pack_qty * frd.qty) qty
            , ((il.unit_retail * frd.unit_cost)/temp.total_retail) unit_cost
            , ((il.unit_retail * frd.unit_cost)/temp.total_retail) * pi.pack_qty * frd.qty total_cost
            , frd.origin_document
            , 'N' pack_ind
            , frd.item pack_no
       from  fm_rma_head frh
           , fm_rma_detail frd
           , packitem pi
           , item_master im
           , item_loc il
           , (select sum(il.unit_retail * pi.pack_qty) total_retail
                from item_loc il,packitem pi,fm_rma_head frh, fm_rma_Detail frd 
               where il.item    = pi.item
                 and pack_no    = frd.item 
                 and frd.rma_id = frh.rma_id 
                 and frd.rma_id = I_rma_id 
                 and il.loc in (select wh 
                                  from (select distinct primary_vwh wh from wh wh,
                                                                            fm_rma_head frh,
                                                                            fm_rma_detail frd,
                                                                            item_master im,
                                                                            item_loc il,
                                                                            packitem pi
                                                                      where wh = frh.location
                                                                        and frh.rma_id  = I_rma_id
                                                                        and frh.rma_id  = frd.rma_id 
                                                                        and im.item     = frd.item
                                                                        and im.pack_ind = 'Y'
                                                                        and il.item     = pi.item
                                                                        and pi.pack_no  = frd.item
                                                                        and il.loc      = wh.primary_vwh
                                        union all
                                         select loc 
                                 from (select distinct loc 
                                         from item_loc il,
                                              fm_rma_head frh,
                                              fm_rma_detail frd,
                                              item_master im,
                                              packitem pi 
                                        where loc in (select wh from wh,
                                                                     fm_rma_head frh 
                                                               where wh.physical_wh = frh.location 
                                                                 and frh.rma_id     = I_rma_id) 
                                        and frh.rma_id = frd.rma_id 
                                        and frd.item   = il.item 
                                        and pi.pack_no = frd.item
                                        and im.item    = frd.item
                                        and im.pack_ind = 'Y'
                                        order by loc) 
                                 where ROWNUM < 2)
                                          where rownum<2)) temp
         where frh.rma_status = 'U'
           and frh.rma_id = I_rma_id
           and frh.rma_id = frd.rma_id
           and pi.pack_no = frd.item
           and im.item    = frd.item
           and im.pack_ind = 'Y'
           and il.item = pi.item
           and il.loc in (select wh 
                                  from (select distinct primary_vwh wh from wh wh,
                                                                            fm_rma_head frh,
                                                                            fm_rma_detail frd,
                                                                            item_master im,
                                                                            item_loc il,
                                                                            packitem pi
                                                                      where wh = frh.location
                                                                        and frh.rma_id  = I_rma_id
                                                                        and frh.rma_id  = frd.rma_id 
                                                                        and im.item     = frd.item
                                                                        and im.pack_ind = 'Y'
                                                                        and il.item     = pi.item
                                                                        and pi.pack_no  = frd.item
                                                                        and il.loc      = wh.primary_vwh
                                        union all
                                        select loc 
                                 from (select distinct loc 
                                         from item_loc il,
                                              fm_rma_head frh,
                                              fm_rma_detail frd,
                                              item_master im,
                                              packitem pi 
                                        where loc in (select wh from wh,
                                                                     fm_rma_head frh 
                                                               where wh.physical_wh = frh.location 
                                                                 and frh.rma_id     = I_rma_id) 
                                        and frh.rma_id = frd.rma_id 
                                        and frd.item   = il.item 
                                        and pi.pack_no = frd.item
                                        and im.item    = frd.item
                                        and im.pack_ind = 'Y'
                                        order by loc) 
                                 where ROWNUM < 2)
                          where rownum<2);
                               
   R_detail C_GET_RMA_DETAILS%ROWTYPE;
   
   cursor C_GET_DOC_VALUE is
      select sum(fdd.total_cost)
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.fiscal_doc_id = I_fiscal_doc_id;

   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
         
   BEGIN
      
      open C_GET_RMA_DETAILS; 
      LOOP
       fetch C_GET_RMA_DETAILS into R_detail;
       EXIT when C_GET_RMA_DETAILS%NOTFOUND;
       ---
       if FM_FISCAL_DETAIL_VAL_SQL.GET_ITEM_CLASSIFICATION (O_error_message,
                                                            L_classif_id,
                                                            L_classif_desc,
                                                            R_detail.item,
                                                            I_location_id,
                                                            I_location_id,
                                                            I_location_type,
                                                            L_create_datetime) = FALSE then
           ---
          L_new_status := C_ERROR_STATUS;
       end if;
            ---
       if L_classif_id is NULL then
        O_error_message := SQL_LIB.CREATE_MSG('ORFM_CLASS_FISC_REQ',
                                               NULL,
                                               NULL,
                                               NULL);
         ---
          L_new_status := C_ERROR_STATUS;
       end if;
       if FM_FISCAL_DOC_DETAIL_SQL.GET_NEXT_LINE_NO(O_error_message,
                                                    L_line_no,
                                                    I_fiscal_doc_id) = FALSE then
           ---
          L_new_status := C_ERROR_STATUS;
       end if;
      insert into fm_fiscal_doc_detail(fiscal_doc_line_id,
                   fiscal_doc_id,
                   location_id,
                   location_type,
                   line_no,
                   requisition_no,
                   item,
                   classification_id,
                   quantity,
                   unit_cost,
                   total_cost,
                   freight_cost,
                   net_cost,
                   fiscal_doc_line_id_ref,
                   fiscal_doc_id_ref,
                   pack_ind,
                   pack_no,
                   create_datetime,
                   create_id,
                   last_update_datetime,
                   last_update_id)
                  values
                  ( fm_fiscal_doc_line_id_seq.nextval,
                   I_fiscal_doc_id,
                   I_location_id,            -- location_id
                   I_location_type,          -- location_type
                   L_line_no,                -- line_no
                   I_rma_id,                 -- requisition_no
                   R_detail.item,            -- item
                   L_classif_id,             -- classification_id
                   R_detail.qty,             -- quantity
                   R_detail.unit_cost,       -- unit_cost (It will be populated during the fiscal document creation)
                   R_detail.total_cost,
                   0,                        -- freight_cost
                   NULL,                     -- net_cost
                   NULL,                     -- fiscal_doc_line_id_ref
                   R_detail.origin_document, -- fiscal_doc_id_ref
                   R_detail.pack_ind,        -- pack inicator
                   R_detail.pack_no,         -- pack number
                   L_create_datetime,        -- create_datetime
                   L_create_id,              -- create_id
                   L_create_datetime,        -- last_update_datetime
                   L_create_id);             -- last_update_id

         ---
      END LOOP;
      CLOSE C_GET_RMA_DETAILS;

    open C_GET_DOC_VALUE;
   fetch C_GET_DOC_VALUE into L_doc_value;
   close C_GET_DOC_VALUE;

   L_cursor := 'C_LOCK_DOC_HEADER';
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_LOCK_DOC_HEADER;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_LOCK_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);

   update fm_fiscal_doc_header
      set total_item_value = L_doc_value
         ,total_doc_value  = L_doc_value
    where fiscal_doc_id    = I_fiscal_doc_id;

      return(true);

   EXCEPTION
      when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', L_table, L_key);
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
         return FALSE;
END PROCESS_RMA_DETAILS;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_PAYMENTS(O_error_message IN OUT VARCHAR2,
                             I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                             I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_PAYMENTS';
   ---
   cursor C_GET_EDI_PAYMENTS is
      select I_fiscal_doc_id fiscal_doc_id
           , payment_date
           , value
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
        from fm_edi_doc_payment p
       where p.edi_doc_id = I_edi_doc_id;
          
   TYPE SEL_EDI_PAYMENT_TYPE IS TABLE OF C_GET_EDI_PAYMENTS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_edi_payment SEL_EDI_PAYMENT_TYPE;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_PAYMENTS', 'FM_EDI_DOC_PAYMENT', I_edi_doc_id);
   open C_GET_EDI_PAYMENTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_PAYMENTS', 'FM_EDI_DOC_PAYMENT', I_edi_doc_id);
   fetch C_GET_EDI_PAYMENTS BULK COLLECT into L_edi_payment;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_PAYMENTS', 'FM_EDI_DOC_PAYMENT', I_edi_doc_id);
   close C_GET_EDI_PAYMENTS;
   ---

   SQL_LIB.SET_MARK('INSERT', 'NULL', 'FM_FISCAL_DOC_PAYMENTS', NULL);
   FORALL i IN L_edi_payment.FIRST..L_edi_payment.LAST
      insert into fm_fiscal_doc_payments( fiscal_doc_id
                                        , payment_date
                                        , value
                                        , create_datetime
                                        , create_id
                                        , last_update_datetime
                                        , last_update_id)
                                  values( L_edi_payment(i).fiscal_doc_id
                                        , L_edi_payment(i).payment_date
                                        , L_edi_payment(i).value
                                        , L_edi_payment(i).create_datetime
                                        , L_edi_payment(i).create_id
                                        , L_edi_payment(i).last_update_datetime
                                        , L_edi_payment(i).last_update_id);

   return(true);
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_GET_EDI_PAYMENTS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_PAYMENTS', 'FM_EDI_DOC_PAYMENT', I_edi_doc_id);
         close C_GET_EDI_PAYMENTS;
      end if;
      
      return FALSE;
END PROCESS_PAYMENTS;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_COMPLEMENTS(O_error_message IN OUT VARCHAR2,
                             I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                             I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_COMPLEMENTS';
   L_compl_fiscal_doc_id  FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   ---
   cursor C_GET_EDI_COMPLEMENTS is
      select fedh.fiscal_doc_id compl_fiscal_doc_id
           , I_fiscal_doc_id fiscal_doc_id
           , fedc.create_datetime
           , fedc.create_id
           , fedc.last_update_datetime
           , fedc.last_update_id
        from fm_edi_doc_complement fedc,
             fm_edi_doc_header fedh
       where fedc.compl_edi_doc_id = fedh.edi_doc_id
         and fedc.edi_doc_id       = I_edi_doc_id;
     
   TYPE SEL_EDI_COMPL_TYPE IS TABLE OF C_GET_EDI_COMPLEMENTS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_edi_complement SEL_EDI_COMPL_TYPE;
      
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_COMPLEMENTS', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
   open C_GET_EDI_COMPLEMENTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_COMPLEMENTS', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
   fetch C_GET_EDI_COMPLEMENTS BULK COLLECT into L_edi_complement;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_COMPLEMENTS', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
   close C_GET_EDI_COMPLEMENTS;
   ---

   SQL_LIB.SET_MARK('INSERT', NULL , 'FM_FISCAL_DOC_COMPLEMENT', NULL);  
   FORALL i IN L_edi_complement.FIRST..L_edi_complement.LAST
      insert into fm_fiscal_doc_complement( compl_fiscal_doc_id
                                          , fiscal_doc_id
                                          , create_datetime
                                          , create_id
                                          , last_update_datetime
                                          , last_update_id)
                                    values( L_edi_complement(i).compl_fiscal_doc_id
                                          , L_edi_complement(i).fiscal_doc_id
                                          , L_edi_complement(i).create_datetime
                                          , L_edi_complement(i).create_id
                                          , L_edi_complement(i).last_update_datetime
                                          , L_edi_complement(i).last_update_id);
   return(true);
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_GET_EDI_COMPLEMENTS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_COMPLEMENTS', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
         close C_GET_EDI_COMPLEMENTS;
      end if;
      
      return FALSE;
END PROCESS_COMPLEMENTS;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_TAX_HEAD(O_error_message IN OUT VARCHAR2,
                             I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                             I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_TAX_HEAD';
   ---
   cursor C_GET_EDI_TAX_HEAD is
      select vat_code
           , I_fiscal_doc_id fiscal_doc_id
           , total_value
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
           , tax_basis
           , modified_tax_basis
        from fm_edi_doc_tax_head h
       where h.edi_doc_id = I_edi_doc_id;

   TYPE SEL_EDI_TAXHEAD_TYPE IS TABLE OF C_GET_EDI_TAX_HEAD%ROWTYPE INDEX BY BINARY_INTEGER;
   L_tax_head SEL_EDI_TAXHEAD_TYPE;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_TAX_HEAD', 'FM_EDI_DOC_TAX_HEAD', I_edi_doc_id);
   open C_GET_EDI_TAX_HEAD;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_TAX_HEAD', 'FM_EDI_DOC_TAX_HEAD', I_edi_doc_id);
   fetch C_GET_EDI_TAX_HEAD BULK COLLECT into L_tax_head;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_TAX_HEAD', 'FM_EDI_DOC_TAX_HEAD', I_edi_doc_id);
   close C_GET_EDI_TAX_HEAD;
   ---
   
   SQL_LIB.SET_MARK('INSERT', NULL, 'FM_FISCAL_DOC_TAX_HEAD', NULL);
   FORALL i IN L_tax_head.FIRST..L_tax_head.LAST
      insert into fm_fiscal_doc_tax_head( vat_code
                                        , fiscal_doc_id
                                        , total_value
                                        , tax_basis
                                        , modified_tax_basis
                                        , create_datetime
                                        , create_id
                                        , last_update_datetime
                                        , last_update_id)
                                  values( L_tax_head(i).vat_code
                                        , L_tax_head(i).fiscal_doc_id
                                        , L_tax_head(i).total_value
                                        , L_tax_head(i).tax_basis
                                        , L_tax_head(i).modified_tax_basis
                                        , L_tax_head(i).create_datetime
                                        , L_tax_head(i).create_id
                                        , L_tax_head(i).last_update_datetime
                                        , L_tax_head(i).last_update_id);
   return(true);
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_GET_EDI_TAX_HEAD%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_TAX_HEAD', 'FM_EDI_DOC_TAX_HEAD', I_edi_doc_id);
         close C_GET_EDI_TAX_HEAD;
      end if;
      
      return FALSE;
END PROCESS_TAX_HEAD;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_TAX_DETAIL(O_error_message IN OUT VARCHAR2,
                            I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_TAX_DETAIL';
   ---
   cursor C_GET_EDI_TAX_DETAIL is
      select edtd.vat_code
           , fdd.fiscal_doc_line_id
           , edtd.percentage_rate
           , edtd.total_value
           , edtd.create_datetime
           , edtd.create_id
           , edtd.last_update_datetime
           , edtd.last_update_id
           , edtd.tax_basis
           , edtd.modified_tax_basis
           , edtd.tax_base_ind
        from fm_fiscal_doc_detail fdd,
             fm_edi_doc_tax_detail edtd,
             fm_edi_doc_detail edd
       where fdd.fiscal_doc_id   = I_fiscal_doc_id
         and edd.item            = fdd.item
         and edd.location_id     = fdd.location_id
         and nvl(edd.requisition_no,-999)  = nvl(fdd.requisition_no,-999)
         and edd.edi_doc_line_id = edtd.edi_doc_line_id
         and edd.edi_doc_id      = I_edi_doc_id;
 
   TYPE SEL_EDI_TAXDTL_TYPE IS TABLE OF C_GET_EDI_TAX_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_tax_detail SEL_EDI_TAXDTL_TYPE;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_TAX_DETAIL', 'FM_EDI_DOC_TAX_DETAIL', I_edi_doc_id);
   open C_GET_EDI_TAX_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_TAX_DETAIL', 'FM_EDI_DOC_TAX_DETAIL', I_edi_doc_id);
   fetch C_GET_EDI_TAX_DETAIL BULK COLLECT into L_tax_detail;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_TAX_DETAIL', 'FM_EDI_DOC_TAX_DETAIL', I_edi_doc_id);
   close C_GET_EDI_TAX_DETAIL;
   ---
   
   SQL_LIB.SET_MARK('INSERT', NULL, 'FM_FISCAL_DOC_TAX_DETAIL', NULL);
   FORALL i IN L_tax_detail.FIRST..L_tax_detail.LAST
      insert into fm_fiscal_doc_tax_detail( vat_code
                                          , fiscal_doc_line_id
                                          , percentage_rate
                                          , total_value
                                          , tax_basis
                                          , modified_tax_basis
                                          , tax_base_ind
                                          , create_datetime
                                          , create_id
                                          , last_update_datetime
                                          , last_update_id)
                                    values( L_tax_detail(i).vat_code
                                          , L_tax_detail(i).fiscal_doc_line_id
                                          , L_tax_detail(i).percentage_rate
                                          , L_tax_detail(i).total_value
                                          , L_tax_detail(i).tax_basis
                                          , L_tax_detail(i).modified_tax_basis
                                          , L_tax_detail(i).tax_base_ind
                                          , L_tax_detail(i).create_datetime
                                          , L_tax_detail(i).create_id
                                          , L_tax_detail(i).last_update_datetime
                                          , L_tax_detail(i).last_update_id);
   return(true);
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_GET_EDI_TAX_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_TAX_DETAIL', 'FM_EDI_DOC_TAX_DETAIL', I_edi_doc_id);
         close C_GET_EDI_TAX_DETAIL;
      end if;
      
      return FALSE;
END PROCESS_TAX_DETAIL;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_EDI(O_error_message IN OUT VARCHAR2,
                     O_status        IN OUT BOOLEAN,
                     I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN is

   L_program         VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_EDI';
   L_status          integer;
   L_fiscal_doc_id    FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_new_status       VARCHAR2(1);
   L_fiscal_doc_no    FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE;
   L_series_no        FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE;
   L_subseries_no     FM_FISCAL_DOC_HEADER.SUBSERIES_NO%TYPE;

BEGIN

   -- get the next fiscal_doc_id sequence
   L_fiscal_doc_id := 0;
   if (FM_FISCAL_HEADER_VAL_SQL.GET_NEXT_FISCAL_DOC_ID(O_error_message, L_fiscal_doc_id) = FALSE) then
      return (FALSE);
   end if;
   -- insert doc header
   if (PROCESS_EDI_HEADER(O_error_message, L_fiscal_doc_no, I_edi_doc_id, L_fiscal_doc_id) = FALSE) then
      return (FALSE);
   end if;
   -- insert doc details
   if (PROCESS_EDI_DETAILS(O_error_message, I_edi_doc_id, L_fiscal_doc_id) = FALSE) then
      return (FALSE);
   end if;
   -- insert doc payments
   if (PROCESS_PAYMENTS(O_error_message, I_edi_doc_id, L_fiscal_doc_id) = FALSE) then
      return (FALSE);
   end if;
   -- insert doc complements
   if (PROCESS_COMPLEMENTS(O_error_message, I_edi_doc_id, L_fiscal_doc_id) = FALSE) then
      return (FALSE);
   end if;
   -- insert doc header taxes
   if (PROCESS_TAX_HEAD(O_error_message, I_edi_doc_id, L_fiscal_doc_id) = FALSE) then
      return (FALSE);
   end if;
   -- insert doc detail taxes
   if (PROCESS_TAX_DETAIL(O_error_message, I_edi_doc_id,L_fiscal_doc_id) = FALSE) then
      return (FALSE);
   end if;

   -- Updates the fiscal_doc_id field of the current processed record
   if FM_EDI_DOC_HEADER_SQL.UPDATE_FISCAL_DOC(O_error_message,
                                              I_edi_doc_id,
                                              L_fiscal_doc_id) = FALSE then
      return (FALSE);
   end if;

   return(true);

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END PROCESS_EDI;
--------------------------------------------------------------------------------
FUNCTION PROCESS_RMA(O_error_message IN OUT VARCHAR2,
                     O_status        IN OUT BOOLEAN,
                     I_rma_id        IN     FM_RMA_HEAD.RMA_ID%TYPE,
                     I_schedule_no   IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN is
      L_program         VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS_RMA';
      L_status          integer;
      L_fiscal_doc_id    FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
      L_new_status       VARCHAR2(1);
      L_fiscal_doc_no    FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE;
      L_series_no        FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE;
      L_subseries_no     FM_FISCAL_DOC_HEADER.SUBSERIES_NO%TYPE;
      L_auto_approve_ind FM_UTILIZATION_ATTRIBUTES.AUTO_APPROVE_IND%TYPE;
      L_utilization_id   FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE;
      ---
      cursor C_GET_UTIL_PARAM(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
         select a.auto_approve_ind,
                h.utilization_id
--                a.auto_print_ind
           from fm_fiscal_utilization ffu,
                fm_utilization_attributes a,
                fm_fiscal_doc_header h
          where h.fiscal_doc_id = P_fiscal_doc_id
            and ffu.utilization_id   = h.utilization_id
            and ffu.utilization_id   = a.utilization_id
            and ffu.status = 'A';

     cursor C_GET_LOC is
        select loc_type,
               location
          from fm_rma_head
         where rma_id = I_rma_id;
    R_get_loc C_GET_LOC%ROWTYPE;
--      cursor C_GET_REQ_TYPE is
--         select requisition_type
--           from fm_edi_doc_header
--          where edi_doc_id = I_edi_doc_id;
--      Rec_req_type C_GET_REQ_TYPE%ROWTYPE;
--      ---
     cursor C_GET_FISCAL is
        select location_type,
               location_id,
               series_no,
               type_id
          from fm_fiscal_doc_header
         where fiscal_doc_id = L_fiscal_doc_id;
         R_get_fiscal C_GET_FISCAL%ROWTYPE;
   BEGIN
      -- no validation , proceed with the integration
         -- get the next fiscal_doc_id sequence
       L_fiscal_doc_id := 0;
       if (FM_FISCAL_HEADER_VAL_SQL.GET_NEXT_FISCAL_DOC_ID(O_error_message, L_fiscal_doc_id) = FALSE) then
          return (FALSE);
       end if;
       -- insert doc header
       if (PROCESS_RMA_HEADER(O_error_message, I_schedule_no, L_fiscal_doc_no, I_rma_id, L_fiscal_doc_id) = FALSE) then
           open C_GET_FISCAL;
           fetch C_GET_FISCAL into R_get_fiscal;
            if FM_LOC_FISCAL_NUMBER_SQL.ROLLBACK_FISCAL_NO(O_error_message,
                                                           L_fiscal_doc_no,
                                                           R_get_fiscal.location_type,
                                                           R_get_fiscal.location_id,
                                                           R_get_fiscal.type_id, 
                                                           R_get_fiscal.series_no) = FALSE then
               return FALSE;
            end if;
           close C_GET_FISCAL;
          O_status := FALSE;
          return (FALSE);
       end if;
       -- insert doc details

       open C_GET_LOC;
       fetch C_GET_LOC into R_get_loc;
       if (PROCESS_RMA_DETAILS(O_error_message, I_rma_id, L_fiscal_doc_id,R_get_loc.loc_type,R_get_loc.location) = FALSE) then
          O_status := FALSE;
          return (FALSE);
       end if;
       close C_GET_LOC;
       -- Updates the fiscal_doc_id field of the current processed record
       if FM_RMA_NF_CREATION_SQL.UPDATE_FISCAL_DOC(O_error_message,
                                                   I_rma_id,
                                                   L_fiscal_doc_id) = FALSE then
          return (FALSE);
       end if;
      open C_GET_UTIL_PARAM(L_fiscal_doc_id);
      fetch C_GET_UTIL_PARAM into  L_auto_approve_ind,L_utilization_id;
      close C_GET_UTIL_PARAM;

      if L_auto_approve_ind = 'Y' then
         if FM_EXIT_NF_CREATION_SQL.PROCESS_OUTBOUND(O_error_message,
                                                     L_status,
                                                     L_fiscal_doc_id,
                                                     L_utilization_id,
                                                     I_schedule_no,
                                                     'RMA') = FALSE then
             O_status := FALSE;
             return FALSE;
         end if;

          if FM_EXIT_NF_CREATION_SQL.SCHEDULE_APPROVAL(O_error_message
                                                   ,L_fiscal_doc_id
                                                   ,I_schedule_no
                                                   ,'RMA') = FALSE then
            O_status := FALSE;
             return FALSE;
          end if;
       end if;
      return(true);
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
         O_status := FALSE;
         return FALSE;
END PROCESS_RMA;
--------------------------------------------------------------------------------
-- Function and procedure implementations
FUNCTION PROCESS(O_error_message IN OUT VARCHAR2,
                 I_schedule_no   IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                 I_doc_id        IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                 I_edi_ind       IN     BOOLEAN)
return BOOLEAN is
   L_program          VARCHAR2(100) := 'FM_EDI_RMA_INTEGRATION_SQL.PROCESS';
   L_status           BOOLEAN := TRUE;
   L_header_status    VARCHAR2(1) := 'P';
   L_fiscal_doc_id    FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_compl_edi_doc_id FM_EDI_DOC_COMPLEMENT.COMPL_EDI_DOC_ID%TYPE;
   L_comp_triang_ind  VARCHAR2(1) := 'N';
   L_nf_type_ind      VARCHAR2(1) := 'M';
   ---
   cursor C_GET_RMA_DOCS is
      select h.rma_id
        from fm_rma_head h
       where h.rma_status   = 'U'  --
         and h.rma_id       = I_doc_id;

   L_rma_header C_GET_RMA_DOCS%ROWTYPE;
   ---
   
   --- cursor to fetch the compl doc id for triangulation PO
   cursor C_GET_COMPL_DOC_ID is
      select fedc.compl_edi_doc_id
        from fm_edi_doc_complement fedc,
             fm_edi_doc_detail fedd,
             ordhead ord
       where fedc.edi_doc_id       = I_doc_id
         and fedc.edi_doc_id       = fedd.edi_doc_id
         and fedd.requisition_no   = ord.order_no
         and ord.triangulation_ind = 'Y';

BEGIN

   if I_edi_ind = TRUE then
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_COMPL_DOC_ID', 'FM_EDI_DOC_COMPLEMENT', I_doc_id);
      open C_GET_COMPL_DOC_ID;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_COMPL_DOC_ID', 'FM_EDI_DOC_COMPLEMENT', I_doc_id);
      fetch C_GET_COMPL_DOC_ID into L_compl_edi_doc_id;
      if C_GET_COMPL_DOC_ID%FOUND then
         L_comp_triang_ind := 'Y';
         if (PROCESS_EDI(O_error_message, L_status, L_compl_edi_doc_id) = false) then
             return false;
         end if;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_COMPL_DOC_ID', 'FM_EDI_DOC_COMPLEMENT', I_doc_id);
      close C_GET_COMPL_DOC_ID;
      ---
      if (PROCESS_EDI(O_error_message, L_status, I_doc_id) = false) then
          return FALSE;
      end if;

      -- Updates the status of the current processed record
      if L_comp_triang_ind = 'Y' then
         L_nf_type_ind := 'B';
      end if;
      if FM_EDI_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                             I_doc_id,
                                             'P',
                                             L_nf_type_ind) = FALSE then
         return FALSE;
      end if;      
   else
      open C_GET_RMA_DOCS;
      fetch C_GET_RMA_DOCS into L_rma_header;
      if (PROCESS_RMA(O_error_message, L_status, L_rma_header.rma_id,I_schedule_no) = false) then
         return false;
      end if;
      if (L_status = false) then
         L_header_status := 'E'; -- Error
      else
         L_header_status := 'R'; -- Processed
      end if;
      -- Updates the status of the current processed record
      if FM_RMA_NF_CREATION_SQL.UPDATE_STATUS(O_error_message,
                                              L_rma_header.rma_id,
                                              L_header_status) = FALSE then
         return (false);
      end if;
      close C_GET_RMA_DOCS;
   end if;
  
   return(true);
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_COMPL_DOC_ID%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_COMPL_DOC_ID', 'FM_EDI_DOC_COMPLEMENT', I_doc_id);
         close C_GET_COMPL_DOC_ID;
      end if;
      
      if C_GET_RMA_DOCS%ISOPEN then
         close C_GET_RMA_DOCS;
      end if;
      
      return FALSE;
END PROCESS;
---------------------------------------------------------------------------------------
END FM_EDI_RMA_INTEGRATION_SQL;
/
