create or replace PACKAGE BODY FM_STG_VALIDATION_SQL is
--------------------------------------------------------------------------------
 FUNCTION VALIDATE_RTV_DETAIL(O_error_message IN OUT VARCHAR2,
                              I_rtvdesc_rec   IN    "RIB_RTVDesc_REC",
                              L_rtvDtl_tbl    IN    "RIB_RTVDtl_TBL")
   return BOOLEAN is

     L_program           VARCHAR2(100) := 'FM_STG_VALIDATION_SQL.VALIDATE_RTV_DETAIL';
     L_exists            BOOLEAN := TRUE;
     L_from_disposition  FM_STG_RTV_DTL.FROM_DISPOSITION%TYPE;
     L_rtvDtl_rec       "RIB_RTVDtl_REC"  := NULL;
     L_count             NUMBER           := NULL;
     L_exist             VARCHAR2(1)      := NULL;
     L_receive_as_type   ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
     L_pack_ind          ITEM_MASTER.PACK_IND%TYPE := 'N';
     L_loc_type          FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;

    cursor C_INV_STATUS is
         select inv_status
           from inv_status_codes
          where inv_status_code = L_rtvDtl_rec.from_disposition;
      ---
    cursor C_REASON_EXISTS is
   select 'x'
     from code_detail
    where code_type = 'RTVR'
      and code = L_rtvDtl_rec.reason;

   cursor C_ITEM_SUPP_EXIST is
      select 'x'
        from item_supplier
       where item     = L_rtvDtl_rec.item_id
         and supplier = I_rtvdesc_rec.vendor_nbr;

   cursor C_RECEIVE_AS_TYPE is
   select NVL(receive_as_type, 'E')
     from item_loc
    where item = L_rtvDtl_rec.item_id
      and loc  = I_rtvdesc_rec.dc_dest_id;

   cursor C_GET_PACK_IND is
    select pack_ind
      from item_master
      where item = L_rtvDtl_rec.item_id;

   BEGIN

      for L_count in L_rtvDtl_tbl.First..L_rtvDtl_tbl.Last loop

         L_rtvDtl_rec := L_rtvDtl_tbl(L_count);

         open C_INV_STATUS;
         fetch C_INV_STATUS into L_from_disposition;

         if C_INV_STATUS%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_DISPOSITION',
                                                  L_program,
                                                  NULL,
                                                  L_rtvDtl_rec.from_disposition);
            close C_INV_STATUS;
            return FALSE;
          end if;
      close C_INV_STATUS;

      if L_rtvDtl_rec.reason is NOT NULL then
         L_exist := NULL;

         open C_REASON_EXISTS;
        fetch C_REASON_EXISTS into L_exist;
        close C_REASON_EXISTS;
      ---
        if L_exist is NULL then
           O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                                 L_program,
                                                 'reason',
                                                 L_rtvDtl_rec.reason);
           return FALSE;
        end if;
      end if;

         -- Verify the item
         if L_rtvDtl_rec.item_id is not null then
            if (item_validate_sql.exist(O_error_message,
                                        L_exists,
                                        L_rtvDtl_rec.item_id) = FALSE) then
               return FALSE;
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',NULL,NULL,L_rtvDtl_rec.item_id);
            return FALSE;
         end if;

         if (L_exists = FALSE) then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',NULL,NULL,L_rtvDtl_rec.item_id);
            return FALSE;
         end if;

        L_exist := NULL;
       open C_ITEM_SUPP_EXIST;
      fetch C_ITEM_SUPP_EXIST into L_exist;

      if C_ITEM_SUPP_EXIST%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_SUP', NULL, NULL, NULL);
         close C_ITEM_SUPP_EXIST;
         return FALSE;
      end if;
      close C_ITEM_SUPP_EXIST;
   ---
      open C_RECEIVE_AS_TYPE;
     fetch C_RECEIVE_AS_TYPE into L_receive_as_type;
     close C_RECEIVE_AS_TYPE;
   ---
   -- At warehouses, packs can only be received as packs not eaches.
      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_loc_type,
                                      I_rtvDesc_rec.dc_dest_id) = FALSE then
         return FALSE;
      end if;
      if L_loc_type = 'W' then
          open C_GET_PACK_IND;
         fetch C_GET_PACK_IND into L_pack_ind;
         close C_GET_PACK_IND;
         if L_pack_ind = 'Y' and L_receive_as_type = 'E' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_PACK_EACH',
                                                  L_rtvDtl_rec.item_id,
                                                  NULL,
                                                  NULL);
               return FALSE;
         end if;
      end if;

         if (L_rtvDtl_rec.unit_qty is null or L_rtvDtl_rec.unit_qty <= 0) then
            O_error_message := SQL_LIB.CREATE_MSG('QTY_GREATER_ZERO',NULL,NULL,L_rtvDtl_rec.unit_qty);
            return FALSE;
         end if;
         ---
      END LOOP;
      ---
      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
         return FALSE;

   END VALIDATE_RTV_DETAIL;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_RTV_HEAD(O_error_message IN OUT VARCHAR2,
                           L_rtvDesc_rec   IN "RIB_RTVDesc_REC")
   return BOOLEAN IS

   L_program VARCHAR2(64) := 'FM_STG_VALIDATION_SQL.VALIDATE_RTV_HEAD';

      L_loc_type     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
      L_valid_module NUMBER;
      L_exists       BOOLEAN;

      cursor C_GET_MODULE (P_key_value1 VARCHAR2) is
         select 1
           from v_br_sups_fiscal_addr v
          where v.supplier = P_key_value1;

   BEGIN

      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_loc_type,
                                      L_rtvDesc_rec.dc_dest_id) = FALSE then
            return FALSE;
      end if;

      if (L_loc_type = 'S') then
         if (store_validate_sql.exist(O_error_message,
                                      L_rtvDesc_rec.dc_dest_id,
                                      L_exists) = FALSE) then
             return FALSE;
          end if;
          if (L_exists = FALSE) then
               O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,L_rtvDesc_rec.dc_dest_id);
               return FALSE;
          end if;
      elsif (L_loc_type = 'W') then
         if (warehouse_validate_sql.exist(O_error_message,
                                          L_rtvDesc_rec.dc_dest_id,
                                          L_exists) = FALSE) then
            return FALSE;
         end if;
         if (L_exists = FALSE) then
            O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,L_rtvDesc_rec.dc_dest_id);
               return FALSE;
         end if;
      end if;
         ---
        SQL_LIB.SET_MARK('OPEN', 'C_GET_MODULE', 'V_BR_SUPS_FISCAL_ADDR', L_rtvDesc_rec.vendor_nbr);
        open C_GET_MODULE (L_rtvDesc_rec.vendor_nbr);
         ---
        SQL_LIB.SET_MARK('FETCH', 'C_GET_MODULE', 'V_BR_SUPS_FISCAL_ADDR', L_rtvDesc_rec.vendor_nbr);
        fetch C_GET_MODULE into L_valid_module;
        ---
        if (L_valid_module is null or C_GET_MODULE%NOTFOUND) then
           O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER',NULL,NULL, L_rtvDesc_rec.vendor_nbr);
           close C_GET_MODULE;
           return FALSE;
        end if;

         SQL_LIB.SET_MARK('CLOSE', 'C_GET_MODULE', 'V_BR_SUPS_FISCAL_ADDR', L_rtvDesc_rec.vendor_nbr);
         close C_GET_MODULE;

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
         return FALSE;
   END VALIDATE_RTV_HEAD;
--------------------------------------------------------------------------------------------
 FUNCTION VALIDATE_INV_DETAIL(O_error_message IN OUT VARCHAR2,
                              L_invtl_tbl     IN    "RIB_InvAdjustDtl_TBL")
   return BOOLEAN is

     L_program           VARCHAR2(100) := 'FM_STG_VALIDATION_SQL.VALIDATE_INV_DETAIL';

     L_invdtl_rec    "RIB_InvAdjustDtl_REC" := NULL;
     L_count         NUMBER                 := NULL;
     L_reason        FM_STG_ADJUST_DTL.ADJUSTMENT_REASON_CODE%TYPE;
     L_not_used      INV_ADJ_REASON.REASON_DESC%TYPE;
     L_found         BOOLEAN;

   BEGIN
   for L_count in L_invtl_tbl.First..L_invtl_tbl.Last loop
      L_invdtl_rec := L_invtl_tbl(L_count);

      if (L_invdtl_rec.unit_qty is null) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_QTY',NULL,NULL,L_invdtl_rec.unit_qty);
         return FALSE;
      end if;

         -- Verify the item
      if L_invdtl_rec.item_id is not null then
         if (item_validate_sql.exist(O_error_message,
                                     L_found,
                                     L_invdtl_rec.item_id) = FALSE) then
             return FALSE;
         end if;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',L_program,NULL,L_invdtl_rec.item_id);
         return FALSE;
      end if;

      if (L_found = FALSE) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',L_program,NULL,L_invdtl_rec.item_id);
         return FALSE;
      end if;

      if L_invdtl_rec.adjustment_reason_code is not null then
         if INVADJ_VALIDATE_SQL.REASON_EXIST(L_invdtl_rec.adjustment_reason_code,
                                             L_not_used,
                                             O_error_message,
                                             L_found) = FALSE then
            return FALSE;
         end if;
      end if;
   ---
      if L_found = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_REASON_CODE',NULL,NULL,L_invdtl_rec.adjustment_reason_code);
         return FALSE;
      end if;
      ---
   end loop;

return TRUE;

   EXCEPTION

      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
         return FALSE;

   END VALIDATE_INV_DETAIL;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_INV_HEAD(O_error_message IN OUT VARCHAR2,
                           L_invDesc_rec   IN "RIB_InvAdjustDesc_REC")
   return BOOLEAN IS

   L_program VARCHAR2(64) := 'FM_STG_VALIDATION_SQL.VALIDATE_INV_HEAD';

      L_loc_type     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
      L_valid_module NUMBER;
      L_exists       BOOLEAN;

   BEGIN

      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_loc_type,
                                      L_invDesc_rec.dc_dest_id) = FALSE then
            return FALSE;
      end if;

      if (L_loc_type = 'S') then
         if (store_validate_sql.exist(O_error_message,
                                      L_invDesc_rec.dc_dest_id,
                                      L_exists) = FALSE) then
             return FALSE;
          end if;
          if (L_exists = FALSE) then
               O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,L_invDesc_rec.dc_dest_id);
               return FALSE;
          end if;
      elsif (L_loc_type = 'W') then
         if (warehouse_validate_sql.exist(O_error_message,
                                          L_invDesc_rec.dc_dest_id,
                                          L_exists) = FALSE) then
            return FALSE;
         end if;
         if (L_exists = FALSE) then
            O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,L_invDesc_rec.dc_dest_id);
               return FALSE;
         end if;
      end if;
         ---

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
         return FALSE;
   END VALIDATE_INV_HEAD;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_HEAD(O_error_message IN OUT VARCHAR2,
                           I_asnoutdesc    IN "RIB_ASNOutDesc_REC")
   return BOOLEAN IS

   L_program          VARCHAR2(64) := 'FM_STG_VALIDATION_SQL.VALIDATE_TSF_HEAD';
   L_from_loc_type    FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
   L_to_loc_type      FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
   L_exists        BOOLEAN := TRUE;

BEGIN

   if I_asnoutdesc.to_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_asnoutdesc.to_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_asnoutdesc.from_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_asnoutdesc.from_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_from_loc_type,
                                   I_asnoutdesc.from_location) = FALSE then
      return FALSE;
   end if;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_to_loc_type,
                                   I_asnoutdesc.to_location) = FALSE then
      return FALSE;
   end if;

   if (L_from_loc_type = 'S') then

      if (store_validate_sql.exist(O_error_message,
                                   I_asnoutdesc.from_location,
                                   L_exists) = FALSE) then
               return FALSE;
      end if;

      if (L_exists = FALSE) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,NULL);
         return FALSE;
      end if;

   elsif (L_from_loc_type = 'W') then

      if (warehouse_validate_sql.exist(O_error_message,
                                       I_asnoutdesc.from_location,
                                       L_exists) = FALSE) then
         return FALSE;
      end if;

      if (L_exists = FALSE) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,NULL);
         return FALSE;
      end if;
   end if;

   if (L_to_loc_type = 'S') then

      if (store_validate_sql.exist(O_error_message,
                                   I_asnoutdesc.to_location,
                                   L_exists) = FALSE) then
               return FALSE;
      end if;

      if (L_exists = FALSE) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,NULL);
         return FALSE;
      end if;

   elsif (L_to_loc_type = 'W') then

      if (warehouse_validate_sql.exist(O_error_message,
                                       I_asnoutdesc.to_location,
                                       L_exists) = FALSE) then
          return FALSE;
      end if;

      if (L_exists = FALSE) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,NULL);
         return FALSE;
      end if;
   end if;
---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_TSF_HEAD;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_DISTRO(O_error_message IN OUT VARCHAR2,
                             I_asnoutdesc    IN "RIB_ASNOutDesc_REC",
                             I_asndistro     IN "RIB_ASNOutDistro_TBL")
   return BOOLEAN IS

   L_program VARCHAR2(64) := 'FM_STG_VALIDATION_SQL.VALIDATE_TSF_DISTRO';

   L_asnoutdistro      "RIB_ASNOutDistro_REC";
   L_count             NUMBER := NULL;

   L_vwh_exist                  VARCHAR2(1) := NULL;
   L_tsf_type                   TSFHEAD.TSF_TYPE%TYPE := NULL;
   L_from_loc_type              FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE := NULL;
   L_to_loc_type                FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE := NULL;
   L_to_loc_entity              TSF_ENTITY.TSF_ENTITY_ID%TYPE := NULL;
   L_from_loc_entity            TSF_ENTITY.TSF_ENTITY_ID%TYPE := NULL;
   L_to_loc_sob                 TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE  := NULL;
   L_from_loc_sob               TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE  := NULL;
   L_from_sob_desc              FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE           := NULL;
   L_to_sob_desc                FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE           := NULL;
   L_entity_name                TSF_ENTITY.TSF_ENTITY_DESC%TYPE := NULL;
   L_from_tsf_zone              STORE.TRANSFER_ZONE%TYPE := NULL;
   L_to_tsf_zone                STORE.TRANSFER_ZONE%TYPE := NULL;
   L_from_zone_exits            BOOLEAN := NULL;
   L_to_zone_exits              BOOLEAN := NULL;
   L_system_options_row         SYSTEM_OPTIONS%ROWTYPE;

   L_exist                      VARCHAR2(1)           := NULL;
   L_to_loc                     ITEM_LOC.LOC%TYPE     := NULL;
   L_from_loc                   ITEM_LOC.LOC%TYPE     := NULL;
   L_vir_loc                    ITEM_LOC.LOC%TYPE     := NULL;
   L_phy_loc                    ITEM_LOC.LOC%TYPE     := NULL;

   cursor C_CHECK_WH is
      select 'x'
        from wh w
       where w.physical_wh = L_phy_loc
         and w.wh          = L_vir_loc;

   cursor C_CHECK_TSF is
      select th.from_loc,
             th.to_loc
        from tsfhead th
       where TO_CHAR(th.tsf_no) = L_asnoutdistro.distro_nbr;

   cursor C_TSF_TYPE is
      select tsf_type
        from tsfhead
       where tsf_no = L_asnoutdistro.distro_nbr;

   cursor C_VWH_EXIST is
      select 'x'
        from wh
       where physical_wh = I_asnoutdesc.to_location
         and physical_wh <> wh
         and tsf_entity_id = L_from_loc_entity
         and ROWNUM = 1;

   cursor C_FR_VWH_EXIST is
      select 'x'
        from wh
       where physical_wh = I_asnoutdesc.from_location
         and physical_wh <> wh
         and tsf_entity_id = L_to_loc_entity
         and ROWNUM = 1;

   cursor C_TO_VWH_SOB is
      select 'x'
        from mv_loc_sob,
          wh w
       where w.physical_wh = I_asnoutdesc.to_location
         and w.wh = location
         and location_type = L_to_loc_type
         and set_of_books_id = L_from_loc_sob
         and ROWNUM = 1;

   cursor C_FR_VWH_SOB is
      select 'x'
        from mv_loc_sob,
          wh w
       where w.physical_wh = I_asnoutdesc.from_location
         and w.wh = location
         and location_type = L_from_loc_type
         and set_of_books_id = L_to_loc_sob
         and ROWNUM = 1;

BEGIN

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_from_loc_type,
                                   I_asnoutdesc.from_location) = FALSE then
      return FALSE;
   end if;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_to_loc_type,
                                   I_asnoutdesc.to_location) = FALSE then
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   FOR L_count IN I_asndistro.FIRST .. I_asndistro.LAST
   LOOP
      
      L_asnoutdistro :=I_asndistro(L_count);
      if L_asnoutdistro.distro_doc_type ='T' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_TSF_TYPE',
                          'tsfhead',
                          NULL);
         open C_TSF_TYPE;
         SQL_LIB.SET_MARK('FETCH',
                          'C_TSF_TYPE',
                          'tsfhead',
                          NULL);
         fetch C_TSF_TYPE into L_tsf_type;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_TSF_TYPE',
                          'tsfhead',
                          NULL);
         close C_TSF_TYPE;
         L_tsf_type := NVL(L_tsf_type, 'EG');
   ---
        if L_tsf_type = 'EG' then
      ---
              if L_from_loc_type = 'W' and L_to_loc_type = 'W' then
                 O_error_message := SQL_LIB.CREATE_MSG('EG_TSF_FR_TO_LOC_TYPE_WH',
                                                       I_asnoutdesc.from_location,
                                                       I_asnoutdesc.to_location,
                                                       L_program);
                 return FALSE;
              end if;
      ---
             if L_from_loc_type = 'S' then
                 if L_system_options_row.intercompany_transfer_basis = 'T' then
                    if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                                      L_from_loc_entity,
                                                      L_entity_name,
                                                      I_asnoutdesc.from_location,
                                                      L_from_loc_type) = FALSE then
                       return FALSE;
                    end if;
                    if STORE_ATTRIB_SQL.GET_TSF_ZONE(O_error_message,
                                                     L_from_tsf_zone,
                                                     L_from_zone_exits,
                                                     I_asnoutdesc.from_location) = FALSE then
                       return FALSE;
                    end if;
                 else
                    if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                                    L_from_sob_desc,
                                                    L_from_loc_sob,
                                                    I_asnoutdesc.from_location,
                                                    L_from_loc_type) = FALSE then
                       return FALSE;
                    end if;
                 end if;
              end if;
              if L_to_loc_type = 'S' then
                 if L_system_options_row.intercompany_transfer_basis = 'T' then
                    if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                                      L_to_loc_entity,
                                                      L_entity_name,
                                                      I_asnoutdesc.to_location,
                                                      L_to_loc_type) = FALSE then
                       return FALSE;
                    end if;
                    if STORE_ATTRIB_SQL.GET_TSF_ZONE(O_error_message,
                                                     L_to_tsf_zone,
                                                     L_to_zone_exits,
                                                     I_asnoutdesc.to_location) = FALSE then
                       return FALSE;
                    end if;
                 else
                    if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                                    L_to_sob_desc,
                                                    L_to_loc_sob,
                                                    I_asnoutdesc.to_location,
                                                    L_to_loc_type) = FALSE then
                       return FALSE;
                    end if;
                 end if;
              end if;
         ---
              if L_from_loc_type = 'S' and L_to_loc_type = 'W' then
                 if L_system_options_row.intercompany_transfer_basis = 'T' then
                    SQL_LIB.SET_MARK('OPEN',
                                     'C_VWH_EXIST',
                                     'wh',
                                     NULL);
                    open C_VWH_EXIST;
                    SQL_LIB.SET_MARK('FETCH',
                                     'C_VWH_EXIST',
                                     'wh',
                                      NULL);
                    fetch C_VWH_EXIST into L_vwh_exist;
                    SQL_LIB.SET_MARK('CLOSE',
                                     'C_VWH_EXIST',
                                     'wh',
                                      NULL);
                    close C_VWH_EXIST;

                    if L_vwh_exist is NULL then
                       O_error_message := SQL_LIB.CREATE_MSG('EG_TSF_NO_VWH_EXIST',
                                                              I_asnoutdesc.from_location,
                                                              I_asnoutdesc.to_location,
                                                              L_from_loc_entity);
                       return FALSE;
                    end if;
                 else
                    SQL_LIB.SET_MARK('OPEN',
                                     'C_TO_VWH_SOB',
                                     'mv_loc_sob',
                                      NULL);
                    open C_TO_VWH_SOB;
                    SQL_LIB.SET_MARK('FETCH',
                                     'C_TO_VWH_SOB',
                                     'mv_loc_sob',
                                      NULL);
                    fetch C_TO_VWH_SOB into L_vwh_exist;
                    SQL_LIB.SET_MARK('CLOSE',
                                     'C_TO_VWH_SOB',
                                     'mv_loc_sob',
                                      NULL);
                    close C_TO_VWH_SOB;

                    if L_vwh_exist is NULL then
                       O_error_message := SQL_LIB.CREATE_MSG('EG_SOB_NO_VWH_EXIST',
                                                             I_asnoutdesc.from_location,
                                                             I_asnoutdesc.to_location,
                                                             L_from_loc_sob);
                          return FALSE;
                    end if;
                 end if;
              elsif L_from_loc_type = 'W' and L_to_loc_type = 'S' then
                 if L_system_options_row.intercompany_transfer_basis = 'T' then
                    SQL_LIB.SET_MARK('OPEN',
                                     'C_FR_VWH_EXIST',
                                     'wh',
                                     NULL);
                    open C_FR_VWH_EXIST;
                    SQL_LIB.SET_MARK('FETCH',
                                     'C_FR_VWH_EXIST',
                                     'wh',
                                     NULL);
                    fetch C_FR_VWH_EXIST into L_vwh_exist;
                    SQL_LIB.SET_MARK('CLOSE',
                                     'C_FR_VWH_EXIST',
                                     'wh',
                                     NULL);
                    close C_FR_VWH_EXIST;

                    if L_vwh_exist is NULL then
                       O_error_message := SQL_LIB.CREATE_MSG('EG_TSF_NO_VWH_EXIST',
                                                              I_asnoutdesc.to_location,
                                                              I_asnoutdesc.from_location,
                                                              L_to_loc_entity);
                       return FALSE;
                    end if;
                 else
                    SQL_LIB.SET_MARK('OPEN',
                                     'C_FR_VWH_SOB',
                                     'mv_loc_sob',
                                      NULL);
                    open C_FR_VWH_SOB;
                    SQL_LIB.SET_MARK('FETCH',
                                     'C_FR_VWH_SOB',
                                     'mv_loc_sob',
                                      NULL);
                    fetch C_FR_VWH_SOB into L_vwh_exist;
                    SQL_LIB.SET_MARK('CLOSE',
                                     'C_FR_VWH_SOB',
                                     'mv_loc_sob',
                                     NULL);
                    close C_FR_VWH_SOB;

                    if L_vwh_exist is NULL then
                       O_error_message := SQL_LIB.CREATE_MSG('EG_SOB_NO_VWH_EXIST',
                                                             I_asnoutdesc.to_location,
                                                             I_asnoutdesc.from_location,
                                                             L_to_loc_sob);
                       return FALSE;
                    end if;
                 end if;
              end if;
         ---
              if L_from_loc_type = 'S' and L_to_loc_type = 'S' and L_from_loc_sob <> L_to_loc_sob then
                 O_error_message := SQL_LIB.CREATE_MSG('EG_TSF_STORE_DIFF_SOB',
                                                       I_asnoutdesc.to_location,
                                                       I_asnoutdesc.from_location,
                                                       L_program);
                 return FALSE;
              end if;
         ---
              if L_from_loc_type = 'S' and L_to_loc_type = 'S' and L_from_tsf_zone <> L_to_tsf_zone then
                 O_error_message := SQL_LIB.CREATE_MSG('TF_STR_TRAN_ZONE',
                                                        L_program,
                                                        NULL,
                                                        NULL);
                 return FALSE;
              end if;
         ---           
        end if;
     end if;

      open C_CHECK_TSF;
      fetch C_CHECK_TSF into L_from_loc,
                             L_to_loc;
      close C_CHECK_TSF;

      -- Verify From Locations Match
      if I_asnoutdesc.from_location != TO_CHAR(L_from_loc) then

         L_phy_loc := I_asnoutdesc.from_location;
         L_vir_loc := L_from_loc;

         open C_CHECK_WH;
         fetch C_CHECK_WH into L_exist;
         close C_CHECK_WH;

         if L_exist is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FROM_LOC',
                                                  TO_CHAR(L_phy_loc),
                                                  TO_CHAR(L_asnoutdistro.distro_nbr), 
                                                  NULL);
            return FALSE;
         end if;
      end if;

      -- Verify To Locations Match
      if I_asnoutdesc.to_location != TO_CHAR(L_to_loc) then

         L_phy_loc := I_asnoutdesc.to_location;
         L_vir_loc := L_to_loc;

         open C_CHECK_WH;
         fetch C_CHECK_WH into L_exist;
         close C_CHECK_WH;

         if L_exist is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_TO_LOC',
                                                  TO_CHAR(L_phy_loc),
                                                  TO_CHAR(L_asnoutdistro.distro_nbr), 
                                                  NULL);
            return FALSE;
         end if;
      end if;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_TSF_DISTRO;
-------------------------------------------------------------------------------------
FUNCTION PROCESS_VALIDATION(O_error_message IN OUT VARCHAR2,
                            I_message        IN      RIB_OBJECT,
                            I_message_type   IN      VARCHAR2,
                            I_requisition_type IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN is
      L_program       VARCHAR2(100) := 'FM_STG_VALIDATION_SQL.PROCESS_VALIDATION';

      L_rtvDesc_rec     "RIB_RTVDesc_REC"       := NULL;
      L_invDesc_rec     "RIB_InvAdjustDesc_REC" := NULL;
      L_asnoutdesc      "RIB_ASNOutDesc_REC"    := NULL;
      L_asnoutdistro    "RIB_ASNOutDistro_REC";

   BEGIN

   if I_requisition_type = 'RTV' then

        L_rtvDesc_rec := treat ( I_message as "RIB_RTVDesc_REC");

        if (VALIDATE_RTV_HEAD(O_error_message,
                              L_rtvDesc_rec)  = FALSE) then
           return FALSE;
        end if;

        if (VALIDATE_RTV_DETAIL(O_error_message,
                                L_rtvDesc_rec,
                                L_rtvDesc_rec.RTVDtl_TBL) = FALSE) then
           return FALSE;
        end if;

   elsif I_requisition_type = 'STOCK' then

      L_invDesc_rec := treat ( I_message as "RIB_InvAdjustDesc_REC");

        if (VALIDATE_INV_HEAD(O_error_message,
                              L_invDesc_rec)  = FALSE) then
           return FALSE;
        end if;

        if (VALIDATE_INV_DETAIL(O_error_message,
                                L_invDesc_rec.InvAdjustDtl_TBL) = FALSE) then
           return FALSE;
        end if;

   elsif I_requisition_type = 'TSF' then

      L_asnoutdesc := treat(I_message as "RIB_ASNOutDesc_REC");

        if (VALIDATE_TSF_HEAD(O_error_message,
                              L_asnoutdesc)  = FALSE) then
           return FALSE;
        end if;

        if VALIDATE_TSF_DISTRO(O_error_message,
                               L_asnoutdesc,
                               L_asnoutdesc.ASNOutDistro_TBL) = FALSE then
           return FALSE;
        end if;

   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END PROCESS_VALIDATION;
--------------------------------------------------------------------------------
END FM_STG_VALIDATION_SQL;
/