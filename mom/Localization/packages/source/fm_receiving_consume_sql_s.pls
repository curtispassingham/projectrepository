CREATE or REPLACE PACKAGE FM_RECEIVING_CONSUME_SQL AS
---------------------------------------------------------------------------------------------------------
-- Function Name: CONSUME
-- Purpose      : Consume Message the FM_RECEIVING_HEADER and FM_RECEIVING_DETAIL 
---------------------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind       IN OUT   BOOLEAN,
                 I_schedule_no     IN       FM_RECEIVING_HEADER.RECV_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_transaction_type   IN       FM_RECEIVING_HEADER.REQUISITION_TYPE%TYPE,
                              I_thread_no          IN       NUMBER,
                              I_num_threads        IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
END FM_RECEIVING_CONSUME_SQL;
/