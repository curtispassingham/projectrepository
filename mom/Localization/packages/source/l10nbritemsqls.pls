CREATE OR REPLACE PACKAGE L10N_BR_ITEM_SQL AS
--------------------------------------------------------------------
-- Function Name: CLASS_ID_EXISTS
-- Purpose      : Verify if fiscal classification.exists.
---------------------------------------------------------------------------------------------
FUNCTION CLASS_ID_EXISTS(O_error_message       IN OUT VARCHAR2,
                         O_exists              IN OUT BOOLEAN,
                         I_country             IN     L10N_BR_NCM_CODES.COUNTRY_ID%TYPE,
                         I_classification      IN     L10N_BR_NCM_CODES.NCM_CODE%TYPE,
                         I_classification_type IN     VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name:  GET_FISCAL_CLASS_INFO 
-- Purpose      : Gets fiscal classification description and type
---------------------------------------------------------------------------------------------
FUNCTION  GET_FISCAL_CLASS_INFO(O_error_message       IN OUT VARCHAR2,
                                O_classification_desc IN OUT L10N_BR_NCM_CODES.NCM_DESC%TYPE,
                                O_classification_type IN OUT VARCHAR2,
                                I_country_id          IN     L10N_BR_NCM_CODES.COUNTRY_ID%TYPE,
                                I_classification_id   IN     L10N_BR_NCM_CODES.NCM_CODE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------
-- LFAS Group Validation Functions
----------------------------------------------------------------------------------------------
FUNCTION CHK_FISCAL_ATTRIB_GRP(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_service_ind          IN       VARCHAR2,
                               I_origin_code          IN       VARCHAR2,
                               I_classification_id    IN       VARCHAR2,
                               I_ncm_char_code        IN       VARCHAR2,
                               I_ex_ipi               IN       VARCHAR2,
                               I_pauta_code           IN       VARCHAR2,
                               I_service_code         IN       VARCHAR2,
                               I_federal_service      IN       VARCHAR2,
                               I_state_of_manufacture IN       VARCHAR2,
                               I_pharma_list_type     IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
--LFAS Entity Validation Function(s)
----------------------------------------------------------------------------------------------
-- Function: CHK_ITEM_CTRY_ENT
-- Purpose : This function will chcek required fields depending on tax payer type selected.
--           Applicable entity: ITEM_COUNTRY
----------------------------------------------------------------------------------------------
FUNCTION CHK_ITEM_CTRY_ENT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_service_ind          IN     VARCHAR2,
                           I_origin_code          IN     VARCHAR2,
                           I_classification_id    IN     VARCHAR2,
                           I_ncm_char_code        IN     VARCHAR2,
                           I_ex_ipi               IN     VARCHAR2,
                           I_pauta_code           IN     VARCHAR2,
                           I_service_code         IN     VARCHAR2,
                           I_federal_service      IN     VARCHAR2,
                           I_state_of_manufacture IN     VARCHAR2,
                           I_pharma_list_type     IN     VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_FISCAL_RECLASS_SEQ
-- Purpose      : This function will retrieve the sequence to be used as the Reclassification ID.
----------------------------------------------------------------------------------------------
FUNCTION GET_FISCAL_RECLASS_SEQ(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_fiscal_reclass_seq   IN OUT   NUMBER)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEM_FRECLASS
-- Purpose      : This function will validate the item for reclassification and return the item description and level.
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_FRECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_item_desc       IN OUT   ITEM_MASTER.ITEM_DESC%TYPE,
                                O_item_level      IN OUT   ITEM_MASTER.ITEM_LEVEL%TYPE,
                                I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_CLASSIFICATION
-- Purpose      : This function will retrieve the NCM code and characteristics or Service code of an item.
----------------------------------------------------------------------------------------------
FUNCTION GET_CLASSIFICATION(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_classification_id      IN OUT   V_BR_ITEM_FISCAL_ATTRIB.CLASSIFICATION_ID%TYPE,
                            O_service_ind            IN OUT   V_BR_ITEM_FISCAL_ATTRIB.SERVICE_IND%TYPE,
                            O_origin_code            IN OUT   V_BR_ITEM_FISCAL_ATTRIB.ORIGIN_CODE%TYPE,
                            O_service_code           IN OUT   V_BR_ITEM_FISCAL_ATTRIB.SERVICE_CODE%TYPE,
                            O_federal_service_code   IN OUT   V_BR_ITEM_FISCAL_ATTRIB.FEDERAL_SERVICE%TYPE,
                            O_country_id             IN OUT   V_BR_ITEM_FISCAL_ATTRIB.COUNTRY_ID%TYPE,
                            O_ncm_char_code          IN OUT   V_BR_ITEM_FISCAL_ATTRIB.NCM_CHAR_CODE%TYPE,
                            O_ex_ipi_code            IN OUT   V_BR_ITEM_FISCAL_ATTRIB.EX_IPI%TYPE,
                            O_pauta_code             IN OUT   V_BR_ITEM_FISCAL_ATTRIB.PAUTA_CODE%TYPE,
                            O_state_of_manufacture   IN OUT   V_BR_ITEM_FISCAL_ATTRIB.STATE_OF_MANUFACTURE%TYPE,
                            O_pharma_list_type       IN OUT   V_BR_ITEM_FISCAL_ATTRIB.PHARMA_LIST_TYPE%TYPE,
                            I_item                   IN       V_BR_ITEM_FISCAL_ATTRIB.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: FRECLASS_EXISTS
-- Purpose      : This function will check if an item is already set for fiscal reclassification.
----------------------------------------------------------------------------------------------
FUNCTION FRECLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_item            IN       L10N_BR_FISCAL_RECLASS.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_SERVICE_CODE_DESC
-- Purpose      : This function will retrieve the description of the service code.
----------------------------------------------------------------------------------------------
FUNCTION GET_SERVICE_CODE_DESC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_mtr_service_code_desc   IN OUT   L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_DESC%TYPE,
                               O_fed_service_code_desc   IN OUT   L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_DESC%TYPE,
                               I_mtr_service_code        IN       L10N_BR_MASTERSAF_SVC_CODES.MASTERSAF_SERVICE_CODE%TYPE,
                               I_fed_service_code        IN       L10N_BR_FEDERAL_SVC_CODES.FEDERAL_SERVICE_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_NCM_ATTR
-- Purpose      : This function will retrieve the characteristics associated to a given NCM code.
----------------------------------------------------------------------------------------------
FUNCTION GET_NCM_ATTR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_ncm_char_code   IN OUT   L10N_BR_NCM_CHAR_CODES.NCM_CHAR_CODE%TYPE,
                      O_ipi_code        IN OUT   L10N_BR_NCM_IPI_CODES.IPI_CODE%TYPE,
                      O_pauta_code      IN OUT   L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_CODE%TYPE,
                      I_ncm_code        IN       L10N_BR_NCM_CHAR_CODES.NCM_CODE%TYPE,
                      I_country_id      IN       L10N_BR_NCM_CHAR_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_NCM_ATTR_DESC
-- Purpose      : This function will check if the entered NCM Characteristic, EX IPI or Pauta Code 
--                is valid for a given NCM code.
----------------------------------------------------------------------------------------------
FUNCTION GET_NCM_ATTR_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_ncm_char_desc     IN OUT   L10N_BR_NCM_CHAR_CODES.NCM_CHAR_DESC%TYPE,
                           O_ipi_desc          IN OUT   L10N_BR_NCM_IPI_CODES.IPI_DESC%TYPE,
                           O_ncm_pauta_desc    IN OUT   L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_DESC%TYPE,
                           I_ncm_char_code     IN       L10N_BR_NCM_CHAR_CODES.NCM_CHAR_CODE%TYPE,
                           I_ipi_code          IN       L10N_BR_NCM_IPI_CODES.IPI_CODE%TYPE,
                           I_pauta_code        IN       L10N_BR_NCM_PAUTA_CODES.NCM_PAUTA_CODE%TYPE,
                           I_ncm_code          IN       L10N_BR_NCM_CHAR_CODES.NCM_CODE%TYPE,
                           I_country_id        IN       L10N_BR_NCM_CHAR_CODES.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_ATTRIB(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_l10n_obj     IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: FRECLASS_ITEM_EXISTS
-- Purpose      : This function will check if an item is already set for fiscal reclassification.
----------------------------------------------------------------------------------------------
FUNCTION FRECLASS_ITEM_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_item            IN       L10N_BR_FISCAL_RECLASS.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_OLD_VALUE
-- Purpose      : This function will retrieve the value for a particular attribute.
------------------------------------------------------------------------------------------------
FUNCTION GET_OLD_VALUE(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item                IN     ITEM_MASTER.ITEM%type,
                       I_name                IN     L10N_BR_FISCAL_RECLASS_NV.NAME%TYPE,
                       O_old_value           OUT    L10N_BR_FISCAL_RECLASS_NV.OLD_VALUE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_FRECLASS_CUSTOM_ATTRIB
-- Purpose      : This function deletes from l10n_br_fiscal_reclass_nv for input reclassification_id.
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_FRECLASS_CUSTOM_ATTRIB(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_reclass_id          IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE,
                                       I_name                IN     L10N_BR_FISCAL_RECLASS_NV.NAME%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function Name: CUSTOM_ATTRIB_EXISTS
-- Purpose      : This function checks if the input custom attribute is already set for the input fiscal reclassification.
---------------------------------------------------------------------------------------------------------------
FUNCTION CUSTOM_ATTRIB_EXISTS(O_error_message        IN OUT VARCHAR2,
                              O_exists               IN OUT BOOLEAN,
                              I_reclassification_id  IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE,
                              I_attrib_name          IN     L10N_BR_FISCAL_RECLASS_NV.NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQ_CUSTOM_ATTRIB
-- Purpose      : This function checks if the required custom attributes are entered for the fiscal reclassification.
---------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQ_CUSTOM_ATTRIB(O_error_message        IN OUT VARCHAR2,
                                 O_exists               IN OUT BOOLEAN,
                                 I_reclassification_id  IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE,
                                 I_base_rms_table       IN     EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_FRECLASS
-- Purpose      : This function deletes from l10n_br_fiscal_reclass and l10n_br_fiscal_reclass_nv for input reclassification_id.
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_FRECLASS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_reclass_id          IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_FRECLASS_EXISTS
-- Purpose      : This function checks if the input reclassification_id exists.
---------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FRECLASS_EXISTS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists              IN OUT BOOLEAN,
                               I_reclass_id          IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function Name: MERGE_FRECLASS
-- Purpose      : This function merges the values to the fiscal reclassification table from a collection.
---------------------------------------------------------------------------------------------------------------
FUNCTION MERGE_FRECLASS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_return_field_tbl    IN     L10N_FLEX_ATTRIB_VAL_SQL.TBL_attrib_val_tbl,
                        I_reclass_id          IN     L10N_BR_FISCAL_RECLASS_NV.RECLASSIFICATION_ID%TYPE,
                        I_item                IN     L10N_BR_FISCAL_RECLASS.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
END;
/
