create or replace PACKAGE FM_PRINT_FISCAL_DOC_SQL is
  
   ----------------------------------------------------------------------------------
   -- Function Name: SET_WHERE_CLAUSE
   -- Purpose:       This function returns the query for where clause of block.
   ----------------------------------------------------------------------------------
   FUNCTION SET_WHERE_CLAUSE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_where             IN OUT VARCHAR2,
                             I_schedule_no       IN FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                             I_fiscal_doc_no     IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                             I_type_id           IN FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                             I_fiscal_doc_id     IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

   ----------------------------------------------------------------------------------
      
end FM_PRINT_FISCAL_DOC_SQL;
/
