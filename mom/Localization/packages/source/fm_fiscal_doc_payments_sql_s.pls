CREATE OR REPLACE PACKAGE FM_FISCAL_DOC_PAYMENTS_SQL as
---------------------------------------------------------------------------------
-- CREATE DATE - 03-05-2007
-- CREATE USER ? Sigma.EPP
-- PROJECT / FRD - 10481 / 10481_ORFMI_FRD_001
-- DESCRIPTION ? Package associated to table FM_FISCAL_DOC_PAYMENTS.
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Function Name: EXISTS
-- Purpose:       This function checks if already exists.
----------------------------------------------------------------------------------
FUNCTION EXISTS(O_error_message  IN OUT VARCHAR2,
                O_exists         IN OUT BOOLEAN,
                I_fiscal_doc_id  IN     FM_FISCAL_DOC_PAYMENTS.FISCAL_DOC_ID%TYPE,
                I_payment_date   IN     FM_FISCAL_DOC_PAYMENTS.PAYMENT_DATE%TYPE)
   return BOOLEAN;


----------------------------------------------------------------------------------
END FM_FISCAL_DOC_PAYMENTS_SQL;
/
 