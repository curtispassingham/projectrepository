CREATE OR REPLACE PACKAGE BODY FM_EDI_DOC_HEADER_SQL is
------------------------------------------------------------------------
FUNCTION VALIDATE_SCHEDULE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,O_rma_exists     IN OUT BOOLEAN
                          ,O_edi_exists     IN OUT BOOLEAN
                          ,O_status         IN OUT FM_SCHEDULE.STATUS%TYPE
                          ,I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(50) := 'FM_EDI_DOC_HEADER_SQL.VALIDATE_SCHEDULE';
   L_dummy     NUMBER(1);
   ---
   cursor C_CHECK_SCHEDULE (P_req_type  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)is
      select '1'
        from fm_schedule fs,fm_fiscal_doc_header fdh
       where fs.schedule_no= I_schedule_no
         and fs.status in ('W','V','E')
         and fdh.schedule_no = fs.schedule_no
         and fdh.requisition_type =  P_req_type
         and fs.mode_type ='ENT';

   cursor C_GET_STATUS is
      select fs.status
        from fm_schedule fs
       where fs.schedule_no=I_schedule_no;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_STATUS','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   open C_GET_STATUS ;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_STATUS','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   fetch C_GET_STATUS  into O_status;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_STATUS','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   close C_GET_STATUS ;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_SCHEDULE','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   open C_CHECK_SCHEDULE('PO');
   ---
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SCHEDULE','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   fetch C_CHECK_SCHEDULE into L_dummy;
   O_edi_exists := C_CHECK_SCHEDULE%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SCHEDULE','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   close C_CHECK_SCHEDULE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_SCHEDULE','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   open C_CHECK_SCHEDULE('RMA');
   ---
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SCHEDULE','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   fetch C_CHECK_SCHEDULE into L_dummy;
   O_rma_exists := C_CHECK_SCHEDULE%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SCHEDULE','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
   close C_CHECK_SCHEDULE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_GET_STATUS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_GET_STATUS','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
         close C_GET_STATUS;
      end if;
      
      if C_CHECK_SCHEDULE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_SCHEDULE','FM_SCHEDULE','SCHEDULE NO: '||I_schedule_no);
         close C_CHECK_SCHEDULE;
      end if;
      
      return FALSE;
END VALIDATE_SCHEDULE;
--------------------------------------------------------------------------------
FUNCTION CHECK_EDI_RMA_LOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,O_rma_exists     IN OUT BOOLEAN
                          ,O_edi_exists     IN OUT BOOLEAN
                          ,I_loc_type       IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE
                          ,I_location       IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(50) := 'FM_EDI_DOC_HEADER_SQL.CHECK_EDI_RMA_LOC';
   L_dummy     NUMBER(1);
   ---
   cursor C_CHECK_EDI is
      select '1'
        from fm_edi_doc_header edh
       where process_status = 'U'
         and location_type = I_loc_type
         and location_id = I_location;

   cursor C_CHECK_RMA is
      select '1'
        from fm_rma_head frh
       where rma_status = 'U'
         and loc_type = I_loc_type
         and location = I_location;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_EDI','FM_EDI_DOC_HEADER','LOCATION ID: '||I_location);
   open C_CHECK_EDI ;
   ---
   SQL_LIB.SET_MARK('FETCH','C_CHECK_EDI','FM_EDI_DOC_HEADER','LOCATION ID: '||I_location);
   fetch C_CHECK_EDI  into L_dummy;
   O_edi_exists := C_CHECK_EDI%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_EDI','FM_EDI_DOC_HEADER','LOCATION ID: '||I_location);
   close C_CHECK_EDI ;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_RMA','FM_RMA_HEAD','LOCATION ID: '||I_location);
   open C_CHECK_RMA;
   ---
   SQL_LIB.SET_MARK('FETCH','C_CHECK_RMA','FM_RMA_HEAD','LOCATION ID: '||I_location);
   fetch C_CHECK_RMA into L_dummy;
   O_rma_exists := C_CHECK_RMA%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_RMA','FM_RMA_HEAD','LOCATION ID: '||I_location);
   close C_CHECK_RMA;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_CHECK_EDI%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_EDI','FM_EDI_DOC_HEADER','LOCATION ID: '||I_location);
         close C_CHECK_EDI;
      end if;
      
      if C_CHECK_RMA%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_RMA','FM_RMA_HEAD','LOCATION ID: '||I_location);
         close C_CHECK_RMA;
      end if;

      return FALSE;
END CHECK_EDI_RMA_LOC;
--------------------------------------------------------------------------------
FUNCTION EXISTS_FISCAL_DOC_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists         IN OUT BOOLEAN,
                              I_fiscal_doc_no  IN     FM_EDI_DOC_HEADER.FISCAL_DOC_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_EDI_DOC_HEADER_SQL.EXISTS_FISCAL_DOC_NO';
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_EDI is
      select '1'
        from fm_edi_doc_header
       where fiscal_doc_no = I_fiscal_doc_no
         and process_status = 'U';
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_EDI','FM_EDI_DOC_HEADER','EDI ID: '||I_fiscal_doc_no);
   open C_FM_EDI;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_EDI','FM_EDI_DOC_HEADER','EDI ID: '||I_fiscal_doc_no);
   fetch C_FM_EDI into L_dummy;
   O_exists := C_FM_EDI%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_EDI','FM_EDI_DOC_HEADER','EDI ID: '||I_fiscal_doc_no);
   close C_FM_EDI;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_FM_EDI%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_FM_EDI','FM_EDI_DOC_HEADER','EDI ID: '||I_fiscal_doc_no);
         close C_FM_EDI;
      end if;
      
      return FALSE;

END EXISTS_FISCAL_DOC_NO;
----------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                       I_status         IN     FM_EDI_DOC_HEADER.PROCESS_STATUS%TYPE,
                       I_nf_type_ind    IN     VARCHAR2 DEFAULT 'M')

   return BOOLEAN is
   --- 
   L_program      VARCHAR2(70) := 'FM_EDI_DOC_HEADER_SQL.UPDATE_STATUS';
   L_key          VARCHAR2(500) := 'I_edi_doc_id: '||I_edi_doc_id||' I_status '||I_status;
   L_compl_doc_id FM_EDI_DOC_COMPLEMENT.COMPL_EDI_DOC_ID%TYPE;
   RECORD_LOCKED  EXCEPTION;
   ---
   cursor C_EDI is
      select 'X'
        from fm_edi_doc_header
       where edi_doc_id = I_edi_doc_id
         for update nowait;
            
   cursor C_COMPL_EDI is
      select 'X'
        from fm_edi_doc_header
       where edi_doc_id in (select compl_edi_doc_id
                              from fm_edi_doc_complement
                             where edi_doc_id = I_edi_doc_id)
         for update nowait;
BEGIN
   if I_nf_type_ind = 'M' or I_nf_type_ind = 'B' then
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_EDI', 'FM_EDI_DOC_HEADER', L_key);
      open C_EDI;
      SQL_LIB.SET_MARK('CLOSE', 'C_EDI', 'FM_EDI_DOC_HEADER', L_key);
      close C_EDI;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_EDI_DOC_HEADER', L_key);
      update fm_edi_doc_header
         set process_status = I_status,
             process_date = DECODE(I_status, 'P', SYSDATE, NULL),
             last_update_datetime = SYSDATE,
             last_update_id = USER
       where edi_doc_id = I_edi_doc_id;
      ---
   end if;
      
   if I_nf_type_ind = 'C' or I_nf_type_ind = 'B' then
      ---
      L_key  := 'Compl NF for: '||I_edi_doc_id||' I_status '||I_status;
      SQL_LIB.SET_MARK('OPEN', 'C_COMPL_EDI', 'FM_EDI_DOC_HEADER', L_key);
      open C_COMPL_EDI;
      SQL_LIB.SET_MARK('CLOSE', 'C_COMPL_EDI', 'FM_EDI_DOC_HEADER', L_key);
      close C_COMPL_EDI;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_EDI_DOC_HEADER', L_key);
      update fm_edi_doc_header
         set process_status = I_status,
             process_date = DECODE(I_status, 'P', SYSDATE, NULL),
             last_update_datetime = SYSDATE,
             last_update_id = USER
       where edi_doc_id in (select compl_edi_doc_id
                              from fm_edi_doc_complement
                             where edi_doc_id = I_edi_doc_id);
      ---      
   end if;

   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_EDI_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_EDI%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_EDI', 'FM_EDI_DOC_HEADER', L_key);
         close C_EDI;
      end if;
      
      if C_COMPL_EDI%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_COMPL_EDI', 'FM_EDI_DOC_HEADER', L_key);
         close C_COMPL_EDI;
      end if;

      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_EDI%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_EDI', 'FM_EDI_DOC_HEADER', L_key);
         close C_EDI;
      end if;
      
      if C_COMPL_EDI%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_COMPL_EDI', 'FM_EDI_DOC_HEADER', L_key);
         close C_COMPL_EDI;
      end if;
      ---
      return FALSE;
END UPDATE_STATUS;
----------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                           I_fiscal_doc_id  IN     FM_EDI_DOC_HEADER.FISCAL_DOC_ID%TYPE)

   return BOOLEAN is
   ---
   L_program     VARCHAR2(70) := 'FM_EDI_DOC_HEADER_SQL.UPDATE_FISCAL_DOC';
   L_key         VARCHAR2(500) := 'I_edi_doc_id: '||I_edi_doc_id||' I_fiscal_doc_id: '|| I_fiscal_doc_id;
   RECORD_LOCKED EXCEPTION;
   ---
   cursor C_EDI is
      select 'X'
        from fm_edi_doc_header
       where edi_doc_id = I_edi_doc_id
         for update nowait;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EDI', 'FM_EDI_DOC_HEADER', L_key);
   open C_EDI;
   SQL_LIB.SET_MARK('CLOSE', 'C_EDI', 'FM_EDI_DOC_HEADER', L_key);
   close C_EDI;
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_EDI_DOC_HEADER', L_key);
   update fm_edi_doc_header
      set fiscal_doc_id = I_fiscal_doc_id,
          last_update_datetime = SYSDATE,
          last_update_id = USER
    where edi_doc_id = I_edi_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_EDI_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_EDI%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_EDI', 'FM_EDI_DOC_HEADER', L_key);
         close C_EDI;
      end if;

      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_EDI%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_EDI', 'FM_EDI_DOC_HEADER', L_key);
         close C_EDI;
      end if;
      ---
      return FALSE;
END UPDATE_FISCAL_DOC;
--------------------------------------------------------------------------------
FUNCTION EXISTS_LOC_UTIL_NFE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists         IN OUT BOOLEAN,
                             I_location       IN     FM_NFE_CONFIG_LOC.LOCATION%TYPE,
                             I_utilization    IN     FM_NFE_CONFIG_LOC.UTILIZATION_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_EDI_DOC_HEADER_SQL.EXISTS_LOC_UTIL_NFE';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(50);

   cursor C_LOC_UTIL_NFE is
      select '1'
        from fm_nfe_config_loc
       where location = I_location
         and utilization_id = I_utilization;

BEGIN
   L_key := 'utilization_id = '||I_utilization || ' location = ' || I_location;
   SQL_LIB.SET_MARK('OPEN', 'C_LOC_UTIL_NFE', 'FM_NFE_CONFIG_LOC', L_key);
   open C_LOC_UTIL_NFE;
   --
   SQL_LIB.SET_MARK('FETCH', 'C_LOC_UTIL_NFE', 'FM_NFE_CONFIG_LOC', L_key);
   fetch C_LOC_UTIL_NFE into L_dummy;
   --
   if C_LOC_UTIL_NFE%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   --
   SQL_LIB.SET_MARK('CLOSE', 'C_LOC_UTIL_NFE', 'FM_NFE_CONFIG_LOC', L_key);
   close C_LOC_UTIL_NFE;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_LOC_UTIL_NFE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOC_UTIL_NFE', 'FM_NFE_CONFIG_LOC', L_key);
         close C_LOC_UTIL_NFE;
      end if;

      return FALSE;
END EXISTS_LOC_UTIL_NFE;
--------------------------------------------------------------------------------
END FM_EDI_DOC_HEADER_SQL;
/
 
