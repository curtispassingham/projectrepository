CREATE OR REPLACE PACKAGE L10N_BR_FIN_SQL AS
----------------------------------------------------------------------------
--Function Name : CONSUME_RECEIPT
--Purpose       : This function will be called in RMS when the country is localized.
--                This function will route the receipt message coming from WMS/SIM 
--                in RMS to RFM.The RFM will process the Nota Fiscal for the receipt
--                and then will call  the RMSSUB_RECEIVING.CONSUME function to update
--                WAC, create receipt records and post appropriate tran data.
----------------------------------------------------------------------------
FUNCTION CONSUME_RECEIPT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
--Function Name : CONSUME_RTV
--Purpose       : This function will be called in RMS when the country is localized.
--                This function will route the RTV message in RMS to RFM.The RFM will 
--                process the Nota Fiscal for the RTV and then will call  the 
--                RMSSUB_RTV.CONSUME function to update WAC, create RTV records and 
--                post appropriate tran data.
----------------------------------------------------------------------------
FUNCTION CONSUME_RTV(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
--Function Name : CONSUME_INVADJ
--Purpose       : This function will be called in RMS when the country is localized.
--                This function will route the inventory adjustment message in RMS to RFM.
--                The RFM will process the Nota Fiscal for the inventory adjustment and
--                then will call the RMSSUB_INVADJ.CONSUME function to update WAC, create 
--                inventory adjustment records and post appropriate tran data.
----------------------------------------------------------------------------
FUNCTION CONSUME_INVADJ(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
--Function Name : CONSUME_SHIPMENT
--Purpose       : This function will be called in RMS when the country is localized.
--                This function will route the shipment message coming from WMS/SIM 
--                in RMS to RFM.The RFM will process the Nota Fiscal for the shipment
--                and then will call  the RMSSUB_ASNOUT.CONSUME function to update
--                WAC, create shipment records and post appropriate tran data.
----------------------------------------------------------------------------
FUNCTION CONSUME_SHIPMENT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------   
--Function Name : GET_LOC_AV_COST
--Purpose       : This function will calculate the AV_COST for locations with 
--                the same CNPJ.
----------------------------------------------------------------------------
FUNCTION GET_LOC_AV_COST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
--Function Name : GET_CURRENT_SOH_WAC
--Purpose       : This function will be used to compute the SOH of all locations 
--                with the same CNPJ. 
----------------------------------------------------------------------------
FUNCTION GET_CURRENT_SOH_WAC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
--Function Name : UPDATE_AV_COST
--Purpose       : This function will be used to update the AV_COST in item_loc_soh 
--                for all locations with the same CNPJ.
----------------------------------------------------------------------------
FUNCTION UPDATE_AV_COST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
--Function Name : POST_VAT
--Purpose       : This function will be used to not to post tran_codes 87,88 in tran_data
--                if location country is Brazil and is localized
----------------------------------------------------------------------------
FUNCTION POST_VAT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
--Function Name : RECOVERABLE_TAX_POSTING
--Purpose       : This function will post recoverable_tax to tran_data table.
--                Tran_code 74 is used to post recoverable tax for the destination
--                location and tran_code 75 posts recoverable tax for the source 
--                location. It also posts tran_code 30/32/37/38 with negative 
--                recovarable tax amount.
----------------------------------------------------------------------------
FUNCTION RECOVERABLE_TAX_POSTING(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
END L10N_BR_FIN_SQL;
/