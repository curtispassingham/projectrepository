CREATE OR REPLACE PACKAGE BODY FM_SCHEDULE_VAL_SQL is
   -------------------------------------------------------------------
   FUNCTION LOV_SCHEDULE_NO(O_error_message IN OUT VARCHAR2,
                            O_query         IN OUT VARCHAR2,
                            I_location      IN FM_SCHEDULE.LOCATION_ID%TYPE,
                            I_loc_type      IN FM_SCHEDULE.LOCATION_TYPE%TYPE,
                            I_where_clause  IN VARCHAR2) return BOOLEAN is
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.LOV_SCHEDULE_NO';
   BEGIN
      ---
      if I_where_clause is NOT NULL then
         O_query := 'Select schedule_no from fm_schedule fs
                   where fs.location_id = ' ||I_location || '
                     and fs.location_type = ''' ||I_loc_type || '''
                     and mode_type = ''' ||I_where_clause || ''' order by schedule_no';
      else
         O_query := 'Select schedule_no from fm_schedule fs
                   where fs.location_id = ' ||I_location || '
                     and fs.location_type = ''' ||I_loc_type || ''' order by schedule_no';
      end if;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END LOV_SCHEDULE_NO;
   -----------------------------------------------------------------------------------
   FUNCTION EXISTS_SCHEDULE(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                            I_mode_type      IN     FM_SCHEDULE.MODE_TYPE%TYPE,
                            I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE   DEFAULT NULL,
                            I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE DEFAULT NULL)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.EXISTS_SCHEDULE';
      L_dummy   VARCHAR2(1);
      L_status  VARCHAR2(1) := 'I';
      ---
      cursor C_FM_SCHEDULE is
         select 'x'
           from fm_schedule fs
          where fs.schedule_no = I_schedule_no
            and fs.mode_type = I_mode_type
            and ((I_location is NOT NULL and
                 fs.location_id = I_location) or
                 (I_location is NULL))
            and ((I_loc_type is NOT NULL and
                 fs.location_type = I_loc_type) or
                 (I_loc_type is NULL))
            and fs.status <> L_status;
      ---
   BEGIN
      ---
      open C_FM_SCHEDULE;
      ---
      fetch C_FM_SCHEDULE into L_dummy;
      O_exists := C_FM_SCHEDULE%FOUND;
      ---
      close C_FM_SCHEDULE;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END EXISTS_SCHEDULE;
   -----------------------------------------------------------------------------------
   FUNCTION GET_SCHEDULE_NUMBER_SEQ(O_error_message   IN OUT VARCHAR2,
                                    O_seq_schedule_no IN OUT NUMBER)

    return BOOLEAN is
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.GET_SCHEDULE_NUMBER_SEQ';
      ---
      cursor C_SEQ_SCHEDULE_NUMBER is
         select fm_schedule_seq.NEXTVAL seq_no from dual;
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_SEQ_SCHEDULE_NUMBER',
                       'SEQ_SCHEDULE_NUMBER',
                       NULL);
      open C_SEQ_SCHEDULE_NUMBER;
      SQL_LIB.SET_MARK('FETCH',
                       'C_SEQ_SCHEDULE_NUMBER',
                       'SEQ_SCHEDULE_NUMBER',
                       NULL);
      fetch C_SEQ_SCHEDULE_NUMBER
         into O_seq_schedule_no;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SEQ_SCHEDULE_NUMBER',
                       'SEQ_SCHEDULE_NUMBER',
                       NULL);
      close C_SEQ_SCHEDULE_NUMBER;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_SEQ_SCHEDULE_NUMBER%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_SEQ_SCHEDULE_NUMBER',
                             'SEQ_SCHEDULE_NUMBER',
                             NULL);
            close C_SEQ_SCHEDULE_NUMBER;
         end if;
         ---
         return FALSE;

   END GET_SCHEDULE_NUMBER_SEQ;
   -----------------------------------------------------------------------------------
   FUNCTION FISCAL_NO_DOC_EXISTS(O_error_message IN OUT VARCHAR2,
                                 O_exists        IN OUT BOOLEAN,
                                 I_schedule_no   IN NUMBER)

    return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.FISCAL_NO_DOC_EXISTS';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_dummy   VARCHAR2(1);
      ---
      cursor C_FISCAL_DOC_EXISTS is
         select 'X'
           from fm_fiscal_doc_header fh
          where fh.schedule_no = I_schedule_no
            and fh.status <> 'I';
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_DOC_EXISTS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FISCAL_DOC_EXISTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_DOC_EXISTS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_FISCAL_DOC_EXISTS
         into L_dummy;
      ---
      if C_FISCAL_DOC_EXISTS%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_DOC_EXISTS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FISCAL_DOC_EXISTS;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_FISCAL_DOC_EXISTS%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FISCAL_DOC_EXISTS',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FISCAL_DOC_EXISTS;
         end if;
         ---
         return FALSE;

   END FISCAL_NO_DOC_EXISTS;
   -----------------------------------------------------------------------------------
   FUNCTION UPDATE_SCHD_STATUS(O_error_message IN OUT VARCHAR2,
                               I_schedule_no    IN     NUMBER,
                               I_status         IN     FM_SCHEDULE.STATUS%TYPE)

   return BOOLEAN is
      ---
      RECORD_LOCKED   EXCEPTION;
      PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.UPDATE_SCHD_STATUS';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      ---
      cursor C_LOCK_SCHEDULE is
         select 'X'
           from fm_schedule s
          where s.schedule_no = I_schedule_no
            for update nowait;
      ---
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      if I_status is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_status',
                                               I_status,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_SCHEDULE',
                       'FM_SCHEDULE',
                       L_key);
      open C_LOCK_SCHEDULE;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_SCHEDULE',
                       'FM_SCHEDULE',
                       L_key);
      close C_LOCK_SCHEDULE;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_SCHEDULE', L_key);
      update fm_schedule s
         set s.status = DECODE(I_status,'CN','W',I_status)
       where s.schedule_no = I_schedule_no;
      ---
      return TRUE;
      ---
   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               'FM_SCHEDULE',
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_LOCK_SCHEDULE%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_LOCK_SCHEDULE',
                             'FM_SCHEDULE',
                             L_key);
            close C_LOCK_SCHEDULE;
         end if;
         ---
         return FALSE;

   END UPDATE_SCHD_STATUS;
--------------------------------------------------------------------------------
   FUNCTION FISCAL_NO_DOC_WI(O_error_message IN OUT VARCHAR2,
                             O_exists        IN OUT BOOLEAN,
                             I_schedule_no   IN NUMBER)

    return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.FISCAL_NO_DOC_WI';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_dummy   VARCHAR2(1);
      ---
      cursor C_FISCAL_DOC_STATUS is
         select fh.status
           from fm_fiscal_doc_header fh
          where fh.schedule_no = I_schedule_no;
      ---
      R_fiscal_doc_status FM_FISCAL_DOC_HEADER.STATUS%TYPE;
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      O_exists := TRUE;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_DOC_STATUS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FISCAL_DOC_STATUS;
      ---
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_FISCAL_DOC_STATUS',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_FISCAL_DOC_STATUS
            into R_fiscal_doc_status;
         EXIT when C_FISCAL_DOC_STATUS%NOTFOUND;
         ---
         if R_fiscal_doc_status NOT IN ('W','I') then
            O_exists := FALSE;
         end if;
         ---
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_DOC_STATUS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FISCAL_DOC_STATUS;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_FISCAL_DOC_STATUS%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FISCAL_DOC_STATUS',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FISCAL_DOC_STATUS;
         end if;
         ---
         return FALSE;

   END FISCAL_NO_DOC_WI;
   -----------------------------------------------------------------------------------
   FUNCTION UPDATE_STATUS_FISCAL_DOC(O_error_message IN OUT VARCHAR2,
                                     I_location      IN ITEM_LOC.LOC%TYPE,
                                     I_loc_type      IN ITEM_LOC.LOC_TYPE%TYPE,
                                     I_schedule_no   IN FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                                     I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                     I_status        IN FM_FISCAL_DOC_HEADER.STATUS%TYPE)

    return BOOLEAN is
      ---
      RECORD_LOCKED   EXCEPTION;
      PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_status  VARCHAR2(1) := 'I';
      ---
      cursor C_LOCK_DOC_HEADER is
         select 'X'
           from fm_fiscal_doc_header fh
          where fh.schedule_no = I_schedule_no
            and fh.location_id = I_location
            and fh.location_type = I_loc_type
            and ((I_fiscal_doc_id is NOT NULL and
                fh.fiscal_doc_id = I_fiscal_doc_id) or
                (I_fiscal_doc_id is NULL))
            and fh.status <> L_status
            for update nowait;
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      if I_status is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_status',
                                               I_status,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_LOCK_DOC_HEADER;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_LOCK_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_FISCAL_DOC_HEADER', L_key);
      update fm_fiscal_doc_header fh
         set fh.status = I_status
       where fh.schedule_no = I_schedule_no
         and fh.location_id = I_location
         and fh.location_type = I_loc_type
         and ((I_fiscal_doc_id is NOT NULL and
             fh.fiscal_doc_id = I_fiscal_doc_id) or
             (I_fiscal_doc_id is NULL))
         and fh.status <> L_status;
      ---
      return TRUE;
      ---
   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               'FM_FISCAL_DOC_HEADER',
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_LOCK_DOC_HEADER%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_LOCK_DOC_HEADER',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_LOCK_DOC_HEADER;
         end if;
         ---
         return FALSE;

   END UPDATE_STATUS_FISCAL_DOC;
   -----------------------------------------------------------------------------------
   FUNCTION WORKSHEET_SCHED(O_error_message IN OUT VARCHAR2,
                            I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(100) := 'FM_SCHEDULE_VAL_SQL.WORKSHEET_SCHED';
      L_key     VARCHAR2(100) := 'schedule_no: ' || TO_CHAR(I_schedule_no);
      ---
      cursor C_FM_FISCAL_DOC_HEADER is
         select fdd.fiscal_doc_id, fdd.location_id, fdd.location_type
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status IN ('V', 'D', 'E');
      ---
      R_fiscal_doc_header  C_FM_FISCAL_DOC_HEADER%ROWTYPE;
      ---
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FM_FISCAL_DOC_HEADER;
      ---
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_FM_FISCAL_DOC_HEADER',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_FM_FISCAL_DOC_HEADER
            into R_fiscal_doc_header ;
         EXIT when C_FM_FISCAL_DOC_HEADER%NOTFOUND;
         ---
         if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                         R_fiscal_doc_header.location_id,
                                                         R_fiscal_doc_header.location_type,
                                                         I_schedule_no,
                                                         R_fiscal_doc_header.fiscal_doc_id,
                                                         'W') = FALSE then
            return FALSE;
         end if;
         ---
         --- Delete the discrepancy flags , resolution action , tax tables and clear calculated columns during validation
         if FM_FISCAL_DOC_HEADER_SQL.RESET_WORKSHEET(O_error_message,
                                                     R_fiscal_doc_header.fiscal_doc_id) = FALSE then
             return FALSE;
         end if;
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_FM_FISCAL_DOC_HEADER%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FM_FISCAL_DOC_HEADER',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FM_FISCAL_DOC_HEADER;
         end if;
         ---
         return FALSE;
   END WORKSHEET_SCHED;
   -----------------------------------------------------------------------------------
   FUNCTION ALL_FISCAL_DOC_INACTIVE(O_error_message  IN OUT VARCHAR2,
                                    O_exists         IN OUT BOOLEAN,
                                    I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.ALL_FISCAL_DOC_INACTIVE';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      ---
      cursor C_FISCAL_DOC_STATUS is
         select count(1)
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status <> 'I';
      ---
      L_count NUMBER;
      ---
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      O_exists := TRUE;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_DOC_STATUS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FISCAL_DOC_STATUS;
      ---
      SQL_LIB.SET_MARK('FETCH',
                          'C_FISCAL_DOC_STATUS',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
      fetch C_FISCAL_DOC_STATUS
            into L_count;
      ---
      if L_count > 0 then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_DOC_STATUS',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FISCAL_DOC_STATUS;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_FISCAL_DOC_STATUS%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FISCAL_DOC_STATUS',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FISCAL_DOC_STATUS;
         end if;
         ---
         return FALSE;

   END ALL_FISCAL_DOC_INACTIVE;
   -----------------------------------------------------------------------------------
   FUNCTION VALIDATE_SCHED(O_error_message IN OUT VARCHAR2,
                           O_status        IN OUT FM_SCHEDULE.STATUS%TYPE,
                           I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.VALIDATE_SCHED';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_dummy   VARCHAR2(1);
      L_match_opr_type        V_FISCAL_ATTRIBUTES.MATCH_OPR_TYPE%TYPE;
      L_cost_disp             FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE := 'N';
      L_qty_disp              FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE  := 'N';
      L_tax_disp              FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE  := 'N';
      L_other_cost_disp       FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE := 'N';
      L_other_qty_disp        FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE  := 'N';
      L_other_tax_disp        FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE  := 'N';
      L_comp_nf_ind           FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
      L_triang_ind            ORDHEAD.TRIANGULATION_IND%TYPE;
      L_other_nf_id           FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
     
      L_status_process INTEGER;
      ---
      cursor C_FM_FISCAL_DOC_HEADER is
         select fdd.fiscal_doc_id, fdd.location_id, fdd.location_type,fs.mode_type,fdd.utilization_id,fdd.requisition_type
           from fm_fiscal_doc_header fdd,
                fm_schedule fs
          where fdd.schedule_no = I_schedule_no
            and fdd.schedule_no = fs.schedule_no
            and fdd.status = 'W';
      ---
      R_fiscal_doc_header  C_FM_FISCAL_DOC_HEADER%ROWTYPE;
      ---
      cursor C_EXISTS_ERROR is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = 'E';
      ---
      cursor C_EXISTS_DISP is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = 'D';
     ---
      cursor C_FM_EXIST_FISCAL_DOC is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
	    and fdd.status != 'CN';
      ---
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      --- Verify if exists fiscal docs
      SQL_LIB.SET_MARK('OPEN',
                       'C_FM_EXIST_FISCAL_DOC',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FM_EXIST_FISCAL_DOC;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FM_EXIST_FISCAL_DOC',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_FM_EXIST_FISCAL_DOC into L_dummy;
      ---
      if C_FM_EXIST_FISCAL_DOC%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_NOT_FISCAL_DOCS',
                                               NULL,
                                               NULL,
                                               NULL);
         O_status := 'W';
         SQL_LIB.SET_MARK('CLOSE',
                          'C_FM_EXIST_FISCAL_DOC',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         close C_FM_EXIST_FISCAL_DOC;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FM_EXIST_FISCAL_DOC',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FM_EXIST_FISCAL_DOC;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
    open C_FM_FISCAL_DOC_HEADER;
      ---
    LOOP
         ---
       SQL_LIB.SET_MARK('FETCH',
                          'C_FM_FISCAL_DOC_HEADER',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
       fetch C_FM_FISCAL_DOC_HEADER into R_fiscal_doc_header ;
         ---
       EXIT when C_FM_FISCAL_DOC_HEADER%NOTFOUND;
         ---
       if R_fiscal_doc_header.mode_type = 'EXIT' then
          if FM_EXIT_NF_CREATION_SQL.PROCESS_OUTBOUND(O_error_message,
                             L_status_process,
                             R_fiscal_doc_header.fiscal_doc_id,
                             R_fiscal_doc_header.utilization_id,
                             I_schedule_no,
                             R_fiscal_doc_header.requisition_type,
                             'Y') = FALSE then
            return FALSE;
         end if;
       else
         if FM_FISCAL_VALIDATION_SQL.PROCESS_VALIDATION(O_error_message,
                                                        L_status_process,
                                                        R_fiscal_doc_header.fiscal_doc_id) = FALSE then
            O_status := 'W';
            return FALSE;
            ---
         else --- Validation OK
            if L_status_process = 1 then
               -- check if location is centralize or decentralize
               if FM_DIS_RESOLUTION_SQL.GET_FM_LOC_OPR(O_error_message,
                                                       R_fiscal_doc_header.location_id,
                                                       R_fiscal_doc_header.location_type,
                                                       L_match_opr_type) = FALSE then
                  O_status := 'W';
                  return FALSE;
               end if;
              -- check if discrepant
                 if FM_FISCAL_DOC_HEADER_SQL.CHECK_DISCREPANCY(O_error_message,
                                                               R_fiscal_doc_header.fiscal_doc_id,
                                                               L_cost_disp,
                                                               L_qty_disp,
                                                               L_tax_disp) = FALSE then
                 O_status := 'W';
                 return FALSE;
              end if;
              if (FM_FISCAL_VALIDATION_SQL.GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, R_fiscal_doc_header.fiscal_doc_id) = FALSE) then
                 return FALSE;
              else
                 if L_triang_ind = 'Y' then
                    ---
                    if (FM_FISCAL_VALIDATION_SQL.GET_OTHER_NF(O_error_message, L_other_nf_id, L_comp_nf_ind, R_fiscal_doc_header.fiscal_doc_id) = FALSE) then
                       return FALSE;
                    end if;
                    ---
                    if FM_FISCAL_DOC_HEADER_SQL.CHECK_DISCREPANCY(O_error_message,
                                                                  L_other_nf_id,
                                                                  L_other_cost_disp,
                                                                  L_other_qty_disp,
                                                                  L_other_tax_disp) = FALSE then
                       O_status := 'W';
                       return FALSE;
                    end if;
                    ---
                 end if;
                 ---
              end if;
              ---

              if L_triang_ind = 'N' then
                 -- based on location type set discrepant or validated
                 If L_match_opr_type = 'D' then -- de-centralize location
                    If L_cost_disp = 'Y' or L_qty_disp= 'Y' or L_tax_disp= 'Y' then
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header .fiscal_doc_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;

                    else
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header .fiscal_doc_id,
                                                                       'V') = FALSE then
                          return FALSE;
                       end if;
                    end if;
                 else -- centralize location
                    If L_qty_disp= 'Y' then
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header.location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header.fiscal_doc_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;

                    else
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                             R_fiscal_doc_header.location_id,
                             R_fiscal_doc_header.location_type,
                             I_schedule_no,
                             R_fiscal_doc_header.fiscal_doc_id,
                             'V') = FALSE then
                           return FALSE;
                       end if;

                    end if;
                 end if;
                 ---
              elsif L_triang_ind = 'Y' then
                 ---
                 If L_match_opr_type = 'D' then -- de-centralize location
                    If (L_cost_disp = 'Y' or L_qty_disp= 'Y') or (L_other_cost_disp = 'Y' or L_other_qty_disp= 'Y') then
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header .fiscal_doc_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;
                       ---
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       L_other_nf_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;
                       ---
                    elsif L_tax_disp= 'Y' and L_other_tax_disp = 'N'then
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header .fiscal_doc_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;
                       ---
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       L_other_nf_id,
                                                                       'V') = FALSE then
                          return FALSE;
                       end if;
                       ---
                    elsif L_tax_disp= 'N' and L_other_tax_disp = 'Y'then
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header .fiscal_doc_id,
                                                                       'V') = FALSE then
                          return FALSE;
                       end if;
                       ---
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       L_other_nf_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;
                       ---
                    elsif L_tax_disp= 'Y' and L_other_tax_disp = 'Y'then
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header .fiscal_doc_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;
                       ---
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       L_other_nf_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;
                       ---
                    elsif L_tax_disp= 'N' and L_other_tax_disp = 'N'then
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header .fiscal_doc_id,
                                                                       'V') = FALSE then
                          return FALSE;
                       end if;
                       ---
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header .location_type,
                                                                       I_schedule_no,
                                                                       L_other_nf_id,
                                                                       'V') = FALSE then
                          return FALSE;
                       end if;
                       ---
                    end if;
                 else -- centralize location
                    If L_qty_disp= 'Y' or L_other_qty_disp= 'Y' then
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header.location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header.fiscal_doc_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;
                       ---
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header.location_type,
                                                                       I_schedule_no,
                                                                       L_other_nf_id,
                                                                       'D') = FALSE then
                          return FALSE;
                       end if;
                       ---

                    else
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header.location_type,
                                                                       I_schedule_no,
                                                                       R_fiscal_doc_header.fiscal_doc_id,
                                                                       'V') = FALSE then
                           return FALSE;
                       end if;
                       ---
                       if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                       R_fiscal_doc_header.location_id,
                                                                       R_fiscal_doc_header.location_type,
                                                                       I_schedule_no,
                                                                       L_other_nf_id,
                                                                       'V') = FALSE then
                           return FALSE;
                       end if;
                    end if;
                 end if;
                 ---
              end if;
              ---
            else -- L_status_process = 0
               ---
               if (FM_FISCAL_VALIDATION_SQL.GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, R_fiscal_doc_header.fiscal_doc_id) = FALSE) then
                  return FALSE;
               end if;
               ---
               if L_triang_ind = 'N' then
                  if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                  R_fiscal_doc_header.location_id,
                                                                  R_fiscal_doc_header.location_type,
                                                                  I_schedule_no,
                                                                  R_fiscal_doc_header.fiscal_doc_id,
                                                                  'E') = FALSE then
                     return FALSE;
                  end if;
                  ---
               elsif L_triang_ind = 'Y' then
                  ---
                  if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                  R_fiscal_doc_header.location_id,
                                                                  R_fiscal_doc_header.location_type,
                                                                  I_schedule_no,
                                                                  R_fiscal_doc_header.fiscal_doc_id,
                                                                  'E') = FALSE then
                     return FALSE;
                  end if;
                  ---
                  if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                                  R_fiscal_doc_header.location_id,
                                                                  R_fiscal_doc_header.location_type,
                                                                  I_schedule_no,
                                                                  L_other_nf_id,
                                                                  'E') = FALSE then
                     return FALSE;
                  end if;
                  ---
               end if;
               ---
            end if;
         end if;
         ---
         
         --sid for triang
         if L_triang_ind = 'Y' then
            EXIT;
         end if;
      end if; 
         ---
         --sid for triang
      END LOOP;
      ---
    if R_fiscal_doc_header.mode_type = 'ENT' then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FM_FISCAL_DOC_HEADER;

      --- Check is exists any NF with error status

      SQL_LIB.SET_MARK('OPEN',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                        L_key);
      open C_EXISTS_ERROR;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                        L_key);
      fetch C_EXISTS_ERROR into L_dummy;
      if C_EXISTS_ERROR%FOUND then
         O_status := 'E';
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                        L_key);
      close C_EXISTS_ERROR;

     --- Check is exists any NF with Discrepancy status

      if O_status not in ('E') then
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_EXISTS_DISP',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         open C_EXISTS_DISP;
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_EXISTS_DISP',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_EXISTS_DISP into L_dummy;
         if C_EXISTS_DISP%FOUND then
            O_status := 'D';
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_DISP',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         close C_EXISTS_DISP;

      end if;

       if O_status not in ('E','D') then
            O_status := 'V';
       end if;

    end if;
      ---
    return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_FM_FISCAL_DOC_HEADER%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FM_FISCAL_DOC_HEADER',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FM_FISCAL_DOC_HEADER;
         end if;
         ---
         if C_EXISTS_ERROR%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_ERROR',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_ERROR;
         end if;
         ---
         if C_EXISTS_DISP%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_DISP',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_ERROR;
         end if;
         ---
         return FALSE;
   END VALIDATE_SCHED;
   -----------------------------------------------------------------------------------
   FUNCTION MATCH_SCHED(O_error_message IN OUT VARCHAR2,
                        O_status        IN OUT FM_SCHEDULE.STATUS%TYPE,
                        I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.MATCH_SCHED';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_dummy   VARCHAR2(1);
      ---
      L_status_process INTEGER;
      ---
      cursor C_FM_FISCAL_DOC_HEADER is
         select fdd.fiscal_doc_id, fdd.location_id, fdd.location_type
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = 'V';
      ---
      R_fiscal_doc_header  C_FM_FISCAL_DOC_HEADER%ROWTYPE;
      ---
      cursor C_EXISTS_ERROR is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status = 'E';
      ---
      cursor C_NOT_ALL_FISCAL_DOC_VALIDATED is
         select 'X'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.status IN ('W');
      ---
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_EXISTS_ERROR;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_EXISTS_ERROR into L_dummy;
      if C_EXISTS_ERROR%FOUND then
         O_status := 'E';
         SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
         close C_EXISTS_ERROR;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS_ERROR',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_EXISTS_ERROR;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_NOT_ALL_FISCAL_DOC_VALIDATED',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_NOT_ALL_FISCAL_DOC_VALIDATED;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_NOT_ALL_FISCAL_DOC_VALIDATED',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_NOT_ALL_FISCAL_DOC_VALIDATED into L_dummy;
      if C_NOT_ALL_FISCAL_DOC_VALIDATED%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NOT_ALL_FISCAL_DOC_VAL',
                                               NULL,
                                               NULL,
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE',
                       'C_NOT_ALL_FISCAL_DOC_VALIDATED',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
         close C_NOT_ALL_FISCAL_DOC_VALIDATED;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_NOT_ALL_FISCAL_DOC_VALIDATED',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_NOT_ALL_FISCAL_DOC_VALIDATED;
      ---
      O_status := 'M';
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FM_FISCAL_DOC_HEADER;
      ---
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_FM_FISCAL_DOC_HEADER',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_FM_FISCAL_DOC_HEADER
            into R_fiscal_doc_header ;
         EXIT when C_FM_FISCAL_DOC_HEADER%NOTFOUND;
         ---
         /*commented for code drop 1
         if ORFM_VALIDATIONS_SQL.MATCH_NF(O_error_message,
                                              L_status_process,
                                              R_fiscal_doc_header .fiscal_doc_id) =
            FALSE then
            O_status := 'E';
            ---
            if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                            R_fiscal_doc_header .location_id,
                                                            R_fiscal_doc_header .location_type,
                                                            I_schedule_no,
                                                            R_fiscal_doc_header .fiscal_doc_id,
                                                            'E') = FALSE then
               return FALSE;
            end if;
            ---
         else
            if L_status_process = 1 then
               --- Validation OK
               ---
               if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                               R_fiscal_doc_header .location_id,
                                                               R_fiscal_doc_header .location_type,
                                                               I_schedule_no,
                                                               R_fiscal_doc_header .fiscal_doc_id,
                                                               'M') = FALSE then
                  return FALSE;
               end if;
               ---
            else
               O_status := 'E';
               ---
               if FM_SCHEDULE_VAL_SQL.UPDATE_STATUS_FISCAL_DOC(O_error_message,
                                                               R_fiscal_doc_header .location_id,
                                                               R_fiscal_doc_header .location_type,
                                                               I_schedule_no,
                                                               R_fiscal_doc_header .fiscal_doc_id,
                                                               'E') = FALSE then
                  return FALSE;
               end if;
               ---
            end if;
         end if;
         */
         ---
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      -- Pack validations...
      /*commented for code drop 1
      if ORFM_VALIDATIONS_SQL.MATCH_PO_SCHEDULE(O_error_message,
                                                L_status_process,
                                                I_schedule_no) = FALSE then
         O_status := 'E';
         return FALSE;
      end if;
      if L_status_process != 1 then
         ---
         O_status := 'E';
         ---
      end if;
      */
      --- Check is exists any NF with error status
      if O_status = 'M' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_EXISTS_ERROR',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         open C_EXISTS_ERROR;
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_EXISTS_ERROR',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_EXISTS_ERROR into L_dummy;
         if C_EXISTS_ERROR%FOUND then
            O_status := 'E';
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS_ERROR',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         close C_EXISTS_ERROR;
      end if;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_FM_FISCAL_DOC_HEADER%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FM_FISCAL_DOC_HEADER',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FM_FISCAL_DOC_HEADER;
         end if;
         ---
         if C_EXISTS_ERROR%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_ERROR',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_ERROR;
         end if;
         ---
         if C_NOT_ALL_FISCAL_DOC_VALIDATED%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_NOT_ALL_FISCAL_DOC_VALIDATED',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_NOT_ALL_FISCAL_DOC_VALIDATED;
         end if;
         ---
         return FALSE;
   END MATCH_SCHED;
   -----------------------------------------------------------------------------------
FUNCTION APPROVE_SCHED(O_error_message IN OUT VARCHAR2,
                        I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
    return BOOLEAN is
    ---
    L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.APPROVE_SCHED';
    L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
    L_dummy   VARCHAR2(1);
    ---
    L_requisition_type  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
    L_fiscal_doc_id     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
    ---
    cursor C_FM_FISCAL_DOC_HEADER is
       select fdd.fiscal_doc_id,
              fdd.requisition_type
         from fm_fiscal_doc_header fdd, fm_schedule fs
        where fs.schedule_no = fdd.schedule_no
          and fdd.schedule_no = I_schedule_no
          and fdd.status in ('V','F');
    ---
BEGIN
      ---
    if I_schedule_no is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_schedule_no',
                                             I_schedule_no,
                                             NULL);
       return FALSE;
    end if;
    ---
    SQL_LIB.SET_MARK('OPEN','C_FM_FISCAL_DOC_HEADER','FM_FISCAL_DOC_HEADER',L_key);
    open C_FM_FISCAL_DOC_HEADER;
    ---
    SQL_LIB.SET_MARK('FETCH','C_FM_FISCAL_DOC_HEADER','FM_FISCAL_DOC_HEADER',L_key);
    fetch C_FM_FISCAL_DOC_HEADER  into L_fiscal_doc_id,L_requisition_type ;

    SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DOC_HEADER','FM_FISCAL_DOC_HEADER',L_key);
    close C_FM_FISCAL_DOC_HEADER;
    if FM_EXIT_NF_CREATION_SQL.SCHEDULE_APPROVAL(O_error_message,
                                                 L_fiscal_doc_id,
                                                 I_schedule_no,
                                                 L_requisition_type) = FALSE then
       return FALSE;
    end if;

    return TRUE;
    ---
EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1, 254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
       ---
       if C_FM_FISCAL_DOC_HEADER%ISOPEN then
          SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DOC_HEADER','FM_FISCAL_DOC_HEADER',L_key);
          close C_FM_FISCAL_DOC_HEADER;
       end if;
    return FALSE;
END APPROVE_SCHED;
   -----------------------------------------------------------------------------------
   FUNCTION INSERT_NON_EXIST_DOCS(O_error_message IN OUT VARCHAR2,
                                  I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      PRAGMA AUTONOMOUS_TRANSACTION;
      ---
      RECORD_LOCKED   EXCEPTION;
      PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
      ---
      L_program VARCHAR2(80) := 'FM_SCHEDULE_VAL_SQL.INSERT_NON_EXIST_DOCS';
      L_key     VARCHAR2(80) := 'I_schedule_no: ' || I_schedule_no;
      ---
      L_exists         BOOLEAN;
      ---
      cursor C_FM_FISCAL_DOC_HEADER is
         select fdd.fiscal_doc_id,
                fdd.location_id,
                fdd.location_type,
                fdd.fiscal_doc_no
           from fm_fiscal_doc_header fdd
               ,fm_utilization_attributes att
          where fdd.utilization_id = att.utilization_id
--            and att.allow_print_ind = 'Y'
            and fdd.schedule_no = I_schedule_no
            and fdd.status IN ('A','C');
      ---
      R_fiscal_doc_header  C_FM_FISCAL_DOC_HEADER%ROWTYPE;

   BEGIN
      ---
       SQL_LIB.SET_MARK('OPEN',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FM_FISCAL_DOC_HEADER;
      ---
      LOOP
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_FM_FISCAL_DOC_HEADER',
                          'FM_FISCAL_DOC_HEADER',
                          L_key);
         fetch C_FM_FISCAL_DOC_HEADER
            into R_fiscal_doc_header ;
         EXIT when C_FM_FISCAL_DOC_HEADER%NOTFOUND;
         ---

         /*commented for code drop 1
         if FM_PRINTER_QUEUE_SQL.EXIST_KEY (O_error_message,
                                            L_exists,
                                            R_fiscal_doc_header.fiscal_doc_id,
                                            I_schedule_no) = FALSE then
            return FALSE;
         end if;
         ---

         if NOT L_exists then
            -- Insert non existent values in fm_printer_queue table
            if FM_PRINTER_QUEUE_SQL.INSERT_QUEUE(O_error_message,
                                                 R_fiscal_doc_header.fiscal_doc_no,
                                                 R_fiscal_doc_header.fiscal_doc_id,
                                                 I_schedule_no) = FALSE then
               return FALSE;
            end if;
         end if;
         */

         ---
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      COMMIT;
      ---
      return TRUE;
      ---
   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               'FM_FISCAL_DOC_HEADER',
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_FM_FISCAL_DOC_HEADER%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FM_FISCAL_DOC_HEADER',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FM_FISCAL_DOC_HEADER;
         end if;

         ---
         return FALSE;
   END INSERT_NON_EXIST_DOCS;
   -----------------------------------------------------------------------------------

   FUNCTION SET_WHERE_CLAUSE(O_error_message IN OUT VARCHAR2,
                          O_where         IN OUT VARCHAR2,
                          I_location_id   IN FM_SCHEDULE.LOCATION_ID%TYPE,
                          I_location_type IN FM_SCHEDULE.LOCATION_TYPE%TYPE,
                          I_mode_type     IN FM_SCHEDULE.MODE_TYPE%TYPE,
                          I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE,
--                          I_schedule_date IN FM_SCHEDULE.SCHEDULE_DATE%TYPE,
                          I_start_date    IN FM_SCHEDULE.SCHEDULE_DATE%TYPE,
                          I_end_date      IN FM_SCHEDULE.SCHEDULE_DATE%TYPE,
                          I_status        IN FM_SCHEDULE.STATUS%TYPE
                          )
   return BOOLEAN is
   ---
   C_INACTIVE_STATUS   CONSTANT FM_SCHEDULE.STATUS%TYPE := 'I';
   L_program           VARCHAR2(100)  := 'FM_SCHEDULE_VAL_SQL.SET_WHERE_CLAUSE';
   ---
   BEGIN
      ---
      if I_location_id is NOT NULL then
         O_where := O_where || ' scr.location_id = '||I_location_id||' and ';
      end if;
      ---
      if I_location_type is NOT NULL then
         O_where := O_where || ' scr.location_type = '''||I_location_type||''' and ';
      end if;
      ---
      if I_mode_type is NOT NULL then
         O_where := O_where || ' scr.mode_type = '''||I_mode_type||''' and ';
      end if;
      ---
      if I_schedule_no is NOT NULL then
         O_where := O_where || ' scr.schedule_no = '''||I_schedule_no||''' and ';
      end if;
      ---
      /*if I_schedule_date is NOT NULL then
         O_where := O_where || ' scr.schedule_date = '''||I_schedule_date||''' and ';
      end if;
      ---*/
      if I_start_date is NOT NULL then
         O_where := O_where || ' scr.schedule_date >= '''||I_start_date||''' and ';
      end if;
      ---
      if I_end_date is NOT NULL then
         O_where := O_where || ' scr.schedule_date <= '''||I_end_date||''' and ';
      end if;
      ---
      if I_status is NOT NULL then
         O_where := O_where || ' scr.status = '''||I_status||''' and ';
      end if;
      ---
      if I_status is NULL then
         O_where := O_where || ' scr.status <> '''||C_INACTIVE_STATUS||''' and ';
      end if;
      ---

      O_where := SUBSTR(O_where,1,LENGTH(O_where)-4);

      return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END SET_WHERE_CLAUSE;

------------------------------------------------------------------------------------
FUNCTION VALIDATE_REQ_TYPE(O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(80) := 'FM_SCHEDULE_VAL_SQL.VALIDATE_REQ_TYPE';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      L_dummy   VARCHAR2(1);
      ---
      cursor C_FM_FISCAL_DOC_HEADER is
         select '1'
           from fm_fiscal_doc_header fdd
          where fdd.schedule_no = I_schedule_no
            and fdd.requisition_type != 'PO';
      ---
   BEGIN
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;

      --- Verify if exists fiscal docs
      SQL_LIB.SET_MARK('OPEN',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      open C_FM_FISCAL_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      fetch C_FM_FISCAL_DOC_HEADER into L_dummy;
      ---
      if C_FM_FISCAL_DOC_HEADER%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FM_FISCAL_DOC_HEADER',
                       'FM_FISCAL_DOC_HEADER',
                       L_key);
      close C_FM_FISCAL_DOC_HEADER;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_FM_FISCAL_DOC_HEADER%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FM_FISCAL_DOC_HEADER',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FM_FISCAL_DOC_HEADER;
         end if;
         ---
         return FALSE;
END VALIDATE_REQ_TYPE;
-----------------------------------------------------------------------------------
   -- 001 - Begin
      FUNCTION VALIDATE_APROVE_SCHED(O_error_message IN OUT VARCHAR2,
                                  O_status        IN OUT BOOLEAN,
                                  I_schedule_no   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_SCHEDULE_VAL_SQL.VALIDATE_APROVE_SCHED';
      L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
      ---
      L_description    FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
      L_number_value   FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
      L_string_value   FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
      L_date_value     FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
      ---
      cursor C_FM_VALIDATE_APROVE is
         select distinct fdh.fiscal_doc_id
           from fm_schedule fs, fm_fiscal_doc_header fdh
          where fs.schedule_no = fdh.schedule_no
            and fs.schedule_no = I_schedule_no
            and fdh.status NOT in ('A','P','R','I');
      ---
      R_fiscal_doc_header  C_FM_VALIDATE_APROVE%ROWTYPE;
   BEGIN
      ---
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_description,
                                            L_number_value,
                                            L_string_value,
                                            L_date_value,
                                            'CHAR',
                                            'CHECK_APPROVED_DOCS') = FALSE then
         return FALSE;
      end if;
      ---
      if I_schedule_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_schedule_no',
                                               I_schedule_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      if L_string_value = 'Y' then
         SQL_LIB.SET_MARK('OPEN','C_FM_VALIDATE_APROVE','FM_FISCAL_DOC_HEADER',L_key);
         open C_FM_VALIDATE_APROVE;
         SQL_LIB.SET_MARK('FETCH','C_FM_VALIDATE_APROVE','FM_FISCAL_DOC_HEADER',L_key);
         fetch C_FM_VALIDATE_APROVE into R_fiscal_doc_header;
         ---
         if C_FM_VALIDATE_APROVE%FOUND then
            O_status := FALSE;
         else
            O_status := TRUE;
         end if;
         ---
      else
         O_status := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_FM_VALIDATE_APROVE','FM_FISCAL_DOC_HEADER',L_key);
      close C_FM_VALIDATE_APROVE;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
         if C_FM_VALIDATE_APROVE%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FM_VALIDATE_APROVE',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_FM_VALIDATE_APROVE;
         end if;
         ---
         return FALSE;
   END VALIDATE_APROVE_SCHED;
   -- 001 - End
   -----------------------------------------------------------------------------------
END FM_SCHEDULE_VAL_SQL;
/