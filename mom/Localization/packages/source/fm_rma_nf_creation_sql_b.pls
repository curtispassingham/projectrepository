CREATE OR REPLACE PACKAGE BODY FM_RMA_NF_CREATION_SQL is
--------------------------------------------------------------------------------
 FUNCTION EXISTS_RMA(O_error_message  IN OUT VARCHAR2,
                     O_exists         IN OUT BOOLEAN,
                     I_rma_id         IN     FM_RMA_HEAD.RMA_ID%TYPE)
     return BOOLEAN is
     ---
     L_program   VARCHAR2(50) := 'FM_RMA_NF_CREATION_SQL.EXISTS_RMA';
     L_dummy     VARCHAR2(1);
     ---
     cursor C_RMA_EXISTS is
        select '1'
          from fm_rma_head
         where rma_id = I_rma_id;
     ---
  BEGIN
     ---
     SQL_LIB.SET_MARK('OPEN','C_RMA_EXISTS','FM_RMA_HEAD','RMA ID: '||I_RMA_id);
     open C_RMA_EXISTS;
     ---
     SQL_LIB.SET_MARK('FETCH','C_RMA_EXISTS','FM_RMA_HEAD','RMA ID: '||I_RMA_id);
     fetch C_RMA_EXISTS into L_dummy;
     O_exists := C_RMA_EXISTS%FOUND;
     ---
     SQL_LIB.SET_MARK('CLOSE','C_RMA_EXISTS','FM_RMA_HEAD','RMA ID: '||I_RMA_id);
     close C_RMA_EXISTS;
     ---
     return TRUE;
     ---
  EXCEPTION
     when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
        return FALSE;

  END EXISTS_RMA;
--------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message  IN OUT VARCHAR2,
                          O_query          IN OUT VARCHAR2,
                          I_customer_id    IN FM_RMA_HEAD.CUST_ID%TYPE,
                          I_rma_id         IN FM_RMA_HEAD.RMA_ID%TYPE,
                          I_rma_date       IN FM_RMA_HEAD.RMA_DATE%TYPE,
                          I_location_type  IN FM_RMA_HEAD.LOC_TYPE%TYPE,
                          I_location_id    IN FM_RMA_HEAD.LOCATION%TYPE)
 return BOOLEAN is
   L_program VARCHAR2(50) := 'FM_RMA_NF_CREATION_SQL.SET_WHERE_CLAUSE';

BEGIN
   O_query :=' rma_status=''U'' ' ||
             ' and loc_type = '''||I_location_type||''' '||
             ' and location = '||I_location_id;

   if I_customer_id is NOT NULL then
      O_query := O_query || ' and cust_id = '|| I_customer_id ;
   end if;

   if I_rma_id is NOT NULL  then
      O_query := O_query || ' and rma_id = '||I_rma_id;
   end if;

   if I_rma_date is NOT NULL  then
      O_query := O_query || ' and rma_date = '||I_rma_date;
   end if;

   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1, 254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE;

-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOV_RMA(O_error_message  IN OUT VARCHAR2,
                 O_query          IN OUT VARCHAR2,
                 I_location_type  IN     FM_RMA_HEAD.LOC_TYPE%TYPE,
                 I_location_id    IN     FM_RMA_HEAD.LOCATION%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_RMA_NF_CREATION_SQL.LOV_RMA';
   ---
BEGIN
   ---
   O_query := 'select distinct frh.rma_id rma_id'||
                 ' from fm_rma_head frh '||
                 ' where frh.loc_type = '''||I_location_type||''' '||
                 ' and frh.location = '||I_location_id ||
                 ' and frh.rma_status = ''U'' '||
               ' order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_RMA;

-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOV_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_RMA_NF_CREATION_SQL.LOV_CUSTOMER';
   ---
BEGIN
   ---
   O_query := 'select distinct frh.cust_name,frh.cust_id'||
                ' from fm_rma_head frh '||
                ' order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_CUSTOMER;
-------------------------------------------------------------------------------------------------------------------------------------------------------
 FUNCTION EXISTS_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                          O_customer_desc  IN OUT FM_RMA_HEAD.CUST_NAME%TYPE,
                          I_customer_id    IN     FM_RMA_HEAD.CUST_ID%TYPE)
     return BOOLEAN is
     ---

     L_program   VARCHAR2(50) := 'FM_RMA_NF_CREATION_SQL.EXISTS_CUSTOMER';
     ---
     cursor C_EXISTS_CUSTOMER is
        select cust_name
          from fm_rma_head
         where cust_id = I_customer_id;
     ---
  BEGIN
     ---
     SQL_LIB.SET_MARK('OPEN','C_EXISTS_CUSTOMER','FM_RMA_HEAD','RMA ID: '||I_customer_id);
     open C_EXISTS_CUSTOMER;
     ---
     SQL_LIB.SET_MARK('FETCH','C_EXISTS_CUSTOMER','FM_RMA_HEAD','RMA ID: '||I_customer_id);
     fetch C_EXISTS_CUSTOMER into O_customer_desc;
     if C_EXISTS_CUSTOMER%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('INV_CUST',NULL,NULL,NULL);
        return FALSE;
     end if;
     ---
     SQL_LIB.SET_MARK('CLOSE','C_EXISTS_CUSTOMER','FM_RMA_HEAD','RMA ID: '||I_customer_id);
     close C_EXISTS_CUSTOMER;
     ---
     return TRUE;
     ---
  EXCEPTION
     when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
        return FALSE;

END EXISTS_CUSTOMER;
----------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message  IN OUT VARCHAR2,
                       I_rma_id         IN     FM_RMA_HEAD.RMA_ID%TYPE,
                       I_status         IN     FM_RMA_HEAD.RMA_STATUS%TYPE)

   return BOOLEAN is
      ---
      L_program   VARCHAR2(70) := 'FM_RMA_NF_CREATION_SQL.UPDATE_STATUS';
      L_key       VARCHAR2(500) := 'I_rma_id: '||I_rma_id||' I_status '||I_status;
      RECORD_LOCKED EXCEPTION;
      ---
      cursor C_RMA is
         select 'X'
           from fm_rma_head
          where rma_id = I_rma_id
            for update nowait;
 BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_RMA', 'FM_RMA_HEAD', L_key);
      open C_RMA;
      SQL_LIB.SET_MARK('CLOSE', 'C_RMA', 'FM_RMA_HEAD', L_key);
      close C_RMA;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_RMA_HEAD', L_key);
      update fm_rma_head
         set rma_status = I_status,
             last_update_datetime = SYSDATE,
             last_update_id = USER
       where rma_id = I_rma_id;
      ---
      return TRUE;
      ---
 EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               'FM_RMA_HEAD',
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1,254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_RMA%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE', 'C_RMA', 'FM_RMA_HEAD', L_key);
            close C_RMA;
         end if;
         ---
         return FALSE;
END UPDATE_STATUS;
--------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC(O_error_message  IN OUT VARCHAR2,
                           I_rma_id         IN     FM_RMA_HEAD.RMA_ID%TYPE,
                           I_fiscal_doc_id  IN     FM_EDI_DOC_HEADER.FISCAL_DOC_ID%TYPE)

   return BOOLEAN is
      ---
      L_program   VARCHAR2(70) := 'FM_RMA_NF_CREATION_SQL.UPDATE_FISCAL_DOC';
      L_key       VARCHAR2(500) := 'I_rma_id: '||I_rma_id||' I_fiscal_doc_id: '|| I_fiscal_doc_id;
      RECORD_LOCKED EXCEPTION;
      ---
      cursor C_RMA is
         select 'X'
           from fm_rma_head
          where rma_id = I_rma_id
            for update nowait;
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_RMA', 'FM_RMA_HEAD', L_key);
      open C_RMA;
      SQL_LIB.SET_MARK('CLOSE', 'C_RMA', 'FM_RMA_HEAD', L_key);
      close C_RMA;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, 'FM_RMA_HEAD', L_key);
      update fm_rma_head
         set fiscal_doc_id = I_fiscal_doc_id,
             last_update_datetime = SYSDATE,
             last_update_id = USER
       where rma_id = I_rma_id;
      ---
      return TRUE;
      ---
   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               'FM_EDI_DOC_HEADER',
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1,254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         if C_RMA%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE', 'C_RMA', 'FM_EDI_DOC_HEADER', L_key);
            close C_RMA;
         end if;
         ---
         return FALSE;
END UPDATE_FISCAL_DOC;
--------------------------------------------------------------------------------
end FM_RMA_NF_CREATION_SQL;
/
 
