CREATE OR REPLACE PACKAGE L10N_BR_WACADJ_SQL AS
-------------------------------------------------------------------------------------
-- Name      : WAC_UPADTE
-- Called by :
-- Purpose   : This API is called from the RFM batch to Update the Av_cost of the item loc combination
---            when the product turns in or out of ICMSST regime. This new av cost to be updated is provided
---            by the customer.
-------------------------------------------------------------------------------------
FUNCTION WAC_UPDATE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_wac_update_tbl   IN OUT   NOCOPY WAC_UPDATE_TBL,
                    I_action_type      IN       VARCHAR2)
RETURN BOOLEAN;

END L10N_BR_WACADJ_SQL;
/