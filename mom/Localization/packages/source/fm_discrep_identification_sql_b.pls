create or replace PACKAGE BODY FM_DISCREP_IDENTIFICATION_SQL is
-----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Function Name: PROCESS_DISCREP_IDEN
-- Purpose:       This function calls the function for identifying the quantity
--                , cost and tax discrepancies and then call the function for
--                apportioning the line level NF taxes if not mentioned
----------------------------------------------------------------------------------
FUNCTION PROCESS_DISCREP_IDEN( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,O_status          IN OUT INTEGER
                              ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                             )
   return BOOLEAN is
   ---
   L_program              VARCHAR2(50) := 'FM_DISCREP_IDENTIFICATION_SQL.PROCESS_DISCREP_IDEN';
   L_table                VARCHAR2(30);
   L_key                  VARCHAR2(100);
   L_appr                 BOOLEAN := TRUE;
   L_number               NUMBER;
   O_other_proc           BOOLEAN := FALSE;
   ---
BEGIN
   ---
   L_key := 'FISCAL_DOC_ID = '||I_fiscal_doc_id;
   if (FM_DISCREP_IDENTIFICATION_SQL.UPD_LEGAL_MSG_REC_VAL(O_error_message, O_status, O_other_proc, I_fiscal_doc_id) = FALSE) then
      O_status := 0;
      return (FALSE);
   end if;
   ---
   if (FM_DISCREP_IDENTIFICATION_SQL.CHECK_QTY_DISCREP(O_error_message, I_fiscal_doc_id) = FALSE) then
      O_status := 0;
      return (FALSE);
   end if;
   ---
   if (FM_DISCREP_IDENTIFICATION_SQL.CHECK_QTY_DISCREP_SI(O_error_message, I_fiscal_doc_id) = FALSE) then
      O_status := 0;
      return (FALSE);
   end if;
   --
   if (FM_DISCREP_IDENTIFICATION_SQL.CHECK_COST_DISCREP(O_error_message, I_fiscal_doc_id) = FALSE) then
      O_status := 0;
      return (FALSE);
   end if;
   ---
   if (FM_DISCREP_IDENTIFICATION_SQL.CHECK_COST_DISCREP_SI(O_error_message, I_fiscal_doc_id) = FALSE) then
      O_status := 0;
      return (FALSE);
   end if;
   ---
   if (FM_DISCREP_IDENTIFICATION_SQL.CHECK_TAX_DISCREP(O_error_message, I_fiscal_doc_id) = FALSE) then
      O_status := 0;
      return (FALSE);
   end if;
   ---
   if (FM_DISCREP_IDENTIFICATION_SQL.UPDATE_DISCREP_STATUS_MATCHED(O_error_message, I_fiscal_doc_id) = FALSE) then
      O_status := 0;
      return (FALSE);
   end if;
   --
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := 0;
      return FALSE;
END PROCESS_DISCREP_IDEN;
----------------------------------------------------------------------------------
-- Function Name: CHECK_FOR_TRIANGULATION
-- Purpose:       This function checks if the given NF needs to processed for Traingulation
----------------------------------------------------------------------------------
FUNCTION CHECK_FOR_TRIANGULATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                ,O_triangulation       OUT ORDHEAD.TRIANGULATION_IND%TYPE
                                ,O_compl_fiscal_doc_id OUT FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE
                                 )
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION';
   L_key                 VARCHAR2(80) := 'fdc.fiscal_doc_id = '|| I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message       VARCHAR2(255);
   L_discrepant          VARCHAR2(1);
   L_yes                 VARCHAR2(1) := 'Y';
--
   L_count               NUMBER;
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
--
   CURSOR C_GET_COMPL_NF IS
      SELECT count(1)
            ,fdc.compl_fiscal_doc_id
        FROM fm_fiscal_doc_header fdh
            ,fm_fiscal_doc_detail fdd
            ,fm_fiscal_doc_complement fdc
            ,ordhead ord
       WHERE fdh.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdh.fiscal_doc_id     = fdc.fiscal_doc_id
         AND fdd.requisition_no    = ord.order_no
         AND ord.triangulation_ind = L_yes
         AND fdh.fiscal_doc_id     = I_fiscal_doc_id
    GROUP BY fdc.compl_fiscal_doc_id;
--
BEGIN
   OPEN C_GET_COMPL_NF;
   FETCH C_GET_COMPL_NF INTO L_count , L_compl_fiscal_doc_id;
   CLOSE C_GET_COMPL_NF;
--
   if nvl(L_count,0) > 0 THEN
      O_triangulation := 'Y';
      O_compl_fiscal_doc_id := L_compl_fiscal_doc_id;
   else
      O_triangulation := 'N';
      O_compl_fiscal_doc_id := L_compl_fiscal_doc_id;
   end if;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_COMPLEMENT',
                                             L_key,
                                             TO_CHAR(SQLCODE));
--
      if C_GET_COMPL_NF%ISOPEN then
         close C_GET_COMPL_NF;
      end if;
---
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_GET_COMPL_NF%ISOPEN then
         close C_GET_COMPL_NF;
      end if;
--
      return FALSE;
--
END CHECK_FOR_TRIANGULATION;
----------------------------------------------------------------------------------
-- Function Name: CHECK_FOR_TRIANGULATION
-- Purpose:       This function checks if the given NF needs to processed for Traingulation
----------------------------------------------------------------------------------
FUNCTION CHECK_FOR_COMPL_TRIANGULATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                ,O_triangulation       OUT ORDHEAD.TRIANGULATION_IND%TYPE
                                ,O_compl_fiscal_doc_id OUT FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE
                                 )
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION';
   L_key                 VARCHAR2(80) := 'fdc.fiscal_doc_id = '|| I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message       VARCHAR2(255);
   L_discrepant          VARCHAR2(1);
   L_yes                 VARCHAR2(1) := 'Y';
--
   L_count               NUMBER;
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
--
   CURSOR C_GET_COMPL_NF IS
      SELECT count(1)
            ,fdc.fiscal_doc_id
        FROM fm_fiscal_doc_header fdh
            ,fm_fiscal_doc_detail fdd
            ,fm_fiscal_doc_complement fdc
            ,ordhead ord
       WHERE fdh.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdh.fiscal_doc_id     = fdc.compl_fiscal_doc_id
         AND fdd.requisition_no    = ord.order_no
         AND ord.triangulation_ind = L_yes
         AND fdc.compl_fiscal_doc_id    = I_fiscal_doc_id
    GROUP BY fdc.fiscal_doc_id;
--
BEGIN
   OPEN C_GET_COMPL_NF;
   FETCH C_GET_COMPL_NF INTO L_count , L_compl_fiscal_doc_id;
   CLOSE C_GET_COMPL_NF;
--
   if nvl(L_count,0) > 0 THEN
      O_triangulation := 'Y';
      O_compl_fiscal_doc_id := L_compl_fiscal_doc_id;
   else
      O_triangulation := 'N';
      O_compl_fiscal_doc_id := L_compl_fiscal_doc_id;
   end if;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_COMPLEMENT',
                                             L_key,
                                             TO_CHAR(SQLCODE));
--
      if C_GET_COMPL_NF%ISOPEN then
         close C_GET_COMPL_NF;
      end if;
---
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_GET_COMPL_NF%ISOPEN then
         close C_GET_COMPL_NF;
      end if;
--
      return FALSE;
--
END CHECK_FOR_COMPL_TRIANGULATION;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_DISREP_FOR_TRIANG
-- Purpose:       This function updates the fm_fiscal_doc_detail table for discrepancy status
--                for triangulation.
----------------------------------------------------------------------------------
FUNCTION UPDATE_DISREP_FOR_TRIANG(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                 ,I_discrepancy          IN VARCHAR2
                                 ,I_compl_fiscal_doc_id  IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE
                                 ,I_po_number            IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                                 ,I_item                 IN FM_FISCAL_DOC_DETAIL.ITEM%TYPE
                                 )
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.UPDATE_DISREP_FOR_TRIANGULATION';
   L_key                 VARCHAR2(80) := 'fdd.fiscal_doc_id = '|| I_compl_fiscal_doc_id;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message       VARCHAR2(255);
   L_discrepant          VARCHAR2(1):= 'D';
--
   CURSOR C_COMPL_DISCREP_UPDATE IS
      SELECT fdd.fiscal_doc_line_id
        FROM fm_fiscal_doc_detail fdd
       WHERE fdd.fiscal_doc_id     = I_compl_fiscal_doc_id
         AND fdd.requisition_no    = I_po_number
         AND fdd.item              = I_item;
--
BEGIN
--
   OPEN C_COMPL_DISCREP_UPDATE;
   CLOSE C_COMPL_DISCREP_UPDATE;
--
   if I_discrepancy = 'Q' then
--
      UPDATE fm_fiscal_doc_detail
         SET qty_discrep_status = L_discrepant
       WHERE fiscal_doc_id      = I_compl_fiscal_doc_id
         AND requisition_no     = I_po_number
         AND item               = I_item;
--
   elsif I_discrepancy = 'C' then
--
      UPDATE fm_fiscal_doc_detail
         SET cost_discrep_status = L_discrepant
       WHERE fiscal_doc_id       = I_compl_fiscal_doc_id
         AND requisition_no      = I_po_number
         AND item                = I_item;
--
   end if;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_COMPLEMENT',
                                             L_key,
                                             TO_CHAR(SQLCODE));
--
      if C_COMPL_DISCREP_UPDATE%ISOPEN then
         close C_COMPL_DISCREP_UPDATE;
      end if;
---
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_COMPL_DISCREP_UPDATE%ISOPEN then
         close C_COMPL_DISCREP_UPDATE;
      end if;
--
      return FALSE;
--
END UPDATE_DISREP_FOR_TRIANG;
----------------------------------------------------------------------------------
-- Function Name: CHECK_QTY_DISCREP
-- Purpose:       This function identifies all the Quantity discrepancy
--                and inserts quantity discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_QTY_DISCREP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                          )
   return BOOLEAN is
--
   L_program            VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.CHECK_QTY_DISCREP';
   L_key                VARCHAR2(80) := 'fdh.fiscal_doc_id = '||I_fiscal_doc_id;
   L_key1               VARCHAR2(80);
   L_discrep_type       VARCHAR2(1)  := 'Q';
   L_qty_discrep_status VARCHAR2(1)  := 'D';
   L_triangulation      VARCHAR2(1);
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
--
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message      VARCHAR2(255);
   L_discrepant         VARCHAR2(1);
   L_wh                 VARCHAR2(1) := 'W';
   L_no                 VARCHAR2(1) := 'N';
   L_po                 VARCHAR2(2) := 'PO';
   L_nf_qty             FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_item               FM_FISCAL_DOC_DETAIL.ITEM%TYPE;
   L_req_no             FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
--
   cursor C_GET_ITEM is
     SELECT fdd.item,requisition_no
       FROM fm_fiscal_doc_detail fdd
      WHERE fdd.fiscal_doc_id = I_fiscal_doc_id
        AND fdd.pack_no is null;
--
   cursor C_GET_SUM_QTY(I_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE,I_req_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
     SELECT sum(qty)
       FROM
         (SELECT nvl(sum(fdd.quantity),0) qty
            FROM fm_fiscal_doc_header fdh
                ,fm_fiscal_doc_detail fdd
           WHERE fdh.status IN ('V','E','S','D','H')
             AND fdh.fiscal_doc_id = fdd.fiscal_doc_id
             AND fdd.fiscal_doc_id != I_fiscal_doc_id
             AND fdd.fiscal_doc_id_ref is NULL
             AND fdd.fiscal_doc_line_id_ref is NULL
             AND EXISTS (SELECT 'X'
                           FROM fm_fiscal_doc_detail ffdd
                          WHERE ffdd.fiscal_doc_id = I_fiscal_doc_id
                            AND fdd.location_id    = ffdd.location_id
                            AND fdd.location_type  = ffdd.location_type
                            AND fdd.requisition_no = ffdd.requisition_no
                            AND ffdd.item = I_item
                            AND fdd.requisition_no = I_req_no
                            AND fdd.item  = ffdd.item
                            AND ffdd.pack_no  IS NULL
                            AND fdd.pack_no IS NULL
                            AND fdd.item  = ffdd.item
                            AND fdd.requisition_no  = ffdd.requisition_no)
     UNION ALL
        SELECT distinct(nvl((fdd.quantity),0)/pki.pack_item_qty) qty
          FROM fm_fiscal_doc_header fdh
              ,fm_fiscal_doc_detail fdd
              ,packitem_breakout pki
         WHERE fdh.status IN ('V','E','S','D','H')
           AND fdh.fiscal_doc_id = fdd.fiscal_doc_id
           AND fdd.fiscal_doc_id != I_fiscal_doc_id
           AND fdd.fiscal_doc_id_ref is NULL
           AND fdd.fiscal_doc_line_id_ref is NULL
           AND fdd.pack_no=pki.pack_no
           AND fdd.item=pki.item
           AND EXISTS (SELECT 'X'
                         FROM fm_fiscal_doc_detail ffdd
                        WHERE ffdd.fiscal_doc_id = I_fiscal_doc_id
                          AND fdd.location_id    = ffdd.location_id
                          AND fdd.location_type  = ffdd.location_type
                          AND fdd.requisition_no = ffdd.requisition_no
                          AND ffdd.item = I_item
                          AND fdd.requisition_no = I_req_no
                          AND ffdd.pack_no  IS NULL     
                          AND fdd.pack_no IS NOT NULL 
                          AND fdd.pack_no = ffdd.item 
                          AND fdd.requisition_no = ffdd.requisition_no));

--
   cursor C_GET_QTY_DIS(I_qty IN NUMBER,I_item IN FM_FISCAL_DOC_DETAIL.ITEM%TYPE,I_req_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
     SELECT fdh.fiscal_doc_id
           ,fdd.fiscal_doc_line_id
           ,CASE WHEN ((sum(ord.qty_ordered) - nvl(sum(ord.qty_received),0)) - I_qty) <= 0 THEN 0
                 ELSE ((sum(ord.qty_ordered) - nvl(sum(ord.qty_received),0)) - I_qty)
             END system_value
           ,fdd.quantity nf_value
           ,fdd.fiscal_doc_line_id_ref
           ,fdh.module
           ,fdh.key_value_1
           ,fdh.key_value_2
           ,fdd.requisition_no
           ,fdd.item
       FROM fm_fiscal_doc_header fdh
           ,fm_fiscal_doc_detail fdd
           ,ordloc ord
      WHERE fdh.fiscal_doc_id      = fdd.fiscal_doc_id
        AND fdd.requisition_no     = ord.order_no
        AND fdd.item               = ord.item
        AND fdd.location_id        = DECODE(fdh.location_type ,L_wh,(select PHYSICAL_WH from WH where WH.WH=ord.location),ord.location)
        AND fdd.location_type      = ord.loc_type
        AND fdh.location_id        = fdd.location_id
        AND fdh.location_type      = fdd.location_type
        AND fdh.requisition_type   = L_po
        AND fdh.fiscal_doc_id      = I_fiscal_doc_id
        AND nvl(fdd.unexpected_item,L_no)  = L_no
        AND fdd.pack_no is null
        AND fdd.item = I_item
        AND fdd.requisition_no = I_req_no
   GROUP BY fdh.fiscal_doc_id,fdd.fiscal_doc_line_id,fdd.quantity,fdd.fiscal_doc_line_id_ref,fdh.module,fdh.key_value_1,fdh.key_value_2,fdd.requisition_no,fdd.item
     HAVING fdd.quantity > ((sum(ord.qty_ordered) - nvl(sum(ord.qty_received),0)) - I_qty);
--
   cursor C_LOCK_FM_FISCAL_DOC_DETAIL(I_FISCAL_DOC_LINE_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_detail fdtd
       WHERE fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
         --FOR update nowait;
--
BEGIN
---This code for passing the item
   FOR L_GET_ITEM IN C_GET_ITEM
   LOOP

---This code for passing the qty
   OPEN  C_GET_SUM_QTY(L_get_item.item,L_get_item.requisition_no);
   FETCH C_GET_SUM_QTY INTO L_nf_qty;
   CLOSE C_GET_SUM_QTY;

---This code if for Triangulation
   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message      => O_error_message
                                                           ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                           ,O_triangulation      => L_triangulation
                                                           ,O_compl_fiscal_doc_id => L_compl_fiscal_doc_id ) = FALSE then
      return FALSE;
   end if;
-- end for Triangulation
--
   FOR L_GET_QTY_DIS IN C_GET_QTY_DIS(L_nf_qty,L_get_item.item,L_get_item.requisition_no)
   LOOP
--
      if FM_DISCREP_IDENTIFICATION_SQL.GET_TOLERANCE(L_error_message,
                                                     L_discrepant,
                                                     L_get_qty_dis.module,
                                                     L_get_qty_dis.key_value_1,
                                                     L_get_qty_dis.key_value_2,
                                                     'QTY',
                                                     L_get_qty_dis.nf_value,
                                                     L_get_qty_dis.system_value)=FALSE then
         return FALSE;
      end if;

      if L_discrepant = 'N' then
         update fm_fiscal_doc_detail
            set tolerance_value_qty  = abs(L_get_qty_dis.nf_value - L_get_qty_dis.system_value)
          where fiscal_doc_id        =  L_get_qty_dis.fiscal_doc_id
            and fiscal_doc_line_id   =  L_get_qty_dis.fiscal_doc_line_id ;
      end if;

--
      if L_discrepant = 'Y' then
--
         INSERT INTO FM_RESOLUTION( fiscal_doc_id
                                   ,fiscal_doc_line_id
                                   ,discrep_type
                                   ,resolution_type
                                   ,system_value
                                   ,nf_value
                                   ,create_datetime
                                   ,create_id
                                   ,last_update_datetime
                                   ,last_update_id
                                 ) values
                                (
                                   L_get_qty_dis.fiscal_doc_id
                                  ,L_get_qty_dis.fiscal_doc_line_id
                                  ,L_discrep_type
                                  ,NULL
                                  ,L_get_qty_dis.system_value
                                  ,L_get_qty_dis.nf_value
                                  ,sysdate
                                  ,user
                                  ,sysdate
                                  ,user
                                );
--
         L_key1:= 'fiscal_doc_line_id   = '||L_get_qty_dis.fiscal_doc_line_id;
--
         OPEN C_LOCK_FM_FISCAL_DOC_DETAIL(L_get_qty_dis.fiscal_doc_line_id);
         CLOSE C_LOCK_FM_FISCAL_DOC_DETAIL;
--

       if L_get_qty_dis.nf_value > L_get_qty_dis.system_value then

--
          UPDATE fm_fiscal_doc_detail fdt
             SET fdt.qty_discrep_status   = L_qty_discrep_status
                ,fdt.last_update_datetime = sysdate
                ,fdt.last_update_id       = user
           WHERE fdt.fiscal_doc_line_id   = L_get_qty_dis.fiscal_doc_line_id;
--

       end if;
--
      end if;
--
/* -- No Need to update the qty discrep status
--This code if for Triangulation
      if L_triangulation = 'Y' then
         if FM_DISCREP_IDENTIFICATION_SQL.UPDATE_DISREP_FOR_TRIANG(O_error_message       => O_error_message
                                                                  ,I_discrepancy         => 'Q'
                                                                  ,I_compl_fiscal_doc_id => L_compl_fiscal_doc_id
                                                                  ,I_po_number           => L_get_qty_dis.requisition_no
                                                                  ,I_item                => L_get_qty_dis.item) = FALSE then
            return FALSE;
         end if;
      end if;
--end for Triangulation
*/
   END LOOP;

--C_GET_ITEM

  END LOOP;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('FM_RESOLUTION',
                                            'FM_FISCAL_DOC_DETAIL',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
--
      if C_LOCK_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_DOC_DETAIL;
      end if;
---
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END CHECK_QTY_DISCREP;
----------------------------------------------------------------------------------
-- Function Name: CHECK_QTY_DISCREP_SI
-- Purpose:       This function identifies all the Quantity discrepancy for Special Items
--                and inserts quantity discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_QTY_DISCREP_SI(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                             ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                             )
   return BOOLEAN is
--
   L_program            VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.CHECK_QTY_DISCREP_SI';
   L_key                VARCHAR2(80) := 'fdh.fiscal_doc_id = '||I_fiscal_doc_id;
   L_key1               VARCHAR2(80);
   L_discrep_type       VARCHAR2(1)  := 'Q';
   L_qty_discrep_status VARCHAR2(1)  := 'D';
   L_triangulation      VARCHAR2(1);
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
--
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message      VARCHAR2(255);
   L_discrepant         VARCHAR2(1);
   L_wh                 VARCHAR2(1) := 'W';
   L_no                 VARCHAR2(1) := 'N';
   L_po                 VARCHAR2(2) := 'PO';
   L_nf_qty             FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_item               FM_FISCAL_DOC_DETAIL.ITEM%TYPE;
   L_pk_no              FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE;
   L_req_no             FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
--
   cursor C_GET_ITEM is
     SELECT fdd.item,fdd.pack_no,fdd.requisition_no
       FROM fm_fiscal_doc_detail fdd
      WHERE fdd.fiscal_doc_id = I_fiscal_doc_id
        AND fdd.pack_no is not null;
--
  cursor C_GET_SUM_QTY(I_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE,I_pk_no FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,I_req_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
     SELECT sum(qty)
       FROM
         (SELECT nvl(sum(fdd.quantity),0) qty
            FROM fm_fiscal_doc_header fdh
                ,fm_fiscal_doc_detail fdd
           WHERE fdh.status IN ('V','E','S','D','H')
             AND fdh.fiscal_doc_id = fdd.fiscal_doc_id
             AND fdd.fiscal_doc_id_ref is NULL
             AND fdd.fiscal_doc_line_id_ref is NULL
             AND fdd.fiscal_doc_id != I_fiscal_doc_id
             AND EXISTS (SELECT 'X'
                           FROM fm_fiscal_doc_detail ffdd
                          WHERE ffdd.fiscal_doc_id = I_fiscal_doc_id
                            AND fdd.location_id    = ffdd.location_id
                            AND fdd.location_type  = ffdd.location_type
                            AND fdd.requisition_no = ffdd.requisition_no
                            AND fdd.item           = I_pk_no
                            AND fdd.requisition_no = I_req_no
                            AND ffdd.pack_no  IS NOT NULL
                            AND fdd.pack_no IS NULL
                            AND fdd.item != ffdd.item
                            AND fdd.requisition_no = ffdd.requisition_no)
     UNION ALL
        SELECT distinct(nvl((fdd.quantity),0)/pki.pack_item_qty) qty
          FROM fm_fiscal_doc_header fdh
              ,fm_fiscal_doc_detail fdd
              ,packitem_breakout pki
         WHERE fdh.status IN ('V','E','S','D','H')
           AND fdh.fiscal_doc_id = fdd.fiscal_doc_id
           AND fdd.fiscal_doc_id != I_fiscal_doc_id
           AND fdd.fiscal_doc_id_ref is NULL
           AND fdd.fiscal_doc_line_id_ref is NULL
           AND fdd.pack_no=pki.pack_no
           AND fdd.item=pki.item
           AND EXISTS (SELECT 'X'
                         FROM fm_fiscal_doc_detail ffdd
                        WHERE ffdd.fiscal_doc_id = I_fiscal_doc_id
                          AND fdd.location_id    = ffdd.location_id
                          AND fdd.location_type  = ffdd.location_type
                          AND fdd.requisition_no = ffdd.requisition_no
                          AND ffdd.item          = I_item
                          AND ffdd.pack_no       = I_pk_no
                          AND ffdd.requisition_no = I_req_no
                          AND ffdd.pack_no  IS NOT NULL
                          AND fdd.pack_no IS NOT NULL
                          AND fdd.pack_no  = ffdd.pack_no
                          AND fdd.requisition_no  = ffdd.requisition_no)); 
--

   cursor C_GET_QTY_DIS(I_sum_qty IN NUMBER,I_item IN FM_FISCAL_DOC_DETAIL.ITEM%TYPE,I_pk_no FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,I_req_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      SELECT fdh.fiscal_doc_id
            ,fdd.fiscal_doc_line_id
            ,CASE WHEN (((sum(ord.qty_ordered) - nvl(sum(ord.qty_received),0)) - I_sum_qty)*pki.pack_item_qty) <= 0 THEN 0
                  ELSE (((sum(ord.qty_ordered) - nvl(sum(ord.qty_received),0)) - I_sum_qty)*pki.pack_item_qty)
             END system_value
            ,fdd.quantity nf_value
            ,fdd.fiscal_doc_line_id_ref
            ,fdh.module
            ,fdh.key_value_1
            ,fdh.key_value_2
            ,fdd.requisition_no
            ,fdd.item
        FROM fm_fiscal_doc_header fdh
           , fm_fiscal_doc_detail fdd
           , ordloc ord
           , packitem_breakout pki
       WHERE fdh.fiscal_doc_id      = fdd.fiscal_doc_id
         AND fdd.requisition_no     = ord.order_no
         AND fdd.pack_no            = ord.item
         AND fdd.item               = pki.item
         AND pki.pack_no            = ord.item
         AND fdd.location_id        = DECODE(fdh.location_type ,L_wh,(select PHYSICAL_WH from WH where WH.WH=ord.location),ord.location)
         AND fdd.location_type      = ord.loc_type
         AND fdh.location_id        = fdd.location_id
         AND fdh.location_type      = fdd.location_type
         AND fdh.requisition_type   = L_po
         AND fdh.fiscal_doc_id      = I_fiscal_doc_id
         AND nvl(fdd.unexpected_item,L_no)  = L_no
         AND fdd.pack_no is not null
         AND fdd.item    = I_item
         AND fdd.pack_no = I_pk_no
         AND fdd.requisition_no = I_req_no
    GROUP BY fdh.fiscal_doc_id,fdd.fiscal_doc_line_id,pki.pack_item_qty,fdd.quantity,fdd.fiscal_doc_line_id_ref,fdh.module,fdh.key_value_1,fdh.key_value_2,fdd.requisition_no,fdd.item
      HAVING fdd.quantity > (((sum(ord.qty_ordered) - nvl(sum(ord.qty_received),0)) - I_sum_qty)*pki.pack_item_qty);
--
   cursor C_LOCK_FM_FISCAL_DOC_DETAIL(I_FISCAL_DOC_LINE_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_detail fdtd
       WHERE fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
         --FOR update nowait;
--
BEGIN

---This code for passing the item
   FOR L_GET_ITEM IN C_GET_ITEM
   LOOP

---This code for passing the qty
   OPEN  C_GET_SUM_QTY(L_get_item.item,L_get_item.pack_no,L_get_item.requisition_no);
   FETCH C_GET_SUM_QTY INTO L_nf_qty;
   CLOSE C_GET_SUM_QTY;

----This code if for Triangulation
   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message      => O_error_message
                                                           ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                           ,O_triangulation      => L_triangulation
                                                           ,O_compl_fiscal_doc_id => L_compl_fiscal_doc_id ) = FALSE then
      return FALSE;
   end if;
-- end for Triangulation
--
   FOR L_GET_QTY_DIS IN C_GET_QTY_DIS(L_nf_qty,L_get_item.item,L_get_item.pack_no,L_get_item.requisition_no)
   LOOP
--
      if FM_DISCREP_IDENTIFICATION_SQL.GET_TOLERANCE(L_error_message,
                                                     L_discrepant,
                                                     L_get_qty_dis.module,
                                                     L_get_qty_dis.key_value_1,
                                                     L_get_qty_dis.key_value_2,
                                                     'QTY',
                                                     L_get_qty_dis.nf_value,
                                                     L_get_qty_dis.system_value)=FALSE then
         return FALSE;
      end if;
--

      if L_discrepant = 'N' then
         update fm_fiscal_doc_detail
            set tolerance_value_qty  = abs(L_get_qty_dis.nf_value - L_get_qty_dis.system_value)
          where fiscal_doc_id        =  L_get_qty_dis.fiscal_doc_id
            and fiscal_doc_line_id   =  L_get_qty_dis.fiscal_doc_line_id ;
      end if;

--
      if L_discrepant = 'Y' then
--
         INSERT INTO FM_RESOLUTION( fiscal_doc_id
                                   ,fiscal_doc_line_id
                                   ,discrep_type
                                   ,resolution_type
                                   ,system_value
                                   ,nf_value
                                   ,create_datetime
                                   ,create_id
                                   ,last_update_datetime
                                   ,last_update_id
                                 ) values
                                (
                                   L_get_qty_dis.fiscal_doc_id
                                  ,L_get_qty_dis.fiscal_doc_line_id
                                  ,L_discrep_type
                                  ,NULL
                                  ,L_get_qty_dis.system_value
                                  ,L_get_qty_dis.nf_value
                                  ,sysdate
                                  ,user
                                  ,sysdate
                                  ,user
                                );
--
         L_key1:= 'fiscal_doc_line_id   = '||L_get_qty_dis.fiscal_doc_line_id;
--
         OPEN C_LOCK_FM_FISCAL_DOC_DETAIL(L_get_qty_dis.fiscal_doc_line_id);
         CLOSE C_LOCK_FM_FISCAL_DOC_DETAIL;
--

       if L_get_qty_dis.nf_value > L_get_qty_dis.system_value then
--
         UPDATE fm_fiscal_doc_detail fdt
            SET fdt.qty_discrep_status   = L_qty_discrep_status
               ,fdt.last_update_datetime = sysdate
               ,fdt.last_update_id       = user
          WHERE fdt.fiscal_doc_line_id   = L_get_qty_dis.fiscal_doc_line_id;
--
       end if;
--
      end if;
--
   END LOOP;
--
  END LOOP;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('FM_RESOLUTION',
                                            'FM_FISCAL_DOC_DETAIL',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
--
      if C_LOCK_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_DOC_DETAIL;
      end if;
---
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END CHECK_QTY_DISCREP_SI;
----------------------------------------------------------------------------------
-- Function Name: CHECK_COST_DISCREP
-- Purpose:       This function identifies all the Cost discrepancy
--                and inserts Cost discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_COST_DISCREP(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id   IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.CHECK_COST_DISCREP';
   L_key                 VARCHAR2(80) := 'fdh.fiscal_doc_id = '|| I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   L_cost_discrep_status VARCHAR2(1)  := 'D';
   L_discrep_type        VARCHAR2(1)  := 'C';
   L_triangulation       VARCHAR2(1);
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
--
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message       VARCHAR2(255);
   L_discrepant          VARCHAR2(1);
   L_wh                 VARCHAR2(1) := 'W';
   L_no                 VARCHAR2(1) := 'N';
   L_po                 VARCHAR2(2) := 'PO';
--
   cursor C_GET_COST_DIS is
     SELECT fdh.fiscal_doc_id
           ,fdd.fiscal_doc_line_id
           ,fdd.unit_cost_with_disc  nf_value
           ,ord.unit_cost  system_value
           ,fdd.fiscal_doc_line_id_ref
           ,fdh.module
           ,fdh.key_value_1
           ,fdh.key_value_2
           ,fdd.requisition_no
           ,fdd.item
       FROM fm_fiscal_doc_header fdh
           ,fm_fiscal_doc_detail fdd
           ,ordloc ord
      WHERE fdh.fiscal_doc_id       = fdd.fiscal_doc_id
        AND fdd.requisition_no      = ord.order_no
        AND fdd.item                = ord.item
	       AND fdd.location_id         = DECODE(fdh.location_type ,L_wh,(select PHYSICAL_WH from WH where WH.WH=ord.location),ord.location)
        AND fdd.location_type       = ord.loc_type
        AND fdh.location_id         = fdd.location_id
        AND fdh.location_type       = fdd.location_type
        AND fdh.requisition_type    = L_po
        AND fdd.unit_cost_with_disc > ord.unit_cost
        AND fdh.fiscal_doc_id       = I_fiscal_doc_id
        AND nvl(fdd.unexpected_item,L_no)  = L_no
        AND fdd.pack_no is null;
--
   cursor C_LOCK_FM_FISCAL_DOC_DETAIL(I_FISCAL_DOC_LINE_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_detail fdtd
       WHERE fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
       --  FOR update nowait;
--
BEGIN
----This code if for Triangulation
   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message       => O_error_message
                                                           ,I_fiscal_doc_id       => I_fiscal_doc_id
                                                           ,O_triangulation       => L_triangulation
                                                           ,O_compl_fiscal_doc_id => L_compl_fiscal_doc_id ) = FALSE then
      return FALSE;
   end if;
-- end for Triangulation
--
   FOR L_GET_COST_DIS IN C_GET_COST_DIS
   LOOP
--
      if FM_DISCREP_IDENTIFICATION_SQL.GET_TOLERANCE(L_error_message,
                                                     L_discrepant,
                                                     L_get_cost_dis.module,
                                                     L_get_cost_dis.key_value_1,
                                                     L_get_cost_dis.key_value_2,
                                                     'COST',
                                                     L_get_cost_dis.nf_value,
                                                     L_get_cost_dis.system_value)=FALSE then
         return FALSE;
      end if;
--
      if L_discrepant = 'N' then
         update fm_fiscal_doc_detail
            set tolerance_value_cost = abs(L_get_cost_dis.nf_value - L_get_cost_dis.system_value)
          where fiscal_doc_id        =  L_get_cost_dis.fiscal_doc_id
            and fiscal_doc_line_id   =  L_get_cost_dis.fiscal_doc_line_id ;
      end if;
--
      if L_discrepant = 'Y' then
--
         INSERT INTO FM_RESOLUTION(fiscal_doc_id
                                  ,fiscal_doc_line_id
                                  ,discrep_type
                                  ,resolution_type
                                  ,system_value
                                  ,nf_value
                                  ,create_datetime
                                  ,create_id
                                  ,last_update_datetime
                                  ,last_update_id
                                  ) values
                                 (
                                  L_get_cost_dis.fiscal_doc_id
                                 ,L_get_cost_dis.fiscal_doc_line_id
                                 ,L_discrep_type
                                 ,NULL
                                 ,L_get_cost_dis.system_value
                                 ,L_get_cost_dis.nf_value
                                 ,sysdate
                                 ,user
                                 ,sysdate
                                 ,user
                                );
         L_key1 := 'fiscal_doc_line_id = '||L_get_cost_dis.fiscal_doc_line_id;
--
         OPEN C_LOCK_FM_FISCAL_DOC_DETAIL(L_get_cost_dis.fiscal_doc_line_id);
         CLOSE C_LOCK_FM_FISCAL_DOC_DETAIL;
--
         UPDATE fm_fiscal_doc_detail fdd
            SET fdd.cost_discrep_status  = L_cost_discrep_status
               ,fdd.last_update_datetime = sysdate
               ,fdd.last_update_id       = user
          WHERE fiscal_doc_line_id       = L_get_cost_dis.fiscal_doc_line_id;
--
      end if;
--
/* -- No Need to update the qty discrep status
--This code if for Triangulation
      if L_triangulation = 'Y' then
         if FM_DISCREP_IDENTIFICATION_SQL.UPDATE_DISREP_FOR_TRIANG(O_error_message       => O_error_message
                                                                  ,I_discrepancy         => 'C'
                                                                  ,I_compl_fiscal_doc_id => L_compl_fiscal_doc_id
                                                                  ,I_po_number           => L_get_cost_dis.requisition_no
                                                                  ,I_item                => L_get_cost_dis.item) = FALSE then
            return FALSE;
         end if;
      end if;
--end for Triangulation
*/
   END LOOP;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_DETAIL',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
--
      if C_LOCK_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_DOC_DETAIL;
      end if;
---
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END CHECK_COST_DISCREP;
----------------------------------------------------------------------------------
-- Function Name: CHECK_COST_DISCREP_SI
-- Purpose:       This function identifies all the Cost discrepancy for Special Items
--                and inserts Cost discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_COST_DISCREP_SI(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_fiscal_doc_id   IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                               )
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.CHECK_COST_DISCREPS_SI';
   L_key                 VARCHAR2(80) := 'fdh.fiscal_doc_id = '|| I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   L_cost_discrep_status VARCHAR2(1)  := 'D';
   L_discrep_type        VARCHAR2(1)  := 'C';
   L_triangulation       VARCHAR2(1);
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
--
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message       VARCHAR2(255);
   L_discrepant          VARCHAR2(1);
   L_wh                 VARCHAR2(1) := 'W';
   L_no                 VARCHAR2(1) := 'N';
   L_po                 VARCHAR2(2) := 'PO';
--
   cursor C_GET_COST_DIS is
      SELECT fdh.fiscal_doc_id
            ,fdd.fiscal_doc_line_id
            ,fdd.unit_cost_with_disc  nf_value
            ,ROUND(((ord.unit_cost *((isc.negotiated_item_cost * pki.pack_qty) / pki.pack_qty / (select sum(isc_1.negotiated_item_cost * pi.pack_qty)
                                                                                  from item_supp_country_loc isc_1 ,packitem pi
                                                                                 where pi.item    = isc_1.item
                                                                                   and pi.pack_no = fdd.pack_no
                                                                                   and isc_1.supplier       = oh.supplier
                                                                                   and ord.location         = isc_1.loc
                                                                                   and ord.loc_type         = isc_1.loc_type)))),4) system_value
                                                                                  -- and isc_1.loc_type = fdd.location_type
                                                                                  -- and fdd.location_id = decode(fdh.location_type ,'W',(select PHYSICAL_WH from WH where WH.WH=isc_1.loc),isc_1.loc))))),4) system_value
           ,fdd.fiscal_doc_line_id_ref
           ,fdh.module
           ,fdh.key_value_1
           ,fdh.key_value_2
           ,fdd.requisition_no
           ,fdd.item
       FROM fm_fiscal_doc_header fdh
           ,fm_fiscal_doc_detail fdd
           ,ordloc ord
           ,ordhead oh
           ,packitem pki
           ,item_supp_country_loc isc  --ils
      WHERE fdh.fiscal_doc_id       = fdd.fiscal_doc_id
        AND fdd.requisition_no      = ord.order_no
        AND ord.order_no            = oh.order_no
        AND fdd.pack_no             = ord.item
        AND pki.pack_no             = ord.item
        AND fdd.item                = pki.item
        AND fdd.item                = isc.item
        AND fdd.location_id         = DECODE(fdh.location_type ,L_wh,(select PHYSICAL_WH from WH where WH.WH=ord.location),ord.location)
        AND fdd.location_type       = ord.loc_type
        AND fdd.location_id         = DECODE(fdh.location_type ,L_wh,(select PHYSICAL_WH from WH where WH.WH=isc.loc),isc.loc)
        AND fdd.location_type       = isc.loc_type
        AND fdh.location_id         = fdd.location_id
        AND fdh.location_type       = fdd.location_type
        AND fdh.requisition_type    = L_po
        AND fdh.fiscal_doc_id       = I_fiscal_doc_id
        AND nvl(fdd.unexpected_item,L_no)  = L_no
        AND fdd.unit_cost_with_disc > ROUND((ord.unit_cost *((isc.negotiated_item_cost * pki.pack_qty) / pki.pack_qty / (select sum(isc_1.negotiated_item_cost * pi.pack_qty)
                                                                                           from item_supp_country_loc isc_1 , packitem pi
                                                                                          where pi.item    = isc_1.item
                                                                                            and pi.pack_no = fdd.pack_no
                                                                                            and isc_1.supplier       = oh.supplier
                                                                                            and ord.location         = isc_1.loc
                                                                                            and ord.loc_type         = isc_1.loc_type))),4)
                                                                                            --and isc_1.loc_type = fdd.location_type
                                                                                            --and fdd.location_id = decode(fdh.location_type ,'W',(select PHYSICAL_WH from WH where WH.WH=isc_1.loc),isc_1.loc)))),4)
        AND fdd.pack_no is not null
        AND isc.supplier = oh.supplier
        AND ord.location         = isc.loc
        AND ord.loc_type       = isc.loc_type;
--
   cursor C_LOCK_FM_FISCAL_DOC_DETAIL(I_FISCAL_DOC_LINE_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_detail fdtd
       WHERE fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
       --  FOR update nowait;
--
BEGIN
----This code if for Triangulation
   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message       => O_error_message
                                                           ,I_fiscal_doc_id       => I_fiscal_doc_id
                                                           ,O_triangulation       => L_triangulation
                                                           ,O_compl_fiscal_doc_id => L_compl_fiscal_doc_id ) = FALSE then
      return FALSE;
   end if;
-- end for Triangulation
--
   FOR L_GET_COST_DIS IN C_GET_COST_DIS
   LOOP
--
      if FM_DISCREP_IDENTIFICATION_SQL.GET_TOLERANCE(L_error_message,
                                                     L_discrepant,
                                                     L_get_cost_dis.module,
                                                     L_get_cost_dis.key_value_1,
                                                     L_get_cost_dis.key_value_2,
                                                     'COST',
                                                     L_get_cost_dis.nf_value,
                                                     L_get_cost_dis.system_value)=FALSE then
         return FALSE;
      end if;
--
      if L_discrepant = 'N' then
         update fm_fiscal_doc_detail
            set tolerance_value_cost = abs(L_get_cost_dis.nf_value - L_get_cost_dis.system_value)
          where fiscal_doc_id        =  L_get_cost_dis.fiscal_doc_id
            and fiscal_doc_line_id   =  L_get_cost_dis.fiscal_doc_line_id ;
      end if;
--
      if L_discrepant = 'Y' then
--
         INSERT INTO FM_RESOLUTION(fiscal_doc_id
                                  ,fiscal_doc_line_id
                                  ,discrep_type
                                  ,resolution_type
                                  ,system_value
                                  ,nf_value
                                  ,create_datetime
                                  ,create_id
                                  ,last_update_datetime
                                  ,last_update_id
                                  ) values
                                 (
                                  L_get_cost_dis.fiscal_doc_id
                                 ,L_get_cost_dis.fiscal_doc_line_id
                                 ,L_discrep_type
                                 ,NULL
                                 ,L_get_cost_dis.system_value
                                 ,L_get_cost_dis.nf_value
                                 ,sysdate
                                 ,user
                                 ,sysdate
                                 ,user
                                );
         L_key1 := 'fiscal_doc_line_id = '||L_get_cost_dis.fiscal_doc_line_id;
--
         OPEN C_LOCK_FM_FISCAL_DOC_DETAIL(L_get_cost_dis.fiscal_doc_line_id);
         CLOSE C_LOCK_FM_FISCAL_DOC_DETAIL;
--
         UPDATE fm_fiscal_doc_detail fdd
            SET fdd.cost_discrep_status  = L_cost_discrep_status
               ,fdd.last_update_datetime = sysdate
               ,fdd.last_update_id       = user
          WHERE fiscal_doc_line_id       = L_get_cost_dis.fiscal_doc_line_id;
--
      end if;
--
   END LOOP;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_DETAIL',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
--
      if C_LOCK_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_DOC_DETAIL;
      end if;
---
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END CHECK_COST_DISCREP_SI;
----------------------------------------------------------------------------------
-- Function Name: CHECK_ITEM_LEVEL_TAX_EXISTS
-- Purpose:       This function identifies if there are item level taxes available in fm_fiscal_doc_tax_detail
--                for fm_fiscal_doc_detail.
----------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LEVEL_TAX_EXISTS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                    ,O_count            IN OUT NUMBER
                                    ,I_fiscal_doc_id    IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                     )
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.CHECK_ITEM_LEVEL_TAX_EXISTS';
   L_key                 VARCHAR2(80) := 'fdd.fiscal_doc_id = '|| I_fiscal_doc_id;
   L_count               NUMBER;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
  cursor C_COUNT is
    SELECT count(1)
      FROM fm_fiscal_doc_detail  fdd
          ,fm_fiscal_doc_tax_detail fdtd
     WHERE fdd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
       AND fdd.fiscal_doc_id      = I_fiscal_doc_id
       AND fdtd.total_value > 0;
BEGIN
   OPEN C_COUNT;
   FETCH c_count INTO L_count;
   CLOSE c_count;
   O_count := l_count;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_COUNT%ISOPEN then
         close C_COUNT;
      end if;
      return FALSE;
END CHECK_ITEM_LEVEL_TAX_EXISTS;
--
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Function Name: GET_MISMATCH_TAX_DIS_HEAD
-- Purpose:       This function identifies all the tax codes which is extra in the NF tables
--                or missing from the NF tables and inserts Tax discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION GET_MISMATCH_TAX_DIS_HEAD(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_fiscal_doc_id      IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                 )
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.GET_MISMATCH_TAX_DIS_HEAD';
   L_key                 VARCHAR2(80) := 'fdthe.fiscal_doc_id = '||I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   L_tax_discrep_status  VARCHAR2(1)  := 'D';
   L_discrep_type        VARCHAR2(1)  := 'T';
   L_yes                 VARCHAR2(1)  := 'Y';
   L_NO                 VARCHAR2(1) := 'N';
   L_count               NUMBER;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   cursor C_GET_TAX_MISMATCH is
      SELECT fdthe.fiscal_doc_id
            ,fdthe.tax_code tax_code
            ,fdtde.fiscal_doc_line_id
        FROM fm_fiscal_doc_tax_head_ext fdthe
            ,fm_fiscal_doc_tax_detail_ext fdtde
            ,fm_tax_codes ftc
            ,fm_fiscal_doc_detail ffdd
       WHERE fdthe.fiscal_doc_id = fdtde.fiscal_doc_id
         AND fdthe.tax_code = fdtde.tax_code
         AND fdthe.tax_code = ftc.tax_code
         AND fdthe.fiscal_doc_id = I_fiscal_doc_id
         AND ftc.matching_ind = L_yes
         AND fdthe.tax_value > 0
         AND NOT EXISTS (SELECT 1
                           FROM fm_fiscal_doc_tax_head fdth
                          WHERE fdth.vat_code = fdthe.tax_code
                            AND fdthe.fiscal_doc_id = fdth.fiscal_doc_id)
         AND ffdd.fiscal_doc_line_id = fdtde.fiscal_doc_line_id
         AND nvl(ffdd.unexpected_item,L_no) = L_no
   UNION
       SELECT fdth.fiscal_doc_id
             ,fdth.vat_code tax_code
             ,fdtd.fiscal_doc_line_id
         FROM fm_fiscal_doc_tax_head fdth
             ,fm_fiscal_doc_detail fdd
             ,fm_fiscal_doc_tax_detail fdtd
             ,fm_tax_codes ftc
        WHERE fdth.fiscal_doc_id = fdd.fiscal_doc_id
          AND fdd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
          AND fdth.vat_code = fdtd.vat_code
          AND fdtd.vat_code = ftc.tax_code
          AND fdth.fiscal_doc_id = I_fiscal_doc_id
          AND ftc.matching_ind = L_yes
          AND NOT EXISTS (SELECT 1
                            FROM fm_fiscal_doc_tax_head_ext fdthe
                           WHERE fdth.vat_code = fdthe.tax_code
                             AND fdthe.fiscal_doc_id = fdth.fiscal_doc_id)
          AND nvl(fdd.unexpected_item,L_no) = L_no ;


   cursor C_LOCK_FM_FISCAL_DOC_HEADER(I_FISCAL_DOC_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_header fdth
       WHERE fdth.fiscal_doc_id = I_fiscal_doc_id;
        -- FOR update nowait;
BEGIN
--
--
   FOR L_GET_TAX_MISMATCH IN C_GET_TAX_MISMATCH
   LOOP

      INSERT INTO FM_RESOLUTION(  fiscal_doc_id
                                 ,fiscal_doc_line_id
                                 ,discrep_type
                                 ,tax_code
                                 ,resolution_type
                                 ,create_datetime
                                 ,create_id
                                 ,last_update_datetime
                                 ,last_update_id
                                )values
                               (
                                 L_get_tax_mismatch.fiscal_doc_id
                                ,L_get_tax_mismatch.fiscal_doc_line_id
                                ,L_discrep_type
                                ,L_get_tax_mismatch.tax_code
                                ,NULL
                                ,sysdate
                                ,user
                                ,sysdate
                                ,user
                               );
--
      L_key1:= 'fiscal_doc_id   = '||L_get_tax_mismatch.fiscal_doc_id;
--
      OPEN C_LOCK_FM_FISCAL_DOC_HEADER(L_get_tax_mismatch.fiscal_doc_id);
      CLOSE C_LOCK_FM_FISCAL_DOC_HEADER;
--
      UPDATE fm_fiscal_doc_header
         SET tax_discrep_status   = L_tax_discrep_status
            ,last_update_datetime = sysdate
            ,last_update_id       = user
       WHERE fiscal_doc_id        = L_get_tax_mismatch.fiscal_doc_id;
--
      UPDATE fm_fiscal_doc_detail
         SET tax_discrep_status   = L_tax_discrep_status
            ,last_update_datetime = sysdate
            ,last_update_id       = user
       WHERE fiscal_doc_line_id        = L_get_tax_mismatch.fiscal_doc_line_id;
--
   END LOOP;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
---
      if C_LOCK_FM_FISCAL_DOC_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_DOC_HEADER;
      end if;
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
   return FALSE;
END GET_MISMATCH_TAX_DIS_HEAD;
--
----------------------------------------------------------------------------------
-- Function Name: GET_NF_TAX_HEAD_DIS
-- Purpose:       This function identifies all the tax discrepant in
--                fm_fiscal_doc_tax_head and fm_fiscal_doc_tax_head_ext based on the matching indicator in fm_tax_codes
----------------------------------------------------------------------------------
FUNCTION GET_NF_TAX_HEAD_DIS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                            ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.GET_MISMATCH_TAX_DISCREP';
   L_key                 VARCHAR2(80) := 'fdth.fiscal_doc_id ='|| I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   L_tax_discrep_status  VARCHAR2(1)  := 'D';
   L_discrep_type        VARCHAR2(1)  := 'T';
   L_yes                 VARCHAR2(1)  := 'Y';
   L_no                  VARCHAR2(1)  := 'N';
   L_count               NUMBER;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message      VARCHAR2(255);
   L_discrepant         VARCHAR2(1):=NULL;
--
   cursor C_NF_TAX_HEAD_DIS is
      SELECT fdth.fiscal_doc_id
            ,fdtd.fiscal_doc_line_id
            ,fdth.vat_code tax_code
            ,fdth.tax_basis   nf_tax_basis
            ,fdthe.tax_basis  sys_tax_basis
            ,fdth.total_value nf_tax_value
            ,fdthe.tax_value  sys_tax_value
        FROM fm_fiscal_doc_tax_head fdth,
             fm_fiscal_doc_tax_head_ext fdthe,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_tax_detail fdtd,
             fm_tax_codes ftc
       WHERE fdth.fiscal_doc_id      = fdthe.fiscal_doc_id
         AND fdth.vat_code           = fdthe.tax_code
         AND fdthe.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdd.fiscal_doc_line_id  = fdtd.fiscal_doc_line_id
         AND fdtd.vat_code           = fdthe.tax_code
         AND fdth.vat_code           = ftc.tax_code
         AND fdth.vat_code           = fdthe.tax_code
         AND fdth.total_value <> fdthe.tax_value
         AND fdth.fiscal_doc_id      = I_fiscal_doc_id
         AND ftc.matching_ind        = L_yes
         AND nvl(fdd.unexpected_item,L_no) = L_no;
   /*
   CURSOR C_NF_TAX_HEAD_DIS is
     SELECT fdth.fiscal_doc_id
            ,fdtd.fiscal_doc_line_id
            ,fdth.vat_code tax_code
            ,(fdth.tax_basis - fdtde.tax_basis) nf_tax_basis
            ,(fdthe.tax_basis - fdtde.tax_basis) sys_tax_basis
            ,(fdth.total_value - fdtde.tax_value) nf_tax_value
            ,(fdthe.tax_value - fdtde.tax_value)  sys_tax_value
        FROM fm_fiscal_doc_tax_head fdth,
             fm_fiscal_doc_tax_head_ext fdthe,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_tax_detail fdtd,
             fm_fiscal_doc_tax_detail_ext fdtde,
             fm_tax_codes ftc
       WHERE fdth.fiscal_doc_id      = fdthe.fiscal_doc_id
         AND fdth.vat_code           = fdthe.tax_code
         AND fdthe.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdd.fiscal_doc_line_id  = fdtd.fiscal_doc_line_id
         AND fdtd.fiscal_doc_line_id = fdtde.fiscal_doc_line_id
         AND fdthe.fiscal_doc_id     = fdtde.fiscal_doc_id
         AND fdtd.vat_code           = fdthe.tax_code
         AND fdthe.tax_code          = fdtde.tax_code
         AND fdth.vat_code           = ftc.tax_code
         AND fdth.vat_code           = fdthe.tax_code
         AND ( (fdth.tax_basis - fdtde.tax_basis)      <> (fdthe.tax_basis - fdtde.tax_basis)
               OR (fdth.total_value - fdtde.tax_value) <> (fdthe.tax_value - fdtde.tax_value))
         AND fdth.fiscal_doc_id      = I_fiscal_doc_id
         AND ftc.matching_ind        = 'Y'
         AND nvl(fdd.unexpected_item,'N') = 'N';
     */
--
   CURSOR C_LOCK_FM_FISCAL_DOC_HEADER(I_FISCAL_DOC_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_header fdth
       WHERE fdth.fiscal_doc_id = I_fiscal_doc_id;
        -- FOR update nowait;
BEGIN
--
--
   FOR L_NF_TAX_HEAD_DIS IN C_NF_TAX_HEAD_DIS
   LOOP
      if FM_DISCREP_IDENTIFICATION_SQL.GET_TAX_TOLERANCE(L_error_message ,
                                                         L_discrepant    ,
                                                         nvl(L_nf_tax_head_dis.nf_tax_value,0),
                                                         nvl(L_nf_tax_head_dis.sys_tax_value,0)
                                                         ) = FALSE then
            return FALSE;
      end if;
--

      if L_discrepant = 'Y' then

         INSERT INTO FM_RESOLUTION(  fiscal_doc_id
                                    ,fiscal_doc_line_id
                                    ,discrep_type
                                    ,tax_code
                                    ,resolution_type
                                    ,create_datetime
                                    ,create_id
                                    ,last_update_datetime
                                    ,last_update_id
                                   )values
                                  (
                                    L_nf_tax_head_dis.fiscal_doc_id
                                   ,L_nf_tax_head_dis.fiscal_doc_line_id
                                   ,L_discrep_type
                                   ,L_nf_tax_head_dis.tax_code
                                   ,NULL
                                   ,sysdate
                                   ,user
                                   ,sysdate
                                   ,user
                                  );
--
         L_key1:= 'fiscal_doc_id   = '||L_nf_tax_head_dis.fiscal_doc_id;
--
         OPEN C_LOCK_FM_FISCAL_DOC_HEADER(L_nf_tax_head_dis.fiscal_doc_id);
         CLOSE C_LOCK_FM_FISCAL_DOC_HEADER;
--
         UPDATE fm_fiscal_doc_header
            SET tax_discrep_status   = L_tax_discrep_status
               ,last_update_datetime = sysdate
               ,last_update_id       = user
          WHERE fiscal_doc_id        = L_nf_tax_head_dis.fiscal_doc_id;
--
         UPDATE fm_fiscal_doc_detail
            SET tax_discrep_status   = L_tax_discrep_status
               ,last_update_datetime = sysdate
               ,last_update_id       = user
          WHERE fiscal_doc_line_id        = L_nf_tax_head_dis.fiscal_doc_line_id;
--
      end if; --L_discrepant
   END LOOP;
--
   UPDATE fm_fiscal_doc_header
      SET tax_discrep_status   = 'M'
         ,last_update_datetime = sysdate
         ,last_update_id       = user
    WHERE fiscal_doc_id        = I_fiscal_doc_id
      AND tax_discrep_status is null;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
---
      if C_LOCK_FM_FISCAL_DOC_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_DOC_HEADER;
      end if;
---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
---
   return FALSE;
END GET_NF_TAX_HEAD_DIS;
--
----------------------------------------------------------------------------------
-- Function Name: GET_NF_TAX_HEAD_DIS_SI
-- Purpose:       This function identifies all the tax discrepant in
--                fm_fiscal_doc_tax_head and fm_fiscal_doc_tax_head_ext based on the matching indicator in fm_tax_codes for unexpected items.
----------------------------------------------------------------------------------
FUNCTION GET_NF_TAX_HEAD_DIS_SI(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                               ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.GET_MISMATCH_TAX_DISCREP_SI';
   L_key                 VARCHAR2(80) := 'fdth.fiscal_doc_id ='|| I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   L_tax_discrep_status  VARCHAR2(1)  := 'D';
   L_discrep_type        VARCHAR2(1)  := 'T';
   L_yes                 VARCHAR2(1)  := 'Y';
   L_count               NUMBER;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message      VARCHAR2(255);
   L_discrepant         VARCHAR2(1):=NULL;
--
/*
   cursor C_NF_TAX_HEAD_DIS is
      SELECT fdth.fiscal_doc_id
            ,fdtd.fiscal_doc_line_id
            ,fdth.vat_code tax_code
            ,(fdth.tax_basis - fdtde.tax_basis)   nf_tax_basis
            ,(fdthe.tax_basis - fdtde.tax_basis)  sys_tax_basis
            ,(fdth.total_value - fdtde.tax_value) nf_tax_value
            ,(fdthe.tax_value - fdtde.tax_value)  sys_tax_value
        FROM fm_fiscal_doc_tax_head fdth,
             fm_fiscal_doc_tax_head_ext fdthe,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_tax_detail fdtd,
             fm_fiscal_doc_tax_detail_ext fdtde,
             fm_tax_codes ftc
       WHERE fdth.fiscal_doc_id      = fdthe.fiscal_doc_id
         AND fdth.vat_code           = fdthe.tax_code
         AND fdthe.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdd.fiscal_doc_line_id  = fdtd.fiscal_doc_line_id
         AND fdtd.fiscal_doc_line_id = fdtde.fiscal_doc_line_id
         AND fdthe.fiscal_doc_id     = fdtde.fiscal_doc_id
         AND fdtd.vat_code           = fdthe.tax_code
         AND fdthe.tax_code          = fdtde.tax_code
         AND fdth.vat_code           = ftc.tax_code
         AND fdth.vat_code           = fdthe.tax_code
         AND ( (fdth.tax_basis - fdtde.tax_basis)      <> (fdthe.tax_basis - fdtde.tax_basis)
               OR (fdth.total_value - fdtde.tax_value) <> (fdthe.tax_value - fdtde.tax_value))
         AND fdth.fiscal_doc_id      = I_fiscal_doc_id
         AND ftc.matching_ind        = 'Y'
         AND fdd.unexpected_item     = 'Y';
*/
   cursor C_NF_TAX_HEAD_DIS is
      SELECT fdth.fiscal_doc_id
            ,fdtd.fiscal_doc_line_id
            ,fdth.vat_code tax_code
            ,fdth.tax_basis   nf_tax_basis
            ,fdthe.tax_basis  sys_tax_basis
            ,fdth.total_value nf_tax_value
            ,fdthe.tax_value  sys_tax_value
        FROM fm_fiscal_doc_tax_head fdth,
             fm_fiscal_doc_tax_head_ext fdthe,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_tax_detail fdtd,
             fm_tax_codes ftc
       WHERE fdth.fiscal_doc_id      = fdthe.fiscal_doc_id
         AND fdth.vat_code           = fdthe.tax_code
         AND fdthe.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdd.fiscal_doc_line_id  = fdtd.fiscal_doc_line_id
         AND fdtd.vat_code           = fdthe.tax_code
         AND fdth.vat_code           = ftc.tax_code
         AND fdth.vat_code           = fdthe.tax_code
         AND fdth.total_value <> fdthe.tax_value
         AND fdth.fiscal_doc_id      = I_fiscal_doc_id
         AND ftc.matching_ind        = L_yes
         AND fdd.unexpected_item     = L_yes;

--
   CURSOR C_LOCK_FM_FISCAL_DOC_HEADER(I_FISCAL_DOC_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_header fdth
       WHERE fdth.fiscal_doc_id = I_fiscal_doc_id;
        -- FOR update nowait;
BEGIN
--
--
   FOR L_NF_TAX_HEAD_DIS IN C_NF_TAX_HEAD_DIS
   LOOP
      if FM_DISCREP_IDENTIFICATION_SQL.GET_TAX_TOLERANCE(L_error_message ,
                                                         L_discrepant    ,
                                                         nvl(L_nf_tax_head_dis.nf_tax_value,0),
                                                         nvl(L_nf_tax_head_dis.sys_tax_value,0)
                                                         ) = FALSE then
            return FALSE;
     end if;
--
      if L_discrepant = 'Y' then

         INSERT INTO FM_RESOLUTION(  fiscal_doc_id
                                    ,fiscal_doc_line_id
                                    ,discrep_type
                                    ,tax_code
                                    ,resolution_type
                                    ,create_datetime
                                    ,create_id
                                    ,last_update_datetime
                                    ,last_update_id
                                   )values
                                  (
                                    L_nf_tax_head_dis.fiscal_doc_id
                                   ,L_nf_tax_head_dis.fiscal_doc_line_id
                                   ,L_discrep_type
                                   ,L_nf_tax_head_dis.tax_code
                                   ,NULL
                                   ,sysdate
                                   ,user
                                   ,sysdate
                                   ,user
                                  );
   --
         L_key1:= 'fiscal_doc_id   = '||L_nf_tax_head_dis.fiscal_doc_id;
   --
         OPEN C_LOCK_FM_FISCAL_DOC_HEADER(L_nf_tax_head_dis.fiscal_doc_id);
         CLOSE C_LOCK_FM_FISCAL_DOC_HEADER;
   --
         UPDATE fm_fiscal_doc_header
            SET tax_discrep_status   = L_tax_discrep_status
               ,last_update_datetime = sysdate
               ,last_update_id       = user
          WHERE fiscal_doc_id        = L_nf_tax_head_dis.fiscal_doc_id;
--
         UPDATE fm_fiscal_doc_detail
            SET tax_discrep_status   = L_tax_discrep_status
               ,last_update_datetime = sysdate
               ,last_update_id       = user
          WHERE fiscal_doc_line_id        = L_nf_tax_head_dis.fiscal_doc_line_id;
--
      end if;
--
   END LOOP;
--
   UPDATE fm_fiscal_doc_header
      SET tax_discrep_status   = 'M'
         ,last_update_datetime = sysdate
         ,last_update_id       = user
    WHERE fiscal_doc_id        = I_fiscal_doc_id
      AND tax_discrep_status is null;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
---
      if C_LOCK_FM_FISCAL_DOC_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_DOC_HEADER;
      end if;
---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
---
   return FALSE;
END GET_NF_TAX_HEAD_DIS_SI;
------------------------------------------------------------------------------------
-- Function Name: GET_MISMATCH_TAX_DIS_DETAIL
-- Purpose:       This function identifies all the tax codes which is extra in the NF Detail tables
--                or missing from the NF Detail tables and inserts Tax discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION GET_MISMATCH_TAX_DIS_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                    ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                  )
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.GET_MISMATCH_TAX_DIS_DETAIL';
   L_key                 VARCHAR2(80) := 'fdd.fiscal_doc_id = '|| I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   L_tax_discrep_status  VARCHAR2(1)  := 'D';
   L_discrep_type        VARCHAR2(1)  := 'T';
   L_yes                 VARCHAR2(1)  := 'Y';
   L_no                  VARCHAR2(1)  := 'N';
   L_count               NUMBER;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   cursor C_GET_DETAIL_TAX_MISMATCH is
    SELECT  fdtde.fiscal_doc_id
           ,fdd.fiscal_doc_line_id
           ,fdtde.tax_code tax_code
      FROM  fm_fiscal_doc_tax_detail_ext fdtde
           ,fm_fiscal_doc_detail fdd
           ,fm_tax_codes ftc
     WHERE fdtde.fiscal_doc_id = fdd.fiscal_doc_id
       AND fdtde.fiscal_doc_line_id = fdd.fiscal_doc_line_id
       AND fdtde.tax_code = ftc.tax_code
       AND fdd.fiscal_doc_id   = I_fiscal_doc_id
       AND nvl(fdd.unexpected_item,L_no) = L_no
       AND ftc.matching_ind = L_yes
       AND fdtde.tax_value > 0
       AND NOT EXISTS (SELECT 1
                         FROM fm_fiscal_doc_tax_detail fdtd
                        WHERE fdtd.vat_code = fdtde.tax_code
                          AND fdtde.fiscal_doc_line_id = fdtd.fiscal_doc_line_id)
     UNION
    SELECT fdd.fiscal_doc_id
          ,fdtd.fiscal_doc_line_id
          ,fdtd.vat_code tax_code
      FROM fm_fiscal_doc_tax_detail fdtd
          ,fm_fiscal_doc_detail fdd
          ,fm_tax_codes ftc
     WHERE fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
       AND fdtd.vat_code = ftc.tax_code
       AND fdd.fiscal_doc_id   = I_fiscal_doc_id
       AND nvl(fdd.unexpected_item,L_no) = L_no
       AND ftc.matching_ind = L_yes
       AND NOT EXISTS (SELECT 1
                         FROM fm_fiscal_doc_tax_detail_ext fdtde
                        WHERE fdtd.vat_code = fdtde.tax_code
                          AND fdtde.fiscal_doc_line_id = fdtd.fiscal_doc_line_id);
--
   cursor C_LOCK_FM_FISCAL_DOC_DETAIL(I_FISCAL_DOC_LINE_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_detail fdtd
       WHERE fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
        -- FOR update nowait;
BEGIN
--
--
   FOR L_GET_DETAIL_TAX_MISMATCH IN C_GET_DETAIL_TAX_MISMATCH
   LOOP

      INSERT INTO FM_RESOLUTION(  fiscal_doc_id
                                 ,fiscal_doc_line_id
                                 ,discrep_type
                                 ,tax_code
                                 ,resolution_type
                                 ,create_datetime
                                 ,create_id
                                 ,last_update_datetime
                                 ,last_update_id
                                )values
                                (
                                 L_get_detail_tax_mismatch.fiscal_doc_id
                                ,L_get_detail_tax_mismatch.fiscal_doc_line_id
                                ,L_discrep_type
                                ,L_get_detail_tax_mismatch.tax_code
                                ,NULL
                                ,sysdate
                                ,user
                                ,sysdate
                                ,user
                               );
--
      L_key1 := 'GET_MISMATCH_TAX_DIS_DETAIL - fiscal_doc_line_id = '||L_get_detail_tax_mismatch.fiscal_doc_line_id;
      OPEN C_LOCK_FM_FISCAL_DOC_DETAIL(L_get_detail_tax_mismatch.fiscal_doc_line_id);
      CLOSE C_LOCK_FM_FISCAL_DOC_DETAIL;
--
      UPDATE fm_fiscal_doc_detail
         SET tax_discrep_status   = L_tax_discrep_status
            ,last_update_datetime = sysdate
            ,last_update_id       = user
       WHERE fiscal_doc_id        = L_get_detail_tax_mismatch.fiscal_doc_id
         AND fiscal_doc_line_id   = L_get_detail_tax_mismatch.fiscal_doc_line_id;
--
   END LOOP;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_DETAIL',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
---
      if C_LOCK_FM_FISCAL_DOC_DETAIL%ISOPEN then
           close C_LOCK_FM_FISCAL_DOC_DETAIL;
      end if;
---
    return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
---
   return FALSE;
END GET_MISMATCH_TAX_DIS_DETAIL;
--
----------------------------------------------------------------------------------
-- Function Name: GET_NF_TAX_DETAIL_DIS
-- Purpose:       This function identifies all the tax discrepant in
--                fm_fiscal_doc_tax_detail and fm_fiscal_doc_tax_detail_ext based on the matching indicator in fm_tax_codes
----------------------------------------------------------------------------------
FUNCTION GET_NF_TAX_DETAIL_DIS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.GET_NF_TAX_DETAIL_DIS';
   L_key                 VARCHAR2(80) := 'fdtde.fiscal_doc_id ='|| I_fiscal_doc_id;
   L_key1                VARCHAR2(80);
   L_tax_discrep_status  VARCHAR2(1)  := 'D';
   L_discrep_type        VARCHAR2(1)  := 'T';
   L_yes                 VARCHAR2(1)  := 'Y';
   L_no                  VARCHAR2(1)  := 'N';
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   L_table               VARCHAR2(30):='fm_fiscal_doc_detail';
--
   L_error_message      VARCHAR2(255);
   L_discrepant         VARCHAR2(1):=NULL;
--
   cursor C_NF_TAX_DETAIL_DIS is
      SELECT fdtd.fiscal_doc_line_id
             ,fdtde.fiscal_doc_id
             ,fdtd.vat_code tax_code
             ,fdtd.total_value nf_value
             ,fdtde.tax_value system_value
             ,decode(fdtd.modified_tax_basis,null,fdtd.tax_basis,fdtd.modified_tax_basis) nf_tax_basis
             --,nvl(fdtd.tax_basis, fdtd.modified_tax_basis)   nf_tax_basis
             ,fdtde.modified_tax_basis sys_tax_basis
             ,fdtd.percentage_rate nf_tax_rate
             ,fdtde.tax_rate       sys_tax_rate
        FROM fm_fiscal_doc_tax_detail fdtd,
             fm_fiscal_doc_tax_detail_ext fdtde,
             fm_fiscal_doc_detail fdd,
             fm_tax_codes ftc
       WHERE fdtd.fiscal_doc_line_id = fdtde.fiscal_doc_line_id
         AND fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         AND fdtd.vat_code           = fdtde.tax_code
         AND fdtd.vat_code           = ftc.tax_code
         AND fdtde.fiscal_doc_id     = I_fiscal_doc_id
         AND nvl(unexpected_item,'N')= 'N'
         AND ftc.matching_ind        = 'Y'
         AND ((fdtde.tax_type = 'P'
               and (decode(fdtd.modified_tax_basis,null,fdtd.tax_basis,fdtd.modified_tax_basis)     <> fdtde.modified_tax_basis
              OR  fdtd.total_value     <> fdtde.tax_value
              OR  fdtd.percentage_rate <> fdtde.tax_rate))
              or(fdtde.tax_type = 'A'
                and fdtd.total_value   <> fdtde.tax_value));
--
   cursor C_LOCK_FM_FISCAL_DOC_DETAIL(I_FISCAL_DOC_LINE_ID IN NUMBER) is
      SELECT 'x'
        FROM fm_fiscal_doc_detail fdtd
       WHERE fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
         --FOR update nowait;
BEGIN
--
--

   FOR L_NF_TAX_DETAIL_DIS IN C_NF_TAX_DETAIL_DIS
   LOOP


      if FM_DISCREP_IDENTIFICATION_SQL.GET_TAX_TOLERANCE(L_error_message ,
                                                         L_discrepant    ,
                                                         nvl(L_nf_tax_detail_dis.nf_value,0),
                                                         nvl(L_nf_tax_detail_dis.system_value,0)
                                                         ) = FALSE then
         return FALSE;
      end if;
--
      if L_discrepant = 'N' then
         if FM_DISCREP_IDENTIFICATION_SQL.GET_TAX_TOLERANCE(L_error_message ,
                                                            L_discrepant    ,
                                                            nvl(L_nf_tax_detail_dis.nf_tax_basis,0),
                                                            nvl(L_nf_tax_detail_dis.sys_tax_basis,0)
                                                            ) = FALSE then
            return FALSE;
         end if;
      end if;
--
      if L_discrepant = 'Y' then
--
         INSERT INTO FM_RESOLUTION(  fiscal_doc_id
                                    ,fiscal_doc_line_id
                                    ,discrep_type
                                    ,tax_code
                                    ,resolution_type
                                    ,create_datetime
                                    ,create_id
                                    ,last_update_datetime
                                    ,last_update_id
                                    )values
                                    (
                                    L_nf_tax_detail_dis.fiscal_doc_id
                                   ,L_nf_tax_detail_dis.fiscal_doc_line_id
                                   ,L_discrep_type
                                   ,L_nf_tax_detail_dis.tax_code
                                   ,NULL
                                   ,sysdate
                                   ,user
                                   ,sysdate
                                   ,user
                                   );
--
         L_key1:= 'fiscal_doc_line_id   = '||L_nf_tax_detail_dis.fiscal_doc_line_id;
--
         OPEN C_LOCK_FM_FISCAL_DOC_DETAIL(L_nf_tax_detail_dis.fiscal_doc_line_id);
         CLOSE C_LOCK_FM_FISCAL_DOC_DETAIL;
--
         UPDATE fm_fiscal_doc_detail
            SET tax_discrep_status   = L_tax_discrep_status
               ,last_update_datetime = sysdate
               ,last_update_id       = user
          WHERE fiscal_doc_line_id   = L_nf_tax_detail_dis.fiscal_doc_line_id;
--
      end if;
--
   END LOOP;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_DETAIL',
                                             L_key1,
                                             TO_CHAR(SQLCODE));
--
      if C_LOCK_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_LOCK_FM_FISCAL_DOC_DETAIL;
      end if;
---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
END GET_NF_TAX_DETAIL_DIS;
----------------------------------------------------------------------------------
-- Function Name: CHECK_TAX_DISCREP
-- Purpose:       This function identifies all the Tax discrepancy
--                and inserts Tax discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_TAX_DISCREP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.CHECK_TAX_DISCREP';
   L_count               NUMBER;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   L_triangulation       VARCHAR2(1);
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   L_key                 VARCHAR2(100):= ' . Fiscal Doc Id '||I_fiscal_doc_id;
   L_key1                VARCHAR2(100);
BEGIN
--
   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_ITEM_LEVEL_TAX_EXISTS(O_error_message
                                                               ,L_count
                                                               ,I_fiscal_doc_id) = FALSE then
      return FALSE;
   end if;
--
   if L_count > 0 then
--
      if FM_DISCREP_IDENTIFICATION_SQL.GET_MISMATCH_TAX_DIS_DETAIL(O_error_message,I_fiscal_doc_id) = FALSE then
         return FALSE;
      end if;
--
      if FM_DISCREP_IDENTIFICATION_SQL.GET_NF_TAX_DETAIL_DIS(O_error_message,I_fiscal_doc_id) = FALSE then
         return FALSE;
      end if;
    else
--
       if FM_DISCREP_IDENTIFICATION_SQL.GET_MISMATCH_TAX_DIS_HEAD(O_error_message,I_fiscal_doc_id) = FALSE then
          return FALSE;
       end if;
--
       if FM_DISCREP_IDENTIFICATION_SQL.GET_NF_TAX_HEAD_DIS(O_error_message,I_fiscal_doc_id) = FALSE then
          return FALSE;
       end if;
--
   end if;
-- This code for Trangulation
   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message       => O_error_message
                                                           ,I_fiscal_doc_id       => I_fiscal_doc_id
                                                           ,O_triangulation       => L_triangulation
                                                           ,O_compl_fiscal_doc_id => L_compl_fiscal_doc_id) = FALSE then
      return FALSE;
   end if;
   if L_triangulation = 'Y' then
      L_key1 := 'Fiscal Doc Id '||L_compl_fiscal_doc_id;
      if FM_DISCREP_IDENTIFICATION_SQL.CHECK_ITEM_LEVEL_TAX_EXISTS(O_error_message
                                                                  ,L_count
                                                                  ,L_compl_fiscal_doc_id) = FALSE then
         return FALSE;
      end if;
--
      if L_count > 0 then
--
         if FM_DISCREP_IDENTIFICATION_SQL.GET_MISMATCH_TAX_DIS_DETAIL(O_error_message,L_compl_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;
--
         if FM_DISCREP_IDENTIFICATION_SQL.GET_NF_TAX_DETAIL_DIS(O_error_message,L_compl_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;
      else
--
         if FM_DISCREP_IDENTIFICATION_SQL.GET_MISMATCH_TAX_DIS_HEAD(O_error_message,L_compl_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;
--
         if FM_DISCREP_IDENTIFICATION_SQL.GET_NF_TAX_HEAD_DIS(O_error_message,L_compl_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;
--
         /*if FM_DISCREP_IDENTIFICATION_SQL.GET_NF_TAX_HEAD_DIS_SI(O_error_message,L_compl_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;*/
      end if;
   end if;
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
   return FALSE;
END CHECK_TAX_DISCREP;
--
----------------------------------------------------------------------------------
-- Function Name: GET_TOLERANCE
-- Purpose:       This function identifies if the nf value is withing the tolerance limit.
----------------------------------------------------------------------------------
FUNCTION GET_TOLERANCE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                      ,O_discripent      OUT    VARCHAR2
                      ,I_module          IN FM_FISCAL_DOC_HEADER.MODULE%TYPE
                      ,I_key_value_1     IN FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE
                      ,I_key_value_2     IN FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE
                      ,I_tolerance_level IN FM_TOLERANCES.TOLERANCE_LEVEL%TYPE
                      ,I_nf_value        IN FM_FISCAL_DOC_HEADER.QUANTITY%TYPE
                      ,I_system_value    IN FM_FISCAL_DOC_HEADER.QUANTITY%TYPE)
   return BOOLEAN is
--
   L_program            VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.GET_TOLERANCE';
   L_key                VARCHAR2(80) := 'ft.tolerance_level = '||I_tolerance_level||'ft.module ='||I_module||'ft.key_value_2 = '||I_key_value_2;
   L_tolerance_type     FM_TOLERANCES.TOLERANCE_TYPE%TYPE;
   L_tolerance_value    FM_TOLERANCES.TOLERANCE_VALUE%TYPE;
   L_comp               FM_TOLERANCES.MODULE%TYPE    := 'COMP';
   L_discrepant         VARCHAR2(1):=NULL;
--
   cursor C_GET_TOLERANCE is
     SELECT ft.tolerance_type
           ,ft.tolerance_value
       FROM fm_tolerances ft
      WHERE ft.key_value_1     = I_key_value_1
        AND ft.key_value_2     = I_key_value_2
        AND ft.module          = I_module
        AND ft.tolerance_level = I_tolerance_level --'QTY','COST'
        AND (I_nf_value > ft.lower_limit AND I_nf_value <= ft.upper_limit);
   ---
   cursor C_GET_TOLERANCE_COMP is
     SELECT ft.tolerance_type
           ,ft.tolerance_value
       FROM fm_tolerances ft
      WHERE ft.module          = L_comp
        AND ft.tolerance_level = I_tolerance_level --'QTY','COST'
        AND (I_nf_value > ft.lower_limit AND I_nf_value <= ft.upper_limit);
--
BEGIN
--
--
   OPEN C_GET_TOLERANCE;
--
   FETCH C_GET_TOLERANCE INTO L_tolerance_type , L_tolerance_value;
--
   ---
   if C_GET_TOLERANCE%NOTFOUND then
   ---
      OPEN C_GET_TOLERANCE_COMP;
      ---
      FETCH C_GET_TOLERANCE_COMP INTO L_tolerance_type , L_tolerance_value;
      ---
      CLOSE C_GET_TOLERANCE_COMP;
      ---
   end if;
   ---
   CLOSE C_GET_TOLERANCE;
--
   if L_tolerance_type = 'V' then
      if (I_nf_value - I_system_value) > nvl(L_tolerance_value,0) then
         L_discrepant := 'Y';
      elsif (I_nf_value - I_system_value) <= nvl(L_tolerance_value,0) then
         L_discrepant := 'N';
      end if;
   elsif L_tolerance_type = 'P' then
      if (I_system_value * nvl(L_tolerance_value,0) / 100) >= abs(I_nf_value - I_system_value) then
         L_discrepant := 'N';
      else
         L_discrepant := 'Y';
      end if;
   end if;
--
   O_discripent := nvl(L_discrepant,'Y');
--
   return TRUE;
--
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_GET_TOLERANCE%ISOPEN then
         close C_GET_TOLERANCE;
      end if;
      ---
      return FALSE;
END GET_TOLERANCE;
--
----------------------------------------------------------------------------------
-- Function Name: GET_TAX_TOLERANCE
-- Purpose:       This function identifies if the tax value is within the tolerance limit.
----------------------------------------------------------------------------------
FUNCTION GET_TAX_TOLERANCE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,O_discripent      OUT    VARCHAR2
                          ,I_total_value     IN FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE
                          ,I_tax_value       IN FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE
                           )
   return BOOLEAN is
--
   L_tolerance_type     VARCHAR2(30) :='CALC_TOL_TYPE';--'TAX_TOLERANCE_TYPE';
   L_tolerance_value    VARCHAR2(30) :='CALC_TOL_VALUE';
   L_program            VARCHAR2(50) :='FM_DISCREP_IDENTIFICATION_SQL.GET_TAX_TOLERANCE';
   L_key                VARCHAR2(80) :='Variable  = '||L_tolerance_type;
   L_string_value       FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_number_value       FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_discrepant         VARCHAR2(1):=NULL;
--
   cursor C_GET_TAX_TOLERANCE_TYPE is
     SELECT fmo.string_value
       FROM fm_system_options fmo
      WHERE fmo.variable = L_tolerance_type;
--
   cursor C_GET_TAX_TOLERANCE_VALUE is
     SELECT fmo.number_value
       FROM fm_system_options fmo
      WHERE fmo.variable = L_tolerance_value;
--
BEGIN
--
--
   OPEN C_GET_TAX_TOLERANCE_TYPE;
   FETCH C_GET_TAX_TOLERANCE_TYPE INTO L_string_value ;
   CLOSE C_GET_TAX_TOLERANCE_TYPE;
--
   OPEN C_GET_TAX_TOLERANCE_VALUE;
   FETCH C_GET_TAX_TOLERANCE_VALUE INTO L_number_value;
   CLOSE C_GET_TAX_TOLERANCE_VALUE;
--
   if L_string_value = 'V' then
      if (abs(I_total_value - I_tax_value) > nvl(L_number_value,0)) then
      --OR ((I_total_value - I_tax_value) < nvl(L_number_value,0)) then
         L_discrepant := 'Y';
      else
         L_discrepant := 'N';
      end if;
   elsif L_string_value = 'P' then
--
      if abs(I_total_value - I_tax_value) > (I_tax_value * nvl(L_number_value,0) / 100) then
--                                            OR
--         ((I_total_value * nvl(L_number_value,0) / 100) < (abs(I_total_value - I_tax_value))) then
--
         L_discrepant := 'Y';
      else
         L_discrepant := 'N';
      end if;
   end if;
--
   O_discripent := nvl(L_discrepant,'Y');
--
   return TRUE;
--
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_GET_TAX_TOLERANCE_TYPE%ISOPEN then
         close C_GET_TAX_TOLERANCE_TYPE;
      end if;
      ---
      if C_GET_TAX_TOLERANCE_VALUE%ISOPEN then
         close C_GET_TAX_TOLERANCE_VALUE;
      end if;
      return FALSE;
END GET_TAX_TOLERANCE;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_DISCREP_STATUS_MATCHED
-- Purpose:       This function updates all the discrepany status (Qty/Cost/Tax) to Matched
--                if there are no discrepancy for a given NF .
----------------------------------------------------------------------------------
FUNCTION UPDATE_DISCREP_STATUS_MATCHED(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                      ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.UPDATE_DISCREP_STATUS_MATCHED';
   L_key                 VARCHAR2(100):= 'I_FISCAL_DOC_ID :'||I_fiscal_doc_id;
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   L_yes                 VARCHAR2(1) := 'Y';
--
  CURSOR C_GET_COMPL_NF IS
      SELECT fdc.compl_fiscal_doc_id
        FROM fm_fiscal_doc_header fdh
            ,fm_fiscal_doc_detail fdd
            ,fm_fiscal_doc_complement fdc
            ,ordhead ord
       WHERE fdh.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdh.fiscal_doc_id     = fdc.fiscal_doc_id
         AND fdd.requisition_no    = ord.order_no
         AND ord.triangulation_ind = L_yes
         AND fdh.fiscal_doc_id     = I_fiscal_doc_id;
--
BEGIN
--
   OPEN C_GET_COMPL_NF;
   FETCH C_GET_COMPL_NF INTO L_compl_fiscal_doc_id;
   CLOSE C_GET_COMPL_NF;
--
   UPDATE fm_fiscal_doc_detail
      SET cost_discrep_status = 'M'
    WHERE fiscal_doc_id       = I_fiscal_doc_id
      AND cost_discrep_status is null;
--
   UPDATE fm_fiscal_doc_detail
      SET qty_discrep_status  = 'M'
    WHERE fiscal_doc_id       = I_fiscal_doc_id
      AND qty_discrep_status  is null;
--
   UPDATE fm_fiscal_doc_detail
      SET tax_discrep_status  = 'M'
    WHERE fiscal_doc_id       = I_fiscal_doc_id
      AND tax_discrep_status  is null;
--
   UPDATE fm_fiscal_doc_header
      SET tax_discrep_status   = 'M'
         ,last_update_datetime = sysdate
         ,last_update_id       = user
    WHERE fiscal_doc_id        = I_fiscal_doc_id
      AND tax_discrep_status is null;
--For Complementory NF
   UPDATE fm_fiscal_doc_detail
      SET cost_discrep_status = 'M'
    WHERE fiscal_doc_id       = L_compl_fiscal_doc_id
      AND cost_discrep_status is null;
--
   UPDATE fm_fiscal_doc_detail
      SET qty_discrep_status  = 'M'
    WHERE fiscal_doc_id       = L_compl_fiscal_doc_id
      AND qty_discrep_status  is null;
--
   UPDATE fm_fiscal_doc_detail
      SET tax_discrep_status  = 'M'
    WHERE fiscal_doc_id       = L_compl_fiscal_doc_id
      AND tax_discrep_status  is null;
--
   UPDATE fm_fiscal_doc_header
      SET tax_discrep_status   = 'M'
         ,last_update_datetime = sysdate
         ,last_update_id       = user
    WHERE fiscal_doc_id        = L_compl_fiscal_doc_id
      AND tax_discrep_status is null;
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
END UPDATE_DISCREP_STATUS_MATCHED;
----------------------------------------------------------------------------------
 FUNCTION CALC_LINE_TAXES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_status        IN OUT INTEGER,
                          O_other_proc    IN OUT BOOLEAN,
                          I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program              VARCHAR2(50) := 'FM_DISCREP_IDENTIFICATION_SQL.CALC_LINE_TAXES';
   L_table                VARCHAR2(30);
   L_key                  VARCHAR2(100);
   L_error_message        VARCHAR2(255) := NULL;
   L_head_tax_code        FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE;
   L_head_tax_discrep_st  FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE;
   L_sys_tax_po_item      FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE;
   L_sys_base_po_item     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE;
   L_sys_modified_base_po_item     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE;
   L_total_tax_value      FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE;
   L_total_base_value     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE;
   L_total_modified_base_value     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE;
   L_head_tax_value       FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
   L_head_base_value      FM_FISCAL_DOC_TAX_HEAD.TAX_BASIS%TYPE;
   L_head_mod_base_value      FM_FISCAL_DOC_TAX_HEAD.TAX_BASIS%TYPE;
   L_appr_tax_po_item     FM_FISCAL_DOC_TAX_DETAIL.APPR_TAX_VALUE%TYPE;
   L_appr_base_po_item    FM_FISCAL_DOC_TAX_DETAIL.APPR_BASE_VALUE%TYPE;
   L_appr_modified_base_po_item    FM_FISCAL_DOC_TAX_DETAIL.APPR_BASE_VALUE%TYPE;
   L_appr_rate_po_item    FM_FISCAL_DOC_TAX_DETAIL.APPR_TAX_RATE%TYPE;
   L_unit_tax_amt         FM_FISCAL_DOC_TAX_DETAIL.UNIT_TAX_AMT%TYPE;
   L_dummy                NUMBER;
   L_total_doc_value      FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_appr_base_po_item_calc    FM_FISCAL_DOC_TAX_DETAIL.APPR_BASE_VALUE%TYPE;
   ---
   C_tax_discrep_unres    FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE := 'D';
   C_tax_discrep_match    FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE := 'M';
   ---
   L_comp_nf_ind                 FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind                  ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_fiscal_doc_id         FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   ---
   cursor C_FM_HEAD_EXT_TAX_CODE (P_tax_code       FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE) is
      select 1
        from fm_fiscal_doc_tax_head_ext
       where fiscal_doc_id = I_fiscal_doc_id
         and tax_code      = P_tax_code;
   ---
   cursor C_GET_TOTAL_DOC_VALUE is
      select sum(unit_cost_with_disc * quantity) total_value
        from fm_fiscal_doc_detail
       where fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_FM_HEAD_TAX_CODE is
      select fdth.vat_code, fdth.total_value, fdth.tax_basis, fdth.modified_tax_basis
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_FM_FISCAL_DOC_DETAIL is
      select fdd.fiscal_doc_line_id, fdd.item, fdd.requisition_no, fdd.quantity
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_SYS_TAX_TOTAL (P_tax_code              FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE) is
      select decode(SUM(NVL(tax_value,0)),0,1,SUM(NVL(tax_value,0))), decode(SUM(NVL(tax_basis,0)),0,1,SUM(NVL(tax_basis,0))),decode(SUM(NVL(MODIFIED_TAX_BASIS,0)),0,1,SUM(NVL(MODIFIED_TAX_BASIS,0)))  --, decode(SUM(NVL(tax_rec_value,0)),0,1,SUM(NVL(tax_rec_value,0)))
        from fm_fiscal_doc_tax_detail_ext
       where fiscal_doc_id = I_fiscal_doc_id and
             tax_code = P_tax_code;
   ---
   cursor C_SYS_TAX_PO_ITEM (P_fiscal_doc_line_id    FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                             P_tax_code              FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE) is
      select NVL(tax_value,0), NVL(tax_basis,0), NVL(modified_tax_basis,0)
        from fm_fiscal_doc_tax_detail_ext
       where fiscal_doc_line_id = P_fiscal_doc_line_id and
             tax_code = P_tax_code;
   ---
   L_fiscal_doc_detail  C_FM_FISCAL_DOC_DETAIL%ROWTYPE;
   ---
BEGIN
   ---
   L_key := 'I_FISCAL_DOC_ID';
   ---

   open C_FM_HEAD_TAX_CODE;
   ---
   LOOP
      ---
      fetch C_FM_HEAD_TAX_CODE into L_head_tax_code, L_head_tax_value, L_head_base_value, L_head_mod_base_value;
      EXIT when C_FM_HEAD_TAX_CODE%NOTFOUND;
      ---
      open C_FM_HEAD_EXT_TAX_CODE (L_head_tax_code);
      ---
      fetch C_FM_HEAD_EXT_TAX_CODE into L_dummy;
      ---
      if C_FM_HEAD_EXT_TAX_CODE%NOTFOUND then
         ---
         open C_GET_TOTAL_DOC_VALUE;
         ---
         fetch C_GET_TOTAL_DOC_VALUE into L_total_doc_value;
         ---
         close C_GET_TOTAL_DOC_VALUE;
         ---
         insert into fm_fiscal_doc_tax_detail (vat_code,
                                               fiscal_doc_line_id,
                                               percentage_rate,
                                               total_value,
                                               appr_base_value,
                                               appr_modified_base_value,
                                               appr_tax_value,
                                               appr_tax_rate,
                                               create_datetime,
                                               create_id,
                                               last_update_datetime,
                                               last_update_id,
                                               tax_basis,
                                               unit_tax_amt)
                                        select fdth.vat_code,
                                               fdd.fiscal_doc_line_id,
                                               0,
                                               0,
                                               ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.tax_basis,
                                               ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.modified_tax_basis,
                                               ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.total_value,
                                               fdth.total_value / fdth.tax_basis * 100,
                                               sysdate,
                                               user,
                                               sysdate,
                                               user,
                                               null,
                                               ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.total_value / fdd.quantity
                                          from fm_fiscal_doc_tax_head fdth,
                                               fm_fiscal_doc_detail fdd
                                         where fdth.fiscal_doc_id = fdd.fiscal_doc_id
                                           and fdth.fiscal_doc_id = I_fiscal_doc_id
                                           and fdth.vat_code      = L_head_tax_code;
         ---
      else
         ---
         L_total_tax_value := 0;
         L_total_base_value := 0;
         ---
         open C_SYS_TAX_TOTAL(L_head_tax_code);
         ---
         fetch C_SYS_TAX_TOTAL into L_total_tax_value, L_total_base_value,L_total_modified_base_value;
         ---
         close C_SYS_TAX_TOTAL;
         ---
         open C_FM_FISCAL_DOC_DETAIL;
         ---
         LOOP
            fetch C_FM_FISCAL_DOC_DETAIL into L_fiscal_doc_detail;
            EXIT when C_FM_FISCAL_DOC_DETAIL%NOTFOUND;
            ---
            open C_SYS_TAX_PO_ITEM(L_fiscal_doc_detail.fiscal_doc_line_id, L_head_tax_code);
            ---
            fetch C_SYS_TAX_PO_ITEM into L_sys_tax_po_item, L_sys_base_po_item, L_sys_modified_base_po_item;
            ---
            if C_SYS_TAX_PO_ITEM%NOTFOUND then
               ---
               L_appr_tax_po_item := 0;
               L_appr_base_po_item := 0;
               L_appr_rate_po_item := 0;
               L_unit_tax_amt := 0;
               ---

            elsif C_SYS_TAX_PO_ITEM%FOUND then
               if L_total_tax_value = 0 then
                  L_appr_tax_po_item := 0;
               else
                  L_appr_tax_po_item := (L_sys_tax_po_item / L_total_tax_value) * L_head_tax_value;
               end if;
               ---
               if L_total_base_value = 0 and L_total_modified_base_value = 0 then
                  L_appr_base_po_item := 0;
                  L_appr_modified_base_po_item := 0;
               else
                  L_appr_base_po_item := (L_sys_base_po_item / L_total_base_value) * L_head_base_value;
                  L_appr_modified_base_po_item := (L_sys_modified_base_po_item / L_total_modified_base_value) * L_head_mod_base_value;
               end if;
               ---
               if L_appr_base_po_item = 0 and L_appr_modified_base_po_item = 0 then
                  L_appr_rate_po_item := 0;
               else

                  L_appr_base_po_item_calc := (Case
                                                 when nvl(L_appr_base_po_item,0) = 0 then
                                                      L_appr_modified_base_po_item
                                                 else
                                                      L_appr_base_po_item
                                                 end
                                              );
                  L_appr_rate_po_item := L_appr_tax_po_item / L_appr_base_po_item_calc * 100;
               end if;
               ---
               L_unit_tax_amt := L_appr_tax_po_item / L_fiscal_doc_detail.quantity;
               ---
            end if;
            ---

            if (L_sys_tax_po_item = 0 ) then




               open C_GET_TOTAL_DOC_VALUE;
         ---
               fetch C_GET_TOTAL_DOC_VALUE into L_total_doc_value;
               ---
               close C_GET_TOTAL_DOC_VALUE;


              insert into fm_fiscal_doc_tax_detail (vat_code,
                                               fiscal_doc_line_id,
                                               percentage_rate,
                                               total_value,
                                               appr_base_value,
                                               appr_modified_base_value,
                                               appr_tax_value,
                                               appr_tax_rate,
                                               create_datetime,
                                               create_id,
                                               last_update_datetime,
                                               last_update_id,
                                               tax_basis,
                                               unit_tax_amt)
                                        select fdth.vat_code,
                                               fdd.fiscal_doc_line_id,
                                               0,
                                               0,
                                               ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.tax_basis,
                                               ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.modified_tax_basis,
                                               ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.total_value,
                                               fdth.total_value / fdth.tax_basis * 100,
                                               sysdate,
                                               user,
                                               sysdate,
                                               user,
                                               null,
                                               ((fdd.unit_cost_with_disc * fdd.quantity) / L_total_doc_value)* fdth.total_value / fdd.quantity
                                          from fm_fiscal_doc_tax_head fdth,
                                               fm_fiscal_doc_detail fdd
                                         where fdth.fiscal_doc_id = fdd.fiscal_doc_id
                                           and fdth.fiscal_doc_id = I_fiscal_doc_id
                                           and fdth.vat_code      = L_head_tax_code
					   and fdd.fiscal_doc_line_id = L_fiscal_doc_detail.fiscal_doc_line_id;
                else

              insert into fm_fiscal_doc_tax_detail(vat_code,
                                                   fiscal_doc_line_id,
                                                   appr_tax_value,
                                                   appr_base_value,
                                                   appr_modified_base_value,
                                                   appr_tax_rate,
                                                   unit_tax_amt,
                                                   percentage_rate,
                                                   total_value,
                                                   create_datetime,
                                                   create_id,
                                                   last_update_datetime,
                                                   last_update_id)
                                           values (L_head_tax_code,
                                                   L_fiscal_doc_detail.fiscal_doc_line_id,
                                                   L_appr_tax_po_item,
                                                   L_appr_base_po_item,
                                                   L_appr_modified_base_po_item,
                                                   L_appr_rate_po_item,
                                                   L_unit_tax_amt,
                                                   0,
                                                   0,
                                                   sysdate,
                                                   user,
                                                   sysdate,
                                                   user);
                end if;
            ---
            close C_SYS_TAX_PO_ITEM;
            ---
         END LOOP;
         ---
         close C_FM_FISCAL_DOC_DETAIL;
         ---
      end if;
      ---
      close C_FM_HEAD_EXT_TAX_CODE;
      ---
   END LOOP;
   ---
   close C_FM_HEAD_TAX_CODE;
   ---
   if (FM_FISCAL_VALIDATION_SQL.GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;
   ---
   if L_triang_ind = 'Y' then
   ---
      if (FM_FISCAL_VALIDATION_SQL.GET_OTHER_NF(O_error_message, L_other_fiscal_doc_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      if not O_other_proc then
      ---
         O_other_proc := TRUE;
         if (CALC_LINE_TAXES(O_error_message, O_status, O_other_proc, L_other_fiscal_doc_id) = FALSE) then
            return FALSE;
         end if;
      ---
      end if;
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      if C_FM_HEAD_EXT_TAX_CODE%ISOPEN then
         close C_FM_HEAD_EXT_TAX_CODE;
      end if;
      ---
      if C_GET_TOTAL_DOC_VALUE%ISOPEN then
         close C_GET_TOTAL_DOC_VALUE;
      end if;
      ---
      if C_FM_HEAD_TAX_CODE%ISOPEN then
         close C_FM_HEAD_TAX_CODE;
      end if;
      ---
      if C_SYS_TAX_TOTAL%ISOPEN then
         close C_SYS_TAX_TOTAL;
      end if;
      ---
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      if C_SYS_TAX_PO_ITEM%ISOPEN then
         close C_SYS_TAX_PO_ITEM;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := 0;
      return FALSE;
END CALC_LINE_TAXES;
--------------------------------------------------------------------------------
FUNCTION UPD_LEGAL_MSG_REC_VAL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_status        IN OUT INTEGER,
                               O_other_proc    IN OUT BOOLEAN,
                               I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program              VARCHAR2(100) := 'FM_DISCREP_IDENTIFICATION_SQL.UPD_LEGAL_MSG_REC_VAL';
   L_table                VARCHAR2(30);
   L_key                  VARCHAR2(100);
   L_error_message        VARCHAR2(255) := NULL;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   L_legal_msg            FM_FISCAL_DOC_TAX_DETAIL.LEGAL_MESSAGE_TEXT%TYPE;
   L_rec_val              FM_FISCAL_DOC_TAX_DETAIL.REC_VALUE%TYPE;
   L_unit_rec_val         FM_FISCAL_DOC_TAX_DETAIL.UNIT_REC_VALUE%TYPE;
   L_tax_value            FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE;
   ---
   L_comp_nf_ind                 FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind                  ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_fiscal_doc_id         FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   ---
   cursor C_TAX_EXT_DETAILS (P_fiscal_doc_line_id     FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                             P_tax_code               FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE) is
      select legal_msg, tax_value, tax_rec_value
        from fm_fiscal_doc_tax_detail_ext
       where fiscal_doc_line_id = P_fiscal_doc_line_id and
             tax_code = P_tax_code;
   ---
   cursor C_TAX_DETAILS is
      select fdd.fiscal_doc_line_id, fdd.quantity, fdtd.vat_code, fdtd.total_value, fdtd.appr_tax_value
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_tax_detail fdtd
       where fdd.fiscal_doc_id = I_fiscal_doc_id and
             fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id;
   ---
   cursor C_UPDATE_DETAIL_TAXES (P_fiscal_doc_line_id     FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                 P_tax_code               FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE) is
      select 'X'
        from fm_fiscal_doc_tax_detail
       where fiscal_doc_line_id = P_fiscal_doc_line_id
         and vat_code = P_tax_code;
   ---
   L_tax_ext_details  C_TAX_EXT_DETAILS%ROWTYPE;
   L_tax_details      C_TAX_DETAILS%ROWTYPE;
   ---
BEGIN
   ---
   open C_TAX_DETAILS;
   ---
   LOOP
      ---
      fetch C_TAX_DETAILS into L_tax_details;
      EXIT when C_TAX_DETAILS%NOTFOUND;
      ---
      open C_TAX_EXT_DETAILS(L_tax_details.fiscal_doc_line_id, L_tax_details.vat_code);
      ---
      fetch C_TAX_EXT_DETAILS into L_tax_ext_details;
      ---
      if C_TAX_EXT_DETAILS%NOTFOUND then
         ---
         L_rec_val := 0;
         L_unit_rec_val := 0;
         L_legal_msg := NULL;
         ---
      else
         ---L_tax_value
         if L_tax_details.total_value = 0 then
            L_tax_value := L_tax_details.appr_tax_value;
         else
            L_tax_value := L_tax_details.total_value;
         end if;
         ---

         if (L_tax_ext_details.tax_value <> 0 ) then

           L_rec_val := (L_tax_ext_details.tax_rec_value / L_tax_ext_details.tax_value) * L_tax_value;
           L_unit_rec_val := L_rec_val / L_tax_details.quantity;
         else
             L_rec_val := 0;
             L_unit_rec_val := 0;
         end if;
         L_legal_msg := L_tax_ext_details.legal_msg;
         ---
      end if;
      ---
      close C_TAX_EXT_DETAILS;
      ---
      open C_UPDATE_DETAIL_TAXES(L_tax_details.fiscal_doc_line_id, L_tax_details.vat_code);
      close C_UPDATE_DETAIL_TAXES;
      ---
      ---
      update fm_fiscal_doc_tax_detail fdd
         set fdd.legal_message_text = L_legal_msg,
             fdd.rec_value = L_rec_val
             ,fdd.unit_rec_value = L_unit_rec_val
             ,fdd.last_update_datetime = sysdate
             ,fdd.last_update_id = user
       where fdd.fiscal_doc_line_id = L_tax_details.fiscal_doc_line_id and
             fdd.vat_code = L_tax_details.vat_code;
   ---
   END LOOP;
   ---
   close C_TAX_DETAILS;
   ---
   if (FM_FISCAL_VALIDATION_SQL.GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   end if;
   ---
   if L_triang_ind = 'Y' then
   ---
      if (FM_FISCAL_VALIDATION_SQL.GET_OTHER_NF(O_error_message, L_other_fiscal_doc_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
         return FALSE;
      end if;
      ---
      if not O_other_proc then
      ---
         O_other_proc := TRUE;
         if (UPD_LEGAL_MSG_REC_VAL(O_error_message, O_status, O_other_proc, L_other_fiscal_doc_id) = FALSE) then
            return FALSE;
         end if;
      ---
      end if;
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_TAX_DETAIL',
                                             L_key,
                                             TO_CHAR(SQLCODE));
--
      if C_UPDATE_DETAIL_TAXES%ISOPEN then
         close C_UPDATE_DETAIL_TAXES;
      end if;
---
      return FALSE;
--
   when OTHERS then
      if C_TAX_DETAILS%ISOPEN then
         close C_TAX_DETAILS;
      end if;
      ---
      if C_TAX_EXT_DETAILS%ISOPEN then
         close C_TAX_EXT_DETAILS;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := 0;
      return FALSE;
      ---
END UPD_LEGAL_MSG_REC_VAL;
--
--
END FM_DISCREP_IDENTIFICATION_SQL;
/