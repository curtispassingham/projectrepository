CREATE OR REPLACE PACKAGE L10N_BR_INT_SQL AS
-------------------------------------------------------------------------------------------------------------
-- Type declarations
-------------------------------------------------------------------------------------------------------------
TYPE result_data_tbl                IS TABLE OF L10N_BR_TAX_CALL_RES%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_eco_tbl                 IS TABLE OF L10N_BR_TAX_CALL_RES_ECO_CLASS%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_contrib_tbl             IS TABLE OF L10N_BR_TAX_CALL_RES_TAX_CNTRB%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_data_item_tbl           IS TABLE OF L10N_BR_TAX_CALL_RES_ITEM%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_data_item_tax_tbl       IS TABLE OF L10N_BR_TAX_CALL_RES_ITEM_TAX%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_data_item_sug_rule_tbl  IS TABLE OF L10N_BR_TAX_CALL_RES_ITEM_RULE%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_data_item_srvc_prd      IS TABLE OF L10N_BR_TAX_CALL_RES_SRVC_PRD%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_rms_tbl                  IS TABLE OF L10N_BR_TAX_CALL_STAGE_RMS%ROWTYPE INDEX BY BINARY_INTEGER;
---
TYPE stage_routing_tbl              IS TABLE OF L10N_BR_TAX_CALL_STAGE_ROUTING%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_eco_tbl                  IS TABLE OF L10N_BR_TAX_STAGE_ECO%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_fis_entity_tbl           IS TABLE OF L10N_BR_TAX_STAGE_FIS_ENTITY%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_item_tbl                 IS TABLE OF L10N_BR_TAX_STAGE_ITEM%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_name_value_tbl           IS TABLE OF L10N_BR_TAX_STAGE_NAME_VALUE%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_order_tbl                IS TABLE OF L10N_BR_TAX_STAGE_ORDER%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_order_exp_tbl            IS TABLE OF L10N_BR_TAX_STAGE_ORDER_EXP%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_order_info_tbl           IS TABLE OF L10N_BR_TAX_STAGE_ORDER_INFO%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stage_regime_tbl               IS TABLE OF L10N_BR_TAX_STAGE_REGIME%ROWTYPE INDEX BY BINARY_INTEGER;

-------------------------------------------------------------------------------------------------------------
-- Global public variables
-------------------------------------------------------------------------------------------------------------
LP_CALC_TAX  VARCHAR(10) := 'CALC_TAX';

-------------------------------------------------------------------------------------------------------------
-- Public function declarations
-------------------------------------------------------------------------------------------------------------
-- Function Name  : LOAD_TAX_OBJECT
-- Purpose        :
--
-- Required Input :
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_TAX_OBJECT (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_l10n_obj      IN OUT  L10N_OBJ)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : LOAD_ORDER_TAX_OBJECT
-- Purpose        :
--
-- Required Input :
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_ORDER_TAX_OBJECT (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_l10n_obj      IN OUT  L10N_OBJ)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : DEAL_ORDER_DISCOUNT
-- Purpose        :
--
--
-- Required Input : L10N_OBJ
-------------------------------------------------------------------------------------------------------------
FUNCTION DEAL_ORDER_DISCOUNT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_l10n_obj      IN OUT  L10N_OBJ)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : GET_REQUEST_DATA
-- Purpose        :
--
-- Required Input : L10N_OBJ
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_REQUEST_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                          I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : STAGE_RESULTS
-- Purpose        :
--
-- Required Input :
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_RESULTS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                       I_businessObject  IN      "RIB_FiscDocTaxColRBM_REC")
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : LOAD_UOM_INFO
-- Purpose        : This function is used for loading the UOM information before calling the Tax Engine.
--                  Called by packages LOAD_CANONICAL_OBJECT_PURCHASE, LOAD_CANONICAL_OBJECT_SALES, LOAD_CANONICAL_OBJECT_PO
--                  This will also be used by RFM.
-- Required Input : TBL_OBJ_DOCLINEITEMRBO
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_UOM_INFO(O_error_message      IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_doclineitem_tbl   IN OUT NOCOPY  "RIB_LineItemRBO_TBL",
                       I_l10n_tax_tbl       IN             OBJ_TAX_TBL,
                       I_nf_item            IN             ITEM_MASTER.ITEM%TYPE,
                       I_nf_quantity        IN             NUMBER,
                       I_nf_uom             IN             VARCHAR2,
                       I_order_no           IN             ORDHEAD.ORDER_NO%TYPE,
                       I_call_type          IN             VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
   FUNCTION LOAD_STAGE_TABLE(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tax_service_id  IN OUT         L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                             IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                             I_call_type       IN             VARCHAR2,
                             I_po_deal_ind     IN             VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION LOAD_PO_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                           I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION LOAD_SALE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                             I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION LOAD_PURCHASE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                                 I_tax_service_id  IN      NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                            I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                            I_call_type       IN             L10N_BR_TAX_CALL_STAGE_RMS.CALL_TYPE%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_PO_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                               I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_PURCHASE_SALE_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                          IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                                          I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION GET_TAX_INDS(O_error_message        IN OUT rtk_errors.rtk_text%type,
                         O_incl_nic_ind_pis        OUT vat_codes.incl_nic_ind%TYPE,
                         O_incl_nic_ind_cofins     OUT vat_codes.incl_nic_ind%TYPE,
                         O_incl_nic_ind_icms       OUT vat_codes.incl_nic_ind%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_TAX_REGIME(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_NAME_VALUE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_SUPPLIER(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_LOCATION(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_ITEM(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_PO_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_PURCHASE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_SALE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PERSIST_STAGE_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                               I_call_type       IN      VARCHAR2)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PERSIST_STAGE_RMS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE) 
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
      
$end
-------------------------------------------------------------------------------------------------------------
END L10N_BR_INT_SQL;
/
