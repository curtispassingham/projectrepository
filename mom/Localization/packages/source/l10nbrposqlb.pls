CREATE OR REPLACE PACKAGE BODY L10N_BR_PO_SQL AS
----------------------------------------------------------------------------------------------------
FUNCTION CHECK_UTIL_CODE_EXISTS (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_l10n_obj     IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'L10N_BR_PO_SQL.CHECK_UTIL_CODE_EXISTS';
   L_util_code          VARCHAR2(30);
   L_exists             VARCHAR2(1);
   L_l10n_exists_rec    L10N_EXISTS_REC;

   cursor C_util_code_exists_po is
      select 'x'
        from v_br_ord_util_code
       where order_no = L_l10n_exists_rec.doc_id;

   cursor C_util_code_exists_tsf is
      select 'x'
        from v_br_tsf_util_code
       where tsf_no = L_l10n_exists_rec.doc_id;

   cursor C_get_util_code_exists_mrt is
      select 'x'
        from v_br_mrt_util_code
       where mrt_no = L_l10n_exists_rec.doc_id;

BEGIN

   L_l10n_exists_rec := TREAT(IO_l10n_obj as L10N_EXISTS_REC);
   L_l10n_exists_rec.exists_ind := 'N';

   if L_l10n_exists_rec.doc_type = 'PO' then

      SQL_LIB.SET_MARK('OPEN',
                       'C_util_code_exists_po',
                       'V_BR_ORD_UTIL_CODE',
                        NULL);
      open C_util_code_exists_po;

      SQL_LIB.SET_MARK('FETCH',
                       'C_util_code_exists_po',
                       'V_BR_ORD_UTIL_CODE',
                        NULL);
      fetch C_util_code_exists_po into L_exists;

      if C_util_code_exists_po%FOUND then
         L_l10n_exists_rec.exists_ind := 'Y';
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_util_code_exists_po',
                       'V_BR_ORD_UTIL_CODE',
                        NULL);
      close C_util_code_exists_po;
   elsif L_l10n_exists_rec.doc_type = 'TSF' then

      SQL_LIB.SET_MARK('OPEN',
                       'C_util_code_exists_tsf',
                       'V_BR_TSF_UTIL_CODE',
                        NULL);
      open C_util_code_exists_tsf;

      SQL_LIB.SET_MARK('FETCH',
                       'C_util_code_exists_tsf',
                       'V_BR_TSF_UTIL_CODE',
                        NULL);
      fetch C_util_code_exists_tsf into L_exists;
      if C_util_code_exists_tsf%FOUND then
         L_l10n_exists_rec.exists_ind := 'Y';
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_util_code_exists_tsf',
                       'V_BR_TSF_UTIL_CODE',
                        NULL);
      close C_util_code_exists_tsf;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_get_util_code_exists_mrt',
                       'V_BR_MRT_UTIL_CODE',
                        NULL);
      open C_get_util_code_exists_mrt;

      SQL_LIB.SET_MARK('FETCH',
                       'C_get_util_code_exists_mrt',
                       'V_BR_MRT_UTIL_CODE',
                        NULL);
      fetch c_get_util_code_exists_mrt into L_exists;

      if C_get_util_code_exists_mrt%FOUND then
         L_l10n_exists_rec.exists_ind := 'Y';
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_get_util_code_exists_mrt',
                       'V_BR_MRT_UTIL_CODE',
                        NULL);
      close C_get_util_code_exists_mrt;
   end if;

   IO_l10n_obj := L_l10n_exists_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      if C_util_code_exists_po%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_util_code_exists_po',
                          'V_BR_ORD_UTIL_CODE',
                          NULL);
         close C_util_code_exists_po;
      end if;

      if C_util_code_exists_tsf%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_util_code_exists_tsf',
                          'V_BR_TSF_UTIL_CODE',
                          NULL);
         close C_util_code_exists_tsf;
      end if;

      if C_get_util_code_exists_mrt%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_get_util_code_exists_mrt',
                          'V_BR_MRT_UTIL_CODE',
                          NULL);
         close C_get_util_code_exists_mrt;
      end if;
      ---
      return FALSE;
END CHECK_UTIL_CODE_EXISTS;
----------------------------------------------------------------------------------------------------
FUNCTION CREATE_TRAN_UTIL_CODE (O_error_message   IN   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_l10n_obj       IN   OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(60) := 'L10N_BR_PO_SQL.CREATE_TRAN_UTIL_CODE';
   L_util_code                VARCHAR2(30);
   L_util_code_ic             VARCHAR2(30);
   L_util_code_rpr            VARCHAR2(30);
   L_util_code_co             VARCHAR2(30);
   L_util_code_mult_co        VARCHAR2(30);
   L_util_code_mult_ic        VARCHAR2(30);
   L_description              FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_number_value             FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value             FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_string_value_ic          FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_string_value_rpr         FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_string_value_co          FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_string_value_mult_co     FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_string_value_mult_ic     FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value               FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_variable                 FM_SYSTEM_OPTIONS.VARIABLE%TYPE;
   L_variable_ic              FM_SYSTEM_OPTIONS.VARIABLE%TYPE;
   L_variable_rpr             FM_SYSTEM_OPTIONS.VARIABLE%TYPE;
   L_variable_co              FM_SYSTEM_OPTIONS.VARIABLE%TYPE;
   L_variable_mult_co         FM_SYSTEM_OPTIONS.VARIABLE%TYPE;
   L_variable_mult_ic         FM_SYSTEM_OPTIONS.VARIABLE%TYPE;
   L_variable_type            FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE;
   L_tsf_no                   TSFHEAD.TSF_NO%TYPE;
   L_tsf_type                 TSFHEAD.TSF_TYPE%TYPE;
   L_context_type             TSFHEAD.CONTEXT_TYPE%TYPE;
   L_group_id                 ORDHEAD_L10N_EXT.GROUP_ID%TYPE;
   L_ext_ref_no               TSFHEAD.EXT_REF_NO%TYPE;
   L_tsfhd_ext                VARCHAR2(1)  := NULL;
   L_exists                   VARCHAR2(1);
   L_source_loc_type          ORDCUST.SOURCE_LOC_TYPE%TYPE;
   L_source_loc_id            ORDCUST.SOURCE_LOC_ID%TYPE;
   L_source_stock_holding     VARCHAR2(1);
   L_source_entity_id         NUMBER(10); 
   L_fulfill_loc_type         ORDCUST.FULFILL_LOC_TYPE%TYPE;
   L_fulfill_loc_id           ORDCUST.FULFILL_LOC_ID%TYPE;
   L_fulfill_stock_holding    VARCHAR2(1);
   L_fulfill_entity_id        NUMBER(10); 


   cursor C_TSF_TYPE is
      select tsf_type,
             context_type,
             ext_ref_no
        from tsfhead
       where tsf_no = IO_l10n_obj.doc_id;

   cursor C_TSFHD_L10N_EXT_EXISTS is
      select 'x'
        from tsfhead_l10n_ext
       where tsf_no = IO_l10n_obj.doc_id
         and l10n_country_id = IO_l10n_obj.country_id;

   cursor C_CHK_DROP_SHIP_CO is
      select 'Y'
        from ordcust
       where order_no = IO_l10n_obj.doc_id
         and source_loc_type = 'SU'
         and fulfill_loc_type = 'V';         
   
   cursor C_GET_FULFILLORD(P_customer_order SVC_FULFILORD.CUSTOMER_ORDER_NO%TYPE) is
      select source_loc_type,
             source_loc_id,
             fulfill_loc_type,
             fulfill_loc_id
             -- consumer_direct is set to 'Y' for all CO transfers from warehouse
        from svc_fulfilord ffo
       where ffo.customer_order_no = P_customer_order
       order by 1 desc;

   cursor C_GET_WH_INFO(P_wh WH.WH%TYPE) is
      select nvl(stockholding_ind,'N') stockholding_ind,
             tsf_entity_id
        from wh
       where wh = P_wh;

   cursor C_GET_STORE_INFO(P_store STORE.STORE%TYPE) is
      select nvl(stockholding_ind,'N') stockholding_ind,
             tsf_entity_id
        from store
       where store = P_store;
BEGIN
   logger.log('L10N_BR_PO_SQL.CREATE_TRAN_UTIL_CODE()-> IO_l10n_obj.doc_type: ' || IO_l10n_obj.doc_type || ' IO_l10n_obj.doc_id:' || IO_l10n_obj.doc_id,L_program);
   if IO_l10n_obj.doc_type = 'PO' then
      L_variable_type := 'CHAR';
      open C_CHK_DROP_SHIP_CO;
      fetch C_CHK_DROP_SHIP_CO into L_exists;
      if C_CHK_DROP_SHIP_CO%FOUND then
         L_variable      := 'DEFAULT_DRSP_CO_PO_TYPE';
      else
         L_variable      := 'DEFAULT_PO_TYPE';
      end if;
      close C_CHK_DROP_SHIP_CO;
      L_group_id      := 25;
      -- Call the FM_SYSTEM_OPTIONS_SQL to get default UTIL code.
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_description,
                                            L_number_value,
                                            L_string_value,
                                            L_date_value,
                                            L_variable_type,
                                            L_variable)  = FALSE then
         return FALSE;
      end if;
   elsif IO_l10n_obj.doc_type = 'TSF' then
      L_variable_type      := 'CHAR';
      L_group_id           := 24;
      L_variable           := 'DEFAULT_OUTBOUND_TSF_UTIL_ID';
      L_variable_ic        := 'DEFAULT_OUTBOUND_IC_UTIL_ID';
      L_variable_rpr       := 'DEFAULT_OUTBOUND_REP_UTIL_ID';
      L_variable_co        := 'DEFAULT_OUTBOUND_CUST_UTIL_ID';
      L_variable_mult_co   := 'DEFT_MULTISITE_CO_OUT_TSF_UTIL';
      L_variable_mult_ic   := 'DEFT_MULTISITE_CO_OUT_IC_UTIL';
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_description,
                                            L_number_value,
                                            L_string_value,
                                            L_date_value,
                                            L_variable_type,
                                            L_variable)  = FALSE then
         return FALSE;
      end if;
      -- Call for transfers other than inter company
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_description,
                                            L_number_value,
                                            L_string_value_ic,
                                            L_date_value,
                                            L_variable_type,
                                            L_variable_ic)  = FALSE then
         return FALSE;
      end if;

      -- Call for transfers for context type = REPAIR
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_description,
                                            L_number_value,
                                            L_string_value_rpr,
                                            L_date_value,
                                            L_variable_type,
                                            L_variable_rpr)  = FALSE then
         return FALSE;
      end if;

      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_description,
                                            L_number_value,
                                            L_string_value_co,
                                            L_date_value,
                                            L_variable_type,
                                            L_variable_co)  = FALSE then
         return FALSE;
      end if;
      
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_description,
                                            L_number_value,
                                            L_string_value_mult_co,
                                            L_date_value,
                                            L_variable_type,
                                            L_variable_mult_co)  = FALSE then
         return FALSE;
      end if;
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_description,
                                            L_number_value,
                                            L_string_value_mult_ic,
                                            L_date_value,
                                            L_variable_type,
                                            L_variable_mult_ic)  = FALSE then
         return FALSE;
      end if;
   end if;

   L_util_code          := L_string_value;
   L_util_code_ic       := L_string_value_ic;
   L_util_code_rpr      := L_string_value_rpr;
   L_util_code_co       := L_string_value_co;
   L_util_code_mult_ic  := L_string_value_mult_ic;
   L_util_code_mult_co  := L_string_value_mult_co;

   if IO_l10n_obj.doc_id is not NULL then
      if IO_l10n_obj.doc_type = 'PO' then
         insert into ordhead_l10n_ext(order_no,
                                      l10n_country_id,
                                      group_id,
                                      varchar2_1)
                               values(IO_l10n_obj.doc_id,
                                      IO_l10n_obj.country_id,
                                      L_group_id,
                                      L_util_code);
      elsif IO_l10n_obj.doc_type = 'TSF' then
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_TSFHD_L10N_EXT_EXISTS', 'TSFHEAD_L10N_EXT', 'TSF_NO: '||TO_CHAR(IO_l10n_obj.doc_id));
         open C_TSFHD_L10N_EXT_EXISTS;
         SQL_LIB.SET_MARK('FETCH', 'C_TSFHD_L10N_EXT_EXISTS', 'TSFHEAD_L10N_EXT', 'TSF_NO: '||TO_CHAR(IO_l10n_obj.doc_id));
         fetch C_TSFHD_L10N_EXT_EXISTS into L_tsfhd_ext;
         SQL_LIB.SET_MARK('CLOSE', 'C_TSFHD_L10N_EXT_EXISTS', 'TSFHEAD_L10N_EXT', 'TSF_NO: '||TO_CHAR(IO_l10n_obj.doc_id));
         close C_TSFHD_L10N_EXT_EXISTS;
         ---
         if L_tsfhd_ext is NULL then
            ---
            SQL_LIB.SET_MARK('OPEN', 'C_TSF_TYPE', 'TSFHEAD', NULL);
            open C_TSF_TYPE;
            SQL_LIB.SET_MARK('FETCH', 'C_TSF_TYPE', 'TSFHEAD', NULL);
            fetch C_TSF_TYPE into L_tsf_type, L_context_type, L_ext_ref_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_TSF_TYPE', 'TSFHEAD', NULL);
            close C_TSF_TYPE;
            ---
            logger.log('L10N_BR_PO_SQL.CREATE_TRAN_UTIL_CODE()-> L_tsf_type: ' || L_tsf_type || ' L_ext_ref_no:' || L_ext_ref_no,L_program);
            ---
            if L_tsf_type is NOT NULL and L_context_type = 'REPAIR' then
               L_util_code := L_util_code_rpr;
            elsif L_tsf_type = 'CO' then 
               if L_ext_ref_no is NOT NULL then
                  --CO transfer initiated by external COM system via web service.
                  -- Get for Customer Order info.
                  open C_GET_FULFILLORD(L_ext_ref_no);
                  fetch C_GET_FULFILLORD into L_source_loc_type, L_source_loc_id, L_fulfill_loc_type, L_fulfill_loc_id;
                  if C_GET_FULFILLORD%FOUND then
                     -- Get locations (source and fulfill) info.
                     if L_source_loc_type = 'WH' and L_source_loc_id is not null then
                        open C_GET_WH_INFO(L_source_loc_id);
                        fetch C_GET_WH_INFO into L_source_stock_holding, L_source_entity_id;
                        close C_GET_WH_INFO;
                     elsif L_source_loc_type in ('S','ST','V') and L_source_loc_id is not null then
                        open C_GET_STORE_INFO(L_source_loc_id);
                        fetch C_GET_STORE_INFO into L_source_stock_holding, L_source_entity_id;
                        close C_GET_STORE_INFO;
                     end if;
                     if L_fulfill_loc_type = 'WH' and L_fulfill_loc_id is not null then
                        open C_GET_WH_INFO(L_fulfill_loc_id);
                        fetch C_GET_WH_INFO into L_fulfill_stock_holding, L_fulfill_entity_id;
                        close C_GET_WH_INFO;
                     elsif L_fulfill_loc_type in ('S','ST','V') and L_fulfill_loc_id is not null then
                        open C_GET_STORE_INFO(L_fulfill_loc_id);
                        fetch C_GET_STORE_INFO into L_fulfill_stock_holding, L_fulfill_entity_id;
                        close C_GET_STORE_INFO;
                     end if;

                     -- Define Utilization Id.
                     if L_source_loc_type in ('WH','S','ST') and 
                        L_fulfill_loc_type in ('WH','S','ST') and 
                        L_fulfill_stock_holding = 'Y' then
                        --DEFT_MULTISITE_CO_OUT_TSF_UTIL
                        L_util_code := L_util_code_mult_co;
                     elsif (L_source_loc_type = 'WH' and 
                            L_fulfill_loc_type = 'V' and 
                            L_fulfill_stock_holding = 'N') or
                           (L_source_loc_type is null and
                            L_fulfill_loc_type in ('S','ST') and 
                            L_fulfill_stock_holding = 'Y') then
                        --DEFAULT_OUTBOUND_CUST_UTIL_ID
                        L_util_code := L_util_code_co;
                     else
                        -- Default Utilization Id
                        L_util_code := L_util_code;
                     end if;
                  end if;
                  close C_GET_FULFILLORD;
               end if;
               
            elsif L_tsf_type = 'IC' then 
               L_util_code := L_util_code_ic;
            else
               -- Default Utilization Id
               L_util_code := L_util_code;
            end if;

            insert into tsfhead_l10n_ext(tsf_no,
                                         l10n_country_id,
                                         group_id,
                                         varchar2_1)
                                  values(IO_l10n_obj.doc_id,
                                         IO_l10n_obj.country_id,
                                         L_group_id,
                                         L_util_code);
         end if;

      else
         insert into mrt_l10n_ext(mrt_no,
                                  l10n_country_id,
                                  group_id,
                                  varchar2_1)
                           values(IO_l10n_obj.doc_id,
                                  IO_l10n_obj.country_id,
                                  L_group_id,
                                  L_util_code);
      end if;
   else --if IO_l10n_obj.doc_id is not NULL
      if IO_l10n_obj.doc_type = 'PO' then
         insert into ordhead_l10n_ext(order_no,
                                      l10n_country_id,
                                      group_id,
                                      varchar2_1)
                               select d.doc_id,
                                      d.country_id,
                                      '25',
                                      L_util_code
                                 from l10n_doc_details_gtt d
                                where d.country_id = IO_l10n_obj.country_id;
      elsif
         IO_l10n_obj.doc_type = 'TSF' then
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_TSF_TYPE', 'TSFHEAD', NULL);
         open C_TSF_TYPE;
         SQL_LIB.SET_MARK('FETCH', 'C_TSF_TYPE', 'TSFHEAD', NULL);
         fetch C_TSF_TYPE into L_tsf_type, L_context_type, L_ext_ref_no;
         SQL_LIB.SET_MARK('CLOSE', 'C_TSF_TYPE', 'TSFHEAD', NULL);
         close C_TSF_TYPE;
         ---
         if L_tsf_type is NOT NULL and L_context_type is NOT NULL then
            if L_context_type = 'REPAIR' then
               L_util_code := L_util_code_rpr;
            end if;
         end if;
         ---
         insert into tsfhead_l10n_ext(tsf_no,
                                      l10n_country_id,
                                      group_id,
                                      varchar2_1)
                               select DISTINCT d.doc_id,
                                      d.country_id,
                                      '24',
                                      decode(d.tsf_type, 'IC', L_util_code_ic, L_util_code)
                                 from l10n_doc_details_gtt d
                                where d.country_id = IO_l10n_obj.country_id
                                  and d.doc_id NOT in (select tle.tsf_no
                                                         from tsfhead_l10n_ext tle
                                                        where d.doc_id = tle.tsf_no
                                                          and d.country_id = tle.l10n_country_id
                                                          and tle.group_id = '24');
      end if;
   end if; --if IO_l10n_obj.doc_id is not NULL

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if c_tsf_type%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_TSF_TYPE',
                          'TSFHEAD',
                          NULL);
         close C_TSF_TYPE;
      end if;
      ---
      return FALSE;
END CREATE_TRAN_UTIL_CODE;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_IF_BRAZIL_LOCALIZED (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_l10n_obj     IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'L10N_BR_PO_SQL.CHECK_IF_BRAZIL_LOCALIZED';
   L_l10n_exists_rec    L10N_EXISTS_REC;

BEGIN

   L_l10n_exists_rec            := TREAT(IO_l10n_obj as L10N_EXISTS_REC);
   L_l10n_exists_rec.exists_ind := 'Y';
   IO_l10n_obj                  := L_l10n_exists_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_IF_BRAZIL_LOCALIZED;
---------------------------------------------------------------------------------------------------------
FUNCTION COPY_TRAN_UTIL_CODE (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'L10N_BR_PO_SQL.COPY_TRAN_UTIL_CODE';

BEGIN

   if IO_l10n_obj.doc_type = 'TSF' then
      insert into tsfhead_l10n_ext(tsf_no,
                                   l10n_country_id,
                                   group_id,
                                   varchar2_1)
                            select d.doc_id,
                                   d.country_id,
                                   '24',
                                   m.varchar2_1
                              from l10n_doc_details_gtt d,
                                   tsfhead t,
                                   mrt_l10n_ext m
                             where d.doc_id = t.tsf_no
                               and d.doc_type = 'TSF'
                               and d.country_id = IO_l10n_obj.country_id
                               and t.mrt_no = m.mrt_no;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END COPY_TRAN_UTIL_CODE;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TRAN_UTIL_CODE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_l10n_obj       IN OUT   L10N_OBJ)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(60) := 'L10N_BR_PO_SQL.UPDATE_TRAN_UTIL_CODE';
   L_util_code                VARCHAR2(30);
   L_description              FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_number_value             FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value             FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value               FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_variable                 FM_SYSTEM_OPTIONS.VARIABLE%TYPE;
   L_variable_type            FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE;
   L_group_id                 ORDHEAD_L10N_EXT.GROUP_ID%TYPE;
   L_system_options_row       SYSTEM_OPTIONS%ROWTYPE;
   L_from_loc                 TSFHEAD.FROM_LOC%TYPE;
   L_from_loc_type            TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_entity_name              TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   L_from_loc_tsf_entity      TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_to_loc_tsf_entity        TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_from_sob_desc            FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE;
   L_to_loc_sob               TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE;
   L_from_loc_sob             TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE;
   L_table                    VARCHAR2(20) := 'TSFHEAD_L10N_EXT';
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);

   cursor C_TSF_INFO is
      select from_loc,
             from_loc_type
        from tsfhead
       where tsf_no = IO_l10n_obj.doc_id;

   cursor C_GET_TSF_ENTITY_ID_TO_LOC is
      select w.tsf_entity_id
        from wh w,
             tsfitem_inv_flow t
       where t.to_loc = w.wh
         and t.to_loc_type = 'W'
         and t.tsf_no = IO_l10n_obj.doc_id
         and rownum = 1;

  cursor C_GET_SOB_TO_LOC is
      select mv.set_of_books_id
        from mv_loc_sob mv,
             tsfitem_inv_flow t
       where t.to_loc = mv.location
         and t.to_loc_type = mv.location_type
         and t.to_loc_type = 'W'
         and t.tsf_no = IO_l10n_obj.doc_id;

   cursor C_LOCK_TSFHEAD_L10N_EXT is
      select 'x'
        from tsfhead_l10n_ext
       where tsf_no = IO_l10n_obj.doc_id
         for update nowait;

BEGIN
   logger.log('L10N_BR_PO_SQL.UPDATE_TRAN_UTIL_CODE',L_program);
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   L_variable_type := 'CHAR';
   L_group_id      := 24;
   L_variable      := 'DEFAULT_OUTBOUND_TSF_UTIL_ID';

   -- Get the default utilization code for outbound transfers
   if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                         L_description,
                                         L_number_value,
                                         L_string_value,
                                         L_date_value,
                                         L_variable_type,
                                         L_variable)  = FALSE then
      return FALSE;
   end if;

   L_util_code     := L_string_value;

   open C_TSF_INFO;
   fetch C_TSF_INFO into L_from_loc,
                         L_from_loc_type;
   close C_TSF_INFO;

   -- EG transfers should always be intracompany transfers only.  An error should be returned
   -- if the transfer entities or set of books of the From and To locations are different.
   -- For this scenario, the transfer is always 'EG', the From Location is always a store and
   -- the to Location is always a warehouse.

   if L_system_options_row.intercompany_transfer_basis = 'T' then
      -- Get the transfer entity of the From Location (Store)
      if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                        L_from_loc_tsf_entity,
                                        L_entity_name,
                                        L_from_loc,
                                        L_from_loc_type) = FALSE then
         return FALSE;
      end if;

      -- Get the transfer entity of the To Location (Warehouse)
      open C_GET_TSF_ENTITY_ID_TO_LOC;
      fetch C_GET_TSF_ENTITY_ID_TO_LOC into L_to_loc_tsf_entity;
      close C_GET_TSF_ENTITY_ID_TO_LOC;

      if L_from_loc_tsf_entity != L_to_loc_tsf_entity then
         O_error_message := SQL_LIB.CREATE_MSG('EG_TSF_SAME_ENTITY');
         return FALSE;
      end if;
   else -- intercompany_transfer_basis = 'B'
      -- Get the set of books of the From Location (Store)
      if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                      L_from_sob_desc,
                                      L_from_loc_sob,
                                      L_from_loc,
                                      L_from_loc_type) = FALSE then
         return FALSE;
      end if;

      -- Get the set of books of the To Location (Warehouse)
      open C_GET_SOB_TO_LOC;
      fetch C_GET_SOB_TO_LOC into L_to_loc_sob;
      close C_GET_SOB_TO_LOC;

      if L_from_loc_sob != L_to_loc_sob then
         O_error_message := SQL_LIB.CREATE_MSG('EG_TSF_SAME_SOB');
         return FALSE;
      end if;
   end if;

   open C_LOCK_TSFHEAD_L10N_EXT;
   close C_LOCK_TSFHEAD_L10N_EXT;

   update tsfhead_l10n_ext
      set varchar2_1 = L_util_code
    where tsf_no = IO_l10n_obj.doc_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'tsf_no: '|| to_char(IO_l10n_obj.doc_id),
                                            NULL);
      if C_TSF_INFO%ISOPEN then
         close C_TSF_INFO;
      end if;

      if C_GET_TSF_ENTITY_ID_TO_LOC%ISOPEN then
         close C_GET_TSF_ENTITY_ID_TO_LOC;
      end if;

      if C_GET_SOB_TO_LOC%ISOPEN then
         close C_GET_SOB_TO_LOC;
      end if;

      if C_LOCK_TSFHEAD_L10N_EXT%ISOPEN then
         close C_LOCK_TSFHEAD_L10N_EXT;
      end if;
      ---
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      if C_TSF_INFO%ISOPEN then
         close C_TSF_INFO;
      end if;

      if C_GET_TSF_ENTITY_ID_TO_LOC%ISOPEN then
         close C_GET_TSF_ENTITY_ID_TO_LOC;
      end if;

      if C_GET_SOB_TO_LOC%ISOPEN then
         close C_GET_SOB_TO_LOC;
      end if;

      if C_LOCK_TSFHEAD_L10N_EXT%ISOPEN then
         close C_LOCK_TSFHEAD_L10N_EXT;
      end if;
      ---
      return FALSE;
END UPDATE_TRAN_UTIL_CODE;
---------------------------------------------------------------------------------------------

--LFAS Field Validation Function(s)
---------------------------------------------------------------------------------------------
FUNCTION GET_UTILTSF_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_util_desc       IN OUT   FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                          I_util_id         IN       FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
RETURN BOOLEAN AS

   L_program     VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_UTILTSF_DESC';
   L_util_desc   FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE;

   cursor C_UTILTSF_DESC is
      select utilization_desc description
        from fm_fiscal_utilization
       where utilization_id = I_util_id
         and requisition_type in ('TSF','IC','REP');

BEGIN

   open C_UTILTSF_DESC;
   fetch C_UTILTSF_DESC into L_util_desc;
   close C_UTILTSF_DESC;

   if L_util_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_UTIL_CODE', I_util_id);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_util_desc,
                                O_util_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      if C_UTILTSF_DESC%ISOPEN then
         close C_UTILTSF_DESC;
      end if;
      ---
      return FALSE;
END GET_UTILTSF_DESC;
-------------------------------------------------------------------------------------------
FUNCTION GET_UTILPO_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_util_desc       IN OUT   FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                         I_util_id         IN       FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
RETURN BOOLEAN AS

   L_program     VARCHAR2(100) := 'L10N_BR_FND_SQL.GET_UTILPO_DESC';
   L_util_desc   FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE;

   cursor C_UTILPO_DESC is
      select utilization_desc description
        from fm_fiscal_utilization
       where utilization_id = I_util_id
         and requisition_type in ('PO', 'COSHIP');

BEGIN

   open C_UTILPO_DESC;
   fetch C_UTILPO_DESC into L_util_desc;
   close C_UTILPO_DESC;

   if L_util_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_UTIL_CODE', I_util_id);
      return FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(L_util_desc,
                                O_util_desc,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if C_UTILPO_DESC%ISOPEN then
         close C_UTILPO_DESC;
      end if;
      ---
      return FALSE;
END GET_UTILPO_DESC;
---------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ORDCUST_L10N_EXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_l10n_obj        IN       L10N_OBJ)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64)                   := 'L10N_BR_PO_SQL.CREATE_ORDCUST_L10N_EXT';
   L_country_id             TSFHEAD_L10N_EXT.L10N_COUNTRY_ID%TYPE;
   L_group_id               TSFHEAD_L10N_EXT.GROUP_ID%TYPE;
   L_l10n_ordcust_ext_rec   L10N_ORDCUST_EXT_REC;
   L_svc_brfulfilord_rec    SVC_BRFULFILORD%ROWTYPE;

   cursor C_GET_COUNTRY_TSF is
      select l10n_country_id
        from tsfhead_l10n_ext
       where tsf_no = L_l10n_ordcust_ext_rec.doc_id;

   cursor C_GET_COUNTRY_ORD is
      select l10n_country_id
        from ordhead_l10n_ext
       where order_no = L_l10n_ordcust_ext_rec.doc_id;       

   cursor C_GET_BR_ATTR is
      select cust_neighborhood,
             cust_cpf,
             presence_ind,
             taxpayer_type,
             fiscal_address_information,
             individual_taxpayer_id,
             corporate_taxpayer_id,
             free_zone_manaus_inscrptn_id,
             city_inscription_id,
             state_inscription_id,
             contributor_type,
             tax_exception_type,
             iss_contributor,
             ipi_contributor,
             icms_contributor,
             pis_contributor,
             cofins_contributor,
             income_range_eligible,
             distributor_manufacturer,
             customer_cnae,
             ie_exempt
        from svc_brfulfilord
       where fulfilord_id = L_l10n_ordcust_ext_rec.reference_id;

   cursor C_GET_BR_PMT_DTLS is
      select brfulfilord_pmt_id,
             cust_payment_method,
             cust_payment_amount
        from svc_brfulfilord_pmt
       where fulfilord_id = L_l10n_ordcust_ext_rec.reference_id
       order by create_datetime;
           
BEGIN

   L_l10n_ordcust_ext_rec := TREAT(I_l10n_obj as L10N_ORDCUST_EXT_REC);

   -- retrieve the country and group ID values
   if L_l10n_ordcust_ext_rec.doc_type = 'TSF' then
      open c_get_country_tsf;
      fetch c_get_country_tsf into L_country_id;
      close c_get_country_tsf;
   elsif L_l10n_ordcust_ext_rec.doc_type = 'PO' then
      open c_get_country_ord;
      fetch c_get_country_ord into L_country_id;
      close c_get_country_ord;
   end if;
   
   if L_country_id is NULL then
      L_country_id :='BR';
   end if;
   -- retrieve the Brazil attributes
   open C_GET_BR_ATTR;
   fetch C_GET_BR_ATTR into L_svc_brfulfilord_rec.cust_neighborhood,
                            L_svc_brfulfilord_rec.cust_cpf,
                            L_svc_brfulfilord_rec.presence_ind,
                            L_svc_brfulfilord_rec.taxpayer_type,
                            L_svc_brfulfilord_rec.fiscal_address_information,
                            L_svc_brfulfilord_rec.individual_taxpayer_id,
                            L_svc_brfulfilord_rec.corporate_taxpayer_id,
                            L_svc_brfulfilord_rec.free_zone_manaus_inscrptn_id,
                            L_svc_brfulfilord_rec.city_inscription_id,
                            L_svc_brfulfilord_rec.state_inscription_id,
                            L_svc_brfulfilord_rec.contributor_type,
                            L_svc_brfulfilord_rec.tax_exception_type,
                            L_svc_brfulfilord_rec.iss_contributor,
                            L_svc_brfulfilord_rec.ipi_contributor,
                            L_svc_brfulfilord_rec.icms_contributor,
                            L_svc_brfulfilord_rec.pis_contributor,
                            L_svc_brfulfilord_rec.cofins_contributor,
                            L_svc_brfulfilord_rec.income_range_eligible,
                            L_svc_brfulfilord_rec.distributor_manufacturer,
                            L_svc_brfulfilord_rec.customer_cnae,
                            L_svc_brfulfilord_rec.ie_exempt;
   close C_GET_BR_ATTR;

   L_group_id := 34;
   -- insert the record into the ORDCUST_L10N_EXT table
   insert into ordcust_l10n_ext(ordcust_no,
                                l10n_country_id,
                                group_id,
                                varchar2_1,
                                varchar2_2,
                                varchar2_3,
                                varchar2_4,
                                varchar2_5,
                                varchar2_6,
                                varchar2_7,
                                varchar2_8,
                                varchar2_9,
                                varchar2_10,
                                number_11)
                         values(L_l10n_ordcust_ext_rec.ordcust_no,
                                L_country_id,
                                L_group_id,
                                L_svc_brfulfilord_rec.cust_neighborhood,
                                L_svc_brfulfilord_rec.cust_cpf,                                
                                L_svc_brfulfilord_rec.taxpayer_type,
                                L_svc_brfulfilord_rec.corporate_taxpayer_id,
                                L_svc_brfulfilord_rec.individual_taxpayer_id,
                                L_svc_brfulfilord_rec.fiscal_address_information,
                                L_svc_brfulfilord_rec.free_zone_manaus_inscrptn_id,                                
                                L_svc_brfulfilord_rec.customer_cnae,
                                L_svc_brfulfilord_rec.city_inscription_id,
                                L_svc_brfulfilord_rec.state_inscription_id,
                                L_svc_brfulfilord_rec.presence_ind);
                                
   L_group_id := 38;
   -- insert the record into the ORDCUST_L10N_EXT table
   insert into ordcust_l10n_ext(ordcust_no,
                                l10n_country_id,
                                group_id,
                                varchar2_1,
                                varchar2_2,
                                varchar2_3,
                                varchar2_4,
                                varchar2_5,
                                varchar2_6,
                                varchar2_7,
                                varchar2_8,
                                varchar2_9,
                                varchar2_10)
                         values(L_l10n_ordcust_ext_rec.ordcust_no,
                                L_country_id,
                                L_group_id,
                                L_svc_brfulfilord_rec.contributor_type,
                                L_svc_brfulfilord_rec.tax_exception_type,
                                L_svc_brfulfilord_rec.income_range_eligible,
                                L_svc_brfulfilord_rec.distributor_manufacturer,
                                L_svc_brfulfilord_rec.iss_contributor,
                                L_svc_brfulfilord_rec.ipi_contributor,
                                L_svc_brfulfilord_rec.icms_contributor,
                                L_svc_brfulfilord_rec.pis_contributor,
                                L_svc_brfulfilord_rec.cofins_contributor,
                                L_svc_brfulfilord_rec.ie_exempt);
                                
   for r_get_br_pmt_dtls_rec in C_GET_BR_PMT_DTLS
   loop       
      insert into ordcust_l10n_payment(ordcust_seq_no,
                                       ordcust_payment_seq_no,
                                       tsf_no,
                                       payment_method,
                                       payment_amount)
                                values(L_l10n_ordcust_ext_rec.ordcust_no,
                                       r_get_br_pmt_dtls_rec.brfulfilord_pmt_id,
                                       NVL(L_l10n_ordcust_ext_rec.doc_id,L_l10n_ordcust_ext_rec.ordcust_no),
                                       r_get_br_pmt_dtls_rec.cust_payment_method,
                                       r_get_br_pmt_dtls_rec.cust_payment_amount);
   end loop;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      if C_GET_COUNTRY_TSF%ISOPEN then
         close C_GET_COUNTRY_TSF;
      end if;

      if C_GET_COUNTRY_ORD%ISOPEN then
         close C_GET_COUNTRY_ORD;
      end if;

      if C_GET_BR_ATTR%ISOPEN then
         close C_GET_BR_ATTR;
      end if;
      ---
      return FALSE;
END CREATE_ORDCUST_L10N_EXT;
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDCUST_EXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_l10n_obj        IN       L10N_OBJ)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'L10N_BR_PO_SQL.UPDATE_ORDCUST_EXT';
   L_table                  VARCHAR2(20) := 'ORDCUST_L10N_EXT';
   L_ordcust_no             ORDCUST_L10N_EXT.ORDCUST_NO%TYPE;
   L_cust_neighborhood      SVC_BRFULFILORD.CUST_NEIGHBORHOOD%TYPE;
   L_cust_cpf               SVC_BRFULFILORD.CUST_CPF%TYPE;
   L_l10n_ordcust_ext_rec   L10N_ORDCUST_EXT_REC;
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDCUST_EXT is
      select 'x'
        from ordcust_l10n_ext
       where ordcust_no = L_ordcust_no
         for update nowait;

   cursor C_GET_BR_ATTR is
      select cust_neighborhood,
             cust_cpf
        from svc_brfulfilord
       where fulfilord_id = L_l10n_ordcust_ext_rec.reference_id;

BEGIN

   L_l10n_ordcust_ext_rec := TREAT(I_l10n_obj as L10N_ORDCUST_EXT_REC);
   L_ordcust_no := L_l10n_ordcust_ext_rec.ordcust_no;

   -- retrieve the values that will be used to update the ORDCUST_L10N_EXT table
   open C_GET_BR_ATTR;
   fetch C_GET_BR_ATTR into L_cust_neighborhood,
                            L_cust_cpf;
   close C_GET_BR_ATTR;

   -- lock the ORDCUST_L10N_EXT table
   open C_LOCK_ORDCUST_EXT;
   close C_LOCK_ORDCUST_EXT;

   update ordcust_l10n_ext
      set varchar2_1 = L_cust_neighborhood,
          varchar2_2 = L_cust_cpf
    where ordcust_no = L_ordcust_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'ordcust_no: ' || TO_CHAR(L_ordcust_no),
                                            NULL);
      if C_LOCK_ORDCUST_EXT%ISOPEN then
         close C_LOCK_ORDCUST_EXT;
      end if;

      if C_GET_BR_ATTR%ISOPEN then
         close C_GET_BR_ATTR;
      end if;
      ---
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_LOCK_ORDCUST_EXT%ISOPEN then
         close C_LOCK_ORDCUST_EXT;
      end if;

      if C_GET_BR_ATTR%ISOPEN then
         close C_GET_BR_ATTR;
      end if;
      ---
      return FALSE;
END UPDATE_ORDCUST_EXT;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_SVC_TABLES_FULFILORD_L10N(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       IO_l10n_obj       IN OUT   L10N_OBJ)
RETURN BOOLEAN IS
   L_table_rec   TABLE_REC :=NULL;
   L_table_tbl   TABLE_TBL :=NULL;
   L_program     VARCHAR2(64) := 'L10N_BR_PO_SQL.GET_SVC_TABLES_FULFILORD_L10N';
BEGIN
   if IO_l10n_obj.country_id is NOT NULL then
      L_table_rec := new TABLE_REC('SVC_BRFULFILORD');
      L_table_tbl := new TABLE_TBL();
      L_table_tbl.extend();
      L_table_tbl(L_table_tbl.count) :=L_table_rec;
      L_table_rec := new TABLE_REC('SVC_BRFULFILORD_PMT');
      L_table_tbl.extend();
      L_table_tbl(L_table_tbl.count) :=L_table_rec;
      IO_l10n_obj.table_names := L_table_tbl;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return FALSE;
END GET_SVC_TABLES_FULFILORD_L10N;
---------------------------------------------------------------------------------------------------------
END L10N_BR_PO_SQL;
/
