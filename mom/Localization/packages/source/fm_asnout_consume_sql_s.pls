CREATE OR REPLACE PACKAGE FM_ASNOUT_CONSUME_SQL IS
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind       IN OUT   BOOLEAN,
                 I_seq_no          IN       FM_STG_ASNOUT_DESC.SEQ_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_no       IN       NUMBER,
                              I_num_threads     IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END FM_ASNOUT_CONSUME_SQL;
/