CREATE OR REPLACE PACKAGE FM_EDI_VALIDATION is

-- Author  : CLAUDIO.SAWATZKY
-- Created : 20/06/2007 16:51:39
-- Purpose : EDI document integration
-------------------------------------------------------------------------------
-- CHANGE SEQ  - 001 - Enabler Latin America
-- CHANGE DATE - 28-MAY-2008
-- CHANGE USER - BCA
-- PROJECT / PT / TRD - 10481 - Oracle Retail Localization
-- DESCRIPTION - Alter error messages
-------------------------------------------------------------------------------    
   
-- Constants
   C_EDI_VAL_PROCESS_ID CONSTANT INTEGER := 3;
   C_SUCCESS            CONSTANT INTEGER := 1;
   C_ERROR              CONSTANT INTEGER := 0;
  
-- Public function and procedure declarations
--------------------------------------------------------------------------------
FUNCTION PROCESS_VALIDATION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_status        IN OUT INTEGER,
                            O_nf_type_ind   IN OUT VARCHAR2,
                            I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_HEADER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_status        IN OUT INTEGER,
                         I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DETAILS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_status         IN OUT INTEGER,
                          I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                          I_freight_nf_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------   
FUNCTION VALIDATE_PAYMENTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_status        IN OUT INTEGER,
                           I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------   
FUNCTION VALIDATE_COMPLEMENTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_status         IN OUT INTEGER,
                              I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                              I_freight_nf_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TAX_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_status        IN OUT INTEGER,
                           I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------   
FUNCTION VALIDATE_TAX_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status         IN OUT INTEGER,
                             I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                             I_freight_nf_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
END FM_EDI_VALIDATION;
/
 
