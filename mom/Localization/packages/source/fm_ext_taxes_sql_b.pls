CREATE OR REPLACE PACKAGE BODY FM_EXT_TAXES_SQL is
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_RESULTS(O_line_tax             IN OUT    "RIB_TaxDetRBO_TBL",
                         O_error_message        IN OUT VARCHAR2,
                         O_status               IN OUT INTEGER,
                         I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                         I_quantity             IN     FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                         I_cost                 IN     FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                         I_form_ind             IN     VARCHAR2 ,
                         I_compensation_ind     IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
FUNCTION LOAD_NF_OBJECT(O_error_message        IN OUT VARCHAR2,
                        O_status               IN OUT INTEGER,
                        I_tax_service_id       IN     FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE,
                        I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                        I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                        I_quantity             IN     FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                        I_cost                 IN     FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                        O_businessObject       IN OUT "RIB_FiscDocColRBM_REC")
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION LOAD_HIST_OBJECT(O_error_message        IN OUT VARCHAR2,
                          O_status               IN OUT INTEGER,
                          I_tax_service_id       IN     FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE,
                          I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          O_businessObject       IN OUT "RIB_FiscDocColRBM_REC")
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION CLEAR_STG_TABLES(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tax_service_id   IN     FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE,
                          I_fiscal_doc_id    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_update_hist_ind  IN     VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
TYPE stg_dtl_tbl        IS TABLE OF FM_FISCAL_STG_DTL%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stg_dtl_tax_tbl    IS TABLE OF FM_FISCAL_STG_TAX_DTL%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE stg_comp_tbl       IS TABLE OF FM_FISCAL_STG_COMPLEMENT%ROWTYPE INDEX BY BINARY_INTEGER;
---
TYPE StgObj IS RECORD (
StgHdrRec    FM_FISCAL_STG_HDR%ROWTYPE,
StgDtlTbl    stg_dtl_tbl,
StgDtlTaxTbl stg_dtl_tax_tbl,
StgCompTbl   stg_comp_tbl);
-----------------------------------------------------------------------------------------------
FUNCTION GET_LEGAL_NAME (I_module       IN    FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                         I_module_id    IN    FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                         I_module_type  IN    FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE)
   return VARCHAR2 is
   ---
   L_legal_name     VARCHAR2(240);
   ---
   cursor C_SUPP_LEGAL_NAME is
      select sup_name_secondary
        from sups
       where supplier = I_module_id;
   ---
   cursor C_WH_LEGAL_NAME is
      select wh_name_secondary
        from wh
       where wh = I_module_id;
   ---
   cursor C_STORE_LEGAL_NAME is
      select store_name_secondary
        from store
       where store = I_module_id;
   ---
   cursor C_PARTNER_LEGAL_NAME is
      select partner_name_secondary
        from partner
       where partner_type = I_module_type
         and partner_id   = I_module_id;
   ---
BEGIN
   ---
   if I_module = 'SUPP' then
      ---
      open C_SUPP_LEGAL_NAME;
      ---
      fetch C_SUPP_LEGAL_NAME into L_legal_name;
      ---
      close C_SUPP_LEGAL_NAME;
      ---
   end if;
   ---
   if I_module = 'LOC' and I_module_type = 'S' then
      ---
      open C_STORE_LEGAL_NAME;
      ---
      fetch C_STORE_LEGAL_NAME into L_legal_name;
      ---
      close C_STORE_LEGAL_NAME;
      ---
   end if;
   ---
   if I_module = 'LOC' and I_module_type = 'W' then
      ---
      open C_WH_LEGAL_NAME;
      ---
      fetch C_WH_LEGAL_NAME into L_legal_name;
      ---
      close C_WH_LEGAL_NAME;
      ---
   end if;
   ---
   if I_module = 'PNTR' then
      ---
      open C_PARTNER_LEGAL_NAME;
      ---
      fetch C_PARTNER_LEGAL_NAME into L_legal_name;
      ---
      close C_PARTNER_LEGAL_NAME;
      ---
   end if;
   ---
   return L_legal_name;
   ---
END GET_LEGAL_NAME;
---------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_REQUEST_DATA(O_error_msg             IN OUT  VARCHAR2,
                          I_tax_service_id        IN      NUMBER,
                          O_businessObject        IN OUT  "RIB_FiscDocColRBM_REC",
                                            I_update_history_ind    IN      L10N_BR_TAX_CALL_STAGE_ROUTING.UPDATE_HISTORY_IND%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(62) := 'FM_EXT_TAXES_SQL.GET_REQUEST_DATA';
   O_status       INTEGER(1);
   ---
   cursor C_GET_INPUT_PARAM is
      select tax_service_id,
             fiscal_doc_id,
             fiscal_doc_line_id,
                   quantity,
                   cost
        from fm_tax_call_stage
       where tax_service_id = I_tax_service_id;

   R_get_input_param  C_GET_INPUT_PARAM%ROWTYPE;

BEGIN
   open C_GET_INPUT_PARAM;
   fetch C_GET_INPUT_PARAM into R_get_input_param;
   close C_GET_INPUT_PARAM;
   if I_update_history_ind  = 'N' then
      if LOAD_NF_OBJECT(O_error_msg,
                                  O_status,
                                    R_get_input_param.tax_service_id,
                                    R_get_input_param.fiscal_doc_id,
                                    R_get_input_param.fiscal_doc_line_id,
                                    R_get_input_param.quantity,
                                    R_get_input_param.cost,
                                    O_businessObject) = FALSE then
         return FALSE;
      end if;
    else
      if LOAD_HIST_OBJECT(O_error_msg,
                                    O_status,
                                      R_get_input_param.tax_service_id,
                                      R_get_input_param.fiscal_doc_id,
                                      O_businessObject) = FALSE then
         return FALSE;
      end if;
    end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END GET_REQUEST_DATA;
---------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_STAGE_TABLE(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                          I_quantity             IN     FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                          I_cost                 IN     FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                          O_tax_service_id       IN OUT NUMBER,
                          I_update_hist_ind      IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   PRAGMA AUTONOMOUS_TRANSACTION;
   L_program          varchar2(64) := 'FM_EXT_TAXES_SQL.LOAD_STAGE_TABLE';
   cursor C_GET_SEQ is
      select l10n_br_tax_service_seq.nextval
         from dual;
BEGIN
   open C_GET_SEQ;
   fetch C_GET_SEQ into O_tax_service_id;
   close C_GET_SEQ;
   insert into fm_tax_call_stage(tax_service_id,
                                 fiscal_doc_id,
                                 fiscal_Doc_line_id,
                                 quantity,
                                 cost)
                         values (O_tax_service_id,
                                 I_fiscal_doc_id,
                                 I_fiscal_Doc_line_id,
                                 I_quantity,
                                 I_cost);
   insert into l10n_br_tax_call_stage_routing
               (tax_service_id,
                client_id,
                update_history_ind)
        values (O_tax_service_id,
                'RFM_TAX',
                I_update_hist_ind);
   commit;
   return TRUE;
EXCEPTION
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_STAGE_TABLE;
---------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_STAGE_INSERTION(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_stgobj            IN     StgObj)
RETURN BOOLEAN IS
   PRAGMA AUTONOMOUS_TRANSACTION;
   L_program        VARCHAR2(64) := 'FM_EXT_TAXES_SQL.LOAD_STAGE_INSERTION';
BEGIN
   ---
     insert into fm_fiscal_stg_hdr values I_stgobj.StgHdrRec;
     ---
     if I_stgobj.StgDtlTbl is not NULL and I_stgobj.StgDtlTbl.COUNT > 0 then
         FORALL i in I_stgobj.StgDtlTbl.FIRST..I_stgobj.StgDtlTbl.LAST
            insert into fm_fiscal_stg_dtl(fiscal_doc_line_id,
                                          fiscal_doc_id,
                                          fiscal_doc_line_id_ref,
                                          fiscal_doc_id_ref,
                                          icms_cst,
                                          pis_cst,
                                          cofins_cst,
                                          item,
                                          requisition_no,
                                          quantity,
                                          unit_item_disc,
                                          unit_header_disc ,
                                          freight_cost,
                                          other_expenses_cost,
                                          insurance_cost,
                                          unit_cost,
                                          recoverable_base,
                                          recoverable_value,
                                          entry_cfop,
                                          nf_cfop,
                                          pack_ind,
                                          tax_service_id)
                                  values (I_stgobj.StgDtlTbl(i).fiscal_doc_line_id ,
                                          I_stgobj.StgDtlTbl(i).fiscal_doc_id  ,
                                          I_stgobj.StgDtlTbl(i).fiscal_doc_line_id_ref,
                                          I_stgobj.StgDtlTbl(i).fiscal_doc_id_ref,
                                          I_stgobj.StgDtlTbl(i).icms_cst,
                                          I_stgobj.StgDtlTbl(i).pis_cst,
                                          I_stgobj.StgDtlTbl(i).cofins_cst,
                                          I_stgobj.StgDtlTbl(i).item  ,
                                          I_stgobj.StgDtlTbl(i).requisition_no,
                                          I_stgobj.StgDtlTbl(i).quantity  ,
                                          I_stgobj.StgDtlTbl(i).unit_item_disc ,
                                          I_stgobj.StgDtlTbl(i).unit_header_disc ,
                                          I_stgobj.StgDtlTbl(i).freight_cost,
                                          I_stgobj.StgDtlTbl(i).other_expenses_cost,
                                          I_stgobj.StgDtlTbl(i).insurance_cost,
                                          I_stgobj.StgDtlTbl(i).unit_cost,
                                          I_stgobj.StgDtlTbl(i).recoverable_base,
                                          I_stgobj.StgDtlTbl(i).recoverable_value,
                                          I_stgobj.StgDtlTbl(i).entry_cfop ,
                                          I_stgobj.StgDtlTbl(i).nf_cfop ,
                                          I_stgobj.StgDtlTbl(i).pack_ind,
                                          I_stgobj.StgDtlTbl(i).tax_service_id);
     end if;
     ---
     if I_stgobj.StgDtlTaxTbl is not NULL and I_stgobj.StgDtlTaxTbl.COUNT > 0 then
         FORALL i in I_stgobj.StgDtlTaxTbl.FIRST..I_stgobj.StgDtlTaxTbl.LAST
            insert into fm_fiscal_stg_tax_dtl(fiscal_doc_line_id ,
                                              fiscal_doc_id  ,
                                              vat_code,
                                              percentage_rate ,
                                              tax_basis,
                                              total_value ,
                                              appr_base_value,
                                              appr_tax_value,
                                              appr_tax_rate,
                                              modified_tax_basis,
                                              appr_modified_base_value,
                                              received_qty,
                                              tax_service_id)
                                       values (I_stgobj.StgDtlTaxTbl(i).fiscal_doc_line_id ,
                                              I_stgobj.StgDtlTaxTbl(i).fiscal_doc_id  ,
                                              I_stgobj.StgDtlTaxTbl(i).vat_code  ,
                                              I_stgobj.StgDtlTaxTbl(i).percentage_rate ,
                                              I_stgobj.StgDtlTaxTbl(i).tax_basis,
                                              I_stgobj.StgDtlTaxTbl(i).total_value ,
                                              I_stgobj.StgDtlTaxTbl(i).appr_base_value,
                                              I_stgobj.StgDtlTaxTbl(i).appr_tax_value,
                                              I_stgobj.StgDtlTaxTbl(i).appr_tax_rate,
                                              I_stgobj.StgDtlTaxTbl(i).modified_tax_basis,
                                              I_stgobj.StgDtlTaxTbl(i).appr_modified_base_value,
                                              I_stgobj.StgDtlTaxTbl(i).received_qty,
                                              I_stgobj.StgDtlTaxTbl(i).tax_service_id);
     end if;
     ---
     if I_stgobj.StgCompTbl is not NULL and I_stgobj.StgCompTbl.COUNT > 0 then
         FORALL i in I_stgobj.StgCompTbl.FIRST..I_stgobj.StgCompTbl.LAST
            insert into fm_fiscal_stg_complement(fiscal_doc_id,
                                                 compl_fiscal_doc_id,
                                                 tax_service_id)
                                         values (I_stgobj.StgCompTbl(i).fiscal_doc_id,
                                                 I_stgobj.StgCompTbl(i).compl_fiscal_doc_id,
                                                 I_stgobj.StgCompTbl(i).tax_service_id);
     end if;
   ---
   commit;
   return TRUE;
EXCEPTION
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_STAGE_INSERTION;
---------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_STAGE_DATA(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tax_service_id       IN     FM_FISCAL_STG_HDR.TAX_SERVICE_ID%TYPE,
                         I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_update_hist_ind      IN     VARCHAR2)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64) := 'FM_EXT_TAXES_SQL.LOAD_STAGE_DATA';
   L_get_payment_date         FM_FISCAL_DOC_PAYMENTS.PAYMENT_DATE%TYPE;
   L_stg_obj                  StgObj;
   ---
   cursor C_GET_DOC_HEADER is
      select fdh.fiscal_doc_id,
             decode(nvl(fdh.schedule_no,-999),-999,'ENT',fs.mode_type) mode_type,
             fdh.requisition_type,
             fdh.issue_date,
             fdh.entry_or_exit_date,
             fdh.freight_cost,
             fdh.freight_type,
             fdh.discount_type,
             fdh.total_discount_value,
             fdh.total_item_value,
             fdh.other_expenses_cost,
             fdh.insurance_cost,
             fdh.net_weight,
             fdh.total_weight,
             fdh.type_id,
             fdh.series_no,
             fdh.fiscal_doc_no,
             fdh.module,
             fdh.key_value_1,
             fdh.key_value_2,
             fdh.location_id,
             fdh.location_type,
             fdh.partner_type,
             fdh.partner_id,
             fdh.entry_cfop,
             fdh.nf_cfop,
             L_get_payment_date,
             fdh.utilization_id,
             I_tax_service_id
        from fm_fiscal_doc_header fdh,
             fm_schedule fs
       where fiscal_doc_id = I_fiscal_doc_id
         and fs.schedule_no(+) = fdh.schedule_no
         and fdh.status != 'I';
   ---
   cursor C_GET_DOC_DETAIL is
      select fiscal_doc_id,
             fiscal_doc_line_id,
             fiscal_doc_line_id_ref,
             fiscal_doc_id_ref,
             icms_cst,
             pis_cst,
             cofins_cst,
             item,
             requisition_no,
             quantity  ,
             unit_item_disc ,
             unit_header_disc ,
             freight_cost,
             other_expenses_cost,
             insurance_cost,
             unit_cost,
             recoverable_base,
             recoverable_value,
             entry_cfop ,
             nf_cfop ,
             pack_ind,
             I_tax_service_id
       from  fm_fiscal_doc_Detail where fiscal_doc_id =I_fiscal_doc_id;
   ---
   cursor C_GET_DOC_TAX_DETAIL is
      select fdd.fiscal_doc_id  ,
             fdtd.fiscal_doc_line_id ,
             fdtd.vat_code,
             fdtd.percentage_rate ,
             fdtd.tax_basis,
             fdtd.total_value ,
             fdtd.appr_base_value,
             fdtd.appr_tax_value,
             fdtd.appr_tax_rate,
             fdtd.modified_tax_basis,
             fdtd.appr_modified_base_value,
             null,
             I_tax_service_id
        from fm_fiscal_doc_tax_detail fdtd,
             fm_fiscal_doc_detail fdd
       where fdtd.fiscal_doc_line_id    = fdd.fiscal_doc_line_id
         and fdd.fiscal_doc_id          = I_fiscal_doc_id
         and I_update_hist_ind          = 'N'
      union
      select fdd.fiscal_doc_id  ,
             fdd.fiscal_doc_line_id ,
             ftdw.tax_code vat_code,
             ftdw.tax_rate percentage_rate ,
             ftdw.tax_basis,
             (ftdw.unit_tax_value * ftdw.rcvd_qty) total_value ,
             null appr_base_value,
             null appr_tax_value,
             null appr_tax_rate,
             ftdw.modified_tax_basis,
             null appr_modified_base_value,
             ftdw.rcvd_qty,
             I_tax_service_id
        from fm_fiscal_doc_tax_detail_wac ftdw,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where ftdw.fiscal_doc_line_id        = fdd.fiscal_doc_line_id
         and fdd.fiscal_doc_id              = fdh.fiscal_doc_id
         and fdh.requisition_type           = 'PO'
         and fdh.fiscal_doc_id              = I_fiscal_doc_id
         and I_update_hist_ind              = 'Y';
   ---
   cursor C_GET_DOC_COMPLEMENT is
      select fdc.fiscal_doc_id,
             fdc.compl_fiscal_doc_id,
             I_tax_service_id
        from fm_fiscal_doc_complement fdc
       where fdc.fiscal_doc_id = I_fiscal_doc_id
          or fdc.compl_fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_PAYMENT_DATE is
      select max(fdp.payment_date)
        from fm_fiscal_doc_payments fdp,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id    = I_fiscal_doc_id
         and fdh.fiscal_doc_id    = fdp.fiscal_doc_id;
BEGIN
   open C_GET_PAYMENT_DATE;
   ---
   fetch C_GET_PAYMENT_DATE into L_get_payment_date;
   ---
   close C_GET_PAYMENT_DATE;
   -- Header Info
   open C_GET_DOC_HEADER;
   fetch C_GET_DOC_HEADER into L_stg_obj.StgHdrRec;
   close C_GET_DOC_HEADER;
   ---
   open C_GET_DOC_DETAIL;
   fetch C_GET_DOC_DETAIL BULK COLLECT into L_stg_obj.StgDtlTbl;
   close C_GET_DOC_DETAIL;
   ---
   open C_GET_DOC_TAX_DETAIL;
   fetch C_GET_DOC_TAX_DETAIL BULK COLLECT into L_stg_obj.StgDtlTaxTbl;
   close C_GET_DOC_TAX_DETAIL;
   ---
   open C_GET_DOC_COMPLEMENT;
   fetch C_GET_DOC_COMPLEMENT BULK COLLECT into L_stg_obj.StgCompTbl;
   close C_GET_DOC_COMPLEMENT;
   ---
    if LOAD_STAGE_INSERTION(O_error_message,
                            L_stg_obj)= FALSE then
        return FALSE;
    end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if C_GET_PAYMENT_DATE%ISOPEN then
         close C_GET_PAYMENT_DATE;
      end if;

      if C_GET_DOC_HEADER%ISOPEN then
         close C_GET_DOC_HEADER;
      end if;

      if C_GET_DOC_DETAIL%ISOPEN then
         close C_GET_DOC_DETAIL;
      end if;

      if C_GET_DOC_TAX_DETAIL%ISOPEN then
         close C_GET_DOC_TAX_DETAIL;
      end if;

      if C_GET_DOC_COMPLEMENT%ISOPEN then
         close C_GET_DOC_COMPLEMENT;
      end if;

      return FALSE;
END LOAD_STAGE_DATA;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_EXT_TAX_VALUES(O_line_tax             IN OUT    "RIB_TaxDetRBO_TBL",
                            O_error_message        IN OUT VARCHAR2,
                            O_status               IN OUT INTEGER,
                            I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            I_quantity             IN     FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                            I_cost                 IN     FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                            I_form_ind             IN     VARCHAR2 DEFAULT 'Y',
                            I_compensation_ind     IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64) := 'FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES';
   L_tax_service_id          FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE := null;
   L_call_type               VARCHAR2(10)   := 'CALC_TAX';
   L_object                  RIB_OBJECT;
   L_businessObject          "RIB_FiscDocColRBM_REC" :=NULL;
   L_status                  VARCHAR2(1);
BEGIN
   O_status := 0;
   if I_fiscal_doc_id is not NULL then
        if LOAD_STAGE_TABLE(O_error_message,
                            I_fiscal_doc_id,
                            I_fiscal_doc_line_id,
                            I_quantity,
                            I_cost,
                            L_tax_service_id)= FALSE then
            return FALSE;
        end if;
        if LOAD_STAGE_DATA(O_error_message,
                           L_tax_service_id,
                           I_fiscal_doc_id,
                           'N')= FALSE then
            return FALSE;
        end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_FISCAL_DOC_ID',NULL,NULL,NULL);
      return FALSE;
   end if;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                       L_status,
                                                       L_tax_service_id,
                                                       L_call_type)= FALSE then    
     if I_form_ind = 'Y' and L_status = 'M' then
       if CLEAR_STG_TABLES(O_error_message,
                           L_tax_service_id,
                           I_fiscal_doc_id,
                           'N') = FALSE then
          return FALSE;
       end if;
       if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                       I_fiscal_doc_id,
                                       L_program,
                                       L_program,
                                       'E',
                                       'VALIDATION_ERROR',
                                       O_error_message,
                                       'Fiscal_doc_id: '||
                                       TO_CHAR(I_fiscal_doc_id)) = FALSE then
           return FALSE;
       end if;
       return TRUE;
     else
       return FALSE;
     end if;
   end if;

   if L_status = 'S' then
     if PROCESS_RESULTS(O_line_tax,
                        O_error_message,
                        O_status,
                        I_fiscal_doc_id,
                        I_fiscal_doc_line_id,
                        I_quantity,
                        I_cost,
                        I_form_ind)= FALSE then
         return FALSE;
     end if;
     ---
     if CLEAR_STG_TABLES(O_error_message,
                         L_tax_service_id,
                         I_fiscal_doc_id,
                         'N') = FALSE then
        return FALSE;
      end if;
   end if;
   ---
   O_status := 1;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_EXT_TAX_VALUES;
------------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_NF_OBJECT(O_error_message        IN OUT VARCHAR2,
                        O_status               IN OUT INTEGER,
                        I_tax_service_id       IN     FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE,
                        I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                        I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                        I_quantity             IN     FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                        I_cost                 IN     FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                                          O_businessObject       IN OUT "RIB_FiscDocColRBM_REC")
RETURN BOOLEAN IS
   ---
   L_program   VARCHAR2(100) := 'FM_EXT_TAXES_SQL.LOAD_NF_OBJECT';
   ---
   L_service_ind           V_BR_ITEM_FISCAL_ATTRIB.SERVICE_IND%TYPE;
      ---
   TYPE vat_code IS RECORD
   (vat_code              VAT_CODES.VAT_CODE%TYPE,
    incl_nic_ind          VAT_CODES.INCL_NIC_IND%TYPE);
   ---
   TYPE vat_code_tab IS TABLE OF vat_code INDEX BY BINARY_INTEGER;
   ---
   L_vat_code              VAT_CODE;
   L_vat_code_tab          VAT_CODE_TAB;
   L_vat_code_ind          NUMBER       := 0;
   L_doc_type              VARCHAR2(10) := 'FT';
   L_supp_issuer           VARCHAR2(1)  := 'Y';
   L_transac_type          VARCHAR2(1)  := 'I';
   L_calc_process_type     VARCHAR2(3)  := 'REC';
   L_no_rec_st             VARCHAR2(10) := '1';
   L_item_attr             VARCHAR2(1)  := 'Y';
   L_serv_rendered_city    VARCHAR2(1)  := 'T';        --borrower
   L_item_util             VARCHAR2(1)  := 'C';        --commercialization
   L_partner               VARCHAR2(4)  := 'PNTR';
   L_error                 VARCHAR2(1)  := 'E';
   L_item_number           NUMBER       := 1;
   L_tax_number            NUMBER       := 1;
   L_source_cont           NUMBER       := 0;
   L_dest_cont             NUMBER       := 0;
   tax_cont                NUMBER       := 0;
   L_source_eco            NUMBER       := 0;
   L_dest_eco              NUMBER       := 0;
   L_tax_engine_err        BOOLEAN;
   cnae_src_found          BOOLEAN      :=  FALSE;
   cnae_dst_found          BOOLEAN      :=  FALSE;
   L_dummy                 VARCHAR2(1);
   L_triang_po_exists      BOOLEAN      := FALSE;
   L_ignore_tax            VARCHAR2(20) := NULL;
   L_cofins                VARCHAR2(6)  := 'COFINS';
   L_pis                   VARCHAR2(6)  := 'PIS';
   L_icms                  VARCHAR2(6)  := 'ICMS';
   L_ipi                   VARCHAR2(6)  := 'IPI';
   L_ii                    VARCHAR2(6)  := 'II';
   L_st                    VARCHAR2(6)  := 'ST';
   L_icmsst                VARCHAR2(6)  := 'ICMSST';
   L_iss                   VARCHAR2(6)  := 'ISS';
   L_inss                  VARCHAR2(6)  := 'INSS';
   L_null                  VARCHAR2(1)  := ' ';
   L_true                  VARCHAR2(1)  := 'S';
   L_addr                  VARCHAR2(1)  := '1';
   L_outbound              VARCHAR2(1)  := 'O';
   L_inbound               VARCHAR2(1)  := 'I';
   L_tax                   VARCHAR2(3)  := 'TAX';
   L_rma                   VARCHAR2(3)  := 'RMA';
   L_rtv                   VARCHAR2(3)  := 'RTV';
   L_rnf                   VARCHAR2(3)  := 'RNF';
   L_deduced_cfop          VARCHAR2(1)  := 'Y';
   L_tax_discrep_type      VARCHAR2(1)  := 'T';
   L_nf_res                VARCHAR2(6)  := 'NF';
   L_cost_discrep_type     VARCHAR2(1)  := 'C';
   L_sys_res               VARCHAR2(6)  := 'SYS';
   L_deduce_icms_cst       VARCHAR2(1)  := 'Y';
   L_deduce_pis_cst        VARCHAR2(1)  := 'Y';
   L_deduce_cofins_cst     VARCHAR2(1)  := 'Y';
   L_loc_st_ind            VARCHAR2(1)  := 'N';
   L_util_st_ind           VARCHAR2(1)  := 'N';

   L_other_doc_id          FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_src_location          FM_FISCAL_DOC_HEADER.MODULE%TYPE := NULL;
   L_dest_location         FM_FISCAL_DOC_HEADER.MODULE%TYPE := NULL;
   L_base                  FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE;
   L_modified_base         FM_FISCAL_DOC_TAX_DETAIL_EXT.MODIFIED_TAX_BASIS%TYPE;
   L_tax_val               FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE;
   L_tax_rate              FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_RATE%TYPE;
   L_po_cost               FM_RESOLUTION.SYSTEM_VALUE%TYPE;
   L_po_cost_appr          FM_RESOLUTION.SYSTEM_VALUE%TYPE;
   L_qty                   FM_RESOLUTION.SYSTEM_VALUE%TYPE;
   L_nf_qty                FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_nf_qty_appr           FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_source                FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE   :=NULL;
   L_dest                  FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE   :=NULL;
   L_source_type           FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE   :=NULL;
   L_dest_type             FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE   :=NULL;
   L_origin_fiscal_code    FM_FISCAL_DOC_HEADER.NF_CFOP%TYPE       :=NULL;
   L_transformed_item      VARCHAR2(1)  := 'N';
   L_exit                  VARCHAR2(6) := 'EXIT';
   L_cif                   VARCHAR2(6) := 'CIF';
   L_fob                   VARCHAR2(6) := 'FOB';
   L_c                     VARCHAR2(6) := 'C';
   L_f                     VARCHAR2(6) := 'F';
   L_disc_perc             VARCHAR2(1) := 'P';
   L_disc_val              VARCHAR2(1) := 'V';
   L_yes                   VARCHAR2(1) := 'Y';
   L_no                    VARCHAR2(1) := 'N';
   L_s                     VARCHAR2(1) := 'S';
   L_i                     VARCHAR2(1) := 'I';
   L_m                     VARCHAR2(1) := 'M';
   L_juris                 VARCHAR2(1) := 'J';
   L_fatura                VARCHAR2(1) := 'F';
   L_customer              VARCHAR2(4) := 'CUST';
   L_loc                   VARCHAR2(4) := 'LOC';
   L_store                 VARCHAR2(1) := 'S';
   L_wh                    VARCHAR2(1) := 'W';
   L_approved              VARCHAR2(1) := 'A';
   L_tsf                   VARCHAR2(3)  := 'TSF';
   L_ic                    VARCHAR2(2)  := 'IC';
   L_rep                   VARCHAR2(3)  := 'REP';
   L_difftaxregime           NUMBER    := 0;
   L_supp_nameval            NUMBER    := 0;
   L_loc_nameval             NUMBER    := 0;
   L_doc_comp_item_nameval   NUMBER    := 0;
   L_doc_detail_item_nameval NUMBER    := 0;
   L_detail_item_nameval     NUMBER    := 0;
   L_comp_item_nameval       NUMBER    := 0;
   L_tbl_documentLineItem  "RIB_LineItemRBO_TBL"  := "RIB_LineItemRBO_TBL"();
   L_doclineItemRBO        "RIB_LineItemRBO_REC";
   L_uom_qty               FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE   :=NULL;
   ---
   L_sup_name_value_tbl    ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   L_loc_name_value_tbl    ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   L_item_name_value_tbl   ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   ---
   cursor C_GET_DOC_HEADER is
      select decode(fdh.mode_type,L_exit,L_outbound,L_inbound) operation_type,
             fdh.requisition_type,
             fdh.issue_date,
             NVL(fdh.entry_or_exit_date,sysdate) entry_or_exit_date,
             fdh.freight_cost,
             decode(fdh.freight_type,L_cif,L_c,L_fob,L_f,'') freight_type,
             fdh.discount_type,
             fdh.total_discount_value,
             ROUND(decode(discount_type, L_disc_perc, total_item_value * total_discount_value / 100, L_disc_val, total_discount_value, 0),4) header_disc,
             ROUND(fdh.other_expenses_cost,4) other_expenses_cost,
             ROUND(fdh.insurance_cost,4) insurance_cost,
             fdh.total_weight,
             fdh.net_weight,
             fdh.type_id,
             ffu.nop,
             fua.comp_nf_ind,
             fdh.series_no,
             fdh.fiscal_doc_no,
             fdh.module,
             fdh.key_value_1,
             fdh.key_value_2,
             fdh.location_type,
             fdh.location_id,
             fdh.partner_type,
             fdh.partner_id,
             fdh.nf_cfop,
             fdh.entry_cfop,
             fdh.payment_date
        from fm_fiscal_utilization ffu,
             fm_utilization_attributes fua,
             fm_fiscal_stg_hdr fdh
       where fdh.fiscal_doc_id    = I_fiscal_doc_id
         and fdh.utilization_id   = ffu.utilization_id
         and ffu.utilization_id   = fua.utilization_id
         and fdh.tax_service_id   = I_tax_service_id;
   ---
   cursor C_GET_UOM_ITEM (P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
     select iscd.item,
            iscd.supplier,
            iscd.origin_country_id
       from item_supp_country iscd
      where iscd.item = P_item
        and primary_supp_ind = 'Y'
        and primary_country_ind = 'Y';
  ---
  cursor C_GET_CNAE_CODES(P_module         FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                          P_key_value_1    FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                          P_key_value_2    FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE) is
     select ecc.cnae_code
       from l10n_br_entity_cnae_codes ecc
      where ecc.module         = P_module
        and ecc.key_value_1    = P_key_value_1
  union all
    select null cnae_code
     from fm_rma_head frh,
          l10n_br_entity_cnae_codes ecc,
          v_br_country_attrib lc,
          country_tax_jurisdiction c
    where frh.cust_id    =  P_key_value_1
      and frh.cust_id = ecc.key_value_1
      and frh.country_id =  c.country_id
      and frh.state      =  c.state
      and frh.city       =  c.jurisdiction_code
      and P_key_value_2  =  L_c
      and P_module       =  L_customer
      and frh.country_id =  lc.country_id
      and rma_id         = (select max(rma_id) from fm_rma_head where cust_id = P_key_value_1);
   ---
   cursor C_GET_ENTITY_ATTRIBUTES(P_module         FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                                  P_key_value_1    FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                  P_key_value_2    FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE) is
    select fa.key_value_1,
           fa.city,
           c.jurisdiction_desc city_desc,
           lc.fiscal_code siscomex_id,
           fa.country_id,
           decode(fa.ipi_ind,L_yes,L_s,L_no) ipi_ind,
           decode(fa.iss_contrib_ind,L_yes,L_s,L_no) iss_ind,
           decode(fa.st_contrib_ind,L_yes,L_s,L_no) st_ind,
           decode(fa.icms_contrib_ind,L_yes,L_s,L_no) icms_ind,
           GET_LEGAL_NAME(P_module, P_key_value_1, P_key_value_2) legal_name,
           fa.state,
           fa.cnpj,
           fa.ie,
           fa.cpf,
           decode(fa.type,L_juris,L_s,L_fatura,L_no) type,
           fa.im,
           fa.simples_ind simples_ind,
           fa.rural_prod_ind rural_prod_ind,
           fa.is_income_range_eligible,
           fa.is_distr_a_manufacturer,
           fa.icms_simples_rate,
           CAST(MULTISET(select "RIB_DiffTaxRegime_REC"(0,tax_regime)
                           from l10n_br_sup_tax_regime
                          where to_char(supplier) = fa.key_value_1) as "RIB_DiffTaxRegime_TBL") DiffTaxRegime_TBL  -- DiffTaxRegime_TBL
      from v_fiscal_attributes fa,
           v_br_country_attrib lc,
           country_tax_jurisdiction c
           --city c
     where fa.module         = P_module
       and fa.key_value_1    = P_key_value_1
       and fa.key_value_2    = P_key_value_2
       and fa.country_id     = c.country_id
       and fa.state          = c.state
       and fa.city           = c.jurisdiction_code
       and fa.country_id     = lc.country_id
  union all
    select frh.cust_id,
           frh.city,
           c.jurisdiction_desc city_desc,
           lc.fiscal_code siscomex_id,
           frh.country_id,
           L_no ipi_ind,
           L_no iss_ind,
           L_no st_ind,
           L_no icms_ind,
           frh.cust_name legal_name,
           frh.state,
           frh.cnpj,
           null ie,
           frh.cpf,
           decode(cnpj,null,L_no,L_s) type,
           null im,
           L_no simples_ind,
           L_no rural_prod_ind,
           NULL,
           NULL,
           NULL,
           NULL
     from fm_rma_head frh,
          v_br_country_attrib lc,
          country_tax_jurisdiction c
           --city c
    where frh.cust_id    =  P_key_value_1
      and frh.country_id =  c.country_id
      and frh.state      =  c.state
      and frh.city       =  c.jurisdiction_code
      and P_key_value_2  =  L_c
      and P_module       =  L_customer
      and frh.country_id =  lc.country_id
      and rma_id         = (select max(rma_id) from fm_rma_head where cust_id = P_key_value_1);
   ---
   cursor C_GET_DOC_DETAIL is
      select fdh.fiscal_doc_id,
             fdd.fiscal_doc_line_id,
             fdd.fiscal_doc_line_id_ref,
             fdd.fiscal_doc_id_ref,
             fdd.icms_cst icms_cst,
             fdd.pis_cst pis_cst,
             fdd.cofins_cst cofins_cst,
             decode(ifa.service_ind,L_no,L_m,L_s) classification,
             fdd.item,
             fdd.requisition_no,
             fdd.quantity,
             ROUND((NVL(fdd.unit_item_disc,0) + NVL(fdd.unit_header_disc,0)) * fdd.quantity,4) total_disc,
             ROUND(fdd.unit_header_disc * fdd.quantity,4) header_line_disc,
             ROUND(fdd.freight_cost * fdd.quantity,4) freight_cost,
             ROUND(fdd.insurance_cost * fdd.quantity,4) insurance_cost,
             ROUND(fdd.other_expenses_cost * fdd.quantity,4) other_expenses_cost,
             ROUND(fdd.unit_cost * fdd.quantity,4) total_cost,
             ROUND(fdd.unit_cost,4) unit_cost,
             fdd.recoverable_base,
             fdd.recoverable_value,
             fdd.nf_cfop,
             fdd.entry_cfop,
             fdd.pack_ind,
             im.item_desc,
             im.standard_uom,
             ifa.origin_code origin_code,
             ifa.classification_id ncm_char_code,
             ifa.ncm_char_code||decode(ifa.pauta_code,NULL,'','.'||ifa.pauta_code) ncm_ex_char_code,
             ifa.ncm_char_code ncm_code,
             ifa.ex_ipi,
             ifa.federal_service,
             decode(fdh.location_type, L_store, (select city from v_br_store where store=fdh.location_id ),
                                        L_wh, (select city from v_br_wh where wh = fdh.location_id), null) city,
             ifa.service_code,
             ifa.service_ind,
             ifa.state_of_manufacture,
             ifa.pharma_list_type
        from fm_fiscal_stg_hdr fdh,
             fm_fiscal_stg_dtl fdd,
             item_master im ,
             v_br_item_fiscal_attrib ifa
       where fdd.fiscal_doc_id       = I_fiscal_doc_id
         and fdd.fiscal_doc_id       = fdh.fiscal_doc_id
         and fdd.item                = im.item
         and fdd.item                = ifa.item
         and fdd.pack_ind            = L_no
         and fdd.tax_service_id      = I_tax_service_id
         and fdd.tax_service_id      = fdh.tax_service_id
   union
      select fdh.fiscal_doc_id,
             fdd.fiscal_doc_line_id,
             fdd.fiscal_doc_line_id_ref,
             fdd.fiscal_doc_id_ref,
             fdd.icms_cst icms_cst,
             fdd.pis_cst pis_cst,
             fdd.cofins_cst cofins_cst,
             NULL classification,
             fdd.item,
             fdd.requisition_no,
             fdd.quantity,
             ROUND((NVL(fdd.unit_item_disc,0) + NVL(fdd.unit_header_disc,0)) * fdd.quantity,4) total_disc,
             ROUND(fdd.unit_header_disc * fdd.quantity,4) header_line_disc,
             ROUND(fdd.freight_cost * fdd.quantity,4) freight_cost,
             ROUND(fdd.insurance_cost * fdd.quantity,4) insurance_cost,
             ROUND(fdd.other_expenses_cost * fdd.quantity,4) other_expenses_cost,
             ROUND(fdd.unit_cost * fdd.quantity,4) total_cost,
             ROUND(fdd.unit_cost,4) unit_cost,
             fdd.recoverable_base,
             fdd.recoverable_value,
             fdd.nf_cfop,
             fdd.entry_cfop,
             fdd.pack_ind,
             im.item_desc,
             im.standard_uom,
             NULL origin_code,
             NULL ncm_char_code,
             NULL ncm_ex_char_code,
             NULL ncm_code,
             NULL ex_ipi,
             NULL federal_service,
             decode(fdh.location_type, L_store, (select city from v_br_store where store=fdh.location_id ),
                                        L_wh, (select city from v_br_wh where wh = fdh.location_id), null) city,
             NULL service_code,
             L_no service_ind,
             null,
             null
        from fm_fiscal_stg_hdr fdh,
             fm_fiscal_stg_dtl fdd,
             item_master im
       where fdd.fiscal_doc_id       = I_fiscal_doc_id
         and fdd.fiscal_doc_id       = fdh.fiscal_doc_id
         and fdd.item                = im.item
         and fdd.pack_ind            = L_yes
         and fdd.tax_service_id      = I_tax_service_id
         and fdd.tax_service_id      = fdh.tax_service_id;
   ---
   cursor C_GET_TRIANG_IND (P_fiscal_doc_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE) is
      select 'X'
        from fm_fiscal_stg_dtl fdd,
             ordhead ord
       where fdd.fiscal_doc_id       = P_fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and fdd.tax_service_id      = I_tax_service_id;
   ---
    cursor C_FM_TRIANG_OTHER_DOC (P_comp_nf_ind        FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE) is
      select fdc.fiscal_doc_id other_doc_id
        from fm_fiscal_stg_complement fdc,
             fm_fiscal_stg_dtl fdd,
             ordhead ord
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and P_comp_nf_ind           = L_yes
         and fdc.tax_service_id      = I_tax_service_id
         and fdd.tax_service_id      = fdc.tax_service_id
         and fdc.fiscal_doc_id is NOT NULL
       UNION
      select fdc.compl_fiscal_doc_id other_doc_id
        from fm_fiscal_stg_complement fdc,
             fm_fiscal_stg_dtl fdd,
             ordhead ord
       where fdc.fiscal_doc_id       = I_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and P_comp_nf_ind           = L_no
         and fdc.tax_service_id      = I_tax_service_id
         and fdd.tax_service_id      = fdc.tax_service_id
         and fdc.fiscal_doc_id is NOT NULL;
   ---
   cursor C_FM_OTHER_HEADER_DET (P_fiscal_doc_id       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select fdh.freight_cost,
             decode(fdh.freight_type,L_cif,L_c,L_fob,L_f,'') freight_type,
             ROUND(decode(discount_type, L_disc_perc, total_item_value * total_discount_value / 100, L_disc_val, total_discount_value, 0),4) header_disc,
             ROUND(fdh.other_expenses_cost,4) other_expenses_cost,
             ROUND(fdh.insurance_cost,4) insurance_cost,
             fdh.fiscal_doc_no,
             fdh.series_no,
             fdh.module,
             fdh.key_value_1,
             fdh.key_value_2
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = P_fiscal_doc_id;
   ---
   cursor C_FM_OTHER_DETAIL_DET (P_fiscal_doc_id       FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                 P_req_no              FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                 P_item                FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select ROUND((NVL(fdd.unit_item_disc,0) + NVL(fdd.unit_header_disc,0)) * fdd.quantity,4) total_disc,
             ROUND(fdd.freight_cost        * fdd.quantity,4) freight_cost,
             ROUND(fdd.insurance_cost      * fdd.quantity,4) insurance_cost,
             ROUND(fdd.other_expenses_cost * fdd.quantity,4) other_expenses_cost,
             fdd.nf_cfop,
             fdh.issue_date
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id   = P_fiscal_doc_id
         and fdd.fiscal_doc_id   = fdh.fiscal_doc_id
         and fdd.requisition_no  = P_req_no
         and fdd.item            = P_item;
   ---
   cursor C_GET_COMP_ITEMS (P_fiscal_doc_line_id             FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            P_unit_cost                      FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                            P_quantity                       FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE) is
      select distinct pi.item, P_quantity * pi.pack_qty quantity
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * P_unit_cost / pi.pack_qty),4) unit_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.unit_item_disc, 0) / pi.pack_qty),4) unit_item_disc
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.unit_header_disc, 0) / pi.pack_qty),4) unit_header_disc
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.freight_cost, 0) / pi.pack_qty),4) unit_freight_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.insurance_cost, 0) / pi.pack_qty),4) unit_insurance_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.other_expenses_cost, 0) / pi.pack_qty),4) unit_other_exp_cost
             , decode(ifa.service_ind,L_no,L_m,L_s) classification
             , im.item_desc
             , im.standard_uom
             , ifa.federal_service
             ,decode(fdh.location_type, L_store, (select city from v_br_store where store=fdh.location_id ),
                                        L_wh, (select city from v_br_wh where wh = fdh.location_id), null) city
             , ifa.service_code
             , ifa.ncm_char_code ncm_code
             , ifa.classification_id ncm_char_code
             , ifa.ncm_char_code||decode(ifa.pauta_code,NULL,'','.'||ifa.pauta_code) ncm_ex_char_code
             , ifa.origin_code origin_code
             , ifa.state_of_manufacture
             , ifa.pharma_list_type
        from fm_fiscal_stg_dtl fdd,
             fm_fiscal_stg_hdr fdh,
             packitem pi,
             item_supp_country_loc iscl,
             item_master im,
             v_br_item_fiscal_attrib ifa,
             ordloc o,
                   ordhead oh,
             (select pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,iscl1.origin_country_id,
                     sum(pi1.pack_qty * iscl1.negotiated_item_cost) tot_comp_cost
                from packitem pi1,
                     item_supp_country_loc iscl1
               where pi1.item                    = iscl1.item
               group by pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,iscl1.origin_country_id) total_pack
       where fdd.fiscal_doc_id            = fdh.fiscal_doc_id
         and fdd.tax_service_id           = fdh.tax_service_id
         and pi.pack_no                   = total_pack.pack_no
         and pi.pack_no                   = fdd.item
         and pi.item                      = iscl.item
         and pi.item                      = im.item
         and pi.item                      = ifa.item
         and oh.order_no                  = o.order_no
         and oh.supplier                  = iscl.supplier
         and oh.supplier                  = total_pack.supplier
         and fdd.requisition_no           = o.order_no
         and fdd.item                     = o.item
         and fdh.location_type            = total_pack.loc_type
         and o.location                   = total_pack.loc
         and o.location                  in ((select wh loc
                                                from wh
                                               where physical_wh = fdh.location_id)
                                             union
                                             (select store loc
                                                from store
                                               where store = fdh.location_id))
         and fdh.location_type            = iscl.loc_type
         and iscl.loc                     = o.location
         and iscl.loc                   in ((select wh loc
                                                 from wh
                                                where physical_wh = fdh.location_id)
                                              union
                                              (select store loc
                                                 from store
                                                where store = fdh.location_id))
         and fdd.fiscal_doc_id            = I_fiscal_doc_id
         and fdd.tax_service_id           = I_tax_service_id
         and fdd.fiscal_doc_line_id       = P_fiscal_doc_line_id
         and ifa.country_id               = iscl.origin_country_id
         and total_pack.origin_country_id = iscl.origin_country_id
       union 
        select distinct pi.item, P_quantity * pi.pack_qty quantity
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * P_unit_cost / pi.pack_qty),4) unit_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.unit_item_disc, 0) / pi.pack_qty),4) unit_item_disc
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.unit_header_disc, 0) / pi.pack_qty),4) unit_header_disc
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.freight_cost, 0) / pi.pack_qty),4) unit_freight_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.insurance_cost, 0) / pi.pack_qty),4) unit_insurance_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.other_expenses_cost, 0) / pi.pack_qty),4) unit_other_exp_cost
             , decode(ifa.service_ind,L_no,L_m,L_s) classification
             , im.item_desc
             , im.standard_uom
             , ifa.federal_service
             ,decode(fdh.location_type, L_store, (select city from v_br_store where store=fdh.location_id ),
                                        L_wh, (select city from v_br_wh where wh = fdh.location_id), null) city
             , ifa.service_code
             , ifa.ncm_char_code ncm_code
             , ifa.classification_id ncm_char_code
             , ifa.ncm_char_code||decode(ifa.pauta_code,NULL,'','.'||ifa.pauta_code) ncm_ex_char_code
             , ifa.origin_code origin_code
             , ifa.state_of_manufacture
             , ifa.pharma_list_type
        from fm_fiscal_stg_hdr fdh,
             fm_fiscal_stg_dtl fdd,
             packitem pi,
             item_supp_country_loc iscl,
             item_master im,
             v_br_item_fiscal_attrib ifa,
             (select pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,iscl1.origin_country_id,
                     sum(pi1.pack_qty * iscl1.negotiated_item_cost) tot_comp_cost
                from packitem pi1,
                     item_supp_country_loc iscl1
               where pi1.item                    = iscl1.item
               group by pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,iscl1.origin_country_id) total_pack
       where fdd.fiscal_doc_id            = fdh.fiscal_doc_id
         and fdd.tax_service_id           = fdh.tax_service_id
         and pi.pack_no                   = total_pack.pack_no
         and pi.pack_no                   = fdd.item
         and pi.item                      = iscl.item
         and pi.item                      = im.item
         and pi.item                      = ifa.item
         and fdd.requisition_no  is null
         and fdh.location_type            = total_pack.loc_type
         and fdh.location_id              = total_pack.loc
         and iscl.loc                    = total_pack.loc
         and fdh.location_type            = iscl.loc_type
         and iscl.loc                   in ((select wh loc
                                                 from wh
                                                where physical_wh = fdh.location_id)
                                              union
                                              (select store loc
                                                 from store
                                                where store = fdh.location_id))
         and fdd.fiscal_doc_id            = I_fiscal_doc_id
         and fdd.fiscal_doc_line_id       = P_fiscal_doc_line_id
         and fdd.tax_service_id           = I_tax_service_id
         and ifa.country_id               = iscl.origin_country_id
         and total_pack.origin_country_id = iscl.origin_country_id;
   ---
   cursor C_GET_COMP_ITEMS_RTV (P_fiscal_doc_line_id             FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                P_unit_cost                      FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                                P_quantity                       FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE) is
      select pi.item, P_quantity * pi.pack_qty quantity
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * P_unit_cost / pi.pack_qty),4) unit_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.unit_item_disc, 0) / pi.pack_qty),4) unit_item_disc
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.unit_header_disc, 0) / pi.pack_qty),4) unit_header_disc
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.freight_cost, 0) / pi.pack_qty),4) unit_freight_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.insurance_cost, 0) / pi.pack_qty),4) unit_insurance_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.other_expenses_cost, 0) / pi.pack_qty),4) unit_other_exp_cost
             , decode(ifa.service_ind,L_no,L_m,L_s) classification
             , im.item_desc
             , im.standard_uom
             , ifa.federal_service
             ,decode(fdh.location_type, L_store, (select city from v_br_store where store=fdh.location_id ),
                                        L_wh, (select city from v_br_wh where wh = fdh.location_id), null) city
             , ifa.service_code
             , ifa.ncm_char_code ncm_code
             , ifa.classification_id ncm_char_code
             ,ifa.ncm_char_code||decode(ifa.pauta_code,NULL,'','.'||ifa.pauta_code) ncm_ex_char_code
             , ifa.origin_code origin_code
             , ifa.state_of_manufacture
             , ifa.pharma_list_type
        from fm_fiscal_stg_dtl fdd,
             fm_fiscal_stg_hdr fdh,
             packitem pi,
             item_supp_country_loc iscl,
             item_master im,
             v_br_item_fiscal_attrib ifa,
             (select pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,iscl1.origin_country_id,
                     sum(pi1.pack_qty * iscl1.negotiated_item_cost) tot_comp_cost
                from packitem pi1,
                     item_supp_country_loc iscl1
               where pi1.item                    = iscl1.item
               group by pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,iscl1.origin_country_id) total_pack
       where fdd.fiscal_doc_id            = fdh.fiscal_doc_id
         and fdd.tax_service_id           = fdh.tax_service_id
         and pi.pack_no                   = total_pack.pack_no
         and pi.pack_no                   = fdd.item
         and pi.item                      = iscl.item
         and pi.item                      = im.item
         and pi.item                      = ifa.item
         and fdh.location_type            = total_pack.loc_type
         and fdh.location_type            = iscl.loc_type
         and iscl.loc                     = total_pack.loc
         and iscl.loc                   in ((select primary_vwh loc
                                                 from wh
                                                where physical_wh = fdh.location_id)
                                              union
                                              (select store loc
                                                 from store
                                                where store = fdh.location_id))
         and fdd.fiscal_doc_id            = I_fiscal_doc_id
         and fdd.tax_service_id           = I_tax_service_id
         and fdd.fiscal_doc_line_id       = P_fiscal_doc_line_id
         and fdh.key_value_1              = iscl.supplier
         and fdh.key_value_1              = total_pack.supplier
         and ifa.country_id               = iscl.origin_country_id
         and total_pack.origin_country_id   = iscl.origin_country_id;
   ---
   cursor C_GET_COMP_ITEM_TAXES (P_fiscal_doc_line_id             FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                 P_item                           PACKITEM.ITEM%TYPE,
                                 P_tax_code                       FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE) is
      select ROUND((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(ftdw.tax_basis,ftdw.appr_base_value),4) tax_basis,
             ROUND((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(ftdw.modified_tax_basis,ftdw.appr_modified_base_value),4) modified_tax_basis,
             ROUND((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * DECODE(ftdw.total_value,0,ftdw.appr_tax_value,ftdw.total_value),4) tax_value,
             DECODE(ftdw.percentage_rate,0,ftdw.appr_tax_rate,ftdw.percentage_rate) tax_rate
        from fm_fiscal_stg_tax_dtl ftdw,
             fm_fiscal_stg_dtl fdd,
             fm_fiscal_stg_hdr fdh,
             packitem pi,
             item_supp_country_loc iscl,
             ordloc o,
             ordhead oh,
             (select pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,
                     sum(pi1.pack_qty * iscl1.negotiated_item_cost) tot_comp_cost
                from packitem pi1,
                     item_supp_country_loc iscl1
               where pi1.item                    = iscl1.item
               group by pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc) total_pack
       where ftdw.fiscal_doc_line_id      = P_fiscal_doc_line_id
         and ftdw.tax_service_id          = I_tax_service_id
         and ftdw.tax_service_id          = fdd.tax_service_id
         and ftdw.fiscal_doc_line_id      = fdd.fiscal_doc_line_id
         and ftdw.vat_code                = P_tax_code
         and fdd.fiscal_doc_id            = fdh.fiscal_doc_id
         and fdd.tax_service_id           = fdh.tax_service_id
         and fdd.requisition_no           = o.order_no
         and fdd.item                     = o.item
         and pi.pack_no                   = total_pack.pack_no
         and pi.pack_no                   = fdd.item
         and pi.item                      = iscl.item
         and pi.item                      = P_item
         and oh.order_no                  = o.order_no
         and oh.supplier                  = total_pack.supplier
         and oh.supplier                  = iscl.supplier
         and fdh.location_type            = total_pack.loc_type
         and o.location                   = total_pack.loc
         and o.location                  in ((select wh loc
                                                from wh
                                               where physical_wh = fdh.location_id)
                                             union
                                             (select store loc
                                                from store
                                               where store = fdh.location_id))
         and fdh.location_type            = iscl.loc_type
         and iscl.loc                     = o.location
         and iscl.loc                   in ((select wh loc
                                                 from wh
                                                where physical_wh = fdh.location_id)
                                              union
                                              (select store loc
                                                 from store
                                                where store = fdh.location_id));
   ---
   cursor C_GET_NF_TAX_DETAIL (P_fiscal_doc_line_id        FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                               P_tax_code                  FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE) is
      select NVL(fdtd.tax_basis,fdtd.appr_base_value) tax_basis,
             NVL(fdtd.modified_tax_basis,fdtd.appr_modified_base_value) modified_tax_basis,
             DECODE(fdtd.total_value,0,fdtd.appr_tax_value,fdtd.total_value) tax_value,
             DECODE(fdtd.percentage_rate,0,fdtd.appr_tax_rate,fdtd.percentage_rate) tax_rate
        from fm_fiscal_stg_tax_dtl     fdtd,
             fm_fiscal_stg_dtl         fdd
       where fdtd.fiscal_doc_line_id    = fdd.fiscal_doc_line_id
         and fdd.fiscal_doc_id          = I_fiscal_doc_id
         and fdd.tax_service_id         = I_tax_service_id
         and fdtd.tax_service_id        = fdd.tax_service_id
         and fdtd.fiscal_doc_line_id    = P_fiscal_doc_line_id
         and fdtd.vat_code              = P_tax_code;
   ---
   cursor C_GET_TAX_RESL_NF is
      select fmr.tax_code
        from fm_resolution fmr
       where fmr.fiscal_doc_id              = I_fiscal_doc_id
         and fmr.fiscal_doc_line_id         = I_fiscal_doc_line_id
         and fmr.discrep_type               = L_tax_discrep_type
         and fmr.resolution_type            = L_nf_res;
   ---
   cursor C_GET_NF_COMP_QTY (P_item        PACKITEM.ITEM%TYPE) is
      select fdd.quantity * pi.pack_qty quantity
        from fm_fiscal_stg_dtl  fdd,
             packitem             pi
       where fdd.fiscal_doc_id               = I_fiscal_doc_id
         and fdd.tax_service_id              = I_tax_service_id
         and fdd.fiscal_doc_line_id          = I_fiscal_doc_line_id
         and fdd.pack_ind                    = L_yes
         and fdd.item                        = pi.pack_no
         and pi.item                         = P_item;
   ---
   ---
   cursor C_GET_NIC_IND is
      select vat_code, incl_nic_ind
        from vat_codes;
   ---
   cursor C_GET_ST_IND is
      select distinct DECODE(fdh.location_type,L_store,scf.control_rec_st_ind,L_wh,wcf.control_rec_st_ind) as control_ind,
                      fa.icmsst_recovery_ind
                 from v_br_store_control_flags scf,
                      v_br_wh_control_flags wcf,
                      fm_fiscal_stg_hdr fdh,
                      fm_fiscal_utilization fu,
                      fm_utilization_attributes fa
                where fdh.fiscal_doc_id     = I_fiscal_doc_id
                  and fdh.tax_service_id    = I_tax_service_id
                  and ((fdh.location_type   = L_store
                  and fdh.location_id       = scf.store)
                  or (fdh.location_type     = L_wh
                  and fdh.location_id       = wcf.wh))
                  and fu.status             = L_approved
                  and fa.utilization_id     = fu.utilization_id
                  and fa.utilization_id     = fdh.utilization_id;
   ---
    cursor C_GET_ORIG_CFOP_TSF(P_location_type FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                               P_location_id   FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                               P_item          FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select fdh.nf_cfop
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_utilization_attributes fua,
             fm_fiscal_utilization ffu
       where fdh.fiscal_doc_id        = fdd.fiscal_doc_id
         and fdh.utilization_id       = fua.utilization_id
         and fdh.utilization_id       = ffu.utilization_id
         and fdd.location_type        = P_location_type
         and fdd.location_id          = P_location_id
         and fdd.item                 = P_item
         and fdh.status               in ('A','C','FP')
         and fdh.requisition_type     = 'PO'
         and fua.comp_nf_ind          = L_no
         and ffu.status               = 'A'
    order by fdh.entry_or_exit_date desc,
             fdh.fiscal_Doc_id desc;
   ---
  cursor C_SUP_CUSTOM_ATTRIB IS
      select ENTITY_ATTRIB_REC(
             'SUPP',        --I_entity_type
             ffdh.key_value_1,  --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from fm_fiscal_stg_hdr ffdh
       where ffdh.fiscal_doc_id  = I_fiscal_doc_id
         and ffdh.tax_service_id  = I_tax_service_id
         and ffdh.module = 'SUPP';
   ---
   cursor C_LOC_CUSTOM_ATTRIB IS
      select ENTITY_ATTRIB_REC(
             'LOC',        --I_entity_type
             ffdh.location_id,    --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
       from fm_fiscal_stg_hdr ffdh
       where ffdh.fiscal_doc_id  = I_fiscal_doc_id
         and ffdh.tax_service_id  = I_tax_service_id
      union all
      select ENTITY_ATTRIB_REC(
             'LOC',        --I_entity_type
             ffdh.key_value_1,    --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from fm_fiscal_stg_hdr ffdh
       where ffdh.fiscal_doc_id  = I_fiscal_doc_id
         and ffdh.module = 'PTNR'
         and ffdh.key_value_2 = 'E'
      union all
      select ENTITY_ATTRIB_REC(
             'LOC',        --I_entity_type
             ffdh.key_value_1,    --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from fm_fiscal_stg_hdr ffdh
       where ffdh.fiscal_doc_id  = I_fiscal_doc_id
         and ffdh.tax_service_id  = I_tax_service_id
         and ffdh.module = 'LOC';
   ---
   cursor C_DETAIL_ITEM_CUSTOM_ATTRIB(P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) IS
      select ENTITY_ATTRIB_REC(
            'ITEM',     --I_entity_type
             ffdd.item,  --I_entity_id
             NULL,       --O_name
             NULL)       --O_value
        from fm_fiscal_stg_hdr ffdh,
             fm_fiscal_stg_dtl ffdd
       where I_fiscal_doc_id = ffdh.fiscal_doc_id
         and ffdh.tax_service_id  = I_tax_service_id
         and ffdh.tax_service_id = ffdd.tax_service_id
         and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
         and ffdd.item = P_item;
   ---
   cursor C_COMP_ITEM_CUSTOM_ATTRIB(P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) IS
      select ENTITY_ATTRIB_REC(
            'ITEM',     --I_entity_type
             pi.item,  --I_entity_id
             NULL,       --O_name
             NULL)       --O_value
        from fm_fiscal_stg_hdr ffdh,
             fm_fiscal_stg_dtl ffdd,
             packitem pi
       where I_fiscal_doc_id = ffdh.fiscal_doc_id
         and ffdh.tax_service_id  = I_tax_service_id
         and ffdh.tax_service_id = ffdd.tax_service_id
         and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
         and pi.item = P_item
         and pi.pack_no = ffdd.item;
   ---
   L_get_doc_header           C_GET_DOC_HEADER%ROWTYPE;
   L_get_doc_detail           C_GET_DOC_DETAIL%ROWTYPE;
   L_get_comp_items           C_GET_COMP_ITEMS%ROWTYPE;
   L_ent_supp                 C_GET_ENTITY_ATTRIBUTES%ROWTYPE;
   L_ent_loc                  C_GET_ENTITY_ATTRIBUTES%ROWTYPE;
   L_ent_partner              C_GET_ENTITY_ATTRIBUTES%ROWTYPE;
   L_fm_other_header_det      C_FM_OTHER_HEADER_DET%ROWTYPE;
   L_fm_other_detail_det      C_FM_OTHER_DETAIL_DET%ROWTYPE;
   L_ent_main_nf_supp         C_GET_ENTITY_ATTRIBUTES%ROWTYPE;
   L_get_nf_tax_detail        C_GET_NF_TAX_DETAIL%ROWTYPE;
   L_get_comp_item_taxes      C_GET_COMP_ITEM_TAXES%ROWTYPE;
   L_get_nic_ind              C_GET_NIC_IND%ROWTYPE;
   L_get_payment_date         FM_FISCAL_DOC_PAYMENTS.PAYMENT_DATE%TYPE;
   L_tax_code                 FM_RESOLUTION.TAX_CODE%TYPE;
   L_ref_fiscal_doc_id        FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID_REF%TYPE;
   L_cnae_code                      L10N_BR_ENTITY_CNAE_CODES.CNAE_CODE%TYPE;
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
      fiscalDocumentRBO           "RIB_FiscDocRBO_REC"            := "RIB_FiscDocRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   sources                     "RIB_FromEntity_REC"            := "RIB_FromEntity_REC"(0,null);
   destinations                "RIB_ToEntity_REC"              := "RIB_ToEntity_REC"(0,null);
   fiscalEntRBOSource          "RIB_FiscEntityRBO_REC"         := "RIB_FiscEntityRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null) ;
   fiscalEntRBODest            "RIB_FiscEntityRBO_REC"         := "RIB_FiscEntityRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null) ;
   sourceAddress               "RIB_AddrRBO_REC"               := "RIB_AddrRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);
   destinationAddress          "RIB_AddrRBO_REC"               := "RIB_AddrRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);

   fiscDocChnkRBM              "RIB_FiscDocChnkRBO_REC"        := "RIB_FiscDocChnkRBO_REC"(0,null,null);
   tbl_fiscDocChnkRBM          "RIB_FiscDocChnkRBO_TBL"        := "RIB_FiscDocChnkRBO_TBL"();

   fiscalDocumentColRBM        "RIB_FiscDocColRBM_REC"         := "RIB_FiscDocColRBM_REC"(0,null,null,null,null,null);

   taxdetailRBO                "RIB_InformTaxRBO_REC"          := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);

   tax_contributor1            "RIB_TaxContributor_REC"        := "RIB_TaxContributor_REC"(0,null);
   tax_contributor2            "RIB_TaxContributor_REC"        := "RIB_TaxContributor_REC"(0,null);
   tax_contributor3            "RIB_TaxContributor_REC"        := "RIB_TaxContributor_REC"(0,null);
   tax_contributor4            "RIB_TaxContributor_REC"        := "RIB_TaxContributor_REC"(0,null);
   tax_contributor5            "RIB_TaxContributor_REC"        := "RIB_TaxContributor_REC"(0,null);
   tax_contributor6            "RIB_TaxContributor_REC"        := "RIB_TaxContributor_REC"(0,null);
   tax_contributor7            "RIB_TaxContributor_REC"        := "RIB_TaxContributor_REC"(0,null);
   source_eco                  "RIB_EcoClassCd_REC"            := "RIB_EcoClassCd_REC"(0,null);
   dest_eco                    "RIB_EcoClassCd_REC"            := "RIB_EcoClassCd_REC"(0,null);
   documentLineItem            "RIB_LineItemRBO_REC"           := "RIB_LineItemRBO_REC"(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   serviceItem                 "RIB_SvcItemRBO_REC"            := "RIB_SvcItemRBO_REC"(0,null,null,null,null,null,null,null);
   productItem                 "RIB_PrdItemRBO_REC"            := "RIB_PrdItemRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null);
   ---
   difftaxregime               "RIB_DiffTaxRegime_REC"     := "RIB_DiffTaxRegime_REC"(0,null);
   tbl_difftaxregime           "RIB_DiffTaxRegime_TBL"     := "RIB_DiffTaxRegime_TBL"();
   supp_nameval                "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   loc_nameval                 "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   doc_comp_item_nameval       "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   doc_detail_item_nameval     "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   detail_item_nameval         "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   comp_item_nameval           "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   tbl_supp_nameval            "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_doc_comp_item_nameval   "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_doc_detail_item_nameval "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_detail_item_nameval     "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_comp_item_nameval       "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_loc_nameval             "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   ---
   tbl_documentLineItem        "RIB_LineItemRBO_TBL"       := "RIB_LineItemRBO_TBL"();
   tbl_sources                 "RIB_FromEntity_TBL"        := "RIB_FromEntity_TBL"();
   tbl_destinations            "RIB_ToEntity_TBL"          := "RIB_ToEntity_TBL"();
   tbl_fiscalDocument_request  "RIB_FiscDocRBO_TBL"        := "RIB_FiscDocRBO_TBL"();
   tbl_taxdetail               "RIB_InformTaxRBO_TBL"      := "RIB_InformTaxRBO_TBL"();
   tbl_tax_contrib_source      "RIB_TaxContributor_TBL"    := "RIB_TaxContributor_TBL"(null);
   tbl_tax_contrib_dest        "RIB_TaxContributor_TBL"    := "RIB_TaxContributor_TBL"(null);
   tbl_source_eco              "RIB_EcoClassCd_TBL"        := "RIB_EcoClassCd_TBL"();
   tbl_dest_eco                "RIB_EcoClassCd_TBL"        := "RIB_EcoClassCd_TBL"();
   tbl_source_addr             "RIB_AddrRBO_TBL"           := "RIB_AddrRBO_TBL"();
   tbl_dest_addr               "RIB_AddrRBO_TBL"           := "RIB_AddrRBO_TBL"();
   tbl_fiscalEntRBOSource      "RIB_FiscEntityRBO_TBL"     :="RIB_FiscEntityRBO_TBL"();
   tbl_fiscalEntRBODest        "RIB_FiscEntityRBO_TBL"     :="RIB_FiscEntityRBO_TBL"();
   tbl_productItem             "RIB_PrdItemRBO_TBL"        :="RIB_PrdItemRBO_TBL"();
   tbl_serviceItem             "RIB_SvcItemRBO_TBL"        :="RIB_SvcItemRBO_TBL"();
   ---
   startTime    TIMESTAMP;
   endTime      TIMESTAMP;
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
   ---
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_FISCAL_DOC_ID',NULL,NULL,NULL);
      return FALSE;
   ---
   end if;
   ---
   open C_GET_NIC_IND;
   ---
   LOOP
      ---
      fetch C_GET_NIC_IND bulk collect into L_vat_code_tab limit 500;
      EXIT when C_GET_NIC_IND%NOTFOUND;
      ---
   END LOOP;
   ---
   close C_GET_NIC_IND;
   --
   open C_GET_DOC_HEADER;
   ---
   FETCH C_GET_DOC_HEADER into L_get_doc_header;
   ---
   if C_GET_DOC_HEADER%NOTFOUND then
      --
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_HEAD',NULL,NULL,NULL);
      ---
      close C_GET_DOC_HEADER;
      ---
      return FALSE;
   end if;
   ---
   if L_get_doc_header.operation_type = L_outbound or L_get_doc_header.requisition_type = L_rma then
      L_supp_issuer := 'N';
      ---
      if L_get_doc_header.requisition_type = L_rma then
         L_item_attr := 'Y';
      else
         L_item_attr := 'N';
         L_transac_type :='O';
      end if;
      ---
   end if;
   ---
   if L_get_doc_header.requisition_type = L_rma or L_get_doc_header.operation_type = L_outbound then
      L_calc_process_type  := L_tax;
   end if;
   ---
   close C_GET_DOC_HEADER;
   ---
   if L_get_doc_header.operation_type = L_outbound then
      L_source       := L_get_doc_header.location_id;
      L_source_type  := L_get_doc_header.location_type;
      L_src_location := 'LOC';
   else
      L_source       := L_get_doc_header.key_value_1;
      L_source_type  := L_get_doc_header.key_value_2;
      L_src_location := L_get_doc_header.module;
   end if;
   open C_GET_ENTITY_ATTRIBUTES(L_src_location, L_source, L_source_type);
   ---
   fetch C_GET_ENTITY_ATTRIBUTES into L_ent_supp;
   ---
   if C_GET_ENTITY_ATTRIBUTES%NOTFOUND then
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_SOURCE',TO_CHAR(L_source),NULL,NULL);
      return FALSE;
   end if;
   ---
   close C_GET_ENTITY_ATTRIBUTES;
   ---
   if L_get_doc_header.operation_type = L_outbound then
      L_dest_location := L_get_doc_header.module;
      L_dest          := L_get_doc_header.key_value_1;
      L_dest_type     := L_get_doc_header.key_value_2;
   else
      L_dest       := L_get_doc_header.location_id;
      L_dest_type  := L_get_doc_header.location_type;
      L_dest_location   := 'LOC';
   end if;
   open C_GET_ENTITY_ATTRIBUTES(L_dest_location, L_dest, L_dest_type);
   ---
   fetch C_GET_ENTITY_ATTRIBUTES into L_ent_loc;
   ---
   if C_GET_ENTITY_ATTRIBUTES%NOTFOUND then
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_DEST',TO_CHAR(L_dest),NULL,NULL);
      return FALSE;
   end if;
   ---
   close C_GET_ENTITY_ATTRIBUTES;
   ---
   open C_GET_TRIANG_IND(I_fiscal_doc_id);
   ---
   fetch C_GET_TRIANG_IND into L_dummy;
   L_triang_po_exists := C_GET_TRIANG_IND%FOUND;
   ---
   close C_GET_TRIANG_IND;
   ---
   if L_triang_po_exists then
   ---
      L_calc_process_type  := L_tax;
      open C_FM_TRIANG_OTHER_DOC (L_get_doc_header.comp_nf_ind);
      ---
      fetch C_FM_TRIANG_OTHER_DOC into L_other_doc_id;
      ---
      close C_FM_TRIANG_OTHER_DOC;
      ---
   ---
   end if;
   ---
   if (L_get_doc_header.operation_type = L_inbound and L_get_doc_header.requisition_type not in  (L_rma,L_tsf,L_ic,L_rep)) or L_get_doc_header.requisition_type = L_rtv then
     open C_SUP_CUSTOM_ATTRIB;
     fetch C_SUP_CUSTOM_ATTRIB bulk collect into L_sup_name_value_tbl;
     close C_SUP_CUSTOM_ATTRIB;
     if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                   L_sup_name_value_tbl,
                                                   'SUPP') = FALSE then
        return FALSE;
     end if;
   end if;
   -- build a collection of non-base localization attributes for locations
   open C_LOC_CUSTOM_ATTRIB;
   fetch C_LOC_CUSTOM_ATTRIB bulk collect into L_loc_name_value_tbl;
   close C_LOC_CUSTOM_ATTRIB;
   if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                 L_loc_name_value_tbl,
                                                 'LOC') = FALSE then
      return FALSE;
   end if;

   ---
   fiscalEntRBOSource.federal_tax_reg_id           :=NVL(L_ent_supp.cnpj,L_ent_supp.cpf);
   fiscalEntRBOSource.fiscal_type                  :=L_ent_supp.type;
   ---
    open C_GET_CNAE_CODES(L_src_location, L_source, L_source_type);
      ---
    fetch C_GET_CNAE_CODES into L_cnae_code;
      ---
      if C_GET_CNAE_CODES%FOUND then
        cnae_src_found := true;
      end if;
    close  C_GET_CNAE_CODES;
    if (cnae_src_found) then
           for cnae_codes in C_GET_CNAE_CODES(L_src_location, L_source, L_source_type) loop
                    tbl_source_eco.extend();
                    L_source_eco := L_source_eco + 1;
                   source_eco.value := cnae_codes.cnae_code;
                   tbl_source_eco(L_source_eco) := source_eco;
           end loop;
     end if;
   ---
   if L_sup_name_value_tbl.exists(1) then
      for i in  L_sup_name_value_tbl.FIRST .. L_sup_name_value_tbl.LAST loop
         tbl_supp_nameval.extend();
               L_supp_nameval := L_supp_nameval + 1;
                     supp_nameval.name := L_sup_name_value_tbl(i).O_name;
                     supp_nameval.value := L_sup_name_value_tbl(i).O_value;
                     tbl_supp_nameval(L_supp_nameval) := supp_nameval;
      end loop;
   end if;
   ---
   fiscalEntRBOSource.NameValPairRBO_TBL          := tbl_supp_nameval;
   ---
   fiscalEntRBOSource.EcoClassCd_TBL      := tbl_source_eco;
   -- Supplier Address
   sourceAddress.city_id               := L_ent_supp.city;
   sourceAddress.state                 := L_ent_supp.state;
   sourceAddress.country_id            := L_ent_supp.siscomex_id;
   sourceAddress.addr                  := L_addr;
   sourceAddress.addr_type             := L_addr;
   sourceAddress.primary_addr_type_ind := L_addr;
   sourceAddress.add_1                 := L_addr;
   sourceAddress.primary_addr_ind      := L_addr;
   sourceAddress.city                  := L_ent_supp.city_desc;
   ---
   --not in client but mapped in tcd
   fiscalEntRBOSource.entity_code         := L_ent_supp.key_value_1;
   fiscalEntRBOSource.legal_name          := L_ent_supp.legal_name;
   fiscalEntRBOSource.is_income_range_eligible  := L_ent_supp.is_income_range_eligible;
   fiscalEntRBOSource.is_distr_a_manufacturer   := L_ent_supp.is_distr_a_manufacturer;
   fiscalEntRBOSource.icms_simples_rate         := L_ent_supp.icms_simples_rate;
   fiscalEntRBOSource.is_simples_contributor         := L_ent_supp.simples_ind;
   if L_ent_supp.DiffTaxRegime_TBL.exists(1) then
      for rec in L_ent_supp.DiffTaxRegime_TBL.FIRST .. L_ent_supp.DiffTaxRegime_TBL.LAST loop
         tbl_difftaxregime.extend();
               L_difftaxregime := L_difftaxregime + 1;
                     difftaxregime.value := L_ent_supp.DiffTaxRegime_TBL(rec).value;
                     tbl_difftaxregime(L_difftaxregime) := difftaxregime;
      end loop;
    end if;
   fiscalEntRBOSource.DiffTaxRegime_TBL := tbl_difftaxregime;
   ---
   -- Tax Contributor's Details
   if L_ent_supp.ipi_ind = L_true then
      tax_contributor3.value := L_ipi;
   end if;
   ---
   if L_ent_supp.iss_ind = L_true then
      tax_contributor1.value := L_iss;
   end if;
   ---
   if L_ent_supp.st_ind = L_true then
      tax_contributor2.value := L_st;
   end if;
   ---
   if L_ent_supp.icms_ind = L_true then
      tax_contributor4.value := L_icms;
   end if;
   ---
   tbl_tax_contrib_source.DELETE();
   tax_cont:=0;
   if tax_contributor1.value=L_iss then
      tbl_tax_contrib_source.extend();
      tax_cont:=tax_cont+1;
      tbl_tax_contrib_source(tax_cont):=tax_contributor1;
   end if;
   if tax_contributor2.value=L_st then
      tbl_tax_contrib_source.extend();
      tax_cont:=tax_cont+1;
      tbl_tax_contrib_source(tax_cont):=tax_contributor2;
   end if;
   if tax_contributor3.value=L_ipi then
      tbl_tax_contrib_source.extend();
      tax_cont:=tax_cont+1;
      tbl_tax_contrib_source(tax_cont):=tax_contributor3;
   end if;
   if tax_contributor4.value=L_icms then
      tbl_tax_contrib_source.extend();
      tax_cont:=tax_cont+1;
      tbl_tax_contrib_source(tax_cont):=tax_contributor4;
   end if;
   ---
   if tbl_tax_contrib_source.count >0 then
      fiscalEntRBOSource.taxContributor_TBL :=tbl_tax_contrib_source;
      tbl_tax_contrib_source.DELETE();
   end if;
   ---
   tbl_source_addr.extend();
   tbl_source_addr(1) := sourceAddress;
   fiscalEntRBOSource.AddrRBO_TBL          := tbl_source_addr;
   fiscalEntRBOSource.is_rural_producer := L_ent_supp.rural_prod_ind;
   tbl_fiscalEntRBOSource.extend();
   tbl_fiscalEntRBOSource(1) := fiscalEntRBOSource;
   ---
   sources.FiscEntityRBO_TBL           := tbl_fiscalEntRBOSource;
   L_source_cont  := L_source_cont + 1;
   tbl_sources.extend();
   tbl_sources(L_source_cont):=sources;
   -----------------
   --Destination Details
   fiscalEntRBODest.federal_tax_reg_id           := NVL(L_ent_loc.cnpj,L_ent_loc.cpf);
   fiscalEntRBODest.fiscal_type                  := L_ent_loc.type;
   ---
   open C_GET_CNAE_CODES(L_dest_location, L_dest, L_dest_type);
      ---
      fetch C_GET_CNAE_CODES into L_cnae_code;
      ---
      if C_GET_CNAE_CODES%FOUND then
        cnae_dst_found := true;
      end if;
    close  C_GET_CNAE_CODES;
    if (cnae_dst_found) then
           for cnae_codes in C_GET_CNAE_CODES(L_dest_location, L_dest, L_dest_type) loop
                tbl_dest_eco.extend();
              L_dest_eco := L_dest_eco + 1;
              dest_eco.value := cnae_codes.cnae_code;
              tbl_dest_eco(L_dest_eco) := dest_eco;
           end loop;
    end if;
   ---
   if L_loc_name_value_tbl.exists(1) then
      for i in  L_loc_name_value_tbl.FIRST .. L_loc_name_value_tbl.LAST loop
         tbl_loc_nameval.extend();
               L_loc_nameval := L_loc_nameval + 1;
                     loc_nameval.name := L_loc_name_value_tbl(i).O_name;
                     loc_nameval.value := L_loc_name_value_tbl(i).O_value;
                     tbl_loc_nameval(L_loc_nameval) := loc_nameval;
      end loop;
   end if;
   ---
   fiscalEntRBODest.NameValPairRBO_TBL          := tbl_loc_nameval;
   ---
   fiscalEntRBODest.EcoClassCd_TBL      := tbl_dest_eco;
   ---
   fiscalEntRBODest.entity_code         := L_ent_loc.key_value_1;
   fiscalEntRBODest.legal_name          := L_ent_loc.legal_name;
   ---
   -- Destination Address
   destinationAddress.city_id               := L_ent_loc.city;
   destinationAddress.state                 := L_ent_loc.state;
   destinationAddress.country_id            := L_ent_loc.siscomex_id;
   destinationAddress.addr                  := L_addr;
   destinationAddress.addr_type             := L_addr;
   destinationAddress.primary_addr_type_ind := L_addr;
   destinationAddress.add_1                 := L_addr;
   destinationAddress.primary_addr_ind      := L_addr;
   destinationAddress.city                  := L_ent_loc.city_desc;
   ---- Tax Contributor's Details
   if L_ent_loc.ipi_ind = L_true then
      tax_contributor6.value := L_ipi;
   end if;
   ---
   if L_ent_loc.iss_ind = L_true then
      tax_contributor5.value := L_iss;
   end if;
   ---
   if L_ent_loc.icms_ind = L_true then
      tax_contributor7.value := L_icms;
   end if;
   ---
   tbl_tax_contrib_dest.DELETE();
   tax_cont:=0;
   if tax_contributor5.value=L_iss then
     tbl_tax_contrib_dest.extend();
     tax_cont:=tax_cont+1;
     tbl_tax_contrib_dest(tax_cont):=tax_contributor5;
   end if;
   if tax_contributor6.value=L_ipi then
     tbl_tax_contrib_dest.extend();
     tax_cont:=tax_cont+1;
     tbl_tax_contrib_dest(tax_cont):=tax_contributor6;
   end if;
   if tax_contributor7.value=L_icms then
     tbl_tax_contrib_dest.extend();
     tax_cont:=tax_cont+1;
     tbl_tax_contrib_dest(tax_cont):=tax_contributor7;
   end if;
   if tbl_tax_contrib_dest.count >0 then
      fiscalEntRBODest.taxContributor_tbl:=tbl_tax_contrib_dest;
      tbl_tax_contrib_dest.DELETE();
   end if;
   ---
   tbl_dest_addr.extend();
   tbl_dest_addr(1) := destinationAddress;
   fiscalEntRBODest.AddrRBO_TBL          := tbl_dest_addr;
   fiscalEntRBODest.is_rural_producer  := L_ent_loc.rural_prod_ind;
   tbl_fiscalEntRBODest.extend();
   tbl_fiscalEntRBODest(1) := fiscalEntRBODest;
   ---
   destinations.FiscEntityRBO_TBL           := tbl_fiscalEntRBODest;
   L_dest_cont    := L_dest_cont + 1;
   tbl_destinations.extend();
   tbl_destinations(L_dest_cont) := destinations;
   ---
   ---
   open C_GET_DOC_DETAIL;
   ---
   L_item_number := 1;
   LOOP
      ---
      L_tax_number  := 1;
      ---
      fetch C_GET_DOC_DETAIL into L_get_doc_detail;
      EXIT when C_GET_DOC_DETAIL%NOTFOUND;
      ---
      if L_get_doc_detail.pack_ind = 'Y' then
      ---
         if I_fiscal_doc_line_id is NOT NULL and L_get_doc_detail.fiscal_doc_line_id = I_fiscal_doc_line_id then
         ---
            L_po_cost    := I_cost;
            L_qty        := I_quantity;
            GP_line_pack  := TRUE;
         ---
         else
         ---
            L_po_cost  := L_get_doc_detail.unit_cost;
            L_qty      := L_get_doc_detail.quantity;
         ---
         end if;
         ---
         if L_triang_po_exists and L_get_doc_header.comp_nf_ind = 'Y' then
         ---
            open C_FM_OTHER_HEADER_DET(L_other_doc_id);
            ---
            FETCH C_FM_OTHER_HEADER_DET into L_fm_other_header_det;
            ---
            close C_FM_OTHER_HEADER_DET;
            ---
            open C_FM_OTHER_DETAIL_DET(L_other_doc_id, L_get_doc_detail.requisition_no, L_get_doc_detail.item);
            ---
            FETCH C_FM_OTHER_DETAIL_DET into L_fm_other_detail_det;
            ---
            close C_FM_OTHER_DETAIL_DET;
            ---
            open C_GET_ENTITY_ATTRIBUTES(L_fm_other_header_det.module, L_fm_other_header_det.key_value_1, L_fm_other_header_det.key_value_2);
            ---
            fetch C_GET_ENTITY_ATTRIBUTES into L_ent_main_nf_supp;
            ---
            if C_GET_ENTITY_ATTRIBUTES%NOTFOUND then
               ---
               O_status := 0;
               O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_SOURCE',TO_CHAR(L_fm_other_header_det.key_value_1),NULL,NULL);
               return FALSE;
            end if;
            ---
            close C_GET_ENTITY_ATTRIBUTES;
            ---
         end if;
         ---
         --triang
         if L_get_doc_header.requisition_type = L_rtv and L_get_doc_detail.fiscal_doc_id_ref is NOT NULL then
            open C_GET_COMP_ITEMS_RTV (L_get_doc_detail.fiscal_doc_line_id, L_po_cost, L_qty);
         else
            open C_GET_COMP_ITEMS (L_get_doc_detail.fiscal_doc_line_id, L_po_cost, L_qty);
         end if;
         ---
         LOOP
         ---
         if L_get_doc_header.requisition_type = L_rtv and L_get_doc_detail.fiscal_doc_id_ref is NOT NULL then
            fetch C_GET_COMP_ITEMS_RTV into L_get_comp_items;
            EXIT when C_GET_COMP_ITEMS_RTV%NOTFOUND;
         else
            fetch C_GET_COMP_ITEMS into L_get_comp_items;
            EXIT when C_GET_COMP_ITEMS%NOTFOUND;
         end if;
            ---
            documentLineItem        := "RIB_LineItemRBO_REC"(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
            documentLineItem.discount                 := (L_get_comp_items.unit_item_disc + L_get_comp_items.unit_header_disc) * L_get_comp_items.quantity;
            documentLineItem.freight                  := L_get_comp_items.unit_freight_cost * L_get_comp_items.quantity;
            documentLineItem.other_expenses           := L_get_comp_items.unit_other_exp_cost * L_get_comp_items.quantity;
            documentLineItem.insurance                := L_get_comp_items.unit_insurance_cost * L_get_comp_items.quantity;
            ---
            documentLineItem.item_type        := L_get_comp_items.classification;
          --  documentLineItem.document_line_id := L_get_doc_detail.fiscal_doc_line_id;

            documentLineItem.document_line_id := L_item_number;
            documentLineItem.pack_item_id := L_get_doc_detail.fiscal_doc_line_id  ;

            documentLineItem.item_id          := L_get_comp_items.item;
            ---
            open C_COMP_ITEM_CUSTOM_ATTRIB(L_get_comp_items.item);
            fetch C_COMP_ITEM_CUSTOM_ATTRIB bulk collect into L_item_name_value_tbl;
            close C_COMP_ITEM_CUSTOM_ATTRIB;
            if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                          L_item_name_value_tbl,
                                                          'ITEM') = FALSE then
               return FALSE;
            end if;
            if L_item_name_value_tbl.exists(1) then
               for i in  L_item_name_value_tbl.FIRST .. L_item_name_value_tbl.LAST loop
                  tbl_doc_comp_item_nameval.extend();
                        L_doc_comp_item_nameval := L_doc_comp_item_nameval + 1;
                              doc_comp_item_nameval.name := L_item_name_value_tbl(i).O_name;
                              doc_comp_item_nameval.value := L_item_name_value_tbl(i).O_value;
                              tbl_doc_comp_item_nameval(L_doc_comp_item_nameval) := doc_comp_item_nameval;
               end loop;
            end if;
            documentLineItem.NameValPairRBO_TBL      := tbl_doc_comp_item_nameval;
            ---
            documentLineItem.quantity         := L_get_comp_items.quantity;
            documentLineItem.unit_cost        := L_get_comp_items.unit_cost;
            documentLineItem.total_cost       := L_get_comp_items.quantity * L_get_comp_items.unit_cost;
            documentLineItem.unit_of_measure  := L_get_comp_items.standard_uom;
            ---
            documentLineItem.item_tran_code   := L_get_doc_detail.nf_cfop;
            ---
            documentLineItem.icms_cst_code       := L_get_doc_detail.icms_cst;
            ---
            if L10N_BR_INT_SQL.LOAD_UOM_INFO(O_error_message,
                                             L_tbl_documentLineItem,
                                             null,
                                             L_get_comp_items.item,
                                             L_get_comp_items.quantity,
                                             L_get_comp_items.standard_uom,
                                             L_get_doc_detail.requisition_no,
                                             'NF') = FALSE then
                return FALSE;
            end if;
           if L_tbl_documentLineItem is not NULL and L_tbl_documentLineItem.COUNT > 0 then
                          select "RIB_LineItemRBO_REC"(0,
                                           item_table.document_line_id,
                                           item_table.item_id,
                                           item_table.item_tran_Code,
                                           item_table.item_type,
                                           item_table.quantity,
                                           item_table.unit_of_measure,
                                           item_table.quantity_in_eaches,
                                           item_table.origin_doc_date,
                                           item_table.pack_item_id,
                                           item_table.total_cost,
                                           item_table.unit_cost,
                                           item_table.dim_object,
                                           item_table.src_taxpayer_type,
                                           item_table.orig_fiscal_doc_number,
                                           item_table.orig_fiscal_doc_series,
                                           item_table.length,
                                           item_table.width,
                                           item_table.lwh_uom,
                                           item_table.weight,
                                           item_table.net_weight,
                                           item_table.weight_uom,
                                           item_table.liquid_volume,
                                           item_table.liquid_volume_uom,
                                           item_table.freight,
                                           item_table.insurance,
                                           item_table.discount,
                                           item_table.commision,
                                           item_table.freight_type,
                                           item_table.other_expenses,
                                           item_table.origin_fiscal_code_opr,
                                           item_table.deduced_fiscal_code_opr,
                                           item_table.deduce_cfop_code,
                                           item_table.icms_cst_code,
                                           item_table.pis_cst_code,
                                           item_table.cofins_cst_code,
                                           item_table.deduce_icms_cst_code,
                                           item_table.deduce_pis_cst_code,
                                           item_table.deduce_cofins_cst_code,
                                           item_table.recoverable_icmsst,
                                           item_table.item_cost_contains_cofins,
                                           item_table.recoverable_base_icmsst,
                                           item_table.item_cost_contains_pis,
                                           item_table.item_cost_contains_icms,
                                           item_table.PrdItemRBO_TBL,
                                           item_table.SvcItemRBO_TBL,
                                           item_table.InformTaxRBO_TBL,
                                           item_table.NameValPairRBO_TBL)
                    into L_doclineItemRBO
                 from TABLE(L_tbl_documentLineItem) item_table;
                 --linedimensions := L_doclineItemRBO.iSCDimDesc;
            if (L_doclineItemRBO.item_id is not NULL) then
               documentLineItem.dim_object := L_doclineItemRBO.dim_object;
               documentLineItem.length := L_doclineItemRBO.length;
               documentLineItem.width := L_doclineItemRBO.width;
               documentLineItem.lwh_uom := L_doclineItemRBO.lwh_uom;
               documentLineItem.weight := L_doclineItemRBO.weight;
               documentLineItem.net_weight := L_doclineItemRBO.net_weight;
               documentLineItem.weight_uom := L_doclineItemRBO.weight_uom;
               documentLineItem.liquid_volume := L_doclineItemRBO.liquid_volume;
               documentLineItem.liquid_volume_uom := L_doclineItemRBO.liquid_volume_uom;
            end if;
                L_tbl_documentLineItem := null;
            end if;
            if L_get_doc_detail.icms_cst is NOT NULL then
            ---
               L_deduce_icms_cst := 'N';
            ---
            else
               L_deduce_icms_cst := 'Y';
            end if;
            ---
            documentLineItem.pis_cst_code        := L_get_doc_detail.pis_cst;
            ---
            if L_get_doc_detail.pis_cst is NOT NULL then
            ---
               L_deduce_pis_cst := 'N';
            ---
            else
               L_deduce_pis_cst := 'Y';
            end if;
            ---
            documentLineItem.cofins_cst_code     := L_get_doc_detail.cofins_cst;
            ---
            if L_get_doc_detail.cofins_cst is NOT NULL then
            ---
               L_deduce_cofins_cst := 'N';
            ---
            else
               L_deduce_cofins_cst := 'Y';
            end if;
            ---
            documentLineItem.deduce_icms_cst_code   := L_deduce_icms_cst;
            documentLineItem.deduce_pis_cst_code    := L_deduce_pis_cst;
            documentLineItem.deduce_cofins_cst_code := L_deduce_cofins_cst;
            ---
            if L_get_doc_header.requisition_type = 'TSF' and L_get_doc_header.operation_type = L_outbound then
                open C_GET_ORIG_CFOP_TSF (L_get_doc_header.location_type,L_get_doc_header.location_id,L_get_doc_detail.item);
                fetch C_GET_ORIG_CFOP_TSF into L_origin_fiscal_code;
                close C_GET_ORIG_CFOP_TSF;
                documentLineItem.origin_fiscal_code_opr   := L_origin_fiscal_code;
            end if;

            if L_triang_po_exists and L_get_doc_header.comp_nf_ind = 'Y' then
               documentLineItem.orig_fiscal_doc_number   := L_fm_other_header_det.fiscal_doc_no;
               documentLineItem.origin_fiscal_code_opr   := L_fm_other_detail_det.nf_cfop;
               documentLineItem.origin_doc_date          := L_fm_other_detail_det.issue_date;
               documentLineItem.orig_fiscal_doc_series   := L_fm_other_header_det.series_no;
               documentLineItem.src_taxpayer_type        := L_ent_main_nf_supp.cnpj;
            end if;
            ---
            --triang
            if L_get_doc_detail.service_ind = 'Y' then
               ---
               serviceItem                :=  "RIB_SvcItemRBO_REC"(0,null,null,null,null,null,null,null);
               ---
               serviceItem.item_code                  := L_get_comp_items.item;
               serviceItem.item_description           := L_get_comp_items.item_desc;
               serviceItem.federal_service_code       := L_get_comp_items.federal_service;
               serviceItem.service_provider_city      := L_get_comp_items.city;
               serviceItem.ext_fiscal_class_code      := L_get_comp_items.service_code;
               serviceItem.NameValPairRBO_TBL         := tbl_comp_item_nameval;
               ---
               tbl_serviceItem.extend();
               tbl_serviceItem(1) := serviceItem;
               documentLineItem.SvcItemRBO_TBL        := tbl_serviceItem;
               ---
            elsif L_get_doc_detail.service_ind = 'N' then
               --Product Details
               productItem             := "RIB_PrdItemRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null);
               ---
               productItem.item_code                   := L_get_comp_items.item;
               productItem.item_description            := L_get_comp_items.item_desc;
               productItem.fiscal_classification_code  := L_get_comp_items.ncm_char_code;
               productItem.ext_fiscal_class_code       := L_get_comp_items.ncm_ex_char_code;
               productItem.item_origin                 := L_get_comp_items.origin_code;
               productItem.item_utilization            := L_item_util;
               productItem.is_transformed_item         := L_transformed_item;
               productItem.state_of_manufacture        := L_get_comp_items.state_of_manufacture;
               productItem.pharma_list_type            := L_get_comp_items.pharma_list_type;
               productItem.NameValPairRBO_TBL          := tbl_comp_item_nameval;
               ---
               tbl_productItem.extend();
               tbl_productItem(1) := productItem;
               documentLineItem.PrdItemRBO_TBL         := tbl_productItem;
               ---
            end if;
            ---
            if L_get_doc_header.operation_type = L_inbound then
                FOR C_vat_code in L_vat_code_tab.first .. L_vat_code_tab.last LOOP
                   ---
                   if L_vat_code_tab(C_vat_code).vat_code = L_icms then
                      documentLineItem.item_cost_contains_icms := NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                   elsif L_vat_code_tab(C_vat_code).vat_code = L_cofins then
                      documentLineItem.item_cost_contains_cofins  := NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                   elsif L_vat_code_tab(C_vat_code).vat_code = L_pis then
                      documentLineItem.item_cost_contains_pis  :=  NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                   end if;
                   ---
                END LOOP;
                ---
             elsif L_get_doc_header.requisition_type = L_rtv and L_get_doc_detail.fiscal_doc_id_ref is NOT NULL then
                ---
                documentLineItem.item_cost_contains_cofins := 'Y';
                documentLineItem.item_cost_contains_pis    := 'Y';
                documentLineItem.item_cost_contains_icms   := 'Y';
                ---
             else
                documentLineItem.item_cost_contains_cofins := L_item_attr;
                documentLineItem.item_cost_contains_pis    := L_item_attr;
                documentLineItem.item_cost_contains_icms   := L_item_attr;
            end if;
            ---
            documentLineItem.deduce_cfop_code          := L_deduced_cfop;
            documentLineItem.recoverable_base_icmsst   := L_get_doc_detail.recoverable_base;
            documentLineItem.recoverable_icmsst        := L_get_doc_detail.recoverable_value;
            ---
            if I_fiscal_doc_line_id is NOT NULL and L_get_doc_detail.fiscal_doc_line_id = I_fiscal_doc_line_id then
            ---
               L_ignore_tax      := NULL;
               ---
               open C_GET_NF_COMP_QTY (L_get_comp_items.item);
               ---
               FETCH C_GET_NF_COMP_QTY into L_nf_qty;
               ---
               close C_GET_NF_COMP_QTY;
               ---
               open C_GET_TAX_RESL_NF;
               ---
               LOOP
               ---
                  fetch C_GET_TAX_RESL_NF into L_tax_code;
                  EXIT when C_GET_TAX_RESL_NF%NOTFOUND;
                  ---
                  taxdetailRBO      := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);
                  ---
                  L_ignore_tax      := L_ignore_tax || ',' || L_tax_code;
                  ---
                  open C_GET_COMP_ITEM_TAXES (L_get_doc_detail.fiscal_doc_line_id, L_get_comp_items.item, L_tax_code);
                  ---
                  fetch C_GET_COMP_ITEM_TAXES into L_get_comp_item_taxes;
                  ---
                  close C_GET_COMP_ITEM_TAXES;
                  ---
                  L_base     :=  NVL(L_get_comp_item_taxes.tax_basis,0);
                  L_modified_base :=  NVL(L_get_comp_item_taxes.modified_tax_basis,0);
                  L_tax_val  :=  NVL(L_get_comp_item_taxes.tax_value,0);
                  L_tax_rate :=  NVL(L_get_comp_item_taxes.tax_rate,0);
                  ---
                  L_po_cost_appr   := L_get_comp_items.unit_cost;
                  L_nf_qty_appr    := NVL(L_nf_qty,1);
                  ---
                  ---
                  L_base        := L_base * (L_po_cost_appr * L_get_comp_items.quantity) / (L_get_doc_detail.unit_cost * L_nf_qty_appr);
                  L_modified_base        := L_modified_base * (L_po_cost_appr * L_get_comp_items.quantity) / (L_get_doc_detail.unit_cost * L_nf_qty_appr);
                  L_tax_val     := L_tax_val * (L_po_cost_appr * L_get_comp_items.quantity) / (L_get_doc_detail.unit_cost * L_nf_qty_appr);
                  if L_base = 0 then
                     L_tax_rate    := 0;
                  else
                     L_tax_rate    := L_tax_val / L_base * 100;
                  end if;
                  ---
                  taxdetailRBO.tax_code                 := L_tax_code;
                  taxdetailRBO.tax_basis_amount         := L_base;
                  taxdetailRBO.modified_tax_basis_amount := L_modified_base;
                  taxdetailRBO.tax_amount               := L_tax_val;
                  taxdetailRBO.tax_rate                 := L_tax_rate;
                  taxdetailRBO.tax_exception_type       := 'T';   ---has to be chked
                  ---
                  tbl_taxdetail.extend();
                  tbl_taxdetail(L_tax_number)            := taxdetailRBO;
                  L_tax_number                           := L_tax_number + 1;
                  ---
               END LOOP;
               ---
               close C_GET_TAX_RESL_NF;
               ---
               documentLineItem.InformTaxRBO_TBL     := tbl_taxdetail;
               ---
            end if;
            ---
            tbl_documentLineItem.extend();
            tbl_documentLineItem(L_item_number)      := documentLineItem;
            L_item_number:=L_item_number + 1;
            ---
         ---
         END LOOP;
         ---
         if L_get_doc_header.requisition_type = L_rtv and L_get_doc_detail.fiscal_doc_id_ref is NOT NULL then
            close C_GET_COMP_ITEMS_RTV;
         else
            close C_GET_COMP_ITEMS;
         end if;
         ---
         ---
      ---
      else
          ---
          documentLineItem        := "RIB_LineItemRBO_REC"(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
          L_ignore_tax   := NULL;
          ---
          --strt new
          if L_triang_po_exists and L_get_doc_header.comp_nf_ind = 'Y' then
             ---
             open C_FM_OTHER_DETAIL_DET(L_other_doc_id, L_get_doc_detail.requisition_no, L_get_doc_detail.item);
             ---
             FETCH C_FM_OTHER_DETAIL_DET into L_fm_other_detail_det;
             ---
             close C_FM_OTHER_DETAIL_DET;
             ---
             open C_FM_OTHER_HEADER_DET(L_other_doc_id);
            ---
             FETCH C_FM_OTHER_HEADER_DET into L_fm_other_header_det;
            ---
             close C_FM_OTHER_HEADER_DET;
             ---
             open C_GET_ENTITY_ATTRIBUTES(L_fm_other_header_det.module, L_fm_other_header_det.key_value_1, L_fm_other_header_det.key_value_2);
             ---
             fetch C_GET_ENTITY_ATTRIBUTES into L_ent_main_nf_supp;
             ---
             if C_GET_ENTITY_ATTRIBUTES%NOTFOUND then
                O_status := 0;
                O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_SOURCE',TO_CHAR(L_fm_other_header_det.key_value_1),NULL,NULL);
                return FALSE;
             end if;
             ---
             close C_GET_ENTITY_ATTRIBUTES;
             ---
             documentLineItem.discount                 := L_fm_other_detail_det.total_disc;
             documentLineItem.freight                  := L_fm_other_detail_det.freight_cost;
             documentLineItem.other_expenses           := L_fm_other_detail_det.other_expenses_cost;
             documentLineItem.insurance                := L_fm_other_detail_det.insurance_cost;
             documentLineItem.origin_fiscal_code_opr      := L_fm_other_detail_det.nf_cfop;
             documentLineItem.origin_doc_date          := L_fm_other_detail_det.issue_date;
             documentLineItem.orig_fiscal_doc_number   := L_fm_other_header_det.fiscal_doc_no;
             documentLineItem.orig_fiscal_doc_series   := L_fm_other_header_det.series_no;
             documentLineItem.src_taxpayer_type     := L_ent_main_nf_supp.cnpj;
          ---
          else
          ---
             documentLineItem.discount                 := L_get_doc_detail.total_disc;
             documentLineItem.freight                  := L_get_doc_detail.freight_cost;
             documentLineItem.other_expenses           := L_get_doc_detail.other_expenses_cost;
             documentLineItem.insurance                := L_get_doc_detail.insurance_cost;
          ---
          end if;
          ---
          documentLineItem.item_type        := L_get_doc_detail.classification;
          documentLineItem.document_line_id := L_get_doc_detail.fiscal_doc_line_id;
          documentLineItem.item_id          := L_get_doc_detail.item;
          documentLineItem.unit_of_measure  := L_get_doc_detail.standard_uom;
          ---
          open C_DETAIL_ITEM_CUSTOM_ATTRIB(L_get_doc_detail.item);
          fetch C_DETAIL_ITEM_CUSTOM_ATTRIB bulk collect into L_item_name_value_tbl;
          close C_DETAIL_ITEM_CUSTOM_ATTRIB;
          if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                        L_item_name_value_tbl,
                                                        'ITEM') = FALSE then
             return FALSE;
          end if;
          if L_item_name_value_tbl.exists(1) then
             for i in  L_item_name_value_tbl.FIRST .. L_item_name_value_tbl.LAST loop
                tbl_doc_detail_item_nameval.extend();
                      L_doc_detail_item_nameval := L_doc_detail_item_nameval + 1;
                            doc_detail_item_nameval.name := L_item_name_value_tbl(i).O_name;
                            doc_detail_item_nameval.value := L_item_name_value_tbl(i).O_value;
                            tbl_doc_detail_item_nameval(L_doc_detail_item_nameval) := doc_detail_item_nameval;
             end loop;
          end if;
          documentLineItem.NameValPairRBO_TBL      := tbl_doc_detail_item_nameval;
            ---
          ---
      if I_fiscal_doc_line_id is NOT NULL and L_get_doc_detail.fiscal_doc_line_id = I_fiscal_doc_line_id then
         L_uom_qty := I_quantity;
      else
         L_uom_qty := L_get_doc_detail.quantity;
      end if;
         if L10N_BR_INT_SQL.LOAD_UOM_INFO(O_error_message,
                                    L_tbl_documentLineItem,
                                    null,
                                    L_get_doc_detail.item,
                                    L_uom_qty,
                                    L_get_doc_detail.standard_uom,
                                    L_get_doc_detail.requisition_no,
                                    'NF') = FALSE then
                 return FALSE;
           end if;
           if L_tbl_documentLineItem is not NULL and L_tbl_documentLineItem.COUNT > 0 then
                select "RIB_LineItemRBO_REC"(0,
                                             item_table.document_line_id,
                                             item_table.item_id,
                                             item_table.item_tran_Code,
                                             item_table.item_type,
                                             item_table.quantity,
                                             item_table.unit_of_measure,
                                             item_table.quantity_in_eaches,
                                             item_table.origin_doc_date,
                                             item_table.pack_item_id,
                                             item_table.total_cost,
                                             item_table.unit_cost,
                                             item_table.dim_object,
                                             item_table.src_taxpayer_type,
                                             item_table.orig_fiscal_doc_number,
                                             item_table.orig_fiscal_doc_series,
                                             item_table.length,
                                             item_table.width,
                                             item_table.lwh_uom,
                                             item_table.weight,
                                             item_table.net_weight,
                                             item_table.weight_uom,
                                             item_table.liquid_volume,
                                             item_table.liquid_volume_uom,
                                             item_table.freight,
                                             item_table.insurance,
                                             item_table.discount,
                                             item_table.commision,
                                             item_table.freight_type,
                                             item_table.other_expenses,
                                             item_table.origin_fiscal_code_opr,
                                             item_table.deduced_fiscal_code_opr,
                                             item_table.deduce_cfop_code,
                                             item_table.icms_cst_code,
                                             item_table.pis_cst_code,
                                             item_table.cofins_cst_code,
                                             item_table.deduce_icms_cst_code,
                                             item_table.deduce_pis_cst_code,
                                             item_table.deduce_cofins_cst_code,
                                             item_table.recoverable_icmsst,
                                             item_table.item_cost_contains_cofins,
                                             item_table.recoverable_base_icmsst,
                                             item_table.item_cost_contains_pis,
                                             item_table.item_cost_contains_icms,
                                             item_table.PrdItemRBO_TBL,
                                             item_table.SvcItemRBO_TBL,
                                             item_table.InformTaxRBO_TBL,
                                             item_table.NameValPairRBO_TBL)
                           into L_doclineItemRBO
                           from TABLE(L_tbl_documentLineItem) item_table;
                if (L_doclineItemRBO.item_id is not NULL) then
                   documentLineItem.dim_object := L_doclineItemRBO.dim_object;
                   documentLineItem.length := L_doclineItemRBO.length;
                   documentLineItem.width := L_doclineItemRBO.width;
                   documentLineItem.lwh_uom := L_doclineItemRBO.lwh_uom;
                   documentLineItem.weight := L_doclineItemRBO.weight;
                   documentLineItem.net_weight := L_doclineItemRBO.net_weight;
                   documentLineItem.weight_uom := L_doclineItemRBO.weight_uom;
                   documentLineItem.liquid_volume := L_doclineItemRBO.liquid_volume;
                   documentLineItem.liquid_volume_uom := L_doclineItemRBO.liquid_volume_uom;
                end if;
                L_tbl_documentLineItem := null;
            end if;
          --triang
          if I_fiscal_doc_line_id is NOT NULL and L_get_doc_detail.fiscal_doc_line_id = I_fiscal_doc_line_id then
             ---
             L_ignore_tax      := NULL;
             documentLineItem.discount                 := L_get_doc_detail.total_disc / L_get_doc_detail.quantity * I_quantity;
             documentLineItem.freight                  := L_get_doc_detail.freight_cost / L_get_doc_detail.quantity * I_quantity;
             documentLineItem.other_expenses           := L_get_doc_detail.other_expenses_cost / L_get_doc_detail.quantity * I_quantity;
             documentLineItem.insurance                := L_get_doc_detail.insurance_cost / L_get_doc_detail.quantity * I_quantity;
             ---
             documentLineItem.quantity         := I_quantity;
             documentLineItem.unit_cost        := I_cost;
             documentLineItem.total_cost       := I_quantity * I_cost;
             ---
             --for PIS/COFINS calc
             open C_GET_TAX_RESL_NF;
             ---
             LOOP
             ---
                fetch C_GET_TAX_RESL_NF into L_tax_code;
                EXIT when C_GET_TAX_RESL_NF%NOTFOUND;
                ---
                taxdetailRBO      := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);
                ---
                L_ignore_tax      := L_ignore_tax || ',' || L_tax_code;
                ---
                open C_GET_NF_TAX_DETAIL (L_get_doc_detail.fiscal_doc_line_id, L_tax_code);
                ---
                fetch C_GET_NF_TAX_DETAIL into L_get_nf_tax_detail;
                ---
                if C_GET_NF_TAX_DETAIL%NOTFOUND then
                ---
                   L_base     :=  0;
                   L_modified_base     :=  0;
                   L_tax_val  :=  0;
                   L_tax_rate :=  0;
                else
                ---
                   L_base     :=  NVL(L_get_nf_tax_detail.tax_basis,0);
                   L_modified_base     :=  NVL(L_get_nf_tax_detail.modified_tax_basis,0);
                   L_tax_val  :=  NVL(L_get_nf_tax_detail.tax_value,0);
                   L_tax_rate :=  NVL(L_get_nf_tax_detail.tax_rate,0);
                ---
                end if;
                ---
                close C_GET_NF_TAX_DETAIL;
                ---
                L_po_cost_appr     := I_cost;
                L_nf_qty_appr      := NVL(L_get_doc_detail.quantity,1);
                ---
                L_base        := L_base * (L_po_cost_appr * I_quantity) / (L_get_doc_detail.unit_cost * L_nf_qty_appr);
                L_modified_base        := L_modified_base * (L_po_cost_appr * I_quantity) / (L_get_doc_detail.unit_cost * L_nf_qty_appr);
                L_tax_val     := L_tax_val * (L_po_cost_appr * I_quantity) / (L_get_doc_detail.unit_cost * L_nf_qty_appr);
                ---
                if L_base = 0 then
                   if L_modified_base != 0 then
                      L_tax_rate    := L_tax_val / L_modified_base * 100;
                   else
                      L_tax_rate    := 0;
                   end if;
                else
                   L_tax_rate    := L_tax_val / L_base * 100;
                end if;
                ---
                taxdetailRBO.tax_code                 := L_tax_code;
                taxdetailRBO.tax_basis_amount         := L_base;
                taxdetailRBO.modified_tax_basis_amount         := L_modified_base;
                taxdetailRBO.tax_amount               := L_tax_val;
                taxdetailRBO.tax_rate                 := L_tax_rate;
                taxdetailRBO.tax_exception_type       := 'T';   ---has to be chked
                ---
                tbl_taxdetail.extend();
                tbl_taxdetail(L_tax_number)            := taxdetailRBO;
                L_tax_number                           := L_tax_number + 1;
                ---
             END LOOP;
             ---
             close C_GET_TAX_RESL_NF;
             ---
             documentLineItem.InformTaxRBO_TBL     := tbl_taxdetail;
             ---
          else
             ---
             documentLineItem.quantity         := L_get_doc_detail.quantity;
             documentLineItem.unit_cost        := L_get_doc_detail.unit_cost;
             documentLineItem.total_cost       := L_get_doc_detail.total_cost;
             ---
          end if;
          ---
          documentLineItem.item_tran_code   := L_get_doc_detail.nf_cfop;
          ---
          documentLineItem.icms_cst_code       := L_get_doc_detail.icms_cst;
          ---
          if L_get_doc_detail.icms_cst is NOT NULL then
          ---
             L_deduce_icms_cst := 'N';
          ---
          else
             L_deduce_icms_cst := 'Y';
          end if;
          ---
          documentLineItem.pis_cst_code        := L_get_doc_detail.pis_cst;
          ---
          if L_get_doc_detail.pis_cst is NOT NULL then
          ---
             L_deduce_pis_cst := 'N';
          ---
          else
             L_deduce_pis_cst := 'Y';
          end if;
          ---
          documentLineItem.cofins_cst_code     := L_get_doc_detail.cofins_cst;
          ---
          if L_get_doc_detail.cofins_cst is NOT NULL then
          ---
             L_deduce_cofins_cst := 'N';
          ---
          else
             L_deduce_cofins_cst := 'Y';
          end if;
          ---
          documentLineItem.deduce_icms_cst_code   := L_deduce_icms_cst;
          documentLineItem.deduce_pis_cst_code    := L_deduce_pis_cst;
          documentLineItem.deduce_cofins_cst_code := L_deduce_cofins_cst;
          ---
          if L_get_doc_detail.service_ind = 'Y' then
             ---
             serviceItem                :=  "RIB_SvcItemRBO_REC"(0,null,null,null,null,null,null,null);
             ---
             serviceItem.item_code                  := L_get_doc_detail.item;
             serviceItem.item_description           := L_get_doc_detail.item_desc;
             serviceItem.federal_service_code       := L_get_doc_detail.federal_service;
             serviceItem.service_provider_city      := L_get_doc_detail.city;
             serviceItem.ext_fiscal_class_code      := L_get_doc_detail.service_code;
             serviceItem.NameValPairRBO_TBL         := tbl_detail_item_nameval;
             ---
             tbl_serviceItem.extend();
             tbl_serviceItem(1) := serviceItem;
             documentLineItem.SvcItemRBO_TBL        := tbl_serviceItem;
             ---
          elsif L_get_doc_detail.service_ind = 'N' then
             --Product Details
             productItem             := "RIB_PrdItemRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null);
             ---
             productItem.item_code                   := L_get_doc_detail.item;
             productItem.item_description            := L_get_doc_detail.item_desc;
             productItem.fiscal_classification_code  := L_get_doc_detail.ncm_char_code;
             productItem.ext_fiscal_class_code       := L_get_doc_detail.ncm_ex_char_code;
             productItem.item_origin                 := L_get_doc_detail.origin_code;
             productItem.item_utilization            := L_item_util;
             productItem.is_transformed_item         := L_transformed_item;
             productItem.state_of_manufacture        := L_get_doc_detail.state_of_manufacture;
             productItem.pharma_list_type            := L_get_doc_detail.pharma_list_type;
             productItem.NameValPairRBO_TBL          := tbl_detail_item_nameval;
             ---
             tbl_productItem.extend();
             tbl_productItem(1) := productItem;
             documentLineItem.PrdItemRBO_TBL         := tbl_productItem;
             ---
          end if;
          ---
          if L_get_doc_header.requisition_type = 'TSF' and L_get_doc_header.operation_type = L_outbound then
             open C_GET_ORIG_CFOP_TSF (L_get_doc_header.location_type,L_get_doc_header.location_id,L_get_doc_detail.item);
             fetch C_GET_ORIG_CFOP_TSF into L_origin_fiscal_code;
             close C_GET_ORIG_CFOP_TSF;
             documentLineItem.origin_fiscal_code_opr   := L_origin_fiscal_code;
          end if;

          if L_get_doc_header.operation_type = L_inbound then
              FOR C_vat_code in L_vat_code_tab.first .. L_vat_code_tab.last LOOP
                 ---
                 if L_vat_code_tab(C_vat_code).vat_code = L_icms then
                    documentLineItem.item_cost_contains_icms := NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                 elsif L_vat_code_tab(C_vat_code).vat_code = L_cofins then
                    documentLineItem.item_cost_contains_cofins  := NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                 elsif L_vat_code_tab(C_vat_code).vat_code = L_pis then
                    documentLineItem.item_cost_contains_pis  :=  NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                 end if;
                 ---
              END LOOP;
              ---
          elsif L_get_doc_header.requisition_type = L_rtv and L_get_doc_detail.fiscal_doc_id_ref is NOT NULL then
                ---
                documentLineItem.item_cost_contains_cofins := 'Y';
                documentLineItem.item_cost_contains_pis    := 'Y';
                documentLineItem.item_cost_contains_icms   := 'Y';
                ---
          else
              documentLineItem.item_cost_contains_cofins := L_item_attr;
              documentLineItem.item_cost_contains_pis    := L_item_attr;
              documentLineItem.item_cost_contains_icms   := L_item_attr;
          end if;
          ---
          documentLineItem.deduce_cfop_code          := L_deduced_cfop;
          documentLineItem.recoverable_base_icmsst   := L_get_doc_detail.recoverable_base;
          documentLineItem.recoverable_icmsst        := L_get_doc_detail.recoverable_value;
          ---
          tbl_documentLineItem.extend();
          tbl_documentLineItem(L_item_number)      := documentLineItem;
          L_item_number:=L_item_number + 1;
          ---
      end if;
      ---
   END LOOP;
   ---
   close C_GET_DOC_DETAIL;
   ---
   ---
   fiscalDocumentRBO.fiscal_document_date        := L_get_doc_header.issue_date;
   fiscalDocumentRBO.due_date                    := NVL(L_get_doc_header.payment_date,sysdate);
   fiscalDocumentRBO.receipt_date                := L_get_doc_header.entry_or_exit_date;
   fiscalDocumentRBO.document_type               := L_doc_type;
   fiscalDocumentRBO.transaction_type            := L_transac_type;
   fiscalDocumentRBO.operation_type              := L_get_doc_header.operation_type;
   fiscalDocumentRBO.is_supplier_issuer          := L_supp_issuer;
   fiscalDocumentRBO.gross_weight                := L_get_doc_header.total_weight;
   fiscalDocumentRBO.net_weight                  := L_get_doc_header.net_weight;
   fiscalDocumentRBO.calc_process_type           := L_calc_process_type;
   if L_get_doc_header.operation_type = L_outbound then
      open C_GET_ST_IND;
     fetch C_GET_ST_IND into L_loc_st_ind,L_util_st_ind;
     close C_GET_ST_IND;
      if L_loc_st_ind = 'Y' and L_util_st_ind = 'Y' then
         L_no_rec_st := '3';
       --  L_no_rec_st := '1';
      end if;
   end if;
   fiscalDocumentRBO.no_history_tracked          := L_no_rec_st;
   ---
   if L_triang_po_exists and L_get_doc_header.comp_nf_ind = 'Y' then
   ---
      open C_FM_OTHER_HEADER_DET(L_other_doc_id);
      ---
      FETCH C_FM_OTHER_HEADER_DET into L_fm_other_header_det;
      ---
      close C_FM_OTHER_HEADER_DET;
      ---
      fiscalDocumentRBO.freight                  := L_fm_other_header_det.freight_cost;
      fiscalDocumentRBO.freight_type             := L_fm_other_header_det.freight_type;
      fiscalDocumentRBO.other_expenses           := L_fm_other_header_det.other_expenses_cost;
      fiscalDocumentRBO.insurance                := L_fm_other_header_det.insurance_cost;
      ---
   ---
   else
   ---
      fiscalDocumentRBO.freight                  := L_get_doc_header.freight_cost;
      fiscalDocumentRBO.freight_type             := L_get_doc_header.freight_type;
      fiscalDocumentRBO.other_expenses           := L_get_doc_header.other_expenses_cost;
      fiscalDocumentRBO.insurance                := L_get_doc_header.insurance_cost;
      ---
   ---
   end if;
   ---
   fiscalDocumentRBO.fromentity_tbl             := tbl_sources;
   fiscalDocumentRBO.toentity_tbl        := tbl_destinations;
   fiscalDocumentRBO.lineitemrbo_tbl      := tbl_documentLineItem;
   ---
   ---
   fiscalDocumentRBO.document_number        := L_get_doc_header.fiscal_doc_no;
   fiscalDocumentRBO.document_series        := L_get_doc_header.series_no;
   fiscalDocumentRBO.ignore_tax_calc_list   := SUBSTR(L_ignore_tax,2);
   fiscalDocumentRBO.nature_of_operation    := L_get_doc_header.nop;

   tbl_fiscalDocument_request.extend();
   tbl_fiscalDocument_request(1)            := fiscalDocumentRBO;
   ---
   fiscDocChnkRBM.chunk_id       := 1;
   fiscDocChnkRBM.fiscdocrbo_tbl := tbl_fiscalDocument_request;
   tbl_fiscDocChnkRBM.extend();
   tbl_fiscDocChnkRBM(1)         := fiscDocChnkRBM;
   ---
   fiscalDocumentColRBM.fiscDocChnkRBO_TBL  := tbl_fiscDocChnkRBM;
   ---
   O_businessObject := fiscalDocumentColRBM;
   ---
   L_vat_code_tab.delete;
   return TRUE;
   --
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                             'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id),
                                             TO_CHAR(SQLCODE));
      --
      return FALSE;
      ---
   when OTHERS then
      if C_GET_DOC_HEADER%ISOPEN then
         close C_GET_DOC_HEADER;
      end if;
      ---
      if C_GET_DOC_DETAIL%ISOPEN then
         close C_GET_DOC_DETAIL;
      end if;
      ---
      if C_GET_ENTITY_ATTRIBUTES%ISOPEN then
         close C_GET_ENTITY_ATTRIBUTES;
      end if;
      ---

      if C_GET_TRIANG_IND%ISOPEN then
         close C_GET_TRIANG_IND;
      end if;
      ---
      if C_FM_TRIANG_OTHER_DOC%ISOPEN then
         close C_FM_TRIANG_OTHER_DOC;
      end if;
      ---
      if C_FM_OTHER_HEADER_DET%ISOPEN then
         close C_FM_OTHER_HEADER_DET;
      end if;
      ---
      if C_FM_OTHER_DETAIL_DET%ISOPEN then
         close C_FM_OTHER_DETAIL_DET;
      end if;
      ---
      if C_GET_COMP_ITEMS%ISOPEN then
         close C_GET_COMP_ITEMS;
      end if;
      ---
      ---
      if C_GET_COMP_ITEMS_RTV%ISOPEN then
         close C_GET_COMP_ITEMS_RTV;
      end if;
      ---
      if C_GET_COMP_ITEM_TAXES%ISOPEN then
         close C_GET_COMP_ITEM_TAXES;
      end if;
      ---
      if C_GET_NF_TAX_DETAIL%ISOPEN then
         close C_GET_NF_TAX_DETAIL;
      end if;
      ---
      if C_GET_TAX_RESL_NF%ISOPEN then
         close C_GET_TAX_RESL_NF;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      O_status := 0;
      return FALSE;
END  LOAD_NF_OBJECT;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_RESULTS(O_line_tax             IN OUT    "RIB_TaxDetRBO_TBL",
                         O_error_message        IN OUT VARCHAR2,
                         O_status               IN OUT INTEGER,
                         I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                         I_quantity             IN     FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                         I_cost                 IN     FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                         I_form_ind             IN     VARCHAR2 ,
                         I_compensation_ind     IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   L_program                  VARCHAR2(62)     := 'FM_EXT_TAXES_SQL.PROCESS_RESULTS';
   fiscalDocTaxColRBM         "RIB_FiscDocTaxColRBM_REC"      := "RIB_FiscDocTaxColRBM_REC"(0,null,null,null,null,null);
   L_item_number              NUMBER         := 1;
   L_error                    VARCHAR2(1)     := 'E';
   tbl_documentLineItem       "RIB_LineItemTaxRBO_TBL"          := "RIB_LineItemTaxRBO_TBL"();
   L_fiscal_doc_id_ref        FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID_REF%TYPE;
   L_po_ref_ind               VARCHAR2(1)  := 'N';
   L_triang_ind               VARCHAR2(1)  := 'N';
   L_rnf                      VARCHAR2(3)  := 'RNF';
   L_rtv                   VARCHAR2(3)  := 'RTV';
   L_outbound              VARCHAR2(1)  := 'O';
   L_yes                   VARCHAR2(1) := 'Y';
   L_no                   VARCHAR2(1) := 'N';
   L_triang_ref_exists     BOOLEAN := FALSE;
   L_dummy                 VARCHAR2(1);
   L_nf_orig_qty           FM_FISCAL_DOC_DETAIL.Quantity%TYPE;
   L_st                    VARCHAR2(6)  := 'ST';
   L_icmsst_credit_val     NUMBER       := 0;
   L_icms_credit_val       NUMBER       := 0;
   L_icms                  VARCHAR2(6)  := 'ICMS';
   L_rma                   VARCHAR2(3)  := 'RMA';
   L_exit                  VARCHAR2(6) := 'EXIT';
   L_inbound               VARCHAR2(1)  := 'I';

   L_pack_ind              VARCHAR2(1)  := 'Y';
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_GET_DOC_HEADER is
      select decode(decode(nvl(fdh.schedule_no,-999),-999,'ENT',fs.mode_type),L_exit,L_outbound,L_inbound) operation_type,
             fdh.requisition_type,
             fua.comp_nf_ind
        from fm_schedule fs,
             fm_fiscal_utilization ffu,
             fm_utilization_attributes fua,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id    = I_fiscal_doc_id
         and fdh.schedule_no      = fs.schedule_no(+)
         and fdh.utilization_id   = ffu.utilization_id
         and ffu.utilization_id   = fua.utilization_id;
   ---
   cursor C_GET_PACK (P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select pack_ind
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_line_id = P_fiscal_doc_line_id;

   cursor C_GET_DOC_LINE_ID_REF (P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdd.fiscal_doc_id_ref
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id      = fdd.fiscal_doc_id
         and fdh.fiscal_doc_id      = I_fiscal_doc_id
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id;
   ---
   cursor C_GET_TRIANG_IND (P_fiscal_doc_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdd.fiscal_doc_id       = P_fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes;
   ---
   cursor C_GET_QTY (P_fiscal_doc_line_id        FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select quantity
        from fm_fiscal_doc_detail
       where fiscal_doc_line_id = P_fiscal_doc_line_id;
   ---
    cursor C_GET_TAX_DETAIL (P_fiscal_doc_line_id        FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                             P_tax_code                  FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE) is
      select fdtde.tax_basis, fdtde.modified_tax_basis, fdtde.tax_value, fdtde.tax_rate, fdtde.tax_val_mva, fdtde.tax_rec_value, fdtde.unit_tax_amt, fdtde.unit_rec_value, fdd.quantity, fdtde.unit_tax_amt_ext,fdtde.unit_tax_uom
        from fm_fiscal_doc_tax_detail_ext fdtde,
             fm_fiscal_doc_detail         fdd
       where fdtde.fiscal_doc_id         = fdd.fiscal_doc_id
         and fdtde.fiscal_doc_line_id    = fdd.fiscal_doc_line_id
         and fdtde.fiscal_doc_id         = I_fiscal_doc_id
         and fdtde.fiscal_doc_line_id    = P_fiscal_doc_line_id
         and fdtde.tax_code              = P_tax_code;
   ---
    cursor C_LOCK_FM_FISCAL_TAX_DETAIL (P_fiscal_doc_line_id    FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                                       P_tax_code              FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE) is
      select 'X'
        from fm_fiscal_doc_tax_detail_ext
       where fiscal_doc_id        = I_fiscal_doc_id
         and fiscal_doc_line_id   = P_fiscal_doc_line_id
         and tax_code             = P_tax_code
         for update nowait;
   ---
    cursor C_LOCK_FM_FISCAL_DETAIL (P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail
       where fiscal_doc_line_id = P_fiscal_doc_line_id
         for update nowait;
   ---
    cursor C_GET_TAX_RULE_INFO is
      select distinct(fdtr.fiscal_doc_line_id)
        from fm_fiscal_doc_tax_rule_ext fdtr,
             fm_fiscal_doc_detail fdd
       where fdtr.fiscal_doc_line_id    = fdd.fiscal_doc_line_id
         and fdd.fiscal_doc_id          = I_fiscal_doc_id;
   ---
     cursor C_LOCK_FM_FISCAL_HEADER is
      select 'X'
        from fm_fiscal_doc_header
       where fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
    cursor C_GET_HEADER_DISC is
      select sum (NVL(fdd.unit_header_disc,0) * fdd.quantity)
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = I_fiscal_doc_id
       group by fdd.fiscal_doc_id;
   ---
    cursor C_GET_OTHER_DOC_TAX (P_fiscal_doc_id          FM_FISCAL_DOC_TAX_HEAD_EXT.FISCAL_DOC_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_tax_head_ext fdthe
       where fdthe.fiscal_doc_id = P_fiscal_doc_id;
   ---
   cursor C_FM_TRIANG_OTHER_DOC (P_comp_nf_ind        FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE) is
      select fdc.fiscal_doc_id other_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and P_comp_nf_ind           = L_yes
         and fdc.fiscal_doc_id is NOT NULL
       UNION
      select fdc.compl_fiscal_doc_id other_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.fiscal_doc_id       = I_fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and P_comp_nf_ind           = L_no
         and fdc.fiscal_doc_id is NOT NULL;
   ---
   cursor C_FM_TRIANG_OTHER_DOC_LINE (P_comp_nf_ind        FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE) is
       select fdd.fiscal_doc_id other_doc_id,
                 fdd.fiscal_doc_line_id other_doc_line_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_detail fddc,
             ordhead ord
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.fiscal_doc_id = fddc.fiscal_doc_id_ref
         and fddc.fiscal_doc_line_id = I_fiscal_doc_line_id
         and fdd.requisition_no      = fddc.requisition_no
         and fdd.item                = fddc.item
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and P_comp_nf_ind           = L_yes
         and fdc.fiscal_doc_id is NOT NULL
       UNION
      select fdd.fiscal_doc_id other_doc_id,
                 fdd.fiscal_doc_line_id other_doc_line_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.fiscal_doc_id       = I_fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.fiscal_doc_line_id_ref = I_fiscal_doc_line_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and P_comp_nf_ind           = L_no
         and fdc.fiscal_doc_id is NOT NULL;
   ---
   cursor C_GET_RESULTS_ITEM is
       select fiscal_doc_line_id,
                    item,
                    taxed_quantity,
                    taxed_quantity_uom,
                    trim(',' from substr(entry_cfop,1,instr(entry_cfop,',')-1)) entry_cfop,
                    trim(',' from substr(entry_cfop,instr(entry_cfop,',')+1)) nf_cfop,
                    icms_cst_code,
              pis_cst_code,
              cofins_cst_code,
                    icms_cst_ind,
                    pis_cst_ind,
                    cofins_cst_ind,
                    recoverable_icmsst,
                    total_cost_with_icms,
                    unit_cost_with_icms,
                    recoverable_base_icmsst
         from fm_tax_call_dtl_res
              where fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_RESULTS_ITEM_TAX(P_item               FM_TAX_CALL_RES_DTL_TAX.ITEM%TYPE,
                                 P_fiscal_doc_line_id FM_TAX_CALL_RES_DTL_TAX.FISCAL_DOC_LINE_ID%TYPE) is
       select fiscal_doc_line_id,
              tax_code,
              tax_rate,
              tax_basis_amount,
              modified_tax_basis_amount,
              tax_amount,
              legal_message,
              tax_rate_type,
              reg_pct_margin_value,
              regulated_price,
              recoverable_amt,
              recovered_amt,
              unit_tax_uom,
              unit_tax_amount
         from fm_tax_call_res_dtl_tax
        where fiscal_doc_id = I_fiscal_doc_id
          and item = P_item
          and fiscal_doc_line_id = P_fiscal_doc_line_id;
    ---
    cursor C_GET_RESULTS_HDR(P_fiscal_doc_id FM_TAX_CALL_HDR_RES.FISCAL_DOC_ID%TYPE) is
       select total_merch_cost,
                    total_cost,
                    total_services_cost,
                    calculation_status
         from fm_tax_call_hdr_res
              where fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_FM_FISCAL_TAX_DETAIL  is
      select 1
        from fm_fiscal_doc_tax_detail_ext
       where fiscal_doc_id        = I_fiscal_doc_id;
   ---
   L_get_doc_header           C_GET_DOC_HEADER%ROWTYPE;
   L_get_tax_detail           C_GET_TAX_DETAIL%ROWTYPE;
   L_get_results_hdr          C_GET_RESULTS_HDR%ROWTYPE;
   L_pack_tax_rate            FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_RATE%TYPE;
   L_fiscal_doc_line_id       FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_header_disc              FM_FISCAL_DOC_DETAIL.UNIT_HEADER_DISC%TYPE;
   L_triang_po_exists         BOOLEAN := FALSE;
   L_other_doc_id             FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_other_doc_tax            BOOLEAN := FALSE;
   L_error_message            VARCHAR2(255) := NULL;
   L_pack_tax_tab             "RIB_TaxDetRBO_TBL" :=  "RIB_TaxDetRBO_TBL"();
   L_tax_obj                  "RIB_TaxDetRBO_REC"     := "RIB_TaxDetRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);
   L_pack_tax                 "RIB_TaxDetRBO_REC"     := "RIB_TaxDetRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);
   L_other_doc_line_id        FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_tax_code_exists          BOOLEAN;
   L_pack_tax_ind             NUMBER               := 0;
   L_cnt                      NUMBER               := 0;
   L_tax_call_true            BOOLEAN              := FALSE;
BEGIN
---
   open C_GET_DOC_HEADER;
   ---
   FETCH C_GET_DOC_HEADER into L_get_doc_header;
   close C_GET_DOC_HEADER;
   open C_GET_HEADER_DISC;
   ---
   fetch C_GET_HEADER_DISC into L_header_disc;
   ---
   close C_GET_HEADER_DISC;
   open C_GET_TRIANG_IND(I_fiscal_doc_id);
   ---
   fetch C_GET_TRIANG_IND into L_dummy;
   L_triang_po_exists := C_GET_TRIANG_IND%FOUND;
   ---
   close C_GET_TRIANG_IND;
   if L_triang_po_exists then
   ---
      open C_FM_TRIANG_OTHER_DOC (L_get_doc_header.comp_nf_ind);
      ---
      fetch C_FM_TRIANG_OTHER_DOC into L_other_doc_id;
      ---
      close C_FM_TRIANG_OTHER_DOC;
      ---
   ---
   end if;
   if I_fiscal_doc_line_id is NULL then
      ---
         ---for RNF
         if L_get_doc_header.requisition_type not in (L_rnf,L_rtv) then
         ---
                 insert into FM_FISCAL_DOC_TAX_HEAD_EXT (FISCAL_DOC_ID,
                                                         TAX_CODE,
                                                         TAX_BASIS,
                                                         MODIFIED_TAX_BASIS,
                                                         TAX_VALUE,
                                                         CREATE_DATETIME,
                                                         CREATE_ID,
                                                         LAST_UPDATE_DATETIME,
                                                         LAST_UPDATE_ID)
                                                 select  fiscal_doc_id,
                                                         tax_code,
                                                         tax_basis_amount,
                                                         modified_tax_basis_amount,
                                                         tax_amount,
                                                         sysdate,
                                                         user,
                                                         sysdate,
                                                         user
                                                    from FM_TAX_CALL_RES_HDR_TAX where fiscal_doc_id=I_fiscal_doc_id;
         ---
         end if;
         ---end RNF
         ---
         FOR lineitem in C_GET_RESULTS_ITEM
         LOOP
            ---
             L_pack_tax_tab.delete();
         ---
             --GP_tax_tab  :=  "RIB_TaxDetRBO_TBL"();
             if L_get_doc_header.requisition_type = L_rtv then
                  open C_GET_DOC_LINE_ID_REF(lineitem.fiscal_doc_line_id);
                  ---
                  fetch C_GET_DOC_LINE_ID_REF into L_fiscal_doc_id_ref;
                  ---
                  close C_GET_DOC_LINE_ID_REF;
                  ---


                  if L_fiscal_doc_id_ref is not null then
                     open C_GET_TRIANG_IND(L_fiscal_doc_id_ref);
                     ---
                     fetch C_GET_TRIANG_IND into L_dummy;
                     L_triang_ref_exists := C_GET_TRIANG_IND%FOUND;
                     ---
                     close C_GET_TRIANG_IND;
                     ---
                     L_po_ref_ind := 'Y';
                     if not L_triang_ref_exists then
                        L_triang_ind := 'N';
                     else
                        L_triang_ind := 'Y';
                     end if;
                  end if;
               end if;
               ---
            if L_get_doc_header.requisition_type <> L_rnf and L_po_ref_ind ='N' then
            ---
               FOR C_TAX_DETAIL IN C_GET_RESULTS_ITEM_TAX(lineitem.item,lineitem.fiscal_doc_line_id)
               LOOP
               ---
                  open C_GET_QTY(lineitem.fiscal_doc_line_id);
               ---
                  fetch C_GET_QTY into L_nf_orig_qty;
               ---
                  close C_GET_QTY;
                  if  C_TAX_DETAIL.tax_amount is not null then
                                ---
                                   --new pck
                     open C_GET_TAX_DETAIL (lineitem.fiscal_doc_line_id, C_TAX_DETAIL.tax_code);
                    ---
                     fetch C_GET_TAX_DETAIL into L_get_tax_detail;
                    ---
                     if C_GET_TAX_DETAIL%NOTFOUND then
                    ---L_st
                        if C_TAX_DETAIL.tax_code = L_st then
                           L_icmsst_credit_val    := NVL(C_TAX_DETAIL.recovered_amt,0);
                        elsif C_TAX_DETAIL.tax_code = L_icms then
                           L_icms_credit_val      := NVL(C_TAX_DETAIL.recovered_amt,0);
                        end if;
                       ---
                        insert into FM_FISCAL_DOC_TAX_DETAIL_EXT (FISCAL_DOC_ID,
                                                             FISCAL_DOC_LINE_ID,
                                                             TAX_CODE,
                                                             TAX_RATE,
                                                             TAX_BASIS,
                                                             MODIFIED_TAX_BASIS,
                                                             TAX_VALUE,
                                                             UNIT_TAX_AMT,
                                                             LEGAL_MSG,
                                                             TAX_TYPE,
                                                             TAX_PERC_MVA,
                                                             TAX_VAL_MVA,
                                                             TAX_REC_VALUE,
                                                             UNIT_REC_VALUE,
                                                                                                       UNIT_TAX_AMT_EXT,
                                                             UNIT_TAX_UOM,
                                                                               TAX_RATE_TYPE,
                                                             CREATE_DATETIME,
                                                             CREATE_ID,
                                                             LAST_UPDATE_DATETIME,
                                                             LAST_UPDATE_ID)
                                                      values
                                                            (I_fiscal_doc_id,
                                                             lineitem.fiscal_doc_line_id,
                                                             C_TAX_DETAIL.tax_code,
                                                             C_TAX_DETAIL.tax_rate,
                                                             C_TAX_DETAIL.tax_basis_amount,
                                                             C_TAX_DETAIL.modified_tax_basis_amount,
                                                             C_TAX_DETAIL.tax_amount,
                                                             C_TAX_DETAIL.tax_amount / L_nf_orig_qty,
                                                             C_TAX_DETAIL.legal_message,
                                                             --'P',
                                                             C_TAX_DETAIL.tax_rate_type,
                                                             C_TAX_DETAIL.reg_pct_margin_value,
                                                             C_TAX_DETAIL.regulated_price,
                                                             C_TAX_DETAIL.recoverable_amt,
                                                             C_TAX_DETAIL.recoverable_amt / L_nf_orig_qty,
                                                             C_TAX_DETAIL.unit_tax_amount,
                                                             C_TAX_DETAIL.unit_tax_uom,
                                                             C_TAX_DETAIL.tax_rate_type,
                                                             sysdate,
                                                             user,
                                                             sysdate,
                                                             user
                                                             );
                    ---
                     else
                    ---
                        if C_TAX_DETAIL.tax_code = L_st then
                           L_icmsst_credit_val    := L_icmsst_credit_val + NVL(C_TAX_DETAIL.recovered_amt,0);
                        elsif C_TAX_DETAIL.tax_code = L_icms then
                           L_icms_credit_val      := L_icms_credit_val + NVL(C_TAX_DETAIL.recovered_amt,0);
                        end if;
                       ---
                       open C_LOCK_FM_FISCAL_TAX_DETAIL (lineitem.fiscal_doc_line_id, C_TAX_DETAIL.tax_code);
                       close C_LOCK_FM_FISCAL_TAX_DETAIL;
                       ---
                       if (L_get_tax_detail.tax_basis + C_TAX_DETAIL.tax_basis_amount) = 0 then
                          if (L_get_tax_detail.modified_tax_basis + C_TAX_DETAIL.modified_tax_basis_amount) = 0 then
                          ---
                             L_pack_tax_rate := 0;
                          else
                             L_pack_tax_rate := (((L_get_tax_detail.tax_value + C_TAX_DETAIL.tax_amount) / (L_get_tax_detail.modified_tax_basis + C_TAX_DETAIL.modified_tax_basis_amount)) * 100);
                          end if;
                       else
                          L_pack_tax_rate := (((L_get_tax_detail.tax_value + C_TAX_DETAIL.tax_amount) / (L_get_tax_detail.tax_basis + C_TAX_DETAIL.tax_basis_amount)) * 100);
                          ---
                       end if;
                       ---
                       ---
                       update fm_fiscal_doc_tax_detail_ext
                          set tax_basis          = (L_get_tax_detail.tax_basis + C_TAX_DETAIL.tax_basis_amount),
                              modified_tax_basis       = (L_get_tax_detail.modified_tax_basis + C_TAX_DETAIL.modified_tax_basis_amount),
                              tax_value          = (L_get_tax_detail.tax_value + C_TAX_DETAIL.tax_amount),
                              tax_rate           = L_pack_tax_rate,
                              unit_tax_amt       = ((L_get_tax_detail.tax_value + C_TAX_DETAIL.tax_amount) / L_get_tax_detail.quantity),
                              tax_val_mva        = (L_get_tax_detail.tax_val_mva + C_TAX_DETAIL.regulated_price),
                              tax_rec_value      = (L_get_tax_detail.tax_rec_value + C_TAX_DETAIL.recoverable_amt),
                              unit_rec_value     = ((L_get_tax_detail.tax_rec_value + C_TAX_DETAIL.recoverable_amt) / L_get_tax_detail.quantity),
                                                unit_tax_amt_ext   = (L_get_tax_detail.unit_tax_amt_ext+ C_TAX_DETAIL.unit_tax_amount),
                                          unit_tax_uom       = C_TAX_DETAIL.unit_tax_uom,
                                          tax_rate_type      =  C_TAX_DETAIL.tax_rate_type
                        where fiscal_doc_id      = I_fiscal_doc_id
                          and fiscal_doc_line_id = lineitem.fiscal_doc_line_id
                          and tax_code           = C_TAX_DETAIL.tax_code;
                       ---
                    ---
                    end if;
                    ---
                    close C_GET_TAX_DETAIL;
                    ---
                 end if;
                 ---
               END LOOP;
                ---

                 ---update fm_fiscal_doc_detail.entry_cfop
                 open C_LOCK_FM_FISCAL_DETAIL (lineitem.fiscal_doc_line_id);
                 close C_LOCK_FM_FISCAL_DETAIL;
                 ---
                 ---
                 update fm_fiscal_doc_detail fdd
                    set fdd.entry_cfop          = lineitem.entry_cfop,
                        fdd.icms_cst            = decode(lineitem.icms_cst_ind, 'Y', lineitem.icms_cst_code, 'N', fdd.icms_cst),     --substr(lineitem.lineItemBRAttr_.icms_cst_code_,2),
                        fdd.pis_cst             = decode(lineitem.pis_cst_ind, 'Y', lineitem.pis_cst_code, 'N', fdd.pis_cst),      --substr(lineitem.lineItemBRAttr_.pis_cst_code_,2),
                        fdd.cofins_cst          = decode(lineitem.cofins_cst_ind, 'Y', lineitem.cofins_cst_code, 'N', fdd.cofins_cst),   --substr(lineitem.lineItemBRAttr_.cofins_cst_code_,2),
                        fdd.icms_credit_val     = NVL(L_icms_credit_val,0),
                        fdd.icmsst_credit_val   = NVL(L_icmsst_credit_val,0),
                        fdd.fiscal_qty          = lineitem.taxed_Quantity,
                      fdd.fiscal_uom            = lineitem.taxed_Quantity_uom
                  where fdd.fiscal_doc_id       = I_fiscal_doc_id
                    and fdd.fiscal_doc_line_id  = lineitem.fiscal_doc_line_id;
                 if (L_get_doc_header.operation_type = L_outbound and L_po_ref_ind = 'N' and L_triang_ind = 'N')  or L_get_doc_header.requisition_type = L_rma then
                    ---
                    ---update fm_fiscal_doc_header.entry_cfop
                    open C_LOCK_FM_FISCAL_DETAIL (lineitem.fiscal_doc_line_id);
                    close C_LOCK_FM_FISCAL_DETAIL;
                    ---
                    ---
                     update fm_fiscal_doc_detail fdd
                        set fdd.unit_cost          = lineitem.unit_cost_with_icms--lineitem.unit_cost_ (lineitem.lineItemBRAttr_.total_cost_with_icms_)/fdd.quantity
                           ,fdd.unit_cost_with_disc= lineitem.unit_cost_with_icms--lineitem.unit_cost_ (lineitem.lineItemBRAttr_.total_cost_with_icms_)/fdd.quantity
                           ,fdd.total_cost         = lineitem.total_cost_with_icms--lineitem.total_cost_
                           ,fdd.total_calc_cost    = lineitem.total_cost_with_icms--lineitem.total_cost_
                           ,fdd.nf_cfop            = lineitem.nf_cfop
                           ,fdd.recoverable_base   = decode(L_get_doc_header.operation_type, L_outbound, lineitem.recoverable_base_icmsst, null)
                           ,fdd.recoverable_value  = decode(L_get_doc_header.operation_type, L_outbound, lineitem.recoverable_icmsst, null)
                      where fdd.fiscal_doc_id      = I_fiscal_doc_id
                        and fdd.fiscal_doc_line_id = lineitem.fiscal_doc_line_id;
                     ---
                 end if;
                ---
               else
               ---for rtv
                   ---update fm_fiscal_doc_header.entry_cfop
                    open C_LOCK_FM_FISCAL_DETAIL (lineitem.fiscal_doc_line_id);
                    close C_LOCK_FM_FISCAL_DETAIL;
                    ---
                    ---
                     update fm_fiscal_doc_detail fdd
                        set fdd.nf_cfop            = lineitem.nf_cfop,
                            fdd.entry_cfop         = lineitem.entry_cfop,
                            fdd.icms_cst           = decode(lineitem.icms_cst_ind, 'Y', lineitem.icms_cst_code, 'N', fdd.icms_cst),     --substr(lineitem.lineItemBRAttr_.icms_cst_code_,2),
                            fdd.pis_cst            = decode(lineitem.pis_cst_ind, 'Y', lineitem.pis_cst_code, 'N', fdd.pis_cst),      --substr(lineitem.lineItemBRAttr_.pis_cst_code_,2),
                            fdd.cofins_cst         = decode(lineitem.cofins_cst_ind, 'Y', lineitem.cofins_cst_code, 'N', fdd.cofins_cst)   --substr(lineitem.lineItemBRAttr_.cofins_cst_code_,2),
                      where fdd.fiscal_doc_id      = I_fiscal_doc_id
                        and fdd.fiscal_doc_line_id = lineitem.fiscal_doc_line_id;
                    ---
                 ---
               end if;
               ---
               L_fiscal_doc_id_ref := NULL;
               L_po_ref_ind        := 'N';
               L_triang_ind        := 'N';
            --END LOOP;
            ---
            if L_get_doc_header.requisition_type <> L_rnf then
            ---
               insert into FM_FISCAL_DOC_TAX_RULE_EXT (FISCAL_DOC_LINE_ID,
                                                          TAX_RULE_ID,
                                                          TAX_RULE_DESC,
                                                          TAX_RULE_CODE,
                                                          STATUS,
                                                          CREATE_DATETIME,
                                                          CREATE_ID,
                                                          LAST_UPDATE_DATETIME,
                                                          LAST_UPDATE_ID)
                                                  select  fiscal_doc_line_id,
                                                          tax_rule_id,
                                                          tax_rule_description,
                                                          tax_rule_code,
                                                          NVL(status, 'P'),
                                                          sysdate,
                                                          user,
                                                          sysdate,
                                                          user
                                                    from  fm_tax_call_res_rules where fiscal_doc_id=I_fiscal_doc_id;
               ---
               open C_GET_TAX_RULE_INFO;
               ---
               LOOP
               ---
                  fetch C_GET_TAX_RULE_INFO into L_fiscal_doc_line_id;
                  ---
                  EXIT when C_GET_TAX_RULE_INFO%NOTFOUND;
                  ---
                  open C_LOCK_FM_FISCAL_DETAIL (L_fiscal_doc_line_id);
                  close C_LOCK_FM_FISCAL_DETAIL;
                  ---
                  ---
                  update fm_fiscal_doc_detail fdd
                     set fdd.inconcl_rule_ind   = 'Y'
                   where fdd.fiscal_doc_id      = I_fiscal_doc_id
                     and fdd.fiscal_doc_line_id = L_fiscal_doc_line_id;
                  ---
               END LOOP;
               ---
               close C_GET_TAX_RULE_INFO;
               ---
            end if;
            ---
         END LOOP;
         ---
             open C_GET_FM_FISCAL_TAX_DETAIL;
                  fetch C_GET_FM_FISCAL_TAX_DETAIL into L_cnt;
                  close C_GET_FM_FISCAL_TAX_DETAIL;
                  ---
                if L_cnt > 0 and L_get_doc_header.requisition_type = L_rtv then
                       insert into FM_FISCAL_DOC_TAX_HEAD_EXT (FISCAL_DOC_ID,
                                                            TAX_CODE,
                                                            TAX_BASIS,
                                                            MODIFIED_TAX_BASIS,
                                                            TAX_VALUE,
                                                            CREATE_DATETIME,
                                                            CREATE_ID,
                                                            LAST_UPDATE_DATETIME,
                                                            LAST_UPDATE_ID)
                                                               select fiscal_doc_id,
                                                                          tax_code,
                                                                          sum(tax_basis),
                                                            sum(modified_tax_basis),
                                                                          sum(tax_value),
                                                                          sysdate,
                                                                          user,
                                                                          sysdate,
                                                                          user
                                                       from FM_FISCAL_DOC_TAX_DETAIL_EXT
                                                                 where fiscal_doc_id = I_fiscal_doc_id
                                                                 group by tax_code,fiscal_doc_id ;
                 end if;

         open C_GET_RESULTS_HDR (I_fiscal_doc_id);
         fetch C_GET_RESULTS_HDR into L_get_results_hdr;
         close C_GET_RESULTS_HDR;
         if L_get_doc_header.operation_type <> L_outbound then
           ---
           open C_LOCK_FM_FISCAL_HEADER;
           close C_LOCK_FM_FISCAL_HEADER;
           ---
           update fm_fiscal_doc_header fdh
              set fdh.total_doc_calc_value   = L_get_results_hdr.total_cost,
                  fdh.total_item_calc_value  = (L_get_results_hdr.total_merch_cost + L_header_disc),
                  fdh.total_serv_calc_value  = L_get_results_hdr.total_services_cost,
                  fdh.entry_cfop             = (select entry_cfop from (select entry_cfop,count(1) total_cnt
                                                                          from fm_fiscal_doc_detail
                                                                         where fiscal_doc_id = I_fiscal_doc_id
                                                                         group by entry_cfop
                                                                         order by total_cnt desc)
                                                 where rownum=1),
                  fdh.last_update_datetime   = SYSDATE,
                  fdh.last_update_id         = USER
            where fdh.fiscal_doc_id          = I_fiscal_doc_id;
         ---
         else
         ---
           open C_LOCK_FM_FISCAL_HEADER;
           close C_LOCK_FM_FISCAL_HEADER;
           ---
           ---
           update fm_fiscal_doc_header fdh
              set fdh.total_doc_calc_value   = L_get_results_hdr.total_cost,
                  fdh.total_item_calc_value  = (L_get_results_hdr.total_merch_cost + L_header_disc),
                  fdh.total_serv_calc_value  = L_get_results_hdr.total_services_cost,
                  fdh.entry_cfop             =  (select entry_cfop from (select entry_cfop,count(1) total_cnt
                                                                          from fm_fiscal_doc_detail
                                                                         where fiscal_doc_id = I_fiscal_doc_id
                                                                         group by entry_cfop
                                                                         order by total_cnt desc)
                                                 where rownum=1),
                  fdh.nf_cfop                =  (select nf_cfop from (select nf_cfop,count(1) total_cnt
                                                                          from fm_fiscal_doc_detail
                                                                         where fiscal_doc_id = I_fiscal_doc_id
                                                                         group by nf_cfop
                                                                         order by total_cnt desc)
                                                 where rownum=1),
                  fdh.last_update_datetime   = SYSDATE,
                  fdh.last_update_id         = USER
            where fdh.fiscal_doc_id          = I_fiscal_doc_id;
         ---
         end if;
      ---
  --    END LOOP;
      ---
      O_line_tax := NULL;
      ---
      if L_get_doc_header.requisition_type <> L_rnf then
      ---
         if L_triang_po_exists then
         ---
            open C_GET_OTHER_DOC_TAX (L_other_doc_id);
            ---
            fetch C_GET_OTHER_DOC_TAX into L_dummy;
            L_other_doc_tax := C_GET_OTHER_DOC_TAX%FOUND;
            ---
            close C_GET_OTHER_DOC_TAX;
            ---
            open C_GET_RESULTS_HDR (L_other_doc_id);
            fetch C_GET_RESULTS_HDR into L_get_results_hdr;
            L_tax_call_true := C_GET_RESULTS_HDR%FOUND;
            close C_GET_RESULTS_HDR;
            if not L_other_doc_tax and not L_tax_call_true then
            ---
               if (FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(O_line_tax, O_error_message, O_status, L_other_doc_id, null, null, null)= FALSE) then
                  return FALSE;
               end if;
            ---
            end if;
         ---
         end if;
         ---
      end if;
      ---
   else
      ---
         L_item_number    := 1;
         ---
           open C_GET_PACK(I_fiscal_doc_line_id);
                  ---
          fetch C_GET_PACK into L_pack_ind;
          ---
          close C_GET_PACK;

           if L_pack_ind = 'Y' then
              GP_line_pack := TRUE;
           end if;

         L_pack_tax_tab.delete();
         ---
         --GP_tax_tab  :=  "RIB_TaxDetRBO_TBL"();
         FOR lineitem in C_GET_RESULTS_ITEM
         LOOP
            ---
            if lineitem.fiscal_Doc_line_id = I_fiscal_doc_line_id and not GP_line_pack then
               ---
              FOR C_TAX_DETAIL IN C_GET_RESULTS_ITEM_TAX(lineitem.item,lineitem.fiscal_doc_line_id)
               LOOP
                  if  C_TAX_DETAIL.tax_amount is not null then
                     ---
                     L_tax_obj                    := "RIB_TaxDetRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);
                     L_tax_obj.tax_code          :=  C_TAX_DETAIL.tax_code;
                     L_tax_obj.tax_basis_amount  :=  C_TAX_DETAIL.tax_basis_amount;
                     L_tax_obj.modified_tax_basis_amount  :=  C_TAX_DETAIL.modified_tax_basis_amount;
                     L_tax_obj.tax_amount        :=  C_TAX_DETAIL.tax_amount;
                     if (C_TAX_DETAIL.tax_basis_amount = 0 and C_TAX_DETAIL.modified_tax_basis_amount = 0) then
                     ---
                        L_tax_obj.tax_rate          := 0;
                     elsif (C_TAX_DETAIL.tax_basis_amount is null and C_TAX_DETAIL.modified_tax_basis_amount is null) then
                        L_tax_obj.tax_rate          :=  C_TAX_DETAIL.tax_rate;
                     else
                        if C_TAX_DETAIL.tax_basis_amount is not null then
                           L_tax_obj.tax_rate          :=  (L_tax_obj.tax_amount * 100) / NVL(L_tax_obj.tax_basis_amount,1);
                         else
                            L_tax_obj.tax_rate         :=  (L_tax_obj.tax_amount * 100) / NVL(L_tax_obj.modified_tax_basis_amount,1);
                         end if;
                     ---
                     end if;
                     L_tax_obj.recoverable_amt   :=  C_TAX_DETAIL.recoverable_amt;
                     GP_tax_ind := GP_tax_ind + 1;
                     GP_tax_tab.extend();
                     GP_tax_tab(GP_tax_ind) := L_tax_obj;
                  end if;
               End loop;
               ---
               if (L_triang_po_exists and not GP_fetch_comp_taxes) then
               ---
                  GP_fetch_comp_taxes := true;
                  open C_FM_TRIANG_OTHER_DOC_LINE (L_get_doc_header.comp_nf_ind);
                  ---
                  fetch C_FM_TRIANG_OTHER_DOC_LINE into L_other_doc_id,l_other_doc_line_id;
                  ---
                  close C_FM_TRIANG_OTHER_DOC_LINE;
                  ---
                  if (FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(O_line_tax, O_error_message, O_status, L_other_doc_id, l_other_doc_line_id, I_quantity, I_cost)= FALSE) then
                     return FALSE;
                  end if;
                  ---
               end if;
               ---
               O_line_tax := GP_tax_tab;
               exit;
            elsif lineitem.fiscal_Doc_line_id = I_fiscal_doc_line_id and GP_line_pack then

               ---
               FOR C_TAX_DETAIL IN C_GET_RESULTS_ITEM_TAX(lineitem.item,lineitem.fiscal_doc_line_id)
               LOOP
               ---
                  if C_TAX_DETAIL.tax_amount is not null then
                     ---
                     L_tax_code_exists  := FALSE;
                     ---
                     if L_pack_tax_tab.count > 0 then
                     ---
                        FOR C_pack_tax_ind in L_pack_tax_tab.first .. L_pack_tax_tab.last LOOP
                        ---
                           if L_pack_tax_tab(C_pack_tax_ind).tax_code = C_TAX_DETAIL.tax_code then
                           ---
                              L_pack_tax_tab(C_pack_tax_ind).tax_basis_amount  := L_pack_tax_tab(C_pack_tax_ind).tax_basis_amount + C_TAX_DETAIL.tax_basis_amount;
                              L_pack_tax_tab(C_pack_tax_ind).modified_tax_basis_amount  := L_pack_tax_tab(C_pack_tax_ind).modified_tax_basis_amount + C_TAX_DETAIL.modified_tax_basis_amount;
                              L_pack_tax_tab(C_pack_tax_ind).tax_amount  := L_pack_tax_tab(C_pack_tax_ind).tax_amount + C_TAX_DETAIL.tax_amount;
                              if L_pack_tax_tab(C_pack_tax_ind).tax_basis_amount = 0 then
                              ---
                                 if L_pack_tax_tab(C_pack_tax_ind).modified_tax_basis_amount = 0 then
                                    L_pack_tax_tab(C_pack_tax_ind).tax_rate  := 0;
                                 else
                                    L_pack_tax_tab(C_pack_tax_ind).tax_rate  := (L_pack_tax_tab(C_pack_tax_ind).tax_amount * 100) / NVL(L_pack_tax_tab(C_pack_tax_ind).modified_tax_basis_amount,1);
                                 end if;
                              else
                                 L_pack_tax_tab(C_pack_tax_ind).tax_rate  := (L_pack_tax_tab(C_pack_tax_ind).tax_amount * 100) / NVL(L_pack_tax_tab(C_pack_tax_ind).tax_basis_amount,1);
                              ---
                              end if;
                              ---
                              L_pack_tax_tab(C_pack_tax_ind).recoverable_amt   := L_pack_tax_tab(C_pack_tax_ind).recoverable_amt + C_TAX_DETAIL.recoverable_amt;
                              L_tax_code_exists := TRUE;
                              ---
                           ---
                           end if;
                           ---
                        ---
                        END LOOP;
                     ---
                     end if;
                     ---
                     if NOT L_tax_code_exists then
                     ---
                        L_pack_tax                    :=  "RIB_TaxDetRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);
                        L_pack_tax.tax_code          :=  C_TAX_DETAIL.tax_code;
                        L_pack_tax.tax_basis_amount  :=  C_TAX_DETAIL.tax_basis_amount;
                        L_pack_tax.modified_tax_basis_amount  :=  C_TAX_DETAIL.modified_tax_basis_amount;
                        L_pack_tax.tax_amount        :=  C_TAX_DETAIL.tax_amount;
                        if (L_pack_tax.tax_basis_amount = 0 and L_pack_tax.modified_tax_basis_amount = 0) then
                        ---
                           L_pack_tax.tax_rate          :=  0;
                        elsif (C_TAX_DETAIL.tax_basis_amount is null and C_TAX_DETAIL.modified_tax_basis_amount is null) then
                           L_pack_tax.tax_rate          := C_TAX_DETAIL.tax_rate;
                        else
                           if L_pack_tax.tax_basis_amount is not null then
                              L_pack_tax.tax_rate          :=  (L_pack_tax.tax_amount * 100) / NVL(L_pack_tax.tax_basis_amount,1);
                           else
                              L_pack_tax.tax_rate          :=  (L_pack_tax.tax_amount * 100) / NVL(L_pack_tax.modified_tax_basis_amount,1);
                           end if;
                        ---
                        end if;
                        ---
                        L_pack_tax.recoverable_amt   :=  C_TAX_DETAIL.recoverable_amt;
                        L_pack_tax_ind := L_pack_tax_ind + 1;
                        L_pack_tax_tab.extend();
                        L_pack_tax_tab(L_pack_tax_ind) := L_pack_tax;
                     ---
                     end if;
                     ---
                  end if;
                  ---
               END LOOP;
               ---
               O_line_tax  := L_pack_tax_tab;
         if (L_triang_po_exists and GP_line_pack ) then
            if(L_pack_tax_tab.count > 0) then
         ---
                FOR C_pack_ind in L_pack_tax_tab.first .. L_pack_tax_tab.last LOOP
                ---
                   L_tax_obj                   :=  "RIB_TaxDetRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);
                   L_tax_obj.tax_code          :=  L_pack_tax_tab(C_pack_ind).tax_code;
                   L_tax_obj.tax_basis_amount  :=  L_pack_tax_tab(C_pack_ind).tax_basis_amount;
                   L_tax_obj.modified_tax_basis_amount  :=  L_pack_tax_tab(C_pack_ind).modified_tax_basis_amount;
                   L_tax_obj.tax_amount        :=  L_pack_tax_tab(C_pack_ind).tax_amount;
                   if (L_tax_obj.tax_basis_amount = 0 and L_tax_obj.modified_tax_basis_amount = 0) then
                   ---
                      L_tax_obj.tax_rate          :=  0;
                   else
                      if L_tax_obj.tax_basis_amount != 0 then
                                     L_tax_obj.tax_rate          :=  (L_tax_obj.tax_amount * 100) / NVL(L_tax_obj.tax_basis_amount,1);
                      elsif L_tax_obj.modified_tax_basis_amount != 0 then
                         L_tax_obj.tax_rate          :=  (L_tax_obj.tax_amount * 100) / NVL(L_tax_obj.modified_tax_basis_amount,1);
                      end if;
                   end if;
                   ---
                   L_tax_obj.recoverable_amt   :=  L_pack_tax_tab(C_pack_ind).recoverable_amt;
                   GP_tax_ind := GP_tax_ind + 1;
                   GP_tax_tab.extend();
                   GP_tax_tab(GP_tax_ind) := L_tax_obj;
                   ---
                ---
                END LOOP;
              end if;
             O_line_tax  := GP_tax_tab;
                   if not GP_fetch_comp_taxes then
            ---
               GP_fetch_comp_taxes := TRUE;
               ---
               open C_FM_TRIANG_OTHER_DOC_LINE (L_get_doc_header.comp_nf_ind);
               ---
               fetch C_FM_TRIANG_OTHER_DOC_LINE into L_other_doc_id,l_other_doc_line_id;
               ---
               close C_FM_TRIANG_OTHER_DOC_LINE;
               ---
               if (FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(O_line_tax, O_error_message, O_status, L_other_doc_id, l_other_doc_line_id, I_quantity, I_cost)= FALSE) then
                  return FALSE;
               end if;
               ---
            ---
              end if;
         end if;
         ---
         end if;
       --  exit;
      END LOOP;
      ---

     --END LOOP;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                             'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id),
                                             TO_CHAR(SQLCODE));
      --
      if C_LOCK_FM_FISCAL_HEADER%ISOPEN then
         close C_LOCK_FM_FISCAL_HEADER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      --strt new
      if C_GET_TRIANG_IND%ISOPEN then
         close C_GET_TRIANG_IND;
      end if;
      ---
      if C_FM_TRIANG_OTHER_DOC%ISOPEN then
         close C_FM_TRIANG_OTHER_DOC;
      end if;
      ---
      ---
      if C_GET_OTHER_DOC_TAX%ISOPEN then
         close C_GET_OTHER_DOC_TAX;
      end if;
      ---
      if C_GET_TAX_RULE_INFO%ISOPEN then
         open C_GET_TAX_RULE_INFO;
      end if;
      ---
      if C_GET_TAX_DETAIL%ISOPEN then
         close C_GET_TAX_DETAIL;
      end if;
      ---
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      O_status := 0;
      return FALSE;
END  PROCESS_RESULTS;
------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_HIST_TABLES(O_error_message        IN OUT VARCHAR2,
                            O_status               IN OUT INTEGER,
                            I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64) := 'FM_EXT_TAXES_SQL.UPDATE_HIST_TABLES';
   L_tax_service_id           FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE := null;
   L_call_type                VARCHAR2(10)   := 'CALC_TAX';
   L_error                    VARCHAR2(1)  := 'E';
   L_status                  VARCHAR2(1);

   cursor C_GET_SEQ is
      select l10n_br_tax_service_seq.nextval
         from dual;


BEGIN
   O_status := 0;
   if I_fiscal_doc_id is not NULL then
      
      if LOAD_STAGE_TABLE(O_error_message,
                          I_fiscal_doc_id,
                          null,
                          null,
                          null,
                          L_tax_service_id,
                          'Y')= FALSE then
          return FALSE;
      end if;
      if LOAD_STAGE_DATA(O_error_message,
                         L_tax_service_id,
                         I_fiscal_doc_id,
                         'Y')= FALSE then
         return FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_FISCAL_DOC_ID',NULL,NULL,NULL);
      return FALSE;
   end if;
  if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                      L_status,
                                                      L_tax_service_id,
                                                      L_call_type)= FALSE then
      return FALSE;
   end if;

   if CLEAR_STG_TABLES(O_error_message,
                       L_tax_service_id,
                       I_fiscal_doc_id,
                       'Y') = FALSE then
       return FALSE;
    end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_HIST_TABLES;
------------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_HIST_OBJECT(O_error_message        IN OUT VARCHAR2,
                          O_status               IN OUT INTEGER,
                          I_tax_service_id       IN     FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE,
                          I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          O_businessObject       IN OUT    "RIB_FiscDocColRBM_REC")
   return BOOLEAN is
   L_program   VARCHAR2(100) := 'FM_EXT_TAXES_SQL.LOAD_HIST_OBJECT';
   ---
   L_error_message         VARCHAR2(255) := NULL;
   L_service_ind           V_BR_ITEM_FISCAL_ATTRIB.SERVICE_IND%TYPE;

   ---
   TYPE vat_code IS RECORD
   (vat_code              VAT_CODES.VAT_CODE%TYPE,
    incl_nic_ind          VAT_CODES.INCL_NIC_IND%TYPE);
   ---
   TYPE vat_code_tab IS TABLE OF vat_code INDEX BY BINARY_INTEGER;
   ---
   L_vat_code              VAT_CODE;
   L_vat_code_tab          VAT_CODE_TAB;
   L_doc_type              VARCHAR2(10) := 'FT';
   L_supp_issuer           VARCHAR2(1)  := 'Y';
   L_transac_type          VARCHAR2(1)  := 'I';
   L_calc_process_type     VARCHAR2(3)  := 'REC';
   L_no_rec_st             VARCHAR2(10) := '3';
   L_item_attr             VARCHAR2(1)  := 'Y';
   L_serv_rendered_city    VARCHAR2(1)  := 'T';        --borrower
   L_item_util             VARCHAR2(1)  := 'C';        --commercialization
   L_src_location          VARCHAR2(3)  := 'LOC';
   L_dest_location         VARCHAR2(3)  := 'LOC';
   L_partner               VARCHAR2(4)  := 'PNTR';
   L_item_number           NUMBER       := 1;
   L_tax_number            NUMBER       := 1;
   L_source_cont           NUMBER       := 0;
   L_dest_cont             NUMBER       := 0;
   tax_cont                NUMBER       := 0;
   L_source_eco            NUMBER       := 0;
   L_dest_eco              NUMBER       := 0;
   L_tax_engine_err        BOOLEAN;
   L_dummy                 VARCHAR2(1);
   L_po_cost               FM_RESOLUTION.SYSTEM_VALUE%TYPE;
   L_po_qty                FM_RESOLUTION.SYSTEM_VALUE%TYPE;
   L_base                  FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE;
   L_modified_base         FM_FISCAL_DOC_TAX_DETAIL_EXT.MODIFIED_TAX_BASIS%TYPE;
   L_tax_val               FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE;
   L_tax_rate              FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_RATE%TYPE;
   L_triang_po_exists      BOOLEAN := FALSE;
   L_triang_ref_exists     BOOLEAN := FALSE;
   L_other_doc_tax         BOOLEAN := FALSE;
   L_other_doc_id          FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_fiscal_doc_line_id    FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_ignore_tax            VARCHAR2(20) := NULL;
   L_cofins                VARCHAR2(6)  := 'COFINS';
   L_icms                  VARCHAR2(6)  := 'ICMS';
   L_ipi                   VARCHAR2(6)  := 'IPI';
   L_pis                   VARCHAR2(6)  := 'PIS';
   L_ii                    VARCHAR2(6)  := 'II';
   L_st                    VARCHAR2(6)  := 'ST';
   L_iss                   VARCHAR2(6)  := 'ISS';
   L_inss                  VARCHAR2(6)  := 'INSS';
   L_null                  VARCHAR2(1)  := ' ';
   L_true                  VARCHAR2(1)  := 'S';
   L_addr                  VARCHAR2(1)  := '1';
   L_outbound              VARCHAR2(1)  := 'O';
   L_inbound               VARCHAR2(1)  := 'I';
   L_tax                   VARCHAR2(3)  := 'TAX';
   L_rma                   VARCHAR2(3)  := 'RMA';
   L_po                    VARCHAR2(2)  := 'PO';
   L_rtv                   VARCHAR2(3)  := 'RTV';
   L_tsf                   VARCHAR2(3)  := 'TSF';
   L_ic                    VARCHAR2(2)  := 'IC';
   L_rep                   VARCHAR2(3)  := 'REP';
   L_stock                 VARCHAR2(5)  := 'STOCK';
   L_po_ref_ind            VARCHAR2(1)  := 'N';
   L_triang_ind            VARCHAR2(1)  := 'N';
   L_rnf                   VARCHAR2(3)  := 'RNF';
   L_deduced_cfop          VARCHAR2(1)  := 'Y';
   L_tax_discrep_type      VARCHAR2(1)  := 'T';
   L_nf_res                VARCHAR2(6)  := 'NF';
   L_cost_discrep_type     VARCHAR2(1)  := 'C';
   L_qty_discrep_type      VARCHAR2(1)   := 'Q';
   L_sys_res               VARCHAR2(6)  := 'SYS';
   L_deduce_icms_cst       VARCHAR2(1)  := 'Y';
   L_deduce_pis_cst        VARCHAR2(1)  := 'Y';
   L_deduce_cofins_cst     VARCHAR2(1)  := 'Y';
   L_loc_st_ind            VARCHAR2(1)  := 'N';
   L_util_st_ind           VARCHAR2(1)  := 'N';
   L_transformed_item      VARCHAR2(1)  := 'N';
   L_source                FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE   :=NULL;
   L_dest                FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE   :=NULL;
   L_cnae_code                L10N_BR_ENTITY_CNAE_CODES.CNAE_CODE%TYPE;
   L_source_type           FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE   :=NULL;
   L_dest_type             FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE   :=NULL;
   cnae_src_found          BOOLEAN :=  FALSE;
   cnae_dst_found          BOOLEAN :=  FALSE;
   ---
   L_exit                  VARCHAR2(6) := 'EXIT';
   L_cif                   VARCHAR2(6) := 'CIF';
   L_fob                   VARCHAR2(6) := 'FOB';
   L_c                     VARCHAR2(6) := 'C';
   L_f                     VARCHAR2(6) := 'F';
   L_disc_perc             VARCHAR2(1) := 'P';
   L_disc_val              VARCHAR2(1) := 'V';
   L_yes                   VARCHAR2(1) := 'Y';
   L_no                    VARCHAR2(1) := 'N';
   L_s                     VARCHAR2(1) := 'S';
   L_i                     VARCHAR2(1) := 'I';
   L_m                     VARCHAR2(1) := 'M';
   L_juris                 VARCHAR2(1) := 'J';
   L_fatura                VARCHAR2(1) := 'F';
   L_customer              VARCHAR2(4) := 'CUST';
   L_loc                   VARCHAR2(4) := 'LOC';
   L_store                 VARCHAR2(1) := 'S';
   L_wh                    VARCHAR2(1) := 'W';
   L_approved              VARCHAR2(1) := 'A';
   ---
   L_difftaxregime           NUMBER    := 0;
   L_supp_nameval            NUMBER    := 0;
   L_loc_nameval             NUMBER    := 0;
   L_doc_comp_item_nameval   NUMBER    := 0;
   L_doc_detail_item_nameval NUMBER    := 0;
   L_comp_item_nameval       NUMBER    := 0;
   L_detail_item_nameval     NUMBER    := 0;
   ---
   L_sup_name_value_tbl    ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   L_loc_name_value_tbl    ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   L_item_name_value_tbl   ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   ---
   cursor C_GET_DOC_HEADER is
      select decode(decode(nvl(fdh.schedule_no,-999),-999,'ENT',fs.mode_type),L_exit,L_outbound,L_inbound) operation_type,
             fdh.requisition_type,
             fdh.issue_date,
             fdh.entry_or_exit_date,
             fdh.freight_cost,
             decode(fdh.freight_type,L_cif,L_c,L_fob,L_f,'') freight_type,
             fdh.discount_type,
             fdh.total_discount_value,
             decode(discount_type, L_disc_perc, total_item_value * total_discount_value / 100, L_disc_val, total_discount_value, 0) header_disc,
             fdh.other_expenses_cost,
             fdh.insurance_cost,
             fdh.total_weight,
             fdh.net_weight,
             fdh.type_id,
             ffu.nop,
             fua.comp_nf_ind,
             fdh.series_no,
             fdh.fiscal_doc_no,
             fdh.module,
             fdh.key_value_1,
             fdh.key_value_2,
             fdh.location_type,
             fdh.location_id,
             fdh.partner_type,
             fdh.partner_id,
             fdh.nf_cfop,
             fdh.entry_cfop
        from fm_schedule fs,
             fm_fiscal_utilization ffu,
             fm_utilization_attributes fua,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id    = I_fiscal_doc_id
         and fdh.schedule_no      = fs.schedule_no(+)
         and fdh.utilization_id   = ffu.utilization_id
         and ffu.utilization_id   = fua.utilization_id;
   ---
   cursor C_GET_PAYMENT_DATE is
      select max(fdp.payment_date)
        from fm_fiscal_doc_payments fdp,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id    = I_fiscal_doc_id
         and fdh.fiscal_doc_id    = fdp.fiscal_doc_id;
   ---
   cursor C_GET_ENTITY_ATTRIBUTES(P_module         FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                                  P_key_value_1    FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                  P_key_value_2    FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE) is
    select fa.key_value_1,
           fa.city,
           c.jurisdiction_desc city_desc,
           lc.fiscal_code siscomex_id,
           fa.country_id,
           decode(fa.ipi_ind,L_yes,L_s,L_no) ipi_ind,
           decode(fa.iss_contrib_ind,L_yes,L_s,L_no) iss_ind,
           decode(fa.st_contrib_ind,L_yes,L_s,L_no) st_ind,
           decode(fa.icms_contrib_ind,L_yes,L_s,L_no) icms_ind,
           GET_LEGAL_NAME(P_module, P_key_value_1, P_key_value_2) legal_name,
           fa.state,
           fa.cnpj,
           fa.ie,
           fa.cpf,
           decode(fa.type,L_juris,L_s,L_fatura,L_no) type,
           fa.im,
           fa.simples_ind simples_ind,
           fa.rural_prod_ind rural_prod_ind,
           fa.is_income_range_eligible,
           fa.is_distr_a_manufacturer,
           fa.icms_simples_rate,
           CAST(MULTISET(select "RIB_DiffTaxRegime_REC"(0,tax_regime)
                           from l10n_br_sup_tax_regime
                          where to_char(supplier) = P_key_value_1) as "RIB_DiffTaxRegime_TBL") DiffTaxRegime_TBL   -- DiffTaxRegime_TBL
      from v_fiscal_attributes fa,
           v_br_country_attrib lc,
           country_tax_jurisdiction c
     where fa.module         = P_module
       and fa.key_value_1    = P_key_value_1
       and fa.key_value_2    = P_key_value_2
       and fa.country_id     = c.country_id
       and fa.state          = c.state
       and fa.city           = c.jurisdiction_code
       and fa.country_id     = lc.country_id
  union all
    select frh.cust_id,
           frh.city,
           c.jurisdiction_desc city_desc,
           lc.fiscal_code siscomex_id,
           frh.country_id,
           L_no ipi_ind,
           L_no iss_ind,
           L_no st_ind,
           L_no icms_ind,
           frh.cust_name legal_name,
           frh.state,
           frh.cnpj,
           null ie,
           frh.cpf,
           decode(cnpj,null,L_no,L_s) type,
           null im,
           L_no simples_ind,
           L_no rural_prod_ind,
           NULL,
           NULL,
           NULL,
           NULL
     from fm_rma_head frh,
          v_br_country_attrib lc,
          country_tax_jurisdiction c
    where frh.cust_id    =  P_key_value_1
      and frh.country_id =  c.country_id
      and frh.state      =  c.state
      and frh.city       =  c.jurisdiction_code
      and P_key_value_2  =  L_c
      and P_module       =  L_customer
      and frh.country_id =  lc.country_id
      and rma_id         = (select max(rma_id) from fm_rma_head where cust_id = P_key_value_1);
   ---
   cursor C_GET_DOC_DETAIL is
      select fdh.fiscal_doc_id,
             fdd.fiscal_doc_line_id,
             fdd.fiscal_doc_line_id_ref,
             fdd.fiscal_doc_id_ref,
             fdd.icms_cst icms_cst,
             fdd.pis_cst pis_cst,
             fdd.cofins_cst cofins_cst,
             decode(ifa.service_ind,L_no,L_m,L_s) classification,
             fdd.item,
             fdd.requisition_no,
             fdd.quantity,
             ROUND((NVL(fdd.unit_item_disc,0) + NVL(fdd.unit_header_disc,0)) * fdd.quantity,4) total_disc,
             ROUND(fdd.unit_header_disc * fdd.quantity,4) header_line_disc,
             ROUND(fdd.freight_cost * fdd.quantity,4) freight_cost,
             ROUND(fdd.insurance_cost * fdd.quantity,4) insurance_cost,
             ROUND(fdd.other_expenses_cost * fdd.quantity,4) other_expenses_cost,
             ROUND(fdd.unit_cost * fdd.quantity,4) total_cost,
             ROUND(fdd.unit_cost,4) unit_cost,
             fdd.recoverable_base,
             fdd.recoverable_value,
             fdd.nf_cfop,
             fdd.entry_cfop,
             fdd.pack_ind,
             im.item_desc,
             im.standard_uom,
             ifa.origin_code origin_code,
             ifa.classification_id ncm_char_code,
             ifa.ncm_char_code||decode(ifa.pauta_code,NULL,'','.'||ifa.pauta_code) ncm_ex_char_code,
             ifa.ncm_char_code ncm_code,
             ifa.ex_ipi,
             ifa.federal_service,
             decode(fdh.location_type, L_store, (select city from v_br_store where store=fdh.location_id ),
                                        L_wh, (select city from v_br_wh where wh = fdh.location_id), null) city,
             ifa.service_code,
             ifa.service_ind,
             ifa.state_of_manufacture,
             ifa.pharma_list_type
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             item_master im ,
             v_br_item_fiscal_attrib ifa
       where fdd.fiscal_doc_id       = I_fiscal_doc_id
         and fdd.fiscal_doc_id       = fdh.fiscal_doc_id
         and fdd.item                = im.item
         and fdd.item                = ifa.item
         and fdd.pack_ind            = L_no
         and fdh.status             != L_i
         and fdd.unexpected_item     = 'N'
   union
      select fdh.fiscal_doc_id,
             fdd.fiscal_doc_line_id,
             fdd.fiscal_doc_line_id_ref,
             fdd.fiscal_doc_id_ref,
             fdd.icms_cst icms_cst,
             fdd.pis_cst pis_cst,
             fdd.cofins_cst cofins_cst,
             NULL classification,
             fdd.item,
             fdd.requisition_no,
             fdd.quantity,
             ROUND((NVL(fdd.unit_item_disc,0) + NVL(fdd.unit_header_disc,0)) * fdd.quantity,4) total_disc,
             ROUND(fdd.unit_header_disc * fdd.quantity,4) header_line_disc,
             ROUND(fdd.freight_cost * fdd.quantity,4) freight_cost,
             ROUND(fdd.insurance_cost * fdd.quantity,4) insurance_cost,
             ROUND(fdd.other_expenses_cost * fdd.quantity,4) other_expenses_cost,
             ROUND(fdd.unit_cost * fdd.quantity,4) total_cost,
             ROUND(fdd.unit_cost,4) unit_cost,
             fdd.recoverable_base,
             fdd.recoverable_value,
             fdd.nf_cfop,
             fdd.entry_cfop,
             fdd.pack_ind,
             im.item_desc,
             im.standard_uom,
             NULL origin_code,
             NULL ncm_char_code,
             NULL ncm_ex_char_code,
             NULL ncm_code,
             NULL ex_ipi,
             NULL federal_service,
             decode(fdh.location_type, L_store, (select city from v_br_store where store=fdh.location_id ),
                                        L_wh, (select city from v_br_wh where wh = fdh.location_id), null) city,
             NULL service_code,
             L_no service_ind,
             NULL state_of_manufacture,
             NULL pharma_list_type
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             item_master im
       where fdd.fiscal_doc_id       = I_fiscal_doc_id
         and fdd.fiscal_doc_id       = fdh.fiscal_doc_id
         and fdd.item                = im.item
         and fdd.pack_ind            = L_yes
         and fdh.status             != L_i
         and fdd.unexpected_item     = 'N';
   ---
   cursor C_GET_COMP_ITEMS (P_fiscal_doc_line_id             FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            P_unit_cost                      FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                            P_quantity                       FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE) is
       select distinct pi.item, P_quantity * pi.pack_qty quantity
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * P_unit_cost / pi.pack_qty),4) unit_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.unit_item_disc, 0) / pi.pack_qty),4) unit_item_disc
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.unit_header_disc, 0) / pi.pack_qty),4) unit_header_disc
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.freight_cost, 0) / pi.pack_qty),4) unit_freight_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.insurance_cost, 0) / pi.pack_qty),4) unit_insurance_cost
             , ROUND(((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * NVL(fdd.other_expenses_cost, 0) / pi.pack_qty),4) unit_other_exp_cost
             , decode(ifa.service_ind,L_no,L_m,L_s) classification
             , im.item_desc
             , im.standard_uom
             , ifa.federal_service
             ,decode(fdd.location_type, L_store, (select city from v_br_store where store=fdd.location_id ),
                                        L_wh, (select city from v_br_wh where wh = fdd.location_id), null) city
             , ifa.service_code
             , ifa.ncm_char_code ncm_code
             , ifa.classification_id ncm_char_code
             ,ifa.ncm_char_code||decode(ifa.pauta_code,NULL,'','.'||ifa.pauta_code) ncm_ex_char_code
             , ifa.origin_code origin_code
             , ifa.state_of_manufacture
             , ifa.pharma_list_type
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             packitem pi,
             item_supp_country_loc iscl,
             item_master im,
             v_br_item_fiscal_attrib ifa,
             ordloc o,
             ordhead oh,
             (select pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,iscl1.origin_country_id,
                     sum(pi1.pack_qty * iscl1.negotiated_item_cost) tot_comp_cost
                from packitem pi1,
                     item_supp_country_loc iscl1
               where pi1.item                    = iscl1.item
               group by pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,iscl1.origin_country_id) total_pack
       where fdd.fiscal_doc_id            = fdh.fiscal_doc_id
         and pi.pack_no                   = total_pack.pack_no
         and pi.pack_no                   = fdd.item
         and pi.item                      = iscl.item
         and pi.item                      = im.item
         and pi.item                      = ifa.item
         and oh.order_no                  = o.order_no
         and oh.supplier                  = iscl.supplier
         and oh.supplier                  = total_pack.supplier
         and fdd.requisition_no           = o.order_no
         and fdd.item                     = o.item
         and fdd.location_type            = total_pack.loc_type
         and o.location                   = total_pack.loc
         and o.location                  in ((select wh loc
                                                from wh
                                               where physical_wh = fdd.location_id)
                                             union
                                             (select store loc
                                                from store
                                               where store = fdd.location_id))
         and fdd.location_type            = iscl.loc_type
         and iscl.loc                     = o.location
         and iscl.loc                   in ((select wh loc
                                                 from wh
                                                where physical_wh = fdd.location_id)
                                              union
                                              (select store loc
                                                 from store
                                                where store = fdd.location_id))
         and fdd.fiscal_doc_id            = I_fiscal_doc_id
         and fdd.fiscal_doc_line_id       = P_fiscal_doc_line_id
         and ifa.country_id               = iscl.origin_country_id
         and total_pack.origin_country_id   = iscl.origin_country_id;
   ---
   cursor C_GET_COMP_ITEM_TAXES (P_fiscal_doc_line_id             FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                 P_item                           PACKITEM.ITEM%TYPE) is
      select ftdw.vat_code tax_code,
             ROUND((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * ftdw.tax_basis,4) tax_basis,
             ROUND((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * ftdw.modified_tax_basis,4) modified_tax_basis,
             ROUND((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * (ftdw.total_value),4) tax_value,
            -- ROUND((pi.pack_qty * iscl.negotiated_item_cost) / total_pack.tot_comp_cost * ftdw.unit_tax_value,4) unit_tax_amount,
             ftdw.percentage_rate tax_rate
        from fm_fiscal_stg_tax_dtl ftdw,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_tax_head fth,
             packitem pi,
             item_supp_country_loc iscl,
             ordloc o,
             ordhead oh,
             (select pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc,
                     sum(pi1.pack_qty * iscl1.negotiated_item_cost) tot_comp_cost
                from packitem pi1,
                     item_supp_country_loc iscl1
               where pi1.item                    = iscl1.item
               group by pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc) total_pack
       where ftdw.fiscal_doc_line_id      = P_fiscal_doc_line_id
         and ftdw.tax_service_id          = I_tax_service_id
         and ftdw.fiscal_doc_line_id      = fdd.fiscal_doc_line_id
         and fdd.fiscal_doc_id            = fdh.fiscal_doc_id
         and fdh.fiscal_doc_id            = fth.fiscal_doc_id
         and fdd.requisition_no           = o.order_no
         and fdd.item                     = o.item
         and oh.order_no                  = o.order_no
         and oh.supplier                  = iscl.supplier
         and oh.supplier                  = total_pack.supplier
         and fth.vat_code                 = ftdw.vat_code
         and pi.pack_no                   = total_pack.pack_no
         and pi.pack_no                   = fdd.item
         and pi.item                      = iscl.item
         and pi.item                      = P_item
         and fdd.location_type            = total_pack.loc_type
         and o.location                   = total_pack.loc
         and o.location                  in ((select wh loc
                                                from wh
                                               where physical_wh = fdd.location_id)
                                             union
                                             (select store loc
                                                from store
                                               where store = fdd.location_id))
         and fdd.location_type            = iscl.loc_type
         and iscl.loc                     = o.location
         and iscl.loc                   in ((select wh loc
                                                 from wh
                                                where physical_wh = fdd.location_id)
                                              union
                                              (select store loc
                                                 from store
                                                where store = fdd.location_id));
   ---
   cursor C_GET_LINE_TAXES (P_fiscal_doc_line_id      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select ftdw.vat_code     tax_code,
             ftdw.tax_basis    tax_basis,
             ftdw.modified_tax_basis    modified_tax_basis,
             ftdw.total_value tax_value,
             ftdw.percentage_rate     tax_rate
             --ftdw.unit_tax_value unit_tax_amount
        from fm_fiscal_stg_tax_dtl ftdw,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_tax_head fth
       where ftdw.fiscal_doc_line_id        = P_fiscal_doc_line_id
         and ftdw.tax_service_id            = I_tax_service_id
         and ftdw.fiscal_doc_line_id        = fdd.fiscal_doc_line_id
         and fdd.fiscal_doc_id              = fdh.fiscal_doc_id
         and fdh.requisition_type           = L_po
         and fdd.fiscal_doc_id              = fth.fiscal_doc_id
         and fth.vat_code                   = ftdw.vat_code
      union
      select fdtd.vat_code     tax_code,
             decode(NVL(fdtd.tax_basis,0),0,appr_base_value, fdtd.tax_basis)  tax_basis,
             decode(NVL(fdtd.modified_tax_basis,0),0,appr_modified_base_value, fdtd.modified_tax_basis)  modified_tax_basis,
             decode(NVL(fdtd.total_value,0),0,appr_tax_value,fdtd.total_value)    tax_value,
             decode(NVL(fdtd.percentage_rate,0),0,appr_tax_rate,fdtd.percentage_rate)     tax_rate
       --      fdtd.unit_tax_amt unit_tax_amount
        from fm_fiscal_doc_tax_detail fdtd,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_tax_head fth
       where fdtd.fiscal_doc_line_id        = P_fiscal_doc_line_id
         and fdtd.fiscal_doc_line_id        = fdd.fiscal_doc_line_id
         and fdd.fiscal_doc_id              = fdh.fiscal_doc_id
         and fdh.requisition_type           in (L_rtv,L_tsf,L_ic,L_rep,L_stock)
         and fdd.fiscal_doc_id              = fth.fiscal_doc_id
         and fth.vat_code                   = fdtd.vat_code;
   ---
   cursor C_FM_OTHER_HEADER_DET (P_fiscal_doc_id       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select fdh.freight_cost,
             decode(fdh.freight_type,L_cif,L_c,L_fob,L_f,'') freight_type,
             ROUND(decode(discount_type, L_disc_perc, total_item_value * total_discount_value / 100, L_disc_val, total_discount_value, 0),4) header_disc,
             ROUND(fdh.other_expenses_cost,4) other_expenses_cost,
             ROUND(fdh.insurance_cost,4) insurance_cost,
             fdh.fiscal_doc_no,
             fdh.series_no,
             fdh.module,
             fdh.key_value_1,
             fdh.key_value_2
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = P_fiscal_doc_id;
   ---
   cursor C_FM_OTHER_DETAIL_DET (P_fiscal_doc_id       FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                 P_req_no              FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                 P_item                FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select ROUND((NVL(fdd.unit_item_disc,0) + NVL(fdd.unit_header_disc,0)) * fdd.quantity,4) total_disc,
             ROUND(fdd.freight_cost        * fdd.quantity,4) freight_cost,
             ROUND(fdd.insurance_cost      * fdd.quantity,4) insurance_cost,
             ROUND(fdd.other_expenses_cost * fdd.quantity,4) other_expenses_cost,
             fdd.nf_cfop,
             fdh.issue_date
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id   = P_fiscal_doc_id
         and fdd.fiscal_doc_id   = fdh.fiscal_doc_id
         and fdd.requisition_no  = P_req_no
         and fdd.item            = P_item;
   ---
   cursor C_GET_HEADER_TAXES is
      select ftdw.vat_code tax_code,
             sum(ftdw.tax_basis) tax_basis,
             sum(ftdw.modified_tax_basis) modified_tax_basis,
             sum(ftdw.total_value) tax_value
        from fm_fiscal_stg_tax_dtl ftdw,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_tax_head fth
       where fdh.fiscal_doc_id             = I_fiscal_doc_id
         and ftdw.tax_service_id           = I_tax_service_id
         and fdd.fiscal_doc_id             = fdh.fiscal_doc_id
         and fdd.fiscal_doc_line_id        = ftdw.fiscal_doc_line_id
         and fdd.fiscal_doc_id             = fth.fiscal_doc_id
         and fdh.requisition_type          = L_po
         and ftdw.vat_code                 = fth.vat_code
       group by fdh.fiscal_doc_id, ftdw.vat_code
      union
      select fdth.vat_code,
             fdth.tax_basis,
             fdth.modified_tax_basis,
             fdth.total_value
        from fm_fiscal_doc_tax_head fdth,
             fm_fiscal_doc_header fdh
      where fdh.fiscal_doc_id             = fdth.fiscal_doc_id
        and fdh.fiscal_doc_id             = I_fiscal_doc_id
        and fdh.requisition_type          in (L_rtv,L_tsf,L_ic,L_rep,L_stock);
   ---
   cursor C_GET_COST_RESL (P_fiscal_doc_line_id      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select round(((((iscl.negotiated_item_cost*pi.pack_qty)/total_pack.tot_comp_cost)*vo.unit_cost))/pi.pack_qty,4) po_unit_cost
        from fm_resolution fmr,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             ordloc vo,
             ordhead oh,
             packitem pi ,
             item_supp_country_loc iscl,
             (select pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc, round(sum(pi1.pack_qty * iscl1.negotiated_item_cost),4) tot_comp_cost
                from packitem pi1, item_supp_country_loc iscl1
               where pi1.item                    = iscl1.item
               group by pi1.pack_no, iscl1.supplier, iscl1.loc_type, iscl1.loc) total_pack
       where fmr.fiscal_doc_id              = I_fiscal_doc_id
         and fmr.fiscal_doc_line_id         = P_fiscal_doc_line_id
         and fdd.fiscal_doc_id              = fmr.fiscal_doc_id
         and fdd.fiscal_doc_line_id         = fmr.fiscal_doc_line_id
         and fdh.fiscal_doc_id              = fdd.fiscal_doc_id
         and fdh.location_id                = DECODE(fdh.location_type, L_store, vo.location, L_wh,(select physical_wh from WH where wh.wh=vo.location))
         and fmr.discrep_type               = L_cost_discrep_type
         and fmr.resolution_type            = L_sys_res
         and vo.order_no                    = fdd.requisition_no
         and oh.order_no                    = vo.order_no
         and oh.supplier                    = iscl.supplier
         and oh.supplier                    = total_pack.supplier
         and (vo.item = fdd.item or vo.item = fdd.pack_no)
         and fdd.pack_no                    is NOT NULL
         and fdd.pack_no                    = pi.pack_no (+)
         and fdd.item                       = pi.item(+)
         and iscl.item                      = pi.item
         and total_pack.pack_no             = pi.pack_no
         and (P_fiscal_doc_line_id is NOT NULL and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id)
         and iscl.loc_type                  = vo.loc_type
         and iscl.loc                       = vo.location
         and iscl.loc in ((select wh loc
                             from wh
                            where physical_wh = fdd.location_id)
                           union
                          (select store loc
                             from store
                            where store = fdd.location_id))
         and total_pack.loc_type                  = vo.loc_type
         and total_pack.loc                       = vo.location
         and total_pack.loc in ((select wh loc
                                   from wh
                                  where physical_wh = fdd.location_id)
                                 union
                                (select store loc
                                   from store
                                  where store = fdd.location_id))
      union
      select round(nvl(vo.unit_cost,0),4) po_unit_cost
        from fm_resolution fmr,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             ordloc vo
       where fmr.fiscal_doc_id              = I_fiscal_doc_id
         and fmr.fiscal_doc_line_id         = P_fiscal_doc_line_id
         and fdd.fiscal_doc_id              = fmr.fiscal_doc_id
         and fdd.fiscal_doc_line_id         = fmr.fiscal_doc_line_id
         and fdh.fiscal_doc_id              = fdd.fiscal_doc_id
         and fdh.location_id                = DECODE(fdh.location_type, L_store, vo.location, L_wh,(select physical_wh from WH where wh.wh=vo.location))
         and fmr.discrep_type               = L_cost_discrep_type
         and fmr.resolution_type            = L_sys_res
         and vo.order_no                    = fdd.requisition_no
         and vo.item                        = fdd.item
         and fdd.pack_no is NULL
         and (P_fiscal_doc_line_id is NOT NULL and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id)
         and vo.location  in ((select wh loc
                                 from wh
                                where physical_wh = fdd.location_id)
                               union
                              (select store loc
                                 from store
                                where store = fdd.location_id));
   ---
   cursor C_GET_QTY_RESL (P_fiscal_doc_line_id      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select distinct(received_qty) qty
        from fm_fiscal_stg_tax_dtl
       where fiscal_doc_line_id = P_fiscal_doc_line_id
         and tax_service_id     = I_tax_service_id;
   ---
   cursor C_NF_FOR_HIST_UPD is
      select 'X'
        from fm_stg_st_rec_hist
       where fiscal_doc_id  = I_fiscal_doc_id
         and status         = 'R';
   ---
   --rec st
   cursor C_GET_NIC_IND is
      select vat_code, incl_nic_ind
        from vat_codes;
   ---
   cursor C_GET_CNAE_CODES(P_module         FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                                  P_key_value_1    FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                                  P_key_value_2    FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE) is
 select ecc.cnae_code
      from l10n_br_entity_cnae_codes ecc
     where ecc.module         = P_module
       and ecc.key_value_1    = P_key_value_1
  --     and ecc.key_value_2    = P_key_value_2
  union all
    select null cnae_code
     from fm_rma_head frh,
          l10n_br_entity_cnae_codes ecc,
          v_br_country_attrib lc,
          country_tax_jurisdiction c
           --city c
    where frh.cust_id    =  P_key_value_1
        and frh.cust_id = ecc.key_value_1
      and frh.country_id =  c.country_id
      and frh.state      =  c.state
      and frh.city       =  c.jurisdiction_code
      and P_key_value_2  =  L_c
      and P_module       =  L_customer
      and frh.country_id =  lc.country_id
      and rma_id         = (select max(rma_id) from fm_rma_head where cust_id = P_key_value_1);
   ---
   cursor C_GET_TRIANG_IND is
      select 'X'
        from fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdd.fiscal_doc_id       = I_fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes;
 cursor C_GET_UOM_ITEM (P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
         select iscd.item,
                iscd.supplier,
                iscd.origin_country_id
       from item_supp_country iscd
          where iscd.item = P_item
            and primary_supp_ind = 'Y'
         and primary_country_ind = 'Y';
   ---
   cursor C_FM_TRIANG_OTHER_DOC (P_comp_nf_ind        FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE) is
      select fdc.fiscal_doc_id other_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and P_comp_nf_ind           = L_yes
         and fdc.fiscal_doc_id is NOT NULL
       UNION
      select fdc.compl_fiscal_doc_id other_doc_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.fiscal_doc_id       = I_fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = L_yes
         and P_comp_nf_ind           = L_no
         and fdc.fiscal_doc_id is NOT NULL;
   ---
   cursor C_SUP_CUSTOM_ATTRIB IS
      select ENTITY_ATTRIB_REC(
             'SUPP',        --I_entity_type
             ffdh.key_value_1,  --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from fm_tax_call_stage ftcs,
             fm_fiscal_doc_header ffdh
       where ftcs.tax_service_id = I_tax_service_id
         and ftcs.fiscal_doc_id  = I_fiscal_doc_id
         and ftcs.fiscal_doc_id = ffdh.fiscal_doc_id
         and ffdh.module = 'SUPP';
   ---
   cursor C_LOC_CUSTOM_ATTRIB IS
      select ENTITY_ATTRIB_REC(
             'LOC',        --I_entity_type
             ffdh.location_id,    --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from fm_tax_call_stage ftcs,
             fm_fiscal_doc_header ffdh
       where ftcs.tax_service_id = I_tax_service_id
         and ftcs.fiscal_doc_id  = I_fiscal_doc_id
         and ftcs.fiscal_doc_id  = ffdh.fiscal_doc_id
      union all
      select ENTITY_ATTRIB_REC(
             'LOC',        --I_entity_type
             ffdh.key_value_1,    --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from fm_tax_call_stage ftcs,
             fm_fiscal_doc_header ffdh
       where ftcs.tax_service_id = I_tax_service_id
         and ftcs.fiscal_doc_id  = I_fiscal_doc_id
         and ftcs.fiscal_doc_id = ffdh.fiscal_doc_id
         and ffdh.module = 'PTNR'
         and ffdh.key_value_2 = 'E'
      union all
      select ENTITY_ATTRIB_REC(
             'LOC',        --I_entity_type
             ffdh.key_value_1,    --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from fm_tax_call_stage ftcs,
             fm_fiscal_doc_header ffdh
       where ftcs.tax_service_id = I_tax_service_id
         and ftcs.fiscal_doc_id  = I_fiscal_doc_id
         and ftcs.fiscal_doc_id = ffdh.fiscal_doc_id
         and ffdh.module = 'LOC';
   ---
   cursor C_DETAIL_ITEM_CUSTOM_ATTRIB(P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) IS
      select ENTITY_ATTRIB_REC(
            'ITEM',     --I_entity_type
             ffdd.item,  --I_entity_id
             NULL,       --O_name
             NULL)       --O_value
        from fm_tax_call_stage ftcs,
             fm_fiscal_doc_header ffdh,
             fm_fiscal_doc_detail ffdd
       where ftcs.tax_service_id = I_tax_service_id
         and ftcs.fiscal_doc_id  = I_fiscal_doc_id
         and ftcs.fiscal_doc_id = ffdh.fiscal_doc_id
         and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
         and ffdd.item = P_item;
   ---
   cursor C_COMP_ITEM_CUSTOM_ATTRIB(P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) IS
      select ENTITY_ATTRIB_REC(
            'ITEM',     --I_entity_type
             pi.item,  --I_entity_id
             NULL,       --O_name
             NULL)       --O_value
        from fm_tax_call_stage ftcs,
             fm_fiscal_doc_header ffdh,
             fm_fiscal_doc_detail ffdd,
             packitem pi
       where ftcs.fiscal_doc_id = ffdh.fiscal_doc_id
         and ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
         and pi.item = P_item
         and pi.pack_no = ffdd.item;
   ---
   L_get_doc_header           C_GET_DOC_HEADER%ROWTYPE;
   L_get_doc_detail           C_GET_DOC_DETAIL%ROWTYPE;
   L_get_comp_items           C_GET_COMP_ITEMS%ROWTYPE;
   L_get_comp_items_taxes     C_GET_COMP_ITEM_TAXES%ROWTYPE;
   L_get_line_taxes           C_GET_LINE_TAXES%ROWTYPE;
   L_get_header_taxes         C_GET_HEADER_TAXES%ROWTYPE;
   L_ent_supp                 C_GET_ENTITY_ATTRIBUTES%ROWTYPE;
   L_ent_main_nf_supp         C_GET_ENTITY_ATTRIBUTES%ROWTYPE;
   L_ent_loc                  C_GET_ENTITY_ATTRIBUTES%ROWTYPE;
   L_fm_other_header_det      C_FM_OTHER_HEADER_DET%ROWTYPE;
   L_fm_other_detail_det      C_FM_OTHER_DETAIL_DET%ROWTYPE;
   L_get_nic_ind              C_GET_NIC_IND%ROWTYPE;
   L_get_payment_date         FM_FISCAL_DOC_PAYMENTS.PAYMENT_DATE%TYPE;
   ---
   ---
   fiscalDocumentRBO    "RIB_FiscDocRBO_REC"                := "RIB_FiscDocRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   sources              "RIB_FromEntity_REC"            := "RIB_FromEntity_REC"(0,null);
   destinations         "RIB_ToEntity_REC"              := "RIB_ToEntity_REC"(0,null);
   fiscalEntRBOSource   "RIB_FiscEntityRBO_REC"       := "RIB_FiscEntityRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null) ;
   fiscalEntRBODest     "RIB_FiscEntityRBO_REC"       := "RIB_FiscEntityRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null) ;
   sourceAddress        "RIB_AddrRBO_REC"                    := "RIB_AddrRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);
   destinationAddress   "RIB_AddrRBO_REC"                    := "RIB_AddrRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null,null);
   fiscalDocumentColRBM        "RIB_FiscDocColRBM_REC"      := "RIB_FiscDocColRBM_REC"(0,null,null,null,null,null);
   taxdetailRBO                "RIB_InformTaxRBO_REC"         := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);
   tax_contributor1            "RIB_TaxContributor_REC"       := "RIB_TaxContributor_REC"(0,null);
   tax_contributor2            "RIB_TaxContributor_REC"       := "RIB_TaxContributor_REC"(0,null);
   tax_contributor3            "RIB_TaxContributor_REC"       := "RIB_TaxContributor_REC"(0,null);
   tax_contributor4            "RIB_TaxContributor_REC"       := "RIB_TaxContributor_REC"(0,null);
   tax_contributor5            "RIB_TaxContributor_REC"       := "RIB_TaxContributor_REC"(0,null);
   tax_contributor6            "RIB_TaxContributor_REC"       := "RIB_TaxContributor_REC"(0,null);
   tax_contributor7            "RIB_TaxContributor_REC"       := "RIB_TaxContributor_REC"(0,null);
   source_eco                  "RIB_EcoClassCd_REC"           := "RIB_EcoClassCd_REC"(0,null);
   dest_eco                    "RIB_EcoClassCd_REC"           := "RIB_EcoClassCd_REC"(0,null);
   documentLineItem            "RIB_LineItemRBO_REC"          := "RIB_LineItemRBO_REC"(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   linetaxdetailRBO            "RIB_InformTaxRBO_REC"         := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);
   headertaxdetailRBO          "RIB_InformTaxRBO_REC"         := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);
   serviceItem                 "RIB_SvcItemRBO_REC"       := "RIB_SvcItemRBO_REC"(0,null,null,null,null,null,null,null);
   productItem                 "RIB_PrdItemRBO_REC"     := "RIB_PrdItemRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null);
   difftaxregime               "RIB_DiffTaxRegime_REC"     := "RIB_DiffTaxRegime_REC"(0,null);
   tbl_difftaxregime           "RIB_DiffTaxRegime_TBL"     := "RIB_DiffTaxRegime_TBL"();
   supp_nameval                "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   loc_nameval                 "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   doc_comp_item_nameval       "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   doc_detail_item_nameval     "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   detail_item_nameval         "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   comp_item_nameval           "RIB_NameValPairRBO_REC"    := "RIB_NameValPairRBO_REC"(0,null,null);
   tbl_supp_nameval            "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_doc_comp_item_nameval   "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_doc_detail_item_nameval "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_detail_item_nameval     "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_comp_item_nameval       "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   tbl_loc_nameval             "RIB_NameValPairRBO_TBL"    := "RIB_NameValPairRBO_TBL"();
   ---
   tbl_documentLineItem        "RIB_LineItemRBO_TBL"          := "RIB_LineItemRBO_TBL"();
   tbl_sources                 "RIB_FromEntity_TBL"                 := "RIB_FromEntity_TBL"();
   tbl_destinations            "RIB_ToEntity_TBL"            := "RIB_ToEntity_TBL"();
   tbl_fiscalDocument_request  "RIB_FiscDocRBO_TBL"            := "RIB_FiscDocRBO_TBL"();
   tbl_taxdetail               "RIB_InformTaxRBO_TBL"            := "RIB_InformTaxRBO_TBL"();
   tbl_taxheader               "RIB_InformTaxRBO_TBL"            := "RIB_InformTaxRBO_TBL"();
   tbl_tax_contrib_source      "RIB_TaxContributor_TBL"          := "RIB_TaxContributor_TBL"(null);
   tbl_tax_contrib_dest        "RIB_TaxContributor_TBL"          := "RIB_TaxContributor_TBL"(null);
   tbl_source_eco              "RIB_EcoClassCd_TBL"            := "RIB_EcoClassCd_TBL"();
   tbl_dest_eco                "RIB_EcoClassCd_TBL"            := "RIB_EcoClassCd_TBL"();
   tbl_source_addr             "RIB_AddrRBO_TBL"               := "RIB_AddrRBO_TBL"();
   tbl_dest_addr               "RIB_AddrRBO_TBL"               := "RIB_AddrRBO_TBL"();
   tbl_fiscalEntRBOSource      "RIB_FiscEntityRBO_TBL"             :="RIB_FiscEntityRBO_TBL"();
   tbl_fiscalEntRBODest        "RIB_FiscEntityRBO_TBL"             :="RIB_FiscEntityRBO_TBL"();
   tbl_productItem             "RIB_PrdItemRBO_TBL"           :="RIB_PrdItemRBO_TBL"();
   tbl_serviceItem             "RIB_SvcItemRBO_TBL"           :="RIB_SvcItemRBO_TBL"();
   L_tbl_documentLineItem      "RIB_LineItemRBO_TBL"  := "RIB_LineItemRBO_TBL"();
   L_doclineItemRBO            "RIB_LineItemRBO_REC";

   fiscDocChnkRBM              "RIB_FiscDocChnkRBO_REC"    := "RIB_FiscDocChnkRBO_REC"(0,null,null);
   tbl_fiscDocChnkRBM          "RIB_FiscDocChnkRBO_TBL"    := "RIB_FiscDocChnkRBO_TBL"();
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
   ---
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_FISCAL_DOC_ID',NULL,NULL,NULL);
      return FALSE;
   ---
   end if;
   ---
   open C_GET_NIC_IND;
   ---
   LOOP
      ---
      fetch C_GET_NIC_IND bulk collect into L_vat_code_tab limit 500;
      EXIT when C_GET_NIC_IND%NOTFOUND;
      ---
   END LOOP;
   ---
   close C_GET_NIC_IND;
   --
   --rec st
   open C_NF_FOR_HIST_UPD;
   ---
   FETCH C_NF_FOR_HIST_UPD into L_dummy;
   ---
   if C_NF_FOR_HIST_UPD%NOTFOUND then
   ---
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NF_NOT_FOR_HIST',NULL,NULL,NULL);
      return FALSE;
   ---
   end if;
   ---
   close C_NF_FOR_HIST_UPD;
   ---
   --rec st
   --Supplier Details
   open C_GET_DOC_HEADER;
   ---
   FETCH C_GET_DOC_HEADER into L_get_doc_header;
   ---
   if C_GET_DOC_HEADER%NOTFOUND then
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_HEAD',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   if L_get_doc_header.operation_type = L_outbound or L_get_doc_header.requisition_type = L_rma then
      L_supp_issuer := 'N';
      ---
      if L_get_doc_header.requisition_type = L_rma then
         L_item_attr := 'Y';
      else
         L_item_attr    := 'N';
         L_transac_type := 'O';
      end if;
      ---
   end if;
   ---
   if L_get_doc_header.requisition_type = L_rma or L_get_doc_header.operation_type = L_outbound then
      L_calc_process_type  := L_tax;
   end if;
   ---
   close C_GET_DOC_HEADER;
   ---
   open C_GET_PAYMENT_DATE;
   ---
   FETCH C_GET_PAYMENT_DATE into L_get_payment_date;
   ---
   close C_GET_PAYMENT_DATE;
   ---
   open C_GET_ENTITY_ATTRIBUTES(L_get_doc_header.module, L_get_doc_header.key_value_1, L_get_doc_header.key_value_2);
   ---
   fetch C_GET_ENTITY_ATTRIBUTES into L_ent_supp;
   ---
   if C_GET_ENTITY_ATTRIBUTES%NOTFOUND then
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_SOURCE',TO_CHAR(L_get_doc_header.key_value_1),NULL,NULL);
      return FALSE;
   end if;
   ---
   close C_GET_ENTITY_ATTRIBUTES;
   ---
   open C_GET_ENTITY_ATTRIBUTES(L_dest_location, L_get_doc_header.location_id, L_get_doc_header.location_type);
   ---
   fetch C_GET_ENTITY_ATTRIBUTES into L_ent_loc;
   ---
   if C_GET_ENTITY_ATTRIBUTES%NOTFOUND then
      O_status := 0;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_DEST',TO_CHAR(L_get_doc_header.location_id),NULL,NULL);
      return FALSE;
   end if;
   ---
   close C_GET_ENTITY_ATTRIBUTES;
   ---
   open C_GET_TRIANG_IND;
   ---
   fetch C_GET_TRIANG_IND into L_dummy;
   L_triang_po_exists := C_GET_TRIANG_IND%FOUND;
   ---
   close C_GET_TRIANG_IND;
   ---
   if L_triang_po_exists then
   ---
      open C_FM_TRIANG_OTHER_DOC (L_get_doc_header.comp_nf_ind);
      ---
      fetch C_FM_TRIANG_OTHER_DOC into L_other_doc_id;
      ---
      close C_FM_TRIANG_OTHER_DOC;
      ---
   end if;
   ---
   if L_get_doc_header.operation_type = L_inbound and L_get_doc_header.requisition_type not in  ('TSF','IC','REP') then

      open C_SUP_CUSTOM_ATTRIB;
      fetch C_SUP_CUSTOM_ATTRIB bulk collect into L_sup_name_value_tbl;
      close C_SUP_CUSTOM_ATTRIB;
      if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                    L_sup_name_value_tbl,
                                                    'SUPP') = FALSE then
         return FALSE;
      end if;
   end if;
   -- build a collection of non-base localization attributes for locations
   open C_LOC_CUSTOM_ATTRIB;
   fetch C_LOC_CUSTOM_ATTRIB bulk collect into L_loc_name_value_tbl;
   close C_LOC_CUSTOM_ATTRIB;
   if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                 L_loc_name_value_tbl,
                                                 'LOC') = FALSE then
      return FALSE;
   end if;
   ---
   fiscalEntRBOSource.federal_tax_reg_id           :=NVL(L_ent_supp.cnpj,L_ent_supp.cpf);
   fiscalEntRBOSource.fiscal_type                  :=L_ent_supp.type;
   ---
    open C_GET_CNAE_CODES(L_get_doc_header.module, L_get_doc_header.key_value_1, L_get_doc_header.key_value_2);
      ---
      fetch C_GET_CNAE_CODES into L_cnae_code;
      ---
      if C_GET_CNAE_CODES%FOUND then
        cnae_src_found := true;
      end if;
    close  C_GET_CNAE_CODES;
    if (cnae_src_found) then
           for cnae_codes in C_GET_CNAE_CODES(L_get_doc_header.module, L_get_doc_header.key_value_1, L_get_doc_header.key_value_2) loop
                  tbl_source_eco.extend();
                   L_source_eco := L_source_eco + 1;
                   source_eco.value := cnae_codes.cnae_code;
                   tbl_source_eco(L_source_eco) := source_eco;
           end loop;
     end if;
   ---
   fiscalEntRBOSource.EcoClassCd_TBL      := tbl_source_eco;
   ---
   if L_sup_name_value_tbl.exists(1) then
      for i in  L_sup_name_value_tbl.FIRST .. L_sup_name_value_tbl.LAST loop
         tbl_supp_nameval.extend();
               L_supp_nameval := L_supp_nameval + 1;
                     supp_nameval.name := L_sup_name_value_tbl(i).O_name;
                     supp_nameval.value := L_sup_name_value_tbl(i).O_value;
                     tbl_supp_nameval(L_supp_nameval) := supp_nameval;
      end loop;
   end if;
   ---
   fiscalEntRBOSource.NameValPairRBO_TBL          := tbl_supp_nameval;
   -- Supplier Address
   sourceAddress.city_id               := L_ent_supp.city;
   sourceAddress.state                 := L_ent_supp.state;
   sourceAddress.country_id            := L_ent_supp.siscomex_id;
   sourceAddress.addr                  := L_addr;
   sourceAddress.addr_type             := L_addr;
   sourceAddress.primary_addr_type_ind := L_addr;
   sourceAddress.add_1                 := L_addr;
   sourceAddress.primary_addr_ind      := L_addr;
   sourceAddress.city                  := L_ent_supp.city_desc;
   ---
   --not in client but mapped in tcd
   fiscalEntRBOSource.entity_code         := L_ent_supp.key_value_1;
   fiscalEntRBOSource.legal_name          := L_ent_supp.legal_name;
   fiscalEntRBOSource.is_income_range_eligible  := L_ent_supp.is_income_range_eligible;
   fiscalEntRBOSource.is_distr_a_manufacturer   := L_ent_supp.is_distr_a_manufacturer;
   fiscalEntRBOSource.icms_simples_rate         := L_ent_supp.icms_simples_rate;
    if L_ent_supp.DiffTaxRegime_TBL.exists(1) then
   for rec in L_ent_supp.DiffTaxRegime_TBL.FIRST .. L_ent_supp.DiffTaxRegime_TBL.LAST loop
       tbl_difftaxregime.extend();
             L_difftaxregime := L_difftaxregime + 1;
                   difftaxregime.value := L_ent_supp.DiffTaxRegime_TBL(rec).value;
                   tbl_difftaxregime(L_difftaxregime) := difftaxregime;
   end loop;
   end if;
   fiscalEntRBOSource.DiffTaxRegime_TBL := tbl_difftaxregime;
   ---
   -- Tax Contributor's Details
   if L_ent_supp.ipi_ind = L_true then
      tax_contributor3.value := L_ipi;
   end if;
   ---
   if L_ent_supp.iss_ind = L_true then
      tax_contributor1.value := L_iss;
   end if;
   ---
   if L_ent_supp.st_ind = L_true then
      tax_contributor2.value := L_st;
   end if;
   ---
   if L_ent_supp.icms_ind = L_true then
      tax_contributor4.value := L_icms;
   end if;
   ---
   tbl_tax_contrib_source.DELETE();
   tax_cont:=0;
   if tax_contributor1.value='ISS' then
      tbl_tax_contrib_source.extend();
      tax_cont:=tax_cont+1;
      tbl_tax_contrib_source(tax_cont):=tax_contributor1;
   end if;
   if tax_contributor2.value='ST' then
      tbl_tax_contrib_source.extend();
      tax_cont:=tax_cont+1;
      tbl_tax_contrib_source(tax_cont):=tax_contributor2;
   end if;
   if tax_contributor3.value='IPI' then
      tbl_tax_contrib_source.extend();
      tax_cont:=tax_cont+1;
      tbl_tax_contrib_source(tax_cont):=tax_contributor3;
   end if;
   if tax_contributor4.value='ICMS' then
      tbl_tax_contrib_source.extend();
      tax_cont:=tax_cont+1;
      tbl_tax_contrib_source(tax_cont):=tax_contributor4;
   end if;
   ---
   if tbl_tax_contrib_source.count >0 then
      fiscalEntRBOSource.taxContributor_tbl:=tbl_tax_contrib_source;
      tbl_tax_contrib_source.DELETE();
   end if;
   ---
   tbl_source_addr.extend();
   tbl_source_addr(1) := sourceAddress;
   fiscalEntRBOSource.AddrRBO_TBL          := tbl_source_addr;
   fiscalEntRBOSource.is_rural_producer := L_ent_supp.rural_prod_ind;
   ---
   tbl_fiscalEntRBOSource.extend();
   tbl_fiscalEntRBOSource(1) := fiscalEntRBOSource;
   ---
   sources.FiscEntityRBO_TBL           := tbl_fiscalEntRBOSource;
   L_source_cont  := L_source_cont + 1;
   tbl_sources.extend();
   tbl_sources(L_source_cont):=sources;
   ---
   --Destination Details
   fiscalEntRBODest.federal_tax_reg_id           := NVL(L_ent_loc.cnpj,L_ent_loc.cpf);
   fiscalEntRBODest.fiscal_type                  := L_ent_loc.type;
   --
   open C_GET_CNAE_CODES(L_dest_location, L_get_doc_header.location_id, L_get_doc_header.location_type);
      ---
      fetch C_GET_CNAE_CODES into L_cnae_code;
      ---
      if C_GET_CNAE_CODES%FOUND then
          cnae_dst_found := true;
      end if;
    close  C_GET_CNAE_CODES;
    if (cnae_dst_found) then
           for cnae_codes in C_GET_CNAE_CODES(L_dest_location, L_get_doc_header.location_id, L_get_doc_header.location_type) loop
             tbl_dest_eco.extend();
              L_dest_eco := L_dest_eco + 1;
              dest_eco.value := cnae_codes.cnae_code;
              tbl_dest_eco(L_dest_eco) := dest_eco;
           end loop;
    end if;
   ---
   if L_loc_name_value_tbl.exists(1) then
   for i in  L_loc_name_value_tbl.FIRST .. L_loc_name_value_tbl.LAST loop
       tbl_loc_nameval.extend();
             L_loc_nameval := L_loc_nameval + 1;
                   loc_nameval.name := L_loc_name_value_tbl(i).O_name;
                   loc_nameval.value := L_loc_name_value_tbl(i).O_value;
                   tbl_loc_nameval(L_loc_nameval) := loc_nameval;
   end loop;
   end if;
   ---
   fiscalEntRBODest.NameValPairRBO_TBL          := tbl_loc_nameval;
   ---
   fiscalEntRBODest.EcoClassCd_TBL      := tbl_dest_eco;
   ---
   fiscalEntRBODest.entity_code         := L_ent_loc.key_value_1;
   fiscalEntRBODest.legal_name          := L_ent_loc.legal_name;
   ---
   -- Destination Address
   destinationAddress.city_id               := L_ent_loc.city;
   destinationAddress.state                 := L_ent_loc.state;
   destinationAddress.country_id            := L_ent_loc.siscomex_id;
   destinationAddress.addr                  := L_addr;
   destinationAddress.addr_type             := L_addr;
   destinationAddress.primary_addr_type_ind := L_addr;
   destinationAddress.add_1                 := L_addr;
   destinationAddress.primary_addr_ind      := L_addr;
   destinationAddress.city                  := L_ent_loc.city_desc;
   ---- Tax Contributor's Details
   if L_ent_loc.ipi_ind = L_true then
      tax_contributor6.value := L_ipi;
   end if;
   ---
   if L_ent_loc.iss_ind = L_true then
      tax_contributor5.value := L_iss;
   end if;
   ---
   if L_ent_loc.icms_ind = L_true then
      tax_contributor7.value := L_icms;
   end if;
   ---
   tbl_tax_contrib_dest.DELETE();
   tax_cont:=0;
   if tax_contributor5.value='ISS' then
     tbl_tax_contrib_dest.extend();
     tax_cont:=tax_cont+1;
     tbl_tax_contrib_dest(tax_cont):=tax_contributor5;
   end if;
   if tax_contributor6.value='IPI' then
     tbl_tax_contrib_dest.extend();
     tax_cont:=tax_cont+1;
     tbl_tax_contrib_dest(tax_cont):=tax_contributor6;
   end if;
   if tax_contributor7.value='ICMS' then
     tbl_tax_contrib_dest.extend();
     tax_cont:=tax_cont+1;
     tbl_tax_contrib_dest(tax_cont):=tax_contributor7;
   end if;
   if tbl_tax_contrib_dest.count >0 then
      fiscalEntRBODest.taxContributor_tbl:=tbl_tax_contrib_dest;
      tbl_tax_contrib_dest.DELETE();
   end if;
   ---
   tbl_dest_addr.extend();
   tbl_dest_addr(1) := destinationAddress;
   fiscalEntRBODest.AddrRBO_TBL          := tbl_dest_addr;
   fiscalEntRBODest.is_rural_producer  := L_ent_loc.rural_prod_ind;
   tbl_fiscalEntRBODest.extend();
   tbl_fiscalEntRBODest(1) := fiscalEntRBODest;
   ---
   destinations.FiscEntityRBO_TBL           := tbl_fiscalEntRBODest;
   L_dest_cont    := L_dest_cont + 1;
   tbl_destinations.extend();
   tbl_destinations(L_dest_cont) := destinations;
   ---
   open C_GET_DOC_DETAIL;
   ---
   L_item_number := 1;
   --L_tax_number  := 1;
   LOOP
      ---
      fetch C_GET_DOC_DETAIL into L_get_doc_detail;
      EXIT when C_GET_DOC_DETAIL%NOTFOUND;
      ---
      open C_GET_COST_RESL(L_get_doc_detail.fiscal_doc_line_id);
      ---
      FETCH C_GET_COST_RESL into L_po_cost;
      ---
      if C_GET_COST_RESL%NOTFOUND then
      ---
         L_po_cost    := L_get_doc_detail.unit_cost;
      ---
      end if;
      ---
      close C_GET_COST_RESL;
      ---
      open C_GET_QTY_RESL(L_get_doc_detail.fiscal_doc_line_id);
      ---
      FETCH C_GET_QTY_RESL into L_po_qty;
      ---
      if C_GET_QTY_RESL%NOTFOUND then
      ---
         L_po_qty     := L_get_doc_detail.quantity;
      ---
      end if;
      ---
      close C_GET_QTY_RESL;
      ---
      if L_get_doc_detail.pack_ind = 'Y' then
      ---
         if L_triang_po_exists and L_get_doc_header.comp_nf_ind = 'Y' then
         ---
            open C_FM_OTHER_HEADER_DET(L_other_doc_id);
            ---
            FETCH C_FM_OTHER_HEADER_DET into L_fm_other_header_det;
            ---
            close C_FM_OTHER_HEADER_DET;
            ---
            open C_FM_OTHER_DETAIL_DET(L_other_doc_id, L_get_doc_detail.requisition_no, L_get_doc_detail.item);
            ---
            FETCH C_FM_OTHER_DETAIL_DET into L_fm_other_detail_det;
            ---
            close C_FM_OTHER_DETAIL_DET;
            ---
            open C_GET_ENTITY_ATTRIBUTES(L_fm_other_header_det.module, L_fm_other_header_det.key_value_1, L_fm_other_header_det.key_value_2);
            ---
            fetch C_GET_ENTITY_ATTRIBUTES into L_ent_main_nf_supp;
            ---
            if C_GET_ENTITY_ATTRIBUTES%NOTFOUND then
               ---
               O_status := 0;
               O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_SOURCE',TO_CHAR(L_fm_other_header_det.key_value_1),NULL,NULL);
               return FALSE;
            end if;
            ---
            close C_GET_ENTITY_ATTRIBUTES;
            ---
         end if;
         ---
         open C_GET_COMP_ITEMS (L_get_doc_detail.fiscal_doc_line_id, L_po_cost, L_po_qty);
         ---
         LOOP
         ---
            fetch C_GET_COMP_ITEMS into L_get_comp_items;
            EXIT when C_GET_COMP_ITEMS%NOTFOUND;
            ---
            documentLineItem        := "RIB_LineItemRBO_REC"(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
            documentLineItem.discount                 := (L_get_comp_items.unit_item_disc + L_get_comp_items.unit_header_disc) * L_get_comp_items.quantity;
            documentLineItem.freight                  := L_get_comp_items.unit_freight_cost * L_get_comp_items.quantity;
            documentLineItem.other_expenses           := L_get_comp_items.unit_other_exp_cost * L_get_comp_items.quantity;
            documentLineItem.insurance                := L_get_comp_items.unit_insurance_cost * L_get_comp_items.quantity;
            ---
            documentLineItem.item_type        := L_get_comp_items.classification;
            documentLineItem.document_line_id := L_get_doc_detail.fiscal_doc_line_id;
            documentLineItem.item_id          := L_get_comp_items.item;
            ---
            open C_COMP_ITEM_CUSTOM_ATTRIB(L_get_comp_items.item);
            fetch C_COMP_ITEM_CUSTOM_ATTRIB bulk collect into L_item_name_value_tbl;
            close C_COMP_ITEM_CUSTOM_ATTRIB;
            if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                          L_item_name_value_tbl,
                                                          'ITEM') = FALSE then
               return FALSE;
            end if;
             if L_item_name_value_tbl.exists(1) then
            for i in  L_item_name_value_tbl.FIRST .. L_item_name_value_tbl.LAST loop
               tbl_doc_comp_item_nameval.extend();
                     L_doc_comp_item_nameval := L_doc_comp_item_nameval + 1;
                           doc_comp_item_nameval.name := L_item_name_value_tbl(i).O_name;
                           doc_comp_item_nameval.value := L_item_name_value_tbl(i).O_value;
                           tbl_doc_comp_item_nameval(L_doc_comp_item_nameval) := doc_comp_item_nameval;
            end loop;
            end if;
            documentLineItem.NameValPairRBO_TBL      := tbl_doc_comp_item_nameval;
            ---
            documentLineItem.quantity         := L_get_comp_items.quantity;
            documentLineItem.unit_cost        := L_get_comp_items.unit_cost;
            documentLineItem.total_cost       := L_get_comp_items.quantity * L_get_comp_items.unit_cost;
            documentLineItem.unit_of_measure  := L_get_comp_items.standard_uom;
            ---
            documentLineItem.item_tran_code   := L_get_doc_detail.nf_cfop;
            ---
            documentLineItem.icms_cst_code       := L_get_doc_detail.icms_cst;
             if L10N_BR_INT_SQL.LOAD_UOM_INFO(O_error_message,
                                             L_tbl_documentLineItem,
                                             null,
                                             L_get_comp_items.item,
                                             L_get_comp_items.quantity,
                                             L_get_comp_items.standard_uom,
                                             L_get_doc_detail.requisition_no,
                                             'NF') = FALSE then
                return FALSE;
            end if;
           if L_tbl_documentLineItem is not NULL and L_tbl_documentLineItem.COUNT > 0 then
                          select "RIB_LineItemRBO_REC"(0,
                                           item_table.document_line_id,
                                           item_table.item_id,
                                           item_table.item_tran_Code,
                                           item_table.item_type,
                                           item_table.quantity,
                                           item_table.unit_of_measure,
                                           item_table.quantity_in_eaches,
                                           item_table.origin_doc_date,
                                           item_table.pack_item_id,
                                           item_table.total_cost,
                                           item_table.unit_cost,
                                           item_table.dim_object,
                                           item_table.src_taxpayer_type,
                                           item_table.orig_fiscal_doc_number,
                                           item_table.orig_fiscal_doc_series,
                                           item_table.length,
                                           item_table.width,
                                           item_table.lwh_uom,
                                           item_table.weight,
                                           item_table.net_weight,
                                           item_table.weight_uom,
                                           item_table.liquid_volume,
                                           item_table.liquid_volume_uom,
                                           item_table.freight,
                                           item_table.insurance,
                                           item_table.discount,
                                           item_table.commision,
                                           item_table.freight_type,
                                           item_table.other_expenses,
                                           item_table.origin_fiscal_code_opr,
                                           item_table.deduced_fiscal_code_opr,
                                           item_table.deduce_cfop_code,
                                           item_table.icms_cst_code,
                                           item_table.pis_cst_code,
                                           item_table.cofins_cst_code,
                                           item_table.deduce_icms_cst_code,
                                           item_table.deduce_pis_cst_code,
                                           item_table.deduce_cofins_cst_code,
                                           item_table.recoverable_icmsst,
                                           item_table.item_cost_contains_cofins,
                                           item_table.recoverable_base_icmsst,
                                           item_table.item_cost_contains_pis,
                                           item_table.item_cost_contains_icms,
                                           item_table.PrdItemRBO_TBL,
                                           item_table.SvcItemRBO_TBL,
                                           item_table.InformTaxRBO_TBL,
                                           item_table.NameValPairRBO_TBL)
                    into L_doclineItemRBO
                 from TABLE(L_tbl_documentLineItem) item_table;
                 --linedimensions := L_doclineItemRBO.iSCDimDesc;
            if (L_doclineItemRBO.item_id is not NULL) then
               documentLineItem.dim_object := L_doclineItemRBO.dim_object;
               documentLineItem.length := L_doclineItemRBO.length;
               documentLineItem.width := L_doclineItemRBO.width;
               documentLineItem.lwh_uom := L_doclineItemRBO.lwh_uom;
               documentLineItem.weight := L_doclineItemRBO.weight;
               documentLineItem.net_weight := L_doclineItemRBO.net_weight;
               documentLineItem.weight_uom := L_doclineItemRBO.weight_uom;
               documentLineItem.liquid_volume := L_doclineItemRBO.liquid_volume;
               documentLineItem.liquid_volume_uom := L_doclineItemRBO.liquid_volume_uom;
            end if;
                L_tbl_documentLineItem := null;
            end if;
--          for item_uom in C_GET_UOM_ITEM(L_get_comp_items.item) loop
--                           documentLineItem.dim_object := 'EA';
--                           documentLineItem.item_id := item_uom.item;
--                --documentLineItem.supplier := L_ent_supp.key_value_1;
--                --documentLineItem.origin_country := item_uom.origin_country_id;
--            end loop;
            ---
            if L_get_doc_detail.icms_cst is NOT NULL then
            ---
               L_deduce_icms_cst := 'N';
            ---
            else
               L_deduce_icms_cst := 'Y';
            end if;
            ---
            documentLineItem.pis_cst_code        := L_get_doc_detail.pis_cst;
            ---
            if L_get_doc_detail.pis_cst is NOT NULL then
            ---
               L_deduce_pis_cst := 'N';
            ---
            else
               L_deduce_pis_cst := 'Y';
            end if;
            ---
            documentLineItem.cofins_cst_code     := L_get_doc_detail.cofins_cst;
            ---
            if L_get_doc_detail.cofins_cst is NOT NULL then
            ---
               L_deduce_cofins_cst := 'N';
            ---
            else
               L_deduce_cofins_cst := 'Y';
            end if;
            ---
            documentLineItem.deduce_icms_cst_code   := L_deduce_icms_cst;
            documentLineItem.deduce_pis_cst_code    := L_deduce_pis_cst;
            documentLineItem.deduce_cofins_cst_code := L_deduce_cofins_cst;
            ---
            ---
            if L_triang_po_exists and L_get_doc_header.comp_nf_ind = 'Y' then
               documentLineItem.orig_fiscal_doc_number   := L_fm_other_header_det.fiscal_doc_no;
               documentLineItem.origin_fiscal_code_opr      := L_fm_other_detail_det.nf_cfop;
               documentLineItem.origin_doc_date          := L_fm_other_detail_det.issue_date;
               documentLineItem.orig_fiscal_doc_series   := L_fm_other_header_det.series_no;
               documentLineItem.src_taxpayer_type     := L_ent_main_nf_supp.cnpj;
            end if;
            --triang
            if L_get_doc_detail.service_ind = 'Y' then
               ---
               serviceItem                :=  "RIB_SvcItemRBO_REC"(0,null,null,null,null,null,null,null);
               ---
               serviceItem.item_code                  := L_get_comp_items.item;
               serviceItem.item_description           := L_get_comp_items.item_desc;
               serviceItem.federal_service_code       := L_get_comp_items.federal_service;
               serviceItem.service_provider_city      := L_get_comp_items.city;
               serviceItem.ext_fiscal_class_code      := L_get_comp_items.service_code;
               serviceItem.NameValPairRBO_TBL         := tbl_comp_item_nameval;
               ---
               tbl_serviceItem.extend();
               tbl_serviceItem(1) := serviceItem;
               documentLineItem.SvcItemRBO_TBL        := tbl_serviceItem;
               ---
            elsif L_get_doc_detail.service_ind = 'N' then
               --Product Details
               productItem             := "RIB_PrdItemRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null);
               ---
               productItem.item_code                   := L_get_comp_items.item;
               productItem.item_description            := L_get_comp_items.item_desc;
               productItem.fiscal_classification_code  := L_get_comp_items.ncm_char_code;
               productItem.ext_fiscal_class_code       := L_get_comp_items.ncm_ex_char_code;
               productItem.item_origin                 := L_get_comp_items.origin_code;
               productItem.item_utilization            := L_item_util;
               productItem.is_transformed_item         := L_transformed_item;
               productItem.state_of_manufacture        := L_get_doc_detail.state_of_manufacture;
               productItem.pharma_list_type            := L_get_doc_detail.pharma_list_type;
               productItem.NameValPairRBO_TBL          := tbl_comp_item_nameval;
               ---
               tbl_productItem.extend();
               tbl_productItem(1) := productItem;
               documentLineItem.PrdItemRBO_TBL         := tbl_productItem;
               ---
            end if;
            ---
            if L_get_doc_header.operation_type = L_inbound then
                FOR C_vat_code in L_vat_code_tab.first .. L_vat_code_tab.last LOOP
                   ---
                   if L_vat_code_tab(C_vat_code).vat_code = L_icms then
                      documentLineItem.item_cost_contains_icms := NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                   elsif L_vat_code_tab(C_vat_code).vat_code = L_cofins then
                      documentLineItem.item_cost_contains_cofins  := NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                   elsif L_vat_code_tab(C_vat_code).vat_code = L_pis then
                      documentLineItem.item_cost_contains_pis  :=  NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
                   end if;
                   ---
                END LOOP;
                ---
            else
                documentLineItem.item_cost_contains_cofins := L_item_attr;
                documentLineItem.item_cost_contains_pis    := L_item_attr;
                documentLineItem.item_cost_contains_icms   := L_item_attr;
            end if;
            ---
            documentLineItem.deduce_cfop_code          := L_deduced_cfop;
            documentLineItem.recoverable_base_icmsst   := L_get_doc_detail.recoverable_base;
            documentLineItem.recoverable_icmsst        := L_get_doc_detail.recoverable_value;
            ---
            L_tax_number  := 1;
            ---
             open C_GET_COMP_ITEM_TAXES (L_get_doc_detail.fiscal_doc_line_id, L_get_comp_items.item);
             ---
             LOOP
             ---
                fetch C_GET_COMP_ITEM_TAXES into L_get_comp_items_taxes;
                EXIT when C_GET_COMP_ITEM_TAXES%NOTFOUND;
                ---
                linetaxdetailRBO      := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);
                ---
                linetaxdetailRBO.tax_code                 := L_get_comp_items_taxes.tax_code;
                linetaxdetailRBO.tax_basis_amount         := L_get_comp_items_taxes.tax_basis;
                linetaxdetailRBO.modified_tax_basis_amount         := L_get_comp_items_taxes.modified_tax_basis;
                linetaxdetailRBO.tax_amount               := L_get_comp_items_taxes.tax_value;
                linetaxdetailRBO.tax_rate                 := L_get_comp_items_taxes.tax_rate;
                ---
                tbl_taxdetail.extend();
                tbl_taxdetail(L_tax_number)            := linetaxdetailRBO;
                L_tax_number                           := L_tax_number + 1;
                ---
             END LOOP;
             ---
             close C_GET_COMP_ITEM_TAXES;
             ---
--            documentLineItem.InformTaxRBO_TBL           := tbl_taxdetail;
            tbl_documentLineItem.extend();
            tbl_documentLineItem(L_item_number)      := documentLineItem;
            L_item_number:=L_item_number + 1;
            ---
         ---
         END LOOP;
         ---
         close C_GET_COMP_ITEMS;
         ---
      else
         ---
         documentLineItem        := "RIB_LineItemRBO_REC"(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
         if L_triang_po_exists and L_get_doc_header.comp_nf_ind = 'Y' then
             ---
             open C_FM_OTHER_DETAIL_DET(L_other_doc_id, L_get_doc_detail.requisition_no, L_get_doc_detail.item);
             ---
             FETCH C_FM_OTHER_DETAIL_DET into L_fm_other_detail_det;
             ---
             close C_FM_OTHER_DETAIL_DET;
             ---
             open C_FM_OTHER_HEADER_DET(L_other_doc_id);
            ---
             FETCH C_FM_OTHER_HEADER_DET into L_fm_other_header_det;
            ---
             close C_FM_OTHER_HEADER_DET;
             ---
             open C_GET_ENTITY_ATTRIBUTES(L_fm_other_header_det.module, L_fm_other_header_det.key_value_1, L_fm_other_header_det.key_value_2);
             ---
             fetch C_GET_ENTITY_ATTRIBUTES into L_ent_main_nf_supp;
             ---
             if C_GET_ENTITY_ATTRIBUTES%NOTFOUND then
                O_status := 0;
                O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_MAN_SOURCE',TO_CHAR(L_fm_other_header_det.key_value_1),NULL,NULL);
                return FALSE;
             end if;
             ---
             close C_GET_ENTITY_ATTRIBUTES;
             ---
             documentLineItem.discount                 := L_fm_other_detail_det.total_disc;
             documentLineItem.freight                  := L_fm_other_detail_det.freight_cost;
             documentLineItem.other_expenses           := L_fm_other_detail_det.other_expenses_cost;
             documentLineItem.insurance                := L_fm_other_detail_det.insurance_cost;
             documentLineItem.origin_fiscal_code_opr   := L_fm_other_detail_det.nf_cfop;
             documentLineItem.origin_doc_date          := L_fm_other_detail_det.issue_date;
             documentLineItem.orig_fiscal_doc_number   := L_fm_other_header_det.fiscal_doc_no;
             documentLineItem.orig_fiscal_doc_series   := L_fm_other_header_det.series_no;
             documentLineItem.src_taxpayer_type        := L_ent_main_nf_supp.cnpj;
          ---
          else
          ---
             documentLineItem.discount                 := L_get_doc_detail.total_disc;
             documentLineItem.freight                  := L_get_doc_detail.freight_cost;
             documentLineItem.other_expenses           := L_get_doc_detail.other_expenses_cost;
             documentLineItem.insurance                := L_get_doc_detail.insurance_cost;
          ---
          end if;
          ---
         documentLineItem.item_type        := L_get_doc_detail.classification;
         documentLineItem.document_line_id := L_get_doc_detail.fiscal_doc_line_id;
         documentLineItem.item_id          := L_get_doc_detail.item;
         ---
         open C_DETAIL_ITEM_CUSTOM_ATTRIB(L_get_doc_detail.item);
         fetch C_DETAIL_ITEM_CUSTOM_ATTRIB bulk collect into L_item_name_value_tbl;
         close C_DETAIL_ITEM_CUSTOM_ATTRIB;
         if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                       L_item_name_value_tbl,
                                                       'ITEM') = FALSE then
            return FALSE;
         end if;
          if L_item_name_value_tbl.exists(1) then
         for i in  L_item_name_value_tbl.FIRST .. L_item_name_value_tbl.LAST loop
            tbl_doc_detail_item_nameval.extend();
                  L_doc_detail_item_nameval := L_doc_detail_item_nameval + 1;
                        doc_detail_item_nameval.name := L_item_name_value_tbl(i).O_name;
                        doc_detail_item_nameval.value := L_item_name_value_tbl(i).O_value;
                        tbl_doc_detail_item_nameval(L_doc_detail_item_nameval) := doc_detail_item_nameval;
         end loop;
         end if;
         documentLineItem.NameValPairRBO_TBL      := tbl_doc_detail_item_nameval;
         ---
         documentLineItem.quantity         := L_po_qty;
         documentLineItem.unit_cost        := L_po_cost;
         documentLineItem.total_cost       := L_po_qty * L_po_cost;
         documentLineItem.unit_of_measure  := L_get_doc_detail.standard_uom;
         ---
         documentLineItem.item_tran_code   := L_get_doc_detail.nf_cfop;
         ---
         documentLineItem.icms_cst_code       := L_get_doc_detail.icms_cst;
         ---
          if L10N_BR_INT_SQL.LOAD_UOM_INFO(O_error_message,
                                    L_tbl_documentLineItem,
                                    null,
                                    L_get_doc_detail.item,
                                    L_get_doc_detail.quantity,
                                    L_get_doc_detail.standard_uom,
                                    L_get_doc_detail.requisition_no,
                                    'NF') = FALSE then
                 return FALSE;
           end if;
           if L_tbl_documentLineItem is not NULL and L_tbl_documentLineItem.COUNT > 0 then
                select "RIB_LineItemRBO_REC"(0,
                                             item_table.document_line_id,
                                             item_table.item_id,
                                             item_table.item_tran_Code,
                                             item_table.item_type,
                                             item_table.quantity,
                                             item_table.unit_of_measure,
                                             item_table.quantity_in_eaches,
                                             item_table.origin_doc_date,
                                             item_table.pack_item_id,
                                             item_table.total_cost,
                                             item_table.unit_cost,
                                             item_table.dim_object,
                                             item_table.src_taxpayer_type,
                                             item_table.orig_fiscal_doc_number,
                                             item_table.orig_fiscal_doc_series,
                                             item_table.length,
                                             item_table.width,
                                             item_table.lwh_uom,
                                             item_table.weight,
                                             item_table.net_weight,
                                             item_table.weight_uom,
                                             item_table.liquid_volume,
                                             item_table.liquid_volume_uom,
                                             item_table.freight,
                                             item_table.insurance,
                                             item_table.discount,
                                             item_table.commision,
                                             item_table.freight_type,
                                             item_table.other_expenses,
                                             item_table.origin_fiscal_code_opr,
                                             item_table.deduced_fiscal_code_opr,
                                             item_table.deduce_cfop_code,
                                             item_table.icms_cst_code,
                                             item_table.pis_cst_code,
                                             item_table.cofins_cst_code,
                                             item_table.deduce_icms_cst_code,
                                             item_table.deduce_pis_cst_code,
                                             item_table.deduce_cofins_cst_code,
                                             item_table.recoverable_icmsst,
                                             item_table.item_cost_contains_cofins,
                                             item_table.recoverable_base_icmsst,
                                             item_table.item_cost_contains_pis,
                                             item_table.item_cost_contains_icms,
                                             item_table.PrdItemRBO_TBL,
                                             item_table.SvcItemRBO_TBL,
                                             item_table.InformTaxRBO_TBL,
                                             item_table.NameValPairRBO_TBL)
                           into L_doclineItemRBO
                           from TABLE(L_tbl_documentLineItem) item_table;
                if (L_doclineItemRBO.item_id is not NULL) then
                   documentLineItem.dim_object := L_doclineItemRBO.dim_object;
                   documentLineItem.length := L_doclineItemRBO.length;
                   documentLineItem.width := L_doclineItemRBO.width;
                   documentLineItem.lwh_uom := L_doclineItemRBO.lwh_uom;
                   documentLineItem.weight := L_doclineItemRBO.weight;
                   documentLineItem.net_weight := L_doclineItemRBO.net_weight;
                   documentLineItem.weight_uom := L_doclineItemRBO.weight_uom;
                   documentLineItem.liquid_volume := L_doclineItemRBO.liquid_volume;
                   documentLineItem.liquid_volume_uom := L_doclineItemRBO.liquid_volume_uom;
                end if;
                L_tbl_documentLineItem := null;
            end if;
--         for item_uom in C_GET_UOM_ITEM(L_get_doc_detail.item) loop
--                        documentLineItem.dim_object := 'EA';
--                        documentLineItem.item_id := item_uom.item;
--                --linedimensions.supplier_ := item_uom.supplier;
--                --linedimensions.origin_country_ := item_uom.origin_country_id;
--               end loop;
         if L_get_doc_detail.icms_cst is NOT NULL then
            ---
               L_deduce_icms_cst := 'N';
            ---
            else
               L_deduce_icms_cst := 'Y';
            end if;
            ---
            documentLineItem.pis_cst_code        := L_get_doc_detail.pis_cst;
            ---
            if L_get_doc_detail.pis_cst is NOT NULL then
            ---
               L_deduce_pis_cst := 'N';
            ---
            else
               L_deduce_pis_cst := 'Y';
            end if;
            ---
            documentLineItem.cofins_cst_code     := L_get_doc_detail.cofins_cst;
            ---
            if L_get_doc_detail.cofins_cst is NOT NULL then
            ---
               L_deduce_cofins_cst := 'N';
            ---
            else
               L_deduce_cofins_cst := 'Y';
            end if;
            ---
         documentLineItem.deduce_icms_cst_code   := L_deduce_icms_cst;
         documentLineItem.deduce_pis_cst_code    := L_deduce_pis_cst;
         documentLineItem.deduce_cofins_cst_code := L_deduce_cofins_cst;
         ---
--         open C_DETAIL_ITEM_CUSTOM_ATTRIB(L_get_doc_detail.item);
--         fetch C_DETAIL_ITEM_CUSTOM_ATTRIB bulk collect into L_item_name_value_tbl;
--         close C_DETAIL_ITEM_CUSTOM_ATTRIB;
--         if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
--                                                       L_item_name_value_tbl,
--                                                       'ITEM') = FALSE then
--            return FALSE;
--         end if;
--         for i in  L_item_name_value_tbl.FIRST .. L_item_name_value_tbl.LAST loop
--            tbl_detail_item_nameval.extend();
--                L_detail_item_nameval := L_detail_item_nameval + 1;
--                      detail_item_nameval.name := L_item_name_value_tbl(i).O_name;
--                      detail_item_nameval.value := L_item_name_value_tbl(i).O_value;
--                      tbl_detail_item_nameval(L_detail_item_nameval) := detail_item_nameval;
--         end loop;
         if L_get_doc_detail.service_ind = 'Y' then
               ---
            serviceItem                :=  "RIB_SvcItemRBO_REC"(0,null,null,null,null,null,null,null);
            ---
            serviceItem.item_code                  := L_get_doc_detail.item;
            serviceItem.item_description           := L_get_doc_detail.item_desc;
            serviceItem.federal_service_code       := L_get_doc_detail.federal_service;
            serviceItem.service_provider_city      := L_get_doc_detail.city;
            serviceItem.ext_fiscal_class_code      := L_get_doc_detail.service_code;
            serviceItem.NameValPairRBO_TBL         := tbl_detail_item_nameval;
               ---
               tbl_serviceItem.extend();
            tbl_serviceItem(1) := serviceItem;
            documentLineItem.SvcItemRBO_TBL        := tbl_serviceItem;
               ---
         elsif L_get_doc_detail.service_ind = 'N' then
               --Product Details
            productItem             := "RIB_PrdItemRBO_REC"(0,null,null,null,null,null,null,null,null,null,null,null,null);
            productItem.item_code                   := L_get_doc_detail.item;
            productItem.item_description            := L_get_doc_detail.item_desc;
            productItem.fiscal_classification_code  := L_get_doc_detail.ncm_char_code;
            productItem.ext_fiscal_class_code       := L_get_doc_detail.ncm_ex_char_code;
            productItem.item_origin                 := L_get_doc_detail.origin_code;
            productItem.item_utilization            := L_item_util;
            productItem.is_transformed_item         := L_transformed_item;
            productItem.state_of_manufacture        := L_get_doc_detail.state_of_manufacture;
            productItem.pharma_list_type            := L_get_doc_detail.pharma_list_type;
            productItem.NameValPairRBO_TBL          := tbl_detail_item_nameval;
               ---
               tbl_productItem.extend();
            tbl_productItem(1) := productItem;
            documentLineItem.PrdItemRBO_TBL         := tbl_productItem;
               ---
         end if;
         ---
         if L_get_doc_header.operation_type = L_inbound then
            FOR C_vat_code in L_vat_code_tab.first .. L_vat_code_tab.last LOOP
               ---
               if L_vat_code_tab(C_vat_code).vat_code = L_icms then
                  documentLineItem.item_cost_contains_icms := NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
               elsif L_vat_code_tab(C_vat_code).vat_code = L_cofins then
                  documentLineItem.item_cost_contains_cofins  := NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
               elsif L_vat_code_tab(C_vat_code).vat_code = L_pis then
                  documentLineItem.item_cost_contains_pis  :=  NVL(L_vat_code_tab(C_vat_code).incl_nic_ind,'N');
               end if;
               ---
            END LOOP;
            ---
        else
            documentLineItem.item_cost_contains_cofins := L_item_attr;
            documentLineItem.item_cost_contains_pis    := L_item_attr;
            documentLineItem.item_cost_contains_icms   := L_item_attr;
        end if;
        ---
        documentLineItem.deduce_cfop_code          := L_deduced_cfop;
        documentLineItem.recoverable_base_icmsst   := L_get_doc_detail.recoverable_base;
        documentLineItem.recoverable_icmsst        := L_get_doc_detail.recoverable_value;
        ---
        L_tax_number := 1;
        ---
        open C_GET_LINE_TAXES (L_get_doc_detail.fiscal_doc_line_id);
        ---
        LOOP
        ---
           fetch C_GET_LINE_TAXES into L_get_line_taxes;
           EXIT when C_GET_LINE_TAXES%NOTFOUND;
           ---
           linetaxdetailRBO      := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);
           ---
           linetaxdetailRBO.tax_code                 := L_get_line_taxes.tax_code;
           linetaxdetailRBO.tax_basis_amount         := L_get_line_taxes.tax_basis;
           linetaxdetailRBO.modified_tax_basis_amount         := L_get_line_taxes.modified_tax_basis;
           linetaxdetailRBO.tax_amount               := L_get_line_taxes.tax_value;
           linetaxdetailRBO.tax_rate                 := L_get_line_taxes.tax_rate;
           ---
           tbl_taxdetail.extend();
           tbl_taxdetail(L_tax_number)            := linetaxdetailRBO;
           L_tax_number                           := L_tax_number + 1;
           ---
        END LOOP;
        ---
        close C_GET_LINE_TAXES;
        ---
      --  documentLineItem.InformTaxRBO_TBL           := tbl_taxdetail;
        tbl_documentLineItem.extend();
        tbl_documentLineItem(L_item_number)      := documentLineItem;
        L_item_number:=L_item_number + 1;
        ---
      end if;
      ---
   END LOOP;
   ---
   close C_GET_DOC_DETAIL;
   ---
   fiscalDocumentRBO.fiscal_document_date        := L_get_doc_header.issue_date;
   fiscalDocumentRBO.due_date                    := NVL(L_get_payment_date,sysdate);
   fiscalDocumentRBO.receipt_date                := L_get_doc_header.entry_or_exit_date;
   fiscalDocumentRBO.document_type               := L_doc_type;
   fiscalDocumentRBO.transaction_type            := L_transac_type;
   fiscalDocumentRBO.operation_type              := L_get_doc_header.operation_type;
   fiscalDocumentRBO.is_supplier_issuer          := L_supp_issuer;
   fiscalDocumentRBO.gross_weight                := L_get_doc_header.total_weight;
   fiscalDocumentRBO.net_weight                  := L_get_doc_header.net_weight;
   fiscalDocumentRBO.calc_process_type           := L_calc_process_type;
   fiscalDocumentRBO.no_history_tracked          := L_no_rec_st;
   ---
   if L_triang_po_exists and L_get_doc_header.comp_nf_ind = 'Y' then
   ---
      open C_FM_OTHER_HEADER_DET(L_other_doc_id);
      ---
      FETCH C_FM_OTHER_HEADER_DET into L_fm_other_header_det;
      ---
      close C_FM_OTHER_HEADER_DET;
      ---
      fiscalDocumentRBO.freight                  := L_fm_other_header_det.freight_cost;
      fiscalDocumentRBO.freight_type             := L_fm_other_header_det.freight_type;
      fiscalDocumentRBO.other_expenses           := L_fm_other_header_det.other_expenses_cost;
      fiscalDocumentRBO.insurance                := L_fm_other_header_det.insurance_cost;
   ---
   else
   ---
      fiscalDocumentRBO.freight                  := L_get_doc_header.freight_cost;
      fiscalDocumentRBO.freight_type             := L_get_doc_header.freight_type;
      fiscalDocumentRBO.other_expenses           := L_get_doc_header.other_expenses_cost;
      fiscalDocumentRBO.insurance                := L_get_doc_header.insurance_cost;
   ---
   end if;
   ---
   L_tax_number := 1;
   ---
   open C_GET_HEADER_TAXES;
   ---
   LOOP
   ---
      fetch C_GET_HEADER_TAXES into L_get_header_taxes;
      EXIT when C_GET_HEADER_TAXES%NOTFOUND;
      ---
      headertaxdetailRBO      := "RIB_InformTaxRBO_REC"(0,null,null,null,null,null,null,null);
      ---
      headertaxdetailRBO.tax_code                 := L_get_header_taxes.tax_code;
      headertaxdetailRBO.tax_basis_amount         := L_get_header_taxes.tax_basis;
      headertaxdetailRBO.modified_tax_basis_amount         := L_get_header_taxes.modified_tax_basis;
      headertaxdetailRBO.tax_amount               := L_get_header_taxes.tax_value;
    --  headertaxdetailRBO.is_tax_retention_         := 'N';
      ---
      tbl_taxheader.extend();
      tbl_taxheader(L_tax_number)            := headertaxdetailRBO;
      L_tax_number                           := L_tax_number + 1;
      ---
   END LOOP;
   ---
   close C_GET_HEADER_TAXES;
   ---
   fiscalDocumentRBO.fromentity_tbl             := tbl_sources;
   fiscalDocumentRBO.toentity_tbl        := tbl_destinations;
   fiscalDocumentRBO.lineitemrbo_tbl      := tbl_documentLineItem;
  --fiscalDocumentRBO.InformTaxRBO_TBL        := tbl_taxheader;
   ---
   fiscalDocumentRBO.document_number        := L_get_doc_header.fiscal_doc_no;
   fiscalDocumentRBO.document_series        := L_get_doc_header.series_no;
   fiscalDocumentRBO.ignore_tax_calc_list   := SUBSTR(L_ignore_tax,2);
   fiscalDocumentRBO.nature_of_operation    := L_get_doc_header.nop;
   --
   tbl_fiscalDocument_request.extend();
   tbl_fiscalDocument_request(1)            := fiscalDocumentRBO;
   --
   fiscDocChnkRBM.chunk_id       := 1;
   fiscDocChnkRBM.fiscdocrbo_tbl := tbl_fiscalDocument_request;
   tbl_fiscDocChnkRBM.extend();
   tbl_fiscDocChnkRBM(1)         := fiscDocChnkRBM;
   ---
   fiscalDocumentColRBM.fiscDocChnkRBO_TBL  := tbl_fiscDocChnkRBM;
   ---
   O_businessObject := fiscalDocumentColRBM;
   ---
   return TRUE;

EXCEPTION
      ---
   when OTHERS then
      if C_GET_DOC_HEADER%ISOPEN then
         close C_GET_DOC_HEADER;
      end if;
      ---
      if C_GET_DOC_DETAIL%ISOPEN then
         close C_GET_DOC_DETAIL;
      end if;
      ---
      if C_GET_PAYMENT_DATE%ISOPEN then
         close C_GET_PAYMENT_DATE;
      end if;
      ---
      if C_GET_ENTITY_ATTRIBUTES%ISOPEN then
         close C_GET_ENTITY_ATTRIBUTES;
      end if;
      ---

      if C_GET_TRIANG_IND%ISOPEN then
         close C_GET_TRIANG_IND;
      end if;
      ---
      if C_FM_TRIANG_OTHER_DOC%ISOPEN then
         close C_FM_TRIANG_OTHER_DOC;
      end if;
      ---
      if C_FM_OTHER_HEADER_DET%ISOPEN then
         close C_FM_OTHER_HEADER_DET;
      end if;
      ---
      if C_FM_OTHER_DETAIL_DET%ISOPEN then
         close C_FM_OTHER_DETAIL_DET;
      end if;
      ---
      if C_GET_COMP_ITEMS%ISOPEN then
         close C_GET_COMP_ITEMS;
      end if;
      ---
      if C_GET_COMP_ITEM_TAXES%ISOPEN then
         close C_GET_COMP_ITEM_TAXES;
      end if;
      ---
      if C_GET_LINE_TAXES%ISOPEN then
         close C_GET_LINE_TAXES;
      end if;
      ---
      if C_GET_HEADER_TAXES%ISOPEN then
         close C_GET_HEADER_TAXES;
      end if;
      ---
      if C_GET_COST_RESL%ISOPEN then
         close C_GET_COST_RESL;
      end if;
      ---
      if C_GET_QTY_RESL%ISOPEN then
         close C_GET_QTY_RESL;
      end if;
      ---
      if C_NF_FOR_HIST_UPD%ISOPEN then
         close C_NF_FOR_HIST_UPD;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      O_status := 0;
      return FALSE;
---
END LOAD_HIST_OBJECT;
------------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_RESULTS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tax_service_id  IN      FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE,
                       I_businessObject  IN      "RIB_FiscDocTaxColRBM_REC")
RETURN BOOLEAN IS
   PRAGMA AUTONOMOUS_TRANSACTION;
   L_program        VARCHAR2(61) := 'FM_EXT_TAXES_SQL.STAGE_RESULTS';
   L_hdr_tbl        RESULT_DATA_HDR_TBL;
   L_hdr_ctr        NUMBER := NULL;
   L_hdr_tax_tbl    RESULT_DATA_HDR_TAX_TBL;
   L_hdr_tax_ctr    NUMBER := NULL;
   L_item_tbl       RESULT_DATA_ITEM_TBL;
   L_item_ctr       NUMBER := NULL;
   L_item_tax_tbl   RESULT_DATA_ITEM_TAX_TBL;
   L_item_tax_ctr   NUMBER := NULL;
   L_sug_tbl        RESULT_DATA_RULE_TBL;
   L_sug_ctr        NUMBER := NULL;
   L_fiscal_doc_id  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   cursor C_GET_FISCAL_DOC_ID is
      select fiscal_doc_id
        from fm_tax_call_stage
       where tax_service_id = I_tax_service_id;
BEGIN
   OPEN C_GET_FISCAL_DOC_ID;
   FETCH C_GET_FISCAL_DOC_ID into L_fiscal_doc_id;
   CLOSE C_GET_FISCAL_DOC_ID;
   if I_businessObject is not NULL and
      I_businessObject.FiscDocChnkTaxRBO_TBL is not NULL and
      I_businessObject.FiscDocChnkTaxRBO_TBL.COUNT > 0 then
      for a in I_businessObject.FiscDocChnkTaxRBO_TBL.FIRST..I_businessObject.FiscDocChnkTaxRBO_TBL.LAST LOOP

         if I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL is not NULL and
            I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL.COUNT > 0 then
            L_hdr_ctr := 1;
            for i in I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL.FIRST..I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL.LAST LOOP

               L_hdr_tbl(L_hdr_ctr).fiscal_doc_id := L_fiscal_doc_id;
               L_hdr_tbl(L_hdr_ctr).total_merch_cost := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).disc_merch_cost;
               L_hdr_tbl(L_hdr_ctr).total_cost := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).total_cost;
               L_hdr_tbl(L_hdr_ctr).total_services_cost := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).total_services_cost;
               L_hdr_tbl(L_hdr_ctr).calculation_status := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).calculation_status;
                     if I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).TaxDetRBO_TBL is not NULL and
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).TaxDetRBO_TBL.COUNT > 0 then
                        if L_hdr_tax_ctr is NULL then
                     L_hdr_tax_ctr := 1;
                  end if;
                        for k in I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).TaxDetRBO_TBL.FIRST..
                            I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).TaxDetRBO_TBL.LAST LOOP
                           L_hdr_tax_tbl(L_hdr_tax_ctr).fiscal_doc_id      := L_fiscal_doc_id;
                     L_hdr_tax_tbl(L_hdr_tax_ctr).tax_code :=
                                   I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).TaxDetRBO_TBL(k).tax_code;
                           L_hdr_tax_tbl(L_hdr_tax_ctr).tax_amount :=
                                   I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).TaxDetRBO_TBL(k).tax_amount;
                           L_hdr_tax_tbl(L_hdr_tax_ctr).tax_basis_amount :=
                                   I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).TaxDetRBO_TBL(k).tax_basis_amount;
                           L_hdr_tax_tbl(L_hdr_tax_ctr).modified_tax_basis_amount :=
                                   I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).TaxDetRBO_TBL(k).modified_tax_basis_amount;
                           L_hdr_tax_ctr := L_hdr_tax_ctr + 1;
                        end LOOP;
               end if;
               if I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL is not NULL and
                  I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL.COUNT > 0 then
                  if L_item_ctr is NULL then
                     L_item_ctr := 1;
                  end if;
                  for j in I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL.FIRST..
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL.LAST LOOP
                     L_item_tbl(L_item_ctr).fiscal_doc_id := L_fiscal_doc_id;
                      if (I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).pack_item_id is not null) then
                         L_item_tbl(L_item_ctr).fiscal_doc_line_id := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).pack_item_id;
                      else
                          L_item_tbl(L_item_ctr).fiscal_doc_line_id := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).document_line_id;
                      end if;

                     L_item_tbl(L_item_ctr).item :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).item_id;
                     L_item_tbl(L_item_ctr).taxed_quantity :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).taxed_quantity;
                     L_item_tbl(L_item_ctr).taxed_quantity_uom :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).taxed_quantity_uom;
                     L_item_tbl(L_item_ctr).entry_cfop :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).deduced_fiscal_code_opr || ',' ||I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).item_tran_code;
                     L_item_tbl(L_item_ctr).icms_cst_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).icms_cst_code;
                     L_item_tbl(L_item_ctr).pis_cst_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).pis_cst_code;
                     L_item_tbl(L_item_ctr).cofins_cst_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).cofins_cst_code;
                     L_item_tbl(L_item_ctr).icms_cst_ind :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).deduce_icms_cst_code;
                     L_item_tbl(L_item_ctr).pis_cst_ind :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).deduce_pis_cst_code;
                     L_item_tbl(L_item_ctr).cofins_cst_ind :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).deduce_cofins_cst_code;
                     L_item_tbl(L_item_ctr).recoverable_icmsst :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).recoverable_icmsst;
                     L_item_tbl(L_item_ctr).total_cost_with_icms :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).total_cost_with_icms;
                     L_item_tbl(L_item_ctr).unit_cost_with_icms :=
                                    I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).unit_cost_with_icms;
                     L_item_tbl(L_item_ctr).recoverable_base_icmsst :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).recoverable_base_icmsst;
                ---
                     if I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL is not NULL and
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL.COUNT > 0 then
                        if L_item_tax_ctr is NULL then
                           L_item_tax_ctr := 1;
                        end if;
                        for k in I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL.FIRST..
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL.LAST LOOP
                              L_item_tax_tbl(L_item_tax_ctr).fiscal_doc_id := L_fiscal_doc_id;
                                       --L_item_tax_tbl(L_item_tax_ctr).fiscal_doc_line_id := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).document_line_id;
                           if (I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).pack_item_id is not null) then
                            L_item_tax_tbl(L_item_tax_ctr).fiscal_doc_line_id := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).pack_item_id;
                           else
                            L_item_tax_tbl(L_item_tax_ctr).fiscal_doc_line_id := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).document_line_id;
                           end if;

                           L_item_tax_tbl(L_item_tax_ctr).item := I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).item_id;
                           L_item_tax_tbl(L_item_tax_ctr).legal_message :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).legal_message;
                           L_item_tax_tbl(L_item_tax_ctr).tax_amount :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_amount;
                           L_item_tax_tbl(L_item_tax_ctr).tax_basis_amount :=
                                                I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_basis_amount;
                           L_item_tax_tbl(L_item_tax_ctr).unit_tax_amount :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).unit_tax_amount;
                           L_item_tax_tbl(L_item_tax_ctr).modified_tax_basis_amount :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).modified_tax_basis_amount;
                           L_item_tax_tbl(L_item_tax_ctr).unit_tax_uom :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).unit_tax_uom;
                           L_item_tax_tbl(L_item_tax_ctr).tax_rate_type :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_rate_type;
                           L_item_tax_tbl(L_item_tax_ctr).tax_code :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_code;
                           L_item_tax_tbl(L_item_tax_ctr).tax_rate :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_rate;
                           L_item_tax_tbl(L_item_tax_ctr).recoverable_amt :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).recoverable_amt;
                           L_item_tax_tbl(L_item_tax_ctr).recovered_amt :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).recovered_amt;
                           L_item_tax_tbl(L_item_tax_ctr).reg_pct_margin_value :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).reg_pct_margin_value;
                           L_item_tax_tbl(L_item_tax_ctr).regulated_price :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).regulated_price;
                           L_item_tax_ctr := L_item_tax_ctr + 1;
                        end LOOP;
                     end if;
                     if I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL is not NULL and
                        I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL.COUNT > 0 then
                        if L_sug_ctr is NULL then
                           L_sug_ctr := 1;
                        end if;
                        for k in I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL.FIRST..
                                I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL.LAST LOOP
                                       L_sug_tbl(L_sug_ctr).fiscal_doc_line_id :=
                                          I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).document_line_id;
                           L_sug_tbl(L_sug_ctr).status :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL(k).status;
                           L_sug_tbl(L_sug_ctr).tax_rule_code :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL(k).tax_rule_code;
                           L_sug_tbl(L_sug_ctr).tax_rule_description :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL(k).tax_rule_description;
                           L_sug_tbl(L_sug_ctr).tax_rule_id :=
                              I_businessObject.FiscDocChnkTaxRBO_TBL(a).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL(k).tax_rule_id;
                           L_sug_ctr := L_sug_ctr + 1;
                        end LOOP;
                     end if;
                     L_item_ctr := L_item_ctr + 1;
                  end LOOP;
               end if;
               L_hdr_ctr := L_hdr_ctr + 1;

            end LOOP;
         end if;
      end LOOP;
   end if;

   if L_hdr_tbl is not NULL and L_hdr_tbl.COUNT > 0 then
      FORALL i in L_hdr_tbl.FIRST..L_hdr_tbl.LAST
         insert into fm_tax_call_hdr_res (
           tax_service_id,
                 fiscal_doc_id,
           total_merch_cost,
           total_cost,
           total_services_cost,
           calculation_status)
         values (I_tax_service_id,
           L_hdr_tbl(i).fiscal_doc_id,
           L_hdr_tbl(i).total_merch_cost,
           L_hdr_tbl(i).total_cost,
           L_hdr_tbl(i).total_services_cost,
           decode(L_hdr_tbl(i).calculation_status,'E','E','S'));
   end if;
   if L_hdr_tax_tbl is not NULL and L_hdr_tax_tbl.COUNT > 0 then
      FORALL i in L_hdr_tax_tbl.FIRST..L_hdr_tax_tbl.LAST
         insert into fm_tax_call_res_hdr_tax (tax_service_id,
           fiscal_doc_id,
           tax_code,
           tax_amount,
           tax_basis_amount,
           modified_tax_basis_amount)
         values (I_tax_service_id,
           L_hdr_tax_tbl(i).fiscal_doc_id,
           L_hdr_tax_tbl(i).tax_code,
           L_hdr_tax_tbl(i).tax_amount,
           L_hdr_tax_tbl(i).tax_basis_amount,
           L_hdr_tax_tbl(i).modified_tax_basis_amount);
   end if;
   if L_item_tbl is not NULL and L_item_tbl.COUNT > 0 then
      FORALL i in L_item_tbl.FIRST..L_item_tbl.LAST
         insert into fm_tax_call_dtl_res (tax_service_id,
           fiscal_doc_id,
                 fiscal_doc_line_id,
           item,
           taxed_quantity,
           taxed_quantity_uom,
           entry_cfop,
                 icms_cst_code,
           pis_cst_code,
           cofins_cst_code,
           icms_cst_ind,
           pis_cst_ind,
           cofins_cst_ind,
           recoverable_icmsst,
           total_cost_with_icms,
           unit_cost_with_icms,
           recoverable_base_icmsst)
         values (I_tax_service_id,
           L_item_tbl(i).fiscal_doc_id,
           L_item_tbl(i).fiscal_doc_line_id,
           L_item_tbl(i).item,
           L_item_tbl(i).taxed_quantity,
           L_item_tbl(i).taxed_quantity_uom,
           L_item_tbl(i).entry_cfop,
           L_item_tbl(i).icms_cst_code,
           L_item_tbl(i).pis_cst_code,
           L_item_tbl(i).cofins_cst_code,
           L_item_tbl(i).icms_cst_ind,
           L_item_tbl(i).pis_cst_ind,
           L_item_tbl(i).cofins_cst_ind,
           L_item_tbl(i).recoverable_icmsst,
           L_item_tbl(i).total_cost_with_icms,
           L_item_tbl(i).unit_cost_with_icms,
           L_item_tbl(i).recoverable_base_icmsst);
   end if;
    if L_item_tax_tbl is not NULL and L_item_tax_tbl.COUNT > 0 then
      FORALL i in L_item_tax_tbl.FIRST..L_item_tax_tbl.LAST
         insert into fm_tax_call_res_dtl_tax(tax_service_id,
           fiscal_doc_id,
           fiscal_doc_line_id,
           item,
           legal_message,
           tax_amount,
           tax_basis_amount,
           modified_tax_basis_amount,
           unit_tax_amount,
           unit_tax_uom,
           tax_rate_type,
           tax_code,
           tax_rate,
--               tax_type,
           recoverable_amt,
           recovered_amt,
           reg_pct_margin_value,
           regulated_price)
         values (I_tax_service_id,
           L_item_tax_tbl(i).fiscal_doc_id,
           L_item_tax_tbl(i).fiscal_doc_line_id,
           L_item_tax_tbl(i).item,
           L_item_tax_tbl(i).legal_message,
           L_item_tax_tbl(i).tax_amount,
           L_item_tax_tbl(i).tax_basis_amount,
           L_item_tax_tbl(i).modified_tax_basis_amount,
           L_item_tax_tbl(i).unit_tax_amount,
           L_item_tax_tbl(i).unit_tax_uom,
           L_item_tax_tbl(i).tax_rate_type,
           L_item_tax_tbl(i).tax_code,
           L_item_tax_tbl(i).tax_rate,
                -- L_item_tax_tbl(i).tax_rate_type,
           L_item_tax_tbl(i).recoverable_amt,
           L_item_tax_tbl(i).recovered_amt,
           L_item_tax_tbl(i).reg_pct_margin_value,
           L_item_tax_tbl(i).regulated_price);
   end if;
   if (L_item_tax_tbl is not NULL and L_item_tax_tbl.COUNT > 0) and (L_hdr_tax_tbl is not NULL and L_hdr_tax_tbl.COUNT > 0) then
      for i in L_hdr_tax_tbl.FIRST..L_hdr_tax_tbl.LAST loop
         update fm_tax_call_res_hdr_tax ftcrht
            set ftcrht.modified_tax_basis_amount = (select sum(nvl(ftcrdt.modified_tax_basis_amount,0))
                                                      from fm_tax_call_res_dtl_tax ftcrdt
                                                     where ftcrdt.fiscal_doc_id = ftcrht.fiscal_doc_id
                                                       and ftcrdt.tax_service_id = ftcrht.tax_service_id
                                                       and ftcrdt.tax_code = ftcrht.tax_code)
          where ftcrht.tax_service_id = I_tax_service_id
            and ftcrht.tax_code = L_hdr_tax_tbl(i).tax_code
            and ftcrht.fiscal_doc_id = L_hdr_tax_tbl(i).fiscal_doc_id;
      end loop;
   end if;
   if L_sug_tbl is not NULL and L_sug_tbl.COUNT > 0 then
      FORALL i in L_sug_tbl.FIRST..L_sug_tbl.LAST
         insert into fm_tax_call_res_rules (tax_service_id,
           fiscal_doc_id,
           fiscal_doc_line_id,
           status,
           tax_rule_code,
           tax_rule_description,
           tax_rule_id)
         values (I_tax_service_id,
           L_sug_tbl(i).fiscal_doc_id,
           L_sug_tbl(i).fiscal_doc_line_id,
           L_sug_tbl(i).status,
           L_sug_tbl(i).tax_rule_code,
           L_sug_tbl(i).tax_rule_description,
           L_sug_tbl(i).tax_rule_id);
   end if;
   commit;
   return TRUE;
EXCEPTION
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STAGE_RESULTS;
-------------------------------------------------------------------------------------------------------------
FUNCTION CLEAR_STG_TABLES(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tax_service_id   IN     FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE,
                          I_fiscal_doc_id    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_update_hist_ind  IN     VARCHAR2)
RETURN BOOLEAN IS
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   PRAGMA AUTONOMOUS_TRANSACTION;
   L_program       VARCHAR2(255) := 'FM_EXT_TAXES_SQL.CLEAR_STG_TABLES';
   L_table         VARCHAR2(50);
   ---
   cursor C_LOCK_FM_TAX_CALL_HDR_RES  is
      select 'x'
        from fm_tax_call_hdr_res
       where tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_TAX_CALL_RES_HDR_TAX  is
      select 'x'
        from fm_tax_call_res_hdr_tax
       where tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_TAX_CALL_DTL_RES  is
      select 'x'
        from fm_tax_call_dtl_res
       where tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_TAX_CALL_RES_DTL_TAX  is
      select 'x'
        from fm_tax_call_res_dtl_tax
       where tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_TAX_CALL_RES_RULES  is
      select 'x'
        from fm_tax_call_res_rules
       where tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_FISCAL_STG_HDR  is
      select 'x'
        from fm_fiscal_stg_hdr
       where fiscal_doc_id = I_fiscal_doc_id
         and tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_FISCAL_STG_DTL  is
      select 'x'
        from fm_fiscal_stg_dtl
       where fiscal_doc_id = fiscal_doc_id
         and tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_FISCAL_STG_TAX_DTL  is
      select 'x'
        from fm_fiscal_stg_tax_dtl
       where fiscal_doc_id = fiscal_doc_id
         and tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_FISCAL_STG_COMP  is
      select 'x'
        from fm_fiscal_stg_complement
       where fiscal_doc_id = fiscal_doc_id
         and tax_service_id = I_tax_service_id
             for update nowait;
   cursor C_LOCK_FM_STG_ST_REC_HIST is
      select 'X'
        from fm_stg_st_rec_hist
       where fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
BEGIN
   -- Check required input parameter
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      return FALSE;
   end if;
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_fiscal_doc_id', L_program, NULL);
      return FALSE;
   end if;
   ---
   L_table := 'FM_TAX_CALL_STAGE';
   delete from fm_tax_call_stage
      where tax_service_id = I_tax_service_id;
   ---
   if I_update_hist_ind = 'N' then
     L_table := 'FM_TAX_CALL_RES_DTL_TAX';
     open C_LOCK_FM_TAX_CALL_RES_DTL_TAX ;
     close C_LOCK_FM_TAX_CALL_RES_DTL_TAX;
     delete from fm_tax_call_res_dtl_tax
        where tax_service_id = I_tax_service_id;
     ---
     L_table := 'FM_TAX_CALL_DTL_RES';
     open C_LOCK_FM_TAX_CALL_DTL_RES ;
     close C_LOCK_FM_TAX_CALL_DTL_RES;
     delete from fm_tax_call_dtl_res
        where tax_service_id = I_tax_service_id;
     ---
     L_table := 'FM_TAX_CALL_RES_HDR_TAX';
     open C_lock_fm_tax_call_res_hdr_tax ;
     close C_lock_fm_tax_call_res_hdr_tax;
     delete from fm_tax_call_res_hdr_tax
        where tax_service_id = I_tax_service_id;
     ---
     L_table := 'FM_TAX_CALL_HDR_RES';
     open C_lock_fm_tax_call_hdr_res ;
     close C_lock_fm_tax_call_hdr_res;
     delete from fm_tax_call_hdr_res
        where tax_service_id = I_tax_service_id;
     ---
     L_table := 'FM_TAX_CALL_RES_RULES';
     open C_LOCK_FM_TAX_CALL_RES_RULES ;
     close C_LOCK_FM_TAX_CALL_RES_RULES;
     delete from fm_tax_call_res_rules
        where tax_service_id = I_tax_service_id;
   else
     L_table := 'FM_STG_ST_REC_HIST';
     open C_LOCK_FM_STG_ST_REC_HIST;
     close C_LOCK_FM_STG_ST_REC_HIST;
     ---
     delete fm_stg_st_rec_hist
        where fiscal_doc_id   = I_fiscal_doc_id;
   end if;
   ---
   L_table := 'FM_FISCAL_STG_TAX_DTL';
   open c_lock_fm_fiscal_stg_tax_dtl ;
   close c_lock_fm_fiscal_stg_tax_dtl;
   delete from fm_fiscal_stg_tax_dtl
      where fiscal_doc_id = I_fiscal_doc_id
        and tax_service_id = I_tax_service_id;
   ---
    L_table := 'FM_FISCAL_STG_COMPLEMENT';
   open c_lock_fm_fiscal_stg_comp;
   close c_lock_fm_fiscal_stg_comp;
   delete from fm_fiscal_stg_complement
      where fiscal_doc_id = I_fiscal_doc_id
        and tax_service_id = I_tax_service_id;
   ---
   L_table := 'FM_FISCAL_STG_DTL';
   open c_lock_fm_fiscal_stg_dtl ;
   close c_lock_fm_fiscal_stg_dtl;
   delete from fm_fiscal_stg_dtl
      where fiscal_doc_id = I_fiscal_doc_id
        and tax_service_id = I_tax_service_id;
   ---
   L_table := 'FM_FISCAL_STG_HDR';
   open c_lock_fm_fiscal_stg_hdr ;
   close c_lock_fm_fiscal_stg_hdr;
   delete from fm_fiscal_stg_hdr
      where fiscal_doc_id = I_fiscal_doc_id
        and tax_service_id = I_tax_service_id;
   ---
   commit;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('L_program',
                                             L_table,
                                             I_fiscal_doc_id);
      return FALSE;
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLEAR_STG_TABLES;
---------------------------------------------------------------------------------------------------------------
END  FM_EXT_TAXES_SQL;
/