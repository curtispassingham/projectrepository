
CREATE OR REPLACE PACKAGE FM_RMA_NF_CREATION_SQL is
--------------------------------------------------------------------------------
FUNCTION EXISTS_RMA(O_error_message  IN OUT VARCHAR2,
                     O_exists         IN OUT BOOLEAN,
                     I_rma_id         IN     FM_RMA_HEAD.RMA_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message  IN OUT VARCHAR2,
                          O_query          IN OUT VARCHAR2,
                          I_customer_id    IN FM_RMA_HEAD.CUST_ID%TYPE,
                          I_rma_id         IN FM_RMA_HEAD.RMA_ID%TYPE,
                          I_rma_date       IN FM_RMA_HEAD.RMA_DATE%TYPE,
                          I_location_type  IN FM_RMA_HEAD.LOC_TYPE%TYPE,
                          I_location_id    IN FM_RMA_HEAD.LOCATION%TYPE)
   return BOOLEAN;

-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOV_RMA(O_error_message  IN OUT VARCHAR2,
                 O_query          IN OUT VARCHAR2,
                 I_location_type  IN     FM_RMA_HEAD.LOC_TYPE%TYPE,
                 I_location_id    IN     FM_RMA_HEAD.LOCATION%TYPE)
   return BOOLEAN;

-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOV_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN;                   
-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXISTS_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                         O_customer_desc  IN OUT FM_RMA_HEAD.CUST_NAME%TYPE,
                         I_customer_id    IN     FM_RMA_HEAD.CUST_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message  IN OUT VARCHAR2,
                       I_rma_id         IN     FM_RMA_HEAD.RMA_ID%TYPE,
                       I_status         IN     FM_RMA_HEAD.RMA_STATUS%TYPE)

   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC(O_error_message  IN OUT VARCHAR2,
                           I_rma_id         IN     FM_RMA_HEAD.RMA_ID%TYPE,
                           I_fiscal_doc_id  IN     FM_EDI_DOC_HEADER.FISCAL_DOC_ID%TYPE)

   return BOOLEAN;
--------------------------------------------------------------------------------
END FM_RMA_NF_CREATION_SQL;
/
 
