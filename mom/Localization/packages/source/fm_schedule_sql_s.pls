CREATE OR REPLACE PACKAGE FM_SCHEDULE_SQL is
---------------------------------------------------------------------------------
-- CREATE DATE - 04/07/2007 11:47:51
-- CREATE USER ? META.GMF
-- PROJECT / FRD - 10481 / 10481_ORFMI_FRD_001
-- DESCRIPTION ? Package associated to FM_SCHEDULE table
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Function Name: EXIST_SCHED_NO
-- Purpose:       Verify if the Schedule no already exists on table
----------------------------------------------------------------------------------
FUNCTION EXIST_SCHED_NO (O_error_message    IN OUT VARCHAR2,
                         O_exists           IN OUT BOOLEAN,
                         I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: UPDATE_STATUS
-- Purpose:       Updates status of schedule and all NFs too.
----------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS (O_error_message IN OUT VARCHAR2,
                        I_schedule_no   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                        I_status        IN     FM_SCHEDULE.STATUS%TYPE)
   return BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function Name: EXIST_SCHED_NO_LOC
-- Purpose:       Verify if the Schedule no already exists on table in a especified location
--------------------------------------------------------------------------------------------

FUNCTION EXIST_SCHED_NO_LOC (O_error_message    IN OUT VARCHAR2,
                             O_exists           IN OUT BOOLEAN,
                             I_location         IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                             I_loc_type         IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                             I_schedule_no      IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_MODE_TYPE
-- Purpose:       This function gets the mode type.
----------------------------------------------------------------------------------

FUNCTION GET_MODE_TYPE(O_error_message  IN OUT VARCHAR2,
                       O_mode_type      IN OUT FM_SCHEDULE.MODE_TYPE%TYPE,
                       I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;


--------------------------------------------------------------------------------------------
-- Function Name: EXIST_SCHED_NO_ERR
-- Purpose:       Verify if the Schedule no already exists on table with status = Error
--------------------------------------------------------------------------------------------

FUNCTION EXIST_SCHED_NO_ERR (O_error_message    IN OUT VARCHAR2,
                             O_exists           IN OUT BOOLEAN,
                             I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;


--------------------------------------------------------------------------------------------
-- Function Name: CREATE_NEW_SCHEDULE
-- Purpose:       Creates a new schedule for a given location. This process is used by
--                The integration process that generates fiscal documents for RTVs,
--                Transfers and Inventory Adjustments.
--------------------------------------------------------------------------------------------

FUNCTION CREATE_NEW_SCHEDULE (O_error_message  IN OUT VARCHAR2,
                              O_schedule_no    IN OUT FM_SCHEDULE.SCHEDULE_NO%TYPE,
                              I_loc_id         IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                              I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                              I_mode_type      IN     FM_SCHEDULE.MODE_TYPE%TYPE)
   return BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function Name: EXIST_SCHED_NO_LOG
-- Purpose:       Verify if the Schedule no already exists on table fm_log_errors
--------------------------------------------------------------------------------------------

FUNCTION EXIST_SCHED_NO_LOG (O_error_message    IN OUT VARCHAR2,
                             O_exists           IN OUT BOOLEAN,
                             I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function Name: DISAPPROVE_SCHEDULE
-- Purpose:       This function will disapprove all NFs of receiving informed.
--------------------------------------------------------------------------------------------
FUNCTION DISAPPROVE_SCHEDULE(O_error_message    IN OUT VARCHAR2,
                             I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE,
                             I_accounting_date  IN FM_SCHEDULE.ACCOUNTING_DATE%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: EXIST_REQUISITON_TYPE_PO
-- Purpose:       Verify if exists at least one NF with requisition type 'PO' that
--                is with status completed.
----------------------------------------------------------------------------------
FUNCTION EXIST_REQUISITON_TYPE_PO_NO (O_error_message    IN OUT VARCHAR2,
                                      O_exists           IN OUT BOOLEAN,
                                      I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
FUNCTION GET_REQUISITON_TYPE(O_error_message    IN OUT VARCHAR2,
                             O_requisition_type IN OUT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                             O_nfe_status       IN OUT FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                             I_schedule_no      IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION GET_UTIL_FISCAL_ID(O_error_message    IN OUT VARCHAR2,
                            O_utilization_id   OUT FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                            O_fiscal_doc_id    OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: GET_SCHD_STATUS
-- Purpose:       This function gets the Schedule status.
----------------------------------------------------------------------------------

FUNCTION GET_SCHD_STATUS(O_error_message  IN OUT VARCHAR2,
                         O_status         IN OUT FM_SCHEDULE.STATUS%TYPE,
                         I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------------------


END FM_SCHEDULE_SQL;
/