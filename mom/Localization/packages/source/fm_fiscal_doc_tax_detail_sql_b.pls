CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DOC_TAX_DETAIL_SQL is
-------------------------------------------------------------------
FUNCTION EXISTS(O_error_message       IN OUT VARCHAR2,
                O_exists              IN OUT BOOLEAN,
                I_vat_code            IN     FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE,
                I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_TAX_DETAIL_SQL.EXISTS';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_FISCAL_DOC_TAX_DETAIL is
      select 'x'
        from fm_fiscal_doc_tax_detail fdtd
       where fdtd.vat_code = I_vat_code
         and fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
BEGIN
   ---
   L_key := 'vat_code = '||I_vat_code||' fiscal_doc_line_id = '||TO_CHAR(I_fiscal_doc_line_id);
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
   open C_FM_FISCAL_DOC_TAX_DETAIL;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL',L_key);
   fetch C_FM_FISCAL_DOC_TAX_DETAIL into L_dummy;
   O_exists := C_FM_FISCAL_DOC_TAX_DETAIL%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL',L_key);
   close C_FM_FISCAL_DOC_TAX_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXISTS;
-----------------------------------------------------------------------------------
FUNCTION EXISTS_ANY_TAX_DETAIL(O_error_message       IN OUT VARCHAR2,
                               O_exists              IN OUT BOOLEAN,
                               I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_TAX_DETAIL_SQL.EXISTS_ANY_TAX_DETAIL';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_FISCAL_DOC_TAX_DETAIL is
      select 'x'
        from fm_fiscal_doc_tax_detail fdtd
       where fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
BEGIN
   ---
   L_key := 'fiscal_doc_line_id = '||TO_CHAR(I_fiscal_doc_line_id);
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
   open C_FM_FISCAL_DOC_TAX_DETAIL;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL',L_key);
   fetch C_FM_FISCAL_DOC_TAX_DETAIL into L_dummy;
   O_exists := C_FM_FISCAL_DOC_TAX_DETAIL%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL',L_key);
   close C_FM_FISCAL_DOC_TAX_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXISTS_ANY_TAX_DETAIL;
-----------------------------------------------------------------------------------
FUNCTION DELETE_TAX_DETAIL(O_error_message       IN OUT VARCHAR2,
                           I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_TAX_DETAIL_SQL.DELETE_TAX_DETAIL';
   L_key       VARCHAR2(100);
   ---
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_FM_FISCAL_DOC_TAX_DET is
      select 'x'
        from fm_fiscal_doc_tax_detail fdtd
       where fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id
         for update nowait;
   ---
BEGIN
   ---
   L_key := 'fiscal_doc_line_id = '||TO_CHAR(I_fiscal_doc_line_id);
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_FM_FISCAL_DOC_TAX_DET', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
   open C_LOCK_FM_FISCAL_DOC_TAX_DET;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_FM_FISCAL_DOC_TAX_DET', 'FM_FISCAL_DOC_TAX_DETAIL',L_key);
   close C_LOCK_FM_FISCAL_DOC_TAX_DET;
   ---
   SQL_LIB.SET_MARK('DELETE', 'C_LOCK_FM_FISCAL_DOC_TAX_DET', 'FM_FISCAL_DOC_TAX_DETAIL',L_key);
   delete from fm_fiscal_doc_tax_detail fdtd
    where fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_TAX_DETAIL',
                                            L_key,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_TAX_DETAIL;
-----------------------------------------------------------------------------------

END FM_FISCAL_DOC_TAX_DETAIL_SQL;
/
 