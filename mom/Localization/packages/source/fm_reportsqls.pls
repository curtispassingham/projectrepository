CREATE OR REPLACE PACKAGE FM_REPORTS_SQL AS

-- ORFM Report Parameters 
-------------------------------------------------------------------------------------------------
   PM_schedule_no                FM_SCHEDULE.SCHEDULE_NO%TYPE;
   PM_fiscal_doc_id              FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   PM_user_id                     USER_ATTRIB.USER_ID%TYPE;


-- ORFM Report supported functions
-------------------------------------------------------------------------------------------------
   FUNCTION GET_MODE_TYPE(I_schedule_no IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_CITY_DESC(I_city IN COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_CNPJ(I_cnpj IN V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_MESSAGE(I_fiscal_doc_line_id IN FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_TOTAL_NO_SERVICE(I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
          RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_TOTAL_SERVICE(I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
          RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_LEGENT_CNPJ(I_loc      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                            I_loc_type IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_CORR_DOC_COUNT(I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_UOM(I_item IN ITEM_MASTER.ITEM%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_ITEM_DESC(I_item IN ITEM_MASTER.ITEM%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_SUPP_PARTNER_NAME(I_supplier     IN SUPS.SUPPLIER%TYPE,
                                  I_partner_id   IN PARTNER.PARTNER_ID%TYPE,
                                  I_partner_type IN PARTNER.PARTNER_TYPE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_COMPANY_HEADER
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_LOC_NAME(I_loc_type IN ITEM_LOC.LOC_TYPE%TYPE,
                         I_loc      IN ITEM_LOC.LOC%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
END FM_REPORTS_SQL;
/
