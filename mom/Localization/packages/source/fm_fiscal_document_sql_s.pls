CREATE OR REPLACE PACKAGE FM_FISCAL_DOCUMENT_SQL is

  -------------------------------------------------------------
  -- Author  : META.GMF
  -- Created : 05/10/2007 15:13:06
  -- Purpose : Package of Report Fiscal Document
  -------------------------------------------------------------

  ---------------------------------------------------------------------------------
  ---------------------------------------------------------------------------------
  -- Function Name: GET_MASK_CNPJ
  -- Purpose: Get the cnpj mask
  ----------------------------------------------------------------------------------

  FUNCTION GET_MASK_CNPJ (O_error_message IN OUT VARCHAR2
                         ,O_cnpj_mask     IN OUT VARCHAR2
                         ,I_cnpj          IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
  return BOOLEAN;


  ---------------------------------------------------------------------------------
  -- Function Name: GET_TOTAL_SERVICE
  -- Purpose: Get the total service value
  ----------------------------------------------------------------------------------
   FUNCTION GET_TOTAL_SERVICE(O_error_message  IN OUT VARCHAR2,
                              O_total          IN OUT NUMBER,
                              I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                              I_service_ind    IN     V_BR_ITEM_FISCAL_ATTRIB.SERVICE_IND%TYPE)
   return BOOLEAN;

END FM_FISCAL_DOCUMENT_SQL;
/
 