CREATE OR REPLACE PACKAGE BODY L10N_BR_FLEX_API_SQL AS
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION PROTOTYPES
---------------------------------------------------------------------------------------------------
FUNCTION BUILD_ITEM_NAME_VALUE_PAIR(O_error_message        IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_entity_name_values  IN OUT NOCOPY  ENTITY_ATTRIB_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION BUILD_SUPS_NAME_VALUE_PAIR(O_error_message        IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_entity_name_values  IN OUT NOCOPY  ENTITY_ATTRIB_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION BUILD_LOC_NAME_VALUE_PAIR(O_error_message        IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_entity_name_values  IN OUT NOCOPY  ENTITY_ATTRIB_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_RIB_L10N_ATTRIB(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_10n_rib_obj  IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_BR_FLEX_API_SQL.VALIDATE_RIB_L10N_ATTRIB';

   L_10n_rib_rec   L10N_RIB_REC;
   L_msg           RIB_OBJECT;
   L_msg_type      VARCHAR2(30);
   L_xitem_msg     "RIB_XItemDesc_REC";

BEGIN

   L_10n_rib_rec := treat(IO_10n_rib_obj as L10N_RIB_REC);

   if L_10n_rib_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE',
                                            'L10N_RIB_REC', 
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_msg := L_10n_rib_rec.rib_msg;
   L_msg_type := L_10n_rib_rec.rib_msg_type;

   --Assumption: localization attributes are only supported for create messages, not update messages.   
   if L_msg_type in (RMSSUB_XITEM.ITEM_ADD,RMSSUB_XITEM.ICTRY_ADD) then
      L_xitem_msg := treat(L_msg as "RIB_XItemDesc_REC");
      if L_xitem_msg is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE',
                                               'RIB_XItemDesc_REC', 
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      -- call rmssub_xitem_l10n_br package
      if rmssub_xitem_l10n_br.validate_l10n_attrib(O_error_message,
                                                   L_xitem_msg,
                                                   L_msg_type) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END VALIDATE_RIB_L10N_ATTRIB;  
---------------------------------------------------------------------------------------
FUNCTION PERSIST_RIB_L10N_ATTRIB(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_10n_rib_obj IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_BR_FLEX_API_SQL.PERSIST_RIB_L10N_ATTRIB';

   L_10n_rib_rec   L10N_RIB_REC;
   L_msg           RIB_OBJECT;
   L_msg_type      VARCHAR2(30);
   L_xitem_msg     "RIB_XItemDesc_REC";

BEGIN

   L_10n_rib_rec := treat(IO_10n_rib_obj as L10N_RIB_REC);

   if L_10n_rib_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE',
                                            'L10N_RIB_REC', 
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_msg := L_10n_rib_rec.rib_msg;
   L_msg_type := L_10n_rib_rec.rib_msg_type;
   
   if L_msg_type in (RMSSUB_XITEM.ITEM_ADD,RMSSUB_XITEM.ICTRY_ADD) then
      L_xitem_msg := treat(L_msg as "RIB_XItemDesc_REC");
      if L_xitem_msg is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE',
                                               'RIB_XItemDesc_REC', 
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      -- call rmssub_xitem_l10n_br package
      if rmssub_xitem_l10n_br.persist_l10n_attrib(O_error_message,
                                                  L_xitem_msg,
                                                  L_msg_type) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END PERSIST_RIB_L10N_ATTRIB;  
---------------------------------------------------------------------------------------
FUNCTION BUILD_NAME_VALUE_PAIR(O_error_message        IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_entity_name_values  IN OUT NOCOPY  ENTITY_ATTRIB_TBL,
                               I_entity_type          IN             VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR';

BEGIN
   -- check input
   if I_entity_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_entity_type',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_entity_type NOT in ('ITEM', 'SUPP', 'LOC') then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_ENTITY_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if IO_entity_name_values is NULL  or IO_entity_name_values.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'IO_entity_name_values',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- retrieve attributes
   if I_entity_type = 'ITEM' then
      if BUILD_ITEM_NAME_VALUE_PAIR(O_error_message, 
                                    IO_entity_name_values) = FALSE then
         return FALSE;
      end if;
   elsif I_entity_type = 'SUPP' then
      if BUILD_SUPS_NAME_VALUE_PAIR(O_error_message, 
                                    IO_entity_name_values) = FALSE then
         return FALSE;
      end if;
   elsif I_entity_type = 'LOC' then
      -- this covers store, warehouses and external finishers
      if BUILD_LOC_NAME_VALUE_PAIR(O_error_message, 
                                    IO_entity_name_values) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END BUILD_NAME_VALUE_PAIR;
---------------------------------------------------------------------------------------
FUNCTION BUILD_ITEM_NAME_VALUE_PAIR(O_error_message        IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_entity_name_values  IN OUT NOCOPY  ENTITY_ATTRIB_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_BR_FLEX_API_SQL.BUILD_ITEM_NAME_VALUE_PAIR';

   L_entity_attrib_tbl     ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();

   cursor C_ATTRIBUTES is
      select ENTITY_ATTRIB_REC(
                'ITEM',             --I_entity_type
                piv.item,           --I_entity id
                att.view_col_name,  --O_name
                piv.attrib_value)   --O_value
        from ext_entity ext,
             l10n_attrib_group grp,
             l10n_attrib att,
             -- define an unpivotted data view (piv) with VARCHAR/NUMBER/DATE 
             -- columns on xxx_L10N_EXT table converted to rows 
             (with raw_data as 
                 (select item,
                         group_id,
                         varchar2_1,
                         varchar2_2,
                         varchar2_3,
                         varchar2_4,
                         varchar2_5,
                         varchar2_6,
                         varchar2_7,
                         varchar2_8,
                         varchar2_9,
                         varchar2_10,
                         to_char(number_11) number_11,
                         to_char(number_12) number_12,
                         to_char(number_13) number_13,
                         to_char(number_14) number_14,
                         to_char(number_15) number_15,
                         to_char(number_16) number_16,
                         to_char(number_17) number_17,
                         to_char(number_18) number_18,
                         to_char(number_19) number_19,
                         to_char(number_20) number_20,
                         to_char(date_21, 'YYYYMMDD') date_21,
                         to_char(date_22, 'YYYYMMDD') date_22
                    from item_country_l10n_ext,
                         TABLE(IO_entity_name_values) ent
                   where l10n_country_id = 'BR'
                     and country_id = 'BR'      --hard code to limit to Brazil
                     and item = ent.I_entity_id)
            select item,
                   group_id,
                   attrib_value,
                   attrib_col
              from raw_data
           unpivot 
                   (attrib_value for attrib_col in (varchar2_1  as 'VARCHAR2_1',
                                                    varchar2_2  as 'VARCHAR2_2',
                                                    varchar2_3  as 'VARCHAR2_3',
                                                    varchar2_4  as 'VARCHAR2_4',
                                                    varchar2_5  as 'VARCHAR2_5',
                                                    varchar2_6  as 'VARCHAR2_6',
                                                    varchar2_7  as 'VARCHAR2_7',
                                                    varchar2_8  as 'VARCHAR2_8',
                                                    varchar2_9  as 'VARCHAR2_9',
                                                    varchar2_10 as 'VARCHAR2_10',
                                                    number_11   as 'NUMBER_11',
                                                    number_12   as 'NUMBER_12',
                                                    number_13   as 'NUMBER_13',
                                                    number_14   as 'NUMBER_14',
                                                    number_15   as 'NUMBER_15',
                                                    number_16   as 'NUMBER_16',
                                                    number_17   as 'NUMBER_17',
                                                    number_18   as 'NUMBER_18',
                                                    number_19   as 'NUMBER_19',
                                                    number_20   as 'NUMBER_20',
                                                    date_21     as 'DATE_21', 
                                                    date_22     as 'DATE_22'))) piv
       -- main where clause
       where ext.base_rms_table   = 'ITEM_COUNTRY'
         and ext.ext_entity_id    = grp.ext_entity_id
         and grp.country_id       = 'BR'     --this package is for Brazil only
         and grp.base_ind         = 'N'      --for non-base attribute groups only
         and att.group_id         = grp.group_id
         and piv.attrib_col       = att.attrib_storage_col
         and piv.group_id         = att.group_id;

BEGIN
   -- query
   open C_ATTRIBUTES;
   fetch C_ATTRIBUTES BULK COLLECT into L_entity_attrib_tbl;
   close C_ATTRIBUTES;

   -- Assign L_entity_attrib_tbl back to the input/output paramater IO_entity_name_values
   -- If there are no non-base attributes for the entity, then the return object will be empty.
   IO_entity_name_values := L_entity_attrib_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END BUILD_ITEM_NAME_VALUE_PAIR;  
---------------------------------------------------------------------------------------
FUNCTION BUILD_SUPS_NAME_VALUE_PAIR(O_error_message        IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_entity_name_values  IN OUT NOCOPY  ENTITY_ATTRIB_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_BR_FLEX_API_SQL.BUILD_SUPS_NAME_VALUE_PAIR';

   L_entity_attrib_tbl     ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();

   cursor C_ATTRIBUTES is
      select ENTITY_ATTRIB_REC(
                'SUPP',             --I_entity_type
                piv.supplier,       --I_entity id
                att.view_col_name,  --O_name
                piv.attrib_value)   --O_value
        from ext_entity ext,
             l10n_attrib_group grp,
             l10n_attrib att,
             -- define an unpivotted data view (piv) with VARCHAR/NUMBER/DATE 
             -- columns on xxx_L10N_EXT table converted to rows 
             (with raw_data as 
                 (select supplier, 
                         group_id,
                         varchar2_1,
                         varchar2_2,
                         varchar2_3,
                         varchar2_4,
                         varchar2_5,
                         varchar2_6,
                         varchar2_7,
                         varchar2_8,
                         varchar2_9,
                         varchar2_10,
                         to_char(number_11) number_11,
                         to_char(number_12) number_12,
                         to_char(number_13) number_13,
                         to_char(number_14) number_14,
                         to_char(number_15) number_15,
                         to_char(number_16) number_16,
                         to_char(number_17) number_17,
                         to_char(number_18) number_18,
                         to_char(number_19) number_19,
                         to_char(number_20) number_20,
                         to_char(date_21, 'YYYYMMDD') date_21,
                         to_char(date_22, 'YYYYMMDD') date_22
                    from sups_l10n_ext,
                         TABLE(IO_entity_name_values) ent
                   where l10n_country_id = 'BR'
                     and supplier = to_number(ent.I_entity_id))
            select supplier,
                   group_id,
                   attrib_value,
                   attrib_col
              from raw_data
           unpivot 
                   (attrib_value for attrib_col in (varchar2_1  as 'VARCHAR2_1',
                                                    varchar2_2  as 'VARCHAR2_2',
                                                    varchar2_3  as 'VARCHAR2_3',
                                                    varchar2_4  as 'VARCHAR2_4',
                                                    varchar2_5  as 'VARCHAR2_5',
                                                    varchar2_6  as 'VARCHAR2_6',
                                                    varchar2_7  as 'VARCHAR2_7',
                                                    varchar2_8  as 'VARCHAR2_8',
                                                    varchar2_9  as 'VARCHAR2_9',
                                                    varchar2_10 as 'VARCHAR2_10',
                                                    number_11   as 'NUMBER_11',
                                                    number_12   as 'NUMBER_12',
                                                    number_13   as 'NUMBER_13',
                                                    number_14   as 'NUMBER_14',
                                                    number_15   as 'NUMBER_15',
                                                    number_16   as 'NUMBER_16',
                                                    number_17   as 'NUMBER_17',
                                                    number_18   as 'NUMBER_18',
                                                    number_19   as 'NUMBER_19',
                                                    number_20   as 'NUMBER_20',
                                                    date_21     as 'DATE_21', 
                                                    date_22     as 'DATE_22'))) piv
       -- main where clause
       where ext.base_rms_table   = 'SUPS'
         and ext.ext_entity_id    = grp.ext_entity_id
         and grp.country_id       = 'BR'     --this package is for Brazil only
         and grp.base_ind         = 'N'      --for non-base attribute groups only
         and att.group_id         = grp.group_id
         and piv.attrib_col       = att.attrib_storage_col
         and piv.group_id         = att.group_id;

BEGIN
   -- query
   open C_ATTRIBUTES;
   fetch C_ATTRIBUTES BULK COLLECT into L_entity_attrib_tbl;
   close C_ATTRIBUTES;

   -- Assign L_entity_attrib_tbl back to the input/output paramater IO_entity_name_values
   -- If there are no non-base attributes for the entity, then the return object will be empty.
   IO_entity_name_values := L_entity_attrib_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END BUILD_SUPS_NAME_VALUE_PAIR;  
---------------------------------------------------------------------------------------
FUNCTION BUILD_LOC_NAME_VALUE_PAIR(O_error_message        IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_entity_name_values  IN OUT NOCOPY  ENTITY_ATTRIB_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_BR_FLEX_API_SQL.BUILD_LOC_NAME_VALUE_PAIR';

   L_entity_attrib_tbl     ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();

   cursor C_ATTRIBUTES is
      select ENTITY_ATTRIB_REC(
                'LOC',             --I_entity_type
                piv.loc,           --I_entity id
                att.view_col_name,  --O_name
                piv.attrib_value)   --O_value
        from ext_entity ext,
             l10n_attrib_group grp,
             l10n_attrib att,
             -- define an unpivotted data view (piv) with VARCHAR/NUMBER/DATE 
             -- columns on xxx_L10N_EXT table converted to rows 
             (with raw_data as 
                 (select to_char(store) loc,
                         group_id,
                         varchar2_1,
                         varchar2_2,
                         varchar2_3,
                         varchar2_4,
                         varchar2_5,
                         varchar2_6,
                         varchar2_7,
                         varchar2_8,
                         varchar2_9,
                         varchar2_10,
                         to_char(number_11) number_11,
                         to_char(number_12) number_12,
                         to_char(number_13) number_13,
                         to_char(number_14) number_14,
                         to_char(number_15) number_15,
                         to_char(number_16) number_16,
                         to_char(number_17) number_17,
                         to_char(number_18) number_18,
                         to_char(number_19) number_19,
                         to_char(number_20) number_20,
                         to_char(date_21, 'YYYYMMDD') date_21,
                         to_char(date_22, 'YYYYMMDD') date_22
                    from store_l10n_ext,
                         TABLE(IO_entity_name_values) ent
                   where l10n_country_id = 'BR'
                     and store = to_number(ent.I_entity_id)
                   union all
                  select to_char(wh) loc,
                         group_id,
                         varchar2_1,
                         varchar2_2,
                         varchar2_3,
                         varchar2_4,
                         varchar2_5,
                         varchar2_6,
                         varchar2_7,
                         varchar2_8,
                         varchar2_9,
                         varchar2_10,
                         to_char(number_11) number_11,
                         to_char(number_12) number_12,
                         to_char(number_13) number_13,
                         to_char(number_14) number_14,
                         to_char(number_15) number_15,
                         to_char(number_16) number_16,
                         to_char(number_17) number_17,
                         to_char(number_18) number_18,
                         to_char(number_19) number_19,
                         to_char(number_20) number_20,
                         to_char(date_21, 'YYYYMMDD') date_21,
                         to_char(date_22, 'YYYYMMDD') date_22
                    from wh_l10n_ext,
                         TABLE(IO_entity_name_values) ent
                   where l10n_country_id = 'BR'
                     and wh = to_number(ent.I_entity_id)
                   union all
                  select partner_id loc,
                         group_id,
                         varchar2_1,
                         varchar2_2,
                         varchar2_3,
                         varchar2_4,
                         varchar2_5,
                         varchar2_6,
                         varchar2_7,
                         varchar2_8,
                         varchar2_9,
                         varchar2_10,
                         to_char(number_11) number_11,
                         to_char(number_12) number_12,
                         to_char(number_13) number_13,
                         to_char(number_14) number_14,
                         to_char(number_15) number_15,
                         to_char(number_16) number_16,
                         to_char(number_17) number_17,
                         to_char(number_18) number_18,
                         to_char(number_19) number_19,
                         to_char(number_20) number_20,
                         to_char(date_21, 'YYYYMMDD') date_21,
                         to_char(date_22, 'YYYYMMDD') date_22
                    from partner_l10n_ext,
                         TABLE(IO_entity_name_values) ent
                   where l10n_country_id = 'BR'
                     and partner_type = 'E'     -- limit to external finishers
                     and partner_id = ent.I_entity_id)
            select loc,
                   group_id,
                   attrib_value,
                   attrib_col
              from raw_data
           unpivot 
                   (attrib_value for attrib_col in (varchar2_1  as 'VARCHAR2_1',
                                                    varchar2_2  as 'VARCHAR2_2',
                                                    varchar2_3  as 'VARCHAR2_3',
                                                    varchar2_4  as 'VARCHAR2_4',
                                                    varchar2_5  as 'VARCHAR2_5',
                                                    varchar2_6  as 'VARCHAR2_6',
                                                    varchar2_7  as 'VARCHAR2_7',
                                                    varchar2_8  as 'VARCHAR2_8',
                                                    varchar2_9  as 'VARCHAR2_9',
                                                    varchar2_10 as 'VARCHAR2_10',
                                                    number_11   as 'NUMBER_11',
                                                    number_12   as 'NUMBER_12',
                                                    number_13   as 'NUMBER_13',
                                                    number_14   as 'NUMBER_14',
                                                    number_15   as 'NUMBER_15',
                                                    number_16   as 'NUMBER_16',
                                                    number_17   as 'NUMBER_17',
                                                    number_18   as 'NUMBER_18',
                                                    number_19   as 'NUMBER_19',
                                                    number_20   as 'NUMBER_20',
                                                    date_21     as 'DATE_21', 
                                                    date_22     as 'DATE_22'))) piv
       -- main where clause
       where ext.base_rms_table   in ('STORE', 'WH', 'PARTNER')
         and ext.ext_entity_id    = grp.ext_entity_id
         and grp.country_id       = 'BR'     --this package is for Brazil only
         and grp.base_ind         = 'N'      --for non-base attribute groups only
         and att.group_id         = grp.group_id
         and piv.attrib_col       = att.attrib_storage_col
         and piv.group_id         = att.group_id;

BEGIN
   -- query
   open C_ATTRIBUTES;
   fetch C_ATTRIBUTES BULK COLLECT into L_entity_attrib_tbl;
   close C_ATTRIBUTES;

   -- Assign L_entity_attrib_tbl back to the input/output paramater IO_entity_name_values
   -- If there are no non-base attributes for the entity, then the return object will be empty.
   IO_entity_name_values := L_entity_attrib_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END BUILD_LOC_NAME_VALUE_PAIR;  
---------------------------------------------------------------------------------------
END L10N_BR_FLEX_API_SQL;
/