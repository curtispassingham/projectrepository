create or replace PACKAGE FM_FISCAL_FIND_SQL as
----------------------------------------------------------------------------------
FUNCTION LOV_DOC_NO(O_error_message    IN OUT VARCHAR2,
                    O_query            IN OUT VARCHAR2,
                    I_location_id      IN     FM_fiscal_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type    IN     FM_fiscal_DOC_HEADER.LOCATION_TYPE%TYPE,
                    I_requisition_type IN     FM_fiscal_DOC_HEADER.REQUISITION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_TYPE_ID
-- Purpose:       This function returns the query for record_grup REC_TYPE_ID.
----------------------------------------------------------------------------------
FUNCTION LOV_TYPE_ID(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_UTILIZATION
-- Purpose:       This function returns the query for record_grup REC_LOV_UTILIZATION.
----------------------------------------------------------------------------------
FUNCTION LOV_UTILIZATION(O_error_message  IN OUT VARCHAR2,
                         O_query          IN OUT VARCHAR2,
                         I_utilization_id IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_SUPPLIER
-- Purpose:       This function returns the query for record_grup REC_SUPPLIER.
----------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2,
                      I_supplier_sites_ind IN SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE Default 'N')
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_PARTNER
-- Purpose:       This function returns the query for record_grup REC_PARTNER.
----------------------------------------------------------------------------------
FUNCTION LOV_PARTNER(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_where_clause   IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_LOC
-- Purpose:       This function returns the query for record_grup REC_LOC.
----------------------------------------------------------------------------------
FUNCTION LOV_LOC(O_error_message  IN OUT VARCHAR2,
                 O_query          IN OUT VARCHAR2,
                 I_where_clause   IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_OUTLOC
-- Purpose:       This function returns the query for record_grup REC_OUTLOC.
----------------------------------------------------------------------------------
FUNCTION LOV_OUTLOC(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2,
                    I_where_clause   IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_ENTITY
-- Purpose:       This function validates the entity of NF.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_ENTITY(O_error_message  IN OUT VARCHAR2,
                         O_description    IN OUT VARCHAR2,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_CNPJCPF
-- Purpose:       This function returns the query for record_grup REC_CNPJCPF.
----------------------------------------------------------------------------------
FUNCTION LOV_CNPJCPF(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                     I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                     I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                     I_cnpjcpf        IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_CNPJCPF
-- Purpose:       This function validates the CNPJ/CPF.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_CNPJCPF(O_error_message  IN OUT VARCHAR2,
                          O_unique         IN OUT BOOLEAN,
                          O_tipo_cnpjcpf   IN OUT VARCHAR2,
                          O_key_value_1    IN OUT V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                          I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                          I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                          I_cnpjcpf        IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_ORDER_NO
-- Purpose:       This function returns the query for record_grup REC_ORDER_NO.
----------------------------------------------------------------------------------
FUNCTION LOV_ORDER_NO(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2,
                      I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_ITEM
-- Purpose:       This function returns the query for record_grup REC_ITEM.
----------------------------------------------------------------------------------
FUNCTION LOV_ITEM(O_error_message  IN OUT VARCHAR2,
                  O_query          IN OUT VARCHAR2,
                  I_where_clause   IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for where clause of find block.
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message      IN OUT VARCHAR2,
                          O_where_clause       IN OUT VARCHAR2,
                          I_schedule_no        IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                          I_location_id        IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                          I_location_type      IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                          I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_type_id            IN     FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                          I_utilization_id     IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE,
                          I_cfop               IN     FM_FISCAL_DOC_HEADER.NF_CFOP%TYPE,
                          I_nfe_access_key     IN     FM_FISCAL_DOC_HEADER.NFE_ACCESSKEY%TYPE,
                          I_status             IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                          I_module             IN     FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                          I_key_value_1        IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                          I_key_value_2        IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                          I_cnpj               IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
                          I_cpf                IN     V_FISCAL_ATTRIBUTES.CPF%TYPE,
                          I_issue_date         IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                          I_entry_or_exit_date IN     FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                          I_exit_hour          IN     FM_FISCAL_DOC_HEADER.EXIT_HOUR%TYPE,
                          I_requisition_type   IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                          I_requisition_no     IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                          I_item               IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                          I_mode_type          IN     FM_SCHEDULE.MODE_TYPE%TYPE,
                          I_link_nf_ind        IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_ENTITY_INFO
-- Purpose:       This function gets the entity informations.
----------------------------------------------------------------------------------
FUNCTION GET_ENTITY_INFO(O_error_message  IN OUT VARCHAR2,
                         O_cnpjcfp_type   IN OUT V_FISCAL_ATTRIBUTES.TYPE%TYPE,
                         O_cnpjcfp        IN OUT V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: LOV_SCHEDULE_NO
-- Purpose:       This function returns the query for record_grup.
----------------------------------------------------------------------------------
FUNCTION LOV_SCHEDULE_NO(O_error_message  IN OUT VARCHAR2,
                         O_query          IN OUT VARCHAR2,
                         I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                         I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                         I_where_clause   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                         I_mode_type      IN     FM_SCHEDULE.MODE_TYPE%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_CUSTOMER
-- Purpose:       This function returns the query for record_grup REC_CUSTOMER.
----------------------------------------------------------------------------------
/*
FUNCTION LOV_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN; */
----------------------------------------------------------------------------------
-- Function Name: GET_MODE_TYPE
-- Purpose:       This function returns the type.
----------------------------------------------------------------------------------
FUNCTION GET_MODE_TYPE(O_error_message  IN OUT VARCHAR2,
                       O_mode_type      IN OUT FM_SCHEDULE.MODE_TYPE%TYPE,
                       I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_TSF_NO
-- Purpose:       This function returns the query for record_grup REC_TSF_NO.
----------------------------------------------------------------------------------
FUNCTION LOV_TSF_NO(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2,
                    I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_RTV_NO
-- Purpose:       This function returns the query for record_grup REC_RTV_NO.
----------------------------------------------------------------------------------
FUNCTION LOV_RTV_NO(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2,
                    I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                    I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_RMA_NO
-- Purpose:       This function returns the query for record_grup REC_RMA_NO.
----------------------------------------------------------------------------------
FUNCTION LOV_RMA_NO(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LOV_STOCK_NO
-- Purpose:       This function returns the query for record_grup REC_STOCK_NO.
----------------------------------------------------------------------------------
FUNCTION LOV_STOCK_NO(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2,
                      I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                      I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_TSF
-- Purpose:       This function validates the TSF.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_RTV
-- Purpose:       This function validates the RTV.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_RTV(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      I_rtv_order_no   IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                      I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_RMA
-- Purpose:       This function validates the RMA.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_RMA(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      I_rma_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_STOCK
-- Purpose:       This function validates the STOCK.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_STOCK(O_error_message  IN OUT VARCHAR2,
                        O_exists         IN OUT BOOLEAN,
                        I_stock_no       IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                        I_location_id    IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                        I_location_type  IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: LINK_SCHEDULE
-- Purpose:       This function adds schedule to the Fiscal Documents.
----------------------------------------------------------------------------------
FUNCTION LINK_SCHEDULE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_fiscal_doc_id   IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                       I_schedule        IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
END FM_FISCAL_FIND_SQL;
/