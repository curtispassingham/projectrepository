CREATE OR REPLACE PACKAGE BODY FM_SCHED_SUBMIT AS

   LP_error_status   VARCHAR2(1)   := NULL;

-------------------------------------------------------------------------------
                     /* Private Program Declarations */
-------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD (O_error_message IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY RIB_OBJECT,
                               O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                               I_recv_no       IN            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                               I_seq_no        IN            FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN;


FUNCTION BUILD_HEADER_OBJECT (O_error_message          IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_poscheduledesc_rec     IN OUT        "RIB_POScheduleDesc_REC",
                              O_routing_info           IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              I_recv_no                IN            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE)
RETURN BOOLEAN;


FUNCTION BUILD_DETAIL_OBJECTS (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_POSchedule_tbl         IN OUT   "RIB_POSchedule_TBL",
                               O_status_code            IN OUT   VARCHAR2,
                               I_recv_no                IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE)
RETURN BOOLEAN;


PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_message   IN  OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_seq_no          IN           FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE);

FUNCTION LOCK_THE_BLOCK (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_queue_locked    IN OUT   BOOLEAN,
                         I_seq_no          IN       FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN;


FUNCTION DELETE_QUEUE_REC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_recv_no         IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE)
RETURN BOOLEAN;



-------------------------------------------------------------------------------
                      /*** Public Program Bodies ***/
-------------------------------------------------------------------------------
FUNCTION ADDTOQ (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_recv_no         IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE)
RETURN BOOLEAN IS


   L_msg_type                      VARCHAR2(15) := 'poschedcre';
   L_family                        VARCHAR2(15) := 'posched';
   L_thread                        NUMBER       := 1;
   L_pub_status                    FM_RIB_RECEIVING_MFQUEUE.PUB_STATUS%TYPE := 'U';
   L_program                       VARCHAR2(64) := 'FM_SCHED_SUBMIT.ADDTOQ';
BEGIN


   if I_recv_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_recv_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   insert into FM_RIB_RECEIVING_MFQUEUE
      (SEQ_NO,
       MESSAGE_TYPE,
       PUB_STATUS,
       FAMILY,
       THREAD_NO,
       RECV_NO)
   values
      (FM_RECEIVING_MFQUEUE_SEQ.nextval,
       L_msg_type,
       L_pub_status,
       L_family,
       L_thread,
       I_recv_no);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SCHED_SUBMIT.ADDTOQ',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ADDTOQ;

-------------------------------------------------------------------------------
PROCEDURE GETNXT (O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_message_type    IN OUT   VARCHAR2,
                  O_message         IN OUT   RIB_OBJECT,
                  O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                  O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                  I_num_threads     IN       NUMBER DEFAULT 1,
                  I_thread_val      IN       NUMBER DEFAULT 1)
IS

   L_recv_no            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE       := NULL;
   L_pub_status         FM_RIB_RECEIVING_MFQUEUE.PUB_STATUS%TYPE;
   L_seq_no             FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE;
   L_seq_limit          FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE := 0;
   L_queue_locked       BOOLEAN     := FALSE;
   L_hosp               VARCHAR2(1) := 'N';

   PROGRAM_ERROR        EXCEPTION;

   cursor C_QUEUE is
      select seq_no,
             recv_no,
             pub_status
        from fm_rib_receiving_mfqueue
       where seq_no = (select min(seq_no)
                         from fm_rib_receiving_mfqueue
                        where pub_status = 'U'
                          and message_type = 'poschedcre');

   cursor C_HOSP is
      select 'Y'
        from fm_rib_receiving_mfqueue
       where recv_no = L_recv_no
         and pub_status = API_CODES.HOSPITAL;

BEGIN

   LP_error_status := API_CODES.HOSPITAL;

      O_message      := NULL;
      O_bus_obj_id   := NULL;
      O_routing_info := NULL;

   open C_QUEUE;
   fetch C_QUEUE into L_seq_no,
                      L_recv_no,
                      L_pub_status;

      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
   close C_QUEUE;

      if LOCK_THE_BLOCK(O_error_message,
                        L_queue_locked,
                        L_seq_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then
         open C_HOSP;
         fetch C_HOSP into L_hosp;
         close C_HOSP;

         if L_hosp = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            raise PROGRAM_ERROR;
         end if;

         if PROCESS_QUEUE_RECORD (O_error_message,
                                  O_message,
                                  O_routing_info,
                                  O_bus_obj_id,
                                  L_recv_no,
                                  L_seq_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;

      end if;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_recv_no);
      O_message_type := FM_SCHED_SUBMIT.LP_cre_type;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    O_routing_info,
                    L_seq_no);
END GETNXT;

-------------------------------------------------------------------------------
PROCEDURE PUB_RETRY (O_status_code     IN OUT   VARCHAR2,
                     O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_message_type    IN OUT   VARCHAR2,
                     O_message         IN OUT   RIB_OBJECT,
                     O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                     O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                     I_ref_object      IN       RIB_OBJECT)
IS

   L_seq_limit        FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE:=0;
   L_seq_no           FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE;
   L_recv_no          FM_RIB_STG_RECEIVING_DETAIL.RECV_NO%TYPE       := NULL;
   L_queue_locked     BOOLEAN     := FALSE;
   PROGRAM_ERROR      EXCEPTION;

      cursor C_RETRY_QUEUE is
         select seq_no,
                recv_no
           from FM_RIB_RECEIVING_MFQUEUE
          where pub_status = 'H'
            and message_type = 'poschedcre';

BEGIN
   LP_error_status := API_CODES.HOSPITAL;

   O_message := NULL;

   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_seq_no,
                            L_recv_no;

   if C_RETRY_QUEUE%NOTFOUND then
      close C_RETRY_QUEUE;
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   close C_RETRY_QUEUE;


   if LOCK_THE_BLOCK(O_error_message,
                     L_queue_locked,
                     L_seq_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then
      if PROCESS_QUEUE_RECORD(O_error_message,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              L_recv_no,
                              L_seq_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message is NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         O_status_code := API_CODES.NEW_MSG;
         O_message_type := FM_SCHED_SUBMIT.LP_cre_type;
      end if;

      O_bus_obj_id  := RIB_BUSOBJID_TBL(L_recv_no);
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    O_routing_info,
                    L_seq_no);

END PUB_RETRY;

-------------------------------------------------------------------------------
                      /*** Private Program Bodies ***/
-------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD (O_error_message IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY RIB_OBJECT,
                               O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                               I_recv_no       IN            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                               I_seq_no        IN            FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_status_code           VARCHAR2(1) := NULL;
   L_status                VARCHAR2(1) := NULL;
   L_max_details           RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 12;
   L_num_threads           RIB_SETTINGS.NUM_THREADS%TYPE            := NULL;
   L_min_time_lag          RIB_SETTINGS.MINUTES_TIME_LAG%TYPE       := NULL;
   L_poscheduledesc_rec    "RIB_POScheduleDesc_REC"                 := NULL;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                FM_SCHED_SUBMIT.FAMILY);

   if L_status_code = API_CODES.UNHANDLED_ERROR then
      return FALSE;
   end if;

   if BUILD_HEADER_OBJECT(O_error_message,
                          L_poscheduledesc_rec,
                          O_routing_info,
                          I_recv_no) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if DELETE_QUEUE_REC(O_error_message,
                       I_recv_no) = FALSE then

      return FALSE;
   end if;

   O_message := L_poscheduledesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SCHED_SUBMIT.PROCESS_QUEUE_RECORD',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_RECORD;
-------------------------------------------------------------------------------
FUNCTION BUILD_HEADER_OBJECT (O_error_message          IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_poscheduledesc_rec     IN OUT        "RIB_POScheduleDesc_REC",
                              O_routing_info           IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              I_recv_no                IN            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE)
RETURN BOOLEAN IS

   L_poscheduledesc_rec       "RIB_POScheduleDesc_REC"   := NULL;
   L_poschedule_tbl           "RIB_POSchedule_TBL"       := NULL;
   L_rib_routinginfo_rec       RIB_ROUTINGINFO_REC       := NULL;
   L_location_id              FM_RIB_STG_RECEIVING_HEADER.LOCATION_ID%TYPE :=NULL;
   L_status_code              varchar2(20);

   cursor C_GET_HEADER is
       select  location_id
         from fm_rib_stg_receiving_header
        where recv_no = I_recv_no;

BEGIN

   O_routing_info       := NULL;

   L_poschedule_tbl := "RIB_POSchedule_TBL"();

   if BUILD_DETAIL_OBJECTS (O_error_message,
                            L_poschedule_tbl,
                            L_status_code,
                            I_recv_no) = FALSE then
      return FALSE;
   end if;
   L_poscheduledesc_rec := "RIB_POScheduleDesc_REC"(0,
                                                    NULL,
                                                    NULL,
                                                    NULL);
   open C_GET_HEADER;
   fetch C_GET_HEADER into L_location_id;
   close C_GET_HEADER;
   L_poscheduledesc_rec.schedule_nbr          := I_recv_no;
   L_poscheduledesc_rec.physical_wh := L_location_id;
   L_poscheduledesc_rec.POSchedule_TBL        := L_poschedule_tbl;

   O_poscheduledesc_rec := L_poscheduledesc_rec;

   O_routing_info := RIB_ROUTINGINFO_TBL();

   L_rib_routinginfo_rec := RIB_ROUTINGINFO_REC('to_phys_loc',
                                                 L_location_id,
                                                'to_phys_loc_type',
                                                'W',
                                                 NULL,
                                                 NULL);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;
   L_rib_routinginfo_rec := RIB_ROUTINGINFO_REC('source_app',
                                                'RFM',
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SCHED_SUBMIT.BUILD_HEADER_OBJECT',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;
-------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_POSchedule_tbl         IN OUT   "RIB_POSchedule_TBL",
                               O_status_code            IN OUT   VARCHAR2,
                               I_recv_no                IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE)
RETURN BOOLEAN IS
   L_po_number                FM_RIB_STG_RECEIVING_DETAIL.PO_NUMBER%TYPE     := NULL;
   L_po_type                  VARCHAR2(1);
   L_poscheduledtl_rec        "RIB_POScheduleDtl_REC"    := NULL;
   L_poscheduledtl_tbl        "RIB_POScheduleDtl_TBL"    := NULL;
   L_poschedule_rec           "RIB_POSchedule_REC"       := NULL;
   L_poschedule_tbl           "RIB_POSchedule_TBL"       := NULL;
   L_poscheduledesc_rec       "RIB_POScheduleDesc_REC"   := NULL;
   L_item                     FM_RIB_STG_RECEIVING_DETAIL.ITEM%TYPE;
   L_ordered_qty              FM_RIB_STG_RECEIVING_DETAIL.ORDERED_QTY%TYPE;

   cursor C_GET_PO is
        select distinct(po_number),
               'P'
          from fm_rib_stg_receiving_detail
         where recv_no = I_recv_no;

  cursor C_GET_ITEM is
        select item,
               ordered_qty
          from fm_rib_stg_receiving_detail
         where po_number = L_po_number
           and recv_no   = I_recv_no;

BEGIN

   O_POSchedule_tbl := "RIB_POSchedule_TBL"();
   L_poscheduledtl_tbl :="RIB_POScheduleDtl_TBL"();
   L_poschedule_rec := "RIB_POSchedule_REC"(0,
                                            NULL,
                                            NULL,
                                            NULL);

   L_poscheduledtl_rec := "RIB_POScheduleDtl_REC"(0,NULL,NULL);

   open C_GET_PO;
      loop
          fetch C_GET_PO into L_po_number,L_po_type;
          EXIT when C_GET_PO%NOTFOUND;
          open C_GET_ITEM;
          loop
              fetch C_GET_ITEM into L_item,L_ordered_qty;
              EXIT when C_GET_ITEM%NOTFOUND;
                 L_poscheduledtl_rec := "RIB_POScheduleDtl_REC"(0, L_item, L_ordered_qty);
                 L_poscheduledtl_tbl.EXTEND;
                 L_poscheduledtl_tbl(L_poscheduledtl_tbl.count) := L_poscheduledtl_rec;
          end loop;
          close C_GET_ITEM;

          L_poschedule_rec.requisition_nbr := L_po_number;
          L_poschedule_rec.requisition_type := L_po_type;
          L_poschedule_rec.POScheduleDtl_TBL := L_poscheduledtl_tbl;
          O_POSchedule_tbl.EXTEND;
          O_POSchedule_tbl(O_POSchedule_tbl.count):=L_poschedule_rec;
          L_poscheduledtl_tbl :="RIB_POScheduleDtl_TBL"();
      end loop;
   close C_GET_PO;


   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SCHED_SUBMIT.BUILD_DETAIL_OBJECTS',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BUILD_DETAIL_OBJECTS;
-------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_message   IN  OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_seq_no          IN           FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
IS

   L_error_type         VARCHAR2(5) := NULL;

BEGIN
   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                  I_seq_no,
                                                                  NULL,
                                                                  NULL,
                                                                  NULL,
                                                                  NULL));
      update fm_rib_receiving_mfqueue
         set pub_status = LP_error_status
       where seq_no     = I_seq_no;
   end if;

   SQL_LIB.API_MSG(L_error_type,
                   O_error_message);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'FM_SCHED_SUBMIT');
END HANDLE_ERRORS;
-------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_queue_locked    IN OUT   BOOLEAN,
                         I_seq_no          IN       FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'FM_RIB_RECEIVING_MFQUEUE';
   L_key1              VARCHAR2(100) := I_seq_no;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked,-54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from FM_RIB_RECEIVING_MFQUEUE
       where seq_no = I_seq_no
         for update nowait;

BEGIN
   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      O_queue_locked := TRUE;
      return TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SCHED_SUBMIT.LOCK_THE_BLOCK',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_THE_BLOCK;
-------------------------------------------------------------------------------
FUNCTION DELETE_QUEUE_REC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_recv_no         IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE)
RETURN BOOLEAN IS

BEGIN

   delete from FM_RIB_RECEIVING_MFQUEUE
    where RECV_NO = I_recv_no;
   delete from FM_RIB_STG_RECEIVING_DETAIL
    where RECV_NO = I_recv_no;
   delete from FM_RIB_STG_RECEIVING_HEADER
    where RECV_NO = I_recv_no;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SCHED_SUBMIT.DELETE_QUERY_REC',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DELETE_QUEUE_REC;
-------------------------------------------------------------------------------

END FM_SCHED_SUBMIT ;
/