CREATE OR REPLACE PACKAGE FM_SCHED_SUBMIT  AS
---
FAMILY               CONSTANT VARCHAR2(25) := 'poschedule';
---
LP_cre_type                VARCHAR2(15) := 'poschedcre';

-------------------------------------------------------------------------------
-- This function adds the PO Scedule to the FM_RIB_RECEIVING_MFQUEUE table
-- in 'U'npublished status. It stages the recv_no(schedule number)for publishing to the RIB.
-------------------------------------------------------------------------------------------

FUNCTION ADDTOQ (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_recv_no         IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE)
RETURN BOOLEAN;

------------------------------------------------------------------------------------
-- This procedure is called from the RIB to get the next unpublished schedule number
-- on FM_RIB_RECEIVING_MFQUEUE table for publishing. Based on the seq_no number on
-- FM_RIB_RECEIVING_MFQUEUE, it calls the PROCESS_QUEUE_RECORD procedure.
-------------------------------------------------------------------------------

PROCEDURE GETNXT (O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_message_type    IN OUT   VARCHAR2,
                  O_message         IN OUT   RIB_OBJECT,
                  O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                  O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                  I_num_threads     IN       NUMBER DEFAULT 1,
                  I_thread_val      IN       NUMBER DEFAULT 1);
-------------------------------------------------------------------------------
-- This function re-processes the records in the FM_RIB_RECEIVING_MFQUEUE table
-- with pub_status equal to 'H'.
-------------------------------------------------------------------------------
PROCEDURE PUB_RETRY (O_status_code     IN OUT   VARCHAR2,
                     O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_message_type    IN OUT   VARCHAR2,
                     O_message         IN OUT   RIB_OBJECT,
                     O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                     O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                     I_ref_object      IN       RIB_OBJECT);

-------------------------------------------------------------------------------
END FM_SCHED_SUBMIT;
/