CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DOCUMENT_VAL_SQL is
-----------------------------------------------------------------------------------
FUNCTION GET_ENTITY_INFO(O_error_message  IN OUT VARCHAR2,
                         O_cnpjcfp_type   IN OUT V_FISCAL_ATTRIBUTES.TYPE%TYPE,
                         O_cnpjcfp        IN OUT V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
                         O_addr_1         IN OUT V_FISCAL_ATTRIBUTES.ADDR_1%TYPE,
                         O_addr_2         IN OUT V_FISCAL_ATTRIBUTES.ADDR_2%TYPE,
                         O_addr_3         IN OUT V_FISCAL_ATTRIBUTES.ADDR_3%TYPE,
                         O_postal_code    IN OUT V_FISCAL_ATTRIBUTES.POSTAL_CODE%TYPE,
                         O_city           IN OUT V_FISCAL_ATTRIBUTES.CITY%TYPE,
                         O_district       IN OUT V_FISCAL_ATTRIBUTES.NEIGHBORHOOD%TYPE,
                         O_state          IN OUT V_FISCAL_ATTRIBUTES.STATE%TYPE,
                         O_ie             IN OUT V_FISCAL_ATTRIBUTES.IE%TYPE,
                         --O_ie_subs        IN OUT V_FISCAL_ATTRIBUTES.IE_SUBS%TYPE,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(60) := 'ORFM_FISCAL_DOCUMENT_SQL.GET_ENTITY_INFO';
   L_cnpj      V_FISCAL_ATTRIBUTES.CNPJ%TYPE;
   L_cpf       V_FISCAL_ATTRIBUTES.CPF%TYPE;
   ---
   cursor C_FISCAL_ATTRIBUTES  is
      select a.type,
             a.cnpj,
             a.cpf,
             a.addr_1,
             a.addr_2,
             a.addr_3,
             a.postal_code,
             a.city,
             a.neighborhood,
             a.state,
             a.ie
             --a.ie_subs
        from v_fiscal_attributes a
       where a.module = I_module
         and a.key_value_1 = I_key_value_1
         and a.key_value_2 = I_key_value_2;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_FISCAL_ATTRIBUTES ',
                    'V_FISCAL_ATTRIBUTES',
                    SUBSTR('Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1 ||
                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
   open C_FISCAL_ATTRIBUTES ;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_FISCAL_ATTRIBUTES ',
                    'V_FISCAL_ATTRIBUTES',
                    SUBSTR('Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1 ||
                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
   fetch C_FISCAL_ATTRIBUTES  into O_cnpjcfp_type,
                                          L_cnpj,
                                          L_cpf,
                                          O_addr_1,
                                          O_addr_2,
                                          O_addr_3,
                                          O_postal_code,
                                          O_city,
                                          O_district,
                                          O_state,
                                          O_ie;
                                          --O_ie_subs;
   if C_FISCAL_ATTRIBUTES %FOUND then
      if O_cnpjcfp_type = 'J' then
         O_cnpjcfp := L_cnpj;
      elsif O_cnpjcfp_type = 'F' then
         O_cnpjcfp := L_cpf;
      else
         O_cnpjcfp := NULL;
      end if;
   else
      O_cnpjcfp := NULL;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_FISCAL_ATTRIBUTES ',
                    'V_FISCAL_ATTRIBUTES',
                    SUBSTR('Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1 ||
                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
   close C_FISCAL_ATTRIBUTES ;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ENTITY_INFO;
-----------------------------------------------------------------------------------

FUNCTION GET_CITY_DESC(O_error_message  IN OUT VARCHAR2,
                       O_city_desc      IN OUT COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                       I_city           IN     COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_DOCUMENT_SQL.GET_CITY_DESC';
   ---
   cursor C_CITY is
      select a.jurisdiction_desc
        from country_tax_jurisdiction a
       where a.jurisdiction_code= I_city
        and  a.country_id = 'BR';
        
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
			  'C_CITY',
                    'COUNTRY_TAX_JURISDICTION',                    
                    'JURISDICTION_CODE= ' || I_city || 'COUNTRY_ID= ' || '''BR''');
                    
   open C_CITY;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CITY',
                    'COUNTRY_TAX_JURISDICTION',                    
                    'JURISDICTION_CODE= ' || I_city || 'COUNTRY_ID= ' || '''BR''');
                    
   fetch C_CITY into O_city_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CITY',
                    'COUNTRY_TAX_JURISDICTION',                    
                    'JURISDICTION_CODE= ' || I_city || 'COUNTRY_ID= ' || '''BR''');
                    
   close C_CITY;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CITY_DESC;
-----------------------------------------------------------------------------------
FUNCTION GET_SUPP(O_error_message  IN OUT VARCHAR2,
                  O_supp_desc      IN OUT SUPS.SUP_NAME%TYPE,
                  I_supp           IN     SUPS.SUPPLIER%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_DOCUMENT_SQL.GET_SUPP';
   ---
   cursor C_SUPP is
      select sup_name
        from sups
       where supplier = I_supp;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUPP',
                    'SUPPS',
                    'Supplier = ' || I_supp);
   open C_SUPP;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUPP',
                    'SUPPS',
                    'Supplier = ' || I_supp);
   fetch C_SUPP into O_supp_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUPP',
                    'SUPPS',
                    'Supplier = ' || I_supp);
   close C_SUPP;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_SUPP;
-----------------------------------------------------------------------------------
FUNCTION GET_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                      O_cust_desc      IN OUT CUSTOMER.CUST_NAME%TYPE,
                      I_customer       IN     CUSTOMER.CUST_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_DOCUMENT_SQL.GET_CUSTOMER';
   ---
   cursor C_CUST is
      select cust_name
        from customer
       where cust_id = I_customer;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CUST',
                    'CUSTOMER',
                    'Customer = ' || I_customer);
   open C_CUST;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CUST',
                    'CUSTOMER',
                    'Customer = ' || I_customer);
   fetch C_CUST into O_cust_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CUST',
                    'CUSTOMER',
                    'Customer = ' || I_customer);
   close C_CUST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CUSTOMER;
-----------------------------------------------------------------------------------
FUNCTION GET_PARTNER(O_error_message  IN OUT VARCHAR2,
                     O_part_desc      IN OUT PARTNER.PARTNER_DESC%TYPE,
                     I_partner        IN     PARTNER.PARTNER_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_DOCUMENT_SQL.GET_PARTNER';
   ---
   cursor C_PART is
      select partner_desc
        from partner
       where partner_id = I_partner;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_PART',
                    'PARTNER',
                    'Partner = ' || I_partner);
   open C_PART;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_PART',
                    'PARTNER',
                    'Partner = ' || I_partner);
   fetch C_PART into O_part_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_PART',
                    'PARTNER',
                    'Partner = ' || I_partner);
   close C_PART;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PARTNER;
-----------------------------------------------------------------------------------

FUNCTION GET_LOC(O_error_message  IN OUT VARCHAR2,
                 O_loc_desc       IN OUT STORE.STORE_NAME%TYPE,
                 I_loc            IN     STORE.STORE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_DOCUMENT_SQL.GET_LOC';
   ---
   cursor C_LOC is
      select store_name
        from store
       where store = I_loc;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOC',
                    'STORE',
                    'Store = ' || I_loc);
   open C_LOC;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOC',
                    'STORE',
                    'Store = ' || I_loc);
   fetch C_LOC into O_loc_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOC',
                    'STORE',
                    'Store = ' || I_loc);
   close C_LOC;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_LOC;
-----------------------------------------------------------------------------------

FUNCTION GET_OUTLOC(O_error_message  IN OUT VARCHAR2,
                    O_outloc_desc    IN OUT OUTLOC.OUTLOC_DESC%TYPE,
                    I_outloc         IN     OUTLOC.OUTLOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_DOCUMENT_SQL.GET_OUTLOC';
   ---
   cursor C_OUTLOC is
      select outloc_desc
        from outloc
       where outloc_id = I_outloc;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_OUTLOC',
                    'OUTLOC',
                    'Outloc_Id = ' || I_outloc);
   open C_OUTLOC;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_OUTLOC',
                    'OUTLOC',
                    'Outloc_Id = ' || I_outloc);
   fetch C_OUTLOC into O_outloc_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_OUTLOC',
                    'OUTLOC',
                    'Outloc_Id = ' || I_outloc);
   close C_OUTLOC;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_OUTLOC;
-----------------------------------------------------------------------------------
FUNCTION GET_COMP(O_error_message  IN OUT VARCHAR2,
                  O_comp_desc      IN OUT COMPHEAD.CO_NAME%TYPE,
                  I_comp           IN     COMPHEAD.COMPANY%TYPE)

   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_DOCUMENT_SQL.GET_COMP';
   ---
   cursor C_COMP is
      select co_name
        from comphead
       where company = I_comp;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_COMP',
                    'COMPHEAD',
                    'Company = ' || I_comp);
   open C_COMP;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_COMP',
                    'COMPHEAD',
                    'Company = ' || I_comp);
   fetch C_COMP into O_comp_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COMP',
                    'COMPHEAD',
                    'Company = ' || I_comp);
   close C_COMP;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_COMP;

-----------------------------------------------------------------------------------
END FM_FISCAL_DOCUMENT_VAL_SQL;
/