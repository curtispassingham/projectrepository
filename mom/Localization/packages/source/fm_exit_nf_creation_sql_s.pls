CREATE OR REPLACE PACKAGE FM_EXIT_NF_CREATION_SQL IS
-------------------------------------------------------------------------------
  -- Constants
  ---
   C_SUCCESS                   CONSTANT INTEGER := 1;
   C_APPROVED_STATUS           CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'A';
   C_COMPLETED_STATUS          CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'C';
   C_VALIDATED_STATUS          CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'V';
   C_PROCESSED_STATUS          CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'P';
   C_ERROR_STATUS              CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'E';
   C_HOLD_STATUS               CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'H';

  -- Public function and procedure declarations
-------------------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_unit_cost                IN OUT   FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                       O_fiscal_doc_line_id_ref   IN OUT   FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID_REF%TYPE,
                       O_fiscal_doc_id_ref        IN OUT   FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID_REF%TYPE,
                       I_location_type            IN       FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                       I_location_id              IN       FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                       I_item                     IN       FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                       I_comp_item                IN       FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                       I_pack_level               IN       FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                       I_requisition_type         IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION SCHEDULE_APPROVAL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_fiscal_doc_id      IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE ,
                           I_schedule_no        IN       FM_SCHEDULE.SCHEDULE_NO%TYPE,
                           I_requisition_type   IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                           I_api_ind            IN       VARCHAR2 DEFAULT 'N',
                           I_wasteadj_ind       IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS_OUTBOUND(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_process_status     IN OUT   INTEGER,
                          I_fiscal_doc_id      IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE ,
                          I_utilization_id     IN       FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE,
                          I_schedule_no        IN       FM_SCHEDULE.SCHEDULE_NO%TYPE,
                          I_requisition_type   IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                          I_form_ind           IN       VARCHAR2 DEFAULT 'N',
                          I_wasteadj_ind       IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CALC_EBC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_ebc_cost             IN OUT   FM_STG_RTV_DTL.EBC_COST%TYPE,
                  O_base_cost            IN OUT   FM_FISCAL_DOC_DETAIL.BASE_COST%TYPE,
                  I_nic_cost             IN       FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                  I_fiscal_doc_line_id   IN       FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_status_code        IN OUT   INTEGER,
                 I_requisition_type   IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                 I_seq_no             IN       FM_STG_RTV_DESC.SEQ_NO%TYPE,
                 I_wasteadj_ind       IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC_ID_STG_TB(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END FM_EXIT_NF_CREATION_SQL;
/