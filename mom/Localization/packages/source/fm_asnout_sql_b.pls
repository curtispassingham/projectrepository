CREATE OR REPLACE PACKAGE BODY FM_ASNOUT_SQL IS

/* Declare functions.*/
----------------------------------------------------------------------------------
-- Function Name: PARSE_ASNOUTDISTRO
----------------------------------------------------------------------------------
FUNCTION PARSE_ASNOUTDISTRO(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_asndistro        IN       "RIB_ASNOutDistro_TBL",
                            I_seq_asnoutdesc   IN       NUMBER)
RETURN BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: PARSE_ASNOUTCTN
----------------------------------------------------------------------------------
FUNCTION PARSE_ASNOUTCTN(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_asnctn             IN       "RIB_ASNOutCtn_TBL",
                         I_seq_asnoutdesc     IN       NUMBER,
                         I_seq_asnoutdistro   IN       NUMBER)
RETURN BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: PARSE_ASNOUTITEM
----------------------------------------------------------------------------------
FUNCTION PARSE_ASNOUTITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_asnitem            IN       "RIB_ASNOutItem_TBL",
                          I_seq_asnoutdesc     IN       NUMBER,
                          I_seq_asnoutdistro   IN       NUMBER,
                          I_seq_asnoutctn      IN       NUMBER)
RETURN BOOLEAN;

/* Function and Procedure Bodies */
-------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_seq_no          IN OUT   FM_STG_ASNOUT_DESC.SEQ_NO%TYPE,
                 I_message         IN       RIB_OBJECT,
                 I_message_type    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'FM_ASNOUT_SQL.CONSUME';
   L_asnoutdesc   "RIB_ASNOutDesc_REC";

BEGIN

   L_asnoutdesc := treat(I_message as "RIB_ASNOutDesc_REC");

   if FM_STG_VALIDATION_SQL.PROCESS_VALIDATION(O_error_message,
                                               L_asnoutdesc,
                                               I_message_type,
                                               'TSF') = FALSE then

     return FALSE;
   end if;

   if FM_ASNOUT_SQL.PARSE_ASNOUTDESC(O_error_message,
                                     O_seq_no,
                                     L_asnoutdesc) = FALSE then

     return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CONSUME;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_ASNOUTDESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_seq_no          IN OUT   FM_STG_ASNOUT_DESC.SEQ_NO%TYPE,
                          I_asnoutdesc      IN       "RIB_ASNOutDesc_REC")
RETURN BOOLEAN IS

   L_program          VARCHAR2(100)   := 'FM_ASNOUT_SQL.PARSE_ASNOUTDESC';
   L_seq_asnoutdesc   NUMBER          := NULL;

   cursor C_SEQ_ASNOUTDESC is
     select fm_stg_asnout_desc_seq.nextval
       from dual;

BEGIN

   --
   open C_SEQ_ASNOUTDESC;
   fetch C_SEQ_ASNOUTDESC into L_seq_asnoutdesc;
   close C_SEQ_ASNOUTDESC;
   --
   insert into fm_stg_asnout_desc(seq_no,
                                  to_location,
                                  from_location,
                                  asn_nbr,
                                  asn_type,
                                  container_qty,
                                  bol_nbr,
                                  shipment_date,
                                  est_arr_date,
                                  ship_address1,
                                  ship_address2,
                                  ship_address3,
                                  ship_address4,
                                  ship_address5,
                                  ship_city,
                                  ship_state,
                                  ship_zip,
                                  ship_country_id,
                                  trailer_nbr,
                                  seal_nbr,
                                  carrier_code,
                                  transshipment_nbr,
                                  comments,
                                  status,
                                  create_datetime,
                                  create_id,
                                  last_update_datetime,
                                  last_update_id)
                          values (L_seq_asnoutdesc,
                                  I_asnoutdesc.to_location,
                                  I_asnoutdesc.from_location,
                                  I_asnoutdesc.asn_nbr,
                                  I_asnoutdesc.asn_type,
                                  I_asnoutdesc.container_qty,
                                  I_asnoutdesc.bol_nbr,
                                  I_asnoutdesc.shipment_date,
                                  I_asnoutdesc.est_arr_date,
                                  I_asnoutdesc.ship_address1,
                                  I_asnoutdesc.ship_address2,
                                  I_asnoutdesc.ship_address3,
                                  I_asnoutdesc.ship_address4,
                                  I_asnoutdesc.ship_address5,
                                  I_asnoutdesc.ship_city,
                                  I_asnoutdesc.ship_state,
                                  I_asnoutdesc.ship_zip,
                                  I_asnoutdesc.ship_country_id,
                                  I_asnoutdesc.trailer_nbr,
                                  I_asnoutdesc.seal_nbr,
                                  I_asnoutdesc.carrier_code,
                                  I_asnoutdesc.transshipment_nbr,
                                  I_asnoutdesc.comments,
                                  'U',
                                  sysdate,
                                  user,
                                  sysdate,
                                  user);

   -- Invoke PARSE_ASNOUTDISTRO function
   if PARSE_ASNOUTDISTRO(O_error_message,
                         I_asnoutdesc.ASNOutDistro_TBL,
                         L_seq_asnoutdesc) = FALSE then
     return FALSE;
   end if;

   O_seq_no := L_seq_asnoutdesc;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
        return FALSE;
END PARSE_ASNOUTDESC;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_ASNOUTDISTRO(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_asndistro        IN       "RIB_ASNOutDistro_TBL",
                            I_seq_asnoutdesc   IN       NUMBER)
RETURN BOOLEAN IS

   L_program            VARCHAR2(100)   := 'FM_ASNOUT_SQL.PARSE_ASNOUTDISTRO';

   L_asnoutdistro       "RIB_ASNOutDistro_REC";
   L_seq_asnoutdistro   NUMBER := NULL;
   L_count              NUMBER := NULL;

   cursor C_SEQ_ASNOUTDISTRO is
      select fm_stg_asnout_distro_seq.nextval
        from dual;

BEGIN

   FOR L_count IN I_asndistro.FIRST .. I_asndistro.LAST LOOP
      --
      open C_SEQ_ASNOUTDISTRO;
      fetch C_SEQ_ASNOUTDISTRO into L_seq_asnoutdistro;
      close C_SEQ_ASNOUTDISTRO;
      --
      L_asnoutdistro := I_asndistro(L_count);
      insert into fm_stg_asnout_distro(seq_no,
                                       desc_seq_no,
                                       distro_nbr,
                                       distro_doc_type,
                                       customer_order_nbr,
                                       consumer_direct,
                                       comments,
                                       create_datetime,
                                       create_id,
                                       last_update_datetime,
                                       last_update_id)
                               values (L_seq_asnoutdistro,
                                       I_seq_asnoutdesc,
                                       L_asnoutdistro.distro_nbr,
                                       L_asnoutdistro.distro_doc_type,
                                       L_asnoutdistro.customer_order_nbr,
                                       L_asnoutdistro.consumer_direct,
                                       L_asnoutdistro.comments,
                                       sysdate,
                                       user,
                                       sysdate,
                                       user);
      -- Call function PARSE_ASNOUTCTN
      if PARSE_ASNOUTCTN(O_error_message,
                         L_asnoutdistro.ASNOutCtn_TBL,
                         I_seq_asnoutdesc,
                         L_seq_asnoutdistro) = FALSE then
        return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
        return FALSE;
END PARSE_ASNOUTDISTRO;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_ASNOUTCTN(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_asnctn             IN       "RIB_ASNOutCtn_TBL",
                         I_seq_asnoutdesc     IN       NUMBER,
                         I_seq_asnoutdistro   IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(100)   := 'FM_ASNOUT_SQL.PARSE_ASNOUTCTN';

   L_asnoutctn       "RIB_ASNOutCtn_REC";
   L_seq_asnoutctn   NUMBER := NULL;
   L_count           NUMBER := NULL;

   cursor C_SEQ_ASNOUTCTN is
     select fm_stg_asnout_ctn_seq.nextval
       from dual;

BEGIN

   FOR L_count IN I_asnctn.FIRST .. I_asnctn.LAST LOOP
      --
      open C_SEQ_ASNOUTCTN;
      fetch C_SEQ_ASNOUTCTN into L_seq_asnoutctn;
      close C_SEQ_ASNOUTCTN;
      --
      L_asnoutctn := I_asnctn(L_count);
      insert into fm_stg_asnout_ctn(seq_no,
                                    desc_seq_no,
                                    distro_seq_no,
                                    final_location,
                                    container_id,
                                    container_weight,
                                    container_length,
                                    container_width,
                                    container_height,
                                    container_cube,
                                    expedite_flag,
                                    in_store_date,
                                    rma_nbr,
                                    tracking_nbr,
                                    freight_charge,
                                    master_container_id,
                                    comments,
                                    weight,
                                    weight_uom,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                             values(L_seq_asnoutctn,
                                    I_seq_asnoutdesc,
                                    I_seq_asnoutdistro,
                                    L_asnoutctn.final_location,
                                    L_asnoutctn.container_id,
                                    L_asnoutctn.container_weight,
                                    L_asnoutctn.container_length,
                                    L_asnoutctn.container_width,
                                    L_asnoutctn.container_height,
                                    L_asnoutctn.container_cube,
                                    L_asnoutctn.expedite_flag,
                                    L_asnoutctn.in_store_date,
                                    L_asnoutctn.rma_nbr,
                                    L_asnoutctn.tracking_nbr,
                                    L_asnoutctn.freight_charge,
                                    L_asnoutctn.master_container_id,
                                    L_asnoutctn.comments,
                                    L_asnoutctn.weight,
                                    L_asnoutctn.weight_uom,
                                    sysdate,
                                    user,
                                    sysdate,
                                    user);

      -- Call function PARSE_ASNOUTITEM
      if PARSE_ASNOUTITEM(O_error_message,
                          L_asnoutctn.ASNOutItem_TBL,
                          I_seq_asnoutdesc,
                          I_seq_asnoutdistro,
                          L_seq_asnoutctn ) = FALSE then
        return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
        return FALSE;
END PARSE_ASNOUTCTN;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_ASNOUTITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_asnitem            IN       "RIB_ASNOutItem_TBL",
                          I_seq_asnoutdesc     IN       NUMBER,
                          I_seq_asnoutdistro   IN       NUMBER,
                          I_seq_asnoutctn      IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(100)   := 'FM_ASNOUT_SQL.PARSE_ASNOUTITEM';

   L_asnoutitem       "RIB_ASNOutItem_REC";

   L_seq_asnoutitem   NUMBER := NULL;
   L_count            NUMBER := NULL;

   cursor C_SEQ_ASNOUTITEM is
     select fm_stg_asnout_item_seq.nextval
       from dual;

BEGIN

   FOR L_count IN I_asnitem.FIRST .. I_asnitem.LAST LOOP
      --
      open C_SEQ_ASNOUTITEM;
      fetch C_SEQ_ASNOUTITEM into L_seq_asnoutitem;
      close C_SEQ_ASNOUTITEM;
      --
      L_asnoutitem :=  I_asnitem(L_count);
      insert into fm_stg_asnout_item(seq_no,
                                     desc_seq_no,
                                     distro_seq_no,
                                     ctn_seq_no,
                                     item_id,
                                     unit_qty,
                                     priority_level,
                                     order_line_nbr,
                                     lot_nbr,
                                     final_location,
                                     from_disposition,
                                     to_disposition,
                                     voucher_number,
                                     voucher_expiration_date,
                                     container_qty,
                                     comments,
                                     ebc_cost,
                                     weight,
                                     weight_uom,
                                     create_datetime,
                                     create_id,
                                     last_update_datetime,
                                     last_update_id,
                                     nic_cost,
                                     base_cost)
                              values(L_seq_asnoutitem,
                                     I_seq_asnoutdesc,
                                     I_seq_asnoutdistro,
                                     I_seq_asnoutctn,
                                     L_asnoutitem.item_id,
                                     L_asnoutitem.unit_qty,
                                     L_asnoutitem.priority_level,
                                     L_asnoutitem.order_line_nbr,
                                     L_asnoutitem.lot_nbr,
                                     L_asnoutitem.final_location,
                                     L_asnoutitem.from_disposition,
                                     L_asnoutitem.to_disposition,
                                     L_asnoutitem.voucher_number,
                                     L_asnoutitem.voucher_expiration_date,
                                     L_asnoutitem.container_qty,
                                     L_asnoutitem.comments,
                                     L_asnoutitem.unit_cost,
                                     L_asnoutitem.weight,
                                     L_asnoutitem.weight_uom,
                                     sysdate,
                                     user,
                                     sysdate,
                                     user,
                                     L_asnoutitem.gross_cost,
                                     L_asnoutitem.base_cost);
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
        return FALSE;
END PARSE_ASNOUTITEM;
----------------------------------------------------------------------------------
END FM_ASNOUT_SQL;
/