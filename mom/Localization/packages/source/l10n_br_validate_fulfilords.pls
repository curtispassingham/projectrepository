CREATE OR REPLACE PACKAGE L10N_BR_FULFILORD_VALIDATE AS

---------------------------------------------------------------------------------------------------
-- Name   : VALIDATE_SVC_FULFILORD_L10N
-- Purpose: This is a public function that performs basic validation of staged br fulfill order
--          creation requests on SVC_BRFULFILORD. It should process all messages
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SVC_FULFILORD_L10N(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END L10N_BR_FULFILORD_VALIDATE;
/