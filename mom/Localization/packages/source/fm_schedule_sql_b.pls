CREATE OR REPLACE PACKAGE BODY FM_SCHEDULE_SQL is
-----------------------------------------------------------------------------------
FUNCTION EXIST_SCHED_NO (O_error_message    IN OUT VARCHAR2,
                         O_exists           IN OUT BOOLEAN,
                         I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_SCHEDULE_SQL.EXIST_SCHED_NO';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(100);
   ---
   cursor C_SCH is
      select 'x'
        from fm_schedule
       where schedule_no = I_schedule_no;
   ---
BEGIN
   ---
   L_key := 'schedule_no: '||I_schedule_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SCH', 'FM_SCHEDULE', L_key);
   open C_SCH;
   ---
   SQL_LIB.SET_MARK('FECTH', 'C_SCH', 'FM_SCHEDULE', L_key);
   fetch C_SCH into L_dummy;
   if C_SCH%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_SCH', 'FM_SCHEDULE', L_key);
   close C_SCH;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_SCHED_NO;
-----------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS (O_error_message IN OUT VARCHAR2,
                        I_schedule_no   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                        I_status        IN     FM_SCHEDULE.STATUS%TYPE)
   return BOOLEAN is
   ---
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program      VARCHAR2(100) := 'FM_SCHEDULE_SQL.UPDATE_STATUS';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_SCHEDULE';
   L_key          VARCHAR2(100) := 'schedule_no: ' || TO_CHAR(I_schedule_no);
   ---
   cursor C_LOCK_SCHEDULE is
      select 'X'
        from fm_schedule fs
       where fs.schedule_no = I_schedule_no
         for update nowait;
   ---
BEGIN
   ---
   if I_schedule_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_schedule_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_LOCK_SCHEDULE';
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_LOCK_SCHEDULE;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_LOCK_SCHEDULE;
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
   update fm_schedule fs
      set fs.status = I_status
     where fs.schedule_no = I_schedule_no;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_SCHEDULE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SCHEDULE', L_table, L_key);
         close C_LOCK_SCHEDULE;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_SCHEDULE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SCHEDULE', L_table, L_key);
         close C_LOCK_SCHEDULE;
      end if;
      ---
      return FALSE;
END UPDATE_STATUS;
----------------------------------------------------------------------------------
FUNCTION EXIST_SCHED_NO_LOC (O_error_message    IN OUT VARCHAR2,
                             O_exists           IN OUT BOOLEAN,
                             I_location         IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                             I_loc_type         IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                             I_schedule_no      IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_SCHEDULE_SQL.EXIST_SCHED_NO_LOC';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(500);
   ---
   cursor C_SCH is
      select 'x'
        from fm_schedule
       where schedule_no = I_schedule_no
         and location_id = I_location
         and location_type = ''||I_loc_type||'';
   ---
BEGIN
   ---
   L_key := 'schedule_no: '||I_schedule_no||' location_id: '||I_location||' location_type: '||I_loc_type;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SCH', 'FM_SCHEDULE', L_key);
   open C_SCH;
   ---
   SQL_LIB.SET_MARK('FECTH', 'C_SCH', 'FM_SCHEDULE', L_key);
   fetch C_SCH into L_dummy;
   if C_SCH%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_SCH', 'FM_SCHEDULE', L_key);
   close C_SCH;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_SCHED_NO_LOC;
-----------------------------------------------------------------------------------
FUNCTION GET_MODE_TYPE(O_error_message  IN OUT VARCHAR2,
                       O_mode_type      IN OUT FM_SCHEDULE.MODE_TYPE%TYPE,
                       I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.GET_MODE_TYPE';
   ---
   cursor C_MODE is
      select mode_type
        from fm_schedule
       where schedule_no = I_schedule_no;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_MODE','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   open C_MODE;
   ---
   SQL_LIB.SET_MARK('FETCH','C_MODE','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   fetch C_MODE into O_mode_type;
   if C_MODE%NOTFOUND then
      O_mode_type := NULL;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_MODE','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   close C_MODE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_MODE_TYPE;
----------------------------------------------------------------------------

FUNCTION EXIST_SCHED_NO_ERR (O_error_message    IN OUT VARCHAR2,
                             O_exists           IN OUT BOOLEAN,
                             I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_SCHEDULE_SQL.EXIST_SCHED_NO';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(100);
   ---
   cursor C_SCH is
      select 'x'
        from fm_schedule
       where schedule_no = I_schedule_no
         and status = 'E';
   ---
BEGIN
   ---
   L_key := 'schedule_no: '||I_schedule_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SCH', 'FM_SCHEDULE', L_key);
   open C_SCH;
   ---
   SQL_LIB.SET_MARK('FECTH', 'C_SCH', 'FM_SCHEDULE', L_key);
   fetch C_SCH into L_dummy;
   if C_SCH%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_SCH', 'FM_SCHEDULE', L_key);
   close C_SCH;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_SCHED_NO_ERR;
-----------------------------------------------------------------------------------
FUNCTION CREATE_NEW_SCHEDULE (O_error_message  IN OUT VARCHAR2,
                              O_schedule_no    IN OUT FM_SCHEDULE.SCHEDULE_NO%TYPE,
                              I_loc_id         IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                              I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                              I_mode_type      IN     FM_SCHEDULE.MODE_TYPE%TYPE)
   return BOOLEAN is
   ---
   C_NEW_STATUS    FM_SCHEDULE.STATUS%TYPE := 'W';
   ---
   L_program       VARCHAR2(100) := 'FM_SCHEDULE_SQL.CREATE_NEW_SCHEDULE';
   L_schedule_no   FM_SCHEDULE.SCHEDULE_NO%TYPE;
   L_vdate         DATE := TRUNC(SYSDATE);
   ---
BEGIN
   ---
   if FM_SCHEDULE_VAL_SQL.GET_SCHEDULE_NUMBER_SEQ(O_error_message,
                                                  L_schedule_no) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'FM_SCHEDULE', 'Schedule_no: ' || TO_CHAR(L_schedule_no));
   insert into fm_schedule
      (schedule_no,
       location_id,
       location_type,
       schedule_date,
       mode_type,
       status,
       accounting_date,
       create_datetime,
       create_id,
       last_update_datetime,
       last_update_id)
      values
      (L_schedule_no,
       I_loc_id,
       I_loc_type,
       L_vdate,
       I_mode_type,
       C_NEW_STATUS,
       L_vdate,
       SYSDATE,
       USER,
       SYSDATE,
       USER);
   ---
   O_schedule_no := L_schedule_no;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
END CREATE_NEW_SCHEDULE;

----------------------------------------------------------------------------

FUNCTION EXIST_SCHED_NO_LOG (O_error_message    IN OUT VARCHAR2,
                             O_exists           IN OUT BOOLEAN,
                             I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_SCHEDULE_SQL.EXIST_SCHED_NO_LOG';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(100);
   ---
   cursor C_SCH is
      select DISTINCT 'x'
        from fm_schedule s
           , fm_fiscal_doc_header h
           , fm_error_log e
        where s.schedule_no = h.schedule_no
          and s.schedule_no = I_schedule_no
          and e.fiscal_doc_id = h.fiscal_doc_id;
   ---
BEGIN
   ---
   L_key := 'schedule_no: '||I_schedule_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SCH', 'FM_SCHEDULE', L_key);
   open C_SCH;
   ---
   SQL_LIB.SET_MARK('FECTH', 'C_SCH', 'FM_SCHEDULE', L_key);
   fetch C_SCH into L_dummy;
   if C_SCH%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_SCH', 'FM_SCHEDULE', L_key);
   close C_SCH;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_SCHED_NO_LOG;
-----------------------------------------------------------------------------------
FUNCTION DISAPPROVE_SCHEDULE(O_error_message    IN OUT VARCHAR2,
                             I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE,
                             I_accounting_date  IN FM_SCHEDULE.ACCOUNTING_DATE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_SCHEDULE_SQL.DISAPPROVE_SCHEDULE';
   L_key       VARCHAR2(100);
   ---
   L_new_schedule    FM_SCHEDULE.SCHEDULE_NO%TYPE;
   ---
   cursor C_FM_SCHEDULE is
      select fs.location_id, fs.location_type, fs.mode_type
        from fm_schedule fs
       where fs.schedule_no = I_schedule_no;
   ---
   R_schedule  C_FM_SCHEDULE%ROWTYPE;
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select fdd.fiscal_doc_id
        from fm_fiscal_doc_header fdd
       where fdd.schedule_no = I_schedule_no
         and fdd.requisition_type = 'PO'
         and fdd.status <> 'I';
   ---
   R_doc_header  C_FM_FISCAL_DOC_HEADER%ROWTYPE;
   ---
BEGIN
   ---
   L_key := 'schedule_no: '||I_schedule_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_SCHEDULE', 'FM_SCHEDULE', L_key);
   open C_FM_SCHEDULE;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_FM_SCHEDULE', 'FM_SCHEDULE', L_key);
   fetch C_FM_SCHEDULE into R_schedule;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_SCHEDULE', 'FM_SCHEDULE', L_key);
   close C_FM_SCHEDULE;

   --- Create a new schedule to reversion
   if FM_SCHEDULE_SQL.CREATE_NEW_SCHEDULE(O_error_message,
                                          L_new_schedule,
                                          R_schedule.location_id,
                                          R_schedule.location_type,
                                          R_schedule.mode_type) = FALSE then
      return FALSE;
   end if;

   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   LOOP
      SQL_LIB.SET_MARK('FECTH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      fetch C_FM_FISCAL_DOC_HEADER into R_doc_header;
      EXIT when C_FM_FISCAL_DOC_HEADER%NOTFOUND;
      ---
      if FM_FISCAL_DOC_HEADER_SQL.REVERSE_FISCAL_DOC(O_error_message,
                                                     L_new_schedule,
                                                     R_doc_header.fiscal_doc_id) = FALSE then
         return FALSE;
      end if;
      ---
   END LOOP;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DOC_HEADER;

   --- Revertes all interface staging tables with information
   --- from an entire schedule.
   /*commented for code drop 1--exit NF team
   if FM_INTERFACE_PROCESSES_SQL.POPULATE_STG_TABLES(O_error_message,
                                                     'Y',--Reversion_ind
                                                     I_schedule_no) = FALSE then
      return FALSE;
   end if;
   */

   --- Changes status of schedule
   if FM_SCHEDULE_SQL.UPDATE_STATUS(O_error_message,
                                    I_schedule_no,
                                    'D') = FALSE then
      return FALSE;
   end if;

   --- Update the accounting date
   if I_accounting_date is NOT NULL then
      update fm_schedule fs
         set fs.accounting_date = I_accounting_date
       where fs.schedule_no = L_new_schedule;
   end if;

   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DISAPPROVE_SCHEDULE;
-----------------------------------------------------------------------------------
FUNCTION EXIST_REQUISITON_TYPE_PO_NO (O_error_message    IN OUT VARCHAR2,
                                      O_exists           IN OUT BOOLEAN,
                                      I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_SCHEDULE_SQL.EXIST_REQUISITON_TYPE_PO_NO';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(100);
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select 'x'
        from fm_fiscal_doc_header fdh
       where fdh.schedule_no = I_schedule_no
         and fdh.requisition_type = 'PO'
         and fdh.status = 'C';
   ---
BEGIN
   ---
   L_key := 'schedule_no: '||I_schedule_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('FECTH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   fetch C_FM_FISCAL_DOC_HEADER into L_dummy;
   if C_FM_FISCAL_DOC_HEADER%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_REQUISITON_TYPE_PO_NO;
-----------------------------------------------------------------------------------
FUNCTION GET_REQUISITON_TYPE(O_error_message    IN OUT VARCHAR2,
                             O_requisition_type IN OUT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                             O_nfe_status       IN OUT FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                             I_schedule_no      IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_SCHEDULE_SQL.GET_REQUISITON_TYPE';
   L_key       VARCHAR2(100);
   
   cursor C_FM_FISCAL_DOC_HEADER is
      select requisition_type,
             status 
        from fm_fiscal_doc_header fdh
       where fdh.schedule_no = I_schedule_no;
BEGIN
   ---
   L_key := 'schedule_no: '||I_schedule_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
   --
   SQL_LIB.SET_MARK('FECTH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   fetch C_FM_FISCAL_DOC_HEADER into O_requisition_type,O_nfe_status;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_REQUISITON_TYPE;
----------------------------------------------------------------------------------------------------
FUNCTION GET_UTIL_FISCAL_ID(O_error_message    IN OUT VARCHAR2,
                            O_utilization_id   OUT FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                            O_fiscal_doc_id    OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_schedule_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE)

   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_SCHEDULE_SQL.GET_UTIL_FISCAL_ID';
   L_key       VARCHAR2(100);
   ---
   cursor C_FM_FISCAL_DOC_HEADER is
      select utilization_id,fiscal_doc_id
        from fm_fiscal_doc_header fdh
       where fdh.schedule_no = I_schedule_no;
   ---
BEGIN
   ---
   L_key := 'schedule_no: '||I_schedule_no;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('FECTH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   fetch C_FM_FISCAL_DOC_HEADER into O_utilization_id,O_fiscal_doc_id;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_UTIL_FISCAL_ID;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_SCHD_STATUS(O_error_message  IN OUT VARCHAR2,
                         O_status         IN OUT FM_SCHEDULE.STATUS%TYPE,
                         I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_FIND_SQL.GET_SCHD_STATUS';
   ---
   cursor C_STATUS is
      select status
        from fm_schedule
       where schedule_no = I_schedule_no;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_STATUS','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   open C_STATUS;
   ---
   SQL_LIB.SET_MARK('FETCH','C_STATUS','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   fetch C_STATUS  into O_status;
   if C_STATUS%NOTFOUND then
      O_status := NULL;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_STATUS','FM_SCHEDULE','schedule_no = '  || I_schedule_no);
   close C_STATUS ;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_SCHD_STATUS;
-------------------------------------------------------------------------------------------------------------------
END FM_SCHEDULE_SQL;
/