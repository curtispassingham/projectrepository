CREATE OR REPLACE PACKAGE L10N_CODES_SQL AS
---------------------------------------------------------------------
  -- Function: GET_L10N_CODE_DESC
  -- Purpose: Retrieve the decode value of a code for a given code type
---------------------------------------------------------------------
FUNCTION GET_L10N_CODE_DESC(O_error_message 	IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
                       I_code_type      IN      VARCHAR2,
                       I_code           IN      VARCHAR2,
                       O_code_desc      IN OUT  VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------
END L10N_CODES_SQL;
/