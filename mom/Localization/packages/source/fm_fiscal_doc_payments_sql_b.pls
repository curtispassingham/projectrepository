CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DOC_PAYMENTS_SQL is
-------------------------------------------------------------------
FUNCTION EXISTS(O_error_message  IN OUT VARCHAR2,
                O_exists         IN OUT BOOLEAN,
                I_fiscal_doc_id  IN     FM_FISCAL_DOC_PAYMENTS.FISCAL_DOC_ID%TYPE,
                I_payment_date   IN     FM_FISCAL_DOC_PAYMENTS.PAYMENT_DATE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_DOC_PAYMENTS_SQL.EXISTS';
   L_key       VARCHAR2(100);
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_FISCAL_DOC_PAYMENTS is
      select 'x'
        from fm_fiscal_doc_payments fdp
       where fdp.fiscal_doc_id = I_fiscal_doc_id
         and fdp.payment_date = I_payment_date;
BEGIN
   ---
   L_key := 'fiscal_doc_id = '||TO_CHAR(I_fiscal_doc_id)||' payment_date = '||TO_CHAR(I_payment_date);
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_PAYMENTS', 'FM_FISCAL_DOC_PAYMENTS', L_key);
   open C_FM_FISCAL_DOC_PAYMENTS;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_PAYMENTS', 'FM_FISCAL_DOC_PAYMENTS',L_key);
   fetch C_FM_FISCAL_DOC_PAYMENTS into L_dummy;
   O_exists := C_FM_FISCAL_DOC_PAYMENTS%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_PAYMENTS', 'FM_FISCAL_DOC_PAYMENTS',L_key);
   close C_FM_FISCAL_DOC_PAYMENTS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXISTS;
-----------------------------------------------------------------------------------
END FM_FISCAL_DOC_PAYMENTS_SQL;
/
 