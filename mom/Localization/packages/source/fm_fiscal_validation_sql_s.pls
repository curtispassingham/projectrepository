CREATE OR REPLACE PACKAGE FM_FISCAL_VALIDATION_SQL AS
   C_DISC_PERC              CONSTANT      VARCHAR2(1) := 'P';
   C_DISC_VALUE             CONSTANT      VARCHAR2(1) := 'V';
---------------------------------------------------------------------------------
-- Function Name: CHECK_DATA_INTEGRITY
-- Purpose:       This function checks for any data errors during the proces of validation
----------------------------------------------------------------------------------
FUNCTION CHECK_DATA_INTEGRITY(O_error_message IN OUT VARCHAR2,
                              O_status        IN OUT INTEGER,
                              O_other_proc    IN OUT BOOLEAN,
                              I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                              I_form_ind      IN     VARCHAR2 DEFAULT 'Y')
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_TRIANG_IND
-- Purpose:       This function finds if there exists any triangulation PO in the NF.
--------------------------------------------------------------------------------
FUNCTION GET_TRIANG_IND(O_error_message IN OUT VARCHAR2,
                        O_comp_nf_ind      OUT VARCHAR2,
                        O_triang_ind       OUT VARCHAR2,
                        I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_OTHER_NF
-- Purpose:       This function gets the other complementary NF id.
--------------------------------------------------------------------------------
FUNCTION GET_OTHER_NF  (O_error_message IN OUT VARCHAR2,
                        O_other_nf_id      OUT VARCHAR2,
                        I_comp_nf_ind   IN     VARCHAR2,
                        I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: PROCESS_VALIDATION
-- Purpose:       This function calls all the relevant package functions necessary for the validation process
----------------------------------------------------------------------------------
FUNCTION PROCESS_VALIDATION(O_error_message IN OUT VARCHAR2,
                            O_status        IN OUT INTEGER,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CALC_TOTAL_DISCOUNT
-- Purpose:       This function calculates the header and the line level discount.
--------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_DISCOUNT(O_error_message IN OUT VARCHAR2,
                             O_status        IN OUT INTEGER,
                             O_other_proc    IN OUT BOOLEAN,
                             I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CALC_UNIT_ITEM_COST_COMP
-- Purpose:       This function calculates the cost components applicable at the individual line level
--------------------------------------------------------------------------------
FUNCTION CALC_UNIT_ITEM_COST_COMP(O_error_message IN OUT VARCHAR2,
                                  O_status        IN OUT INTEGER,
                                  O_other_proc    IN OUT BOOLEAN,
                                  I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: UPDATE_CALC_VALUES
-- Purpose:       This function updates the header columns with the calculated values
--------------------------------------------------------------------------------
FUNCTION UPDATE_CALC_VALUES(O_error_message IN OUT VARCHAR2,
                            O_status        IN OUT INTEGER,
                            O_other_proc    IN OUT BOOLEAN,
                            I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_VALUE_INFORMED
-- Purpose:       This function checks if the values informed are within tolerances or not
--------------------------------------------------------------------------------
FUNCTION CHECK_VALUE_INFORMED(O_error_message IN OUT VARCHAR2,
                              O_status        IN OUT INTEGER,
                              O_other_proc    IN OUT BOOLEAN,
                              I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_VALUE_INFORMED_VS_CALC
-- Purpose:       This function checks the values informed against the ones calculated and checks if it is within tolerance or not
--------------------------------------------------------------------------------
FUNCTION CHECK_VALUE_INFORMED_VS_CALC(O_error_message IN OUT VARCHAR2,
                                      O_status        IN OUT INTEGER,
                                      O_other_proc    IN OUT BOOLEAN,
                                      I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)

   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CALC_SECOND_LEG_TAXES
-- Purpose:       This function calculates the detail taxes for the second leg in case of two-legged or repairing transfer.
--------------------------------------------------------------------------------
FUNCTION CALC_SECOND_LEG_TAXES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_status        IN OUT INTEGER,
                               O_other_proc    IN OUT BOOLEAN,
                               I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: COMPL_DATA_VALIDATION
-- Purpose:       This function checks for any data errors for COMPLEMENTARY NF during the proces of validation
----------------------------------------------------------------------------------
FUNCTION COMPL_DATA_VALIDATION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_status        IN OUT INTEGER,
                               I_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: PROCESS_EDI_VALIDATION
-- Purpose:       This function will perform the validation steps for NF, corresponding to passed edi_doc_id 
----------------------------------------------------------------------------------
FUNCTION PROCESS_EDI_VALIDATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_edi_doc_id       IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
END FM_FISCAL_VALIDATION_SQL;
/