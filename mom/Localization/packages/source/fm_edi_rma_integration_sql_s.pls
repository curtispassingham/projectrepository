CREATE OR REPLACE PACKAGE FM_EDI_RMA_INTEGRATION_SQL is
-------------------------------------------------------------------------------
-- CHANGE SEQ  - 001
-- CHANGE DATE - 15-SEP-2008
-- CHANGE USER - CNN
-- PROJECT     - 10481
-- DESCRIPTION - This process was fixed because it must consider not only the
--               POs received with base utilization_cfop, but any utilization_cfop
--               because some products can be received with a utilization_cfop different
--               from the base one.
-------------------------------------------------------------------------------

  -- Constants
  ---
  C_SUCCESS                   CONSTANT INTEGER := 1;
  C_ERROR_STATUS              CONSTANT INTEGER := 7;

  -- Public function and procedure declarations
-------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT VARCHAR2,
                   I_schedule_no   IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                   I_doc_id        IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                   I_edi_ind       IN     BOOLEAN) 
return BOOLEAN;
-------------------------------------------------------------------------------
end FM_EDI_RMA_INTEGRATION_SQL;
/
 
