CREATE OR REPLACE PACKAGE BODY FM_AUTO_SHIPPING_SQL IS
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind       IN OUT   BOOLEAN,
                 I_schedule_no     IN       FM_RECEIVING_HEADER.RECV_NO%TYPE)
RETURN BOOLEAN is

   L_program               VARCHAR2(100) := 'FM_AUTO_SHIPPING_SQL.CONSUME';
   L_status_code           VARCHAR2(1);
   L_message_type          VARCHAR(20)   := RMSSUB_ASNOUT.LP_cre_type;

   L_item                  FM_RECEIVING_DETAIL.ITEM%TYPE;
   L_requisition_no        FM_RECEIVING_DETAIL.REQUISITION_No%TYPE;
   L_document_type         FM_RECEIVING_HEADER.DOCUMENT_TYPE%TYPE;

   L_total_chrgs_prim      ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_total_chrgs_fin       ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_profit_chrgs_to_loc   ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_exp_chrgs_to_loc      ITEM_LOC.UNIT_RETAIL%TYPE := 0;

   L_total_tax_amt         FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   L_total_rec_val         FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_REC_VALUE%TYPE;
   L_ebc_cost              FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_bc_cost               FM_RECEIVING_DETAIL.EBC_COST%TYPE;

   L_distro_index          BINARY_INTEGER  := 0;
   L_ctn_index             BINARY_INTEGER  := 0;
   L_item_index            BINARY_INTEGER  := 0;

   L_batch_running_ind     RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE;
   L_seq_no                FM_STG_ASNOUT_DESC.SEQ_NO%TYPE;

   L_asnout_message        "RIB_ASNOutDesc_REC" := NULL;

   cursor C_GET_AUTO_SHIP is
      select "RIB_ASNOutDesc_REC" (0,              -- rib_oid,
                                   fh.recv_no,     -- schedule_nbr
                                   NULL,           -- auto_receive
                                   fh.location_id, -- to_location
                                   fh.key_value_1, -- from_location
                                   fh.asn_nbr,     -- asn_nbr
                                   NULL,           -- asn_type
                                   NULL,           -- container_qty
                                   fh.asn_nbr,     -- bol_nbr
                                   fh.issue_date,  -- shipment_date
                                   NULL,           -- est_arr_date
                                   NULL,           -- ship_address1
                                   NULL,           -- ship_address2
                                   NULL,           -- ship_address3
                                   NULL,           -- ship_address4
                                   NULL,           -- ship_address5
                                   NULL,           -- ship_city
                                   NULL,           -- ship_state
                                   NULL,           -- ship_zip
                                   NULL,           -- ship_country_id
                                   NULL,           -- trailer_nbr
                                   NULL,           -- seal_nbr
                                   NULL,           -- carrier_code
                                   NULL,           -- transshipment_nbr
                                   CAST (MULTISET(select "RIB_ASNOutDistro_REC"(0,                   -- rib_oid
                                                                                fh.requisition_no,   -- distro_nbr
                                                                                fh.document_type,    -- distro_doc_type
                                                                                NULL,                -- customer_order_nbr
                                                                                NULL,                -- consumer_direct
                                                                                CAST(MULTISET(select "RIB_ASNOutCtn_REC"(0,                 -- rib_oid
                                                                                                                         fc.location_id,    -- final_location
                                                                                                                         fc.container_id,   -- container_id
                                                                                                                         NULL,              -- container_weight
                                                                                                                         NULL,              -- container_length
                                                                                                                         NULL,              -- container_width
                                                                                                                         NULL,              -- container_height
                                                                                                                         NULL,              -- container_cube
                                                                                                                         NULL,              -- expedite_flag
                                                                                                                         NULL,              -- in_store_date
                                                                                                                         NULL,              -- rma_nbr
                                                                                                                         NULL,              -- tracking_nbr
                                                                                                                         NULL,              -- freight_charge
                                                                                                                         NULL,              -- master_container_id
                                                                                                                         CAST(MULTISET(select "RIB_ASNOutItem_REC"(0,                     -- rib_oid
                                                                                                                                                                   frd.item,              -- item_id
                                                                                                                                                                   SUM(frd.quantity),     -- unit_qty
                                                                                                                                                                   SUM(frd.nic_cost),     -- gross_cost
                                                                                                                                                                   NULL,                  -- priority_level
                                                                                                                                                                   NULL,                  -- order_line_nbr
                                                                                                                                                                   NULL,                  -- lot_nbr
                                                                                                                                                                   NULL,                  -- final_location
                                                                                                                                                                   NULL,                  -- from_disposition
                                                                                                                                                                   NULL,                  -- to_disposition
                                                                                                                                                                   NULL,                  -- voucher_number
                                                                                                                                                                   NULL,                  -- voucher_expiration_date
                                                                                                                                                                   NULL,                  -- container_qty
                                                                                                                                                                   NULL,                  -- comments
                                                                                                                                                                   SUM(frd.ebc_cost),     -- unit_cost
                                                                                                                                                                   NULL,                  -- base_cost
                                                                                                                                                                   NULL,                  -- weight
                                                                                                                                                                   NULL)                  -- weight_uom
                                                                                                                                         from fm_receiving_detail frd,
                                                                                                                                              fm_receiving_header frh
                                                                                                                                        where frd.header_seq_no  = frh.seq_no
                                                                                                                                          and frd.requisition_no = fc.requisition_no
                                                                                                                                          and frh.recv_no        = fc.recv_no
                                                                                                                                          and frd.container_id   = fc.container_id
                                                                                                                                          and frd.quantity is NOT NULL
                                                                                                                                     group by frd.item,
                                                                                                                                              frd.container_id,
                                                                                                                                              frd.requisition_no,
                                                                                                                                              frh.recv_no)  as "RIB_ASNOutItem_TBL"),   -- ASNOutItem_TBL
                                                                                                                         NULL,              -- comments
                                                                                                                         NULL,              -- weight
                                                                                                                         NULL)              -- weight_uom
                                                                                                from (select DISTINCT frd.container_id container_id,
                                                                                                             frh.recv_no,
                                                                                                             frd.requisition_no,
                                                                                                             frh.location_id
                                                                                                        from fm_receiving_detail frd,
                                                                                                             fm_receiving_header frh
                                                                                                       where frd.header_seq_no  = frh.seq_no
                                                                                                             and frh.status = 'R'
                                                                                                             and frd.quantity is not null) fc
                                                                                                         where fh.recv_no        = fc.recv_no
                                                                                                           and fh.requisition_no = fc.requisition_no ) as "RIB_ASNOutCtn_TBL"),   -- ASNOutCtn_TBL
                                                                                NULL)                -- comments
                                                    from dual) as "RIB_ASNOutDistro_TBL"), -- ASNOutDistro_TBL
                                   NULL)           -- comments
        from ( select DISTINCT frd.requisition_no,
                      frh.location_id,
                      frh.document_type,
                      frd.asn_nbr,
                      frh.recv_no,
                      fdh.key_value_1,
                      frh.fiscal_doc_id,
                      fdh.issue_date
                 from fm_receiving_detail frd,
                      fm_receiving_header frh,
                      fm_fiscal_doc_header fdh
                where frd.header_seq_no = frh.seq_no
                  and frh.recv_no       = fdh.schedule_no
                  and frh.fiscal_doc_id = fdh.fiscal_doc_id
                  and frh.status        = 'R'
                  and frh.recv_no       = I_schedule_no) fh;

   cursor C_GET_COSTS(P_item     FM_RECEIVING_DETAIL.ITEM%TYPE,
                      P_req_no   FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE) is
      select sum(tab.unit_tax_amt) total_tax_amt ,
             sum (tab.unit_rec_value) total_rec_value,
             sum (decode(tab.nic_ind,'Y',tab.unit_tax_amt,0)) total_bc_tax_amt,
             tab.ent_line_id,
             tab.ent_nf_item
        from (select fdtde.tax_code,
                     fdtde.tax_value as tax_value,
                     NVL(fdtde.unit_tax_amt,0) as unit_tax_amt,
                     NVL(fdtde.unit_rec_value,0) as unit_rec_value,
                     vc.incl_nic_ind as nic_ind,
                     exit_nf.ent_line_id,
                     exit_nf.ent_nf_item
                from fm_fiscal_doc_tax_detail_ext fdtde,
                     vat_codes vc,
                     fm_fiscal_doc_detail ffdd,
                     (select fdd.fiscal_doc_id,
                             fdd.fiscal_doc_line_id,
                             entry_nf.fiscal_doc_line_id ent_line_id,
                             entry_nf.nf_item ent_nf_item
                        from fm_fiscal_doc_detail fdd,
                             fm_fiscal_doc_detail fdd2,
                             (select ffdh.fiscal_doc_id,
                                     ffdd.fiscal_doc_line_id,
                                     frh.recv_no,
                                     frd.item rcvd_item,
                                     ffdd.item nf_item,
                                     nvl(frd.quantity,0)*nvl(pi.pack_qty,1) rcvd_qty,
                                     nvl(ffdd.quantity,0) nf_qty
                                from fm_receiving_detail frd,
                                     fm_fiscal_doc_detail ffdd,
                                     tsfdetail td,
                                     tsfhead th,
                                     fm_fiscal_doc_header ffdh,
                                     fm_receiving_header frh,
                                     packitem pi
                               where (frd.item = ffdd.item or
                                      frd.item = ffdd.pack_no)
                                 and td.tsf_no = th.tsf_no
                                 and ((frd.requisition_no = ffdd.requisition_no and
                                       th.tsf_no          = ffdd.requisition_no)
                                     or th.tsf_parent_no = ffdd.requisition_no)
                                 and td.item = decode(ffdd.pack_ind, 'Y', ffdd.item, NVL(ffdd.pack_no, ffdd.item))
                                 and ffdd.pack_no                   = pi.pack_no (+)
                                 and ffdd.item                      = pi.item(+)
                                 and frh.fiscal_doc_id              = ffdh.fiscal_doc_id
                                 and ffdh.fiscal_doc_id             = ffdd.fiscal_doc_id
                                 and frh.seq_no                     = frd.header_seq_no
                                 and frd.reason_code is NULL
                                 and nvl(ffdd.unexpected_item, 'N') = 'N'
                                 and nvl(ffdd.pack_no, ffdd.item)   = P_item
                                 and frh.recv_no                    = I_schedule_no
                                 and frd.requisition_no             = P_req_no
                                 and frh.requisition_type in ('TSF','IC','REP')
                                 and frd.quantity > 0) entry_nf
                       where fdd.fiscal_doc_line_id  != entry_nf.fiscal_doc_line_id
                         and fdd2.fiscal_doc_line_id  = entry_nf.fiscal_doc_line_id
                         and fdd.item = fdd2.item
                         and (fdd.requisition_no = fdd2.requisition_no
                              or (fdd.requisition_no != fdd2.requisition_no
                                     and exists (select 'X' from tsfhead
                                                  where tsf_no = fdd2.requisition_no
                                                    and tsf_parent_no = fdd.requisition_no)))) exit_nf
       where ffdd.fiscal_doc_id       = fdtde.fiscal_doc_id
         and ffdd.fiscal_doc_line_id  = fdtde.fiscal_doc_line_id
         and fdtde.fiscal_doc_line_id = exit_nf.fiscal_doc_line_id
         and fdtde.tax_code           = vc.vat_code) tab
    group by ent_line_id,
             ent_nf_item;

   R_get_costs           C_GET_COSTS%ROWTYPE;

   cursor C_GET_TSF_LOCS (P_fiscal_doc_line_id   FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select t.from_loc_type,
             t.from_loc,
             t.to_loc_type,
             t.to_loc,
             d.tsf_seq_no
        from tsfhead t,
             tsfdetail d,
             fm_fiscal_doc_detail fdd
       where t.tsf_no               = d.tsf_no
         and t.tsf_no               = fdd.requisition_no
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id
   union all
      select 'W',
             t.wh,
             d.to_loc_type,
             d.to_loc,
             NULL
        from alloc_header t,
             alloc_detail d,
             fm_fiscal_doc_detail fdd
       where t.alloc_no               = d.alloc_no
         and t.alloc_no               = fdd.requisition_no
         and fdd.fiscal_doc_line_id   = P_fiscal_doc_line_id;

   R_get_tsf_details C_GET_TSF_LOCS%ROWTYPE;

BEGIN

   open C_GET_AUTO_SHIP;
   fetch C_GET_AUTO_SHIP into L_asnout_message;
   close C_GET_AUTO_SHIP;

   L_distro_index := L_asnout_message.ASNOutDistro_TBL.FIRST;

   WHILE L_distro_index is NOT NULL LOOP
      L_requisition_no := L_asnout_message.ASNOutDistro_TBL(L_distro_index).distro_nbr;
      L_document_type  := L_asnout_message.ASNOutDistro_TBL(L_distro_index).distro_doc_type;
      L_ctn_index     := L_asnout_message.ASNOutDistro_TBL(L_distro_index).ASNOutCtn_TBL.FIRST;

      WHILE L_ctn_index is NOT NULL LOOP
         L_item_index := L_asnout_message.ASNOutDistro_TBL(L_distro_index).ASNOutCtn_TBL(L_ctn_index).ASNOutItem_TBL.FIRST;

         WHILE  L_item_index is NOT NULL LOOP
            L_item := L_asnout_message.ASNOutDistro_TBL(L_distro_index).ASNOutCtn_TBL(L_ctn_index).ASNOutItem_TBL(L_item_index).item_id;

            open C_GET_COSTS(L_item, L_requisition_no);

            -- Initialize Cost Fields
            L_total_chrgs_fin := 0;
            L_total_tax_amt   := 0;
            L_total_rec_val   := 0;
            L_ebc_cost        := 0;
            L_bc_cost         := 0;

            -- Loop thru all item, requisition_no
            LOOP
               fetch C_GET_COSTS into R_get_costs;
               EXIT when C_GET_COSTS%NOTFOUND;

               -- Get locations for transfers and allocations
               open C_GET_TSF_LOCS(R_get_costs.ent_line_id);
               fetch C_GET_TSF_LOCS into R_get_tsf_details;
               close C_GET_TSF_LOCS;

               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_total_chrgs_prim,
                                                              L_profit_chrgs_to_loc,
                                                              L_exp_chrgs_to_loc,
                                                              L_document_type,
                                                              L_requisition_no,
                                                              R_get_tsf_details.tsf_seq_no,
                                                              NULL,
                                                              NULL,
                                                              R_get_costs.ent_nf_item,
                                                              NULL,
                                                              R_get_tsf_details.from_loc,
                                                              R_get_tsf_details.from_loc_type,
                                                              R_get_tsf_details.to_loc,
                                                              R_get_tsf_details.to_loc_type) = FALSE then
                  return FALSE;
               end if;

               L_total_chrgs_fin := L_total_chrgs_fin + L_total_chrgs_prim;
               L_total_tax_amt   := L_total_tax_amt   + R_get_costs.total_tax_amt;
               L_total_rec_val   := L_total_rec_val   + R_get_costs.total_rec_value;

            end LOOP;  -- for C_GET_COSTS cursor

            close C_GET_COSTS;

            -- Set ebc and base_cost
            L_ebc_cost := L_asnout_message.ASNOutDistro_TBL(L_distro_index).ASNOutCtn_TBL(L_ctn_index).ASNOutItem_TBL(L_item_index).unit_cost;
            L_bc_cost  := L_ebc_cost - (L_total_tax_amt - L_total_rec_val ) + L_total_chrgs_fin;

            -- Assign computed base_cost to asnout_message
            L_asnout_message.ASNOutDistro_TBL(L_distro_index).ASNOutCtn_TBL(L_ctn_index).ASNOutItem_TBL(L_item_index).base_cost := L_bc_cost;

            -- Increment L_item_index
            L_item_index := L_asnout_message.ASNOutDistro_TBL(L_distro_index).ASNOutCtn_TBL(L_ctn_index).ASNOutItem_TBL.NEXT(L_item_index);

         end LOOP;  -- for all L_item_index

         -- Increment L_ctn_index
         L_ctn_index := L_asnout_message.ASNOutDistro_TBL(L_distro_index).ASNOutCtn_TBL.NEXT(L_ctn_index);

      end LOOP;  -- for all L_ctn_index

      -- Increment L_distro_index
      L_distro_index := L_asnout_message.ASNOutDistro_TBL.NEXT(L_distro_index);

   end LOOP;  -- for all L_distro_index

   L_status_code   := NULL;
   O_error_message := NULL;

   -- Check Batch Running Indicator
   if RMS_BATCH_STATUS_SQL.GET_BATCH_RUNNING_IND(O_error_message,
                                                 L_batch_running_ind) = FALSE then
      return FALSE;
   end if;

   if L_batch_running_ind = 'Y' then
      --- Call Parse ASNOutDesc
      if FM_ASNOUT_SQL.PARSE_ASNOUTDESC (O_error_message,
                                         L_seq_no,
                                         L_asnout_message) = FALSE then
         return FALSE;
      end if;

      --- Update Status to 'H'old
      if FM_INTEGRATION_SQL.UPDATE_STG_STATUS(O_error_message,
                                              'H',
                                              'TSF',
                                              L_seq_no) = FALSE then
         return FALSE;
      end if;

   else
      -- Call procedure Consume
      RMSSUB_ASNOUT.CONSUME_SHIPMENT(L_status_code,
                                     O_error_message,
                                     L_asnout_message,
                                     L_message_type,
                                     'N');

      if L_status_code = API_CODES.UNHANDLED_ERROR or O_error_message is NOT NULL then
         O_error_ind := TRUE;
         return FALSE;
      else
         O_error_ind := FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_COSTS%ISOPEN then
         close C_GET_COSTS;
      end if;
      if C_GET_TSF_LOCS%ISOPEN then
         close C_GET_TSF_LOCS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CONSUME;
---------------------------------------------------------------------------------------------
END FM_AUTO_SHIPPING_SQL;
/
