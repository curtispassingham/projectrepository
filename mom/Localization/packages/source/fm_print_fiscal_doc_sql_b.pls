create or replace PACKAGE BODY FM_PRINT_FISCAL_DOC_SQL is

   FUNCTION SET_WHERE_CLAUSE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_where             IN OUT VARCHAR2,
                             I_schedule_no       IN FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                             I_fiscal_doc_no     IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                             I_type_id           IN FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                             I_fiscal_doc_id     IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   
      L_program   VARCHAR2(100) := 'FM_PRINT_FISCAL_DOC_SQL.SET_WHERE_CLAUSE';
      L_text      VARCHAR2(2000) := NULL;
   BEGIN
      ---
      if I_schedule_no is NOT NULL then
         L_text := L_text || ' schedule_no = '|| I_schedule_no ||' and ';
      end if;
      ---
      if I_fiscal_doc_no is NOT NULL then
         L_text := L_text || ' fiscal_doc_no = '|| I_fiscal_doc_no ||' and ' ;
      end if;
      ---
      if  I_schedule_no is NULL or I_fiscal_doc_no is NULL then
       L_text := L_text || ' schedule_no in (select schedule_no
                                              from fm_schedule
                                              where status in (''A'', ''C'')) and ';
      end if;
      ---
      if I_type_id is NOT NULL then
         L_text := L_text || ' fiscal_doc_id in (select h.fiscal_doc_id
                                                  from fm_fiscal_doc_header h
                                                     , fm_fiscal_doc_type t
                                                  where h.fiscal_doc_id = fm_fiscal_doc_header.fiscal_doc_id
                                                    and h.type_id = t.type_id
                                                    and h.type_id = '||I_type_id||' ) and ' ;
      end if;
      ---
      if L_text is NOT NULL then
         L_text := SUBSTR(L_text,1,LENGTH(L_text) - 5);
      end if;
      ---
      O_where := (L_text);
      
      return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END SET_WHERE_CLAUSE;
-------------------------------------------------------------------------------     
end FM_PRINT_FISCAL_DOC_SQL;
/