CREATE OR REPLACE PACKAGE BODY FM_DOC_TYPE_UTILIZATION_SQL is
-------------------------------------------------------------------
FUNCTION EXISTS_UTILIZATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         IN OUT BOOLEAN,
                            I_type_id        IN     FM_DOC_TYPE_UTILIZATION.TYPE_ID%TYPE,
                            I_utilization_id IN     FM_DOC_TYPE_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_DOC_TYPE_UTILIZATION_SQL.EXISTS_UTILIZATION';
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_DOC_TYPE_UTILIZATION is
      select 'X'
        from fm_doc_type_utilization
       where type_id = I_type_id
         and utilization_id = I_utilization_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_DOC_TYPE_UTILIZATION','FM_DOC_TYPE_UTILIZATION','Type id: '||I_type_id||' Utilization_id: '||I_utilization_id);
   open C_FM_DOC_TYPE_UTILIZATION;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_DOC_TYPE_UTILIZATION','FM_DOC_TYPE_UTILIZATION','Type id: '||I_type_id||' Utilization_id: '||I_utilization_id);
   fetch C_FM_DOC_TYPE_UTILIZATION into L_dummy;
   O_exists := C_FM_DOC_TYPE_UTILIZATION%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_DOC_TYPE_UTILIZATION','FM_DOC_TYPE_UTILIZATION','Type id: '||I_type_id||' Utilization_id: '||I_utilization_id);
   close C_FM_DOC_TYPE_UTILIZATION;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXISTS_UTILIZATION;
-------------------------------------------------------------------
FUNCTION DELETE_DOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_type_id        IN     FM_DOC_TYPE_UTILIZATION.TYPE_ID%TYPE)

return BOOLEAN is
   ---
   L_program     VARCHAR2(50) := 'FM_DOC_TYPE_UTILIZATION_SQL.DELETE_DOC';
   L_key         VARCHAR2(50) := 'I_type_id: '||I_type_id;
   RECORD_LOCKED EXCEPTION;

   cursor C_DOC_TYPE IS
      select 'x'
        from fm_doc_type_utilization
       where type_id = I_type_id
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_DOC_TYPE', 'FM_DOC_TYPE_UTILIZATION', 'I_type_id: '||I_type_id);
   open C_DOC_TYPE;
   SQL_LIB.SET_MARK('CLOSE', 'C_DOC_TYPE', 'FM_DOC_TYPE_UTILIZATION', 'I_type_id: '||I_type_id);
   close C_DOC_TYPE;

   ---
   SQL_LIB.SET_MARK('DELETE', NULL, 'FM_DOC_TYPE_UTILIZATION', L_key);
   delete fm_doc_type_utilization
    where type_id = I_type_id;
   ---

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_DOC_TYPE_UTILIZATION',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
END DELETE_DOC;
-------------------------------------------------------------------
FUNCTION EXIST_CHILD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists         IN OUT BOOLEAN,
                     I_type_id        IN     FM_DOC_TYPE_UTILIZATION.TYPE_ID%TYPE,
                     I_utilization_id IN     FM_DOC_TYPE_UTILIZATION.UTILIZATION_ID%TYPE)
return BOOLEAN is
---
L_dummy      VARCHAR2(1);
---

cursor C_EXIST is
select 1
  from fm_fiscal_doc_header e, fm_doc_type_utilization fdtu
 where fdtu.type_id = e.type_id 
   and e.type_id = I_type_id
   and e.utilization_id = I_utilization_id;

BEGIN
   O_exists := FALSE;
   ---

   SQL_LIB.SET_MARK('OPEN','C_TYPE','FM_DOC_TYPE_UTILIZATION','Type id: '||I_type_id||' Utilization_id: '||I_utilization_id);
   open C_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_TYPE','FM_DOC_TYPE_UTILIZATION','Type id: '||I_type_id||' Utilization_id: '||I_utilization_id);
   fetch C_EXIST into L_dummy;
   if C_EXIST%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_TYPE','FM_DOC_TYPE_UTILIZATION','Type id: '||I_type_id||' Utilization_id: '||I_utilization_id);
   close C_EXIST;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_DOC_TYPE_UTILIZATION.EXIST_CHILD',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_CHILD;
-------------------------------------------------------------------
FUNCTION EXISTS_ANY_UTILIZATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists         IN OUT BOOLEAN,
                                I_utilization_id IN     FM_DOC_TYPE_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(60) := 'FM_DOC_TYPE_UTILIZATION_SQL.EXISTS_ANY_UTILIZATION';
   L_dummy     VARCHAR2(1);
   ---
   cursor C_FM_DOC_TYPE_UTILIZATION is
      select 'X'
        from fm_doc_type_utilization
       where utilization_id = I_utilization_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_DOC_TYPE_UTILIZATION','FM_DOC_TYPE_UTILIZATION',' Utilization_id: '||I_utilization_id);
   open C_FM_DOC_TYPE_UTILIZATION;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_DOC_TYPE_UTILIZATION','FM_DOC_TYPE_UTILIZATION',' Utilization_id: '||I_utilization_id);
   fetch C_FM_DOC_TYPE_UTILIZATION into L_dummy;
   O_exists := C_FM_DOC_TYPE_UTILIZATION%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_DOC_TYPE_UTILIZATION','FM_DOC_TYPE_UTILIZATION',' Utilization_id: '||I_utilization_id);
   close C_FM_DOC_TYPE_UTILIZATION;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXISTS_ANY_UTILIZATION;
-------------------------------------------------------------------
END FM_DOC_TYPE_UTILIZATION_SQL;
/
