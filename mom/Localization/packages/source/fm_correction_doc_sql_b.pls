create or replace PACKAGE BODY FM_CORRECTION_DOC_SQL as

----------------------------------------------------------------------------------
-- Function Name: GEN_CORRECT_DOC_TAX
-- Purpose:       This function inserts a record into FM_CORRECTION_DOC and
--                FM_CORRECTION_TAX_DOC tables
----------------------------------------------------------------------------------
FUNCTION GEN_CORRECT_DOC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_record             IN     FM_CORRECTION_DOC%ROWTYPE,
                         I_tax_code           IN     FM_CORRECTION_TAX_DOC.TAX_CODE%TYPE,
                         I_tax_basis          IN     FM_CORRECTION_TAX_DOC.TAX_BASIS%TYPE,
                         I_tax_amount         IN     FM_CORRECTION_TAX_DOC.TAX_AMOUNT%TYPE,
                         I_tax_rate           IN     FM_CORRECTION_TAX_DOC.TAX_RATE%TYPE,
                         I_modified_tax_basis IN     FM_CORRECTION_TAX_DOC.MODIFIED_TAX_BASIS%TYPE)
   return BOOLEAN is
   ---
   L_seq_no      FM_CORRECTION_DOC.SEQ_NO%TYPE;
   L_hdr_seq_no  FM_CORRECTION_DOC.SEQ_NO%TYPE;
   L_program     VARCHAR2(80) := 'FM_CORRECTION_DOC_SQL.GEN_CORRECT_DOC_TAX';
   L_key         VARCHAR2(80) := 'I_Fiscal_doc_line_id ' || to_char(I_record.fiscal_doc_line_id);
   L_exists      VARCHAR2(1)  := NULL;
   ---
   SEQ_MAXVAL    EXCEPTION;
   PRAGMA        EXCEPTION_INIT(SEQ_MAXVAL,-08004);
   ---
   cursor C_NEXTVAL_HDR is
   select fm_correction_doc_seq.NEXTVAL
     from dual;
   ---
   cursor C_NEXTVAL is
   select fm_correction_tax_doc_seq.NEXTVAL
     from dual;
   ---
   cursor C_EXISTS is
   select 'X' , seq_no
     from fm_correction_doc
    where fiscal_doc_line_id = I_record.fiscal_doc_line_id
      and action_type = I_record.action_type;

   cursor C_LOCK_FM_CORRECTION_DOC is
   select 'X'
     from fm_correction_doc
    where fiscal_doc_line_id = I_record.fiscal_doc_line_id
      and action_type = I_record.action_type
      for update nowait;
   ---
   cursor C_TAX_EXISTS is
   select 'X' , seq_no
     from fm_correction_tax_doc
    where header_seq_no = (select seq_no
                             from fm_correction_doc
                            where fiscal_doc_line_id = I_record.fiscal_doc_line_id
                              and action_type = I_record.action_type)
      and tax_code = I_tax_code;
   ---
   cursor C_LOCK_FM_CORRECTION_TAX_DOC is
   select 'X'
     from fm_correction_tax_doc
    where header_seq_no =  (select seq_no
                              from fm_correction_doc
                             where fiscal_doc_line_id = I_record.fiscal_doc_line_id
                               and action_type = I_record.action_type)
      and tax_code = I_tax_code
      for update nowait;
   ---
begin
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','FM_CORRECTION_DOC',L_key);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','FM_CORRECTION_DOC',L_key);
   fetch C_EXISTS into L_exists, L_hdr_seq_no;
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','FM_CORRECTION_DOC',L_key);
   close C_EXISTS;
   ---
   if L_exists is NOT NULL THEN
     ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_CORRECTION_DOC','FM_CORRECTION_DOC',L_key);
      open C_LOCK_FM_CORRECTION_DOC;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_CORRECTION_DOC','FM_CORRECTION_DOC',L_key);
      close C_LOCK_FM_CORRECTION_DOC;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'FM_CORRECTION_DOC',L_key);
      ---
      update FM_CORRECTION_DOC
         set ACTION_VALUE = I_record.action_value,
             CREATE_DATETIME = SYSDATE,
             CREATE_ID = USER,
             LAST_UPDATE_DATETIME = SYSDATE,
             LAST_UPDATE_ID = USER
       where fiscal_doc_line_id = I_record.fiscal_doc_line_id
         and action_type = I_record.action_type;
   else
      ---
      open  C_NEXTVAL_HDR;
      fetch C_NEXTVAL_HDR into L_hdr_seq_no;
      close C_NEXTVAL_HDR;
      ---
      insert into FM_CORRECTION_DOC(SEQ_NO,
                                    FISCAL_DOC_ID,
                                    ACTION_TYPE,
                                    ACTION_VALUE,
                                    CREATE_DATETIME,
                                    CREATE_ID,
                                    LAST_UPDATE_DATETIME,
                                    LAST_UPDATE_ID,
                                    FISCAL_DOC_LINE_ID)
                            values (L_hdr_seq_no,
                                    I_record.FISCAL_DOC_ID,
                                    I_record.ACTION_TYPE,
                                    I_record.ACTION_VALUE,
                                    SYSDATE,
                                    USER,
                                    SYSDATE,
                                    USER,
                                    I_record.FISCAL_DOC_LINE_ID);
   end if;
   ---
   L_exists := NULL;
   if I_tax_code is NOT NULL then
      ---

      SQL_LIB.SET_MARK('OPEN','C_TAX_EXISTS','FM_CORRECTION_TAX_DOC',L_key);
      open C_TAX_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_TAX_EXISTS','FM_CORRECTION_TAX_DOC',L_key);
      fetch C_TAX_EXISTS into L_exists, L_seq_no;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS','FM_CORRECTION_TAX_DOC',L_key);
      close C_TAX_EXISTS;
      ---
      if L_exists is NOT NULL THEN
      ---
         SQL_LIB.SET_MARK('OPEN','C_LOCK_FM_CORRECTION_TAX_DOC','FM_CORRECTION_DOC',L_key);
         open C_LOCK_FM_CORRECTION_TAX_DOC;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_FM_CORRECTION_TAX_DOC','FM_CORRECTION_DOC',L_key);
         close C_LOCK_FM_CORRECTION_TAX_DOC;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,'FM_CORRECTION_DOC',L_key);
         update FM_CORRECTION_TAX_DOC
            set tax_basis          = I_tax_basis,
                modified_tax_basis = I_modified_tax_basis,
                tax_amount         = I_tax_amount,
                tax_rate           = I_tax_rate,
                create_datetime    = SYSDATE,
                create_id          = USER,
                last_update_datetime = SYSDATE,
                last_update_id = USER
          where header_seq_no =  (select seq_no
                                    from fm_correction_doc
                                   where fiscal_doc_line_id = I_record.fiscal_doc_line_id
                                     and action_type = I_record.action_type)
            and tax_code = I_tax_code;
      else
         ---
         open  C_NEXTVAL;
         fetch C_NEXTVAL into L_seq_no;
         close C_NEXTVAL;
         ---
         insert into FM_CORRECTION_TAX_DOC(SEQ_NO,
                                           HEADER_SEQ_NO,
                                           TAX_CODE,
                                           TAX_BASIS,
                                           TAX_AMOUNT,
                                           TAX_RATE,
                                           MODIFIED_TAX_BASIS,
                                           CREATE_DATETIME,
                                           CREATE_ID,
                                           LAST_UPDATE_DATETIME,
                                           LAST_UPDATE_ID)
                                   values (L_seq_no,
                                           L_hdr_seq_no,
                                           I_tax_code,
                                           I_tax_basis,
                                           I_tax_amount,
                                           I_tax_rate,
                                           I_modified_tax_basis,
                                           SYSDATE,
                                           USER,
                                           SYSDATE,
                                           USER);
       end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;

      if C_NEXTVAL_HDR%ISOPEN then
         close C_NEXTVAL_HDR;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('NO_CE_NUM',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;

  when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;

      if C_NEXTVAL_HDR%ISOPEN then
         close C_NEXTVAL_HDR;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GEN_CORRECT_DOC;
----------------------------------------------------------------------------------
-- Function Name: DEL_CORRECTION_DOC
-- Purpose:       This function deltes record from FM_CORRECTION_DOC and FM_CORRECTION_TAX_DOC table
----------------------------------------------------------------------------------
FUNCTION DEL_CORRECTION_DOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_line_id  IN     FM_CORRECTION_DOC.FISCAL_DOC_LINE_ID%TYPE,
                            I_action_type         IN     FM_CORRECTION_DOC.ACTION_TYPE%TYPE)

return BOOLEAN is
   ---
   L_program     VARCHAR2(50) := 'FM_CORRECTION_DOC_SQL.DEL_CORRECTION_DOC';
   L_key         VARCHAR2(50) := 'I_fiscal_doc_line_id: '||I_fiscal_doc_line_id;
   RECORD_LOCKED EXCEPTION;
   L_exists      VARCHAR2(1)  := NULL;
   ---
   cursor C_EXISTS is
   select 'x'
     from fm_correction_doc
    where fiscal_doc_line_id = I_fiscal_doc_line_id
      and action_type = I_action_type;
   ---
   cursor C_TAX_EXISTS is
   select 'x'
     from fm_correction_tax_doc
    where header_seq_no =  (select seq_no
                              from FM_CORRECTION_DOC
                             where fiscal_doc_line_id = I_fiscal_doc_line_id
                               and action_type = I_action_type);
   ---
    cursor C_LOCK_CORRECTION_TAX_DOC is
      select 'x'
        from FM_CORRECTION_TAX_DOC
       where header_seq_no
          in (select seq_no
                from FM_CORRECTION_DOC
               where fiscal_doc_line_id = I_fiscal_doc_line_id
                 and action_type = I_action_type)
         for update nowait;
   ---
   cursor C_LOCK_CORRECTION_DOC IS
      select 'x'
        from fm_correction_doc
       where fiscal_doc_line_id= I_fiscal_doc_line_id
         and action_type = I_action_type
         for update nowait;

BEGIN
   L_exists := NULL;
   ---
   SQL_LIB.SET_MARK('OPEN','C_TAX_EXISTS','FM_CORRECTION_TAX_DOC',L_key);
   open C_TAX_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_TAX_EXISTS','FM_CORRECTION_TAX_DOC',L_key);
   fetch C_TAX_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_TAX_EXISTS','FM_CORRECTION_TAX_DOC',L_key);
   close C_TAX_EXISTS;
   ---
   if L_exists is NOT NULL THEN
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_CORRECTION_TAX_DOC', 'FM_CORRECTION_TAX_DOC', L_key);
      open C_LOCK_CORRECTION_TAX_DOC;
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_CORRECTION_TAX_DOC', 'FM_CORRECTION_TAX_DOC', L_key);
      close C_LOCK_CORRECTION_TAX_DOC;
      ---
      SQL_LIB.SET_MARK('DELETE', NULL, 'FM_CORRECTION_TAX_DOC', L_key);
      delete from FM_CORRECTION_TAX_DOC
       where header_seq_no
          in (select seq_no
                from FM_CORRECTION_DOC
               where fiscal_doc_line_id = I_fiscal_doc_line_id
                 and action_type = I_action_type);
   end if;
   ---
   L_exists := NULL;
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','FM_CORRECTION_DOC',L_key);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','FM_CORRECTION_DOC',L_key);
   fetch C_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','FM_CORRECTION_DOC',L_key);
   close C_EXISTS;
   ---
   if L_exists is NOT NULL THEN
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_CORRECTION_DOC', 'FM_CORRECTION_DOC', L_key);
      open C_LOCK_CORRECTION_DOC;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_CORRECTION_DOC','FM_CORRECTION_DOC',L_key);
      close C_LOCK_CORRECTION_DOC;
      ---
      SQL_LIB.SET_MARK('DELETE', NULL, 'FM_CORRECTION_DOC', L_key);
      delete fm_correction_doc
       where fiscal_doc_line_id= I_fiscal_doc_line_id
         and action_type = I_action_type;
      ---
   end if;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELETE',
                                            'FM_CORRECTION_DOC',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
END DEL_CORRECTION_DOC;
----------------------------------------------------------------------------------
-- Function Name: CREATE_FOR_CORRECTION_DOC
-- Purpose:       This function deltes record from FM_CORRECTION_DOC and FM_CORRECTION_TAX_DOC table
----------------------------------------------------------------------------------
FUNCTION CREATE_FOR_CORRECTION_DOC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_schedule_no        IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   cursor C_NF_HEAD is
      select fiscal_doc_id
        from fm_fiscal_doc_header ffdh
       where ffdh.status = 'R'
         and ffdh.schedule_no = I_schedule_no;
   ---
   L_nf_head             C_NF_HEAD%ROWTYPE;
   ---
   L_program             VARCHAR2(50) := 'FM_CORRECTION_DOC_SQL.CREATE_FOR_CORRECTION_DOC';
   L_key                 VARCHAR2(80) := 'Schedule No = '|| I_schedule_no;
   L_exists              BOOLEAN;
   ---
BEGIN
   ---

   for L_nf_head in C_NF_HEAD loop
      ---
      if FM_CORRECTION_DOC_SQL.CREATE_FOR_CORRECTION_DOC(O_error_message,
                                                         L_exists,
                                                         L_nf_head.fiscal_doc_id,
                                                         I_schedule_no) = FALSE then
         return FALSE;
      end if;
      ---
   end loop;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
      ---
END CREATE_FOR_CORRECTION_DOC;
----------------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_FOR_CORRECTION_DOC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists         IN OUT  BOOLEAN,
                                   I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                   I_schedule_no    IN      FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'FM_CORRECTION_DOC_SQL.CREATE_FOR_CORRECTION_DOC';
   L_dummy    VARCHAR(1)    := NULL;
   L_key      VARCHAR2(100) := 'I_fiscal_doc_id ='||TO_CHAR(I_fiscal_doc_id);
   L_status   FM_FISCAL_DOC_HEADER.STATUS%TYPE;
   ---

   L_exists              BOOLEAN;

   cursor C_V_FM_RECEIVED_QTY is
      select nf_qty,
             appt_qty,
             recv_qty,
             rej_qty,
             fiscal_doc_id,
             fiscal_doc_line_id,
             status
        from v_fm_received_qty_calc
       where fiscal_doc_id = I_fiscal_doc_id
         and status = 'R';
   ---
   cursor C_CHK_FM_CORRECTION_DOC is
      select count(*)
        from fm_correction_doc
       where fiscal_doc_id = I_fiscal_doc_id
         and action_type in('CLQ','RNF','MWN');
   ---
   cursor C_CHK_FREIGHT_INS_OTHS(P_fiscal_doc_id       FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                 P_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE )is
      select nvl(freight_cost,0)         freight_cost,
             nvl(insurance_cost,0)       insurance_cost,
             nvl(other_expenses_cost,0)  other_expenses_cost
        from fm_fiscal_doc_detail
       where (nvl(freight_cost,0) > 0 or nvl(insurance_cost,0) > 0 or nvl(other_expenses_cost,0) > 0)
         and fiscal_doc_id = P_fiscal_doc_id
         and fiscal_doc_line_id = P_fiscal_doc_line_id;
   ---
   L_rec_recv_qty       V_FM_RECEIVED_QTY_CALC%ROWTYPE;
   L_return_nf_qty      FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_unacc_return_qty   FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_correction_qty     FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_qty_discrep        NUMBER;
   L_triangulation        ORDHEAD.TRIANGULATION_IND%TYPE;
   L_compl_fiscal_doc_id  FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   ---
   C_RECEIPT_VER_STATUS  CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'RV';-- Received Verified status
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_V_FM_RECEIVED_QTY',
                    'V_FM_RECEIVED_QTY_CALC',
                    L_key);
   open C_V_FM_RECEIVED_QTY;
   LOOP
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_V_FM_RECEIVED_QTY',
                       'V_FM_RECEIVED_QTY_CALC',
                       L_key);
      fetch C_V_FM_RECEIVED_QTY into L_rec_recv_qty;
      EXIT when C_V_FM_RECEIVED_QTY%NOTFOUND;
      ---
      if ( L_rec_recv_qty.rej_qty > (L_rec_recv_qty.nf_qty - L_rec_recv_qty.recv_qty) ) then
         ---
         L_unacc_return_qty := (L_rec_recv_qty.rej_qty - (L_rec_recv_qty.nf_qty - L_rec_recv_qty.recv_qty));
         ---
         if FM_CORRECTION_DOC_SQL.CREATE_CORRECTION_DOC(O_error_message,
                                                        L_rec_recv_qty,
                                                        'MWN',
                                                        L_unacc_return_qty) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
      if ( L_rec_recv_qty.rej_qty < (L_rec_recv_qty.nf_qty - L_rec_recv_qty.recv_qty) ) then
         ---

         L_correction_qty := (L_rec_recv_qty.nf_qty - L_rec_recv_qty.recv_qty) - L_rec_recv_qty.rej_qty;
         L_correction_qty := (L_rec_recv_qty.nf_qty - L_correction_qty);
         ---
         if FM_CORRECTION_DOC_SQL.CREATE_CORRECTION_DOC(O_error_message,
                                                        L_rec_recv_qty,
                                                        'CLQ',
                                                        L_correction_qty) = FALSE then
            return FALSE;
         end if;
         ---
         for rec_freight_ins_oths in c_chk_freight_ins_oths(L_rec_recv_qty.fiscal_doc_id,L_rec_recv_qty.fiscal_doc_line_id )
         LOOP
            ---
            if (rec_freight_ins_oths.freight_cost > 0) then
               ---
               if FM_CORRECTION_DOC_SQL.CREATE_CORRECTION_DOC(O_error_message,
                                                              L_rec_recv_qty,
                                                              'CLF',
                                                              (L_correction_qty * rec_freight_ins_oths.freight_cost)
                                                              ) = FALSE then
                  return FALSE;
               end if;
               ---
            end if;
            ---
            if (rec_freight_ins_oths.insurance_cost > 0) then
               ---
               if FM_CORRECTION_DOC_SQL.CREATE_CORRECTION_DOC(O_error_message,
                                                              L_rec_recv_qty,
                                                              'CLI',
                                                              (L_correction_qty * rec_freight_ins_oths.insurance_cost)
                                                              ) = FALSE then
                  return FALSE;
               end if;
               ---
            end if;
            ---
            if (rec_freight_ins_oths.other_expenses_cost > 0) then
               ---
               if FM_CORRECTION_DOC_SQL.CREATE_CORRECTION_DOC(O_error_message,
                                                              L_rec_recv_qty,
                                                              'CLO',
                                                              (L_correction_qty * rec_freight_ins_oths.other_expenses_cost)
                                                              ) = FALSE then
                  return FALSE;
               end if;
               ---
            end if;
            ---
         END LOOP;
      end if;
      ---
      L_return_nf_qty :=  (CASE
                              WHEN ( L_rec_recv_qty.rej_qty >= (L_rec_recv_qty.nf_qty - L_rec_recv_qty.recv_qty) ) then
                                   (L_rec_recv_qty.nf_qty - L_rec_recv_qty.recv_qty)
                              WHEN ( L_rec_recv_qty.rej_qty < (L_rec_recv_qty.nf_qty - L_rec_recv_qty.recv_qty) ) then
                                     L_rec_recv_qty.rej_qty
                              ELSE 0
                              END
                          );
      ---
      if L_return_nf_qty > 0 then
         if FM_CORRECTION_DOC_SQL.CREATE_CORRECTION_DOC(O_error_message,
                                                        L_rec_recv_qty,
                                                        'RNF',
                                                        L_return_nf_qty) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   END LOOP;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHK_FM_CORRECTION_DOC', 'FM_CORRECTION_DOC', L_key);
   OPEN C_CHK_FM_CORRECTION_DOC;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_CHK_FM_CORRECTION_DOC', 'FM_CORRECTION_DOC', L_key);
   FETCH C_CHK_FM_CORRECTION_DOC into L_qty_discrep;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_CHK_FM_CORRECTION_DOC', 'FM_CORRECTION_DOC', L_key);
   CLOSE C_CHK_FM_CORRECTION_DOC;



   if (L_qty_discrep = 0) then

      ---
      O_exists := FALSE;
      ---
      if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message ,
                                                I_fiscal_doc_id,
                                                C_RECEIPT_VER_STATUS) = FALSE then
         return FALSE;
      end if;
      ---
      L_status := 'RV';
      ---
      if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message ,
                                                               I_fiscal_doc_id,
                                                               L_triangulation,
                                                               L_compl_fiscal_doc_id
                                                               ) = FALSE then
         return FALSE;
      end if;
      ---
      if (L_triangulation = 'Y') then
         ---
        if FM_FISCAL_DOC_HEADER_SQL .UPDATE_STATUS(
                                                    O_error_message,
                                                    L_compl_fiscal_doc_id,
                                                    'RV'
                                                    ) = FALSE then
            return FALSE;
         end if;
         ---
     end if;
      ---
      if FM_UPD_SCHD_STATUS_SQL.NF_COST_TAX_DISCREP_EXISTS(O_error_message,
                                                           I_fiscal_doc_id,
                                                           L_status) = FALSE then
         return FALSE;
      end if;
      ---
      if FM_UPD_SCHD_STATUS_SQL.UPD_SCHEDULE_STATUS(O_error_message,
                                                    I_schedule_no) = FALSE then
         return FALSE;
      end if;
      ---
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_V_FM_RECEIVED_QTY',
                    'V_FM_RECEIVED_QTY_CALC',
                    L_key);
   close C_V_FM_RECEIVED_QTY;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_V_FM_RECEIVED_QTY%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_V_FM_RECEIVED_QTY',
                          'V_FM_RECEIVED_QTY_CALC',
                          L_key);
         close C_V_FM_RECEIVED_QTY;
      end if;
      ---
      return FALSE;
END CREATE_FOR_CORRECTION_DOC;
----------------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_CORRECTION_DOC(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_v_fm_received_qty  IN      V_FM_RECEIVED_QTY_CALC%ROWTYPE,
                               I_action_type        IN      FM_CORRECTION_DOC.ACTION_TYPE%TYPE,
                               I_action_value       IN      FM_CORRECTION_DOC.ACTION_VALUE%TYPE)
RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(64)  := 'FM_CORRECTION_DOC_SQL.CREATE_CORRECTION_DOC';
   L_key     VARCHAR2(200) := 'fiscal_doc_id: ' || TO_CHAR(i_v_fm_received_qty.fiscal_doc_id)||'fiscal_doc_line_id: ' || TO_CHAR(i_v_fm_received_qty.fiscal_doc_line_id);
   L_dup     NUMBER;
   L_triangulation        ORDHEAD.TRIANGULATION_IND%TYPE;
   L_compl_fiscal_doc_id  FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   L_comp_nf_ind  VARCHAR2(10) := 'N';
   L_fiscal_doc_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE;
   L_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   ---
   cursor C_CHK_DUP is
      select count(*)
        from fm_correction_doc
       where fiscal_doc_line_id = i_v_fm_received_qty.fiscal_doc_line_id
         and action_type        = i_action_type;


cursor C_CHK_DUP_COMPL(I_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select count(*)
        from fm_correction_doc
       where fiscal_doc_line_id = I_fiscal_doc_line_id
         and action_type        = i_action_type;

 cursor C_GET_COMP_NF_IND is
     select fua.comp_nf_ind
     from fm_utilization_attributes fua,
          fm_fiscal_doc_header fdh
     where fdh.utilization_id = fua.utilization_id
     and fdh.fiscal_doc_id = I_v_fm_received_qty.fiscal_doc_id;

 cursor C_FM_TRIANG_OTHER_DOC_LINE (P_comp_nf_ind        FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE, I_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
 I_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
       select fdd.fiscal_doc_id other_doc_id,
                 fdd.fiscal_doc_line_id other_doc_line_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_detail fddc,
             ordhead ord
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.fiscal_doc_id = fddc.fiscal_doc_id_ref
         and fddc.fiscal_doc_line_id = I_fiscal_doc_line_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = 'Y'
         and P_comp_nf_ind           = 'Y'
         and fdc.fiscal_doc_id is NOT NULL
       UNION
      select fdd.fiscal_doc_id other_doc_id,
                 fdd.fiscal_doc_line_id other_doc_line_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.fiscal_doc_id       = I_fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.fiscal_doc_line_id_ref = I_fiscal_doc_line_id
         and fdd.requisition_no      = ord.order_no
         and ord.triangulation_ind   = 'Y'
         and P_comp_nf_ind           = 'N'
         and fdc.fiscal_doc_id is NOT NULL
         UNION select fdd.fiscal_doc_id other_doc_id,
                 fdd.fiscal_doc_line_id other_doc_line_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_detail fddc
       where fdc.compl_fiscal_doc_id = I_fiscal_doc_id
         and fdc.fiscal_doc_id       = fdd.fiscal_doc_id
         and fdd.fiscal_doc_id = fddc.fiscal_doc_id_ref
         and fddc.fiscal_doc_line_id = I_fiscal_doc_line_id
        and fdd.unexpected_item = 'Y'
         and P_comp_nf_ind           = 'Y'
         and fdc.fiscal_doc_id is NOT NULL
       UNION
      select fdd.fiscal_doc_id other_doc_id,
                 fdd.fiscal_doc_line_id other_doc_line_id
        from fm_fiscal_doc_complement fdc,
             fm_fiscal_doc_detail fdd,
             ordhead ord
       where fdc.fiscal_doc_id       = I_fiscal_doc_id
         and fdc.compl_fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.fiscal_doc_line_id_ref = I_fiscal_doc_line_id
         and fdd.unexpected_item = 'Y'
         and P_comp_nf_ind           = 'N'
         and fdc.fiscal_doc_id is NOT NULL;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHK_DUP', 'FM_CORRECTION_DOC', L_key);
   OPEN C_CHK_DUP;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_CHK_DUP', 'FM_CORRECTION_DOC', L_key);
   FETCH C_CHK_DUP into L_dup;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_CHK_DUP', 'FM_CORRECTION_DOC', L_key);
   CLOSE C_CHK_DUP;
   ---
   if ( L_dup = 0) then
      if (i_action_type in ('CLQ','RNF','MWN','CLF','CLI','CLO') ) then


         insert into fm_correction_doc
                    (seq_no,
                     fiscal_doc_id,
                     fiscal_doc_line_id,
                     action_type,
                     action_value,
                     create_datetime,
                     create_id,
                     last_update_datetime,
                     last_update_id
                    )
            values  (
                     fm_correction_doc_seq.nextval,
                     i_v_fm_received_qty.fiscal_doc_id,
                     i_v_fm_received_qty.fiscal_doc_line_id,
                     i_action_type,           --'CLQ','RNF','MWN'
                     i_action_value,
                     sysdate,
                     user,
                     sysdate,
                     user
                    );
      end if;
   end if;

   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message ,
                                                               i_v_fm_received_qty.fiscal_doc_id,
                                                               L_triangulation,
                                                               L_compl_fiscal_doc_id
                                                               ) = FALSE then
         return FALSE;
    end if;



    if (L_triangulation = 'Y' ) then

       L_fetch_comp_taxes := true;

        SQL_LIB.SET_MARK('OPEN', 'C_GET_COMP_NF_IND', 'FM_FISCAL_DOC_HEADER', L_key);
	OPEN C_GET_COMP_NF_IND;
	---
	SQL_LIB.SET_MARK('FETCH', 'C_GET_COMP_NF_IND', 'FM_FISCAL_DOC_HEADER', L_key);
	FETCH C_GET_COMP_NF_IND into L_comp_nf_ind;
	---
	SQL_LIB.SET_MARK('CLOSE', 'C_GET_COMP_NF_IND', 'FM_FISCAL_DOC_HEADER', L_key);
	CLOSE C_GET_COMP_NF_IND;


	SQL_LIB.SET_MARK('OPEN', 'C_FM_TRIANG_OTHER_DOC_LINE', 'FM_FISCAL_DOC_HEADER', L_key);
	OPEN C_FM_TRIANG_OTHER_DOC_LINE(L_comp_nf_ind, i_v_fm_received_qty.fiscal_doc_id,i_v_fm_received_qty.fiscal_doc_line_id);
	---
	SQL_LIB.SET_MARK('FETCH', 'C_FM_TRIANG_OTHER_DOC_LINE', 'FM_FISCAL_DOC_HEADER', L_key);
	FETCH C_FM_TRIANG_OTHER_DOC_LINE into L_fiscal_doc_id,L_fiscal_doc_line_id ;
	---
	SQL_LIB.SET_MARK('CLOSE', 'C_FM_TRIANG_OTHER_DOC_LINE', 'FM_FISCAL_DOC_HEADER', L_key);
	CLOSE C_FM_TRIANG_OTHER_DOC_LINE;



	SQL_LIB.SET_MARK('OPEN', 'C_CHK_DUP_COMPL', 'FM_CORRECTION_DOC', L_key);
	OPEN C_CHK_DUP_COMPL(L_fiscal_doc_line_id);
	---
	SQL_LIB.SET_MARK('FETCH', 'C_CHK_DUP_COMPL', 'FM_CORRECTION_DOC', L_key);
	FETCH C_CHK_DUP_COMPL into L_dup;
	---
	SQL_LIB.SET_MARK('CLOSE', 'C_CHK_DUP_COMPL', 'FM_CORRECTION_DOC', L_key);
	CLOSE C_CHK_DUP_COMPL;
	---
	if ( L_dup = 0) then
	   if (i_action_type in ('CLQ','RNF','MWN','CLF','CLI','CLO') ) then
	   insert into fm_correction_doc
		    (seq_no,
		     fiscal_doc_id,
		     fiscal_doc_line_id,
		     action_type,
		     action_value,
		     create_datetime,
		     create_id,
		     last_update_datetime,
		     last_update_id
		    )
	    values  (
		     fm_correction_doc_seq.nextval,
		     L_fiscal_doc_id,
		     L_fiscal_doc_line_id,
		     i_action_type,           --'CLQ','RNF','MWN'
		     i_action_value,
		     sysdate,
		     user,
		     sysdate,
		     user
		    );
	      end if;
        end if;

    end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_CHK_DUP%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHK_DUP',
                          'FM_CORRECTION_DOC',
                          L_key);
         close C_CHK_DUP;
      end if;
      ---
   return FALSE;
END CREATE_CORRECTION_DOC;

END FM_CORRECTION_DOC_SQL;
/