CREATE OR REPLACE PACKAGE BODY L10N_CODES_SQL AS

----------------------------------------------------------------------
FUNCTION GET_L10N_CODE_DESC( O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                             I_code_type       IN            VARCHAR2,
                             I_code            IN            VARCHAR2,
                             O_code_desc       IN OUT        VARCHAR2)

RETURN BOOLEAN IS

   L_code_type_desc  L10N_CODE_HEAD.CODE_TYPE_DESC%TYPE         := NULL;
   L_primary_lang    LANG.LANG%TYPE                             := NULL;
   L_user_lang       LANG.LANG%TYPE                             := NULL;
   ---
   cursor C_DECODE is
      select l10n_code_desc
        from l10n_code_detail_descs
       where l10n_code_type = I_code_type
         and l10n_code = I_code
         and lang = L_user_lang; 
         
   ---
   cursor C_TYPE_DESC  is
      select code_type_desc
        from l10n_code_head
       where l10n_code_type = I_code_type;
BEGIN

   O_code_desc := NULL;
   
   if I_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_code',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_code_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_code_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   L_user_lang := GET_USER_LANG;
   L_primary_lang := GET_PRIMARY_LANG;
   ---
   if L_user_lang = L_primary_lang then
      sql_lib.set_mark('OPEN','C_DECODE','L10N_CODE_DETAIL_DESCS','L10N CODE TYPE: '||I_code_type||', CODE: '||I_code);
      open C_DECODE;
      sql_lib.set_mark('FETCH','C_DECODE','L10N_CODE_DETAIL_DESCS','L10N CODE TYPE: '||I_code_type||', CODE: '||I_code);
      fetch C_DECODE into O_code_desc;
      sql_lib.set_mark('CLOSE','C_DECODE','L10N_CODE_DETAIL_DESCS','L10N CODE TYPE: '||I_code_type||', CODE: '||I_code);
      close C_DECODE;
   end if;
  
   -- if the code_desc is not found then fetch the code_type_desc to
   -- populate the error message and return FALSE
   if O_code_desc is NULL then 
      sql_lib.set_mark('OPEN','C_type_desc','L10N_CODE_HEAD','L10N CODE TYPE: '||I_code_type);
      open C_TYPE_DESC;
      sql_lib.set_mark('FETCH','C_type_desc','L10N_CODE_HEAD','L10N CODE TYPE: '||I_code_type);
      fetch C_TYPE_DESC into L_code_type_desc;
      sql_lib.set_mark('CLOSE','C_type_desc','L10N_CODE_HEAD','L10N CODE TYPE: '||I_code_type);
      close C_TYPE_DESC;
      O_error_message := sql_lib.create_msg('MISSING_CODE', I_code, L_code_type_desc , I_code_type);
      return FALSE;
   end if;
   ---

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_CODES_SQL.GET_L10N_CODE_DESC',
                                            to_char(SQLCODE));
      return FALSE;

END GET_L10N_CODE_DESC;   
------------------------------------------------------------------------------------------------------
END  L10N_CODES_SQL;  
/