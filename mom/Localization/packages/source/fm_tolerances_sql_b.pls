create or replace PACKAGE BODY FM_TOLERANCES_SQL is
----------------------------------------------------------------------------------------------------
FUNCTION EXIST_SUPPLIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exist	         IN OUT BOOLEAN,
                        I_supplier       IN     VARCHAR2)
   return BOOLEAN is

   L_program    VARCHAR2(64) := 'FM_TOLERANCES_SQL.EXIST_SUPPLIER';
   L_dummy      VARCHAR2(1);

   cursor C_TOLERANCES is
     select '*'
       from sups, v_fiscal_attributes
      where module = 'SUPP'
        and (key_value_1 = supplier
        and I_supplier = supplier);

BEGIN
  open C_TOLERANCES;
  fetch C_TOLERANCES into L_dummy;
  if C_TOLERANCES%NOTFOUND then
     O_exist := FALSE;
  else
     O_exist := TRUE;
  end if;

  close C_TOLERANCES;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             null);
	 RETURN FALSE;
END EXIST_SUPPLIER;
----------------------------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_supplier_desc   IN OUT   SUPS.SUP_NAME%TYPE,
                           I_supplier        IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(64) := 'FM_TOLERANCES_SQL.GET_SUPPLIER_DESC';

   cursor C_GET_SUPPLIER_DESC is
     select sup_name
       from sups
      where supplier = I_supplier;

BEGIN

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'SUPPLIER');
      return FALSE;
   end if;

   open C_GET_SUPPLIER_DESC;
   fetch C_GET_SUPPLIER_DESC into O_supplier_desc;
   ---
   if C_GET_SUPPLIER_DESC%NOTFOUND then

      close C_GET_SUPPLIER_DESC;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUP',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---

   close C_GET_SUPPLIER_DESC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END GET_SUPPLIER_DESC;
----------------------------------------------------------------------------------------------------
FUNCTION CHECK_TOLERANCES (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_lower_overlap     IN OUT  BOOLEAN,
                           O_upper_overlap     IN OUT  BOOLEAN,
                           I_tolerance_id      IN      FM_TOLERANCES.TOLERANCE_ID%TYPE,
                           I_module            IN      FM_TOLERANCES.MODULE%TYPE,
                           I_key_value_1       IN      FM_TOLERANCES.KEY_VALUE_1%TYPE,
                           I_lower_limit       IN      FM_TOLERANCES.LOWER_LIMIT%TYPE,
                           I_upper_limit       IN      FM_TOLERANCES.UPPER_LIMIT%TYPE,
                           I_tolerance_level   IN      FM_TOLERANCES.TOLERANCE_LEVEL%TYPE)
   return BOOLEAN is
   ---
   L_program              VARCHAR2(50) :='FM_TOLERANCES_SQL.CHECK_TOLERANCES';
   L_lower_match          VARCHAR2(1);
   L_upper_match          VARCHAR2(1);
   
   cursor C_CHECK_LOWER_TOLERANCE is
      select 'x'
        from fm_tolerances
       where key_value_1 = I_key_value_1
         and ((I_tolerance_id is NOT NULL
         and tolerance_id != I_tolerance_id)
          or (I_tolerance_id is NULL))
         and tolerance_level = I_tolerance_level
         and (I_lower_limit between lower_limit and (upper_limit)-1);
      
   cursor C_CHECK_UPPER_TOLERANCE is
      select 'x'
        from fm_tolerances
       where key_value_1 = I_key_value_1
         and ((I_tolerance_id is NOT NULL
         and tolerance_id != I_tolerance_id)
          or (I_tolerance_id is NULL))
         and tolerance_level = I_tolerance_level
         and (I_upper_limit between (lower_limit)+1 and upper_limit);

BEGIN

   if I_module is NOT NULL
    and I_key_value_1 is NOT NULL then
      -- search the lower tolerance
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_LOWER_TOLERANCE',
                       'FM_TOLERANCES',
                       NULL);
      open C_CHECK_LOWER_TOLERANCE;
    
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_LOWER_TOLERANCE',
                       'FM_TOLERANCES',
                       NULL);
      fetch C_CHECK_LOWER_TOLERANCE into L_lower_match;
      ---
      if C_CHECK_LOWER_TOLERANCE%FOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_LOWER_TOLERANCE',
                          'FM_TOLERANCES',
                          NULL);
         close C_CHECK_LOWER_TOLERANCE;
         O_lower_overlap := TRUE;
         return TRUE;
      end if;
    
      -- closes the lower cursor
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_LOWER_TOLERANCE',
                       'FM_TOLERANCES',
                       NULL);
    
      close C_CHECK_LOWER_TOLERANCE;
    
      -- search the upper tolerance
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_UPPER_TOLERANCE',
                       'FM_TOLERANCES',
                       NULL);
      open C_CHECK_UPPER_TOLERANCE;
    
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_UPPER_TOLERANCE',
                       'FM_TOLERANCES',
                       NULL);
      fetch C_CHECK_UPPER_TOLERANCE into L_upper_match;
      ---
      if C_CHECK_UPPER_TOLERANCE%FOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_UPPER_TOLERANCE',
                          'FM_TOLERANCES',
                          NULL);
         close C_CHECK_UPPER_TOLERANCE;
         O_upper_overlap := TRUE;
         return TRUE;
      end if;
    
      -- closes the upper cursor
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_UPPER_TOLERANCE',
                       'FM_TOLERANCES',
                       NULL);
    
      close C_CHECK_UPPER_TOLERANCE;
   else
   ---
   O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                         'NULL',
                                         'NULL',
                                         'NOT NULL');
   ---
   end if;
   ---
   O_lower_overlap := FALSE;
   O_upper_overlap := FALSE;
   ---
   return TRUE;
   ---
EXCEPTION
      WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_TOLERANCES;
----------------------------------------------------------------------------------------------------
FUNCTION EXIST_TOLERANCES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exist	         IN OUT BOOLEAN,
                          I_module         IN     FM_TOLERANCES.MODULE%TYPE,
                          I_key_value_1    IN     FM_TOLERANCES.KEY_VALUE_1%TYPE)
   return BOOLEAN is

   L_program    VARCHAR2(64) := 'FM_TOLERANCES_SQL.EXIST_TOLERANCES';
   L_dummy      VARCHAR2(1);

   cursor C_SEARCH_TOLERANCES is
      select '*'
        from fm_tolerances
       where module = I_module
         and key_value_1 = I_key_value_1;

BEGIN
  open C_SEARCH_TOLERANCES;
  fetch C_SEARCH_TOLERANCES into L_dummy;
  if C_SEARCH_TOLERANCES%NOTFOUND then
     O_exist := FALSE;
  else
     O_exist := TRUE;
  end if;

  close C_SEARCH_TOLERANCES;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             null);
   RETURN FALSE;
END EXIST_TOLERANCES;
----------------------------------------------------------------------------------------------------
FUNCTION EXIST_PV (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists           IN OUT BOOLEAN,
                   I_pv               IN     FM_TOLERANCES.TOLERANCE_TYPE%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_TOLERANCES_SQL.EXIST_PV';
   L_code      CODE_DETAIL.CODE%TYPE;

   cursor C_PV is
      select code 
        from code_detail 
       where code_type = 'FMVP'
         and code = I_pv;
BEGIN
   open C_PV;
   fetch C_PV into L_code;
   if C_PV%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_PV;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXIST_PV;
----------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TOLERANCE_ID(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_tolerance_id   IN OUT FM_TOLERANCES.TOLERANCE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_TOLERANCES_SQL.GET_NEXT_TOLERANCE_ID';
   ---
   cursor C_NEXT_TOLERANCE_ID is
      select fm_tolerance_id_seq.NEXTVAL seq_no
        from dual;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_NEXT_TOLERANCE_ID', 'FM_TOLERANCE_ID_SEQ', NULL);
   open C_NEXT_TOLERANCE_ID;
   SQL_LIB.SET_MARK('FETCH', 'C_NEXT_TOLERANCE_ID', 'FM_TOLERANCE_ID_SEQ',NULL);
   fetch C_NEXT_TOLERANCE_ID into O_tolerance_id;
   SQL_LIB.SET_MARK('CLOSE', 'C_NEXT_TOLERANCE_ID', 'FM_TOLERANCE_ID_SEQ',NULL);
   close C_NEXT_TOLERANCE_ID;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_NEXT_TOLERANCE_ID%ISOPEN then
          SQL_LIB.SET_MARK('CLOSE', 'C_NEXT_TOLERANCE_ID', 'FM_TOLERANCE_ID_SEQ',NULL);
         close C_NEXT_TOLERANCE_ID;
      end if;
      ---
      return FALSE;

END GET_NEXT_TOLERANCE_ID;
----------------------------------------------------------------------------------------------------
FUNCTION LOV_COMPANY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_TOLERANCES_SQL.LOV_COMPANY';
   ---
BEGIN
   ---
   O_query := 'select co_name, company '||
                'from comphead '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||                 
              'union all '||
              'select NVL(tl_shadow.translated_value, co_name) co_name, company '||
                'from comphead, tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
               'and UPPER(company) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_COMPANY;
----------------------------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   L_program   VARCHAR2(50) := 'ORFM_TOLERANCES_SQL.LOV_SUPPLIER';
BEGIN
   ---
   O_query := 'select sup_name, supplier '||
                'from sups, v_fiscal_attributes '||
               'where module = ''SUPP'' '||
                 'and key_value_1 = supplier '||
                 'and LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
               'union all '||
              'select NVL(tl_shadow.translated_value, sup_name) sup_name, supplier '||
                'from sups, v_fiscal_attributes , tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and module = ''SUPP'' '||
                 'and key_value_1 = supplier '||
                 'and UPPER(sup_name) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_SUPPLIER;
----------------------------------------------------------------------------------------------------
FUNCTION LOV_PERC_VALUE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_query          IN OUT VARCHAR2) 
   return BOOLEAN is
   L_program   VARCHAR2(50) := 'ORFM_TOLERANCES_SQL.LOV_PERC_VALUE';
BEGIN
   ---
   O_query := 'select code, code_desc Name from code_detail where code_type = ''FMVP'' order by code_seq';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_PERC_VALUE;
----------------------------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where_clause     IN OUT VARCHAR2,
                          I_module           IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                          I_key_value_1      IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                          I_key_value_2      IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN is
   ---
   L_program VARCHAR2(100) := 'ORFM_TOLERANCES_SQL.SET_WHERE_CLAUSE';
   ---
BEGIN
   ---
   O_where_clause := 'module = '''||I_module||'''';
   ----------------------------------------------------------------------------
   -- I_MODULE = 'SUPP'
   ----------------------------------------------------------------------------
   if I_module = 'SUPP' then
      O_where_clause := O_where_clause||' and key_value_1 = '''||I_key_value_1||'''';
   ----------------------------------------------------------------------------
   -- I_MODULE = 'COMP'
   ----------------------------------------------------------------------------
   elsif I_module = 'COMP' then
        O_where_clause := O_where_clause||' and key_value_1 = '''||I_key_value_1||'''';
   end if; 
   ----------------------------------------------------------------------------
 
   return TRUE;
   ---
EXCEPTION
  when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
     return FALSE;
END SET_WHERE_CLAUSE;
----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FISCAL_ENTITY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_description    IN OUT VARCHAR2,
                                I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                                I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                                I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_TOLERANCES_SQL.VALIDATE_FISCAL_ENTITY';
   L_dummy     VARCHAR2(1);
   L_valid     BOOLEAN;
   ---
   cursor C_ENTITY_ATTRIB is
      select 'X'
        from v_fiscal_attributes a
       where a.module = I_module
         and a.key_value_1 = I_key_value_1
         and ((I_key_value_2 is NOT NULL
         and  a.key_value_2 = I_key_value_2)
          or I_key_value_2 is NULL);
   ---
BEGIN
   ---
   if I_module is NOT NULL and
      I_key_value_1 is NOT NULL then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2);
      open C_ENTITY_ATTRIB;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2);
      fetch C_ENTITY_ATTRIB into L_dummy;
      if I_module = 'SUPP' then
        if C_ENTITY_ATTRIB%NOTFOUND then
           ---
           SQL_LIB.SET_MARK('CLOSE',
                            'C_ENTITY_ATTRIB',
                            'V_FISCAL_ATTRIBUTES',
                            'Module = '      || I_module ||
                            ' Key_value_1 = '|| I_key_value_1 ||
                            ' Key_value_2 = '|| I_key_value_2);
           close C_ENTITY_ATTRIB;
           ---
           
           
           O_error_message := SQL_LIB.CREATE_MSG('INV_SUP',NULL,NULL,NULL);
           --end if;
           ---
           return FALSE;
           ---
          end if;
        end if;  
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ENTITY_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       'Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2);
      close C_ENTITY_ATTRIB;
      ---
      if I_module = 'COMP' then
         ---
         if ORGANIZATION_ATTRIB_SQL.GET_NAME(O_error_message,
                                             1,
                                             I_key_value_1,
                                             O_description) = FALSE then
            return FALSE;
         end if;
         ---
      elsif I_module = 'SUPP' then
         ---
         if SUPP_ATTRIB_SQL.GET_SUPP_DESC(O_error_message,
                                          I_key_value_1,
                                          O_description) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   else
      O_description := NULL;
   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_FISCAL_ENTITY;
----------------------------------------------------------------------------------------------------
END FM_TOLERANCES_SQL;
/