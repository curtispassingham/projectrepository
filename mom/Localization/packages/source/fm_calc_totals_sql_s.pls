CREATE OR REPLACE PACKAGE FM_CALC_TOTALS_SQL is
---------------------------------------------------------------------------------
  C_SUCCESS                   CONSTANT INTEGER := 1;
  C_ERROR_STATUS              CONSTANT INTEGER := 7;
---------------------------------------------------------------------------------
FUNCTION POPULATE_TAX_HEAD(O_error_message  IN OUT VARCHAR2,
                           O_status         IN OUT INTEGER,
                           I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
 return BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION POPULATE_VALUES(O_error_message  IN OUT VARCHAR2,
                         O_status         IN OUT INTEGER,
                         I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_form_ind       IN     VARCHAR2)
 return BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION UPDATE_TOTALS(O_error_message    IN OUT VARCHAR2,
                       O_status         IN OUT INTEGER,
                       I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
 return BOOLEAN;
 -------------------------------------------------------------------------------------
END FM_CALC_TOTALS_SQL;
/