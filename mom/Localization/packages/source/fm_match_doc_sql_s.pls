CREATE OR REPLACE PACKAGE FM_MATCH_DOC_SQL is
---------------------------------------------------------------------------------
-- Function Name: LOV_PO
-- Purpose:       This function returns the query for record_grup REC_ORDER_NO.
---------------------------------------------------------------------------------
FUNCTION LOV_PO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                O_query          IN OUT VARCHAR2,
                I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                I_supplier       IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXPLODE_ORDER_DETAIL
-- Purpose:       This function populate the table FM_FISCAL_DOC_DETAIL behind the
--                PO numbers.
---------------------------------------------------------------------------------
FUNCTION EXPLODE_ORDER_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_pre_load       IN OUT BOOLEAN,
                              I_fiscal_doc_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                              I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXISTS_PO
-- Purpose:       This function checks if PO is valid.
---------------------------------------------------------------------------------
FUNCTION EXISTS_PO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists         IN OUT BOOLEAN,
                   I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                   I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                   I_supplier       IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                   I_location_type  IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                   I_location_id    IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for where clause of block.
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where            IN OUT VARCHAR2,
                          I_supplier         IN     V_FM_REQUISITION_DOCS.SUPPLIER%TYPE,
                          I_location_type    IN     V_ORDLOC.LOC_TYPE%TYPE,
                          I_location_id      IN     V_ORDLOC.LOCATION%TYPE,
                          I_fiscal_doc_id    IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                          I_doc_no           IN     V_FM_REQUISITION_DOCS.DOC_NO%TYPE,
                          I_requisition_type IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_PARENT_SUPS
-- Purpose:       This function returns the Parent supplier and supplier name.
---------------------------------------------------------------------------------
FUNCTION GET_PARENT_SUPS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_sup_name       OUT    SUPS.SUP_NAME%TYPE,
                         O_supplier       OUT    SUPS.SUPPLIER_PARENT%TYPE,
                         I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE
                         )
return BOOLEAN;
---------------------------------------------------------------------------------
END FM_MATCH_DOC_SQL;
/