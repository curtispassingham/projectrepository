CREATE OR REPLACE PACKAGE BODY FM_SECONDARY_ASNOUT AS

   LP_error_status   VARCHAR2(1)   := NULL;

------------------------------------------------------------------------------------------------
                     /* Private Program Declarations */
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD (O_error_message IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY RIB_OBJECT,
                               O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                               I_recv_no       IN            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                               I_seq_no        IN            FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE,
                               I_po_number     IN            FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION BUILD_HEADER_OBJECT (O_error_message          IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_rib_asnoutdesc_rec     IN OUT        "RIB_ASNOutDesc_REC",
                              O_routing_info           IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              I_recv_no                IN            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                              I_po_number              IN            FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_rib_asnoutdistro_tbl   IN OUT   "RIB_ASNOutDistro_TBL",
                               O_asn_number             IN OUT   FM_RECEIVING_DETAIL.ASN_NBR%TYPE,
                               I_recv_no                IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                               I_po_number              IN       FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_message   IN  OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                        O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_recv_no         IN           FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                        I_seq_no          IN           FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE);
------------------------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_queue_locked    IN OUT   BOOLEAN,
                         I_seq_no          IN       FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION DELETE_QUEUE_REC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_recv_no         IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                           I_po_number       IN       FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE,
                           I_seq_no          IN       FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
                      /*** Public Program Bodies ***/
------------------------------------------------------------------------------------------------
FUNCTION ADDTOQ (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_recv_no         IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                 I_po_number       IN       FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE,
                 I_msg_type        IN       VARCHAR2)
RETURN BOOLEAN IS

   L_family                        VARCHAR2(25) := 'asnout';
   L_thread                        NUMBER       := 1;
   L_pub_status                    FM_RIB_RECEIVING_MFQUEUE.PUB_STATUS%TYPE := 'U';
   L_max_details_to_publish        RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := NULL;
   L_minutes_time_lag              RIB_SETTINGS.MINUTES_TIME_LAG%TYPE       := NULL;
   L_num_threads                   RIB_SETTINGS.NUM_THREADS%TYPE            := NULL;
   L_program                       VARCHAR2(64) := 'FM_SECONDARY_ASNOUT.ADDTOQ';
   L_status_code                   VARCHAR2(1)  := NULL;
   L_cnt_unexpected_item           NUMBER;

   CURSOR C_GET_UNEXPC_IND IS
      select count(*)
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.requisition_no IS NULL
         and fdd.unexpected_item = 'N'
         and fdh.schedule_no = I_recv_no;

BEGIN
   open C_GET_UNEXPC_IND;
   fetch C_GET_UNEXPC_IND into L_cnt_unexpected_item;
   close C_GET_UNEXPC_IND;

   if I_recv_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_recv_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_po_number is NULL and L_cnt_unexpected_item > 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_po_number',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_msg_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_msg_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details_to_publish,
                                L_num_threads,
                                L_minutes_time_lag,
                                RMSMFM_SHIPMENT.FAMILY);

   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   insert into FM_RIB_RECEIVING_MFQUEUE (SEQ_NO,
                                         MESSAGE_TYPE,
                                         PUB_STATUS,
                                         FAMILY,
                                         THREAD_NO,
                                         RECV_NO,
                                         PO_NUMBER)
                                 values (FM_RECEIVING_MFQUEUE_SEQ.nextval,
                                         I_msg_type,
                                         L_pub_status,
                                         L_family,
                                         L_thread,
                                         I_recv_no,
                                         I_po_number);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SECONDARY_ASNOUT.ADDTOQ',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ADDTOQ;
-------------------------------------------------------------------------------
PROCEDURE GETNXT (O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_message_type    IN OUT   VARCHAR2,
                  O_message         IN OUT   RIB_OBJECT,
                  O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                  O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                  I_num_threads     IN       NUMBER DEFAULT 1,
                  I_thread_val      IN       NUMBER DEFAULT 1)
IS

   L_recv_no            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE := NULL;
   L_po_number          FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE := NULL;
   L_seq_no             FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE;
   L_message_type       VARCHAR2(15);
   L_queue_locked       BOOLEAN := FALSE;

   PROGRAM_ERROR        EXCEPTION;

   cursor C_QUEUE is
      select seq_no,
             recv_no,
             po_number,
             message_type
        from fm_rib_receiving_mfqueue
       where seq_no = (select min(seq_no)
                         from fm_rib_receiving_mfqueue
                        where pub_status = 'U'
                          and family = FM_SECONDARY_ASNOUT.L_FAMILY);

BEGIN

   LP_error_status := API_CODES.HOSPITAL;

   O_message      := NULL;
   O_bus_obj_id   := NULL;
   O_routing_info := NULL;

   open C_QUEUE;
   fetch C_QUEUE into L_seq_no,
                      L_recv_no,
                      L_po_number,
                      L_message_type;

   if C_QUEUE%NOTFOUND then
      close C_QUEUE;
      O_status_code := API_CODES.NO_MSG;

      return;
   end if;
   close C_QUEUE;

   if LOCK_THE_BLOCK(O_error_message,
                     L_queue_locked,
                     L_seq_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if PROCESS_QUEUE_RECORD (O_error_message,
                            O_message,
                            O_routing_info,
                            O_bus_obj_id,
                            L_recv_no,
                            L_seq_no,
                            L_po_number) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_recv_no);
      O_message_type := L_message_type;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_recv_no,
                    L_seq_no);
END GETNXT;
-------------------------------------------------------------------------------
PROCEDURE PUB_RETRY (O_status_code     IN OUT   VARCHAR2,
                     O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_message_type    IN OUT   VARCHAR2,
                     O_message         IN OUT   RIB_OBJECT,
                     O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                     O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                     I_ref_object      IN       RIB_OBJECT)
IS

   L_queue_locked       BOOLEAN := FALSE;
   L_seq_no             FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE;
   L_recv_no            FM_RIB_STG_RECEIVING_DETAIL.RECV_NO%TYPE := NULL;
   L_po_number          FM_RIB_STG_RECEIVING_DETAIL.PO_NUMBER%TYPE := NULL;
   L_message_type       VARCHAR2(15);

   PROGRAM_ERROR      EXCEPTION;

   cursor C_RETRY_QUEUE is
      select seq_no,
             recv_no,
             po_number,
             message_type
        from fm_rib_receiving_mfqueue
       where seq_no = (select min(seq_no)
                         from fm_rib_receiving_mfqueue
                        where pub_status = API_CODES.HOSPITAL
                          and family = FM_SECONDARY_ASNOUT.L_FAMILY);

BEGIN
   LP_error_status := API_CODES.HOSPITAL;

   O_message      := NULL;
   O_bus_obj_id   := NULL;
   O_routing_info := NULL;

   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_seq_no,
                            L_recv_no,
                            L_po_number,
                            L_message_type;
   if C_RETRY_QUEUE%NOTFOUND then
      close C_RETRY_QUEUE;
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   close C_RETRY_QUEUE;

   if L_seq_no is NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   if LOCK_THE_BLOCK(O_error_message,
                     L_queue_locked,
                     L_seq_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then
      if PROCESS_QUEUE_RECORD(O_error_message,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              L_recv_no,
                              L_seq_no,
                              L_po_number) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message is NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         O_status_code := API_CODES.NEW_MSG;
         O_message_type := L_message_type;
      end if;
      O_bus_obj_id  := RIB_BUSOBJID_TBL(L_recv_no);
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_recv_no,
                    L_seq_no);

END PUB_RETRY;
-------------------------------------------------------------------------------
                      /*** Private Program Bodies ***/
-------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD (O_error_message IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY RIB_OBJECT,
                               O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                               I_recv_no       IN            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                               I_seq_no        IN            FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE,
                               I_po_number     IN            FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE)
RETURN BOOLEAN IS

   L_status_code           VARCHAR2(1) := NULL;
   L_status                VARCHAR2(1) := NULL;
   L_max_details           RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 12;
   L_num_threads           RIB_SETTINGS.NUM_THREADS%TYPE            := NULL;
   L_min_time_lag          RIB_SETTINGS.MINUTES_TIME_LAG%TYPE       := NULL;
   L_rib_asnout_desc_rec   "RIB_ASNOutDesc_REC" := NULL;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                FM_SECONDARY_ASNOUT.L_FAMILY);

   if L_status_code = API_CODES.UNHANDLED_ERROR then
      return FALSE;
   end if;

   if BUILD_HEADER_OBJECT(O_error_message,
                          L_rib_asnout_desc_rec,
                          O_routing_info,
                          I_recv_no,
                          I_po_number) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if DELETE_QUEUE_REC(O_error_message,
                       I_recv_no,
                       I_po_number,
                       I_seq_no) = FALSE then

      return FALSE;
   end if;

   O_message := L_rib_asnout_desc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SECONDARY_ASNOUT.PROCESS_QUEUE_RECORD',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_RECORD;
-------------------------------------------------------------------------------
FUNCTION BUILD_HEADER_OBJECT (O_error_message          IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_rib_asnoutdesc_rec     IN OUT        "RIB_ASNOutDesc_REC",
                              O_routing_info           IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              I_recv_no                IN            FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                              I_po_number              IN            FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE)
RETURN BOOLEAN IS

   L_rib_asnoutdesc_rec     "RIB_ASNOutDesc_REC" := NULL;
   L_rib_routinginfo_rec    RIB_ROUTINGINFO_REC := NULL;
   L_rib_asnoutdistro_tbl   "RIB_ASNOutDistro_TBL" := NULL;
   L_store_name             STORE.STORE_NAME%TYPE;
   L_fax_number             STORE.FAX_NUMBER%TYPE;
   L_phone_number           STORE.PHONE_NUMBER%TYPE;
   L_add_1                  ADDR.ADD_1%TYPE;
   L_add_2                  ADDR.ADD_2%TYPE;
   L_city                   ADDR.CITY%TYPE;
   L_state                  ADDR.STATE%TYPE;
   L_country_id             ADDR.COUNTRY_ID%TYPE;
   L_pcode                  ADDR.POST%TYPE;
   L_wh_rec                 WH_ATTRIB_SQL.WH_RECTYPE;
   L_recv_no                FM_RIB_STG_RECEIVING_DETAIL.RECV_NO%TYPE := NULL;
   L_location_id            FM_RIB_STG_RECEIVING_HEADER.LOCATION_ID%TYPE :=NULL;
   L_asn_no                 FM_RECEIVING_DETAIL.ASN_NBR%TYPE := NULL;
   L_from_loc_type          FM_RIB_STG_RECEIVING_HEADER.LOCATION_TYPE%TYPE;
   L_from_loc               FM_RIB_STG_RECEIVING_HEADER.LOCATION_ID%TYPE;
   L_to_loc                 FM_RIB_STG_RECEIVING_HEADER.LOCATION_ID%TYPE;
   L_to_loc_type            FM_RIB_STG_RECEIVING_HEADER.LOCATION_TYPE%TYPE;
   L_requisition_type       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_key_value_1            FM_RIB_STG_RECEIVING_HEADER.LOCATION_ID%TYPE;
   L_key_value_2            FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE;
   L_auto_receive           VARCHAR2(1);
   L_ship_date              SHIPMENT.SHIP_DATE%TYPE      := NULL;
   L_bol_no                 SHIPMENT.EXT_REF_NO_OUT%TYPE := NULL;
   L_est_arr_date           SHIPMENT.EST_ARR_DATE%TYPE   := NULL;
   L_no_boxes               SHIPMENT.NO_BOXES%TYPE       := NULL;
   L_asn_type               VARCHAR2(1);
   L_requisition_no         FM_RIB_STG_RECEIVING_DETAIL.PO_NUMBER%TYPE := NULL;
   L_count                  NUMBER := 0;

   cursor C_GET_REQUISITION_TYPE is
      select distinct fh.requisition_type,
             fsr.location_id,
             to_number(fh.key_value_1) key_value_1,
             fh.key_value_2
        from fm_fiscal_doc_header fh,
             fm_rib_stg_receiving_header fsr
       where fh.schedule_no = fsr.recv_no
         and fsr.recv_no    = I_recv_no;

   cursor C_GET_LOC_DETAIL is
      select DISTINCT sm.from_loc,
                      sm.from_loc_type,
                      sm.to_loc,
                      sm.to_loc_type,
                      sm.ship_date,
                      sm.ext_ref_no_out,
                      sm.no_boxes,
                      sm.est_arr_date
        from shipsku sk,
             fm_rib_stg_receiving_detail fsr,
             shipment sm,
             fm_fiscal_doc_header fdh
       where sk.distro_no    = fsr.po_number
         and fdh.ref_no_1    = sm.bol_no
         and fdh.schedule_no = fsr.recv_no
         and sk.shipment     = sm.shipment
         and fsr.recv_no     = I_recv_no
         and fsr.po_number   = NVL(I_po_number,fsr.po_number)
   union all
      select DISTINCT TO_NUMBER(ds.from_location),
                      loc.loc_type from_loc_type,           
                      TO_NUMBER(fdh.location_id),
                      fdh.location_type to_loc_type,
                      ds.shipment_date,
                      ds.bol_nbr,
                      ds.container_qty,
                      ds.est_arr_date
        from fm_stg_asnout_desc ds,
             fm_rib_stg_receiving_detail fsr,
             fm_stg_asnout_distro dt,
             fm_fiscal_doc_header fdh,
             (select store location,
                     'S' loc_type
                from store
               union all
              select wh location,
                     'W' loc_type
                from wh
               union all
              select TO_NUMBER (partner_id) location,
                     'E' loc_type
                from partner
               where partner_type = 'E') loc
       where ds.seq_no       = dt.desc_seq_no
         and dt.distro_nbr   = fsr.po_number
         and fdh.ref_no_1    = ds.asn_nbr
         and fdh.schedule_no = fsr.recv_no
         and fdh.location_id = ds.to_location
         and loc.location    = ds.from_location
         and fsr.recv_no     = I_recv_no
         and fsr.po_number   = NVL(I_po_number,fsr.po_number);

   cursor C_GET_REP_INFO is
      select th.from_loc, th.from_loc_type, th.to_loc, th.to_loc_type, frd.asn_nbr
        from fm_receiving_header frh,
             fm_receiving_detail frd,
             tsfhead th
       where frh.seq_no = frd.header_seq_no
         and th.tsf_no = frd.requisition_no
         and frh.recv_no = I_recv_no
         and frd.requisition_no = L_requisition_no;

   cursor C_GET_REQUISITION_NO is
    select DISTINCT fsd.po_number
      from fm_rib_stg_receiving_detail fsd
     where fsd.recv_no = I_recv_no
       and fsd.po_number = NVL(I_po_number,fsd.po_number);

   cursor C_GET_BOL_NO is
     select DISTINCT frd.asn_nbr,
                     fdh.entry_or_exit_date,
                     fdh.location_id,
                     fdh.location_type
       from fm_receiving_detail frd,
            fm_receiving_header frh,
            fm_fiscal_doc_header fdh
      where frh.seq_no = frd.header_seq_no
        and fdh.fiscal_doc_id = frh.fiscal_doc_id
        and fdh.schedule_no = frh.recv_no
        and frh.recv_no = I_recv_no
        and frd.requisition_no = L_requisition_no;


   cursor C_LEG_COUNT(P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select count(1)
        from tsfhead t
       where (t.tsf_parent_no = P_requisition_no and from_loc_type != 'W'
           or (tsf_no = P_requisition_no
          and tsf_parent_no is not null and from_loc_type != 'W'))
          and NVL(context_type,'X') != 'REPAIR';


BEGIN

   O_rib_asnoutdesc_rec := NULL;
   O_routing_info       := NULL;

   L_rib_asnoutdesc_rec := "RIB_ASNOutDesc_REC"(NULL,                         -- RIB_OID
                                                NULL,                         -- SCHEDULE NBR
                                                NULL,                         -- AUTORECEIVE
                                                NULL,                         -- TO_LOCATION
                                                NULL,                         -- FROM_LOCATION
                                                NULL,                         -- ASN_NBR
                                                NULL,                         -- ASN_TYPE
                                                NULL,                         -- CONTAINER_QTY
                                                NULL,                         -- BOL_NBR
                                                NULL,                         -- SHIPMENT_DATE
                                                NULL,                         -- EST_ARR_DATE
                                                NULL,                         -- SHIP_ADDRESS1
                                                NULL,                         -- SHIP_ADDRESS2
                                                NULL,                         -- SHIP_ADDRESS3
                                                NULL,                         -- SHIP_ADDRESS4
                                                NULL,                         -- SHIP_ADDRESS5
                                                NULL,                         -- SHIP_CITY
                                                NULL,                         -- SHIP_STATE
                                                NULL,                         -- SHIP_ZIP
                                                NULL,                         -- SHIP_COUNTRY_ID
                                                NULL,                         -- TRAILER_NBR
                                                NULL,                         -- SEAL_NBR
                                                NULL,                         -- CARRIER_CODE
                                                NULL,                         -- TRANSSHIPMENT_NBR
                                                NULL                          -- ASNOUTDISTRO_TBL
                                                );
   L_rib_asnoutdistro_tbl := "RIB_ASNOutDistro_TBL"();
   open  C_GET_REQUISITION_TYPE;
   fetch C_GET_REQUISITION_TYPE into L_requisition_type,
                                     L_location_id,
                                     L_key_value_1,
                                     L_key_value_2;
   close C_GET_REQUISITION_TYPE;

   open C_GET_REQUISITION_NO;
   fetch C_GET_REQUISITION_NO into L_requisition_no;
   close C_GET_REQUISITION_NO;

   if L_requisition_type in ('TSF','IC') then
      open C_LEG_COUNT(L_requisition_no);
      fetch C_LEG_COUNT into L_count;
      close C_LEG_COUNT;
   end if;

   if L_requisition_type = 'PO' then

      L_from_loc      := L_key_value_1;
      L_from_loc_type := L_key_value_2;
      L_add_1         := NULL;
      L_add_2         := NULL;
      L_city          := NULL;
      L_state         := NULL;
      L_country_id    := NULL;
      L_no_boxes      := 1;
      L_auto_receive  := 'Y';
      L_est_arr_date  := GET_VDATE;

      open C_GET_BOL_NO;
      fetch C_GET_BOL_NO into L_bol_no, L_ship_date, L_to_loc, L_to_loc_type;
      close C_GET_BOL_NO;
   else
      open  C_GET_LOC_DETAIL;
      fetch C_GET_LOC_DETAIL into L_from_loc,
                                  L_from_loc_type,
                                  L_to_loc,
                                  L_to_loc_type,
                                  L_ship_date,
                                  L_bol_no,
                                  L_no_boxes,
                                  L_est_arr_date;
      close C_GET_LOC_DETAIL;

      if (L_requisition_type = 'REP' or (L_requisition_type in ('TSF','IC') and L_count > 0 )) then
         open C_GET_REP_INFO;
         fetch C_GET_REP_INFO into L_from_loc, L_from_loc_type, L_to_loc, L_to_loc_type, L_bol_no;
         close C_GET_REP_INFO;

         open C_GET_BOL_NO;
         fetch C_GET_BOL_NO into L_bol_no, L_ship_date, L_to_loc, L_to_loc_type;
         close C_GET_BOL_NO;
         L_no_boxes     := 1;
         L_est_arr_date := GET_VDATE;
      end if;

      if L_to_loc_type = 'S' then
         ---
         if STORE_ATTRIB_SQL.GET_INFO(O_error_message,
                                      L_store_name,
                                      L_add_1,
                                      L_add_2,
                                      L_city,
                                      L_state,
                                      L_country_id,
                                      L_pcode,
                                      L_fax_number,
                                      L_phone_number,
                                      L_to_loc) = FALSE then
            return FALSE;
         end if;
         ---
      else

         if WH_ATTRIB_SQL.GET_WH_INFO(O_error_message,
                                      L_wh_rec,
                                      L_to_loc) = FALSE then
            return FALSE;
         end if;

         L_add_1       := L_wh_rec.wh_add1;
         L_add_2       := L_wh_rec.wh_add2;
         L_city        := L_wh_rec.wh_city;
         L_state       := L_wh_rec.state;
         L_country_id  := L_wh_rec.country_id;

      end if;
      --if the location is store then autoreceipt flag set to 'Y'
      if L_to_loc_type = 'W' then
         L_auto_receive := NULL;
      else
         L_auto_receive := 'Y';
      end if;
   end if;
   if L_to_loc_type = 'S' or L_from_loc_type = 'S' then
      L_asn_type := 'C';
   else
      L_asn_type := 'T';
   end if;


   if BUILD_DETAIL_OBJECTS(O_error_message,
                           L_rib_asnoutdistro_tbl,
                           L_asn_no,
                           I_recv_no,
                           I_po_number) = FALSE then
      return FALSE;
   end if;

   L_rib_asnoutdesc_rec := "RIB_ASNOutDesc_REC"(0,                            -- RIB_OID
                                                I_recv_no,                    -- SCHEDULE NBR
                                                L_auto_receive,               -- AUTORECEIVE
                                                L_location_id,                -- TO_LOCATION
                                                L_from_loc,                   -- FROM_LOCATION
                                                L_asn_no,                     -- ASN_NBR
                                                L_asn_type,                   -- ASN_TYPE
                                                L_no_boxes,                   -- CONTAINER_QTY
                                                L_bol_no,                     -- BOL_NBR
                                                L_ship_date,                  -- SHIPMENT_DATE
                                                L_est_arr_date,               -- EST_ARR_DATE
                                                L_add_1,                      -- SHIP_ADDRESS1
                                                L_add_2,                      -- SHIP_ADDRESS2
                                                NULL,                         -- SHIP_ADDRESS3
                                                NULL,                         -- SHIP_ADDRESS4
                                                NULL,                         -- SHIP_ADDRESS5
                                                L_city,                       -- SHIP_CITY
                                                L_state,                      -- SHIP_STATE
                                                NULL,                         -- SHIP_ZIP
                                                L_country_id,                 -- SHIP_COUNTRY_ID
                                                1,                            -- TRAILER_NBR
                                                NULL,                         -- SEAL_NBR
                                                'DC',                          -- CARRIER_CODE
                                                NULL,                         -- TRANSSHIPMENT_NBR
                                                L_rib_asnoutdistro_tbl);      -- ASNOUTDISTRO_TBL


   O_routing_info := RIB_ROUTINGINFO_TBL();
   if L_requisition_type <> 'PO' then
      L_rib_routinginfo_rec := RIB_ROUTINGINFO_REC('from_phys_loc',
                                                   L_from_loc,
                                                   'from_phys_loc_type',
                                                   L_from_loc_type,
                                                   NULL,
                                                   NULL);

      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;

      L_rib_routinginfo_rec := "RIB_ROUTINGINFO_REC"('to_phys_loc',
                                                     L_to_loc,
                                                    'to_phys_loc_type',
                                                     L_to_loc_type,
                                                     NULL,
                                                     NULL);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;
   else
      L_rib_routinginfo_rec := RIB_ROUTINGINFO_REC('from_phys_loc',
                                                   L_from_loc,
                                                   'from_phys_loc_type',
                                                   'V',
                                                   NULL,
                                                   NULL);

      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;

      L_rib_routinginfo_rec := "RIB_ROUTINGINFO_REC"('to_phys_loc',
                                                     L_to_loc,
                                                     'to_phys_loc_type',
                                                     L_to_loc_type,
                                                     NULL,
                                                     NULL);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;
   end if;


   L_rib_routinginfo_rec := "RIB_ROUTINGINFO_REC"('source_app',
                                                  'RFM',
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;
   O_rib_asnoutdesc_rec := L_rib_asnoutdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SECONDARY_ASNOUT.BUILD_HEADER_OBJECT',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;
-------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_rib_asnoutdistro_tbl   IN OUT   "RIB_ASNOutDistro_TBL",
                               O_asn_number             IN OUT   FM_RECEIVING_DETAIL.ASN_NBR%TYPE,
                               I_recv_no                IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                               I_po_number              IN       FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE)
RETURN BOOLEAN IS

   L_recv_no              FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE := NULL;
   L_requisition_no       FM_RIB_STG_RECEIVING_DETAIL.PO_NUMBER%TYPE := NULL;
   L_item                 FM_RIB_STG_RECEIVING_DETAIL.ITEM%TYPE;
   L_ordered_qty          FM_RIB_STG_RECEIVING_DETAIL.ORDERED_QTY%TYPE;
   L_rib_asnoutitem_rec   "RIB_ASNOutItem_REC";
   L_rib_asnoutitem_tbl   "RIB_ASNOutItem_TBL" := "RIB_ASNOutItem_TBL"();
   L_rib_asnoutctn_rec    "RIB_ASNOutCtn_REC";
   L_rib_asnoutctn_tbl    "RIB_ASNOutCtn_TBL" := "RIB_ASNOutCtn_TBL"();
   L_rib_asnoutdistro_rec "RIB_ASNOutDistro_REC";
   L_carton_no            FM_RECEIVING_DETAIL.CONTAINER_ID%TYPE;
   L_asn_number           FM_RECEIVING_DETAIL.ASN_NBR%TYPE;
   L_doc_type             FM_RECEIVING_HEADER.DOCUMENT_TYPE%TYPE;
   L_to_loc               TSFHEAD.TO_LOC%TYPE       := NULL;
   L_to_loc_type          TSFHEAD.TO_LOC_TYPE%TYPE  := NULL;
   L_freight_code         TSFHEAD.FREIGHT_CODE%TYPE :=NULL;
   L_phy_wh               TSFHEAD.TO_LOC%TYPE       := NULL;
   L_expedite_flag        VARCHAR2(1);
   L_in_store_date        DATE;

   cursor C_GET_REQUISITION_NO is
      select distinct(po_number) requisition_no
        from fm_rib_stg_receiving_detail
       where recv_no = I_recv_no
         and po_number = NVL(I_po_number,po_number);

   TYPE requisition_tbl is table of C_GET_REQUISITION_NO%ROWTYPE INDEX BY BINARY_INTEGER;
   L_requisition_tbl          requisition_tbl;   

   cursor C_GET_ITEM (P_carton_no FM_RECEIVING_DETAIL.CONTAINER_ID%TYPE) is
      select fsrd.item item,
             fsrd.ordered_qty ordered_qty
        from fm_rib_stg_receiving_detail fsrd
       where fsrd.recv_no   = I_recv_no
         and fsrd.po_number = L_requisition_no
         and exists (select 'x' 
                       from fm_receiving_detail frd,
                            fm_receiving_header frh
                      where frd.requisition_no = fsrd.po_number
                        and frd.header_seq_no = frh.seq_no
                        and frh.recv_no = fsrd.recv_no
                        and frd.item = fsrd.item
                        and frd.container_id = P_carton_no);

   cursor C_GET_CARTON(P_requisition_no  FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE) is
      select distinct frd.container_id carton,
             frd.asn_nbr asn_nbr
        from fm_receiving_detail frd,
             fm_receiving_header frh
       where frh.recv_no        = I_recv_no
         and frh.seq_no         = frd.header_seq_no
         and frd.requisition_no = P_requisition_no;

   cursor C_GET_DOC_TYPE is
      select distinct frh.document_type
        from fm_receiving_header frh
        where frh.recv_no = I_recv_no;

   cursor C_GET_FREIGHT_CODE is
      select tsf.freight_code,
             tsf.to_loc,
             tsf.to_loc_type
        from tsfhead tsf
      where tsf.tsf_no = L_requisition_no;
      
   cursor C_GET_ALLOC_DETAIL is
      select ad.in_store_date,
             ad.rush_flag,
             ad.to_loc,
             ad.to_loc_type
        from alloc_detail ad,
             fm_receiving_detail frd,
             fm_receiving_header frh             
       where frd.header_seq_no = frh.seq_no
         and frd.requisition_no = ad.alloc_no       
         and ad.alloc_no = L_requisition_no
         and frh.recv_no = I_recv_no
         and DECODE(ad.to_loc_type,'S', ad.to_loc,'W',(select physical_wh from wh where wh.wh=ad.to_loc)) = frh.location_id;

   cursor C_GET_WH is
      select wh.physical_wh
        from wh wh
       where wh = L_to_loc;

   cursor C_LOC_INFO is
      select fsh.location_id,
             fsh.location_type
        from fm_rib_stg_receiving_header fsh
       where fsh.recv_no = I_recv_no;

BEGIN

   L_rib_asnoutitem_rec   := "RIB_ASNOutItem_REC"(0,      -- RIB_OID
                                                  NULL,   -- ITEM_ID
                                                  NULL,   -- UNIT_QTY
                                                  NULL,   -- GROSS_COST
                                                  NULL,   -- PRIORITY_LEVEL
                                                  NULL,   -- ORDER_LINE_NBR
                                                  NULL,   -- LOT_NBR
                                                  NULL,   -- FINAL_LOCATION
                                                  NULL,   -- FROM_DISPOSITION
                                                  NULL,   -- TO_DISPOSITION
                                                  NULL,   -- VOUCHER_NUMBER
                                                  NULL,   -- VOUCHER_EXPIRATION_DATE
                                                  NULL,   -- CONTAINER_QTY
                                                  NULL,   -- COMMENTS
                                                  NULL,   -- UNIT_COST
                                                  NULL,   -- WEIGHT
                                                  NULL);  -- WEIGHT_UOM

   L_rib_asnoutctn_rec   :=   "RIB_ASNOutCtn_REC"(0,     -- RIB_OID
                                                  NULL,  -- FINAL_LOCATION
                                                  NULL,  -- CONTAINER_ID
                                                  NULL,  -- CONTAINER_WEIGHT
                                                  NULL,  -- CONTAINER_LENGTH
                                                  NULL,  -- CONTAINER_WIDTH
                                                  NULL,  -- CONTAINER_HEIGHT
                                                  NULL,  -- CONTAINER_CUBE
                                                  NULL,  -- EXPEDITE_FLAG
                                                  NULL,  -- IN_STORE_DATE
                                                  NULL,  -- RMA_NBR
                                                  NULL,  -- TRACKING_NBR
                                                  NULL,  -- FREIGHT_CHARGE
                                                  NULL,  -- MASTER_CONTAINER_ID
                                                  NULL,  -- ASNOUTITEM_TBL
                                                  NULL,  -- COMMENTS
                                                  NULL,  -- WEIGHT
                                                  NULL); -- WEIGHT_UOM

   L_rib_asnoutdistro_rec := "RIB_ASNOutDistro_REC"(0,     -- RIB_OID
                                                    NULL,  -- DISTRO_NBR
                                                    NULL,  -- DISTRO_DOC_TYPE
                                                    NULL,  -- CUSTOMER_ORDER_NBR
                                                    NULL,  -- CONSUMER_DIRECT
                                                    NULL,  -- ASNOUTCTN_TBL
                                                    NULL); -- COMMENTS
   open C_GET_REQUISITION_NO;
   fetch C_GET_REQUISITION_NO BULK COLLECT into L_requisition_tbl;
   close C_GET_REQUISITION_NO;

   O_rib_asnoutdistro_tbl.DELETE;
   FOR i in 1..L_requisition_tbl.COUNT LOOP
      L_requisition_no := L_requisition_tbl(i).requisition_no;

      open C_GET_DOC_TYPE;
      fetch C_GET_DOC_TYPE into L_doc_type;
      close C_GET_DOC_TYPE;

      if L_doc_type = 'T' then
         open C_GET_FREIGHT_CODE;
         fetch C_GET_FREIGHT_CODE into L_freight_code,L_to_loc,L_to_loc_type;
         close C_GET_FREIGHT_CODE;
         if L_freight_code = 'E' then
            L_expedite_flag := 'Y';
         else
            L_expedite_flag := 'N';
         end if;
         L_in_store_date := NULL;   
      
      elsif L_doc_type = 'A' then
         open C_GET_ALLOC_DETAIL;
         fetch C_GET_ALLOC_DETAIL into L_in_store_date,
                                       L_expedite_flag,
                                       L_to_loc,
                                       L_to_loc_type;
         close  C_GET_ALLOC_DETAIL;                                   
     
      else
         open C_LOC_INFO;
         fetch C_LOC_INFO into L_to_loc, L_to_loc_type;
         close C_LOC_INFO;
      end if;

      if L_to_loc_type = 'W' then
         open C_GET_WH;
         fetch C_GET_WH into L_phy_wh;
         close C_GET_WH;
         L_to_loc := L_phy_wh;
      end if;

      L_rib_asnoutctn_tbl.DELETE;
      FOR L_ctn IN C_GET_CARTON(L_requisition_no) LOOP
         L_rib_asnoutitem_tbl.DELETE;
         FOR L_item IN C_GET_ITEM(L_ctn.carton) LOOP

            L_rib_asnoutitem_rec.item_id        := L_item.item;
            L_rib_asnoutitem_rec.unit_qty       := L_item.ordered_qty;
            L_rib_asnoutitem_rec.final_location := L_to_loc;
            L_rib_asnoutitem_rec.unit_cost      := NULL;

         -- Container_qty is a required field so RFM has to send 1 instead of NULL

            L_rib_asnoutitem_rec.container_qty  := 1;
            L_rib_asnoutitem_tbl.EXTEND;
            L_rib_asnoutitem_tbl(L_rib_asnoutitem_tbl.COUNT) := L_rib_asnoutitem_rec;

         END LOOP;

   --    Carton Details
         L_rib_asnoutctn_rec.final_location := L_to_loc;
         L_rib_asnoutctn_rec.container_id   := L_ctn.carton;
         L_rib_asnoutctn_rec.expedite_flag  := L_expedite_flag;
         L_rib_asnoutctn_rec.in_store_date  := L_in_store_date;
         L_rib_asnoutctn_rec.asnoutitem_tbl := L_rib_asnoutitem_tbl;
         O_asn_number := L_ctn.asn_nbr;         
         L_rib_asnoutctn_tbl.EXTEND;
         L_rib_asnoutctn_tbl(L_rib_asnoutctn_tbl.COUNT) := L_rib_asnoutctn_rec;
      END LOOP;
   -- AsnoutDistro
      L_rib_asnoutdistro_rec.distro_nbr         := L_requisition_no;
      L_rib_asnoutdistro_rec.distro_doc_type    := L_doc_type;
      L_rib_asnoutdistro_rec.customer_order_nbr := 1;
      L_rib_asnoutdistro_rec.asnoutctn_tbl      := L_rib_asnoutctn_tbl;
   ---
      O_rib_asnoutdistro_tbl.EXTEND;
      O_rib_asnoutdistro_tbl(O_rib_asnoutdistro_tbl.COUNT) := L_rib_asnoutdistro_rec;
   ---
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SECONDARY_ASNOUT.BUILD_DETAIL_OBJECTS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_OBJECTS;
-------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_message   IN  OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                        O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_recv_no         IN           FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                        I_seq_no          IN           FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
IS

   L_error_type         VARCHAR2(5) := NULL;

BEGIN
   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then
      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_recv_no);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                 I_seq_no,
                                                                 NULL,
                                                                 NULL,
                                                                 NULL,
                                                                 NULL));
      update FM_RIB_RECEIVING_MFQUEUE
         set pub_status = LP_error_status
       where seq_no     = I_seq_no;
   end if;

   SQL_LIB.API_MSG(L_error_type,
                   O_error_message);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'FM_SECONDARY_ASNOUT');
END HANDLE_ERRORS;
-------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_queue_locked    IN OUT   BOOLEAN,
                         I_seq_no          IN       FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'FM_RIB_RECEIVING_MFQUEUE';
   L_key1              VARCHAR2(100) := I_seq_no;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked,-54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from FM_RIB_RECEIVING_MFQUEUE
       where seq_no = I_seq_no
         for update nowait;

BEGIN
   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      O_queue_locked := TRUE;
      return TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SECONDARY_ASNOUT.LOCK_THE_BLOCK',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_THE_BLOCK;
-------------------------------------------------------------------------------
FUNCTION DELETE_QUEUE_REC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_recv_no         IN       FM_RIB_RECEIVING_MFQUEUE.RECV_NO%TYPE,
                           I_po_number       IN       FM_RIB_RECEIVING_MFQUEUE.PO_NUMBER%TYPE,
                           I_seq_no          IN       FM_RIB_RECEIVING_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN IS

 cursor C_CHECK_RIB_DTL is
      select count(1)
        from FM_RIB_STG_RECEIVING_DETAIL
       where recv_no = I_recv_no;

L_count NUMBER := 0;

BEGIN

   delete from FM_RIB_STG_RECEIVING_DETAIL
    where RECV_NO = I_recv_no
      and PO_NUMBER = NVL(I_po_number,PO_NUMBER);
   delete from FM_RIB_RECEIVING_MFQUEUE
    where RECV_NO = I_recv_no
      and SEQ_NO = I_seq_no;

   open C_CHECK_RIB_DTL;
   fetch C_CHECK_RIB_DTL into L_count;
   close C_CHECK_RIB_DTL;

   if L_count = 0 then
      delete from FM_RIB_STG_RECEIVING_HEADER
       where RECV_NO = I_recv_no;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_SECONDARY_ASNOUT.DELETE_QUEUE_REC',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DELETE_QUEUE_REC;
-------------------------------------------------------------------------------
END FM_SECONDARY_ASNOUT;
/