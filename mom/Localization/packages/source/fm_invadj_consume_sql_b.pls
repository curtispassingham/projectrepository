CREATE OR REPLACE PACKAGE BODY FM_INVADJ_CONSUME_SQL as
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind            IN OUT  BOOLEAN,
                 I_seq_no               IN      FM_STG_ADJUST_DESC.SEQ_NO%TYPE)
return BOOLEAN is
   L_program           VARCHAR2(100)            := 'FM_INVADJ_CONSUME_SQL.CONSUME';
   L_status_code       VARCHAR2(1);
   L_message_type      VARCHAR(20)              := 'invadjustcre';
   L_cursor            VARCHAR2(32)             := NULL;
   L_table             VARCHAR2(255)            := NULL;   
   L_key               VARCHAR2(150)            := NULL;
   L_invadj_message    "RIB_InvAdjustDesc_REC";
  
   cursor C_GET_INVADJ_DESC is
      select "RIB_InvAdjustDesc_REC" (0,                 -- rib_oid
                                      iadesc.dc_dest_id, -- dc_dest_id
                                      CAST(MULTISET(select "RIB_InvAdjustDtl_REC" (0,              -- rib_oid
                                                                                   iadtl.item_id,
                                                                                   iadtl.adjustment_reason_code,
                                                                                   iadtl.unit_qty,
                                                                                   iadtl.transshipment_nbr,
                                                                                   iadtl.from_disposition,
                                                                                   iadtl.to_disposition,
                                                                                   iadtl.from_trouble_code,
                                                                                   iadtl.to_trouble_code,
                                                                                   iadtl.from_wip_code,
                                                                                   iadtl.to_wip_code,
                                                                                   iadtl.transaction_code,
                                                                                   iadtl.user_id,
                                                                                   iadtl.create_date,
                                                                                   iadtl.po_nbr,
                                                                                   iadtl.doc_type,
                                                                                   iadtl.aux_reason_code,
                                                                                   iadtl.weight,
                                                                                   iadtl.weight_uom,
                                                                                   iadtl.ebc_cost)
                                                      from fm_stg_adjust_dtl iadtl
                                                     where iadtl.desc_seq_no = iadesc.seq_no) as "RIB_InvAdjustDtl_TBL"))
        from fm_stg_adjust_desc iadesc
       where iadesc.seq_no = I_seq_no
         and iadesc.status in ('P','H');

BEGIN
   -- Populate inventory adjustment message
   L_table  := 'fm_stg_adjust_desc, fm_stg_adjust_dtl';
   L_cursor := 'C_GET_INVADJ_DESC';
   L_key    := 'seq_no = '||TO_CHAR(I_seq_no);
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_GET_INVADJ_DESC;
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_GET_INVADJ_DESC into L_invadj_message;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_GET_INVADJ_DESC;  

   -- Call procedure Consume
   L_status_code   := NULL;
   O_error_message := NULL;    
   RMSSUB_INVADJUST.CONSUME_INVADJ(L_status_code,
                                   O_error_message,
                                   L_invadj_message,
                                   L_message_type,
                                   'N');
   if L_status_code = API_CODES.UNHANDLED_ERROR or O_error_message is NOT NULL then
      O_error_ind := TRUE;
      return FALSE;
   else
      O_error_ind := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_INVADJ_DESC%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_INVADJ_DESC',
                          'fm_stg_adjust_desc, fm_stg_adjust_dtl',
                          'seq_no = '||TO_CHAR(I_seq_no));       
         close C_GET_INVADJ_DESC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CONSUME;
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_no     IN     NUMBER,
                              I_num_threads   IN     NUMBER)
return BOOLEAN is
   L_program           VARCHAR2(100)  := 'FM_INVADJ_CONSUME_SQL.BATCH_CONSUME_THREAD';
   L_seq_tbl           NUMBER_TBL  := NUMBER_TBL ();
   L_fiscal_doc_id_tbl NUMBER_TBL  := NUMBER_TBL ();
   L_error_ind         BOOLEAN;

   L_table             VARCHAR2(255)   := NULL;
   L_cursor            VARCHAR2(32)    := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   -- Retrieves sequence numbers and fiscal doc IDs from staging table
   -- records that are in 'H'old status for the current location thread
   cursor C_GET_STG_ADJUST is
      select seq_no,
             fiscal_doc_id
        from fm_stg_adjust_desc
       where status = 'H'
         and mod(dc_dest_id, I_num_threads)+1 = I_thread_no
    order by seq_no;
   ---
   -- Locks staging table records that are in 'H'old stsatus for the current
   -- location thread
   cursor C_LOCK_FM_STG_ADJUST_DESC is
      select 'x'
        from fm_stg_adjust_desc
       where status = 'H'
         and mod(dc_dest_id, I_num_threads)+1 = I_thread_no
         for update nowait;
   ---
   -- Lock records in FM_STG_ADJUST with sequence numbers fetched from
   -- C_GET_STG_ADJUST cursor
   cursor C_LOCK_FM_STG_ADJUST_DTL is
      select 'x'
        from fm_stg_adjust_dtl sad
       where exists (select 'x'
                       from TABLE(CAST(L_seq_tbl as NUMBER_TBL )) seq
                      where value(seq) = sad.desc_seq_no
                        and rownum = 1)
         for update nowait;
   ---
   -- Lock records with a requisition type of 'STOCK' and that 
   -- have already been processed by the CONSUME call
   cursor C_LOCK_FM_FISCAL_DOC_HEADER is
      select 'x'
        from fm_fiscal_doc_header fdh
       where requisition_type = 'STOCK'
         and exists (select 'x'  
                       from TABLE(CAST(L_fiscal_doc_id_tbl as NUMBER_TBL )) fdit
                      where value(fdit) = fdh.fiscal_doc_id
                        and rownum = 1)
         and not exists (select 'x'
                           from fm_stg_adjust_desc sad
                          where sad.fiscal_doc_id = fdh.fiscal_doc_id
                            and rownum = 1)
         for update nowait;
   ---
   -- Lock records in the schedule table that correspond to records
   -- in FM_FISCAL_DOC_HEADER that have already been processed
   cursor C_LOCK_FM_SCHEDULE is
      select 'x'
        from fm_schedule s
       where exists (select 'x'
                       from fm_fiscal_doc_header fdh
                      where fdh.status = 'A'
                        and fdh.schedule_no = s.schedule_no
                        and exists (select 'x'
                                      from TABLE(CAST(L_fiscal_doc_id_tbl as NUMBER_TBL )) fdit
                                     where value(fdit) = fdh.fiscal_doc_id
                                       and rownum = 1)
                       and rownum = 1)
         for update nowait;                    
   ---
BEGIN
   -- Get records  from the staging table from the staging table 
   -- that are in 'H'old status for this location
   L_cursor := 'C_GET_STG_ADJUST';
   L_table  := 'fm_stg_adjust_desc';
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, NULL);
   open C_GET_STG_ADJUST;
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, NULL);
   fetch C_GET_STG_ADJUST BULK COLLECT into L_seq_tbl, L_fiscal_doc_id_tbl;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, NULL);
   close C_GET_STG_ADJUST;
   
   -- Check that records in 'H'old status were retrieved
   if L_seq_tbl is NULL or L_seq_tbl.count <= 0 then
      return TRUE;
   end if;
   
   -- Consume each staged message for this location
   FOR rec in L_seq_tbl.FIRST..L_seq_tbl.LAST LOOP
      if FM_INVADJ_CONSUME_SQL.CONSUME(O_error_message,
                                       L_error_ind,
                                       L_seq_tbl(rec)) = FALSE then
         return FALSE;
      end if;   
   END LOOP;
   
   -- Delete records from FM_STG_ADJUST_DTL that correspond to consumed messages
   L_cursor := 'C_LOCK_FM_STG_ADJUST_DTL';
   L_table  := 'fm_stg_adjust_dtl';
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, NULL);
   open C_LOCK_FM_STG_ADJUST_DTL;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, NULL);
   close C_LOCK_FM_STG_ADJUST_DTL;

   SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
   delete from fm_stg_adjust_dtl sad
    where exists (select 'x'
                    from TABLE(CAST(L_seq_tbl as NUMBER_TBL )) seq
                   where value(seq) = sad.desc_seq_no
                     and rownum = 1);

   -- Delete records from FM_STG_ADJUST_DESC that correspond to consumed messages
   L_cursor := 'C_LOCK_FM_STG_ADJUST_DESC';
   L_table  := 'fm_stg_adjust_desc';
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, NULL);
   open C_LOCK_FM_STG_ADJUST_DESC;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, NULL);
   close C_LOCK_FM_STG_ADJUST_DESC;
   
   SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
   delete from fm_stg_adjust_desc sad
    where exists (select 'x'
                    from TABLE(CAST(L_seq_tbl as NUMBER_TBL )) seq
                   where value(seq) = sad.seq_no
                     and rownum = 1);

   -- Update status to 'A'pproved for processed records in FM_FISCAL_DOC_HEADER
   L_cursor := 'C_LOCK_FM_FISCAL_DOC_HEADER';
   L_table  := 'fm_fiscal_doc_header';
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, NULL);
   open C_LOCK_FM_FISCAL_DOC_HEADER;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, NULL);
   close C_LOCK_FM_FISCAL_DOC_HEADER;
   
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, NULL);
   update fm_fiscal_doc_header fdh
      set fdh.status = 'A'
    where requisition_type = 'STOCK'
      and exists (select 'x'  
                    from TABLE(CAST(L_fiscal_doc_id_tbl as NUMBER_TBL )) fdit
                   where value(fdit) = fdh.fiscal_doc_id
                     and rownum = 1)
      and not exists (select 'x'    -- Do not update the status for records that have not been processed
                        from fm_stg_adjust_desc sad
                       where sad.fiscal_doc_id = fdh.fiscal_doc_id
                         and rownum = 1);
   
   -- Update schedule status to 'A'pproved for processed records
   L_cursor := 'C_LOCK_FM_SCHEDULE';
   L_table  := 'fm_schedule';
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, NULL);
   open C_LOCK_FM_SCHEDULE;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, NULL);
   close C_LOCK_FM_SCHEDULE;
   
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, NULL);
   update fm_schedule s
      set s.status = 'A'
    where exists (select 'x'
                    from fm_fiscal_doc_header fdh
                   where fdh.status = 'A'
                     and fdh.schedule_no = s.schedule_no
                     and exists (select 'x'
                                   from TABLE(CAST(L_fiscal_doc_id_tbl as NUMBER_TBL )) fdit
                                  where value(fdit) = fdh.fiscal_doc_id
                                    and rownum = 1)
                     and rownum = 1);
   
   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END BATCH_CONSUME_THREAD;
---------------------------------------------------------------------------------------------
END FM_INVADJ_CONSUME_SQL;
/
