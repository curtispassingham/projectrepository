create or replace PACKAGE BODY FM_UPD_SCHD_STATUS_SQL is
------------------------------------------------------------------------------------------------
FUNCTION NF_COST_TAX_DISCREP_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                    O_status         OUT     FM_FISCAL_DOC_HEADER.STATUS%TYPE)
RETURN BOOLEAN IS
   ---
   L_program              VARCHAR2(50) := 'FM_UPD_SCHD_STATUS_SQL.NF_COST_TAX_DISCREP_EXISTS';
   L_key                  VARCHAR2(50) := 'I_fiscal_doc_id: ' || I_fiscal_doc_id;
   L_cost_tax_desc        VARCHAR(1)   := NULL;
   L_cost_tax_desc_compl  VARCHAR(1)   := NULL;
   L_yes                  VARCHAR2(1)  := 'Y';
   L_status               INTEGER;
   ---
   C_DISCREP_STATUS       CONSTANT FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE := 'D';--Discrepancy status
   C_APPROVED_STATUS      CONSTANT FM_SCHEDULE.STATUS%TYPE := 'A';--Approved status
   C_HOLD_STATUS          CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'H';--Hold status
   ---
   L_rec_st_ind           V_BR_WH_CONTROL_FLAGS.CONTROL_REC_ST_IND%TYPE := NULL;
   L_utilization_st_ind   FM_UTILIZATION_ATTRIBUTES.ICMSST_RECOVERY_IND%TYPE := NULL;
   ---
   L_comp_nf_ind          FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind           ORDHEAD.TRIANGULATION_IND%TYPE;
   L_triangulation        ORDHEAD.TRIANGULATION_IND%TYPE;
   L_compl_fiscal_doc_id  FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   L_batch_running_ind    RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE;
    ---
   cursor C_COST_TAX_DESCREP(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = P_fiscal_doc_id
         and (cost_discrep_status = C_DISCREP_STATUS OR tax_discrep_status = C_DISCREP_STATUS )
         ---
       union
         ---
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = P_fiscal_doc_id
         and tax_discrep_status = C_DISCREP_STATUS ;
   ---
   cursor C_GET_REC_ST_IND is
      select distinct DECODE(fs.location_type,'S',scf.control_rec_st_ind,'W',wcf.control_rec_st_ind) as control_ind,
             fa.icmsst_recovery_ind
       from v_br_store_control_flags scf,
            v_br_wh_control_flags wcf,
            fm_schedule fs,
            fm_fiscal_doc_header fdh,
            fm_fiscal_utilization fu,
            fm_utilization_attributes fa
      where ((fs.location_type = 'S'
        and fs.location_id     = scf.store)
         or (fs.location_type  = 'W'
        and fs.location_id     = wcf.wh))
        and fdh.schedule_no    = fs.schedule_no
        and fu.status          = 'A'
        and fa.utilization_id  = fu.utilization_id
        and fa.utilization_id  = fdh.utilization_id
        and fdh.fiscal_doc_id  = I_fiscal_doc_id;
   ---
   cursor C_GET_REQUISITION_NO is
      SELECT DISTINCT requisition_no
        FROM fm_fiscal_doc_detail
       WHERE fiscal_doc_id = I_fiscal_doc_id;
   --
   CURSOR C_CHECK_FOR_TRIAG IS
      SELECT fdc.compl_fiscal_doc_id,ord.triangulation_ind
        FROM fm_fiscal_doc_header fdh
            ,fm_fiscal_doc_detail fdd
            ,fm_fiscal_doc_complement fdc
            ,ordhead ord
       WHERE fdh.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdh.fiscal_doc_id     = fdc.fiscal_doc_id
         AND fdd.requisition_no    = ord.order_no
         AND ord.triangulation_ind = L_yes
         AND fdh.fiscal_doc_id     = I_fiscal_doc_id
       UNION
   SELECT fdc.fiscal_doc_id,ord.triangulation_ind
        FROM fm_fiscal_doc_header fdh
            ,fm_fiscal_doc_detail fdd
            ,fm_fiscal_doc_complement fdc
            ,ordhead ord
       WHERE fdh.fiscal_doc_id     = fdd.fiscal_doc_id
         AND fdh.fiscal_doc_id     = fdc.compl_fiscal_doc_id
         AND fdd.requisition_no    = ord.order_no
         AND ord.triangulation_ind = L_yes
         AND fdh.fiscal_doc_id     = I_fiscal_doc_id;
BEGIN
   --- Checking any  COST/TAX discrepancy exists, if not then update the NF status to "Approved"
   SQL_LIB.SET_MARK('OPEN',
                    'C_COST_TAX_DESCREP',
                    'FM_FISCAL_DOC_DETAIL',
                    L_key);
   open C_COST_TAX_DESCREP(I_fiscal_doc_id);
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_COST_TAX_DESCREP',
                    'FM_FISCAL_DOC_DETAIL',
                    L_key);
   fetch C_COST_TAX_DESCREP into L_cost_tax_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COST_TAX_DESCREP',
                    'FM_FISCAL_DOC_DETAIL',
                    L_key);
   close C_COST_TAX_DESCREP;
   ---
   open C_CHECK_FOR_TRIAG;
   fetch C_CHECK_FOR_TRIAG into L_compl_fiscal_doc_id,L_triangulation;
   close C_CHECK_FOR_TRIAG;
   ---
   if RMS_BATCH_STATUS_SQL.GET_BATCH_RUNNING_IND(O_error_message,
                                                  L_batch_running_ind) = FALSE then
	   return FALSE;
   end if;
   ---
   if L_triangulation = 'Y' then
   /*check if  comp NF has cost or tax discrep*/
      SQL_LIB.SET_MARK('OPEN',
             'C_COST_TAX_DESCREP',
             'FM_FISCAL_DOC_DETAIL',
             L_compl_fiscal_doc_id);
      open C_COST_TAX_DESCREP(L_compl_fiscal_doc_id);
      ---
      SQL_LIB.SET_MARK('FETCH',
             'C_COST_TAX_DESCREP',
             'FM_FISCAL_DOC_DETAIL',
             L_compl_fiscal_doc_id);
      fetch C_COST_TAX_DESCREP into L_cost_tax_desc_compl;
      ---
      SQL_LIB.SET_MARK('CLOSE',
             'C_COST_TAX_DESCREP',
             'FM_FISCAL_DOC_DETAIL',
             L_compl_fiscal_doc_id);
      close C_COST_TAX_DESCREP;
      ---
      if (L_cost_tax_desc IS NULL and L_cost_tax_desc_compl is NULL) then
         ---update header status to 'A' for both Compl NF and Main NF.
         if FM_RECEIVING_SQL.CALC_FINAL_CLT(O_error_message,
                   I_fiscal_doc_id)= FALSE then
            return FALSE;
         end if;
         if FM_RECEIVING_SQL.CALC_FINAL_CLT(O_error_message,
                   L_compl_fiscal_doc_id)= FALSE then
            return FALSE;
         end if;
         ---	 
         if L_batch_running_ind = 'N' then
		 if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message ,
			     I_fiscal_doc_id,
			     C_APPROVED_STATUS) = FALSE then
		    return FALSE;
		 end if;
		 if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message ,
			     L_compl_fiscal_doc_id,
			     C_APPROVED_STATUS) = FALSE then
		    return FALSE;
		 end if;
		 ---
		 O_status := 'A';
         else
	       if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
							   I_fiscal_doc_id,
							   C_HOLD_STATUS) = FALSE then
                    return FALSE;
		 end if;
             if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
						           L_compl_fiscal_doc_id,
						           C_HOLD_STATUS) = FALSE then
                    return FALSE;
		 end if;
             O_status := 'H';
             ---
         end if;
      else
         O_status := 'RV';
      end if;
   else
      if (L_cost_tax_desc IS NULL) then
         ---
         if FM_RECEIVING_SQL.CALC_FINAL_CLT(O_error_message,
                   I_fiscal_doc_id)= FALSE then
             return FALSE;
         end if;
         ---
	 if L_batch_running_ind = 'N' then
		  if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message ,
			                                    I_fiscal_doc_id,
			                                    C_APPROVED_STATUS) = FALSE then
		      return FALSE;
		  end if;
		  ---
		  O_status := 'A';
		  ---
         else
		if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message ,
			                                  I_fiscal_doc_id,
			                                  C_HOLD_STATUS) = FALSE then
		      return FALSE;
		end if;
		---
		O_status := 'H';
		---
         end if;
      else
         O_status := 'RV';
      end if;
   end if;

   if (O_status = 'A') then
	 for rec_requisition_no in C_GET_REQUISITION_NO
	 LOOP
		if REC_COST_ADJ_SQL.FM_RECEIPT_MATCHED(O_error_message,
						   rec_requisition_no.requisition_no,
						   NULL) = FALSE then
		return FALSE;
		end if;
	 END LOOP;
	end if;
	---
	if O_status in ('A','H') then
		 SQL_LIB.SET_MARK('OPEN','C_GET_REC_ST_IND','C_GET_REC_ST_IND',L_key);
		 open  C_GET_REC_ST_IND;
		 SQL_LIB.SET_MARK('FETCH','C_GET_REC_ST_IND','C_GET_REC_ST_IND',L_key);
		 fetch C_GET_REC_ST_IND into L_rec_st_ind,L_utilization_st_ind;
		 SQL_LIB.SET_MARK('CLOSE','C_GET_REC_ST_IND','C_GET_REC_ST_IND',L_key);
		 close C_GET_REC_ST_IND;
		 ---
		 if (FM_FISCAL_VALIDATION_SQL.GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
		      return FALSE;
		 end if;
		 ---
		 if L_triang_ind = 'N' then
		    if NVL(L_rec_st_ind,'N') ='Y' and NVL(L_utilization_st_ind,'N') = 'Y' then
		       if FM_POST_RECEIVING_SQL.UPDATE_ST_HISTORY(O_error_message,
							       I_fiscal_doc_id) = FALSE then
			  return FALSE;
		       end if;
		       if FM_EXT_TAXES_SQL.UPDATE_HIST_TABLES(O_error_message,
							      L_status,
							      I_fiscal_doc_id) = FALSE then
			  return FALSE;
		       end if;
		 end if;
        end if;
		---
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_COST_TAX_DESCREP%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_COST_TAX_DESCREP',
                          'FM_FISCAL_DOC_DETAIL',
                          L_key);
         close C_COST_TAX_DESCREP;
      end if;
      ---
      if C_GET_REC_ST_IND%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_REC_ST_IND',
                          'C_GET_REC_ST_IND',
                          L_key);
         close C_GET_REC_ST_IND;
      end if;
      ---
      return FALSE;
END NF_COST_TAX_DISCREP_EXISTS;
------------------------------------------------------------------------------------------------
-- Function Name: UPD_SCHED_STATUS
-- Purpose:       This function sets the schedule status, based on the various NF status under that schedule.
------------------------------------------------------------------------------------------------
FUNCTION UPD_SCHEDULE_STATUS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_schedule_no    IN      FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program VARCHAR2(50) := 'FM_UPD_SCHD_STATUS_SQL.UPD_SCHED_STATUS';
   L_key     VARCHAR2(50) := 'I_schedule_no: ' || I_schedule_no;
   L_dummy   VARCHAR2(1);
   L_status  FM_SCHEDULE.STATUS%TYPE ;
   ---
   L_fiscal_doc_id  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_supplier       FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE;
   L_rural_prod_ind varchar2(1):= NULL;
   L_rpo            varchar2(3):= 'RPO';
   L_rpo_nf_count   integer;
   ---

   cursor C_EXISTS_RECEIVED is
      select 'X'
        from fm_fiscal_doc_header fdd
       where fdd.schedule_no = I_schedule_no
         and fdd.status = 'R';
   ---
   cursor C_EXISTS_RECE_VERIFIED is
      select 'X'
        from fm_fiscal_doc_header fdd
       where fdd.schedule_no = I_schedule_no
         and fdd.status = 'RV';
   ---
   cursor C_EXISTS_HOLD is
      select 'X'
        from fm_fiscal_doc_header fdd
       where fdd.schedule_no = I_schedule_no
         and fdd.status = 'H';

   CURSOR C_GET_DOC_ID IS
      select fdh.fiscal_doc_id,
             fdh.key_value_1
        from fm_fiscal_doc_header fdh
       where fdh.schedule_no      = I_schedule_no
         and fdh.requisition_type = 'PO'
         and fdh.module           = 'SUPP';

   CURSOR C_GET_RURAL_PROD_IND (P_supplier FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE) IS
      select sfc.rural_prod_ind
        from v_br_sups_fiscal_class sfc
       where sfc.supplier=P_supplier;

   CURSOR C_CHK_RPO_NF(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select count(fdd.fiscal_doc_id)
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id_ref = P_fiscal_doc_id
         and fdd.fiscal_doc_id     = fdh.fiscal_doc_id
         and fdh.requisition_type  = L_rpo;

   BEGIN
   ---
   if I_schedule_no is NULL then
        O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_schedule_no',
                                               I_schedule_no,
                                                NULL);
        return FALSE;
   end if;
   --- Check is exists any NF with Received status
   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS_RECEIVED',
                    'FM_FISCAL_DOC_HEADER',
                     L_key);
   open C_EXISTS_RECEIVED;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS_RECEIVED',
                    'FM_FISCAL_DOC_HEADER',
                     L_key);
   ---
   fetch C_EXISTS_RECEIVED into L_dummy;
   if C_EXISTS_RECEIVED%FOUND then
       L_status := 'R';
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS_RECEIVED',
                    'FM_FISCAL_DOC_HEADER',
                     L_key);
   close C_EXISTS_RECEIVED;

   --- Check is exists any NF with Receipt Verified status
   if NVL(L_status,'N') <> 'R' then
      SQL_LIB.SET_MARK('OPEN',
         'C_EXISTS_RECE_VERIFIED',
         'FM_FISCAL_DOC_HEADER',
          L_key);
      open C_EXISTS_RECE_VERIFIED;
      ---
      SQL_LIB.SET_MARK('FETCH',
         'C_EXISTS_RECE_VERIFIED',
         'FM_FISCAL_DOC_HEADER',
          L_key);
      fetch C_EXISTS_RECE_VERIFIED into L_dummy;
      if C_EXISTS_RECE_VERIFIED%FOUND then
       L_status := 'RV';
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
         'C_EXISTS_RECE_VERIFIED',
         'FM_FISCAL_DOC_HEADER',
          L_key);
      close C_EXISTS_RECE_VERIFIED;
   end if;
   ---
   --- Check is exists any NF with Hold status
   if NVL(L_status,'N') not in ('R','RV') then
      SQL_LIB.SET_MARK('OPEN',
         'C_EXISTS_HOLD',
         'FM_FISCAL_DOC_HEADER',
          L_key);
      open C_EXISTS_HOLD;
      ---
      SQL_LIB.SET_MARK('FETCH',
         'C_EXISTS_HOLD',
         'FM_FISCAL_DOC_HEADER',
          L_key);
      fetch C_EXISTS_HOLD into L_dummy;
      if C_EXISTS_HOLD%FOUND then
         L_status := 'H';
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
         'C_EXISTS_HOLD',
         'FM_FISCAL_DOC_HEADER',
          L_key);
      close C_EXISTS_HOLD;
   end if;
   ---
   --- if none of NF is in Received, Receipt Verified Status
   if NVL(L_status,'N') = 'H' then
        L_status := 'H';
   elsif NVL(L_status,'N') not in ('R','RV') then
        L_status := 'A';
   end if;
   ---
   -- update schedule
   if FM_SCHEDULE_VAL_SQL.UPDATE_SCHD_STATUS(O_error_message,
                                             I_schedule_no,
                                             L_status) = FALSE then
      return FALSE;
   end if;
   ---
   if L_status in ('A','H') then
      open C_GET_DOC_ID;
         loop
         fetch C_GET_DOC_ID into L_fiscal_doc_id,L_supplier;
          exit when C_GET_DOC_ID%NOTFOUND;
            open C_GET_RURAL_PROD_IND(L_supplier);
           fetch C_GET_RURAL_PROD_IND into L_rural_prod_ind;
           close C_GET_RURAL_PROD_IND;
          if NVL(L_rural_prod_ind,'N') = 'Y' then
              open C_CHK_RPO_NF(L_fiscal_doc_id);
             fetch C_CHK_RPO_NF into L_rpo_nf_count;
             close C_CHK_RPO_NF;
             if  L_rpo_nf_count = 0 then
                if FM_RURAL_PROD_NF_SQL.CREATE_NEW_NF(O_error_message,
                                                      I_schedule_no,
                                                      L_fiscal_doc_id)=FALSE then
                   return FALSE;
                end if;
             end if;
          end if;
         end loop;
      close C_GET_DOC_ID;
   end if;

   return TRUE;
   ---
   EXCEPTION
      when OTHERS then
         ---
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SUBSTR(SQLERRM, 1, 254),
                                               L_program,
                                               TO_CHAR(SQLCODE));
         ---
         if C_EXISTS_RECEIVED%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_RECEIVED',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_RECEIVED;
         end if;
         ---
         if C_EXISTS_RECE_VERIFIED%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXISTS_RECE_VERIFIED',
                             'FM_FISCAL_DOC_HEADER',
                             L_key);
            close C_EXISTS_RECE_VERIFIED;
         end if;
         ---
         L_status := 'R';
         ---
         return FALSE;
         ---
END UPD_SCHEDULE_STATUS;
-----------------------------------------------------------------------------------
END FM_UPD_SCHD_STATUS_SQL;
/