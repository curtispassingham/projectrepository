CREATE OR REPLACE PACKAGE FM_NFE_PUB_SQL IS
--------------------------------------------------------------------------------
PROCEDURE GET_NFE_INFO(O_status_code       IN OUT  NUMBER,
                       O_error_message     IN OUT  VARCHAR2,
                       O_message           IN OUT  "OBJ_FM_NFE_DOCHDR_REC",
                       I_fiscal_doc_id     IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE);
--------------------------------------------------------------------------------
END FM_NFE_PUB_SQL;
/
 
