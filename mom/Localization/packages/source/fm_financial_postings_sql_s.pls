CREATE OR REPLACE PACKAGE FM_FINANCIAL_POSTING_SQL as
----------------------------------------------------------------------------------------------
TYPE SEG_VAL_REC IS RECORD
(
segment_value   varchar2(25)
);

TYPE SEG_VAL_TBL IS TABLE OF SEG_VAL_REC
INDEX BY BINARY_INTEGER;
---
TYPE SEG_TYPE IS RECORD
(
credit_debit    varchar2(1),
L_seg_val_tab   SEG_VAL_TBL
);

TYPE SEG_TYPE_TBL IS TABLE OF SEG_TYPE
INDEX BY BINARY_INTEGER;
----------------------------------------------------------------------------------------------
FUNCTION FINANCIAL_ROLL_UP(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id    IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                          )
   return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION POST_AP_FINANCIALS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                         ,I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                         ,I_coa_id             IN     FM_COA_SETUP.COA_ID%TYPE
                          )
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_TEMP_TABLE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_seq_no         IN OUT NUMBER
                          ,I_sob_id         IN     FM_SOB_SETUP.SET_OF_BOOKS_ID%TYPE
                          ,I_coa_id         IN OUT FM_COA_SETUP.COA_ID%TYPE
                          ,I_org_unit_id    IN     STORE.ORG_UNIT_ID%TYPE
                          ,I_tran_code      IN     FM_TRAN_DATA.TRAN_CODE%TYPE
                          ,I_fiscal_doc_id  IN     FM_TRAN_DATA.FISCAL_DOC_ID%TYPE
                          ,I_fiscal_doc_no  IN     FM_TRAN_DATA.FISCAL_DOC_NO%TYPE
                          ,I_dept           IN     FM_TRAN_DATA.DEPT%TYPE
                          ,I_class          IN     FM_TRAN_DATA.CLASS%TYPE
                          ,I_subclass       IN     FM_TRAN_DATA.SUBCLASS%TYPE
                          ,I_location_type  IN     FM_TRAN_DATA.LOCATION_TYPE%TYPE
                          ,I_location       IN     FM_TRAN_DATA.LOCATION%TYPE
                          ,I_tsf_entity_id  IN     STORE.TSF_ENTITY_ID%TYPE
                          ,I_total_cost     IN     FM_TRAN_DATA.TOTAL_COST%TYPE
                          ,I_ref_no_1       IN     FM_TRAN_DATA.REF_NO_1%TYPE
                          ,I_ref_no_2       IN     FM_TRAN_DATA.REF_NO_2%TYPE 
                          ,I_ref_no_3       IN     FM_TRAN_DATA.REF_NO_3%TYPE
                          ,I_ref_no_4       IN     FM_TRAN_DATA.REF_NO_4%TYPE
                          ,I_ref_no_5       IN     FM_TRAN_DATA.REF_NO_5%TYPE
                          ,I_tran_date      IN     FM_TRAN_DATA.TRAN_DATE%TYPE
                          ,I_segment        IN OUT SEG_TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: UPDATE_NF_STATUS_FP
-- Purpose:       This function will update the NF status to FP once the amount is posted to ledgers.
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_NF_STATUS_FP( O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                            ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION GET_DB_CR_SEGMENTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_credit_seg_val_rec IN OUT SEG_TYPE,
                            O_debit_seg_val_rec  IN OUT SEG_TYPE,
                            I_tran_code          IN     FM_TRAN_DATA.TRAN_CODE%TYPE,
                            I_ref_no_1           IN     FM_TRAN_DATA.REF_NO_1%TYPE,
                            I_ref_no_2           IN     FM_TRAN_DATA.REF_NO_2%TYPE,
                            I_ref_no_3           IN     FM_TRAN_DATA.REF_NO_3%TYPE,
                            I_ref_no_4           IN     FM_TRAN_DATA.REF_NO_4%TYPE,
                            I_ref_no_5           IN     FM_TRAN_DATA.REF_NO_5%TYPE,
                            I_seq_no             IN OUT FM_GL_CROSS_REF.SEQ_NO%TYPE,
                            I_dynamic_ind        IN     VARCHAR2,
                            I_sob_id             IN     FM_GL_CROSS_REF.SET_OF_BOOKS_ID%TYPE,
                            I_dept               IN     FM_TRAN_DATA.DEPT%TYPE,
                            I_class              IN     FM_TRAN_DATA.CLASS%TYPE,
                            I_subclass           IN     FM_TRAN_DATA.CLASS%TYPE,
                            I_location           IN     FM_TRAN_DATA.LOCATION%TYPE,
                            I_tsf_entity         IN     STORE.TSF_ENTITY_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION GET_COA_SEGMENT_COUNT(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_coa_id           IN OUT FM_COA_SETUP.COA_ID%TYPE)
   return NUMBER;
-----------------------------------------------------------------------------------------------
FUNCTION GET_DB_CR_COUNT(I_seq_no           IN     FM_GL_CROSS_REF.SEQ_NO%TYPE,
                         I_coa_seg_count    IN     NUMBER,
                         I_cr_db_type         IN     VARCHAR2)
   return NUMBER;
-----------------------------------------------------------------------------------------------
FUNCTION GET_ACCOUNT_ID(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                       ,I_seq_no         IN NUMBER)
   return BOOLEAN ;
-----------------------------------------------------------------------------------------------
END FM_FINANCIAL_POSTING_SQL;
/