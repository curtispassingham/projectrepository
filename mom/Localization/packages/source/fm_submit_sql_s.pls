CREATE OR REPLACE PACKAGE FM_SUBMIT_SQL is
--------------------------------------------------------------------------------
-- DESCRIPTION - This package manipulate the form orfm_schedule.fmb 
--               when the option selected is 'SUBMIT'
--------------------------------------------------------------------------------
-- Function Name: GET_NEXT_SEQ_HEADER
-- Purpose:This function fetchs the next sequence code for the header.
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_HEADER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_seq_no         IN OUT FM_RECEIVING_HEADER.SEQ_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_NEXT_SEQ_LINE
-- Purpose:This function fetchs the next sequence code for each line.
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_LINE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_seq_no         IN OUT FM_RECEIVING_DETAIL.SEQ_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_SUBMIT_STATUS
-- Purpose:This function checks the submit status of the schedule
--------------------------------------------------------------------------------
FUNCTION GET_SUBMIT_STATUS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_submit_status IN OUT FM_SCHEDULE.SUBMITTED_IND%TYPE,
                           I_schedule_no   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: UPD_SUBMIT_STATUS
-- Purpose:This function updates the SUBMITTED_IND column when the schedule is submitted
--------------------------------------------------------------------------------
FUNCTION UPD_SUBMIT_STATUS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_submit_status IN     FM_SCHEDULE.SUBMITTED_IND%TYPE,
                           I_schedule_no   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: SUBMIT_SCHED
-- Purpose:       This function updates the SUBMITTED_IND column when the schedule is submitted
--------------------------------------------------------------------------------
FUNCTION SUBMIT_SCHED(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_status         IN OUT FM_SCHEDULE.STATUS%TYPE,
                      I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CLEAR_SCHEDULE_ERRORS
-- Purpose:       This function clears all the errors occuring for the schedule in a particular program
--------------------------------------------------------------------------------
FUNCTION CLEAR_SCHEDULE_ERRORS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                               I_program        IN     VARCHAR2)
return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: WRITE_SCHEDULE_ERRORS
-- Purpose: This function writes all the errors occuring for the schedule in a particular program
--------------------------------------------------------------------------------
FUNCTION WRITE_SCHEDULE_ERRORS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                               I_program        IN     VARCHAR2)
return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: POPULATE_STG_TABLES
-- Purpose: This function populates the staging tables when the submit option 
--          is selected and the schedule has not been submitted before
--------------------------------------------------------------------------------
FUNCTION POPULATE_STG_TABLES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status         IN OUT FM_SCHEDULE.STATUS%TYPE,
                             I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_ITEM_VALUES
-- Purpose: This function calculates the quantity and the item cost based on
--          item type informed.
----------------------------------------------------------------------------------
FUNCTION GET_ITEM_VALUES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_quantity       IN OUT FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE,
                         I_schedule_no    IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                         I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_req_type       IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                         I_req_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                         I_recv_type      IN     VARCHAR2,
                         I_item           IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                         I_unexp_ind      IN     FM_FISCAL_DOC_DETAIL.UNEXPECTED_ITEM%TYPE,
                         I_item_type      IN     VARCHAR2,
                         I_compl_ind      IN     FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: POPULATE
-- Purpose:       This function populates the staging tables
--------------------------------------------------------------------------------
FUNCTION POPULATE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------
END;
/