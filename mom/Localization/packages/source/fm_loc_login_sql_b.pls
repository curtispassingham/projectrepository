CREATE OR REPLACE PACKAGE BODY FM_LOC_LOGIN_SQL is
-------------------------------------------------------------------
FUNCTION LOV_STORE(O_error_message  IN OUT VARCHAR2,
                   O_query          IN OUT VARCHAR2,
                   I_where_clause   IN     VARCHAR2)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'ORFM_LOC_LOGIN_SQL.LOV_STORE';
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'Select store_name, store '||
                   'from v_store '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE  = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(store_name) like UPPER(''%'||I_where_clause||'%'') '||
                  'union '||
                 'Select store_name, store '||
                   'from v_store vs, tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(vs.store_name)  = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and UPPER(store_name) like UPPER(''%'||I_where_clause||'%'') '||
                  'order by store_name ';
   else
      O_query := 'Select store_name, store '||
                   'from v_store '||
                  'union '||
                 'Select store_name, store '||
                   'from v_store vs, tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(vs.store_name)  = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                  'order by store_name ';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOV_STORE;
-----------------------------------------------------------------------------------
FUNCTION LOV_WH(O_error_message  IN OUT VARCHAR2,
                O_query          IN OUT VARCHAR2,
                I_where_clause   IN     VARCHAR2)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'ORFM_LOC_LOGIN_SQL.LOV_WH';
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'Select wh_name, wh '||
                   'from v_wh '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE  = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(wh_name) like UPPER(''%'||I_where_clause||'%'') '||
                    'and wh  = physical_wh '||
                  'union '||
                 'Select NVL(tl_shadow.translated_value, vwh.wh_name) wh_name, vwh.wh wh '||
                   'from v_wh vwh, tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                   'and UPPER(vwh.wh_name)  = tl_shadow.key(+) '||
                   'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and UPPER(NVL(tl_shadow.translated_value, vwh.wh_name)) like UPPER(''%'||I_where_clause||'%'') '||
                    'and vwh.wh  = vwh.physical_wh '||
                  'order by wh_name ';
   else
      O_query := 'Select wh_name, wh '||
                   'from v_wh '||
                   'where wh  = physical_wh '||
                  'union '||
                 'Select NVL(tl_shadow.translated_value, vwh.wh_name) wh_name, vwh.wh wh '||
                   'from v_wh vwh, tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                   'and UPPER(vwh.wh_name)  = tl_shadow.key(+) '||
                   'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                   'and vwh.wh  = vwh.physical_wh '||
                  'order by wh_name ';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOV_WH;
-----------------------------------------------------------------------------------
FUNCTION LOV_MULTI_WH(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2,
                      I_where_clause   IN     VARCHAR2)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'ORFM_LOC_LOGIN_SQL.LOV_MULTI_WH';
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'select distinct vwh.wh_name wh_name, '||
                        'DECODE(vwh.stockholding_ind, ''Y'', c1.code_desc, c2.code_desc) type, '||
                        'vwh.wh wh '||
                   'from v_sec_wh vwh, '||
                        'code_detail c1, '||
                        'code_detail c2  '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE  = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and c1.code_type      = c2.code_type '||
                    'and c1.code_type      = ''LABL'' '||
                    'and c1.code           = ''V'' '||
                    'and c2.code           = ''P'' '||
                    'and UPPER(wh_name) like UPPER(''%'||I_where_clause||'%'') '||
                  'union '||
                 'select distinct NVL(tl_shadow.translated_value, vwh.wh_name) wh_name, '||
                        'DECODE(vwh.stockholding_ind, ''Y'', c1.code_desc, c2.code_desc) type, '||
                        'vwh.wh wh '||
                  'from tl_shadow, '||
                       'v_wh vwh, '||
                       'code_detail c1, '||
                       'code_detail c2  '||
                 'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE  != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                   'and UPPER(vwh.wh_name)  = tl_shadow.key(+) '||
                   'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                   'and UPPER(NVL(tl_shadow.translated_value, vwh.wh_name)) like UPPER(''%'||I_where_clause||'%'') '||
                   'and c1.code_type = c2.code_type '||
                   'and c1.code_type = ''LABL'' '||
                   'and c1.code = ''V'' '||
                   'and c2.code = ''P'' '||
                 'order by 1 ';
   else
      O_query := 'select distinct vwh.wh_name wh_name, '||
                        'DECODE(vwh.stockholding_ind, ''Y'', c1.code_desc, c2.code_desc) type, '||
                        'vwh.wh wh '||
                   'from v_sec_wh vwh, '||
                        'code_detail c1, '||
                        'code_detail c2  '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE  = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and c1.code_type      = c2.code_type '||
                    'and c1.code_type      = ''LABL'' '||
                    'and c1.code           = ''V'' '||
                    'and c2.code           = ''P'' '||
                  'union '||
                 'select distinct NVL(tl_shadow.translated_value, vwh.wh_name) wh_name, '||
                        'DECODE(vwh.stockholding_ind, ''Y'', c1.code_desc, c2.code_desc) type, '||
                        'vwh.wh wh '||
                  'from tl_shadow, '||
                       'v_wh vwh, '||
                       'code_detail c1, '||
                       'code_detail c2  '||
                 'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE  != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                   'and UPPER(vwh.wh_name)  = tl_shadow.key(+) '||
                   'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                   'and c1.code_type = c2.code_type '||
                   'and c1.code_type = ''LABL'' '||
                   'and c1.code = ''V'' '||
                   'and c2.code = ''P'' '||
                 'order by 1 ';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOV_MULTI_WH;
-----------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION VALIDATE_WH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_valid         IN OUT BOOLEAN,
                     O_wh            IN OUT V_WH%ROWTYPE,
                     I_wh            IN     V_WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FM_LOC_LOGIN_SQL.VALIDATE_WH';

   CURSOR C_CHECK_WH_TB IS
      SELECT wh_name
        FROM wh
       WHERE wh = I_wh
         AND wh = physical_wh;


BEGIN
   open C_CHECK_WH_TB;
   fetch C_CHECK_WH_TB into O_wh.wh_name;
   if C_CHECK_WH_TB%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_WH', NULL, NULL, NULL);
      O_valid := FALSE;
   else
      if LANGUAGE_SQL.TRANSLATE(O_wh.wh_name,
                                O_wh.wh_name,
                                O_error_message) = FALSE then
         return FALSE;
      end if;
      O_valid := TRUE;
   end if;
   close C_CHECK_WH_TB;         
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_WH;
-----------------------------------------------------------------------------
END FM_LOC_LOGIN_SQL;
/