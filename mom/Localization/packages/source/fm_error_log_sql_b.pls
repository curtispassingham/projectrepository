create or replace PACKAGE BODY FM_ERROR_LOG_SQL is
   -- Function and procedure implementations
   -----------------------------------------------------------------------------------------
   FUNCTION WRITE_ERROR(O_error_message IN OUT VARCHAR2,
                        I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE,
                        I_program       IN FM_ERROR_LOG.PROGRAM%TYPE,
                        I_function      IN FM_ERROR_LOG.FUNCTION%TYPE,
                       I_err_type      IN FM_ERROR_LOG.ERR_TYPE%TYPE,
                        I_err_code      IN FM_ERROR_LOG.ERR_CODE%TYPE,
                        I_err_msg       IN FM_ERROR_LOG.ERR_MSG%TYPE,
                        I_err_data      IN FM_ERROR_LOG.ERR_DATA%TYPE,
                        I_form_ind      IN NUMBER DEFAULT 1)
      return BOOLEAN is
      L_program           VARCHAR2(255) := 'FM_ERROR_LOG_SQL.WRITE_ERROR';
   -- 001 - Begin  
      L_requisition_type  VARCHAR2(10);
   
   cursor C_GET_REQUISITION is
      select dh.requisition_type   req_type
        from fm_fiscal_doc_header dh
       where dh.fiscal_doc_id = I_fiscal_doc_id;  
       
   Rec_req_type       C_GET_REQUISITION%ROWTYPE;
   -- 001 - End      
   BEGIN
   -- 001 - Begin
   SQL_LIB.SET_MARK('OPEN','C_GET_REQUISITION','FM_FISCAL_DOC_HEADER','fiscal_doc_id: ' || I_fiscal_doc_id);
   open C_GET_REQUISITION;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_REQUISITION','FM_FISCAL_DOC_HEADER','fiscal_doc_id: ' || I_fiscal_doc_id);
   fetch C_GET_REQUISITION into Rec_req_type;
   ---
   L_requisition_type := Rec_req_type.req_type;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_REQUISITION','FM_FISCAL_DOC_HEADER','fiscal_doc_id: ' || I_fiscal_doc_id);
   close C_GET_REQUISITION;
   -- 001 - End   
   if I_form_ind = 1 then 
      if WRITE_ERROR(O_error_message,
                     I_fiscal_doc_id,
                     I_program,
                     I_function,
                     I_err_type,
                     I_err_code,
                     I_err_msg,
                     I_err_data,
                     L_requisition_type) = FALSE then
         return FALSE;
      end if;
    end if;
      --- 
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
         return FALSE;
   END WRITE_ERROR;
   -----------------------------------------------------------------------------------------
   FUNCTION WRITE_ERROR(O_error_message IN OUT VARCHAR2,
                        I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE,
                        I_program       IN FM_ERROR_LOG.PROGRAM%TYPE,
                        I_function      IN FM_ERROR_LOG.FUNCTION%TYPE,
                        I_err_type      IN FM_ERROR_LOG.ERR_TYPE%TYPE,
                        I_err_code      IN FM_ERROR_LOG.ERR_CODE%TYPE,
                        I_err_msg       IN FM_ERROR_LOG.ERR_MSG%TYPE,
                        I_err_data      IN FM_ERROR_LOG.ERR_DATA%TYPE,
                        I_err_source    IN FM_ERROR_LOG.ERR_SOURCE_TYPE%TYPE)
      return BOOLEAN is
      L_program VARCHAR2(255) := 'FM_ERROR_LOG_SQL.WRITE_ERROR';
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      insert into FM_ERROR_LOG
         (PROGRAM,
          FISCAL_DOC_ID,
          ERR_DATE,
          FUNCTION,
          ERR_TYPE,
          ERR_CODE,
          ERR_MSG,
          ERR_DATA,
          ERR_STATUS,
          ERR_SOURCE_TYPE)
      values
         (I_program,
          I_fiscal_doc_id,
          SYSDATE,
          I_function,
          I_err_type,
          I_err_code,
          I_err_msg,
          I_err_data,
          'O',
          I_err_source);
      ---
      commit;
      ---
      return(TRUE);
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
         return FALSE;
   END WRITE_ERROR;
   ------------------------------------------------------------------------------------------
   FUNCTION CLEAR_ERROR(O_error_message IN OUT VARCHAR2,
                        I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE,
                        I_program       IN FM_ERROR_LOG.PROGRAM%TYPE)
      return BOOLEAN is
      L_program VARCHAR2(255) := 'FM_ERROR_LOG_SQL.WRITE_ERROR';
      PRAGMA AUTONOMOUS_TRANSACTION;
      L_closed  VARCHAR2(1) := 'C';
   BEGIN
      update FM_ERROR_LOG
         set err_status = L_closed
       where program = NVL(I_program, program)
         and fiscal_doc_id = NVL(I_fiscal_doc_id, fiscal_doc_id)
         and err_status != L_closed
         and (I_fiscal_doc_id is NOT NULL or -- to guarantee that at least one parameter is informed...
             I_program is NOT NULL);
      ---
      commit;
      ---
      return(TRUE);
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
         return FALSE;
   END CLEAR_ERROR;
   -----------------------------------------------------------------------------------------
   FUNCTION EXISTS_FISCAL(O_error_message IN OUT VARCHAR2,
                          O_exists        IN OUT BOOLEAN,
                          I_fiscal_id     IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_ERROR_LOG_SQL.EXISTS_FISCAL';
      L_dummy   VARCHAR2(1);
      ---
      cursor C_FM_ERR_LOG is
         select '1' from fm_error_log where fiscal_doc_id = I_fiscal_id;
--    ---
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FM_ERR_LOG',
                       'FM_ERROR_LOG',
                       'Fiscal ID: ' || I_fiscal_id);
      open C_FM_ERR_LOG;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FM_ERR_LOG',
                       'FM_ERROR_LOG',
                       'Fiscal ID: ' || I_fiscal_id);
      fetch C_FM_ERR_LOG
         into L_dummy;
      O_exists := C_FM_ERR_LOG%FOUND;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FM_ERR_LOG',
                       'FM_ERROR_LOG',
                       'Fiscal ID: ' || I_fiscal_id);
      close C_FM_ERR_LOG;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
      
   END EXISTS_FISCAL;
   -----------------------------------------------------------------------------------------

   FUNCTION EXISTS_PROGRAM(O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           I_program       IN FM_ERROR_LOG.PROGRAM%TYPE)
      return BOOLEAN is
      ---
      L_program VARCHAR2(50) := 'FM_ERROR_LOG_SQL.EXISTS_PROGRAM';
      L_dummy   VARCHAR2(1);
      ---
      cursor C_FM_ERR_LOG is
         select '1' from fm_error_log where program = I_program;
      ---
   BEGIN
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FM_ERR_LOG',
                       'FM_ERROR_LOG',
                       'Program: ' || SUBSTR(I_program, 1, 50));
      open C_FM_ERR_LOG;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FM_ERR_LOG',
                       'FM_ERROR_LOG',
                       'Program: ' || SUBSTR(I_program, 1, 50));
      fetch C_FM_ERR_LOG
         into L_dummy;
      O_exists := C_FM_ERR_LOG%FOUND;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FM_ERR_LOG',
                       'FM_ERROR_LOG',
                       'Program: ' || SUBSTR(I_program, 1, 50));
      close C_FM_ERR_LOG;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
      
   END EXISTS_PROGRAM;
   -----------------------------------------------------------------------------------------
END FM_ERROR_LOG_SQL;
/