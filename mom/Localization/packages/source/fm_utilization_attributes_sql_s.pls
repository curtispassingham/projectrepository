CREATE OR REPLACE PACKAGE FM_UTILIZATION_ATTRIBUTES_SQL as
---------------------------------------------------------------------------------
-- DESCRIPTION : Package associated to table FM_UTILIZATION_ATTRIBUTES.         
---------------------------------------------------------------------------------
-- Function Name: GET_UTILIZATION_ATTRIB_INFO
-- Purpose:       This function gets the FM_UTILIZATION_ATTRIBUTES informations.
----------------------------------------------------------------------------------
FUNCTION GET_UTILIZATION_ATTRIB_INFO(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_fm_utilization_attributes IN OUT FM_UTILIZATION_ATTRIBUTES%ROWTYPE,
                                     I_utilization_id            IN     FM_UTILIZATION_ATTRIBUTES.UTILIZATION_ID%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_ALLOW_REC_INFO
-- Purpose:       This function gets the allow receiving information.
----------------------------------------------------------------------------------
FUNCTION GET_ALLOW_REC_INFO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_allow_rec       IN OUT FM_UTILIZATION_ATTRIBUTES.ALLOW_RECEIVING_IND%TYPE,
                            I_utilization_id  IN     FM_UTILIZATION_ATTRIBUTES.UTILIZATION_ID%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CHECK_CREATE_UTIL_ATTRIB
-- Purpose:       This function checks if there exists utiization attributes for particular utilization ID.
--                If not there then insert the record into fm_utilization_attributes table.
----------------------------------------------------------------------------------
FUNCTION CHECK_CREATE_UTIL_ATTRIB(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_utilization_id            IN     FM_UTILIZATION_ATTRIBUTES.UTILIZATION_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
END FM_UTILIZATION_ATTRIBUTES_SQL;
/
