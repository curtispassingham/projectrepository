CREATE OR REPLACE PACKAGE BODY FM_EXIT_NF_CREATION_SQL is
   C_NUMBER_TYPE      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE := 'NUMBER';
   C_RTV_TYPE_ID      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_RTV_DOC_TYPE';
   C_STOCK_TYPE_ID    CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_STOCK_DOC_TYPE';
   C_TSF_TYPE_ID      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_OUTBOUND_TSF_DOC_TYPE';
   C_IC_TYPE_ID       CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_OUTBOUND_IC_DOC_TYPE';
   C_REP_TYPE_ID      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_OUTBOUND_REP_DOC_TYPE';
   C_NFE_TYPE_ID      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE := 'DEFAULT_NFE_DOC_TYPE';
--------------------------------------------------------------------------------
FUNCTION GETCNPJ(I_fiscal_doc_id   IN   FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
RETURN VARCHAR2 IS
   L_program        VARCHAR2(255) := 'FM_EXIT_NF_CREATION_SQL.GETCNPJ';

   l_cnpj           VARCHAR2(250);
   L_error_message  VARCHAR2(255) := NULL;
   L_status_code    varchar2(1)   := NULL;
   L_country_id     VARCHAR2(10)  := 'BR';
   L_module         VARCHAR2(10)  := 'LOC';

   cursor C_GET_LOC_SUPP_PTNR is
      select location_type,
             location_id,
             key_value_1,
             key_value_2,
             partner_id,
             module,
             requisition_type
        from fm_fiscal_doc_header
       where fiscal_doc_id = I_fiscal_doc_id;

   r_get_loc_supp_ptnr C_GET_LOC_SUPP_PTNR % ROWTYPE;
   ---
   cursor C_GET_LOC_INFO(p_location_id v_fiscal_attributes.key_value_1%TYPE) is
      select nvl(vfa.cnpj, vfa.cpf) cnpj
        from v_fiscal_attributes vfa,
             country_tax_jurisdiction c,
             country co
       where vfa.MODULE = L_module
         and c.jurisdiction_code = vfa.city
         and co.country_id = vfa.country_id
         and vfa.country_id = c.country_id
         and vfa.state = c.state
         and co.country_id = L_country_id
         and key_value_1 = p_location_id;
   ---
   cursor C_GET_SUPP_INFO(p_supplier fm_fiscal_doc_header.key_value_1%TYPE) is
      select nvl(vsrn.cnpj,   vsrn.cpf) cnpj
        from v_br_sups_fiscal_addr vsfa,
             v_br_sups_reg_num vsrn,
             sups s,
             country_tax_jurisdiction c,
             country co
       where vsfa.supplier = vsrn.supplier
         and s.supplier = vsrn.supplier
         and vsfa.city = c.jurisdiction_code
         and vsfa.country = c.country_id
         and vsfa.state = c.state
         and vsfa.country = co.country_id
         and co.country_id = L_country_id
         and s.supplier = p_supplier;
   ---
   cursor C_GET_CUST_INFO(p_cust_id fm_fiscal_doc_header.key_value_1%TYPE) is
      select nvl(frh.cnpj, frh.cpf) cnpj
        from fm_rma_head frh,
             country_tax_jurisdiction c
       where frh.fiscal_doc_id = I_fiscal_doc_id
         and frh.city = c.jurisdiction_code
         and frh.country_id = c.country_id
         and frh.state = c.state
         and frh.cust_id = p_cust_id;
   ---
BEGIN

   open C_GET_LOC_SUPP_PTNR;
   fetch C_GET_LOC_SUPP_PTNR into r_get_loc_supp_ptnr;
   close C_GET_LOC_SUPP_PTNR;

   if r_get_loc_supp_ptnr.requisition_type IN ('RTV','TSF','STOCK','IC','REP') then
      open C_GET_LOC_INFO(r_get_loc_supp_ptnr.location_id);
      fetch C_GET_LOC_INFO into l_cnpj;
   elsif r_get_loc_supp_ptnr.requisition_type = 'RPO' then
      open C_GET_SUPP_INFO(r_get_loc_supp_ptnr.key_value_1);
      fetch C_GET_SUPP_INFO into l_cnpj;
   elsif r_get_loc_supp_ptnr.requisition_type = 'RMA' then
      open C_GET_CUST_INFO(r_get_loc_supp_ptnr.key_value_1);
      fetch C_GET_CUST_INFO into l_cnpj;
   end if;

   return l_cnpj;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FM_EXIT_NF_CREATION_SQL.GETCNPJ',
                                            TO_CHAR(SQLCODE));
      if FM_ERROR_LOG_SQL.WRITE_ERROR(L_error_message,
                                      I_fiscal_doc_id,
                                      L_program,
                                      L_program,
                                      'E',
                                      'NF_PROCESSING_ERROR',
                                      L_error_message,
                                      'Fiscal_doc_id: '||
                                      TO_CHAR(I_fiscal_doc_id)) = FALSE then
         return 0;
      end if;
END GETCNPJ;
--------------------------------------------------------------------------------
FUNCTION GET_UNIT_COST (O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_unit_cost              IN OUT FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                        O_fiscal_doc_line_id_ref IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID_REF%TYPE,
                        O_fiscal_doc_id_ref      IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID_REF%TYPE,
                        I_location_type          IN     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                        I_location_id            IN     FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                        I_item                   IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                        I_comp_item              IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                        I_pack_level             IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                        I_requisition_type       IN     FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
RETURN BOOLEAN IS
   ---
   L_program             VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.GET_UNIT_COST';
   L_table               VARCHAR2(150);
   L_key                 VARCHAR2(100);
   L_dummy               VARCHAR2(1);
   L_cost_discrep_status FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE;
   L_resolution_type     FM_RESOLUTION.RESOLUTION_TYPE%TYPE;
   L_item                FM_FISCAL_DOC_DETAIL.ITEM%TYPE;
   L_no                  VARCHAR2(1)                        := 'N';
   L_cost_discrep_type   FM_RESOLUTION.DISCREP_TYPE%TYPE    := 'C';
   L_cost_action_type    FM_CORRECTION_DOC.ACTION_TYPE%TYPE := 'CLC';
   ---
   C_PO                  CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE :='PO';
   C_ENTRY_MODE          CONSTANT FM_SCHEDULE.MODE_TYPE%TYPE := 'ENT';
   ---
   cursor C_GET_AVG_COST(P_location_type  FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                         P_location_id    FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                         P_item           FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select distinct ils.av_cost
        from item_loc_soh ils,
             wh wh
       where ils.item       = P_item
         and ((ils.loc_type = P_location_type
         and ils.loc        = P_location_id)
          or (ils.loc_type  = P_location_type
         and ils.loc        = wh.wh
         and wh.physical_wh = P_location_id
         and wh.wh         <> wh.physical_wh));
   ---
   cursor C_GET_UNIT_COST (P_location_type     FM_FISCAL_DOC_DETAIL.LOCATION_TYPE%TYPE,
                           P_location_id       FM_FISCAL_DOC_DETAIL.LOCATION_ID%TYPE,
                           P_item              FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select unit_cost_with_disc,
             fiscal_doc_line_id,
             fiscal_doc_id,
             utilization_id
       from (select fdd.unit_cost_with_disc,
                    fdd.fiscal_doc_line_id,
                    fdd.fiscal_doc_id,
                    fdh.utilization_id
               from fm_fiscal_doc_header fdh,
                    fm_fiscal_doc_detail fdd,
                    fm_schedule fs,
                    fm_utilization_attributes fua,
                    fm_fiscal_utilization ffu
              where fdh.fiscal_doc_id        = fdd.fiscal_doc_id
                and fdh.schedule_no          = fs.schedule_no
                and fdh.utilization_id       = fua.utilization_id
                and fdh.utilization_id       = ffu.utilization_id
                and fdd.location_type        = P_location_type
                and fdd.location_id          = P_location_id
                and ((I_pack_level is NULL
                       and fdd.item   = P_item)
                      or (I_pack_level is NOT NULL
                     and fdd.pack_ind = L_no
                     and fdd.pack_no  = I_item
                     and fdd.item     = I_comp_item))
                and fdh.status             in (C_APPROVED_STATUS, C_COMPLETED_STATUS)
                and fdh.requisition_type     = C_PO
                and fs.status              in (C_APPROVED_STATUS, C_COMPLETED_STATUS)
                and fs.mode_type             = C_ENTRY_MODE
                and fua.comp_nf_ind          = L_no
                and ffu.status               = C_APPROVED_STATUS
                order by fdh.entry_or_exit_date desc,
                         fdh.fiscal_doc_id desc)
      where rownum < 2;
   ---
   R_get_unit_cost C_GET_UNIT_COST%ROWTYPE;
   ---
   cursor C_CHK_COST_DISCREP_STATUS(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                    P_fiscal_doc_id      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select fdd.cost_discrep_status,
             fr.resolution_type
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             fm_resolution fr
       where fdh.fiscal_doc_id      = P_fiscal_doc_id
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id
         and fdd.fiscal_doc_id      = fdh.fiscal_doc_id
         and fr.fiscal_doc_line_id  = fdd.fiscal_doc_line_id
         and fr.discrep_type        = L_cost_discrep_type;
   ---
   cursor C_GET_CORRECTED_COST(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                               P_fiscal_doc_id      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
   select fcd.action_value
     from fm_correction_doc fcd,
          fm_fiscal_doc_detail fdd,
          fm_fiscal_doc_header fdh
    where fdh.fiscal_doc_id      = P_fiscal_doc_id
      and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id
      and fdd.fiscal_doc_id      = fdh.fiscal_doc_id
      and fcd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
      and fcd.action_type        = L_cost_action_type;
   ---
   BEGIN

      O_unit_cost              := NULL;
      O_fiscal_doc_line_id_ref := NULL;
      O_fiscal_doc_id_ref      := NULL;

      if I_requisition_type ='RTV' then
         L_table := 'FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL/FM_SCHEDULE/FM_UTILIZATION_ATTRIBUTES/FM_FISCAL_UTILIZATION';
         L_key   := 'location_type = '||I_location_type||
                    'location_id = '  ||I_location_id||
                    'item = '         ||I_item;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_UNIT_COST',L_table,L_key);
         open C_GET_UNIT_COST(I_location_type,
                              I_location_id,
                              I_item);
         ---
         SQL_LIB.SET_MARK('FETCH','C_GET_UNIT_COST',L_table,L_key);
         fetch C_GET_UNIT_COST into R_get_unit_cost;
         ---
         O_unit_cost               := R_get_unit_cost.unit_cost_with_disc;
         O_fiscal_doc_line_id_ref  := R_get_unit_cost.fiscal_doc_line_id;
         O_fiscal_doc_id_ref       := R_get_unit_cost.fiscal_doc_id;

         SQL_LIB.SET_MARK('CLOSE','C_GET_UNIT_COST',L_table,L_key);
         close C_GET_UNIT_COST;
         ---
         if O_fiscal_doc_id_ref is not null then
            L_table := 'FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL/FM_RESOLUTION/FM_CORRECTION_DOC';
            L_key   := 'fiscal_doc_line_id = '||O_fiscal_doc_line_id_ref||
                       'fiscal_doc_id = '     ||O_fiscal_doc_id_ref;
            ---
            SQL_LIB.SET_MARK('OPEN','C_CHK_COST_DISCREP_STATUS',L_table,L_key);
            open C_CHK_COST_DISCREP_STATUS(O_fiscal_doc_line_id_ref,
                                           O_fiscal_doc_id_ref);
            ---
            SQL_LIB.SET_MARK('FETCH','C_CHK_COST_DISCREP_STATUS',L_table,L_key);
            fetch C_CHK_COST_DISCREP_STATUS into L_cost_discrep_status,L_resolution_type;
            ---
            if L_cost_discrep_status='R' AND L_resolution_type='SYS' then
               SQL_LIB.SET_MARK('OPEN','C_GET_CORRECTED_COST',L_table,L_key);
               open C_GET_CORRECTED_COST(O_fiscal_doc_line_id_ref,
                                         O_fiscal_doc_id_ref);
               ---
               SQL_LIB.SET_MARK('FETCH','C_GET_CORRECTED_COST',L_table,L_key);
               fetch C_GET_CORRECTED_COST INTO O_unit_cost;
               ---
               SQL_LIB.SET_MARK('CLOSE','C_GET_CORRECTED_COST',L_table,L_key);
               close C_GET_CORRECTED_COST;
               ---
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE','C_CHK_COST_DISCREP_STATUS',L_table,L_key);
            close C_CHK_COST_DISCREP_STATUS;
         end if;
         ---
      end if;
      ---
      if O_unit_cost is null then
         if I_pack_level is not NULL then
            L_item := I_comp_item;
         else
            L_item := I_item;
         end if;
         ---
         L_table := 'ITEM_LOC_SOH/WH';
         L_key   := 'location_type = '||I_location_type||
                    'location_id = '  ||I_location_id||
                    'item = '         ||L_item;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_AVG_COST',L_table,L_key);
         open C_GET_AVG_COST(I_location_type,
                             I_location_id,
                             L_item);
         ---
         SQL_LIB.SET_MARK('FETCH','C_GET_AVG_COST',L_table,L_key);
         fetch C_GET_AVG_COST into O_unit_cost;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_AVG_COST',L_table,L_key);
         close C_GET_AVG_COST;
         ---
      end if;
      ---
   return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
         return FALSE;
END GET_UNIT_COST;
--------------------------------------------------------------------------------
FUNCTION GET_TYPE_ID(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     L_type_id          OUT FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE,
                     I_location         IN  FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                     I_utilization      IN  FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                     I_requisition_type IN  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE)
RETURN BOOLEAN IS
   ---
   L_program          VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.GET_TYPE_ID';
   L_table            VARCHAR2(150) := 'FM_NFE_CONFIG_LOC';
   L_key              VARCHAR2(100) := 'location ='       ||I_location||
                                       'utilization_id =' ||I_utilization;
   L_dummy            VARCHAR2(1);
   L_default_doc_type FM_SYSTEM_OPTIONS.VARIABLE%TYPE;
   L_dummy_desc       FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date       FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_dummy_number     FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_dummy_string     FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   ---
   cursor C_CHECK_NFE_CONFIG_LOC(P_Location    FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                                 P_utilization FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE) is
      select '1'
        from fm_nfe_config_loc
       where location       = P_location
         and utilization_id = P_utilization;
   ---
   BEGIN
      SQL_LIB.SET_MARK('OPEN','C_CHECK_NFE_CONFIG_LOC',L_table,L_key);
      open C_CHECK_NFE_CONFIG_LOC(I_location,
                                  I_utilization);
      ---
      SQL_LIB.SET_MARK('FETCH','C_CHECK_NFE_CONFIG_LOC',L_table,L_key);
      fetch C_CHECK_NFE_CONFIG_LOC into L_dummy;
      ---
      if C_CHECK_NFE_CONFIG_LOC%FOUND then
         ---Get the default document type
         if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                               L_dummy_desc,
                                               L_type_id,
                                               L_dummy_string,
                                               L_dummy_date,
                                               C_NUMBER_TYPE,
                                               C_NFE_TYPE_ID) = FALSE then
            return FALSE;
         end if;
         ---
      else
         if I_requisition_type ='RTV' then
            L_default_doc_type := C_RTV_TYPE_ID;
         elsif I_requisition_type ='STOCK' then
            L_default_doc_type := C_STOCK_TYPE_ID;
         elsif I_requisition_type = 'TSF' then
            L_default_doc_type := C_TSF_TYPE_ID;
         elsif I_requisition_type = 'IC' then
            L_default_doc_type := C_IC_TYPE_ID;
         elsif I_requisition_type = 'REP' then
            L_default_doc_type := C_REP_TYPE_ID;
         end if;
         ---
         if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                               L_dummy_desc,
                                               L_type_id,
                                               L_dummy_string,
                                               L_dummy_date,
                                               C_NUMBER_TYPE,
                                               L_default_doc_type) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_NFE_CONFIG_LOC',L_table,L_key);
      close C_CHECK_NFE_CONFIG_LOC;
      ---
   return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
         return FALSE;
END GET_TYPE_ID;
--------------------------------------------------------------------------------
FUNCTION CALC_EBC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_ebc_cost             IN OUT   FM_STG_RTV_DTL.EBC_COST%TYPE,
                  O_base_cost            IN OUT   FM_FISCAL_DOC_DETAIL.BASE_COST%TYPE,
                  I_nic_cost             IN       FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                  I_fiscal_doc_line_id   IN       FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
RETURN BOOLEAN IS
   ---
   L_program                 VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.CALC_EBC';
   L_table                   VARCHAR2(150) := 'FM_FISCAL_DOC_TAX_DETAIL/VAT_CODES';
   L_key                     VARCHAR2(100) := 'fiscal_doc_line_id = '||I_fiscal_doc_line_id;
   ---
   L_base_cost               FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_ebc_cost                FM_STG_RTV_DTL.EBC_COST%TYPE;
   L_bc_tax_amt              FM_FISCAL_DOC_TAX_DETAIL.UNIT_TAX_AMT%TYPE   := 0;
   L_total_rec_amt           FM_FISCAL_DOC_TAX_DETAIL.UNIT_REC_VALUE%TYPE := 0;
   L_total_tax_amt           FM_FISCAL_DOC_TAX_DETAIL.UNIT_TAX_AMT%TYPE   := 0;
   ---
   cursor C_GET_TAX_DETAIL is
      select fdtd.vat_code,
             fdtd.total_value as tax_value,
             NVL(fdtd.unit_tax_amt,0) as unit_tax_amt,
             NVL(fdtd.unit_rec_value,0) as unit_rec_value,
             vc.incl_nic_ind as nic_ind
        from fm_fiscal_doc_tax_detail fdtd,
             vat_codes vc
       where fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id
         and fdtd.vat_code           = vc.vat_code;
   ---
   R_get_tax_detail     C_GET_TAX_DETAIL%ROWTYPE;
   ---
   BEGIN
   ---
      SQL_LIB.SET_MARK('OPEN','C_GET_TAX_DETAIL',L_table,L_key);
      open C_GET_TAX_DETAIL;
      loop
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_TAX_DETAIL',L_table,L_key);
      fetch C_GET_TAX_DETAIL into R_get_tax_detail;
      exit when C_GET_TAX_DETAIL%NOTFOUND;
      ---
      if R_get_tax_detail.nic_ind ='Y' then
         L_bc_tax_amt := L_bc_tax_amt + R_get_tax_detail.unit_tax_amt;
      end if;
      ---
      L_total_tax_amt   := L_total_tax_amt + R_get_tax_detail.unit_tax_amt;
      L_total_rec_amt   := L_total_rec_amt + R_get_tax_detail.unit_rec_value;
      ---
      end loop;
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_TAX_DETAIL',L_table,L_key);
      close  C_GET_TAX_DETAIL;
      ---
      L_base_cost := I_nic_cost  - L_bc_tax_amt;
      L_ebc_cost  := L_base_cost + (L_total_tax_amt - L_total_rec_amt);
      ---
      O_base_cost := L_base_cost;
      O_ebc_cost  := L_ebc_cost;
      ---
   return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
            return FALSE;
      ---
END CALC_EBC;
--------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC_ID_STG_TB(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
RETURN BOOLEAN IS
   ---
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program          VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.UPDATE_FISCAL_DOC_ID_STG_TB';
   L_table            VARCHAR2(150);
   L_key              VARCHAR2(100);
   ---
   L_old_reqno_stock  FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
   L_old_reqno_tsf    FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
   L_old_reqno_rtv    FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
   L_old_reqno_rma    FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
   L_pack_item        FM_FISCAL_DOC_DETAIL.ITEM%TYPE;
   L_item             FM_FISCAL_DOC_DETAIL.ITEM%TYPE;
   L_status           FM_STG_RTV_DESC.STATUS%TYPE;
   L_ebc_cost         FM_STG_RTV_DTL.EBC_COST%TYPE := NULL;
   L_base_cost        FM_STG_ASNOUT_ITEM.BASE_COST%TYPE :=NULL;
   ---
   L_total_chrgs_prim         item_loc.unit_retail%TYPE := 0;
   L_profit_chrgs_to_loc      item_loc.unit_retail%TYPE := 0;
   L_exp_chrgs_to_loc         item_loc.unit_retail%TYPE := 0;
   L_distro_doc_type          FM_STG_ASNOUT_DISTRO.DISTRO_NBR%TYPE:=NULL;
   L_stock            VARCHAR2(10) := 'STOCK';
   L_rtv              VARCHAR2(10) :=  'RTV';
   L_tsf              VARCHAR2(10) :=  'TSF';
   L_ic               VARCHAR2(10) :=  'IC';
   L_rep              VARCHAR2(10) :=  'REP';
   L_rma              VARCHAR2(10) :=  'RMA';
   L_pck_ind          VARCHAR2(1)  :=  'N';


   ---
   cursor C_GET_FISCAL_DOC_ID is
      select fdh.fiscal_doc_id,
             fdh.requisition_type,
             fdh.schedule_no,
             fdd.requisition_no,
             fdd.location_type,
             fdd.location_id,
             fdd.item,
             fdd.unit_cost,
             fdd.quantity,
             fdd.pack_ind,
             fdd.fiscal_doc_line_id,
             fdd.fiscal_doc_line_id_ref
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             fm_schedule fs
       where fdh.fiscal_doc_id    = fdd.fiscal_doc_id
         and fdh.schedule_no      = fs.schedule_no
         and fdh.requisition_type IN (L_stock,L_rtv,L_tsf,L_ic,L_rep,L_rma)
         and fdh.schedule_no         = I_schedule_no
         order by fdh.fiscal_doc_id;
   ---
   R_get_fiscal_doc_id C_GET_FISCAL_DOC_ID%ROWTYPE;
   ---
   cursor C_GET_COST is
      select fdd.fiscal_doc_id,
             fdh.requisition_type,
             fdd.requisition_no,
             fdd.pack_no as item,
             sum(fdd.unit_cost) as NIC,
             sum(NVL(fdd.base_cost,0)) as base_cost,
             sum(NVL(fdd.net_cost,0)) as ebc_cost
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             fm_schedule fs
       where fdh.fiscal_doc_id    = fdd.fiscal_doc_id
         and fdh.schedule_no      = fs.schedule_no
         and fdh.requisition_type IN (L_stock,L_rtv,L_tsf,L_ic,L_rep,L_rma)
         and fdh.schedule_no       = I_schedule_no
         and fdd.pack_no is NOT NULL and fdd.pack_ind = L_pck_ind
    group by fdd.pack_no,
             fdd.fiscal_doc_id,
             fdh.requisition_type,
             fdd.requisition_no
      union
      select fdd.fiscal_doc_id,
             fdh.requisition_type,
             fdd.requisition_no,
             fdd.item,
             fdd.unit_cost as NIC,
             fdd.base_cost as base_cost,
             fdd.net_cost as ebc_cost
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh,
             fm_schedule fs
       where fdh.fiscal_doc_id    = fdd.fiscal_doc_id
         and fdh.schedule_no      = fs.schedule_no
         and fdh.requisition_type IN (L_stock,L_rtv,L_tsf,L_ic,L_rep,L_rma)
         and fdh.schedule_no      = I_schedule_no
         and fdd.pack_no is NULL;
   ---
   R_get_cost C_GET_COST%ROWTYPE;
   ---
   cursor C_GET_TSF_LOCS is
      select t.from_loc_type,
             t.from_loc,
             t.to_loc_type,
             t.to_loc,
             d.tsf_seq_no
        from tsfhead t,
             tsfdetail d,
             fm_fiscal_doc_detail fdd
       where t.tsf_no               = d.tsf_no
         and t.tsf_no               = fdd.requisition_no
         and fdd.fiscal_doc_line_id = R_get_fiscal_doc_id.fiscal_doc_line_id
      union
      select 'W',
             ah.wh,
             ad.to_loc_type,
             ad.to_loc,
             null
        from alloc_header ah,
             alloc_detail ad,
             fm_fiscal_doc_detail fdd
       where ah.alloc_no = ad.alloc_no
         and ah.alloc_no = fdd.requisition_no
         and fdd.fiscal_doc_line_id = R_get_fiscal_doc_id.fiscal_doc_line_id;

   ---
   R_get_tsf_details C_GET_TSF_LOCS%ROWTYPE;
   ---
   cursor C_NF_DETAIL_PACK_ITEMS(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select distinct pack_no
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = P_fiscal_doc_id
         and fdd.pack_no is not null;
   ---
   cursor C_LOCK_STG_ADJ_DESC (P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select 1
        from fm_stg_adjust_desc fsad
       where fsad.seq_no = P_requisition_no
         for update nowait;
   ---
   cursor C_LOCK_STG_ADJ_DTL (P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                              P_item           FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select 1
        from fm_stg_adjust_dtl fsad
       where fsad.desc_seq_no  = P_requisition_no
         and fsad.item_id      = P_item
         for update nowait;
   ---
   cursor C_LOCK_STG_RTV_DESC (P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select 1
        from fm_stg_rtv_desc fsrd
       where fsrd.rtv_id     = P_requisition_no
         for update nowait;
   ---
   cursor C_LOCK_STG_RTV_DTL (P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                              P_item           FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select 1
        from fm_stg_rtv_dtl dtl
       where dtl.desc_seq_no IN (select hdr.seq_no
                                   from fm_stg_rtv_desc hdr
                                  where hdr.rtv_id = P_requisition_no)
         and dtl.item_id  = P_item
         for update nowait;
   ---
   cursor C_LOCK_STG_ASNOUT_DESC (P_requisition_no FM_STG_ASNOUT_DISTRO.DISTRO_NBR%TYPE) is
      select 1
        from fm_stg_asnout_desc fsah
       where fsah.seq_no     IN (select max(fsad.desc_seq_no)
                                   from fm_stg_asnout_distro fsad
                                  where fsad.distro_nbr = P_requisition_no)
         for update nowait;
   ---
   cursor C_LOCK_STG_ASNOUT_ITEM (P_requisition_no FM_STG_ASNOUT_DISTRO.DISTRO_NBR%TYPE,
                                  P_item           FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select 1
       from fm_stg_asnout_item fsai
      where fsai.distro_seq_no IN (select max(fsad.seq_no)
                                     from fm_stg_asnout_distro fsad
                                    where fsad.distro_nbr = P_requisition_no)
        and fsai.item_id  = P_item
        for update nowait;
   ---
   cursor C_LOCK_RMA_HEAD (P_requisition_no FM_RMA_HEAD.RMA_ID%TYPE) is
      select 1
        from fm_rma_head
       where rma_id = P_requisition_no
         for update nowait;
   ---
   cursor C_GET_PACK_ITEM (P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                           P_item           FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select dtl.item_id
        from fm_stg_rtv_desc hdr , packitem pi, fm_stg_rtv_dtl dtl
       where hdr.seq_no=dtl.desc_seq_no
         and dtl.item_id = pi.pack_no
         and hdr.rtv_id  = P_requisition_no
         and pi.item = P_item;
   ---
   cursor C_CHECK_STG_RTV_STATUS (P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select fsrd.status
        from fm_stg_rtv_desc fsrd
       where fsrd.rtv_id     = P_requisition_no;
   ---
   cursor C_LOCK_DOC_HEADER(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = P_fiscal_doc_id
         for update nowait;
   ---
   cursor C_LOCK_DOC_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = P_fiscal_doc_line_id
         for update nowait;
   ---
   cursor C_GET_DOC_TYPE(P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select distro.distro_doc_type
        from fm_stg_asnout_distro distro
       where distro.distro_nbr = P_requisition_no;
   ---
BEGIN

   L_table := 'FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL/FM_SCHEDULE';
   L_key   := 'schedule_no = '||I_schedule_no;
   SQL_LIB.SET_MARK('OPEN','C_GET_FISCAL_DOC_ID',L_table,L_key);
   open C_GET_FISCAL_DOC_ID;
   ---
   LOOP
      SQL_LIB.SET_MARK('FETCH','C_GET_FISCAL_DOC_ID',L_table,L_key);
      fetch C_GET_FISCAL_DOC_ID into R_get_fiscal_doc_id;
      EXIT when C_GET_FISCAL_DOC_ID%NOTFOUND;
      ---
      if (FM_EXIT_NF_CREATION_SQL.CALC_EBC(O_error_message,
                                           L_ebc_cost,
                                           L_base_cost,
                                           R_get_fiscal_doc_id.unit_cost,
                                           R_get_fiscal_doc_id.fiscal_doc_line_id)= FALSE) then
         return FALSE;
      end if;
      ---
      if R_get_fiscal_doc_id.requisition_type in ('TSF','IC','REP') then
         SQL_LIB.SET_MARK('OPEN','C_GET_TSF_LOCS','TSFHEAD/TSFDETAIL/FM_FISCAL_DOC_DETAIL','Fiscal_doc_line_id: ' || TO_CHAR(R_get_fiscal_doc_id.fiscal_doc_line_id));
         open C_GET_TSF_LOCS;
         ---
         SQL_LIB.SET_MARK('FETCH','C_GET_TSF_LOCS','TSFHEAD/TSFDETAIL/FM_FISCAL_DOC_DETAIL','Fiscal_doc_line_id: ' || TO_CHAR(R_get_fiscal_doc_id.fiscal_doc_line_id));
         fetch C_GET_TSF_LOCS into R_get_tsf_details;
         ---
          open C_GET_DOC_TYPE(R_get_fiscal_doc_id.requisition_no);
         fetch C_GET_DOC_TYPE into L_distro_doc_type;
         close C_GET_DOC_TYPE;

         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_total_chrgs_prim,
                                                        L_profit_chrgs_to_loc,
                                                        L_exp_chrgs_to_loc,
                                                        L_distro_doc_type,
                                                        R_get_fiscal_doc_id.requisition_no,
                                                        R_get_tsf_details.tsf_seq_no,
                                                        NULL,
                                                        NULL,
                                                        R_get_fiscal_doc_id.item,
                                                        NULL,
                                                        R_get_tsf_details.from_loc,
                                                        R_get_tsf_details.from_loc_type,
                                                        R_get_tsf_details.to_loc,
                                                        R_get_tsf_details.to_loc_type) = FALSE then
            return FALSE;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_LOCS','TSFHEAD/TSFDETAIL/FM_FISCAL_DOC_DETAIL','Fiscal_doc_line_id: ' || TO_CHAR(R_get_fiscal_doc_id.fiscal_doc_line_id));
         close C_GET_TSF_LOCS;
         ---
         L_ebc_cost  := L_ebc_cost  - L_total_chrgs_prim;
         L_base_cost := L_base_cost - L_total_chrgs_prim;
         ---
      end if;
      ---
      L_table  := 'FM_FISCAL_DOC_DETAIL';
      L_key    := 'Fiscal_doc_line_id: ' || TO_CHAR(R_get_fiscal_doc_id.fiscal_doc_line_id);

      SQL_LIB.SET_MARK('OPEN','C_LOCK_DOC_DETAIL',L_table,L_key);
      open C_LOCK_DOC_DETAIL(R_get_fiscal_doc_id.fiscal_doc_line_id);
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_DETAIL', L_table, L_key);
      close C_LOCK_DOC_DETAIL;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
      ---
      update fm_fiscal_doc_detail fdd
         set fdd.net_cost  = L_ebc_cost
            ,fdd.base_cost = L_base_cost
       where fdd.fiscal_doc_line_id = R_get_fiscal_doc_id.fiscal_doc_line_id;
      ---
   END LOOP;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ID', 'FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL/FM_SCHEDULE', 'schedule_no = '||I_schedule_no);
   close C_GET_FISCAL_DOC_ID;
   ---
   L_table := 'FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL/FM_SCHEDULE';
   L_key   := 'schedule_no = '||I_schedule_no;

   SQL_LIB.SET_MARK('OPEN','C_GET_COST',L_table,L_key);
   open C_GET_COST;
   ---
   LOOP
   SQL_LIB.SET_MARK('FETCH','C_GET_COST',L_table,L_key);
   fetch C_GET_COST into R_get_cost;
   EXIT when C_GET_COST%NOTFOUND;
   ---
   -- Update fm_stg_adjust_desc and fm_stg_adjust_dtl tables
   if R_get_cost.requisition_type = 'STOCK' then
      ---
      L_old_reqno_stock := R_get_fiscal_doc_id.requisition_no;
      -- Update fm_stg_adjust_dtl. fields: unit_cost, net_cost, gross_cost and
      -- unit_qty (this field always will be negative.).
      L_table := 'FM_STG_ADJUST_DTL';
      L_key   := 'desc_seq_no = '||R_get_cost.requisition_no||
                 'item = '||R_get_cost.item;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ADJ_DTL',L_table,L_key);
      open C_LOCK_STG_ADJ_DTL (R_get_cost.requisition_no,R_get_cost.item);
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ADJ_DTL',L_table,L_key);
      close C_LOCK_STG_ADJ_DTL;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,L_table,L_key);
      ---
      update fm_stg_adjust_dtl fsad
           set fsad.ebc_cost     = R_get_cost.ebc_cost
       where fsad.desc_seq_no  = R_get_cost.requisition_no
         and fsad.item_id      = R_get_cost.item;

      -- Update fm_stg_rtv_desc table
   elsif R_get_fiscal_doc_id.requisition_type = 'RTV' then
      ---
      L_old_reqno_rtv := R_get_cost.requisition_no;
      L_table := 'FM_STG_RTV_DTL';
      L_key   := 'rtv_id = '||R_get_cost.requisition_no||
                 'item = '||R_get_cost.item;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_RTV_DESC',L_table,L_key);
      open C_LOCK_STG_RTV_DESC (R_get_cost.requisition_no);
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_RTV_DESC',L_table,L_key);
      close C_LOCK_STG_RTV_DESC;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,L_table,L_key);
      ---
      update fm_stg_rtv_dtl dtl
         set dtl.ebc_cost      = R_get_cost.ebc_cost,
             dtl.nic_cost      = R_get_cost.NIC
       where dtl.desc_seq_no   IN (select hdr.seq_no
                                     from fm_stg_rtv_desc hdr
                                    where hdr.rtv_id = R_get_cost.requisition_no)
         and dtl.item_id  = R_get_cost.item;

   -- Update fm_stg_asnout_desc and fm_stg_asnout_item tables

   elsif R_get_fiscal_doc_id.requisition_type in ('TSF','IC','REP') then
   -- Update fm_stg_asnout_desc
      if NVL(L_old_reqno_tsf,-1) != R_get_cost.requisition_no then
      ---
         ---
         L_table := 'FM_FISCAL_DOC_HEADER';
         L_key   := 'fiscal_doc_id = '||R_get_cost.fiscal_doc_id;

         SQL_LIB.SET_MARK('OPEN','C_LOCK_DOC_HEADER',L_table,L_key);
         open C_LOCK_DOC_HEADER (R_get_cost.fiscal_doc_id);
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_DOC_HEADER',L_table,L_key);
         close C_LOCK_DOC_HEADER;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,L_table,L_key);
         ---
         update fm_fiscal_doc_header fdh
            set fdh.ref_no_1 = (select fsad.asn_nbr
                                  from fm_stg_asnout_desc fsad
                                 where fsad.fiscal_doc_id = R_get_fiscal_doc_id.fiscal_doc_id)
           where fdh.fiscal_doc_id = R_get_cost.fiscal_doc_id;
      end if;
      ---
      L_old_reqno_tsf := R_get_cost.requisition_no;

      -- Update fm_stg_asnout_item

      L_table := 'FM_STG_ASNOUT_ITEM';
      L_key   := 'distro_nbr = '||R_get_cost.requisition_no||
                 'item = '||R_get_cost.item;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ASNOUT_ITEM',L_table,L_key);
      open C_LOCK_STG_ASNOUT_ITEM (R_get_cost.requisition_no,R_get_cost.item);
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ASNOUT_ITEM',L_table,L_key);
      close C_LOCK_STG_ASNOUT_ITEM;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,L_table,L_key);
      ---
      update fm_stg_asnout_item fsai
         set fsai.ebc_cost     = R_get_cost.ebc_cost
            ,fsai.base_cost    = R_get_cost.base_cost
            ,fsai.NIC_cost     = R_get_cost.NIC
       where fsai.distro_seq_no IN (select max(fsad.seq_no)
                                      from fm_stg_asnout_distro fsad
                                     where fsad.distro_nbr = TO_CHAR(R_get_cost.requisition_no))
         and fsai.item_id  = R_get_cost.item;
   end if;
   ---
   END LOOP;
   ---
   L_table := 'FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL/FM_SCHEDULE';
   L_key   := 'schedule_no = '||I_schedule_no;
   SQL_LIB.SET_MARK('CLOSE','C_GET_COST',L_table,L_key);
   close C_GET_COST;
   ---
   return TRUE;
   ---
EXCEPTION

   when RECORD_LOCKED then
   -- Close all cursors
      if C_GET_FISCAL_DOC_ID%ISOPEN then
         close C_GET_FISCAL_DOC_ID;
      end if;

      if C_LOCK_STG_ADJ_DESC%ISOPEN then
         close C_LOCK_STG_ADJ_DESC;
      end if;

      if C_LOCK_STG_ADJ_DTL%ISOPEN then
         close C_LOCK_STG_ADJ_DTL;
      end if;

      if C_LOCK_STG_RTV_DESC%ISOPEN then
         close C_LOCK_STG_RTV_DESC;
      end if;

      if C_LOCK_STG_ASNOUT_DESC%ISOPEN then
         close C_LOCK_STG_ASNOUT_DESC;
      end if;

      if C_LOCK_STG_ASNOUT_ITEM%ISOPEN then
         close C_LOCK_STG_ASNOUT_ITEM;
      end if;

      if C_LOCK_RMA_HEAD%ISOPEN then
         close C_LOCK_RMA_HEAD;
      end if;

      if C_LOCK_DOC_DETAIL%ISOPEN then
         close C_LOCK_DOC_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            L_key,
                                            NULL);
      return FALSE;

   when OTHERS then
   -- Close all cursors
      if C_GET_FISCAL_DOC_ID%ISOPEN then
         close C_GET_FISCAL_DOC_ID;
      end if;

      if C_LOCK_STG_ADJ_DESC%ISOPEN then
         close C_LOCK_STG_ADJ_DESC;
      end if;

      if C_LOCK_STG_ADJ_DTL%ISOPEN then
         close C_LOCK_STG_ADJ_DTL;
      end if;

      if C_LOCK_STG_RTV_DESC%ISOPEN then
         close C_LOCK_STG_RTV_DESC;
      end if;

      if C_LOCK_STG_ASNOUT_DESC%ISOPEN then
         close C_LOCK_STG_ASNOUT_DESC;
      end if;

      if C_LOCK_STG_ASNOUT_ITEM%ISOPEN then
         close C_LOCK_STG_ASNOUT_ITEM;
      end if;

      if C_LOCK_RMA_HEAD%ISOPEN then
         close C_LOCK_RMA_HEAD;
      end if;

      if C_LOCK_DOC_DETAIL%ISOPEN then
         close C_LOCK_DOC_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
         return FALSE;

END UPDATE_FISCAL_DOC_ID_STG_TB;
--------------------------------------------------------------------------------
FUNCTION DELETE_STAGING_RECORDS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_requisition_type IN  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                                I_seq_no           IN  FM_STG_RTV_DESC.SEQ_NO%TYPE)
RETURN BOOLEAN IS
   ---
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   L_program          VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.DELETE_STAGING_RECORDS';
   L_key              VARCHAR2(100);
   ---
   cursor C_LOCK_STG_ADJ_DESC is
      select 1
        from fm_stg_adjust_desc fsad
       where fsad.seq_no = I_seq_no
         for update nowait;
   ---
   cursor C_LOCK_STG_ADJ_DTL is
      select 1
        from fm_stg_adjust_dtl fsad
       where fsad.desc_seq_no  = I_seq_no
         for update nowait;
   ---
   cursor C_LOCK_STG_RTV_DESC is
      select 1
        from fm_stg_rtv_desc fsrd
       where fsrd.seq_no     = I_seq_no
         for update nowait;
   ---
   cursor C_LOCK_STG_RTV_DTL is
      select 1
       from fm_stg_rtv_dtl dtl
      where dtl.desc_seq_no = I_seq_no
        for update nowait;
   ---
   cursor C_LOCK_STG_ASNOUT_DESC is
      select 1
        from fm_stg_asnout_desc fsad
       where fsad.seq_no = I_seq_no
         for update nowait;
   ---
    cursor C_LOCK_STG_ASNOUT_DISTRO is
      select 1
       from fm_stg_asnout_distro fsad
      where fsad.desc_seq_no = I_seq_no
        for update nowait;
   ---
   cursor C_LOCK_STG_ASNOUT_CTN is
      select 1
       from fm_stg_asnout_item fsai
      where fsai.desc_seq_no = I_seq_no
        for update nowait;
   ---
   cursor C_LOCK_STG_ASNOUT_ITEM is
      select 1
       from fm_stg_asnout_item fsai
      where fsai.desc_seq_no = I_seq_no
        for update nowait;
   ---
   BEGIN
      if I_requisition_type ='RTV' then
         L_key   := 'desc_seq_no = '||I_seq_no;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_RTV_DTL','FM_STG_RTV_DTL',L_key);
         open C_LOCK_STG_RTV_DTL;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_RTV_DTL','FM_STG_RTV_DTL',L_key);
         close C_LOCK_STG_RTV_DTL;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'FM_STG_RTV_DTL',L_key);
         ---
         delete from fm_stg_rtv_dtl
               where desc_seq_no = I_seq_no;

         L_key   := 'seq_no = '||I_seq_no;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_RTV_DESC','FM_STG_RTV_DESC',L_key);
         open C_LOCK_STG_RTV_DESC;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_RTV_DESC','FM_STG_RTV_DESC',L_key);
         close C_LOCK_STG_RTV_DESC;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'FM_STG_RTV_DESC',L_key);
         ---
         delete from fm_stg_rtv_desc
               where seq_no      = I_seq_no;
         ---
      elsif I_requisition_type ='STOCK' then
         L_key   := 'desc_seq_no = '||I_seq_no;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ADJ_DTL','FM_STG_ADJUST_DTL',L_key);
         open C_LOCK_STG_ADJ_DTL;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ADJ_DESC','FM_STG_ADJUST_DTL',L_key);
         close C_LOCK_STG_ADJ_DTL;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'FM_STG_ADJUST_DTL',L_key);
         ---
         delete from fm_stg_adjust_dtl
               where desc_seq_no = I_seq_no;
         ---
         L_key   := 'seq_no = '||I_seq_no;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ADJ_DESC','FM_STG_ADJUST_DESC',L_key);
         open C_LOCK_STG_ADJ_DESC;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ADJ_DESC','FM_STG_ADJUST_DESC',L_key);
         close C_LOCK_STG_ADJ_DESC;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'FM_STG_ADJUST_DESC',L_key);
         ---
         delete from fm_stg_adjust_desc
               where seq_no      = I_seq_no;
         ---
      elsif I_requisition_type in ('TSF','IC','REP') then
         L_key   := 'desc_seq_no = '||I_seq_no;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ASNOUT_ITEM','FM_STG_ASNOUT_ITEM',L_key);
         open C_LOCK_STG_ASNOUT_ITEM;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ASNOUT_ITEM','FM_STG_ASNOUT_ITEM',L_key);
         close C_LOCK_STG_ASNOUT_ITEM;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'FM_STG_ASNOUT_ITEM',L_key);
         ---
         delete from fm_stg_asnout_item
               where desc_seq_no = I_seq_no;
         ---
         L_key   := 'desc_seq_no = '||I_seq_no;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ASNOUT_CTN','FM_STG_ASNOUT_CTN',L_key);
         open C_LOCK_STG_ASNOUT_CTN;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ASNOUT_CTN','FM_STG_ASNOUT_CTN',L_key);
         close C_LOCK_STG_ASNOUT_CTN;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'FM_STG_ASNOUT_CTN',L_key);
         ---
         delete from fm_stg_asnout_ctn
               where desc_seq_no = I_seq_no;
         ---
         L_key   := 'desc_seq_no = '||I_seq_no;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ASNOUT_DISTRO','FM_STG_ASNOUT_DISTRO',L_key);
         open C_LOCK_STG_ASNOUT_DISTRO;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ASNOUT_DISTRO','FM_STG_ASNOUT_DISTRO',L_key);
         close C_LOCK_STG_ASNOUT_DISTRO;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'FM_STG_ASNOUT_DISTRO',L_key);
         ---
         delete from fm_stg_asnout_distro
               where desc_seq_no = I_seq_no;
         ---
         L_key   := 'desc_seq_no = '||I_seq_no;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ASNOUT_DESC','FM_STG_ASNOUT_DESC',L_key);
         open C_LOCK_STG_ASNOUT_DESC;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ASNOUT_DESC','FM_STG_ASNOUT_DESC',L_key);
         close C_LOCK_STG_ASNOUT_DESC;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'FM_STG_ASNOUT_DESC',L_key);
         ---
         delete from fm_stg_asnout_desc
               where seq_no = I_seq_no;
         ---
      end if;
      ---
   return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('Staging Records Not Deleted',
                                                SQLERRM,
                                                L_program,
                                                NULL);
            return FALSE;

END DELETE_STAGING_RECORDS;
--------------------------------------------------------------------------------
FUNCTION CREATE_INBOUND_TSF_NF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
RETURN BOOLEAN IS
   ---
   L_program            VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.CREATE_INBOUND_TSF_NF';
   L_table              VARCHAR2(150) := 'FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL/V_TSFHEAD';
   L_key                VARCHAR2(100) := 'fiscal_doc_id ='||I_fiscal_doc_id;
   ---
   C_ENTRY_MODE         CONSTANT FM_SCHEDULE.MODE_TYPE%TYPE := 'ENT';
   L_new_Fiscal_doc_id  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE := NULL;
   L_schedule_no        FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE;
   ---
   CURSOR C_GET_NF_DETAILS is
      select distinct fdh.key_value_1 as to_loc,
             tsf.to_loc_type
        from tsfhead tsf,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where tsf.tsf_no   = fdd.requisition_no
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.fiscal_doc_id = I_fiscal_doc_id
      union
      select distinct fdh.key_value_1 as to_loc,
             ald.to_loc_type
        from alloc_detail ald,
             alloc_header alh,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where alh.alloc_no = ald.alloc_no
         and alh.alloc_no   = fdd.requisition_no
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   R_get_nf_details C_GET_NF_DETAILS%ROWTYPE;
   ---
   BEGIN
      SQL_LIB.SET_MARK('OPEN','C_GET_NF_DETAILS',L_table,L_key);
      open C_GET_NF_DETAILS;
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_NF_DETAILS',L_table,L_key);
      fetch C_GET_NF_DETAILS into R_get_nf_details;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_NF_DETAILS',L_table,L_key);
      close C_GET_NF_DETAILS;
      ---
      if FM_SCHEDULE_SQL.CREATE_NEW_SCHEDULE(O_error_message,
                                             L_schedule_no,
                                             R_get_nf_details.to_loc,
                                             R_get_nf_details.to_loc_type,
                                             C_ENTRY_MODE) = FALSE then
         return FALSE;
      end if;
      ---
      if FM_FISCAL_DOC_HEADER_SQL.COPY_DOC_HEADER(O_error_message,
                                                  L_new_fiscal_doc_id,
                                                  L_schedule_no,
                                                  R_get_nf_details.to_loc,
                                                  R_get_nf_details.to_loc_type,
                                                  I_fiscal_doc_id,
                                                  'C') = FALSE then
         return FALSE;
      end if;
      ---
      if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                L_new_fiscal_doc_id,
                                                C_VALIDATED_STATUS) = FALSE then
         return FALSE;
      end if;
      ---
      if FM_SCHEDULE_SQL.UPDATE_STATUS(O_error_message,
                                       L_schedule_no,
                                       C_VALIDATED_STATUS) = FALSE then
         return FALSE;
      end if;
      ---
   return TRUE;
   ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
            return FALSE;
END CREATE_INBOUND_TSF_NF;
--------------------------------------------------------------------------------
FUNCTION CHECK_NFE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_nfe_ind          IN OUT BOOLEAN,
                   I_fiscal_doc_id    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
RETURN BOOLEAN IS
   ---
   L_program           VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.CHECK_NFE';
   L_table             VARCHAR2(150):= 'FM_FISCAL_DOC_HEADER';
   L_key               VARCHAR2(100) := 'fiscal_doc_id = '||I_fiscal_doc_id;
   L_dummy             VARCHAR2(1);
   L_new_status        FM_FISCAL_DOC_HEADER.STATUS%TYPE;
   L_type_id           FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE;
   L_nfe_type_id       FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE;
   L_dummy_string      FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_dummy_desc        FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date        FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_dummy_number      FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_fiscal_doc_no     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE;
   L_series_no         FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE;
   L_cnpj              VARCHAR2(250);

   ---
   cursor C_GET_DOC_TYPE  is
      select fdh.type_id,fdh.status
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_CHECK_FISCAL_STG  is
      select '1'
        from fm_stg_nfe fns
       where fns.fiscal_doc_id =I_fiscal_doc_id;
   ---

   cursor C_GET_SRS_FIS_DOC_NO  is
         select fdh.fiscal_doc_no,fdh.series_no
          from fm_fiscal_doc_header fdh
         where fdh.fiscal_doc_id = I_fiscal_doc_id;

   BEGIN
      SQL_LIB.SET_MARK('OPEN','C_GET_DOC_TYPE',L_table,L_key);
      open C_GET_DOC_TYPE;
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_DOC_TYPE',L_table,L_key);
      fetch C_GET_DOC_TYPE into L_type_id,L_new_status;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_DOC_TYPE',L_table,L_key);
      close C_GET_DOC_TYPE;
      ---
      if L_new_status <> 'F' then
        SQL_LIB.SET_MARK('OPEN','C_GET_SRS_FIS_DOC_NO',L_table,L_key);
        open C_GET_SRS_FIS_DOC_NO;
        ---
        SQL_LIB.SET_MARK('FETCH','C_GET_SRS_FIS_DOC_NO',L_table,L_key);
        fetch C_GET_SRS_FIS_DOC_NO into L_fiscal_doc_no,L_series_no;
        ---
        SQL_LIB.SET_MARK('CLOSE','C_GET_SRS_FIS_DOC_NO',L_table,L_key);
        close C_GET_SRS_FIS_DOC_NO;

        if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                              L_dummy_desc,
                                              L_nfe_type_id,
                                              L_dummy_string,
                                              L_dummy_date,
                                              C_NUMBER_TYPE,
                                              C_NFE_TYPE_ID) = FALSE then
           return FALSE;
        end if;
        if L_type_id = L_nfe_type_id then
           O_nfe_ind := TRUE;
           L_table := 'FM_STG_NFE';
           SQL_LIB.SET_MARK('OPEN','C_CHECK_FISCAL_STG',L_table,L_key);
           open C_CHECK_FISCAL_STG;
           ---
           SQL_LIB.SET_MARK('FETCH','C_CHECK_FISCAL_STG',L_table,L_key);
           fetch C_CHECK_FISCAL_STG into L_dummy;
           ---
           if C_CHECK_FISCAL_STG%FOUND then
              L_new_status := 'NFE_X';
              if FM_MS_NFE_SQL.UPDATE_NFE_STG_STATUS(O_error_message,
                                                     L_new_status,
                                                     I_fiscal_doc_id) = FALSE then
                return FALSE;
              end if;
              ---
              L_new_status := 'NFE_P';
              if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                        I_fiscal_doc_id,
                                                        L_new_status) = FALSE then
                 return FALSE;
              end if;
           else
              L_new_status := 'NFE_P';
              if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                        I_fiscal_doc_id,
                                                        L_new_status) = FALSE then
                 return FALSE;
              end if;
              ---
              L_cnpj:=GETCNPJ(I_fiscal_doc_id);
              insert into fm_stg_nfe(fiscal_doc_id,
                                     status,
                                     event_id,
                                     fiscal_doc_no,
                                     series_no,
                                     cnpj,
                                     prev_status)
                              values(I_fiscal_doc_id,
                                     L_new_status,
                                     fm_stg_nfe_seq.nextval,
                                     L_fiscal_doc_no,
                                     L_series_no,
                                     L_cnpj,
                                     L_new_status);
           end if;
           ---
           SQL_LIB.SET_MARK('CLOSE','C_CHECK_FISCAL_STG',L_table,L_key);
           close C_CHECK_FISCAL_STG;
        else
           O_nfe_ind := FALSE;
        end if;
      end if;
      ---
   return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('NFE_PROCESSING_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
            return FALSE;
END CHECK_NFE;
--------------------------------------------------------------------------------
FUNCTION SCHEDULE_APPROVAL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_fiscal_doc_id      IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE ,
                           I_schedule_no        IN       FM_SCHEDULE.SCHEDULE_NO%TYPE,
                           I_requisition_type   IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                           I_api_ind            IN       VARCHAR2 DEFAULT 'N',
                           I_wasteadj_ind       IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   ---
   L_program           VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.SCHEDULE_APPROVAL';

   L_table             VARCHAR2(150);
   L_key               VARCHAR2(100);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_new_status        FM_FISCAL_DOC_HEADER.STATUS%TYPE;
   L_seq_no            FM_STG_RTV_DESC.SEQ_NO%TYPE;
   L_rms_err_ind       FM_FISCAL_DOC_HEADER.RMS_UPD_ERR_IND%TYPE := 'N';
   L_requisition_no    FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE;
   L_count             INTEGER :=0;
   L_error_message     VARCHAR2(255) := NULL;
   L_error_ind         BOOLEAN := FALSE;
   L_nfe_ind           BOOLEAN := FALSE;
   L_wh                VARCHAR2(1) := 'W';
   L_batch_running_ind RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE;
   ---
   cursor C_GET_RTV_SEQ_NO is
      select distinct frd.seq_no
        from fm_stg_rtv_desc frd
       where frd.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_ADJUST_SEQ_NO is
      select distinct fad.seq_no
        from fm_stg_adjust_desc fad
       where fad.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_TSF_SEQ_NO is
      select distinct fad.seq_no
        from fm_stg_asnout_desc fad
       where fad.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_GET_RMS_ERR_IND is
      select fdh.rms_upd_err_ind
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
   cursor C_GET_REQ_NO is
      select distinct fdd.requisition_no
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdd.fiscal_doc_id = I_fiscal_doc_id;
   ---
   cursor C_LEG_COUNT(P_requisition_no FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select count(1)
        from tsfhead t
       where (t.tsf_parent_no = P_requisition_no and from_loc_type != L_wh
           or (tsf_no = P_requisition_no
          and tsf_parent_no is not null and from_loc_type != L_wh))
          and NVL(context_type,'X') != 'REPAIR';

BEGIN
   ---
   if I_api_ind = 'N' then
      if FM_EXIT_NF_CREATION_SQL.CHECK_NFE(O_error_message,
                                           L_nfe_ind,
                                           I_fiscal_doc_id) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   if NOT L_nfe_ind then
      if FM_EXIT_NF_CREATION_SQL.UPDATE_FISCAL_DOC_ID_STG_TB(O_error_message,
                                                             I_schedule_no) = FALSE then
         return FALSE;
      end if;
      ---
      if FM_ERROR_LOG_SQL.CLEAR_ERROR(O_error_message,
                                      I_fiscal_doc_id,
                                      L_program) = FALSE then
         return FALSE;
      end if;
      ---
      if RMS_BATCH_STATUS_SQL.GET_BATCH_RUNNING_IND(O_error_message,
                                                    L_batch_running_ind) = FALSE then
         return FALSE;
      end if;

      -- Only call FM_xxx_CONSUME_SQL.CONSUME if batch is NOT running.
      if I_requisition_type ='RTV' then
         L_table := 'FM_STG_RTV_DESC';
         L_key   := 'fiscal_doc_id = '||I_fiscal_doc_id;
         SQL_LIB.SET_MARK('OPEN','C_GET_RTV_SEQ_NO',L_table,L_key);
         open C_GET_RTV_SEQ_NO;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_RTV_SEQ_NO',L_table,L_key);
         fetch C_GET_RTV_SEQ_NO into L_seq_no;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_RTV_SEQ_NO',L_table,L_key);
         close C_GET_RTV_SEQ_NO;
         ---
         if L_batch_running_ind = 'N' then
            if FM_RTV_CONSUME_SQL.CONSUME(O_error_message,
                                          L_error_ind,
                                          L_seq_no) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      elsif I_requisition_type ='STOCK' then
         L_table := 'FM_STG_ADJUST_DESC';
         L_key   := 'fiscal_doc_id = '||I_fiscal_doc_id;
         SQL_LIB.SET_MARK('OPEN','C_GET_ADJUST_SEQ_NO',L_table,L_key);
         open C_GET_ADJUST_SEQ_NO;
         ---
         SQL_LIB.SET_MARK('FETCH','C_GET_ADJUST_SEQ_NO',L_table,L_key);
         fetch C_GET_ADJUST_SEQ_NO into L_seq_no;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_ADJUST_SEQ_NO',L_table,L_key);
         close C_GET_ADJUST_SEQ_NO;
         ---
         -- I_wasteadj_ind of 'Y' indicates the function is invoked from wasteadj.pc (through l10nbrfinlib.pc).
         -- In that case, FM_INVADJ_CONSUME_SQL.CONSUME should be invoked to update RMS inventory even if batch is running.
         if L_batch_running_ind = 'N' or I_wasteadj_ind = 'Y' then
            if FM_INVADJ_CONSUME_SQL.CONSUME(O_error_message,
                                             L_error_ind,
                                             L_seq_no) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      elsif I_requisition_type in ('TSF', 'REP', 'IC') then
         L_table := 'FM_STG_ASNOUT_DESC';
         L_key   := 'fiscal_doc_id = '||I_fiscal_doc_id;
         SQL_LIB.SET_MARK('OPEN','C_GET_TSF_SEQ_NO',L_table,L_key);
         open C_GET_TSF_SEQ_NO;
         ---
         SQL_LIB.SET_MARK('FETCH','C_GET_TSF_SEQ_NO',L_table,L_key);
         fetch C_GET_TSF_SEQ_NO into L_seq_no;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_SEQ_NO',L_table,L_key);
         close C_GET_TSF_SEQ_NO;
         ---
         if L_batch_running_ind = 'N' then
            if FM_ASNOUT_CONSUME_SQL.CONSUME(O_error_message,
                                             L_error_ind,
                                             L_seq_no) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if;
      ---
      if L_error_ind = FALSE then
         L_table := 'FM_FISCAL_DOC_HEADER';
         L_key   := 'fiscal_doc_id = '||I_fiscal_doc_id;
         SQL_LIB.SET_MARK('OPEN','C_GET_RMS_ERR_IND',L_table,L_key);
         open C_GET_RMS_ERR_IND;
         ---
         SQL_LIB.SET_MARK('FETCH','C_GET_RMS_ERR_IND',L_table,L_key);
         fetch C_GET_RMS_ERR_IND into L_rms_err_ind;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_RMS_ERR_IND',L_table,L_key);
         close C_GET_RMS_ERR_IND;
         ---
         if L_rms_err_ind = 'Y' then
            if FM_FISCAL_DOC_HEADER_SQL.UPDATE_RMS_ERR_STATUS(O_error_message,
                                                              I_fiscal_doc_id) = FALSE then
               return FALSE;
            end if;
            ---
         end if;
         ---
         if I_requisition_type = 'RMA' then
            L_new_status := C_PROCESSED_STATUS;
         else
            L_new_status := C_APPROVED_STATUS;
         end if;
         ---
         if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                   I_fiscal_doc_id,
                                                   L_new_status) = FALSE then
            return FALSE;
         end if;
         ---
         if  I_requisition_type = 'RMA' then
            if FM_FISCAL_DOC_HEADER_SQL.UPD_SCHED_STATUS_RMA(O_error_message,
                                                             I_schedule_no)= FALSE then
               return FALSE;
            end if;
         else
            if FM_SCHEDULE_SQL.UPDATE_STATUS(O_error_message,
                                             I_schedule_no,
                                             L_new_status) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if I_requisition_type != 'RMA' then
            if L_batch_running_ind = 'Y' and I_wasteadj_ind = 'N' then
               -- batch cycle, RMS consume logic is NOT processed, update stg to Hold status
               if FM_INTEGRATION_SQL.UPDATE_STG_STATUS(O_error_message,
                                                       C_HOLD_STATUS,
                                                       I_requisition_type,
                                                       L_seq_no) = FALSE then
                  return FALSE;
               end if;

               -- update FM_FISCAL_DOC_HEADER and FM_SCHEDULE to 'H'old status as well to
               -- prevent RFM batch jobs (financial posting and SPEC batch) from happening
               -- till after RMS consume processing has been completed.
               if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                         I_fiscal_doc_id,
                                                         C_HOLD_STATUS) = FALSE then
                  return FALSE;
               end if;

               if FM_SCHEDULE_SQL.UPDATE_STATUS(O_error_message,
                                                I_schedule_no,
                                                C_HOLD_STATUS) = FALSE then
                  return FALSE;
               end if;
            else
               -- non-batch cycle, RMS consume logic is processed, can delete the stg records
               if O_error_message is NULL and L_new_status = C_APPROVED_STATUS then
                  if FM_EXIT_NF_CREATION_SQL.DELETE_STAGING_RECORDS(O_error_message,
                                                                    I_requisition_type,
                                                                    L_seq_no) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;
         end if;
      else
         if FM_FISCAL_DOC_HEADER_SQL.UPDATE_RMS_ERR_STATUS(O_error_message,
                                                           I_fiscal_doc_id,
                                                           'Y') = FALSE then
            return FALSE;
         end if;
         ---
         L_error_message := SQL_LIB.CREATE_MSG('ORFM_RMS_UPATION_ERROR',
                                                NULL,
                                                NULL,
                                                NULL);

         if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                         I_fiscal_doc_id,
                                         L_program,
                                         L_program,
                                        'E',
                                        'RMS_UPDATION_ERROR',
                                         O_error_message,
                                        'Fiscal_doc_id: '||
                                         TO_CHAR(I_fiscal_doc_id)) = FALSE then
            return FALSE;
         end if;
         return FALSE;
      end if;
      ---
      if I_requisition_type in ('TSF','IC') then
         SQL_LIB.SET_MARK('OPEN','C_GET_REQ_NO','FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL','fiscal_doc_id '||TO_CHAR(I_fiscal_doc_id));
         open C_GET_REQ_NO;
         ---
         SQL_LIB.SET_MARK('FETCH','C_GET_REQ_NO','FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL','fiscal_doc_id '||TO_CHAR(I_fiscal_doc_id));
         fetch C_GET_REQ_NO into L_requisition_no;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_REQ_NO','FM_FISCAL_DOC_HEADER/FM_FISCAL_DOC_DETAIL','fiscal_doc_id '||TO_CHAR(I_fiscal_doc_id));
         close C_GET_REQ_NO;
         ---
         SQL_LIB.SET_MARK('OPEN','C_LEG_COUNT','TSFHEAD','tsf_parent_no '||L_requisition_no||'tsf_no '||L_requisition_no);
         open C_LEG_COUNT(L_requisition_no);
         ---
         SQL_LIB.SET_MARK('FETCH','C_LEG_COUNT','TSFHEAD','tsf_parent_no '||L_requisition_no||'tsf_no '||L_requisition_no);
         fetch C_LEG_COUNT into L_count;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LEG_COUNT','TSFHEAD','tsf_parent_no '||L_requisition_no||'tsf_no '||L_requisition_no);
         close C_LEG_COUNT;
         ---
      end if;
      ---
      if L_new_status = C_APPROVED_STATUS and I_requisition_type in ('TSF','IC') and L_count = 0 then
         if FM_EXIT_NF_CREATION_SQL.CREATE_INBOUND_TSF_NF(O_error_message,
                                                          I_fiscal_doc_id)= FALSE then
            return False;
         end if;
      end if;
      ---
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('APPROVAL_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END SCHEDULE_APPROVAL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_OUTBOUND(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_process_status     IN OUT   INTEGER,
                          I_fiscal_doc_id      IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE ,
                          I_utilization_id     IN       FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE,
                          I_schedule_no        IN       FM_SCHEDULE.SCHEDULE_NO%TYPE,
                          I_requisition_type   IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                          I_form_ind           IN       VARCHAR2 DEFAULT 'N',
                          I_wasteadj_ind       IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   ---
   L_program           VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.PROCESS_OUTBOUND';
   L_cursor            VARCHAR2(100);
   L_table             VARCHAR2(150);
   L_key               VARCHAR2(100);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_new_status        FM_FISCAL_DOC_HEADER.STATUS%TYPE;
   L_other_proc               BOOLEAN :=TRUE;
   ---
   cursor C_GET_UTIL_PARAM(P_utilization_id  FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE) is
      select fua.auto_approve_ind,
             fua.choose_doc_ind
        from fm_utilization_attributes fua
       where fua.utilization_id   = P_utilization_id;
   ---
   R_get_util_param C_GET_UTIL_PARAM%ROWTYPE;
   ---
   cursor C_CHECK_LAST_NF(P_fiscal_doc_id  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)  is
      select count(1)
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.fiscal_doc_id_ref is null
         and fdh.fiscal_doc_id = P_fiscal_doc_id;
   ---
   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_UTIL_PARAM','FM_UTILIZATION_ATTRIBUTES','utilization_id '||I_utilization_id);
   open C_GET_UTIL_PARAM(I_utilization_id);
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_UTIL_PARAM','FM_UTILIZATION_ATTRIBUTES','utilization_id '||I_utilization_id);
   fetch C_GET_UTIL_PARAM into R_get_util_param;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_UTIL_PARAM','FM_UTILIZATION_ATTRIBUTES','utilization_id '||I_utilization_id);
   close C_GET_UTIL_PARAM;
   ---

   if (I_form_ind ='Y' or (R_get_util_param.auto_approve_ind = 'Y' and (I_requisition_type <> 'RTV' or R_get_util_param.choose_doc_ind = 'N'))) then
      O_process_status := 1;
      if FM_FISCAL_VALIDATION_SQL.CHECK_DATA_INTEGRITY(O_error_message,
                                                       O_process_status,
                                                       L_other_proc,
                                                       I_fiscal_doc_id,
                                                       I_form_ind) = FALSE then
         return FALSE;
      end if;
      if O_process_status = 0 then
         L_new_status := C_ERROR_STATUS;
      else
         if FM_CALC_TOTALS_SQL.POPULATE_VALUES(O_error_message,
                                               O_process_status,
                                               I_fiscal_doc_id,
                                               I_form_ind) = FALSE then
            return FALSE;
         end if;
         ---
         if O_process_status = 1 then
            if I_requisition_type = 'RTV' then
               if FM_CALC_TOTALS_SQL.UPDATE_TOTALS(O_error_message,
                                                   O_process_status,
                                                   I_fiscal_doc_id)= FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            L_cursor := 'C_LOCK_DOC_HEADER';
            L_table  := 'FM_FISCAL_DOC_HEADER';
            L_key    := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
            ---
            SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
            open C_LOCK_DOC_HEADER;
            ---
            SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
            close C_LOCK_DOC_HEADER;
            ---
            SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
            ---
            update fm_fiscal_doc_header
               set total_serv_value           = total_serv_calc_value,
                   total_item_value           = total_item_calc_value,
                   total_doc_value            = total_doc_calc_value,
                   total_doc_value_with_disc  = total_doc_calc_value
             where fiscal_doc_id              = I_fiscal_doc_id;

            L_new_status := C_VALIDATED_STATUS;
         else
            L_new_status := C_ERROR_STATUS;
         end if;
         ---
      end if;
      ---
      if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                I_fiscal_doc_id,
                                                L_new_status) = FALSE then
         return FALSE;
      end if;
      ---
      if FM_SCHEDULE_SQL.UPDATE_STATUS(O_error_message,
                                       I_schedule_no,
                                       L_new_status) = FALSE then
         return FALSE;
      end if;
      ---
      if (L_new_status != C_ERROR_STATUS and R_get_util_param.auto_approve_ind = 'Y' and R_get_util_param.choose_doc_ind='N' and I_form_ind ='N') then
         if FM_EXIT_NF_CREATION_SQL.SCHEDULE_APPROVAL(O_error_message,
                                                      I_fiscal_doc_id,
                                                      I_schedule_no,
                                                      I_requisition_type,
                                                      'N', -- I_api_ind
                                                      I_wasteadj_ind) = FALSE then
            return FALSE;
         end if;
      elsif L_new_status = C_ERROR_STATUS and I_form_ind ='N' then
         return FALSE;
      end if;
      ---
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      -- Close all cursors
      if C_LOCK_DOC_HEADER%ISOPEN then
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            L_key,
                                            NULL);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('VALIDATION_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
         return FALSE;
END PROCESS_OUTBOUND;
----------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_status_code        IN OUT   INTEGER,
                 I_requisition_type   IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                 I_seq_no             IN       FM_STG_RTV_DESC.SEQ_NO%TYPE,
                 I_wasteadj_ind       IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program               VARCHAR2(100) := 'FM_EXIT_NF_CREATION_SQL.PROCESS';
   L_cursor                VARCHAR2(100);
   L_table                 VARCHAR2(150);
   L_key                   VARCHAR2(100);
   ---
   C_CHAR_TYPE             CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE        := 'CHAR';
   C_NUMBER_TYPE           CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE        := 'NUMBER';
   C_TYPE_ID               CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE             := 'DEFAULT_DOCUMENT_TYPE';
   C_FREIGHT_TYPE          CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE             := 'DEFAULT_FREIGHT_TYPE';
   C_RTV_UTIL_ID           CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE             := 'DEFAULT_RTV_UTIL_ID';
   C_OUTBOUND_UTIL_ID      CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE             := 'DEFAULT_OUTBOUND_TSF_UTIL_ID';
   C_OUTBOUND_IC_UTIL_ID   CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE             := 'DEFAULT_OUTBOUND_IC_UTIL_ID';
   C_OUTBOUND_REP_UTIL_ID  CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE             := 'DEFAULT_OUTBOUND_REP_UTIL_ID';
   C_PROCESS_STATUS        CONSTANT FM_EDI_DOC_HEADER.PROCESS_STATUS%TYPE       := 'R'; --Received
   C_INITIAL_STATUS        CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE            := 'W'; --Worksheet
   C_UNPROC_STATUS         CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE            := 'U'; --Unprocessed
   C_KEY_VALUE_2           CONSTANT FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE       := 'S';
   C_PO                    CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE  := 'PO';
   C_ENTRY_MODE            CONSTANT FM_SCHEDULE.MODE_TYPE%TYPE                  := 'ENT';
   C_EXT_MODULE            CONSTANT FM_EDI_DOC_HEADER.MODULE%TYPE               := 'PTNR';
   C_INT_MODULE            CONSTANT FM_EDI_DOC_HEADER.MODULE%TYPE               := 'LOC';
   ---
   L_utilization_id           FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE;
   L_create_datetime          FM_FISCAL_DOC_HEADER.CREATE_DATETIME%TYPE := SYSDATE;
   L_create_id                FM_FISCAL_DOC_HEADER.CREATE_ID%TYPE := USER;
   L_type_id                  FM_FISCAL_DOC_HEADER.TYPE_ID%TYPE;
   L_freight_type             FM_FISCAL_DOC_HEADER.FREIGHT_TYPE%TYPE;
   L_classif_id               FM_FISCAL_DOC_DETAIL.CLASSIFICATION_ID%TYPE := NULL;
   L_classif_desc             V_FISCAL_CLASSIFICATION.CLASSIFICATION_DESC%TYPE;
   L_loc_type                 FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
   L_new_status               FM_FISCAL_DOC_HEADER.STATUS%TYPE;
   L_schedule_no              FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE;
   L_partner_type             FM_FISCAL_DOC_HEADER.PARTNER_TYPE%TYPE;
   L_partner_id               FM_FISCAL_DOC_HEADER.PARTNER_ID%TYPE;
   L_schedule_mode            FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_unit_cost                FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_total_cost               FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE;
   L_fiscal_doc_line_id_ref   FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID_REF%TYPE := NULL;
   L_fiscal_doc_id_ref        FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID_REF%TYPE      := NULL;
   L_fiscal_doc_id            FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_to_loc_type              FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
   L_from_loc_type            FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE;
   L_to_state                 V_FISCAL_ATTRIBUTES.STATE%TYPE;
   L_from_state               V_FISCAL_ATTRIBUTES.STATE%TYPE;
   L_requisition_type         FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_from_ent_id              WH.TSF_ENTITY_ID%TYPE;
   L_to_ent_id                WH.TSF_ENTITY_ID%TYPE;
   L_tsf_cost                 FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE := NULL;
   L_tsf_price                FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE := NULL;
   L_fiscal_doc_no            FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE;
   L_series_no                FM_FISCAL_DOC_HEADER.SERIES_NO%TYPE;
   L_subseries_no             FM_FISCAL_DOC_HEADER.SUBSERIES_NO%TYPE;
   L_no                       VARCHAR2(1) :='N';
   ---
   L_total_chrgs_prim         item_loc.unit_retail%TYPE := 0;
   L_profit_chrgs_to_loc      item_loc.unit_retail%TYPE := 0;
   L_exp_chrgs_to_loc         item_loc.unit_retail%TYPE := 0;
   L_tsf_seq_no               tsfdetail.tsf_seq_no%TYPE;
   ---
   L_dummy_desc               FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date               FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_dummy_number             FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_dummy_string             FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_status                   integer;
   ---
   L_pack_level               FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE := NULL;
   L_to_loc                   tsfhead.to_loc%type;
   L_from_loc                 tsfhead.from_loc%type;
   L_pck_ind                  VARCHAR2(1) := 'Y';
   L_exit_hour                TIMESTAMP := NULL;
   L_dummy                    VARCHAR2(30);
   ---
   L_pack_ind                 FM_FISCAL_DOC_DETAIL.PACK_IND%TYPE;
   L_pack_no                  FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE;
   L_line_no                  FM_FISCAL_DOC_DETAIL.LINE_NO%TYPE  := 0;
   L_rtv_qty                  FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE := 0;
   L_exists                   BOOLEAN;
   L_header_cost              FM_FISCAL_DOC_HEADER.TOTAL_ITEM_VALUE%TYPE :=0;
   L_weight_uom               FM_FISCAL_DOC_HEADER.UNIT_TYPE%TYPE :='KG';
   L_total_weight             FM_FISCAL_DOC_HEADER.TOTAL_WEIGHT%TYPE := 0;
   L_weight_cnvt              ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := 0;
   L_multiple_weight_uom_ind  VARCHAR2(1) :='N';
   L_weight_uom_temp          FM_STG_ASNOUT_ITEM.WEIGHT_UOM%TYPE := '-999';

   TYPE T_fiscal_doc_dtl_tbl IS TABLE OF FM_FISCAL_DOC_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE T_fiscal_doc_head_rec IS RECORD (L_fiscal_head_rec    FM_FISCAL_DOC_HEADER%ROWTYPE,
                                         L_fiscal_doc_dtl_tab T_fiscal_doc_dtl_tbl);

   L_fiscal_doc_head_rec  T_fiscal_doc_head_rec;

   i BINARY_INTEGER := 0;
   j BINARY_INTEGER := 0;
   k INTEGER :=0;
   l INTEGER :=0;
   ---
   cursor C_CHK_PO_REF(P_location_type FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                       P_location_id   FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                       P_item          FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select tab.pack_level,
             tab.fiscal_doc_id
       from (select fdd.pack_ind as pack_level,
                    fdh.fiscal_doc_id,
                    fdh.entry_or_exit_date
               from fm_fiscal_doc_header fdh,
                    fm_fiscal_doc_detail fdd,
                    fm_schedule fs,
                    fm_utilization_attributes fua,
                    fm_fiscal_utilization ffu
              where fdh.fiscal_doc_id        = fdd.fiscal_doc_id
                and fdh.schedule_no          = fs.schedule_no
                and fdh.utilization_id       = fua.utilization_id
                and fdh.utilization_id       = ffu.utilization_id
                and fdd.location_type        = P_location_type
                and fdd.location_id          = P_location_id
                and fdd.item                 = P_item
                and fdh.status             in (C_APPROVED_STATUS, C_COMPLETED_STATUS)
                and fdh.requisition_type     = C_PO
                and fs.status              in (C_APPROVED_STATUS, C_COMPLETED_STATUS)
                and fs.mode_type             = C_ENTRY_MODE
                and fua.comp_nf_ind          = L_no
                and ffu.status               = C_APPROVED_STATUS
           order by fdh.entry_or_exit_date desc,
                    fdh.fiscal_doc_id desc)tab
              where rownum<2;
   ---
   cursor C_GET_RTV_HDR is
      select hdr.seq_no,
             hdr.rtv_id,
             hdr.dc_dest_id,
             hdr.vendor_nbr,
             SUM(dtl.unit_qty) total_qty
        from fm_stg_rtv_desc hdr,
             fm_stg_rtv_dtl dtl
       where hdr.status in (C_UNPROC_STATUS,C_ERROR_STATUS)
         and dtl.desc_seq_no = hdr.seq_no
         and hdr.seq_no = I_seq_no
    group by hdr.seq_no,
             hdr.rtv_id,
             status,
             hdr.dc_dest_id,
             hdr.vendor_nbr
    order by rtv_id,
             dc_dest_id,
             vendor_nbr;
   ---
   L_rtv_rec_hdr          C_GET_RTV_HDR%ROWTYPE;
   ---
   cursor C_GET_RTV_LINES (P_rtv_id      FM_STG_RTV_DESC.RTV_ID%TYPE,
                           P_dc_dest_id  FM_STG_RTV_DESC.DC_DEST_ID%TYPE,
                           P_vendor_nbr  FM_STG_RTV_DESC.VENDOR_NBR%TYPE) is
      select tab.rtv_id,
             tab.dc_dest_id,
             tab.item_id,
             tab.weight_uom,
             tab.weight,
             tab.unit_qty,
             tab.pack_no,
             rownum line_no
        from (select items.rtv_id,
                     items.dc_dest_id,
                     items.item_id,
                     items.weight_uom,
                     SUM(items.weight) weight,
                     SUM(items.unit_qty) unit_qty,
                     items.pack_no
                from (
                      select hdr.rtv_id,
                             hdr.dc_dest_id,
                             hdr.vendor_nbr,
                             dtl.item_id,
                             dtl.weight_uom,
                             dtl.weight,
                             dtl.unit_qty,
                             NULL as pack_no
                        from fm_stg_rtv_desc hdr,
                             fm_stg_rtv_dtl dtl
                       where hdr.seq_no      = I_seq_no
                         and dtl.desc_seq_no = hdr.seq_no
                         and hdr.status      in (C_UNPROC_STATUS,C_ERROR_STATUS)
                         and hdr.rtv_id      = P_rtv_id
                         and hdr.dc_dest_id  = P_dc_dest_id
                         and hdr.vendor_nbr  = P_vendor_nbr
                 ) items
               group by items.rtv_id,
                        items.dc_dest_id,
                        items.pack_no,
                        items.item_id,
                        items.weight_uom
               order by items.dc_dest_id,
                        items.weight_uom,
                        items.pack_no,
                        items.item_id
              )tab;
   ---
   TYPE T_rtv_lines_tab IS TABLE OF C_GET_RTV_LINES%ROWTYPE
   INDEX BY BINARY_INTEGER;
   L_rtv_lines_tab T_rtv_lines_tab;
   ---
   cursor C_CHK_PACK_IND(P_item FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
      select im.pack_ind
        from item_master im
       where im.item    = P_item;
   ---
   cursor C_PACK_EXPLODE(P_pack_no FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                         P_pack_unit_qty FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE) is
      select (pi.pack_item_qty) * (P_pack_unit_qty) as unit_qty,
              pi.item as item_id,
              P_pack_no as pack_no
        from packitem_breakout pi
       where pi.pack_no = P_pack_no;

   TYPE T_rtv_pack_lines_tab IS TABLE OF C_PACK_EXPLODE%ROWTYPE
   INDEX BY BINARY_INTEGER;
   L_rtv_pack_lines_tab T_rtv_pack_lines_tab;
   ---
   cursor C_GET_ADJ_HDR is
      select hdr.seq_no,
             hdr.dc_dest_id,
             fuir.utilization_id util_id,
             abs(tab.unit_qty) total_qty,
             hdr.rowid header_rowid
        from fm_stg_adjust_desc hdr,
             fm_stg_adjust_dtl items,
             fm_util_invadj_reason fuir,
             inv_adj_reason ir,
            (select SUM(item.unit_qty) unit_qty
               from (select dtl.unit_qty
                       from fm_stg_adjust_dtl dtl
                      where dtl.desc_seq_no = I_seq_no
                        and ABS(dtl.unit_qty) > 0
                        and NOT EXISTS (select 0
                                          from item_master im
                                         where im.item = dtl.item_id
                                           and im.pack_ind = L_pck_ind)
                      UNION ALL
                      select (pi.pack_item_qty * dtl.unit_qty) unit_qty
                         from fm_stg_adjust_dtl dtl,
                              packitem_breakout pi
                        where pi.pack_no = dtl.item_id
                         and dtl.desc_seq_no = I_seq_no
                         and ABS(dtl.unit_qty) > 0)item)tab
       where hdr.status in (C_UNPROC_STATUS,C_ERROR_STATUS)
         and hdr.seq_no = items.desc_seq_no
         and items.adjustment_reason_code = ir.reason
         and ir.reason = fuir.reason
         and hdr.seq_no = I_seq_no
         and fuir.utilization_id is NOT NULL
    group by hdr.seq_no,
             dc_dest_id,
             fuir.utilization_id,
             tab.unit_qty,
             hdr.rowid
    order by dc_dest_id,
             fuir.utilization_id;

   L_adj_rec_hdr          C_GET_ADJ_HDR%ROWTYPE;
   ---
   cursor C_GET_ADJ_LINES (P_seq_no            FM_STG_ADJUST_DESC.SEQ_NO%TYPE,
                           P_dc_dest_id        FM_STG_ADJUST_DESC.DC_DEST_ID%TYPE,
                           P_utilization_id    FM_UTIL_INVADJ_REASON.UTILIZATION_ID%TYPE) is
      select tab.seq_no,
             tab.dc_dest_id,
             tab.item_id,
             tab.weight_uom,
             tab.weight,
             abs(tab.unit_qty) unit_qty,
             tab.pack_no,
             tab.pack_ind,
             rownum line_no
        from (select items.seq_no,
                     items.dc_dest_id,
                     items.item_id,
                     items.weight_uom,
                     SUM(items.weight) weight,
                     SUM(items.unit_qty) unit_qty,
                     items.pack_ind,
                     items.pack_no
                from (-- SINGLE ITEMS
                      select hdr.seq_no,
                             hdr.dc_dest_id,
                             dtl.item_id,
                             dtl.weight_uom,
                             dtl.weight,
                             dtl.unit_qty,
                             NULL as pack_no,
                             'N' as pack_ind
                        from fm_stg_adjust_desc hdr,
                             fm_stg_adjust_dtl dtl,
                             fm_util_invadj_reason iar
                       where hdr.status in (C_UNPROC_STATUS,C_ERROR_STATUS)
                         and hdr.seq_no = dtl.desc_seq_no
                         and dtl.adjustment_reason_code = iar.reason
                         and hdr.seq_no = P_seq_no
                         and hdr.dc_dest_id = P_dc_dest_id
                         and iar.utilization_id = P_utilization_id
                         and ABS(dtl.unit_qty) > 0
                         and NOT EXISTS (select 0
                                           from item_master vim
                                          where vim.item = dtl.item_id
                                            and vim.pack_ind = L_pck_ind)
                      UNION ALL
                      -- PACK ITEMS
                      select hdr.seq_no,
                             hdr.dc_dest_id,
                             pi.item item_id,
                             dtl.weight_uom,
                             dtl.weight,
                             (pi.pack_item_qty * dtl.unit_qty) unit_qty,
                             dtl.item_id as pack_no,
                             'N' as pack_ind
                        from fm_stg_adjust_desc hdr,
                             fm_stg_adjust_dtl dtl,
                             packitem_breakout pi,
                             fm_util_invadj_reason iar
                       where hdr.status in (C_UNPROC_STATUS,C_ERROR_STATUS)
                         and hdr.seq_no = dtl.desc_seq_no
                         and pi.pack_no = dtl.item_id
                         and dtl.adjustment_reason_code = iar.reason
                         and hdr.seq_no = P_seq_no
                         and hdr.dc_dest_id = P_dc_dest_id
                         and iar.utilization_id = P_utilization_id
                         and ABS(dtl.unit_qty) > 0
              ) items
               group by seq_no,
                        dc_dest_id,
                        item_id,
                        pack_no,
                        pack_ind,
                        weight_uom
               order by dc_dest_id,
                        weight_uom,
                        pack_no,
                        item_id
             ) tab;
   ---
   TYPE T_adj_lines_tab IS TABLE OF C_GET_ADJ_LINES%ROWTYPE
   INDEX BY BINARY_INTEGER;
   L_adj_lines_tab T_adj_lines_tab;
   ---
   cursor C_GET_TSF_HDR is
      select hdr.seq_no,
             hdr.from_location,
             hdr.to_location,
             hdr.asn_nbr,
             tab.unit_qty total_qty,
             distro.distro_doc_type,
             hdr.rowid header_rowid
        from fm_stg_asnout_desc hdr,
             fm_stg_asnout_distro distro,
            (select SUM(item.unit_qty) unit_qty
               from (select dtl.unit_qty
                       from fm_stg_asnout_item dtl
                      where dtl.desc_seq_no = I_seq_no
                        and NOT EXISTS (select 0
                                          from item_master vim
                                         where vim.item = dtl.item_id
                                           and vim.pack_ind = L_pck_ind)
                      UNION ALL
                      select (pi.pack_qty * dtl.unit_qty) unit_qty
                         from fm_stg_asnout_item dtl,
                              packitem pi
                        where pi.pack_no = dtl.item_id
                         and dtl.desc_seq_no = I_seq_no)item)tab
       where hdr.status in (C_UNPROC_STATUS,C_ERROR_STATUS)
         and hdr.seq_no = distro.desc_seq_no
         and hdr.seq_no = I_seq_no
    group by hdr.seq_no,
             hdr.from_location,
             hdr.to_location,
             hdr.asn_nbr,
             tab.unit_qty,
             distro.distro_doc_type,
             hdr.rowid
    order by from_location,
             to_location;
   ---
   L_tsf_rec_hdr          C_GET_TSF_HDR%ROWTYPE;
   ---
   cursor C_GET_TSF_LINES (P_hdr_seq_no     FM_STG_ASNOUT_DESC.SEQ_NO%TYPE,
                           P_from_location  FM_STG_ASNOUT_DESC.FROM_LOCATION%TYPE,
                           P_to_location    FM_STG_ASNOUT_DESC.TO_LOCATION%TYPE) is
      select tab.seq_no,
             tab.from_location,
             tab.to_location,
             tab.distro_nbr,
             tab.distro_doc_type,
             tab.container_id,
             tab.item_id,
             tab.weight_uom,
             tab.weight,
             tab.unit_qty,
             tab.pack_no,
             tab.pack_ind,
             rownum line_no
        from (select items.seq_no,
                     items.from_location,
                     items.to_location,
                     items.distro_nbr,
                     items.distro_doc_type,
                     items.container_id,
                     items.item_id,
                     items.weight_uom,
                     SUM(items.weight) weight,
                     SUM(items.unit_qty) unit_qty,
                     items.pack_ind,
                     items.pack_no
                from (-- SINGLE ITEMS
                      select hdr.seq_no,
                             hdr.from_location,
                             hdr.to_location,
                             distro.distro_nbr,
                             distro.distro_doc_type,
                             ctn.container_id,
                             dtl.item_id,
                             dtl.weight_uom,
                             dtl.weight,
                             dtl.unit_qty,
                             NULL as pack_no,
                             'N' as pack_ind
                        from fm_stg_asnout_desc hdr,
                             fm_stg_asnout_distro distro,
                             fm_stg_asnout_ctn ctn,
                             fm_stg_asnout_item dtl
                       where hdr.status in (C_UNPROC_STATUS,C_ERROR_STATUS)
                         and distro.desc_seq_no = hdr.seq_no
                         and ctn.desc_seq_no    = hdr.seq_no
                         and dtl.desc_seq_no    = hdr.seq_no
                         and ctn.distro_seq_no  = distro.seq_no
                         and dtl.distro_seq_no  = distro.seq_no
                         and dtl.ctn_seq_no     = ctn.seq_no
                         ---
                         and hdr.seq_no = P_hdr_seq_no
                         and hdr.to_location = P_to_location
                         and hdr.from_location = P_from_location
                         and NOT EXISTS (select 0
                                           from item_master vim
                                          where vim.item = dtl.item_id
                                            and vim.pack_ind = L_pck_ind)
                      UNION ALL
                      --  PACK ITEMS
                      select hdr.seq_no,
                             hdr.from_location,
                             hdr.to_location,
                             distro.distro_nbr,
                             distro.distro_doc_type,
                             ctn.container_id,
                             pi.item item_id,
                             dtl.weight_uom,
                             dtl.weight,
                             (pi.pack_qty * dtl.unit_qty) unit_qty,
                              dtl.item_id as pack_no,
                              'N' as pack_ind
                        from fm_stg_asnout_desc hdr,
                             fm_stg_asnout_distro distro,
                             fm_stg_asnout_ctn ctn,
                             fm_stg_asnout_item dtl,
                             packitem pi
                       where hdr.status in(C_UNPROC_STATUS,C_ERROR_STATUS)
                         and distro.desc_seq_no = hdr.seq_no
                         and ctn.desc_seq_no    = hdr.seq_no
                         and dtl.desc_seq_no    = hdr.seq_no
                         and ctn.distro_seq_no  = distro.seq_no
                         and dtl.distro_seq_no  = distro.seq_no
                         and dtl.ctn_seq_no     = ctn.seq_no
                         and pi.pack_no         = dtl.item_id
                         ---
                         and hdr.seq_no        = P_hdr_seq_no
                         and hdr.to_location   = P_to_location
                         and hdr.from_location = P_from_location
               )items
               group by seq_no,
                        from_location,
                        to_location,
                        item_id,
                        weight_uom,
                        distro_nbr,
                        distro_doc_type,
                        items.container_id,
                        pack_no,
                        pack_ind
               order by from_location,
                        to_location,
                        weight_uom,
                        pack_no,
                        item_id
             ) tab;
   ---
   TYPE T_tsf_lines_tab IS TABLE OF C_GET_TSF_LINES%ROWTYPE
   INDEX BY BINARY_INTEGER;
   L_tsf_lines_tab T_tsf_lines_tab;
   ---

   cursor C_GET_STATE (P_module       V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                       P_key_value_1  V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                       P_key_value_2  V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)is
      select fea.state
        from v_fiscal_attributes fea
       where fea.module = P_module
         and fea.key_value_1 = P_key_value_1
         and fea.key_value_2 = P_key_value_2;
   ---
   cursor C_GET_TSF_UTIL_ID (P_hdr_seq_no     FM_STG_ASNOUT_DESC.SEQ_NO%TYPE,
                             P_from_location  FM_STG_ASNOUT_DESC.FROM_LOCATION%TYPE,
                             P_to_location    FM_STG_ASNOUT_DESC.TO_LOCATION%TYPE) is
      select uc.util_code,
             decode(tsf.context_type,'REPAIR','REP',decode(tsf.tsf_type,'IC','IC','TSF')) requisition_type,
             tsf.to_loc,
             tsf.from_loc
        from fm_stg_asnout_desc hdr,
             fm_stg_asnout_distro distro,
             v_br_tsf_util_code uc,
             tsfhead tsf
       where hdr.status         in (C_UNPROC_STATUS,C_ERROR_STATUS)
         and distro.desc_seq_no = hdr.seq_no
         and distro.distro_nbr  = tsf.tsf_no
         and NVL(tsf.tsf_parent_no,tsf.tsf_no) = uc.tsf_no
         and hdr.seq_no         = P_hdr_seq_no
         and hdr.to_location    = P_to_location
         and hdr.from_location  = P_from_location;
    ---
   cursor C_GET_ALLOC_DTL (P_hdr_seq_no     FM_STG_ASNOUT_DESC.SEQ_NO%TYPE,
                           P_from_location  FM_STG_ASNOUT_DESC.FROM_LOCATION%TYPE,
                           P_to_location    FM_STG_ASNOUT_DESC.TO_LOCATION%TYPE) is
      select ald.to_loc,
             alh.wh
        from fm_stg_asnout_desc hdr,
             fm_stg_asnout_distro distro,
             alloc_header alh,
             alloc_detail ald
       where hdr.status         in (C_UNPROC_STATUS,C_ERROR_STATUS)
         and distro.desc_seq_no = hdr.seq_no
         and distro.distro_nbr  = alh.alloc_no
         and alh.alloc_no       = ald.alloc_no
         and hdr.seq_no         = P_hdr_seq_no
         and hdr.to_location    = P_to_location
         and hdr.from_location  = P_from_location;
   ---
   cursor C_GET_TSF_COST (P_distro_no FM_STG_ASNOUT_DISTRO.DISTRO_NBR%TYPE,
                          P_item      FM_STG_ASNOUT_ITEM.ITEM_ID%TYPE)is
      select d.tsf_cost,
             d.tsf_price,
             d.tsf_seq_no
        from tsfdetail d,
             tsfhead_l10n_ext tle
       where tle.tsf_no = P_distro_no
         and tle.tsf_no = d.tsf_no
         and d.item     = P_item
         UNION ALL
      select (((iscl.unit_cost * pb.pack_item_qty) / tot_pack.unit_cost_pack) * d.tsf_cost) / pb.pack_item_qty tsf_cost,
             (((iscl.unit_cost * pb.pack_item_qty) / tot_pack.unit_cost_pack) * d.tsf_price) / pb.pack_item_qty tsf_price,
             d.tsf_seq_no
        from tsfdetail d,
             tsfhead_l10n_ext tle,
             item_supp_country_loc iscl,
             packitem_breakout pb,
            (select pb2.pack_no,
                    isc2.loc,
                    SUM(pb2.pack_item_qty * isc2.unit_cost) unit_cost_pack,
                    SUM(pb2.pack_item_qty) tot_item_pack
               from packitem_breakout pb2,
                    item_supp_country_loc isc2
              where isc2.item = pb2.item
          group by pb2.pack_no,
                   isc2.loc) tot_pack
       where tle.tsf_no = P_distro_no
         and tle.tsf_no = d.tsf_no
         and d.item = pb.pack_no
         and pb.pack_no = tot_pack.pack_no
         and pb.item = iscl.item
         and iscl.loc = tot_pack.loc
         and pb.item = P_item;
   ---
   cursor C_GET_STORE_ENTITY_ID (P_location  FM_STG_ASNOUT_DESC.FROM_LOCATION%TYPE) is
      select st.tsf_entity_id
        from store st
       where st.store = P_location;
   ---
   cursor C_GET_WH_ENTITY_ID (P_location  FM_STG_ASNOUT_DESC.FROM_LOCATION%TYPE) is
      select wh.tsf_entity_id
        from wh wh
       where wh.wh in (select primary_vwh from wh where wh=P_location);
   ---    
   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = L_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   O_status_code :=0;
   if I_seq_no is NOT NULL or I_seq_no != 0 then
      ---Get the default freight type
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_freight_type,
                                            L_dummy_date,
                                            C_CHAR_TYPE,
                                            C_FREIGHT_TYPE) = FALSE then
         return FALSE;
      end if;

      if I_requisition_type = 'RTV' then
         ---
         if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                               L_dummy_desc,
                                               L_dummy_number,
                                               L_utilization_id,
                                               L_dummy_date,
                                               C_CHAR_TYPE,
                                               C_RTV_UTIL_ID) = FALSE then
            return FALSE;
         end if;
         ---
         open C_GET_RTV_HDR;
         fetch C_GET_RTV_HDR into L_rtv_rec_hdr;
         L_new_status := C_PROCESS_STATUS;
         ---
         if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                         L_loc_type,
                                         L_rtv_rec_hdr.dc_dest_id) = FALSE then
            L_new_status := C_ERROR_STATUS;
            return FALSE;
         end if;
         ---
         if L_new_status != C_ERROR_STATUS then
            ---Get the Default Document Type for the Location and Utilization
            if GET_TYPE_ID(O_error_message,
                           L_type_id,
                           L_rtv_rec_hdr.dc_dest_id,
                           L_utilization_id,
                           I_requisition_type) = FALSE then
               return FALSE;
            end if;
            ---
            if FM_FISCAL_DOC_TYPE_SQL.EXISTS(O_error_message,
                                             L_exists,
                                             L_dummy_desc,
                                             L_type_id) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_DOC_TYPE',NULL,L_program,NULL);
               return FALSE;
            end if;
            ---
            if FM_DOC_TYPE_UTILIZATION_SQL.EXISTS_UTILIZATION(O_error_message,
                                                              L_exists,
                                                              L_type_id,
                                                              L_utilization_id) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_INV_UTIL_SETUP',L_type_id,L_utilization_id,L_program);
               return FALSE;
            end if;
            ---
            if FM_FISCAL_UTILIZATION_SQL.EXISTS_UTILIZATION_REQ_TYPE(O_error_message,
                                                                     L_exists,
                                                                     I_requisition_type,
                                                                     L_utilization_id) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_INV_UTIL',NULL,L_program,NULL);
               return FALSE;
            end if;
            ---Get the schedule mode
            if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                                  L_dummy_desc,
                                                  L_dummy_number,
                                                  L_schedule_mode,
                                                  L_dummy_date,
                                                  'CHAR',
                                                  'EDI_'||I_requisition_type) = FALSE then
               return FALSE;
            end if;
            --Generates one Schedule number for each fiscal document generated
            if FM_SCHEDULE_SQL.CREATE_NEW_SCHEDULE(O_error_message,
                                                   L_schedule_no,
                                                   L_rtv_rec_hdr.dc_dest_id,
                                                   L_loc_type,
                                                   L_schedule_mode) = FALSE then
               return FALSE;
            end if;
         end if;   --L_new_status
         ---
         L_fiscal_doc_head_rec.L_fiscal_head_rec.location_id    := L_rtv_rec_hdr.dc_dest_id;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.location_type  := L_loc_type;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.schedule_no    := L_schedule_no;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.utilization_id := L_utilization_id;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.module         := 'SUPP';
         L_fiscal_doc_head_rec.L_fiscal_head_rec.key_value_1    := L_rtv_rec_hdr.vendor_nbr;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.key_value_2    := C_KEY_VALUE_2;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.partner_type   := NULL;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.partner_id     := NULL;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.document_type  := NULL;

         open C_GET_RTV_LINES(L_rtv_rec_hdr.rtv_id,
                              L_rtv_rec_hdr.dc_dest_id,
                              L_rtv_rec_hdr.vendor_nbr);

         LOOP
            fetch C_GET_RTV_LINES into L_rtv_lines_tab(k);
            exit when C_GET_RTV_LINES%NOTFOUND;
            k := k + 1;
         END LOOP;

         if k > 0 then
            FOR k in L_rtv_lines_tab.first .. L_rtv_lines_tab.last LOOP

               if NVL(L_rtv_lines_tab(k).weight_uom,'-1') <> NVL(L_weight_uom_temp,'-1') and L_multiple_weight_uom_ind <> 'Y' then
                  if NVL(L_weight_uom_temp,'-1') <> '-999' then
                     L_multiple_weight_uom_ind := 'Y';
                     EXIT;
                  end if;
                  L_weight_uom_temp := L_rtv_lines_tab(k).weight_uom;
               end if;

            END LOOP;
         end if;
         if L_multiple_weight_uom_ind = 'Y' then
            L_fiscal_doc_head_rec.L_fiscal_head_rec.unit_type       := L_weight_uom;
         end if;
         if k > 0 then
            FOR k in L_rtv_lines_tab.first .. L_rtv_lines_tab.last LOOP
               ---
               open C_CHK_PACK_IND(L_rtv_lines_tab(k).item_id);
               fetch C_CHK_PACK_IND into L_pack_ind;
               close C_CHK_PACK_IND;

               if L_pack_ind = 'Y' then
                  L_pack_level := NULL;
                  open C_CHK_PO_REF(L_loc_type,
                                    L_rtv_rec_hdr.dc_dest_id,
                                    L_rtv_lines_tab(k).item_id);
                  fetch C_CHK_PO_REF into L_pack_level,L_fiscal_doc_id_ref;
                  close C_CHK_PO_REF;


                  if NVL(L_pack_level,'N') = 'N' then
                    j := 0;
                    L_rtv_pack_lines_tab.delete;
                     open C_PACK_EXPLODE(L_rtv_lines_tab(k).item_id,
                                         L_rtv_lines_tab(k).unit_qty);
                     LOOP

                        fetch C_PACK_EXPLODE into L_rtv_pack_lines_tab(j);
                        exit when C_PACK_EXPLODE%NOTFOUND;
                        j := j+1;
                     END LOOP;
                     close C_PACK_EXPLODE;

                     FOR j in L_rtv_pack_lines_tab.first .. L_rtv_pack_lines_tab.last LOOP
                        if FM_FISCAL_DETAIL_VAL_SQL.GET_ITEM_CLASSIFICATION(O_error_message,
                                                                            L_classif_id,
                                                                            L_classif_desc,
                                                                            L_rtv_pack_lines_tab(j).item_id,
                                                                            L_rtv_rec_hdr.vendor_nbr,
                                                                            L_rtv_rec_hdr.dc_dest_id,
                                                                            L_loc_type,
                                                                            L_create_datetime) = FALSE then
                           L_new_status := C_ERROR_STATUS;
                        end if;

                        if L_classif_id is NULL then
                           O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_CLASS_FISC_REQ',
                                                                       NULL,
                                                                       NULL,
                                                                       NULL);
                           L_new_status := C_ERROR_STATUS;
                           return FALSE;
                        end if;

                        if GET_UNIT_COST(O_error_message,
                                         L_unit_cost,
                                         L_fiscal_doc_line_id_ref,
                                         L_fiscal_doc_id_ref,
                                         L_loc_type,
                                         L_rtv_rec_hdr.dc_dest_id,
                                         L_rtv_lines_tab(k).item_id,--pack_no
                                         L_rtv_pack_lines_tab(j).item_id,--component item
                                         NVL(L_pack_level,'N'),
                                         I_requisition_type) = FALSE then
                           return FALSE;
                        end if;

                        L_total_cost := (L_unit_cost * L_rtv_pack_lines_tab(j).unit_qty);
                        l:=l+1;
                        L_line_no := L_line_no + 1;
                        L_rtv_qty := L_rtv_qty + L_rtv_pack_lines_tab(j).unit_qty;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).line_no                := L_line_no;--L_rtv_lines_tab(k).line_no;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).requisition_no         := L_rtv_lines_tab(k).rtv_id;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).item                   := L_rtv_pack_lines_tab(j).item_id;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).classification_id      := L_classif_id;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).quantity               := L_rtv_pack_lines_tab(j).unit_qty;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).unit_cost              := L_unit_cost;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).total_cost             := L_total_cost;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_line_id_ref := L_fiscal_doc_line_id_ref;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_id_ref      := L_fiscal_doc_id_ref;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_no                := L_rtv_lines_tab(k).item_id;
                        L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_ind               := 'N';
                        L_header_cost := L_header_cost + L_total_cost;
                     END LOOP;  --for j
                  else
                     L_classif_id := NULL;
                     L_pack_no    := NULL;
                  end if;   --L_pack_level
               else
                  if FM_FISCAL_DETAIL_VAL_SQL.GET_ITEM_CLASSIFICATION(O_error_message,
                                                                      L_classif_id,
                                                                      L_classif_desc,
                                                                      L_rtv_lines_tab(k).item_id,
                                                                      L_rtv_rec_hdr.vendor_nbr,
                                                                      L_rtv_rec_hdr.dc_dest_id,
                                                                      L_loc_type,
                                                                      L_create_datetime) = FALSE then
                     ---
                     L_new_status := C_ERROR_STATUS;
                  end if;
                  ---
                  if L_classif_id is NULL then
                     O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_CLASS_FISC_REQ',
                                                                 NULL,
                                                                 NULL,
                                                                 NULL);
                     ---
                     L_new_status := C_ERROR_STATUS;
                     return FALSE;
                  end if;
               end if;  --L_pack_ind = 'Y'
               ---
               if L_pack_level = 'Y' or L_pack_ind ='N' then
                  if GET_UNIT_COST(O_error_message,
                                   L_unit_cost,
                                   L_fiscal_doc_line_id_ref,
                                   L_fiscal_doc_id_ref,
                                   L_loc_type,
                                   L_rtv_rec_hdr.dc_dest_id,
                                   L_rtv_lines_tab(k).item_id,
                                   NULL,
                                   NULL,
                                   I_requisition_type) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  L_total_cost := (L_unit_cost * L_rtv_lines_tab(k).unit_qty);
                  l:=l+1;
                  L_line_no := L_line_no + 1;
                  L_rtv_qty := L_rtv_qty + L_rtv_lines_tab(k).unit_qty;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).line_no                := L_line_no;--L_rtv_lines_tab(k).line_no;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).requisition_no         := L_rtv_lines_tab(k).rtv_id;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).item                   := L_rtv_lines_tab(k).item_id;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).classification_id      := L_classif_id;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).quantity               := L_rtv_lines_tab(k).unit_qty;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).unit_cost              := L_unit_cost;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).total_cost             := L_total_cost;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_line_id_ref := L_fiscal_doc_line_id_ref;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_id_ref      := L_fiscal_doc_id_ref;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_no                := L_pack_no;
                  L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_ind               := L_pack_ind;
                  L_header_cost := L_header_cost + L_total_cost;
               end if;   -- L_pack_level
               if UOM_SQL.CONVERT(O_error_message,
                                  L_weight_cnvt,
                                  L_weight_uom,
                                  L_rtv_lines_tab(k).weight,
                                  L_rtv_lines_tab(k).weight_uom,
                                  L_rtv_lines_tab(k).item_id,
                                  NULL,
                                  NULL) = FALSE then
                  return FALSE;
               end if;
               L_total_weight := L_total_weight + L_weight_cnvt;
            END LOOP;  --for k
         end if;  --if k>0

         L_fiscal_doc_head_rec.L_fiscal_head_rec.quantity := L_rtv_qty;
         close C_GET_RTV_LINES;
         close C_GET_RTV_HDR;
      end if;   --requisition_type = 'RTV'
      ---
      if I_requisition_type = 'STOCK' then

         open C_GET_ADJ_HDR;
         fetch C_GET_ADJ_HDR into L_adj_rec_hdr;

         L_new_status := C_PROCESS_STATUS;
         ---
         if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                         L_loc_type,
                                         L_adj_rec_hdr.dc_dest_id) = FALSE then
            L_new_status := C_ERROR_STATUS;
            return FALSE;
         end if;
         ---
         if L_new_status != C_ERROR_STATUS then
            ---Get the Default Document Type for the Location and Utilization
            if GET_TYPE_ID(O_error_message,
                           L_type_id,
                           L_adj_rec_hdr.dc_dest_id,
                           L_adj_rec_hdr.util_id,
                           I_requisition_type) = FALSE then
               return FALSE;
            end if;
            if FM_FISCAL_DOC_TYPE_SQL.EXISTS(O_error_message,
                                             L_exists,
                                             L_dummy_desc,
                                             L_type_id) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_DOC_TYPE',NULL,L_program,NULL);
               return FALSE;
            end if;
            ---
            if FM_DOC_TYPE_UTILIZATION_SQL.EXISTS_UTILIZATION(O_error_message,
                                                              L_exists,
                                                              L_type_id,
                                                              L_adj_rec_hdr.util_id) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_INV_UTIL_SETUP',L_type_id,L_adj_rec_hdr.util_id,L_program);
               return FALSE;
            end if;
            ---
            if FM_FISCAL_UTILIZATION_SQL.EXISTS_UTILIZATION_REQ_TYPE(O_error_message,
                                                                     L_exists,
                                                                     I_requisition_type,
                                                                     L_adj_rec_hdr.util_id) = FALSE then
               return FALSE;
            end if;

            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_INV_UTIL',NULL,L_program,NULL);
               return FALSE;
            end if;

            ---Get the schedule mode
            if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                                  L_dummy_desc,
                                                  L_dummy_number,
                                                  L_schedule_mode,
                                                  L_dummy_date,
                                                  'CHAR',
                                                  'EDI_'||I_requisition_type) = FALSE then
               return FALSE;
            end if;

            --Generates one Schedule number for each fiscal document generated
            if FM_SCHEDULE_SQL.CREATE_NEW_SCHEDULE(O_error_message,
                                                   L_schedule_no,
                                                   L_adj_rec_hdr.dc_dest_id,
                                                   L_loc_type,
                                                   L_schedule_mode) = FALSE then
               return FALSE;
            end if;
         end if;  --L_new_status
         ---
         L_fiscal_doc_head_rec.L_fiscal_head_rec.location_id    := L_adj_rec_hdr.dc_dest_id;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.location_type  := L_loc_type;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.schedule_no    := L_schedule_no;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.utilization_id := L_adj_rec_hdr.util_id;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.module         := 'LOC';
         L_fiscal_doc_head_rec.L_fiscal_head_rec.key_value_1    := L_adj_rec_hdr.dc_dest_id;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.key_value_2    := L_loc_type;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.partner_type   := NULL;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.partner_id     := NULL;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.quantity       := L_adj_rec_hdr.total_qty;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.document_type  := NULL;

         open C_GET_ADJ_LINES(L_adj_rec_hdr.seq_no,
                              L_adj_rec_hdr.dc_dest_id,
                              L_adj_rec_hdr.util_id);

         LOOP
            fetch C_GET_ADJ_LINES into L_adj_lines_tab(k);
            exit when C_GET_ADJ_LINES%NOTFOUND;
            k := k + 1;
         END LOOP;

         if k > 0 then
            FOR k in L_adj_lines_tab.first .. L_adj_lines_tab.last LOOP

               if NVL(L_adj_lines_tab(k).weight_uom,'-1') <> NVL(L_weight_uom_temp,'-1') and L_multiple_weight_uom_ind <> 'Y' then
                  if NVL(L_weight_uom_temp,'-1') <> '-999' then
                     L_multiple_weight_uom_ind := 'Y';
                     EXIT;
                  end if;
                  L_weight_uom_temp := L_adj_lines_tab(k).weight_uom;
               end if;

            END LOOP;
         end if;
         if L_multiple_weight_uom_ind = 'Y' then
            L_fiscal_doc_head_rec.L_fiscal_head_rec.unit_type       := L_weight_uom;
         end if;
         if k > 0 then
            FOR k in L_adj_lines_tab.first ..L_adj_lines_tab.last LOOP
               if FM_FISCAL_DETAIL_VAL_SQL.GET_ITEM_CLASSIFICATION(O_error_message,
                                                                   L_classif_id,
                                                                   L_classif_desc,
                                                                   L_adj_lines_tab(k).item_id,
                                                                   L_adj_rec_hdr.dc_dest_id,
                                                                   L_adj_rec_hdr.dc_dest_id,
                                                                   L_loc_type,
                                                                   L_create_datetime) = FALSE then
                  L_new_status := C_ERROR_STATUS;
               end if;
               ---
               if L_classif_id is NULL then
                  O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_CLASS_FISC_REQ',
                                                              NULL,
                                                              NULL,
                                                              NULL);
                  L_new_status := C_ERROR_STATUS;
                  return FALSE;
               end if;
               ---
               if GET_UNIT_COST(O_error_message,
                                L_unit_cost,
                                L_fiscal_doc_line_id_ref,
                                L_fiscal_doc_id_ref,
                                L_loc_type,
                                L_adj_rec_hdr.dc_dest_id,
                                L_adj_lines_tab(k).item_id,
                                NULL,
                                NULL,
                                I_requisition_type) = FALSE then
                  return FALSE;
               end if;
               ---
               if UOM_SQL.CONVERT(O_error_message,
                                  L_weight_cnvt,
                                  L_weight_uom,
                                  L_adj_lines_tab(k).weight,
                                  L_adj_lines_tab(k).weight_uom,
                                  L_adj_lines_tab(k).item_id,
                                  NULL,
                                  NULL) = FALSE then
                  return FALSE;
               end if;
               L_total_weight := L_total_weight + L_weight_cnvt;
               ---
               L_total_cost := (L_unit_cost * L_adj_lines_tab(k).unit_qty);
               l:=l+1;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).line_no                := L_adj_lines_tab(k).line_no;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).requisition_no         := L_adj_lines_tab(k).seq_no;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).item                   := L_adj_lines_tab(k).item_id;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).classification_id      := L_classif_id;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).quantity               := L_adj_lines_tab(k).unit_qty;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).unit_cost              := L_unit_cost;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).total_cost             := L_total_cost;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_line_id_ref := NULL;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_id_ref      := NULL;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_no                := L_adj_lines_tab(k).pack_no;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_ind               := L_adj_lines_tab(k).pack_ind;
               L_header_cost := L_header_cost + L_total_cost;
               ---
            END LOOP;  --for k
         end if; --if k>0
         close C_GET_ADJ_LINES;
         close C_GET_ADJ_HDR;
      end if;  --requisition_type = 'STOCK'
      ---
      if I_requisition_type in ('TSF', 'REP', 'IC') then
         open C_GET_TSF_HDR;
         fetch C_GET_TSF_HDR into L_tsf_rec_hdr;
         ---
         L_new_status := C_PROCESS_STATUS;

         ---Get the type for the from_location
         if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                         L_from_loc_type,
                                         L_tsf_rec_hdr.from_location) = FALSE then
            L_new_status := C_ERROR_STATUS;
            return FALSE;
         end if;

         ---Get the type for the to_location
         if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                         L_to_loc_type,
                                         L_tsf_rec_hdr.to_location) = FALSE then
            L_new_status := C_ERROR_STATUS;
            return FALSE;
         end if;
         ---
         open C_GET_STATE (C_INT_MODULE, L_tsf_rec_hdr.from_location, L_from_loc_type);
         fetch C_GET_STATE into L_from_state;
         ---
         if C_GET_STATE%NOTFOUND then
            L_new_status := C_ERROR_STATUS;
         end if;
         ---
         close C_GET_STATE;
         ---
         if (L_to_loc_type = 'E') then
            open C_GET_STATE (C_EXT_MODULE, L_tsf_rec_hdr.to_location, L_to_loc_type);
            fetch C_GET_STATE into L_to_state;
            ---
            if C_GET_STATE%NOTFOUND then
               L_new_status := C_ERROR_STATUS;
            end if;
            ---
            close C_GET_STATE;
         else
            open C_GET_STATE (C_INT_MODULE, L_tsf_rec_hdr.to_location, L_to_loc_type);
            fetch C_GET_STATE into L_to_state;
            ---
            if C_GET_STATE%NOTFOUND then
               L_new_status := C_ERROR_STATUS;
            end if;
            ---
            close C_GET_STATE;
         end if;
         ---
         if (L_from_loc_type = 'S') then
            ---
            open C_GET_STORE_ENTITY_ID (L_tsf_rec_hdr.from_location);
            fetch C_GET_STORE_ENTITY_ID into L_from_ent_id;
            ---
            if C_GET_STORE_ENTITY_ID%NOTFOUND then
               L_new_status := C_ERROR_STATUS;
            end if;
            ---
            close C_GET_STORE_ENTITY_ID;
         end if;
         ---
         if (L_to_loc_type = 'S') then
            open C_GET_STORE_ENTITY_ID (L_tsf_rec_hdr.to_location);
            fetch C_GET_STORE_ENTITY_ID into L_to_ent_id;
            ---
            if C_GET_STORE_ENTITY_ID%NOTFOUND then
               L_new_status := C_ERROR_STATUS;
            end if;
            ---
            close C_GET_STORE_ENTITY_ID;
         end if;
         ---
         if (L_from_loc_type = 'W') then
            ---
            open C_GET_WH_ENTITY_ID (L_tsf_rec_hdr.from_location);
            fetch C_GET_WH_ENTITY_ID into L_from_ent_id;
            ---
            if C_GET_WH_ENTITY_ID%NOTFOUND then
               L_new_status := C_ERROR_STATUS;
            end if;
            ---
            close C_GET_WH_ENTITY_ID;
         end if;
         ---
         if (L_to_loc_type = 'W') then
            open C_GET_WH_ENTITY_ID (L_tsf_rec_hdr.to_location);
            fetch C_GET_WH_ENTITY_ID into L_to_ent_id;
            ---
            if C_GET_WH_ENTITY_ID%NOTFOUND then
               L_new_status := C_ERROR_STATUS;
            end if;
            ---
            close C_GET_WH_ENTITY_ID;
         end if;
         ---
         if L_new_status = C_ERROR_STATUS then
            O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_FISCAL_ATTR',
                                                        NULL,
                                                        NULL,
                                                        NULL);
            return FALSE;
         end if;
         ---
         open C_GET_TSF_UTIL_ID (L_tsf_rec_hdr.seq_no,L_tsf_rec_hdr.from_location,L_tsf_rec_hdr.to_location);
         fetch C_GET_TSF_UTIL_ID into L_utilization_id,L_requisition_type,L_to_loc,L_from_loc;
         ---
         if C_GET_TSF_UTIL_ID%NOTFOUND then
            ---
            open C_GET_ALLOC_DTL (L_tsf_rec_hdr.seq_no,L_tsf_rec_hdr.from_location,L_tsf_rec_hdr.to_location);
            fetch C_GET_ALLOC_DTL into L_to_loc,L_from_loc;
            ---
            if (L_to_state = L_from_state and L_from_ent_id = L_to_ent_id) then
               ---Get the default utilization cfop for inner state operations.
               if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                                     L_dummy_desc,
                                                     L_dummy_number,
                                                     L_utilization_id,
                                                     L_dummy_date,
                                                     C_CHAR_TYPE,
                                                     C_OUTBOUND_UTIL_ID) = FALSE then
                  return FALSE;
               end if;
            else
               ---Get the default utilization cfop for inter state operations.
               if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                                     L_dummy_desc,
                                                     L_dummy_number,
                                                     L_utilization_id,
                                                     L_dummy_date,
                                                     C_CHAR_TYPE,
                                                     C_OUTBOUND_IC_UTIL_ID) = FALSE then
                  return FALSE;
               end if;
            end if;

            if C_GET_ALLOC_DTL%FOUND then
               if (L_to_state = L_from_state and L_from_ent_id = L_to_ent_id) then
                  L_requisition_type := 'TSF';
               else
                  L_requisition_type := 'IC';
               end if;
            else
               L_requisition_type := 'REP';
            end if;
            close C_GET_ALLOC_DTL;
            ---
            if I_requisition_type = 'REP' then
               if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                                     L_dummy_desc,
                                                     L_dummy_number,
                                                     L_utilization_id,
                                                     L_dummy_date,
                                                     C_CHAR_TYPE,
                                                     C_OUTBOUND_REP_UTIL_ID) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;  --if C_GET_TSF_UTIL_ID
         close C_GET_TSF_UTIL_ID;

         if L_requisition_type = 'REP' and L_to_loc_type !='E' then
            if (L_to_state = L_from_state and L_from_ent_id = L_to_ent_id) then
               L_requisition_type := 'TSF';
            else
               L_requisition_type := 'IC';
            end if;
         end if;   

         if L_new_status != C_ERROR_STATUS then
            ---Get the Default Document Type for the Location and Utilization
            if GET_TYPE_ID(O_error_message,
                           L_type_id,
                           L_tsf_rec_hdr.from_location,
                           L_utilization_id,
                           NVL(L_requisition_type,I_requisition_type)) = FALSE then
               return FALSE;
            end if;
            if FM_FISCAL_DOC_TYPE_SQL.EXISTS(O_error_message,
                                               L_exists,
                                               L_dummy_desc,
                                               L_type_id) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_DOC_TYPE',NULL,L_program,NULL);
               return FALSE;
            end if;
            ---
            if FM_DOC_TYPE_UTILIZATION_SQL.EXISTS_UTILIZATION(O_error_message,
                                                              L_exists,
                                                              L_type_id,
                                                              L_utilization_id) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_INV_UTIL_SETUP',L_type_id,L_utilization_id,L_program);
               return FALSE;
            end if;
            ---
            if FM_FISCAL_UTILIZATION_SQL.EXISTS_UTILIZATION_REQ_TYPE(O_error_message,
                                                                     L_exists,
                                                                     L_requisition_type,
                                                                     L_utilization_id) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_exists then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_INV_UTIL',NULL,L_program,NULL);
               return FALSE;
            end if;

            ---Get the schedule mode
            if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                                  L_dummy_desc,
                                                  L_dummy_number,
                                                  L_schedule_mode,
                                                  L_dummy_date,
                                                  'CHAR',
                                                  'EDI_'||I_requisition_type) = FALSE then
               return FALSE;
            end if;

            --Generates one Schedule number for each fiscal document generated
            if FM_SCHEDULE_SQL.CREATE_NEW_SCHEDULE(O_error_message,
                                                   L_schedule_no,
                                                   L_tsf_rec_hdr.from_location,
                                                   L_from_loc_type,
                                                   L_schedule_mode) = FALSE then
               return FALSE;
            end if;
         end if;  --if L_new_status
         ---
         if L_to_loc_type ='E' then
            L_fiscal_doc_head_rec.L_fiscal_head_rec.module      := C_EXT_MODULE;
         else
            L_fiscal_doc_head_rec.L_fiscal_head_rec.module      := C_INT_MODULE;
         end if;

         L_fiscal_doc_head_rec.L_fiscal_head_rec.location_id     := L_tsf_rec_hdr.from_location;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.location_type   := L_from_loc_type;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.schedule_no     := L_schedule_no;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.utilization_id  := L_utilization_id;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.key_value_1     := L_tsf_rec_hdr.to_location;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.key_value_2     := L_to_loc_type;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.partner_type    := L_partner_type;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.partner_id      := L_partner_id;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.quantity        := L_tsf_rec_hdr.total_qty;
         L_fiscal_doc_head_rec.L_fiscal_head_rec.document_type   := L_tsf_rec_hdr.distro_doc_type;
         ---
         open C_GET_TSF_LINES(L_tsf_rec_hdr.seq_no,
                              L_tsf_rec_hdr.from_location,
                              L_tsf_rec_hdr.to_location);
         LOOP
            fetch C_GET_TSF_LINES into L_tsf_lines_tab(k);
            exit when C_GET_TSF_LINES%NOTFOUND;
            k := k + 1;
         END LOOP;

         if k > 0 then
            FOR k in L_tsf_lines_tab.first .. L_tsf_lines_tab.last LOOP

               if NVL(L_tsf_lines_tab(k).weight_uom,'-1') <> NVL(L_weight_uom_temp,'-1') and L_multiple_weight_uom_ind <> 'Y' then
                  if NVL(L_weight_uom_temp,'-1') <> '-999' then
                     L_multiple_weight_uom_ind := 'Y';
                     EXIT;
                  end if;
                  L_weight_uom_temp := L_tsf_lines_tab(k).weight_uom;
               end if;

            END LOOP;
         end if;
         if L_multiple_weight_uom_ind = 'Y' then
            L_fiscal_doc_head_rec.L_fiscal_head_rec.unit_type       := L_weight_uom;
         end if;
         if k > 0 then
            FOR k in L_tsf_lines_tab.first .. L_tsf_lines_tab.last LOOP
               ---
               if FM_FISCAL_DETAIL_VAL_SQL.GET_ITEM_CLASSIFICATION(O_error_message,
                                                                   L_classif_id,
                                                                   L_classif_desc,
                                                                   L_tsf_lines_tab(k).item_id,
                                                                   L_tsf_rec_hdr.to_location,
                                                                   L_tsf_rec_hdr.from_location,
                                                                   L_from_loc_type,
                                                                   L_create_datetime) = FALSE then
                  L_new_status := C_ERROR_STATUS;
               end if;
               ---
               if L_classif_id is NULL then
                  O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_CLASS_FISC_REQ',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  L_new_status := C_ERROR_STATUS;
                  return FALSE;
               end if;
               ---
               open C_GET_TSF_COST (L_tsf_lines_tab(k).distro_nbr,
                                    L_tsf_lines_tab(k).item_id);
               fetch C_GET_TSF_COST into L_tsf_cost,L_tsf_price,L_tsf_seq_no;
               close C_GET_TSF_COST;

               if L_tsf_price is not null then
                  L_tsf_cost := L_tsf_price;
               end if;

               if L_tsf_cost is null then
                  if GET_UNIT_COST(O_error_message,
                                   L_tsf_cost,
                                   L_fiscal_doc_line_id_ref,
                                   L_fiscal_doc_id_ref,
                                   L_from_loc_type,
                                   L_tsf_rec_hdr.from_location,
                                   L_tsf_lines_tab(k).item_id,
                                   NULL,
                                   NULL,
                                   NVL(L_requisition_type,I_requisition_type)) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_total_chrgs_prim,
                                                              L_profit_chrgs_to_loc,
                                                              L_exp_chrgs_to_loc,
                                                              L_tsf_lines_tab(k).distro_doc_type,
                                                              L_tsf_lines_tab(k).distro_nbr,
                                                              L_tsf_seq_no,
                                                              NULL,
                                                              NULL,
                                                              L_tsf_lines_tab(k).item_id,
                                                              NULL,
                                                              L_from_loc,
                                                              L_from_loc_type,
                                                              L_to_loc,
                                                              L_to_loc_type) = FALSE then
                  return FALSE;
               end if;
               if UOM_SQL.CONVERT(O_error_message,
                                  L_weight_cnvt,
                                  L_weight_uom,
                                  L_tsf_lines_tab(k).weight,
                                  L_tsf_lines_tab(k).weight_uom,
                                  L_tsf_lines_tab(k).item_id,
                                  NULL,
                                  NULL) = FALSE then
                  return FALSE;
               end if;
               L_total_weight := L_total_weight + L_weight_cnvt;
               ---
               L_unit_cost := L_tsf_cost + L_total_chrgs_prim;
               L_total_cost := (L_unit_cost * L_tsf_lines_tab(k).unit_qty);
               l:=l+1;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).line_no                := L_tsf_lines_tab(k).line_no;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).requisition_no         := L_tsf_lines_tab(k).distro_nbr;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).item                   := L_tsf_lines_tab(k).item_id;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).classification_id      := L_classif_id;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).quantity               := L_tsf_lines_tab(k).unit_qty;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).unit_cost              := L_unit_cost;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).total_cost             := L_total_cost;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_line_id_ref := NULL;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_id_ref      := NULL;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_no                := L_tsf_lines_tab(k).pack_no;
               L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_ind               := L_tsf_lines_tab(k).pack_ind;
               L_header_cost := L_header_cost + L_total_cost;
            END LOOP;  --for k
         end if;  --if k>0
         close C_GET_TSF_LINES;
         close C_GET_TSF_HDR;
      end if;  --requisition_type in ('TSF', 'REP', 'IC')

      if L_partner_type is NULL or L_partner_id is NULL then
         --- Get the Default Partner Type
         if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                               L_dummy_desc,
                                               L_dummy_number,
                                               L_partner_type,
                                               L_dummy_date,
                                               'CHAR',
                                               'EDI_DEF_PTNR_TYPE') = FALSE then
            return FALSE;
         end if;
         -- Get the Default Partner ID
         if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                               L_dummy_desc,
                                               L_dummy_number,
                                               L_partner_id,
                                               L_dummy_date,
                                               'CHAR',
                                               'EDI_DEF_PTNR_ID') = FALSE then
            return FALSE;
         end if;
      end if;

      L_fiscal_doc_id := 0;
      if FM_FISCAL_HEADER_VAL_SQL.GET_NEXT_FISCAL_DOC_ID(O_error_message,
                                                         L_fiscal_doc_id) = FALSE then
         L_new_status := C_ERROR_STATUS;
      end if;
      if FM_LOC_FISCAL_NUMBER_SQL.NEXT_NUMBER(O_error_message,
                                              L_fiscal_doc_no,
                                              L_series_no,
                                              L_subseries_no,
                                              L_fiscal_doc_head_rec.L_fiscal_head_rec.location_type,
                                              L_fiscal_doc_head_rec.L_fiscal_head_rec.location_id,
                                              L_create_datetime,
                                              L_type_id)  = FALSE then
         return FALSE;
      end if;
      ---
      if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_dummy_string,
                                            L_dummy_date,
                                            'CHAR',
                                            'HOUR_FORMAT') = FALSE then
         return FALSE;
      end if;
      if L_dummy_string = 'HH24:MI' then
         L_exit_hour := TO_DATE('00:00','HH24:MI');
      elsif L_dummy_string = 'HH:MI AM' then
         L_exit_hour := TO_DATE('12:00 AM','HH:MI AM');
      end if;
      if L_exit_hour is NOT NULL then  
         L_dummy := substr(to_timestamp(L_exit_hour),11,22);
         L_exit_hour := to_char(L_create_datetime,'DD-MON-YY') ||' '|| L_dummy;       
      end if;
      ---
      if L_new_status != C_ERROR_STATUS then
         insert into fm_fiscal_doc_header (fiscal_doc_id
                                         , location_id
                                         , location_type
                                         , fiscal_doc_no
                                         , series_no
                                         , subseries_no
                                         , schedule_no
                                         , status
                                         , type_id
                                         , utilization_id
                                         , requisition_type
                                         , module
                                         , key_value_1
                                         , key_value_2
                                         , issue_date
                                         , entry_or_exit_date
                                         , exit_hour
                                         , partner_type
                                         , partner_id
                                         , quantity
                                         , unit_type
                                         , freight_type
                                         , net_weight
                                         , total_weight
                                         , total_serv_value
                                         , total_serv_calc_value
                                         , total_item_value
                                         , total_item_calc_value
                                         , total_doc_value
                                         , total_doc_calc_value
                                         , freight_cost
                                         , insurance_cost
                                         , other_expenses_cost
                                         , extra_costs_calc
                                         , discount_type
                                         , total_discount_value
                                         , total_doc_value_with_disc
                                         , create_datetime
                                         , create_id
                                         , last_update_datetime
                                         , last_update_id
                                         , document_type)
                                   values( L_fiscal_doc_id
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.location_id
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.location_type
                                         , L_fiscal_doc_no
                                         , L_series_no
                                         , L_subseries_no
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.schedule_no
                                         , C_INITIAL_STATUS
                                         , L_type_id
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.utilization_id
                                         , NVL(L_requisition_type,I_requisition_type)
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.module
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.key_value_1
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.key_value_2
                                         , L_create_datetime
                                         , L_create_datetime
                                         , L_exit_hour
                                         , L_partner_type
                                         , L_partner_id
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.quantity
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.unit_type
                                         , L_freight_type
                                         , NULL
                                         , L_total_weight
                                         , 0
                                         , NULL --total_serv_calc_value
                                         , L_header_cost
                                         , NULL --total_item_calc_value
                                         , L_header_cost
                                         , NULL --total_doc_calc_value
                                         , 0
                                         , 0
                                         , 0
                                         , NULL -- extra_costs_calc
                                         , NULL
                                         , NULL
                                         , NULL -- total_doc_value_with_disc
                                         , L_create_datetime
                                         , L_create_id
                                         , L_create_datetime
                                         , L_create_id
                                         , L_fiscal_doc_head_rec.L_fiscal_head_rec.document_type);
         ---
         FOR l in L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab.first .. L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab.last LOOP
            insert into fm_fiscal_doc_detail( fiscal_doc_line_id
                                            , fiscal_doc_id
                                            , location_id
                                            , location_type
                                            , line_no
                                            , requisition_no
                                            , item
                                            , classification_id
                                            , quantity
                                            , unit_cost
                                            , unit_cost_with_disc
                                            , total_cost
                                            , total_calc_cost
                                            , freight_cost
                                            , net_cost
                                            , fiscal_doc_line_id_ref
                                            , fiscal_doc_id_ref
                                            , discount_type
                                            , discount_value
                                            , pack_no
                                            , pack_ind
                                            , create_datetime
                                            , create_id
                                            , last_update_datetime
                                            , last_update_id)
                                      values( fm_fiscal_doc_line_id_seq.nextval
                                            , L_fiscal_doc_id
                                            , L_fiscal_doc_head_rec.L_fiscal_head_rec.location_id
                                            , L_fiscal_doc_head_rec.L_fiscal_head_rec.location_type
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).line_no
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).requisition_no
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).item
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).classification_id
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).quantity
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).unit_cost
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).unit_cost
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).total_cost
                                            , NULL --total_calc_cost
                                            , 0
                                            , NULL
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_line_id_ref
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).fiscal_doc_id_ref
                                            , NULL
                                            , NULL
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_no
                                            , L_fiscal_doc_head_rec.L_fiscal_doc_dtl_tab(l).pack_ind
                                            , L_create_datetime
                                            , L_create_id
                                            , L_create_datetime
                                            , L_create_id);
         END LOOP;  --for l
      end if;
      ---
      if L_new_status = C_ERROR_STATUS then
         return FALSE;
      else
         if FM_INTEGRATION_SQL.UPDATE_STG_STATUS(O_error_message,
                                                 'P',
                                                 I_requisition_type,
                                                 I_seq_no,
                                                 L_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_new_status != C_ERROR_STATUS then
         if PROCESS_OUTBOUND(O_error_message,
                             O_status_code,
                             L_fiscal_doc_id,
                             L_fiscal_doc_head_rec.L_fiscal_head_rec.utilization_id,
                             L_fiscal_doc_head_rec.L_fiscal_head_rec.schedule_no,
                             NVL(L_requisition_type ,I_requisition_type),
                             'N',   --I_form_ind
                             I_wasteadj_ind) = FALSE then
            if FM_LOC_FISCAL_NUMBER_SQL.ROLLBACK_FISCAL_NO(O_error_message,
                                                           L_fiscal_doc_no,
                                                           L_fiscal_doc_head_rec.L_fiscal_head_rec.location_type,
                                                           L_fiscal_doc_head_rec.L_fiscal_head_rec.location_id,
                                                           L_type_id,
                                                           L_series_no) = FALSE then
               return FALSE;
            end if;
            O_status_code := 0;
            return FALSE;
         end if;
      end if;
   end if;  --I_seq_no

   O_status_code := 1;

   return TRUE;

EXCEPTION
   when OTHERS then
      if L_fiscal_doc_no is NOT NULL then
         if FM_LOC_FISCAL_NUMBER_SQL.ROLLBACK_FISCAL_NO(O_error_message,
                                                        L_fiscal_doc_no,
                                                        L_fiscal_doc_head_rec.L_fiscal_head_rec.location_type,
                                                        L_fiscal_doc_head_rec.L_fiscal_head_rec.location_id,
                                                        L_type_id,
                                                        L_series_no) = FALSE then
            return FALSE;
         end if;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if I_requisition_type = 'RTV' then
         if C_GET_RTV_HDR%ISOPEN then
            close C_GET_RTV_HDR;
         end if;
         ---
         if C_GET_RTV_LINES%ISOPEN then
            close C_GET_RTV_LINES;
         end if;
         ---
         return FALSE;
      end if;

      if I_requisition_type in ('TSF','IC','REP') then
         if C_GET_TSF_HDR%ISOPEN then
            close C_GET_TSF_HDR;
         end if;
         ---
         if C_GET_TSF_LINES%ISOPEN then
            close C_GET_TSF_LINES;
         end if;
         ---
         return FALSE;
      end if;

      if I_requisition_type = 'STOCK' then
         if C_GET_ADJ_HDR%ISOPEN then
            close C_GET_ADJ_HDR;
         end if;
         ---
         if C_GET_ADJ_LINES%ISOPEN then
            close C_GET_ADJ_LINES;
         end if;
         ---
         return FALSE;
      end if;
END PROCESS;
--------------------------------------------------------------------------------
END FM_EXIT_NF_CREATION_SQL;
/