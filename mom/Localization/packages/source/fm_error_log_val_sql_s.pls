create or replace PACKAGE FM_ERROR_LOG_VAL_SQL is

   -------------------------------------------------------
   -- Author  : META.GMF
   -- Created : 31/05/2007 16:22:46
   -- Purpose : form validation: orfm_error_log
   -------------------------------------------------------

   ---------------------------------------------------------------------------------
   -- Function Name: LOV_FISCAL
   -- Purpose:       List of values of Fiscal Docs
   ----------------------------------------------------------------------------------
   FUNCTION LOV_FISCAL(O_error_message IN OUT VARCHAR2,
                       O_query         IN OUT VARCHAR2,
                       I_fiscal_doc_no IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                       I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                       I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      return BOOLEAN;

   ---------------------------------------------------------------------------------
   -- Function Name: LOV_PROGRAM
   -- Purpose:       List of values of Programs
   ----------------------------------------------------------------------------------
   FUNCTION LOV_PROGRAM(O_error_message IN OUT VARCHAR2,
                        O_query         IN OUT VARCHAR2) return BOOLEAN;

   ---------------------------------------------------------------------------------
   -- Function Name: SET_WHERE_CLAUSE
   -- Purpose:       Where clause of the main block
   ----------------------------------------------------------------------------------
   FUNCTION SET_WHERE_CLAUSE(O_error_message IN OUT VARCHAR2,
                             O_where         IN OUT VARCHAR2,
                             I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE,
                             I_err_type      IN FM_ERROR_LOG.ERR_TYPE%TYPE,
                             I_err_status    IN FM_ERROR_LOG.ERR_STATUS%TYPE,
                             I_err_date      IN FM_ERROR_LOG.ERR_DATE%TYPE,
                             I_fiscal_doc_no IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                             I_schedule_no   IN FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                             I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                             I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                             I_source_type   IN FM_ERROR_LOG.ERR_SOURCE_TYPE%TYPE)
      return BOOLEAN;

   ---------------------------------------------------------------------------------
   FUNCTION SET_WHERE_CLAUSE(O_error_message IN OUT VARCHAR2,
                             O_where         IN OUT VARCHAR2,
                             I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                             I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      return BOOLEAN;
      
    ---------------------------------------------------------------------------------
    -- Function Name: LOV_FISCAL_NO
   -- Purpose:       List of values of Fiscal number
   ----------------------------------------------------------------------------------
   FUNCTION LOV_FISCAL_NO(O_error_message IN OUT VARCHAR2,
                          O_query         IN OUT VARCHAR2,
                          I_fiscal_doc_id IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                          I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      return BOOLEAN;

   ---------------------------------------------------------------------------------
   -- Function Name: GET_FISCAL_DOC_NO
   -- Purpose:       This function gets the fiscal document number.
   ----------------------------------------------------------------------------------
   FUNCTION GET_FISCAL_DOC_NO(O_error_message IN OUT VARCHAR2,
                              O_fiscal_doc_no IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                              O_fiscal_doc_type IN OUT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                              I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE)
      return BOOLEAN;

   ---------------------------------------------------------------------------------
   -- Function Name: LOV_SCHEDULE_NO
   -- Purpose:       List of values of Schedule number
   ----------------------------------------------------------------------------------
   FUNCTION LOV_SCHEDULE_NO(O_error_message IN OUT VARCHAR2,
                            O_query         IN OUT VARCHAR2,
                            I_sched_no      IN FM_SCHEDULE.SCHEDULE_NO%TYPE,
                            I_location      IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                            I_loc_type      IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE)
      return BOOLEAN;

   ---------------------------------------------------------------------------------
   -- Function Name: GET_SCHEDULE_NO
   -- Purpose:       This function gets the schedule number.
   ----------------------------------------------------------------------------------
   FUNCTION GET_SCHEDULE_NO(O_error_message IN OUT VARCHAR2,
                            O_schedule_no   IN OUT FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                            I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE)
      return BOOLEAN;

   ---------------------------------------------------------------------------------
   -- Function Name: EXIST_FISCAL_DOC_ID
   -- Purpose:       This function will check if exists fiscal_doc_id on table fm_fiscal_doc_header
   ----------------------------------------------------------------------------------
   FUNCTION EXIST_FISCAL_DOC_ID(O_error_message IN OUT VARCHAR2,
                                O_exist         IN OUT BOOLEAN,
                                I_fiscal_doc_id IN FM_ERROR_LOG.FISCAL_DOC_ID%TYPE)
      return BOOLEAN;
   -----------------------------------------------------------------------------------

end FM_ERROR_LOG_VAL_SQL;
/