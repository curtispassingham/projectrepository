CREATE OR REPLACE PACKAGE L10N_BR_FLEX_API_SQL AS
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
-- Function name:  VALIDATE_RIB_L10N_ATTRIB
-- Purpose      :  This function validates data in the RIB object extension hook.
-- Inputs       :  
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RIB_L10N_ATTRIB(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_10n_rib_obj  IN OUT L10N_OBJ)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  PERSIST_RIB_L10N_ATTRIB
-- Purpose      :  This function persist data in the RIB object extension hook to entity extension
--                 tables (e.g. item_country_l10n_ext).
-- Inputs       :  
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_RIB_L10N_ATTRIB(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_10n_rib_obj  IN OUT L10N_OBJ)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  BUILD_NAME_VALUE_PAIR
-- Purpose      :  This function returns a collection of name/value pairs for custom-defined
--                 Brazilian Localization attributes for the input entities. Supported entity 
--                 types are Item, Supplier, Locations (including store, warehouse and external
--                 finishers). For a single function call, all entities must be of the same type.
-- Inputs       :  I_entity_type (Type of entity to retrieve attributes for - 'ITEM', 'SUPP' (supplier),
--                                'LOC' (store, warehouse, external finishers))
--                 IO_entity_name_values (a collection of entities to retrieve name/value pairs for)
-- Outputs      :  IO_entity_name_values (a collection of name/value pairs for non-base attributes
--                                        associated with the entity keys)
---------------------------------------------------------------------------------------
FUNCTION BUILD_NAME_VALUE_PAIR(O_error_message        IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_entity_name_values  IN OUT NOCOPY  ENTITY_ATTRIB_TBL,
                               I_entity_type          IN             VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END L10N_BR_FLEX_API_SQL;
/