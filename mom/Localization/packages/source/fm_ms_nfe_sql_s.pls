CREATE OR REPLACE PACKAGE FM_MS_NFE_SQL IS
----------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code       IN OUT  NUMBER,
                  O_error_message     IN OUT  VARCHAR2,
                  I_message           IN      "OBJ_MS_RFM_NFE_REC");
--------------------------------------------------------------------------------
FUNCTION UPDATE_NFE_STG_STATUS(O_error_message  IN OUT  VARCHAR2,
                               I_status         IN      FM_STG_NFE.STATUS%TYPE,
                               I_fiscal_doc_id  IN      FM_STG_NFE.FISCAL_DOC_ID%TYPE)
  ---
return BOOLEAN;
--------------------------------------------------------------------------------
END FM_MS_NFE_SQL;
/
 
