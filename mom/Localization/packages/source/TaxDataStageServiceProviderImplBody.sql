CREATE OR REPLACE PACKAGE BODY TaxDataStageServiceProviderImp AS


/******************************************************************************
 *
 * Operation       : getTaxRequestData
 * Description     : Get staged tax data to send to thirdparty tax provider.
 *
 * Input           : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Output          : "RIB_FiscDocColRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscDocColRBM/v1
 * Description     : Tax data to send to thirdparty tax provider.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "Server"
                                        side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE getTaxRequestData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                            I_businessObject          IN  "RIB_ChannelRef_REC",
                            O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                            O_businessObject          OUT "RIB_FiscDocColRBM_REC")

IS

   L_status             "RIB_SuccessStatus_REC" := NULL;
   L_successStatus_tbl  "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail_rec           "RIB_FailStatus_REC"    := NULL;
   L_fail_tbl           "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
   L_tax_service_id     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_name               VARCHAR2(20);   
   L_error_msg          RTK_ERRORS.RTK_TEXT%TYPE;
   ----
   L_status_code        VARCHAR2(1);
   
BEGIN

   for rec in I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.FIRST..I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.LAST LOOP
      L_name := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).NAME;

      if L_name='invocationKey' then
         L_tax_service_id := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).VALUE;
      end if;
   end loop;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.GET_TAX_REQUEST_DATA(L_error_msg,
                                                        L_status_code,
                                                        O_businessObject,
                                                        L_tax_service_id) = FALSE then
      L_fail_rec := "RIB_FailStatus_REC"(0,L_error_msg,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
      return;
   end if;

   L_status := "RIB_SuccessStatus_REC"(0, 'getTaxRequestData service call was successful.');
   L_successStatus_tbl.EXTEND;
   L_successStatus_tbl(L_successStatus_tbl.count) := L_status;
   O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);

EXCEPTION
  when OTHERS then
     L_fail_rec := "RIB_FailStatus_REC"(0,SQLERRM,'error','error',null);
     L_fail_tbl.EXTEND;
     L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
     O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
END getTaxRequestData;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : getTaxRequestFoundationData
 * Description     : Get staged tax Foundation data to send to thirdparty tax provider.
 *
 * Input           : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Output          : "RIB_FiscalFDNQryRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscalFDNQryRBM/v1
 * Description     : Foundation data Request to send to thirdparty tax provider.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "Server"
                                        side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
  ******************************************************************************/

PROCEDURE getFiscalFdnData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                           I_businessObject          IN  "RIB_ChannelRef_REC",
                           O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                           O_businessObject          OUT "RIB_FiscalFDNQryRBM_REC")

IS

   L_status             "RIB_SuccessStatus_REC" := NULL;
   L_successStatus_tbl  "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail_rec           "RIB_FailStatus_REC"    := NULL;
   L_fail_tbl           "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
   L_tax_service_id     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_name               VARCHAR2(20);
   L_error_msg          RTK_ERRORS.RTK_TEXT%TYPE;
   ----
   L_status_code        VARCHAR2(1);

BEGIN

   for rec in I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.FIRST..I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.LAST LOOP
     L_name := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).NAME;

     if L_name='invocationKey' then
        L_tax_service_id := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).VALUE;
     end if;

   end loop;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.GET_FISCAL_FDN_DATA(L_error_msg,
                                                       L_status_code,
                                                       O_businessObject,
                                                       L_tax_service_id) = FALSE then
      L_fail_rec := "RIB_FailStatus_REC"(0,L_error_msg,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
      return; 
   end if;

   L_status := "RIB_SuccessStatus_REC"(0, 'getTaxRequestData service call was successful.');
   L_successStatus_tbl.EXTEND;
   L_successStatus_tbl(L_successStatus_tbl.count) := L_status;
   O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   
exception
   when others then
      L_fail_rec := "RIB_FailStatus_REC"(0, SQLERRM,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
END getFiscalFdnData;

/******************************************************************************/



/******************************************************************************
 *
 * Operation       : setTaxResponseData
 * Description     : Save tax data from thirdparty tax provider.

 *
 * Input           : "RIB_FiscDocTaxColRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscDocTaxColRBM/v1
 * Description     : Tax data from thirdparty provider.
 *
 * Output          : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "Server"
                    side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE setTaxResponseData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                             I_businessObject          IN  "RIB_FiscDocTaxColRBM_REC",
                             O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                             O_businessObject          OUT "RIB_ChannelRef_REC")

IS

   L_status             "RIB_SuccessStatus_REC" := NULL;
   L_successStatus_tbl  "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail_rec           "RIB_FailStatus_REC"    := NULL;
   L_fail_tbl           "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
   L_tax_service_id     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_name               VARCHAR2(20);
   L_error_msg          RTK_ERRORS.RTK_TEXT%TYPE;   
   ----
   L_status_code        VARCHAR2(1);
     
BEGIN

   L_status_code :='S';

   for rec in I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.FIRST..I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.LAST LOOP
     L_name := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).NAME;
     if L_name='invocationKey' then
        L_tax_service_id := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).VALUE;
     end if;
   end loop;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.SET_TAX_RESPONSE_DATA(L_error_msg,
                                                         L_status_code,
                                                         L_tax_service_id,
                                                         I_businessObject) = FALSE then
      L_fail_rec := "RIB_FailStatus_REC"(0,L_error_msg,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
      return; 
   end if; 
   
   O_businessObject := "RIB_ChannelRef_REC"(0,999);

   L_status := "RIB_SuccessStatus_REC"(0, 'getTaxRequestData service call was successful.');
   L_successStatus_tbl.EXTEND;
   L_successStatus_tbl(L_successStatus_tbl.count) := L_status;
   O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   
exception
when others then
      L_fail_rec := "RIB_FailStatus_REC"(0, SQLERRM,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
END setTaxResponseData;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : setTaxResponseFoundationData
 * Description     : Save tax Foundation data from thirdparty tax provider.

 *
 * Input           : "RIB_FiscalFDNColRBM_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscalFDNColRBM/v1
 * Description     : Tax data from thirdparty provider.
 *
 * Output          : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "Server"
                    side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
  ******************************************************************************/

PROCEDURE setFiscalFdnData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                           I_businessObject          IN  "RIB_FiscalFDNColRBM_REC",
                           O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                           O_businessObject          OUT "RIB_ChannelRef_REC")

IS

   L_status             "RIB_SuccessStatus_REC" := NULL;
   L_successStatus_tbl  "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail_rec           "RIB_FailStatus_REC"    := NULL;
   L_fail_tbl           "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
   L_tax_service_id     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_name               VARCHAR2(20);
   L_error_msg          RTK_ERRORS.RTK_TEXT%TYPE;
   ----
   L_status_code        VARCHAR2(1);

BEGIN

   L_status_code :='S';

   for rec in I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.FIRST..I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.LAST LOOP
      L_name := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).NAME;

      if L_name='invocationKey' then
         L_tax_service_id := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).VALUE;
      end if;
   end loop;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.SET_FISCAL_FDN_DATA(L_error_msg,
                                                       L_status_code,
                                                       L_tax_service_id,
                                                       I_businessObject) = FALSE then
      L_fail_rec := "RIB_FailStatus_REC"(0,L_error_msg,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
      return; 
   end if;

   O_businessObject := "RIB_ChannelRef_REC"(0,999);
   L_status := "RIB_SuccessStatus_REC"(0, 'getTaxRequestData service call was successful.');
   L_successStatus_tbl.EXTEND;
   L_successStatus_tbl(L_successStatus_tbl.count) := L_status;
   O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);

exception
when others then
      L_fail_rec := "RIB_FailStatus_REC"(0,SQLERRM,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
END setFiscalFdnData;

/******************************************************************************/



/******************************************************************************
 *
 * Operation       : getTaxCancelRequestData
 * Description     : get tax cancel data

 *
 * Input           : "RIB_FiscDocRBO_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FiscDocRBO/v1
 * Description     : Tax cancel data
 *
 * Output          : "RIB_ChannelRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ChannelRef/v1
 * Description     : This is not used at this time.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.TaxDataStageException
 * Description     : Throw this exception when an unknown "Server"
                    side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
    ******************************************************************************/

PROCEDURE cancelTaxTransactionData(I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                                   I_businessObject          IN  "RIB_ChannelRef_REC",
                                   O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                                   O_businessObject          OUT "RIB_FiscDocColRBM_REC")


IS

   L_status             "RIB_SuccessStatus_REC" := NULL;
   L_successStatus_tbl  "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail_rec           "RIB_FailStatus_REC"    := NULL;
   L_fail_tbl           "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
   L_tax_service_id     L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_name               VARCHAR2(20);
   L_error_msg          RTK_ERRORS.RTK_TEXT%TYPE;
   ----
   L_status_code        VARCHAR2(1);
   
BEGIN
  /* RTIL team to specify what code needs to be used here
   for rec in I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.FIRST..I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL.LAST LOOP
      L_name := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).NAME;

      if L_name='invocationKey' then
         L_tax_service_id := I_serviceOperationContext.MESSAGECONTEXT.PROPERTY_TBL(rec).VALUE;
      end if;
   end loop;


-- RFM changes to come here 

   status := "RIB_SuccessStatus_REC"(0, 'getTaxRequestData service call was successful.');
   successStatus_TBL.EXTEND;
   successStatus_TBL(1) := status;
   O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, successStatus_TBL);   */

   null;

----
exception
when others then
      L_fail_rec := "RIB_FailStatus_REC"(0,SQLERRM ,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
END cancelTaxTransactionData;

/******************************************************************************/

END TaxDataStageServiceProviderImp;
/