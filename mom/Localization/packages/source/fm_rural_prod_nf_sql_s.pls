create or replace PACKAGE FM_RURAL_PROD_NF_SQL is
---------------------------------------------------------------------------------
FUNCTION CREATE_NEW_NF(O_error_message  IN OUT VARCHAR2,
                       I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                       I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
 return BOOLEAN;
-----------------------------------------------------------------------------------
END FM_RURAL_PROD_NF_SQL;
/