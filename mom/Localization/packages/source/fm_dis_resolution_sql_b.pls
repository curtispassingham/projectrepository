CREATE OR REPLACE PACKAGE BODY FM_DIS_RESOLUTION_SQL IS
-----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Function Name: LOCK_RESOLUTION
-- Purpose:       This function Locks the record. This is for concurrency check
----------------------------------------------------------------------------------
FUNCTION LOCK_RESOLUTION (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_fiscal_doc_id  IN      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                          I_discrep_type   IN      FM_RESOLUTION.DISCREP_TYPE%TYPE)
   RETURN BOOLEAN IS
   L_program      VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOCK_RESOLUTION';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   CURSOR C_LOCK_RESOLUTION IS
      SELECT 'x'
        FROM FM_RESOLUTION
       WHERE fiscal_doc_id = I_fiscal_doc_id
         AND discrep_type  = I_discrep_type
         AND  rownum = 1;
         --FOR update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_RESOLUTION', 'FM_RESOLUTION','fiscal_doc_line_id '||I_fiscal_doc_id);
   open C_LOCK_RESOLUTION;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_RESOLUTION', 'FM_RESOLUTION','fiscal_doc_line_id '||I_fiscal_doc_id);
   close C_LOCK_RESOLUTION;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_RESOLUTION',
                                            I_fiscal_doc_id,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_RESOLUTION;
----------------------------------------------------------------------------------
-- Function Name: GET_MAIN_NF_LINE_FOR_COMPL_NF
-- Purpose:       This function gets the Main NF and NF line for complementory NF
----------------------------------------------------------------------------------
 FUNCTION GET_MAIN_NF_LINE_FOR_COMPL_NF(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_FISCAL_DOC_ID       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                   I_FISCAL_DOC_LINE_ID  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                   O_MAIN_FISCAL_DOC_ID  OUT     FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE,
                                   O_MAIN_FISCAL_DOC_LINE_ID  OUT     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN is

   L_program           VARCHAR2(50)  := 'FM_DIS_RESOLUTION_SQL.GET_MAIN_NF_FOR_COMPL_NF';
   L_key               VARCHAR2(100) := 'Fiscal_doc_id: ' ||I_fiscal_doc_id;
--
   l_main_nf           FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
   l_main_nf_line      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
--
   CURSOR C_GET_MAIN_NF is
      SELECT fiscal_doc_id_ref,
             fiscal_doc_line_id_ref
        FROM fm_fiscal_doc_detail fdd
       WHERE fdd.fiscal_doc_id = I_fiscal_doc_id
       and fdd.fiscal_doc_line_id = I_fiscal_doc_line_id;
--
BEGIN
--
   SQL_LIB.SET_MARK('OPEN', 'C_GET_MAIN_NF', 'fm_fiscal_doc_complement', L_key);
   OPEN C_GET_MAIN_NF;
   FETCH C_GET_MAIN_NF INTO l_main_nf,l_main_nf_line;
   CLOSE C_GET_MAIN_NF;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_MAIN_NF', 'fm_fiscal_doc_complement', L_key);
--
   o_main_fiscal_doc_id := l_main_nf;
   o_main_fiscal_doc_line_id := l_main_nf_line;
--
   return TRUE;
--
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END GET_MAIN_NF_LINE_FOR_COMPL_NF;
---------------------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_query          IN OUT VARCHAR2,
                           I_location_id    IN FM_EDI_DOC_DETAIL.LOCATION_ID%TYPE,
                           I_loc_type       IN FM_EDI_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_DIS_RESOLUTION_SQL.LOV_FISCAL_DOC_NO';
   ---
BEGIN
--
   O_query := 'select distinct fiscal_doc_no from fm_fiscal_doc_header where 1 = 1 ';
---
   if I_location_id is NOT NULL then
      O_query := O_query ||
                 'and location_id = '||I_location_id||' ';
   end if;
--
   if I_loc_type is NOT NULL then
      O_query := O_query ||
                 'and location_type = '''||I_loc_type||''' ';
   end if;
--
   O_query := O_query ||' order by fiscal_doc_no ';
--
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_FISCAL_DOC_NO;
----------------------------------------------------------------------------------
-- Function Name: LOV_VALIDATE_FISCAL_DOC_NO
-- Purpose:       This function validates the Fiscal Document number entered from form before opening the LOV
----------------------------------------------------------------------------------
FUNCTION LOV_VALIDATE_FISCAL_DOC_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_valid          IN OUT BOOLEAN,
                                    I_fiscal_doc_no  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOV_VALIDATE_FISCAL_DOC_NO';
--
   L_count     Number;
--
   cursor C_CHECK_FISCAL_DOC_NO is
      SELECT count(1)
        FROM fm_fiscal_doc_header
       WHERE fiscal_doc_no = I_fiscal_doc_no;
--
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_CHECK_FISCAL_DOC_NO','fm_fiscal_doc_header','fiscal_doc_no = '||I_fiscal_doc_no);
   OPEN  C_CHECK_FISCAL_DOC_NO;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_FISCAL_DOC_NO','fm_fiscal_doc_header','fiscal_doc_no = '||I_fiscal_doc_no);
   FETCH C_CHECK_FISCAL_DOC_NO INTO L_count;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_FISCAL_DOC_NO','fm_fiscal_doc_header','fiscal_doc_no = '||I_fiscal_doc_no);
   CLOSE C_CHECK_FISCAL_DOC_NO;
--
   if L_count > 0 then
      O_valid := TRUE;
   else
      O_valid := FALSE;
   end if;
--
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOV_VALIDATE_FISCAL_DOC_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_SCHEDULE_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_query          IN OUT VARCHAR2,
                         I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                         I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                         I_mode_type      IN     FM_SCHEDULE.MODE_TYPE%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOV_SCHEDULE_NO';
BEGIN
   ---
    O_query := 'Select schedule_no, mode_type, DECODE(mode_type,''ENT'',''Entry'',''EXIT'',''Exit'') mode_type_desc from fm_schedule fs where 1 = 1 ';
   ---
   if I_location is NOT NULL then
      O_query := O_query ||
                 'and fs.location_id = '||I_location||' ';
   end if;
   if I_loc_type is NOT NULL then
      O_query := O_query ||
                 'and fs.location_type = '''||I_loc_type||''' ';
   end if;
   if I_mode_type is NOT NULL then
      O_query := O_query ||' and mode_type = '''||I_mode_type||'''';
   end if;
   O_query := O_query ||' order by schedule_no ';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOV_SCHEDULE_NO;
----------------------------------------------------------------------------------
-- Function Name: LOV_VALIDATE_SCHED_NO
-- Purpose:       This function validates the schedule number entered from form before opening the LOV
----------------------------------------------------------------------------------
FUNCTION LOV_VALIDATE_SCHED_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid          IN OUT BOOLEAN,
                               I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOV_VALIDATE_SCHED_NO';
--
   L_count     Number;
    cursor C_CHECK_SCHED_NO is
      SELECT count(1)
        FROM fm_schedule
       WHERE schedule_no = I_schedule_no;
--
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_CHECK_SCHED_NO','fm_schedule','schedule_no = '||I_schedule_no);
   open C_CHECK_SCHED_NO;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SCHED_NO','fm_schedule','schedule_no = '||I_schedule_no);
   fetch C_CHECK_SCHED_NO into L_count;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SCHED_NO','fm_schedule','schedule_no = '||I_schedule_no);
   close C_CHECK_SCHED_NO;
--
   if L_count > 0 then
      O_valid := TRUE;
   else
      O_valid := FALSE;
   end if;
--
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOV_VALIDATE_SCHED_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_PO_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_query          IN OUT VARCHAR2,
                   I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                   I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                   I_supplier       IN     FM_SCHEDULE.MODE_TYPE%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOV_PO_NO';
BEGIN
   ---
    O_query := 'select ord.order_no po_number from ordhead ord where 1 = 1 ';
   ---
   if I_location is NOT NULL then
      O_query := O_query ||
                 'and ord.location = '||I_location||' ';
   end if;
   if I_loc_type is NOT NULL then
      O_query := O_query ||
                 'and ord.loc_type = '''||I_loc_type||''' ';
   end if;
   if I_supplier is NOT NULL then
      O_query := O_query ||' and supplier = '''||I_supplier||'''';
   end if;
   O_query := O_query ||' order by 1 ';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOV_PO_NO;
----------------------------------------------------------------------------------
-- Function Name: LOV_VALIDATE_PO_NO
-- Purpose:       This function validates the PO number entered from form before opening the LOV
----------------------------------------------------------------------------------
FUNCTION LOV_VALIDATE_PO_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT BOOLEAN,
                            I_po_no          IN     ORDHEAD.ORDER_NO%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOV_VALIDATE_PO_NO';
--
   L_count     Number;
    cursor C_CHECK_PO_NO is
      select count(1)
        from ordhead
       where order_no = I_po_no;
--
BEGIN
   open C_CHECK_PO_NO;
   fetch C_CHECK_PO_NO into L_count;
   close C_CHECK_PO_NO;
--
   if L_count > 0 then
      O_valid := TRUE;
   else
      O_valid := FALSE;
   end if;
--
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOV_VALIDATE_PO_NO;
-----------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOV_SUPPLIER';
   ---
BEGIN
   ---
   O_query := 'select a.sup_name, a.supplier, decode(b.taxpayer_type,''F'',b.cpf,b.cnpj) cpfcnpf '||
                'from sups a , v_br_sups b '||
               'where a.supplier = b.supplier '||
                 'and a.supplier_parent is NULL '||
                 'and LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
              'union all '||
              'select NVL(tl_shadow.translated_value, a.sup_name) sup_name, a.supplier, decode(b.taxpayer_type,''F'',b.cpf,b.cnpj) cpfcnpf '||
                'from sups a , v_br_sups b, tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and a.supplier = b.supplier '||
                 'and a.supplier_parent is NULL '||
                 'and UPPER(a.sup_name) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_SUPPLIER;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT BOOLEAN,
                           O_sups            IN OUT V_SUPS%ROWTYPE,
                           I_supplier        IN     SUPS.SUPPLIER%TYPE)
   return BOOLEAN is
   ---
   L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.VALIDATE_SUPPLIER';
   L_dummy             VARCHAR2(1);
   ---
   cursor LOV_SUPP is
      select a.sup_name, a.supplier_parent
        from sups a
       where a.supplier = I_supplier
         and a.supplier_parent is NULL;
   ---
BEGIN
   ---
   O_exists := TRUE;
   open LOV_SUPP;
   fetch LOV_SUPP into O_sups.sup_name, O_sups.supplier_parent;
   if LOV_SUPP%NOTFOUND then
      O_exists := FALSE;
   end if;
   close LOV_SUPP;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_SUPPLIER;
------------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER_SITE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOV_SUPPLIER_SITE';
   ---
BEGIN
   ---
   O_query := 'select sup_name ,supplier '||
                'from v_sups  '||
               'where supplier_parent is not null '||
                 'and sup_status=''A'' '||
                'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_SUPPLIER_SITE;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER_SITE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists          IN OUT BOOLEAN,
                                O_sups            IN OUT V_SUPS%ROWTYPE,
                                I_supplier_site   IN     SUPS.SUPPLIER%TYPE)
   return BOOLEAN is
   ---
   L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.VALIDATE_SUPPLIER_SITE';
   L_dummy             VARCHAR2(1);
   ---
   cursor LOV_SUPP_SITE is
      select *
        from v_sups a
       where a.supplier = I_supplier_site
         and a.supplier_parent is NOT NULL
         and a.sup_status = 'A';
   ---
BEGIN
   ---
   O_exists := TRUE;
   open LOV_SUPP_SITE;
   fetch LOV_SUPP_SITE into O_sups;
   if LOV_SUPP_SITE%NOTFOUND then
      O_exists := FALSE;
   end if;
   close LOV_SUPP_SITE;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_SUPPLIER_SITE;
------------------------------------------------------------------------------------
FUNCTION LOV_LOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_query          IN OUT VARCHAR2,
                 I_where_clause   IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.LOV_LOC';
   ---
BEGIN
   ---
   if I_where_clause = 'S' then
      O_query := 'select a.store_name loc_name, a.store loc, decode(b.taxpayer_type,''F'',b.cpf,b.cnpj) cpfcnpf '||
                   'from store a, v_br_store b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.store = b.store '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, a.store_name) loc_name, a.store loc, decode(b.taxpayer_type,''F'',b.cpf,b.cnpj) cpfcnpf '||
                   'from store a, v_br_store b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.store = b.store '||
                    'and UPPER(a.store) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                  'order by 1';
   elsif I_where_clause = 'W' then
      O_query := 'select a.wh_name loc_name, a.wh loc, decode(b.taxpayer_type,''F'',b.cpf,b.cnpj) cpfcnpf '||
                   'from wh a, v_br_wh b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.wh = b.wh '||
                  'union all '||
                 'select NVL(tl_shadow.translated_value, a.wh_name) loc_name, a.wh loc, decode(b.taxpayer_type,''F'',b.cpf,b.cnpj) cpfcnpf '||
                   'from wh a, v_br_wh b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.wh = b.wh '||
                    'and UPPER(a.wh) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                   'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_LOC;
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for block B_FM_DIS_RESOLUTION
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where_clause       OUT VARCHAR2,
                          I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_supplier           IN     SUPS.SUPPLIER%TYPE,
                          I_po_number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                          I_location_type      IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                          I_location_id        IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                          I_schedule_no        IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                          I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_NF_status          IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                          I_from_receipt_date  IN     FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                          I_to_receipt_date    IN     FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                          I_from_issue_date    IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                          I_to_issue_date      IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                          I_discrepancy_status IN     VARCHAR2,
                          I_cost_disc          IN     FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE,
                          I_qty_disc           IN     FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE,
                          I_tax_disc           IN     FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE)
   return BOOLEAN IS
   ---
   L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.SET_WHERE_CLAUSE';
   L_resolved          VARCHAR2(1)  := 'R';
   L_unresolved        VARCHAR2(1)  := 'D';
   ---
   I_cost_disc_1       FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE;
   I_qty_disc_1        FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE;
   I_tax_disc_1        FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE;
BEGIN
   O_where_clause := ' 1 = 1 and ';
   ---
   if I_fiscal_doc_id is NOT NULL then
      O_where_clause := ' fiscal_doc_id = ' || TO_CHAR(I_fiscal_doc_id) ||' and ';
   end if;
   ---
   if I_supplier is NOT NULL then
      O_where_clause := ' supplier = ' || TO_CHAR(I_supplier) ||' and ';
   end if;
   ---
   if I_po_number is NOT NULL then
         O_where_clause := ' po_number = ' || I_po_number ||' and ';
   end if;
   ---
   if I_location_type is NOT NULL then
      O_where_clause := O_where_clause || ' location_type = ''' || I_location_type ||''' and ';
   end if;
   ---
   if I_location_id is NOT NULL THEN
      O_where_clause := O_where_clause || ' location_id = ' || TO_CHAR(I_location_id) ||' and ';
   end if;
   ---
   if I_schedule_no is NOT NULL then
      O_where_clause := O_where_clause || ' schedule_no = '|| To_Char(I_schedule_no) || ' and ';
   end if;
   ---
   if I_fiscal_doc_no is NOT NULL then
      O_where_clause := O_where_clause || ' fiscal_doc_no = '|| to_char(I_fiscal_doc_no) ||' and ';
   end if;
   ---
   if I_nf_status is NOT NULL then
      O_where_clause := O_where_clause || ' nf_status = '''|| I_nf_status ||''' and ';
   end if;
   ---
   if I_from_receipt_date is NOT NULL then
      O_where_clause := O_where_clause || ' TRUNC(entry_or_exit_date) >= '''|| I_from_receipt_date ||''' and ';
      end if;
   ---
   if I_to_receipt_date is NOT NULL then
      O_where_clause := O_where_clause || ' TRUNC(entry_or_exit_date) <= '''|| I_to_receipt_date ||''' and ';
   end if;
   ---
   if I_from_issue_date is NOT NULL then
      O_where_clause := O_where_clause || ' TRUNC(issue_date) >= '''|| I_from_issue_date ||''' and ';
   end if;
   ---
   if I_to_issue_date is NOT NULL then
      O_where_clause := O_where_clause || ' TRUNC(issue_date) <= '''|| I_to_issue_date ||''' and ' ;
   end if;
   ---
   if I_discrepancy_status IS NOT NULL then
      if I_discrepancy_status = 'R' then
--
         if I_cost_disc = 'D' then
            I_cost_disc_1 := 'R';
         else
            I_cost_disc_1 := 'D';
         end if;
--
         if I_qty_disc = 'D' then
            I_qty_disc_1 := 'R';
         else
            I_qty_disc_1 := 'D';
         end if;
--
         if I_tax_disc = 'D' then
            I_tax_disc_1 := 'R';
         else
            I_tax_disc_1 := 'D';
         end if;
--
         if I_cost_disc_1 ='R' then
            O_where_clause := O_where_clause || ' cost_discrep_status = '''||I_cost_disc_1||'''  and ';
         end if;
   ---
         if I_qty_disc_1 ='R' then
            O_where_clause := O_where_clause || ' qty_discrep_status = '''||I_qty_disc_1||'''  and ';
         end if;
  ---
         if I_tax_disc_1 ='R' then
            O_where_clause := O_where_clause ||' ((tax_disc_status_header = '''||I_tax_disc_1||''') or '||
                                               '(tax_discrep_status = '''||I_tax_disc_1||''')) and ';
         end if;
         ---
      elsif I_discrepancy_status = 'U' then
         if I_cost_disc = 'D' then
            O_where_clause := O_where_clause || ' (cost_discrep_status = '''||I_cost_disc||''' )  and ';
         end if;
         ---
         if I_qty_disc = 'D' then
            O_where_clause := O_where_clause || ' (qty_discrep_status = '''||I_qty_disc||''' )  and ';
         end if;
         ---
         if I_tax_disc = 'D' then
            O_where_clause := O_where_clause ||'((tax_disc_status_header = '''||I_tax_disc||''') or '||
                                               '(tax_discrep_status = '''||I_tax_disc||''')) or ';
         end if;
         ---
      end if;
   end if;
   ---
   if O_where_clause is NOT NULL then
      O_where_clause := SUBSTR(O_where_clause, 1, NVL(LENGTH(O_where_clause), 0)-4);
   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE)||'where clause');
      return FALSE;
END SET_WHERE_CLAUSE;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_FISCAL_DOC_DETAIL
-- Purpose:       This function Updates FM_FISCAL_DOC_DETAIL discrepancy flags
----------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                  I_disc_type          IN FM_RESOLUTION.DISCREP_TYPE%TYPE,
                                  I_discrep_status     IN FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE,
                                  I_appt_qty           IN FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE)
   return BOOLEAN is
   L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DOC_DETAIL';
   L_key               VARCHAR2(80) := 'I_Fiscal_doc_line_id: ' || to_char(I_fiscal_doc_line_id) ||
                                       ' I_disc_type: '||I_disc_type;
   L_dummy             VARCHAR2(1);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_count             NUMBER := 0;
   cursor C_FM_FISCAL_DOC_DETAIL IS
   select 'x'
     from FM_FISCAL_DOC_DETAIL
    where fiscal_doc_line_id = I_fiscal_doc_line_id;
      --for update nowait;

   CURSOR C_GET_TAX_COUNT is
      SELECT COUNT(1)
        FROM fm_resolution fr
       WHERE fr.fiscal_doc_line_id = I_fiscal_doc_line_id
         AND fr.discrep_type = 'T'
         AND fr.resolution_type is null;
BEGIN
    SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
    open C_FM_FISCAL_DOC_DETAIL;
    SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
    fetch C_FM_FISCAL_DOC_DETAIL into L_dummy;
    SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
    close C_FM_FISCAL_DOC_DETAIL;
   ---
   if I_disc_type = 'Q' then
      SQL_LIB.SET_MARK('UPDATE','C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
      Update FM_FISCAL_DOC_DETAIL
         set qty_discrep_status = I_discrep_status,
             appt_qty = I_appt_qty,
             LAST_UPDATE_DATETIME = sysdate,
             LAST_UPDATE_ID = user
       where Fiscal_doc_line_id = I_fiscal_doc_line_id;
   elsif I_disc_type = 'C' then
      SQL_LIB.SET_MARK('UPDATE','C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
      Update FM_FISCAL_DOC_DETAIL
         set cost_discrep_status = I_discrep_status,
             LAST_UPDATE_DATETIME = sysdate,
             LAST_UPDATE_ID = user
       where Fiscal_doc_line_id = I_fiscal_doc_line_id;
   elsif I_disc_type = 'T' then
      SQL_LIB.SET_MARK('UPDATE','C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
--
      OPEN C_GET_TAX_COUNT;
      FETCH C_GET_TAX_COUNT INTO L_COUNT;
      CLOSE C_GET_TAX_COUNT;
--
      if L_count = 0 then
         Update FM_FISCAL_DOC_DETAIL
            set tax_discrep_status = I_discrep_status,
                last_update_datetime = sysdate,
                last_update_id = user
          where Fiscal_doc_line_id = I_fiscal_doc_line_id;
      end if;
   end if;
  ---
   return TRUE;
 EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_DETAIL',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_DOC_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_FM_FISCAL_DOC_DETAIL;
      end if;
      ---
      return FALSE;
END UPDATE_FISCAL_DOC_DETAIL;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_FISCAL_DOC_HEADER
-- Purpose:       This function Updates FM_FISCAL_DOC_HEADER discrepancy flags
----------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC_HEADER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_fiscal_doc_id      IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                  I_tax_code           IN FM_RESOLUTION.TAX_CODE%TYPE,
                                  I_disc_type          IN FM_RESOLUTION.DISCREP_TYPE%TYPE,
                                  I_disc_status        IN FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                  I_discrep_status     IN FM_FISCAL_DOC_HEADER.STATUS%TYPE)
   return BOOLEAN is
   L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DOC_HEADER';
   L_key               VARCHAR2(80) := 'I_Fiscal_doc_id: ' || to_char(I_fiscal_doc_id) ||
                                       ' I_disc_type: '||I_disc_type;
   L_dummy             VARCHAR2(1);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_count             NUMBER;
   cursor C_FM_RESOLUTION is
      SELECT count(1)
        FROM fm_resolution fr
       WHERE fr.fiscal_doc_id = I_fiscal_doc_id
         --AND fr.tax_code      = I_tax_code
         AND fr.discrep_type  = 'T'
         AND fr.resolution_type is null;
--
   cursor C_FM_FISCAL_DOC_HEADER IS
   select 'x'
     from FM_FISCAL_DOC_HEADER
    where fiscal_doc_id   = I_fiscal_doc_id
      and tax_discrep_status = 'D';
      --for update nowait;
BEGIN
--
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
--
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   fetch C_FM_FISCAL_DOC_HEADER into L_dummy;
--
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER',L_key);
   close C_FM_FISCAL_DOC_HEADER;
--
   OPEN C_FM_RESOLUTION;
   FETCH C_FM_RESOLUTION INTO L_count;
   CLOSE C_FM_RESOLUTION;
  ---
   if I_disc_type = 'T' then
   SQL_LIB.SET_MARK('UPDATE','C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
      if L_count = 0 then
         Update FM_FISCAL_DOC_HEADER
            set tax_discrep_status = I_disc_status,
                last_update_datetime = sysdate,
                last_update_id = user
          where Fiscal_doc_id = I_fiscal_doc_id;
      end if;
   end if;
   ---
   return TRUE;
 EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_DOC_HEADER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_DOC_HEADER;
      end if;
      ---
      return FALSE;
END UPDATE_FISCAL_DOC_HEADER;
----------------------------------------------------------------------------------
-- Function Name: CHECK_UPDATE_NF_STATUS
-- Purpose:       This function Updates FM_FISCAL_DOC_HEADER NF Status
----------------------------------------------------------------------------------
FUNCTION CHECK_UPDATE_NF_STATUS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_fiscal_doc_id         IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                I_current_nf_status     IN FM_FISCAL_DOC_HEADER.STATUS%TYPE)
   return BOOLEAN is
   L_program           VARCHAR2(50)  := 'FM_DIS_RESOLUTION_SQL.CHECK_UPDATE_NF_STATUS';
   L_key               VARCHAR2(100) := 'I_Fiscal_doc_id: ' || to_char(I_fiscal_doc_id) ||
                                        ' I_current_nf_status: '||I_current_nf_status;
   L_dummy             VARCHAR2(1);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_unresolved        VARCHAR2(1):= 'U';
   L_discrepant        VARCHAR2(1):= 'D';
   L_validated         VARCHAR2(1):= 'V';
   L_approved          VARCHAR2(1):= 'A';
   L_header_count      NUMBER;
   L_detail_count      NUMBER;
   L_cost_disp             FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE := 'N';
   L_qty_disp              FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE := 'N';
   L_tax_disp              FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE := 'N';
   L_other_cost_disp       FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE := 'N';
   L_other_qty_disp        FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE := 'N';
   L_other_tax_disp        FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE := 'N';
   L_comp_nf_ind           FM_UTILIZATION_ATTRIBUTES.COMP_NF_IND%TYPE;
   L_triang_ind            ORDHEAD.TRIANGULATION_IND%TYPE;
   L_other_nf_id           FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_status            INTEGER;
--
   L_rec_st_ind         V_BR_WH_CONTROL_FLAGS.CONTROL_REC_ST_IND%TYPE := NULL;
   L_utilization_st_ind FM_UTILIZATION_ATTRIBUTES.ICMSST_RECOVERY_IND%TYPE := NULL;
--
   cursor C_FM_FISCAL_HEAD_CNT IS
      SELECT count(1)
        FROM fm_fiscal_doc_header
       WHERE fiscal_doc_id = I_fiscal_doc_id
         AND (tax_discrep_status = L_discrepant);
--
   cursor C_FM_FISCAL_DETAIL_CNT IS
      SELECT count(1)
        FROM FM_FISCAL_DOC_DETAIL
       WHERE fiscal_doc_id  = I_fiscal_doc_id
         AND ((tax_discrep_status  = L_discrepant)
              or ( cost_discrep_status = L_discrepant)
              or (qty_discrep_status   = L_discrepant));
--
    cursor C_GET_REC_ST_IND is
      SELECT distinct DECODE(fs.location_type,'S',scf.control_rec_st_ind,'W',wcf.control_rec_st_ind) as control_ind,
             fa.icmsst_recovery_ind
       FROM v_br_store_control_flags scf,
            v_br_wh_control_flags wcf,
            fm_schedule fs,
            fm_fiscal_doc_header fdh,
            fm_fiscal_utilization fu,
            fm_utilization_attributes fa
      WHERE ((fs.location_type = 'S'
        AND fs.location_id     = scf.store)
         OR (fs.location_type  = 'W'
        AND fs.location_id     = wcf.wh))
        AND fdh.schedule_no    = fs.schedule_no
        AND fu.status          = 'A'
        AND fa.utilization_id  = fu.utilization_id
        AND fa.utilization_id  = fdh.utilization_id
        AND fdh.fiscal_doc_id  = I_fiscal_doc_id;
--
    cursor C_GET_REQUISITION_NO is
      SELECT DISTINCT requisition_no
        FROM fm_fiscal_doc_detail
       WHERE fiscal_doc_id = I_fiscal_doc_id;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_HEAD_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_HEAD_CNT;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_HEAD_CNT','FM_FISCAL_DOC_HEADER',L_key);
   fetch C_FM_FISCAL_HEAD_CNT into L_header_count;
--   L_header_count := C_FM_FISCAL_HEAD_CNT%ROWCOUNT;
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_HEAD_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_HEAD_CNT;
--
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_DETAIL', L_key);
   open C_FM_FISCAL_DETAIL_CNT;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DETAIL_CNT','FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_FM_FISCAL_DETAIL_CNT into L_detail_count;
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DETAIL_CNT;
   ---
   if (FM_FISCAL_VALIDATION_SQL.GET_TRIANG_IND(O_error_message, L_comp_nf_ind, L_triang_ind, I_fiscal_doc_id) = FALSE) then
      return FALSE;
   else
      if L_triang_ind = 'Y' then
         ---
         if (FM_FISCAL_VALIDATION_SQL.GET_OTHER_NF(O_error_message, L_other_nf_id, L_comp_nf_ind, I_fiscal_doc_id) = FALSE) then
            return FALSE;
         end if;
         ---
         if FM_FISCAL_DOC_HEADER_SQL.CHECK_DISCREPANCY(O_error_message,
                                                       L_other_nf_id,
                                                       L_other_cost_disp,
                                                       L_other_qty_disp,
                                                       L_other_tax_disp) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   --- check if the current status of NF is in discrepancy,received, received-verified, then change the status
   if I_current_nf_status in ('D','R','RV') then
   --- Check if there are discrepant records and all of them have been resolved, only then update the NF status
      if L_detail_count = 0 and L_header_count = 0 then
   --- if the current nf status is discrepant then update to validated
            if I_current_nf_status = 'D' then
               if (L_triang_ind = 'Y' and (L_other_cost_disp = 'N' and L_other_qty_disp = 'N')) or L_triang_ind = 'N' then
                  SQL_LIB.SET_MARK('UPDATE','C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
                  Update FM_FISCAL_DOC_HEADER
                     set status = L_validated,
                         LAST_UPDATE_DATETIME = sysdate,
                         LAST_UPDATE_ID = user
                   where Fiscal_doc_id = I_fiscal_doc_id;
                end if;
                ---
   --- if the current status in receipt, receipt-verified then update the nf status to 'Approved'
            elsif I_current_nf_status in ('RV') then
              SQL_LIB.SET_MARK('UPDATE','C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
--
              if FM_RECEIVING_SQL.CALC_FINAL_CLT(O_error_message  => O_error_message,
                                                 I_fiscal_doc_id  => I_fiscal_doc_id)= FALSE then
                 return FALSE;
              end if;
--
              if (L_triang_ind = 'Y' and (L_other_cost_disp = 'N' and L_other_qty_disp = 'N' and L_other_tax_disp = 'N')) or L_triang_ind = 'N' then
                 Update FM_FISCAL_DOC_HEADER
                    set status = L_approved,
                        LAST_UPDATE_DATETIME = sysdate,
                        LAST_UPDATE_ID = user
                 where Fiscal_doc_id in (I_fiscal_doc_id,L_other_nf_id);
              end if;
--
               FOR rec_requisition_no IN c_get_requisition_no
               LOOP
                  if rec_cost_adj_sql.fm_receipt_matched(O_error_message,
                                                         rec_requisition_no.requisition_no,
                                                         NULL) = FALSE then
                     return FALSE;
                  end if;
               END LOOP;
--
               SQL_LIB.SET_MARK('OPEN','C_GET_REC_ST_IND','C_GET_REC_ST_IND',L_key);
               open  C_GET_REC_ST_IND;
               SQL_LIB.SET_MARK('FETCH','C_GET_REC_ST_IND','C_GET_REC_ST_IND',L_key);
               fetch C_GET_REC_ST_IND into L_rec_st_ind,L_utilization_st_ind;
               SQL_LIB.SET_MARK('CLOSE','C_GET_REC_ST_IND','C_GET_REC_ST_IND',L_key);
               close C_GET_REC_ST_IND;
               ---
               if NVL(L_triang_ind, 'N') = 'N' then
                  if NVL(L_rec_st_ind,'N') ='Y' and NVL(L_utilization_st_ind,'N') = 'Y' then
                     if FM_POST_RECEIVING_SQL.UPDATE_ST_HISTORY(O_error_message,
                                                                I_fiscal_doc_id) = FALSE then
                        return FALSE;
                     end if;
                     if FM_EXT_TAXES_SQL.UPDATE_HIST_TABLES(O_error_message,
                                                            L_status,
                                                            I_fiscal_doc_id) = FALSE then
                        return FALSE;
                     end if;
                  end if;
               end if;
--
            end if;
         end if;
   end if;
   ---
   return TRUE;
 EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_HEAD_CNT%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_HEAD_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_HEAD_CNT;
      end if;
      ---
      if C_FM_FISCAL_DETAIL_CNT%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_FM_FISCAL_DETAIL_CNT;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_HEAD_CNT%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_HEAD_CNT;
      end if;
      ---
      if C_FM_FISCAL_DETAIL_CNT%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_FM_FISCAL_DETAIL_CNT;
      end if;
      return FALSE;
END CHECK_UPDATE_NF_STATUS;
----------------------------------------------------------------------------------
-- Function Name: EXISTS_FISCAL_TAX_DETAIL
-- Purpose:       This function checks if detail records exists in the FM_FISCAL_DOC_TAX_DETAIL table
----------------------------------------------------------------------------------
FUNCTION EXISTS_FISCAL_TAX_DETAIL(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists              IN OUT BOOLEAN,
                                  I_fiscal_doc_id       IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.EXISTS_FISCAL_TAX_DETAIL';
   L_count     NUMBER;
   L_key       VARCHAR2(100) := 'Fiscal Doc id: '||I_fiscal_doc_id;
   ---
   cursor C_FM_TAX_DETAIL is
      SELECT count(*)
        FROM fm_fiscal_doc_detail fd
       WHERE fd.fiscal_doc_id = I_FISCAL_DOC_ID
         AND EXISTS (SELECT 1
                       FROM fm_fiscal_doc_tax_detail td
                      WHERE td.fiscal_doc_line_id = fd.fiscal_doc_line_id);
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
   open C_FM_TAX_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
   fetch C_FM_TAX_DETAIL into L_count;
   ---
   if L_count > 0 then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
   close C_FM_TAX_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_TAX_DETAIL%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE','C_FM_TAX_DETAIL', 'FM_FISCAL_DCO_TAX_DETAIL', L_key);
         close C_FM_TAX_DETAIL;
      end if;
      ---
      return FALSE;
END EXISTS_FISCAL_TAX_DETAIL;
----------------------------------------------------------------------------------
-- Function Name: GET_ITEM_DETAILS
-- Purpose:       This function retrieves the Item and Item Description
----------------------------------------------------------------------------------
FUNCTION GET_ITEM_DETAILS(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_FISCAL_DOC_LINE_ID        IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                          O_ITEM                      IN OUT FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                          O_ITEM_DESC                 IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                          O_PO_NUMBER                 IN OUT FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                          O_INCONCLUSIVE_RULES        IN OUT FM_FISCAL_DOC_DETAIL.INCONCLUSIVE_RULES%TYPE,
                          O_PACK_NO                   IN OUT FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE)
   return BOOLEAN is
 ---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_ITEM_DETAILS';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(100):= 'Fiscal Doc Line id: '||I_fiscal_doc_line_id;
   ---
   cursor C_FM_ITEM is
      SELECT im.item_desc
           , im.item
           , ffdd.requisition_no po_number
           , ffdd.inconclusive_rules
           , ffdd.pack_no
        FROM item_master im
           , fm_fiscal_doc_detail ffdd
       WHERE language_sql.get_primary_language = language_sql.get_user_language
         AND im.item = ffdd.item
         AND ffdd.fiscal_doc_line_id = I_fiscal_doc_line_id
         --AND im.pack_ind = 'N'
   UNION ALL
      SELECT NVL(tl_shadow.translated_value, im.item_desc) item_desc
           , im.item
           , ffdd.requisition_no po_number
           , ffdd.inconclusive_rules
           , ffdd.pack_no
        FROM item_master im
           , tl_shadow tl_shadow
           , fm_fiscal_doc_detail ffdd
       WHERE language_sql.get_primary_language != language_sql.get_user_language
         AND UPPER(im.item) = tl_shadow.key(+)
         AND LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+)
         AND im.item = ffdd.item
         AND ffdd.fiscal_doc_line_id = I_fiscal_doc_line_id;
         --AND im.pack_ind = 'N';
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_ITEM','V_ITEM_MASTER',L_key);
   open C_FM_ITEM;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_ITEM','V_ITEM_MASTER',L_key);
   fetch C_FM_ITEM into O_ITEM_DESC, O_ITEM, O_PO_NUMBER , O_INCONCLUSIVE_RULES, O_PACK_NO;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_ITEM','V_ITEM_MASTER',L_key);
   close C_FM_ITEM;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_ITEM%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE','C_FM_ITEM', 'ITEM', L_key);
         close C_FM_ITEM;
      end if;
      ---
      return FALSE;
END GET_ITEM_DETAILS ;
----------------------------------------------------------------------------------
-- Function Name: CHECK_RULE
-- Purpose:       This function retrieves the Item and Item Description
----------------------------------------------------------------------------------
FUNCTION CHECK_RULE(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists                    IN OUT BOOLEAN,
                    I_CODE_TYPE                 IN CODE_DETAIL.CODE_TYPE%TYPE)
   return BOOLEAN is
---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.CHECK_RULE';
   L_count     NUMBER;
   L_key       VARCHAR2(100):= 'Code Type: '||I_code_type;
   ---
   cursor C_FM_CODE is
          select count(*)
            from code_detail CD
           where code_type = 'FMRA'
             and code = I_CODE_TYPE;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_CODE','V_ITEM_MASTER',L_key);
   open C_FM_CODE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_CODE','V_ITEM_MASTER',L_key);
   fetch C_FM_CODE into L_count;
   ---
   if L_count > 0 then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_CODE','V_ITEM_MASTER',L_key);
   close C_FM_CODE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_CODE%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE','C_FM_CODE', 'CODE_DETAIL', L_key);
         close C_FM_CODE;
      end if;
      ---
      return FALSE;
END CHECK_RULE;
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE_DETAIL
-- Purpose:       This function returns the query for detail blocks except Apportioned / Informed blocks
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_where_clause       IN OUT VARCHAR2,
                                 I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                 I_PO_Number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                 I_discrepancy_status IN     FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE,
                                 I_discrep_type       IN     FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                 I_tax_type           IN     VARCHAR2,
                                 I_mode               IN     NUMBER)
   return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.SET_WHERE_CLAUSE_DETAIL';
BEGIN
   ---
   if I_fiscal_doc_id is NOT NULL then
      O_where_clause := ' fiscal_doc_id = ' || TO_CHAR(I_fiscal_doc_id) ||' and ';
   end if;
   ---
   if I_discrep_type is NOT NULL then
      if I_discrep_type in ('C','Q','T') then
         O_where_clause := O_where_clause || ' discrep_type = '''||I_discrep_type||''' and ';
      end if;
   end if;
   ---

   if I_mode = 0 then
      if (I_discrepancy_status = 'R') then
          O_where_clause := O_where_clause||' resolution_type is NOT NULL and ';
      elsif I_discrepancy_status = 'U' then
         O_where_clause := O_where_clause ||' resolution_type is NULL and ';
      end if;
   end if;
   ---
   if I_PO_Number IS NOT NULL then
      O_where_clause  := O_where_clause||' fiscal_doc_line_id  in (SELECT fiscal_doc_line_id FROM fm_fiscal_doc_detail WHERE requisition_no = '||I_PO_Number||')and ';
   end if;
   ---
   if O_where_clause is NOT NULL then
      O_where_clause := SUBSTR(O_where_clause, 1, NVL(LENGTH(O_where_clause), 0)-4);
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE_DETAIL;
----------------------------------------------------------------------------------
-- Function Name: GET_FM_EXT_TAX_APPOR_VALUES
-- Purpose:       This function retrieves the External System Tax detail info for system and NF value.
----------------------------------------------------------------------------------
FUNCTION GET_FM_EXT_TAX_APPOR_VALUES(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_FISCAL_DOC_ID             IN      FM_FISCAL_DOC_TAX_HEAD_EXT.FISCAL_DOC_ID%TYPE,
                                     I_TAX_CODE                  IN      FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE,
                                     I_PO_NUMBER                 IN      FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                     O_TAX_BASIS                 IN OUT  FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_BASIS%TYPE,
                                     O_TAX_VALUE                 IN OUT  FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_VALUE%TYPE,
                                     O_INF_TAX_BASIS             IN OUT  FM_FISCAL_DOC_TAX_HEAD.TAX_BASIS%TYPE,
                                     O_INF_TAX_VALUE             IN OUT  FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE,
                                     O_TAX_DESC                  IN OUT  VAT_CODES.VAT_CODE_DESC%TYPE,
                                     O_MOD_SYS_TAX_BASIS         IN OUT  FM_FISCAL_DOC_TAX_DETAIL_EXT.MODIFIED_TAX_BASIS%TYPE,
                                     O_MOD_TAX_BASIS             IN OUT  FM_FISCAL_DOC_TAX_DETAIL.MODIFIED_TAX_BASIS%TYPE)
   return BOOLEAN is
---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_FM_EXT_TAX_HEADER';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(100):= 'Fiscal Doc Id: '||I_FISCAL_DOC_ID ||' Tax Code: '||I_TAX_CODE;
   L_TAX_DESC   VAT_CODES.VAT_CODE_DESC%TYPE;
   L_TAX_DESC1  VAT_CODES.VAT_CODE_DESC%TYPE;
   ---
/*
   CURSOR C_FM_TAX_HEAD_EXT is
     SELECT sys_tax_basis
           ,sys_tax_value
           ,nf_tax_basis
           ,nf_tax_value
           ,vc.vat_code_desc
       FROM (select nvl(fdthe.fiscal_doc_id,fdth.fiscal_doc_id) fiscal_doc_id
                   ,nvl(fdthe.tax_code,fdth.vat_code) vat_code
                   ,fdthe.tax_basis sys_tax_basis
                   , fdthe.tax_value sys_tax_value --system tax value
                   , fdth.tax_basis nf_tax_basis  --Informed_tax_base_value
                   , fdth.total_value nf_tax_value
               from fm_fiscal_doc_tax_head_ext fdthe full outer join
                    fm_fiscal_doc_tax_head fdth
                 on fdthe.fiscal_doc_id = fdth.fiscal_doc_id
                and fdthe.tax_code  = fdth.vat_code) a,
             vat_codes vc
      WHERE a.fiscal_doc_id = I_fiscal_doc_id
        AND a.vat_code = vc.vat_code
        AND a.vat_code = I_tax_code;
*/
   CURSOR C_FM_SYSTEM_VALUE is
     SELECT sum(fdtde.tax_basis) sys_tax_basis
           ,sum(fdtde.tax_value) sys_tax_value
           ,vc.vat_code_desc
           ,sum(fdtde.modified_tax_basis) sys_modified_tax_basis
       FROM fm_fiscal_doc_tax_detail_ext fdtde
           ,fm_fiscal_doc_detail fdd
           ,vat_codes vc
           ,fm_resolution fr
      WHERE fdtde.fiscal_doc_line_id = fdd.fiscal_doc_line_id
        AND fdd.fiscal_doc_line_id = fr.fiscal_doc_line_id
        AND fr.tax_code        = fdtde.tax_code
        AND fdtde.tax_code     = vc.vat_code
        AND fdd.fiscal_doc_id  = I_fiscal_doc_id
        AND fdtde.tax_code     = I_tax_code
        AND fdd.requisition_no = I_po_number
      --  AND fr.resolution_type is null
        group by vc.vat_code_desc;
--
   CURSOR C_FM_NF_VALUE is
      select sum(nvl(fdtd.appr_base_value,fdtd.tax_basis)) nf_tax_basis
            ,sum(nvl(fdtd.appr_tax_value,fdtd.total_value))  nf_tax_value
            ,vc.vat_code_desc
            ,sum(nvl(fdtd.appr_modified_base_value,fdtd.modified_tax_basis)) nf_modified_tax_basis
        from fm_fiscal_doc_tax_detail fdtd
            ,fm_fiscal_doc_detail fdd
            ,vat_codes vc
            ,fm_resolution fr
       where fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         and fdd.fiscal_doc_line_id = fr.fiscal_doc_line_id
         and fr.tax_code        = fdtd.vat_code
         and fdtd.vat_code      = vc.vat_code
         and fdd.fiscal_doc_id  = I_fiscal_doc_id
         and fdtd.vat_code      = I_tax_code
         and fdd.requisition_no = I_po_number
     --    and fr.resolution_type is null
         group by vc.vat_code_desc;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_SYSTEM_VALUE','fm_fiscal_doc_tax_detail_ext',L_key);
   open C_FM_SYSTEM_VALUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_SYSTEM_VALUE','fm_fiscal_doc_tax_detail_ext',L_key);
   fetch C_FM_SYSTEM_VALUE into O_TAX_BASIS,O_TAX_VALUE,L_TAX_DESC,O_MOD_SYS_TAX_BASIS;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_SYSTEM_VALUE','fm_fiscal_doc_tax_detail_ext',L_key);
   close C_FM_SYSTEM_VALUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_NF_VALUE','fm_fiscal_doc_tax_detail',L_key);
   open C_FM_NF_VALUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_NF_VALUE','fm_fiscal_doc_tax_detail',L_key);
   fetch C_FM_NF_VALUE into O_INF_TAX_BASIS,O_INF_TAX_VALUE,L_TAX_DESC1,O_MOD_TAX_BASIS;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_NF_VALUE','fm_fiscal_doc_tax_detail',L_key);
   close C_FM_NF_VALUE;
   ---
   O_TAX_DESC := NVL(L_TAX_DESC,L_TAX_DESC1);
   --
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_SYSTEM_VALUE%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE','C_FM_SYSTEM_VALUE', 'fm_fiscal_doc_tax_detail_ext', L_key);
         close C_FM_SYSTEM_VALUE;
      end if;
      ---
      ---
      if C_FM_NF_VALUE%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE','C_FM_NF_VALUE', 'fm_fiscal_doc_tax_detail', L_key);
         close C_FM_NF_VALUE;
      end if;
      return FALSE;
END GET_FM_EXT_TAX_APPOR_VALUES;
----------------------------------------------------------------------------------
-- Function Name: GET_FM_EXT_TAX_INFOR_VALUES
-- Purpose:       This function retrieves the External System Tax Header info
----------------------------------------------------------------------------------
FUNCTION GET_FM_EXT_TAX_INFOR_VALUES(O_error_message              IN OUT  VARCHAR2,
                                     I_FISCAL_DOC_ID             IN      FM_FISCAL_DOC_TAX_HEAD_EXT.FISCAL_DOC_ID%TYPE,
                                     I_TAX_CODE                  IN      FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE,
                                     O_TAX_BASIS                 IN OUT  FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_BASIS%TYPE,
                                     O_TAX_VALUE                 IN OUT  FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_VALUE%TYPE,
                                     O_INF_TAX_BASIS             IN OUT  FM_FISCAL_DOC_TAX_HEAD.TAX_BASIS%TYPE,
                                     O_INF_TAX_VALUE             IN OUT  FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE,
                                     O_TAX_DESC                  IN OUT  VAT_CODES.VAT_CODE_DESC%TYPE)
   return BOOLEAN is

---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_FM_EXT_TAX_HEADER';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(100):= 'Fiscal Doc Id: '||I_FISCAL_DOC_ID ||' Tax Code: '||I_TAX_CODE;
   ---
   /*
   cursor C_FM_TAX_HEAD_EXT is
      select fdthe.tax_basis  --system tax base value
           , fdthe.tax_value  --system tax value
           , fdth.tax_basis   --Informed_tax_base_value
           , fdth.total_value --Informed_tax_value
           , vc.vat_code_desc
       from fm_fiscal_doc_tax_head_ext fdthe
           ,fm_fiscal_doc_tax_head fdth
           ,vat_codes vc
      where fdthe.fiscal_doc_id = fdth.fiscal_doc_id
        and fdthe.tax_code = fdth.vat_code
        and fdthe.tax_code = vc.vat_code
        and fdthe.fiscal_doc_id = I_fiscal_doc_id
        and fdthe.tax_code = I_tax_code;
        */
   CURSOR C_FM_TAX_HEAD_EXT is
     SELECT sys_tax_basis
           ,sys_tax_value
           ,nf_tax_basis
           ,nf_tax_value
           ,vc.vat_code_desc
       FROM (select nvl(fdthe.fiscal_doc_id,fdth.fiscal_doc_id) fiscal_doc_id
                   ,nvl(fdthe.tax_code,fdth.vat_code) vat_code
                   ,fdthe.tax_basis sys_tax_basis
                   , fdthe.tax_value sys_tax_value --system tax value
                   , fdth.tax_basis nf_tax_basis  --Informed_tax_base_value
                   , fdth.total_value nf_tax_value
               from fm_fiscal_doc_tax_head_ext fdthe full outer join
                    fm_fiscal_doc_tax_head fdth
                 on fdthe.fiscal_doc_id = fdth.fiscal_doc_id
                and fdthe.tax_code  = fdth.vat_code) a,
             vat_codes vc
      WHERE a.fiscal_doc_id = I_fiscal_doc_id
        AND a.vat_code = vc.vat_code
        AND a.vat_code = I_tax_code;
---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_TAX_HEAD_EXT','fm_fiscal_doc_tax_head_ext',L_key);
   open C_FM_TAX_HEAD_EXT;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_TAX_HEAD_EXT','fm_fiscal_doc_tax_head_ext',L_key);
   fetch C_FM_TAX_HEAD_EXT into O_TAX_BASIS,O_TAX_VALUE,O_INF_TAX_BASIS,O_INF_TAX_VALUE,O_TAX_DESC;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_TAX_HEAD_EXT','fm_fiscal_doc_tax_head_ext',L_key);
   close C_FM_TAX_HEAD_EXT;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_TAX_HEAD_EXT%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE','C_FM_TAX_HEAD_EXT', 'fm_fiscal_doc_tax_head_ext', L_key);
         close C_FM_TAX_HEAD_EXT;
      end if;
      ---

      return FALSE;
--GET_FM_EXT_TAX_HEADER
END GET_FM_EXT_TAX_INFOR_VALUES;
----------------------------------------------------------------------------------
-- Function Name: GET_FM_EXT_TAX_DETAIL
-- Purpose:       This function retrieves the External system Tax detail info
----------------------------------------------------------------------------------
FUNCTION GET_FM_EXT_TAX_DETAIL (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_FISCAL_DOC_ID          IN      FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_ID%TYPE,
                                I_FISCAL_DOC_LINE_ID     IN      FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                                I_TAX_CODE               IN      FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE,
                                O_TAX_DET_REC            IN OUT  FM_FISCAL_DOC_TAX_DETAIL_EXT%ROWTYPE)
   return BOOLEAN is
---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_FM_EXT_TAX_DETAIL';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(100):= 'Fiscal Doc Id: '||I_FISCAL_DOC_ID ||' Tax Code: '||I_TAX_CODE;
   ---
   cursor C_FM_EXT_TAX_DET is
      SELECT *
        FROM fm_fiscal_doc_tax_detail_ext fmtd
       WHERE fmtd.fiscal_doc_id = i_fiscal_doc_id
         AND fmtd.fiscal_doc_line_id = i_fiscal_doc_line_id
         AND fmtd.tax_code = I_tax_code;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_EXT_TAX_DET','FM_FISCAL_DOC_TAX_DETAIL_EXT',L_key);
   open C_FM_EXT_TAX_DET;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_EXT_TAX_DET','FM_FISCAL_DOC_TAX_DETAIL_EXT',L_key);
   fetch C_FM_EXT_TAX_DET into O_TAX_DET_REC;
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_EXT_TAX_DET','FM_FISCAL_DOC_TAX_DETAIL_EXT',L_key);
   close C_FM_EXT_TAX_DET;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_EXT_TAX_DET%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE','C_FM_EXT_TAX_DET', 'FM_FISCAL_DOC_TAX_DETAIL_EXT', L_key);
         close C_FM_EXT_TAX_DET;
      end if;
      ---
      return FALSE;
END GET_FM_EXT_TAX_DETAIL;
----------------------------------------------------------------------------------
-- Function Name: FM_RES_ALL_DISC
-- Purpose:       This function Updates FM_FISCAL_DOC_HEADER NF Status
----------------------------------------------------------------------------------
 FUNCTION FM_RES_ALL_DISC(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_FISCAL_DOC_ID           IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_FISCAL_DOC_LINE_ID      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                         I_NF_STATUS               IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                         I_PO_NUMBER               IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                         I_ITEM                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                         I_COST_RESOLUTION_RULE    IN     FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                         I_QTY_RESOLUTION_RULE     IN     FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                         I_TAX_RESOLUTION_RULE     IN     FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                         I_CENTRALIZED             IN     VARCHAR2)
   return BOOLEAN is
   L_program                  VARCHAR2(50)  := 'FM_DIS_RESOLUTION_SQL.FM_RES_ALL_DISC';
   L_key                      VARCHAR2(100) := 'I_Fiscal_doc_id: ' ||I_fiscal_doc_id||' fiscal_doc_line_id = '||I_FISCAL_DOC_LINE_ID;
   L_dummy                    VARCHAR2(1);
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);
   L_unresolved               VARCHAR2(1):= 'U';
   L_discrepant               VARCHAR2(1):= 'D';
   L_validated                VARCHAR2(1):= 'V';
   L_approved                 VARCHAR2(1):= 'A';
   L_header_count             NUMBER;
   L_detail_count             NUMBER;
   L_error_msg                VARCHAR2(255):=NULL;
   L_qty_val                  FM_RESOLUTION.SYSTEM_VALUE%TYPE;
   L_appt_qty                 FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE;
   L_old_ebc                  FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_old_nic                  FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_new_ebc                  FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   L_new_nic                  FM_RECEIVING_DETAIL.NIC_COST%TYPE;
   L_triangulation            Varchar2(1);
   L_compl_fiscal_doc_id      FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   L_Schedule_no              FM_SCHEDULE.SCHEDULE_NO%TYPE;
   L_compl_nf                 VARCHAR2(1);
   L_main_nf                  VARCHAR2(1);
   L_main_fiscal_doc_id       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
--
   cursor C_FM_RESOLUTION IS
      SELECT fr.fiscal_doc_id
            ,fr.fiscal_doc_line_id
            ,fr.discrep_type
            ,fr.tax_code
            ,fr.system_value
            ,fr.nf_value
            ,fr.resolution_type
            ,fr.last_update_datetime
            ,fr.last_update_id
            ,fdd.item
            ,fdd.requisition_no po_number
            ,fdd.pack_no
        FROM fm_resolution fr
            ,fm_fiscal_doc_detail fdd
       WHERE fr.fiscal_doc_id      = fdd.fiscal_doc_id
         and fr.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         AND fr.fiscal_doc_id        = I_fiscal_doc_id
         AND fr.resolution_type is NULL
    ORDER BY fr.discrep_type;
         --FOR update nowait;
--
   CURSOR C_GET_SCHEDULE_NO is
      SELECT schedule_no
        FROM fm_fiscal_doc_header
       WHERE fiscal_doc_id = I_fiscal_doc_id;

   L_res_rec  C_FM_RESOLUTION%ROWTYPE;
BEGIN
  --For Triangulation
   if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message       => O_error_message
                                                           ,I_fiscal_doc_id       => I_fiscal_doc_id
                                                           ,O_triangulation       => L_triangulation
                                                           ,O_compl_fiscal_doc_id => L_compl_fiscal_doc_id ) = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
   open C_FM_RESOLUTION;
   loop
      fetch C_FM_RESOLUTION into L_res_rec;
      SQL_LIB.SET_MARK('FETCH', 'C_FM_RESOLUTION','FM_RESOLUTION', L_key);
      exit when C_FM_RESOLUTION%NOTFOUND;

      if L_res_rec.discrep_type = 'C' then
         --- start update FM_resolution
         update FM_RESOLUTION
            set resolution_type      = I_cost_resolution_rule,
                last_update_datetime = SYSDATE,
                last_update_id       = USER
          where fiscal_doc_id        = L_res_rec.fiscal_doc_id
            and fiscal_doc_line_id   = L_res_rec.fiscal_doc_line_id;
         --- end update FM_resolution
             --- start Update FISCAL_DOC_DETAIL flag
          if FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DOC_DETAIL(O_error_message,
                                                            L_res_rec.fiscal_doc_line_id,
                                                            'C',
                                                            'R',
                                                             null) = FALSE then
              return FALSE;
          end if;

          if L_triangulation = 'Y' then
             if FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DTAIL_FOR_TRIANG(O_error_message       => O_error_message
                                                                    ,I_discrepancy         => 'C'
                                                                    ,I_compl_fiscal_doc_id => L_compl_fiscal_doc_id
                                                                    ,I_po_number           => L_res_rec.po_number
                                                                    ,I_item                => L_res_rec.item
                                                                    ,I_appt_qty            => null) = FALSE then
                return FALSE;
             end if;
          end if;

          --Invoke 20c posting when Cost discrepancy is resolved when the location is Centralised.
          if I_CENTRALIZED = 'C' and (I_NF_STATUS = 'RV' or I_NF_STATUS = 'R') then
             if  L_res_rec.pack_no is null then
                --INVOKE Tax Integration
                if FM_RECEIVING_SQL.CALC_FINAL_EBC(O_error_message      => O_error_message,
                                                   I_fiscal_doc_id      => L_res_rec.fiscal_doc_id,
                                                   I_fiscal_doc_line_id => L_res_rec.fiscal_doc_line_id,
                                                   I_pack_no            => null,
                                                   O_old_ebc            => L_old_ebc,
                                                   O_old_nic            => L_old_nic,
                                                   O_new_ebc            => L_new_ebc,
                                                   O_new_nic            => L_new_nic) = FALSE then
                   return FALSE;
                end if;
             elsif L_res_rec.pack_no is not null then
                --INVOKE Tax Integration
                if FM_RECEIVING_SQL.CALC_FINAL_EBC(O_error_message      => O_error_message,
                                                   I_fiscal_doc_id      => L_res_rec.fiscal_doc_id,
                                                   I_fiscal_doc_line_id => null,
                                                   I_pack_no            => L_res_rec.pack_no,
                                                   O_old_ebc            => L_old_ebc,
                                                   O_old_nic            => L_old_nic,
                                                   O_new_ebc            => L_new_ebc,
                                                   O_new_nic            => L_new_nic) = FALSE then
                   return FALSE;
                end if;
             end if;
             if L_old_ebc != L_new_ebc then
             --Invoke 20C Positng
                if L_res_rec.pack_no is null then
                   if REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT (O_error_message  => O_error_message,
                                                     I_order_no       => L_res_rec.po_number,
                                                     I_ref_doc_no     => L_res_rec.fiscal_doc_id,
                                                     I_item           => L_res_rec.item,
                                                     I_old_ebc        => L_old_ebc,
                                                     I_new_ebc        => L_new_ebc,
                                                     I_new_nic        => L_new_nic) = FALSE then
                   return FALSE;
                   end if;
                 elsif L_res_rec.pack_no is not null then
                   if REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT (O_error_message  => O_error_message,
                                                     I_order_no       => L_res_rec.po_number,
                                                     I_ref_doc_no     => L_res_rec.fiscal_doc_id,
                                                     I_item           => L_res_rec.pack_no,
                                                     I_old_ebc        => L_old_ebc,
                                                     I_new_ebc        => L_new_ebc,
                                                     I_new_nic        => L_new_nic) = FALSE then
                   return FALSE;
                   end if;
                end if;
             end if;
          end if;
      end if;
      --- begin if Qty Discrepancy
      if L_res_rec.discrep_type = 'Q' then

          update FM_RESOLUTION
             set resolution_type      = I_qty_resolution_rule,
                 last_update_datetime = SYSDATE,
                 last_update_id       = USER
           where fiscal_doc_id        = L_res_rec.fiscal_doc_id
             and fiscal_doc_line_id   = L_res_rec.fiscal_doc_line_id;
--
          if I_QTY_RESOLUTION_RULE = 'NF' then
             L_appt_qty := L_res_rec.nf_value ;
          elsif I_QTY_RESOLUTION_RULE = 'SYS' then
             L_appt_qty := L_res_rec.system_value ;
          end if;
--
          --- start Update FISCAL_DOC_DETAIL flag
          if FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DOC_DETAIL(O_error_message,
                                                              L_res_rec.fiscal_doc_line_id,
                                                             'Q',
                                                             'R',
                                                              L_appt_qty) = FALSE then
              return FALSE;
          end if;
          if L_triangulation = 'Y' then
             if FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DTAIL_FOR_TRIANG(O_error_message       => O_error_message
                                                                    ,I_discrepancy         => 'Q'
                                                                    ,I_compl_fiscal_doc_id => L_compl_fiscal_doc_id
                                                                    ,I_po_number           => L_res_rec.po_number
                                                                    ,I_item                => L_res_rec.item
                                                                    ,I_appt_qty            => L_appt_qty) = FALSE then
                return FALSE;
             end if;
          end if;
      end if;
--
      if L_res_rec.discrep_type = 'T' then
          update FM_RESOLUTION
             set resolution_type      = I_tax_resolution_rule,
                 last_update_datetime = SYSDATE,
                 last_update_id       = USER
           where fiscal_doc_id        = L_res_rec.fiscal_doc_id
             and fiscal_doc_line_id   = L_res_rec.fiscal_doc_line_id;
          --- start Update FISCAL_DOC_DETAIL flag
          if FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DOC_DETAIL(O_error_message,
                                                              L_res_rec.fiscal_doc_line_id,
                                                             'T',
                                                             'R',
                                                              NULL) = FALSE then
             return FALSE;
          end if;
          --Invoke 20c posting when Tax discrepancy is resolved when the location is Centralised.
        --Invoke 20c posting when Tax discrepancy is resolved when the location is Centralised.
           if I_CENTRALIZED = 'C' and (I_NF_STATUS = 'RV' or I_NF_STATUS = 'R') then
                   if L_res_rec.pack_no is null then
               --INVOKE Tax Integration
                 if FM_RECEIVING_SQL.CALC_FINAL_EBC(O_error_message      => O_error_message,
                                                    I_fiscal_doc_id      => L_res_rec.fiscal_doc_id,
                                                    I_fiscal_doc_line_id => L_res_rec.fiscal_doc_line_id,
                                                    I_pack_no            => null,
                                                    O_old_ebc            => L_old_ebc,
                                                    O_old_nic            => L_old_nic,
                                                    O_new_ebc            => L_new_ebc,
                                                    O_new_nic            => L_new_nic) = FALSE then
                    return FALSE;
                 end if;
              elsif L_res_rec.pack_no is not null then
                 if FM_RECEIVING_SQL.CALC_FINAL_EBC(O_error_message      => O_error_message,
                                                    I_fiscal_doc_id      => L_res_rec.fiscal_doc_id,
                                                    I_fiscal_doc_line_id => null,
                                                    I_pack_no            => L_res_rec.pack_no,
                                                    O_old_ebc            => L_old_ebc,
                                                    O_old_nic            => L_old_nic,
                                                    O_new_ebc            => L_new_ebc,
                                                    O_new_nic            => L_new_nic) = FALSE then
                    return FALSE;
                 end if;
              end if;
--
              --Invoke 20C Positng
              if L_old_ebc <> L_new_ebc then
                 if L_triangulation = 'Y' then
                    if FM_DIS_RESOLUTION_SQL.CHECK_COMPL_OR_MAIN_NF(O_error_message   => O_error_message,
                                                                     I_FISCAL_DOC_ID   => L_res_rec.fiscal_doc_id,
                                                                     O_COMPL_NF        => L_compl_nf,
                                                                     O_MAIN_NF         => L_main_nf) = FALSE then
                       return FALSE;
                    end if;
                    --
                    if L_compl_nf = 'Y' and L_main_nf = 'N' then
                       if FM_DIS_RESOLUTION_SQL.GET_MAIN_NF_FOR_COMPL_NF(O_error_message      => O_error_message,
                                                                          I_FISCAL_DOC_ID      => L_res_rec.fiscal_doc_id,
                                                                          O_MAIN_FISCAL_DOC_ID => L_main_fiscal_doc_id) = FALSE then
                          return FALSE;
                       end if;
                       if L_res_rec.pack_no is null then
                          if REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT (O_error_message  => O_error_message,
                                                               I_order_no       => L_res_rec.po_number,
                                                               I_ref_doc_no     => L_main_fiscal_doc_id,
                                                               I_item           => L_res_rec.item,
                                                               I_old_ebc        => L_old_ebc,
                                                               I_new_ebc        => L_new_ebc,
                                                               I_new_nic        => L_new_nic) = FALSE then
                             return FALSE;
                          end if;
                       elsif L_res_rec.pack_no is not null then
                          if REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT (O_error_message  => O_error_message,
                                                               I_order_no       => L_res_rec.po_number,
                                                               I_ref_doc_no     => L_main_fiscal_doc_id,
                                                               I_item           => L_res_rec.pack_no,
                                                               I_old_ebc        => L_old_ebc,
                                                               I_new_ebc        => L_new_ebc,
                                                               I_new_nic        => L_new_nic) = FALSE then
                             return FALSE;
                          end if;
                       end if;
                    elsif  L_compl_nf = 'N' and L_main_nf = 'Y' then
                       if L_res_rec.pack_no is null then
                          if REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT (O_error_message  => O_error_message,
                                                               I_order_no       => L_res_rec.po_number,
                                                               I_ref_doc_no     => L_res_rec.fiscal_doc_id,
                                                               I_item           => L_res_rec.item,
                                                               I_old_ebc        => L_old_ebc,
                                                               I_new_ebc        => L_new_ebc,
                                                               I_new_nic        => L_new_nic) = FALSE then
                             return FALSE;
                          end if;
                        elsif L_res_rec.pack_no is not null then
                          if REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT (O_error_message  => O_error_message,
                                                               I_order_no       => L_res_rec.po_number,
                                                               I_ref_doc_no     => L_res_rec.fiscal_doc_id,
                                                               I_item           => L_res_rec.pack_no,
                                                               I_old_ebc        => L_old_ebc,
                                                               I_new_ebc        => L_new_ebc,
                                                               I_new_nic        => L_new_nic) = FALSE then
                             return FALSE;
                          end if;
                       end if;
                    end if;
                 else
                    if L_res_rec.pack_no is null then
                       if REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT (O_error_message  => O_error_message,
                                                            I_order_no       => L_res_rec.po_number,
                                                            I_ref_doc_no     => L_res_rec.fiscal_doc_id,
                                                            I_item           => L_res_rec.item,
                                                            I_old_ebc        => L_old_ebc,
                                                            I_new_ebc        => L_new_ebc,
                                                            I_new_nic        => L_new_nic) = FALSE then
                          return FALSE;
                       end if;
                    elsif L_res_rec.pack_no is not null then
                       if REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT (O_error_message  => O_error_message,
                                                            I_order_no       => L_res_rec.po_number,
                                                            I_ref_doc_no     => L_res_rec.fiscal_doc_id,
                                                            I_item           => L_res_rec.pack_no,
                                                            I_old_ebc        => L_old_ebc,
                                                            I_new_ebc        => L_new_ebc,
                                                            I_new_nic        => L_new_nic) = FALSE then
                          return FALSE;
                       end if;
                    end if;
                 end if;
              end if;
           end if;
      end if;
   end loop;
   ---
   if I_CENTRALIZED = 'C' and I_NF_STATUS in ('W','E','D','V') then
   --- To Change the NF status in centralised location if all the Qty discrepancy is resolved.
      if FM_DIS_RESOLUTION_SQL.UPDATE_NF_STATUS_QTY_CENT(O_error_message   => O_error_message,
                                                         I_fiscal_doc_id   => L_res_rec.fiscal_doc_id) = FALSE then
         return FALSE;
      end if;
      if L_triangulation = 'Y' then
         if FM_DIS_RESOLUTION_SQL.UPDATE_NF_STATUS_QTY_CENT(O_error_message   => O_error_message,
                                                            I_fiscal_doc_id   => L_compl_fiscal_doc_id) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
    --- update FM_FISCAL_DOC_HEADER for any Tax discrepancy
   if FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DOC_HEADER(O_error_message,
                                                     I_fiscal_doc_id,
                                                     L_res_rec.tax_code,
                                                     'T',
                                                     'R',
                                                     'R') = FALSE then
      return FALSE;
   end if;
      ---
   if FM_DIS_RESOLUTION_SQL.CHECK_UPDATE_NF_STATUS(O_error_message,
                                                   I_fiscal_doc_id,
                                                   I_nf_status) = FALSE then
     return FALSE;
   end if;
--
   if L_triangulation = 'Y' then
      if FM_DIS_RESOLUTION_SQL.CHECK_UPDATE_NF_STATUS(O_error_message,
                                                      L_compl_fiscal_doc_id,
                                                      I_nf_status) = FALSE then
        return FALSE;
      end if;
   end if;
--
   OPEN C_GET_SCHEDULE_NO;
   FETCH C_GET_SCHEDULE_NO INTO L_Schedule_no;
   CLOSE C_GET_SCHEDULE_NO;

   if L_Schedule_no is NOT NULL then
      if I_NF_STATUS in ('W','E','D','V') then
         --To update the Schedule status if all the NF status is changed to Approved or Validated.
         if FM_FISCAL_DOC_HEADER_SQL.UPD_SCHED_STATUS(O_error_message => O_error_message,
                                                      I_schedule_no   => L_Schedule_no)= FALSE then
            return FALSE;
         end if;
      else
         if FM_UPD_SCHD_STATUS_SQL.UPD_SCHEDULE_STATUS(O_error_message => O_error_message,
                                                       I_schedule_no   => L_Schedule_no)= FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   return TRUE;
 EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_RESOLUTION',
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      if C_FM_RESOLUTION%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
         close C_FM_RESOLUTION;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_RESOLUTION%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
         close C_FM_RESOLUTION;
      end if;
      ---
      return FALSE;
--
END FM_RES_ALL_DISC;
----------------------------------------------------------------------------------
-- Function Name: GET_FM_LOC_OPR
-- Purpose:       This function gets the operation type for a location - central or decentral
----------------------------------------------------------------------------------
FUNCTION GET_FM_LOC_OPR(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_Location       IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                        I_Location_type  IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                        O_Match_opr_type IN OUT V_FISCAL_ATTRIBUTES.MATCH_OPR_TYPE%TYPE)
   return BOOLEAN is
---
   L_program VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_FM_LOC_OPR';
--
   CURSOR C_FM_STORE_OPR is
      SELECT NVL(MATCH_OPR_TYPE,'D')
        FROM V_BR_STORE
       WHERE store = I_Location;
--
   CURSOR C_FM_WH_OPR is
      SELECT NVL(a.MATCH_OPR_TYPE,'D')
        FROM V_BR_WH a , WH b
       WHERE a.wh = b.physical_wh
         AND b.wh = I_Location;
--
BEGIN
--
   if I_Location_type = 'S' then
      SQL_LIB.SET_MARK('OPEN','C_FM_STORE_OPR','V_BR_STORE','store : '||I_Location);
      open C_FM_STORE_OPR;
--
      SQL_LIB.SET_MARK('FETCH','C_FM_STORE_OPR','V_BR_STORE','store : '||I_Location);
      fetch C_FM_STORE_OPR into O_Match_opr_type;
--
      if C_FM_STORE_OPR%NOTFOUND then
         O_Match_opr_type := 'D' ;
      end if;
---
      SQL_LIB.SET_MARK('CLOSE','C_FM_STORE_OPR','V_BR_STORE','store : '||I_Location);
      close C_FM_STORE_OPR;
   else -- if Wh
      SQL_LIB.SET_MARK('OPEN','C_FM_WH_OPR','V_BR_WH,WH','physical_wh : '||I_Location);
      open C_FM_WH_OPR;
---
      SQL_LIB.SET_MARK('FETCH','C_FM_WH_OPR','V_BR_WH,WH','physical_wh : '||I_Location);
      fetch C_FM_WH_OPR into O_Match_opr_type;
--
      if C_FM_WH_OPR%NOTFOUND then
         O_Match_opr_type := 'D' ;
      end if;
---
      SQL_LIB.SET_MARK('CLOSE','C_FM_WH_OPR','V_BR_WH,WH','physical_wh : '||I_Location);
      close C_FM_WH_OPR;
   end if;
---
   return TRUE;
---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_FM_LOC_OPR;
----------------------------------------------------------------------------------
-- Function Name: GET_FM_TAX_DETAIL
-- Purpose:       This function gets the Tax detail information
----------------------------------------------------------------------------------
FUNCTION GET_FM_TAX_DETAIL (O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_TAX_CODE                 IN     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE,
                            I_FISCAL_DOC_LINE_ID       IN     FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                            O_Item                    OUT     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                            O_Tax_code                OUT     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE,
                            O_Tax_Basis               OUT     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE,
                            O_Tax_Rate                OUT     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_RATE%TYPE,
                            O_Tax_Value               OUT     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE)
   return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_FM_TAX_DETAIL';
---
   cursor C_FM_TAX_DETAIL is
      SELECT fdd.Item
            ,tde.Tax_code
            ,tde.tax_basis
            ,tde.tax_rate
            ,tde.tax_value
        FROM fm_fiscal_doc_detail fdd
            ,fm_fiscal_doc_tax_detail_ext tde
       WHERE fdd.fiscal_doc_line_id = tde.fiscal_doc_line_id
         AND tde.tax_code = I_tax_code
         AND fdd.fiscal_doc_line_id = I_fiscal_doc_line_id;
---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_TAX_DETAIL','fm_fiscal_doc_tax_detail_ext','fiscal_doc_line_id : '||I_fiscal_doc_line_id);
   open C_FM_TAX_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_TAX_DETAIL','fm_fiscal_doc_tax_detail_ext','fiscal_doc_line_id : '||I_fiscal_doc_line_id);
   fetch C_FM_TAX_DETAIL into O_Item,O_Tax_code,O_Tax_Basis,O_Tax_Rate,O_Tax_Value;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_TAX_DETAIL','fm_fiscal_doc_tax_detail_ext','fiscal_doc_line_id : '||I_fiscal_doc_line_id);
   close C_FM_TAX_DETAIL;
   ---
   return TRUE;
---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_TAX_DETAIL%ISOPEN then
         close C_FM_TAX_DETAIL;
      end if;
      ---
      return FALSE;
END GET_FM_TAX_DETAIL;

--
----------------------------------------------------------------------------------
-- Function Name: EXISTS_FISCAL_DOC_TAX_DETAIL
-- Purpose:       This function checks if detail tax level exists in the EXISTS_FISCAL_DOC_TAX_DETAIL table
----------------------------------------------------------------------------------
FUNCTION EXISTS_FISCAL_DOC_TAX_DETAIL(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists              IN OUT BOOLEAN,
                                      I_vat_code            IN FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE,
                                      I_fiscal_doc_id       IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.EXISTS_FISCAL_DOC_TAX_DETAIL';
   L_count     NUMBER;
   L_key       VARCHAR2(100) := 'Vat Code: '||I_vat_code;
   ---
   CURSOR C_FM_DOC_TAX_DETAIL is
      SELECT count(1)
        FROM FM_FISCAL_DOC_TAX_DETAIL dtd
            ,fm_fiscal_doc_detail fdd
       WHERE dtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         and dtd.vat_code = I_vat_code
         AND fdd.fiscal_doc_id = I_fiscal_doc_id
         AND ( nvl(dtd.total_value,0) > 0 );
/*
      SELECT count(1)
        FROM FM_FISCAL_DOC_TAX_DETAIL dtd
       WHERE dtd.vat_code = I_vat_code
         AND dtd.fiscal_doc_line_id = I_fiscal_doc_line_id
         AND (  nvl(dtd.percentage_rate,0) > 0
             or nvl(dtd.total_value,0) > 0
             or nvl(dtd.tax_basis,0) > 0 );
*/
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_FM_DOC_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
   open C_FM_DOC_TAX_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_DOC_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
   fetch C_FM_DOC_TAX_DETAIL into L_count;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_DOC_TAX_DETAIL','FM_FISCAL_DOC_TAX_DETAIL',L_key);
   close C_FM_DOC_TAX_DETAIL;
   ---
   if L_count <= 0 then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   --
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_DOC_TAX_DETAIL%ISOPEN then
            SQL_LIB.SET_MARK('CLOSE','C_FM_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
         close C_FM_DOC_TAX_DETAIL;
      end if;
      ---
      return FALSE;
END EXISTS_FISCAL_DOC_TAX_DETAIL;
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE_APPOR_INFOR
-- Purpose:       This function returns the query for Apportioned/Informed block
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_APPOR_INFOR (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_where_clause       IN OUT VARCHAR2,
                                       I_fiscal_doc_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                       I_tax_code           IN     FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE,
                                       I_po_number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                       I_discrepancy_status IN     FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                       I_mode               IN     NUMBER)
   return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.SET_WHERE_CLAUSE_APPOR';
BEGIN
   ---
   if I_fiscal_doc_id is NOT NULL then
      O_where_clause := ' fiscal_doc_id = ' || TO_CHAR(I_fiscal_doc_id) ||' and ';
   end if;
   ---
   if I_tax_code is NOT NULL then
         O_where_clause := O_where_clause || ' Tax_code = '''||I_tax_code||''' and ';
   end if;
   ---
   if I_po_number is NOT NULL then
         O_where_clause := O_where_clause || ' Po_number = '''||I_po_number||''' and ';
   end if;
   ---
   if I_mode = 0 then
      if (I_discrepancy_status = 'R') then
          O_where_clause := O_where_clause||' resolution_type is NOT NULL and ';
      elsif I_discrepancy_status = 'U' then
         O_where_clause := O_where_clause ||' resolution_type is NULL and ';
      end if;
   end if;
   ---

   if O_where_clause is NOT NULL then
      O_where_clause := SUBSTR(O_where_clause, 1, NVL(LENGTH(O_where_clause), 0)-4);
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE_APPOR_INFOR;
--
----------------------------------------------------------------------------------
 -- Function Name: UPDATE_FM_RESOLUTION
 -- Purpose:       This function Updates FM_Resolution table for discrepancy flags
 ----------------------------------------------------------------------------------
 FUNCTION UPDATE_FM_RESOLUTION(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                               I_disc_type          IN VARCHAR2,
                               I_vat_code           IN FM_RESOLUTION.TAX_CODE%TYPE,
                               I_resolution_type    IN FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                               I_reason_code        IN FM_RESOLUTION.REASON_CODE%TYPE)
    return BOOLEAN is
--
    L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.UPDATE_FM_RESOLUTION';
    L_key               VARCHAR2(80) := 'I_fiscal_doc_line_id: ' || to_char(I_fiscal_doc_line_id) ||
                                        'I_disc_type: '||I_disc_type;
    L_dummy             VARCHAR2(1);
    RECORD_LOCKED       EXCEPTION;
    PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
--
    cursor C_FM_RESOLUTION IS
    select 'x'
      from fm_resolution
     where fiscal_doc_line_id   = I_fiscal_doc_line_id
       and discrep_type = I_disc_type;
      -- for update nowait;
 BEGIN
--
    SQL_LIB.SET_MARK('OPEN', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
    open C_FM_RESOLUTION;
--
    SQL_LIB.SET_MARK('FETCH', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
    fetch C_FM_RESOLUTION into L_dummy;
--
    SQL_LIB.SET_MARK('CLOSE','C_FM_RESOLUTION', 'FM_RESOLUTION',L_key);
    close C_FM_RESOLUTION;
   ---
    SQL_LIB.SET_MARK('UPDATE','C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
--
       Update FM_RESOLUTION
          set resolution_type = I_resolution_type,
              reason_code     = I_reason_code,
              last_update_datetime = sysdate,
              last_update_id = user
        where Fiscal_doc_line_id = I_fiscal_doc_line_id
          and discrep_type = I_disc_type
          and tax_code = I_vat_code;
    ---
    return TRUE;
  EXCEPTION
    when RECORD_LOCKED then
       O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'FM_RESOLUTION',
                                             L_program,
                                             TO_CHAR(SQLCODE));
       ---
       if C_FM_RESOLUTION%ISOPEN then
          SQL_LIB.SET_MARK('CLOSE', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
          close C_FM_RESOLUTION;
       end if;
       ---
       return FALSE;
       ---
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
       ---
       if C_FM_RESOLUTION%ISOPEN then
          SQL_LIB.SET_MARK('CLOSE', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
          close C_FM_RESOLUTION;
       end if;
       ---
       return FALSE;
END UPDATE_FM_RESOLUTION;
--
 ---------------------------------------------------------------------------------
 -- Function Name: UPDATE_FISCAL_DOC_TAX_DETAIL
 -- Purpose:       This function Updates FM_FISCAL_DOC_TAX_DETAIL table for resolution tax value,
 --                tax_rate and resolution tax base value.based on the value in Apportioned screen of Tax resolution.
 ----------------------------------------------------------------------------------
 FUNCTION UPDATE_FISCAL_DOC_TAX_DETAIL(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_fiscal_doc_line_id  IN FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                       I_vat_code            IN FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE,
                                       I_res_base_value      IN FM_FISCAL_DOC_TAX_DETAIL.RES_BASE_VALUE%TYPE,
                                       I_res_tax_value       IN FM_FISCAL_DOC_TAX_DETAIL.RES_TAX_VALUE%TYPE,
                                       I_res_tax_rate        IN FM_FISCAL_DOC_TAX_DETAIL.RES_TAX_RATE%TYPE,
                                       I_res_mod_base_value  IN FM_FISCAL_DOC_TAX_DETAIL.APPR_MODIFIED_BASE_VALUE%TYPE)
    return BOOLEAN is
    L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.UPDATE_FISCAL_DOC_TAX_DETAIL';
    L_key               VARCHAR2(80) := 'I_fiscal_doc_line_id: ' || to_char(I_fiscal_doc_line_id);
    L_dummy             VARCHAR2(1);
    RECORD_LOCKED       EXCEPTION;
    PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
--
    cursor C_FISCAL_DOC_TAX_DETAIL IS
    select 'x'
      from FM_FISCAL_DOC_TAX_DETAIL
     where fiscal_doc_line_id   = I_fiscal_doc_line_id;
       --for update nowait;
 BEGIN
    SQL_LIB.SET_MARK('OPEN', 'C_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
    open C_FISCAL_DOC_TAX_DETAIL;
    SQL_LIB.SET_MARK('FETCH', 'C_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
    fetch C_FISCAL_DOC_TAX_DETAIL into L_dummy;
    SQL_LIB.SET_MARK('CLOSE','C_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL',L_key);
    close C_FISCAL_DOC_TAX_DETAIL;
   ---
    SQL_LIB.SET_MARK('UPDATE','C_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
       Update FM_FISCAL_DOC_TAX_DETAIL
          set res_base_value           = I_res_base_value,
              res_tax_value            = I_res_tax_value,
              res_tax_rate             = I_res_tax_rate,
              res_modified_base_value  = I_res_mod_base_value,
              last_update_datetime     = sysdate,
              last_update_id           = user
        where Fiscal_doc_line_id = I_fiscal_doc_line_id
          and vat_code = I_vat_code;
    ---
    return TRUE;
  EXCEPTION
    when RECORD_LOCKED then
       O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'FM_RESOLUTION',
                                             L_program,
                                             TO_CHAR(SQLCODE));
       ---
       if C_FISCAL_DOC_TAX_DETAIL%ISOPEN then
          SQL_LIB.SET_MARK('CLOSE', 'C_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
          close C_FISCAL_DOC_TAX_DETAIL;
       end if;
       ---
       return FALSE;
       ---
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
       ---
       if C_FISCAL_DOC_TAX_DETAIL%ISOPEN then
          SQL_LIB.SET_MARK('CLOSE', 'C_FISCAL_DOC_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
          close C_FISCAL_DOC_TAX_DETAIL;
       end if;
       ---
       return FALSE;
END UPDATE_FISCAL_DOC_TAX_DETAIL;
--
----------------------------------------------------------------------------------
-- Function Name: UPDATE_NF_STATUS_QTY_CENT
-- Purpose:       This function Updates NF Status to validated if all the qty discrepancy is resolved in Centralised location.
----------------------------------------------------------------------------------
FUNCTION UPDATE_NF_STATUS_QTY_CENT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_fiscal_doc_id   IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_program           VARCHAR2(80)  := 'FM_DIS_RESOLUTION_SQL.UPDATE_NF_STATUS_QTY_CENT';
   L_key               VARCHAR2(100) := 'fiscal_doc_id ' || I_fiscal_doc_id;
   L_discrepant        VARCHAR2(1):='D';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_received          VARCHAR2(1):= 'R';
   L_validated         VARCHAR2(1):= 'V';
   L_count             NUMBER;
   L_status            VARCHAR(1);
   L_triangulation     VARCHAR(1);
   L_compl_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   CURSOR C_LOCK_FM_FISCAL_DETAIL is
      SELECT 'X'
        FROM fm_fiscal_doc_detail
       WHERE fiscal_doc_id  = I_fiscal_doc_id;
--         FOR update nowait;
   CURSOR C_FM_FISCAL_DETAIL_CNT IS
      SELECT count(1)
        FROM FM_FISCAL_DOC_DETAIL
       WHERE fiscal_doc_id  = I_fiscal_doc_id
         AND qty_discrep_status   = L_discrepant;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_FM_FISCAL_DETAIL_CNT','FM_FISCAL_DOC_DETAIL',L_key);
   open C_FM_FISCAL_DETAIL_CNT;
   ---
   SQL_LIB.SET_MARK('FETCH','C_FM_FISCAL_DETAIL_CNT','FM_FISCAL_DOC_DETAIL',L_key);
   fetch C_FM_FISCAL_DETAIL_CNT into L_count;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DETAIL_CNT','FM_FISCAL_DOC_DETAIL',L_key);
   close C_FM_FISCAL_DETAIL_CNT;
   if L_count <= 0 then
      OPEN C_LOCK_FM_FISCAL_DETAIL;
      CLOSE C_LOCK_FM_FISCAL_DETAIL;
--
      UPDATE fm_fiscal_doc_header
         SET status = L_validated,
             last_update_datetime = sysdate,
             last_update_id = user
       WHERE fiscal_doc_id = i_fiscal_doc_id;
    --For Triangulation
      if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message       => O_error_message
                                                              ,I_fiscal_doc_id       => I_fiscal_doc_id
                                                              ,O_triangulation       => L_triangulation
                                                              ,O_compl_fiscal_doc_id => L_compl_fiscal_doc_id ) = FALSE then
         return FALSE;
      end if;
--
      if L_triangulation = 'Y' then
         UPDATE fm_fiscal_doc_header
            SET status = L_validated,
                last_update_datetime = sysdate,
                last_update_id = user
          WHERE fiscal_doc_id = L_compl_fiscal_doc_id;
      end if;
   end if;
--
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FM_FISCAL_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_LOCK_FM_FISCAL_DETAIL;
      end if;
      ---
      if C_LOCK_FM_FISCAL_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_FM_FISCAL_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_LOCK_FM_FISCAL_DETAIL;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_FM_FISCAL_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_LOCK_FM_FISCAL_DETAIL;
      end if;
      ---
      if C_LOCK_FM_FISCAL_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_FM_FISCAL_DETAIL', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_LOCK_FM_FISCAL_DETAIL;
      end if;
      return FALSE;
END UPDATE_NF_STATUS_QTY_CENT;
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE_TAX_DETAIL
-- Purpose:       This function returns the query for Only Tax Header Screen
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_TAX_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_where_clause       IN OUT VARCHAR2,
                                     I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                     I_PO_Number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                     I_discrepancy_status IN     FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE,
                                     I_discrep_type       IN     FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                     I_tax_type           IN     VARCHAR2,
                                     I_mode               IN NUMBER)
   return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.SET_WHERE_CLAUSE_TAX_DETAIL';
   L_count             NUMBER;
   L_key       VARCHAR2(100);
   L_where_clause1  VARCHAR2(4000);
   L_where_clause2  VARCHAR2(4000);

BEGIN
   ---
L_where_clause1 := 'tax_code in (SELECT DISTINCT fr.tax_code tax_code FROM fm_resolution fr,fm_fiscal_doc_detail fdd WHERE fr.fiscal_doc_id = '|| TO_CHAR(I_fiscal_doc_id) || ' AND fr.fiscal_doc_line_id = fdd.fiscal_doc_line_id AND fr.discrep_type = '''||I_discrep_type ||''' AND fr.resolution_type is null and to_char(fdd.requisition_no) = '||nvl(to_char(I_PO_Number),'fdd.requisition_no')||')';
L_where_clause2 := 'tax_code in (SELECT DISTINCT fr.tax_code tax_code FROM fm_resolution fr,fm_fiscal_doc_detail fdd WHERE fr.fiscal_doc_id = '|| TO_CHAR(I_fiscal_doc_id) || ' AND fr.fiscal_doc_line_id = fdd.fiscal_doc_line_id AND fr.discrep_type = '''||I_discrep_type ||''' AND fr.resolution_type is not null and to_char(fdd.requisition_no) = '||nvl(to_char(I_PO_Number),'fdd.requisition_no')||')';

   if I_fiscal_doc_id is NOT NULL then
      O_where_clause := ' fiscal_doc_id = ' || TO_CHAR(I_fiscal_doc_id) ||' and ';
   end if;
   ---
if (I_mode = 0) then
   if I_discrepancy_status = 'R' then
         O_where_clause := O_where_clause||L_where_clause2 ||' and ';
   elsif I_discrepancy_status = 'U' then
         O_where_clause := O_where_clause||L_where_clause1 ||' and ';
   end if;
end if;
   ---
   if I_PO_Number IS NOT NULL then
      O_where_clause  := O_where_clause||' po_number = '||I_PO_Number||' and ';
   end if;
   ---
   if O_where_clause is NOT NULL then
      O_where_clause := SUBSTR(O_where_clause, 1, NVL(LENGTH(O_where_clause), 0)-4);
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE_TAX_DETAIL;
--
----------------------------------------------------------------------------------
-- Function Name: UPDATE_FISCAL_DTAIL_FOR_TRIANG
-- Purpose:       This function updates the fm_fiscal_doc_detail table for discrepancy status
--                for triangulation.
----------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DTAIL_FOR_TRIANG(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                       ,I_discrepancy          IN VARCHAR2
                                       ,I_compl_fiscal_doc_id  IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE
                                       ,I_po_number            IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                                       ,I_item                 IN FM_FISCAL_DOC_DETAIL.ITEM%TYPE
                                       ,I_appt_qty             IN FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE
                                 )
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_DISCREP_IDENTIFICATION_SQL.UPDATE_DISREP_FOR_TRIANGULATION';
   L_key                 VARCHAR2(80) := 'fdd.fiscal_doc_id = '|| I_compl_fiscal_doc_id;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
--
   L_error_message       VARCHAR2(255);
   L_resolution          VARCHAR2(1):= 'R';
--
   CURSOR C_COMPL_DISCREP_UPDATE IS
      SELECT fdd.fiscal_doc_line_id
        FROM fm_fiscal_doc_detail fdd
       WHERE fdd.fiscal_doc_id     = I_compl_fiscal_doc_id
         AND fdd.requisition_no    = I_po_number
         AND fdd.item              = I_item;
--
BEGIN
--
   SQL_LIB.SET_MARK('OPEN','C_COMPL_DISCREP_UPDATE', 'fm_fiscal_doc_detail', L_key);
   OPEN C_COMPL_DISCREP_UPDATE;
   SQL_LIB.SET_MARK('CLOSE','C_COMPL_DISCREP_UPDATE', 'fm_fiscal_doc_detail', L_key);
   CLOSE C_COMPL_DISCREP_UPDATE;
--
   if I_discrepancy = 'Q' then
--
      UPDATE fm_fiscal_doc_detail
         SET qty_discrep_status = L_resolution
       WHERE fiscal_doc_id      = I_compl_fiscal_doc_id
         AND requisition_no     = I_po_number
         AND item               = I_item;
--
   elsif I_discrepancy = 'C' then
--
      UPDATE fm_fiscal_doc_detail
         SET cost_discrep_status = L_resolution
       WHERE fiscal_doc_id       = I_compl_fiscal_doc_id
         AND requisition_no      = I_po_number
         AND item                = I_item
         AND appt_qty            = I_appt_qty;
--
   end if;
--
   return TRUE;
--
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_FISCAL_DOC_COMPLEMENT',
                                             L_key,
                                             TO_CHAR(SQLCODE));
--
      if C_COMPL_DISCREP_UPDATE%ISOPEN then
         close C_COMPL_DISCREP_UPDATE;
      end if;
---
      return FALSE;
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_COMPL_DISCREP_UPDATE%ISOPEN then
         close C_COMPL_DISCREP_UPDATE;
      end if;
--
      return FALSE;
--
END UPDATE_FISCAL_DTAIL_FOR_TRIANG;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_COMPL_OR_MAIN_NF
-- Purpose:       This function identifies whether the given fiscal_doc_id is Complimentory or Main NF
----------------------------------------------------------------------------------
 FUNCTION CHECK_COMPL_OR_MAIN_NF(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_FISCAL_DOC_ID    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                 O_COMPL_NF         OUT     VARCHAR2,
                                 O_MAIN_NF          OUT     VARCHAR2)
   return BOOLEAN is

   L_program           VARCHAR2(50)  := 'FM_DIS_RESOLUTION_SQL.CHECK_COMPL_OR_MAIN_NF';
   L_key               VARCHAR2(100) := 'Fiscal_doc_id: ' ||I_fiscal_doc_id;
--
   l_compl_count       NUMBER;
   l_main_count        NUMBER;
--
   CURSOR C_GET_COMPL_NF is
      SELECT count(1)
        FROM fm_fiscal_doc_complement fdc
       WHERE fdc.compl_fiscal_doc_id = I_fiscal_doc_id;
--
   CURSOR C_GET_MAIN_NF is
      SELECT count(1)
        FROM fm_fiscal_doc_complement fdc
       WHERE fdc.fiscal_doc_id = I_fiscal_doc_id;
--
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_COMPL_NF', 'fm_fiscal_doc_complement', L_key);
   OPEN C_GET_COMPL_NF;
   FETCH C_GET_COMPL_NF INTO l_compl_count;
   CLOSE C_GET_COMPL_NF;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_COMPL_NF', 'fm_fiscal_doc_complement', L_key);
--
   SQL_LIB.SET_MARK('OPEN', 'C_GET_MAIN_NF', 'fm_fiscal_doc_complement', L_key);
   OPEN C_GET_MAIN_NF;
   FETCH C_GET_MAIN_NF INTO l_main_count;
   CLOSE C_GET_MAIN_NF;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_MAIN_NF', 'fm_fiscal_doc_complement', L_key);
--
   if l_compl_count > 0 then
      O_compl_nf := 'Y';
      O_main_nf  := 'N';
   elsif l_main_count > 0 then
      O_compl_nf := 'N';
      O_main_nf  := 'Y';
   end if;
--
   return TRUE;
--
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END CHECK_COMPL_OR_MAIN_NF;

----------------------------------------------------------------------------------
-- Function Name: GET_MAIN_NF_FOR_COMPL_NF
-- Purpose:       This function gets the Main NF for complementory NF
----------------------------------------------------------------------------------
 FUNCTION GET_MAIN_NF_FOR_COMPL_NF(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_FISCAL_DOC_ID       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                   O_MAIN_FISCAL_DOC_ID  OUT     FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is

   L_program           VARCHAR2(50)  := 'FM_DIS_RESOLUTION_SQL.GET_MAIN_NF_FOR_COMPL_NF';
   L_key               VARCHAR2(100) := 'Fiscal_doc_id: ' ||I_fiscal_doc_id;
--
   l_main_nf           FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE;
--
   CURSOR C_GET_MAIN_NF is
      SELECT fiscal_doc_id
        FROM fm_fiscal_doc_complement fdc
       WHERE fdc.compl_fiscal_doc_id = I_fiscal_doc_id;
--
BEGIN
--
   SQL_LIB.SET_MARK('OPEN', 'C_GET_MAIN_NF', 'fm_fiscal_doc_complement', L_key);
   OPEN C_GET_MAIN_NF;
   FETCH C_GET_MAIN_NF INTO l_main_nf;
   CLOSE C_GET_MAIN_NF;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_MAIN_NF', 'fm_fiscal_doc_complement', L_key);
--
   o_main_fiscal_doc_id := l_main_nf;
--
   return TRUE;
--
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END GET_MAIN_NF_FOR_COMPL_NF;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_UPDATE_COMP_NF_STATUS
-- Purpose:       This function Updates FM_FISCAL_DOC_HEADER NF Status for complementory NF
----------------------------------------------------------------------------------
FUNCTION CHECK_UPDATE_COMP_NF_STATUS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_fiscal_doc_id         IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                     I_current_nf_status     IN FM_FISCAL_DOC_HEADER.STATUS%TYPE)
   return BOOLEAN is
   L_program           VARCHAR2(50)  := 'FM_DIS_RESOLUTION_SQL.CHECK_UPDATE_NF_STATUS';
   L_key               VARCHAR2(100) := 'I_Fiscal_doc_id: ' || to_char(I_fiscal_doc_id) ||
                                        ' I_current_nf_status: '||I_current_nf_status;
   L_dummy             VARCHAR2(1);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_unresolved        VARCHAR2(1):= 'U';
   L_discrepant        VARCHAR2(1):= 'D';
   L_validated         VARCHAR2(1):= 'V';
   L_approved          VARCHAR2(1):= 'A';
   L_header_count      NUMBER;
   L_detail_count      NUMBER;
--
   cursor C_FM_FISCAL_HEAD_CNT IS
      SELECT count(1)
        FROM fm_fiscal_doc_header
       WHERE fiscal_doc_id = I_fiscal_doc_id
         AND (tax_discrep_status = L_discrepant);
--
   cursor C_FM_FISCAL_DETAIL_CNT IS
      SELECT count(1)
        FROM FM_FISCAL_DOC_DETAIL
       WHERE fiscal_doc_id  = I_fiscal_doc_id
         AND tax_discrep_status  = L_discrepant;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_HEAD_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_HEAD_CNT;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_HEAD_CNT','FM_FISCAL_DOC_HEADER',L_key);
   fetch C_FM_FISCAL_HEAD_CNT into L_header_count;
--   L_header_count := C_FM_FISCAL_HEAD_CNT%ROWCOUNT;
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_HEAD_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_HEAD_CNT;
--
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_DETAIL', L_key);
   open C_FM_FISCAL_DETAIL_CNT;
   SQL_LIB.SET_MARK('FETCH', 'C_FM_FISCAL_DETAIL_CNT','FM_FISCAL_DOC_DETAIL', L_key);
   fetch C_FM_FISCAL_DETAIL_CNT into L_detail_count;
   SQL_LIB.SET_MARK('CLOSE','C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DETAIL_CNT;
   ---
   --- check if the current status of NF is in discrepancy,received, received-verified, then change the status
   if I_current_nf_status in ('D','R','RV') then
   --- Check if there are discrepant records and all of them have been resolved, only then update the NF status
      if L_detail_count = 0 and L_header_count = 0 then
   --- if the current nf status is discrepant then update to validated
            if I_current_nf_status = 'D' then
              SQL_LIB.SET_MARK('UPDATE','C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
               Update FM_FISCAL_DOC_HEADER
                  set status = L_validated,
                      LAST_UPDATE_DATETIME = sysdate,
                      LAST_UPDATE_ID = user
                where Fiscal_doc_id = I_fiscal_doc_id;
   --- if the current status in receipt, receipt-verified then update the nf status to 'Approved'
            elsif I_current_nf_status in ('R','RV') then
              SQL_LIB.SET_MARK('UPDATE','C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
--
              if FM_RECEIVING_SQL.CALC_FINAL_CLT(O_error_message  => O_error_message,
                                                 I_fiscal_doc_id  => I_fiscal_doc_id)= FALSE then
                 return FALSE;
              end if;
--
              Update FM_FISCAL_DOC_HEADER
                 set status = L_approved,
                     LAST_UPDATE_DATETIME = sysdate,
                     LAST_UPDATE_ID = user
               where Fiscal_doc_id = I_fiscal_doc_id;
            end if;
         end if;
   end if;
   ---
   return TRUE;
 EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_HEAD_CNT%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_HEAD_CNT', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_HEAD_CNT;
      end if;
      ---
      if C_FM_FISCAL_DETAIL_CNT%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_FM_FISCAL_DETAIL_CNT;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_FISCAL_HEAD_CNT%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
         close C_FM_FISCAL_HEAD_CNT;
      end if;
      ---
      if C_FM_FISCAL_DETAIL_CNT%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DETAIL_CNT', 'FM_FISCAL_DOC_DETAIL', L_key);
         close C_FM_FISCAL_DETAIL_CNT;
      end if;
      return FALSE;
END CHECK_UPDATE_COMP_NF_STATUS;
--
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE_INCON_RULE
-- Purpose:       This function returns the query for Only for Inconclusive Tax rule screen
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_INCON_RULE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_where_clause       IN OUT VARCHAR2,
                                     I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.SET_WHERE_CLAUSE_INCON_RULE';
BEGIN
   ---
   if I_fiscal_doc_id is NOT NULL then
      O_where_clause := ' fiscal_doc_line_id in (select fiscal_doc_line_id from fm_fiscal_doc_detail where fiscal_doc_id = ' || TO_CHAR(I_fiscal_doc_id)||')and ';
   end if;
   ---
   if O_where_clause is NOT NULL then
      O_where_clause := SUBSTR(O_where_clause, 1, NVL(LENGTH(O_where_clause), 0)-4);
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_WHERE_CLAUSE_INCON_RULE;
--
----------------------------------------------------------------------------------
-- Function Name: GET_RESOLUTION_TYPE
-- Purpose:       This function returns the Resolution Type for the line Items which are already resolved.
--                This is to check all the Pack Items should have the Same resolution type for Qty Disc resolution.
----------------------------------------------------------------------------------
FUNCTION GET_RESOLUTION_TYPE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_resolution_type    IN OUT VARCHAR2,
                             I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                             I_pack_no            IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE)
   return BOOLEAN is
--
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_RESOLUTION_TYPE';
   L_key       VARCHAR2(100) := 'I_Fiscal_doc_id: ' || to_char(I_fiscal_doc_id);
--
   cursor C_GET_RESOLUTION_TYPE IS
      SELECT distinct fr.resolution_type
        FROM fm_resolution fr
            ,fm_fiscal_doc_detail fdd
       WHERE fr.fiscal_doc_id      = fdd.fiscal_doc_id
         AND fr.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         AND fr.fiscal_doc_id      = I_fiscal_doc_id
         AND fdd.pack_no           = I_pack_no
         AND fr.discrep_type       = 'Q'
         AND resolution_type is not null;
BEGIN
--
   SQL_LIB.SET_MARK('OPEN', 'C_GET_RESOLUTION_TYPE', 'fm_resolution', L_key);
   open C_GET_RESOLUTION_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_RESOLUTION_TYPE','fm_resolution',L_key);
   fetch C_GET_RESOLUTION_TYPE into O_resolution_type;
   SQL_LIB.SET_MARK('CLOSE','C_GET_RESOLUTION_TYPE', 'fm_resolution', L_key);
   close C_GET_RESOLUTION_TYPE;
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_GET_RESOLUTION_TYPE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_RESOLUTION_TYPE', 'fm_resolution', L_key);
         close C_GET_RESOLUTION_TYPE;
      end if;
--
      return FALSE;
--
END GET_RESOLUTION_TYPE;
--
----------------------------------------------------------------------------------
-- Function Name: GET_DISCREP_STATUS
-- Purpose:       This function returns the discrepancy status to the view based on the parameters passed.
----------------------------------------------------------------------------------
FUNCTION GET_DISCREP_STATUS(I_discrep_type       IN     VARCHAR2,
                            I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_po_number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE)
   return VARCHAR2 is
--
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_DISCREP_STATUS';
   L_key       VARCHAR2(100) := 'I_Fiscal_doc_id: ' || to_char(I_fiscal_doc_id);
   L_discrep_status VARCHAR2(5);
--
   cursor C_QTY_DISCREP_STATUS IS
      SELECT qty_discrep_status
        FROM(SELECT qty_discrep_status
               FROM fm_fiscal_doc_detail
              WHERE requisition_no = I_po_number
                AND fiscal_doc_id  = I_fiscal_doc_id
                AND qty_discrep_status in ('D','R')
           ORDER BY DECODE(qty_discrep_status,'D',1,'R',2))
       WHERE rownum < 2;
--
   cursor C_COST_DISCREP_STATUS IS
      SELECT cost_discrep_status
        FROM(SELECT cost_discrep_status
               FROM fm_fiscal_doc_detail
              WHERE requisition_no = I_po_number
                AND fiscal_doc_id  = I_fiscal_doc_id
                AND cost_discrep_status in ('D','R')
           ORDER BY DECODE(cost_discrep_status,'D',1,'R',2))
       WHERE rownum < 2;
--
   cursor C_TAX_DISCREP_STATUS IS
      SELECT tax_discrep_status
        FROM(SELECT tax_discrep_status
               FROM fm_fiscal_doc_detail
              WHERE requisition_no = I_po_number
                AND fiscal_doc_id  = I_fiscal_doc_id
                AND tax_discrep_status in ('D','R')
           ORDER BY DECODE(tax_discrep_status,'D',1,'R',2))
       WHERE rownum < 2;
--
BEGIN
--
   if I_discrep_type = 'Q' then
      SQL_LIB.SET_MARK('OPEN', 'C_QTY_DISCREP_STATUS', 'fm_fiscal_doc_detail', L_key);
      open C_QTY_DISCREP_STATUS;
      SQL_LIB.SET_MARK('FETCH', 'C_QTY_DISCREP_STATUS','fm_fiscal_doc_detail',L_key);
      fetch C_QTY_DISCREP_STATUS into L_discrep_status;
      SQL_LIB.SET_MARK('CLOSE','C_QTY_DISCREP_STATUS', 'fm_fiscal_doc_detail', L_key);
      close C_QTY_DISCREP_STATUS;
   elsif I_discrep_type = 'C' then
      SQL_LIB.SET_MARK('OPEN', 'C_COST_DISCREP_STATUS', 'fm_fiscal_doc_detail', L_key);
      open C_COST_DISCREP_STATUS;
      SQL_LIB.SET_MARK('FETCH', 'C_COST_DISCREP_STATUS','fm_fiscal_doc_detail',L_key);
      fetch C_COST_DISCREP_STATUS into L_discrep_status;
      SQL_LIB.SET_MARK('CLOSE','C_COST_DISCREP_STATUS', 'fm_fiscal_doc_detail', L_key);
      close C_COST_DISCREP_STATUS;
   elsif I_discrep_type = 'T' then
      SQL_LIB.SET_MARK('OPEN', 'C_TAX_DISCREP_STATUS', 'fm_fiscal_doc_detail', L_key);
      open C_TAX_DISCREP_STATUS;
      SQL_LIB.SET_MARK('FETCH', 'C_TAX_DISCREP_STATUS','fm_fiscal_doc_detail',L_key);
      fetch C_TAX_DISCREP_STATUS into L_discrep_status;
      SQL_LIB.SET_MARK('CLOSE','C_TAX_DISCREP_STATUS', 'fm_fiscal_doc_detail', L_key);
      close C_TAX_DISCREP_STATUS;
    end if;
--
   return nvl(L_discrep_status,'M');
--
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
END GET_DISCREP_STATUS;

--

FUNCTION GET_HEADER_DISC_STATUS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cost_disc_status          IN OUT FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE,
                                O_qty_disc_status           IN OUT FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE,
                                O_tax_disc_status           IN OUT FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                I_schedule_no               IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                                I_po_number                 IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                I_fiscal_doc_id             IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
return BOOLEAN is
   L_program   VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_HEADER_DISC_STATUS';
--
   L_count     Number;
--
   cursor C_GET_DISC is
      SELECT COST_DISCREP_STATUS,QTY_DISCREP_STATUS,TAX_DISCREP_STATUS
        FROM v_fm_dis_resolution
       WHERE nvl(schedule_no,-999) = nvl(I_schedule_no,-999)
         AND po_number = I_po_number
         AND fiscal_doc_id = I_fiscal_doc_id;
--
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_DISC','fm_fiscal_doc_header','fiscal_doc_no = '||I_fiscal_doc_id);
   OPEN  C_get_disc;
   SQL_LIB.SET_MARK('FETCH','C_GET_DISC','fm_fiscal_doc_header','fiscal_doc_no = '||I_fiscal_doc_id);
   FETCH C_get_disc INTO O_cost_disc_status,O_qty_disc_status,O_tax_disc_status;
   SQL_LIB.SET_MARK('CLOSE','C_GET_DISC','fm_fiscal_doc_header','fiscal_doc_no = '||I_fiscal_doc_id);
   CLOSE C_get_disc;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END get_header_disc_status;
-------------------------------------------------------------------------------------------------------
 FUNCTION GET_FM_RESOLUTION(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            I_disc_type          IN VARCHAR2,
                            I_vat_code           IN FM_RESOLUTION.TAX_CODE%TYPE,
                            O_resolution_type    IN OUT FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                            O_reason_code        IN OUT FM_RESOLUTION.REASON_CODE%TYPE)
    return BOOLEAN is
--
    L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.GET_FM_RESOLUTION';
    L_key               VARCHAR2(80) := 'I_fiscal_doc_line_id: ' || to_char(I_fiscal_doc_line_id) ||
                                        'I_disc_type: '||I_disc_type;
--
    cursor C_FM_RESOLUTION IS
    select resolution_type,reason_code
      from fm_resolution
     where fiscal_doc_line_id   = I_fiscal_doc_line_id
       and discrep_type = I_disc_type
       and tax_code = I_vat_code;
 BEGIN
--
    SQL_LIB.SET_MARK('OPEN', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
    open C_FM_RESOLUTION;
--
    SQL_LIB.SET_MARK('FETCH', 'C_FM_RESOLUTION', 'FM_RESOLUTION', L_key);
    fetch C_FM_RESOLUTION into O_resolution_type,O_reason_code;
--
    SQL_LIB.SET_MARK('CLOSE','C_FM_RESOLUTION', 'FM_RESOLUTION',L_key);
    close C_FM_RESOLUTION;

    return TRUE;
  EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_FM_RESOLUTION;
---
---------------------------------------------------------------------------------------------------------------
 FUNCTION EXIST_DISCREP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_exists           IN OUT    BOOLEAN)
   return BOOLEAN is
    ---
    L_program           VARCHAR2(50) := 'FM_DIS_RESOLUTION_SQL.EXIST_DISCREP';
    L_key               VARCHAR2(80) := 'I_fiscal_doc_id: ' || to_char(I_fiscal_doc_id);
    L_cnt               NUMBER;
    ---
    cursor C_EXIST_RESOLUTION IS
    select COUNT(1)
      from fm_resolution
     where fiscal_doc_id   = I_fiscal_doc_id;
    ---
 BEGIN
    ---
    SQL_LIB.SET_MARK('OPEN', 'C_EXIST_RESOLUTION', 'FM_RESOLUTION', L_key);
    open C_EXIST_RESOLUTION ;
    ---
    SQL_LIB.SET_MARK('FETCH', 'C_EXIST_RESOLUTION', 'FM_RESOLUTION', L_key);
    fetch C_EXIST_RESOLUTION into L_cnt;
    ---
    SQL_LIB.SET_MARK('CLOSE','C_EXIST_RESOLUTION', 'FM_RESOLUTION',L_key);
    close C_EXIST_RESOLUTION ;
    ---
    if L_cnt > 0 then
       I_exists := TRUE;
    else
       I_exists := FALSE;
    end if;
    ---
    return TRUE;
 EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
 END EXIST_DISCREP;
---------------------------------------------------------------------------------------------------------------

END fm_dis_resolution_sql;
/