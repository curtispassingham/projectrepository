CREATE OR REPLACE PACKAGE FM_FISCAL_DOC_DETAIL_SQL as
---------------------------------------------------------------------------------
-- CREATE DATE - 03-05-2007
-- CREATE USER ? Sigma.EPP
-- PROJECT / FRD - 10481 / 10481_ORFMI_FRD_001_V1.0
-- DESCRIPTION ? Package associated to table FM_FISCAL_DOC_DETAIL.
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Function Name: GET_NEXT_FISCAL_DOC_ID
-- Purpose:       This function gets the next fiscal_doc_line_id number.
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_FISCAL_DOC_LINE_ID(O_error_message       IN OUT VARCHAR2,
                                     O_fiscal_doc_line_id  IN OUT FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: GET_NEXT_LINE_NO
-- Purpose:       This function gets the next line_no for a document.
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_LINE_NO(O_error_message IN OUT VARCHAR2,
                          O_line_no       IN OUT FM_FISCAL_DOC_DETAIL.LINE_NO%TYPE,
                          I_fiscal_doc_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: EXISTS_DETAIL
-- Purpose:       This function will check if exists detail on document.
----------------------------------------------------------------------------------
FUNCTION EXISTS_DETAIL(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_fiscal_doc_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: GET_FISCAL_DOC_DETAIL_INFO
-- Purpose:       This function gets the FISCAL_DOC_DETAIL informations.
----------------------------------------------------------------------------------
FUNCTION GET_FISCAL_DOC_DETAIL_INFO(O_error_message      IN OUT VARCHAR2,
                                    O_fiscal_doc_detail  IN OUT FM_FISCAL_DOC_DETAIL%ROWTYPE,
                                    I_fiscal_doc_line_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
END FM_FISCAL_DOC_DETAIL_SQL;
/
 