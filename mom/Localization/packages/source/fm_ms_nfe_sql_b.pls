CREATE OR REPLACE PACKAGE BODY FM_MS_NFE_SQL IS
--------------------------------------------------------------------------------
FUNCTION UPDATE_NFE_INFO(O_error_message  IN OUT  VARCHAR2,
                         I_ms_rfm_rec     IN      "OBJ_MS_RFM_NFE_REC")
   return BOOLEAN is
   ---
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program      VARCHAR2(100) := 'FM_MS_RFM_NFE.UPDATE_NFE_INFO';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER';
   L_key          VARCHAR2(100);
   ---
   cursor C_LOCK_HEADER(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = P_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   if I_ms_rfm_rec.nfe_access_key is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'nfe_access_key',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_ms_rfm_rec.nfe_protocol is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'nfe_protocol',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_ms_rfm_rec.nfe_danfe_url is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'nfe_danfe_url',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_LOCK_HEADER';
   L_key    := 'Fiscal_doc_id: ' || TO_CHAR(I_ms_rfm_rec.fiscal_doc_id);
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_LOCK_HEADER(I_ms_rfm_rec.fiscal_doc_id);
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_LOCK_HEADER;
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
   update fm_fiscal_doc_header ffdh
      set ffdh.nfe_accesskey = I_ms_rfm_rec.nfe_access_key,
          ffdh.nfe_protocol  = I_ms_rfm_rec.nfe_protocol,
          ffdh.nfe_danfe_url = I_ms_rfm_rec.nfe_danfe_url
    where ffdh.fiscal_doc_id = I_ms_rfm_rec.fiscal_doc_id;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_HEADER', L_table, L_key);
         close C_LOCK_HEADER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_HEADER', L_table, L_key);
         close C_LOCK_HEADER;
      end if;
      ---
      return FALSE;
END UPDATE_NFE_INFO;
--------------------------------------------------------------------------------
FUNCTION CONSUME_NFE(O_status_code    IN OUT  NUMBER,
                     O_error_message  IN OUT  VARCHAR2,
                     I_message        IN      "OBJ_MS_RFM_NFE_REC")
RETURN BOOLEAN IS
    L_program              VARCHAR2(100)             := 'FM_MS_RFM_NFE.CONSUME_NFE';
    L_count                NUMBER                    := NULL;
    L_ms_rfm_rec           "OBJ_MS_RFM_NFE_REC"      := NULL;
    L_errorDtl_tbl         "OBJ_MS_RFM_ErrorDtl_TBL" := NULL;
    L_errorDtl_rec         "OBJ_MS_RFM_ErrorDtl_REC" := NULL;
    L_status               VARCHAR2(6)               := NULL;
    ---
    L_schedule_no          FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE      := NULL;
    L_req_type             FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := NULL;
    L_table                VARCHAR2(150);
    L_key                  VARCHAR2(100);
    ---
    cursor C_GET_SCHEDULE(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) is
     select ffdh.schedule_no,
            ffdh.requisition_type
       from fm_fiscal_doc_header ffdh
      where ffdh.fiscal_doc_id = P_fiscal_doc_id;
    cursor C_LOCK_NFE_STG_DESC(P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)  is
      select 1
        from fm_stg_nfe fsn
       where fsn.fiscal_doc_id = P_fiscal_doc_id
         for update nowait;
    ---
    cursor C_GET_PREV_STATUS (P_fiscal_doc_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)  is
      select prev_status
        from fm_stg_nfe fsn
       where fsn.fiscal_doc_id = P_fiscal_doc_id;
BEGIN
    ---
    O_status_code := 1;
    ---
    if I_message is null then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_message',
                                              L_program,
                                              NULL);
       return FALSE;
    end if;
    L_ms_rfm_rec := I_message;
    L_errorDtl_tbl := L_ms_rfm_rec.errorDtl_tbl;
    L_status := L_ms_rfm_rec.status;
    ---
    open C_GET_SCHEDULE(L_ms_rfm_rec.fiscal_doc_id);
    fetch C_GET_SCHEDULE into L_schedule_no,L_req_type;
    close C_GET_SCHEDULE;
    ---
    if L_status = 'NFE_I' then
        if UPDATE_NFE_STG_STATUS(O_error_message,
                                 L_status,
                                 L_ms_rfm_rec.fiscal_doc_id) = FALSE then
            return FALSE;
        end if;
    elsif L_status = 'A' then
        if UPDATE_NFE_INFO(O_error_message,
                           L_ms_rfm_rec) = FALSE then
            return FALSE;
        end if;
         --Delete the entry from NFE Staging Table.
        L_table := 'FM_STG_NFE';
        L_key   := 'fiscal_doc_id = '|| L_ms_rfm_rec.fiscal_doc_id;
        SQL_LIB.SET_MARK('OPEN','C_LOCK_NFE_STG_DESC',L_table,L_key);

        open C_LOCK_NFE_STG_DESC(L_ms_rfm_rec.fiscal_doc_id);
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_NFE_STG_DESC',L_table,L_key);

        close C_LOCK_NFE_STG_DESC;
        ---
        SQL_LIB.SET_MARK('DELETE',NULL,L_table,L_key);
        delete fm_stg_nfe where fiscal_doc_id = L_ms_rfm_rec.fiscal_doc_id;
        savepoint before_approve;
        if FM_EXIT_NF_CREATION_SQL.SCHEDULE_APPROVAL(O_error_message,
                                                     L_ms_rfm_rec.fiscal_doc_id,
                                                     L_schedule_no,
                                                     L_req_type,
                                                     'Y') = FALSE then
             rollback to before_approve;
             if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                             L_ms_rfm_rec.fiscal_doc_id,
                                             L_program,
                                             L_program,
                                             'E',
                                             'NFE_PROCESSING_ERROR',
                                              O_error_message,
                                             'Fiscal_doc_id: '||
                                             TO_CHAR(L_ms_rfm_rec.fiscal_doc_id)) = FALSE then
                  return FALSE;
             end if;
             if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                       L_ms_rfm_rec.fiscal_doc_id,
                                                       'F') = FALSE then
                 return FALSE;
             end if;
             O_error_message:=NULL;
             return TRUE;
         end if;
    elsif L_status = 'E' then
        for L_count in L_errorDtl_tbl.first .. L_errorDtl_tbl.last
        loop
          L_errorDtl_rec := L_errorDtl_tbl(L_Count);
          if L_errorDtl_rec.error_id in (286,296) then
             OPEN C_GET_PREV_STATUS (L_ms_rfm_rec.fiscal_doc_id);
             FETCH C_GET_PREV_STATUS into L_status;
             CLOSE C_GET_PREV_STATUS;
             if UPDATE_NFE_STG_STATUS(O_error_message,
                                      L_status,
                                      L_ms_rfm_rec.fiscal_doc_id) = FALSE then
                 return FALSE;
             end if;
          else
             if UPDATE_NFE_STG_STATUS(O_error_message,
                                      L_status,
                                      L_ms_rfm_rec.fiscal_doc_id) = FALSE then
                 return FALSE;
             end if;
             if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                       L_ms_rfm_rec.fiscal_doc_id,
                                                       L_status) = FALSE then
                 return FALSE;
             end if;
             ---
             if FM_SCHEDULE_SQL.UPDATE_STATUS(O_error_message,
                                              L_schedule_no,
                                              L_status) = FALSE then
               return FALSE;
             end if;
             ---
             if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                             L_ms_rfm_rec.fiscal_doc_id,
                                             L_program,
                                             L_program,
                                             'E',
                                             'NFE_PROCESSING_ERROR',
                                              L_errorDtl_rec.error_desc,
                                             'Fiscal_doc_id: '||
                                             TO_CHAR(L_ms_rfm_rec.fiscal_doc_id)) = FALSE then
                  return FALSE;
             end if;
          end if;
        end loop;
    elsif L_status in ('C_A','N_A') then
       if L_status = 'C_A' then
          L_status := 'CN';
       else
          L_status := 'N';
       end if;
       if FM_FISCAL_DOC_HEADER_SQL.UPDATE_STATUS(O_error_message,
                                                 L_ms_rfm_rec.fiscal_doc_id,
                                                 L_status) = FALSE then
           return FALSE;
       end if;
       L_status := 'W';
       if FM_SCHEDULE_SQL.UPDATE_STATUS(O_error_message,
                                        L_schedule_no,
                                        L_status) = FALSE then
           return FALSE;
       end if;
       if FM_EXIT_NF_CREATION_SQL.UPDATE_FISCAL_DOC_ID_STG_TB(O_error_message,
                                                              L_schedule_no) = FALSE then
           return FALSE;
       end if;

       L_table := 'FM_STG_NFE';
       L_key   := 'fiscal_doc_id = '|| L_ms_rfm_rec.fiscal_doc_id;
       SQL_LIB.SET_MARK('OPEN','C_LOCK_NFE_STG_DESC',L_table,L_key);
       open C_LOCK_NFE_STG_DESC(L_ms_rfm_rec.fiscal_doc_id);
       SQL_LIB.SET_MARK('CLOSE','C_LOCK_NFE_STG_DESC',L_table,L_key);
       close C_LOCK_NFE_STG_DESC;
       ---
       SQL_LIB.SET_MARK('DELETE',NULL,L_table,L_key);
       delete fm_stg_nfe where fiscal_doc_id = L_ms_rfm_rec.fiscal_doc_id;
   end if;
return TRUE;
EXCEPTION
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
        if C_LOCK_NFE_STG_DESC%ISOPEN then
         close C_LOCK_NFE_STG_DESC;
        end if;
        return FALSE;
END CONSUME_NFE;
--------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code    IN OUT  NUMBER,
                  O_error_message  IN OUT  VARCHAR2,
                  I_message        IN      "OBJ_MS_RFM_NFE_REC")
is
L_program      VARCHAR2(100) := 'FM_MS_RFM_NFE.CONSUME';
BEGIN
    ---
    if CONSUME_NFE(O_status_code,
                   O_error_message,
                   I_message) = FALSE then
        O_status_code := 0;
    end if;
    ---
EXCEPTION
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
        O_status_code := 0;
END CONSUME;
--------------------------------------------------------------------------------
FUNCTION UPDATE_NFE_STG_STATUS(O_error_message  IN OUT  VARCHAR2,
                               I_status         IN      FM_STG_NFE.STATUS%TYPE,
                               I_fiscal_doc_id  IN      FM_STG_NFE.FISCAL_DOC_ID%TYPE)
  ---
   return BOOLEAN is
   ---
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program       VARCHAR2(100) := 'FM_MS_RFM_NFE.UPDATE_NFE_STG_STATUS';
   L_cursor        VARCHAR2(100);
   L_table         VARCHAR2(100) := 'FM_NFE_STG';
   L_key           VARCHAR2(100) := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
   L_justification FM_STG_NFE.JUSTIFICATION%TYPE := 'NF cancelada pelo emitente';
   ---
   cursor C_LOCK_NFE_STG is
      select 'X'
        from fm_stg_nfe fsn
       where fsn.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;
   ---
BEGIN
   ---
   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_LOCK_STG_NFE';
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_LOCK_NFE_STG;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_LOCK_NFE_STG;
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
   update fm_stg_nfe fsn
      set fsn.status = I_status,
          fsn.justification = decode(I_status,'NFE_C',L_justification,null)
    where fsn.fiscal_doc_id = I_fiscal_doc_id;
   ---
   update fm_stg_nfe fsn
      set fsn.prev_status = I_status
    where fsn.fiscal_doc_id = I_fiscal_doc_id
      and I_status in ('NFE_C','NFE_P','NFE_X');
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FN_STG_NFE',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_NFE_STG%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_STG_NFE', L_table, L_key);
         close C_LOCK_NFE_STG;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_NFE_STG%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_STG_NFE', L_table, L_key);
         close C_LOCK_NFE_STG;
      end if;
      ---
      return FALSE;
END UPDATE_NFE_STG_STATUS;
--------------------------------------------------------------------------------
END FM_MS_NFE_SQL;
/