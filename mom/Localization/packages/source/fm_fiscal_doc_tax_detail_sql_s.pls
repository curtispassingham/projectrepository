CREATE OR REPLACE PACKAGE FM_FISCAL_DOC_TAX_DETAIL_SQL as
---------------------------------------------------------------------------------
-- CREATE DATE - 03-05-2007
-- CREATE USER ? Sigma.EPP
-- PROJECT / FRD - 10481 / 10481_ORFMI_FRD_001
-- DESCRIPTION ? Package associated to table FM_FISCAL_DOC_TAX_DETAIL.
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Function Name: EXISTS
-- Purpose:       This function checks if already exists.
----------------------------------------------------------------------------------
FUNCTION EXISTS(O_error_message       IN OUT VARCHAR2,
                O_exists              IN OUT BOOLEAN,
                I_vat_code            IN     FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE,
                I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: EXISTS_ANY_TAX_DETAIL
-- Purpose:       This function checks if already exists any tax detail to document.
----------------------------------------------------------------------------------
FUNCTION EXISTS_ANY_TAX_DETAIL(O_error_message       IN OUT VARCHAR2,
                               O_exists              IN OUT BOOLEAN,
                               I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------
-- Function Name: DELETE_TAX_DETAIL
-- Purpose:       This function deletes all taxes details from an especific item of NF.
----------------------------------------------------------------------------------
FUNCTION DELETE_TAX_DETAIL(O_error_message       IN OUT VARCHAR2,
                           I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
END FM_FISCAL_DOC_TAX_DETAIL_SQL;
/
 