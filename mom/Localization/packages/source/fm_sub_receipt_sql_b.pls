CREATE OR REPLACE PACKAGE BODY FM_SUB_RECEIPT_SQL AS
---------------------------------------------------------------------------------
--Local Specs
---------------------------------------------------------------------------------
   TYPE NF_REC_TYPE IS RECORD(fiscal_doc_id    FM_RECEIVING_HEADER.FISCAL_DOC_ID%TYPE,
                              requisition_no   FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE,
                              item             FM_RECEIVING_DETAIL.ITEM%TYPE,
                              appt_qty         FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE);

   TYPE NF_TBL IS TABLE OF NF_REC_TYPE INDEX BY BINARY_INTEGER;
------------------------------------------------------------------------------------
   TYPE NF_OVER_REC_TYPE IS RECORD(fiscal_doc_id    FM_RECEIVING_HEADER.FISCAL_DOC_ID%TYPE,
                                   requisition_no   FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE,
                                   item             FM_RECEIVING_DETAIL.ITEM%TYPE,
                                   nf_qty           FM_RECEIVING_DETAIL.QUANTITY%TYPE,
                                   qty_received     FM_RECEIVING_DETAIL.QUANTITY%TYPE,
                                   overage_qty      FM_RECEIVING_DETAIL.OVERAGE_QTY%TYPE);

   TYPE NF_OVER_TBL IS TABLE OF NF_OVER_REC_TYPE INDEX BY BINARY_INTEGER;
----------------------------------------------------------------------------------------
/* internal functions */
----------------------------------------------------------------------------------------
FUNCTION PROCESS_RECEIPT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_nf               IN OUT   NF_TBL,
                         I_schedule_no      IN       FM_RECEIVING_HEADER.RECV_NO%TYPE,
                         I_requisition_no   IN       FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE,
                         I_item             IN       FM_RECEIVING_DETAIL.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program   VARCHAR2(100)  := 'FM_SUB_RECEIPT_SQL.PROCESS_RECEIPT';

   L_nf_tab   NF_TBL;
   L_index    NUMBER   := 1;
   L_count    INTEGER  := 0;

   cursor C_GET_NF is
      select frh.fiscal_doc_id,
             frd.requisition_no,
             frd.item,
             fdd.appt_qty
        from fm_receiving_header frh,
             fm_receiving_detail frd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where frh.seq_no = frd.header_seq_no
         and fdh.schedule_no = frh.recv_no
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.fiscal_doc_id = frh.fiscal_doc_id
         and fdh.status = 'S'
         and frd.item = fdd.item
         and frd.requisition_no = fdd.requisition_no
         and frh.recv_no= I_schedule_no
         and frd.requisition_no = I_requisition_no
         and frd.item = I_item
       union all
      select frh.fiscal_doc_id,
             frd.requisition_no,
             frd.item,
             fdd1.appt_qty
        from fm_receiving_header frh,
             fm_receiving_detail frd,
             fm_fiscal_doc_header fdh,
             (select DISTINCT fdd.fiscal_doc_id fiscal_doc_id,
                     fdd.pack_no pack_no,
                     (fdd.appt_qty/pi.pack_qty) appt_qty,
                     fdd.requisition_no requisition_no
                from fm_fiscal_doc_detail fdd, packitem pi
               where fdd.item = pi.item
                 and fdd.pack_no = pi.pack_no) fdd1
       where frh.seq_no = frd.header_seq_no
         and fdh.schedule_no = frh.recv_no
         and fdh.fiscal_doc_id = fdd1.fiscal_doc_id
         and fdh.fiscal_doc_id = frh.fiscal_doc_id
         and fdh.status = 'S'
         and frd.item = fdd1.pack_no
         and NVL(frd.requisition_no,0) = NVL(fdd1.requisition_no,0)
         and frh.recv_no= I_schedule_no
         and frd.requisition_no = I_requisition_no
         and frd.item = I_item
       group by frh.fiscal_doc_id, frd.requisition_no, frd.item, fdd1.appt_qty
       union all
      select frh.fiscal_doc_id,
             frd.requisition_no requisition_no,
             frd.item, fdd.appt_qty
        from fm_receiving_header frh,
             fm_receiving_detail frd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where frh.seq_no = frd.header_seq_no
         and fdh.schedule_no = frh.recv_no
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.fiscal_doc_id = frh.fiscal_doc_id
         and fdh.status = 'S'
         and frd.item = fdd.item
         and frh.recv_no= I_schedule_no
         and (frd.requisition_no = I_requisition_no or
             (frd.requisition_no in (select tsf.tsf_no
                                       from tsfhead tsf
                                      where tsf.tsf_parent_no = I_requisition_no)))
         and frh.requisition_type in ('REP','TSF','IC')
         and frd.requisition_no != fdd.requisition_no
         and frd.item = I_item
       order by 1 asc;

BEGIN

   open C_GET_NF;

   LOOP
     fetch C_GET_NF  into L_nf_tab(L_index);
     EXIT when C_GET_NF%NOTFOUND;
     L_index := L_index + 1;
   END LOOP;

   if L_nf_tab.FIRST is NULL then
      L_count := 1;
      O_nf(L_count) := NULL;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_SUB_DOC',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      FOR L_index in L_nf_tab.first..L_nf_tab.last LOOP
         L_count := L_count + 1;
         O_nf(L_count) := L_nf_tab(L_index);
      END LOOP;
   end if;

   close C_GET_NF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_RECEIPT;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_OVERAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_over_nf         IN OUT   NF_OVER_TBL,
                         I_schedule_no     IN       FM_RECEIVING_HEADER.RECV_NO%TYPE,
                         I_item            IN       FM_RECEIVING_DETAIL.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100)  := 'FM_SUB_RECEIPT_SQL.PROCESS_OVERAGE';

   L_over_nf   NF_OVER_TBL;
   L_index     NUMBER  :=1;
   L_count     INTEGER :=0;

   cursor C_GET_OVERAGE_NF is
      select frh.fiscal_doc_id,
             frd.requisition_no,
             frd.item,
             fdd.quantity nf_qty,
             SUM(NVL(frd.quantity,0)) recv_qty,
             SUM(NVL(frd.overage_qty,0)) overage_qty
        from fm_receiving_header frh,
             fm_receiving_detail frd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd,
             item_master itm
       where frh.seq_no = frd.header_seq_no
         and fdh.schedule_no = frh.recv_no
         and fdh.fiscal_doc_id = fdd.fiscal_doc_id
         and fdh.status = 'S'
         and frd.item = itm.item
         and frd.item = fdd.item
         and NVL(frd.requisition_no,0) = NVL(fdd.requisition_no,0)
         and fdh.fiscal_doc_id = frh.fiscal_doc_id
         and frh.recv_no= I_schedule_no
         and frd.item = I_item
       group by frh.fiscal_doc_id, frd.requisition_no, frd.item, fdd.quantity
       union all
      select frh.fiscal_doc_id,
             frd.requisition_no,
             frd.item,
             fdd1.qty,
             SUM(NVL(frd.quantity,0)) recv_qty,
             SUM(NVL(frd.overage_qty,0)) overage_qty
        from fm_receiving_header frh,
             fm_receiving_detail frd,
             fm_fiscal_doc_header fdh,
             (select DISTINCT fdd.fiscal_doc_id fiscal_doc_id,
                     fdd.pack_no pack_no,
                     (fdd.quantity/pi.pack_item_qty) qty,
                     fdd.requisition_no requisition_no
                from fm_fiscal_doc_detail fdd, 
                     packitem_breakout pi
               where fdd.item = pi.item
                 and fdd.pack_no = pi.pack_no) fdd1
        where frh.seq_no = frd.header_seq_no
          and fdh.schedule_no = frh.recv_no
          and fdh.status = 'S'
          and fdh.fiscal_doc_id = fdd1.fiscal_doc_id
          and fdh.fiscal_doc_id = frh.fiscal_doc_id
          and frd.item = fdd1.pack_no
          and NVL(frd.requisition_no,0) = NVL(fdd1.requisition_no,0)
          and frh.recv_no= I_schedule_no
          and frd.item = fdd1.pack_no
          and frd.item = I_item
        group by frh.fiscal_doc_id, frd.requisition_no, frd.item, fdd1.qty
        order by 1 asc;

BEGIN

   open C_GET_OVERAGE_NF;

   LOOP
     fetch C_GET_OVERAGE_NF into L_over_nf(L_index);
     EXIT when C_GET_OVERAGE_NF%NOTFOUND;
     L_index := L_index + 1;
   END LOOP;

   if L_over_nf.first is NULL then
      L_count := 1;
      O_over_nf(L_count) := NULL;
   else
      FOR L_index in L_over_nf.first..L_over_nf.last LOOP
         L_count := L_count + 1;
         O_over_nf(L_count) := L_over_nf(L_index);
      END LOOP;
   end if;

   close C_GET_OVERAGE_NF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_OVERAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RECEIPT(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_rib_receiptdesc_rec   IN       "RIB_ReceiptDesc_REC")

return BOOLEAN IS

   L_program        VARCHAR2(61)    := 'FM_SUB_RECEIPT_SQL.VALIDATE_RECEIPT';

   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   L_head_index     BINARY_INTEGER  := 0;
   L_detail_index   BINARY_INTEGER  := 0;

   L_item_exists    BOOLEAN;
   L_order_exists   VARCHAR2(1);
   L_exists         VARCHAR2(1);
   L_rowid          ROWID := NULL;

   L_physical_wh    WH.PHYSICAL_WH%TYPE;
   L_to_loc         TSFHEAD.TO_LOC%TYPE;

   L_tsf_type       TSFHEAD.TSF_TYPE%TYPE;
   L_shipment       SHIPMENT.SHIPMENT%TYPE;
   L_loc_type       ITEM_LOC.LOC_TYPE%TYPE;

   L_OverageHead_index     BINARY_INTEGER  := 0;
   L_OverageDetail_index   BINARY_INTEGER  := 0;

   L_uom_class      UOM_CLASS.UOM_CLASS%TYPE;
   L_to_loc_type    TSFHEAD.TO_LOC_TYPE%TYPE;
   L_loc_currency   ORDHEAD.CURRENCY_CODE%TYPE;

   L_sellable_ind    ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind   ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_inventory_ind   ITEM_MASTER.INVENTORY_IND%TYPE;

   L_from_inv_status    INV_ADJ.INV_STATUS%TYPE;
   L_to_inv_status      INV_ADJ.INV_STATUS%TYPE;

   L_rib_receiptdesc_rec    "RIB_ReceiptDesc_REC" := I_rib_receiptdesc_rec;

   L_batch_running_ind    RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE;
   L_stg_exists           VARCHAR2(1);

   cursor C_CONTENTS_ITEMS(P_item ITEM_MASTER.ITEM%TYPE) is
      select 'Y'
        from item_master
       where container_item = P_item;

   cursor C_ITEM_EXIST(P_item ITEM_MASTER.ITEM%TYPE) is
      select im1.rowid,
             im1.sellable_ind,
             im1.orderable_ind,
             im1.inventory_ind
        from item_master im1,
             item_master im2
      where (im2.item = P_item and
             im2.item_level = im2.tran_level and im1.item = im2.item)
         or (im2.item = P_item and
             im2.item_level = im2.tran_level + 1 and
             im1.item = im2.item_parent);

   cursor C_ORD_EXIST(P_order_no ORDHEAD.ORDER_NO%TYPE) is
      select 'x'
        from ordhead oh
       where oh.order_no = P_order_no;

   -- Carton level receipts will not validate the distro number
   cursor C_SHIPMENT(P_bol_no SHIPMENT.BOL_NO%TYPE, P_to_loc FM_SCHEDULE.LOCATION_ID%TYPE) is
      select shipment
        from shipment
       where bol_no = P_bol_no
         and to_loc = P_to_loc;

   cursor C_CHECK_TSF(P_po_nbr TSFHEAD.TSF_NO%TYPE) is
      select 'Y',
             th.to_loc,
             th.to_loc_type,
             th.tsf_type
        from tsfhead th
       where th.tsf_no = P_po_nbr;

   cursor C_CHECK_ALLOC(P_po_nbr TSFHEAD.TSF_NO%TYPE) is
      select 'Y'
        from alloc_header ah
       where ah.alloc_no = P_po_nbr;

   cursor C_STG_EXISTS(P_bol_no SHIPMENT.BOL_NO%TYPE, P_to_loc FM_SCHEDULE.LOCATION_ID%TYPE) is
      select 'Y'
        from fm_stg_asnout_desc
       where bol_nbr = P_bol_no
         and to_location = P_to_loc;

BEGIN

   L_head_index := L_rib_receiptdesc_rec.receipt_tbl.FIRST;

   WHILE L_head_index is NOT NULL LOOP

      L_detail_index := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.FIRST;

      WHILE L_detail_index is NOT NULL LOOP

         if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_NULL_DOC_TYPE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type = 'P' then

            -- NULL these values which will be used to check for existence later
            L_rowid        := NULL;
            L_order_exists := NULL;

            if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Location',
                                                     'NULL',
                                                     'NOT_NULL ');
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Order_no',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).item_id is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Item',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).unit_qty is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Quantity',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_xactn_type is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Receipt_xactn_type',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_date is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Transaction Date',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight is NULL and
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight_uom is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('WGT_WGTUOM_REQUIRED',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight_uom is NULL and
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('WGT_WGTUOM_REQUIRED',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_disposition is NULL and
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_disposition is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INV_DISPOSITION',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_disposition is NOT NULL then
               if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                            L_to_inv_status,
                                            L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_disposition) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_disposition is NOT NULL then
               if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                            L_from_inv_status,
                                            L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_disposition) = FALSE then
                  return FALSE;
               end if;
            end if;

            L_exists := 'N';

            open C_CONTENTS_ITEMS(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).item_id);

            fetch C_CONTENTS_ITEMS into L_exists;

            close C_CONTENTS_ITEMS;

            if L_exists = 'Y' then
               O_error_message := SQL_LIB.CREATE_MSG('SA_CANT_USE_DEPOSIT_CONTR',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                            L_loc_type,
                                            L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id) = FALSE then
               return FALSE;
            end if;

            if L_loc_type = 'W' then
               if L_rib_receiptdesc_rec.schedule_nbr is NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                        'Schedule Number',
                                                        'NULL',
                                                        'NOT NULL');
                  return FALSE;
               end if;
            elsif L_loc_type = 'S' then
               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr is NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                        'ASN Number',
                                                        'NULL',
                                                        'NOT NULL');
                  return FALSE;
               end if;
            end if;

            if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                         L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id,
                                         L_loc_type,
                                         NULL,
                                         L_loc_currency) = FALSE then
               return FALSE;
            end if;

            if (L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_xactn_type NOT in ('R', 'A')) then
               O_error_message := SQL_LIB.CREATE_MSG('INV_TRAN_TYPE',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            if (L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_xactn_type = 'R') and
               (L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).unit_qty < 0) then
               O_error_message := SQL_LIB.CREATE_MSG('PO_RECEIPT_GT_ZERO',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight_uom is NOT NULL then
               if UOM_SQL.GET_CLASS(O_error_message,
                                    L_uom_class,
                                    UPPER(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight_uom)) = FALSE then
                  return FALSE;
               end if;

               if L_uom_class != 'MASS' then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_WGTUOM_CLASS',
                                                        L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight_uom,
                                                        L_uom_class,
                                                        NULL);
                  return FALSE;
               end if;
            end if;

            --- Get item master info for transaction level item which may be
            --- different than the input_item.
            open C_ITEM_EXIST(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).item_id);
            fetch C_ITEM_EXIST into L_rowid,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_inventory_ind;
            close C_ITEM_EXIST;
               ---
            if L_rowid is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;
            ---
            if L_orderable_ind = 'Y' and L_sellable_ind = 'N' and
               L_inventory_ind = 'N' then
               O_error_message := SQL_LIB.CREATE_MSG('NO_NONINVENT_ITEM',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            --- Check if the Order Exists
            open C_ORD_EXIST(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr);
            fetch C_ORD_EXIST into L_order_exists;
            close C_ORD_EXIST;

            if L_order_exists is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

         elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type in ('T','A','D','V') then

            if ( L_rib_receiptdesc_rec.receipt_tbl(L_head_index).receipt_type is NULL ) then
               L_rib_receiptdesc_rec.receipt_tbl(L_head_index).receipt_type := 'SK';
            end if;

            if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_NULL_LOC',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_NULL_BOL',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).receipt_type is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_NULL_RECEIPT_TYPE',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            if  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).receipt_type = 'BL' then

               open  C_SHIPMENT(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr, L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id);

               fetch C_SHIPMENT into L_shipment;

               close C_SHIPMENT;

               -- If L_shipment is NULL then no record was found for this BOL/to_loc
               if L_shipment is NULL then
                  if RMS_BATCH_STATUS_SQL.GET_BATCH_RUNNING_IND(O_error_message,
                                                                L_batch_running_ind) = FALSE then
                     return FALSE;
                  end if;

                  if L_batch_running_ind = 'Y' then
                     open  C_STG_EXISTS(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr, L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id);

                     fetch C_STG_EXISTS into L_stg_exists;

                     close C_STG_EXISTS;
                  end if;

                  -- In a 24x7 scenario, for shipping/receiving completed in the same batch cycle, SHIPMENT/SHIPSKU records won't be created yet.
                  -- In this case, there would only be records in the shipment staging table (fm_stg_asnout_desc)
                  -- When batch_running_ind is Y, only error out if the shipment record is not in the staging table.
                  -- When batch_running_ind is N, error out.
                  if (L_batch_running_ind = 'Y' and L_stg_exists is NULL) or
                     L_batch_running_ind = 'N' then
                     O_error_message := SQL_LIB.CREATE_MSG('INV_BOL_LOC',
                                                           L_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr,
                                                           L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id,
                                                           NULL);
                     return FALSE;
                  end if;
               end if;

               -- Item level records must NOT exist
               -- (carton records may or may not exist)
               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl IS NOT NULL and
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.COUNT > 0 then
                  O_error_message := SQL_LIB.CREATE_MSG('CTN_RCPT_HAS_ITEMS',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;

            -- SK = item level receipt
            elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).receipt_type = 'SK' then

               -- Item level records must exist
               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl IS NULL or
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.COUNT < 1 then
                  O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_ITEM_NO_DETAILS',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;

               -- Carton level records must NOT exist
               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptCartonDtl_tbl IS NOT NULL and
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptCartonDtl_tbl.COUNT > 0 then
                  O_error_message := SQL_LIB.CREATE_MSG('ITEM_RCPT_HAS_CARTONS',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;

               -- Validate transfer if it is not null
               -- (it may be null for items in dummy cartons)
               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr is NOT NULL and
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type in ('T','D') then

                  L_exists := 'N';

                  open  C_CHECK_TSF(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr);
                  fetch C_CHECK_TSF into L_exists,
                                         L_to_loc,
                                         L_to_loc_type,
                                         L_tsf_type;
                  close C_CHECK_TSF;
                  ---
                  if L_exists = 'N' then
                     O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO',
                                                           L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr,
                                                           NULL,
                                                           NULL);
                     return FALSE;
                  end if;

                     -- Validate transfer/location combination.
                     -- Do not validate stores since this might be a wrong_store_receipt
                     -- or walk_through_store, which are handled in stock_order_rcv_sql.
                  if L_to_loc_type = 'W' and  L_tsf_type != 'EG' then
                     if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                                      L_physical_wh,
                                                      L_to_loc) = FALSE then
                        return FALSE;
                     end if;

                     if L_physical_wh != L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id then
                        O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_LOC',
                                                              L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr,
                                                              L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id,
                                                              NULL);
                        return FALSE;
                     end if;

                  elsif L_to_loc_type = 'E' and L_to_loc != L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id then
                     O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_LOC',
                                                           L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr,
                                                           L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id,
                                                           NULL);
                     return FALSE;
                  end if;

               -- Validate allocation if it is not null.
               -- (it may be null for items in dummy cartons)
               elsif L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr is NOT NULL and
                     L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type = 'A' then

                  -- Check if alloc is valid.  Since allocations are always
                  -- to stores, do not validate the location since this might be a
                  -- wrong_store_receipt or walk_through_store, which are handled
                  -- in stock_order_rcv_sql.
                  L_exists := 'N';

                  open  C_CHECK_ALLOC(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr);
                  fetch C_CHECK_ALLOC into L_exists;
                  close C_CHECK_ALLOC;
                  ---

                  if L_exists = 'N' then
                     O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_LOC',
                                                           L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr,
                                                           L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id,
                                                           NULL);
                     return FALSE;
                  end if;
               end if;

               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_disposition is NULL and
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_disposition is NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_DISPOSITION',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;

               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_disposition is NOT NULL then
                  if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                               L_to_inv_status,
                                               L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_disposition) = FALSE then
                     return FALSE;
                  end if;
               end if;
               ---
               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_disposition is not NULL then
                  if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                               L_from_inv_status,
                                               L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_disposition) = FALSE then
                     return FALSE;
                  end if;
               end if;
               ---
               if ITEM_VALIDATE_SQL.EXIST(O_error_message,
                                          L_item_exists,
                                          L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).item_id) = FALSE then
                  return FALSE;
               end if;

               if L_item_exists = FALSE then
                  O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
                                                        L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).item_id,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;
            end if;
         end if;

         L_detail_index := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.NEXT(L_detail_index);
      END LOOP;
      ---
      L_head_index := L_rib_receiptdesc_rec.receipt_tbl.NEXT(L_head_index);
   END LOOP;
   ---
   if L_rib_receiptdesc_rec.ReceiptOverage_TBL.exists(0) then
      NULL;
   elsif L_rib_receiptdesc_rec.ReceiptOverage_TBL.exists(1) then
      L_OverageHead_index := L_rib_receiptdesc_rec.ReceiptOverage_TBL.FIRST;

      WHILE L_OverageHead_index is NOT NULL LOOP
         if L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL IS NULL or
            L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL.COUNT < 1 then
            O_error_message := SQL_LIB.CREATE_MSG('ORFM_OVERAGE_NO_DETAIL',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         L_OverageDetail_index := L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL.FIRST;

         WHILE L_OverageDetail_index is NOT NULL LOOP
            if L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Overage Item',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            elsif L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).qty_received is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Overage\damaged Qty',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            elsif L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'Reason Code',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            elsif L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code NOT in (110,151) then
               O_error_message := SQL_LIB.CREATE_MSG('INV_REASON_CODE',
                                                     L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code,
                                                     '110 or 151',
                                                     NULL);
               return FALSE;
            elsif L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).qty_received < 0 then
               O_error_message := SQL_LIB.CREATE_MSG('ORFM_RECEIPT_GT_ZERO',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;
            if ITEM_VALIDATE_SQL.EXIST(O_error_message,
                                       L_item_exists,
                                       L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id) = FALSE then
               return FALSE;
            end if;
            if L_item_exists = FALSE then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
                                                     L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            L_OverageDetail_index := L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL.NEXT(L_OverageDetail_index);
         END LOOP;

         L_OverageHead_index := L_rib_receiptdesc_rec.ReceiptOverage_TBL.NEXT(L_OverageHead_index);
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_CONTENTS_ITEMS%ISOPEN then
         close C_CONTENTS_ITEMS;
      end if;
      if C_ITEM_EXIST%ISOPEN then
         close C_ITEM_EXIST;
      end if;
      if C_ORD_EXIST%ISOPEN then
         close C_ORD_EXIST;
      end if;
      if C_SHIPMENT%ISOPEN then
         close C_SHIPMENT;
      end if;
      if C_CHECK_TSF%ISOPEN then
         close C_CHECK_TSF;
      end if;
      if C_CHECK_ALLOC%ISOPEN then
         close C_CHECK_ALLOC;
      end if;

      return FALSE;
END VALIDATE_RECEIPT;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
-- Called by FM_INTEGRATION.CONSUME_RECEIPT when the message type corresponds to a Receipt
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_schedule_no     IN OUT   FM_SCHEDULE.SCHEDULE_NO%TYPE,
                 I_message         IN       RIB_OBJECT,
                 I_message_type    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61)    := 'FM_SUB_RECEIPT_SQL.CONSUME';

   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   L_head_index            BINARY_INTEGER        := 0;
   L_detail_index          BINARY_INTEGER        := 0;
   L_OverageHead_index     BINARY_INTEGER        := 0;
   L_OverageDetail_index   BINARY_INTEGER        := 0;
   L_rib_receiptdesc_rec   "RIB_ReceiptDesc_REC" := NULL;

   L_nf_tbl        NF_TBL;
   L_nf_over_tbl   NF_OVER_TBL;

   L_index1       BINARY_INTEGER;
   L_index3       BINARY_INTEGER;
   L_last_index   BINARY_INTEGER;

   L_flag              NUMBER := 0;
   L_upd_idx           BINARY_INTEGER;
   L_upd_idx2          BINARY_INTEGER;
   L_schedule          FM_RECEIVING_HEADER.RECV_NO%TYPE :=0;
   L_upd_qty           FM_RECEIVING_DETAIL.QUANTITY%TYPE := 0;
   L_recv_qty          FM_RECEIVING_DETAIL.QUANTITY%TYPE := 0;
   L_overage_qty       FM_RECEIVING_DETAIL.OVERAGE_QTY%TYPE := 0;
   L_damaged_qty       FM_RECEIVING_DETAIL.OVERAGE_QTY%TYPE :=0;
   L_insert_qty        FM_RECEIVING_DETAIL.OVERAGE_QTY%TYPE :=0;
   L_dmg_insert_qty    FM_RECEIVING_DETAIL.OVERAGE_QTY%TYPE :=0;
   L_over_insert_qty   FM_RECEIVING_DETAIL.OVERAGE_QTY%TYPE :=0;
   L_hold              FM_RECEIVING_HEADER.STATUS%TYPE := 'H';

   L_count        NUMBER := 0;
   L_nf_count     NUMBER := 0;
   L_over_count   NUMBER := 0;

   L_overage_reason  FM_RECEIVING_DETAIL.REASON_CODE%TYPE := 151;
   L_damaged_reason  FM_RECEIVING_DETAIL.REASON_CODE%TYPE := 110;

   L_fiscal_doc_id   FM_RECEIVING_HEADER.FISCAL_DOC_ID%TYPE;

   cursor C_GET_UNRCVD_NF(P_schedule FM_RECEIVING_HEADER.RECV_NO%TYPE) is
      select count(1)
         from fm_receiving_header frh , fm_receiving_detail frd
        where frd.header_seq_no = frh.seq_no
          and frh.recv_no = P_schedule
          and frd.quantity is NULL
          and frd.reason_code is NULL
          and frd.requisition_no is NOT NULL;

   cursor C_LATEST_NF(P_recv_no FM_RECEIVING_HEADER.RECV_NO%TYPE) is
      select max(frh.fiscal_doc_id)
        from fm_receiving_header frh
       where frh.recv_no = P_recv_no
         and frh.requisition_type = 'PO';

   cursor C_FIND_NF(P_reason_code FM_RECEIVING_DETAIL.REASON_CODE%TYPE, P_fiscal_doc_id FM_RECEIVING_HEADER.FISCAL_DOC_ID%TYPE, P_recv_no FM_RECEIVING_HEADER.RECV_NO%TYPE, P_requisition_no FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE, P_item FM_RECEIVING_DETAIL.ITEM%TYPE) is
      select count(1)
        from fm_receiving_detail frd, fm_receiving_header frh
       where frd.header_seq_no = frh.seq_no
         and frd.reason_code = P_reason_code
         and frh.fiscal_doc_id = P_fiscal_doc_id
         and frh.recv_no = P_recv_no
         and NVL(frd.requisition_no,0) = NVL(P_requisition_no,0)
         and frd.item = P_item;

   cursor C_GET_SCHEDULE(P_asn_no FM_RECEIVING_DETAIL.ASN_NBR%TYPE) is
      select DISTINCT frh.recv_no
        from fm_receiving_header frh, fm_receiving_detail frd
       where frd.header_seq_no= frh.seq_no
         and frd.asn_nbr= P_asn_no;

   cursor C_LOCK_RECV_HEADER(P_recv_no FM_RECEIVING_HEADER.RECV_NO%TYPE) is
      select 'X'
        from fm_receiving_header frh
       where frh.recv_no = P_recv_no
         for update nowait;

   cursor C_LOCK_DOC_HEADER(P_recv_no FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE) is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.schedule_no = P_recv_no
         and fdh.status = 'S'
         for update nowait;

   cursor C_LOCK_SCHEDULE(P_recv_no FM_SCHEDULE.SCHEDULE_NO%TYPE) is
      select 'X'
        from fm_schedule fs
       where fs.schedule_no = P_recv_no
         for update nowait;

   cursor C_LOCK_RECV_UPDATE (P_fiscal_doc_id FM_RECEIVING_HEADER.FISCAL_DOC_ID%TYPE, P_requisition_no FM_RECEIVING_DETAIL.REQUISITION_NO%TYPE, P_item FM_RECEIVING_DETAIL.ITEM%TYPE) is
      select 'X'
        from fm_receiving_detail frd, fm_receiving_header frh
       where frh.seq_no = frd.header_seq_no
         and frh.fiscal_doc_id = P_fiscal_doc_id
         and frd.requisition_no = P_requisition_no
         and frd.item = P_item
         for update nowait;

BEGIN

   L_rib_receiptdesc_rec := treat(I_message as "RIB_ReceiptDesc_REC");

   if FM_SUB_RECEIPT_SQL.VALIDATE_RECEIPT(O_error_message,
                                          L_rib_receiptdesc_rec) = FALSE then
      return FALSE;
   end if;

   L_head_index := L_rib_receiptdesc_rec.receipt_tbl.FIRST;

   WHILE L_head_index is NOT NULL LOOP
      if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type in ('P','A','T','D') then
         L_detail_index := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.FIRST;

         if L_rib_receiptdesc_rec.schedule_nbr is not NULL then
            L_schedule := L_rib_receiptdesc_rec.schedule_nbr;
         else
            open C_GET_SCHEDULE(L_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr);
            fetch C_GET_SCHEDULE into L_schedule;
            --assuming ASN can not be present in different schedules
            close C_GET_SCHEDULE;
         end if;

         if L_rib_receiptdesc_rec.appt_nbr is NOT NULL then
            update fm_receiving_header frh
               set frh.appt_nbr = L_rib_receiptdesc_rec.appt_nbr
             where frh.recv_no = NVL(L_rib_receiptdesc_rec.schedule_nbr, L_schedule);
         end if;

         WHILE L_detail_index is NOT NULL LOOP

            L_nf_tbl.delete;
            if FM_SUB_RECEIPT_SQL.PROCESS_RECEIPT(O_error_message,
                                                  L_nf_tbl,
                                                  L_schedule,
                                                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr,
                                                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).item_id) = FALSE then
               return FALSE;
            end if;

            --- If this is a PO receipt then loop through each detail record and
            --- update qty and status in staging table

            L_flag :=1;

            FOR L_index1 in L_nf_tbl.first .. L_nf_tbl.last LOOP
               if L_flag=1 then
                  L_upd_qty := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).unit_qty;
                  L_flag := L_flag + 1;
               end if;

               if L_nf_tbl(L_index1).appt_qty >= L_upd_qty then
                  L_recv_qty := L_upd_qty;
                  L_upd_qty := 0;
               else
                  L_recv_qty := L_nf_tbl(L_index1).appt_qty;
                  L_upd_qty := L_upd_qty - L_recv_qty;
               end if;
               ---
               open C_LOCK_RECV_UPDATE(L_nf_tbl(L_index1).fiscal_doc_id, L_nf_tbl(L_index1).requisition_no, L_nf_tbl(L_index1).item);
               close C_LOCK_RECV_UPDATE;

               update (select frd.quantity, frd.ordered_qty, frd.requisition_no, frd.item, frh.location_id, frd.rms_upd_status
                         from fm_receiving_detail frd, fm_receiving_header frh
                        where frh.seq_no = frd.header_seq_no
                          and frh.fiscal_doc_id = L_nf_tbl(L_index1).fiscal_doc_id
                          and frd.requisition_no = L_nf_tbl(L_index1).requisition_no
                          and frd.item = L_nf_tbl(L_index1).item) recv_table
                  set recv_table.quantity = NVL(recv_table.quantity, 0) + L_recv_qty,
                      recv_table.rms_upd_status = 'R',
                      recv_table.ordered_qty = ((select sum(nvl(olc.qty_ordered,0)) - sum(nvl(olc.qty_received,0))
                                                  from ordloc olc
                                                 where order_no = recv_table.requisition_no
                                                   and item = recv_table.item
                                                   and DECODE(olc.loc_type,'S', olc.location,'W',(select physical_wh from wh where wh.wh=olc.location)) = recv_table.location_id)
                                                - (select nvl(sum(NVL(frd2.quantity,0)),0)
                                                    from fm_receiving_header frh2,
                                                         fm_receiving_detail frd2
                                                   where frd2.requisition_no = L_nf_tbl(L_index1).requisition_no
                                                     and frd2.header_seq_no = frh2.seq_no
                                                     and frd2.item = L_nf_tbl(L_index1).item
                                                     and frh2.status = L_hold
                                                     and frh2.location_id = recv_table.location_id));

               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type in ('T','A','D','V') and
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).receipt_type is NULL then
                  L_rib_receiptdesc_rec.receipt_tbl(L_head_index).receipt_type := 'SK';
               end if;

               -- general receipt update other than quantity
               update (select frd.asn_nbr,
                              frd.receipt_type,
                              frd.from_loc,
                              frd.from_loc_type,
                              frd.container_id,
                              frd.receipt_xactn_type,
                              frd.receipt_date,
                              frd.receipt_nbr,
                              frd.to_disposition,
                              frd.from_disposition,
                              frd.to_wip,
                              frd.from_wip,
                              frd.to_trouble,
                              frd.from_trouble,
                              frd.dummy_carton_ind,
                              frd.tampered_carton_ind,
                              frd.weight,
                              frd.weight_uom
                         from fm_receiving_detail frd, fm_receiving_header frh
                        where frh.seq_no = frd.header_seq_no
                          and frh.fiscal_doc_id = L_nf_tbl(L_index1).fiscal_doc_id
                          and frd.requisition_no = L_nf_tbl(L_index1).requisition_no
                          and frd.item = L_nf_tbl(L_index1).item) recv_table
                  set recv_table.asn_nbr = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr,
                      recv_table.receipt_type = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).receipt_type,
                      recv_table.from_loc = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).from_loc,
                      recv_table.from_loc_type = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).from_loc_type,
                      recv_table.container_id = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).container_id,
                      recv_table.receipt_xactn_type = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_xactn_type,
                      recv_table.receipt_date = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_date,
                      recv_table.receipt_nbr = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_nbr,
                      recv_table.to_disposition = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_disposition,
                      recv_table.from_disposition = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_disposition,
                      recv_table.to_wip = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_wip,
                      recv_table.from_wip = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_wip,
                      recv_table.to_trouble = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_trouble,
                      recv_table.from_trouble = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_trouble,
                      recv_table.dummy_carton_ind = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).dummy_carton_ind,
                      recv_table.tampered_carton_ind = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).tampered_carton_ind,
                      recv_table.weight = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight,
                      recv_table.weight_uom = L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight_uom;
            END LOOP;

            L_index1 := 0;
            L_detail_index := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.NEXT(L_detail_index);
         END LOOP;

      --- If not PO/TSF then reject with error.
      else
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'document_type',
                                                L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type,
                                               'P or T or D or A');
         return FALSE;
      end if;

      L_head_index := L_rib_receiptdesc_rec.receipt_tbl.NEXT(L_head_index);

   END LOOP;

   if L_rib_receiptdesc_rec.ReceiptOverage_TBL.exists(0) then
      NULL;
   elsif L_rib_receiptdesc_rec.ReceiptOverage_TBL.exists(1) then
      L_OverageHead_index := L_rib_receiptdesc_rec.ReceiptOverage_TBL.FIRST;

      WHILE L_OverageHead_index is NOT NULL LOOP

         FOR L_counter in 1..2 LOOP

            L_OverageDetail_index := L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL.FIRST;
            WHILE L_OverageDetail_index is NOT NULL LOOP
               L_nf_over_tbl.delete;

               if FM_SUB_RECEIPT_SQL.PROCESS_OVERAGE(O_error_message,
                                                     L_nf_over_tbl,
                                                     L_schedule,
                                                     L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id) = FALSE then
                  return FALSE;
               end if;
               --- insert damaged/overage qty in staging table
               L_flag := 1;

               if L_nf_over_tbl(L_nf_over_tbl.FIRST).item is NOT NULL then
                  FOR L_index3 in L_nf_over_tbl.first .. L_nf_over_tbl.last LOOP
                     if L_nf_over_tbl(L_index3).requisition_no is NOT NULL then
                        if L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code = L_damaged_reason then
                           if L_counter = 1 then
                              if L_flag = 1 then
                                 L_damaged_qty := L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).qty_received;
                                 L_flag := L_flag + 1;
                              end if;

                              if (L_nf_over_tbl(L_index3).nf_qty - L_nf_over_tbl(L_index3).qty_received ) >= L_damaged_qty then
                                 L_dmg_insert_qty := L_damaged_qty;
                                 L_damaged_qty := 0;
                              else
                                 if (L_nf_over_tbl(L_index3).nf_qty - L_nf_over_tbl(L_index3).qty_received ) != 0 then
                                    L_dmg_insert_qty := ( L_nf_over_tbl(L_index3).nf_qty - L_nf_over_tbl(L_index3).qty_received );
                                    L_damaged_qty := L_damaged_qty - L_dmg_insert_qty;
                                 end if;
                              end if;

                              if ((L_nf_over_tbl(L_index3).nf_qty - L_nf_over_tbl(L_index3).qty_received) != 0) and L_dmg_insert_qty >= 0 then
                                 insert into fm_receiving_detail(seq_no,
                                                                 header_seq_no,
                                                                 item,
                                                                 ebc_cost,
                                                                 quantity,
                                                                 nic_cost,
                                                                 container_id,
                                                                 create_datetime,
                                                                 create_id,
                                                                 last_update_datetime,
                                                                 last_update_id,
                                                                 requisition_no,
                                                                 asn_nbr,
                                                                 overage_qty,
                                                                 reason_code)
                                                          select fm_recv_detail_seq.nextval,
                                                                 header_seq_no,
                                                                 item,
                                                                 ebc_cost,
                                                                 NULL,
                                                                 nic_cost,
                                                                 container_id,
                                                                 SYSDATE,
                                                                 USER,
                                                                 SYSDATE,
                                                                 USER,
                                                                 requisition_no,
                                                                 asn_nbr,
                                                                 L_dmg_insert_qty,
                                                                 L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code
                                                            from fm_receiving_detail frd
                                                           where frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id
                                                             and frd.requisition_no = L_nf_over_tbl(L_index3).requisition_no
                                                             and frd.reason_code is NULL
                                                             and frd.header_seq_no = (select seq_no
                                                                                        from fm_receiving_header
                                                                                       where fiscal_doc_id = L_nf_over_tbl(L_index3).fiscal_doc_id
                                                                                         and recv_no = L_schedule);

                                 L_dmg_insert_qty := 0;
                              end if;

                              if L_index3 = L_nf_over_tbl.LAST then
                                 if L_damaged_qty > 0 then
                                    L_upd_idx := L_nf_over_tbl.FIRST;

                                    open C_FIND_NF(L_damaged_reason,
                                                   L_nf_over_tbl(L_upd_idx).fiscal_doc_id,
                                                   L_schedule,
                                                   L_nf_over_tbl(L_upd_idx).requisition_no,
                                                   L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id);
                                    fetch C_FIND_NF into L_nf_count;
                                    close C_FIND_NF;

                                    if L_nf_count > 0 then
                                       update (select frd.overage_qty
                                                 from fm_receiving_detail frd, fm_receiving_header frh
                                                where frh.seq_no = frd.header_seq_no
                                                  and frh.fiscal_doc_id = L_nf_over_tbl(L_upd_idx).fiscal_doc_id
                                                  and frh.recv_no = L_schedule
                                                  and frd.reason_code = L_damaged_reason
                                                  and frd.requisition_no = L_nf_over_tbl(L_upd_idx).requisition_no
                                                  and frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id) recv_table
                                          set recv_table.overage_qty = recv_table.overage_qty + L_damaged_qty;
                                    else
                                       insert into fm_receiving_detail(seq_no,
                                                                       header_seq_no,
                                                                       item,
                                                                       ebc_cost,
                                                                       quantity,
                                                                       nic_cost,
                                                                       container_id,
                                                                       create_datetime,
                                                                       create_id,
                                                                       last_update_datetime,
                                                                       last_update_id,
                                                                       requisition_no,
                                                                       asn_nbr,
                                                                       overage_qty,
                                                                       reason_code)
                                                                select fm_recv_detail_seq.nextval,
                                                                       header_seq_no,
                                                                       item,
                                                                       ebc_cost,
                                                                       NULL,
                                                                       nic_cost,
                                                                       container_id,
                                                                       SYSDATE,
                                                                       USER,
                                                                       SYSDATE,
                                                                       USER,
                                                                       requisition_no,
                                                                       asn_nbr,
                                                                       L_damaged_qty,
                                                                       L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code
                                                                  from fm_receiving_detail frd
                                                                 where frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id
                                                                   and frd.requisition_no = L_nf_over_tbl(L_upd_idx).requisition_no
                                                                   and frd.reason_code is NULL
                                                                   and frd.header_seq_no = (select seq_no
                                                                                              from fm_receiving_header
                                                                                             where fiscal_doc_id = L_nf_over_tbl(L_upd_idx).fiscal_doc_id
                                                                                               and recv_no = L_schedule);
                                    end if; -- L_nf_count > 0
                                 end if;   -- L_damaged_qty > 0
                              end if; -- L_index3 = L_nf_over_tbl.LAST
                           end if;  -- L_counter = 1
                        elsif L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code = L_overage_reason and L_counter != 1 then
                           if L_flag=1 then
                              L_overage_qty := L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).qty_received;
                              L_flag := L_flag + 1;
                           end if;

                           if (L_nf_over_tbl(L_index3).nf_qty - L_nf_over_tbl(L_index3).qty_received - NVL(L_nf_over_tbl(L_index3).overage_qty,0)) >= L_overage_qty then
                              L_over_insert_qty := L_overage_qty;
                              L_overage_qty := 0;
                           else
                              if (L_nf_over_tbl(L_index3).nf_qty - L_nf_over_tbl(L_index3).qty_received - NVL(L_nf_over_tbl(L_index3).overage_qty,0)) > 0 then
                                 L_over_insert_qty := ( L_nf_over_tbl(L_index3).nf_qty - L_nf_over_tbl(L_index3).qty_received - NVL(L_nf_over_tbl(L_index3).overage_qty,0));
                                 L_overage_qty := L_overage_qty - L_over_insert_qty;
                              end if;
                           end if;

                           if ((L_nf_over_tbl(L_index3).nf_qty - L_nf_over_tbl(L_index3).qty_received - NVL(L_nf_over_tbl(L_index3).overage_qty,0)) > 0) and L_over_insert_qty >= 0 then
                              insert into fm_receiving_detail(seq_no,
                                                              header_seq_no,
                                                              item,
                                                              ebc_cost,
                                                              quantity,
                                                              nic_cost,
                                                              container_id,
                                                              create_datetime,
                                                              create_id,
                                                              last_update_datetime,
                                                              last_update_id,
                                                              requisition_no,
                                                              asn_nbr,
                                                              overage_qty,
                                                              reason_code)
                                                       select fm_recv_detail_seq.nextval,
                                                              header_seq_no,
                                                              item,
                                                              ebc_cost,
                                                              NULL,
                                                              nic_cost,
                                                              container_id,
                                                              SYSDATE,
                                                              USER,
                                                              SYSDATE,
                                                              USER,
                                                              requisition_no,
                                                              asn_nbr,
                                                              L_over_insert_qty,
                                                              L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code
                                                         from fm_receiving_detail frd
                                                        where frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id
                                                          and frd.requisition_no = L_nf_over_tbl(L_index3).requisition_no
                                                          and frd.reason_code is NULL
                                                          and frd.header_seq_no = (select seq_no
                                                                                     from fm_receiving_header
                                                                                    where fiscal_doc_id = L_nf_over_tbl(L_index3).fiscal_doc_id
                                                                                      and recv_no = L_schedule);

                              L_over_insert_qty :=0;
                           end if;

                           if L_index3 = L_nf_over_tbl.LAST then
                              if L_overage_qty > 0 then
                                 L_upd_idx2 := L_nf_over_tbl.FIRST;

                                 open C_FIND_NF(L_overage_reason,
                                                L_nf_over_tbl(L_upd_idx2).fiscal_doc_id,
                                                L_schedule,
                                                L_nf_over_tbl(L_upd_idx2).requisition_no,
                                                L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id);
                                 fetch C_FIND_NF into L_over_count;
                                 close C_FIND_NF;

                                 if L_over_count > 0 then
                                    update (select frd.overage_qty
                                              from fm_receiving_detail frd, fm_receiving_header frh
                                             where frh.seq_no = frd.header_seq_no
                                               and frh.fiscal_doc_id = L_nf_over_tbl(L_upd_idx2).fiscal_doc_id
                                               and frh.recv_no = L_schedule
                                               and frd.reason_code = L_overage_reason
                                               and frd.requisition_no = L_nf_over_tbl(L_upd_idx2).requisition_no
                                               and frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id) recv_table
                                       set recv_table.overage_qty = recv_table.overage_qty + L_overage_qty;
                                 else
                                    insert into fm_receiving_detail(seq_no,
                                                                    header_seq_no,
                                                                    item,
                                                                    ebc_cost,
                                                                    quantity,
                                                                    nic_cost,
                                                                    container_id,
                                                                    create_datetime,
                                                                    create_id,
                                                                    last_update_datetime,
                                                                    last_update_id,
                                                                    requisition_no,
                                                                    asn_nbr,
                                                                    overage_qty,
                                                                    reason_code)
                                                             select fm_recv_detail_seq.nextval,
                                                                    header_seq_no,
                                                                    item,
                                                                    ebc_cost,
                                                                    NULL,
                                                                    nic_cost,
                                                                    container_id,
                                                                    SYSDATE,
                                                                    USER,
                                                                    SYSDATE,
                                                                    USER,
                                                                    requisition_no,
                                                                    asn_nbr,
                                                                    L_overage_qty,
                                                                    L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code
                                                               from fm_receiving_detail frd
                                                              where frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id
                                                                and frd.requisition_no = L_nf_over_tbl(L_upd_idx2).requisition_no
                                                                and frd.reason_code is NULL
                                                                and frd.header_seq_no = (select seq_no
                                                                                           from fm_receiving_header
                                                                                          where fiscal_doc_id = L_nf_over_tbl(L_upd_idx2).fiscal_doc_id
                                                                                            and recv_no = L_schedule);
                                 end if;  -- L_over_count > 0
                              end if;  -- L_overage_qty > 0
                           end if;  -- L_index3 = L_nf_over_tbl.LAST
                        end if;
                     elsif L_nf_over_tbl(L_index3).requisition_no is NULL and L_nf_over_tbl(L_index3).fiscal_doc_id is NOT NULL and L_rib_receiptdesc_rec.receipt_tbl(L_rib_receiptdesc_rec.receipt_tbl.LAST).document_type = 'P' and L_counter != 2 then
                        if L_counter =1 then
                           if L_flag = 1 then
                              L_overage_qty := L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).qty_received;
                              L_flag := L_flag + 1;
                           end if;
                        end if;

                        if L_nf_over_tbl(L_index3).nf_qty >= L_overage_qty then
                           L_over_insert_qty := L_overage_qty;
                           L_overage_qty := 0;
                        else
                           L_over_insert_qty := L_nf_over_tbl(L_index3).nf_qty;
                           L_overage_qty := L_overage_qty - L_over_insert_qty;
                        end if;

                        L_fiscal_doc_id := L_nf_over_tbl(L_index3).fiscal_doc_id;

                        if ((L_nf_over_tbl(L_index3).nf_qty - NVL(L_nf_over_tbl(L_index3).overage_qty,0)) > 0) and L_over_insert_qty >= 0 then
                           update (select frd.overage_qty, frd.reason_code, frd.quantity
                                     from fm_receiving_detail frd, fm_receiving_header frh
                                    where frh.seq_no = frd.header_seq_no
                                      and frh.fiscal_doc_id = L_fiscal_doc_id
                                      and frh.recv_no = L_schedule
                                      and frd.requisition_no is NULL
                                      and frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id) recv_table
                              set recv_table.overage_qty = nvl(recv_table.overage_qty,0) + L_over_insert_qty,
                                  recv_table.reason_code = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code,
                                  recv_table.quantity = NULL;
                           L_over_insert_qty :=0;
                        end if;

                        if L_index3 = L_nf_over_tbl.LAST then
                           if L_overage_qty > 0 then
                              L_upd_idx2 := L_nf_over_tbl.FIRST;

                              open C_FIND_NF(L_overage_reason,
                                             L_nf_over_tbl(L_upd_idx2).fiscal_doc_id,
                                             L_schedule,
                                             L_nf_over_tbl(L_upd_idx2).requisition_no,
                                             L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id);
                              fetch C_FIND_NF into L_over_count;
                              close C_FIND_NF;

                              if L_over_count > 0 then
                                 update (select frd.overage_qty
                                           from fm_receiving_detail frd, fm_receiving_header frh
                                          where frh.seq_no = frd.header_seq_no
                                            and frh.fiscal_doc_id = L_nf_over_tbl(L_upd_idx2).fiscal_doc_id
                                            and frh.recv_no = L_schedule
                                            and frd.reason_code = L_overage_reason
                                            and frd.requisition_no is NULL
                                            and frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id) recv_table
                                    set recv_table.overage_qty = recv_table.overage_qty + L_overage_qty;
                              else
                                 insert into fm_receiving_detail(seq_no,
                                                                 header_seq_no,
                                                                 item,
                                                                 ebc_cost,
                                                                 quantity,
                                                                 nic_cost,
                                                                 container_id,
                                                                 create_datetime,
                                                                 create_id,
                                                                 last_update_datetime,
                                                                 last_update_id,
                                                                 requisition_no,
                                                                 asn_nbr,
                                                                 overage_qty,
                                                                 reason_code)
                                                          select fm_recv_detail_seq.nextval,
                                                                 header_seq_no,
                                                                 item,
                                                                 ebc_cost,
                                                                 NULL,
                                                                 nic_cost,
                                                                 container_id,
                                                                 SYSDATE,
                                                                 USER,
                                                                 SYSDATE,
                                                                 USER,
                                                                 requisition_no,
                                                                 asn_nbr,
                                                                 L_overage_qty,
                                                                 L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code
                                                            from fm_receiving_detail frd
                                                           where frd.item = L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id
                                                             and frd.requisition_no is NULL
                                                             and frd.reason_code is NULL
                                                             and frd.header_seq_no = (select seq_no
                                                                                        from fm_receiving_header
                                                                                       where fiscal_doc_id = L_nf_over_tbl(L_upd_idx2).fiscal_doc_id
                                                                                         and recv_no = L_schedule);
                              end if; -- L_over_count > 0
                           end if; -- L_overage_qty > 0
                        end if;  -- L_index3 = L_nf_over_tbl.LAST
                     end if;
                  END LOOP; -- End loop through L_nf_over_tbl
               else  -- L_nf_over_tbl(L_nf_over_tbl.FIRST).item is NULL
                  if L_rib_receiptdesc_rec.receipt_tbl(L_rib_receiptdesc_rec.receipt_tbl.LAST).document_type = 'P' and L_counter = 2 then
                     open C_LATEST_NF(L_schedule);
                     fetch C_LATEST_NF into L_fiscal_doc_id;
                     close C_LATEST_NF;

                     insert into fm_receiving_detail(seq_no,
                                                     header_seq_no,
                                                     item,
                                                     ebc_cost,
                                                     quantity,
                                                     nic_cost,
                                                     container_id,
                                                     create_datetime,
                                                     create_id,
                                                     last_update_datetime,
                                                     last_update_id,
                                                     requisition_no,
                                                     asn_nbr,
                                                     overage_qty,
                                                     reason_code)
                                             values (fm_recv_detail_seq.nextval,
                                                    (select seq_no
                                                       from fm_receiving_header
                                                      where fiscal_doc_id = L_fiscal_doc_id
                                                        and recv_no = L_schedule),
                                                     L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).item_id,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     SYSDATE,
                                                     USER,
                                                     SYSDATE,
                                                     USER,
                                                    (select max(frd.requisition_no)
                                                       from fm_receiving_header frh, fm_receiving_detail frd
                                                      where frh.seq_no = frd.header_seq_no
                                                        and frh.fiscal_doc_id = L_fiscal_doc_id),
                                                     L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).asn_nbr,
                                                     L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).qty_received,
                                                     L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL(L_OverageDetail_index).reason_code);
                  end if;
               end if;  -- L_nf_over_tbl(L_nf_over_tbl.FIRST).item is NOT NULL

               L_OverageDetail_index := L_rib_receiptdesc_rec.ReceiptOverage_TBL(L_OverageHead_index).ReceiptOverageDtl_TBL.NEXT(L_OverageDetail_index);
            END LOOP;  -- End loop to L_OverageDetail_index
         END LOOP;  -- End loop to L_counter

         L_OverageHead_index := L_rib_receiptdesc_rec.ReceiptOverage_TBL.NEXT(L_OverageHead_index);

      END LOOP;  -- End loopt to L_OverageHead_index
   end if;

---------receiving update----------------------

   update (select frd.quantity
             from fm_receiving_detail frd, fm_receiving_header frh
            where frd.header_seq_no = frh.seq_no
              and frh.recv_no = L_schedule
              and frd.quantity is NULL
              and frh.status = 'S'
              and frh.location_type = 'W') recv_table
      set recv_table.quantity = 0;

   open C_GET_UNRCVD_NF(L_schedule);
   fetch C_GET_UNRCVD_NF into L_count;
   close C_GET_UNRCVD_NF;

   if L_count = 0 then
      ---
      open C_LOCK_RECV_HEADER(L_schedule);
      close C_LOCK_RECV_HEADER;
       ---

      update fm_receiving_header frh
         set frh.status = 'R',
             frh.document_type = L_rib_receiptdesc_rec.receipt_tbl(L_rib_receiptdesc_rec.receipt_tbl.LAST).document_type
       where frh.recv_no = L_schedule
         and frh.status = 'S';
      ---
      open C_LOCK_DOC_HEADER(L_schedule);
      close C_LOCK_DOC_HEADER;
      ---
      update fm_fiscal_doc_header fdh
         set fdh.status = 'R'
       where fdh.schedule_no = L_schedule
         and fdh.status = 'S';
      ---
      open C_LOCK_SCHEDULE(L_schedule);
      close C_LOCK_SCHEDULE;
      ---
      update fm_schedule fs
         set fs.status = 'R'
       where fs.schedule_no = L_schedule;
   end if;

   O_schedule_no := L_schedule;
--------------------------------------------------

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER, FM_RECEIVING_HEADER, FM_SCHEDULE',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      if C_LOCK_RECV_HEADER%ISOPEN then
         close C_LOCK_RECV_HEADER;
      end if;
      ---
      if C_LOCK_SCHEDULE%ISOPEN then
         close C_LOCK_SCHEDULE;
      end if;
      ---
      if C_LOCK_RECV_UPDATE%ISOPEN then
         CLOSE C_LOCK_RECV_UPDATE;
      end if;
      ---
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_LOCK_DOC_HEADER%ISOPEN then
         close C_LOCK_DOC_HEADER;
      end if;
      if C_LOCK_RECV_HEADER%ISOPEN then
         close C_LOCK_RECV_HEADER;
      end if;
      if C_LOCK_SCHEDULE%ISOPEN then
         close C_LOCK_SCHEDULE;
      end if;
      if C_LOCK_RECV_UPDATE%ISOPEN then
         CLOSE C_LOCK_RECV_UPDATE;
      end if;

      return FALSE;
END CONSUME;
-------------------------------------------------------------------------------
END FM_SUB_RECEIPT_SQL;
/