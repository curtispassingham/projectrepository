CREATE OR REPLACE PACKAGE BODY FM_UTILIZATION_ATTRIBUTES_SQL is
-----------------------------------------------------------------------------------
FUNCTION GET_UTILIZATION_ATTRIB_INFO(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_fm_utilization_attributes IN OUT FM_UTILIZATION_ATTRIBUTES%ROWTYPE,
                                     I_utilization_id            IN     FM_UTILIZATION_ATTRIBUTES.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_UTILIZATION_ATTRIBUTES_SQL.GET_UTILIZATION_ATTRIB_INFO';
   L_cursor    VARCHAR2(100) := 'C_FM_UTILIZATION_ATTRIBUTES';
   L_table     VARCHAR2(100) := 'FM_UTILIZATION_ATTRIBUTES';
   L_key       VARCHAR2(100) := 'Utilization_id: ' || I_utilization_id;
   ---
   cursor C_FM_UTILIZATION_ATTRIBUTES is
      select *
        from fm_utilization_attributes fua
       where fua.utilization_id = I_utilization_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_FM_UTILIZATION_ATTRIBUTES;
   ---
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_FM_UTILIZATION_ATTRIBUTES into O_fm_utilization_attributes;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_FM_UTILIZATION_ATTRIBUTES;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_UTILIZATION_ATTRIBUTES%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
         close C_FM_UTILIZATION_ATTRIBUTES;
      end if;
      ---
      return FALSE;
END GET_UTILIZATION_ATTRIB_INFO;
----------------------------------------------------------------------------------
FUNCTION GET_ALLOW_REC_INFO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_allow_rec       IN OUT FM_UTILIZATION_ATTRIBUTES.ALLOW_RECEIVING_IND%TYPE,
                            I_utilization_id  IN     FM_UTILIZATION_ATTRIBUTES.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_UTILIZATION_ATTRIBUTES_SQL.GET_ALLOW_REC_INFO';
   L_cursor    VARCHAR2(100) := 'C_FM_UTILIZATION_ATTRIBUTES';
   L_table     VARCHAR2(100) := 'FM_UTILIZATION_ATTRIBUTES';
   L_key       VARCHAR2(100) := 'Utilization_id: ' || I_utilization_id;
   ---
   cursor C_FM_UTILIZATION_ATTRIBUTES is
      select allow_receiving_ind
        from fm_utilization_attributes fua
       where fua.utilization_id = I_utilization_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_FM_UTILIZATION_ATTRIBUTES;
   ---
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_FM_UTILIZATION_ATTRIBUTES into O_allow_rec;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_FM_UTILIZATION_ATTRIBUTES;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_UTILIZATION_ATTRIBUTES%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
         close C_FM_UTILIZATION_ATTRIBUTES;
      end if;
      ---
      return FALSE;
END GET_ALLOW_REC_INFO;
----------------------------------------------------------------------------------
FUNCTION CHECK_CREATE_UTIL_ATTRIB(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_utilization_id            IN     FM_UTILIZATION_ATTRIBUTES.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_UTILIZATION_ATTRIBUTES_SQL.CHECK_CREATE_UTIL_ATTRIB';
   L_cursor    VARCHAR2(100) := 'C_FM_UTIL_ATTRIB';
   L_table     VARCHAR2(100) := 'FM_UTILIZATION_ATTRIBUTES';
   L_key       VARCHAR2(100) := 'Utilization_id: ' || I_utilization_id;
   ---
   cursor C_FM_UTIL_ATTRIB is
      select count(*)
        from fm_utilization_attributes fua
       where fua.utilization_id = I_utilization_id;
   ---
   L_count number := 0;
   C_IND   CONSTANT fm_utilization_attributes.allow_receiving_ind%TYPE := 'N';
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_FM_UTIL_ATTRIB;
   ---
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
   fetch C_FM_UTIL_ATTRIB into L_count;
   ---
      if (L_count = 0) then
         Insert into fm_utilization_attributes (utilization_id,
                                                comp_nf_ind,
                                                allow_receiving_ind,
                                                icmsst_recovery_ind,
                                                choose_doc_ind,
                                                auto_approve_ind,
                                                comp_freight_nf_ind,
                                                create_datetime,
                                                create_id,
                                                last_update_datetime,
                                                last_update_id) 
                                         values (I_utilization_id,
                                                 c_ind,
                                                 c_ind,
                                                 c_ind,
                                                 c_ind,
                                                 c_ind,
                                                 c_ind,
                                                 sysdate,
                                                 user,
                                                 sysdate,
                                                 user);
      end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_FM_UTIL_ATTRIB;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_FM_UTIL_ATTRIB%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
         close C_FM_UTIL_ATTRIB;
      end if;
      ---
      return FALSE;
END CHECK_CREATE_UTIL_ATTRIB;
----------------------------------------------------------------------------------
END FM_UTILIZATION_ATTRIBUTES_SQL;
/
