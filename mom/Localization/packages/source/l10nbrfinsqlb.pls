CREATE OR REPLACE PACKAGE BODY L10N_BR_FIN_SQL AS
----------------------------------------------------------------------------
FUNCTION CONSUME_RECEIPT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   
   L_program        VARCHAR2(64)     := 'L10N_BR_FIN_SQL.CONSUME_RECEIPT';
   L_l10n_rib_rec   "L10N_RIB_REC"   := L10N_RIB_REC();

BEGIN
   
   L_l10n_rib_rec := treat(IO_l10n_obj as "L10N_RIB_REC");
   if FM_INTEGRATION_SQL.CONSUME_RECEIPT(L_l10n_rib_rec.status_code,
                                         O_error_message,
                                         L_l10n_rib_rec.rib_msg,
                                         L_l10n_rib_rec.rib_msg_type) = FALSE then
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      raise PROGRAM_ERROR;
   end if;
   --
   L_l10n_rib_rec.status_code := API_CODES.SUCCESS;
   IO_l10n_obj := L_l10n_rib_rec;
   
   return TRUE;
EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      return FALSE;
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      return FALSE;
END CONSUME_RECEIPT;
----------------------------------------------------------------------------
FUNCTION CONSUME_RTV(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   
   L_program        VARCHAR2(64)     := 'L10N_BR_FIN_SQL.CONSUME_RTV';
   L_l10n_rib_rec   "L10N_RIB_REC"   := L10N_RIB_REC();

BEGIN
   
   L_l10n_rib_rec := treat(IO_l10n_obj as "L10N_RIB_REC");
   if FM_INTEGRATION_SQL.CONSUME_RTV(L_l10n_rib_rec.status_code,
                                     O_error_message,
                                     L_l10n_rib_rec.rib_msg,
                                     L_l10n_rib_rec.rib_msg_type) = FALSE then
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      raise PROGRAM_ERROR;
   end if;
   --
   L_l10n_rib_rec.status_code := API_CODES.SUCCESS;
   IO_l10n_obj := L_l10n_rib_rec;
   
   return TRUE;
EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      return FALSE;
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      return FALSE;
END CONSUME_RTV;
----------------------------------------------------------------------------
FUNCTION CONSUME_INVADJ(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   
   L_program   VARCHAR2(64) := 'L10N_BR_FIN_SQL.CONSUME_INVADJ';
   L_l10n_rib_rec   "L10N_RIB_REC"   := L10N_RIB_REC();

BEGIN
   
   L_l10n_rib_rec := treat(IO_l10n_obj as "L10N_RIB_REC");
   if FM_INTEGRATION_SQL.CONSUME_INVADJUST(L_l10n_rib_rec.status_code,
                                           O_error_message,
                                           L_l10n_rib_rec.rib_msg,
                                           L_l10n_rib_rec.rib_msg_type) = FALSE then
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      raise PROGRAM_ERROR;
   end if;
   --
   L_l10n_rib_rec.status_code := API_CODES.SUCCESS;
   IO_l10n_obj := L_l10n_rib_rec;
   
   return TRUE;
EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      return FALSE;
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      return FALSE;
END CONSUME_INVADJ;
----------------------------------------------------------------------------
FUNCTION CONSUME_SHIPMENT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   
   L_program   VARCHAR2(64) := 'L10N_BR_FIN_SQL.CONSUME_SHIPMENT';
   L_l10n_rib_rec   "L10N_RIB_REC"   := L10N_RIB_REC();

BEGIN

   L_l10n_rib_rec := treat(IO_l10n_obj as "L10N_RIB_REC");
   if FM_INTEGRATION_SQL.CONSUME_SHIPMENT(L_l10n_rib_rec.status_code,
                                          O_error_message,
                                          L_l10n_rib_rec.rib_msg,
                                          L_l10n_rib_rec.rib_msg_type) = FALSE then
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      raise PROGRAM_ERROR;
   end if;
   --
   L_l10n_rib_rec.status_code := API_CODES.SUCCESS;
   IO_l10n_obj := L_l10n_rib_rec;
   
   return TRUE;
EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      return FALSE;
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
      -- pass L_l10n_rib_rec.status_code back to the calling function
      IO_l10n_obj := L_l10n_rib_rec;
      return FALSE;
END CONSUME_SHIPMENT;
----------------------------------------------------------------------------
FUNCTION GET_LOC_AV_COST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   
   L_program              VARCHAR2(64) := 'L10N_BR_FIN_SQL.GET_LOC_AV_COST';
   L_l10n_fin_rec         "L10N_FIN_REC" := L10N_FIN_REC();
   L_item                 ITEM_LOC_SOH.ITEM%TYPE := NULL;
   L_location             ITEM_LOC_SOH.LOC%TYPE := NULL;
   L_loc_type             ITEM_LOC_SOH.LOC_TYPE%TYPE := NULL;
   L_cnpj                 V_BR_WH_REG_NUM.CNPJ%TYPE := NULL;
   L_av_cost              ITEM_LOC_SOH.AV_COST%TYPE := NULL;
   L_item_cost_info_rec   ITEM_COST_SQL.ITEM_COST_INFO_REC := NULL;
   
   cursor C_GET_CNPJ is
      select cnpj
        from v_br_wh_reg_num
       where wh = L_location
         and L_loc_type = 'W'
       union all
      select cnpj
        from v_br_store_reg_num
       where store = L_location
         and L_loc_type = 'S'
       union all
      select cnpj
        from v_br_partner_reg_num
       where partner_id = L_location
         and partner_type = L_loc_type
         and L_loc_type = 'E';
         
   cursor C_GET_AV_COST_CNPJ is
      select SUM(av_cost * DECODE(SIGN(stock_on_hand), -1,0, 0,0, stock_on_hand)) /
             DECODE(SUM(DECODE(SIGN(stock_on_hand),
                               -1,0, 0,0, stock_on_hand)),
                    0,1,SUM(DECODE(SIGN(stock_on_hand),-1,0, 0,0, stock_on_hand))) av_cost
        from item_loc_soh ils
        where item = L_item
          and exists(select 'x'
                       from (select TO_CHAR(wh) loc,
                                    'W' loc_type
                               from v_br_wh_reg_num
                              where cnpj = L_cnpj
                              union all
                             select TO_CHAR(store) loc,
                                    'S' loc_type
                               from v_br_store_reg_num
                              where cnpj = L_cnpj
                              union all
                             select partner_id loc,
                                    'E' loc_type
                               from v_br_partner_reg_num
                              where cnpj = L_cnpj
                                and partner_type = 'E') v_br_loc_reg_num
                      where ils.loc = v_br_loc_reg_num.loc
                        and ils.loc_type = v_br_loc_reg_num.loc_type);

BEGIN

   L_l10n_fin_rec := TREAT(IO_l10n_obj as "L10N_FIN_REC");
   
   L_item       := L_l10n_fin_rec.item;
   L_location   := NVL(L_l10n_fin_rec.dest_id,L_l10n_fin_rec.source_id);
   L_loc_type   := NVL(L_l10n_fin_rec.dest_type,L_l10n_fin_rec.source_type);
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   open C_GET_CNPJ;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   fetch C_GET_CNPJ into L_cnpj;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   close C_GET_CNPJ;
   ---   
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_AV_COST_CNPJ',
                    'ITEM_LOC_SOH',
                    NULL);
   open C_GET_AV_COST_CNPJ;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_AV_COST_CNPJ',
                    'ITEM_LOC_SOH',
                    NULL);
   fetch C_GET_AV_COST_CNPJ into L_av_cost;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_AV_COST_CNPJ',
                    'ITEM_LOC_SOH',
                    NULL);
   close C_GET_AV_COST_CNPJ;
   ---
   if NVL(L_av_cost,0) = 0 then
      if ITEM_COST_SQL.GET_COST_INFO(O_error_message,
                                     L_item_cost_info_rec,
                                     L_item,
                                     NULL,
                                     NULL) = FALSE then
         return FALSE;
      end if;
      L_av_cost := L_item_cost_info_rec.extended_base_cost;
   end if;
   ---
   L_l10n_fin_rec.av_cost := L_av_cost;
   IO_l10n_obj.av_cost := L_av_cost;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END GET_LOC_AV_COST;
----------------------------------------------------------------------------
FUNCTION GET_CURRENT_SOH_WAC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   
   L_program   VARCHAR2(64) := 'L10N_BR_FIN_SQL.GET_CURRENT_SOH_WAC';
   L_l10n_fin_rec         "L10N_FIN_REC" := L10N_FIN_REC();
   L_item                 ITEM_LOC_SOH.ITEM%TYPE := NULL;
   L_location             ITEM_LOC_SOH.LOC%TYPE := NULL;
   L_loc_type             ITEM_LOC_SOH.LOC_TYPE%TYPE := NULL;
   L_cnpj                 V_BR_WH_REG_NUM.CNPJ%TYPE := NULL;
   L_av_cost              ITEM_LOC_SOH.AV_COST%TYPE := NULL;
   L_item_cost_info_rec   ITEM_COST_SQL.ITEM_COST_INFO_REC := NULL;
   L_stock_on_hand        ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_average_weight       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := 0;
   L_calc_fld_type        VARCHAR2(10);
   L_rowid                ROWID;

   cursor C_GET_CNPJ is
      select cnpj
        from v_br_wh_reg_num
       where wh = L_location
         and L_loc_type = 'W'
       union all
      select cnpj
        from v_br_store_reg_num
       where store = L_location
         and L_loc_type = 'S'
       union all
      select cnpj
        from v_br_partner_reg_num
       where partner_id = L_location
         and partner_type = L_loc_type
         and L_loc_type = 'E';
         
   cursor C_GET_WAC is 
      select rowid,
             average_weight,
             av_cost
        from item_loc_soh
       where item = L_item
         and loc = L_location;

   cursor C_GET_SOH_CNPJ is
      select SUM(stock_on_hand) soh_cnpj
        from item_loc_soh ils
       where item = L_item
         and exists(select 'x'
                      from (select TO_CHAR(wh) loc,
                                   'W' loc_type
                              from v_br_wh_reg_num
                             where cnpj = L_cnpj
                             union all
                            select TO_CHAR(store) loc,
                                   'S' loc_type
                              from v_br_store_reg_num
                             where cnpj = L_cnpj
                             union all
                            select partner_id loc,
                                   'E' loc_type
                              from v_br_partner_reg_num
                             where cnpj = L_cnpj
                               and partner_type = 'E') v_br_loc_reg_num
                     where ils.loc = v_br_loc_reg_num.loc
                       and ils.loc_type = v_br_loc_reg_num.loc_type);
                       
   cursor C_GET_TOTAL_STOCK_CNPJ is
      select SUM(stock_on_hand + in_transit_qty + pack_comp_intran + pack_comp_soh) total_stock_cnpj
        from item_loc_soh ils
       where item = L_item
         and exists(select 'x'
                      from (select TO_CHAR(wh) loc,
                                   'W' loc_type
                              from v_br_wh_reg_num
                             where cnpj = L_cnpj
                             union all
                            select TO_CHAR(store) loc,
                                   'S' loc_type
                              from v_br_store_reg_num
                             where cnpj = L_cnpj
                             union all
                            select partner_id loc,
                                   'E' loc_type
                              from v_br_partner_reg_num
                             where cnpj = L_cnpj
                               and partner_type = 'E') v_br_loc_reg_num
                     where ils.loc = v_br_loc_reg_num.loc
                       and ils.loc_type = v_br_loc_reg_num.loc_type);
                       
   cursor C_GET_SOH_INTRAN_CNPJ is
      select SUM(stock_on_hand + in_transit_qty) soh_intran_cnpj
        from item_loc_soh ils
       where item = L_item
         and exists(select 'x'
                      from (select TO_CHAR(wh) loc,
                                   'W' loc_type
                              from v_br_wh_reg_num
                             where cnpj = L_cnpj
                             union all
                            select TO_CHAR(store) loc,
                                   'S' loc_type
                              from v_br_store_reg_num
                             where cnpj = L_cnpj
                             union all
                            select partner_id loc,
                                   'E' loc_type
                              from v_br_partner_reg_num
                             where cnpj = L_cnpj
                               and partner_type = 'E') v_br_loc_reg_num
                     where ils.loc = v_br_loc_reg_num.loc
                       and ils.loc_type = v_br_loc_reg_num.loc_type);
                       

BEGIN

   L_l10n_fin_rec := TREAT(IO_l10n_obj as "L10N_FIN_REC");
   
   L_item          := L_l10n_fin_rec.item;
   L_location      := NVL(L_l10n_fin_rec.dest_id,L_l10n_fin_rec.source_id);
   L_loc_type      := NVL(L_l10n_fin_rec.dest_type,L_l10n_fin_rec.source_type);
   L_calc_fld_type := L_l10n_fin_rec.calc_fld_type;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   open C_GET_CNPJ;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   fetch C_GET_CNPJ into L_cnpj;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   close C_GET_CNPJ;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_WAC',
                    'ITEM_LOC_SOH',
                    NULL);
   open C_GET_WAC;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_WAC',
                    'ITEM_LOC_SOH',
                    NULL);
   fetch C_GET_WAC into L_rowid,
                        L_average_weight,
                        L_av_cost;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_WAC',
                    'ITEM_LOC_SOH',
                    NULL);
   close C_GET_WAC;
   ---
   if L_calc_fld_type = 'SOH' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SOH_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      open C_GET_SOH_CNPJ;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SOH_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      fetch C_GET_SOH_CNPJ into L_stock_on_hand;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SOH_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      close C_GET_SOH_CNPJ;
   elsif L_calc_fld_type = 'TOTSTK' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_TOTAL_STOCK_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      open C_GET_TOTAL_STOCK_CNPJ;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_TOTAL_STOCK_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      fetch C_GET_TOTAL_STOCK_CNPJ into L_stock_on_hand;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_TOTAL_STOCK_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      close C_GET_TOTAL_STOCK_CNPJ;
   elsif L_calc_fld_type = 'INTRAN' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SOH_INTRAN_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      open C_GET_SOH_INTRAN_CNPJ;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SOH_INTRAN_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      fetch C_GET_SOH_INTRAN_CNPJ into L_stock_on_hand;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SOH_INTRAN_CNPJ',
                       'ITEM_LOC_SOH',
                       NULL);
      close C_GET_SOH_INTRAN_CNPJ;
   end if;
   ---
   L_l10n_fin_rec.stock_on_hand  := L_stock_on_hand;
   L_l10n_fin_rec.av_cost        := L_av_cost;
   L_l10n_fin_rec.fin_rowid      := ROWIDTOCHAR(L_rowid);
   L_l10n_fin_rec.average_weight := L_average_weight;
   IO_l10n_obj := L_l10n_fin_rec;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END GET_CURRENT_SOH_WAC;
----------------------------------------------------------------------------
FUNCTION UPDATE_AV_COST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   
   L_program        VARCHAR2(64) := 'L10N_BR_FIN_SQL.UPDATE_AV_COST';
   L_l10n_fin_rec   "L10N_FIN_REC" := L10N_FIN_REC();
   L_item           ITEM_LOC_SOH.ITEM%TYPE := NULL;
   L_location       ITEM_LOC_SOH.LOC%TYPE := NULL;
   L_loc_type       ITEM_LOC_SOH.LOC_TYPE%TYPE := NULL;
   L_cnpj           V_BR_WH_REG_NUM.CNPJ%TYPE := NULL;
   L_av_cost        ITEM_LOC_SOH.AV_COST%TYPE := 0;

   TYPE rowid_TBL IS TABLE OF ROWID INDEX BY PLS_INTEGER;
   L_ils_rowid_tbl  rowid_TBL;

   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_ITEM_LOC_SOH is
      select rowid
        from item_loc_soh ils
       where item = L_item
         and exists(select 'x'
                      from (select TO_CHAR(wh) loc,
                                   'W' loc_type
                              from v_br_wh_reg_num
                             where cnpj = L_cnpj
                             union all
                            select TO_CHAR(store) loc,
                                   'S' loc_type
                              from v_br_store_reg_num
                             where cnpj = L_cnpj
                             union all
                            select partner_id loc,
                                   'E' loc_type
                              from v_br_partner_reg_num
                             where cnpj = L_cnpj
                               and partner_type = 'E') v_br_loc_reg_num
                      where ils.loc = v_br_loc_reg_num.loc
                        and ils.loc_type = v_br_loc_reg_num.loc_type)
         for update of av_cost nowait;
   
   cursor C_GET_CNPJ is
      select cnpj
        from v_br_wh_reg_num
       where wh = L_location
         and L_loc_type = 'W'
       union all
      select cnpj
        from v_br_store_reg_num
       where store = L_location
         and L_loc_type = 'S'
       union all
      select cnpj
        from v_br_partner_reg_num
       where partner_id = L_location
         and partner_type = L_loc_type
         and L_loc_type = 'E';
         
BEGIN

   L_l10n_fin_rec := TREAT(IO_l10n_obj as "L10N_FIN_REC");
   L_item         := L_l10n_fin_rec.item;
   L_location     := NVL(L_l10n_fin_rec.dest_id,L_l10n_fin_rec.source_id);
   L_loc_type     := NVL(L_l10n_fin_rec.dest_type,L_l10n_fin_rec.source_type);
   L_av_cost      := L_l10n_fin_rec.av_cost;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   open C_GET_CNPJ;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   fetch C_GET_CNPJ into L_cnpj;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CNPJ',
                    'V_BR_WH_REG_NUM,
                     V_BR_STORE_REG_NUM,
                     V_BR_PARTNER_REG_NUM',
                    NULL);
   close C_GET_CNPJ;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    NULL);
   open C_LOCK_ITEM_LOC_SOH;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCK_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    NULL);
   fetch C_LOCK_ITEM_LOC_SOH bulk collect into L_ils_rowid_tbl;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    NULL);
   close C_LOCK_ITEM_LOC_SOH;
   
   if L_ils_rowid_tbl.count > 0 then
      FORALL i IN L_ils_rowid_tbl.first .. L_ils_rowid_tbl.last
         update item_loc_soh
            set av_cost = L_av_cost
          where rowid = L_ils_rowid_tbl(i);
   end if;
                      
   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_LOC_SOH',
                                            NULL,
                                            TO_CHAR(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;      
END UPDATE_AV_COST;
----------------------------------------------------------------------------
FUNCTION POST_VAT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   
   L_program        VARCHAR2(64) := 'L10N_BR_FIN_SQL.POST_VAT';
         
BEGIN

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END POST_VAT;
----------------------------------------------------------------------------
FUNCTION RECOVERABLE_TAX_POSTING(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_l10n_obj       IN OUT L10N_OBJ)
RETURN BOOLEAN IS
   L_program                          TRAN_DATA.PGM_NAME%TYPE        := 'L10N_BR_FIN_SQL.RECOVERABLE_TAX_POSTING';
   L_l10n_fin_rec                     "L10N_FIN_REC"                 := L10N_FIN_REC();
   L_av_cost                          ITEM_LOC_SOH.AV_COST%TYPE;
   L_tran_code                        TRAN_DATA.TRAN_CODE%TYPE;
   L_country_id                       COUNTRY_ATTRIB.COUNTRY_ID%TYPE := NULL;
   L_source_loc_recov_tax             TRAN_DATA.TOTAL_COST%TYPE      := 0;
   L_sorc_loc_recov_tax_dest_curr     TRAN_DATA.TOTAL_COST%TYPE      := 0;
   L_dest_loc_recov_tax               TRAN_DATA.TOTAL_COST%TYPE      := 0;
   L_dest_loc_recov_tax_dest_curr     TRAN_DATA.TOTAL_COST%TYPE      := 0; 

BEGIN

   L_l10n_fin_rec := TREAT(IO_l10n_obj as "L10N_FIN_REC");

   L_source_loc_recov_tax := (L_l10n_fin_rec.av_cost - L_l10n_fin_rec.base_cost) * L_l10n_fin_rec.tran_data.units;
   L_dest_loc_recov_tax   := (L_l10n_fin_rec.unit_cost - L_l10n_fin_rec.base_cost) * L_l10n_fin_rec.tran_data.units;

   if L_l10n_fin_rec.tran_data.tran_code in (30, 37) then
      --Tran_code 74 will only be posted if recoverable tax is non-zero.
      if L_dest_loc_recov_tax != 0 then
         L_tran_code := 74;
         --conversion of recoverable tax to to_loc currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_l10n_fin_rec.source_id,
                                             L_l10n_fin_rec.source_type,
                                             NULL,
                                             L_l10n_fin_rec.dest_id,
                                             L_l10n_fin_rec.dest_type,
                                             NULL,
                                             L_dest_loc_recov_tax,
                                             L_dest_loc_recov_tax_dest_curr,
                                             NULL,
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                L_l10n_fin_rec.tran_data.item,
                                                L_l10n_fin_rec.tran_data.dept,
                                                L_l10n_fin_rec.tran_data.class,
                                                L_l10n_fin_rec.tran_data.subclass,
                                                L_l10n_fin_rec.dest_id,
                                                L_l10n_fin_rec.dest_type,
                                                L_l10n_fin_rec.tran_data.tran_date,
                                                L_tran_code,
                                                NULL,
                                                L_l10n_fin_rec.tran_data.units,
                                                L_dest_loc_recov_tax_dest_curr,
                                                NULL,
                                                L_l10n_fin_rec.tran_data.ref_no_1,
                                                L_l10n_fin_rec.tran_data.ref_no_2,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_program) = FALSE then
            return FALSE;
         end if;
      end if;

      --Tran_code 75 and tran_code 30/32 and 37/38 will only be posted if recoverable tax is non-zero.
      if L_source_loc_recov_tax != 0 then
         L_tran_code := 75;
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                L_l10n_fin_rec.tran_data.item,
                                                L_l10n_fin_rec.tran_data.dept,
                                                L_l10n_fin_rec.tran_data.class,
                                                L_l10n_fin_rec.tran_data.subclass,
                                                L_l10n_fin_rec.source_id,
                                                L_l10n_fin_rec.source_type,
                                                L_l10n_fin_rec.tran_data.tran_date,
                                                L_tran_code,
                                                NULL,
                                                L_l10n_fin_rec.tran_data.units,
                                                L_source_loc_recov_tax,
                                                NULL,
                                                L_l10n_fin_rec.tran_data.ref_no_1,
                                                L_l10n_fin_rec.tran_data.ref_no_2,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_program) = FALSE then
            return FALSE;
         end if;

         --This basically converts the portion of the tax previously written for 30/32 or 37/38 in the TRAN_DATA.
         --This will balance the book written by 30/32, 37/38 and 74/75.

         --Posting tran_code 32/38 with negative recoverable tax.
         STKLEDGR_SQL.P_tran_data_size                                                        := STKLEDGR_SQL.P_tran_data_size + 1;
         STKLEDGR_SQL.P_tran_data_item(STKLEDGR_SQL.P_tran_data_size)                         := L_l10n_fin_rec.tran_data.item;
         STKLEDGR_SQL.P_tran_data_dept(STKLEDGR_SQL.P_tran_data_size)                         := L_l10n_fin_rec.tran_data.dept;
         STKLEDGR_SQL.P_tran_data_class(STKLEDGR_SQL.P_tran_data_size)                        := L_l10n_fin_rec.tran_data.class;
         STKLEDGR_SQL.P_tran_data_subclass(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.tran_data.subclass;
         STKLEDGR_SQL.P_tran_data_pack_ind(STKLEDGR_SQL.P_tran_data_size)                     := NULL;
         STKLEDGR_SQL.P_tran_data_location(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.source_id;
         STKLEDGR_SQL.P_tran_data_loc_type(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.source_type;
         STKLEDGR_SQL.P_tran_data_tran_date(STKLEDGR_SQL.P_tran_data_size)                    := TO_DATE(TO_CHAR(L_l10n_fin_rec.tran_data.tran_date,'YYYYMMDD'),'YYYYMMDD');
         STKLEDGR_SQL.P_tran_data_units(STKLEDGR_SQL.P_tran_data_size)                        := L_l10n_fin_rec.tran_data.units;
         STKLEDGR_SQL.P_tran_data_total_cost(STKLEDGR_SQL.P_tran_data_size)                   := (-1 * L_source_loc_recov_tax);
         STKLEDGR_SQL.P_tran_data_total_retail(STKLEDGR_SQL.P_tran_data_size)                 := L_l10n_fin_rec.total_retail;
         STKLEDGR_SQL.P_tran_data_ref_no_1(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.tran_data.ref_no_1;
         STKLEDGR_SQL.P_tran_data_ref_no_2(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.tran_data.ref_no_2;
         STKLEDGR_SQL.P_tran_data_old_unit_retail(STKLEDGR_SQL.P_tran_data_size)              := NULL;
         STKLEDGR_SQL.P_tran_data_new_unit_retail(STKLEDGR_SQL.P_tran_data_size)              := NULL;
         STKLEDGR_SQL.P_tran_data_pgm_name(STKLEDGR_SQL.P_tran_data_size)                     := L_program;
         STKLEDGR_SQL.P_tran_data_sales_type(STKLEDGR_SQL.P_tran_data_size)                   := NULL;
         STKLEDGR_SQL.P_tran_data_vat_rate(STKLEDGR_SQL.P_tran_data_size)                     := NULL;
         STKLEDGR_SQL.P_tran_data_av_cost(STKLEDGR_SQL.P_tran_data_size)                      := NULL;
         STKLEDGR_SQL.P_tran_data_timestamp(STKLEDGR_SQL.P_tran_data_size)                    := SYSDATE;
         STKLEDGR_SQL.P_tran_data_ref_pack_no(STKLEDGR_SQL.P_tran_data_size)                  := NULL;
         STKLEDGR_SQL.P_tran_data_tot_cost_excl_elc(STKLEDGR_SQL.P_tran_data_size)            := NULL;
         STKLEDGR_SQL.P_tran_data_act_location(STKLEDGR_SQL.P_tran_data_size)                 := NULL;
         STKLEDGR_SQL.P_tran_data_act_loc_type(STKLEDGR_SQL.P_tran_data_size)                 := NULL;
         if L_l10n_fin_rec.tran_data.tran_code = 30 then
            STKLEDGR_SQL.P_tran_data_tran_code(STKLEDGR_SQL.P_tran_data_size)                 := 32;
            STKLEDGR_SQL.P_tran_data_adj_code(STKLEDGR_SQL.P_tran_data_size)                  := L_l10n_fin_rec.tran_data.adj_code;
            STKLEDGR_SQL.P_tran_data_gl_ref_no(STKLEDGR_SQL.P_tran_data_size)                 := L_l10n_fin_rec.tran_data.gl_ref_no;
         elsif L_l10n_fin_rec.tran_data.tran_code = 37 then
            STKLEDGR_SQL.P_tran_data_tran_code(STKLEDGR_SQL.P_tran_data_size)                 := 38;
            STKLEDGR_SQL.P_tran_data_adj_code(STKLEDGR_SQL.P_tran_data_size)                  := NULL;
            STKLEDGR_SQL.P_tran_data_gl_ref_no(STKLEDGR_SQL.P_tran_data_size)                 := L_l10n_fin_rec.dest_id;
         end if;

         --conversion of recoverable tax to to_loc currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_l10n_fin_rec.source_id,
                                             L_l10n_fin_rec.source_type,
                                             NULL,
                                             L_l10n_fin_rec.dest_id,
                                             L_l10n_fin_rec.dest_type,
                                             NULL,
                                             L_source_loc_recov_tax,
                                             L_sorc_loc_recov_tax_dest_curr,
                                             NULL,
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         --To post 30/37 with negative recoverable tax
         STKLEDGR_SQL.P_tran_data_size                                                        := STKLEDGR_SQL.P_tran_data_size + 1;
         STKLEDGR_SQL.P_tran_data_item(STKLEDGR_SQL.P_tran_data_size)                         := L_l10n_fin_rec.tran_data.item;
         STKLEDGR_SQL.P_tran_data_dept(STKLEDGR_SQL.P_tran_data_size)                         := L_l10n_fin_rec.tran_data.dept;
         STKLEDGR_SQL.P_tran_data_class(STKLEDGR_SQL.P_tran_data_size)                        := L_l10n_fin_rec.tran_data.class;
         STKLEDGR_SQL.P_tran_data_subclass(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.tran_data.subclass;
         STKLEDGR_SQL.P_tran_data_pack_ind(STKLEDGR_SQL.P_tran_data_size)                     := NULL;
         STKLEDGR_SQL.P_tran_data_location(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.dest_id;
         STKLEDGR_SQL.P_tran_data_loc_type(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.dest_type;
         STKLEDGR_SQL.P_tran_data_tran_date(STKLEDGR_SQL.P_tran_data_size)                    := TO_DATE(TO_CHAR(L_l10n_fin_rec.tran_data.tran_date,'YYYYMMDD'),'YYYYMMDD');
         STKLEDGR_SQL.P_tran_data_units(STKLEDGR_SQL.P_tran_data_size)                        := L_l10n_fin_rec.tran_data.units;
         STKLEDGR_SQL.P_tran_data_total_cost(STKLEDGR_SQL.P_tran_data_size)                   := (-1 * L_sorc_loc_recov_tax_dest_curr);
         STKLEDGR_SQL.P_tran_data_total_retail(STKLEDGR_SQL.P_tran_data_size)                 := L_l10n_fin_rec.tran_data.total_retail;
         STKLEDGR_SQL.P_tran_data_ref_no_1(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.tran_data.ref_no_1;
         STKLEDGR_SQL.P_tran_data_ref_no_2(STKLEDGR_SQL.P_tran_data_size)                     := L_l10n_fin_rec.tran_data.ref_no_2;
         STKLEDGR_SQL.P_tran_data_pgm_name(STKLEDGR_SQL.P_tran_data_size)                     := L_program;
         STKLEDGR_SQL.P_tran_data_sales_type(STKLEDGR_SQL.P_tran_data_size)                   := NULL;
         STKLEDGR_SQL.P_tran_data_vat_rate(STKLEDGR_SQL.P_tran_data_size)                     := NULL;
         STKLEDGR_SQL.P_tran_data_av_cost(STKLEDGR_SQL.P_tran_data_size)                      := NULL;
         STKLEDGR_SQL.P_tran_data_timestamp(STKLEDGR_SQL.P_tran_data_size)                    := SYSDATE;
         STKLEDGR_SQL.P_tran_data_tot_cost_excl_elc(STKLEDGR_SQL.P_tran_data_size)            := NULL;
         STKLEDGR_SQL.P_tran_data_act_location(STKLEDGR_SQL.P_tran_data_size)                 := NULL;
         STKLEDGR_SQL.P_tran_data_act_loc_type(STKLEDGR_SQL.P_tran_data_size)                 := NULL;
         if L_l10n_fin_rec.tran_data.tran_code = 30 then
            STKLEDGR_SQL.P_tran_data_tran_code(STKLEDGR_SQL.P_tran_data_size)                 := 30;
            STKLEDGR_SQL.P_tran_data_adj_code(STKLEDGR_SQL.P_tran_data_size)                  := L_l10n_fin_rec.tran_data.adj_code;
            STKLEDGR_SQL.P_tran_data_gl_ref_no(STKLEDGR_SQL.P_tran_data_size)                 := L_l10n_fin_rec.tran_data.gl_ref_no;
            STKLEDGR_SQL.P_tran_data_old_unit_retail(STKLEDGR_SQL.P_tran_data_size)           := L_l10n_fin_rec.tran_data.old_unit_retail;
            STKLEDGR_SQL.P_tran_data_new_unit_retail(STKLEDGR_SQL.P_tran_data_size)           := L_l10n_fin_rec.tran_data.new_unit_retail;
            STKLEDGR_SQL.P_tran_data_ref_pack_no(STKLEDGR_SQL.P_tran_data_size)               := L_l10n_fin_rec.tran_data.ref_pack_no;
         elsif L_l10n_fin_rec.tran_data.tran_code = 37 then
            STKLEDGR_SQL.P_tran_data_tran_code(STKLEDGR_SQL.P_tran_data_size)                 := 37;
            STKLEDGR_SQL.P_tran_data_adj_code(STKLEDGR_SQL.P_tran_data_size)                  := NULL;
            STKLEDGR_SQL.P_tran_data_gl_ref_no(STKLEDGR_SQL.P_tran_data_size)                 := L_l10n_fin_rec.source_id;
            STKLEDGR_SQL.P_tran_data_old_unit_retail(STKLEDGR_SQL.P_tran_data_size)           := NULL;
            STKLEDGR_SQL.P_tran_data_new_unit_retail(STKLEDGR_SQL.P_tran_data_size)           := NULL;
            STKLEDGR_SQL.P_tran_data_ref_pack_no(STKLEDGR_SQL.P_tran_data_size)               := NULL;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RECOVERABLE_TAX_POSTING;
----------------------------------------------------------------------------
END L10N_BR_FIN_SQL;
/