CREATE OR REPLACE PACKAGE BODY L10N_BR_FISCAL_FDN_QUERY_SQL AS
--------------------------------------------------------
FUNCTION LOAD_STAGE_TABLE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_tax_service_id  IN OUT  L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE,
                          I_fiscal_code     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.FISCAL_CODE%TYPE,
                          I_cut_off_date    IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.CUT_OFF_DATE%TYPE default NULL,
                          I_start_index     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.START_INDEX%TYPE default NULL,
                          I_end_index       IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.END_INDEX%TYPE default NULL)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION LOAD_STAGE_TABLE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_tax_service_id  IN OUT  L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE,
                          I_fiscal_code     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.FISCAL_CODE%TYPE,
                          I_cut_off_date    IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.CUT_OFF_DATE%TYPE default NULL,
                          I_start_index     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.START_INDEX%TYPE default NULL,
                          I_end_index       IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.END_INDEX%TYPE default NULL)
RETURN BOOLEAN IS

   PRAGMA AUTONOMOUS_TRANSACTION;

   L_tax_service_id   L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE := NULL;
   L_program          VARCHAR2(62) := 'L10N_BR_FISCAL_FDN_QUERY_SQL.LOAD_STAGE_TABLE';

   cursor C_GET_SEQ is
      select l10n_br_tax_service_seq.nextval
        from dual;

BEGIN

   -- Check for required input parameters
   if I_fiscal_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_fiscal_code', L_program, NULL);
      return FALSE;
   end if;

   open C_GET_SEQ;
   fetch C_GET_SEQ into L_tax_service_id;
   close C_GET_SEQ;

   O_tax_service_id := L_tax_service_id;

   -- Insert into staging table
   insert into l10n_br_tax_call_stage_fsc_fdn
               (tax_service_id,
                fiscal_code,
                cut_off_date,
                start_index,
                end_index)
        values (L_tax_service_id,
                I_fiscal_code,
                I_cut_off_date,
                I_start_index,
                I_end_index);

   -- Insert into routing table
   insert into l10n_br_tax_call_stage_routing
               (tax_service_id,
                client_id,
                update_history_ind)
        values (L_tax_service_id,
                L10N_BR_TAX_SERVICE_MNGR_SQL.FISCAL_FDN,
                'N');

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END LOAD_STAGE_TABLE;
--------------------------------------------------------
FUNCTION QUERY_FISCAL_CODE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_tax_service_id  IN OUT  L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE,
                           I_fiscal_code     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.FISCAL_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(62) := 'L10N_BR_FISCAL_FDN_QUERY_SQL.QUERY_FISCAL_CODE';
   L_status    VARCHAR2(2);

BEGIN

   -- Check for required input parameters
   if I_fiscal_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_fiscal_code', L_program, NULL);
      return FALSE;
   end if;

   -- Stage the fiscal code information
   if LOAD_STAGE_TABLE(O_error_message,
                       O_tax_service_id,
                       I_fiscal_code) = FALSE then
      return FALSE;
   end if;

   -- Invoke RTIL
   if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                       L_status,
                                                       O_tax_service_id,
                                                       L10N_BR_FISCAL_FDN_QUERY_SQL.LP_FISCAL_FDN)= FALSE then
      return FALSE;
   end if;

   -- Update the fiscal_attrib_updates table
   merge into fiscal_attrib_updates fau
   using (select vdate from period) use_this
      on (fau.attribute      = I_fiscal_code
          and fau.country_id = 'BR')
   when matched then
      update set last_upd_datetime = use_this.vdate
   when not matched then
      insert(attribute,
             country_id,
             last_upd_datetime)
      values(I_fiscal_code,
             'BR',
             use_this.vdate);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END QUERY_FISCAL_CODE;
--------------------------------------------------------
FUNCTION EXTAX_MAINT_QUERY_FISCAL_CODE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_tax_service_id     OUT  L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE,
                                       I_fiscal_code     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.FISCAL_CODE%TYPE,
                                       I_cut_off_date    IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.CUT_OFF_DATE%TYPE,
                                       I_start_index     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.START_INDEX%TYPE,
                                       I_end_index       IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.END_INDEX%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(62) := 'L10N_BR_FISCAL_FDN_QUERY_SQL.EXTAX_MAINT_QUERY_FISCAL_CODE';
   L_status    VARCHAR2(2);


BEGIN

   -- Check for required input parameters
   if I_fiscal_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_fiscal_code', L_program, NULL);
      return FALSE;
   end if;

   -- Stage the fiscal code information
   if LOAD_STAGE_TABLE(O_error_message,
                       O_tax_service_id,
                       I_fiscal_code,
                       I_cut_off_date,
                       I_start_index,
                       I_end_index) = FALSE then
      return FALSE;
   end if;

   -- Invoke RTIL
   if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                       L_status,
                                                       O_tax_service_id,
                                                       L10N_BR_FISCAL_FDN_QUERY_SQL.LP_FISCAL_FDN)= FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END EXTAX_MAINT_QUERY_FISCAL_CODE;
--------------------------------------------------------
FUNCTION GET_REQUEST_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_businessObject  IN OUT  "RIB_FiscalFDNQryRBM_REC",
                          I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_fiscal_code       L10N_BR_TAX_CALL_STAGE_FSC_FDN.FISCAL_CODE%TYPE := NULL;
   L_last_upd_datetime DATE := NULL;
   L_start_index       l10n_br_tax_call_stage_fsc_fdn.start_index%type := null;
   L_end_index         l10n_br_tax_call_stage_fsc_fdn.start_index%type := null;
   L_program           VARCHAR2(62) := 'L10N_BR_FISCAL_FDN_QUERY_SQL.GET_REQUEST_DATA';

   cursor C_CODE_DATE is
      select l.fiscal_code,
             nvl(l.cut_off_date, f.last_upd_datetime),
             l.start_index,
             l.end_index
        from fiscal_attrib_updates f,
             l10n_br_tax_call_stage_fsc_fdn l
       where l.tax_service_id = I_tax_service_id
         and l.fiscal_code    = f.attribute(+);

BEGIN

   -- Check for required input parameters
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      return FALSE;
   end if;

   open C_CODE_DATE;
   fetch C_CODE_DATE into L_fiscal_code,
                          L_last_upd_datetime,
                          L_start_index,
                          L_end_index;
   if C_CODE_DATE%NOTFOUND then
      close C_CODE_DATE;
      O_error_message:= SQL_LIB.CREATE_MSG('L10N_STAGE_REC_NOT_FOUND', I_tax_service_id,
                                           NULL, NULL);
     return FALSE;
   end if;
   close C_CODE_DATE;

   O_businessObject := "RIB_FiscalFDNQryRBM_REC"(0, --RIB_OID
                                                 L_fiscal_code, --fiscal_code
                                                 L_last_upd_datetime, --cut_off_date
                                                 L_start_index,  --begin_index
                                                 L_end_index);  --end_index

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END GET_REQUEST_DATA;
--------------------------------------------------------
FUNCTION STAGE_RESULTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_businessObject   IN      "RIB_FiscalFDNColRBM_REC",
                       I_tax_service_id   IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program   VARCHAR2(62) := 'L10N_BR_FISCAL_FDN_QUERY_SQL.STAGE_RESULTS';

BEGIN

   -- Check for required input parameters
   if I_tax_service_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tax_service_id', L_program, NULL);
      return FALSE;
   end if;

   insert into l10n_br_tax_call_res_fsc_count (
      tax_service_id,
      total_fnd_count)
   values (I_tax_service_id,
           I_businessObject.total_fdn_count);

   insert into l10n_br_tax_call_res_fsc_fnd (
      tax_service_id,
      fiscal_code,
      fiscal_code_description,
      fiscal_parent_code,
      fiscal_extended_parent_code,
      origin_state,
      destination_state,
      creation_date,
      effective_date)
   select I_tax_service_id,
          input.fiscal_code,
          input.fiscal_code_description,
          input.fiscal_parent_code,
          input.fiscal_extended_parent_code,
          input.origin_state,
          input.destination_state,
          input.creation_date,
          input.effective_date
     from TABLE(CAST(I_businessObject.FiscalFDNRBO_TBL as "RIB_FiscalFDNRBO_TBL")) input;

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END STAGE_RESULTS;
--------------------------------------------------------
END L10N_BR_FISCAL_FDN_QUERY_SQL;
/
