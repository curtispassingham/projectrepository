CREATE OR REPLACE PACKAGE L10N_BR_FISCAL_FDN_QUERY_SQL AS
-------------------------------------------------------------------------------------------------------------
-- Global public constants
-------------------------------------------------------------------------------------------------------------
LP_FISCAL_FDN  CONSTANT VARCHAR(15) := 'FISCAL_FDN';
-------------------------------------------------------------------------------------------------------------
-- Public function declarations
-------------------------------------------------------------------------------------------------------------
-- Function Name  : QUERY_FISCAL_CODE
-- Purpose        : Main function. Calls functions that performs the following:
--                  1. Stages requested fiscal foundation data into l10n_br_tax_call_stage_fsc_fdn
--                  2. Initiates the RTIL tax process through an HTTP call
--                  3. Processes results from the RTIL tax process callback
--                  4. Cleans up staging and result tables
-- Required Input : I_fiscal_code
-------------------------------------------------------------------------------------------------------------
FUNCTION QUERY_FISCAL_CODE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_tax_service_id  IN OUT  L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE,
                           I_fiscal_code     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.FISCAL_CODE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : EXTAX_MAINT_QUERY_FISCAL_CODE
-- Purpose        : Same as QUERY_FISCAL_CODE but deals with additional seed/refresh requirements
-- Required Input : I_fiscal_code
-------------------------------------------------------------------------------------------------------------
FUNCTION EXTAX_MAINT_QUERY_FISCAL_CODE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_tax_service_id     OUT  L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE,
                                       I_fiscal_code     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.FISCAL_CODE%TYPE,
                                       I_cut_off_date    IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.CUT_OFF_DATE%TYPE,
                                       I_start_index     IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.START_INDEX%TYPE,
                                       I_end_index       IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.END_INDEX%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : GET_REQUEST_DATA
-- Purpose        : Called by the L10N_BR_TAX_SERVICE_MNGR_SQL package.
--                  Retrieves information from the l10n_br_tax_call_stage_fsc_fdn table for the passed-in
--                  I_tax_service_id and builds a O_businessObject of type RIB_FiscalFDNQryRBM_REC from the
--                  retrieved data.
-- Required Input : I_tax_service_id, O_businessObject
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_REQUEST_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_businessObject  IN OUT  "RIB_FiscalFDNQryRBM_REC",
                          I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : STAGE_RESULTS
-- Purpose        : Called by the L10N_BR_TAX_SERVICE_MNGR_SQL package.
--                  Accepts an I_businessObject of type RIB_FiscalFDNColRBM_REC and inserts the information
--                  in the object to l10n_br_tax_call_res_fsc_fnd.  The I_tax_service_id is part of the
--                  information inserted.
-- Required Input : I_tax_service_id, I_businessObject
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_RESULTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_businessObject   IN      "RIB_FiscalFDNColRBM_REC",
                       I_tax_service_id   IN      L10N_BR_TAX_CALL_STAGE_FSC_FDN.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
END L10N_BR_FISCAL_FDN_QUERY_SQL;
/
