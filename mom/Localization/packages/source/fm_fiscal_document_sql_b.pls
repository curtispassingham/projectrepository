CREATE OR REPLACE PACKAGE BODY FM_FISCAL_DOCUMENT_SQL is

  FUNCTION GET_MASK_CNPJ (O_error_message IN OUT VARCHAR2
                         ,O_cnpj_mask     IN OUT VARCHAR2
                         ,I_cnpj          IN V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
  return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_FISCAL_DOCUMENT_SQL.GET_VAT_REGION';

   cursor C_MASK is
   SELECT decode(I_cnpj,NULL,NULL,
       REPLACE(REPLACE(REPLACE(To_Char(LPad(REPLACE(I_cnpj,' ')
                                           ,14
                                          ,'0')
                                      ,'00,000,000,0000,00')
                               ,','
                              ,'.')
                       ,' ')
              ,'.'||Trim(To_Char(Trunc(Mod(LPad(I_cnpj
                                               ,14
                                               ,'0')
                                      ,1000000)/100)
                                ,'0000'))||'.'
              ,'/'||Trim(To_Char(Trunc(Mod(LPad(I_cnpj
                                               ,14
                                               ,'0')
                                       ,1000000)/100)
                                ,'0000'))||'-')) cnpj
   FROM (
     (SELECT  I_cnpj cnpj FROM dual)) ;

  BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_MASK', 'V_FISCAL_ATTRIBUTES', 'cnpj = '||I_cnpj);
   open C_MASK;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_MASK', 'V_FISCAL_ATTRIBUTES', 'cnpj = '||I_cnpj);
   fetch C_MASK into O_cnpj_mask;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_MASK', 'V_FISCAL_ATTRIBUTES', 'cnpj = '||I_cnpj);
   close C_MASK;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_MASK_CNPJ;

-----------------------------------------------------------------------------

FUNCTION GET_TOTAL_SERVICE(O_error_message  IN OUT VARCHAR2,
                           O_total          IN OUT NUMBER,
                           I_fiscal_doc_id  IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                           I_service_ind    IN V_BR_ITEM_FISCAL_ATTRIB.SERVICE_IND%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_FISCAL_DOCUMENT_SQL.GET_TOTAL_SERVICE';

  CURSOR C_TOT IS
  SELECT SUM(total_cost)
  FROM
    (SELECT total_cost
    FROM fm_fiscal_doc_detail dd ,
      v_br_item_fiscal_attrib ia
    WHERE dd.fiscal_doc_id = I_fiscal_doc_id
    AND ia.item            = dd.item
    AND ia.service_ind     = I_service_ind
  UNION ALL
  SELECT DECODE(I_service_ind,'N',total_cost,0) total_cost
  FROM fm_fiscal_doc_detail dd ,
    item_master itm
  WHERE dd.fiscal_doc_id = I_fiscal_doc_id
  AND itm.item           = dd.item
  AND itm.pack_ind       ='Y'
    );

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_TOT', 'FM_FISCAL_DOC_DETAIL', 'fiscal_doc_id = '||I_fiscal_doc_id||',service_ind = '||I_service_ind);
   open C_TOT;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_TOT', 'FM_FISCAL_DOC_DETAIL', 'fiscal_doc_id = '||I_fiscal_doc_id||',service_ind = '||I_service_ind);
   fetch C_TOT into O_total;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_TOT', 'FM_FISCAL_DOC_DETAIL', 'fiscal_doc_id = '||I_fiscal_doc_id||',service_ind = '||I_service_ind);
   close C_TOT;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_TOTAL_SERVICE;


end FM_FISCAL_DOCUMENT_SQL;
/
 