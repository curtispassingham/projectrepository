CREATE or REPLACE PACKAGE fm_return_nf_sql as
--
L_seq_fiscal_doc_id       NUMBER;
L_seq_schedule_no         NUMBER;
L_seq_fiscal_doc_line_id  NUMBER;
--
----------------------------------------------------------------------------------
-- Function Name: RETURN_NF_PROCESS
-- Purpose:       This function calls the other relavant functions for creating Return NF .
----------------------------------------------------------------------------------
FUNCTION RETURN_NF_PROCESS( O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHK_EXISTS_IN_CORREC_DOC
-- Purpose:       This function checks if the fiscal_doc_id exists in fm_correction_doc 
--                table for generating Return NF .
----------------------------------------------------------------------------------
FUNCTION CHK_EXISTS_IN_CORREC_DOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                 ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                 ,O_exists         OUT BOOLEAN)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_SCHED
-- Purpose:       This function will create a new schedule for the return NF 
--                and the status of the schedule will be Approved.
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_SCHED(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                         ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                          )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_HEADER
-- Purpose:       This function will populate header table for the return NF 
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_HEADER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: UPDATE_RNF_HEADER
-- Purpose:       This function will UPDATE all the column in Header table wich needs to be calculated based on the 
--                Return NF in the detail table.
----------------------------------------------------------------------------------
FUNCTION UPDATE_RNF_HEADER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id      IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_DETAIL 
-- Purpose:       This function will populate detail table for the return NF 
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_TAX_HEAD 
-- Purpose:       This function will populate Tax header table for the return NF  
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_TAX_HEAD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                            ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CREATE_RNF_TAX_DETAIL 
-- Purpose:       This function will populate Tax Detail table for the return NF  
----------------------------------------------------------------------------------
FUNCTION CREATE_RNF_TAX_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                               )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: UPDATE_RNF_TAX_HEADER
-- Purpose:       This function will UPDATE all the column in Header table wich needs to be calculated based on the 
--                Return NF in the detail table.
----------------------------------------------------------------------------------
FUNCTION UPDATE_RNF_TAX_HEADER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_fiscal_doc_id      IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                              )
   return BOOLEAN;
--
END fm_return_nf_sql;
/

