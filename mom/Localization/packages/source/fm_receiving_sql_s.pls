CREATE OR REPLACE PACKAGE FM_RECEIVING_SQL as
----------------------------------------------------------------------------------
-- Function Name: calc_wac_update
-- Purpose:       This function inserts a record into FM_CORRECTION_DOC and
--                FM_CORRECTION_TAX_DOC tables
----------------------------------------------------------------------------------
FUNCTION UPDATE_RCV_DETAILS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_id       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_fiscal_doc_line_id  IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            I_status              IN     FM_RECEIVING_HEADER.STATUS%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_RCV_STATUS
-- Purpose:       This function updates the fm_receiving_header status field
--
----------------------------------------------------------------------------------
FUNCTION UPDATE_RCV_STATUS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                           I_status            IN     FM_RECEIVING_HEADER.STATUS%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_FM_RECEIVING_DETAIL
-- Purpose:       This function updates FM_RECEIVING_DETAIL with new NIC and EBC
--                costs
----------------------------------------------------------------------------------
FUNCTION UPDATE_FM_RECEIVING_DETAIL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_seq_no          IN     FM_RECEIVING_DETAIL.SEQ_NO%TYPE,
                                    I_nic_cost        IN     FM_RECEIVING_DETAIL.NIC_COST%TYPE,
                                    I_ebc_cost        IN     FM_RECEIVING_DETAIL.EBC_COST%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_COST_RESOLUTION_TYPE
-- Purpose:       This function gets the resolution type from FM_RESOLUTION table
--                if the Cost discrepancy has been resolved
----------------------------------------------------------------------------------
FUNCTION GET_COST_RESOLUTION_TYPE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_fiscal_doc_line_id    IN     FM_RESOLUTION.FISCAL_DOC_LINE_ID%TYPE,
                                  I_discrep_type          IN     FM_RESOLUTION.DISCREP_TYPE%TYPE,
                                  O_resolution_type       IN OUT FM_RESOLUTION.RESOLUTION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_TAX_RESOLUTION_TYPE
-- Purpose:       This function gets the resolution_type from FM_RESOLUTION table
--                for the tax discrepancies which have been resolved
----------------------------------------------------------------------------------
FUNCTION GET_TAX_RESOLUTION_TYPE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_fiscal_doc_line_id    IN     FM_RESOLUTION.FISCAL_DOC_LINE_ID%TYPE,
                                 I_discrep_type          IN     FM_RESOLUTION.DISCREP_TYPE%TYPE,
                                 I_tax_code              IN     FM_RESOLUTION.TAX_CODE%TYPE,
                                 O_resolution_type       IN OUT FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                                 O_exists                IN OUT BOOLEAN)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_NIC_IND
-- Purpose:       Fetches the INCL_IND_FLG from VAT_CODES table for a given tax
--                codes
----------------------------------------------------------------------------------
FUNCTION GET_NIC_IND(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_vat_code       IN     VAT_CODES.VAT_CODE%TYPe,
                     O_nic_ind        IN OUT VAT_CODES.INCL_NIC_IND%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CALC_FINAL_EBC
-- Purpose:       Overloaded function, this function executes for a Schedule. It
--                Picks all the fiscal_doc_ids and then Line_ids
----------------------------------------------------------------------------------

FUNCTION CALC_FINAL_EBC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_schedule_no        IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CALC_FINAL_EBC
-- Purpose:       Calculates EBC and NIC costs for given Fiscal Doc Line Id
--
----------------------------------------------------------------------------------
FUNCTION CALC_FINAL_EBC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                        I_fiscal_doc_line_id IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                        I_pack_no            IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                        O_old_ebc            IN OUT FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                        O_old_nic            IN OUT FM_RECEIVING_DETAIL.NIC_COST%TYPE,
			                     O_new_ebc            IN OUT FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                        O_new_nic            IN OUT FM_RECEIVING_DETAIL.NIC_COST%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CALC_FINAL_CLT
-- Purpose:       This function inserts records into FM_CORRECTION_DOC and
--                FM_CORRECTION_TAX_DOC tables
----------------------------------------------------------------------------------
FUNCTION CALC_FINAL_CLT(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_fiscal_doc_id     IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_OBJ_TAX_VALUES
-- Purpose:       This function loops thru tax object table and returns tax values
--                for a given Tax code
----------------------------------------------------------------------------------
FUNCTION  GET_OBJ_TAX_VALUES(I_line_tax_obj_tbl     IN     "RIB_TaxDetRBO_TBL",
                             O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status               IN OUT INTEGER,
                             I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                             I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                             I_tax_code             IN     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE,
                             O_tax_value            IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE,
                             O_tax_base             IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE,
                             O_modified_tax_base    IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.MODIFIED_TAX_BASIS%TYPE,                             
                             O_tax_rate             IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_RATE%TYPE,
                             O_tax_rec_val          IN OUT FM_FISCAL_DOC_TAX_DETAIL_EXT.UNIT_REC_VALUE%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: CALC_FINAL_EBC
-- Purpose:       Calculates EBC and NIC costs for given Fiscal Doc Line Id for transfer
--
----------------------------------------------------------------------------------
FUNCTION CALC_FINAL_EBC_TSF(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                            I_pack_no            IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE,
                            O_old_ebc            IN OUT FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                            O_old_nic            IN OUT FM_RECEIVING_DETAIL.NIC_COST%TYPE,
                            O_new_ebc            IN OUT FM_RECEIVING_DETAIL.EBC_COST%TYPE,
                            O_new_nic            IN OUT FM_RECEIVING_DETAIL.NIC_COST%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
END FM_RECEIVING_SQL;
/