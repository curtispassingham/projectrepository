CREATE OR REPLACE PACKAGE BODY FM_LOC_FISCAL_NUMBER_SQL is
---------------------------------------------------------------------------------
-- Function Name: GET_NEXT_NUMBER
-- Purpose: This function gets the next fiscal document number avaliable for
--          a specific location.
--          It is called by the main function NEXT_NUMBER, because the GET_NEXT_NUMBER
--          function needs the current data available in the fiscal document and as
--          it is run in an autonomous session, all fiscal information necessary to
--          generate a new number will be passed as parameters by the main function.
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_NUMBER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_fiscal_doc_no      IN OUT FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE,
                         O_series_no          IN OUT FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE,
                         O_subseries_no       IN OUT FM_LOC_FISCAL_NUMBER.SUBSERIES_NO%TYPE,
                         I_location_type      IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                         I_location_id        IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                         I_issue_date         IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                         I_type_id            IN     FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_NUMBER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_fiscal_doc_no      IN OUT FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE,
                         O_series_no          IN OUT FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE,
                         O_subseries_no       IN OUT FM_LOC_FISCAL_NUMBER.SUBSERIES_NO%TYPE,
                         I_location_type      IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                         I_location_id        IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                         I_issue_date         IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                         I_type_id            IN     FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE)
   return BOOLEAN is
   ---
   PRAGMA          AUTONOMOUS_TRANSACTION;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program   VARCHAR2(80) := 'FM_LOC_FISCAL_NUMBER_SQL.GET_NEXT_NUMBER';
   ---
   L_series_no           FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE;
   L_subseries_no        FM_LOC_FISCAL_NUMBER.SUBSERIES_NO%TYPE;
   L_curr_fiscal_doc_no  FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE;
   L_hdr_fiscal_doc_no   FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE;
   L_next_fiscal_doc_no  FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE := NULL;
   L_fiscal_doc_no_max   FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO_MAX%TYPE;
   L_last_generated_date FM_LOC_FISCAL_NUMBER.LAST_GENERATED_DATE%TYPE;
   L_rowid               ROWID;
   ---
   cursor C_GET_NEXT_NUMBER_INFO(P_location_type  FM_LOC_FISCAL_NUMBER.LOCATION_TYPE%TYPE,
                                 P_location_id    FM_LOC_FISCAL_NUMBER.LOCATION_ID%TYPE,
                                 P_type_id        FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE
                                 ) is
      select lfn.series_no,
             lfn.subseries_no,
             lfn.fiscal_doc_no,
             lfn.fiscal_doc_no_max,
             lfn.last_generated_date,
             lfn.rowid
        from fm_loc_fiscal_number lfn
       where lfn.location_type = P_location_type
         and lfn.location_id   = P_location_id
         and ((lfn.type_id  = P_type_id 
         and P_type_id is NOT NULL)
          or (lfn.type_id  = -1 
         and not exists (select 1 
                           from fm_loc_fiscal_number lfn1
                          where lfn1.location_type = P_location_type
                            and lfn1.location_id   = P_location_id
                            and lfn1.type_id       = P_type_id
                            and  lfn1.last_generated_date <= sysdate)))
         and lfn.last_generated_date = (select MAX(last_generated_date) 
                                          from fm_loc_fiscal_number lfn2
                                         where  lfn.location_type = lfn2.location_type
                                           and  lfn.location_id   = lfn2.location_id
                                           and  lfn.type_id       = lfn2.type_id
                                           and  lfn2.last_generated_date <= sysdate)
         for update nowait;
   ---
   cursor C_COUNT_FISCAL_NUMBER(P_fiscal_doc_no  FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                                P_location_type  FM_LOC_FISCAL_NUMBER.LOCATION_TYPE%TYPE,
                                P_location_id    FM_LOC_FISCAL_NUMBER.LOCATION_ID%TYPE,
                                P_type_id        FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE,
                                P_series_no      FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE
                                ) is
      select fdh.fiscal_doc_no
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_no=P_fiscal_doc_no
         and fdh.location_type = P_location_type
         and fdh.location_id   = P_location_id
         and fdh.type_id       = P_type_id
         and fdh.series_no     = P_series_no;
   ---   
BEGIN
   ---
   -- Get the next fiscal document number for a specific location

   open C_GET_NEXT_NUMBER_INFO (I_location_type,
                                I_location_id,
                                I_type_id);
   fetch C_GET_NEXT_NUMBER_INFO into L_series_no,
                                     L_subseries_no,
                                     L_curr_fiscal_doc_no,
                                     L_fiscal_doc_no_max,
                                     L_last_generated_date,
                                     L_rowid;

   ---
   if C_GET_NEXT_NUMBER_INFO%NOTFOUND then
      ---
      if C_GET_NEXT_NUMBER_INFO%ISOPEN then
         close C_GET_NEXT_NUMBER_INFO;
      end if;
      ---
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_LOC_FIS_NO_NOT_EXIST',TO_CHAR(I_location_id),L_program,NULL);
      ---
      ROLLBACK;
      ---
      return FALSE;
      ---
   else
      ---
      L_hdr_fiscal_doc_no := L_curr_fiscal_doc_no;
      LOOP
         -- Check the fiscal document number exists or not in FM_FISCAL_DOC_HEADER table
         open C_COUNT_FISCAL_NUMBER (L_hdr_fiscal_doc_no,
                                     I_location_type,
                                     I_location_id,
                                     I_type_id,
                                     L_series_no);
         fetch C_COUNT_FISCAL_NUMBER into L_hdr_fiscal_doc_no;
         ---
         if C_COUNT_FISCAL_NUMBER%FOUND then
            L_hdr_fiscal_doc_no := L_hdr_fiscal_doc_no + 1;
            if C_COUNT_FISCAL_NUMBER%ISOPEN then
               close C_COUNT_FISCAL_NUMBER;
            end if;
         else
            ---
            if C_COUNT_FISCAL_NUMBER%ISOPEN then
               close C_COUNT_FISCAL_NUMBER;
            end if;
            exit;
            ----
         end if;
      END LOOP;  
      ---
   end if;
   ---
   close C_GET_NEXT_NUMBER_INFO;
   --When the fiscal doc number reaches the maximum allowed number
   -- for that location, then it display errors.
   if L_hdr_fiscal_doc_no > L_fiscal_doc_no_max then
      ---
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ORFM_MAX_LIMIT_REACHED',NULL,L_program,NULL);
      ---
      ROLLBACK;
      ---
      return FALSE;
   else
   ---
   --Updates the fm_loc_fiscal_number table with the just generated number
      if L_hdr_fiscal_doc_no = L_fiscal_doc_no_max then
         L_next_fiscal_doc_no := L_hdr_fiscal_doc_no;
      else
         L_next_fiscal_doc_no := L_hdr_fiscal_doc_no + 1;
      end if;
      update fm_loc_fiscal_number lfn
         set lfn.fiscal_doc_no = L_next_fiscal_doc_no,
             lfn.last_update_datetime = SYSDATE,
             lfn.last_update_id = USER
       where lfn.rowid = L_rowid;
   ---
   end if;   
   O_fiscal_doc_no := L_hdr_fiscal_doc_no;
   O_series_no := L_series_no;
   O_subseries_no := L_subseries_no;
   ---
   COMMIT;
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_LOC_FISCAL_NUMBER',
                                            'LOC_TYPE: ' || I_location_type
                                            || ' LOC_ID: ' || TO_CHAR(I_location_id),
                                            NULL);
      ---
      if C_GET_NEXT_NUMBER_INFO%ISOPEN then
         close C_GET_NEXT_NUMBER_INFO;
      end if;
      if C_COUNT_FISCAL_NUMBER%ISOPEN then
         close C_COUNT_FISCAL_NUMBER;
      end if;
      ---
      return FALSE;
      ---
   when OTHERS then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_GET_NEXT_NUMBER_INFO%ISOPEN then
         close C_GET_NEXT_NUMBER_INFO;
      end if;
      if C_COUNT_FISCAL_NUMBER%ISOPEN then
         close C_COUNT_FISCAL_NUMBER;
      end if;
      ---
      return FALSE;
      ---
END GET_NEXT_NUMBER;
----------------------------------------------------------------------------------
FUNCTION NEXT_NUMBER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_fiscal_doc_no      IN OUT FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE,
                     O_series_no          IN OUT FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE,
                     O_subseries_no       IN OUT FM_LOC_FISCAL_NUMBER.SUBSERIES_NO%TYPE,
                     I_location_type      IN     FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                     I_location_id        IN     FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                     I_issue_date         IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                     I_type_id            IN     FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(80) := 'FM_LOC_FISCAL_NUMBER_SQL.NEXT_NUMBER';
   ---
   L_series_no           FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE;
   L_subseries_no        FM_LOC_FISCAL_NUMBER.SUBSERIES_NO%TYPE;
   L_next_fiscal_doc_no  FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE;
   L_location_id         FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE;
   ---

   cursor C_GET_PHY_WH is
      select wh.physical_wh
        from wh wh
       where wh.wh = L_location_id;
   ---

BEGIN
   ---
   if I_location_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_location_id is NULL  then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_issue_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_issue_date',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
  L_location_id := I_location_id;

  if I_location_type ='W' then
   open C_GET_PHY_WH;
   fetch C_GET_PHY_WH into L_location_id;
   close C_GET_PHY_WH;
  end if;
      ---
   -- Get the next fiscal document number for a specific location
  if GET_NEXT_NUMBER(O_error_message,
                      L_next_fiscal_doc_no,
                      L_series_no,
                      L_subseries_no,
                      I_location_type,
                      I_location_id,
                      I_issue_date,
                      I_type_id) = FALSE then
      return FALSE;
   end if; 
   ---
   O_fiscal_doc_no := L_next_fiscal_doc_no;
   O_series_no := L_series_no;
   O_subseries_no := L_subseries_no;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));

      if C_GET_PHY_WH%ISOPEN then
         close C_GET_PHY_WH;
      end if;
      return FALSE;
END NEXT_NUMBER;
----------------------------------------------------------------------------------
FUNCTION CHK_LOC_FISCAL_NO(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists              IN OUT BOOLEAN,
                 I_location_type       IN     FM_LOC_FISCAL_NUMBER.LOCATION_TYPE%TYPE,
                 I_location_id         IN     FM_LOC_FISCAL_NUMBER.LOCATION_ID%TYPE,
                 I_series_no           IN     FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE,
                 I_type_id             IN     FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(80) := 'FM_LOC_FISCAL_NUMBER_SQL.CHK_LOC_FISCAL_NO';
   L_dummy     VARCHAR2(1);
   L_key       VARCHAR2(50);

   cursor C_GET_LOCATION is
      select '1'
        from fm_loc_fiscal_number
       where location_type       = I_location_type
         and location_id         = I_location_id
         and series_no           = I_series_no
         and type_id             = I_type_id;

BEGIN
   L_key := 'location_id = '||I_location_id;
   SQL_LIB.SET_MARK('OPEN', 'C_GET_LOCATION', 'FM_LOC_FISCAL_NUMBER', L_key);
   open C_GET_LOCATION;
   --
   SQL_LIB.SET_MARK('FETCH', 'C_GET_LOCATION', 'FM_LOC_FISCAL_NUMBER', L_key);
   fetch C_GET_LOCATION into L_dummy;
   --
   if C_GET_LOCATION%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   --
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_LOCATION', 'FM_LOC_FISCAL_NUMBER', L_key);
   close C_GET_LOCATION;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_GET_LOCATION%ISOPEN then
         close C_GET_LOCATION;
      end if;
      return FALSE;
END CHK_LOC_FISCAL_NO;
-----------------------------------------------------------------------------------
FUNCTION ROLLBACK_FISCAL_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_no  IN     FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE,
                            I_location_type  IN     FM_LOC_FISCAL_NUMBER.LOCATION_TYPE%TYPE,
                            I_location_id    IN     FM_LOC_FISCAL_NUMBER.LOCATION_ID%TYPE,
                            I_type_id        IN     FM_LOC_FISCAL_NUMBER.TYPE_ID%TYPE,
                            I_series_no      IN     FM_LOC_FISCAL_NUMBER.SERIES_NO%TYPE)
   return BOOLEAN is
   ---
   PRAGMA          AUTONOMOUS_TRANSACTION;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_program        VARCHAR2(80) := 'FM_LOC_FISCAL_NUMBER_SQL.ROLLBACK_FISCAL_NO';
   L_fiscal_doc_no  FM_LOC_FISCAL_NUMBER.FISCAL_DOC_NO%TYPE;
   L_key            VARCHAR2(150);
   L_rowid          ROWID;

   cursor C_GET_FISCAL_DOCNO is
      select rowid,
             CASE WHEN lfn.fiscal_doc_no < I_fiscal_doc_no THEN lfn.fiscal_doc_no
                  ELSE I_fiscal_doc_no
             END 
        from fm_loc_fiscal_number lfn
       where lfn.location_type = I_location_type
         and lfn.location_id   = I_location_id
         and ((lfn.type_id  = I_type_id 
         and I_type_id is NOT NULL)
          or (lfn.type_id  = -1 
         and not exists (select 1 
                           from fm_loc_fiscal_number lfn1
                          where lfn1.location_type = I_location_type
                            and lfn1.location_id   = I_location_id
                            and lfn1.type_id       = I_type_id)))
         and lfn.series_no     = I_series_no
         and lfn.last_generated_date = (select MAX(last_generated_date) 
                                          from fm_loc_fiscal_number lfn1
                                         where lfn1.location_type = lfn.location_type
                                           and lfn1.location_id   = lfn.location_id
                                           and lfn1.type_id       = lfn.type_id
                                           and lfn1.series_no     = lfn.series_no)
         for update nowait;
BEGIN
   open C_GET_FISCAL_DOCNO;
   --
   fetch C_GET_FISCAL_DOCNO into L_rowid,L_fiscal_doc_no;
   --
   if C_GET_FISCAL_DOCNO%FOUND then
      --Updates the fm_loc_fiscal_number table with the old fiscal doc number
      update fm_loc_fiscal_number lfn
        set lfn.fiscal_doc_no = L_fiscal_doc_no,
            lfn.last_update_datetime = SYSDATE,
            lfn.last_update_id = USER
       where lfn.rowid = L_rowid;
   end if;
   COMMIT;
   --
   close C_GET_FISCAL_DOCNO;
   --
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'FM_LOC_FISCAL_NUMBER',
                                            'LOC_TYPE: ' || I_location_type
                                            || ' LOC_ID: ' || TO_CHAR(I_location_id),
                                            NULL);
      if C_GET_FISCAL_DOCNO%ISOPEN then
         close C_GET_FISCAL_DOCNO;
      end if;
      return FALSE;
      ---
   when OTHERS then
      ROLLBACK;
      O_error_message := SQL_LIB.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      if C_GET_FISCAL_DOCNO%ISOPEN then
         close C_GET_FISCAL_DOCNO;
      end if;
      return FALSE;
END ROLLBACK_FISCAL_NO;
-----------------------------------------------------------------------------------
END FM_LOC_FISCAL_NUMBER_SQL;
/
