CREATE OR REPLACE PACKAGE BODY FM_VDATE_SQL IS

FUNCTION FM_GET_MAX_VDATE_ORFMI
   return DATE is
   ---
   L_date   DATE := null;
   ---
   L_error_message VARCHAR2(255);
   L_description   FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_num_days      FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value  FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value    FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   ---
BEGIN
   ---
   if (FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(L_error_message
                                        , L_description
                                        , L_num_days
                                        , L_string_value
                                        , L_date_value
                                        , 'NUMBER'
                                        , 'ORFMI_MAX_DAYS') = FALSE)
   then
      L_num_days := 0;
   end if;
   L_date := FM_GET_VDATE_ORFMI + NVL(L_num_days, 0);
   ---
   return L_date;
   ---
EXCEPTION
    -- ignore errors because a null will be returned
    when OTHERS then
       NULL;
END FM_GET_MAX_VDATE_ORFMI;
-------------------------------------------------------------------------------
FUNCTION FM_GET_MAX_VDATE_ORFMO
   return DATE is
   ---
   L_date   DATE := null;
   ---
   L_error_message VARCHAR2(255);
   L_description   FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_num_days      FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value  FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value    FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   ---
BEGIN
   ---
   if (FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(L_error_message
                                        , L_description
                                        , L_num_days
                                        , L_string_value
                                        , L_date_value
                                        , 'NUMBER'
                                        , 'ORFMO_MAX_DAYS') = FALSE)
   then
      L_num_days := 0;
   end if;
   L_date := FM_GET_VDATE_ORFMO + NVL(L_num_days, 0);
   ---
   return L_date;
   ---
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END FM_GET_MAX_VDATE_ORFMO;
-------------------------------------------------------------------------------
FUNCTION FM_GET_MIN_VDATE_ORFMI
   return DATE is
   ---
   L_date   DATE := null;
   ---
   L_error_message VARCHAR2(255);
   L_description   FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_num_days      FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value  FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value    FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   ---
BEGIN
   ---
   if (FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(L_error_message
                                        , L_description
                                        , L_num_days
                                        , L_string_value
                                        , L_date_value
                                        , 'NUMBER'
                                        , 'ORFMI_MIN_DAYS') = FALSE)
   then
      L_num_days := 0;
   end if;
   L_date := FM_GET_VDATE_ORFMI - NVL(L_num_days, 0);
   ---
   return L_date;
   ---
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END FM_GET_MIN_VDATE_ORFMI;
-------------------------------------------------------------------------------
FUNCTION FM_GET_MIN_VDATE_ORFMO
   return DATE is
   ---
   L_date   DATE := null;
   ---
   L_error_message VARCHAR2(255);
   L_description   FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_num_days      FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;
   L_string_value  FM_SYSTEM_OPTIONS.STRING_VALUE%TYPE;
   L_date_value    FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   ---
BEGIN
   ---
   if (FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(L_error_message
                                        , L_description
                                        , L_num_days
                                        , L_string_value
                                        , L_date_value
                                        , 'NUMBER'
                                        , 'ORFMO_MIN_DAYS') = FALSE)
   then
      L_num_days := 0;
   end if;
   L_date := FM_GET_VDATE_ORFMO - NVL(L_num_days, 0);
   ---
   return L_date;
   ---
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END FM_GET_MIN_VDATE_ORFMO;
-------------------------------------------------------------------------------
FUNCTION FM_GET_VDATE_ORFMI
   return DATE is
   ---
   L_vdate   DATE := null;
   ---
BEGIN
   ---
   L_vdate := TRUNC(SYSDATE);
   ---
   return L_vdate;
   ---
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END FM_GET_VDATE_ORFMI;
-------------------------------------------------------------------------------
FUNCTION FM_GET_VDATE_ORFMO
   return DATE is
   ---
   L_vdate   DATE := null;
   ---
BEGIN
   ---
   L_vdate := TRUNC(SYSDATE);
   ---
   return L_vdate;
   ---
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END FM_GET_VDATE_ORFMO;
-------------------------------------------------------------------------------
END FM_VDATE_SQL;
/