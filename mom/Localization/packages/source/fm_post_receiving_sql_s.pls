create or replace PACKAGE FM_POST_RECEIVING_SQL AS
--------------------------------------------------------------------------------
-- DESCRIPTION ? This function will be used for post processing of receipt in RFM.

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION PROCESS (O_error_message  IN OUT  VARCHAR2,
                  I_schedule       IN      FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION UPDATE_ST_HISTORY (O_error_message  IN OUT  VARCHAR2,
                            I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION RECEIVING_STATUS (O_error_message  IN OUT  VARCHAR2,
                           O_status         IN OUT  FM_RECEIVING_HEADER.STATUS%TYPE,
                           I_fiscal_doc_id  IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE )
return BOOLEAN;
--------------------------------------------------------------------------------  
END FM_POST_RECEIVING_SQL;
/
