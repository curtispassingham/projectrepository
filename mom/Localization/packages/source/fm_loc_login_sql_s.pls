CREATE OR REPLACE PACKAGE FM_LOC_LOGIN_SQL is
---------------------------------------------------------------------------------
-- CREATE DATE           - 19-04-2007
-- CREATE USER           ? JLD
-- PROJECT / PT / TRD    - 10481 / /001
-- DESCRIPTION           ? This package manipulate the form orfm_recv_loc_login.fmb
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
-- Function Name: LOV_STORE
-- Purpose:       This function returns the query for record_grup REC_STORE.
----------------------------------------------------------------------------------
FUNCTION LOV_STORE(O_error_message  IN OUT VARCHAR2,
                   O_query          IN OUT VARCHAR2,
                   I_where_clause   IN     VARCHAR2)

return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_WH
-- Purpose:       This function returns the query for record_grup REC_WH.
----------------------------------------------------------------------------------
FUNCTION LOV_WH(O_error_message  IN OUT VARCHAR2,
                O_query          IN OUT VARCHAR2,
                I_where_clause   IN     VARCHAR2)

return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_MULTI_WH
-- Purpose:       This function returns the query for record_grup REC_MULTI_WH.
----------------------------------------------------------------------------------
FUNCTION LOV_MULTI_WH(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2,
                      I_where_clause   IN     VARCHAR2)

return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_WH
-- Purpose:       This function checks if the wh is pysical_wh 
----------------------------------------------------------------------------------
FUNCTION VALIDATE_WH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_valid         IN OUT BOOLEAN,
                     O_wh            IN OUT V_WH%ROWTYPE,
                     I_wh            IN     V_WH.WH%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
END FM_LOC_LOGIN_SQL;
/