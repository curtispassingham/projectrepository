CREATE OR REPLACE PACKAGE BODY FM_FISCAL_HEADER_VAL_SQL is
-----------------------------------------------------------------------------------
FUNCTION GET_NEXT_FISCAL_DOC_ID(O_error_message  IN OUT VARCHAR2,
                                O_fiscal_doc_id  IN OUT FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.GET_NEXT_FISCAL_DOC_ID';
   ---
   cursor C_SEQ_FISCAL_DOC_ID is
      select fm_fiscal_doc_id_seq.NEXTVAL seq_no
        from dual;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SEQ_FISCAL_DOC_ID', 'SEQ_FISCAL_DOC_ID', NULL);
   open C_SEQ_FISCAL_DOC_ID;
   SQL_LIB.SET_MARK('FETCH', 'C_SEQ_FISCAL_DOC_ID', 'SEQ_FISCAL_DOC_ID',NULL);
   fetch C_SEQ_FISCAL_DOC_ID into O_fiscal_doc_id;
   SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_FISCAL_DOC_ID', 'SEQ_FISCAL_DOC_ID',NULL);
   close C_SEQ_FISCAL_DOC_ID;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_SEQ_FISCAL_DOC_ID%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_FISCAL_DOC_ID', 'SEQ_FISCAL_DOC_ID',NULL);
         close C_SEQ_FISCAL_DOC_ID;
      end if;
      ---
      return FALSE;

END GET_NEXT_FISCAL_DOC_ID;
-----------------------------------------------------------------------------------
FUNCTION LOV_TYPE_ID(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_utilization_id IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_TYPE_ID';
   ---
BEGIN
   ---
   O_query :=  'select fdt.type_desc type_desc, fdt.type_id type_id  '||
                'from fm_fiscal_doc_type fdt,fm_doc_type_utilization fdtu '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and fdt.type_id = fdtu.type_id  '||
                 'and fdtu.utilization_id = ''' || I_utilization_id ||'''  '||
              'union all '||
               'select NVL(tl_shadow.translated_value, fdt.type_desc) type_desc, fdt.type_id type_id  '||
                'from fm_fiscal_doc_type fdt,fm_doc_type_utilization fdtu, tl_shadow tl_shadow  '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and UPPER(fdt.type_id) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                 'and fdt.type_id = fdtu.type_id '||
                 'and fdtu.utilization_id = ''' || I_utilization_id ||''' '||
               'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_TYPE_ID;
-----------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER(O_error_message      IN OUT VARCHAR2,
                      O_query              IN OUT VARCHAR2,
                      I_supplier_sites_ind IN SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE Default 'N')

   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_SUPPLIER';
   L_status    VARCHAR2(3) := 'A';
   ---
BEGIN
   ---
if I_supplier_sites_ind = 'N' Then
   O_query := 'select a.sup_name,a.supplier, decode(b.taxpayer_type,''F'',b.cpf,b.cnpj) cpfcnpf '||
                'from sups a, v_br_sups b '||
               'where a.supplier = b.supplier '||
                 'and LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
              'union all '||
              'select NVL(tl_shadow.translated_value, a.sup_name) sup_name, a.supplier, decode(b.taxpayer_type,''F'',b.cpf,b.cnpj) cpfcnpf '||
                'from sups a, v_br_sups b, tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and a.supplier = b.supplier '||
                 'and UPPER(a.sup_name) = tl_shadow.key(+) '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                'order by 1';
 else
   O_query := 'select s.sup_name, s.supplier, decode(t.taxpayer_type,''F'',t.cpf,t.cnpj) cpfcnpf '||
                'from v_sups s , v_br_sups t  '||
               'where s.supplier_parent IS NOT NULL  '||
                 'and t.supplier = s.supplier  '||
                 'and to_char(s.sup_status) = '''||L_status||''' '||
                 'and LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE  '||
              'union all  '||
              'select NVL(tl_shadow.translated_value, s.sup_name) sup_name, s.supplier, decode(t.taxpayer_type,''F'',t.cpf,t.cnpj) cpfcnpf  '||
                'from v_sups s, v_br_sups t, tl_shadow tl_shadow  '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE  '||
                 'and s.supplier_parent IS NOT NULL '||
                 'and t.supplier = s.supplier  '||
                 'and to_char(s.sup_status) = '''||L_status||''' '||
                 'and UPPER(sup_name) = tl_shadow.key(+)  '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+)  '||
                'order by 1';
end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_SUPPLIER;
-----------------------------------------------------------------------------------
FUNCTION LOV_PARTNER(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_where_clause   IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_PARTNER';
   ---
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'select partner_desc, a.partner_id, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from partner a, v_br_partner b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.partner_id = b.partner_id '||
                    'and a.partner_type = b.partner_type '||
                    'and a.partner_type = '''||I_where_clause||''''||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, partner_desc) partner_desc, a.partner_id, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from partner a, v_br_partner b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.partner_id = b.partner_id '||
                    'and a.partner_type = b.partner_type '||
                    'and UPPER(a.partner_id) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and a.partner_type = '''||I_where_clause||''''||
                  'order by 1';
   else
      O_query := 'select partner_desc, a.partner_id, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from partner a, v_br_partner b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.partner_id = b.partner_id '||
                    'and a.partner_type = b.partner_type '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, partner_desc) partner_desc, a.partner_id, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from partner a, v_br_partner b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.partner_id = b.partner_id '||
                    'and a.partner_type = b.partner_type '||
                    'and UPPER(a.partner_id) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                  'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_PARTNER;
-----------------------------------------------------------------------------------
FUNCTION LOV_LOC(O_error_message  IN OUT VARCHAR2,
                 O_query          IN OUT VARCHAR2,
                 I_where_clause   IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_LOC';
   ---
BEGIN
   ---
   if I_where_clause = 'S' then
      O_query := 'select store_name loc_name, a.store loc, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from store a,  v_br_store b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.store = b.store '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, store_name) loc_name, a.store loc, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from store a,  v_br_store b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.store = b.store '||
                    'and UPPER(a.store) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                  'order by 1';
   elsif I_where_clause = 'W' then
      O_query := 'select wh_name loc_name, a.wh loc, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from wh a, v_br_wh b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.wh = b.wh '||
                  'union all '||
                 'select NVL(tl_shadow.translated_value, wh_name) loc_name, a.wh loc, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from wh a, v_br_wh b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.wh = b.wh '||
                    'and UPPER(a.wh) = tl_shadow.key(+) '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                   'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_LOC;
-----------------------------------------------------------------------------------
FUNCTION LOV_OUTLOC(O_error_message  IN OUT VARCHAR2,
                    O_query          IN OUT VARCHAR2,
                    I_where_clause   IN     VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_OUTLOC';
   ---
BEGIN
   ---
   if I_where_clause is NOT NULL then
      O_query := 'select outloc_desc, a.outloc_id, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from outloc a, v_br_outloc b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.outloc_type = '''||I_where_clause||''''||
                    'and a.outloc_type = b.outloc_type '||
                    'and a.outloc_id = b.outloc_id '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, outloc_desc) outloc_desc, a.outloc_id, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from outloc a, v_br_outloc b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(a.outloc_id) = tl_shadow.key(+) '||
                    'and a.outloc_type = b.outloc_type '||
                    'and a.outloc_id = b.outloc_id '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                    'and a.outloc_type = '''||I_where_clause||''''||
                   'order by 1';
   else
      O_query := 'select outloc_desc, a.outloc_id, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from outloc a, v_br_outloc b '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and a.outloc_type = b.outloc_type '||
                    'and a.outloc_id = b.outloc_id '||
                 'union all '||
                 'select NVL(tl_shadow.translated_value, outloc_desc) outloc_desc, a.outloc_id, decode(taxpayer_type,''F'',cpf,cnpj) cpfcnpf '||
                   'from outloc a, v_br_outloc b, tl_shadow tl_shadow '||
                  'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                    'and UPPER(a.outloc_id) = tl_shadow.key(+) '||
                    'and a.outloc_type = b.outloc_type '||
                    'and a.outloc_id = b.outloc_id '||
                    'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                   'order by 1';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_OUTLOC;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ENTITY(O_error_message  IN OUT VARCHAR2,
                         O_description    IN OUT VARCHAR2,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN is
   ---
   L_program      VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.VALIDATE_ENTITY';
   L_dummy        VARCHAR2(1);
   L_module       VARCHAR2(4) := 'CUST';
   L_key_value_2  VARCHAR2(1) := 'C';
   ---
   cursor C_FISCAL_ATTRIB is
      select 'X'
        from v_fiscal_attributes a
       where a.module = I_module
         and a.key_value_1 = I_key_value_1
         and a.key_value_2 = I_key_value_2
      union all
      select 'X'
        from fm_rma_head frh
       where I_module = L_module
         and frh.cust_id = I_key_value_1
         and I_key_value_2 = L_key_value_2;
   ---
BEGIN
   ---
   if I_module is NOT NULL and
      I_key_value_1 is NOT NULL then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2, 1, 120));
      open C_FISCAL_ATTRIB;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2, 1, 120));
      fetch C_FISCAL_ATTRIB into L_dummy;
      if C_FISCAL_ATTRIB%NOTFOUND then
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_FISCAL_ATTRIB',
                          'V_FISCAL_ATTRIBUTES',
                          SUBSTR('Module = '      || I_module ||
                          ' Key_value_1 = '|| I_key_value_1 ||
                          ' Key_value_2 = '|| I_key_value_2,1 , 120));
         close C_FISCAL_ATTRIB;
         ---
         if I_module = 'SUPP' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_SUP',NULL,NULL,NULL);
         elsif I_module = 'PTNR' then
            O_error_message := SQL_LIB.CREATE_MSG('PARTNER_INVALID',NULL,NULL,NULL);
         elsif I_module = 'LOC' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_LOC',NULL,NULL,NULL);
         elsif I_module = 'OLOC' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_OUTLOC',NULL,NULL,NULL);
         elsif I_module = 'CUST' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_CUST',NULL,NULL,NULL);
         end if;
         ---
         return FALSE;
         ---
      else
         if I_module = 'SUPP' then
            if VALIDATE_SUPPLIER(O_error_message,
                                 I_key_value_1) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| I_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2, 1, 120));
      close C_FISCAL_ATTRIB;
      ---
      if I_module = 'SUPP' then
         ---
         if SUPP_ATTRIB_SQL.GET_SUPP_DESC(O_error_message,
                                          I_key_value_1,
                                          O_description) = FALSE then
            return FALSE;
         end if;
         ---
      elsif I_module = 'PTNR' then
         ---
         if PARTNER_SQL.GET_DESC(O_error_message,
                                 O_description,
                                 I_key_value_1,
                                 I_key_value_2) = FALSE then
            return FALSE;
         end if;
         ---
      elsif I_module = 'LOC' then
         ---
         if LOCATION_ATTRIB_SQL.GET_NAME(O_error_message,
                                         O_description,
                                         I_key_value_1,
                                         I_key_value_2) = FALSE then
            return FALSE;
         end if;
         ---
      elsif I_module = 'OLOC' then
         ---
         if OUTSIDE_LOCATION_SQL.GET_DESC(O_error_message,
                                          O_description,
                                          I_key_value_1,
                                          I_key_value_2) = FALSE then
            return FALSE;
         end if;
         ---
      
      elsif I_module = 'CUST' then
         ---
         if FM_RMA_NF_CREATION_SQL.EXISTS_CUSTOMER(O_error_message,
                                                   O_description,
                                                   I_key_value_1) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   else
      O_description := NULL;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ENTITY;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE)
   return BOOLEAN is
   ---
   L_program             VARCHAR2(50) := 'FM_FISCAL_HEADER_VAL_SQL.VALIDATE_SUPPLIER';
   L_dummy               VARCHAR2(1);
   L_sup_status          VARCHAR2(1) := 'A';
   system_options_rec    SYSTEM_OPTIONS%ROWTYPE;  
   ---
   cursor C_CHECK_SUPPLIER_SITE is
      select 1
        from v_sups s , v_br_sups t  
       where s.supplier_parent IS NOT NULL  
         and t.supplier = s.supplier
         and s.supplier = I_key_value_1
         and to_char(s.sup_status) = L_sup_status; 
   ---
   cursor C_CHECK_SUPPLIER is
      select 1
        from v_sups s , v_br_sups t  
       where s.supplier_parent IS NULL  
         and t.supplier = s.supplier
         and s.supplier = I_key_value_1
         and to_char(s.sup_status) = L_sup_status;    
   ---
BEGIN
   ---
   if I_key_value_1 is NOT NULL then
      ---
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               system_options_rec) = FALSE then
         return FALSE;
      end if;
      ---
      If system_options_rec.supplier_sites_ind = 'Y' then
         ---
         open C_CHECK_SUPPLIER_SITE;
         fetch C_CHECK_SUPPLIER_SITE into L_dummy;
         if NVL(L_dummy,0) = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER_SITE',NULL,NULL,NULL);  
            return FALSE;          
         end if;
         close C_CHECK_SUPPLIER_SITE;
         ---
      else
         ---
         open C_CHECK_SUPPLIER;
         fetch C_CHECK_SUPPLIER into L_dummy;
         if NVL(L_dummy,0) = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('SUPP_SITE_NOT_ALLOW',NULL,NULL,NULL); 
            return FALSE;                    
         end if;
         close C_CHECK_SUPPLIER;
         ---
      end if;
      ---
   end if; 
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_SUPPLIER;
-----------------------------------------------------------------------------------
FUNCTION GET_ENTITY_INFO(O_error_message  IN OUT VARCHAR2,
                         O_cnpjcfp_type   IN OUT V_FISCAL_ATTRIBUTES.TYPE%TYPE,
                         O_cnpjcfp        IN OUT V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
                         O_addr_1         IN OUT V_FISCAL_ATTRIBUTES.ADDR_1%TYPE,
                         O_addr_2         IN OUT V_FISCAL_ATTRIBUTES.ADDR_2%TYPE,
                         O_addr_3         IN OUT V_FISCAL_ATTRIBUTES.ADDR_3%TYPE,
                         O_postal_code    IN OUT V_FISCAL_ATTRIBUTES.POSTAL_CODE%TYPE,
                         O_city           IN OUT V_FISCAL_ATTRIBUTES.CITY%TYPE,
                         O_district       IN OUT V_FISCAL_ATTRIBUTES.NEIGHBORHOOD%TYPE,
                         O_state          IN OUT V_FISCAL_ATTRIBUTES.STATE%TYPE,
                         I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                         I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.GET_ENTITY_INFO';
   L_cnpj      V_FISCAL_ATTRIBUTES.CNPJ%TYPE;
   L_cpf       V_FISCAL_ATTRIBUTES.CPF%TYPE;
   L_module    VARCHAR2(4) := 'CUST';
   L_key_value_2   VARCHAR2(1) := 'C';

   ---
   cursor C_FISCAL_ATTRIB is
      select a.type,
             a.cnpj,
             a.cpf,
             a.addr_1,
             a.addr_2,
             a.addr_3,
             a.postal_code,
             a.city,
             a.neighborhood,
             a.state
        from v_fiscal_attributes a
       where a.module = I_module
         and a.key_value_1 = I_key_value_1
         and a.key_value_2 = I_key_value_2
      union all
      select decode(a.cnpj,null,'F','J') as type,
             a.cnpj,
             a.cpf,
             a.addr_1,
             a.addr_2,
             a.addr_3,
             a.postal_code,
             a.city,
             a.neighborhood,
             a.state
        from fm_rma_head a
       where I_module = L_module
         and a.cust_id = I_key_value_1
         and I_key_value_2 = L_key_value_2;

   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_FISCAL_ATTRIB',
                    'V_FISCAL_ATTRIBUTES',
                    SUBSTR('Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1 ||
                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
   open C_FISCAL_ATTRIB;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_FISCAL_ATTRIB',
                    'V_FISCAL_ATTRIBUTES',
                    SUBSTR('Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1 ||
                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
   fetch C_FISCAL_ATTRIB into O_cnpjcfp_type,
                                          L_cnpj,
                                          L_cpf,
                                          O_addr_1,
                                          O_addr_2,
                                          O_addr_3,
                                          O_postal_code,
                                          O_city,
                                          O_district,
                                          O_state;
   if C_FISCAL_ATTRIB%FOUND then
      if O_cnpjcfp_type = 'J' then
         O_cnpjcfp := L_cnpj;
      elsif O_cnpjcfp_type = 'F' then
         O_cnpjcfp  := L_cpf;
      else
         O_cnpjcfp := NULL;
      end if;
   else
      O_cnpjcfp := NULL;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_FISCAL_ATTRIB',
                    'V_FISCAL_ATTRIBUTES',
                    SUBSTR('Module = '      || I_module ||
                    ' Key_value_1 = '|| I_key_value_1 ||
                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
   close C_FISCAL_ATTRIB;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ENTITY_INFO;
--------------------------------------------------------------------------------
--FUNCTION GET_CUST_INFO(O_error_message  IN OUT VARCHAR2,
--                       O_cnpjcfp_type   IN OUT V_FISCAL_ATTRIBUTES.TYPE%TYPE,
--                       O_cnpjcfp        IN OUT V_FISCAL_ATTRIBUTES.CNPJ%TYPE,
--                       O_addr_1         IN OUT FM_RMA_HEAD.ADDR_1%TYPE,
--                       O_addr_2         IN OUT FM_RMA_HEAD.ADDR_2%TYPE,
--                       O_addr_3         IN OUT FM_RMA_HEAD.ADDR_3%TYPE,
--                       O_postal_code    IN OUT FM_RMA_HEAD.POSTAL_CODE%TYPE,
--                       O_city           IN OUT FM_RMA_HEAD.CITY%TYPE,
--                       O_district       IN OUT FM_RMA_HEAD.NEIGHBORHOOD%TYPE,
--                       O_state          IN OUT FM_RMA_HEAD.STATE%TYPE,
--                       I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
--                       I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
--                       I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE)
--   return BOOLEAN is
--   ---
--   L_program   VARCHAR2(150) := 'ORFM_FISCAL_HEADER_SQL.GET_CUST_INFO';
--   L_cnpj      V_FISCAL_ATTRIBUTES.CNPJ%TYPE:=NULL;
--   L_cpf       V_FISCAL_ATTRIBUTES.CPF%TYPE :=NULL;
--   ---
--   cursor C_CUST_ATTRIB is
--      select 'F' as type,
--             a.cnpj,
--             a.cpf,
--             a.addr_1,
--             a.addr_2,
--             a.addr_3,
--             a.postal_code,
--             a.city,
--             a.neighborhood,
--             a.state
--        from fm_rma_head a
--       where I_module = 'CUST'
--         and a.cust_id = I_key_value_1
--         and I_key_value_2 = 'C';
--   ---
--BEGIN
--   ---
--
--   insert into temp values('2',L_cnpj || ' 3' || L_cpf );
--   SQL_LIB.SET_MARK('OPEN',
--                    'C_CUST_ATTRIB',
--                    'FM_RMA_HEAD',
--                    SUBSTR('Module = '      || I_module ||
--                    ' Key_value_1 = '|| I_key_value_1 ||
--                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
--   open C_CUST_ATTRIB;
--   ---
--
--                                          --insert into temp values('2','came');
--   SQL_LIB.SET_MARK('FETCH',
--                    'C_CUST_ATTRIB',
--                    'FM_RMA_HEAD',
--                    SUBSTR('Module = '      || I_module ||
--                    ' Key_value_1 = '|| I_key_value_1 ||
--                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
--
--   fetch C_CUST_ATTRIB into O_cnpjcfp_type,
--                                          L_cnpj,
--                                          L_cpf,
--                                          O_addr_1,
--                                          O_addr_2,
--                                          O_addr_3,
--                                          O_postal_code,
--                                          O_city,
--                                          O_district,
--                                          O_state
--;
--   if C_CUST_ATTRIB%FOUND then
--      if L_cnpj is not NULL then
--         O_cnpjcfp := L_cnpj;
--      else
--         O_cnpjcfp := L_cpf;
--      end if;
--   else
--      O_cnpjcfp := NULL;
--   end if;
--insert into temp values('2',L_cnpj || ' 3' || L_cpf );
--   ---
--   SQL_LIB.SET_MARK('CLOSE',
--                    'C_CUST_ATTRIB',
--                    'FM_RMA_HEAD',
--                    SUBSTR('Module = '      || I_module ||
--                    ' Key_value_1 = '|| I_key_value_1 ||
--                    ' Key_value_2 = '|| I_key_value_2, 1, 120));
--   close C_CUST_ATTRIB;
--   ---
--   return TRUE;
--   ---
--EXCEPTION
--   when OTHERS then
--      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
--                                            SQLERRM,
--                                            L_program,
--                                            TO_CHAR(SQLCODE));
--      return FALSE;
--END GET_CUST_INFO;
-----------------------------------------------------------------------------------
FUNCTION GET_CITY_DESC(O_error_message  IN OUT VARCHAR2,
                       O_city_desc      IN OUT COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                       I_city           IN     COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.GET_CITY_DESC';
   L_country_id VARCHAR2(10) := 'BR';
   ---
   cursor C_CITY is
      select a.jurisdiction_desc
        from country_tax_jurisdiction a
       where a.jurisdiction_code= I_city
        and  a.country_id = L_country_id;

   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
			  'C_CITY',
                    'COUNTRY_TAX_JURISDICTION',
                    'JURISDICTION_CODE= ' || I_city || 'COUNTRY_ID= ' || '''BR''');

   open C_CITY;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CITY',
                    'COUNTRY_TAX_JURISDICTION',
                    'JURISDICTION_CODE= ' || I_city || 'COUNTRY_ID= ' || '''BR''');

   fetch C_CITY into O_city_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CITY',
                    'COUNTRY_TAX_JURISDICTION',
                    'JURISDICTION_CODE= ' || I_city || 'COUNTRY_ID= ' || '''BR''');

   close C_CITY;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CITY_DESC;
-----------------------------------------------------------------------------------
FUNCTION LOV_CNPJCPF(O_error_message  IN OUT VARCHAR2,
                     O_query          IN OUT VARCHAR2,
                     I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                     I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                     I_key_value_1    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                     I_cnpfcpf        IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_CNPJCPF';
   ---
BEGIN
   ---
   if I_module = 'SUPP' then
      ---
      O_query := 'select a.cnpj cnpjcpf, NULL TYPE, c.code_desc type_desc, s.supplier id_name, s.sup_name name '||
                   'from v_fiscal_attributes a, sups s, code_detail c '||
                  'where s.supplier = a.key_value_1 '||
                    'and c.code_type = ''FIMO'' '||
                    'and c.code = ''SUPP'' '||
                    'and module = '''||I_module||''' '||
                    'and cnpj is NOT NULL ';
      if I_cnpfcpf is NOT NULL then
         O_query := O_query || 'and cnpj = '''||I_cnpfcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query  ||
                  'union '||
                 'select a.cpf cnpjcpf, NULL TYPE, c.code_desc type_desc, s.supplier id_name, s.sup_name name '||
                   'from v_fiscal_attributes a, sups s, code_detail c '||
                  'where s.supplier = a.key_value_1 '||
                    'and c.code_type = ''FIMO'' '||
                    'and c.code = ''SUPP'' '||
                    'and module = '''||I_module||''''||
                    'and cpf is NOT NULL ';
      if I_cnpfcpf is NOT NULL then
         O_query := O_query || 'and cpf = '''||I_cnpfcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query ||'order by 1';
      ---
   elsif I_module = 'PTNR' then
      ---
      O_query := 'select a.cnpj cnpjcpf, c.code TYPE, c.code_desc type_desc, p.partner_id id_name, p.partner_desc name '||
                   'from v_fiscal_attributes a, partner p, code_detail c '||
                  'where p.partner_id = a.key_value_1 '||
                    'and p.partner_type = a.key_value_2 '||
                    'and c.code_type = ''PTAL'' '||
                    'and c.code = a.key_value_2 '||
                    'and module = '''||I_module||''' '||
                    'and cnpj is NOT NULL ';
      if I_cnpfcpf is NOT NULL then
         O_query := O_query || 'and cnpj = '''||I_cnpfcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query  ||
                  'union '||
                 'select a.cpf cnpjcpf, c.code  TYPE, c.code_desc type_desc, p.partner_id id_name, p.partner_desc name '||
                   'from v_fiscal_attributes a, partner p, code_detail c '||
                  'where p.partner_id = a.key_value_1 '||
                    'and p.partner_type = a.key_value_2 '||
                    'and c.code_type = ''PTAL'' '||
                    'and c.code = a.key_value_2 '||
                    'and module = '''||I_module||''' '||
                    'and cpf is NOT NULL ';
      if I_cnpfcpf is NOT NULL then
         O_query := O_query || 'and cpf = '''||I_cnpfcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query ||'order by 1';
      ---
   elsif I_module = 'LOC' then
      ---
      if I_key_value_2 = 'S' then
         O_query := 'select a.cnpj cnpjcpf, c.code  TYPE, c.code_desc type_desc, s.store id_name, s.store_name name '||
                      'from v_fiscal_attributes a, store s, code_detail c '||
                     'where s.store = a.key_value_1 '||
                       'and c.code_type = ''LOC4'' '||
                       'and c.code = a.key_value_2 '||
                       'and module = '''||I_module||''' '||
                       'and cnpj is NOT NULL '||
                       'and key_value_2 = '''||I_key_value_2||''' ';
         if I_cnpfcpf is NOT NULL then
            O_query := O_query || 'and cnpj = '''||I_cnpfcpf||''' ';
         end if;
         if I_key_value_1 is NOT NULL then
            O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
         end if;
         O_query := O_query  ||
                     'union '||
                    'select a.cpf cnpjcpf, c.code  TYPE, c.code_desc type_desc, s.store id_name, s.store_name name '||
                      'from v_fiscal_attributes a, store s, code_detail c '||
                     'where s.store = a.key_value_1 '||
                       'and c.code_type = ''LOC4'' '||
                       'and c.code = a.key_value_2 '||
                       'and module = '''||I_module||''' '||
                       'and cpf is NOT NULL '||
                       'and key_value_2 = '''||I_key_value_2||''' ';
         if I_cnpfcpf is NOT NULL then
            O_query := O_query || 'and cpf = '''||I_cnpfcpf||''' ';
         end if;
         if I_key_value_1 is NOT NULL then
            O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
         end if;
         O_query := O_query ||'order by 1';
         ---
      elsif  I_key_value_2 = 'W' then
         ---
         O_query := 'select a.cnpj cnpjcpf, c.code  TYPE, c.code_desc type_desc, w.wh id_name, w.wh_name name '||
                      'from v_fiscal_attributes a, wh w, code_detail c '||
                     'where w.wh = a.key_value_1 '||
                       'and c.code_type = ''LOC4'' '||
                       'and c.code = a.key_value_2 '||
                       'and module = '''||I_module||''' '||
                       'and cnpj is NOT NULL '||
                       'and key_value_2 = '''||I_key_value_2||''' ';
         if I_cnpfcpf is NOT NULL then
            O_query := O_query || 'and cnpj = '''||I_cnpfcpf||''' ';
         end if;
         if I_key_value_1 is NOT NULL then
            O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
         end if;
         O_query := O_query  ||
                     'union '||
                    'select a.cpf cnpjcpf, c.code  TYPE, c.code_desc type_desc, w.wh id_name, w.wh_name name '||
                      'from v_fiscal_attributes a, wh w, code_detail c '||
                     'where w.wh = a.key_value_1 '||
                       'and c.code_type = ''LOC4'' '||
                       'and c.code = a.key_value_2 '||
                       'and module = '''||I_module||''' '||
                       'and cpf is NOT NULL '||
                       'and key_value_2 = '''||I_key_value_2||''' ';
         if I_cnpfcpf is NOT NULL then
            O_query := O_query || 'and cpf = '''||I_cnpfcpf||''' ';
         end if;
         if I_key_value_1 is NOT NULL then
            O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
         end if;
         O_query := O_query ||'order by 1';
         ---
      end if;
      ---
   elsif I_module = 'OLOC' then
      ---
      O_query := 'select a.cnpj cnpjcpf, c.code  TYPE, c.code_desc type_desc, o.outloc_id id_name, o.outloc_desc name '||
                   'from v_fiscal_attributes a, outloc o, code_detail c '||
                  'where o.outloc_id = a.key_value_1 '||
                    'and o.outloc_type = a.key_value_2 '||
                    'and c.code_type = ''LOCT'' '||
                    'and c.code = a.key_value_2 '||
                    'and module = '''||I_module||''' '||
                    'and cnpj is NOT NULL ';
      if I_cnpfcpf is NOT NULL then
         O_query := O_query || 'and cnpj = '''||I_cnpfcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query  ||
                  'union '||
                 'select a.cpf cnpjcpf, c.code  TYPE, c.code_desc type_desc, o.outloc_id id_name, o.outloc_desc name '||
                   'from v_fiscal_attributes a, outloc o, code_detail c '||
                  'where o.outloc_id = a.key_value_1 '||
                    'and o.outloc_type = a.key_value_2 '||
                    'and c.code_type = ''LOCT'' '||
                    'and c.code = a.key_value_2 '||
                    'and module = '''||I_module||''' '||
                    'and cpf is NOT NULL ';
      if I_cnpfcpf is NOT NULL then
         O_query := O_query || 'and cpf = '''||I_cnpfcpf||''' ';
      end if;
      if I_key_value_2 is NOT NULL then
         O_query := O_query || 'and key_value_2 = '''||I_key_value_2||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and key_value_1 = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query ||'order by 1';
      ---
         ---
   elsif I_module = 'CUST' then
      ---
       O_query := 'select h.cnpj cnpjcpf, NULL TYPE, c.code_desc type_desc, h.cust_id id_name, h.cust_name name '||
                   'from fm_rma_head h, code_detail c '||
                  'where c.code_type = ''FIMO'' '||
                    'and c.code = ''CUST'' '||
                    'and h.cnpj is NOT NULL ';
      if I_cnpfcpf is NOT NULL then
         O_query := O_query || 'and h.cnpj = '''||I_cnpfcpf||''' ';
      end if;

      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and h.cust_id = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query  ||
                  'union '||
                 'select h.cpf cnpjcpf, NULL TYPE, c.code_desc type_desc, h.cust_id id_name, h.cust_name name '||
                   'from fm_rma_head h, code_detail c '||
                  'where c.code_type = ''FIMO'' '||
                    'and c.code = ''CUST'' '||
                    'and h.cpf is NOT NULL ';
      if I_cnpfcpf is NOT NULL then
         O_query := O_query || 'and h.cpf = '''||I_cnpfcpf||''' ';
      end if;
      if I_key_value_1 is NOT NULL then
         O_query := O_query || 'and h.cust_id = '''||I_key_value_1||''' ';
      end if;
      O_query := O_query ||'order by 1';
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_CNPJCPF;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_CNPJCPF(O_error_message  IN OUT VARCHAR2,
                          O_unique         IN OUT BOOLEAN,
                          O_tipo_cnpjcpf   IN OUT VARCHAR2,
                          O_key_value_1    IN OUT V_FISCAL_ATTRIBUTES.KEY_VALUE_1%TYPE,
                          I_module         IN     V_FISCAL_ATTRIBUTES.MODULE%TYPE,
                          I_key_value_2    IN     V_FISCAL_ATTRIBUTES.KEY_VALUE_2%TYPE,
                          I_cnpjcpf        IN     V_FISCAL_ATTRIBUTES.CNPJ%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.VALIDATE_CNPJCPF';
   L_count     NUMBER;
   L_key_value_2  VARCHAR2(1) := 'C';
   L_module       VARCHAR2(10) := 'CUST';
   ---
   cursor C_COUNT_CNPFCPF is
      select COUNT(*)
        from ((select 'J' tipo, a.cnpj cnpjcpf
                from v_fiscal_attributes a
               where a.module = I_module
                 and ((O_key_value_1 is NOT NULL
                 and  a.key_value_1 = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and a.key_value_2 = I_key_value_2
                 and a.cnpj = I_cnpjcpf
               union all
              select 'F' tipo, a.cpf cnpjcpf
                from v_fiscal_attributes a
               where a.module = I_module
                 and ((O_key_value_1 is NOT NULL
                 and  a.key_value_1 = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and a.key_value_2 = I_key_value_2
                 and a.cpf = I_cnpjcpf)
              union all
              (select 'J' tipo, a.cnpj cnpjcpf
                from fm_rma_head a
               where I_module = L_module
                 and ((O_key_value_1 is NOT NULL
                 and  a.cust_id = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and I_key_value_2 = L_key_value_2
                 and a.cnpj = I_cnpjcpf
               union all
              select 'F' tipo, a.cpf cnpjcpf
                from fm_rma_head a
               where I_module =  L_module
                 and ((O_key_value_1 is NOT NULL
                 and  a.cust_id = O_key_value_1)
                  or O_key_value_1 is NULL)
                 and I_key_value_2 = L_key_value_2
                 and a.cpf = I_cnpjcpf)) a;
   ---
   cursor C_FISCAL_ATTRIB is
      select 'J' tipo, a.key_value_1
        from v_fiscal_attributes a
       where a.module = I_module
         and ((O_key_value_1 is NOT NULL
         and  a.key_value_1 = O_key_value_1)
          or O_key_value_1 is NULL)
         and a.key_value_2 = I_key_value_2
         and a.cnpj = I_cnpjcpf
       union
      select 'F' tipo, a.key_value_1
        from v_fiscal_attributes a
       where a.module = I_module
         and ((O_key_value_1 is NOT NULL
         and  a.key_value_1 = O_key_value_1)
          or O_key_value_1 is NULL)
         and a.key_value_2 = I_key_value_2
         and a.cpf = I_cnpjcpf
      union all
      select 'J' tipo, a.cust_id
        from fm_rma_head a
       where I_module = L_module
         and ((O_key_value_1 is NOT NULL
         and  a.cust_id = O_key_value_1)
          or O_key_value_1 is NULL)
         and I_key_value_2 = L_key_value_2
         and a.cnpj = I_cnpjcpf
       union
      select 'F' tipo, a.cust_id
        from fm_rma_head a
       where I_module = L_module
         and ((O_key_value_1 is NOT NULL
         and  a.cust_id = O_key_value_1)
          or O_key_value_1 is NULL)
         and I_key_value_2 = L_key_value_2
         and a.cpf = I_cnpjcpf;
   ---
BEGIN
   ---
   if I_module is NOT NULL and
      I_cnpjcpf is NOT NULL then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_COUNT_CNPFCPF',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf, 1, 120));
      open C_COUNT_CNPFCPF;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_COUNT_CNPFCPF',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf, 1, 120));
      fetch C_COUNT_CNPFCPF into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_COUNT_CNPFCPF',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf, 1, 120));
      close C_COUNT_CNPFCPF;
      ---
      if L_count is NOT NULL and L_count > 1 then
         O_unique := FALSE;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_FISCAL_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf, 1, 120));
      open C_FISCAL_ATTRIB;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_FISCAL_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf, 1, 120));
      fetch C_FISCAL_ATTRIB into O_tipo_cnpjcpf, O_key_value_1;
      if C_FISCAL_ATTRIB%NOTFOUND then
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_FISCAL_ATTRIB',
                          'V_FISCAL_ATTRIBUTES',
                          SUBSTR('Module = '      || I_module ||
                          ' Key_value_1 = '|| O_key_value_1 ||
                          ' Key_value_2 = '|| I_key_value_2 ||
                          ' CNPJ/CPF = '   || I_cnpjcpf, 1, 120));
         close C_FISCAL_ATTRIB;
         ---
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_CNPJ_CPF',NULL,NULL,NULL);
         ---
         return FALSE;
         ---
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FISCAL_ATTRIB',
                       'V_FISCAL_ATTRIBUTES',
                       SUBSTR('Module = '      || I_module ||
                       ' Key_value_1 = '|| O_key_value_1 ||
                       ' Key_value_2 = '|| I_key_value_2 ||
                       ' CNPJ/CPF = '   || I_cnpjcpf, 1, 120));
      close C_FISCAL_ATTRIB;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_CNPJCPF;
-----------------------------------------------------------------------------------
FUNCTION LOV_CUSTOMER(O_error_message  IN OUT VARCHAR2,
                      O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_CUSTOMER';
   ---
BEGIN
   ---
   O_query := 'select cust_name, cust_id, decode(cpf,null,cnpj,cpf) cpfcnpf '||
                'from fm_rma_head '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
              'union all '||
              'select NVL(tl_shadow.translated_value, cust_name) cust_name, cust_id, decode(cpf,null,cnpj,cpf) cpfcnpf '||
                'from fm_rma_head, tl_shadow tl_shadow '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
                'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_CUSTOMER;
-----------------------------------------------------------------------------------
FUNCTION LOV_UTILIZATION(O_error_message    IN OUT VARCHAR2,
                         O_query            IN OUT VARCHAR2,
                         I_requisition_type IN     FM_FISCAL_UTILIZATION.REQUISITION_TYPE%TYPE,
                         I_utilization_id   IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE,
                         I_mode_type        IN     FM_FISCAL_UTILIZATION.MODE_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_UTILIZATION';
   L_status    VARCHAR2(1)  := 'A';
   ---
BEGIN
   ---
   O_query := 'select utilization_id,utilization_desc '||
                'from fm_fiscal_utilization '||
               'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 'and requisition_type = ''' || I_requisition_type ||''' '||
                 'and status = '''||L_status||''' ';
   ---
   if I_utilization_id is NOT NULL then
      O_query := O_query ||
                 'and utilization_id = ''' || I_utilization_id ||''' ';
   end if;
   ---
   if I_mode_type is NOT NULL then
      O_query := O_query ||
                 'and mode_type = ''' || I_mode_type ||''' ';
   end if;
   ---
   O_query := O_query ||
                 'union all '||
                 'select ffu.utilization_id utilization_id,
                         NVL(tl1.translated_value,ffu.utilization_desc) utilization_desc '||
                 '  from fm_fiscal_utilization ffu, tl_shadow tl1 '||
                 ' where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
                 '  and UPPER(ffu.utilization_desc) = tl1.key(+) '||
                 '  and LANGUAGE_SQL.GET_USER_LANGUAGE = tl1.lang(+) '||
                 '  and ffu.requisition_type = ''' || I_requisition_type ||''' '||
                 '  and ffu.status = '''||L_status||''' ';
   ---
   if I_utilization_id is NOT NULL then
      O_query := O_query ||
                 'and ffu.utilization_id = ''' || I_utilization_id ||''' ';
   end if;
   ---
   if I_mode_type is NOT NULL then
      O_query := O_query ||
                 'and ffu.mode_type = ''' || I_mode_type ||''' ';
   end if;
   ---
   O_query := O_query ||
                 'order by 1 ';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_UTILIZATION;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_CST (O_error_message    IN OUT VARCHAR2,
                       O_exist           IN OUT BOOLEAN,
                       I_string_value     IN VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_HEADER_VAL_SQL.VALIDATE_CST';
   L_count     NUMBER;
   L_variable  VARCHAR2(30) := 'RECOVERABLE_TAX_CST';

   ---
    cursor C_COUNT_CST is
      select instr(string_value,I_string_value)
      from fm_system_options
      where variable = L_variable;
   ---
BEGIN
   ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_COUNT_CST',
                       'FM_SYSTEM_OPTIONS',
                       'variable  = '||'''RECOVERABLE_TAX_CST''');
      open C_COUNT_CST;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_COUNT_CST',
                       'FM_SYSTEM_OPTIONS',
                       'variable  = '||'''RECOVERABLE_TAX_CST''');

      fetch C_COUNT_CST into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_COUNT_CST',
                       'FM_SYSTEM_OPTIONS',
                        'variable  = '||'''RECOVERABLE_TAX_CST''');


      close C_COUNT_CST;
      ---
      if NVL(L_count,0) > 0 then
         O_exist := TRUE;
      else
        O_exist  := FALSE;
      end if;

      return TRUE;
     ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_CST ;
---------------------------------------------------------------------------------------------------
FUNCTION LOV_PLATE_STATE(O_error_message  IN OUT VARCHAR2,
                         O_query          IN OUT VARCHAR2)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.LOV_PLATE_STATE';
   ---
BEGIN
   ---
   O_query := 'select description,state '||
              'from state '||
              'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE = LANGUAGE_SQL.GET_USER_LANGUAGE '||
              'and country_id in (select string_value from fm_system_options where variable = ''DEFAULT_COUNTRY'')  '||
              'union all '||
              'select NVL(tl_shadow.translated_value,description) description,state '||
              'from state,tl_shadow tl_shadow '||
              'where LANGUAGE_SQL.GET_PRIMARY_LANGUAGE != LANGUAGE_SQL.GET_USER_LANGUAGE '||
              'and LANGUAGE_SQL.GET_USER_LANGUAGE = tl_shadow.lang(+) '||
              'order by 1';
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOV_PLATE_STATE;
-----------------------------------------------------------------------------------
FUNCTION GET_STATE_DESC(O_error_message  IN OUT VARCHAR2,
                        O_description    IN OUT STATE.DESCRIPTION%TYPE,
                        I_state          IN     STATE.STATE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'ORFM_FISCAL_HEADER_SQL.GET_STATE_DESC';
   ---
   cursor C_STATE is
      select description
        from state
       where state = I_state;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_STATE',
                    'STATE',
                    'State = ' || I_state);
   open C_STATE;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_STATE',
                    'STATE',
                    'State = ' || I_state);
   fetch C_STATE into O_description;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_STATE',
                    'STATE',
                    'State = ' || I_state);
   close C_STATE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_STATE_DESC;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PLATE_STATE (O_error_message   IN OUT VARCHAR2,
                               O_exist           IN OUT BOOLEAN,
                               I_state           IN STATE.STATE%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_HEADER_VAL_SQL.VALIDATE_PLATE_STATE';
   L_key       VARCHAR2(500) := 'select string_value from fm_system_options where variable = ''DEFAULT_COUNTRY''';
   L_count     NUMBER;
   L_variable  VARCHAR2(50) := 'DEFAULT_COUNTRY';
   ---
    cursor C_COUNT_PLATE_STATE is
      select COUNT(1)
      from state
      where state = I_state
      and country_id in (select string_value from fm_system_options where variable = L_variable);
   ---
BEGIN
   ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_COUNT_PLATE_STATE',
                       'STATE',
                        L_key);
      open C_COUNT_PLATE_STATE;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_COUNT_PLATE_STATE',
                       'STATE',
                        L_key);

      fetch C_COUNT_PLATE_STATE into L_count;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_COUNT_PLATE_STATE',
                       'STATE',
                        L_key);

      close C_COUNT_PLATE_STATE;
      ---
      if NVL(L_count,0) > 0 then
         O_exist := TRUE;
      else
        O_exist  := FALSE;
      end if;

      return TRUE;
     ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_PLATE_STATE;
-----------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COMPL_NF (O_error_message   IN OUT VARCHAR2,
                            O_exist           IN OUT BOOLEAN,
                            I_utilization_id  IN     FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE,
                            I_Schedule_no     IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_HEADER_VAL_SQL.VALIDATE_COMPL_NF';
   L_key       VARCHAR2(500) := 'Schedule number :'||I_Schedule_no;
   L_table     VARCHAR2(100) := 'FM_FISCAL_DOC_HEADER';
   L_count     NUMBER;
   L_count_compl     NUMBER;
   L_utilization_attributes     FM_UTILIZATION_ATTRIBUTES%ROWTYPE;
   L_comp_freight_nf_ind  VARCHAR2(1) := 'Y';
   ---
    cursor C_COUNT_NF is
      select COUNT(1)
        from fm_fiscal_doc_header
       where schedule_no = I_Schedule_no;
   ---
    cursor C_GET_UTIL_ID is
      select COUNT(fdh.utilization_id)
        from fm_fiscal_doc_header fdh,
             fm_utilization_attributes fua,
             fm_fiscal_doc_complement fdc
       where fdh.schedule_no = I_Schedule_no
         and fdh.utilization_id = fua.utilization_id
         and fua.comp_freight_nf_ind = L_comp_freight_nf_ind
         and fdh.utilization_id <> I_utilization_id
          and fdh.fiscal_doc_id = fdc.compl_fiscal_doc_id;
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_COUNT_NF', L_table, L_key);
   open C_COUNT_NF;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_COUNT_NF', L_table, L_key);
   fetch C_COUNT_NF into L_count;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_NF',  L_table, L_key);
   close C_COUNT_NF;
   ---

   SQL_LIB.SET_MARK('OPEN','C_GET_UTIL_ID', L_table, L_key);
   open C_GET_UTIL_ID;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_UTIL_ID', L_table, L_key);
   fetch C_GET_UTIL_ID into L_count_compl;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_UTIL_ID',  L_table, L_key);
   close C_GET_UTIL_ID;
   ---
   if FM_UTILIZATION_ATTRIBUTES_SQL.GET_UTILIZATION_ATTRIB_INFO(O_error_message,
                                                                L_utilization_attributes,
                                                                I_utilization_id) = FALSE then
      return FALSE;
   end if;
   ---
   ---
   if NVL(L_count,0) >= 1 and L_utilization_attributes.comp_freight_nf_ind = 'Y' and NVL(L_count_compl, 0) > 0 then --sing NF change correct
       O_exist := TRUE;
   elsif NVL(L_count,0) >= 1 and L_utilization_attributes.comp_freight_nf_ind = 'N' and NVL(L_count_compl, 0) > 1 then
      O_exist  := TRUE;
   --els
   /*if NVL(L_count,0) >= 1 and L_utilization_attributes.comp_freight_nf_ind = 'N' and NVL(L_count_compl, 0) = 1 then --1st as ind "Y" and  2nd nf as ind "N"
      O_exist  := TRUE;
   elsif NVL(L_count,0) >= 1 and L_utilization_attributes.comp_freight_nf_ind = 'Y' and NVL(L_count_compl, 0) = 0 then --1st as ind "N" and 2nd nf as ind "Y"
      O_exist  := TRUE;*/
   else
      O_exist := FALSE;
   end if;
   ---

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      ---
      if C_COUNT_NF%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_NF',  L_table, L_key);
         close C_COUNT_NF;
      end if;
      ---
      if C_GET_UTIL_ID%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_UTIL_ID',  L_table, L_key);
         close C_GET_UTIL_ID;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_COMPL_NF;
-----------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MAIN_NF_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        OUT    BOOLEAN,
                             I_fiscal_doc_id IN     FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program       VARCHAR2(100) := 'FM_COMPL_NF_APPROVE_SQL.CHECK_MAIN_NF_EXIST';
   L_table         VARCHAR2(30);
   L_key           VARCHAR2(100);
   ---
   L_cnt           NUMBER;
   ---
   cursor C_GET_MAIN_NFS is
      select count(*)
        from fm_fiscal_doc_complement fdc
       where fdc.fiscal_doc_id = I_fiscal_doc_id
         and fdc.fiscal_doc_id is NOT NULL;
   ---
BEGIN
   ---
   L_key := 'fiscal_doc_id: '||TO_CHAR(I_fiscal_doc_id);
   L_table := 'FM_FISCAL_DOC_COMPLEMENT';
   ---
   open C_GET_MAIN_NFS;
   ---
   fetch C_GET_MAIN_NFS into L_cnt;
   ---
      ---
      if L_cnt > 0 then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      ---
   ---
   close C_GET_MAIN_NFS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      if C_GET_MAIN_NFS%ISOPEN then
         close C_GET_MAIN_NFS;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_MAIN_NF_EXIST;
-----------------------------------------------------------------------------------
FUNCTION GET_DOC_TYPE(O_error_message  IN OUT VARCHAR2,
                      O_doc_type       IN OUT FM_FISCAL_DOC_HEADER.DOCUMENT_TYPE%TYPE,
                      I_tsf_no         IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                      I_key_value_1    IN     FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE)
   return BOOLEAN is
   ---
   L_program   VARCHAR2(50) := 'FM_FISCAL_HEADER_VAL_SQL.GET_DOC_TYPE';
   L_tsf       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'TSF';
   L_ic        FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'IC';
   L_rep       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'REP';
   L_APPROVED_STATUS   CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'A';  
   L_COMPLETED_STATUS  CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'C';
   L_FIN_POSTED_STATUS CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'FP';
   L_HOLD_STATUS CONSTANT FM_FISCAL_DOC_HEADER.STATUS%TYPE := 'H';
   ---
   cursor C_GET_DOC_TYPE is
      select nvl(fdh.document_type,'T') document_type
        from fm_fiscal_doc_header fdh, fm_fiscal_doc_detail fdd, tsfhead th, tsfhead th2 
       where fdh.module in ('PTNR','LOC') 
         and fdh.key_value_1 = TO_CHAR(I_key_value_1)
         and fdh.key_value_1 = th.to_loc 
         and th.tsf_no = th2.tsf_parent_no 
         and fdh.requisition_type in (L_tsf,L_ic,L_rep) 
         and fdd.fiscal_doc_id = fdh.fiscal_doc_id 
         and fdh.status in (L_APPROVED_STATUS,L_COMPLETED_STATUS,L_FIN_POSTED_STATUS,L_HOLD_STATUS) 
         and fdd.requisition_no = th.tsf_no 
         and (th.tsf_no = I_tsf_no 
          or th2.tsf_no = I_tsf_no)
         and rownum = 1;
        
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
			              'C_GET_DOC_TYPE',
                    'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL, TSFHEAD',
                    'KEY_VALUE_1= ' || TO_CHAR(I_key_value_1) || 'TSF_NO= ' || 'I_tsf_no');

   open C_GET_DOC_TYPE;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DOC_TYPE',
                    'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL, TSFHEAD',
                    'KEY_VALUE_1= ' || TO_CHAR(I_key_value_1) || 'TSF_NO= ' || 'I_tsf_no');

   fetch C_GET_DOC_TYPE into O_doc_type;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DOC_TYPE',
                    'FM_FISCAL_DOC_HEADER, FM_FISCAL_DOC_DETAIL, TSFHEAD',
                    'KEY_VALUE_1= ' || TO_CHAR(I_key_value_1) || 'TSF_NO= ' || 'I_tsf_no');

   close C_GET_DOC_TYPE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DOC_TYPE;
-----------------------------------------------------------------------------------------------------------
END FM_FISCAL_HEADER_VAL_SQL;
/