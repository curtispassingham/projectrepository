CREATE OR REPLACE PACKAGE BODY L10N_BR_INT_SQL AS
-------------------------------------------------------------------------------------------------------------
-- Global variables
-------------------------------------------------------------------------------------------------------------
LP_vdate           PERIOD.VDATE%TYPE := GET_VDATE;
LP_order_currency  CURRENCIES.CURRENCY_CODE%TYPE := NULL;
-------------------------------------------------------------------------------------------------------------
-- Global constants
-------------------------------------------------------------------------------------------------------------
LP_DEAL_ORDER  VARCHAR2(1) := 'D';
LP_ORDER       VARCHAR2(1) := 'O';
-------------------------------------------------------------------------------------------------------------
-- Global PL/SQL collections
-------------------------------------------------------------------------------------------------------------
LP_stage_rms_tbl              stage_rms_tbl;
LP_stage_routing_tbl          stage_routing_tbl;
LP_stage_eco_loc_tbl          stage_eco_tbl;
LP_stage_eco_sup_tbl          stage_eco_tbl;
LP_stage_fis_entity_loc_tbl   stage_fis_entity_tbl;
LP_stage_fis_entity_sup_tbl   stage_fis_entity_tbl;
LP_stage_item_tbl             stage_item_tbl;
LP_stage_name_value_tbl       stage_name_value_tbl;
LP_stage_order_tbl            stage_order_tbl;
LP_stage_order_exp_tbl        stage_order_exp_tbl;
LP_stage_order_info_tbl       stage_order_info_tbl;
LP_stage_regime_tbl           stage_regime_tbl;
-------------------------------------------------------------------------------------------------------------
-- Private function prototypes
-------------------------------------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
   FUNCTION LOAD_STAGE_TABLE(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tax_service_id  IN OUT         L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                             IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                             I_call_type       IN             VARCHAR2,
                             I_po_deal_ind     IN             VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION LOAD_PO_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                           I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION LOAD_SALE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                             I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION LOAD_PURCHASE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                                 I_tax_service_id  IN      NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                            I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                            I_call_type       IN             L10N_BR_TAX_CALL_STAGE_RMS.CALL_TYPE%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_PO_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                               I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_PURCHASE_SALE_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                          IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                                          I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION GET_TAX_INDS(O_error_message        IN OUT rtk_errors.rtk_text%type,
                         O_incl_nic_ind_pis        OUT vat_codes.incl_nic_ind%TYPE,
                         O_incl_nic_ind_cofins     OUT vat_codes.incl_nic_ind%TYPE,
                         O_incl_nic_ind_icms       OUT vat_codes.incl_nic_ind%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_TAX_REGIME(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_NAME_VALUE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_SUPPLIER(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_LOCATION(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_ITEM(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_PO_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_PURCHASE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION STAGE_SALE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PERSIST_STAGE_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                               I_call_type       IN      VARCHAR2)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
   FUNCTION PERSIST_STAGE_RMS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------
$end

-------------------------------------------------------------------------------------------------------------
-- Main package body
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_TAX_OBJECT (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_l10n_obj      IN OUT  L10N_OBJ)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'L10N_BR_INT_SQL.LOAD_TAX_OBJECT';
   L_l10n_tax_rec    L10N_TAX_REC;
   L_tax_service_id  L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_call_type       VARCHAR2(20);
   L_status          VARCHAR2(2);

BEGIN

   L_l10n_tax_rec := treat(IO_l10n_obj as L10N_TAX_REC);

   if L_l10n_tax_rec.call_type = 'P' then
      L_call_type := 'P';
   elsif L_l10n_tax_rec.call_type = 'S' then
      L_call_type := 'S';
   end if;

   if LOAD_STAGE_TABLE(O_error_message,
                       L_tax_service_id,
                       IO_l10n_obj,
                       L_call_type)= FALSE then
      return FALSE;
   end if;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                       L_status,
                                                       L_tax_service_id,
                                                       L10N_BR_INT_SQL.LP_CALC_TAX)= FALSE then
      return FALSE;
   end if;

   if PROCESS_RESULTS(O_error_message,
                      IO_l10n_obj,
                      L_tax_service_id,
                      L_call_type) = FALSE then
      return FALSE;
   end if;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_TAX_OBJECT;
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_ORDER_TAX_OBJECT (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_l10n_obj      IN OUT  L10N_OBJ)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(61) := 'L10N_BR_INT_SQL.LOAD_ORDER_TAX_OBJECT';
   L_tax_service_id   L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_call_type        L10N_BR_TAX_CALL_STAGE_RMS.CALL_TYPE%TYPE := 'PO';
   L_status           VARCHAR2(2);

BEGIN

   if LOAD_STAGE_TABLE(O_error_message,
                       L_tax_service_id,
                       IO_l10n_obj,
                       L_call_type,
                       LP_ORDER)= FALSE then
      return FALSE;
   end if;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                       L_status,
                                                       L_tax_service_id,
                                                       L10N_BR_INT_SQL.LP_CALC_TAX)= FALSE then
      return FALSE;
   end if;

   if PROCESS_RESULTS(O_error_message,
                      IO_l10n_obj,
                      L_tax_service_id,
                      L_call_type) = FALSE then
      return FALSE;
   end if;

  if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_ORDER_TAX_OBJECT;
-------------------------------------------------------------------------------------------------------------
FUNCTION DEAL_ORDER_DISCOUNT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_l10n_obj      IN OUT  L10N_OBJ)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(61) := 'L10N_BR_INT_SQL.DEAL_ORDER_DISCOUNT';
   L_tax_service_id   L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_call_type        L10N_BR_TAX_CALL_STAGE_RMS.CALL_TYPE%TYPE := 'PO';
   L_status           VARCHAR2(2);

BEGIN
   
   if LOAD_STAGE_TABLE(O_error_message,
                       L_tax_service_id,
                       IO_l10n_obj,
                       L_call_type,
                       LP_DEAL_ORDER)= FALSE then
      return FALSE;
   end if;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.EXECUTE_TAX_PROCESS(O_error_message,
                                                       L_status,
                                                       L_tax_service_id,
                                                       L10N_BR_INT_SQL.LP_CALC_TAX)= FALSE then
      return FALSE;
   end if;

   if PROCESS_RESULTS(O_error_message,
                      IO_l10n_obj,
                      L_tax_service_id,
                      L_call_type) = FALSE then
      return FALSE;
   end if;

   if L10N_BR_TAX_SERVICE_MNGR_SQL.CLEAR_TABLES(O_error_message,
                                                L_tax_service_id) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEAL_ORDER_DISCOUNT;
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_STAGE_TABLE(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                          O_tax_service_id  IN OUT         L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                          IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                          I_call_type       IN             VARCHAR2,
                          I_po_deal_ind     IN             VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_program          VARCHAR2(61) := 'L10N_BR_INT_SQL.LOAD_STAGE_TABLE';
   L_l10n_item_rec    L10N_TAX_REC;
   L_tax_service_id   L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE := NULL;
   L_order_no         ORDHEAD.ORDER_NO%TYPE := NULL;
   L_supplier         ORDHEAD.SUPPLIER%TYPE := NULL;
   L_exchange_rate    CURRENCY_RATES.EXCHANGE_RATE%TYPE := NULL;

   cursor C_GET_SEQ is
      select l10n_br_tax_service_seq.nextval
         from dual;

   cursor C_ORDER_INFO is
      select order_no,
             supplier,
             currency_code,
             exchange_rate
        from (select oh.order_no,
                     oh.supplier,
                     oh.currency_code,
                     mv.exchange_rate,
                     RANK() OVER(PARTITION BY mv.from_currency, mv.to_currency, mv.exchange_type
                                  ORDER BY mv.effective_date DESC) date_rank
                from ordhead oh,
                     mv_currency_conversion_rates mv
               where oh.order_no        = IO_l10n_obj.doc_id
                 and oh.currency_code   = mv.from_currency
                 and mv.exchange_type   = 'C'
                 and mv.to_currency     = 'BRL'
                 and mv.effective_date <= LP_vdate)
       where date_rank = 1;

    cursor C_XFORM_L10N_OBJ_PURCHASE is
      select L_tax_service_id,
             I_call_type,
             NULL doc_id,
             ltax.from_entity,
             ltax.from_entity_type,
             ltax.to_entity,
             ltax.to_entity_type,
             ltax.effective_from_date,
             ltax.item,
             NULL,  --PACK_ITEM
             ltax.origin_country_id,
             ltax.taxable_base*mc.exchange_rate,
             ltax.taxable_base_tax_incl_ind,
             1,     --QUANTITY
             0      --chunk id
       from  TABLE(L_l10n_item_rec.L10N_TAX_TBL) ltax,
            (select r.from_currency,
                     r.to_currency,
                     r.effective_date,
                     l.effective_date inner_effect_date,
                     r.exchange_rate,
                     rank() over
                        (PARTITION BY r.from_currency, r.to_currency, r.exchange_type
                            ORDER BY r.effective_date DESC) date_rank
                from mv_currency_conversion_rates r,
                     (select distinct taxable_base_curr from_currency,
                             'BRL' to_currency,
                             NVL(effective_from_date, LP_vdate) effective_date
                        from TABLE(L_l10n_item_rec.L10N_TAX_TBL)
                       where pack_ind = 'N') l
               where r.from_currency = l.from_currency
                 and r.to_currency = l.to_currency
                 and r.effective_date <= l.effective_date
                 and r.exchange_type = 'C') mc
      where ltax.pack_ind = 'N'
        and mc.from_currency = ltax.taxable_base_curr
        and mc.inner_effect_date = NVL(ltax.effective_from_date,LP_vdate)
        and mc.to_currency = 'BRL'
        and mc.date_rank = 1
      UNION ALL
      select L_tax_service_id,
             I_call_type,
             NULL doc_id,
             ltax.from_entity,
             ltax.from_entity_type,
             ltax.to_entity,
             ltax.to_entity_type,
             ltax.effective_from_date,
             ltax.item,
             ltax.pack_no,
             ltax.origin_country_id,
             ltax.taxable_base*mc.exchange_rate,
             ltax.taxable_base_tax_incl_ind,
             1,    --QUANTITY
             0     --chunk id
       from (select vpq.item,
                    l10ntax.effective_from_date,
                    isc.origin_country_id, 
                    -- Determine the prorated cost of the component item from the total cost of the pack
                    NVL(DECODE(l10ntax.taxable_base_tax_incl_ind, 'Y', isc.negotiated_item_cost, isc.base_cost), 0) * vpq.qty * l10ntax.taxable_base/
                       DECODE(SUM(NVL(DECODE(l10ntax.taxable_base_tax_incl_ind, 'Y', isc.negotiated_item_cost, isc.base_cost), 0) * vpq.qty) over (PARTITION BY vpq.pack_no,l10ntax.to_entity), 0, 1,
                              SUM(NVL(DECODE(l10ntax.taxable_base_tax_incl_ind, 'Y', isc.negotiated_item_cost, isc.base_cost), 0) * vpq.qty) over (PARTITION BY vpq.pack_no,l10ntax.to_entity)) taxable_base,
                    vpq.pack_no,
                    l10ntax.taxable_base_tax_incl_ind,
                    l10ntax.taxable_base_curr,
                    l10ntax.pack_ind,
                    l10ntax.from_entity,
                    l10ntax.from_entity_type,
                    l10ntax.to_entity,
                    l10ntax.to_entity_type
               from TABLE(L_l10n_item_rec.L10N_TAX_TBL) l10ntax,
                    v_packsku_qty vpq,
                    item_supp_country isc
              where l10ntax.pack_ind = 'Y'
                and vpq.pack_no = l10ntax.item
                and isc.item = vpq.item
                and isc.supplier = l10ntax.from_entity
                and isc.primary_country_ind ='Y') ltax,
             (select r.from_currency,
                     r.to_currency,
                     r.effective_date,
                     l.effective_date inner_effect_date,
                     r.exchange_rate,
                     rank() over
                        (PARTITION BY r.from_currency, r.to_currency, r.exchange_type
                            ORDER BY r.effective_date DESC) date_rank
                from mv_currency_conversion_rates r,
                     (select distinct taxable_base_curr from_currency,
                             'BRL' to_currency,
                             NVL(effective_from_date, LP_vdate) effective_date
                        from TABLE(L_l10n_item_rec.L10N_TAX_TBL)
                       where pack_ind = 'Y') l
               where r.from_currency = l.from_currency
                 and r.to_currency = l.to_currency
                 and r.effective_date <= l.effective_date
                 and r.exchange_type = 'C') mc
      where ltax.pack_ind = 'Y'
        and mc.from_currency = ltax.taxable_base_curr
        and mc.inner_effect_date = nvl(ltax.effective_from_date,LP_vdate)
        and mc.to_currency = 'BRL'
        and mc.date_rank = 1;

   cursor C_XFORM_L10N_OBJ_SALE is
      select L_tax_service_id,
             I_call_type,
             NULL doc_id,
             ltax.from_entity,
             ltax.from_entity_type,
             ltax.to_entity,
             ltax.to_entity_type,
             ltax.effective_from_date,
             ltax.item,
             NULL,  --PACK_ITEM
             NULL,  --ORIGIN_COUNTRY_ID
             ltax.taxable_base*mc.exchange_rate,
             ltax.taxable_base_tax_incl_ind,
             1,      --QUANTITY
             0       --chunk id
        from TABLE(L_l10n_item_rec.L10N_TAX_TBL) ltax,
             (select r.from_currency,
                     r.to_currency,
                     r.effective_date,
                     l.effective_date inner_effect_date,
                     r.exchange_rate,
                     rank() over
                        (PARTITION BY r.from_currency, r.to_currency, r.exchange_type
                            ORDER BY r.effective_date DESC) date_rank
                from mv_currency_conversion_rates r,
                     (select distinct taxable_base_curr from_currency,
                             'BRL' to_currency,
                             NVL(effective_from_date, LP_vdate) effective_date
                        from TABLE(L_l10n_item_rec.L10N_TAX_TBL)
                       where pack_ind = 'N') l
               where r.from_currency = l.from_currency
                 and r.to_currency = l.to_currency
                 and r.effective_date <= l.effective_date
                 and r.exchange_type = 'C') mc
       where ltax.pack_ind = 'N'
         and mc.from_currency = ltax.taxable_base_curr
         and mc.inner_effect_date = NVL(ltax.effective_from_date,LP_vdate)
         and mc.to_currency = 'BRL'
         and mc.date_rank = 1
       union all
      select L_tax_service_id,
             I_call_type,
             NULL doc_id,
             ltax.from_entity,
             ltax.from_entity_type,
             ltax.to_entity,
             ltax.to_entity_type,
             ltax.effective_from_date,
             ltax.item,
             ltax.pack_no,
             NULL,  --ORIGIN_COUNTRY_ID
             ltax.taxable_base*mc.exchange_rate,
             ltax.taxable_base_tax_incl_ind,
             1,      --QUANTITY
             0       --chunk id
        from (select vpq.item,
                     l10ntax.effective_from_date, 
                     -- Determine the prorated cost of the component item from the total cost of the pack
                     NVL(DECODE(l10ntax.taxable_base_tax_incl_ind, 'Y', isc.negotiated_item_cost, isc.base_cost), 0) * vpq.qty * l10ntax.taxable_base/
                         DECODE(SUM(NVL(DECODE(l10ntax.taxable_base_tax_incl_ind, 'Y', isc.negotiated_item_cost, isc.base_cost), 0) * vpq.qty) over (PARTITION BY vpq.pack_no,l10ntax.from_entity), 0, 1,
                                SUM(NVL(DECODE(l10ntax.taxable_base_tax_incl_ind, 'Y', isc.negotiated_item_cost, isc.base_cost), 0) * vpq.qty) over (PARTITION BY vpq.pack_no,l10ntax.from_entity)) taxable_base,
                     vpq.pack_no,
                     l10ntax.taxable_base_tax_incl_ind,
                     l10ntax.taxable_base_curr,
                     l10ntax.pack_ind,
                     l10ntax.from_entity,
                     l10ntax.from_entity_type,
                     l10ntax.to_entity,
                     l10ntax.to_entity_type
                from TABLE(L_l10n_item_rec.L10N_TAX_TBL) l10ntax,
                     v_packsku_qty vpq,
                     item_supp_country isc
               where l10ntax.pack_ind = 'Y'
                 and vpq.pack_no = l10ntax.item
                 and isc.item = vpq.item
                 and isc.primary_supp_ind = 'Y'
                 and isc.primary_country_ind ='Y') ltax,
             (select r.from_currency,
                     r.to_currency,
                     r.effective_date,
                     l.effective_date inner_effect_date,
                     r.exchange_rate,
                     rank() over
                        (PARTITION BY r.from_currency, r.to_currency, r.exchange_type
                            ORDER BY r.effective_date DESC) date_rank
                from mv_currency_conversion_rates r,
                     (select distinct taxable_base_curr from_currency,
                             'BRL' to_currency,
                             NVL(effective_from_date, LP_vdate) effective_date
                        from TABLE(L_l10n_item_rec.L10N_TAX_TBL)
                       where pack_ind = 'Y') l
               where r.from_currency = l.from_currency
                 and r.to_currency = l.to_currency
                 and r.effective_date <= l.effective_date
                 and r.exchange_type = 'C') mc
       where ltax.pack_ind = 'Y'
         and mc.from_currency = ltax.taxable_base_curr
         and mc.inner_effect_date = nvl(ltax.effective_from_date,LP_vdate)
         and mc.to_currency = 'BRL'
         and mc.date_rank = 1;

BEGIN
   
   if I_call_type in ('P','S') then
      -- Treat L_l10n_item_rec as a type of L10N_TAX_REC and initialize.
      L_l10n_item_rec := treat(IO_l10n_obj as L10N_TAX_REC);
   end if;

   -- Generate a new tax service id
   open C_GET_SEQ;
   fetch C_GET_SEQ into L_tax_service_id;
   close C_GET_SEQ;

   -- Assign the tax service id to the output parameter
   O_tax_service_id := L_tax_service_id;

   -- Populate LP_stage_rms_tbl for a Cost (Purchase) tax call
   if I_call_type = 'P' then
      open C_XFORM_L10N_OBJ_PURCHASE;
      fetch C_XFORM_L10N_OBJ_PURCHASE BULK COLLECT into LP_stage_rms_tbl;
      close C_XFORM_L10N_OBJ_PURCHASE;

   -- Populate LP_stage_rms_tbl for a Retail (Sale) tax call
   elsif I_call_type = 'S' then
      open C_XFORM_L10N_OBJ_SALE;
      fetch C_XFORM_L10N_OBJ_SALE BULK COLLECT into LP_stage_rms_tbl;
      close C_XFORM_L10N_OBJ_SALE;
   -- Process for a Purchase Order tax call
   elsif I_call_type = 'PO' then
      ---
      if IO_l10n_obj.doc_type = 'PO' then
         ---
         if IO_l10n_obj.doc_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'DOC_ID', NULL, NULL);
            return FALSE;
         end if;
         ---
      end if;
      -- Fetch currency code and exchange rate
      open C_ORDER_INFO;
      fetch C_ORDER_INFO into L_order_no,
                              L_supplier,
                              LP_order_currency,
                              L_exchange_rate;
      close C_ORDER_INFO;

      -- Check for NULL values in the previous fetch
      if LP_order_currency is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_CODE', NULL, NULL, NULL);
         return FALSE;
      end if;

      if L_exchange_rate is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_CURR_INFO_FOUND', L_exchange_rate, NULL, NULL);
         return FALSE;
      end if;
   end if;

   -- Stage records for a Purchase Order (Deal Order) tax call
   if I_call_type = 'PO' and I_po_deal_ind = LP_DEAL_ORDER then
      select distinct
             L_tax_service_id,
             'PO',
             L_order_no,
             L_supplier,
             'SP',
             location,
             DECODE(loc_type,'S','ST','WH'),
             LP_vdate,
             item,
             pack_no,
             'BR',
             amount,
             tax_incl_ind,
             quantity,     --holds order qty at pack level
             0
      BULK COLLECT INTO LP_stage_rms_tbl
        from (select ol.location,
                     ol.loc_type,
                     ol.item,
                     DECODE(cost_source, 'DEAL', ol.unit_cost_init , ol.unit_cost) * L_exchange_rate amount,  --unit_cost
                     NULL pack_no,
                     DECODE(ca.default_po_cost, 'NIC', 'Y', 'N') tax_incl_ind,
                     ol.qty_ordered quantity
                from ordsku os,
                     ordloc ol,
                     item_master im,
                     l10n_doc_details_gtt ld, --Ensure only the records in the GTT are picked-up
                     country_attrib ca
               where os.order_no = ol.order_no
                 and os.item = ol.item
                 and ol.order_no = L_order_no
                 and ol.order_no = ld.doc_id
                 and ol.item = ld.item
                 and im.item = ol.item
                 and im.pack_ind  = 'N'
                 and ca.country_id = os.origin_country_id
              UNION ALL
              select ol.location,
                     ol.loc_type,
                     vpq.item,
                     -- Determine the prorated cost of the component item from the total cost of the pack
                     DECODE(cost_source, 'DEAL', ol.unit_cost_init
                                               , ol.unit_cost)
                     * NVL(DECODE(ca.default_deal_cost, 'NIC', iscl.negotiated_item_cost, iscl.base_cost), 0) * vpq.qty
                     / DECODE(SUM(NVL(DECODE(ca.default_deal_cost, 'NIC', iscl.negotiated_item_cost, iscl.base_cost), 0) * vpq.qty) over (PARTITION BY vpq.pack_no,ol.location), 0, 1,
                              SUM(NVL(DECODE(ca.default_deal_cost, 'NIC', iscl.negotiated_item_cost, iscl.base_cost), 0) * vpq.qty) over (PARTITION BY vpq.pack_no,ol.location))
                     * L_exchange_rate amount,
                     vpq.pack_no,
                     DECODE(ca.default_po_cost, 'NIC', 'Y', 'N') tax_incl_ind,
                     ol.qty_ordered quantity
                from ordloc ol,
                     item_master im,
                     item_supp_country_loc iscl,
                     v_packsku_qty vpq,
                     country_attrib ca,
                     l10n_doc_details_gtt ld --Ensure only the records in the GTT are picked-up
               where ol.order_no = L_order_no
                 and ld.doc_id = ol.order_no
                 and ld.item = ol.item
                 and ld.location = ol.location
                 and ld.loc_type = ol.loc_type
                 and ld.doc_type = 'PO'
                 and im.item = ol.item
                 and im.pack_ind = 'Y'
                 --Get item supp country location detail for the component items of the pack
                 and vpq.pack_no = ol.item
                 and iscl.item = vpq.item
                 and iscl.supplier = ld.supplier
                 and iscl.origin_country_id = ld.country_id
                 and iscl.loc = ld.location
                 and iscl.loc_type = ld.loc_type
                 and ca.country_id = ld.country_id);
   end if;

   -- Stage records for a Purchase Order (Deal Order) tax call
   if I_call_type = 'PO' and I_po_deal_ind = LP_ORDER then
      select distinct
             L_tax_service_id,
             'PO',
             L_order_no,
             L_supplier,
             'SP',
             location,
             DECODE(loc_type,'S','ST','WH'),
             LP_vdate,
             item,
             pack_no,
             'BR',
             amount,
             tax_incl_ind,
             quantity,    --holds order qty at pack level
             0
      BULK COLLECT INTO LP_stage_rms_tbl
        from (select ol.location,
                     ol.loc_type,
                     ol.item,
                     DECODE(cost_source, 'DEAL', ol.unit_cost_init , ol.unit_cost) * L_exchange_rate amount,
                     NULL pack_no,
                     DECODE(ca.default_po_cost, 'NIC', 'Y', 'N') tax_incl_ind,
                     ol.qty_ordered quantity
                from ordsku os,
                     ordloc ol,
                     item_master im,
                     country_attrib ca
               where os.order_no = ol.order_no
                 and os.item = ol.item
                 and ol.order_no = L_order_no
                 and im.item = ol.item
                 and im.pack_ind  = 'N'
                 and ca.country_id = os.origin_country_id
              UNION ALL
              select ol.location,
                     ol.loc_type,
                     vpq.item,
                     -- Determine the prorated cost of the component item from the total cost of the pack
                     DECODE(cost_source, 'DEAL', ol.unit_cost_init
                                               , ol.unit_cost)
                     * NVL(DECODE(ca.default_po_cost, 'NIC', iscl.negotiated_item_cost, iscl.base_cost), 0) * vpq.qty
                     / DECODE(SUM(NVL(DECODE(ca.default_po_cost, 'NIC', iscl.negotiated_item_cost, iscl.base_cost), 0) * vpq.qty) over (PARTITION BY vpq.pack_no, ol.location), 0, 1,
                              SUM(NVL(DECODE(ca.default_po_cost, 'NIC', iscl.negotiated_item_cost, iscl.base_cost), 0) * vpq.qty) over (PARTITION BY vpq.pack_no, ol.location))
                     * L_exchange_rate amount,
                     vpq.pack_no,
                     DECODE(ca.default_po_cost, 'NIC', 'Y', 'N') tax_incl_ind,
                     ol.qty_ordered quantity
                from ordsku os,
                     ordloc ol,
                     item_master im,
                     item_supp_country_loc iscl,
                     v_packsku_qty vpq,
                     country_attrib ca
              where os.order_no = ol.order_no
                and os.item = ol.item
                and ol.order_no             = L_order_no
                and ol.item                 = im.item
                and im.pack_ind             = 'Y'
                and ol.item                 = vpq.pack_no
                and iscl.item                = vpq.item
                and iscl.supplier            = L_supplier
                and iscl.origin_country_id   = os.origin_country_id
                and iscl.loc                 = ol.location
                and iscl.loc_type            = ol.loc_type
                and iscl.origin_country_id   = ca.country_id);
   end if;

   -- Stage records for a Purchase or Sale tax call or a PO tax call.
   -- A PO tax call originates from the LOAD_ORDER_TAX_OBJECT or DEAL_ORDER_DISCOUNT function.
   -- Persist the l10n_br_tax_call_stage_rms row in a seprate automic transaction first
   -- so that the other staging functions can utilize the data in this driving staging table.
   if I_call_type in ('PO', 'P', 'S') then
      if LP_stage_rms_tbl is not NULL and LP_stage_rms_tbl.COUNT > 0 then
         if PERSIST_STAGE_RMS(O_error_message,
                              O_tax_service_id) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   -- Load auxillary staging collections
   -- The purpose of loading the following staging collections is to resolve transactionality issue w.r.t. RTIL service.
   -- Since GET_REQUEST_DATA is invoked in a separate DB session to call LOAD_SALE/PURCHASE/PO_OBJECT, data created
   -- in the current transaction wouldn't be available to the load functions invoked by GET_REQUEST_DATA. To solve
   -- this problem, we stage all data needed by the load functions and commit them in an automic transaction.
   if I_call_type = 'PO' then
      if STAGE_PO_OBJECT(O_error_message,
                         L_tax_service_id) = FALSE then
         return FALSE;
      end if;
   elsif I_call_type = 'P' then
      if STAGE_PURCHASE_OBJECT(O_error_message,
                               L_tax_service_id)= FALSE then
         return FALSE;
      end if;
   elsif I_call_type = 'S' then
      if STAGE_SALE_OBJECT(O_error_message,
                           L_tax_service_id) = FALSE then
         return FALSE;
      end if;
   end if;

   -- Stage the routing information
   select L_tax_service_id,
          'RMS_TAX',
          'N'
   BULK COLLECT INTO LP_stage_routing_tbl
     from dual;

   --Persist collections into the staging tables in an autonomous transaction
   if PERSIST_STAGE_DATA(O_error_message,
                         O_tax_service_id,
                         I_call_type) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_STAGE_TABLE;
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_SALE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_SALE_OBJECT';

   L_incl_nic_ind_pis      vat_codes.incl_nic_ind%TYPE;
   L_incl_nic_ind_cofins   vat_codes.incl_nic_ind%TYPE;
   L_incl_nic_ind_icms     vat_codes.incl_nic_ind%TYPE;

BEGIN

   -- Get tax indicators for the Brazil types needed for item staging
   if GET_TAX_INDS(O_error_message,
                   L_incl_nic_ind_pis,
                   L_incl_nic_ind_cofins,
                   L_incl_nic_ind_icms) = FALSE then
      return FALSE;
   end if;

   -- stage name/value pair attributes for item and location
   if STAGE_NAME_VALUE(O_error_message,
                       I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   --stage Location entity
   --for sales use from entity for both from entity and to entity
   --valid entity types are 'ST' (including wholesale/franchise stores) and 'WH'
   --External finishers (partner) do not have retail (i.e. n/a for sales).
   select I_tax_service_id,      --tax_service_id
          stg.from_entity_type,  --entity_type
          stg.from_entity,       --entity_id
          NULL,                  --physical_wh
          ads.city_id,           --city_id, holds jurisdicstion_code
          ads.city,              --city, holds jurisdiction_desc
          ads.state,             --state,
          ads.state_name,        --state_name,
          ads.country_id,        --country_id,
          ads.country_name,      --country_name,
          ads.addr,              --addr,
          ads.addr_type,         --addr_type,
          ads.primary_addr_type_ind, --primary_addr_type_ind,
          ads.primary_addr_ind,      --primary_addr_ind,
          ads.add_1,             --add_1,
          ads.add_2,             --add_2,
          ads.add_3,             --add_3,
          attr.iss_contrib_ind,  -- taxContrib node
          attr.ipi_ind,          -- taxContrib node
          attr.icms_contrib_ind, -- taxContrib node
          NULL,                  -- taxContrib node st_ind, n/a for locs
          NULL,                  --legal_name,
          'S',                   --fiscal_type,
          NULL,                  --is_simples_contributor,
          attr.cnpj,             --federal_tax_reg_id, cnpj
          'N',                   --is_rural_producer,
          NULL,                  --is_income_range_eligible, n/a for locs
          NULL,                  --is_distr_a_manufacturer, n/a for locs
          NULL                   --icms_simples_rate, n/a for locs
   BULK COLLECT INTO LP_stage_fis_entity_loc_tbl
     from (select distinct from_entity,
                  from_entity_type
             from l10n_br_tax_call_stage_rms
            where tax_service_id = I_tax_service_id) stg,
          --addr view
          (select key_value_1,
                  ctj.jurisdiction_code city_id,  --CITY_ID
                  NULL state_name,                --STATE_NAME
                  ct.country_desc country_name,   --COUNTRY_NAME
                  '1' addr,                       --ADDR
                  addr.addr_type,                 --ADDR_TYPE
                  '1' primary_addr_type_ind,      --PRIMARY_ADDR_TYPE_IND
                  addr.primary_addr_ind,          --PRIMARY_ADDR_IND
                  addr.add_1,                     --ADD_1
                  NULL add_2,                     --ADD_2
                  NULL add_3,                     --ADD_3
                  ctj.jurisdiction_desc city,     --CITY
                  st.state,                       --STATE
                  vbc.fiscal_code country_id      --COUNTRY_ID
             from addr,
                  state st,
                  country ct,
                  country_tax_jurisdiction ctj,
                  v_br_country_attrib vbc
            where addr.state            = ctj.state
              and addr.country_id       = ctj.country_id
              and addr.jurisdiction_code= ctj.jurisdiction_code
              and addr.state            = st.state
              and addr.country_id       = st.country_id
              and addr.primary_addr_ind = 'Y'
              and addr.addr_type        = DECODE(addr.module,'WFST','07','01')
              and addr.module           in ('WH','ST','WFST')
              and ct.country_id         = addr.country_id
              and ct.country_id         = vbc.country_id) ads,
          --L10N attributes view
          (select store loc,
                  'ST'  loc_type,
                  iss_contrib_ind,
                  ipi_ind,
                  icms_contrib_ind,
                  cnpj
             from v_br_store
            UNION ALL
           select wh loc,
                  'WH' loc_type,
                  iss_contrib_ind,
                  ipi_ind,
                  icms_contrib_ind,
                  cnpj
             from v_br_wh) attr
    where ads.key_value_1 in (select stg.from_entity
                                from dual
                               where stg.from_entity_type = 'ST'
                               UNION ALL
                              select to_char(physical_wh)
                                from wh
                               where wh = stg.from_entity
                                 and stg.from_entity_type = 'WH')
      and attr.loc(+) = stg.from_entity
      and attr.loc_type(+) = stg.from_entity_type;

   -- stage ENTITY_CNAE_CODES
   select I_tax_service_id,
          stg.from_entity_type,
          stg.from_entity,
          cnae_code
   BULK COLLECT INTO LP_stage_eco_loc_tbl
     from l10n_br_entity_cnae_codes,
          (select distinct from_entity,
                           from_entity_type
             from l10n_br_tax_call_stage_rms
            where tax_service_id = I_tax_service_id) stg
    where key_value_1 in(select stg.from_entity
                           from dual
                          where stg.from_entity_type = 'ST'
                          UNION ALL
                         select to_char(physical_wh)
                           from wh
                          where wh = stg.from_entity
                            and stg.from_entity_type = 'WH')
      and key_value_2 = DECODE(stg.from_entity_type,'ST','S','W')
      and module      = 'LOC'
      and cnae_code is NOT NULL;

   -- stage Item entity
   select I_tax_service_id, --TAX_SERVICE_ID
          stg.item,         --DOCUMENT_LINE_ID
          stg.item,         --ITEM_ID
          im.item_desc,     --ITEM_DESCRIPTION
          NULL,             --ITEM_TRAN_CODE
          DECODE(vbi.service_ind,'N','M','S'),  --ITEM_TYPE
          stg.quantity,                        --QUANTITY, staged as 1 on l10n_br_tax_call_stage_rms for SALES
          'EA',                                --UNIT_OF_MEASURE
          NULL,                                --UOM_CONV_FACTOR
          stg.quantity * NVL(im.uom_conv_factor, 1),  --QUANTITY_IN_EACHES
          NULL,             --ORIGIN_DOC_DATE
          stg.pack_item,    --PACK_ITEM_ID
          NULL,             --TOTAL_COST
          NULL,             --UNIT_COST
          NULL,             --SRC_TAXPAYER_TYPE
          NULL,             --ORIG_FISCAL_DOC_NUMBER
          NULL,             --ORIG_FISCAL_DOC_SERIES
          ---
          iscd_tbl.dim_object, --DIM_OBJECT
          DECODE(NVL(iscd_tbl.dim_object, '-999'), 'CA', iscd_tbl.length/iscd_tbl.supp_pack_size/NVL(im.uom_conv_factor,1), iscd_tbl.length),   --length
          DECODE(NVL(iscd_tbl.dim_object, '-999'), 'CA', iscd_tbl.width/iscd_tbl.supp_pack_size/NVL(im.uom_conv_factor,1), iscd_tbl.width),     --width
          iscd_tbl.lwh_uom,    --LWH_UOM
          DECODE(iscd_tbl.dim_object, 'EA', iscd_tbl.weight,
                                      'CA', iscd_tbl.weight/iscd_tbl.supp_pack_size/NVL(im.uom_conv_factor,1),
                                      NULL),     --WEIGHT
          DECODE(iscd_tbl.dim_object, 'EA', iscd_tbl.net_weight * NVL(pi.pack_qty,1),
                                      'CA', iscd_tbl.net_weight/iscd_tbl.supp_pack_size/NVL(im.uom_conv_factor,1) * NVL(pi.pack_qty,1),
                                      NULL),     --NET_WEIGHT
          iscd_tbl.weight_uom,         --WEIGHT_UOM
          DECODE(iscd_tbl.dim_object, 'EA', iscd_tbl.liquid_volume,
                                      'CA', iscd_tbl.liquid_volume/iscd_tbl.supp_pack_size/NVL(im.uom_conv_factor,1),
                                      NULL),     --LIQUID_VOLUME
          iscd_tbl.liquid_volume_uom,  --liquid_volume_uom
          ---
          NULL,   --origin_fiscal_code_opr
          NULL,   --deduced_fiscal_code_opr
          'Y',    --DEDUCE_CFOP_CODE
          NULL,   --icms_cst_code
          NULL,   --pis_cst_code
          NULL,   --cofins_cst_code
          NULL,   --deduce_icms_cst_code
          NULL,   --deduce_pis_cst_codE
          NULL,   --deduce_cofins_cst_code
          NULL,   --recoverable_icmsst
          DECODE(NVL(stg.tax_incl_ind, 'N'), 'Y', L_incl_nic_ind_cofins, 'N'),  --ITEM_COST_CONTAINS_COFINS
          NULL,   --recoverable_base_icmsst
          DECODE(NVL(stg.tax_incl_ind, 'N'), 'Y', L_incl_nic_ind_pis, 'N'),     --ITEM_COST_CONTAINS_PIS
          DECODE(NVL(stg.tax_incl_ind, 'N'), 'Y', L_incl_nic_ind_icms, 'N'),    --ITEM_COST_CONTAINS_ICMS
          ---
          vbi.service_ind,           --service_ind
          vbi.origin_code,           --item_origin,
          'C',                       --item_utilization,
          im.item_xform_ind,         --is_transformed_item,
          vbi.classification_id,     --fiscal_classification_code,
          case
             when vbi.ncm_char_code is NOT NULL and
                  vbi.pauta_code is NOT NULL then
                vbi.ncm_char_code || '.' || vbi.pauta_code
             when vbi.ncm_char_code is NOT NULL and
                  vbi.pauta_code is NULL then
                vbi.ncm_char_code
             when vbi.ncm_char_code is NULL and
                  vbi.pauta_code is NOT NULL then  --not a valid scenario
                vbi.pauta_code
             else
                NULL
          end ext_fiscal_class_code, --ext_fiscal_class_code,
          vbi.ex_ipi,                --ipi_exception_code,
          NULL,                      --product_type,
          vbi.state_of_manufacture,  --state_of_manufacture,
          vbi.pharma_list_type,      --pharma_list_type,
          vbi.federal_service,       --federal_service_code,
          vbi.service_code,           --dst_service_code,
          -1,                        --item_group_id,
          -1                         --item_group_counter
   BULK COLLECT INTO LP_stage_item_tbl
     from (select distinct item,
                           pack_item,
                           tax_incl_ind,
                           quantity
             from l10n_br_tax_call_stage_rms
            where tax_service_id = I_tax_service_id) stg,
          v_br_item_fiscal_attrib vbi,
          item_master im,
          packitem pi,
          -- build a dimension collection based on primary supplier/country
          -- If 'EA' is defined, use dimension of 'EA', otherwise use dimension of 'CA'.
          -- The join with l10n_br_tax_call_stage_rms can have multiple locations per item.  Adding a
          -- distinct would make the item unique across the locations.
          (select distinct
                  iscd.item,
                  iscd.supplier,
                  iscd.origin_country,
                  iscd.dim_object,
                  iscd.length,
                  iscd.width,
                  iscd.height,
                  iscd.lwh_uom,
                  iscd.weight,
                  iscd.net_weight,
                  iscd.weight_uom,
                  iscd.liquid_volume,
                  iscd.liquid_volume_uom,
                  isc.supp_pack_size,
                  rank() over
                     (PARTITION BY iscd.item, iscd.supplier, iscd.origin_country
                         ORDER BY dim_object DESC) dim_rank
             from l10n_br_tax_call_stage_rms stg,
                  item_supp_country_dim iscd,
                  item_supp_country isc
            where stg.tax_service_id = I_tax_service_id
              and stg.item = isc.item
              and isc.primary_supp_ind = 'Y'
              and isc.primary_country_ind = 'Y'
              and isc.item = iscd.item
              and isc.supplier = iscd.supplier
              and isc.origin_country_id = iscd.origin_country
              and iscd.dim_object in ('EA', 'CA')) iscd_tbl
    where stg.item = im.item
      and stg.item = vbi.item
      and stg.item = iscd_tbl.item(+)
      and iscd_tbl.dim_rank(+) = 1
      and stg.item = pi.item(+)
      and stg.pack_item = pi.pack_no(+);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
      return FALSE;
END STAGE_SALE_OBJECT;
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_PURCHASE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_PURCHASE_OBJECT';

BEGIN

   -- Stage the name value pairs for the entities and items
   if STAGE_NAME_VALUE(O_error_message,
                       I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   -- Stage the nature of operation
   select I_tax_service_id,
          NULL,
          fmfu.nop
   BULK COLLECT INTO LP_stage_order_tbl
     from fm_fiscal_utilization fmfu,
          fm_system_options fmso
    where fmfu.utilization_id = fmso.string_value
      and fmso.variable = 'DEFAULT_PO_TYPE';

   -- Stage suppliers
   if STAGE_SUPPLIER(O_error_message,
                     I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   -- Stage the eco class for the suppliers
   select distinct
          I_tax_service_id,
          stg.from_entity_type,
          stg.from_entity,
          c.cnae_code
   BULK COLLECT INTO LP_stage_eco_sup_tbl
     from l10n_br_tax_call_stage_rms stg,
          l10n_br_entity_cnae_codes c
    where stg.tax_service_id = I_tax_service_id
      and c.key_value_1  = stg.from_entity
      and c.key_value_2  = 'S'
      and c.module       = 'SUPP'
      and c.cnae_code    is NOT NULL;

   -- Stage the tax regime for the supplier
   if STAGE_TAX_REGIME(O_error_message,
                       I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   -- Stage locations
   if STAGE_LOCATION(O_error_message,
                     I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   -- Stage the eco class for the locations
   select distinct
          I_tax_service_id,
          stg.to_entity_type,
          stg.to_entity,
          c.cnae_code
   BULK COLLECT INTO LP_stage_eco_loc_tbl
     from l10n_br_tax_call_stage_rms stg,
          l10n_br_entity_cnae_codes c
    where stg.tax_service_id = I_tax_service_id
      and c.key_value_1  = stg.to_entity
      and c.key_value_2  = DECODE(stg.to_entity_type,'ST','S','WH','W','E')
      and c.module       = DECODE(stg.to_entity_type,'E','PTNR','LOC')
      and c.cnae_code    is NOT NULL;

   -- Stage items
   if STAGE_ITEM(O_error_message,
                 I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   -- Stage the item supplier attributes
   select tax_service_id,
          doc_id,
          to_entity,
          to_entity_type,
          from_entity,
          from_entity_type,
          origin_country_id,
          item_id,
          pack_item_id,
          quantity,
          quantity_in_eaches,
          dim_object,
          length,
          width,
          lwh_uom,
          weight,
          net_weight,
          weight_uom,
          liquid_volume,
          liquid_volume_uom,
          amount,
          -1,
          -1
     BULK COLLECT INTO LP_stage_order_info_tbl
     from (select I_tax_service_id tax_service_id,
                  NULL doc_id,
                  stg.to_entity,          --to_entity
                  stg.to_entity_type,     --to_entity_type
                  stg.from_entity,        --from_entity
                  stg.from_entity_type,   --from_entity_type
                  stg.origin_country_id,  --origin_country_id
                  stg.item item_id,
                  stg.pack_item pack_item_id,
                  stg.quantity,                              --quantity, staged as 1 on l10n_br_tax_call_stage_rms for PURCHASE,
                  stg.quantity * NVL(im.uom_conv_factor, 1)  quantity_in_eaches,
                  iscd.dim_object,                           --dim_object
                  DECODE(NVL(iscd.dim_object, '-999'),
                         'CA', (iscd.length/isc.supp_pack_size) / NVL(im.uom_conv_factor, 1),
                         iscd.net_weight) length,
                  iscd.width,               --width
                  iscd.lwh_uom,             --lwh_uom
                  DECODE(NVL(iscd.dim_object, '-999'),
                         'CA', (iscd.weight/isc.supp_pack_size) / NVL(im.uom_conv_factor, 1),
                         iscd.net_weight) weight,
                  DECODE(NVL(iscd.dim_object, '-999'),
                         'CA', (iscd.net_weight/isc.supp_pack_size) / NVL(im.uom_conv_factor, 1),
                         iscd.net_weight) * NVL(pi.pack_qty, 1) net_weight,
                  iscd.weight_uom,          --weight_uom
                  DECODE(NVL(iscd.dim_object, '-999'),
                         'CA', (iscd.liquid_volume/isc.supp_pack_size) / NVL(im.uom_conv_factor, 1),
                         iscd.net_weight) liquid_volume,
                  iscd.liquid_volume_uom,   --liquid_volume_uom
                  stg.amount,
                  -- Rank used to select 'EA' dim_object if it is defined; otherwise use dimension of 'CA'.
                  rank() over
                     (PARTITION BY stg.item, stg.from_entity, stg.origin_country_id
                         ORDER BY iscd.dim_object DESC) dim_rank
             from -- get distinct item/supplier/origin country
                  (select distinct
                          from_entity,
                          from_entity_type,
                          to_entity,
                          to_entity_type,
                          item,
                          pack_item,
                          origin_country_id,
                          quantity,
                          amount
                     from l10n_br_tax_call_stage_rms
                    where tax_service_id = I_tax_service_id) stg,
                  item_supp_country_dim iscd,
                  item_supp_country isc,
                  item_master im,
                  packitem pi
            where stg.item              = isc.item
              and stg.from_entity       = isc.supplier -- FROM entity is a supplier for cost
              and stg.origin_country_id = isc.origin_country_id
              --
              and stg.item              = im.item
              --
              and isc.item              = iscd.item(+)
              and isc.supplier          = iscd.supplier(+)
              and isc.origin_country_id = iscd.origin_country(+)
              and iscd.dim_object(+) in ('EA', 'CA')
              --
              and stg.item              = pi.item(+)
              and stg.pack_item         = pi.pack_no(+))
    -- If 'EA' is defined, use dimension of 'EA'; otherwise use dimension of 'CA'.
    where dim_rank = 1;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STAGE_PURCHASE_OBJECT;
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_PO_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_PO_OBJECT';

BEGIN

   if STAGE_NAME_VALUE(O_error_message,
                       I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   if STAGE_TAX_REGIME(O_error_message,
                       I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   --stage order specific info
   select DISTINCT I_tax_service_id,
          s.doc_id,
          fmfu.nop
   BULK COLLECT INTO LP_stage_order_tbl
     from l10n_br_tax_call_stage_rms s,
          fm_fiscal_utilization fmfu,
          v_br_ord_util_code vboc
    where s.tax_service_id    = I_tax_service_id
      and fmfu.utilization_id = vboc.util_code
      and vboc.order_no       = s.doc_id;

   --stage order expense info
   select I_tax_service_id,
          exp.order_no,
          exp.exp_item,
          exp.pack_item,
          exp.location,
          SUM(DECODE(exp.exp_category,'F',exp.est_exp_value,0)) freight,
          SUM(DECODE(exp.exp_category,'I',exp.est_exp_value,0)) insurance,
          SUM(DECODE(exp.exp_category,'OTH',exp.est_exp_value,0)) other_expenses,
          SUM(DECODE(exp.exp_category,'DIS',exp.est_exp_value,0)) discount
   BULK COLLECT INTO LP_stage_order_exp_tbl
     from ordloc ol,
          (-- Regular items and buyer packs
           select oe.order_no,
                  oe.item exp_item,
                  oe.pack_item pack_item,
                  oe.location,
                  oe.est_exp_value,
                  DECODE(ec.exp_category,'I','I','F','F','OTH') exp_category
             from l10n_br_tax_call_stage_rms s,
                  ordloc_exp oe,
                  elc_comp ec,
                  item_master im
            where s.tax_service_id = I_tax_service_id
              and ec.comp_id     = oe.comp_id
              and ec.comp_type   = 'E'
              and oe.order_no    = s.doc_id
              and oe.item        = s.item
              and oe.location    = s.to_entity
              and ec.comp_id     <> 'TEXP'
              and im.item        = oe.item
              and (im.pack_ind = 'N' and (oe.pack_item is NULL or oe.pack_item = s.pack_item))
           UNION ALL
           --Vendor and simple packs
           select oe.order_no,
                  s.item exp_item,
                  oe.item pack_item,
                  oe.location,
                  (oe.est_exp_value * vpq.qty * NVL(isc.unit_cost, 0)/
                   DECODE(SUM(NVL(isc.unit_cost, 0) * vpq.qty) over (PARTITION BY vpq.pack_no), 0, 1,
                          SUM(NVL(isc.unit_cost, 0) * vpq.qty) over (PARTITION BY vpq.pack_no)))
                   * NVL(mc.exchange_rate, 1) est_exp_value,
                  DECODE( ec.exp_category,'I','I','F','F','OTH') exp_category
             from l10n_br_tax_call_stage_rms s,
                  ordloc_exp oe,
                  elc_comp ec,
                  item_master im,
                  v_packsku_qty vpq,
                  item_supp_country_loc isc,
                  ordsku os,
                  ordhead oh,
                  -- currency conversion
                  (select from_currency,
                          to_currency,
                          effective_date,
                          exchange_rate,
                          rank() over
                          (PARTITION BY from_currency, to_currency, exchange_type
                                                       ORDER BY effective_date DESC) date_rank
                     from mv_currency_conversion_rates
                    where exchange_type = 'C'
                      and effective_date <= get_vdate) mc
            where s.tax_service_id      = I_tax_service_id
              and ec.comp_id            = oe.comp_id
              and ec.comp_type          = 'E'
              and oe.order_no           = s.doc_id
              and oe.item               = s.pack_item
              and oe.location           = s.to_entity
              and ec.comp_id            <> 'TEXP'
              and im.item               = oe.item
              and im.pack_ind           = 'Y'
              and oe.pack_item          is NULL
              and vpq.pack_no           = oe.item
              and vpq.item              = s.item
              and isc.item              = s.item
              and isc.supplier          = oh.supplier
              and oh.order_no           = s.doc_id
              and isc.origin_country_id = os.origin_country_id
              and isc.loc               = s.to_entity
              and os.item               = s.pack_item
              and os.order_no           = s.doc_id
              and mc.from_currency      = oh.currency_code
              and mc.to_currency        = 'BRL'
              and mc.date_rank          = 1
         UNION ALL
         select od.order_no,
                od.item exp_item,
                od.pack_no pack_item,
                od.location,
                od.discount_amt_per_unit est_exp_value,
                'DIS' exp_category
           from l10n_br_tax_call_stage_rms s,
                ordloc_discount od
          where s.tax_service_id = I_tax_service_id
            and od.order_no = s.doc_id
            and od.item     = s.item
            and (od.location = (select physical_wh
                                  from wh
                                 where wh = s.to_entity) or
                 od.location = s.to_entity)
          ) exp
    where ol.item                 = NVL(exp.pack_item, exp.exp_item)
      and ol.location             = exp.location
      and ol.order_no             = exp.order_no
    group by I_tax_service_id,
             exp.order_no,
             exp.exp_item,
             exp.pack_item,
             exp.location;

   --stage order info
   select I_tax_service_id,                             --tax_service_id,
          r.doc_id,                                     --doc_id,
          r.to_entity,                                  --to_entity,
          r.to_entity_type,                             --to_entity_type,
          r.from_entity,                                --from_entity,
          r.from_entity_type,                           --from_entity_type,
          r.orig_cntry_id,                              --origin_country_id,
          r.item,                                       --item_id,     --holds component item if pack
          r.pack_item,                                  --pack_item,
          ol.qty_ordered,                               --quantity,    --holds order qty at pack level
          ol.qty_ordered * NVL(im.uom_conv_factor, 1),  --quantity_in_eaches,
          iscd.dim_object,                              --dim_object,
          iscd.length,                                  --length,
          iscd.width,                                   --width,
          iscd.lwh_uom,                                 --lwh_uom,
          iscd.weight,                                  --weight,
          iscd.net_weight * nvl(pi.pack_qty, 1),        --net_weight,
          iscd.weight_uom,                              --weight_uom,
          iscd.liquid_volume,                           --liquid_volume,
          iscd.liquid_volume_uom,                        --liquid_volume_uom,
          r.amount,                                     --amount
          -1,
          -1
   BULK COLLECT INTO LP_stage_order_info_tbl
     from (select s.*,
                  os.item ordsku_item,
                  os.origin_country_id orig_cntry_id
             from l10n_br_tax_call_stage_rms  s,
                  ordsku os
            where s.tax_service_id = I_tax_service_id
              and s.doc_id = os.order_no
              and ((s.item = os.item and s.pack_item is NULL) or s.pack_item = os.item)) r,
          item_master im,
          ordloc ol,
          item_supp_country_dim iscd,
          packitem pi
    where r.tax_service_id            = I_tax_service_id
      and r.item                      = im.item
      --
      and r.doc_id                    = ol.order_no
      and r.ordsku_item               = ol.item
      and r.to_entity                 = ol.location
      --
      and iscd.item(+)                = r.item
      and iscd.supplier(+)            = r.from_entity
      and iscd.origin_country(+)      = r.orig_cntry_id
      --
      and iscd.dim_object(+)          = 'EA'
      --
      and pi.item(+)                  = r.item
      and pi.pack_no(+)               = r.pack_item;

   --stage distinct location entities
   if STAGE_LOCATION(O_error_message,
                     I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   --stage suppliers
   if STAGE_SUPPLIER(O_error_message,
                     I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   --stage eco info for supplier
   select I_tax_service_id,
          entity_type,
          entity,
          cnae_code
   BULK COLLECT INTO LP_stage_eco_sup_tbl
     from (select DISTINCT
                  s.from_entity_type entity_type,
                  s.from_entity entity,
                  ecc.cnae_code
             from l10n_br_tax_call_stage_rms s,
                  l10n_br_entity_cnae_codes ecc
            where s.tax_service_id = I_tax_service_id
              and ecc.key_value_1  = s.from_entity
              and ecc.key_value_2  = 'S'
              and ecc.module       = 'SUPP'
              and ecc.cnae_code    is NOT NULL);

   --stage eco info for location
   select I_tax_service_id,
          entity_type,
          entity,
          cnae_code
   BULK COLLECT INTO LP_stage_eco_loc_tbl
     from (select DISTINCT
                  s.to_entity_type entity_type,
                  s.to_entity entity,
                  ecc.cnae_code
             from l10n_br_tax_call_stage_rms s,
                  l10n_br_entity_cnae_codes ecc
            where s.tax_service_id = I_tax_service_id
              and ecc.key_value_1  = s.to_entity
              and ecc.key_value_2  = DECODE(s.to_entity_type,'ST','S','WH','W'));


   --stage item info
   if STAGE_ITEM(O_error_message,
                 I_tax_service_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END STAGE_PO_OBJECT;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_REQUEST_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                          I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'L10N_BR_INT_SQL.GET_REQUEST_DATA';
   L_call_type    L10N_BR_TAX_CALL_STAGE_RMS.CALL_TYPE%TYPE := NULL;

   cursor C_CALL_TYPE is
      select MIN(call_type)
        from l10n_br_tax_call_stage_rms
       where tax_service_id = I_tax_service_id;

BEGIN

   open C_CALL_TYPE;
   fetch C_CALL_TYPE into L_call_type;
   if C_CALL_TYPE%NOTFOUND then
      close C_CALL_TYPE;
      O_error_message:= SQL_LIB.CREATE_MSG('L10N_STAGE_REC_NOT_FOUND', I_tax_service_id,
                                           NULL, NULL);
     return FALSE;
   end if;
   close C_CALL_TYPE;

   if L_call_type = 'PO' then
      if LOAD_PO_OBJECT(O_error_message,
                        O_businessObject,
                        I_tax_service_id) = FALSE then
         return FALSE;
      end if;
   elsif L_call_type = 'S' then
      if LOAD_SALE_OBJECT(O_error_message,
                          O_businessObject,
                          I_tax_service_id) = FALSE then
         return FALSE;
      end if;
   elsif L_call_type = 'P' then
      if LOAD_PURCHASE_OBJECT(O_error_message,
                              O_businessObject,
                              I_tax_service_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END GET_REQUEST_DATA;
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_SALE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                          I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'L10N_BR_INT_SQL.LOAD_SALE_OBJECT';

   L_from_entity        l10n_br_tax_call_stage_rms.from_entity%TYPE := NULL;
   L_from_entity_type   l10n_br_tax_call_stage_rms.from_entity_type%TYPE := NULL;
   L_to_entity          l10n_br_tax_call_stage_rms.to_entity%TYPE := NULL;
   L_to_entity_type     l10n_br_tax_call_stage_rms.to_entity_type%TYPE := NULL;
   L_effective_date     l10n_br_tax_call_stage_rms.effective_date%TYPE := NULL;

   L_chunk_id           l10n_br_tax_call_stage_rms.chunk_id%TYPE := NULL;
   L_prev_chunk_id      l10n_br_tax_call_stage_rms.chunk_id%TYPE := -1;

   L_tax_call_chunk_tbl "RIB_FiscDocChnkRBO_TBL" := "RIB_FiscDocChnkRBO_TBL"();
   L_tax_call_chunk_rec "RIB_FiscDocChnkRBO_REC" := NULL;

   L_tax_call_tbl    "RIB_FiscDocRBO_TBL" := "RIB_FiscDocRBO_TBL"();
   L_tax_call_rec    "RIB_FiscDocRBO_REC" := NULL;

   L_to_entity_tbl   "RIB_ToEntity_TBL" := "RIB_ToEntity_TBL"();
   L_to_entity_rec   "RIB_ToEntity_REC" := NULL;
   L_from_entity_tbl "RIB_FromEntity_TBL" := "RIB_FromEntity_TBL"();
   L_from_entity_rec "RIB_FromEntity_REC" := NULL;

   L_entities        "RIB_FiscEntityRBO_TBL" := NULL;
   L_items           "RIB_LineItemRBO_TBL" := NULL;

   cursor C_INPUT is
      select distinct
             from_entity,
             from_entity_type,
             to_entity,
             to_entity_type,
             effective_date,
             chunk_id
        from l10n_br_tax_call_stage_rms
       where tax_service_id  = I_tax_service_id
         and chunk_id       != 0
       order by chunk_id,
                effective_date,
                from_entity,
                to_entity;

   cursor C_ENTITY is
      select "RIB_FiscEntityRBO_REC"(
              0,--RIB_OID
              CAST(MULTISET(select "RIB_AddrRBO_REC"(
                                    0,             --RIB_OID
                                    city_id,       --CITY_ID
                                    state_name,    --STATE_NAME
                                    country_name,  --COUNTRY_NAME
                                    addr,          --ADDR
                                    addr_type,     --ADDR_TYPE
                                    primary_addr_type_ind,  --PRIMARY_ADDR_TYPE_IND
                                    primary_addr_ind,       --PRIMARY_ADDR_IND
                                    add_1,         --ADD_1
                                    add_2,         --ADD_2
                                    add_3,         --ADD_3
                                    city,          --CITY
                                    state,         --STATE
                                    country_id)    --COUNTRY_ID
                               from dual) as "RIB_AddrRBO_TBL"), --AddrRBO_TBL
              ---
              CAST(MULTISET(select "RIB_EcoClassCd_REC"(0,cnae_code)  --value
                              from l10n_br_tax_stage_eco
                             where tax_service_id = I_tax_service_id
                               and entity_type = L_from_entity_type
                               and entity_id = L_from_entity) as "RIB_EcoClassCd_TBL"), --EcoClassCd_TBL
              ---
              NULL,                     --DiffTaxRegime_TBL, n/a for LOCs
              L_from_entity,            --ENTITY_CODE
              L_from_entity_type,       --ENTITY_TYPE
              legal_name,               --LEGAL_NAME
              fiscal_type,              --FISCAL_TYPE
              is_simples_contributor,   --IS_SIMPLES_CONTRIBUTOR
              federal_tax_reg_id,       --FEDERAL_TAX_REG_ID
              is_rural_producer,        --IS_RURAL_PRODUCER
              is_income_range_eligible, --IS_INCOME_RANGE_ELIGIBLE, n/a for LOCs
              is_distr_a_manufacturer,  --IS_DISTR_A_MANUFACTURER, n/a for LOCs
              icms_simples_rate,        --ICMS_SIMPLES_RATE, n/a for LOCs
              ---
              CAST(MULTISET(select "RIB_TaxContributor_REC"(0,'ISS')
                              from l10n_br_tax_stage_fis_entity
                             where tax_service_id = I_tax_service_id
                               and entity_type = L_from_entity_type
                               and entity_id = L_from_entity
                               and iss_ind = 'Y'
                             UNION ALL
                            select "RIB_TaxContributor_REC"(0,'IPI')
                              from l10n_br_tax_stage_fis_entity
                             where tax_service_id = I_tax_service_id
                               and entity_type = L_from_entity_type
                               and entity_id = L_from_entity
                               and ipi_ind = 'Y'
                             UNION ALL
                            select "RIB_TaxContributor_REC"(0,'ICMS')
                              from l10n_br_tax_stage_fis_entity
                             where tax_service_id = I_tax_service_id
                               and entity_type = L_from_entity_type
                               and entity_id = L_from_entity
                               and icms_ind = 'Y') as "RIB_TaxContributor_TBL"),  --TAXCONTRIBUTOR_TBL
              ---
              CAST(MULTISET(select "RIB_NameValPairRBO_REC"(
                                    0,
                                    name,
                                    value)
                              from l10n_br_tax_stage_name_value
                             where tax_service_id = I_tax_service_id
                               and entity_type = 'LOC'
                               and entity_id = L_from_entity) as "RIB_NameValPairRBO_TBL"))  --NAMEVALPAIRRBO_TBL
         from l10n_br_tax_stage_fis_entity
        where tax_service_id = I_tax_service_id
          and entity_type = L_from_entity_type
          and entity_id = L_from_entity;

   cursor C_ITEMS is
      select "RIB_LineItemRBO_REC"(
              0,                     --RIB_OID
              document_line_id,      --DOCUMENT_LINE_ID
              item_id,               --ITEM_ID
              item_tran_Code,        --ITEM_TRAN_CODE
              item_type,             --ITEM_TYPE
              stg_item.quantity,     --QUANTITY
              unit_of_measure,       --UNIT_OF_MEASURE
              quantity_in_eaches,    --QUANTITY_IN_EACHES
              origin_doc_date,       --origin_doc_date
              pack_item_id,          --PACK_ITEM_ID
              stg_rms.amount,        --TOTAL_COST
              stg_rms.amount,        --UNIT_COST
              src_taxpayer_type,     --source_taxpayer_type
              orig_fiscal_doc_number,--orig_fiscal_doc_number
              orig_fiscal_doc_series,--orig_fiscal_doc_series
              ---
              dim_object,            --DIM_OBJECT
              length,                --length
              width,                 --width
              lwh_uom,               --LWH_UOM
              weight,                --WEIGHT
              net_weight,            --NET_WEIGHT
              weight_uom,            --WEIGHT_UOM
              liquid_volume,         --LIQUID_VOLUME
              liquid_volume_uom,     --liquid_volume_uom
              NULL,                  --freight, n/a to sale
              NULL,                  --insurance, n/a to sale
              NULL,                  --discount, n/a to sale
              NULL,                  --commision, n/a to sale
              NULL,                  --freight_type, n/a to sale
              NULL,                  --other_expenses, n/a to sale
              ---
              origin_fiscal_code_opr,     --origin_fiscal_code_opr
              deduced_fiscal_code_opr,    --deduced_fiscal_code_opr
              deduce_cfop_code,           --DEDUCE_CFOP_CODE
              icms_cst_code,              --icms_cst_code
              pis_cst_code,               --pis_cst_code
              cofins_cst_code,            --cofins_cst_code
              deduce_icms_cst_code,       --deduce_icms_cst_code
              deduce_pis_cst_code,        --deduce_pis_cst_codE
              deduce_cofins_cst_code,     --deduce_cofins_cst_code
              recoverable_icmsst,         --recoverable_icmsst
              item_cost_contains_cofins,  --ITEM_COST_CONTAINS_COFINS
              recoverable_base_icmsst,    --recoverable_base_icmsst
              item_cost_contains_pis,     --ITEM_COST_CONTAINS_PIS
              item_cost_contains_icms,    --ITEM_COST_CONTAINS_ICMS
              ---
              DECODE(service_ind,'N',CAST(MULTISET(select "RIB_PrdItemRBO_REC"(
                                                              0,                          --RIB_OID
                                                              item_id,                    --item_code
                                                              item_description,           --item_description
                                                              item_origin,                --item_origin
                                                              item_utilization,           --item_utilization
                                                              is_transformed_item,        --is_transformed_item
                                                              fiscal_classification_code, --fiscal_classification_code
                                                              ext_fiscal_class_code,      --ext_fiscal_class_code
                                                              ipi_exception_code,         --ipi_exception_code
                                                              product_type,               --product_type
                                                              state_of_manufacture,       --state_of_manufacture
                                                              pharma_list_type,           --pharma_list_type
                                                              CAST(MULTISET(select "RIB_NameValPairRBO_REC"(
                                                                                    0,
                                                                                    name,
                                                                                    value)
                                                                              from l10n_br_tax_stage_name_value
                                                                             where tax_service_id = I_tax_service_id
                                                                               and entity_type = 'ITEM'
                                                                               and entity_id = stg_rms.item) as "RIB_NameValPairRBO_TBL")) --NameValPairRBO_TBL
                                                         from dual)as "RIB_PrdItemRBO_TBL"),NULL), --PrdItemRBO_TBL
              DECODE(service_ind,'Y',CAST(MULTISET(select "RIB_SvcItemRBO_REC"(
                                                              0,                    --RIB_OID
                                                              item_id,              --item_code
                                                              item_description,     --item_description
                                                              ext_fiscal_class_code,--ext_fiscal_class_code
                                                              federal_service_code,  --federal_service_code
                                                              dst_service_code,     --destination_service_code
                                                              stg_loc.city_id,      --service_provider_city
                                                              CAST(MULTISET(select "RIB_NameValPairRBO_REC"(
                                                                                    0,
                                                                                    name,
                                                                                    value)
                                                                              from l10n_br_tax_stage_name_value
                                                                             where tax_service_id = I_tax_service_id
                                                                               and entity_type = 'ITEM'
                                                                               and entity_id = stg_rms.item) as "RIB_NameValPairRBO_TBL")) --NameValPairRBO_TBL
                                                         from dual)as "RIB_SvcItemRBO_TBL"),NULL), --SvcItemRBO_TBL
              NULL, --InformTaxRBO_TBL "RIB_InformTaxRBO_TBL",
              CAST(MULTISET(select "RIB_NameValPairRBO_REC"(
                                    0,
                                    name,
                                    value)
                              from l10n_br_tax_stage_name_value
                             where tax_service_id = I_tax_service_id
                               and entity_type = 'ITEM'
                               and entity_id = stg_rms.item) as "RIB_NameValPairRBO_TBL"))  --NAMEVALPAIRRBO_TBL
        from l10n_br_tax_call_stage_rms stg_rms,
             l10n_br_tax_stage_item stg_item,
             l10n_br_tax_stage_fis_entity stg_loc
       where stg_rms.tax_service_id = I_tax_service_id
         and stg_rms.chunk_id            = L_chunk_id
         and stg_rms.effective_date      = L_effective_date
         and stg_rms.from_entity_type    = L_from_entity_type
         and stg_rms.from_entity         = L_from_entity
         --
         and stg_item.item_group_counter = 1
         and stg_rms.tax_service_id      = stg_item.tax_service_id
         and stg_rms.item                = stg_item.item_id
         --
         and stg_rms.tax_service_id      = stg_loc.tax_service_id
         and stg_rms.from_entity         = stg_loc.entity_id
         and stg_rms.from_entity_type    = stg_loc.entity_type;

BEGIN
   for rec in C_INPUT LOOP

      --for sales use from entity for both from entity and to entity
      --valid entity types are 'ST' (including wholesale/franchise stores) and 'WH'
      --External finishers (partner) do not have retail (i.e. n/a for sales).
      L_from_entity       := rec.from_entity;
      L_from_entity_type  := rec.from_entity_type;
      L_effective_date    := rec.effective_date;

      L_chunk_id          := rec.chunk_id;

      if L_prev_chunk_id = -1 then
         L_prev_chunk_id := L_chunk_id;
      end if;

      if L_chunk_id != L_prev_chunk_id then

         L_tax_call_chunk_rec := "RIB_FiscDocChnkRBO_REC" (
                 RIB_OID => 0,
                 CHUNK_ID => L_prev_chunk_id,
                 FISCDOCRBO_TBL => L_tax_call_tbl);
         L_tax_call_chunk_tbl.EXTEND;
         L_tax_call_chunk_tbl(L_tax_call_chunk_tbl.COUNT) := L_tax_call_chunk_rec;

         L_prev_chunk_id := L_chunk_id;
         L_tax_call_tbl.DELETE;

      end if;

      open C_ENTITY;
      fetch C_ENTITY BULK COLLECT into L_entities;
      close C_ENTITY;

      if L_entities is NULL or L_entities.COUNT != 1 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ENTITY', I_tax_service_id, NULL, NULL);
         return false;
      end if;

      --reset for the new TaxCall record
      L_to_entity_tbl.DELETE;
      L_from_entity_tbl.DELETE;

      L_from_entity_rec := "RIB_FromEntity_REC"(0,L_entities);
      L_from_entity_tbl.EXTEND;
      L_from_entity_tbl(L_from_entity_tbl.COUNT) := L_from_entity_rec;

      L_to_entity_rec := "RIB_ToEntity_REC"(0,L_entities);
      L_to_entity_tbl.EXTEND;
      L_to_entity_tbl(L_to_entity_tbl.COUNT) := L_to_entity_rec;

      open C_ITEMS;
      fetch C_ITEMS BULK COLLECT into L_items;
      close C_ITEMS;

      if L_items is NULL or L_items.COUNT < 1 then
         O_error_message := SQL_LIB.CREATE_MSG('L10N_NO_FISC_ATTR_ITEM', L_from_entity, L_from_entity, NULL);
         return false;
      end if;

      L_tax_call_rec := "RIB_FiscDocRBO_REC"(
           RIB_OID => 0,
           TOENTITY_TBL => L_to_entity_tbl,
           FROMENTITY_TBL => L_from_entity_tbl,
           LineItemRBO_TBL => L_items,
           NameValPairRBO_TBL => NULL,
           DUE_DATE => L_effective_date,
           FISCAL_DOCUMENT_DATE => L_effective_date,
           DOCUMENT_TYPE => 'FT',
           GROSS_WEIGHT => NULL,
           NET_WEIGHT => NULL,
           OPERATION_TYPE => NULL,
           FREIGHT => NULL,
           INSURANCE => NULL,
           DISCOUNT => NULL,
           COMMISION => NULL,
           FREIGHT_TYPE => NULL,
           OTHER_EXPENSES => NULL,
           TOTAL_COST => NULL,
           TAX_AMOUNT => NULL,
           TAX_BASIS_AMOUNT => NULL,
           TAX_CODE => NULL,
           RECEIPT_DATE => L_effective_date,
           TRANSACTION_TYPE => 'O',
           IS_SUPPLIER_ISSUER => NULL,
           NO_HISTORY_TRACKED => NULL,
           PROCESS_INCONCLUSIVE_RULES => NULL,
           APPROXIMATION_MODE => NULL,
           DECIMAL_PRECISION => NULL,
           CALCULATION_STATUS => NULL,
           ENABLE_LOG => 'S',
           CALC_PROCESS_TYPE => 'REC',
           NATURE_OF_OPERATION => NULL,
           IGNORE_TAX_CALC_LIST => NULL,
           DOCUMENT_SERIES => 'ITEM',
           DOCUMENT_NUMBER => 492,
           InformTaxRBO_TBL => NULL);

      L_tax_call_tbl.EXTEND;
      L_tax_call_tbl(L_tax_call_tbl.COUNT) := L_tax_call_rec;

   end LOOP;

   --add last chunk
   L_tax_call_chunk_rec := "RIB_FiscDocChnkRBO_REC" (
           RIB_OID => 0,
           CHUNK_ID => L_chunk_id,
           FISCDOCRBO_TBL => L_tax_call_tbl);
   L_tax_call_chunk_tbl.EXTEND;
   L_tax_call_chunk_tbl(L_tax_call_chunk_tbl.COUNT) := L_tax_call_chunk_rec;

   O_businessObject := "RIB_FiscDocColRBM_REC" (
           RIB_OID => 0,
           FISCDOCCHNKRBO_TBL => L_tax_call_chunk_tbl,
           THREAD_USE => 'Y',
           LOGS => NULL,
           VENDOR_TYPE => NULL,
           COUNTRY => NULL,
           TRANSACTION_TYPE => NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
      return FALSE;
END LOAD_SALE_OBJECT;
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_PURCHASE_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                              I_tax_service_id  IN      NUMBER)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'L10N_BR_INT_SQL.LOAD_PURCHASE_OBJECT';
   L_from_entity             L10N_BR_TAX_CALL_STAGE_RMS.FROM_ENTITY%TYPE := NULL;
   L_from_entity_type        L10N_BR_TAX_CALL_STAGE_RMS.FROM_ENTITY_TYPE%TYPE := NULL;
   L_to_entity               L10N_BR_TAX_CALL_STAGE_RMS.TO_ENTITY%TYPE := NULL;
   L_to_entity_type          L10N_BR_TAX_CALL_STAGE_RMS.TO_ENTITY_TYPE%TYPE := NULL;
   L_effective_date          L10N_BR_TAX_CALL_STAGE_RMS.EFFECTIVE_DATE%TYPE := NULL;
   L_origin_country_id       L10N_BR_TAX_CALL_STAGE_RMS.ORIGIN_COUNTRY_ID%TYPE := NULL;
   L_chunk_id                L10N_BR_TAX_CALL_STAGE_RMS.CHUNK_ID%TYPE := NULL;
   L_prev_chunk_id           L10n_br_tax_call_stage_rms.chunk_id%TYPE := -1;

   L_tax_call_chunk_tbl "RIB_FiscDocChnkRBO_TBL" := "RIB_FiscDocChnkRBO_TBL"();
   L_tax_call_chunk_rec "RIB_FiscDocChnkRBO_REC" := NULL;
   L_nop                     FM_FISCAL_UTILIZATION.NOP%TYPE := NULL;
   L_incl_nic_ind_pis        VAT_CODES.INCL_NIC_IND%TYPE := NULL;
   L_incl_nic_ind_cofins     VAT_CODES.INCL_NIC_IND%TYPE := NULL;
   L_incl_nic_ind_icms       VAT_CODES.INCL_NIC_IND%TYPE := NULL;
   L_service_provider_city   COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE := NULL;

   L_tax_call_tbl            "RIB_FiscDocRBO_TBL" := "RIB_FiscDocRBO_TBL"();
   L_tax_call_rec            "RIB_FiscDocRBO_REC" := NULL;
   L_to_entity_tbl           "RIB_ToEntity_TBL" := "RIB_ToEntity_TBL"();
   L_to_entity_rec           "RIB_ToEntity_REC" := NULL;
   L_from_entity_tbl         "RIB_FromEntity_TBL" := "RIB_FromEntity_TBL"();
   L_from_entity_rec         "RIB_FromEntity_REC" := NULL;
   L_to_entities             "RIB_FiscEntityRBO_TBL" := NULL;
   L_from_entities           "RIB_FiscEntityRBO_TBL" := NULL;
   L_items                   "RIB_LineItemRBO_TBL":= NULL;

   L_sup_name_value_tbl      ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   L_loc_name_value_tbl      ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   L_item_name_value_tbl     ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();

   cursor C_NOP is
      select stg_oi.nop
        from l10n_br_tax_stage_order stg_oi
       where stg_oi.tax_service_id = I_tax_service_id;

   cursor C_INPUT is
      select distinct
             from_entity,
             from_entity_type,
             to_entity,
             to_entity_type,
             origin_country_id,
             effective_date,
             chunk_id
        from l10n_br_tax_call_stage_rms
       where tax_service_id  = I_tax_service_id
         and chunk_id       != 0
       order by chunk_id,
                from_entity,
                from_entity_type,
                to_entity,
                to_entity_type,
                origin_country_id,
                effective_date;

   cursor C_TO_ENTITIES is
      select "RIB_FiscEntityRBO_REC"(0,
              -- Address node
              CAST(MULTISET(select "RIB_AddrRBO_REC"(0,
                                    city_id,
                                    state_name,
                                    country_name,
                                    addr,
                                    addr_type,
                                    primary_addr_type_ind,
                                    primary_addr_ind,
                                    add_1,
                                    add_2,
                                    add_3,
                                    city,
                                    state,
                                    country_id)
                               from dual) as "RIB_AddrRBO_TBL"),
              CAST(MULTISET(select "RIB_EcoClassCd_REC"(0,cnae_code)
                              from l10n_br_tax_stage_eco
                             where tax_service_id = I_tax_service_id
                               and entity_id = L_to_entity
                               and entity_type = L_to_entity_type) as "RIB_EcoClassCd_TBL"),
              NULL,  --DiffTaxRegime_TBL, n/a to locations
              entity_id,
              entity_type,
              legal_name,
              fiscal_type,
              is_simples_contributor,
              federal_tax_reg_id,
              is_rural_producer,
              is_income_range_eligible,
              is_distr_a_manufacturer,
              icms_simples_rate,
              ---
              CAST(MULTISET(select "RIB_TaxContributor_REC"(0,'ISS')
                              from l10n_br_tax_stage_fis_entity
                             where tax_service_id = I_tax_service_id
                               and entity_type = L_to_entity_type
                               and entity_id = L_to_entity
                               and iss_ind = 'Y'
                             UNION ALL
                            select "RIB_TaxContributor_REC"(0,'IPI')
                              from l10n_br_tax_stage_fis_entity
                             where tax_service_id = I_tax_service_id
                               and entity_type = L_to_entity_type
                               and entity_id = L_to_entity
                               and ipi_ind = 'Y'
                             UNION ALL
                            select "RIB_TaxContributor_REC"(0,'ICMS')
                              from l10n_br_tax_stage_fis_entity
                             where tax_service_id = I_tax_service_id
                               and entity_type = L_to_entity_type
                               and entity_id = L_to_entity
                               and icms_ind = 'Y') as "RIB_TaxContributor_TBL"),  --TAXCONTRIBUTOR_TBL
              ---
              CAST(MULTISET(select "RIB_NameValPairRBO_REC"(0,
                                    name,
                                    value)
                              from l10n_br_tax_stage_name_value
                             where tax_service_id = I_tax_service_id
                               and entity_type = 'LOC'
                               and entity_id = L_to_entity) as "RIB_NameValPairRBO_TBL"))  --NAMEVALPAIRRBO_TBL
         from l10n_br_tax_stage_fis_entity
        where tax_service_id = I_tax_service_id
          and entity_type = L_to_entity_type
          and entity_id = L_to_entity;

   cursor C_FROM_ENTITIES is
      select "RIB_FiscEntityRBO_REC"(0,
             CAST(MULTISET(select "RIB_AddrRBO_REC"(0,
                                   city_id,
                                   state_name,
                                   country_name,
                                   addr,
                                   addr_type,
                                   primary_addr_type_ind,
                                   primary_addr_ind,
                                   add_1,
                                   add_2,
                                   add_3,
                                   city,
                                   state,
                                   country_id)
                              from dual) as "RIB_AddrRBO_TBL"),
             ---
             CAST(MULTISET(select "RIB_EcoClassCd_REC"(0,cnae_code)
                             from l10n_br_tax_stage_eco
                            where tax_service_id = I_tax_service_id
                              and entity_id = L_from_entity
                              and entity_type = L_from_entity_type) as "RIB_EcoClassCd_TBL"),
             ---
             CAST(MULTISET(select "RIB_DiffTaxRegime_REC"(0,tax_regime)
                             from l10n_br_tax_stage_regime
                            where tax_service_id = I_tax_service_id
                              and supplier = L_from_entity) as "RIB_DiffTaxRegime_TBL"),  -- DiffTaxRegime_TBL
             entity_id,
             entity_type,
             legal_name,
             fiscal_type,
             is_simples_contributor,
             federal_tax_reg_id,
             is_rural_producer,
             is_income_range_eligible,
             is_distr_a_manufacturer,
             icms_simples_rate,
             ---
             CAST(MULTISET(select "RIB_TaxContributor_REC"(0,'ISS')
                             from l10n_br_tax_stage_fis_entity
                            where tax_service_id = I_tax_service_id
                              and entity_type = L_from_entity_type
                              and entity_id = L_from_entity
                              and iss_ind = 'Y'
                            UNION ALL
                           select "RIB_TaxContributor_REC"(0,'IPI')
                             from l10n_br_tax_stage_fis_entity
                            where tax_service_id = I_tax_service_id
                              and entity_type = L_from_entity_type
                              and entity_id = L_from_entity
                              and ipi_ind = 'Y'
                            UNION ALL
                           select "RIB_TaxContributor_REC"(0,'ICMS')
                             from l10n_br_tax_stage_fis_entity
                            where tax_service_id = I_tax_service_id
                              and entity_type = L_from_entity_type
                              and entity_id = L_from_entity
                              and icms_ind = 'Y'
                            UNION ALL
                           select "RIB_TaxContributor_REC"(0,'ICMS')
                             from l10n_br_tax_stage_fis_entity
                            where tax_service_id = I_tax_service_id
                              and entity_type = L_from_entity_type
                              and entity_id = L_from_entity
                              and st_ind = 'Y') as "RIB_TaxContributor_TBL"),  --TAXCONTRIBUTOR_TBL
              ---
              CAST(MULTISET(select "RIB_NameValPairRBO_REC"(0,
                                    name,
                                    value)
                              from l10n_br_tax_stage_name_value
                             where tax_service_id = I_tax_service_id
                               and entity_type = 'SUPP'
                               and entity_id = L_from_entity) as "RIB_NameValPairRBO_TBL"))  --NAMEVALPAIRRBO_TBL
         from l10n_br_tax_stage_fis_entity
        where tax_service_id = I_tax_service_id
          and entity_type = L_from_entity_type
          and entity_id = L_from_entity;

   cursor C_ITEMS is
      select "RIB_LineItemRBO_REC"(0,  --RIB_OID
                                   stg_item.document_line_id,
                                   stg_item.item_id,
                                   stg_item.item_tran_code,
                                   stg_item.item_type,
                                   stg_item.quantity,
                                   stg_item.unit_of_measure,
                                   stg_item.quantity_in_eaches,
                                   stg_item.origin_doc_date,
                                   stg_item.pack_item_id,
                                   stg_item.total_cost,
                                   stg_item.unit_cost,
                                   stg_item.src_taxpayer_type,
                                   stg_item.orig_fiscal_doc_number,
                                   stg_item.orig_fiscal_doc_series,
                                   ---
                                   stg_oi.dim_object,
                                   stg_oi.length,
                                   stg_oi.width,
                                   stg_oi.lwh_uom,
                                   stg_oi.weight,
                                   stg_oi.net_weight,
                                   stg_oi.weight_uom,
                                   stg_oi.liquid_volume,
                                   stg_oi.liquid_volume_uom,
                                   ---
                                   NULL, --stg_item.freight,
                                   NULL, --stg_item.insurance,
                                   NULL, --stg_item.discount,
                                   NULL, --stg_item.commision,
                                   NULL, --stg_item.freight_type,
                                   NULL, --stg_item.other_expenses,
                                   ---
                                   stg_item.origin_fiscal_code_opr,
                                   stg_item.deduced_fiscal_code_opr,
                                   stg_item.deduce_cfop_code,
                                   stg_item.icms_cst_code,
                                   stg_item.pis_cst_code,
                                   stg_item.cofins_cst_code,
                                   stg_item.deduce_icms_cst_code,
                                   stg_item.deduce_pis_cst_code,
                                   stg_item.deduce_cofins_cst_code,
                                   stg_item.recoverable_icmsst,
                                   stg_item.item_cost_contains_cofins,
                                   stg_item.recoverable_base_icmsst,
                                   stg_item.item_cost_contains_pis,
                                   stg_item.item_cost_contains_icms,
                                   --
                                   -- RIB_LineItemRBO_REC can have either RIB_PrdItemRBO_REC or RIB_SvcItemRBO_REC populated
                                   -- Only one record is expected in either node
                                   DECODE(stg_item.service_ind,'N',CAST(MULTISET(select "RIB_PrdItemRBO_REC"(0,
                                                                                        stg_item.item_id,  --item_code,
                                                                                        stg_item.item_description,
                                                                                        stg_item.item_origin,
                                                                                        stg_item.item_utilization,
                                                                                        stg_item.is_transformed_item,
                                                                                        stg_item.fiscal_classification_code,
                                                                                        stg_item.ext_fiscal_class_code,
                                                                                        stg_item.ipi_exception_code,
                                                                                        stg_item.product_type,
                                                                                        stg_item.state_of_manufacture,
                                                                                        stg_item.pharma_list_type,
                                                                                        --Customer attributes are defined at item level, w/o distinguishing
                                                                                        --between Service Item vs Product Item attributes. Pass in all
                                                                                        --name/value pairs of customer attributes for the item. External
                                                                                        --tax engine that uses these attributes will determine if an
                                                                                        --attribute in the name/value pair is applicable to product item or not.
                                                                                        CAST(MULTISET(select "RIB_NameValPairRBO_REC"(0,
                                                                                                             name,
                                                                                                             value)
                                                                                                        from l10n_br_tax_stage_name_value
                                                                                                       where tax_service_id = I_tax_service_id
                                                                                                         and entity_type = 'ITEM'
                                                                                                         and entity_id = stg.item) as "RIB_NameValPairRBO_TBL")) --NameValPairRBO_TBL
                                                                                   from dual)as "RIB_PrdItemRBO_TBL"),NULL), --PrdItemRBO_TBL
                                   DECODE(stg_item.service_ind,'Y',CAST(MULTISET(select "RIB_SvcItemRBO_REC"(0,
                                                                                        stg_item.item_id,  --item_code,
                                                                                        stg_item.item_description,
                                                                                        stg_item.ext_fiscal_class_code,
                                                                                        stg_item.federal_service_code,
                                                                                        stg_item.dst_service_code,
                                                                                        L_service_provider_city, --service_provider_city
                                                                                        --Customer attributes are defined at item level, w/o distinguishing
                                                                                        --between Service @Item vs Product Item attributes. Pass in all
                                                                                        --name/value pairs of customer attributes for the item. External
                                                                                        --tax engine that uses these attributes will determine if an
                                                                                        --attribute in the name/value pair is applicable to service item or not.
                                                                                        CAST(MULTISET(select "RIB_NameValPairRBO_REC"(0,
                                                                                                             name,
                                                                                                             value)
                                                                                                        from l10n_br_tax_stage_name_value
                                                                                                       where tax_service_id = I_tax_service_id
                                                                                                         and entity_type = 'ITEM'
                                                                                                         and entity_id = stg.item) as "RIB_NameValPairRBO_TBL")) --NameValPairRBO_TBL
                                                                                   from dual)as "RIB_SvcItemRBO_TBL"),NULL), --SvcItemRBO_TBL
                                   NULL, --InformTaxRBO_TBL
                                   CAST(MULTISET(select "RIB_NameValPairRBO_REC"(0,
                                                        name,
                                                        value)
                                                   from l10n_br_tax_stage_name_value
                                                  where tax_service_id = I_tax_service_id
                                                    and entity_type = 'ITEM'
                                                    and entity_id = stg.item) as "RIB_NameValPairRBO_TBL"))  --NAMEVALPAIRRBO_TBL
        from l10n_br_tax_call_stage_rms stg,
             l10n_br_tax_stage_item stg_item,
             l10n_br_tax_stage_order_info stg_oi
       where stg.tax_service_id              = I_tax_service_id
         and stg.chunk_id                    = L_chunk_id
         and stg.from_entity_type            = L_from_entity_type
         and stg.from_entity                 = L_from_entity
         and stg.to_entity_type              = L_to_entity_type
         and stg.to_entity                   = L_to_entity
         and stg.effective_date              = L_effective_date
         and stg.origin_country_id           = L_origin_country_id
         ---
         and stg_item.tax_service_id         = stg.tax_service_id
         and stg_item.item_id                = stg.item
         --Items can be staged with different input amounts
         --Join on the cost to tie back the proper staged record
         and stg_item.unit_cost              = stg.amount
         ---
         and stg_oi.item_group_counter       = 1
         and stg_oi.tax_service_id           = stg.tax_service_id
         and stg_oi.item_id                  = stg.item
         and NVL(stg_oi.pack_item_id,'-999') = NVL(stg.pack_item,'-999')
         and stg_oi.to_entity                = stg.to_entity
         and stg_oi.to_entity_type           = stg.to_entity_type
         and stg_oi.unit_cost                = stg.amount
         and stg_oi.from_entity              = stg.from_entity
         and stg_oi.from_entity_type         = stg.from_entity_type
         and stg_oi.origin_country_id        = stg.origin_country_id
         ---
         and stg_oi.tax_service_id           = stg_item.tax_service_id
         and stg_oi.item_id                  = stg_item.item_id
         and NVL(stg_oi.pack_item_id,'-999') = NVL(stg_item.pack_item_id,'-999')
         and stg_oi.unit_cost                = stg_item.unit_cost;

BEGIN
   -- Loop through records in the staging table and build the following:
   --    - FROM location fiscal attribute record.  These are suppliers for a purchase tax call.
   --    - TO location fiscal attribute record.  These are locations or partners for a purchase tax call.
   --    - Item fiscal attribute records for the FROM and TO location
   for rec in C_INPUT LOOP

      L_from_entity       := rec.from_entity;
      L_from_entity_type  := rec.from_entity_type;
      L_to_entity         := rec.to_entity;
      L_to_entity_type    := rec.to_entity_type;
      L_effective_date    := rec.effective_date;
      L_origin_country_id := rec.origin_country_id;
      L_chunk_id          := rec.chunk_id;

      if L_prev_chunk_id = -1 then
         L_prev_chunk_id := L_chunk_id;
      end if;

      if L_chunk_id != L_prev_chunk_id then

         L_tax_call_chunk_rec := "RIB_FiscDocChnkRBO_REC" (
                 RIB_OID => 0,
                 CHUNK_ID => L_prev_chunk_id,
                 FISCDOCRBO_TBL => L_tax_call_tbl);
         L_tax_call_chunk_tbl.EXTEND;
         L_tax_call_chunk_tbl(L_tax_call_chunk_tbl.COUNT) := L_tax_call_chunk_rec;

         L_prev_chunk_id := L_chunk_id;
         L_tax_call_tbl.DELETE;

      end if;

      -- Create object for TO entity
      open C_TO_ENTITIES;
      fetch C_TO_ENTITIES BULK COLLECT into L_to_entities;
      close C_TO_ENTITIES;

      -- Return FALSE if no record found for TO entity
      if L_to_entities is NULL or L_to_entities.COUNT != 1 or
        (L_to_entities is NOT NULL and L_to_entities(1).AddrRBO_TBL is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('L10N_NO_FIS_ATR_ENTY_STWH', L_to_entity, NULL, NULL);
         return FALSE;
      end if;

      -- Create object for FROM entity
      open C_FROM_ENTITIES;
      fetch C_FROM_ENTITIES BULK COLLECT into L_from_entities;
      close C_FROM_ENTITIES;

      -- Return FALSE if no record found for TO entity
      if L_from_entities is NULL or L_from_entities.COUNT != 1 or
        (L_from_entities is NOT NULL and L_from_entities(1).AddrRBO_TBL is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('L10N_NO_FIS_ATR_ENTY_SUPP', L_from_entity, NULL, NULL);
         return FALSE;
      end if;

      -- Store the city_id of the supplier.  To be used for service items.
      L_service_provider_city := L_from_entities(1).AddrRBO_TBL(1).city_id;

      L_from_entity_rec := "RIB_FromEntity_REC"(0,L_from_entities);
      L_from_entity_tbl.EXTEND;
      L_from_entity_tbl(L_from_entity_tbl.COUNT) := L_from_entity_rec;

      L_to_entity_rec := "RIB_ToEntity_REC"(0,L_to_entities);
      L_to_entity_tbl.EXTEND;
      L_to_entity_tbl(L_to_entity_tbl.COUNT) := L_to_entity_rec;

      -- Clear out entity tables
      L_from_entities.DELETE;
      L_to_entities.DELETE;

      -- Create object for item
      open C_ITEMS;
      fetch C_ITEMS BULK COLLECT into L_items;
      close C_ITEMS;

      -- Return FALSE if no record found for item
      if L_items is NULL or L_items.COUNT < 1 then
         O_error_message := SQL_LIB.CREATE_MSG('L10N_NO_FISC_ATTR_ITEM', L_from_entity, L_to_entity, NULL);
         return FALSE;
      end if;

      -- Get the nature_of_operation for Purchase (Cost) tax call
      open C_NOP;
      fetch C_NOP into L_nop;
      if C_NOP%NOTFOUND then
         close C_NOP;
         O_error_message:= SQL_LIB.CREATE_MSG('L10N_NOP_NOT_FOUND', I_tax_service_id,
                                              NULL, NULL);
        return FALSE;
      end if;
      close C_NOP;

      -- Create a fiscal document object
      L_tax_call_rec := "RIB_FiscDocRBO_REC"(
           RIB_OID => 0,
           ToEntity_TBL => L_to_entity_tbl,
           FromEntity_TBL => L_from_entity_tbl,
           LineItemRBO_TBL => L_items,
           NameValPairRBO_TBL => NULL,
           due_date => L_effective_date,
           fiscal_document_date => L_effective_date,
           document_type => 'FT',
           gross_weight => NULL,
           net_weight => NULL,
           operation_type => NULL,
           freight => NULL,
           insurance => NULL,
           discount => NULL,
           commision => NULL,
           freight_type => NULL,
           other_expenses => NULL,
           total_cost => NULL,
           tax_amount => NULL,
           tax_basis_amount => NULL,
           tax_code => NULL,
           receipt_date => L_effective_date,
           transaction_type => 'I',
           is_supplier_issuer => NULL,
           no_history_tracked => NULL,
           process_inconclusive_rules => NULL,
           approximation_mode => NULL,
           decimal_precision => NULL,
           calculation_status => NULL,
           enable_log => 'S',
           calc_process_type => 'REC',
           nature_of_operation => L_nop,
           ignore_tax_calc_list => NULL,
           document_series => 'ITEM',
           document_number => 492,
           InformTaxRBO_TBL => NULL);

      -- Clear out the tables
      L_items.DELETE;
      L_to_entity_tbl.DELETE;
      L_from_entity_tbl.DELETE;

      -- Add fiscal the fiscal document record to the collection
      L_tax_call_tbl.EXTEND;
      L_tax_call_tbl(L_tax_call_tbl.COUNT) := L_tax_call_rec;

   end LOOP;

   --add last chunk
   L_tax_call_chunk_rec := "RIB_FiscDocChnkRBO_REC" (
           RIB_OID => 0,
           CHUNK_ID => L_chunk_id,
           FISCDOCRBO_TBL => L_tax_call_tbl);
   L_tax_call_chunk_tbl.EXTEND;
   L_tax_call_chunk_tbl(L_tax_call_chunk_tbl.COUNT) := L_tax_call_chunk_rec;


   O_businessObject := "RIB_FiscDocColRBM_REC" (
           RIB_OID => 0,
           FISCDOCCHNKRBO_TBL => L_tax_call_chunk_tbl,
           THREAD_USE => 'Y',
           LOGS => NULL,
           VENDOR_TYPE => NULL,
           COUNTRY => NULL,
           TRANSACTION_TYPE => NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_PURCHASE_OBJECT;
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_PO_OBJECT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_businessObject  IN OUT  "RIB_FiscDocColRBM_REC",
                        I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'L10N_BR_INT_SQL.LOAD_PO_OBJECT';

   L_from_entity        L10N_BR_TAX_CALL_STAGE_RMS.FROM_ENTITY%TYPE := NULL;
   L_from_entity_type   L10N_BR_TAX_CALL_STAGE_RMS.FROM_ENTITY_TYPE%TYPE := NULL;
   L_to_entity          L10N_BR_TAX_CALL_STAGE_RMS.TO_ENTITY%TYPE := NULL;
   L_to_entity_type     L10N_BR_TAX_CALL_STAGE_RMS.TO_ENTITY_TYPE%TYPE := NULL;
   L_effective_date     L10N_BR_TAX_CALL_STAGE_RMS.EFFECTIVE_DATE%TYPE := NULL;
   L_chunk_id           L10N_BR_TAX_CALL_STAGE_RMS.CHUNK_ID%TYPE := NULL;
   L_prev_chunk_id      l10n_br_tax_call_stage_rms.chunk_id%TYPE := -1;
   L_tax_call_chunk_tbl "RIB_FiscDocChnkRBO_TBL" := "RIB_FiscDocChnkRBO_TBL"();
   L_tax_call_chunk_rec "RIB_FiscDocChnkRBO_REC" := NULL;

   L_order_id           L10N_BR_TAX_CALL_STAGE_RMS.DOC_ID%TYPE := NULL;
   L_nop                FM_FISCAL_UTILIZATION.NOP%TYPE := NULL;
   L_city_id            L10N_BR_TAX_STAGE_FIS_ENTITY.CITY_ID%TYPE := NULL;

   L_tax_call_tbl       "RIB_FiscDocRBO_TBL" := "RIB_FiscDocRBO_TBL"();
   L_tax_call_rec       "RIB_FiscDocRBO_REC" := NULL;

   L_to_entity_tbl      "RIB_ToEntity_TBL"      := "RIB_ToEntity_TBL"();
   L_to_entity_rec      "RIB_ToEntity_REC"      := NULL;
   L_from_entity_tbl    "RIB_FromEntity_TBL"    := "RIB_FromEntity_TBL"();
   L_from_entity_rec    "RIB_FromEntity_REC"    := NULL;
   L_to_entities        "RIB_FiscEntityRBO_TBL" := "RIB_FiscEntityRBO_TBL"();
   L_from_entities      "RIB_FiscEntityRBO_TBL" := "RIB_FiscEntityRBO_TBL"();
   L_items              "RIB_LineItemRBO_TBL"   := NULL;

   cursor C_INPUT is
      select distinct
             r.from_entity,
             r.from_entity_type,
             r.to_entity,
             r.to_entity_type,
             r.effective_date,
             r.doc_id,
             fis.city_id,
             r.chunk_id
        from l10n_br_tax_call_stage_rms r,
             l10n_br_tax_stage_fis_entity fis
      where r.tax_service_id = I_tax_service_id
         and r.chunk_id      != 0
         --
         and r.tax_service_id = fis.tax_service_id
         and r.to_entity      = fis.entity_id
         and r.to_entity_type = fis.entity_type
       order by r.chunk_id,
                r.from_entity,
                r.from_entity_type,
                r.to_entity,
                r.to_entity_type,
                r.effective_date,
                r.doc_id,
                fis.city_id;

   cursor C_TO_ENTITIES is
      select "RIB_FiscEntityRBO_REC"(0, --rib_oid
                     --addrrbo_tbl
                     CAST(MULTISET(
                        select "RIB_AddrRBO_REC"(0,--RIB_OID
                                  fis.city_id,
                                  fis.state_name,
                                  fis.country_name,
                                  fis.addr,
                                  fis.addr_type,
                                  fis.primary_addr_type_ind,
                                  fis.primary_addr_ind,
                                  fis.add_1,
                                  fis.add_2,
                                  fis.add_3,
                                  fis.city,
                                  fis.state,
                                  fis.country_id)
                          from dual) as "RIB_AddrRBO_TBL"),
                     --ecoclasscd_tbl
                     CAST(MULTISET(
                        select "RIB_EcoClassCd_REC"(0,eco.cnae_code)
                          from l10n_br_tax_stage_eco eco
                         where eco.tax_service_id = fis.tax_service_id
                           and eco.entity_id      = fis.entity_id
                           and eco.entity_type    = fis.entity_type) as "RIB_EcoClassCd_TBL"),
                     null,                            --difftaxregime_tbl (not used for locations)
                     fis.entity_id,                   --entity_code
                     fis.entity_type,                 --entity_type
                     fis.legal_name,                  --legal_name
                     fis.fiscal_type,                 --fiscal_type
                     fis.is_simples_contributor,      --is_simples_contributor
                     fis.federal_tax_reg_id,          --federal_tax_reg_id
                     fis.is_rural_producer,           --is_rural_producer
                     fis.is_income_range_eligible,    --is_income_range_eligible
                     fis.is_distr_a_manufacturer,     --is_distr_a_manufacturer
                     fis.icms_simples_rate,           --icms_simples_rate
                     --taxcontributor_tbl
                     CAST(MULTISET(
                        select "RIB_TaxContributor_REC"(0,'ISS')
                          from dual where fis.iss_ind = 'Y'
                        union all
                        select "RIB_TaxContributor_REC"(0,'IPI')
                          from dual where fis.ipi_ind = 'Y'
                        union all
                        select "RIB_TaxContributor_REC"(0,'ICMS')
                          from dual where fis.icms_ind = 'Y') as "RIB_TaxContributor_TBL"),
                     --namevalpairrbo_tbl
                     CAST(MULTISET(
                        select "RIB_NameValPairRBO_REC"( 0, nv.name, nv.value)
                          from l10n_br_tax_stage_name_value nv
                         where nv.tax_service_id = fis.tax_service_id
                           and nv.entity_id      = fis.entity_id
                           and nv.entity_type    = fis.entity_type) as "RIB_NameValPairRBO_TBL"))
        from l10n_br_tax_stage_fis_entity fis
       where fis.tax_service_id = I_tax_service_id
         and fis.entity_id      = L_to_entity
         and fis.entity_type    = L_to_entity_type;

   cursor C_FROM_ENTITIES is
      select "RIB_FiscEntityRBO_REC"(0, --rib_oid
                     --addrrbo_tbl
                     CAST(MULTISET(
                        select "RIB_AddrRBO_REC"(0,--RIB_OID
                                  fis.city_id,
                                  fis.state_name,
                                  fis.country_name,
                                  fis.addr,
                                  fis.addr_type,
                                  fis.primary_addr_type_ind,
                                  fis.primary_addr_ind,
                                  fis.add_1,
                                  fis.add_2,
                                  fis.add_3,
                                  fis.city,
                                  fis.state,
                                  fis.country_id)
                          from dual) as "RIB_AddrRBO_TBL"),
                     --ecoclasscd_tbl
                     CAST(MULTISET(
                        select "RIB_EcoClassCd_REC"(0,eco.cnae_code)
                          from l10n_br_tax_stage_eco eco
                         where eco.tax_service_id = fis.tax_service_id
                           and eco.entity_id      = fis.entity_id
                           and eco.entity_type    = fis.entity_type) as "RIB_EcoClassCd_TBL"),
                     --difftaxregime_tbl
                     CAST(MULTISET(
                        select "RIB_DiffTaxRegime_REC"(0,tr.tax_regime)
                          from l10n_br_tax_stage_regime tr
                         where tr.tax_service_id = fis.tax_service_id
                           and tr.supplier       = fis.entity_id) as "RIB_DiffTaxRegime_TBL"),
                     fis.entity_id,                   --entity_code
                     fis.entity_type,                 --entity_type
                     fis.legal_name,                  --legal_name
                     fis.fiscal_type,                 --fiscal_type
                     fis.is_simples_contributor,      --is_simples_contributor
                     fis.federal_tax_reg_id,          --federal_tax_reg_id
                     fis.is_rural_producer,           --is_rural_producer
                     fis.is_income_range_eligible,    --is_income_range_eligible
                     fis.is_distr_a_manufacturer,     --is_distr_a_manufacturer
                     fis.icms_simples_rate,           --icms_simples_rate
                     --taxcontributor_tbl
                     CAST(MULTISET(
                        select "RIB_TaxContributor_REC"(0,'ISS')
                          from dual where fis.iss_ind = 'Y'
                        union all
                        select "RIB_TaxContributor_REC"(0,'IPI')
                          from dual where fis.ipi_ind = 'Y'
                        union all
                        select "RIB_TaxContributor_REC"(0,'ICMS')
                          from dual where fis.icms_ind = 'Y'
                        union all
                        select "RIB_TaxContributor_REC"(0,'ST')
                          from dual where fis.st_ind = 'Y') as "RIB_TaxContributor_TBL"),
                     --namevalpairrbo_tbl
                     CAST(MULTISET(
                        select "RIB_NameValPairRBO_REC"( 0, nv.name, nv.value)
                          from l10n_br_tax_stage_name_value nv
                         where nv.tax_service_id = fis.tax_service_id
                           and nv.entity_id      = fis.entity_id
                           and nv.entity_type    = fis.entity_type) as "RIB_NameValPairRBO_TBL"))
        from l10n_br_tax_stage_fis_entity fis
       where fis.tax_service_id = I_tax_service_id
         and fis.entity_id      = L_from_entity
         and fis.entity_type    = L_from_entity_type;

   cursor C_ITEMS is
      select "RIB_LineItemRBO_REC"(
          0,                             --rib_oid
          si.document_line_id,           --document_line_id
          si.item_id,                    --item_id
          si.item_tran_code,             --item_tran_code
          si.item_type,                  --item_type
          oi.quantity,                   --quantity
          si.unit_of_measure,            --unit_of_measure
          oi.quantity_in_eaches * NVL(si.uom_conv_factor, 1),         --quantity_in_eaches
          si.origin_doc_date,            --origin_doc_date
          si.pack_item_id,               --pack_item_id
          si.total_cost,                 --total_cost
          si.unit_cost,                  --unit_cost
          si.src_taxpayer_type,          --src_taxpayer_type
          si.orig_fiscal_doc_number,     --orig_fiscal_doc_number
          si.orig_fiscal_doc_series,     --orig_fiscal_doc_series
          oi.dim_object,                 --dim_object
          oi.length,                     --length
          oi.width,                      --width
          oi.lwh_uom,                    --lwh_uom
          oi.weight,                     --weight
          oi.net_weight,                 --net_weight
          oi.weight_uom,                 --weight_uom
          oi.liquid_volume,              --liquid_volume
          oi.liquid_volume_uom,          --liquid_volume_uom
          exp.freight,                   --freight
          exp.insurance,                 --insurance
          exp.discount,                  --discount
          NULL,                          --commision
          NULL,                          --freight_type
          exp.other_expenses,            --other_expenses
          si.origin_fiscal_code_opr,     --origin_fiscal_code_opr
          si.deduced_fiscal_code_opr,    --deduced_fiscal_code_opr
          si.deduce_cfop_code,           --deduce_cfop_code
          si.icms_cst_code,              --icms_cst_code
          si.pis_cst_code,               --pis_cst_code
          si.cofins_cst_code,            --cofins_cst_code
          si.deduce_icms_cst_code,       --deduce_icms_cst_code
          si.deduce_pis_cst_code,        --deduce_pis_cst_code
          si.deduce_cofins_cst_code,     --deduce_cofins_cst_code
          si.recoverable_icmsst,         --recoverable_icmsst
          si.item_cost_contains_cofins,  --item_cost_contains_cofins
          si.recoverable_base_icmsst,    --recoverable_base_icmsst
          si.item_cost_contains_pis,     --item_cost_contains_pis
          si.item_cost_contains_icms,    --item_cost_contains_icms
          --prditemrbo_tbl
          decode(si.service_ind,'Y',null,
             CAST(MULTISET(
                  select "RIB_PrdItemRBO_REC"(
                           0,                             --rib_oid
                           si.item_id,                    --item_code
                           si.item_description,           --item_description
                           si.item_origin,                --item_origin
                           si.item_utilization,           --item_utilization
                           si.is_transformed_item,        --is_transformed_item
                           si.fiscal_classification_code, --fiscal_classification_code
                           si.ext_fiscal_class_code,      --ext_fiscal_class_code
                           si.ipi_exception_code,         --ipi_exception_code
                           si.product_type,               --product_type
                           si.state_of_manufacture,       --state_of_manufacture
                           si.pharma_list_type,           --pharma_list_type
                           null)                          --namevalpairrbo_tbl
                    from dual) as "RIB_PrdItemRBO_TBL")),
          --svcitemrbo_tbl
          decode(si.service_ind,'N',null,
             CAST(MULTISET(
                  select "RIB_SvcItemRBO_REC"(
                           0,                        --rib_oid
                           si.item_id,               --item_code
                           si.item_description,      --item_description
                           si.ext_fiscal_class_code, --ext_fiscal_class_code
                           si.federal_service_code,  --federal_service_code
                           si.dst_service_code,      --dst_service_code
                           L_city_id,                --service_provider_city
                           null)                     --namevalpairrbo_tbl
                    from dual) as "RIB_SvcItemRBO_TBL")),
          null,                          --informtaxrbo_tbl
          --namevalpairrbo_tbl
          CAST(MULTISET(
               select "RIB_NameValPairRBO_REC"( 0, nv.name, nv.value)
                 from l10n_br_tax_stage_name_value nv
                where nv.tax_service_id = si.tax_service_id
                  and nv.entity_id      = si.item_id
                  and nv.entity_type    = 'ITEM') as "RIB_NameValPairRBO_TBL"))
        from l10n_br_tax_call_stage_rms stg,
             l10n_br_tax_stage_item si,
             l10n_br_tax_stage_order_info oi,
             l10n_br_tax_stage_order_exp exp
       where stg.tax_service_id          = I_tax_service_id
         and stg.chunk_id                = L_chunk_id
         and stg.doc_id                  = L_order_id
         and stg.to_entity_type          = L_to_entity_type
         and stg.to_entity               = L_to_entity
         and stg.from_entity_type        = L_from_entity_type
         and stg.from_entity             = L_from_entity
         and stg.origin_country_id       = 'BR'
         --
         and si.tax_service_id           = stg.tax_service_id
         and si.item_id                  = stg.item
         and si.quantity                 = stg.quantity
         and NVL(si.unit_cost, -999)     = NVL(stg.amount, -999)
         --
         and oi.item_group_counter       = 1
         and oi.tax_service_id           = stg.tax_service_id
         and oi.doc_id                   = stg.doc_id
         and oi.to_entity                = stg.to_entity
         and oi.to_entity_type           = stg.to_entity_type
         and oi.from_entity              = stg.from_entity
         and oi.from_entity_type         = stg.from_entity_type
         and oi.origin_country_id        = stg.origin_country_id
         --
         and oi.tax_service_id           = si.tax_service_id
         and oi.item_id                  = si.item_id 
         and NVL(oi.pack_item_id,'-999') = NVL(si.pack_item_id, '-999')
         and oi.unit_cost                = si.unit_cost
         --
         and exp.tax_service_id(+)       = stg.tax_service_id
         and exp.order_no(+)             = stg.doc_id
         and exp.item (+)                = stg.item
         and nvl(exp.pack_no(+),'-999')  = nvl(stg.pack_item, '-999')
         and exp.location(+)             = stg.to_entity;

   cursor C_NOP is
      select o.nop
        from l10n_br_tax_stage_order o
       where o.tax_service_id = I_tax_service_id
         and o.order_no       = L_order_id;
BEGIN
   for rec in C_INPUT LOOP
      L_from_entity       := rec.from_entity;
      L_from_entity_type  := rec.from_entity_type;
      L_to_entity         := rec.to_entity;
      L_to_entity_type    := rec.to_entity_type;
      L_effective_date    := rec.effective_date;
      L_order_id          := rec.doc_id;
      L_city_id           := rec.city_id;
      L_chunk_id          := rec.chunk_id;

      if L_prev_chunk_id = -1 then
         L_prev_chunk_id := L_chunk_id;
      end if;

      if L_chunk_id != L_prev_chunk_id then

         L_tax_call_chunk_rec := "RIB_FiscDocChnkRBO_REC" (
                 RIB_OID        => 0,
                 CHUNK_ID       => L_prev_chunk_id,
                 FISCDOCRBO_TBL => L_tax_call_tbl);
         L_tax_call_chunk_tbl.EXTEND;
         L_tax_call_chunk_tbl(L_tax_call_chunk_tbl.COUNT) := L_tax_call_chunk_rec;

         L_prev_chunk_id := L_chunk_id;
         L_tax_call_tbl.DELETE;

      end if;

      -- Create object for TO entity
      open C_TO_ENTITIES;
      fetch C_TO_ENTITIES BULK COLLECT into L_to_entities;
      close C_TO_ENTITIES;

      -- Return FALSE if no record found for TO entity
      if L_to_entities is NULL or L_to_entities.COUNT != 1 or
        (L_to_entities is NOT NULL and L_to_entities(1).AddrRBO_TBL is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('L10N_NO_FIS_ATR_ENTY_STWH', I_tax_service_id, NULL, NULL);
         return false;
      end if;

      -- Create object for FROM entity
      open C_FROM_ENTITIES;
      fetch C_FROM_ENTITIES BULK COLLECT into L_from_entities;
      close C_FROM_ENTITIES;

      -- Return FALSE if no record found for TO entity
      if L_from_entities is NULL or L_from_entities.COUNT != 1 or
        (L_from_entities is NOT NULL and L_from_entities(1).AddrRBO_TBL is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('L10N_NO_FIS_ATR_ENTY_SUPP', I_tax_service_id, NULL, NULL);
         return false;
      end if;

      --reset for the new TaxCall record
      L_to_entity_tbl.DELETE;
      L_from_entity_tbl.DELETE;

      L_from_entity_rec := "RIB_FromEntity_REC"(0,L_from_entities);
      L_from_entity_tbl.EXTEND;
      L_from_entity_tbl(L_from_entity_tbl.COUNT) := L_from_entity_rec;

      L_to_entity_rec := "RIB_ToEntity_REC"(0,L_to_entities);
      L_to_entity_tbl.EXTEND;
      L_to_entity_tbl(L_to_entity_tbl.COUNT) := L_to_entity_rec;

      open C_ITEMS;
      fetch C_ITEMS BULK COLLECT into L_items;
      close C_ITEMS;

      if L_items is NULL or L_items.COUNT < 1 then
         O_error_message := SQL_LIB.CREATE_MSG('L10N_NO_FISC_ATTR_ITEM', L_from_entity, L_to_entity, NULL);
         return false;
      end if;

      open C_NOP;
      fetch C_NOP into L_nop;
      close C_NOP;

      -- Create a fiscal document object
      L_tax_call_rec := "RIB_FiscDocRBO_REC"(
                         RIB_OID                     => 0,
                         TOENTITY_TBL                => L_to_entity_tbl,
                         FROMENTITY_TBL              => L_from_entity_tbl,
                         LINEITEMRBO_TBL             => L_items,
                         NAMEVALPAIRRBO_TBL          => NULL,
                         DUE_DATE                    => L_effective_date,
                         FISCAL_DOCUMENT_DATE        => L_effective_date,
                         DOCUMENT_TYPE               => 'FT',
                         GROSS_WEIGHT                => NULL,
                         NET_WEIGHT                  => NULL,
                         OPERATION_TYPE              => NULL,
                         FREIGHT                     => NULL,
                         INSURANCE                   => NULL,
                         DISCOUNT                    => NULL,
                         COMMISION                   => NULL,
                         FREIGHT_TYPE                => NULL,
                         OTHER_EXPENSES              => NULL,
                         TOTAL_COST                  => NULL,
                         TAX_AMOUNT                  => NULL,
                         TAX_BASIS_AMOUNT            => NULL,
                         TAX_CODE                    => NULL,
                         RECEIPT_DATE                => L_effective_date,
                         TRANSACTION_TYPE            => 'I',
                         IS_SUPPLIER_ISSUER          => NULL,
                         NO_HISTORY_TRACKED          => NULL,
                         PROCESS_INCONCLUSIVE_RULES  => NULL,
                         APPROXIMATION_MODE          => NULL,
                         DECIMAL_PRECISION           => NULL,
                         CALCULATION_STATUS          => NULL,
                         ENABLE_LOG                  => 'S',
                         CALC_PROCESS_TYPE           => 'REC',
                         NATURE_OF_OPERATION         => L_nop,
                         IGNORE_TAX_CALC_LIST        => NULL,
                         DOCUMENT_SERIES             => 'ORD',
                         DOCUMENT_NUMBER             => L_order_id,
                         INFORMTAXRBO_TBL            => NULL);

      -- Add the fiscal document record to the collection
      L_tax_call_tbl.EXTEND;
      L_tax_call_tbl(L_tax_call_tbl.COUNT) := L_tax_call_rec;
      
      if L_chunk_id != L_prev_chunk_id then

         L_tax_call_chunk_rec := "RIB_FiscDocChnkRBO_REC" (
                 RIB_OID => 0,
                 CHUNK_ID => L_chunk_id,
                 FISCDOCRBO_TBL => L_tax_call_tbl);
         L_tax_call_chunk_tbl.EXTEND;
         L_tax_call_chunk_tbl(L_tax_call_chunk_tbl.COUNT) := L_tax_call_chunk_rec;

      end if;
      
   end LOOP;

   --add last chunk
   L_tax_call_chunk_rec := "RIB_FiscDocChnkRBO_REC" (
           RIB_OID => 0,
           CHUNK_ID => L_chunk_id,
           FISCDOCRBO_TBL => L_tax_call_tbl);
   L_tax_call_chunk_tbl.EXTEND;
   L_tax_call_chunk_tbl(L_tax_call_chunk_tbl.COUNT) := L_tax_call_chunk_rec;

   O_businessObject := "RIB_FiscDocColRBM_REC" (
           RIB_OID => 0,
           FISCDOCCHNKRBO_TBL => L_tax_call_chunk_tbl,
           THREAD_USE => 'Y',
           LOGS => NULL,
           VENDOR_TYPE => NULL,
           COUNTRY => NULL,
           TRANSACTION_TYPE => NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END LOAD_PO_OBJECT;
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_RESULTS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                       I_businessObject  IN      "RIB_FiscDocTaxColRBM_REC")
RETURN BOOLEAN IS

   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program        VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_RESULTS';
   L_result_tbl     RESULT_DATA_TBL;
   L_result_ctr     NUMBER := NULL;
   L_eco_tbl        RESULT_ECO_TBL;
   L_eco_ctr        NUMBER := NULL;
   L_contrib_tbl    RESULT_CONTRIB_TBL;
   L_contrib_ctr    NUMBER := NULL;
   L_item_tbl       RESULT_DATA_ITEM_TBL;
   L_item_ctr       NUMBER := NULL;
   L_tax_tbl        RESULT_DATA_ITEM_TAX_TBL;
   L_tax_ctr        NUMBER := NULL;
   L_sug_tbl        RESULT_DATA_ITEM_SUG_RULE_TBL;
   L_sug_ctr        NUMBER := NULL;
   L_srvc_prd_tbl   RESULT_DATA_ITEM_SRVC_PRD;
   L_srvc_prd_ctr   NUMBER := NULL;

   L_tax_result_seq NUMBER := 1;

BEGIN

   if I_businessObject is not NULL and
      I_businessObject.FiscDocChnkTaxRBO_TBL is not NULL and
      I_businessObject.FiscDocChnkTaxRBO_TBL.COUNT > 0 then

   L_result_ctr := 1;
   L_tax_result_seq := 1;

   for h in I_businessObject.FiscDocChnkTaxRBO_TBL.FIRST..I_businessObject.FiscDocChnkTaxRBO_TBL.LAST LOOP

   if I_businessObject is not NULL and
      I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL is not NULL and
      I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL.COUNT > 0 then

      for i in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL.FIRST..I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL.LAST LOOP

         L_result_tbl(L_result_ctr).tax_service_id := I_tax_service_id;
         L_result_tbl(L_result_ctr).tax_result_seq := L_tax_result_seq;
         L_result_tbl(L_result_ctr).document_number := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).document_number;
         L_result_tbl(L_result_ctr).fiscal_document_date := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).fiscal_document_date;
         L_result_tbl(L_result_ctr).src_entity_code := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).src_entity_code;
         L_result_tbl(L_result_ctr).src_entity_type := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).src_entity_type;
         L_result_tbl(L_result_ctr).src_addr_city_id := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).src_addr_city_id;
         L_result_tbl(L_result_ctr).src_federal_tax_reg_id := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).src_federal_tax_reg_id;
         L_result_tbl(L_result_ctr).dst_entity_code := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).dst_entity_code;
         L_result_tbl(L_result_ctr).dst_entity_type := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).dst_entity_type;
         L_result_tbl(L_result_ctr).dst_addr_city_id := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).dst_addr_city_id;
         L_result_tbl(L_result_ctr).dst_federal_tax_reg_id := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).dst_federal_tax_reg_id;
         L_result_tbl(L_result_ctr).disc_merch_cost := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).disc_merch_cost;
         L_result_tbl(L_result_ctr).total_cost := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).total_cost;
         L_result_tbl(L_result_ctr).total_services_cost := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).total_services_cost;
         L_result_tbl(L_result_ctr).calculation_status := I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).calculation_status;

         if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcEcoClassCd_TBL is not NULL and
            I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcEcoClassCd_TBL.COUNT > 0 then
            if L_eco_ctr is NULL then L_eco_ctr := 1; end if;
            for j in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcEcoClassCd_TBL.FIRST..
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcEcoClassCd_TBL.LAST LOOP
               L_eco_tbl(L_eco_ctr).tax_service_id         := I_tax_service_id;
               L_eco_tbl(L_eco_ctr).tax_result_seq         := L_tax_result_seq;
               L_eco_tbl(L_eco_ctr).source_destination_ind := 'S';
               L_eco_tbl(L_eco_ctr).value                  :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcEcoClassCd_TBL(j).value;
               L_eco_ctr := L_eco_ctr + 1;
            end LOOP;
         end if;

         if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstEcoClassCd_TBL is not NULL and
            I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstEcoClassCd_TBL.COUNT > 0 then
            if L_eco_ctr is NULL then L_eco_ctr := 1; end if;
            for j in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstEcoClassCd_TBL.FIRST..
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstEcoClassCd_TBL.LAST LOOP
               L_eco_tbl(L_eco_ctr).tax_service_id         := I_tax_service_id;
               L_eco_tbl(L_eco_ctr).tax_result_seq         := L_tax_result_seq;
               L_eco_tbl(L_eco_ctr).source_destination_ind := 'D';
               L_eco_tbl(L_eco_ctr).value                  :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstEcoClassCd_TBL(j).value;
               L_eco_ctr := L_eco_ctr + 1;
            end LOOP;
         end if;

         if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcTaxContributor_TBL is not NULL and
            I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcTaxContributor_TBL.COUNT > 0 then
            if L_contrib_ctr is NULL then L_contrib_ctr := 1; end if;
            for j in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcTaxContributor_TBL.FIRST..
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcTaxContributor_TBL.LAST LOOP
               L_contrib_tbl(L_contrib_ctr).tax_service_id         := I_tax_service_id;
               L_contrib_tbl(L_contrib_ctr).tax_result_seq         := L_tax_result_seq;
               L_contrib_tbl(L_contrib_ctr).source_destination_ind := 'S';
               L_contrib_tbl(L_contrib_ctr).value                  :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).SrcTaxContributor_TBL(j).value;
               L_contrib_ctr := L_contrib_ctr + 1;
            end LOOP;
         end if;

         if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstTaxContributor_TBL is not NULL and
            I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstTaxContributor_TBL.COUNT > 0 then
            if L_contrib_ctr is NULL then L_contrib_ctr := 1; end if;
            for j in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstTaxContributor_TBL.FIRST..
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstTaxContributor_TBL.LAST LOOP
               L_contrib_tbl(L_contrib_ctr).tax_service_id         := I_tax_service_id;
               L_contrib_tbl(L_contrib_ctr).tax_result_seq         := L_tax_result_seq;
               L_contrib_tbl(L_contrib_ctr).source_destination_ind := 'D';
               L_contrib_tbl(L_contrib_ctr).value                  :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).DstTaxContributor_TBL(j).value;
               L_contrib_ctr := L_contrib_ctr + 1;
            end LOOP;
         end if;

         if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL is not NULL and
            I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL.COUNT > 0 then
            if L_item_ctr is NULL then L_item_ctr := 1; end if;
            for j in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL.FIRST..
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL.LAST LOOP
               L_item_tbl(L_item_ctr).tax_service_id := I_tax_service_id;
               L_item_tbl(L_item_ctr).tax_result_seq := L_tax_result_seq;
               L_item_tbl(L_item_ctr).tax_item_seq   := j;
               L_item_tbl(L_item_ctr).item_id :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).item_id;
               L_item_tbl(L_item_ctr).pack_item_id :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).pack_item_id;
               L_item_tbl(L_item_ctr).item_tran_code :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).item_tran_code;
               L_item_tbl(L_item_ctr).taxed_quantity :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).taxed_quantity;
               L_item_tbl(L_item_ctr).taxed_quantity_uom :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).taxed_quantity_uom;
               L_item_tbl(L_item_ctr).total_cost :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).total_cost;
               L_item_tbl(L_item_ctr).deduced_fiscal_code_opr :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).deduced_fiscal_code_opr;
               L_item_tbl(L_item_ctr).icms_cst_code :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).icms_cst_code;
               L_item_tbl(L_item_ctr).pis_cst_code :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).pis_cst_code;
               L_item_tbl(L_item_ctr).cofins_cst_code :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).cofins_cst_code;
               L_item_tbl(L_item_ctr).deduce_icms_cst_code :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).deduce_icms_cst_code;
               L_item_tbl(L_item_ctr).deduce_pis_cst_code :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).deduce_pis_cst_code;
               L_item_tbl(L_item_ctr).deduce_cofins_cst_code :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).deduce_cofins_cst_code;
               L_item_tbl(L_item_ctr).recoverable_icmsst :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).recoverable_icmsst;
               L_item_tbl(L_item_ctr).total_cost_with_icms :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).total_cost_with_icms;
               L_item_tbl(L_item_ctr).unit_cost_with_icms :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).unit_cost_with_icms;
               L_item_tbl(L_item_ctr).recoverable_base_icmsst :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).recoverable_base_icmsst;
               L_item_tbl(L_item_ctr).unit_cost :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).unit_cost;
               L_item_tbl(L_item_ctr).dim_object :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).dim_object;
               L_item_tbl(L_item_ctr).length :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).length;
               L_item_tbl(L_item_ctr).width :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).width;
               L_item_tbl(L_item_ctr).lwh_uom :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).lwh_uom;
               L_item_tbl(L_item_ctr).weight :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).weight;
               L_item_tbl(L_item_ctr).net_weight :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).net_weight;
               L_item_tbl(L_item_ctr).weight_uom :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).weight_uom;
               L_item_tbl(L_item_ctr).liquid_volume :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).liquid_volume;
               L_item_tbl(L_item_ctr).liquid_volume_uom :=
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).liquid_volume_uom;

               if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).SvcItemTaxRBO_TBL is not NULL and
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).SvcItemTaxRBO_TBL.COUNT > 0 then
                  if L_srvc_prd_ctr is NULL then L_srvc_prd_ctr := 1; end if;
                  for k in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).SvcItemTaxRBO_TBL.FIRST..
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).SvcItemTaxRBO_TBL.LAST LOOP
                     L_srvc_prd_tbl(L_srvc_prd_ctr).tax_service_id      := I_tax_service_id;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).tax_result_seq      := L_tax_result_seq;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).tax_item_seq        := j;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).federal_service_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).SvcItemTaxRBO_TBL(k).federal_service_code;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).service_product_item_ind := 'S';
                     L_srvc_prd_ctr := L_srvc_prd_ctr + 1;
                  end LOOP;
               end if;

               if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).PrdItemTaxRBO_TBL is not NULL and
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).PrdItemTaxRBO_TBL.COUNT > 0 then
                  if L_srvc_prd_ctr is NULL then L_srvc_prd_ctr := 1; end if;
                  for k in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).PrdItemTaxRBO_TBL.FIRST..
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).PrdItemTaxRBO_TBL.LAST LOOP
                     L_srvc_prd_tbl(L_srvc_prd_ctr).tax_service_id      := I_tax_service_id;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).tax_result_seq      := L_tax_result_seq;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).tax_item_seq        := j;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).item_origin :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).PrdItemTaxRBO_TBL(k).item_origin;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).fiscal_classification_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).PrdItemTaxRBO_TBL(k).fiscal_classification_code;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).ipi_exception_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).PrdItemTaxRBO_TBL(k).ipi_exception_code;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).ext_fiscal_class_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).PrdItemTaxRBO_TBL(k).ext_fiscal_class_code;
                     L_srvc_prd_tbl(L_srvc_prd_ctr).service_product_item_ind := 'P';
                     L_srvc_prd_ctr := L_srvc_prd_ctr + 1;
                  end LOOP;
               end if;

               if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL is not NULL and
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL.COUNT > 0 then
                  if L_tax_ctr is NULL then L_tax_ctr := 1; end if;
                  for k in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL.FIRST..
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL.LAST LOOP

                     L_tax_tbl(L_tax_ctr).tax_service_id      := I_tax_service_id;
                     L_tax_tbl(L_tax_ctr).tax_result_seq      := L_tax_result_seq;
                     L_tax_tbl(L_tax_ctr).tax_item_seq        := j;
                     L_tax_tbl(L_tax_ctr).legal_message :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).legal_message;
                     L_tax_tbl(L_tax_ctr).tax_amount :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_amount;
                     L_tax_tbl(L_tax_ctr).tax_basis_amount :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_basis_amount;
                     L_tax_tbl(L_tax_ctr).unit_tax_amount :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).unit_tax_amount;
                     L_tax_tbl(L_tax_ctr).modified_tax_basis_amount :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).modified_tax_basis_amount;
                     L_tax_tbl(L_tax_ctr).unit_tax_uom :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).unit_tax_uom;
                     L_tax_tbl(L_tax_ctr).tax_rate_type :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_rate_type;
                     L_tax_tbl(L_tax_ctr).tax_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_code;
                     L_tax_tbl(L_tax_ctr).tax_rate :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).tax_rate;
                     L_tax_tbl(L_tax_ctr).recoverable_amt :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).recoverable_amt;
                     L_tax_tbl(L_tax_ctr).recovered_amt :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).recovered_amt;
                     L_tax_tbl(L_tax_ctr).reg_pct_margin_value :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).reg_pct_margin_value;
                     L_tax_tbl(L_tax_ctr).regulated_price :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).TaxDetRBO_TBL(k).regulated_price;

                     L_tax_ctr := L_tax_ctr + 1;
                  end LOOP;
               end if;

               if I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL is not NULL and
                  I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL.COUNT > 0 then
                  if L_sug_ctr is NULL then L_sug_ctr := 1; end if;
                  for k in I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL.FIRST..
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL.LAST LOOP
                     L_sug_tbl(L_sug_ctr).tax_service_id      := I_tax_service_id;
                     L_sug_tbl(L_sug_ctr).tax_result_seq      := L_tax_result_seq;
                     L_sug_tbl(L_sug_ctr).tax_item_seq        := j;
                     L_sug_tbl(L_sug_ctr).status :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL(k).status;
                     L_sug_tbl(L_sug_ctr).tax_rule_code :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL(k).tax_rule_code;
                     L_sug_tbl(L_sug_ctr).tax_rule_description :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL(k).tax_rule_description;
                     L_sug_tbl(L_sug_ctr).tax_rule_id :=
                        I_businessObject.FiscDocChnkTaxRBO_TBL(h).FiscDocTaxRBO_TBL(i).LineItemTaxRBO_TBL(j).InconclRuleRBO_TBL(k).tax_rule_id;
                     L_sug_ctr := L_sug_ctr + 1;
                  end LOOP;
               end if;
               L_item_ctr := L_item_ctr + 1;
            end LOOP;
         end if;

         L_result_ctr := L_result_ctr + 1;
         L_tax_result_seq := L_tax_result_seq + 1;

      end LOOP;
   end if;
      end LOOP;
   end if;

   if L_result_tbl is not NULL and L_result_tbl.COUNT > 0 then
      FORALL i in L_result_tbl.FIRST..L_result_tbl.LAST
         insert into l10n_br_tax_call_res (
           tax_service_id,
           tax_result_seq,
           document_number,
           fiscal_document_date,
           src_entity_code,
           src_entity_type,
           src_addr_city_id,
           src_federal_tax_reg_id,
           dst_entity_code,
           dst_entity_type,
           dst_addr_city_id,
           dst_federal_tax_reg_id,
           disc_merch_cost,
           total_cost,
           total_services_cost,
           calculation_status)
         values (
           L_result_tbl(i).tax_service_id,
           L_result_tbl(i).tax_result_seq,
           L_result_tbl(i).document_number,
           L_result_tbl(i).fiscal_document_date,
           L_result_tbl(i).src_entity_code,
           L_result_tbl(i).src_entity_type,
           L_result_tbl(i).src_addr_city_id,
           L_result_tbl(i).src_federal_tax_reg_id,
           L_result_tbl(i).dst_entity_code,
           L_result_tbl(i).dst_entity_type,
           L_result_tbl(i).dst_addr_city_id,
           L_result_tbl(i).dst_federal_tax_reg_id,
           L_result_tbl(i).disc_merch_cost,
           L_result_tbl(i).total_cost,
           L_result_tbl(i).total_services_cost,
           L_result_tbl(i).calculation_status);
   end if;

   if L_eco_tbl is not NULL and L_eco_tbl.COUNT > 0 then
      FORALL i in L_eco_tbl.FIRST..L_eco_tbl.LAST
         insert into l10n_br_tax_call_res_eco_class (
           tax_service_id,
           tax_result_seq,
           source_destination_ind,
           value)
         values (
           L_eco_tbl(i).tax_service_id,
           L_eco_tbl(i).tax_result_seq,
           L_eco_tbl(i).source_destination_ind,
           L_eco_tbl(i).value);
   end if;

   if L_contrib_tbl is not NULL and L_contrib_tbl.COUNT > 0 then
      FORALL i in L_contrib_tbl.FIRST..L_contrib_tbl.LAST
         insert into l10n_br_tax_call_res_tax_cntrb (
           tax_service_id,
           tax_result_seq,
           source_destination_ind,
           value)
         values (
           L_contrib_tbl(i).tax_service_id,
           L_contrib_tbl(i).tax_result_seq,
           L_contrib_tbl(i).source_destination_ind,
           L_contrib_tbl(i).value);
   end if;

   if L_item_tbl is not NULL and L_item_tbl.COUNT > 0 then
      FORALL i in L_item_tbl.FIRST..L_item_tbl.LAST
         insert into l10n_br_tax_call_res_item (
           tax_service_id,
           tax_result_seq,
           tax_item_seq,
           item_id,
           pack_item_id,
           item_tran_code,
           taxed_quantity,
           taxed_quantity_uom,
           total_cost,
           deduced_fiscal_code_opr,
           icms_cst_code,
           pis_cst_code,
           cofins_cst_code,
           deduce_icms_cst_code,
           deduce_pis_cst_code,
           deduce_cofins_cst_code,
           recoverable_icmsst,
           total_cost_with_icms,
           unit_cost_with_icms,
           recoverable_base_icmsst,
           unit_cost,
           dim_object,
           length,
           width,
           lwh_uom,
           weight,
           net_weight,
           weight_uom,
           liquid_volume,
           liquid_volume_uom)
         values (
           L_item_tbl(i).tax_service_id,
           L_item_tbl(i).tax_result_seq,
           L_item_tbl(i).tax_item_seq,
           L_item_tbl(i).item_id,
           L_item_tbl(i).pack_item_id,
           L_item_tbl(i).item_tran_code,
           L_item_tbl(i).taxed_quantity,
           L_item_tbl(i).taxed_quantity_uom,
           L_item_tbl(i).total_cost,
           L_item_tbl(i).deduced_fiscal_code_opr,
           L_item_tbl(i).icms_cst_code,
           L_item_tbl(i).pis_cst_code,
           L_item_tbl(i).cofins_cst_code,
           L_item_tbl(i).deduce_icms_cst_code,
           L_item_tbl(i).deduce_pis_cst_code,
           L_item_tbl(i).deduce_cofins_cst_code,
           L_item_tbl(i).recoverable_icmsst,
           L_item_tbl(i).total_cost_with_icms,
           L_item_tbl(i).unit_cost_with_icms,
           L_item_tbl(i).recoverable_base_icmsst,
           L_item_tbl(i).unit_cost,
           L_item_tbl(i).dim_object,
           L_item_tbl(i).length,
           L_item_tbl(i).width,
           L_item_tbl(i).lwh_uom,
           L_item_tbl(i).weight,
           L_item_tbl(i).net_weight,
           L_item_tbl(i).weight_uom,
           L_item_tbl(i).liquid_volume,
           L_item_tbl(i).liquid_volume_uom);
   end if;

   if L_srvc_prd_tbl is not NULL and L_srvc_prd_tbl.COUNT > 0 then
      FORALL i in L_srvc_prd_tbl.FIRST..L_srvc_prd_tbl.LAST
         insert into l10n_br_tax_call_res_srvc_prd  (
           tax_service_id,
           tax_result_seq,
           tax_item_seq,
           federal_service_code,
           item_origin,
           fiscal_classification_code,
           ipi_exception_code,
           ext_fiscal_class_code,
           service_product_item_ind)
         values (
           L_srvc_prd_tbl(i).tax_service_id,
           L_srvc_prd_tbl(i).tax_result_seq,
           L_srvc_prd_tbl(i).tax_item_seq,
           L_srvc_prd_tbl(i).federal_service_code,
           L_srvc_prd_tbl(i).item_origin,
           L_srvc_prd_tbl(i).fiscal_classification_code,
           L_srvc_prd_tbl(i).ipi_exception_code,
           L_srvc_prd_tbl(i).ext_fiscal_class_code,
           L_srvc_prd_tbl(i).service_product_item_ind);
   end if;

   if L_tax_tbl is not NULL and L_tax_tbl.COUNT > 0 then
      FORALL i in L_tax_tbl.FIRST..L_tax_tbl.LAST
         insert into l10n_br_tax_call_res_item_tax (
           tax_service_id,
           tax_result_seq,
           tax_item_seq,
           legal_message,
           tax_amount,
           tax_basis_amount,
           modified_tax_basis_amount,
           unit_tax_amount,
           unit_tax_uom,
           tax_rate_type,
           tax_code,
           tax_rate,
           recoverable_amt,
           recovered_amt,
           reg_pct_margin_value,
           regulated_price)
         values (
           L_tax_tbl(i).tax_service_id,
           L_tax_tbl(i).tax_result_seq,
           L_tax_tbl(i).tax_item_seq,
           L_tax_tbl(i).legal_message,
           L_tax_tbl(i).tax_amount,
           L_tax_tbl(i).tax_basis_amount,
           L_tax_tbl(i).modified_tax_basis_amount,
           L_tax_tbl(i).unit_tax_amount,
           L_tax_tbl(i).unit_tax_uom,
           L_tax_tbl(i).tax_rate_type,
           L_tax_tbl(i).tax_code,
           L_tax_tbl(i).tax_rate,
           L_tax_tbl(i).recoverable_amt,
           L_tax_tbl(i).recovered_amt,
           L_tax_tbl(i).reg_pct_margin_value,
           L_tax_tbl(i).regulated_price);
   end if;

   if L_sug_tbl is not NULL and L_sug_tbl.COUNT > 0 then
      FORALL i in L_sug_tbl.FIRST..L_sug_tbl.LAST
         insert into l10n_br_tax_call_res_item_rule (
           tax_service_id,
           tax_result_seq,
           tax_item_seq,
           status,
           tax_rule_code,
           tax_rule_description,
           tax_rule_id)
         values (
           L_sug_tbl(i).tax_service_id,
           L_sug_tbl(i).tax_result_seq,
           L_sug_tbl(i).tax_item_seq,
           L_sug_tbl(i).status,
           L_sug_tbl(i).tax_rule_code,
           L_sug_tbl(i).tax_rule_description,
           L_sug_tbl(i).tax_rule_id);
   end if;

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STAGE_RESULTS;
-------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                         I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                         I_call_type       IN             L10N_BR_TAX_CALL_STAGE_RMS.CALL_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'L10N_BR_INT_SQL.PROCESS_RESULTS';

BEGIN

   if I_call_type = 'PO' then
      if PROCESS_PO_RESULTS(O_error_message,
                            IO_l10n_obj,
                            I_tax_service_id) = FALSE then
         return FALSE;
      end if;
   elsif I_call_type in ('P', 'S') then
      if PROCESS_PURCHASE_SALE_RESULTS(O_error_message,
                                       IO_l10n_obj,
                                       I_tax_service_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
      return FALSE;
END PROCESS_RESULTS;
-------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PO_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                            I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(61) := 'L10N_BR_INT_SQL.PROCESS_PO_RESULTS';

   L_vdate            DATE := GET_VDATE;
   L_currency_code    CURRENCY_RATES.CURRENCY_CODE%TYPE := NULL;
   L_exchange_rate    CURRENCY_RATES.EXCHANGE_RATE%TYPE := NULL;

   cursor c_conv_rate is
      select currency_code,
             exchange_rate
        from (select oh.currency_code,
                     mc.exchange_rate,
                     rank() over
                        (PARTITION BY from_currency, to_currency, exchange_type
                             ORDER BY effective_date DESC) date_rank
                from mv_currency_conversion_rates mc,
                     ordhead oh
               where oh.order_no        = IO_l10n_obj.doc_id
                 and mc.from_currency   = 'BRL'
                 and mc.to_currency     = oh.currency_code
                 and mc.exchange_type   = 'C'
                 and mc.effective_date <= L_vdate) /*vdate or order date?  will check  and update*/
       where date_rank = 1;

BEGIN

   open c_conv_rate;
   fetch c_conv_rate into L_currency_code, L_exchange_rate;
   close c_conv_rate;

   merge into ord_tax_breakup otb
   using (
      select r.document_number order_no,
             r.dst_entity_code location,
             r.dst_entity_type location_type,
             expl.item_id item,
             t.tax_code,
             t.tax_rate,
             DECODE(t.tax_rate_type,'A','V','P') calculation_basis,
             t.tax_amount * L_exchange_rate total_tax_amt,
             t.tax_basis_amount * L_exchange_rate taxable_base,
             t.recoverable_amt * L_exchange_rate recoverable_amount,
             t.modified_tax_basis_amount * L_exchange_rate modified_taxable_base,
             L_currency_code currency_code
        from l10n_br_tax_call_res r,
             l10n_br_tax_call_res_item i,
             l10n_br_tax_call_res_item_tax t,
             (select rms.from_entity,
                     rms.from_entity_type,
                     rms.to_entity,
                     rms.to_entity_type,
                     rms.effective_date,
                     item.item_id,
                     ex.item_id lead_item
                from l10n_br_tax_call_stage_rms rms,
                     l10n_br_tax_stage_order_info item,
                     l10n_br_tax_stage_order_info ex
               where rms.tax_service_id            = I_tax_service_id
                 and rms.tax_service_id            = item.tax_service_id
                 and item.item_id                  = rms.item
                 and item.from_entity              = rms.from_entity
                 and item.from_entity_type         = rms.from_entity_type
                 and item.to_entity                = rms.to_entity
                 and item.to_entity_type           = rms.to_entity_type
                 --
                 and rms.pack_item                 is NULL
                 and item.pack_item_id             is NULL
                 --
                 and item.tax_service_id           = ex.tax_service_id
                 and item.item_group_id            = ex.item_group_id
                 and item.from_entity              = ex.from_entity
                 and item.from_entity_type         = ex.from_entity_type
                 and item.to_entity                = ex.to_entity
                 and item.to_entity_type           = ex.to_entity_type
                 and ex.item_group_counter         = 1
             ) expl
       where r.tax_service_id      = I_tax_service_id
         and r.tax_service_id      = i.tax_service_id
         and r.tax_result_seq      = i.tax_result_seq
         and i.tax_service_id      = t.tax_service_id
         and i.tax_result_seq      = t.tax_result_seq
         and i.tax_item_seq        = t.tax_item_seq
         and i.pack_item_id        is NULL
         --
         and i.item_id              = expl.lead_item
         and r.src_entity_code      = expl.from_entity
         and r.src_entity_type      = expl.from_entity_type
         and r.dst_entity_code      = expl.to_entity
         and r.dst_entity_type      = expl.to_entity_type
         and r.fiscal_document_date = expl.effective_date
      UNION ALL
      select r.document_number order_no,
             r.dst_entity_code location,
             r.dst_entity_type location_type,
             i.pack_item_id item,
             t.tax_code,
             DECODE(m.simple_pack_ind, 'Y', AVG(t.tax_rate), NULL) tax_rate,
             DECODE(m.simple_pack_ind, 'Y', DECODE(MIN(t.tax_rate_type),'A','V','P'), NULL) calculation_basis,
             SUM(t.tax_amount) * L_exchange_rate total_tax_amt,
             DECODE(m.simple_pack_ind, 'Y', AVG(t.tax_basis_amount) * L_exchange_rate, NULL) taxable_base,
             SUM(t.recoverable_amt) * L_exchange_rate recoverable_amount,
             DECODE(m.simple_pack_ind, 'Y', AVG(t.modified_tax_basis_amount) * L_exchange_rate, NULL) modified_taxable_base,
             L_currency_code currency_code
        from l10n_br_tax_call_res r,
             l10n_br_tax_call_res_item i,
             l10n_br_tax_call_res_item_tax t,
             item_master m
       where r.tax_service_id      = I_tax_service_id
         and r.tax_service_id      = i.tax_service_id
         and r.tax_result_seq      = i.tax_result_seq
         and i.tax_service_id      = t.tax_service_id
         and i.tax_result_seq      = t.tax_result_seq
         and i.tax_item_seq        = t.tax_item_seq
         and i.pack_item_id        is not NULL
         and m.item                = i.pack_item_id
       group by r.document_number,
                r.dst_entity_code,
                r.dst_entity_type,
                i.pack_item_id,
                t.tax_code,
                m.simple_pack_ind) use_this
   on  (    use_this.item     = otb.item
        and use_this.location = otb.location
        and use_this.tax_code = otb.tax_code
        and use_this.order_no = otb.order_no)
   when matched then
   update set otb.tax_rate              = use_this.tax_rate,
              otb.taxable_base          = use_this.taxable_base,
              otb.total_tax_amt         = use_this.total_tax_amt,
              otb.recoverable_amount    = use_this.recoverable_amount,
              otb.modified_taxable_base = use_this.modified_taxable_base,
              otb.last_update_datetime  = SYSDATE,
              otb.last_update_id        = USER
   when NOT matched then
   insert (order_no,
           item,
           location,
           location_type,
           tax_code,
           tax_rate,
           calculation_basis,
           total_tax_amt,
           taxable_base,
           currency_code,
           recoverable_amount,
           create_datetime,
           create_id,
           last_update_datetime,
           last_update_id,
           per_count,
           per_count_uom,
           modified_taxable_base)
   values (use_this.order_no,
           use_this.item,
           use_this.location,
           DECODE(use_this.location_type,'ST', 'S', 'W'),
           use_this.tax_code,
           use_this.tax_rate,
           use_this.calculation_basis,
           use_this.total_tax_amt,
           use_this.taxable_base,
           use_this.currency_code,
           use_this.recoverable_amount,
           SYSDATE,
           USER,
           SYSDATE,
           USER,
           NULL,
           NULL,
           use_this.modified_taxable_base);  

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_PO_RESULTS;
-------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PURCHASE_SALE_RESULTS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                       IO_l10n_obj       IN OUT NOCOPY  L10N_OBJ,
                                       I_tax_service_id  IN             L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'L10N_BR_INT_SQL.PROCESS_PURCHASE_SALE_RESULTS';

   L_obj_tax_tbl      OBJ_TAX_TBL;

   L_l10n_tax_in_obj  L10N_TAX_REC;
   L_l10n_tax_out_obj L10N_TAX_REC;
   L_tax_type         VARCHAR2(1);

   cursor c_get_object is
      select OBJ_TAX_REC(
              ITEM                       => input.item,
              PACK_IND                   => 'N',
              FROM_ENTITY                => input.from_entity,
              FROM_ENTITY_TYPE           => input.from_entity_type,
              TO_ENTITY                  => input.to_entity,
              TO_ENTITY_TYPE             => input.to_entity_type,
              EFFECTIVE_FROM_DATE        => input.effective_from_date,
              TAXABLE_BASE               => input.taxable_base,
              TAXABLE_BASE_CURR          => input.taxable_base_curr,
              TAXABLE_BASE_TAX_INCL_IND  => input.taxable_base_tax_incl_ind,
              ORIGIN_COUNTRY_ID          => input.origin_country_id,
              TAX_DETAIL_TBL             =>
                 CAST(MULTISET(select OBJ_TAX_DETAIL_REC(
                                      TAX_CODE              => trt.tax_code,
                                      TAX_TYPE              => L_tax_type,        --'R'etail or 'C'ost
                                      CALCULATION_BASIS     => trt.tax_rate_type, --'P'(percent) or 'V'(amount)
                                      TAXABLE_BASE          => trt.tax_basis_amount * mc.exchange_rate,
                                      MODIFIED_TAXABLE_BASE => trt.modified_tax_basis_amount * mc.exchange_rate,
                                      TAX_RATE              => DECODE(trt.tax_rate_type,
                                                                      'P', trt.tax_rate,
                                                                      'V', trt.tax_rate * mc.exchange_rate),
                                      ESTIMATED_TAX_VALUE   => NVL(trt.tax_amount, 0) * mc.exchange_rate,
                                      RECOVERABLE_AMOUNT    => NVL(trt.recoverable_amt,0) * mc.exchange_rate,
                                      CURRENCY_CODE         => input.taxable_base_curr)
                                 from l10n_br_tax_call_res_item_tax trt,
                                      -- currency conversion materialized view
                                      (select from_currency,
                                              to_currency,
                                              effective_date,
                                              exchange_rate,
                                              rank() over
                                                 (PARTITION BY from_currency, to_currency, exchange_type
                                                      ORDER BY effective_date DESC) date_rank
                                         from mv_currency_conversion_rates,
                                              -- All l10n_br_tax_call_res should have the same
                                              -- fiscal_document_date for a given tax_service_id
                                              (select distinct NVL(fiscal_document_date, LP_vdate) fiscal_document_date
                                                 from l10n_br_tax_call_res
                                                where tax_service_id = I_tax_service_id) tr2
                                        where exchange_type = 'C'
                                          and effective_date <= tr2.fiscal_document_date) mc
                                where trt.tax_service_id = tri.tax_service_id
                                  and trt.tax_result_seq = tri.tax_result_seq
                                  and trt.tax_item_seq   = tri.tax_item_seq
                                  and mc.from_currency   = 'BRL'   --assume tax engine will always return ammount in Brazilian currency
                                  and mc.to_currency     = input.taxable_base_curr
                                  and mc.date_rank       = 1) as "OBJ_TAX_DETAIL_TBL"))
        from TABLE(CAST(L_l10n_tax_in_obj.L10N_TAX_TBL as OBJ_TAX_TBL)) input,
             l10n_br_tax_call_res tr,
             l10n_br_tax_call_res_item tri,
             (select rms.from_entity,
                     rms.from_entity_type,
                     rms.to_entity,
                     rms.to_entity_type,
                     rms.effective_date,
                     item.item_id,
                     ex.item_id lead_item
                from l10n_br_tax_call_stage_rms rms,
                     l10n_br_tax_stage_item item,
                     l10n_br_tax_stage_item ex
               where L_tax_type                  = 'R'
                 and rms.tax_service_id          = I_tax_service_id
                 and rms.tax_service_id          = item.tax_service_id
                 and item.item_id                = rms.item
                 --
                 and rms.pack_item               is NULL
                 and item.pack_item_id           is NULL
                 --
                 -- Item_group_counter = 1 denotes the lead item which
                 -- has similar attributes to all items under the same item_group_id
                 and item.tax_service_id         = ex.tax_service_id
                 and item.item_group_id          = ex.item_group_id
                 and ex.item_group_counter       = 1
             UNION ALL
              select rms.from_entity,
                     rms.from_entity_type,
                     rms.to_entity,
                     rms.to_entity_type,
                     rms.effective_date,
                     item.item_id,
                     ex.item_id lead_item
                from l10n_br_tax_call_stage_rms rms,
                     l10n_br_tax_stage_order_info item,
                     l10n_br_tax_stage_order_info ex
               where L_tax_type                    = 'C'
                 and rms.tax_service_id            = I_tax_service_id
                 and rms.tax_service_id            = item.tax_service_id
                 and item.item_id                  = rms.item
                 and item.to_entity                = rms.to_entity
                 and item.to_entity_type           = rms.to_entity_type
                 and item.from_entity              = rms.from_entity
                 and item.from_entity_type         = rms.from_entity_type
                 --
                 and rms.pack_item                 is NULL
                 and item.pack_item_id             is NULL
                 --
                 and item.tax_service_id           = ex.tax_service_id
                 and item.item_group_id            = ex.item_group_id
                 and item.to_entity                = ex.to_entity
                 and item.to_entity_type           = ex.to_entity_type
                 and item.from_entity              = ex.from_entity
                 and item.from_entity_type         = ex.from_entity_type
                 and ex.item_group_counter         = 1
             ) expl
       where tr.tax_service_id  = I_tax_service_id
         and tr.tax_service_id  = tri.tax_service_id
         and tr.tax_result_seq  = tri.tax_result_seq
         --
         and input.pack_ind     = 'N'
         and tri.pack_item_id   is NULL
         --
         -- Joins the lead item with the results
         -- and explodes the results to all similarly grouped items
         and tri.item_id        = expl.lead_item
         and expl.item_id       = input.item
         --
         and tr.src_entity_code = input.from_entity
         and tr.src_entity_type = input.from_entity_type

         and tr.dst_entity_code = NVL(input.to_entity, input.from_entity)
         and tr.dst_entity_type = NVL(input.to_entity_type, input.from_entity_type)
         and NVL(tr.fiscal_document_date, TO_DATE('01-01-1900', 'DD-MM-YYYY')) =
                 NVL(input.effective_from_date, TO_DATE('01-01-1900', 'DD-MM-YYYY'))
         --
         and expl.from_entity = input.from_entity
         and expl.from_entity_type = input.from_entity_type
         and NVL(expl.to_entity,expl.from_entity) = NVL(input.to_entity, input.from_entity)
         and NVL(expl.to_entity_type,expl.from_entity_type) = NVL(input.to_entity_type, input.from_entity_type)
         and NVL(expl.effective_date, TO_DATE('01-01-1900', 'DD-MM-YYYY')) =
                 NVL(input.effective_from_date, TO_DATE('01-01-1900', 'DD-MM-YYYY'))
       UNION ALL
      select OBJ_TAX_REC(
              ITEM                       => input.item,
              PACK_IND                   => 'Y',
              FROM_ENTITY                => input.from_entity,
              FROM_ENTITY_TYPE           => input.from_entity_type,
              TO_ENTITY                  => input.to_entity,
              TO_ENTITY_TYPE             => input.to_entity_type,
              EFFECTIVE_FROM_DATE        => input.effective_from_date,
              TAXABLE_BASE               => input.taxable_base,
              TAXABLE_BASE_CURR          => input.taxable_base_curr,
              TAXABLE_BASE_TAX_INCL_IND  => input.taxable_base_tax_incl_ind,
              ORIGIN_COUNTRY_ID          => input.origin_country_id,
              TAX_DETAIL_TBL             =>
                 CAST(MULTISET(select OBJ_TAX_DETAIL_REC(
                                      TAX_CODE              => trt.tax_code,
                                      TAX_TYPE              => MIN(L_tax_type),        --'R'etail or 'C'ost
                                      CALCULATION_BASIS     => trt.tax_rate_type,      --'P'(percent) or 'V'(amount)
                                      TAXABLE_BASE          => MIN(trt.tax_basis_amount * mc.exchange_rate),
                                      MODIFIED_TAXABLE_BASE => MIN(trt.modified_tax_basis_amount * mc.exchange_rate),
                                      TAX_RATE              => DECODE(trt.tax_rate_type, 'P', AVG(NVL(trt.tax_rate,0)),
                                                                                       'V', SUM(NVL(trt.tax_rate,0) * mc.exchange_rate)),
                                      ESTIMATED_TAX_VALUE   => SUM(NVL(trt.tax_amount, 0) * mc.exchange_rate),
                                      RECOVERABLE_AMOUNT    => SUM(NVL(trt.recoverable_amt,0) * mc.exchange_rate),
                                      CURRENCY_CODE         => MIN(input.taxable_base_curr)) --asSUMption: input currency are always the same
                                 from l10n_br_tax_call_res_item tri2,      --for component items
                                      l10n_br_tax_call_res_item_tax trt,
                                      -- currency conversion materialized view
                                      (select from_currency,
                                              to_currency,
                                              effective_date,
                                              exchange_rate,
                                              rank() over
                                                 (PARTITION BY from_currency, to_currency, exchange_type
                                                      ORDER BY effective_date DESC) date_rank
                                         from mv_currency_conversion_rates,
                                              -- All l10n_br_tax_call_res should have the same fiscal_document_date for a given tax_service_id
                                              (select distinct NVL(fiscal_document_date,LP_vdate) fiscal_document_date
                                                 from l10n_br_tax_call_res
                                                where tax_service_id = I_tax_service_id) tr2
                                        where exchange_type = 'C'
                                          and effective_date <= tr2.fiscal_document_date) mc
                                where tri2.tax_service_id  = inner.tax_service_id
                                  and tri2.tax_result_seq  = inner.tax_result_seq
                                  --the grouping logic for item included pack/item_id so this is ok to not explode to the
                                  --group like the item section does.
                                  and tri2.pack_item_id    = inner.pack_item_id
                                  and tri2.tax_service_id  = trt.tax_service_id
                                  and tri2.tax_result_seq  = trt.tax_result_seq
                                  and tri2.tax_item_seq    = trt.tax_item_seq
                                  and mc.from_currency   = 'BRL'   --asSUMe tax engine will always return ammount in Brazilian currency
                                  and mc.to_currency     = input.taxable_base_curr
                                  and mc.date_rank       = 1
                             group by tri2.pack_item_id,
                                      trt.tax_code,
                                      trt.tax_rate_type) as "OBJ_TAX_DETAIL_TBL"))
        from TABLE(CAST(L_l10n_tax_in_obj.L10N_TAX_TBL as OBJ_TAX_TBL)) input,
             (select distinct
                     tri.tax_service_id,
                     tri.tax_result_seq,
                     tri.pack_item_id,
                     tr.src_entity_code,
                     tr.src_entity_type,
                     tr.dst_entity_code,
                     tr.dst_entity_type,
                     tr.fiscal_document_date
                from l10n_br_tax_call_res tr,
                     l10n_br_tax_call_res_item tri
               where tr.tax_service_id = I_tax_service_id
                 and tr.tax_service_id = tri.tax_service_id
                 and tr.tax_result_seq = tri.tax_result_seq
                 and tri.pack_item_id  is not NULL) inner
       where input.pack_ind = 'Y'
         and inner.pack_item_id = input.item   --pack_no
         and inner.src_entity_code = input.from_entity
         and inner.src_entity_type = input.from_entity_type
         --to_entity not sent for retail, pass from_entity as destination entity to tax engine
         and inner.dst_entity_code = NVL(input.to_entity, input.from_entity)
         and inner.dst_entity_type = NVL(input.to_entity_type, input.from_entity_type)
         and NVL(inner.fiscal_document_date, TO_DATE('01-01-1900', 'DD-MM-YYYY')) =
                NVL(input.effective_from_date, TO_DATE('01-01-1900', 'DD-MM-YYYY'))
       UNION ALL
      -- For a cost scenario, non-Brazil supplier were not carried forward into the tax engine call.
      -- Prior to creating the final IO_l10n_obj object, the non-Brazil supplier should be included.
      -- The non-Brazil supplier records are available in the input parameter IO_l10n_obj.
      select OBJ_TAX_REC(input.item,
                         input.pack_ind,
                         input.from_entity,
                         input.from_entity_type,
                         input.to_entity,
                         input.to_entity_type,
                         input.effective_from_date,
                         input.taxable_base,
                         input.taxable_base_curr,
                         input.taxable_base_tax_incl_ind,
                         input.origin_country_id,
                         OBJ_TAX_DETAIL_TBL())
        from TABLE(CAST(L_l10n_tax_in_obj.L10N_TAX_TBL as OBJ_TAX_TBL)) input,
             addr
       -- Retrieve non-Brazil suppliers.
       where L_tax_type = 'C'   --only applies to cost scenario
         and input.from_entity = addr.key_value_1
         and addr.module = 'SUPP'
         and addr.addr_type = '01'
         and addr.primary_addr_ind = 'Y'
         and addr.country_id != 'BR';

BEGIN

   L_l10n_tax_in_obj := treat(IO_l10n_obj as L10N_TAX_REC);

   if L_l10n_tax_in_obj.call_type = 'S' then
      L_tax_type := 'R';   --'S'ale => 'R'etail
   elsif L_l10n_tax_in_obj.call_type = 'P' then
      L_tax_type := 'C';   --'P'urchase => 'C'ost
   end if;

   open c_get_object;
   fetch c_get_object BULK COLLECT into L_obj_tax_tbl;
   close c_get_object;

   L_l10n_tax_out_obj := L10N_TAX_REC(
           PROCEDURE_KEY   => L_l10n_tax_in_obj.procedure_key,
           COUNTRY_ID      => L_l10n_tax_in_obj.country_id,  --'BR'
           DOC_TYPE        => NULL,
           DOC_ID          => NULL,
           SOURCE_ENTITY   => NULL,
           SOURCE_TYPE     => NULL,
           SOURCE_ID       => NULL,
           DEST_ENTITY     => NULL,
           DEST_TYPE       => NULL,
           DEST_ID         => NULL,
           DEPT            => NULL,
           CLASS           => NULL,
           SUBCLASS        => NULL,
           ITEM            => NULL,
           AV_COST         => NULL,
           UNIT_COST       => NULL,
           UNIT_RETAIL     => NULL,
           STOCK_ON_HAND   => NULL,
           L10N_TAX_TBL    => L_obj_tax_tbl,
           CALL_TYPE       => L_l10n_tax_in_obj.call_type);

   -- Assign L_l10n_tax_out_obj into the input/output paramater IO_l10n_obj
   IO_l10n_obj := L_l10n_tax_out_obj;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
      return FALSE;
END PROCESS_PURCHASE_SALE_RESULTS;
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_UOM_INFO(O_error_message      IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_doclineitem_tbl   IN OUT NOCOPY     "RIB_LineItemRBO_TBL",
                       I_l10n_tax_tbl       IN                OBJ_TAX_TBL,
                       I_nf_item            IN                ITEM_MASTER.ITEM%TYPE,
                       I_nf_quantity        IN                NUMBER,
                       I_nf_uom             IN                VARCHAR2,
                       I_order_no           IN                ORDHEAD.ORDER_NO%TYPE,
                       I_call_type          IN                VARCHAR2)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'L10N_BR_INT_SQL.LOAD_UOM_INFO';
   ---
   L_net_weight          ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_weight_uom          ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_lwh_uom             ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_liquid_volume       ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_liquid_volume_uom   ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_unit_of_measure     ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE;
   L_qty_in_eaches       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_quantity            ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_length              ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_weight              ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_unit_cost           ORDLOC.UNIT_COST%TYPE;
   L_total_cost          ORDLOC.UNIT_COST%TYPE;
   L_doc_item            ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE := NULL;
   L_item                ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE := NULL;
   L_supplier            ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE := NULL;
   L_doc_supplier        ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE := NULL;
   L_origin_country      ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE := NULL;
   L_dim_object          ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE := NULL;
   L_default_uop         ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE := NULL;
   L_supp_pack_size      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NULL;
   L_pallet_size         ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NULL;
   L_item_uop            ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE := NULL;
   L_item_supp_pack_size ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NULL;
   L_item_dim            VARCHAR2(1) := 'N';

   cursor C_GET_PURCHASE_UOM is
      select default_uop,
             supp_pack_size,
             (ti*hi) pallet_size
        from item_supp_country
       where item = L_doc_item
         and supplier = L_doc_supplier
         and origin_country_id = L_origin_country;

   cursor C_GET_UOM_ITEM_PURCHASE is
      select iscd.item,
             iscd.supplier,
             iscd.origin_country,
             iscd.dim_object,
             iscd.weight,
             iscd.net_weight,
             iscd.weight_uom,
             iscd.length,
             iscd.lwh_uom,
             iscd.liquid_volume,
             iscd.liquid_volume_uom
        from item_supp_country isc,
             item_supp_country_dim iscd
       where isc.item = L_doc_item
         and isc.supplier = L_doc_supplier
         and isc.origin_country_id = L_origin_country
         and isc.item = iscd.item
         and isc.supplier = iscd.supplier
         and isc.origin_country_id = iscd.origin_country;

   cursor C_GET_UOM_ITEM_SALES is
      select iscd.item,
             iscd.supplier,
             iscd.origin_country,
             iscd.dim_object,
             iscd.weight,
             iscd.net_weight,
             iscd.weight_uom,
             iscd.length,
             iscd.lwh_uom,
             iscd.liquid_volume,
             iscd.liquid_volume_uom
        from item_supp_country isc,
             item_supp_country_dim iscd
       where isc.item = L_doc_item
         and isc.primary_supp_ind = 'Y'
         and isc.primary_country_ind = 'Y'
         and isc.item = iscd.item
         and isc.supplier = iscd.supplier
         and isc.origin_country_id = iscd.origin_country;

   cursor C_GET_UOM_PO is
      select os.item,
             o.supplier,
             os.origin_country_id,
             ol.unit_cost,
             SUM(ol.qty_ordered) qty_ordered,
             os.supp_pack_size,
             ow.uop
        from ordhead o,
             ordsku os,
             ordloc ol,
             ordloc_wksht ow
       where o.order_no = I_order_no
         and os.item = L_doc_item
         and o.order_no = os.order_no
         and o.order_no = ol.order_no
         and o.order_no = ow.order_no
         and os.item = ol.item
         and os.item = ow.item
         and ow.location = ol.location
         and ow.item = ol.item
       group by os.item, o.supplier, os.origin_country_id, ol.unit_cost, os.supp_pack_size, ow.uop;

   cursor C_LOAD_UOM_NF is
      select "RIB_LineItemRBO_REC"(0,                    --RIB OID
                                   NULL,                 --document_line_id
                                   L_item,               --item_id
                                   NULL,                 --item_tran_Code
                                   NULL,                 --item_type
                                   L_quantity,           --quantity
                                   L_unit_of_measure,    --unit_of_measure
                                   L_qty_in_eaches,      --quantity_in_eaches
                                   NULL,                 --origin_doc_date date,
                                   NULL,                 --pack_item_id
                                   NULL,                 --total_cost
                                   NULL,                 --unit_cost
                                   NULL,                 --src_taxpayer_type
                                   NULL,                 --orig_fiscal_doc_number
                                   NULL,                 --orig_fiscal_doc_series
                                   L_dim_object,         --dim_object
                                   L_length,             --length
                                   NULL,                 --width
                                   L_lwh_uom,            --lwh_uom
                                   L_weight,             --weight
                                   L_net_weight,         --net_weight
                                   L_weight_uom,         --weight_uom
                                   L_liquid_volume,      --liquid_volume
                                   L_liquid_volume_uom,  --liquid_volume_uom
                                   NULL,                 --freight
                                   NULL,                 --insurance
                                   NULL,                 --discount
                                   NULL,                 --commision
                                   NULL,                 --freight_type
                                   NULL,                 --other_expenses
                                   NULL,                 --origin_fiscal_code_opr
                                   NULL,                 --deduced_fiscal_code_opr
                                   'Y',                  --deduce_cfop_code
                                   NULL,                 --icms_cst_code
                                   NULL,                 --pis_cst_code
                                   NULL,                 --cofins_cst_code
                                   NULL,                 --deduce_icms_cst_code
                                   NULL,                 --deduce_pis_cst_code
                                   NULL,                 --deduce_cofins_cst_code
                                   NULL,                 --recoverable_icmsst
                                   NULL,                 --item_cost_contains_cofins
                                   NULL,                 --recoverable_base_icmsst
                                   NULL,                 --item_cost_contains_pis
                                   NULL,                 --item_cost_contains_icms
                                   NULL,                 --PrdItemRBO_TBL
                                   NULL,                 --SvcItemRBO_TBL
                                   NULL,                 --InformTaxRBO_TBL
                                   NULL)                 --NameValPairRBO_TBL
        from dual;

BEGIN

   if I_call_type = 'NF' then
      --Check required input
      if I_nf_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_nf_item', L_program, NULL);
         return FALSE;
      elsif I_nf_quantity is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_nf_quantity', L_program, NULL);
         return FALSE;
      elsif I_nf_uom is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_nf_uom', L_program, NULL);
         return FALSE;
      end if;
      ---
      L_doc_item          := I_nf_item;
      L_quantity          := I_nf_quantity;
      L_unit_of_measure   := I_nf_uom;
      ---
      open C_GET_PURCHASE_UOM;
      fetch C_GET_PURCHASE_UOM into L_default_uop,
                                    L_supp_pack_size,
                                    L_pallet_size;
      close C_GET_PURCHASE_UOM;
      ---
      for item in C_GET_UOM_ITEM_SALES LOOP
         L_item              := item.item;
         L_supplier          := item.supplier;
         L_origin_country    := item.origin_country;
         L_dim_object        := item.dim_object;
         L_item_dim          := 'Y';
         --qty_in_eaches
         if L_unit_of_measure = 'CA' then
            L_qty_in_eaches := L_supp_pack_size * L_quantity;
         elsif L_unit_of_measure = 'PA' then
            L_qty_in_eaches := L_quantity * L_pallet_size * L_supp_pack_size;
         elsif L_unit_of_measure = 'EA' then
            L_qty_in_eaches := L_quantity;
         end if;
         --for weight and volume
         if L_unit_of_measure = 'CA' then
            if item.dim_object = 'CA' then
               L_length            := item.length / L_supp_pack_size;
               L_lwh_uom           := item.lwh_uom;
               L_weight            := item.weight / L_supp_pack_size;
               L_net_weight        := item.net_weight / L_supp_pack_size;
               L_weight_uom        := item.weight_uom;
               L_liquid_volume     := item.liquid_volume / L_supp_pack_size;
               L_liquid_volume_uom := item.liquid_volume_uom;
            elsif item.dim_object = 'EA' then
               L_length            := item.length;
               L_lwh_uom           := item.lwh_uom;
               L_weight            := item.weight;
               L_net_weight        := item.net_weight * L_quantity;
               L_weight_uom        := item.weight_uom;
               L_liquid_volume     := item.liquid_volume;
               L_liquid_volume_uom := item.liquid_volume_uom;
            else
               L_length            := NULL;
               L_lwh_uom           := NULL;
               L_weight            := NULL;
               L_net_weight        := NULL;
               L_weight_uom        := NULL;
               L_liquid_volume     := NULL;
               L_liquid_volume_uom := NULL;
            end if;
         elsif L_unit_of_measure = 'PA' then
            if item.dim_object = 'PA' then
               L_length            := item.length / L_pallet_size /L_supp_pack_size;
               L_lwh_uom           := item.lwh_uom;
               L_weight            := item.weight / L_pallet_size /L_supp_pack_size;
               L_net_weight        := item.net_weight / L_pallet_size /L_supp_pack_size;
               L_weight_uom        := item.weight_uom;
               L_liquid_volume     := item.liquid_volume / L_pallet_size /L_supp_pack_size;
               L_liquid_volume_uom := item.liquid_volume_uom;
            elsif item.dim_object = 'CA' then
               L_length            := item.length / L_supp_pack_size;
               L_lwh_uom           := item.lwh_uom;
               L_weight            := item.weight / L_supp_pack_size;
               L_net_weight        := item.net_weight / L_supp_pack_size;
               L_weight_uom        := item.weight_uom;
               L_liquid_volume     := item.liquid_volume / L_supp_pack_size;
               L_liquid_volume_uom := item.liquid_volume_uom;
            else
               L_length            := NULL;
               L_lwh_uom           := NULL;
               L_weight            := NULL;
               L_net_weight        := NULL;
               L_weight_uom        := NULL;
               L_liquid_volume     := NULL;
               L_liquid_volume_uom := NULL;
            end if;
         elsif L_unit_of_measure = 'EA' then
            if item.dim_object = 'EA' then
               L_length            := item.length;
               L_lwh_uom           := item.lwh_uom;
               L_weight            := item.weight;
               L_net_weight        := item.net_weight * L_quantity;
               L_weight_uom        := item.weight_uom;
               L_liquid_volume     := item.liquid_volume;
               L_liquid_volume_uom := item.liquid_volume_uom;
            else
               L_length            := NULL;
               L_lwh_uom           := NULL;
               L_weight            := NULL;
               L_net_weight        := NULL;
               L_weight_uom        := NULL;
               L_liquid_volume     := NULL;
               L_liquid_volume_uom := NULL;
            end if;
         end if;
      end LOOP;
      ---
      open C_LOAD_UOM_NF;
      fetch C_LOAD_UOM_NF BULK COLLECT into IO_doclineitem_tbl;
      close C_LOAD_UOM_NF;
      ---
   end if; -- I_call_type = NF

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_UOM_INFO;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_TAX_REGIME(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_TAX_REGIME';

BEGIN

   select DISTINCT I_tax_service_id,
                   s.from_entity,
                   r.tax_regime
   BULK COLLECT INTO LP_stage_regime_tbl
     from l10n_br_tax_call_stage_rms s,
          l10n_br_sup_tax_regime r
    where r.supplier       = s.from_entity
      and s.tax_service_id = I_tax_service_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END STAGE_TAX_REGIME;
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_NAME_VALUE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_NAME_VALUE';

   L_sup_name_value_tbl      ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   L_loc_name_value_tbl      ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();
   L_item_name_value_tbl     ENTITY_ATTRIB_TBL := ENTITY_ATTRIB_TBL();

   cursor C_SUP_CUSTOM_ATTRIB IS
      select ENTITY_ATTRIB_REC(
             'SUPP',       --I_entity_type
             from_entity,  --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from (select distinct from_entity
                from l10n_br_tax_call_stage_rms
               where tax_service_id = I_tax_service_id
                 and call_type in ('PO','P'));

   cursor C_LOC_CUSTOM_ATTRIB IS
      select ENTITY_ATTRIB_REC(
             'LOC',        --I_entity_type
             entity_id,    --I_entity_id
             NULL,         --O_name
             NULL)         --O_value
        from (select distinct to_entity entity_id
                from l10n_br_tax_call_stage_rms
               where tax_service_id = I_tax_service_id
                 and call_type in ('PO','P')
              union all
              select distinct from_entity entity_id    -- use FROM_ENTITY for sale
                from l10n_br_tax_call_stage_rms
               where tax_service_id = I_tax_service_id
                 and call_type = 'S');

   cursor C_ITEM_CUSTOM_ATTRIB IS
      select ENTITY_ATTRIB_REC(
             'ITEM',     --I_entity_type
             item,       --I_entity_id
             NULL,       --O_name
             NULL)       --O_value
        from (select distinct item
                from l10n_br_tax_call_stage_rms
               where tax_service_id = I_tax_service_id);

BEGIN

   -- build a collection of non-base localization attributes for suppliers
   open C_SUP_CUSTOM_ATTRIB;
   fetch C_SUP_CUSTOM_ATTRIB bulk collect into L_sup_name_value_tbl;
   close C_SUP_CUSTOM_ATTRIB;

   if L_sup_name_value_tbl is NOT NULL and L_sup_name_value_tbl.COUNT > 0 then
      if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                    L_sup_name_value_tbl,
                                                    'SUPP') = FALSE then
         return FALSE;
      end if;
   end if;

   -- build a collection of non-base localization attributes for locations
   open C_LOC_CUSTOM_ATTRIB;
   fetch C_LOC_CUSTOM_ATTRIB bulk collect into L_loc_name_value_tbl;
   close C_LOC_CUSTOM_ATTRIB;

   if L_loc_name_value_tbl is NOT NULL and L_loc_name_value_tbl.COUNT > 0 then
      if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                    L_loc_name_value_tbl,
                                                    'LOC') = FALSE then
         return FALSE;
      end if;
   end if;

   -- build a collection of non-base localization attributes for items
   open C_ITEM_CUSTOM_ATTRIB;
   fetch C_ITEM_CUSTOM_ATTRIB bulk collect into L_item_name_value_tbl;
   close C_ITEM_CUSTOM_ATTRIB;

   if L_item_name_value_tbl is NOT NULL and L_item_name_value_tbl.COUNT > 0 then
      if L10N_BR_FLEX_API_SQL.BUILD_NAME_VALUE_PAIR(O_error_message,
                                                    L_item_name_value_tbl,
                                                    'ITEM') = FALSE then
         return FALSE;
      end if;
   end if;

   select I_tax_service_id,
          i_entity_type,
          i_entity_id,
          o_name,
          o_value
   BULK COLLECT INTO LP_stage_name_value_tbl
     from (select i_entity_type, i_entity_id, o_name, o_value
             from table(cast(L_sup_name_value_tbl as ENTITY_ATTRIB_TBL))
           union all
           select i_entity_type, i_entity_id, o_name, o_value
             from table(cast(L_loc_name_value_tbl as ENTITY_ATTRIB_TBL))
           union all
           select i_entity_type, i_entity_id, o_name, o_value
             from table(cast(L_item_name_value_tbl as ENTITY_ATTRIB_TBL)));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END STAGE_NAME_VALUE;
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_SUPPLIER(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_SUPPLIER';

BEGIN

   select I_tax_service_id,
          sup.from_entity_type,  --entity_type,
          sup.from_entity,       --entity_id,
          NULL,                  --physical_wh,
          ---
          ctj.jurisdiction_code, --city_id
          ctj.jurisdiction_desc, --city
          st.state,              --state
          NULL,                  --state_name
          vbc.fiscal_code,       --country_id
          ct.country_desc,       --country_name
          '1',                   --addr
          addr.addr_type,        --addr_type
          '1',                   --primary_addr_type_ind
          addr.primary_addr_ind, --primary_addr_ind
          addr.add_1,            --add_1
          NULL,                  --add_2
          NULL,                  --add_3
          ---
          sup.iss_contrib_ind,   --iss_ind
          sup.ipi_ind,           --ipi_ind
          sup.icms_contrib_ind,  --icms_ind
          sup.st_contrib_ind,    --st_ind
          NULL,                  --legal_name
          'S',                   --fiscal_type
          sup.simples_ind,       --is_simples_contributor
          sup.cnpj,              --federal_tax_reg_id
          sup.rural_prod_ind,    --is_rural_producer
          sup.is_income_range_eligible, --is_income_range_eligible
          sup.is_distr_a_manufacturer,  --is_distr_a_manufacturer
          sup.icms_simples_rate         --icms_simples_rate
   BULK COLLECT INTO LP_stage_fis_entity_sup_tbl
     from --ensure distinct suppliers from the main staging table
          (select distinct
                  stg.from_entity,
                  stg.from_entity_type,
                  vbs.simples_ind,
                  vbs.cnpj,
                  vbs.rural_prod_ind,
                  vbs.is_income_range_eligible,
                  vbs.is_distr_a_manufacturer,
                  vbs.icms_simples_rate,
                  iss_contrib_ind,
                  st_contrib_ind,
                  ipi_ind,
                  icms_contrib_ind
             from l10n_br_tax_call_stage_rms stg,
                  v_br_sups vbs
            where stg.tax_service_id = I_tax_service_id
              and stg.from_entity = vbs.supplier
              and stg.from_entity_type = 'SP') sup,
          addr,
          state st,
          country ct,
          country_tax_jurisdiction ctj,
          v_br_country_attrib vbc
    where addr.key_value_1       = sup.from_entity
      and addr.state             = ctj.state
      and addr.country_id        = ctj.country_id
      and addr.jurisdiction_code = ctj.jurisdiction_code
      and addr.state             = st.state
      and addr.country_id        = st.country_id
      and addr.primary_addr_ind  = 'Y'
      and addr.addr_type         = '01'
      and addr.module            = 'SUPP'
      and ct.country_id          = addr.country_id
      and ct.country_id          = vbc.country_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END STAGE_SUPPLIER;
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_LOCATION(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_LOCATION';

BEGIN

   select I_tax_service_id,
          loc.to_entity_type,    --entity_type,
          loc.to_entity,         --entity_id,
          loc.physical_loc,      --physical_wh,
          ---
          ctj.jurisdiction_code, --city_id
          ctj.jurisdiction_desc, --city
          st.state,              --state
          NULL,                  --state_name
          vbc.fiscal_code,       --country_id
          ct.country_desc,       --country_name
          '1',                   --addr
          addr.addr_type,        --addr_type
          '1',                   --primary_addr_type_ind
          addr.primary_addr_ind, --primary_addr_ind
          addr.add_1,            --add_1
          NULL,                  --add_2
          NULL,                  --add_3
          ---
          loc.iss_contrib_ind,   --iss_ind
          loc.ipi_ind,           --ipi_ind
          loc.icms_contrib_ind,  --icms_ind
          NULL,                  --st_ind, n/a to locations
          NULL,                  --legal_name
          'S',                   --fiscal_type
          NULL,                  --is_simples_contributor, n/a to locations
          loc.cnpj,              --federal_tax_reg_id
          'N',                   --is_rural_producer
          NULL,                  --is_income_range_eligible, n/a to locations
          NULL,                  --is_distr_a_manufacturer, n/a to locations
          NULL                   --icms_simples_rate, n/a to locations
   BULK COLLECT INTO LP_stage_fis_entity_loc_tbl
     from (--store
           select distinct
                  stg.to_entity_type,
                  stg.to_entity,
                  stg.to_entity physical_loc,
                  vs.cnpj,
                  vs.iss_contrib_ind,
                  vs.ipi_ind,
                  vs.icms_contrib_ind
             from l10n_br_tax_call_stage_rms stg,
                  v_br_store vs
            where stg.tax_service_id = I_tax_service_id
              and stg.to_entity_type = 'ST'
              and stg.to_entity = vs.store
            UNION ALL
           --warehouse
           select distinct
                  stg.to_entity_type,
                  stg.to_entity,
                  TO_CHAR(w.physical_wh) physical_loc,
                  vw.cnpj,
                  vw.iss_contrib_ind,
                  vw.ipi_ind,
                  vw.icms_contrib_ind
             from l10n_br_tax_call_stage_rms stg,
                  wh w,
                  v_br_wh vw
            where stg.tax_service_id = I_tax_service_id
              and stg.to_entity_type = 'WH'
              and stg.to_entity      = w.wh
              and w.physical_wh      = vw.wh
            UNION ALL
           --partner
           select distinct
                  stg.to_entity_type,
                  stg.to_entity,
                  stg.to_entity physical_loc,
                  vp.cnpj,
                  NULL iss_contrib_ind,
                  vp.ipi_ind,
                  vp.icms_contrib_ind
             from l10n_br_tax_call_stage_rms stg,
                  v_br_partner vp
            where stg.tax_service_id = I_tax_service_id
              and stg.to_entity_type = 'E'
              and stg.to_entity      = vp.partner_id) loc,
              addr,
              state st,
              country ct,
              country_tax_jurisdiction ctj,
              v_br_country_attrib vbc
        where addr.key_value_1           = DECODE(loc.to_entity_type,
                                                  'E', 'E',
                                                   loc.physical_loc)
          and NVL(addr.key_value_2,-999) = DECODE(loc.to_entity_type,
                                              'E', loc.physical_loc,
                                               NVL(addr.key_value_2,-999))
          and addr.state                 = ctj.state
          and addr.country_id            = ctj.country_id
          and addr.jurisdiction_code     = ctj.jurisdiction_code
          and addr.state                 = st.state
          and addr.country_id            = st.country_id
          and addr.primary_addr_ind      = 'Y'
          and addr.addr_type             = DECODE(addr.module,'WFST','07','01')
          and addr.module                in ('WH','ST','WFST','PTNR')
          and ct.country_id              = addr.country_id
          and ct.country_id              = vbc.country_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END STAGE_LOCATION;
-------------------------------------------------------------------------------------------------------------
FUNCTION STAGE_ITEM(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS

   L_incl_nic_ind_pis      VAT_CODES.INCL_NIC_IND%TYPE := NULL;
   L_incl_nic_ind_cofins   VAT_CODES.INCL_NIC_IND%TYPE := NULL;
   L_incl_nic_ind_icms     VAT_CODES.INCL_NIC_IND%TYPE := NULL;
   L_program               VARCHAR2(61) := 'L10N_BR_INT_SQL.STAGE_ITEM';

BEGIN

   -- Get tax indicators for the Brazil types needed for item staging
   if GET_TAX_INDS(O_error_message,
                   L_incl_nic_ind_pis,
                   L_incl_nic_ind_cofins,
                   L_incl_nic_ind_icms) = FALSE then
      return FALSE;
   end if;

   -- Stage distinct items
   select distinct
          I_tax_service_id,
          stg.item,            --document_line_id
          stg.item,            --item_id
          im.item_desc,        --item_desdescription
          '5401',              --item_tran_code
          DECODE(vbi.service_ind,'N','M','S'),   --item_type
          stg.quantity,                          --quantity
          im.standard_uom,                       --unit_of_measure
          im.uom_conv_factor,                    --uom_conv_factor
          stg.quantity*NVL(im.uom_conv_factor, 1), --quantity_in_eaches
          NULL,                --origin_doc_date
          stg.pack_item,       --pack_item_id
          stg.amount*stg.quantity,  --total_cost
          stg.amount,          --unit_cost
          NULL,                --src_taxpayer_type
          NULL,                --orig_fiscal_doc_number
          NULL,                --orig_fiscal_doc_series
          --
          NULL,  --dim_object
          NULL,  --length
          NULL,  --width
          NULL,  --lwh_uom
          NULL,  --weight
          NULL,  --net_weight
          NULL,  --weight_uom
          NULL,  --liquid_volume
          NULL,  --liquid_volume_uom
          --
          NULL,  --origin_fiscal_code_opr
          NULL,  --deduced_fiscal_code_opr
          'Y',   --deduce_cfop_code
          NULL,  --icms_cst_code
          NULL,  --pis_cst_code
          NULL,  --cofins_cst_code
          NULL,  --deduce_icms_cst_code
          NULL,  --deduce_pis_cst_codE
          NULL,  --deduce_cofins_cst_code
          NULL,  --recoverable_icmsst
          DECODE(NVL(stg.tax_incl_ind, 'N'), 'Y', L_incl_nic_ind_cofins, 'N'),--item_cost_contains_cofins
          NULL,  --recoverable_base_icmsst
          DECODE(NVL(stg.tax_incl_ind, 'N'), 'Y', L_incl_nic_ind_pis, 'N'),--item_cost_contains_pis
          DECODE(NVL(stg.tax_incl_ind, 'N'), 'Y', L_incl_nic_ind_icms, 'N'),--item_cost_contains_icms
          vbi.service_ind,                             --service_ind
          vbi.origin_code,                             --item_origin
          'C',                                         --item_utilization
          im.item_xform_ind,                           --is_transformed_item
          vbi.classification_id,                       --fiscal_classification_code
          case when vbi.ncm_char_code is NOT NULL and vbi.pauta_code is NOT NULL then
                  vbi.ncm_char_code||'.'||vbi.pauta_code
               when vbi.ncm_char_code is NOT NULL and vbi.pauta_code is NULL then
                  vbi.ncm_char_code
               when vbi.ncm_char_code is NULL and vbi.pauta_code is not NULL then
                  vbi.pauta_code
               else
                  NULL
          end, --ext_fiscal_class_code,
          DECODE(vbi.service_ind,'N',vbi.ex_ipi,NULL), --ipi_exception_code
          NULL,                                        --product_type
          vbi.state_of_manufacture,                    --state_of_manufacture
          vbi.pharma_list_type,                        --pharma_list_type
          vbi.federal_service,                         --federal_service_code
          vbi.service_code,                            --dst_service_code
          -1,                                          --item_group_id
          -1                                           --item_group_counter
   BULK COLLECT INTO LP_stage_item_tbl
     from l10n_br_tax_call_stage_rms stg,
          v_br_item_fiscal_attrib vbi,
          item_master im
    where stg.tax_service_id     = I_tax_service_id
      and stg.item               = vbi.item
      and stg.item               = im.item;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END STAGE_ITEM;
-------------------------------------------------------------------------------------------------------------
FUNCTION PERSIST_STAGE_RMS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE)
RETURN BOOLEAN IS
   PRAGMA AUTONOMOUS_TRANSACTION;
   L_program          VARCHAR2(61) := 'L10N_BR_INT_SQL.PERSIST_STAGE_RMS';

BEGIN

   if LP_stage_rms_tbl is not NULL and LP_stage_rms_tbl.COUNT > 0 then
      FORALL i in LP_stage_rms_tbl.FIRST..LP_stage_rms_tbl.LAST
         insert into l10n_br_tax_call_stage_rms values LP_stage_rms_tbl(i);
   end if;

   commit;

   --Clear arrays
   LP_stage_rms_tbl.DELETE;

   return TRUE;

EXCEPTION
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PERSIST_STAGE_RMS;
-------------------------------------------------------------------------------------------------------------
FUNCTION PERSIST_STAGE_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tax_service_id  IN      L10N_BR_TAX_CALL_STAGE_RMS.TAX_SERVICE_ID%TYPE,
                            I_call_type       IN      VARCHAR2)
RETURN BOOLEAN IS
   PRAGMA AUTONOMOUS_TRANSACTION;
   L_program          VARCHAR2(61) := 'L10N_BR_INT_SQL.PERSIST_STAGE_DATA';

   L_thread_item_loc_count l10n_tax_object_config.thread_item_loc_count%TYPE := NULL;

   cursor c_thread_count is
      select thread_item_loc_count
        from l10n_tax_object_config;

BEGIN
   
   if LP_stage_routing_tbl is not NULL and LP_stage_routing_tbl.COUNT > 0 then
      FORALL i in LP_stage_routing_tbl.FIRST..LP_stage_routing_tbl.LAST
         insert into l10n_br_tax_call_stage_routing values LP_stage_routing_tbl(i);
   end if;

   if LP_stage_eco_sup_tbl is not NULL and LP_stage_eco_sup_tbl.COUNT > 0 then
      FORALL i in LP_stage_eco_sup_tbl.FIRST..LP_stage_eco_sup_tbl.LAST
         insert into l10n_br_tax_stage_eco values LP_stage_eco_sup_tbl(i);
   end if;
   
   if LP_stage_eco_loc_tbl is not NULL and LP_stage_eco_loc_tbl.COUNT > 0 then
      FORALL i in LP_stage_eco_loc_tbl.FIRST..LP_stage_eco_loc_tbl.LAST
         insert into l10n_br_tax_stage_eco values LP_stage_eco_loc_tbl(i);
   end if;

   if LP_stage_fis_entity_sup_tbl is not NULL and LP_stage_fis_entity_sup_tbl.COUNT > 0 then
      FORALL i in LP_stage_fis_entity_sup_tbl.FIRST..LP_stage_fis_entity_sup_tbl.LAST
         insert into l10n_br_tax_stage_fis_entity values LP_stage_fis_entity_sup_tbl(i);
   end if;

   if LP_stage_fis_entity_loc_tbl is not NULL and LP_stage_fis_entity_loc_tbl.COUNT > 0 then
      FORALL i in LP_stage_fis_entity_loc_tbl.FIRST..LP_stage_fis_entity_loc_tbl.LAST
         insert into l10n_br_tax_stage_fis_entity values LP_stage_fis_entity_loc_tbl(i);
   end if;

   if LP_stage_item_tbl is not NULL and LP_stage_item_tbl.COUNT > 0 then
      FORALL i in LP_stage_item_tbl.FIRST..LP_stage_item_tbl.LAST
         insert into l10n_br_tax_stage_item values LP_stage_item_tbl(i);
   end if;

   if LP_stage_name_value_tbl is not NULL and LP_stage_name_value_tbl.COUNT > 0 then
      FORALL i in LP_stage_name_value_tbl.FIRST..LP_stage_name_value_tbl.LAST
         insert into l10n_br_tax_stage_name_value values LP_stage_name_value_tbl(i);
   end if;

   if LP_stage_order_tbl is not NULL and LP_stage_order_tbl.COUNT > 0 then
      FORALL i in LP_stage_order_tbl.FIRST..LP_stage_order_tbl.LAST
         insert into l10n_br_tax_stage_order values LP_stage_order_tbl(i);
   end if;

   if LP_stage_order_exp_tbl is not NULL and LP_stage_order_exp_tbl.COUNT > 0 then
      FORALL i in LP_stage_order_exp_tbl.FIRST..LP_stage_order_exp_tbl.LAST
         insert into l10n_br_tax_stage_order_exp values LP_stage_order_exp_tbl(i);
   end if;

   if LP_stage_order_info_tbl is not NULL and LP_stage_order_info_tbl.COUNT > 0 then
      FORALL i in LP_stage_order_info_tbl.FIRST..LP_stage_order_info_tbl.LAST
         insert into l10n_br_tax_stage_order_info values LP_stage_order_info_tbl(i);
   end if;

   if LP_stage_regime_tbl is not NULL and LP_stage_regime_tbl.COUNT > 0 then
      FORALL i in LP_stage_regime_tbl.FIRST..LP_stage_regime_tbl.LAST
         insert into l10n_br_tax_stage_regime values LP_stage_regime_tbl(i);
   end if;

   open c_thread_count;
   fetch c_thread_count into L_thread_item_loc_count;
   close c_thread_count;

   -- The code section below implements "chunking" logic to allow concurrent execution in RTIL.
   -- This logic also reduces the amount of data sent to RTIL by grouping items by common fiscal attributes and
   -- sending only one 1 item per group.  This only applies to non-pack items.
   -- A wrapper object (RIB_FiscDocChnkRBO_REC) and a sizing parameter (l10n_tax_object_config.thread_item_loc_count)
   -- was introduced to facilitate this logic.
   -- The steps per scenario (Sale, Purchase, PO/DeaL) goes as follows:
   -- 1. Group the staged records according to "common" fiscal attributes.
   --    There is a set of fiscal attributes per scenario that determines the "uniqueness" of a record in RTIL/TaxWeb.
   -- 2. A merge statement is used group and assign a rank number per tax attribute group.
   --    For a sale scenario, the rank is stored in l10n_br_tax_stage_item.item_group_id.
   --    For a purchase scenario, the rank is stored in l10n_br_tax_stage_order_info.item_group_id.
   --    For a PO/deal scenario, the rank is stored in l10n_br_tax_stage_order_info.item_group_id.
   --    This rank number is an "item group id" which identifies which unique set of tax attribute a row belongs to
   -- 3  A second merge statement is used to determine and persist the chunk numbers to l10n_br_tax_call_stage_rms.chunk_id column.
   --    The idea is to group records under a tax attribute group within a single chunk.  This considers the unique "key" used in the staging tables.
   -- 4. Succeeding logic initiated by the RTIL callback (LOAD_*_OBJECT functions) groups fiscal documents (RIB_FiscDocRBO_REC) with the same
   --    chunk id under a single fiscal document chunk (RIB_FiscDocChnkRBO_REC).  For non-pack items, only the representative item per unique
   --    fiscal attribute group is sent.
   -- 5. After RTIL/TaxWeb sends back the results, the STAGE_RESULTS functions persists to the result tables considering the new chunk object.
   -- 6. Once control is returned to this package, the result processing function (PROCESS_RESULTS) maps the representative items on the result tables
   --    back to the rest of the items under the same fiscal attribute group.

   if I_call_type = 'S' then

      merge into l10n_br_tax_stage_item target using (
         select distinct inner.item_id,
                inner.pack_item_id,
                inner.item_group_id,
                rank() over (partition by inner.item_group_id order by inner.item_id) item_group_counter
           from (select i.item_id,
                        i.pack_item_id,
                        dense_rank() over (order by i.fiscal_classification_code,
                                                    i.ext_fiscal_class_code,
                                                    i.ipi_exception_code,
                                                    i.service_ind,
                                                    i.dst_service_code,
                                                    i.federal_service_code,
                                                    i.state_of_manufacture,
                                                    i.pharma_list_type,
                                                    i.dim_object,
                                                    i.length,
                                                    i.lwh_uom,
                                                    i.weight,
                                                    i.net_weight,
                                                    i.weight_uom,
                                                    i.liquid_volume,
                                                    i.liquid_volume_uom,
                                                    nv.item_name_value_pair_id_concat,
                                                    i.pack_item_id,
                                                    DECODE(i.pack_item_id,NULL,NULL,i.item_id),
                                                    i.is_transformed_item) item_group_id
                   from l10n_br_tax_stage_item i,
                        (select entity_id item_id,
                                listagg (name||'+'||value, '~')
                                  within group (order by entity_type, entity_id) item_name_value_pair_id_concat
                           from l10n_br_tax_stage_name_value
                          where tax_service_id = I_tax_service_id
                            and entity_type    = 'ITEM'
                            group by entity_id
                        ) nv
                  where i.tax_service_id = I_tax_service_id
                    and i.item_id        = nv.item_id(+)) inner) use_this
      on (    target.tax_service_id         = I_tax_service_id
          and target.item_id                = use_this.item_id
          and NVL(target.pack_item_id,'-1') = NVL(use_this.pack_item_id,'-1'))
      when matched then
         update set target.item_group_id       = use_this.item_group_id,
                    target.item_group_counter  = use_this.item_group_counter;

      merge into l10n_br_tax_call_stage_rms target using (
         select r.item,
                r.pack_item,
                r.effective_date,
                r.from_entity,
                r.from_entity_type,
                ceil(rank() over (order by r.effective_date,
                                           r.from_entity,
                                           r.from_entity_type,
                                           -- Use i.item_group_id for non-packs
                                           -- Use i_pack_item_id for packs to keep pack components in the same chunk
                                           DECODE(NVL(i.pack_item_id,'-1'),
                                                  '-1', i.item_group_id,
                                                  i.pack_item_id))/ L_thread_item_loc_count) chunk_id
           from l10n_br_tax_call_stage_rms r,
                l10n_br_tax_stage_item i
          where r.tax_service_id      = I_tax_service_id
            and r.tax_service_id      = i.tax_service_id
            and r.item                = i.item_id
            and NVL(r.pack_item,'-1') = NVL(i.pack_item_id,'-1')
            and i.item_group_counter  = 1
      ) use_this
      on (    target.tax_service_id      = I_tax_service_id
          and target.item                = use_this.item
          and NVL(target.pack_item,'-1') = NVL(use_this.pack_item,'-1')
          and target.effective_date      = use_this.effective_date
          and target.from_entity         = use_this.from_entity
          and target.from_entity_type    = use_this.from_entity_type)
      when matched then
         update set target.chunk_id  = use_this.chunk_id;

   elsif I_call_type = 'P' then

      merge into l10n_br_tax_stage_order_info target using (
         select distinct inner.item_id,
                inner.pack_item_id,
                inner.to_entity,
                inner.to_entity_type,
                inner.from_entity,
                inner.from_entity_type,
                inner.origin_country_id,
                inner.unit_cost,
                inner.item_group_id,
                rank() over (partition by inner.item_group_id order by inner.item_id) item_group_counter
           from (select distinct
                        i.item_id,
                        i.pack_item_id,
                        o.to_entity,
                        o.to_entity_type,
                        o.from_entity,
                        o.from_entity_type,
                        o.origin_country_id,
                        i.unit_cost,
                        dense_rank() over (order by o.doc_id,
                                                    o.to_entity,
                                                    o.to_entity_type,
                                                    o.from_entity,
                                                    o.from_entity_type,
                                                    o.origin_country_id,
                                                    i.fiscal_classification_code,
                                                    i.ext_fiscal_class_code,
                                                    i.ipi_exception_code,
                                                    i.service_ind,
                                                    i.dst_service_code,
                                                    i.federal_service_code,
                                                    i.state_of_manufacture,
                                                    i.pharma_list_type,
                                                    o.dim_object,
                                                    o.length,
                                                    o.lwh_uom,
                                                    o.weight,
                                                    o.net_weight,
                                                    o.weight_uom,
                                                    o.liquid_volume,
                                                    o.liquid_volume_uom,
                                                    i.unit_cost,
                                                    nv.item_name_value_pair_id_concat,
                                                    i.pack_item_id,
                                                    DECODE(i.pack_item_id,NULL,NULL,i.item_id),
                                                    i.is_transformed_item) item_group_id
                   from l10n_br_tax_stage_item i,
                        l10n_br_tax_stage_order_info o,
                        (select entity_id item_id,
                                listagg (name||'+'||value, '~')
                                  within group (order by entity_type, entity_id) item_name_value_pair_id_concat
                           from l10n_br_tax_stage_name_value
                          where tax_service_id = I_tax_service_id
                            and entity_type    = 'ITEM'
                            group by entity_id
                        ) nv
                  where i.tax_service_id          = I_tax_service_id
                    --
                    and i.tax_service_id          = o.tax_service_id
                    and i.item_id                 = o.item_id
                    and NVL(i.unit_cost,'-1')    = NVL(o.unit_cost,'-1')
                    and NVL(i.pack_item_id,'-1')  = NVL(o.pack_item_id,'-1')
                    --
                    and i.item_id                 = nv.item_id(+)) inner) use_this
      on (    target.tax_service_id         = I_tax_service_id
          and target.item_id                = use_this.item_id
          and NVL(target.pack_item_id,'-1') = NVL(use_this.pack_item_id,'-1')
          and target.to_entity              = use_this.to_entity
          and target.to_entity_type         = use_this.to_entity_type
          and target.from_entity            = use_this.from_entity
          and target.from_entity_type       = use_this.from_entity_type
          and target.origin_country_id      = use_this.origin_country_id
          and target.unit_cost              = use_this.unit_cost)
      when matched then
         update set target.item_group_id       = use_this.item_group_id,
                    target.item_group_counter  = use_this.item_group_counter;

      merge into l10n_br_tax_call_stage_rms target using (
         select r.item,
                r.pack_item,
                r.effective_date,
                r.from_entity,
                r.from_entity_type,
                r.to_entity,
                r.to_entity_type,
                r.origin_country_id,
                ceil(rank() over (order by r.effective_date,
                                           r.from_entity,
                                           r.from_entity_type,
                                           r.to_entity,
                                           r.to_entity_type,
                                           r.origin_country_id,
                                           -- Use i.item_group_id for non-packs
                                           -- Use i_pack_item_id for packs to keep pack components in the same chunk
                                           DECODE(NVL(i.pack_item_id,'-1'),
                                                  '-1', i.item_group_id,
                                                  i.pack_item_id))/ L_thread_item_loc_count) chunk_id
           from l10n_br_tax_call_stage_rms r,
                l10n_br_tax_stage_order_info i
          where r.tax_service_id              = I_tax_service_id
            and r.tax_service_id              = i.tax_service_id
            and r.item                        = i.item_id
            and NVL(r.pack_item,'-1')         = NVL(i.pack_item_id,'-1')
            and r.to_entity                   = i.to_entity
            and r.to_entity_type              = i.to_entity_type
            and r.from_entity                 = i.from_entity
            and r.from_entity_type            = i.from_entity_type
            and NVL(r.origin_country_id,'-1') = NVL(i.origin_country_id,'-1')
            and i.item_group_counter          = 1
      ) use_this
      on (    target.tax_service_id      = I_tax_service_id
          and target.item                = use_this.item
          and NVL(target.pack_item,'-1') = NVL(use_this.pack_item,'-1')
          and target.effective_date      = use_this.effective_date
          and target.from_entity         = use_this.from_entity
          and target.from_entity_type    = use_this.from_entity_type
          and target.to_entity           = use_this.to_entity
          and target.to_entity_type      = use_this.to_entity_type
          and target.origin_country_id   = use_this.origin_country_id)
      when matched then
         update set target.chunk_id  = use_this.chunk_id;

   elsif I_call_type = 'PO' then

      merge into l10n_br_tax_stage_order_info target using (
         select distinct inner.item_id,
                inner.pack_item_id,
                inner.doc_id,
                inner.from_entity,
                inner.from_entity_type,
                inner.to_entity,
                inner.to_entity_type,
                inner.origin_country_id,
                inner.item_group_id,
                rank() over (partition by inner.item_group_id order by inner.item_id) item_group_counter
           from (select item_id,
                        pack_item_id,
                        doc_id,
                        to_entity,
                        to_entity_type,
                        from_entity,
                        from_entity_type,
                        origin_country_id,
                        dense_rank() over (order by doc_id, 
                                                  to_entity,
                                                  to_entity_type,
                                                  from_entity,
                                                  from_entity_type,
                                                  origin_country_id,
                                                  fiscal_classification_code,
                                                  ext_fiscal_class_code,
                                                  ipi_exception_code,
                                                  service_ind,
                                                  dst_service_code,
                                                  federal_service_code,
                                                  state_of_manufacture,
                                                  pharma_list_type,
                                                  dim_object,
                                                  length,
                                                  lwh_uom,
                                                  weight,
                                                  net_weight,
                                                  weight_uom,
                                                  liquid_volume,
                                                  liquid_volume_uom,
                                                  unit_cost,
                                                  item_name_value_pair_id_concat,
                                                  freight,
                                                  insurance,
                                                  other_expenses,
                                                  discount,
                                                  pack_item_id,
                                                  DECODE(pack_item_id,NULL,NULL,item_id),
                                                  is_transformed_item) item_group_id
                   from (select distinct       
                                    i.item_id,
                                    i.pack_item_id,
                                    i.unit_cost,
                                    o.doc_id,
                                    o.to_entity,
                                    o.to_entity_type,
                                    o.from_entity,
                                    o.from_entity_type,
                                    o.origin_country_id,
                                    i.fiscal_classification_code,
                                    i.ext_fiscal_class_code,
                                    i.ipi_exception_code,
                                    i.service_ind,
                                    i.dst_service_code,
                                    i.federal_service_code,
                                    i.state_of_manufacture,
                                    i.pharma_list_type,
                                    o.dim_object,
                                    o.length,
                                    o.lwh_uom,
                                    o.weight,
                                    o.net_weight,
                                    o.weight_uom,
                                    o.liquid_volume,
                                    o.liquid_volume_uom, 
                                    nv.item_name_value_pair_id_concat, 
                                    --
                                    e.freight,
                                    e.insurance,
                                    e.other_expenses,
                                    e.discount,
                                    --
                                    i.is_transformed_item
                               from l10n_br_tax_stage_item i,
                                    l10n_br_tax_stage_order_info o,
                                    l10n_br_tax_stage_order_exp e,
                                    (select entity_id item_id,
                                            listagg (name||'+'||value, '~')
                                              within group (order by entity_type, entity_id) item_name_value_pair_id_concat
                                       from l10n_br_tax_stage_name_value
                                      where tax_service_id = I_tax_service_id
                                        and entity_type    = 'ITEM'
                                        group by entity_id
                                    ) nv
                  where i.tax_service_id          = I_tax_service_id
                    --
                    and i.tax_service_id          = o.tax_service_id
                    and i.item_id                 = o.item_id
                    and NVL(i.pack_item_id,'-1')  = NVL(o.pack_item_id,'-1')
                    and NVL(i.unit_cost,'-1')     = NVL(o.unit_cost,'-1')
                    and NVL(o.quantity,'-1')      = NVL(i.quantity,'-1')
                    --
                    and o.tax_service_id          = e.tax_service_id(+)
                    and o.doc_id                  = e.order_no(+)
                    and o.item_id                 = e.item(+)
                    and NVL(o.pack_item_id,'-1')  = NVL(e.pack_no(+),'-1')
                    and o.to_entity               = e.location(+)
                    --
                    and i.item_id                 = nv.item_id(+))) inner) use_this
      on (target.tax_service_id      = I_tax_service_id
          and target.item_id                = use_this.item_id
          and NVL(target.pack_item_id,'-1') = NVL(use_this.pack_item_id,'-1')
          and target.doc_id                 = use_this.doc_id
          and target.from_entity            = use_this.from_entity
          and target.from_entity_type       = use_this.from_entity_type
          and target.to_entity              = use_this.to_entity
          and target.to_entity_type         = use_this.to_entity_type
          and target.origin_country_id      = use_this.origin_country_id)
      when matched then
         update set target.item_group_id       = use_this.item_group_id,
                    target.item_group_counter  = use_this.item_group_counter; 
                    
      merge into l10n_br_tax_call_stage_rms target using (
         select r.item,
                r.pack_item,
                r.effective_date,
                r.doc_id,
                r.from_entity,
                r.from_entity_type,
                r.to_entity,
                r.to_entity_type,
                r.origin_country_id,
                ceil(rank() over (order by r.effective_date,
                                           r.doc_id,
                                           r.from_entity,
                                           r.from_entity_type,
                                           r.to_entity,
                                           r.to_entity_type,
                                           r.origin_country_id,
                                           -- Use i.item_group_id for non-packs
                                           -- Use i_pack_item_id for packs to keep pack components in the same chunk
                                           DECODE(NVL(i.pack_item_id,'-1'),
                                                  '-1', i.item_group_id,
                                                  i.pack_item_id))/ L_thread_item_loc_count) chunk_id
           from l10n_br_tax_call_stage_rms r,
                l10n_br_tax_stage_order_info i
          where r.tax_service_id      = I_tax_service_id
            and r.tax_service_id      = i.tax_service_id
            and r.item                = i.item_id
            and NVL(r.pack_item,'-1') = NVL(i.pack_item_id,'-1')
            and r.doc_id              = i.doc_id
            and r.from_entity         = i.from_entity
            and r.from_entity_type    = i.from_entity_type
            and r.to_entity           = i.to_entity
            and r.to_entity_type      = i.to_entity_type
            and NVL(r.origin_country_id,'-1') = NVL(i.origin_country_id,'-1')
            and i.item_group_counter  = 1
      ) use_this
      on (    target.tax_service_id      = I_tax_service_id
          and target.item                = use_this.item
          and NVL(target.pack_item,'-1') = NVL(use_this.pack_item,'-1')
          and target.effective_date      = use_this.effective_date
          and target.doc_id              = use_this.doc_id
          and target.from_entity         = use_this.from_entity
          and target.from_entity_type    = use_this.from_entity_type
          and target.to_entity           = use_this.to_entity
          and target.to_entity_type      = use_this.to_entity_type
          and target.origin_country_id   = use_this.origin_country_id)
      when matched then
         update set target.chunk_id  = use_this.chunk_id;
   
   end if;
         
   commit;

   --Clear arrays
   LP_stage_routing_tbl.DELETE;
   LP_stage_eco_sup_tbl.DELETE;
   LP_stage_eco_loc_tbl.DELETE;
   LP_stage_fis_entity_sup_tbl.DELETE;
   LP_stage_fis_entity_loc_tbl.DELETE;
   LP_stage_item_tbl.DELETE;
   LP_stage_name_value_tbl.DELETE;
   LP_stage_order_tbl.DELETE;
   LP_stage_order_exp_tbl.DELETE;
   LP_stage_order_info_tbl.DELETE;
   LP_stage_regime_tbl.DELETE;
   
  return TRUE;

EXCEPTION
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PERSIST_STAGE_DATA;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INDS(O_error_message        IN OUT rtk_errors.rtk_text%type,
                      O_incl_nic_ind_pis        OUT vat_codes.incl_nic_ind%TYPE,
                      O_incl_nic_ind_cofins     OUT vat_codes.incl_nic_ind%TYPE,
                      O_incl_nic_ind_icms       OUT vat_codes.incl_nic_ind%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'L10N_BR_INT_SQL.GET_TAX_INDS';

   cursor C_GET_TAX_IND is
      select DECODE(v1.vat_code, 'PIS',    v1.incl_nic_ind, NULL) pis,
             DECODE(v2.vat_code, 'COFINS', v2.incl_nic_ind, NULL) cofins,
             DECODE(v3.vat_code, 'ICMS',   v3.incl_nic_ind, NULL) icms
        from vat_codes v1,
             vat_codes v2,
             vat_codes v3
       where v1.vat_code in ('PIS')
         and v2.vat_code in ('COFINS')
         and v3.vat_code in ('ICMS');

BEGIN

   open C_GET_TAX_IND;
   fetch C_GET_TAX_IND into O_incl_nic_ind_pis,
                            O_incl_nic_ind_cofins,
                            O_incl_nic_ind_icms;
   close C_GET_TAX_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;
END GET_TAX_INDS;
-------------------------------------------------------------------------------------------------------------
END L10N_BR_INT_SQL;
/
