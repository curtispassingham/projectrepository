CREATE OR REPLACE PACKAGE L10N_BR_PO_SQL AS
------------------------------------------------------------------------------------
-- Name: CHECK_UTIL_CODE_EXISTS
-- Purpose: This function is used to check if the utilization code
--          existts for a given transaction or not.
------------------------------------------------------------------------------------
FUNCTION CHECK_UTIL_CODE_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:    CREATE_TRAN_UTIL_CODE
-- Purpose: This function is used to create utilization record for
--          the corresponding transactions in the extension tables.
--
-------------------------------------------------------------------------------------
FUNCTION CREATE_TRAN_UTIL_CODE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:    CHECK_IF_BRAZIL_LOCALIZED
-- Purpose: This function if any of the BRAZIL locations are localized
-------------------------------------------------------------------------------------
FUNCTION CHECK_IF_BRAZIL_LOCALIZED(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:    COPY_TRAN_UTIL_CODE
-- Purpose: This function is used to copy the util code from mrt_l10n_ext
--          to tsfhead_l10n_ext table
-------------------------------------------------------------------------------------
FUNCTION COPY_TRAN_UTIL_CODE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Name:    UPDATE_TRAN_UTIL_CODE
-- Purpose: This function is used to update the utilization code for SIM generated
--          Return to Warehouse Repairing transfers
-------------------------------------------------------------------------------------
FUNCTION UPDATE_TRAN_UTIL_CODE(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_l10n_obj      IN OUT   L10N_OBJ)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
--LFAS Field Validation Function(s)
---------------------------------------------------------------------------------------------
-- Function Name: GET_UTILTSF_DESC
-- Purpose      : This function will get the utilization_desc from fm_fiscal_utilization table
--                for the util_id parameter and for the requisition type as Transfers
---------------------------------------------------------------------------------------------
FUNCTION GET_UTILTSF_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_util_desc       IN OUT   FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                          I_util_id         IN       FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_UTILPO_DESC
-- Purpose      : This function will get the utilization_desc from fm_fiscal_utilization table
--                for the util_id parameter and for the requisition type as purchase orders
---------------------------------------------------------------------------------------------
FUNCTION GET_UTILPO_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_util_desc       IN OUT   FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                         I_util_id         IN       FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CREATE_ORDCUST_L10N_EXT
-- Purpose      : This function will create a record in the ORDCUST_L10N_EXT table for the
--                customer order transfer.
---------------------------------------------------------------------------------------------
FUNCTION CREATE_ORDCUST_L10N_EXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_l10n_obj        IN       L10N_OBJ)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDCUST_EXT
-- Purpose      : This function will update a record in the ORDCUST_L10N_EXT table for the
--                customer order transfer. Only the fiscal attributes will be updated.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDCUST_EXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_l10n_obj        IN       L10N_OBJ)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_SVC_TABLES_FULFILORD_L10N
-- Purpose      : this returns a collection of staging table names for the localized country
-- (e.g. SVC_BRFULFILORD)
---------------------------------------------------------------------------------------------
FUNCTION GET_SVC_TABLES_FULFILORD_L10N(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       IO_l10n_obj       IN OUT   L10N_OBJ)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END L10N_BR_PO_SQL;
/