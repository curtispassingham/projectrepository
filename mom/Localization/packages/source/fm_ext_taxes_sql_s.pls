create or replace PACKAGE FM_EXT_TAXES_SQL is

GP_tax_tab            "RIB_TaxDetRBO_TBL"        :=  "RIB_TaxDetRBO_TBL"();
GP_tax_ind            NUMBER                     := 0;
GP_fetch_comp_taxes   BOOLEAN                    := FALSE;
GP_businessObject     "RIB_FiscDocTaxColRBM_REC" := "RIB_FiscDocTaxColRBM_REC"(0,null,null,null,null,null);
GP_line_pack          BOOLEAN                    := FALSE;

-- Type declarations
-------------------------------------------------------------------------------------------------------------
TYPE result_data_hdr_tbl            IS TABLE OF FM_TAX_CALL_HDR_RES%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_data_hdr_tax_tbl        IS TABLE OF FM_TAX_CALL_RES_HDR_TAX%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_data_item_tbl           IS TABLE OF FM_TAX_CALL_DTL_RES%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_data_item_tax_tbl       IS TABLE OF FM_TAX_CALL_RES_DTL_TAX%ROWTYPE INDEX BY BINARY_INTEGER;
TYPE result_data_rule_tbl           IS TABLE OF FM_TAX_CALL_RES_RULES%ROWTYPE INDEX BY BINARY_INTEGER;
--TYPE stage_data_tbl                 IS TABLE OF L10N_BR_TAX_CALL_STAGE_RMS%ROWTYPE INDEX BY BINARY_INTEGER;

----------------------------------------------------------------------------------
FUNCTION GET_REQUEST_DATA(O_error_msg             IN OUT  VARCHAR2,
                          I_tax_service_id        IN      NUMBER,
                          O_businessObject        IN OUT  "RIB_FiscDocColRBM_REC",
			                    I_update_history_ind    IN      L10N_BR_TAX_CALL_STAGE_ROUTING.UPDATE_HISTORY_IND%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
FUNCTION GET_EXT_TAX_VALUES(O_line_tax             IN OUT "RIB_TaxDetRBO_TBL",
                            O_error_message        IN OUT VARCHAR2,
                            O_status               IN OUT INTEGER,
                            I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_fiscal_doc_line_id   IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            I_quantity             IN     FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE,
                            I_cost                 IN     FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                            I_form_ind             IN     VARCHAR2 DEFAULT 'Y',
                            I_compensation_ind     IN     VARCHAR2 DEFAULT 'N')
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_LEGAL_NAME
-- Purpose:       This function fetches the secondary name, i.e. the legal name for the module
----------------------------------------------------------------------------------
FUNCTION GET_LEGAL_NAME (I_module       IN    FM_FISCAL_DOC_HEADER.MODULE%TYPE,
                         I_module_id    IN    FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,
                         I_module_type  IN    FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE)
   return VARCHAR2;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_HIST_TABLES
-- Purpose:       This function calls the External tax engine and updates the history tables.
----------------------------------------------------------------------------------
FUNCTION UPDATE_HIST_TABLES(O_error_message        IN OUT VARCHAR2,
                            O_status               IN OUT INTEGER,
                            I_fiscal_doc_id        IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION STAGE_RESULTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tax_service_id   IN      FM_TAX_CALL_STAGE.TAX_SERVICE_ID%TYPE,
                       I_businessObject   IN     "RIB_FiscDocTaxColRBM_REC")
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
END FM_EXT_TAXES_SQL ;
/
