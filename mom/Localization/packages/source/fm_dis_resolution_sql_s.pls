CREATE or REPLACE PACKAGE FM_DIS_RESOLUTION_SQL as
----------------------------------------------------------------------------------
-- Function Name: LOCK_RESOLUTION
-- Purpose:       This function Locks the record. This is for concurrency check
----------------------------------------------------------------------------------
 FUNCTION LOCK_RESOLUTION (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_fiscal_doc_id  IN      FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                           I_discrep_type   IN      FM_RESOLUTION.DISCREP_TYPE%TYPE)
   RETURN BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_MAIN_NF_LINE_FOR_COMPL_NF
-- Purpose:       This function gets the Main NF and NF line for complementory NF
----------------------------------------------------------------------------------
 FUNCTION GET_MAIN_NF_LINE_FOR_COMPL_NF(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_FISCAL_DOC_ID            IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                        I_FISCAL_DOC_LINE_ID       IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                        O_MAIN_FISCAL_DOC_ID       OUT     FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE,
                                        O_MAIN_FISCAL_DOC_LINE_ID  OUT     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_FISCAL_DOC_NO
-- Purpose:       This function returns the query for record_group REC_FISCAL_DOC_NO
----------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_query           IN OUT VARCHAR2,
                           I_location_id     IN     FM_EDI_DOC_DETAIL.LOCATION_ID%TYPE,
                           I_loc_type        IN     FM_EDI_DOC_DETAIL.LOCATION_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_VALIDATE_FISCAL_DOC_NO
-- Purpose:       This function validates the Fiscal Document number entered from form before opening the LOV
----------------------------------------------------------------------------------
FUNCTION LOV_VALIDATE_FISCAL_DOC_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_valid          IN OUT BOOLEAN,
                                    I_fiscal_doc_no  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_SUPPLIER
-- Purpose:       This function returns the query for record_group REC_SUPPLIER
----------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_query           IN OUT VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_SUPPLIER_SITE
-- Purpose:       This function returns the query for record_group REC_SUPPLIER
----------------------------------------------------------------------------------
FUNCTION LOV_SUPPLIER_SITE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_query          IN OUT VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT BOOLEAN,
			   O_sups            IN OUT V_SUPS%ROWTYPE,
			   I_supplier        IN     SUPS.SUPPLIER%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_SUPPLIER_SITE
-- Purpose:       This function returns the query for record_group REC_SUPPLIER
----------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER_SITE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists         IN OUT BOOLEAN,
			        O_sups           IN OUT V_SUPS%ROWTYPE,
			        I_supplier_site  IN     SUPS.SUPPLIER%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_LOC
-- Purpose:       This function returns the query for record_group REC_LOC
----------------------------------------------------------------------------------
FUNCTION LOV_LOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_query           IN OUT VARCHAR2,
                 I_where_clause    IN     VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_PO_NO
-- Purpose:       This function returns the query for record_group REC_PO_NO
----------------------------------------------------------------------------------
FUNCTION LOV_PO_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_query          IN OUT VARCHAR2,
                   I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                   I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                   I_supplier       IN     FM_SCHEDULE.MODE_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_VALIDATE_PO_NO
-- Purpose:       This function validates the PO number entered from form before opening the LOV
----------------------------------------------------------------------------------
FUNCTION LOV_VALIDATE_PO_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT BOOLEAN,
                            I_po_no          IN     ORDHEAD.ORDER_NO%TYPE)
return BOOLEAN ;
----------------------------------------------------------------------------------
-- Function Name: LOV_SCHEDULE_NO
-- Purpose:       This function returns the query for record_group REC_SCHEDULE
----------------------------------------------------------------------------------
FUNCTION LOV_SCHEDULE_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_query          IN OUT VARCHAR2,
                         I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                         I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                         I_mode_type      IN     FM_SCHEDULE.MODE_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOV_VALIDATE_SCHED_NO
-- Purpose:       This function validates the schedule number entered from form before opening the LOV
----------------------------------------------------------------------------------
FUNCTION LOV_VALIDATE_SCHED_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid          IN OUT BOOLEAN,
                               I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN ;
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for block B_FM_DIS_RESOLUTION
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_where_clause       OUT VARCHAR2,
                          I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                          I_supplier           IN     SUPS.SUPPLIER%TYPE,
                          I_po_number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                          I_location_type      IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                          I_location_id        IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                          I_schedule_no        IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                          I_fiscal_doc_no      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_NF_status          IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                          I_from_receipt_date  IN     FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                          I_to_receipt_date    IN     FM_FISCAL_DOC_HEADER.ENTRY_OR_EXIT_DATE%TYPE,
                          I_from_issue_date    IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                          I_to_issue_date      IN     FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE,
                          I_discrepancy_status IN     VARCHAR2,
                          I_cost_disc          IN     FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE,
                          I_qty_disc           IN     FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE,
                          I_tax_disc           IN     FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: UPDATE_FISCAL_DOC_DETAIL
-- Purpose:       This function updates the FM_FISCAL_DOC_DETAIL table
----------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                  I_disc_type          IN FM_RESOLUTION.DISCREP_TYPE%TYPE,
                                  I_discrep_status     IN FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE,
                                  I_appt_qty           IN FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: UPDATE_FISCAL_HEADER
-- Purpose:       This function updates the FM_FISCAL_DOC_HEADER table
----------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DOC_HEADER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_fiscal_doc_id      IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                  I_tax_code           IN FM_RESOLUTION.TAX_CODE%TYPE,
                                  I_disc_type          IN FM_RESOLUTION.DISCREP_TYPE%TYPE,
                                  I_disc_status        IN FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                  I_discrep_status     IN FM_FISCAL_DOC_HEADER.STATUS%TYPE)
   return BOOLEAN;


----------------------------------------------------------------------------------
-- Function Name: UPDATE_FISCAL_HEADER
-- Purpose:       This function updates the FM_FISCAL_DOC_HEADER table
----------------------------------------------------------------------------------
FUNCTION CHECK_UPDATE_NF_STATUS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                I_current_nf_status    IN FM_FISCAL_DOC_HEADER.STATUS%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: EXISTS_FISCAL_TAX_DETAIL
-- Purpose:       This function checks if exists in the FM_FISCAL_DOC_TAX_DETAIL table
----------------------------------------------------------------------------------
FUNCTION EXISTS_FISCAL_TAX_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists             IN OUT BOOLEAN,
                                  I_fiscal_doc_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_ITEM_DETAILS
-- Purpose:       This function retrieves the Item and Item Description
----------------------------------------------------------------------------------
FUNCTION GET_ITEM_DETAILS(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_FISCAL_DOC_LINE_ID        IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                          O_ITEM                      IN OUT FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                          O_ITEM_DESC                 IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                          O_PO_NUMBER                 IN OUT FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                          O_INCONCLUSIVE_RULES        IN out FM_FISCAL_DOC_DETAIL.INCONCLUSIVE_RULES%TYPE,
                          O_PACK_NO                   IN OUT FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CHECK_RULE
-- Purpose:       This function checks if the resolution rule is set to 'RECONCILE'
----------------------------------------------------------------------------------
FUNCTION CHECK_RULE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists            IN OUT BOOLEAN,
                    I_CODE_TYPE         IN     CODE_DETAIL.CODE_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for detail blocks
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_where_clause       IN OUT VARCHAR2,
                                 I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                 I_PO_Number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                 I_discrepancy_status IN     FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE,
                                 I_discrep_type       IN     FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                 I_tax_type           IN     VARCHAR2,
                                 I_mode IN NUMBER)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_FM_EXT_TAX_APPOR_VALUES
-- Purpose:       This function retrieves the External System Tax detail info for system and NF value.
----------------------------------------------------------------------------------
FUNCTION GET_FM_EXT_TAX_APPOR_VALUES(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_FISCAL_DOC_ID             IN      FM_FISCAL_DOC_TAX_HEAD_EXT.FISCAL_DOC_ID%TYPE,
                                     I_TAX_CODE                  IN      FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE,
                                     I_PO_NUMBER                 IN      FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                     O_TAX_BASIS                 IN OUT  FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_BASIS%TYPE,
                                     O_TAX_VALUE                 IN OUT  FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_VALUE%TYPE,
                                     O_INF_TAX_BASIS             IN OUT  FM_FISCAL_DOC_TAX_HEAD.TAX_BASIS%TYPE,
                                     O_INF_TAX_VALUE             IN OUT  FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE,
                                     O_TAX_DESC                  IN OUT  VAT_CODES.VAT_CODE_DESC%TYPE,
                                     O_MOD_SYS_TAX_BASIS         IN OUT  FM_FISCAL_DOC_TAX_DETAIL_EXT.MODIFIED_TAX_BASIS%TYPE,
                                     O_MOD_TAX_BASIS             IN OUT  FM_FISCAL_DOC_TAX_DETAIL.MODIFIED_TAX_BASIS%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_FM_EXT_TAX_INFOR_VALUES
-- Purpose:       This function retrieves the External System Tax Header info
----------------------------------------------------------------------------------
FUNCTION GET_FM_EXT_TAX_INFOR_VALUES(O_error_message              IN OUT  VARCHAR2,
                                     I_FISCAL_DOC_ID             IN      FM_FISCAL_DOC_TAX_HEAD_EXT.FISCAL_DOC_ID%TYPE,
                                     I_TAX_CODE                  IN      FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE,
                                     O_TAX_BASIS                 IN OUT  FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_BASIS%TYPE,
                                     O_TAX_VALUE                 IN OUT  FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_VALUE%TYPE,
                                     O_INF_TAX_BASIS             IN OUT  FM_FISCAL_DOC_TAX_HEAD.TAX_BASIS%TYPE,
                                     O_INF_TAX_VALUE             IN OUT  FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE,
                                     O_TAX_DESC                  IN OUT  VAT_CODES.VAT_CODE_DESC%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_FM_EXT_TAX_DETAIL
-- Purpose:       This function retrieves the External system Tax detail info
----------------------------------------------------------------------------------
FUNCTION GET_FM_EXT_TAX_DETAIL (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_FISCAL_DOC_ID          IN      FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_ID%TYPE,
                                I_FISCAL_DOC_LINE_ID     IN      FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                                I_TAX_CODE               IN      FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE,
                                O_TAX_DET_REC            IN OUT  FM_FISCAL_DOC_TAX_DETAIL_EXT%ROWTYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: FM_RES_ALL_DISC
-- Purpose:       This function updates all the discrepancies in one single go
----------------------------------------------------------------------------------
FUNCTION FM_RES_ALL_DISC(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_FISCAL_DOC_ID           IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_FISCAL_DOC_LINE_ID      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                         I_NF_STATUS               IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE,
                         I_PO_NUMBER               IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                         I_ITEM                    IN     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                         I_COST_RESOLUTION_RULE    IN     FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                         I_QTY_RESOLUTION_RULE     IN     FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                         I_TAX_RESOLUTION_RULE     IN     FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                         I_CENTRALIZED             IN     VARCHAR2)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: GET_FM_LOC_OPR
-- Purpose:       This function gets the operation type for a location - central or decentral
----------------------------------------------------------------------------------
FUNCTION GET_FM_LOC_OPR(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_Location       IN FM_FISCAL_DOC_HEADER.LOCATION_ID%TYPE,
                        I_Location_type  IN FM_FISCAL_DOC_HEADER.LOCATION_TYPE%TYPE,
                        O_Match_opr_type IN OUT V_FISCAL_ATTRIBUTES.MATCH_OPR_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_FM_TAX_DETAIL
-- Purpose:       This function gets the Tax detail information
----------------------------------------------------------------------------------

FUNCTION GET_FM_TAX_DETAIL (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_TAX_CODE              IN     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE,
                            I_FISCAL_DOC_LINE_ID    IN     FM_FISCAL_DOC_TAX_DETAIL_EXT.FISCAL_DOC_LINE_ID%TYPE,
                            O_Item                  OUT     FM_FISCAL_DOC_DETAIL.ITEM%TYPE,
                            O_Tax_code              OUT     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_CODE%TYPE,
                            O_Tax_Basis             OUT     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_BASIS%TYPE,
                            O_Tax_Rate              OUT     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_RATE%TYPE,
                            O_Tax_Value             OUT     FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: EXISTS_FISCAL_DOC_TAX_DETAIL
-- Purpose:       This function checks if tax detail exists in the EXISTS_FISCAL_DOC_TAX_DETAIL table
----------------------------------------------------------------------------------
FUNCTION EXISTS_FISCAL_DOC_TAX_DETAIL(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists              IN OUT BOOLEAN,
                                      I_vat_code            IN FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE,
                                      I_fiscal_doc_id       IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE_APPOR_INFOR
-- Purpose:       This function returns the query for Apportioned/Informed block
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_APPOR_INFOR (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_where_clause       IN OUT VARCHAR2,
                                       I_fiscal_doc_id      IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE,
                                       I_tax_code           IN     FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE,
                                       I_po_number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                       I_discrepancy_status IN     FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                       I_mode               IN     NUMBER)
   return BOOLEAN;
----------------------------------------------------------------------------------
 -- Function Name: UPDATE_FM_RESOLUTION
 -- Purpose:       This function Updates FM_Resolution table for discrepancy flags
 ----------------------------------------------------------------------------------
 FUNCTION UPDATE_FM_RESOLUTION(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                               I_disc_type          IN VARCHAR2,
                               I_vat_code           IN FM_RESOLUTION.TAX_CODE%TYPE,
                               I_resolution_type    IN FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                               I_reason_code        IN FM_RESOLUTION.REASON_CODE%TYPE)
    return BOOLEAN;

 ---------------------------------------------------------------------------------
 -- Function Name: UPDATE_FISCAL_DOC_TAX_DETAIL
 -- Purpose:       This function Updates FM_FISCAL_DOC_TAX_DETAIL table for resolution tax value,
 --                tax_rate and resolution tax base value.based on the value in Apportioned screen of Tax resolution.
 ----------------------------------------------------------------------------------
 FUNCTION UPDATE_FISCAL_DOC_TAX_DETAIL(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_fiscal_doc_line_id  IN FM_FISCAL_DOC_TAX_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                                       I_vat_code            IN FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE,
                                       I_res_base_value      IN FM_FISCAL_DOC_TAX_DETAIL.RES_BASE_VALUE%TYPE,
                                       I_res_tax_value       IN FM_FISCAL_DOC_TAX_DETAIL.RES_TAX_VALUE%TYPE,
                                       I_res_tax_rate        IN FM_FISCAL_DOC_TAX_DETAIL.RES_TAX_RATE%TYPE,
                                       I_res_mod_base_value  IN FM_FISCAL_DOC_TAX_DETAIL.APPR_MODIFIED_BASE_VALUE%TYPE)
    return BOOLEAN;
/* As per discussion on 17/Mar no need to udpate the schedule stats
----------------------------------------------------------------------------------
-- Function Name: CHECK_UPDATE_SCHEDULE_STATUS
-- Purpose:       This function Updates FM_SCHEDULE Status column if all the NF is resolved.
----------------------------------------------------------------------------------
FUNCTION CHECK_UPDATE_SCHEDULE_STATUS(O_error_message         IN OUT VARCHAR2,
                                      I_schedule_no           IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
   return BOOLEAN;
*/
----------------------------------------------------------------------------------
-- Function Name: UPDATE_NF_STATUS_QTY_CENT
-- Purpose:       This function Updates NF Status to validated if all the qty discrepancy is resolved in Centralised location.
----------------------------------------------------------------------------------
FUNCTION UPDATE_NF_STATUS_QTY_CENT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_fiscal_doc_id   IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE_TAX_DETAIL
-- Purpose:       This function returns the query for Only Tax Header Screen
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_TAX_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_where_clause       IN OUT VARCHAR2,
                                     I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                     I_PO_Number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                     I_discrepancy_status IN     FM_FISCAL_DOC_HEADER.TAX_DISCREP_STATUS%TYPE,
                                     I_discrep_type       IN     FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                     I_tax_type           IN     VARCHAR2,
                                     I_mode               IN NUMBER)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_FISCAL_DTAIL_FOR_TRIANG
-- Purpose:       This function updates the fm_fiscal_doc_detail table for discrepancy status
--                for triangulation.
----------------------------------------------------------------------------------
FUNCTION UPDATE_FISCAL_DTAIL_FOR_TRIANG(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                       ,I_discrepancy          IN VARCHAR2
                                       ,I_compl_fiscal_doc_id  IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE
                                       ,I_po_number            IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                                       ,I_item                 IN FM_FISCAL_DOC_DETAIL.ITEM%TYPE
                                       ,I_appt_qty             IN FM_FISCAL_DOC_DETAIL.APPT_QTY%TYPE
                                 )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_COMPL_OR_MAIN_NF
-- Purpose:       This function identifies whether the given fiscal_doc_id is Complimentory or Main NF
----------------------------------------------------------------------------------
 FUNCTION CHECK_COMPL_OR_MAIN_NF(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_FISCAL_DOC_ID    IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                 O_COMPL_NF         OUT     VARCHAR2,
                                 O_MAIN_NF          OUT     VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_MAIN_NF_FOR_COMPL_NF
-- Purpose:       This function gets the Main NF for complementory NF
----------------------------------------------------------------------------------
 FUNCTION GET_MAIN_NF_FOR_COMPL_NF(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_FISCAL_DOC_ID       IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                   O_MAIN_FISCAL_DOC_ID  OUT     FM_FISCAL_DOC_COMPLEMENT.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CHECK_UPDATE_COMP_NF_STATUS
-- Purpose:       This function Updates FM_FISCAL_DOC_HEADER NF Status for complementory NF
----------------------------------------------------------------------------------
FUNCTION CHECK_UPDATE_COMP_NF_STATUS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_fiscal_doc_id         IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                     I_current_nf_status     IN FM_FISCAL_DOC_HEADER.STATUS%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE_INCON_RULE
-- Purpose:       This function returns the query for Only for Inconclusive Tax rule screen
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE_INCON_RULE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_where_clause       IN OUT VARCHAR2,
                                     I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_RESOLUTION_TYPE
-- Purpose:       This function returns the Resolution Type for the line Items which are already resolved.
--                This is to check all the Pack Items should have the Same resolution type for Qty Disc resolution.
----------------------------------------------------------------------------------
FUNCTION GET_RESOLUTION_TYPE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_resolution_type    IN OUT VARCHAR2,
                             I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                             I_pack_no            IN     FM_FISCAL_DOC_DETAIL.PACK_NO%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_DISCREP_STATUS
-- Purpose:       This function returns the discrepancy status to the view based on the parameters passed.
----------------------------------------------------------------------------------
FUNCTION GET_DISCREP_STATUS(I_discrep_type       IN     VARCHAR2,
                            I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                            I_po_number          IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE)
   return VARCHAR2;
--
-----------------------------------------------------------------------------------------------------------------
FUNCTION GET_HEADER_DISC_STATUS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cost_disc_status          IN OUT FM_FISCAL_DOC_DETAIL.COST_DISCREP_STATUS%TYPE,
                                O_qty_disc_status           IN OUT FM_FISCAL_DOC_DETAIL.QTY_DISCREP_STATUS%TYPE,
                                O_tax_disc_status           IN OUT FM_FISCAL_DOC_DETAIL.TAX_DISCREP_STATUS%TYPE,
                                I_schedule_no               IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                                I_po_number                 IN     FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE,
                                I_fiscal_doc_id             IN     FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE)
return BOOLEAN ;
---
---------------------------------------------------------------------------------------------------
 FUNCTION GET_FM_RESOLUTION(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            I_disc_type          IN VARCHAR2,
                            I_vat_code           IN FM_RESOLUTION.TAX_CODE%TYPE,
                            O_resolution_type    IN OUT FM_RESOLUTION.RESOLUTION_TYPE%TYPE,
                            O_reason_code        IN OUT FM_RESOLUTION.REASON_CODE%TYPE)
    return BOOLEAN ;
---
----------------------------------------------------------------------------------------------------
-- Function Name: EXIST_DISCREP
-- Purpose:       This function checks the record in discrepancy or not.
----------------------------------------------------------------------------------
FUNCTION EXIST_DISCREP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                       I_exists             IN OUT    BOOLEAN)
   return BOOLEAN ;
--
--------------------------------------------------------------------------------------------------------
END FM_DIS_RESOLUTION_SQL;
/