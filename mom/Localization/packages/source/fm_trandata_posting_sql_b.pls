CREATE OR REPLACE PACKAGE BODY FM_TRANDATA_POSTING_SQL AS

L_code_desc           CODE_DETAIL.CODE_DESC%TYPE := NULL;
----------------------------------------------------------------------------------
-- Function Name: PROCESS_TRAN_DATA
-- Purpose:       This function Process Tran Data Posting
----------------------------------------------------------------------------------
FUNCTION PROCESS_TRAN_DATA(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id    IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.PROCESS_TRAN_DATA';
   L_key                 VARCHAR2(80) := 'fdh.status = A';
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   L_triangulation       VARCHAR2(1);
   L_compl_fiscal_doc_id FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE;
   L_ebc_cost            FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
   L_corc_count          NUMBER := 0;
   L_calc_variance       FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE;
   L_comp_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_action_type         FM_CORRECTION_DOC.ACTION_TYPE%TYPE;
   L_rural_prod_ind             V_BR_SUPS.RURAL_PROD_IND%TYPE;
   L_requisition_type           FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_utilization_id             FM_UTILIZATION_ATTRIBUTES.UTILIZATION_ID%TYPE;
   L_utilization_attributes     FM_UTILIZATION_ATTRIBUTES%ROWTYPE;
   L_correction_exists       BOOLEAN := FALSE;

   cursor C_GET_TRAN_DATA IS
      SELECT fdh.fiscal_doc_id
            ,fdh.fiscal_doc_no
            ,fdh.key_value_1 supplier
            ,fdd.item
            ,fdh.location_type
            ,fdh.location_id
            ,fdd.requisition_no
            ,fdd.unit_cost_with_disc unit_cost
            ,fdd.fiscal_doc_line_id
            ,fdh.variance_cost  
            ,nvl(fdd.tolerance_value_cost,0) tolerance_value_cost
            ,nvl(fdd.tolerance_value_qty,0) tolerance_value_qty
            ,fua.utilization_id
            ,fdh.requisition_type
            ,im.dept
            ,im.class
            ,im.subclass
            ,fdd.quantity
            ,fdd.freight_cost
            ,fdd.insurance_cost
            ,fdd.other_expenses_cost
            ,nvl(vfa.rural_prod_ind ,'N') rural_prod_ind
        FROM fm_fiscal_doc_header fdh
            ,fm_fiscal_doc_detail fdd
            ,fm_utilization_attributes fua
            ,Item_master im
            ,v_fiscal_attributes vfa
       WHERE fdh.fiscal_doc_id   = fdd.fiscal_doc_id
         AND fdd.item            = im.item
         AND fdh.utilization_id  = fua.utilization_id
         AND fdh.status          = 'A'
         AND fua.comp_nf_ind     = 'N'
         AND ((fdh.module  in ('SUPP','PTNR') AND fdh.module = vfa.module AND fdh.key_value_1 = vfa.key_value_1 AND fdh.key_value_2 = vfa.key_value_2 )
              or (fdh.module='LOC' AND fdh.module = vfa.module AND fdh.location_id = vfa.key_value_1 AND fdh.location_type = vfa.key_value_2 ))
         AND fdh.fiscal_doc_id   = I_fiscal_doc_id;
--
   cursor C_GET_TAX_DETAIL(I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
     select fdtd.vat_code vat_code
            ,fdtd.fiscal_doc_line_id
            ,nvl(fdtd.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail fdtd,
            fm_tax_codes ftc
      where fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id
        and ftc.tax_code = fdtd.vat_code
        and ftc.matching_ind = 'Y'
      UNION ALL
      select fdtde.tax_code vat_code
            ,fdtde.fiscal_doc_line_id
            ,nvl(fdtde.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtde.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail_ext fdtde  ,
            fm_tax_codes ftc  
      where (fdtde.fiscal_doc_line_id = I_fiscal_doc_line_id
              or fdtde.fiscal_doc_line_id in (select fiscal_doc_line_id_ref from fm_fiscal_doc_detail where fiscal_doc_line_id = I_fiscal_doc_line_id))
        and ftc.tax_code = fdtde.tax_code
        and ftc.matching_ind = 'N';
--
   cursor C_GET_CORC_COUNT( I_fiscal_doc_id      IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE
                           ,I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
   is
      SELECT count(1)
        FROM fm_correction_doc fcd
       WHERE fcd.fiscal_doc_id      = I_fiscal_doc_id
         AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
         AND fcd.action_type in ('CLC','CLT','CLQ');
         
 cursor C_GET_CORC_COUNT_TRAN( I_fiscal_doc_id      IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE
                            ,I_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                            I_compl_fiscal_doc_id      IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE
                            ,I_compl_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE)
    is
       SELECT fcd.action_type
         FROM fm_correction_doc fcd
        WHERE fcd.fiscal_doc_id      = I_fiscal_doc_id
          AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
 AND fcd.action_type in ('CLC','CLT','CLQ')
 UNION
       SELECT fcd.action_type
         FROM fm_correction_doc fcd
        WHERE fcd.fiscal_doc_id      = I_compl_fiscal_doc_id
          AND fcd.fiscal_doc_line_id = I_compl_fiscal_doc_line_id
 AND fcd.action_type in ('CLC','CLT','CLQ');
--
   cursor C_FM_FISCAL_DOC_HEADER is
      SELECT requisition_type
            ,utilization_id
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.fiscal_doc_id = I_fiscal_doc_id
         AND fdh.status = 'A';
         
 cursor C_GET_TRAN_DATA_TRAN(I_comp_fiscal_doc_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_ID%TYPE
                             ,I_requisition_no IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                             ,I_item IN FM_FISCAL_DOC_DETAIL.ITEM%TYPE) is
       SELECT fdd.fiscal_doc_line_id
         FROM fm_fiscal_doc_detail fdd
        WHERE fdd.fiscal_doc_id  = I_comp_fiscal_doc_id
          AND fdd.requisition_no = I_requisition_no
          AND fdd.item           = I_item;

--

BEGIN



   SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_DATA', 'fm_fiscal_doc_header', L_key);
   FOR L_get_tran_data IN C_GET_TRAN_DATA
   LOOP

      L_calc_variance    := L_get_tran_data.variance_cost;
      L_rural_prod_ind   := L_get_tran_data.rural_prod_ind;
      L_requisition_type := L_get_tran_data.requisition_type;
    
      if FM_DISCREP_IDENTIFICATION_SQL.CHECK_FOR_TRIANGULATION(O_error_message       => O_error_message
                                                              ,I_fiscal_doc_id       => L_get_tran_data.fiscal_doc_id
                                                              ,O_triangulation       => L_triangulation
                                                              ,O_compl_fiscal_doc_id => L_compl_fiscal_doc_id ) = FALSE then
         return FALSE;
      end if;

      if (L_get_tran_data.requisition_type = 'PO' AND L_get_tran_data.rural_prod_ind = 'N') OR
         L_get_tran_data.requisition_type = 'RPO'
      then

         if (L_get_tran_data.tolerance_value_cost IS NULL or L_get_tran_data.tolerance_value_cost = 0)
            AND (L_get_tran_data.tolerance_value_qty IS NULL or L_get_tran_data.tolerance_value_qty = 0) then
          
            if L_triangulation = 'N' then
               if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message,
                                                   O_base_cost          => L_base_cost,
                                                   I_nic_cost           => L_get_tran_data.unit_cost,
                                                   I_nic_cost_factor    => L_get_tran_data.unit_cost,
                                                   I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id) = FALSE then
                  return FALSE;
               end if;

               if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 100 
                                                        ,I_fiscal_doc_id      => L_get_tran_data.fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id
                                                        ,I_vat_code           => NULL
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;

               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 100 
                                                          ,I_fiscal_doc_id => L_get_tran_data.fiscal_doc_id
                                                          ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                          ,I_supplier      => L_get_tran_data.supplier
                                                          ,I_dept          => L_get_tran_data.dept
                                                          ,I_class         => L_get_tran_data.class
                                                          ,I_subclass      => L_get_tran_data.subclass
                                                          ,I_item          => L_get_tran_data.item
                                                          ,I_location_type => L_get_tran_data.location_type
                                                          ,I_location      => L_get_tran_data.location_id
                                                          ,I_total_cost    => (L_base_cost * L_get_tran_data.quantity)
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
--
               SQL_LIB.SET_MARK('OPEN','C_GET_TAX_DETAIL', 'FM_FISCAL_DOC_TAX_DETAIL', L_key);
               FOR L_get_tax_detail IN C_GET_TAX_DETAIL(L_get_tran_data.fiscal_doc_line_id)
               LOOP
                  if L_get_tax_detail.unit_rec_value = 0 then
                     if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                              ,I_tran_code          => 101 
                                                              ,I_fiscal_doc_id      => L_get_tran_data.fiscal_doc_id
                                                              ,I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id
                                                              ,I_vat_code           => L_get_tax_detail.vat_code
                                                              ,O_ref_no_1           => L_ref_no_1
                                                              ,O_ref_no_2           => L_ref_no_2
                                                              ,O_ref_no_3           => L_ref_no_3
                                                              ,O_ref_no_4           => L_ref_no_4
                                                              ,O_ref_no_5           => L_ref_no_5) = FALSE then
                         return FALSE;
                      end if;
                   
                      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                                 ,I_tran_code     => 101 
                                                                 ,I_fiscal_doc_id => L_get_tran_data.fiscal_doc_id
                                                                 ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                                 ,I_supplier      => L_get_tran_data.supplier
                                                                 ,I_dept          => L_get_tran_data.dept
                                                                 ,I_class         => L_get_tran_data.class
                                                                 ,I_subclass      => L_get_tran_data.subclass
                                                                 ,I_item          => L_get_tran_data.item
                                                                 ,I_location_type => L_get_tran_data.location_type
                                                                 ,I_location      => L_get_tran_data.location_id
                                                                 ,I_total_cost    => (L_get_tax_detail.unit_tax_amt *  L_get_tran_data.quantity)
                                                                 ,I_ref_no_1      => L_ref_no_1
                                                                 ,I_ref_no_2      => L_ref_no_2
                                                                 ,I_ref_no_3      => L_ref_no_3
                                                                 ,I_ref_no_4      => L_ref_no_4
                                                                 ,I_ref_no_5      => L_ref_no_5
                                                                 ,I_tran_date     => SYSDATE)  = FALSE then
                         return FALSE;
                      end if;
 --
                  elsif L_get_tax_detail.unit_rec_value > 0 and (L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) = 0 then
                        if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                                ,I_tran_code          => 102 
                                                                ,I_fiscal_doc_id      => L_get_tran_data.fiscal_doc_id
                                                                ,I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id
                                                                ,I_vat_code           => L_get_tax_detail.vat_code
                                                                ,O_ref_no_1           => L_ref_no_1
                                                                ,O_ref_no_2           => L_ref_no_2
                                                                ,O_ref_no_3           => L_ref_no_3
                                                                ,O_ref_no_4           => L_ref_no_4
                                                                ,O_ref_no_5           => L_ref_no_5) = FALSE then
                          return FALSE;
                       end if;
   --
                       if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                                  ,I_tran_code     => 102 
                                                                  ,I_fiscal_doc_id => L_get_tran_data.fiscal_doc_id
                                                                  ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                                  ,I_supplier      => L_get_tran_data.supplier
                                                                  ,I_dept          => L_get_tran_data.dept
                                                                  ,I_class         => L_get_tran_data.class
                                                                  ,I_subclass      => L_get_tran_data.subclass
                                                                  ,I_item          => L_get_tran_data.item
                                                                  ,I_location_type => L_get_tran_data.location_type
                                                                  ,I_location      => L_get_tran_data.location_id
                                                                  ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  L_get_tran_data.quantity)
                                                                  ,I_ref_no_1      => L_ref_no_1
                                                                  ,I_ref_no_2      => L_ref_no_2
                                                                  ,I_ref_no_3      => L_ref_no_3
                                                                  ,I_ref_no_4      => L_ref_no_4
                                                                  ,I_ref_no_5      => L_ref_no_5
                                                                  ,I_tran_date     => SYSDATE)  = FALSE then
                          return FALSE;
                       end if;
                  else 
                      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                               ,I_tran_code          => 101 
                                                               ,I_fiscal_doc_id      => L_get_tran_data.fiscal_doc_id
                                                               ,I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id
                                                               ,I_vat_code           => L_get_tax_detail.vat_code
                                                               ,O_ref_no_1           => L_ref_no_1
                                                               ,O_ref_no_2           => L_ref_no_2
                                                               ,O_ref_no_3           => L_ref_no_3
                                                               ,O_ref_no_4           => L_ref_no_4
                                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
                         return FALSE;
                      end if;
   --
                      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                                 ,I_tran_code     => 101 
                                                                 ,I_fiscal_doc_id => L_get_tran_data.fiscal_doc_id
                                                                 ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                                 ,I_supplier      => L_get_tran_data.supplier
                                                                 ,I_dept          => L_get_tran_data.dept
                                                                 ,I_class         => L_get_tran_data.class
                                                                 ,I_subclass      => L_get_tran_data.subclass
                                                                 ,I_item          => L_get_tran_data.item
                                                                 ,I_location_type => L_get_tran_data.location_type
                                                                 ,I_location      => L_get_tran_data.location_id
                                                                 ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) *  L_get_tran_data.quantity)
                                                                 ,I_ref_no_1      => L_ref_no_1
                                                                 ,I_ref_no_2      => L_ref_no_2
                                                                 ,I_ref_no_3      => L_ref_no_3
                                                                 ,I_ref_no_4      => L_ref_no_4
                                                                 ,I_ref_no_5      => L_ref_no_5
                                                                 ,I_tran_date     => SYSDATE)  = FALSE then
                         return FALSE;
                      end if;
                      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                               ,I_tran_code          => 102
                                                               ,I_fiscal_doc_id      => L_get_tran_data.fiscal_doc_id
                                                               ,I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id
                                                               ,I_vat_code           => L_get_tax_detail.vat_code
                                                               ,O_ref_no_1           => L_ref_no_1
                                                               ,O_ref_no_2           => L_ref_no_2
                                                               ,O_ref_no_3           => L_ref_no_3
                                                               ,O_ref_no_4           => L_ref_no_4
                                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
                         return FALSE;
                      end if;
   --
                      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                                 ,I_tran_code     => 102 
                                                                 ,I_fiscal_doc_id => L_get_tran_data.fiscal_doc_id
                                                                 ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                                 ,I_supplier      => L_get_tran_data.supplier
                                                                 ,I_dept          => L_get_tran_data.dept
                                                                 ,I_class         => L_get_tran_data.class
                                                                 ,I_subclass      => L_get_tran_data.subclass
                                                                 ,I_item          => L_get_tran_data.item
                                                                 ,I_location_type => L_get_tran_data.location_type
                                                                 ,I_location      => L_get_tran_data.location_id
                                                                 ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  L_get_tran_data.quantity)
                                                                 ,I_ref_no_1      => L_ref_no_1
                                                                 ,I_ref_no_2      => L_ref_no_2
                                                                 ,I_ref_no_3      => L_ref_no_3
                                                                 ,I_ref_no_4      => L_ref_no_4
                                                                 ,I_ref_no_5      => L_ref_no_5
                                                                 ,I_tran_date     => SYSDATE)  = FALSE then
                          return FALSE;
                      end if;
                  end if;
               END LOOP;
            else   
               if FM_TRANDATA_POSTING_SQL.PROCESS_TRAN_DATA_TRAN( O_error_message        => O_error_message
                                                                 ,I_fiscal_doc_id        => L_get_tran_data.fiscal_doc_id
                                                                 ,I_fiscal_doc_no        => L_get_tran_data.fiscal_doc_no
                                                                 ,I_supplier             => L_get_tran_data.supplier
                                                                 ,I_item                 => L_get_tran_data.item
                                                                 ,I_location_type        => L_get_tran_data.location_type
                                                                 ,I_location_id          => L_get_tran_data.location_id
                                                                 ,I_unit_cost            => L_get_tran_data.unit_cost
                                                                 ,I_fiscal_doc_line_id   => L_get_tran_data.fiscal_doc_line_id
                                                                 ,I_variance_cost        => L_get_tran_data.variance_cost
                                                                 ,I_tolerance_value_cost => L_get_tran_data.tolerance_value_cost
                                                                 ,I_tolerance_value_qty  => L_get_tran_data.tolerance_value_qty
                                                                 ,I_utilization_id       => L_get_tran_data.utilization_id
                                                                 ,I_requisition_type     => L_get_tran_data.requisition_type
                                                                 ,I_dept                 => L_get_tran_data.dept
                                                                 ,I_class                => L_get_tran_data.class
                                                                 ,I_subclass             => L_get_tran_data.subclass
                                                                 ,I_quantity             => L_get_tran_data.quantity
                                                                 ,I_freight_cost         => L_get_tran_data.freight_cost
                                                                 ,I_insurance_cost       => L_get_tran_data.insurance_cost
                                                                 ,I_other_expenses_cost  => L_get_tran_data.other_expenses_cost
                                                                 ,I_requisition_no       => L_get_tran_data.requisition_no
                                                                 ,I_comp_fiscal_doc_id   => L_compl_fiscal_doc_id) = FALSE then
                  return FALSE;
               end if; 
            end if; 
            if (L_get_tran_data.freight_cost is not null and L_get_tran_data.freight_cost > 0) then
               if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 103 
                                                        ,I_fiscal_doc_id      => L_get_tran_data.fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id
                                                        ,I_vat_code           => NULL
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
--
               if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                             'FMCC',
                                             'F',
                                             L_code_desc) = FALSE then
                  return FALSE;
               end if;   

               if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
                  L_ref_no_1 := L_code_desc;
               elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
                  L_ref_no_2 := L_code_desc;
               elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
                  L_ref_no_3 := L_code_desc;
               elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
                  L_ref_no_4 := L_code_desc;
               elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
                  L_ref_no_5 := L_code_desc;
               end if; 
--
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 103 
                                                          ,I_fiscal_doc_id => L_get_tran_data.fiscal_doc_id
                                                          ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                          ,I_supplier      => L_get_tran_data.supplier
                                                          ,I_dept          => L_get_tran_data.dept
                                                          ,I_class         => L_get_tran_data.class
                                                          ,I_subclass      => L_get_tran_data.subclass
                                                          ,I_item          => L_get_tran_data.item
                                                          ,I_location_type => L_get_tran_data.location_type
                                                          ,I_location      => L_get_tran_data.location_id
                                                          ,I_total_cost    => (L_get_tran_data.freight_cost * L_get_tran_data.quantity)
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
            end if; 
--
            if (L_get_tran_data.insurance_cost is not null and L_get_tran_data.insurance_cost > 0 )then
               if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 103 
                                                        ,I_fiscal_doc_id      => L_get_tran_data.fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id
                                                        ,I_vat_code           => NULL
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                 return FALSE;
               end if;
--
               if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                             'FMCC',
                                             'I',
                                             L_code_desc) = FALSE then
                  return FALSE;
               end if;
               
               if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
                  L_ref_no_1 := L_code_desc;
               elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
                  L_ref_no_2 := L_code_desc;
               elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
                  L_ref_no_3 := L_code_desc;
               elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
                  L_ref_no_4 := L_code_desc;
               elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
                  L_ref_no_5 := L_code_desc;
               end if;  
--
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 103 
                                                          ,I_fiscal_doc_id => L_get_tran_data.fiscal_doc_id
                                                          ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                          ,I_supplier      => L_get_tran_data.supplier
                                                          ,I_dept          => L_get_tran_data.dept
                                                          ,I_class         => L_get_tran_data.class
                                                          ,I_subclass      => L_get_tran_data.subclass
                                                          ,I_item          => L_get_tran_data.item
                                                          ,I_location_type => L_get_tran_data.location_type
                                                          ,I_location      => L_get_tran_data.location_id
                                                          ,I_total_cost    => (L_get_tran_data.insurance_cost * L_get_tran_data.quantity)
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                 return FALSE;
               end if;
            end if; 
--
            if (L_get_tran_data.other_expenses_cost is not null and L_get_tran_data.other_expenses_cost > 0) then
               if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                        ,I_tran_code          => 103 
                                                        ,I_fiscal_doc_id      => L_get_tran_data.fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => L_get_tran_data.fiscal_doc_line_id
                                                        ,I_vat_code           => NULL
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
--
               if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                             'FMCC',
                                             'O',
                                             L_code_desc) = FALSE then
                  return FALSE;
               end if;   

               if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
                  L_ref_no_1 := L_code_desc;
               elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
                  L_ref_no_2 := L_code_desc;
               elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
                  L_ref_no_3 := L_code_desc;
               elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
                  L_ref_no_4 := L_code_desc;
               elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
                  L_ref_no_5 := L_code_desc;
               end if; 
--
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 103 
                                                          ,I_fiscal_doc_id => L_get_tran_data.fiscal_doc_id
                                                          ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                          ,I_supplier      => L_get_tran_data.supplier
                                                          ,I_dept          => L_get_tran_data.dept
                                                          ,I_class         => L_get_tran_data.class
                                                          ,I_subclass      => L_get_tran_data.subclass
                                                          ,I_item          => L_get_tran_data.item
                                                          ,I_location_type => L_get_tran_data.location_type
                                                          ,I_location      => L_get_tran_data.location_id
                                                          ,I_total_cost    => (L_get_tran_data.other_expenses_cost * L_get_tran_data.quantity)
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
            end if; 
   --
         else    
            if FM_TRANDATA_POSTING_SQL.PO_WITH_VARIANCE( O_error_message           => O_error_message
                                                        ,I_fiscal_doc_id           => L_get_tran_data.fiscal_doc_id
                                                        ,I_fiscal_doc_no           => L_get_tran_data.fiscal_doc_no
                                                        ,I_supplier                => L_get_tran_data.supplier
                                                        ,I_item                    => L_get_tran_data.item
                                                        ,I_location_type           => L_get_tran_data.location_type
                                                        ,I_location_id             => L_get_tran_data.location_id
                                                        ,I_unit_cost               => L_get_tran_data.unit_cost  
                                                        ,I_fiscal_doc_line_id      => L_get_tran_data.fiscal_doc_line_id
                                                        ,I_variance_cost           => L_get_tran_data.variance_cost
                                                        ,I_tolerance_value_cost    => L_get_tran_data.tolerance_value_cost
                                                        ,I_tolerance_value_qty     => L_get_tran_data.tolerance_value_qty
                                                        ,I_utilization_id          => L_get_tran_data.utilization_id
                                                        ,I_requisition_type        => L_get_tran_data.requisition_type
                                                        ,I_dept                    => L_get_tran_data.dept
                                                        ,I_class                   => L_get_tran_data.class
                                                        ,I_subclass                => L_get_tran_data.subclass
                                                        ,I_quantity                => L_get_tran_data.quantity
                                                        ,I_freight_cost            => L_get_tran_data.freight_cost
                                                        ,I_insurance_cost          => L_get_tran_data.insurance_cost
                                                        ,I_other_expenses_cost     => L_get_tran_data.other_expenses_cost
                                                        ,I_triangulation_ind       => L_triangulation
                                                        ,I_requisition_no          => L_get_tran_data.requisition_no
                                                        ,I_comp_fiscal_doc_id      => L_compl_fiscal_doc_id) = FALSE then
               return FALSE;
            end if;
   --
         end if; 
        if L_triangulation = 'N' then
            SQL_LIB.SET_MARK('OPEN','C_GET_CORC_COUNT', 'FM_CORRECTION_DOC', L_key);
            OPEN C_GET_CORC_COUNT(L_get_tran_data.fiscal_doc_id, L_get_tran_data.fiscal_doc_line_id);
            SQL_LIB.SET_MARK('FETCH','C_GET_CORC_COUNT', 'FM_CORRECTION_DOC', L_key);
            FETCH C_GET_CORC_COUNT INTO L_corc_count;
            SQL_LIB.SET_MARK('CLOSE','C_GET_CORC_COUNT', 'FM_CORRECTION_DOC', L_key);
            CLOSE C_GET_CORC_COUNT;
 --
            if L_corc_count > 0 then
               if FM_TRANDATA_POSTING_SQL.POST_TRANDATA_CORRECTION( O_error_message         => O_error_message
                                                                   ,I_fiscal_doc_id         => L_get_tran_data.fiscal_doc_id
                                                                   ,I_fiscal_doc_no         => L_get_tran_data.fiscal_doc_no
                                                                   ,I_supplier              => L_get_tran_data.supplier
                                                                   ,I_item                  => L_get_tran_data.item
                                                                   ,I_location_type         => L_get_tran_data.location_type
                                                                   ,I_location_id           => L_get_tran_data.location_id
                                                                   ,I_unit_cost             => L_get_tran_data.unit_cost  
                                                                   ,I_fiscal_doc_line_id    => L_get_tran_data.fiscal_doc_line_id
                                                                   ,I_variance_cost         => L_get_tran_data.variance_cost
                                                                   ,I_tolerance_value_cost  => L_get_tran_data.tolerance_value_cost
                                                                   ,I_tolerance_value_qty   => L_get_tran_data.tolerance_value_qty
                                                                   ,I_utilization_id        => L_get_tran_data.utilization_id
                                                                   ,I_requisition_type      => L_get_tran_data.requisition_type
                                                                   ,I_dept                  => L_get_tran_data.dept
                                                                   ,I_class                 => L_get_tran_data.class
                                                                   ,I_subclass              => L_get_tran_data.subclass
                                                                   ,I_quantity              => L_get_tran_data.quantity
                                                                   ,I_freight_cost          => L_get_tran_data.freight_cost
                                                                   ,I_insurance_cost        => L_get_tran_data.insurance_cost
                                                                   ,I_other_expenses_cost   => L_get_tran_data.other_expenses_cost ) = FALSE then
                      return FALSE;
               end if;
            end if; 
--
         else
            OPEN C_GET_TRAN_DATA_TRAN(L_compl_fiscal_doc_id,L_get_tran_data.requisition_no,L_get_tran_data.item);
            FETCH C_GET_TRAN_DATA_TRAN INTO L_comp_fiscal_doc_line_id;
            CLOSE C_GET_TRAN_DATA_TRAN;
             
            SQL_LIB.SET_MARK('OPEN','C_GET_CORC_COUNT_TRAN', 'FM_CORRECTION_DOC', L_key);
            OPEN C_GET_CORC_COUNT_TRAN(L_get_tran_data.fiscal_doc_id, L_get_tran_data.fiscal_doc_line_id,L_compl_fiscal_doc_id,L_comp_fiscal_doc_line_id);
            SQL_LIB.SET_MARK('FETCH','C_GET_CORC_COUNT_TRAN', 'FM_CORRECTION_DOC', L_key);
            FETCH C_GET_CORC_COUNT_TRAN INTO L_action_type;
              if C_GET_CORC_COUNT_TRAN%FOUND then
                 L_correction_exists := TRUE;
              end if;
            SQL_LIB.SET_MARK('CLOSE','C_GET_CORC_COUNT_TRAN', 'FM_CORRECTION_DOC', L_key);
            CLOSE C_GET_CORC_COUNT_TRAN;  
             
            if L_correction_exists then
               if FM_TRANDATA_POSTING_SQL.POST_TRANDATA_CORR_TRAN(O_error_message         => O_error_message
                                                                  ,I_fiscal_doc_id         => L_get_tran_data.fiscal_doc_id
                                                                  ,I_fiscal_doc_no         => L_get_tran_data.fiscal_doc_no
                                                                  ,I_supplier              => L_get_tran_data.supplier
                                                                  ,I_item                  => L_get_tran_data.item
                                                                  ,I_location_type         => L_get_tran_data.location_type
                                                                  ,I_location_id           => L_get_tran_data.location_id
                                                                  ,I_unit_cost             => L_get_tran_data.unit_cost  
                                                                  ,I_fiscal_doc_line_id    => L_get_tran_data.fiscal_doc_line_id
                                                                  ,I_variance_cost         => L_get_tran_data.variance_cost
                                                                  ,I_tolerance_value_cost  => L_get_tran_data.tolerance_value_cost
                                                                  ,I_tolerance_value_qty   => L_get_tran_data.tolerance_value_qty
                                                                  ,I_utilization_id        => L_get_tran_data.utilization_id
                                                                  ,I_requisition_type      => L_get_tran_data.requisition_type
                                                                  ,I_dept                  => L_get_tran_data.dept
                                                                  ,I_class                 => L_get_tran_data.class
                                                                  ,I_subclass              => L_get_tran_data.subclass
                                                                  ,I_quantity              => L_get_tran_data.quantity
                                                                  ,I_freight_cost          => L_get_tran_data.freight_cost
                                                                  ,I_insurance_cost        => L_get_tran_data.insurance_cost
                                                                  ,I_other_expenses_cost   => L_get_tran_data.other_expenses_cost
                                                                  ,I_comp_fiscal_doc_id   => L_compl_fiscal_doc_id
                                                                  ,I_compl_fiscal_doc_line_id   => L_comp_fiscal_doc_line_id) = FALSE then
                    return FALSE;
                 end if;
             end if; 
         end if ; 
      elsif L_get_tran_data.requisition_type = 'RTV' then
--
         if FM_TRANDATA_POSTING_SQL.POST_TRANDATA_RTV( O_error_message         => O_error_message
                                                      ,I_fiscal_doc_id         => L_get_tran_data.fiscal_doc_id
                                                      ,I_fiscal_doc_no         => L_get_tran_data.fiscal_doc_no
                                                      ,I_supplier              => L_get_tran_data.supplier
                                                      ,I_item                  => L_get_tran_data.item
                                                      ,I_location_type         => L_get_tran_data.location_type
                                                      ,I_location_id           => L_get_tran_data.location_id
                                                      ,I_unit_cost             => L_get_tran_data.unit_cost  
                                                      ,I_fiscal_doc_line_id    => L_get_tran_data.fiscal_doc_line_id
                                                      ,I_variance_cost         => L_get_tran_data.variance_cost
                                                      ,I_tolerance_value_cost  => L_get_tran_data.tolerance_value_cost
                                                      ,I_tolerance_value_qty   => L_get_tran_data.tolerance_value_qty
                                                      ,I_utilization_id        => L_get_tran_data.utilization_id
                                                      ,I_requisition_type      => L_get_tran_data.requisition_type
                                                      ,I_dept                  => L_get_tran_data.dept
                                                      ,I_class                 => L_get_tran_data.class
                                                      ,I_subclass              => L_get_tran_data.subclass
                                                      ,I_quantity              => L_get_tran_data.quantity
                                                      ,I_freight_cost          => L_get_tran_data.freight_cost
                                                      ,I_insurance_cost        => L_get_tran_data.insurance_cost
                                                      ,I_other_expenses_cost   => L_get_tran_data.other_expenses_cost
                                                      ,I_triangulation_ind     => L_triangulation
                                                      ,I_requisition_no        => L_get_tran_data.requisition_no
                                                      ,I_comp_fiscal_doc_id    => L_compl_fiscal_doc_id) = FALSE then
                return FALSE;
         end if; 
      elsif L_get_tran_data.requisition_type = 'RNF' then
--
         if FM_TRANDATA_POSTING_SQL.post_trandata_rnf( O_error_message         => O_error_message
                                                      ,I_fiscal_doc_id         => L_get_tran_data.fiscal_doc_id
                                                      ,I_fiscal_doc_no         => L_get_tran_data.fiscal_doc_no
                                                      ,I_supplier              => L_get_tran_data.supplier
                                                      ,I_item                  => L_get_tran_data.item
                                                      ,I_location_type         => L_get_tran_data.location_type
                                                      ,I_location_id           => L_get_tran_data.location_id
                                                      ,I_unit_cost             => L_get_tran_data.unit_cost  
                                                      ,I_fiscal_doc_line_id    => L_get_tran_data.fiscal_doc_line_id
                                                      ,I_variance_cost         => L_get_tran_data.variance_cost
                                                      ,I_tolerance_value_cost  => L_get_tran_data.tolerance_value_cost
                                                      ,I_tolerance_value_qty   => L_get_tran_data.tolerance_value_qty
                                                      ,I_utilization_id        => L_get_tran_data.utilization_id
                                                      ,I_requisition_type      => L_get_tran_data.requisition_type
                                                      ,I_dept                  => L_get_tran_data.dept
                                                      ,I_class                 => L_get_tran_data.class
                                                      ,I_subclass              => L_get_tran_data.subclass
                                                      ,I_quantity              => L_get_tran_data.quantity
                                                      ,I_freight_cost          => L_get_tran_data.freight_cost
                                                      ,I_insurance_cost        => L_get_tran_data.insurance_cost
                                                      ,I_other_expenses_cost   => L_get_tran_data.other_expenses_cost 
                                                      ,I_triangulation_ind     => L_triangulation
                                                      ,I_requisition_no        => L_get_tran_data.requisition_no
                                                      ,I_comp_fiscal_doc_id    => L_compl_fiscal_doc_id) = FALSE then
--
              return FALSE;
           end if; 
      elsif L_get_tran_data.requisition_type in ('TSF','IC') then
         if FM_TRANDATA_POSTING_SQL.POST_TRANDATA_TRANSFERS( O_error_message         => O_error_message
                                                            ,I_fiscal_doc_id         => L_get_tran_data.fiscal_doc_id
                                                            ,I_fiscal_doc_no         => L_get_tran_data.fiscal_doc_no
                                                            ,I_supplier              => NULL
                                                            ,I_item                  => L_get_tran_data.item
                                                            ,I_location_type         => L_get_tran_data.location_type
                                                            ,I_location_id           => L_get_tran_data.location_id
                                                            ,I_unit_cost             => L_get_tran_data.unit_cost  
                                                            ,I_fiscal_doc_line_id    => L_get_tran_data.fiscal_doc_line_id
                                                            ,I_variance_cost         => L_get_tran_data.variance_cost
                                                            ,I_tolerance_value_cost  => L_get_tran_data.tolerance_value_cost
                                                            ,I_tolerance_value_qty   => L_get_tran_data.tolerance_value_qty
                                                            ,I_utilization_id        => L_get_tran_data.utilization_id
                                                            ,I_requisition_type      => L_get_tran_data.requisition_type
                                                            ,I_dept                  => L_get_tran_data.dept
                                                            ,I_class                 => L_get_tran_data.class
                                                            ,I_subclass              => L_get_tran_data.subclass
                                                            ,I_quantity              => L_get_tran_data.quantity
                                                            ,I_freight_cost          => L_get_tran_data.freight_cost
                                                            ,I_insurance_cost        => L_get_tran_data.insurance_cost
                                                            ,I_other_expenses_cost   => L_get_tran_data.other_expenses_cost 
                                                            ,I_triangulation_ind     => L_triangulation
                                                            ,I_requisition_no        => L_get_tran_data.requisition_no
                                                            ,I_comp_fiscal_doc_id    => L_compl_fiscal_doc_id) = FALSE then
              return FALSE;
           end if; 
      end if;

   END LOOP;

   if (L_requisition_type = 'PO' AND L_rural_prod_ind = 'N') OR
            L_requisition_type = 'RPO' then
      if (L_calc_variance is not null or L_calc_variance <> 0) then
         if FM_TRANDATA_POSTING_SQL.POST_TRANDATA_CAL_VARIANCE( O_error_message  => O_error_message
                                                               ,I_fiscal_doc_id  => I_fiscal_doc_id
                                                               ,I_variance       => L_calc_variance) = FALSE then
            return FALSE;
         end if;
      end if; 
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   open C_FM_FISCAL_DOC_HEADER;
   ---
   SQL_LIB.SET_MARK('FECTH', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   fetch C_FM_FISCAL_DOC_HEADER into L_requisition_type, L_utilization_id;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_FM_FISCAL_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', L_key);
   close C_FM_FISCAL_DOC_HEADER;
   ---
   if FM_UTILIZATION_ATTRIBUTES_SQL.GET_UTILIZATION_ATTRIB_INFO(O_error_message,
                                                                L_utilization_attributes,
                                                                L_utilization_id) = FALSE then
      return FALSE;
   end if;
   if (L_requisition_type = 'PO' AND L_utilization_attributes.comp_freight_nf_ind = 'Y') then
      if FM_TRANDATA_POSTING_SQL.POST_TRANDATA_COMPL_FREIGHT( O_error_message  => O_error_message
                                                             ,I_fiscal_doc_id  => I_fiscal_doc_id) = FALSE then
         return FALSE;
      end if; 
   end if;
   if FM_TRANDATA_POSTING_SQL.MOVE_TRAN_STG_BASE( O_error_message  => O_error_message
                                                 ,I_fiscal_doc_id  => I_fiscal_doc_id) = FALSE then
      return FALSE;
   end if;
--
   return TRUE;
--
EXCEPTION
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_GET_TRAN_DATA%ISOPEN then
         close C_GET_TRAN_DATA;
      end if;
      return FALSE;
END PROCESS_TRAN_DATA;
--
-----------------------------------------------------------------------------------
FUNCTION CALC_BC (O_error_message          IN OUT VARCHAR2,
                  O_base_cost              OUT FM_FISCAL_DOC_DETAIL.BASE_COST%TYPE,
                  I_nic_cost               IN  FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                  I_nic_cost_factor        IN  FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE,
                  I_fiscal_doc_line_id     IN  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                )
return BOOLEAN is
--
   L_program                 VARCHAR2(100) := 'FM_TRANDATA_POSTING_SQL.CALC_BC';
   L_base_cost               FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_ebc_cost                FM_STG_RTV_DTL.EBC_COST%TYPE;
   L_bc_tax_amt              FM_FISCAL_DOC_TAX_DETAIL.UNIT_TAX_AMT%TYPE;
   L_total_rec_amt           FM_FISCAL_DOC_TAX_DETAIL.UNIT_REC_VALUE%TYPE :=0;
   L_total_tax_amt           FM_FISCAL_DOC_TAX_DETAIL.UNIT_TAX_AMT%TYPE :=0;
--
   cursor C_GET_TAX_DETAIL is
     select fdtd.vat_code vat_code,
             NVL(fdtd.unit_tax_amt,0) as unit_tax_amt,
             NVL(fdtd.unit_rec_value,0) as unit_rec_value,
             vc.incl_nic_ind as nic_ind
        from fm_fiscal_doc_tax_detail fdtd,
             vat_codes vc,
             fm_tax_codes ftc
       where fdtd.fiscal_doc_line_id = I_fiscal_doc_line_id
         and fdtd.vat_code           = vc.vat_code
         and ftc.tax_code = fdtd.vat_code
         and ftc.matching_ind = 'Y'
      UNION ALL
      select fdtde.tax_code vat_code,
             NVL(fdtde.unit_tax_amt,0) as unit_tax_amt,
             NVL(fdtde.unit_rec_value,0) as unit_rec_value,
             vc.incl_nic_ind as nic_ind
        from fm_fiscal_doc_tax_detail_ext fdtde,
             vat_codes vc,
             fm_tax_codes ftc
       where fdtde.fiscal_doc_line_id = I_fiscal_doc_line_id
         and fdtde.tax_code           = vc.vat_code
         and ftc.tax_code = fdtde.tax_code
         and ftc.matching_ind = 'N'
         UNION ALL
     select fdtde.tax_code vat_code,
             NVL(fdtde.unit_tax_amt,0) as unit_tax_amt,
             NVL(fdtde.unit_rec_value,0) as unit_rec_value,
             vc.incl_nic_ind as nic_ind
        from fm_fiscal_doc_tax_detail_ext fdtde,
             vat_codes vc,
             fm_tax_codes ftc
         where fdtde.fiscal_doc_line_id in (select fdd.fiscal_doc_line_id_ref 
                                            from fm_fiscal_doc_detail fdd,
                                                 fm_fiscal_doc_header fdh 
                                           where fdd.fiscal_doc_line_id = I_fiscal_doc_line_id
                                             and fdh.fiscal_doc_id      = fdd.fiscal_doc_id 
                                             and fdh.requisition_type   <>'PO')
         and fdtde.tax_code           = vc.vat_code
         and ftc.tax_code = fdtde.tax_code
         and ftc.matching_ind = 'N';
--
   R_get_tax_detail     C_GET_TAX_DETAIL%ROWTYPE;
BEGIN
   L_bc_tax_amt :=0;
      open C_GET_TAX_DETAIL;
      loop
      fetch C_GET_TAX_DETAIL into R_get_tax_detail;
      exit when C_GET_TAX_DETAIL%NOTFOUND;
         if R_get_tax_detail.nic_ind ='Y' then
            L_bc_tax_amt := L_bc_tax_amt + R_get_tax_detail.unit_tax_amt;
         end if;
      end loop;
     close  C_GET_TAX_DETAIL;
     L_base_cost := I_nic_cost - L_bc_tax_amt;
     O_base_cost := (L_base_cost * I_nic_cost_factor) / I_nic_cost ;
   return TRUE;
EXCEPTION
    when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                NULL);
    return FALSE;
END CALC_BC;
----------------------------------------------------------------------------------
-- Function Name: INSERT_TRAN_DATA
-- Purpose:       This function Inserts into Tran Data based on the Tran Code
----------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                         ,I_tran_code      IN FM_TRAN_DATA.TRAN_CODE%TYPE
                         ,I_fiscal_doc_id  IN FM_TRAN_DATA.FISCAL_DOC_ID%TYPE
                         ,I_fiscal_doc_no  IN FM_TRAN_DATA.FISCAL_DOC_NO%TYPE
                         ,I_supplier       IN FM_TRAN_DATA.SUPPLIER%TYPE
                         ,I_dept           IN FM_TRAN_DATA.DEPT%TYPE
                         ,I_class          IN FM_TRAN_DATA.CLASS%TYPE
                         ,I_subclass       IN FM_TRAN_DATA.SUBCLASS%TYPE
                         ,I_item           IN FM_TRAN_DATA.ITEM%TYPE 
                         ,I_location_type  IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                         ,I_location       IN FM_TRAN_DATA.LOCATION%TYPE
                         ,I_total_cost     IN FM_TRAN_DATA.TOTAL_COST%TYPE
                         ,I_ref_no_1       IN FM_TRAN_DATA.REF_NO_1%TYPE
                         ,I_ref_no_2       IN FM_TRAN_DATA.REF_NO_2%TYPE 
                         ,I_ref_no_3       IN FM_TRAN_DATA.REF_NO_3%TYPE
                         ,I_ref_no_4       IN FM_TRAN_DATA.REF_NO_4%TYPE
                         ,I_ref_no_5       IN FM_TRAN_DATA.REF_NO_5%TYPE
                         ,I_tran_date      IN FM_TRAN_DATA.TRAN_DATE%TYPE ) 
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '|| I_fiscal_doc_id;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
BEGIN
--
 if (I_total_cost <> 0) then
   INSERT INTO GTT_FM_TRAN_DATA( TRAN_ID
                                ,TRAN_CODE
                                ,FISCAL_DOC_ID
                                ,FISCAL_DOC_NO
                                ,SUPPLIER
                                ,DEPT
                                ,CLASS
                                ,SUBCLASS
                                ,ITEM
                                ,LOCATION_TYPE
                                ,LOCATION
                                ,TOTAL_COST
                                ,REF_NO_1
                                ,REF_NO_2
                                ,REF_NO_3
                                ,REF_NO_4
                                ,REF_NO_5
                                ,TRAN_DATE
                                ,CREATE_DATETIME
                                ,CREATE_ID
                                ,LAST_UPDATE_DATETIME
                                ,LAST_UPDATE_ID
                                ) VALUES
                                (fm_tran_id_seq.nextval
                                ,I_tran_code
                                ,I_fiscal_doc_id
                                ,I_fiscal_doc_no
                                ,I_supplier
                                ,I_dept
                                ,I_class
                                ,I_subclass
                                ,I_item
                                ,I_location_type
                                ,I_location
                                ,I_total_cost
                                ,I_ref_no_1
                                ,I_ref_no_2
                                ,I_ref_no_3
                                ,I_ref_no_4
                                ,I_ref_no_5
                                ,I_tran_date
                                ,sysdate
                                ,user
                                ,sysdate
                                ,user);
end if;                                
--
   return TRUE;
EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
--
END INSERT_TRAN_DATA;
--
----------------------------------------------------------------------------------
-- Function Name: GET_REF_VALUES
-- Purpose:       This function gets the REF values based on the Tran codes
----------------------------------------------------------------------------------
FUNCTION GET_REF_VALUES(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                       ,I_tran_code           IN FM_TRAN_DATA.TRAN_CODE%TYPE
                       ,I_fiscal_doc_id       IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                       ,I_fiscal_doc_line_id  IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                       ,I_vat_code            IN FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE
                       ,O_ref_no_1           OUT FM_TRAN_CODES.REF_NO_1%TYPE
                       ,O_ref_no_2           OUT FM_TRAN_CODES.REF_NO_2%TYPE
                       ,O_ref_no_3           OUT FM_TRAN_CODES.REF_NO_3%TYPE
                       ,O_ref_no_4           OUT FM_TRAN_CODES.REF_NO_4%TYPE
                       ,O_ref_no_5           OUT FM_TRAN_CODES.REF_NO_5%TYPE) 
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.GET_REF_VALUES';
   L_key                 VARCHAR2(80) := 'tran_code = '|| I_tran_code;
   L_count               NUMBER;
--
   cursor C_GET_REF_VALUES is
      SELECT ref_no_1
            ,ref_no_2
            ,ref_no_3
            ,ref_no_4
            ,ref_no_5
       FROM fm_tran_codes
      WHERE tran_code = I_tran_code;
--
   cursor C_GET_UTILIZATION is
      SELECT utilization_id
        FROM fm_fiscal_doc_header  
       WHERE fiscal_doc_id = I_fiscal_doc_id;
--
  cursor C_GET_TAX_COUNT is
     SELECT count(1)
       FROM fm_tran_codes
      WHERE (ref_no_1 = 'T' or ref_no_2 = 'T' or ref_no_3 = 'T' or ref_no_4 = 'T' or ref_no_5 = 'T')
        AND tran_code = I_tran_code;
BEGIN
--
   SQL_LIB.SET_MARK('OPEN','C_GET_REF_VALUES', 'fm_tran_codes', L_key);
   OPEN C_GET_REF_VALUES;
   SQL_LIB.SET_MARK('FETCH','C_GET_REF_VALUES', 'fm_tran_codes', L_key);
   FETCH C_GET_REF_VALUES INTO O_ref_no_1,O_ref_no_2,O_ref_no_3,O_ref_no_4,O_ref_no_5;
   SQL_LIB.SET_MARK('CLOSE','C_GET_REF_VALUES', 'fm_tran_codes', L_key);
   CLOSE C_GET_REF_VALUES;
--
   OPEN C_GET_TAX_COUNT;
   FETCH C_GET_TAX_COUNT INTO L_count;
   CLOSE C_GET_TAX_COUNT;
--
   if O_ref_no_1 is not null then
      if O_ref_no_1 = 'U' then
         OPEN C_GET_UTILIZATION;
         FETCH C_GET_UTILIZATION INTO O_ref_no_1;
         CLOSE C_GET_UTILIZATION;
      elsif O_ref_no_1 = 'T' then
          O_ref_no_1 := NVL(I_vat_code,'T');
      elsif O_ref_no_1 = 'CC' then
         O_ref_no_1 := 'CC';
      end if;
   elsif O_ref_no_2 is not null then
      if O_ref_no_2 = 'U' then
         OPEN C_GET_UTILIZATION;
         FETCH C_GET_UTILIZATION INTO O_ref_no_2;
         CLOSE C_GET_UTILIZATION;
      elsif O_ref_no_2 = 'T' then
          O_ref_no_2 := I_vat_code;
      elsif O_ref_no_2 = 'CC' then
         O_ref_no_2 := 'CC';
      end if;
   elsif O_ref_no_3 is not null then
      if O_ref_no_3 = 'U' then
         OPEN C_GET_UTILIZATION;
         FETCH C_GET_UTILIZATION INTO O_ref_no_3;
         CLOSE C_GET_UTILIZATION;
      elsif O_ref_no_3 = 'T' then
          O_ref_no_3 := I_vat_code;
      elsif O_ref_no_3 = 'CC' then
         O_ref_no_3 := 'CC';
      end if;
   elsif O_ref_no_4 is not null then
      if O_ref_no_4 = 'U' then
         OPEN C_GET_UTILIZATION;
         FETCH C_GET_UTILIZATION INTO O_ref_no_4;
         CLOSE C_GET_UTILIZATION;
      elsif O_ref_no_4 = 'T' then
          O_ref_no_4 := I_vat_code;
      elsif O_ref_no_4 = 'CC' then
         O_ref_no_4 := 'CC';
      end if;
   elsif O_ref_no_5 is not null then
      if O_ref_no_5 = 'U' then
         OPEN C_GET_UTILIZATION;
         FETCH C_GET_UTILIZATION INTO O_ref_no_5;
         CLOSE C_GET_UTILIZATION;
      elsif O_ref_no_5 = 'T' then
          O_ref_no_5 := I_vat_code;
      elsif O_ref_no_5 = 'CC' then
         O_ref_no_5 := 'CC';
      end if;
   end if;
--
   if L_count = 0 then
      if O_ref_no_1 is null then
         O_ref_no_1 := 'NT';
      elsif O_ref_no_2 is null then
         O_ref_no_2 := 'NT';
      elsif O_ref_no_3 is null then
         O_ref_no_3 := 'NT';
      elsif O_ref_no_4 is null then
         O_ref_no_4 := 'NT';
      elsif O_ref_no_5 is null then
         O_ref_no_5 := 'NT';
      end if;
   end if;
--
   return TRUE;
EXCEPTION
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      return FALSE;
--
END GET_REF_VALUES;
--
----------------------------------------------------------------------------------
-- Function Name: PO_WITH_VARIANCE
-- Purpose:       This function This function Process Tran Data when the PO has Variance cost
----------------------------------------------------------------------------------
FUNCTION PO_WITH_VARIANCE( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                          ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                          ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                          ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                          ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                          ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                          ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                          ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                          ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                          ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                          ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                          ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                          ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                          ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                          ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                          ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                          ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                          ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                          ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                          ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                          ,I_triangulation_ind    IN VARCHAR2
                          ,I_requisition_no       IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                          ,I_comp_fiscal_doc_id   IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE) 
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.PO_WITH_VARIANCE';
   L_key                 VARCHAR2(80) := 'Fiscal_Doc_id = '|| I_fiscal_doc_id;
   L_ebc_cost            FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
   I_nf_vwt              FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE;
   L_ord_unit_cost       ORDLOC.UNIT_COST%TYPE;
   L_ord_quantity        ORDLOC.QTY_ORDERED%TYPE;
   L_var_quantity        ORDLOC.QTY_ORDERED%TYPE;
   L_main_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_comp_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_comp_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
--
  cursor C_GET_TAX_DETAIL(P_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
       select fdtd.vat_code vat_code
            ,fdtd.fiscal_doc_line_id
            ,nvl(fdtd.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail fdtd,
            fm_tax_codes ftc
      where fdtd.fiscal_doc_line_id in (I_fiscal_doc_line_id ,P_fiscal_doc_line_id)
        and ftc.tax_code = fdtd.vat_code
        and ftc.matching_ind = 'Y'
      UNION ALL
      select fdtde.tax_code vat_code
            ,fdtde.fiscal_doc_line_id
            ,nvl(fdtde.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtde.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail_ext fdtde,
            fm_tax_codes ftc
      where fdtde.fiscal_doc_line_id = I_fiscal_doc_line_id
        and ftc.tax_code = fdtde.tax_code
        and ftc.matching_ind = 'N';
--
   cursor C_GET_TRAN_DATA_TRAN is
      SELECT fdd.fiscal_doc_line_id
        FROM fm_fiscal_doc_detail fdd
       WHERE fdd.fiscal_doc_id  = I_comp_fiscal_doc_id
         AND fdd.requisition_no = I_requisition_no
         AND fdd.item           = I_item;
BEGIN
--
   if (I_tolerance_value_cost is not null or I_tolerance_value_cost <> 0) then
--
       if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message,
                                           O_base_cost          => L_base_cost,
                                           I_nic_cost           => I_unit_cost,
                                           I_nic_cost_factor    => I_tolerance_value_cost,
                                           I_fiscal_doc_line_id => I_fiscal_doc_line_id ) = FALSE then
          return FALSE;
       end if;
       I_nf_vwt := nvl(L_base_cost,0) * I_quantity;
   end if;      
--
   if (I_tolerance_value_qty is not null or I_tolerance_value_qty <> 0) then
      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                          O_base_cost          => L_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
         return FALSE;
      end if;
      I_nf_vwt := nvl(I_nf_vwt,0) + nvl(L_base_cost,0) * I_tolerance_value_qty;
   end if;   
--
   if I_triangulation_ind = 'N' then
      if I_nf_vwt is not null then
         if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                  ,I_tran_code          => 104 
                                                  ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                  ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                  ,I_vat_code           => NULL
                                                  ,O_ref_no_1           => L_ref_no_1
                                                  ,O_ref_no_2           => L_ref_no_2
                                                  ,O_ref_no_3           => L_ref_no_3
                                                  ,O_ref_no_4           => L_ref_no_4
                                                  ,O_ref_no_5           => L_ref_no_5) = FALSE then
            return FALSE;
         end if;
   --
         if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                    ,I_tran_code     => 104 
                                                    ,I_fiscal_doc_id => I_fiscal_doc_id
                                                    ,I_fiscal_doc_no => I_fiscal_doc_no
                                                    ,I_supplier      => I_supplier
                                                    ,I_dept          => I_dept
                                                    ,I_class         => I_class
                                                    ,I_subclass      => I_subclass
                                                    ,I_item          => I_item
                                                    ,I_location_type => I_location_type
                                                    ,I_location      => I_location_id
                                                    ,I_total_cost    => I_nf_vwt
                                                    ,I_ref_no_1      => L_ref_no_1
                                                    ,I_ref_no_2      => L_ref_no_2
                                                    ,I_ref_no_3      => L_ref_no_3
                                                    ,I_ref_no_4      => L_ref_no_4
                                                    ,I_ref_no_5      => L_ref_no_5
                                                    ,I_tran_date     => SYSDATE)  = FALSE then
            return FALSE;
         end if;
   --
         if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                             O_base_cost          => L_base_cost,
                                             I_nic_cost           => I_unit_cost,
                                             I_nic_cost_factor    => I_unit_cost,
                                             I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
            return FALSE;
         end if;
    
         if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                  ,I_tran_code          => 100 
                                                  ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                  ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                  ,I_vat_code           => NULL
                                                  ,O_ref_no_1           => L_ref_no_1
                                                  ,O_ref_no_2           => L_ref_no_2
                                                  ,O_ref_no_3           => L_ref_no_3
                                                  ,O_ref_no_4           => L_ref_no_4
                                                  ,O_ref_no_5           => L_ref_no_5) = FALSE then
            return FALSE;
         end if;
      --
         if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                    ,I_tran_code     => 100 
                                                    ,I_fiscal_doc_id => I_fiscal_doc_id
                                                    ,I_fiscal_doc_no => I_fiscal_doc_no
                                                    ,I_supplier      => I_supplier
                                                    ,I_dept          => I_dept
                                                    ,I_class         => I_class
                                                    ,I_subclass      => I_subclass
                                                    ,I_item          => I_item
                                                    ,I_location_type => I_location_type
                                                    ,I_location      => I_location_id
                                                    ,I_total_cost    => (L_base_cost * I_quantity - I_nf_vwt)
                                                    ,I_ref_no_1      => L_ref_no_1
                                                    ,I_ref_no_2      => L_ref_no_2
                                                    ,I_ref_no_3      => L_ref_no_3
                                                    ,I_ref_no_4      => L_ref_no_4
                                                    ,I_ref_no_5      => L_ref_no_5
                                                    ,I_tran_date     => SYSDATE)  = FALSE then
            return FALSE;
         end if;
   --
         FOR L_get_tax_detail in C_GET_TAX_DETAIL(I_fiscal_doc_line_id)
         LOOP
            if L_get_tax_detail.unit_rec_value = 0 then
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 106 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 106 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => ((((L_get_tax_detail.unit_tax_amt / I_unit_cost) * nvl(I_tolerance_value_cost,0)) * I_quantity) + ( Nvl(I_tolerance_value_qty,0) * L_get_tax_detail.unit_tax_amt))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 101 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 101 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt * I_quantity) - ((((L_get_tax_detail.unit_tax_amt / I_unit_cost) * nvl(I_tolerance_value_cost,0)) * I_quantity) + ( Nvl(I_tolerance_value_qty,0) * L_get_tax_detail.unit_tax_amt)))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
      --
            elsif L_get_tax_detail.unit_rec_value > 0 and (L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) = 0 then
                  if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                          ,I_tran_code          => 105 
                                                          ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                          ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                          ,I_vat_code           => L_get_tax_detail.vat_code
                                                          ,O_ref_no_1           => L_ref_no_1
                                                          ,O_ref_no_2           => L_ref_no_2
                                                          ,O_ref_no_3           => L_ref_no_3
                                                          ,O_ref_no_4           => L_ref_no_4
                                                          ,O_ref_no_5           => L_ref_no_5) = FALSE then
                    return FALSE;
                 end if;
      --
                 if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                            ,I_tran_code     => 105 
                                                            ,I_fiscal_doc_id => I_fiscal_doc_id
                                                            ,I_fiscal_doc_no => I_fiscal_doc_no
                                                            ,I_supplier      => I_supplier
                                                            ,I_dept          => I_dept
                                                            ,I_class         => I_class
                                                            ,I_subclass      => I_subclass
                                                            ,I_item          => I_item
                                                            ,I_location_type => I_location_type
                                                            ,I_location      => I_location_id
                                                            ,I_total_cost    => ((((L_get_tax_detail.unit_rec_value / I_unit_cost) * nvl(I_tolerance_value_cost,0)) * I_quantity) + ( Nvl(I_tolerance_value_qty,0) * L_get_tax_detail.unit_rec_value))
                                                            ,I_ref_no_1      => L_ref_no_1
                                                            ,I_ref_no_2      => L_ref_no_2
                                                            ,I_ref_no_3      => L_ref_no_3
                                                            ,I_ref_no_4      => L_ref_no_4
                                                            ,I_ref_no_5      => L_ref_no_5
                                                            ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                 end if;
   --
                  if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                          ,I_tran_code          => 102 
                                                          ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                          ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                          ,I_vat_code           => L_get_tax_detail.vat_code
                                                          ,O_ref_no_1           => L_ref_no_1
                                                          ,O_ref_no_2           => L_ref_no_2
                                                          ,O_ref_no_3           => L_ref_no_3
                                                          ,O_ref_no_4           => L_ref_no_4
                                                          ,O_ref_no_5           => L_ref_no_5) = FALSE then
                    return FALSE;
                 end if;
      --
                 if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                            ,I_tran_code     => 102 
                                                            ,I_fiscal_doc_id => I_fiscal_doc_id
                                                            ,I_fiscal_doc_no => I_fiscal_doc_no
                                                            ,I_supplier      => I_supplier
                                                            ,I_dept          => I_dept
                                                            ,I_class         => I_class
                                                            ,I_subclass      => I_subclass
                                                            ,I_item          => I_item
                                                            ,I_location_type => I_location_type
                                                            ,I_location      => I_location_id
                                                            ,I_total_cost    => ((L_get_tax_detail.unit_rec_value * I_quantity) - ((((L_get_tax_detail.unit_rec_value / I_unit_cost) * nvl(I_tolerance_value_cost,0)) * I_quantity) + ( Nvl(I_tolerance_value_qty,0) * L_get_tax_detail.unit_rec_value)))
                                                            ,I_ref_no_1      => L_ref_no_1
                                                            ,I_ref_no_2      => L_ref_no_2
                                                            ,I_ref_no_3      => L_ref_no_3
                                                            ,I_ref_no_4      => L_ref_no_4
                                                            ,I_ref_no_5      => L_ref_no_5
                                                            ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                 end if;
            else 
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 106 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 106 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => (((((L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value) / I_unit_cost) * nvl(I_tolerance_value_cost,0)) *  I_quantity) + ( Nvl(I_tolerance_value_qty,0) * (L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value)))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
   --
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 101 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 101 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value ) * I_quantity ) - (((((L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value) / I_unit_cost) * nvl(I_tolerance_value_cost,0)) *  I_quantity) + ( Nvl(I_tolerance_value_qty,0) * (L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value)))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                          ,I_tran_code          => 105 
                                                          ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                          ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                          ,I_vat_code           => L_get_tax_detail.vat_code
                                                          ,O_ref_no_1           => L_ref_no_1
                                                          ,O_ref_no_2           => L_ref_no_2
                                                          ,O_ref_no_3           => L_ref_no_3
                                                          ,O_ref_no_4           => L_ref_no_4
                                                          ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 105 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => ((((L_get_tax_detail.unit_rec_value / I_unit_cost) * nvl(I_tolerance_value_cost,0)) *  I_quantity) + ( Nvl(I_tolerance_value_qty,0) *  L_get_tax_detail.unit_rec_value))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                end if;
   
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                         ,I_tran_code          => 102 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 102 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => (L_get_tax_detail.unit_rec_value * I_quantity) - ((((L_get_tax_detail.unit_rec_value / I_unit_cost) * nvl(I_tolerance_value_cost,0)) *  I_quantity) + ( Nvl(I_tolerance_value_qty,0) *  L_get_tax_detail.unit_rec_value))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                end if;
            end if;
         END LOOP;
   --
         if I_freight_cost is not null and I_freight_cost > 0 then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 103 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => NULL
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
 --
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FMCC',
                                          'F',
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;   

            if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
               L_ref_no_1 := L_code_desc;
            elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
               L_ref_no_2 := L_code_desc;
            elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
               L_ref_no_3 := L_code_desc;
            elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
               L_ref_no_4 := L_code_desc;
            elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
               L_ref_no_5 := L_code_desc;
            end if; 
        --
--
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 103 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (I_freight_cost * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         end if; 
--
         if (I_insurance_cost is not null and I_insurance_cost > 0) then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 103 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => NULL
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
              return FALSE;
            end if;
 --
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FMCC',
                                          'I',
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;   

            if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
               L_ref_no_1 := L_code_desc;
            elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
               L_ref_no_2 := L_code_desc;
            elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
               L_ref_no_3 := L_code_desc;
            elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
               L_ref_no_4 := L_code_desc;
            elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
               L_ref_no_5 := L_code_desc;
            end if; 
--
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 103 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (I_insurance_cost * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
              return FALSE;
            end if; 
         end if; 
      --
         if (I_other_expenses_cost is not null and I_other_expenses_cost > 0) then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                     ,I_tran_code          => 103 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => NULL
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
 --
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FMCC',
                                          'O',
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;   

            if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
               L_ref_no_1 := L_code_desc;
            elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
               L_ref_no_2 := L_code_desc;
            elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
               L_ref_no_3 := L_code_desc;
            elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
               L_ref_no_4 := L_code_desc;
            elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
               L_ref_no_5 := L_code_desc;
            end if; 
--
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 103 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (I_other_expenses_cost * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if; 
         end if; 
      end if; 
   elsif  I_triangulation_ind = 'Y' then
--
       OPEN C_GET_TRAN_DATA_TRAN;
       FETCH C_GET_TRAN_DATA_TRAN INTO L_comp_fiscal_doc_line_id;
       CLOSE C_GET_TRAN_DATA_TRAN;
--
       if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                           O_base_cost          => L_main_base_cost,
                                           I_nic_cost           => I_unit_cost,
                                           I_nic_cost_factor    => I_unit_cost,
                                           I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
          return FALSE;
       end if;
    --
       if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                           O_base_cost          => L_comp_base_cost,
                                           I_nic_cost           => I_unit_cost,
                                           I_nic_cost_factor    => I_unit_cost,
                                           I_fiscal_doc_line_id => L_comp_fiscal_doc_line_id) = FALSE then
          return FALSE;
       end if;
--
      if I_nf_vwt is not null then
   --
         if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                  ,I_tran_code          => 104 
                                                  ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                  ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                  ,I_vat_code           => NULL
                                                  ,O_ref_no_1           => L_ref_no_1
                                                  ,O_ref_no_2           => L_ref_no_2
                                                  ,O_ref_no_3           => L_ref_no_3
                                                  ,O_ref_no_4           => L_ref_no_4
                                                  ,O_ref_no_5           => L_ref_no_5) = FALSE then
            return FALSE;
         end if;
   --
         if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                    ,I_tran_code     => 104 
                                                    ,I_fiscal_doc_id => I_fiscal_doc_id
                                                    ,I_fiscal_doc_no => I_fiscal_doc_no
                                                    ,I_supplier      => I_supplier
                                                    ,I_dept          => I_dept
                                                    ,I_class         => I_class
                                                    ,I_subclass      => I_subclass
                                                    ,I_item          => I_item
                                                    ,I_location_type => I_location_type
                                                    ,I_location      => I_location_id
                                                    ,I_total_cost    => I_nf_vwt
                                                    ,I_ref_no_1      => L_ref_no_1
                                                    ,I_ref_no_2      => L_ref_no_2
                                                    ,I_ref_no_3      => L_ref_no_3
                                                    ,I_ref_no_4      => L_ref_no_4
                                                    ,I_ref_no_5      => L_ref_no_5
                                                    ,I_tran_date     => SYSDATE)  = FALSE then
            return FALSE;
         end if;
   --
         if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                             O_base_cost          => L_base_cost,
                                             I_nic_cost           => I_unit_cost,
                                             I_nic_cost_factor    => I_unit_cost,
                                             I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
            return FALSE;
         end if;
   --
         if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                  ,I_tran_code          => 100 
                                                  ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                  ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                  ,I_vat_code           => NULL
                                                  ,O_ref_no_1           => L_ref_no_1
                                                  ,O_ref_no_2           => L_ref_no_2
                                                  ,O_ref_no_3           => L_ref_no_3
                                                  ,O_ref_no_4           => L_ref_no_4
                                                  ,O_ref_no_5           => L_ref_no_5) = FALSE then
            return FALSE;
         end if;
      --
         if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                    ,I_tran_code     => 100 
                                                    ,I_fiscal_doc_id => I_fiscal_doc_id
                                                    ,I_fiscal_doc_no => I_fiscal_doc_no
                                                    ,I_supplier      => I_supplier
                                                    ,I_dept          => I_dept
                                                    ,I_class         => I_class
                                                    ,I_subclass      => I_subclass
                                                    ,I_item          => I_item
                                                    ,I_location_type => I_location_type
                                                    ,I_location      => I_location_id
                                                    ,I_total_cost    => ((L_main_base_cost + L_comp_base_cost - I_unit_cost) * I_quantity - I_nf_vwt)
                                                    ,I_ref_no_1      => L_ref_no_1
                                                    ,I_ref_no_2      => L_ref_no_2
                                                    ,I_ref_no_3      => L_ref_no_3
                                                    ,I_ref_no_4      => L_ref_no_4
                                                    ,I_ref_no_5      => L_ref_no_5
                                                    ,I_tran_date     => SYSDATE)  = FALSE then
            return FALSE;
         end if;
   --
         FOR L_get_tax_detail in C_GET_TAX_DETAIL(L_comp_fiscal_doc_line_id)
         LOOP
            if L_get_tax_detail.unit_rec_value = 0 then
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 106 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 106 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => ((((L_get_tax_detail.unit_tax_amt / I_unit_cost) * nvl(I_tolerance_value_cost,0)) * I_quantity) + ( Nvl(I_tolerance_value_qty,0) * L_get_tax_detail.unit_tax_amt))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 101 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 101 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt * I_quantity) - ((((L_get_tax_detail.unit_tax_amt / I_unit_cost) * nvl(I_tolerance_value_cost,0)) * I_quantity) + ( Nvl(I_tolerance_value_qty,0) * L_get_tax_detail.unit_tax_amt)))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
      --
            elsif L_get_tax_detail.unit_rec_value > 0 and (L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) = 0 then
                  if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                          ,I_tran_code          => 105 
                                                          ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                          ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                          ,I_vat_code           => L_get_tax_detail.vat_code
                                                          ,O_ref_no_1           => L_ref_no_1
                                                          ,O_ref_no_2           => L_ref_no_2
                                                          ,O_ref_no_3           => L_ref_no_3
                                                          ,O_ref_no_4           => L_ref_no_4
                                                          ,O_ref_no_5           => L_ref_no_5) = FALSE then
                    return FALSE;
                 end if;
      --
                 if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                            ,I_tran_code     => 105 
                                                            ,I_fiscal_doc_id => I_fiscal_doc_id
                                                            ,I_fiscal_doc_no => I_fiscal_doc_no
                                                            ,I_supplier      => I_supplier
                                                            ,I_dept          => I_dept
                                                            ,I_class         => I_class
                                                            ,I_subclass      => I_subclass
                                                            ,I_item          => I_item
                                                            ,I_location_type => I_location_type
                                                            ,I_location      => I_location_id
                                                            ,I_total_cost    => ((((L_get_tax_detail.unit_rec_value / I_unit_cost) * nvl(I_tolerance_value_cost,0)) * I_quantity) + ( Nvl(I_tolerance_value_qty,0) * L_get_tax_detail.unit_rec_value))
                                                            ,I_ref_no_1      => L_ref_no_1
                                                            ,I_ref_no_2      => L_ref_no_2
                                                            ,I_ref_no_3      => L_ref_no_3
                                                            ,I_ref_no_4      => L_ref_no_4
                                                            ,I_ref_no_5      => L_ref_no_5
                                                            ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                 end if;
   --
                  if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                          ,I_tran_code          => 102 
                                                          ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                          ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                          ,I_vat_code           => L_get_tax_detail.vat_code
                                                          ,O_ref_no_1           => L_ref_no_1
                                                          ,O_ref_no_2           => L_ref_no_2
                                                          ,O_ref_no_3           => L_ref_no_3
                                                          ,O_ref_no_4           => L_ref_no_4
                                                          ,O_ref_no_5           => L_ref_no_5) = FALSE then
                    return FALSE;
                 end if;
      --
                 if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                            ,I_tran_code     => 102 
                                                            ,I_fiscal_doc_id => I_fiscal_doc_id
                                                            ,I_fiscal_doc_no => I_fiscal_doc_no
                                                            ,I_supplier      => I_supplier
                                                            ,I_dept          => I_dept
                                                            ,I_class         => I_class
                                                            ,I_subclass      => I_subclass
                                                            ,I_item          => I_item
                                                            ,I_location_type => I_location_type
                                                            ,I_location      => I_location_id
                                                            ,I_total_cost    => ((L_get_tax_detail.unit_rec_value * I_quantity) - ((((L_get_tax_detail.unit_rec_value / I_unit_cost) * nvl(I_tolerance_value_cost,0)) * I_quantity) + ( Nvl(I_tolerance_value_qty,0) * L_get_tax_detail.unit_rec_value)))
                                                            ,I_ref_no_1      => L_ref_no_1
                                                            ,I_ref_no_2      => L_ref_no_2
                                                            ,I_ref_no_3      => L_ref_no_3
                                                            ,I_ref_no_4      => L_ref_no_4
                                                            ,I_ref_no_5      => L_ref_no_5
                                                            ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                 end if;
            else 
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 106 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 106 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => (((((L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value) / I_unit_cost) * nvl(I_tolerance_value_cost,0)) *  I_quantity) + ( Nvl(I_tolerance_value_qty,0) * (L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value)))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
   --
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 101 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 101 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value ) * I_quantity ) - (((((L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value) / I_unit_cost) * nvl(I_tolerance_value_cost,0)) *  I_quantity) + ( Nvl(I_tolerance_value_qty,0) * (L_get_tax_detail.unit_tax_amt  - L_get_tax_detail.unit_rec_value)))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
                
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                         ,I_tran_code          => 105 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 105 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => ((((L_get_tax_detail.unit_rec_value / I_unit_cost) * nvl(I_tolerance_value_cost,0)) *  I_quantity) + ( Nvl(I_tolerance_value_qty,0) *  L_get_tax_detail.unit_rec_value))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                end if;
   --
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                         ,I_tran_code          => 102 
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                         ,I_vat_code           => L_get_tax_detail.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 102 
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => I_fiscal_doc_no
                                                           ,I_supplier      => I_supplier
                                                           ,I_dept          => I_dept
                                                           ,I_class         => I_class
                                                           ,I_subclass      => I_subclass
                                                           ,I_item          => I_item
                                                           ,I_location_type => I_location_type
                                                           ,I_location      => I_location_id
                                                           ,I_total_cost    => (L_get_tax_detail.unit_rec_value * I_quantity) - ((((L_get_tax_detail.unit_rec_value / I_unit_cost) * nvl(I_tolerance_value_cost,0)) *  I_quantity) + ( Nvl(I_tolerance_value_qty,0) *  L_get_tax_detail.unit_rec_value))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                end if;
            end if;
         END LOOP;
   --
         if I_freight_cost is not null and I_freight_cost > 0 then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 103 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => NULL
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
   --
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FMCC',
                                          'F',
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;   

            if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
               L_ref_no_1 := L_code_desc;
            elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
               L_ref_no_2 := L_code_desc;
            elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
               L_ref_no_3 := L_code_desc;
            elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
               L_ref_no_4 := L_code_desc;
            elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
               L_ref_no_5 := L_code_desc;
            end if; 
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 103 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (I_freight_cost * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         end if; 
      --
         if (I_insurance_cost is not null and I_insurance_cost > 0) then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 103 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => NULL
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
              return FALSE;
            end if;
   --
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FMCC',
                                          'I',
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;   

            if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
               L_ref_no_1 := L_code_desc;
            elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
               L_ref_no_2 := L_code_desc;
            elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
               L_ref_no_3 := L_code_desc;
            elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
               L_ref_no_4 := L_code_desc;
            elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
               L_ref_no_5 := L_code_desc;
            end if; 
--
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 103 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (I_insurance_cost * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
              return FALSE;
            end if;
         end if; 
      --
         if (I_other_expenses_cost is not null and I_other_expenses_cost > 0) then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                     ,I_tran_code          => 103 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => NULL
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
   --
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FMCC',
                                          'O',
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;   

            if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
               L_ref_no_1 := L_code_desc;
            elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
               L_ref_no_2 := L_code_desc;
            elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
               L_ref_no_3 := L_code_desc;
            elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
               L_ref_no_4 := L_code_desc;
            elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
               L_ref_no_5 := L_code_desc;
            end if; 
--
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 103 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (I_other_expenses_cost * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         end if; 
      end if;
   end if; 
--
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_GET_TAX_DETAIL%ISOPEN then
         close C_GET_TAX_DETAIL;
      end if;
      return FALSE;
END PO_WITH_VARIANCE;
----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_RTV
-- Purpose:       This function Post trandata when the requisition type is RTV
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_RTV( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                           ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                           ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                           ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                           ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                           ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                           ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                           ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                           ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                           ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                           ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                           ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                           ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                           ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                           ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                           ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                           ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                           ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                           ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                           ,I_triangulation_ind    IN VARCHAR2
                           ,I_requisition_no       IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                           ,I_comp_fiscal_doc_id   IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE) 
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.POST_TRANDATA_RTV';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
--
   L_ebc_cost            FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
--
   L_main_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_comp_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_comp_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
--
   cursor C_GET_TAX_DETAIL(P_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdtd.vat_code vat_code
            ,fdtd.fiscal_doc_line_id
            ,nvl(fdtd.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail fdtd
      where fdtd.fiscal_doc_line_id in (I_fiscal_doc_line_id ,P_fiscal_doc_line_id);
--
   cursor C_GET_TRAN_DATA_TRAN is
      SELECT fdd.fiscal_doc_line_id
        FROM fm_fiscal_doc_detail fdd
       WHERE fdd.fiscal_doc_id  = I_comp_fiscal_doc_id
         AND fdd.requisition_no = I_requisition_no
         AND fdd.item           = I_item;
BEGIN
--
   if I_triangulation_ind = 'N' then
--
      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                          O_base_cost          => L_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
         return FALSE;
      end if;
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 122 
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
   --
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 122 
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => (L_base_cost * I_quantity)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   --
      FOR L_get_tax_detail IN C_GET_TAX_DETAIL(I_fiscal_doc_line_id)
      LOOP
         if L_get_tax_detail.unit_rec_value = 0 then
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 123 
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                      ,I_vat_code           => L_get_tax_detail.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
   --
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 123 
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => (L_get_tax_detail.unit_tax_amt * I_quantity)
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
   --
         elsif L_get_tax_detail.unit_rec_value > 0 and (L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) = 0 then
               if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                        ,I_tran_code          => 124 
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => L_get_tax_detail.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                 return FALSE;
              end if;
   --
              if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                         ,I_tran_code     => 124 
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => I_fiscal_doc_no
                                                         ,I_supplier      => I_supplier
                                                         ,I_dept          => I_dept
                                                         ,I_class         => I_class
                                                         ,I_subclass      => I_subclass
                                                         ,I_item          => I_item
                                                         ,I_location_type => I_location_type
                                                         ,I_location      => I_location_id
                                                         ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  I_quantity)
                                                         ,I_ref_no_1      => L_ref_no_1
                                                         ,I_ref_no_2      => L_ref_no_2
                                                         ,I_ref_no_3      => L_ref_no_3
                                                         ,I_ref_no_4      => L_ref_no_4
                                                         ,I_ref_no_5      => L_ref_no_5
                                                         ,I_tran_date     => SYSDATE)  = FALSE then
                 return FALSE;
              end if;
         else 
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 123 
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                      ,I_vat_code           => L_get_tax_detail.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
   --
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 123 
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) * I_quantity)
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
               if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 124 
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => L_get_tax_detail.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                 return FALSE;
              end if;
   --
              if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                         ,I_tran_code     => 124 
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => I_fiscal_doc_no
                                                         ,I_supplier      => I_supplier
                                                         ,I_dept          => I_dept
                                                         ,I_class         => I_class
                                                         ,I_subclass      => I_subclass
                                                         ,I_item          => I_item
                                                         ,I_location_type => I_location_type
                                                         ,I_location      => I_location_id
                                                         ,I_total_cost    => (L_get_tax_detail.unit_rec_value * I_quantity)
                                                         ,I_ref_no_1      => L_ref_no_1
                                                         ,I_ref_no_2      => L_ref_no_2
                                                         ,I_ref_no_3      => L_ref_no_3
                                                         ,I_ref_no_4      => L_ref_no_4
                                                         ,I_ref_no_5      => L_ref_no_5
                                                         ,I_tran_date     => SYSDATE)  = FALSE then
                 return FALSE;
              end if;
         end if;
      END LOOP;
   elsif I_triangulation_ind = 'Y' then
      OPEN C_GET_TRAN_DATA_TRAN;
      FETCH C_GET_TRAN_DATA_TRAN INTO L_comp_fiscal_doc_line_id;
      CLOSE C_GET_TRAN_DATA_TRAN;
--
      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                          O_base_cost          => L_main_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
         return FALSE;
      end if;
   --
      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                          O_base_cost          => L_comp_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => L_comp_fiscal_doc_line_id) = FALSE then
         return FALSE;
      end if;
--
      
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 122 
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
      --
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 122 
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => ((L_main_base_cost + L_comp_base_cost - I_unit_cost) * I_quantity)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
--
      FOR L_get_tax_detail IN C_GET_TAX_DETAIL(L_comp_fiscal_doc_line_id)
      LOOP
         if L_get_tax_detail.unit_rec_value = 0 then
            
             
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 123 
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                      ,I_vat_code           => L_get_tax_detail.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
   --
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 123 
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => (L_get_tax_detail.unit_tax_amt * I_quantity)
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
      --
         elsif L_get_tax_detail.unit_rec_value > 0 and (L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) = 0 then
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                     ,I_tran_code          => 124 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
 --
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 124 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         else 
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 123 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
  
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 123 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
                
           
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 124 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
 
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 124 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (L_get_tax_detail.unit_rec_value * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         end if;
      END LOOP; 
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));

      if C_GET_TAX_DETAIL%ISOPEN then
         close C_GET_TAX_DETAIL;
      end if;

      return FALSE;

END POST_TRANDATA_RTV;

----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_RNF
-- Purpose:       This function Post trandata when the requisition type is RNF
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_RNF( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                           ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                           ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                           ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                           ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                           ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                           ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                           ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                           ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                           ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                           ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                           ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                           ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                           ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                           ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                           ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                           ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                           ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                           ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                           ,I_triangulation_ind    IN VARCHAR2
                           ,I_requisition_no       IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                           ,I_comp_fiscal_doc_id   IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE) 
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.POST_TRANDATA_RNF';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
   L_ebc_cost            FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
--
   L_main_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_comp_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_comp_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
--
   cursor C_GET_TAX_DETAIL(P_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdtd.vat_code vat_code
            ,fdtd.fiscal_doc_line_id
            ,nvl(fdtd.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail fdtd,
             fm_tax_codes ftc
      where fdtd.fiscal_doc_line_id in (I_fiscal_doc_line_id ,P_fiscal_doc_line_id)
        and ftc.tax_code = fdtd.vat_code
        and ftc.matching_ind = 'Y'
      UNION ALL
      select fdtde.tax_code vat_code
            ,fdtde.fiscal_doc_line_id
            ,nvl(fdtde.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtde.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail_ext fdtde,
             fm_tax_codes ftc
      where fdtde.fiscal_doc_line_id = I_fiscal_doc_line_id
        and ftc.tax_code = fdtde.tax_code
        and ftc.matching_ind = 'N'
     UNION ALL
     select fdtde.tax_code vat_code
            ,fdtde.fiscal_doc_line_id 
             ,NVL(fdtde.unit_tax_amt,0) as unit_tax_amt
             ,NVL(fdtde.unit_rec_value,0) as unit_rec_value
        from fm_fiscal_doc_tax_detail_ext fdtde,
             fm_tax_codes ftc
       where fdtde.fiscal_doc_line_id in (select fiscal_doc_line_id_ref from fm_fiscal_doc_detail where fiscal_doc_line_id = I_fiscal_doc_line_id)
        and ftc.tax_code = fdtde.tax_code
        and ftc.matching_ind = 'N';
--
   cursor C_GET_TRAN_DATA_TRAN is
      SELECT fdd.fiscal_doc_line_id
        FROM fm_fiscal_doc_detail fdd
       WHERE fdd.fiscal_doc_id  = I_comp_fiscal_doc_id
         AND fdd.requisition_no = I_requisition_no
         AND fdd.item           = I_item;
BEGIN

   if I_triangulation_ind = 'N' then
      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                          O_base_cost          => L_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
         return FALSE;
      end if;
   
      
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 110 
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
   
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 110 
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => (L_base_cost * I_quantity)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   
      FOR L_get_tax_detail IN C_GET_TAX_DETAIL(I_fiscal_doc_line_id)
      LOOP
         if L_get_tax_detail.unit_rec_value = 0 then
            
             
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 111 
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                      ,I_vat_code           => L_get_tax_detail.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
   
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 111 
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => (L_get_tax_detail.unit_tax_amt * I_quantity)
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
   
         elsif L_get_tax_detail.unit_rec_value > 0 and (L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) = 0 then
          
               if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                        ,I_tran_code          => 112 
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => L_get_tax_detail.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                 return FALSE;
              end if;
   
              if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                         ,I_tran_code     => 112 
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => I_fiscal_doc_no
                                                         ,I_supplier      => I_supplier
                                                         ,I_dept          => I_dept
                                                         ,I_class         => I_class
                                                         ,I_subclass      => I_subclass
                                                         ,I_item          => I_item
                                                         ,I_location_type => I_location_type
                                                         ,I_location      => I_location_id
                                                         ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  I_quantity)
                                                         ,I_ref_no_1      => L_ref_no_1
                                                         ,I_ref_no_2      => L_ref_no_2
                                                         ,I_ref_no_3      => L_ref_no_3
                                                         ,I_ref_no_4      => L_ref_no_4
                                                         ,I_ref_no_5      => L_ref_no_5
                                                         ,I_tran_date     => SYSDATE)  = FALSE then
                 return FALSE;
              end if;
         else 
            
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 111 
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                      ,I_vat_code           => L_get_tax_detail.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
   
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 111 
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) * I_quantity)
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
             
             
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 112 
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => L_get_tax_detail.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
             end if;
   
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                         ,I_tran_code     => 112 
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => I_fiscal_doc_no
                                                         ,I_supplier      => I_supplier
                                                         ,I_dept          => I_dept
                                                         ,I_class         => I_class
                                                         ,I_subclass      => I_subclass
                                                         ,I_item          => I_item
                                                         ,I_location_type => I_location_type
                                                         ,I_location      => I_location_id
                                                         ,I_total_cost    => (L_get_tax_detail.unit_rec_value * I_quantity)
                                                         ,I_ref_no_1      => L_ref_no_1
                                                         ,I_ref_no_2      => L_ref_no_2
                                                         ,I_ref_no_3      => L_ref_no_3
                                                         ,I_ref_no_4      => L_ref_no_4
                                                         ,I_ref_no_5      => L_ref_no_5
                                                         ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         end if;
      END LOOP;
   elsif I_triangulation_ind = 'N' then
      OPEN c_get_tran_data_tran;
      FETCH c_get_tran_data_tran INTO L_comp_fiscal_doc_line_id;
      CLOSE c_get_tran_data_tran;

      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                          O_base_cost          => L_main_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
         return FALSE;
      end if;
   
      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                          O_base_cost          => L_comp_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => L_comp_fiscal_doc_line_id) = FALSE then
         return FALSE;
      end if;

      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 110 
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
      
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 110 
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => (L_base_cost * I_quantity)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;

      FOR L_get_tax_detail IN C_GET_TAX_DETAIL(L_comp_fiscal_doc_line_id)
      LOOP
         if L_get_tax_detail.unit_rec_value = 0 then
            
             
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 111 
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                      ,I_vat_code           => L_get_tax_detail.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
      
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 111 
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => (L_get_tax_detail.unit_tax_amt * I_quantity)
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;

         elsif L_get_tax_detail.unit_rec_value > 0 and (L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) = 0 then

            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                     ,I_tran_code          => 112 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
              return FALSE;
            end if;
      
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 112 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         else 
           
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 111 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
      
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 111 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
            
            
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 112 
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
      
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 112 
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (L_get_tax_detail.unit_rec_value * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         end if;
      END LOOP;
   end if;
   if (I_freight_cost is not null and I_freight_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 113 
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;

      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'F',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if; 

      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 113 
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => (I_freight_cost * I_quantity)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if; 

   if (I_insurance_cost is not null and I_insurance_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 113 
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
        return FALSE;
      end if;

      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'I',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if;  

      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 113 
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => (I_insurance_cost * I_quantity)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
        return FALSE;
      end if;
   end if; 

   if (I_other_expenses_cost is not null and I_other_expenses_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 113 
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;

      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'O',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if;  

      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 113 
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => (I_other_expenses_cost * I_quantity)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if; 

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));

      if C_GET_TAX_DETAIL%ISOPEN then
         close C_GET_TAX_DETAIL;
      end if;

      return FALSE;

END POST_TRANDATA_RNF;

----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_CORRECTION
-- Purpose:       This function Process Tran Data Posting for Correction Letter
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_CORRECTION( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                  ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                                  ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                                  ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                                  ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                                  ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                                  ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                                  ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                                  ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                                  ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                                  ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                                  ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                                  ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                                  ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                                  ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                                  ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                                  ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                                  ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                                  ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                                  ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE) 
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.POST_TRANDATA_CORRECTION';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '|| I_fiscal_doc_id;
   L_cost                FM_TRAN_DATA.TOTAL_COST%TYPE;
   L_qty                 FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   
   L_received_qty        FM_RECEIVING_DETAIL.QUANTITY%TYPE :=0;
   L_sum_tax             FM_CORRECTION_TAX_DOC.TAX_AMOUNT%TYPE:=0;
   L_cor_exists          BOOLEAN := FALSE;
   L_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_corr_base_cost      FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_corr_tax_amt        FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE;
   
   L_corr_freight        FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;  
   L_corr_insur          FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_corr_othr           FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;

   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;

   cursor C_GET_CORRECTION_DETAILS is
      SELECT fcd.action_value
            ,fcd.action_type
            ,fdd.unit_cost
            ,fdd.quantity
            ,fcd.seq_no
      FROM fm_correction_doc fcd
          ,fm_fiscal_doc_detail fdd
      WHERE fcd.fiscal_doc_id = fdd.fiscal_doc_id
        AND fcd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
        AND fcd.fiscal_doc_id      = I_fiscal_doc_id
        AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
        AND action_type            in ('CLQ','CLC','CLF','CLI','CLO') ;

   cursor C_GET_SUM_TAX is
      SELECT nvl(SUM(fctd.tax_amount),0)
      FROM fm_correction_doc fcd
          ,fm_correction_tax_doc fctd
          ,vat_codes vc
      WHERE fcd.seq_no             = fctd.header_seq_no
        AND fcd.fiscal_doc_id      = I_fiscal_doc_id
        AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
        AND action_type            = 'CLT'
        AND vc.vat_code            = fctd.tax_code
        AND vc.incl_nic_ind        = 'Y';

   cursor C_GET_TAX_COREC_DETAILS(I_qty FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE)  is
      SELECT fctd.tax_code vat_code
            ,(fctd.tax_amount / I_qty ) unit_corr_tax_amt
            ,((fdtd.unit_rec_value / fdtd.unit_tax_amt) * (fctd.tax_amount / I_qty )) unit_rec_corr_tax_amt
            ,nvl(fdtd.unit_tax_amt,0) unit_nf_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_nf_rec_value
      FROM fm_correction_doc fcd
          ,fm_correction_tax_doc fctd
          ,fm_fiscal_doc_tax_detail fdtd
          ,fm_tax_codes ftc
      WHERE fcd.seq_no             = fctd.header_seq_no
       AND fcd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
       AND fctd.tax_code          = fdtd.vat_code
       AND fcd.fiscal_doc_id      = I_fiscal_doc_id
       AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
       AND action_type            = 'CLT'
       AND ftc.tax_code          = fdtd.vat_code
       AND ftc.matching_ind      = 'Y'
       UNION
       SELECT fctd.tax_code vat_code
            ,(fctd.tax_amount / I_qty ) unit_corr_tax_amt
            ,((fdtde.unit_rec_value / fdtde.unit_tax_amt) * (fctd.tax_amount / I_qty )) unit_rec_corr_tax_amt
            ,nvl(fdtde.unit_tax_amt,0) unit_nf_tax_amt
            ,nvl(fdtde.unit_rec_value,0) unit_nf_rec_value
       FROM fm_correction_doc fcd
           ,fm_correction_tax_doc fctd
           ,fm_fiscal_doc_tax_detail_ext fdtde
           ,fm_tax_codes ftc
     WHERE fcd.seq_no              = fctd.header_seq_no
       AND fcd.fiscal_doc_line_id  = fdtde.fiscal_doc_line_id
       AND fctd.tax_code           = fdtde.tax_code
       AND fcd.fiscal_doc_id       = I_fiscal_doc_id
       AND fcd.fiscal_doc_line_id  = I_fiscal_doc_line_id
       AND fcd.action_type         = 'CLT'
       AND ftc.tax_code            = fdtde.tax_code
       AND ftc.matching_ind        = 'N'
       AND fctd.tax_amount > 0 ;
       
       cursor C_GET_TAX_COREC_DTLS_ZERO  is
      SELECT fctd.tax_code vat_code
           ,nvl(fdtd.unit_tax_amt,0) unit_nf_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_nf_rec_value
      FROM fm_correction_doc fcd
          ,fm_correction_tax_doc fctd
          ,fm_fiscal_doc_tax_detail fdtd
          ,fm_tax_codes ftc
      WHERE fcd.seq_no             = fctd.header_seq_no
       AND fcd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
       AND fctd.tax_code          = fdtd.vat_code
       AND fcd.fiscal_doc_id      = I_fiscal_doc_id
       AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
       AND action_type            = 'CLT'
       AND ftc.tax_code          = fdtd.vat_code
       AND ftc.matching_ind      = 'Y'
       UNION
       SELECT fctd.tax_code vat_code
            ,nvl(fdtde.unit_tax_amt,0) unit_nf_tax_amt
            ,nvl(fdtde.unit_rec_value,0) unit_nf_rec_value
       FROM fm_correction_doc fcd
           ,fm_correction_tax_doc fctd
           ,fm_fiscal_doc_tax_detail_ext fdtde
           ,fm_tax_codes ftc
     WHERE fcd.seq_no              = fctd.header_seq_no
       AND fcd.fiscal_doc_line_id  = fdtde.fiscal_doc_line_id
       AND fctd.tax_code           = fdtde.tax_code
       AND fcd.fiscal_doc_id       = I_fiscal_doc_id
       AND fcd.fiscal_doc_line_id  = I_fiscal_doc_line_id
       AND fcd.action_type         = 'CLT'
       AND ftc.tax_code            = fdtde.tax_code
       AND ftc.matching_ind        = 'N';

   cursor C_GET_RECEIVED_QTY is
      SELECT distinct fdtdw.rcvd_qty
        FROM fm_fiscal_doc_tax_detail_wac fdtdw,
             fm_fiscal_doc_detail fdd
       WHERE fdtdw.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         AND fdd.fiscal_doc_line_id   = I_fiscal_doc_line_id;

BEGIN
   L_cost := NULL;
   L_qty  := NULL;

   SQL_LIB.SET_MARK('OPEN','C_GET_CORRECTION_DETAILS', 'FM_CORRECTION_DOC', L_key);
   FOR rec_get_correction_details in c_get_correction_details
   LOOP
      L_cor_exists := TRUE;
      if rec_get_correction_details.action_type = 'CLC' then
         L_cost := rec_get_correction_details.action_value;
      elsif rec_get_correction_details.action_type = 'CLF' then
         L_corr_freight := rec_get_correction_details.action_value;
      elsif rec_get_correction_details.action_type = 'CLI' then
         L_corr_insur := rec_get_correction_details.action_value;
      elsif rec_get_correction_details.action_type = 'CLO' then
         L_corr_othr := rec_get_correction_details.action_value;
      elsif rec_get_correction_details.action_type = 'CLQ' then
         L_qty := rec_get_correction_details.action_value;
      end if;
    END LOOP;  

   if (I_freight_cost is not null and I_freight_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                ,I_tran_code          => 117
                                                ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                ,I_vat_code           => NULL
                                                ,O_ref_no_1           => L_ref_no_1
                                                ,O_ref_no_2           => L_ref_no_2
                                                ,O_ref_no_3           => L_ref_no_3
                                                ,O_ref_no_4           => L_ref_no_4
                                                ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
   
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'F',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if; 

      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 117
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => ((I_freight_cost * I_quantity) - L_corr_freight)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if;

   if (I_insurance_cost is not null and I_insurance_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                               ,I_tran_code          => 117
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
   
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'I',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if; 

      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 117
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => ((I_insurance_cost * I_quantity) - L_corr_insur)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if;

   if (I_other_expenses_cost is not null and I_other_expenses_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                              ,I_tran_code          => 117
                                              ,I_fiscal_doc_id      => I_fiscal_doc_id
                                              ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                              ,I_vat_code           => NULL
                                              ,O_ref_no_1           => L_ref_no_1
                                              ,O_ref_no_2           => L_ref_no_2
                                              ,O_ref_no_3           => L_ref_no_3
                                              ,O_ref_no_4           => L_ref_no_4
                                              ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
   
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'O',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if; 

      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 117
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => ((I_other_expenses_cost * I_quantity) - L_corr_othr)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if;

   if L_cost is null then
      L_cost := nvl(I_unit_cost,0);
   end if;
--
   if L_qty is null then
      L_qty := nvl(I_quantity,0);
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_RECEIVED_QTY', 'fm_fiscal_doc_tax_detail_wac/fm_fiscal_doc_detail', 'fiscal_doc_line_id = '|| I_fiscal_doc_line_id);
   OPEN C_GET_RECEIVED_QTY;
   SQL_LIB.SET_MARK('FETCH','C_GET_RECEIVED_QTY', 'fm_fiscal_doc_tax_detail_wac/fm_fiscal_doc_detail', 'fiscal_doc_line_id = '|| I_fiscal_doc_line_id);
   FETCH C_GET_RECEIVED_QTY INTO L_received_qty;
   SQL_LIB.SET_MARK('CLOSE','C_GET_RECEIVED_QTY', 'fm_fiscal_doc_tax_detail_wac/fm_fiscal_doc_detail', 'fiscal_doc_line_id = '|| I_fiscal_doc_line_id);
   CLOSE C_GET_RECEIVED_QTY;

   if L_cor_exists then  
      
      SQL_LIB.SET_MARK('OPEN','C_GET_SUM_TAX', 'fm_fiscal_doc_tax_detail', L_key);
      OPEN  C_GET_SUM_TAX;
      SQL_LIB.SET_MARK('FETCH','C_GET_SUM_TAX', 'fm_fiscal_doc_tax_detail', L_key);
      FETCH C_GET_SUM_TAX into L_sum_tax;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SUM_TAX', 'fm_fiscal_doc_tax_detail', L_key);
      CLOSE C_GET_SUM_TAX;
--

      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message,
                                          O_base_cost          => L_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => I_fiscal_doc_line_id ) = FALSE then
         return FALSE;
      end if;
--
      
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 114
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
--
      if (L_cost = I_unit_cost) then
        L_corr_base_cost := L_base_cost;
      elsif L_qty = 0 then
        L_corr_base_cost :=L_cost;
      else
        L_corr_base_cost := (L_cost - (nvl(L_sum_tax,0) / L_qty));
      end if;

      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 114
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * L_base_cost)+ ((L_received_qty) * (L_base_cost-L_corr_base_cost)))
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if; 
--

   if (L_qty = 0) then
   
       FOR rec_get_tax_corec_dtls_zero in C_GET_TAX_COREC_DTLS_ZERO
       LOOP
    --
          if rec_get_tax_corec_dtls_zero.unit_nf_rec_value = 0 then
             if L_corr_base_cost = 0 then
               L_corr_tax_amt := rec_get_tax_corec_dtls_zero.unit_nf_tax_amt;
             else
              L_corr_tax_amt := 0;
             end if;
    --
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 115
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => i_fiscal_doc_line_id
                                                      ,I_vat_code           => rec_get_tax_corec_dtls_zero.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
    --
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 115
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * rec_get_tax_corec_dtls_zero.unit_nf_tax_amt) + ((L_received_qty) * (rec_get_tax_corec_dtls_zero.unit_nf_tax_amt - 0)))
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
    --
          elsif rec_get_tax_corec_dtls_zero.unit_nf_rec_value > 0 and (rec_get_tax_corec_dtls_zero.unit_nf_tax_amt - rec_get_tax_corec_dtls_zero.unit_nf_rec_value) = 0 then
           
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                        ,I_tran_code          => 116
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => rec_get_tax_corec_dtls_zero.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
    --
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 116 
                                                          ,I_fiscal_doc_id => I_fiscal_doc_id
                                                          ,I_fiscal_doc_no => I_fiscal_doc_no
                                                          ,I_supplier      => I_supplier
                                                          ,I_dept          => I_dept
                                                          ,I_class         => I_class
                                                          ,I_subclass      => I_subclass
                                                          ,I_item          => I_item
                                                          ,I_location_type => I_location_type
                                                          ,I_location      => I_location_id
                                                          ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * rec_get_tax_corec_dtls_zero.unit_nf_rec_value) + (L_received_qty) * (rec_get_tax_corec_dtls_zero.unit_nf_rec_value - 0))
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
          else 
             
              if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                       ,I_tran_code          => 115 
                                                       ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                       ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                       ,I_vat_code           => rec_get_tax_corec_dtls_zero.vat_code
                                                       ,O_ref_no_1           => L_ref_no_1
                                                       ,O_ref_no_2           => L_ref_no_2
                                                       ,O_ref_no_3           => L_ref_no_3
                                                       ,O_ref_no_4           => L_ref_no_4
                                                       ,O_ref_no_5           => L_ref_no_5) = FALSE then
                 return FALSE;
              end if;
    --
              if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                         ,I_tran_code     => 115 
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => I_fiscal_doc_no
                                                         ,I_supplier      => I_supplier
                                                         ,I_dept          => I_dept
                                                         ,I_class         => I_class
                                                         ,I_subclass      => I_subclass
                                                         ,I_item          => I_item
                                                         ,I_location_type => I_location_type
                                                         ,I_location      => I_location_id
                                                         ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * (rec_get_tax_corec_dtls_zero.unit_nf_tax_amt - rec_get_tax_corec_dtls_zero.unit_nf_rec_value)) + (L_received_qty)*((rec_get_tax_corec_dtls_zero.unit_nf_tax_amt - rec_get_tax_corec_dtls_zero.unit_nf_rec_value) - 0))
                                                         ,I_ref_no_1      => L_ref_no_1
                                                         ,I_ref_no_2      => L_ref_no_2
                                                         ,I_ref_no_3      => L_ref_no_3
                                                         ,I_ref_no_4      => L_ref_no_4
                                                         ,I_ref_no_5      => L_ref_no_5
                                                         ,I_tran_date     => SYSDATE)  = FALSE then
                 return FALSE;
              end if;
              
           
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 116 
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => rec_get_tax_corec_dtls_zero.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
    --
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 116 
                                                          ,I_fiscal_doc_id => I_fiscal_doc_id
                                                          ,I_fiscal_doc_no => I_fiscal_doc_no
                                                          ,I_supplier      => I_supplier
                                                          ,I_dept          => I_dept
                                                          ,I_class         => I_class
                                                          ,I_subclass      => I_subclass
                                                          ,I_item          => I_item
                                                          ,I_location_type => I_location_type
                                                          ,I_location      => I_location_id
                                                          ,I_total_cost    => (((nvl(I_quantity,0)- L_qty)* (rec_get_tax_corec_dtls_zero.unit_nf_rec_value))+ ((L_received_qty) * (rec_get_tax_corec_dtls_zero.unit_nf_rec_value - 0)))
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
          end if;
    --
       END LOOP; 
   
   else
   
       FOR rec_get_tax_corec_details in C_GET_TAX_COREC_DETAILS(L_qty)
       LOOP
    --
          if rec_get_tax_corec_details.unit_nf_rec_value = 0 then
             
              
             if L_corr_base_cost = 0 then
               L_corr_tax_amt := rec_get_tax_corec_details.unit_nf_tax_amt;
             else
              L_corr_tax_amt := rec_get_tax_corec_details.unit_corr_tax_amt;
             end if;
    --
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 115
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => i_fiscal_doc_line_id
                                                      ,I_vat_code           => rec_get_tax_corec_details.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
    --
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 115
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * rec_get_tax_corec_details.unit_nf_tax_amt) + ((L_received_qty) * (rec_get_tax_corec_details.unit_nf_tax_amt - L_corr_tax_amt)))
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
    --
          elsif rec_get_tax_corec_details.unit_nf_rec_value > 0 and (rec_get_tax_corec_details.unit_nf_tax_amt - rec_get_tax_corec_details.unit_nf_rec_value) = 0 then
           
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                        ,I_tran_code          => 116
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => rec_get_tax_corec_details.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
    --
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 116 
                                                          ,I_fiscal_doc_id => I_fiscal_doc_id
                                                          ,I_fiscal_doc_no => I_fiscal_doc_no
                                                          ,I_supplier      => I_supplier
                                                          ,I_dept          => I_dept
                                                          ,I_class         => I_class
                                                          ,I_subclass      => I_subclass
                                                          ,I_item          => I_item
                                                          ,I_location_type => I_location_type
                                                          ,I_location      => I_location_id
                                                          ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * rec_get_tax_corec_details.unit_nf_rec_value) + (L_received_qty) * (rec_get_tax_corec_details.unit_nf_rec_value - rec_get_tax_corec_details.unit_rec_corr_tax_amt))
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
          else 
             
              if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                       ,I_tran_code          => 115 
                                                       ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                       ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                       ,I_vat_code           => rec_get_tax_corec_details.vat_code
                                                       ,O_ref_no_1           => L_ref_no_1
                                                       ,O_ref_no_2           => L_ref_no_2
                                                       ,O_ref_no_3           => L_ref_no_3
                                                       ,O_ref_no_4           => L_ref_no_4
                                                       ,O_ref_no_5           => L_ref_no_5) = FALSE then
                 return FALSE;
              end if;
    --
              if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                         ,I_tran_code     => 115 
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => I_fiscal_doc_no
                                                         ,I_supplier      => I_supplier
                                                         ,I_dept          => I_dept
                                                         ,I_class         => I_class
                                                         ,I_subclass      => I_subclass
                                                         ,I_item          => I_item
                                                         ,I_location_type => I_location_type
                                                         ,I_location      => I_location_id
                                                         ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * (rec_get_tax_corec_details.unit_nf_tax_amt - rec_get_tax_corec_details.unit_nf_rec_value)) + (L_received_qty)*((rec_get_tax_corec_details.unit_nf_tax_amt - rec_get_tax_corec_details.unit_nf_rec_value) - (rec_get_tax_corec_details.unit_corr_tax_amt - rec_get_tax_corec_details.unit_rec_corr_tax_amt)))
                                                         ,I_ref_no_1      => L_ref_no_1
                                                         ,I_ref_no_2      => L_ref_no_2
                                                         ,I_ref_no_3      => L_ref_no_3
                                                         ,I_ref_no_4      => L_ref_no_4
                                                         ,I_ref_no_5      => L_ref_no_5
                                                         ,I_tran_date     => SYSDATE)  = FALSE then
                 return FALSE;
              end if;
              
           
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 116 
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => rec_get_tax_corec_details.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
    --
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 116 
                                                          ,I_fiscal_doc_id => I_fiscal_doc_id
                                                          ,I_fiscal_doc_no => I_fiscal_doc_no
                                                          ,I_supplier      => I_supplier
                                                          ,I_dept          => I_dept
                                                          ,I_class         => I_class
                                                          ,I_subclass      => I_subclass
                                                          ,I_item          => I_item
                                                          ,I_location_type => I_location_type
                                                          ,I_location      => I_location_id
                                                          ,I_total_cost    => (((nvl(I_quantity,0)- L_qty)* (rec_get_tax_corec_details.unit_nf_rec_value))+ ((L_received_qty) * (rec_get_tax_corec_details.unit_nf_rec_value - rec_get_tax_corec_details.unit_rec_corr_tax_amt)))
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
          end if;
    --
       END LOOP; 
   end if;    
--
   return TRUE;
--
EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      
      return FALSE;
--
END POST_TRANDATA_CORRECTION;
--

----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_CORR_TRAN
-- Purpose:       This function Process Tran Data Posting for Correction Letter
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_CORR_TRAN( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                  ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                                  ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                                  ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                                  ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                                  ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                                  ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                                  ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                                  ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                                  ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                                  ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                                  ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                                  ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                                  ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                                  ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                                  ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                                  ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                                  ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                                  ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                                  ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                                  ,I_comp_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                  ,I_compl_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) 
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.POST_TRANDATA_CORRECTION';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '|| I_fiscal_doc_id;
   L_cost                FM_TRAN_DATA.TOTAL_COST%TYPE;
   L_qty                 FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   
   L_received_qty        FM_RECEIVING_DETAIL.QUANTITY%TYPE :=0;
   L_sum_tax             FM_CORRECTION_TAX_DOC.TAX_AMOUNT%TYPE:=0;
   L_cor_exists          BOOLEAN := FALSE;
   L_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_corr_base_cost      FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_corr_tax_amt        FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE;
   
   L_corr_freight        FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;  
   L_corr_insur          FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
   L_corr_othr           FM_FISCAL_DOC_DETAIL.UNIT_COST%TYPE;
--
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
--
   cursor C_GET_CORRECTION_DETAILS is
      SELECT fcd.action_value
            ,fcd.action_type
            ,fdd.unit_cost
            ,fdd.quantity
            ,fcd.seq_no
      FROM fm_correction_doc fcd
          ,fm_fiscal_doc_detail fdd
      WHERE fcd.fiscal_doc_id = fdd.fiscal_doc_id
        AND fcd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
        AND fcd.fiscal_doc_id      = I_fiscal_doc_id
        AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
        AND action_type            in ('CLQ','CLC','CLF','CLI','CLO') 
        UNION
        SELECT fcd.action_value
                    ,fcd.action_type
                    ,fdd.unit_cost
                    ,fdd.quantity
                    ,fcd.seq_no
              FROM fm_correction_doc fcd
                  ,fm_fiscal_doc_detail fdd
              WHERE fcd.fiscal_doc_id = fdd.fiscal_doc_id
                AND fcd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
                AND fcd.fiscal_doc_id      = I_comp_fiscal_doc_id
                AND fcd.fiscal_doc_line_id = I_compl_fiscal_doc_line_id
        AND action_type            in ('CLQ','CLC','CLF','CLI','CLO') ;
--
   cursor C_GET_SUM_TAX is
    
    SELECT nvl(SUM(tax_amount),0)
    FROM 
    (
      SELECT nvl(fctd.tax_amount,0) tax_amount
      FROM fm_correction_doc fcd
          ,fm_correction_tax_doc fctd
          ,vat_codes vc
      WHERE fcd.seq_no             = fctd.header_seq_no
        AND fcd.fiscal_doc_id      = I_fiscal_doc_id
        AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
        AND action_type            = 'CLT'
        AND vc.vat_code            = fctd.tax_code
        AND vc.incl_nic_ind        = 'Y'
        UNION
        SELECT nvl(fctd.tax_amount,0) tax_amount
              FROM fm_correction_doc fcd
                  ,fm_correction_tax_doc fctd
                  ,vat_codes vc
              WHERE fcd.seq_no             = fctd.header_seq_no
                AND fcd.fiscal_doc_id      = I_comp_fiscal_doc_id
                AND fcd.fiscal_doc_line_id = I_compl_fiscal_doc_line_id
                AND action_type            = 'CLT'
                AND vc.vat_code            = fctd.tax_code
        AND vc.incl_nic_ind        = 'Y');
--
   cursor C_GET_TAX_COREC_DETAILS(I_qty FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE)  is
      SELECT fctd.tax_code vat_code
            ,(fctd.tax_amount / I_qty ) unit_corr_tax_amt
            ,((fdtd.unit_rec_value / fdtd.unit_tax_amt) * (fctd.tax_amount / I_qty )) unit_rec_corr_tax_amt
            ,nvl(fdtd.unit_tax_amt,0) unit_nf_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_nf_rec_value
      FROM fm_correction_doc fcd
          ,fm_correction_tax_doc fctd
          ,fm_fiscal_doc_tax_detail fdtd
          ,fm_tax_codes ftc
      WHERE fcd.seq_no             = fctd.header_seq_no
       AND fcd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
       AND fctd.tax_code          = fdtd.vat_code
       AND fcd.fiscal_doc_id      = I_fiscal_doc_id
       AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
       AND action_type            = 'CLT'
       AND ftc.tax_code          = fdtd.vat_code
       AND ftc.matching_ind      = 'Y'
       UNION
       SELECT fctd.tax_code vat_code
            ,(fctd.tax_amount / I_qty ) unit_corr_tax_amt
            ,((fdtde.unit_rec_value / fdtde.unit_tax_amt) * (fctd.tax_amount / I_qty )) unit_rec_corr_tax_amt
            ,nvl(fdtde.unit_tax_amt,0) unit_nf_tax_amt
            ,nvl(fdtde.unit_rec_value,0) unit_nf_rec_value
       FROM fm_correction_doc fcd
           ,fm_correction_tax_doc fctd
           ,fm_fiscal_doc_tax_detail_ext fdtde
           ,fm_tax_codes ftc
     WHERE fcd.seq_no              = fctd.header_seq_no
       AND fcd.fiscal_doc_line_id  = fdtde.fiscal_doc_line_id
       AND fctd.tax_code           = fdtde.tax_code
       AND fcd.fiscal_doc_id       = I_fiscal_doc_id
       AND fcd.fiscal_doc_line_id  = I_fiscal_doc_line_id
       AND fcd.action_type         = 'CLT'
       AND ftc.tax_code            = fdtde.tax_code
       AND ftc.matching_ind        = 'N'
       UNION
        SELECT fctd.tax_code vat_code
                   ,(fctd.tax_amount / I_qty ) unit_corr_tax_amt
                   ,((fdtd.unit_rec_value / fdtd.unit_tax_amt) * (fctd.tax_amount / I_qty )) unit_rec_corr_tax_amt
                   ,nvl(fdtd.unit_tax_amt,0) unit_nf_tax_amt
                   ,nvl(fdtd.unit_rec_value,0) unit_nf_rec_value
             FROM fm_correction_doc fcd
                 ,fm_correction_tax_doc fctd
                 ,fm_fiscal_doc_tax_detail fdtd
                 ,fm_tax_codes ftc
             WHERE fcd.seq_no             = fctd.header_seq_no
              AND fcd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
              AND fctd.tax_code          = fdtd.vat_code
              AND fcd.fiscal_doc_id      = I_comp_fiscal_doc_id
              AND fcd.fiscal_doc_line_id = I_compl_fiscal_doc_line_id
              AND action_type            = 'CLT'
              AND ftc.tax_code          = fdtd.vat_code
       AND ftc.matching_ind      = 'Y';
       
       cursor C_GET_TAX_COREC_DTLS_ZERO  is
      SELECT fctd.tax_code vat_code
           ,nvl(fdtd.unit_tax_amt,0) unit_nf_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_nf_rec_value
      FROM fm_correction_doc fcd
          ,fm_correction_tax_doc fctd
          ,fm_fiscal_doc_tax_detail fdtd
          ,fm_tax_codes ftc
      WHERE fcd.seq_no             = fctd.header_seq_no
       AND fcd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
       AND fctd.tax_code          = fdtd.vat_code
       AND fcd.fiscal_doc_id      = I_fiscal_doc_id
       AND fcd.fiscal_doc_line_id = I_fiscal_doc_line_id
       AND action_type            = 'CLT'
       AND ftc.tax_code          = fdtd.vat_code
       AND ftc.matching_ind      = 'Y'
       UNION
       SELECT fctd.tax_code vat_code
            ,nvl(fdtde.unit_tax_amt,0) unit_nf_tax_amt
            ,nvl(fdtde.unit_rec_value,0) unit_nf_rec_value
       FROM fm_correction_doc fcd
           ,fm_correction_tax_doc fctd
           ,fm_fiscal_doc_tax_detail_ext fdtde
           ,fm_tax_codes ftc
     WHERE fcd.seq_no              = fctd.header_seq_no
       AND fcd.fiscal_doc_line_id  = fdtde.fiscal_doc_line_id
       AND fctd.tax_code           = fdtde.tax_code
       AND fcd.fiscal_doc_id       = I_fiscal_doc_id
       AND fcd.fiscal_doc_line_id  = I_fiscal_doc_line_id
       AND fcd.action_type         = 'CLT'
       AND ftc.tax_code            = fdtde.tax_code
       AND ftc.matching_ind        = 'N'
       UNION
       SELECT fctd.tax_code vat_code
                  ,nvl(fdtd.unit_tax_amt,0) unit_nf_tax_amt
                   ,nvl(fdtd.unit_rec_value,0) unit_nf_rec_value
             FROM fm_correction_doc fcd
                 ,fm_correction_tax_doc fctd
                 ,fm_fiscal_doc_tax_detail fdtd
                 ,fm_tax_codes ftc
             WHERE fcd.seq_no             = fctd.header_seq_no
              AND fcd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
              AND fctd.tax_code          = fdtd.vat_code
              AND fcd.fiscal_doc_id      = I_comp_fiscal_doc_id
              AND fcd.fiscal_doc_line_id = I_compl_fiscal_doc_line_id
              AND action_type            = 'CLT'
              AND ftc.tax_code          = fdtd.vat_code
       AND ftc.matching_ind      = 'Y';
--
   cursor C_GET_RECEIVED_QTY is
      SELECT distinct fdtdw.rcvd_qty
        FROM fm_fiscal_doc_tax_detail_wac fdtdw,
             fm_fiscal_doc_detail fdd
       WHERE fdtdw.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         AND fdd.fiscal_doc_line_id   = I_fiscal_doc_line_id;

BEGIN
   L_cost := NULL;
   L_qty  := NULL;
--

   SQL_LIB.SET_MARK('OPEN','C_GET_CORRECTION_DETAILS', 'FM_CORRECTION_DOC', L_key);
   FOR rec_get_correction_details in C_GET_CORRECTION_DETAILS
    LOOP
      L_cor_exists := TRUE;
      if rec_get_correction_details.action_type = 'CLC' then
         L_cost := rec_get_correction_details.action_value;
      elsif rec_get_correction_details.action_type = 'CLF' then
         L_corr_freight := rec_get_correction_details.action_value;
      elsif rec_get_correction_details.action_type = 'CLI' then
         L_corr_insur := rec_get_correction_details.action_value;
      elsif rec_get_correction_details.action_type = 'CLO' then
         L_corr_othr := rec_get_correction_details.action_value;
      elsif rec_get_correction_details.action_type = 'CLQ' then
         L_qty := rec_get_correction_details.action_value;
      end if;
    END LOOP;  

   if (I_freight_cost is not null and I_freight_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                ,I_tran_code          => 117
                                                ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                ,I_vat_code           => NULL
                                                ,O_ref_no_1           => L_ref_no_1
                                                ,O_ref_no_2           => L_ref_no_2
                                                ,O_ref_no_3           => L_ref_no_3
                                                ,O_ref_no_4           => L_ref_no_4
                                                ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
   --
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'F',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if; 
--
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 117
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => ((I_freight_cost * I_quantity) - L_corr_freight)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if;
--
   if (I_insurance_cost is not null and I_insurance_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                               ,I_tran_code          => 117
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
   --
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'I',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if;  
--
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 117
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => ((I_insurance_cost * I_quantity) - L_corr_insur)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if;
--
   if (I_other_expenses_cost is not null and I_other_expenses_cost > 0) then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                              ,I_tran_code          => 117
                                              ,I_fiscal_doc_id      => I_fiscal_doc_id
                                              ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                              ,I_vat_code           => NULL
                                              ,O_ref_no_1           => L_ref_no_1
                                              ,O_ref_no_2           => L_ref_no_2
                                              ,O_ref_no_3           => L_ref_no_3
                                              ,O_ref_no_4           => L_ref_no_4
                                              ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
   --
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FMCC',
                                    'O',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;   

      if L_ref_no_1 is not null and L_ref_no_1 = 'CC' then
         L_ref_no_1 := L_code_desc;
      elsif L_ref_no_2 is not null and L_ref_no_2 = 'CC' then
         L_ref_no_2 := L_code_desc;
      elsif L_ref_no_3 is not null and L_ref_no_3 = 'CC' then
         L_ref_no_3 := L_code_desc;
      elsif L_ref_no_4 is not null and L_ref_no_4 = 'CC' then
         L_ref_no_4 := L_code_desc;
      elsif L_ref_no_5 is not null and L_ref_no_5 = 'CC' then
         L_ref_no_5 := L_code_desc;
      end if; 
--
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 117
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => ((I_other_expenses_cost * I_quantity) - L_corr_othr)
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if;
--

   if L_cost is null then
      L_cost := nvl(I_unit_cost,0);
   end if;
--
   if L_qty is null then
      L_qty := nvl(I_quantity,0);
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_RECEIVED_QTY', 'fm_fiscal_doc_tax_detail_wac/fm_fiscal_doc_detail', 'fiscal_doc_line_id = '|| I_fiscal_doc_line_id);
   OPEN C_GET_RECEIVED_QTY;
   SQL_LIB.SET_MARK('FETCH','C_GET_RECEIVED_QTY', 'fm_fiscal_doc_tax_detail_wac/fm_fiscal_doc_detail', 'fiscal_doc_line_id = '|| I_fiscal_doc_line_id);
   FETCH C_GET_RECEIVED_QTY INTO L_received_qty;
   SQL_LIB.SET_MARK('CLOSE','C_GET_RECEIVED_QTY', 'fm_fiscal_doc_tax_detail_wac/fm_fiscal_doc_detail', 'fiscal_doc_line_id = '|| I_fiscal_doc_line_id);
   CLOSE c_get_received_qty;
   
   

   if L_cor_exists then  
      
      SQL_LIB.SET_MARK('OPEN','c_get_sum_tax', 'fm_fiscal_doc_tax_detail', L_key);
      OPEN  C_GET_SUM_TAX;
      SQL_LIB.SET_MARK('FETCH','c_get_sum_tax', 'fm_fiscal_doc_tax_detail', L_key);
      FETCH C_GET_SUM_TAX into L_sum_tax;
      SQL_LIB.SET_MARK('CLOSE','c_get_sum_tax', 'fm_fiscal_doc_tax_detail', L_key);
      CLOSE C_GET_SUM_TAX;
      
--

      if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message,
                                          O_base_cost          => L_base_cost,
                                          I_nic_cost           => I_unit_cost,
                                          I_nic_cost_factor    => I_unit_cost,
                                          I_fiscal_doc_line_id => I_fiscal_doc_line_id ) = FALSE then
         return FALSE;
      end if;
--
      
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 114
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
--
      if (L_cost = I_unit_cost) then
        L_corr_base_cost := L_base_cost;
      elsif L_qty = 0 then
        L_corr_base_cost :=L_cost;
      else
        L_corr_base_cost := (L_cost - (nvl(L_sum_tax,0) / L_qty));
      end if;

      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 114
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * L_base_cost)+ ((L_received_qty) * (L_base_cost-L_corr_base_cost)))
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
   end if; 
--

   if (L_qty = 0) then
   
       FOR rec_get_tax_corec_dtls_zero in C_GET_TAX_COREC_DTLS_ZERO
       LOOP
    --
          if rec_get_tax_corec_dtls_zero.unit_nf_rec_value = 0 then
             
              
             if L_corr_base_cost = 0 then
               L_corr_tax_amt := rec_get_tax_corec_dtls_zero.unit_nf_tax_amt;
             else
              L_corr_tax_amt := 0;
             end if;
    --
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 115
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => i_fiscal_doc_line_id
                                                      ,I_vat_code           => rec_get_tax_corec_dtls_zero.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
    --
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 115
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * rec_get_tax_corec_dtls_zero.unit_nf_tax_amt) + ((L_received_qty) * (rec_get_tax_corec_dtls_zero.unit_nf_tax_amt - 0)))
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
    --
          elsif rec_get_tax_corec_dtls_zero.unit_nf_rec_value > 0 and (rec_get_tax_corec_dtls_zero.unit_nf_tax_amt - rec_get_tax_corec_dtls_zero.unit_nf_rec_value) = 0 then
           
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                        ,I_tran_code          => 116
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => rec_get_tax_corec_dtls_zero.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
    --
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 116 
                                                          ,I_fiscal_doc_id => I_fiscal_doc_id
                                                          ,I_fiscal_doc_no => I_fiscal_doc_no
                                                          ,I_supplier      => I_supplier
                                                          ,I_dept          => I_dept
                                                          ,I_class         => I_class
                                                          ,I_subclass      => I_subclass
                                                          ,I_item          => I_item
                                                          ,I_location_type => I_location_type
                                                          ,I_location      => I_location_id
                                                          ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * rec_get_tax_corec_dtls_zero.unit_nf_rec_value) + (L_received_qty) * (rec_get_tax_corec_dtls_zero.unit_nf_rec_value - 0))
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
          else 
             
              if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                       ,I_tran_code          => 115 
                                                       ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                       ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                       ,I_vat_code           => rec_get_tax_corec_dtls_zero.vat_code
                                                       ,O_ref_no_1           => L_ref_no_1
                                                       ,O_ref_no_2           => L_ref_no_2
                                                       ,O_ref_no_3           => L_ref_no_3
                                                       ,O_ref_no_4           => L_ref_no_4
                                                       ,O_ref_no_5           => L_ref_no_5) = FALSE then
                 return FALSE;
              end if;
    --
              if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                         ,I_tran_code     => 115 
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => I_fiscal_doc_no
                                                         ,I_supplier      => I_supplier
                                                         ,I_dept          => I_dept
                                                         ,I_class         => I_class
                                                         ,I_subclass      => I_subclass
                                                         ,I_item          => I_item
                                                         ,I_location_type => I_location_type
                                                         ,I_location      => I_location_id
                                                         ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * (rec_get_tax_corec_dtls_zero.unit_nf_tax_amt - rec_get_tax_corec_dtls_zero.unit_nf_rec_value)) + (L_received_qty)*((rec_get_tax_corec_dtls_zero.unit_nf_tax_amt - rec_get_tax_corec_dtls_zero.unit_nf_rec_value) - 0))
                                                         ,I_ref_no_1      => L_ref_no_1
                                                         ,I_ref_no_2      => L_ref_no_2
                                                         ,I_ref_no_3      => L_ref_no_3
                                                         ,I_ref_no_4      => L_ref_no_4
                                                         ,I_ref_no_5      => L_ref_no_5
                                                         ,I_tran_date     => SYSDATE)  = FALSE then
                 return FALSE;
              end if;
              
           
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 116 
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => rec_get_tax_corec_dtls_zero.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
    --
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 116 
                                                          ,I_fiscal_doc_id => I_fiscal_doc_id
                                                          ,I_fiscal_doc_no => I_fiscal_doc_no
                                                          ,I_supplier      => I_supplier
                                                          ,I_dept          => I_dept
                                                          ,I_class         => I_class
                                                          ,I_subclass      => I_subclass
                                                          ,I_item          => I_item
                                                          ,I_location_type => I_location_type
                                                          ,I_location      => I_location_id
                                                          ,I_total_cost    => (((nvl(I_quantity,0)- L_qty)* (rec_get_tax_corec_dtls_zero.unit_nf_rec_value))+ ((L_received_qty) * (rec_get_tax_corec_dtls_zero.unit_nf_rec_value - 0)))
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
          end if;
    --
       END LOOP; 
   
   else
   
       FOR rec_get_tax_corec_details in C_GET_TAX_COREC_DETAILS(L_qty)
       LOOP
    --
          if rec_get_tax_corec_details.unit_nf_rec_value = 0 then
             
              
             if L_corr_base_cost = 0 then
               L_corr_tax_amt := rec_get_tax_corec_details.unit_nf_tax_amt;
             else
              L_corr_tax_amt := rec_get_tax_corec_details.unit_corr_tax_amt;
             end if;
    --
             if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                      ,I_tran_code          => 115
                                                      ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                      ,I_fiscal_doc_line_id => i_fiscal_doc_line_id
                                                      ,I_vat_code           => rec_get_tax_corec_details.vat_code
                                                      ,O_ref_no_1           => L_ref_no_1
                                                      ,O_ref_no_2           => L_ref_no_2
                                                      ,O_ref_no_3           => L_ref_no_3
                                                      ,O_ref_no_4           => L_ref_no_4
                                                      ,O_ref_no_5           => L_ref_no_5) = FALSE then
                return FALSE;
             end if;
    --
             if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                        ,I_tran_code     => 115
                                                        ,I_fiscal_doc_id => I_fiscal_doc_id
                                                        ,I_fiscal_doc_no => I_fiscal_doc_no
                                                        ,I_supplier      => I_supplier
                                                        ,I_dept          => I_dept
                                                        ,I_class         => I_class
                                                        ,I_subclass      => I_subclass
                                                        ,I_item          => I_item
                                                        ,I_location_type => I_location_type
                                                        ,I_location      => I_location_id
                                                        ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * rec_get_tax_corec_details.unit_nf_tax_amt) + ((L_received_qty) * (rec_get_tax_corec_details.unit_nf_tax_amt - L_corr_tax_amt)))
                                                        ,I_ref_no_1      => L_ref_no_1
                                                        ,I_ref_no_2      => L_ref_no_2
                                                        ,I_ref_no_3      => L_ref_no_3
                                                        ,I_ref_no_4      => L_ref_no_4
                                                        ,I_ref_no_5      => L_ref_no_5
                                                        ,I_tran_date     => SYSDATE)  = FALSE then
                return FALSE;
             end if;
    --
          elsif rec_get_tax_corec_details.unit_nf_rec_value > 0 and (rec_get_tax_corec_details.unit_nf_tax_amt - rec_get_tax_corec_details.unit_nf_rec_value) = 0 then
           
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                        ,I_tran_code          => 116
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => rec_get_tax_corec_details.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
    --
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 116 
                                                          ,I_fiscal_doc_id => I_fiscal_doc_id
                                                          ,I_fiscal_doc_no => I_fiscal_doc_no
                                                          ,I_supplier      => I_supplier
                                                          ,I_dept          => I_dept
                                                          ,I_class         => I_class
                                                          ,I_subclass      => I_subclass
                                                          ,I_item          => I_item
                                                          ,I_location_type => I_location_type
                                                          ,I_location      => I_location_id
                                                          ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * rec_get_tax_corec_details.unit_nf_rec_value) + (L_received_qty) * (rec_get_tax_corec_details.unit_nf_rec_value - rec_get_tax_corec_details.unit_rec_corr_tax_amt))
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
          else 
             
              if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                       ,I_tran_code          => 115 
                                                       ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                       ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                       ,I_vat_code           => rec_get_tax_corec_details.vat_code
                                                       ,O_ref_no_1           => L_ref_no_1
                                                       ,O_ref_no_2           => L_ref_no_2
                                                       ,O_ref_no_3           => L_ref_no_3
                                                       ,O_ref_no_4           => L_ref_no_4
                                                       ,O_ref_no_5           => L_ref_no_5) = FALSE then
                 return FALSE;
              end if;
    --
              if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                         ,I_tran_code     => 115 
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => I_fiscal_doc_no
                                                         ,I_supplier      => I_supplier
                                                         ,I_dept          => I_dept
                                                         ,I_class         => I_class
                                                         ,I_subclass      => I_subclass
                                                         ,I_item          => I_item
                                                         ,I_location_type => I_location_type
                                                         ,I_location      => I_location_id
                                                         ,I_total_cost    => (((nvl(I_quantity,0)- L_qty) * (rec_get_tax_corec_details.unit_nf_tax_amt - rec_get_tax_corec_details.unit_nf_rec_value)) + (L_received_qty)*((rec_get_tax_corec_details.unit_nf_tax_amt - rec_get_tax_corec_details.unit_nf_rec_value) - (rec_get_tax_corec_details.unit_corr_tax_amt - rec_get_tax_corec_details.unit_rec_corr_tax_amt)))
                                                         ,I_ref_no_1      => L_ref_no_1
                                                         ,I_ref_no_2      => L_ref_no_2
                                                         ,I_ref_no_3      => L_ref_no_3
                                                         ,I_ref_no_4      => L_ref_no_4
                                                         ,I_ref_no_5      => L_ref_no_5
                                                         ,I_tran_date     => SYSDATE)  = FALSE then
                 return FALSE;
              end if;
              
           
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                        ,I_tran_code          => 116 
                                                        ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                        ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                        ,I_vat_code           => rec_get_tax_corec_details.vat_code
                                                        ,O_ref_no_1           => L_ref_no_1
                                                        ,O_ref_no_2           => L_ref_no_2
                                                        ,O_ref_no_3           => L_ref_no_3
                                                        ,O_ref_no_4           => L_ref_no_4
                                                        ,O_ref_no_5           => L_ref_no_5) = FALSE then
                  return FALSE;
               end if;
    --
               if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                          ,I_tran_code     => 116 
                                                          ,I_fiscal_doc_id => I_fiscal_doc_id
                                                          ,I_fiscal_doc_no => I_fiscal_doc_no
                                                          ,I_supplier      => I_supplier
                                                          ,I_dept          => I_dept
                                                          ,I_class         => I_class
                                                          ,I_subclass      => I_subclass
                                                          ,I_item          => I_item
                                                          ,I_location_type => I_location_type
                                                          ,I_location      => I_location_id
                                                          ,I_total_cost    => (((nvl(I_quantity,0)- L_qty)* (rec_get_tax_corec_details.unit_nf_rec_value))+ ((L_received_qty) * (rec_get_tax_corec_details.unit_nf_rec_value - rec_get_tax_corec_details.unit_rec_corr_tax_amt)))
                                                          ,I_ref_no_1      => L_ref_no_1
                                                          ,I_ref_no_2      => L_ref_no_2
                                                          ,I_ref_no_3      => L_ref_no_3
                                                          ,I_ref_no_4      => L_ref_no_4
                                                          ,I_ref_no_5      => L_ref_no_5
                                                          ,I_tran_date     => SYSDATE)  = FALSE then
                  return FALSE;
               end if;
          end if;
    --
       END LOOP; 
   end if;    
--
   return TRUE;
--
EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      
      return FALSE;
--
END POST_TRANDATA_CORR_TRAN;

----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_CAL_VARIANCE
-- Purpose:       This function Process Tran Data Posting for Correction Letter
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_CAL_VARIANCE( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                    ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                    ,I_variance             IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE) 
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.POST_TRANDATA_CAL_VARIANCE';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '|| I_fiscal_doc_id;
--
   L_tax_variance        NUMBER := 0;
   L_fiscal_doc_id       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_fiscal_doc_no       FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE;
   L_supplier            FM_TRAN_DATA.SUPPLIER%TYPE;
   L_location_type       FM_TRAN_DATA.LOCATION_TYPE%TYPE;
   L_location_id         FM_TRAN_DATA.LOCATION%TYPE;
--
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
--
   cursor C_TAX_HEAD 
   IS
      SELECT vat_code
            ,total_value
        FROM fm_fiscal_doc_tax_head 
       WHERE fiscal_doc_id = I_fiscal_doc_id;
--
   cursor C_TAX_DETAIL(I_vat_code IN FM_FISCAL_DOC_TAX_HEAD.VAT_CODE%TYPE) 
   IS
      SELECT sum(nvl(unit_tax_amt,0) * quantity) line_tax
            ,sum(nvl(unit_rec_value,0)* quantity) rec_line_tax
        FROM fm_fiscal_doc_tax_detail fdtd
            ,fm_fiscal_doc_detail fdd 
       WHERE fdtd.fiscal_doc_line_id = fdd.fiscal_doc_line_id
         AND fdd.fiscal_doc_id       = I_fiscal_doc_id
         AND fdtd.vat_code           = I_vat_code;
--
   cursor C_GET_TRAN_DATA 
   IS
      SELECT fdh.fiscal_doc_id
            ,fdh.fiscal_doc_no
            ,fdh.key_value_1 supplier
            ,fdh.location_type
            ,fdh.location_id
        FROM fm_fiscal_doc_header fdh           
       WHERE fdh.fiscal_doc_id   = I_fiscal_doc_id
         AND module              = 'SUPP';
BEGIN
--
   SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_DATA', 'fm_fiscal_doc_header', L_key);
   OPEN C_GET_TRAN_DATA;
   SQL_LIB.SET_MARK('FETCH','C_GET_TRAN_DATA', 'fm_fiscal_doc_header', L_key);
   FETCH C_GET_TRAN_DATA INTO L_fiscal_doc_id , L_fiscal_doc_no ,L_supplier ,L_location_type,L_location_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRAN_DATA', 'fm_fiscal_doc_header', L_key);
   CLOSE C_GET_TRAN_DATA;
--
   SQL_LIB.SET_MARK('OPEN','C_TAX_HEAD', 'fm_fiscal_doc_tax_head', L_key);
   FOR rec_tax_head in C_TAX_HEAD
   LOOP
--
      FOR rec_tax_detail IN C_TAX_DETAIL(rec_tax_head.vat_code)
      LOOP
         L_tax_variance := L_tax_variance + (rec_tax_head.total_value - rec_tax_detail.line_tax);
         
         if (rec_tax_head.total_value - rec_tax_detail.line_tax) <> 0 then
      --
            if rec_tax_detail.rec_line_tax = 0 then
               
                
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 109
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => NULL
                                                         ,I_vat_code           => rec_tax_head.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 109
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => L_fiscal_doc_no
                                                           ,I_supplier      => L_supplier
                                                           ,I_dept          => NULL
                                                           ,I_class         => NULL
                                                           ,I_subclass      => NULL
                                                           ,I_item          => NULL
                                                           ,I_location_type => L_location_type
                                                           ,I_location      => L_location_id
                                                           ,I_total_cost    => (rec_tax_head.total_value - rec_tax_detail.line_tax)
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
      --
            elsif rec_tax_detail.rec_line_tax > 0 and (rec_tax_detail.line_tax - rec_tax_detail.rec_line_tax) = 0 then
             
                  if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                          ,I_tran_code          => 108
                                                          ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                          ,I_fiscal_doc_line_id => NULL
                                                          ,I_vat_code           => rec_tax_head.vat_code
                                                          ,O_ref_no_1           => L_ref_no_1
                                                          ,O_ref_no_2           => L_ref_no_2
                                                          ,O_ref_no_3           => L_ref_no_3
                                                          ,O_ref_no_4           => L_ref_no_4
                                                          ,O_ref_no_5           => L_ref_no_5) = FALSE then
                    return FALSE;
                 end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 108
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => L_fiscal_doc_no
                                                           ,I_supplier      => L_supplier
                                                           ,I_dept          => NULL
                                                           ,I_class         => NULL
                                                           ,I_subclass      => NULL
                                                           ,I_item          => NULL
                                                           ,I_location_type => L_location_type
                                                           ,I_location      => L_location_id
                                                           ,I_total_cost    => (rec_tax_head.total_value - rec_tax_detail.line_tax)
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
            else 
               
                if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                         ,I_tran_code          => 109
                                                         ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                         ,I_fiscal_doc_line_id => NULL
                                                         ,I_vat_code           => rec_tax_head.vat_code
                                                         ,O_ref_no_1           => L_ref_no_1
                                                         ,O_ref_no_2           => L_ref_no_2
                                                         ,O_ref_no_3           => L_ref_no_3
                                                         ,O_ref_no_4           => L_ref_no_4
                                                         ,O_ref_no_5           => L_ref_no_5) = FALSE then
                   return FALSE;
                end if;
      --
                if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                           ,I_tran_code     => 109
                                                           ,I_fiscal_doc_id => I_fiscal_doc_id
                                                           ,I_fiscal_doc_no => L_fiscal_doc_no
                                                           ,I_supplier      => L_supplier
                                                           ,I_dept          => NULL
                                                           ,I_class         => NULL
                                                           ,I_subclass      => NULL
                                                           ,I_item          => NULL
                                                           ,I_location_type => L_location_type
                                                           ,I_location      => L_location_id
                                                           ,I_total_cost    => (rec_tax_head.total_value - rec_tax_detail.line_tax) *(1- (rec_tax_detail.rec_line_tax / rec_tax_detail.line_tax ))
                                                           ,I_ref_no_1      => L_ref_no_1
                                                           ,I_ref_no_2      => L_ref_no_2
                                                           ,I_ref_no_3      => L_ref_no_3
                                                           ,I_ref_no_4      => L_ref_no_4
                                                           ,I_ref_no_5      => L_ref_no_5
                                                           ,I_tran_date     => SYSDATE)  = FALSE then
                   return FALSE;
                end if;
                
                
                  if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                           ,I_tran_code          => 108
                                                           ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                           ,I_fiscal_doc_line_id => NULL
                                                           ,I_vat_code           => rec_tax_head.vat_code
                                                           ,O_ref_no_1           => L_ref_no_1
                                                           ,O_ref_no_2           => L_ref_no_2
                                                           ,O_ref_no_3           => L_ref_no_3
                                                           ,O_ref_no_4           => L_ref_no_4
                                                           ,O_ref_no_5           => L_ref_no_5) = FALSE then
                    return FALSE;
                 end if;
      --
                 if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                            ,I_tran_code     => 108
                                                            ,I_fiscal_doc_id => I_fiscal_doc_id
                                                            ,I_fiscal_doc_no => L_fiscal_doc_no
                                                            ,I_supplier      => L_supplier
                                                            ,I_dept          => NULL
                                                            ,I_class         => NULL
                                                            ,I_subclass      => NULL
                                                            ,I_item          => NULL
                                                            ,I_location_type => L_location_type
                                                            ,I_location      => L_location_id
                                                            ,I_total_cost    => (rec_tax_head.total_value - rec_tax_detail.line_tax) * (rec_tax_detail.rec_line_tax / rec_tax_detail.line_tax )
                                                            ,I_ref_no_1      => L_ref_no_1
                                                            ,I_ref_no_2      => L_ref_no_2
                                                            ,I_ref_no_3      => L_ref_no_3
                                                            ,I_ref_no_4      => L_ref_no_4
                                                            ,I_ref_no_5      => L_ref_no_5
                                                            ,I_tran_date     => SYSDATE)  = FALSE then
                    return FALSE;
                 end if;
            end if;
--
         end if;
      END LOOP; 
--
   END LOOP; 
--

   if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                            ,I_tran_code          => 107
                                            ,I_fiscal_doc_id      => I_fiscal_doc_id
                                            ,I_fiscal_doc_line_id => NULL
                                            ,I_vat_code           => NULL
                                            ,O_ref_no_1           => L_ref_no_1
                                            ,O_ref_no_2           => L_ref_no_2
                                            ,O_ref_no_3           => L_ref_no_3
                                            ,O_ref_no_4           => L_ref_no_4
                                            ,O_ref_no_5           => L_ref_no_5) = FALSE then
      return FALSE;
   end if;
--
   if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                              ,I_tran_code     => 107
                                              ,I_fiscal_doc_id => I_fiscal_doc_id
                                              ,I_fiscal_doc_no => L_fiscal_doc_no
                                              ,I_supplier      => L_supplier
                                              ,I_dept          => NULL
                                              ,I_class         => NULL
                                              ,I_subclass      => NULL
                                              ,I_item          => NULL
                                              ,I_location_type => L_location_type
                                              ,I_location      => L_location_id
                                              ,I_total_cost    => (I_variance - L_tax_variance)
                                              ,I_ref_no_1      => L_ref_no_1
                                              ,I_ref_no_2      => L_ref_no_2
                                              ,I_ref_no_3      => L_ref_no_3
                                              ,I_ref_no_4      => L_ref_no_4
                                              ,I_ref_no_5      => L_ref_no_5
                                              ,I_tran_date     => SYSDATE) = FALSE then
      return FALSE;
   end if;
--
   return TRUE;
--
EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      
      return FALSE;
--
END POST_TRANDATA_CAL_VARIANCE;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_NF_STATUS_C
-- Purpose:       This function will update the NF status to C once all the Posting is done.
----------------------------------------------------------------------------------
FUNCTION UPDATE_NF_STATUS_C( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                            ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.UPDATE_NF_STATUS_C';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
--
   L_completed           VARCHAR2(1):= 'C';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   l_dummy        NUMBER;
   L_count        NUMBER;
   L_schedule     FM_SCHEDULE.SCHEDULE_NO%TYPE;
--
   cursor C_UPDATE_NF is
      SELECT 1
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.fiscal_doc_id = I_fiscal_doc_id
       for update nowait;
--
   cursor C_UPDATE_SCHEDULE is
      SELECT count(1)
            ,fdh.schedule_no
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.schedule_no = (SELECT schedule_no FROM fm_fiscal_doc_header WHERE fiscal_doc_id = I_fiscal_doc_id)
         AND status != 'C'
    GROUP BY schedule_no;
--
BEGIN 
--
   SQL_LIB.SET_MARK('OPEN','C_UPDATE_NF', 'fm_fiscal_doc_header', L_key);
   OPEN C_UPDATE_NF;
   SQL_LIB.SET_MARK('FETCH','C_UPDATE_NF', 'fm_fiscal_doc_header', L_key);
   FETCH C_UPDATE_NF INTO l_dummy;
   SQL_LIB.SET_MARK('CLOSE','C_UPDATE_NF', 'fm_fiscal_doc_header', L_key);
   CLOSE C_UPDATE_NF;
--
   UPDATE fm_fiscal_doc_header
      SET status        = L_completed
    WHERE fiscal_doc_id = I_fiscal_doc_id;
--
   SQL_LIB.SET_MARK('OPEN','C_UPDATE_SCHEDULE', 'fm_fiscal_doc_header', L_key);
   OPEN C_UPDATE_SCHEDULE;
   SQL_LIB.SET_MARK('FETCH','C_UPDATE_SCHEDULE', 'fm_fiscal_doc_header', L_key);
   FETCH C_UPDATE_SCHEDULE INTO L_count, L_schedule;
   SQL_LIB.SET_MARK('CLOSE','C_UPDATE_SCHEDULE', 'fm_fiscal_doc_header', L_key);
   CLOSE C_UPDATE_SCHEDULE;
--
   if nvl(L_count,0) < 1 then
      UPDATE fm_schedule
         SET status = L_completed
       WHERE schedule_no = (SELECT schedule_no FROM fm_fiscal_doc_header WHERE fiscal_doc_id = I_fiscal_doc_id);
   end if;
--
   return TRUE;
--
EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'fm_fiscal_doc_header',
                                            I_fiscal_doc_id,
                                            NULL);
      return FALSE;
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_UPDATE_NF%ISOPEN then
         close C_UPDATE_NF;
      end if;
--
      return FALSE;
END UPDATE_NF_STATUS_C;
--
-----------------------------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_TRANSFERS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                ,I_fiscal_doc_no        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                                ,I_supplier             IN FM_TRAN_DATA.SUPPLIER%TYPE
                                ,I_item                 IN FM_TRAN_DATA.ITEM%TYPE 
                                ,I_location_type        IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                                ,I_location_id          IN FM_TRAN_DATA.LOCATION%TYPE
                                ,I_unit_cost            IN FM_TRAN_DATA.TOTAL_COST%TYPE
                                ,I_fiscal_doc_line_id   IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                                ,I_variance_cost        IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                                ,I_tolerance_value_cost IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                                ,I_tolerance_value_qty  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                                ,I_utilization_id       IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                                ,I_requisition_type     IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                                ,I_dept                 IN FM_TRAN_DATA.DEPT%TYPE
                                ,I_class                IN FM_TRAN_DATA.CLASS%TYPE
                                ,I_subclass             IN FM_TRAN_DATA.SUBCLASS%TYPE
                                ,I_quantity             IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                                ,I_freight_cost         IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                                ,I_insurance_cost       IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                                ,I_other_expenses_cost  IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                                ,I_triangulation_ind    IN VARCHAR2
                                ,I_requisition_no       IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                                ,I_comp_fiscal_doc_id   IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE) 
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.POST_TRANDATA_TRANSFERS';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
   L_mode_type           FM_SCHEDULE.MODE_TYPE%TYPE;
   L_icms_credit_val     FM_FISCAL_DOC_DETAIL.ICMS_CREDIT_VAL%TYPE   := 0;
   L_icmsst_credit_val   FM_FISCAL_DOC_DETAIL.ICMSST_CREDIT_VAL%TYPE := 0;
   L_icms                VARCHAR2(6)  := 'ICMS';
   L_st                  VARCHAR2(6)  := 'ST';
--
   cursor C_GET_TAX_DETAIL(P_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdtd.vat_code vat_code
            ,fdtd.fiscal_doc_line_id
            ,nvl(fdtd.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail fdtd
      where fdtd.fiscal_doc_line_id in (I_fiscal_doc_line_id ,P_fiscal_doc_line_id);
--
   cursor C_GET_MODE_TYPE is
      select fs.mode_type
        from fm_schedule fs,
             fm_fiscal_doc_header fdh
       where fdh.schedule_no   = fs.schedule_no
         and fdh.fiscal_doc_id = I_fiscal_doc_id;

   cursor C_GET_RECOVERED_VALUES(P_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
   select fdd.icms_credit_val,
          fdd.icmsst_credit_val
     from fm_fiscal_doc_detail fdd
    where fdd.fiscal_doc_line_id = P_fiscal_doc_line_id;

BEGIN
--
SQL_LIB.SET_MARK('OPEN','c_get_mode_type', 'FM_SCHEDULE/FM_FISCAL_DOC_HEADER', L_key);
open C_GET_MODE_TYPE;
---
SQL_LIB.SET_MARK('FETCH','c_get_mode_type', 'FM_SCHEDULE/FM_FISCAL_DOC_HEADER', L_key);
fetch C_GET_MODE_TYPE into L_mode_type;
---
SQL_LIB.SET_MARK('CLOSE','c_get_mode_type', 'FM_SCHEDULE/FM_FISCAL_DOC_HEADER', L_key);
close C_GET_MODE_TYPE;
---
if L_mode_type = 'ENT' then
   FOR L_get_tax_detail IN C_GET_TAX_DETAIL(I_fiscal_doc_line_id)
      LOOP
         if L_get_tax_detail.unit_rec_value > 0 then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                     ,I_tran_code          => 132
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
   --
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 132
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value > 0 then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 131
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
            --
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 131
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
            --
         end if;
         --
      END LOOP;
      ---
elsif L_mode_type = 'EXIT' then
   SQL_LIB.SET_MARK('OPEN','c_get_recovered_values', 'FM_FISCAL_DOC_DETAIL', 'fiscal_doc_line_id = ' ||I_fiscal_doc_line_id);
   open C_GET_RECOVERED_VALUES(I_fiscal_doc_line_id);
   ---
   SQL_LIB.SET_MARK('FETCH','c_get_recovered_values', 'FM_FISCAL_DOC_DETAIL', 'fiscal_doc_line_id = '|| I_fiscal_doc_line_id);
   fetch C_GET_RECOVERED_VALUES into L_icms_credit_val,L_icmsst_credit_val;
   ---
   SQL_LIB.SET_MARK('CLOSE','c_get_recovered_values', 'FM_FISCAL_DOC_DETAIL', 'fiscal_doc_line_id = '||I_fiscal_doc_line_id);
   close C_GET_RECOVERED_VALUES;
   ---
   
   if L_icms_credit_val > 0 then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                               ,I_tran_code          => 130
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => L_icms
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
      --
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 130
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => L_icms_credit_val * I_quantity
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
      --
   end if;
   ---
   if L_icmsst_credit_val > 0 then
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                               ,I_tran_code          => 130
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                               ,I_vat_code           => L_st
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
      --
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 130
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => I_fiscal_doc_no
                                                 ,I_supplier      => I_supplier
                                                 ,I_dept          => I_dept
                                                 ,I_class         => I_class
                                                 ,I_subclass      => I_subclass
                                                 ,I_item          => I_item
                                                 ,I_location_type => I_location_type
                                                 ,I_location      => I_location_id
                                                 ,I_total_cost    => L_icmsst_credit_val * I_quantity
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
      --
   end if;
   ---
   FOR L_get_tax_detail IN C_GET_TAX_DETAIL(I_fiscal_doc_line_id)
      LOOP
         if L_get_tax_detail.unit_rec_value > 0 and L_icms_credit_val = 0 and L_icmsst_credit_val = 0 then
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                     ,I_tran_code          => 130
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
            --
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 130
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
            --
         end if;
         --
         if L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value > 0 then

            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                     ,I_tran_code          => 129
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
               return FALSE;
            end if;
            --
            if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                       ,I_tran_code     => 129
                                                       ,I_fiscal_doc_id => I_fiscal_doc_id
                                                       ,I_fiscal_doc_no => I_fiscal_doc_no
                                                       ,I_supplier      => I_supplier
                                                       ,I_dept          => I_dept
                                                       ,I_class         => I_class
                                                       ,I_subclass      => I_subclass
                                                       ,I_item          => I_item
                                                       ,I_location_type => I_location_type
                                                       ,I_location      => I_location_id
                                                       ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) * I_quantity)
                                                       ,I_ref_no_1      => L_ref_no_1
                                                       ,I_ref_no_2      => L_ref_no_2
                                                       ,I_ref_no_3      => L_ref_no_3
                                                       ,I_ref_no_4      => L_ref_no_4
                                                       ,I_ref_no_5      => L_ref_no_5
                                                       ,I_tran_date     => SYSDATE)  = FALSE then
               return FALSE;
            end if;
            --
          end if;
          --
      END LOOP;
 end if;
--
return TRUE;
--
EXCEPTION
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_GET_TAX_DETAIL%ISOPEN then
         close C_GET_TAX_DETAIL;
      end if;
--
      return FALSE;
--
END POST_TRANDATA_TRANSFERS;
----------------------------------------------------------------------------------
-- Function Name: PROCESS_TRAN_DATA_TRAN
-- Purpose:       This function Post trandata when the requisition type is PO for triangulation.
----------------------------------------------------------------------------------
FUNCTION PROCESS_TRAN_DATA_TRAN( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                ,I_fiscal_doc_id         IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                ,I_fiscal_doc_no         IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE
                                ,I_supplier              IN FM_TRAN_DATA.SUPPLIER%TYPE
                                ,I_item                  IN FM_TRAN_DATA.ITEM%TYPE 
                                ,I_location_type         IN FM_TRAN_DATA.LOCATION_TYPE%TYPE
                                ,I_location_id           IN FM_TRAN_DATA.LOCATION%TYPE
                                ,I_unit_cost             IN FM_TRAN_DATA.TOTAL_COST%TYPE
                                ,I_fiscal_doc_line_id    IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE
                                ,I_variance_cost         IN FM_FISCAL_DOC_HEADER.VARIANCE_COST%TYPE
                                ,I_tolerance_value_cost  IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_COST%TYPE
                                ,I_tolerance_value_qty   IN FM_FISCAL_DOC_DETAIL.TOLERANCE_VALUE_QTY%TYPE
                                ,I_utilization_id        IN FM_FISCAL_DOC_HEADER.UTILIZATION_ID%TYPE
                                ,I_requisition_type      IN FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE
                                ,I_dept                  IN FM_TRAN_DATA.DEPT%TYPE
                                ,I_class                 IN FM_TRAN_DATA.CLASS%TYPE
                                ,I_subclass              IN FM_TRAN_DATA.SUBCLASS%TYPE
                                ,I_quantity              IN FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE
                                ,I_freight_cost          IN FM_FISCAL_DOC_DETAIL.FREIGHT_COST%TYPE
                                ,I_insurance_cost        IN FM_FISCAL_DOC_DETAIL.INSURANCE_COST%TYPE
                                ,I_other_expenses_cost   IN FM_FISCAL_DOC_DETAIL.OTHER_EXPENSES_COST%TYPE
                                ,I_requisition_no        IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                                ,I_comp_fiscal_doc_id    IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
--
    L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.PROCESS_TRAN_DATA_TRAN';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
--
   L_main_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_comp_base_cost           FM_RECEIVING_DETAIL.EBC_COST%TYPE;
   L_comp_fiscal_doc_line_id  FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
--
   cursor C_GET_TAX_DETAIL(P_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
       select fdtd.vat_code vat_code
            ,fdtd.fiscal_doc_line_id
            ,nvl(fdtd.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtd.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail fdtd,
            fm_tax_codes ftc
      where fdtd.fiscal_doc_line_id in (I_fiscal_doc_line_id ,P_fiscal_doc_line_id)
      and fdtd.vat_code = ftc.tax_code
      and ftc.matching_ind = 'Y'
      UNION ALL
      select fdtde.tax_code vat_code
            ,fdtde.fiscal_doc_line_id
            ,nvl(fdtde.unit_tax_amt,0) unit_tax_amt
            ,nvl(fdtde.unit_rec_value,0) unit_rec_value
       from fm_fiscal_doc_tax_detail_ext fdtde,
            fm_tax_codes ftc
      where fdtde.fiscal_doc_line_id = I_fiscal_doc_line_id
        and fdtde.tax_code = ftc.tax_code
      and ftc.matching_ind = 'N';
--
   cursor C_GET_TRAN_DATA_TRAN is
      SELECT fdd.fiscal_doc_line_id
        FROM fm_fiscal_doc_detail fdd
       WHERE fdd.fiscal_doc_id  = I_comp_fiscal_doc_id
         AND fdd.requisition_no = I_requisition_no
         AND fdd.item           = I_item;
BEGIN
--
   OPEN C_GET_TRAN_DATA_TRAN;
   FETCH C_GET_TRAN_DATA_TRAN INTO L_comp_fiscal_doc_line_id;
   CLOSE C_GET_TRAN_DATA_TRAN;
--
   if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                       O_base_cost          => L_main_base_cost,
                                       I_nic_cost           => I_unit_cost,
                                       I_nic_cost_factor    => I_unit_cost,
                                       I_fiscal_doc_line_id => I_fiscal_doc_line_id) = FALSE then
      return FALSE;
   end if;
--
   if FM_TRANDATA_POSTING_SQL.CALC_BC (O_error_message      => O_error_message ,
                                       O_base_cost          => L_comp_base_cost,
                                       I_nic_cost           => I_unit_cost,
                                       I_nic_cost_factor    => I_unit_cost,
                                       I_fiscal_doc_line_id => L_comp_fiscal_doc_line_id) = FALSE then
      return FALSE;
   end if;
   
   if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                            ,I_tran_code          => 100
                                            ,I_fiscal_doc_id      => I_fiscal_doc_id
                                            ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                            ,I_vat_code           => NULL
                                            ,O_ref_no_1           => L_ref_no_1
                                            ,O_ref_no_2           => L_ref_no_2
                                            ,O_ref_no_3           => L_ref_no_3
                                            ,O_ref_no_4           => L_ref_no_4
                                            ,O_ref_no_5           => L_ref_no_5) = FALSE then
      return FALSE;
   end if;
--
   if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                              ,I_tran_code     => 100
                                              ,I_fiscal_doc_id => I_fiscal_doc_id
                                              ,I_fiscal_doc_no => I_fiscal_doc_no
                                              ,I_supplier      => I_supplier
                                              ,I_dept          => I_dept
                                              ,I_class         => I_class
                                              ,I_subclass      => I_subclass
                                              ,I_item          => I_item
                                              ,I_location_type => I_location_type
                                              ,I_location      => I_location_id
                                              ,I_total_cost    => ((L_main_base_cost + L_comp_base_cost - I_unit_cost) * I_quantity)
                                              ,I_ref_no_1      => L_ref_no_1
                                              ,I_ref_no_2      => L_ref_no_2
                                              ,I_ref_no_3      => L_ref_no_3
                                              ,I_ref_no_4      => L_ref_no_4
                                              ,I_ref_no_5      => L_ref_no_5
                                              ,I_tran_date     => SYSDATE)  = FALSE then
      return FALSE;
   end if;
--
   FOR L_get_tax_detail IN C_GET_TAX_DETAIL(L_comp_fiscal_doc_line_id)
   LOOP
      if L_get_tax_detail.unit_rec_value = 0 then
         
          
          if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                   ,I_tran_code          => 101
                                                   ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                   ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                   ,I_vat_code           => L_get_tax_detail.vat_code
                                                   ,O_ref_no_1           => L_ref_no_1
                                                   ,O_ref_no_2           => L_ref_no_2
                                                   ,O_ref_no_3           => L_ref_no_3
                                                   ,O_ref_no_4           => L_ref_no_4
                                                   ,O_ref_no_5           => L_ref_no_5) = FALSE then
             return FALSE;
          end if;
--
          if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                     ,I_tran_code     => 101
                                                     ,I_fiscal_doc_id => I_fiscal_doc_id
                                                     ,I_fiscal_doc_no => I_fiscal_doc_no
                                                     ,I_supplier      => I_supplier
                                                     ,I_dept          => I_dept
                                                     ,I_class         => I_class
                                                     ,I_subclass      => I_subclass
                                                     ,I_item          => I_item
                                                     ,I_location_type => I_location_type
                                                     ,I_location      => I_location_id
                                                     ,I_total_cost    => (L_get_tax_detail.unit_tax_amt * I_quantity)
                                                     ,I_ref_no_1      => L_ref_no_1
                                                     ,I_ref_no_2      => L_ref_no_2
                                                     ,I_ref_no_3      => L_ref_no_3
                                                     ,I_ref_no_4      => L_ref_no_4
                                                     ,I_ref_no_5      => L_ref_no_5
                                                     ,I_tran_date     => SYSDATE)  = FALSE then
             return FALSE;
          end if;
--
      elsif L_get_tax_detail.unit_rec_value > 0 and (L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) = 0 then
       
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                    ,I_tran_code          => 102
                                                    ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                    ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                    ,I_vat_code           => L_get_tax_detail.vat_code
                                                    ,O_ref_no_1           => L_ref_no_1
                                                    ,O_ref_no_2           => L_ref_no_2
                                                    ,O_ref_no_3           => L_ref_no_3
                                                    ,O_ref_no_4           => L_ref_no_4
                                                    ,O_ref_no_5           => L_ref_no_5) = FALSE then
              return FALSE;
           end if;
--
           if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                      ,I_tran_code     => 102
                                                      ,I_fiscal_doc_id => I_fiscal_doc_id
                                                      ,I_fiscal_doc_no => I_fiscal_doc_no
                                                      ,I_supplier      => I_supplier
                                                      ,I_dept          => I_dept
                                                      ,I_class         => I_class
                                                      ,I_subclass      => I_subclass
                                                      ,I_item          => I_item
                                                      ,I_location_type => I_location_type
                                                      ,I_location      => I_location_id
                                                      ,I_total_cost    => (L_get_tax_detail.unit_rec_value *  I_quantity)
                                                      ,I_ref_no_1      => L_ref_no_1
                                                      ,I_ref_no_2      => L_ref_no_2
                                                      ,I_ref_no_3      => L_ref_no_3
                                                      ,I_ref_no_4      => L_ref_no_4
                                                      ,I_ref_no_5      => L_ref_no_5
                                                      ,I_tran_date     => SYSDATE)  = FALSE then
              return FALSE;
           end if;
      else 
         
          if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                   ,I_tran_code          => 101
                                                   ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                   ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                   ,I_vat_code           => L_get_tax_detail.vat_code
                                                   ,O_ref_no_1           => L_ref_no_1
                                                   ,O_ref_no_2           => L_ref_no_2
                                                   ,O_ref_no_3           => L_ref_no_3
                                                   ,O_ref_no_4           => L_ref_no_4
                                                   ,O_ref_no_5           => L_ref_no_5) = FALSE then
             return FALSE;
          end if;
--
          if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                     ,I_tran_code     => 101
                                                     ,I_fiscal_doc_id => I_fiscal_doc_id
                                                     ,I_fiscal_doc_no => I_fiscal_doc_no
                                                     ,I_supplier      => I_supplier
                                                     ,I_dept          => I_dept
                                                     ,I_class         => I_class
                                                     ,I_subclass      => I_subclass
                                                     ,I_item          => I_item
                                                     ,I_location_type => I_location_type
                                                     ,I_location      => I_location_id
                                                     ,I_total_cost    => ((L_get_tax_detail.unit_tax_amt - L_get_tax_detail.unit_rec_value) * I_quantity)
                                                     ,I_ref_no_1      => L_ref_no_1
                                                     ,I_ref_no_2      => L_ref_no_2
                                                     ,I_ref_no_3      => L_ref_no_3
                                                     ,I_ref_no_4      => L_ref_no_4
                                                     ,I_ref_no_5      => L_ref_no_5
                                                     ,I_tran_date     => SYSDATE)  = FALSE then
             return FALSE;
          end if;
          
          
            if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                                     ,I_tran_code          => 102
                                                     ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                     ,I_fiscal_doc_line_id => I_fiscal_doc_line_id
                                                     ,I_vat_code           => L_get_tax_detail.vat_code
                                                     ,O_ref_no_1           => L_ref_no_1
                                                     ,O_ref_no_2           => L_ref_no_2
                                                     ,O_ref_no_3           => L_ref_no_3
                                                     ,O_ref_no_4           => L_ref_no_4
                                                     ,O_ref_no_5           => L_ref_no_5) = FALSE then
              return FALSE;
           end if;
--
           if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                      ,I_tran_code     => 102
                                                      ,I_fiscal_doc_id => I_fiscal_doc_id
                                                      ,I_fiscal_doc_no => I_fiscal_doc_no
                                                      ,I_supplier      => I_supplier
                                                      ,I_dept          => I_dept
                                                      ,I_class         => I_class
                                                      ,I_subclass      => I_subclass
                                                      ,I_item          => I_item
                                                      ,I_location_type => I_location_type
                                                      ,I_location      => I_location_id
                                                      ,I_total_cost    => (L_get_tax_detail.unit_rec_value * I_quantity)
                                                      ,I_ref_no_1      => L_ref_no_1
                                                      ,I_ref_no_2      => L_ref_no_2
                                                      ,I_ref_no_3      => L_ref_no_3
                                                      ,I_ref_no_4      => L_ref_no_4
                                                      ,I_ref_no_5      => L_ref_no_5
                                                      ,I_tran_date     => SYSDATE)  = FALSE then
              return FALSE;
           end if;
      end if;
   END LOOP;
--
   return TRUE;
--
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_GET_TAX_DETAIL%ISOPEN then
         close C_GET_TAX_DETAIL;
      end if;
--
      return FALSE;
--
END PROCESS_TRAN_DATA_TRAN;
--
----------------------------------------------------------------------------------
-- Function Name: POST_TRANDATA_COMPL_FREIGHT
-- Purpose:       This function Process Tran Data Posting for Complemetary NF 
--                for Non Merchandise cost
----------------------------------------------------------------------------------
FUNCTION POST_TRANDATA_COMPL_FREIGHT( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                      ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.POST_TRANDATA_COMPL_FREIGHT';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '|| I_fiscal_doc_id;
--
   L_fiscal_doc_id       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_fiscal_doc_no       FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE;
   L_supplier            FM_TRAN_DATA.SUPPLIER%TYPE;
   L_location_type       FM_TRAN_DATA.LOCATION_TYPE%TYPE;
   L_location_id         FM_TRAN_DATA.LOCATION%TYPE;
   L_non_merch_cost      FM_FISCAL_DOC_HEADER.FREIGHT_COST%TYPE;
   L_total_value         FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE;
--
   L_ref_no_1            FM_TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2            FM_TRAN_DATA.REF_NO_2%TYPE;
   L_ref_no_3            FM_TRAN_DATA.REF_NO_3%TYPE;
   L_ref_no_4            FM_TRAN_DATA.REF_NO_4%TYPE;
   L_ref_no_5            FM_TRAN_DATA.REF_NO_5%TYPE;
--
   cursor C_GET_HEADER_NON_MERCH_COST IS
     SELECT (NVL(freight_cost,0) + NVL(insurance_cost, 0) + NVL(other_expenses_cost, 0)) non_merch_cost
       FROM fm_fiscal_doc_header fdh
      WHERE fdh.fiscal_doc_id = I_fiscal_doc_id;
--
   cursor C_TAX_HEAD is
      select sum(NVL(fdth.total_value,0)) total_value
        from fm_fiscal_doc_tax_head fdth
             ,vat_codes vc
       where fdth.fiscal_doc_id = I_fiscal_doc_id
         and fdth.vat_code = vc.vat_code
         and vc.incl_nic_ind = 'Y';
--
 cursor C_TAX_HEAD_LINE is
      SELECT fdth.vat_code
             ,NVL(fdth.total_value,0) total_value
        FROM fm_fiscal_doc_tax_head fdth
             ,vat_codes vc
       WHERE fdth.fiscal_doc_id = I_fiscal_doc_id
         AND fdth.vat_code = vc.vat_code;
         
--
   cursor C_GET_TRAN_DATA 
   IS
      SELECT fdh.fiscal_doc_id
            ,fdh.fiscal_doc_no
            ,fdh.key_value_1 supplier
            ,fdh.location_type
            ,fdh.location_id
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.fiscal_doc_id   = I_fiscal_doc_id
         AND module              = 'SUPP';
--
BEGIN
--
   SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_DATA', 'fm_fiscal_doc_header', L_key);
   OPEN C_GET_TRAN_DATA;
   SQL_LIB.SET_MARK('FETCH','C_GET_TRAN_DATA', 'fm_fiscal_doc_header', L_key);
   FETCH C_GET_TRAN_DATA INTO L_fiscal_doc_id , L_fiscal_doc_no ,L_supplier ,L_location_type,L_location_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRAN_DATA', 'fm_fiscal_doc_header', L_key);
   CLOSE C_GET_TRAN_DATA;
--
   SQL_LIB.SET_MARK('OPEN','C_GET_HEADER_NON_MERCH_COST', 'fm_fiscal_doc_header', L_key);
   OPEN C_GET_HEADER_NON_MERCH_COST;
   SQL_LIB.SET_MARK('FETCH','C_GET_HEADER_NON_MERCH_COST', 'fm_fiscal_doc_header', L_key);
   FETCH C_GET_HEADER_NON_MERCH_COST INTO L_non_merch_cost;
   SQL_LIB.SET_MARK('CLOSE','C_GET_HEADER_NON_MERCH_COST', 'fm_fiscal_doc_header', L_key);
   CLOSE C_GET_HEADER_NON_MERCH_COST;
--
   SQL_LIB.SET_MARK('OPEN','C_TAX_HEAD', 'fm_fiscal_doc_tax_head', L_key);
   OPEN C_TAX_HEAD;
   SQL_LIB.SET_MARK('FETCH','C_TAX_HEAD', 'fm_fiscal_doc_tax_head', L_key);
   FETCH C_TAX_HEAD INTO L_total_value;
   SQL_LIB.SET_MARK('CLOSE','C_TAX_HEAD', 'fm_fiscal_doc_tax_head', L_key);
   CLOSE C_TAX_HEAD;
--
   
   
   if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                            ,I_tran_code          => 120
                                            ,I_fiscal_doc_id      => I_fiscal_doc_id
                                            ,I_fiscal_doc_line_id => NULL
                                            ,I_vat_code           => NULL
                                            ,O_ref_no_1           => L_ref_no_1
                                            ,O_ref_no_2           => L_ref_no_2
                                            ,O_ref_no_3           => L_ref_no_3
                                            ,O_ref_no_4           => L_ref_no_4
                                            ,O_ref_no_5           => L_ref_no_5) = FALSE then
      return FALSE;
   end if;
   
   if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                              ,I_tran_code     => 120
                                              ,I_fiscal_doc_id => I_fiscal_doc_id
                                              ,I_fiscal_doc_no => L_fiscal_doc_no
                                              ,I_supplier      => L_supplier
                                              ,I_dept          => NULL
                                              ,I_class         => NULL
                                              ,I_subclass      => NULL
                                              ,I_item          => NULL
                                              ,I_location_type => L_location_type
                                              ,I_location      => L_location_id
                                              ,I_total_cost    => (L_non_merch_cost - L_total_value)
                                              ,I_ref_no_1      => L_ref_no_1
                                              ,I_ref_no_2      => L_ref_no_2
                                              ,I_ref_no_3      => L_ref_no_3
                                              ,I_ref_no_4      => L_ref_no_4
                                              ,I_ref_no_5      => L_ref_no_5
                                              ,I_tran_date     => SYSDATE)  = FALSE then
      return FALSE;
   end if;
   
   if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message     => O_error_message
                                            ,I_tran_code          => 121 
                                            ,I_fiscal_doc_id      => I_fiscal_doc_id
                                            ,I_fiscal_doc_line_id => NULL
                                            ,I_vat_code           => NULL
                                            ,O_ref_no_1           => L_ref_no_1
                                            ,O_ref_no_2           => L_ref_no_2
                                            ,O_ref_no_3           => L_ref_no_3
                                            ,O_ref_no_4           => L_ref_no_4
                                            ,O_ref_no_5           => L_ref_no_5) = FALSE then
      return FALSE;
   end if;
   
   if (L_ref_no_1 = 'T' or L_ref_no_2 = 'T' or L_ref_no_3 = 'T' or L_ref_no_4 = 'T' or L_ref_no_5 = 'T') then
      
      FOR rec_tax_head in C_TAX_HEAD_LINE
      LOOP
         
         
         if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                                  ,I_tran_code          => 121
                                                  ,I_fiscal_doc_id      => I_fiscal_doc_id
                                                  ,I_fiscal_doc_line_id => NULL
                                                  ,I_vat_code           => rec_tax_head.vat_code
                                                  ,O_ref_no_1           => L_ref_no_1
                                                  ,O_ref_no_2           => L_ref_no_2
                                                  ,O_ref_no_3           => L_ref_no_3
                                                  ,O_ref_no_4           => L_ref_no_4
                                                  ,O_ref_no_5           => L_ref_no_5) = FALSE then
            return FALSE;
         end if;
         
         if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                    ,I_tran_code     => 121
                                                    ,I_fiscal_doc_id => I_fiscal_doc_id
                                                    ,I_fiscal_doc_no => L_fiscal_doc_no
                                                    ,I_supplier      => L_supplier
                                                    ,I_dept          => NULL
                                                    ,I_class         => NULL
                                                    ,I_subclass      => NULL
                                                    ,I_item          => NULL
                                                    ,I_location_type => L_location_type
                                                    ,I_location      => L_location_id
                                                    ,I_total_cost    => rec_tax_head.total_value
                                                    ,I_ref_no_1      => L_ref_no_1
                                                    ,I_ref_no_2      => L_ref_no_2
                                                    ,I_ref_no_3      => L_ref_no_3
                                                    ,I_ref_no_4      => L_ref_no_4
                                                    ,I_ref_no_5      => L_ref_no_5
                                                    ,I_tran_date     => SYSDATE)  = FALSE then
            return FALSE;
         end if;
         
      END LOOP; 
      
   else
      
      
      if FM_TRANDATA_POSTING_SQL.GET_REF_VALUES(O_error_message      => O_error_message
                                               ,I_tran_code          => 121
                                               ,I_fiscal_doc_id      => I_fiscal_doc_id
                                               ,I_fiscal_doc_line_id => NULL
                                               ,I_vat_code           => NULL
                                               ,O_ref_no_1           => L_ref_no_1
                                               ,O_ref_no_2           => L_ref_no_2
                                               ,O_ref_no_3           => L_ref_no_3
                                               ,O_ref_no_4           => L_ref_no_4
                                               ,O_ref_no_5           => L_ref_no_5) = FALSE then
         return FALSE;
      end if;
      
      if FM_TRANDATA_POSTING_SQL.INSERT_TRAN_DATA(O_error_message => O_error_message
                                                 ,I_tran_code     => 121
                                                 ,I_fiscal_doc_id => I_fiscal_doc_id
                                                 ,I_fiscal_doc_no => L_fiscal_doc_no
                                                 ,I_supplier      => L_supplier
                                                 ,I_dept          => NULL
                                                 ,I_class         => NULL
                                                 ,I_subclass      => NULL
                                                 ,I_item          => NULL
                                                 ,I_location_type => L_location_type
                                                 ,I_location      => L_location_id
                                                 ,I_total_cost    => L_total_value
                                                 ,I_ref_no_1      => L_ref_no_1
                                                 ,I_ref_no_2      => L_ref_no_2
                                                 ,I_ref_no_3      => L_ref_no_3
                                                 ,I_ref_no_4      => L_ref_no_4
                                                 ,I_ref_no_5      => L_ref_no_5
                                                 ,I_tran_date     => SYSDATE)  = FALSE then
         return FALSE;
      end if;
      
   end if;
   
   return TRUE;
--
EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      
      return FALSE;
--
END POST_TRANDATA_COMPL_FREIGHT;
----------------------------------------------------------------------------------
-- Function Name: INSERT_TRAN_DATA
-- Purpose:       This function Inserts Tran data from staging table to Base table.
--                This function check if there are any tax tran code available. If not this will sum all the 
--                tax values and insert into base tran data table
----------------------------------------------------------------------------------
FUNCTION MOVE_TRAN_STG_BASE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id  IN FM_TRAN_DATA.FISCAL_DOC_ID%TYPE) 
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_TRANDATA_POSTING_SQL.MOVE_TRAN_STG_BASE';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '|| I_fiscal_doc_id;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   L_count               NUMBER := 0;
   L_seq_no              NUMBER;
--
   cursor C_GET_TRAN_CODE IS
      SELECT distinct ftd.tran_code
        FROM gtt_fm_tran_data ftd
       WHERE fiscal_doc_id = I_fiscal_doc_id
    ORDER BY tran_code;
--
   cursor C_GET_TAX_COUNT(I_tran_code IN FM_TRAN_DATA.TRAN_CODE%TYPE)IS
      SELECT count(1)
        FROM fm_tran_codes
       WHERE (ref_no_1 = 'T' or ref_no_2 = 'T' or ref_no_3 = 'T' or ref_no_4 = 'T' or ref_no_5 = 'T')
         AND tran_code = I_tran_code;
BEGIN
--
   SQL_LIB.SET_MARK('OPEN','GET_TRAN_CODE', 'FM_TRAN_DATA', L_key);
   FOR L_get_tran_code IN C_GET_TRAN_CODE
   LOOP
      OPEN C_GET_TAX_COUNT(L_get_tran_code.tran_code);
      FETCH C_GET_TAX_COUNT INTO L_count;
      CLOSE C_GET_TAX_COUNT;
--
      if L_count > 0 then
         INSERT INTO fm_tran_data( TRAN_ID
                                  ,TRAN_CODE
                                  ,FISCAL_DOC_ID
                                  ,FISCAL_DOC_NO
                                  ,SUPPLIER
                                  ,DEPT
                                  ,CLASS
                                  ,SUBCLASS
                                  ,ITEM
                                  ,LOCATION_TYPE
                                  ,LOCATION
                                  ,TOTAL_COST
                                  ,REF_NO_1
                                  ,REF_NO_2
                                  ,REF_NO_3
                                  ,REF_NO_4
                                  ,REF_NO_5
                                  ,TRAN_DATE
                                  ,CREATE_DATETIME
                                  ,CREATE_ID
                                  ,LAST_UPDATE_DATETIME
                                  ,LAST_UPDATE_ID
                                  )
                          (SELECT fm_tran_id_seq.nextval
                                  ,tran_code
                                  ,fiscal_doc_id
                                  ,fiscal_doc_no
                                  ,supplier
                                  ,dept
                                  ,class
                                  ,subclass
                                  ,item
                                  ,location_type
                                  ,location
                                  ,total_cost
                                  ,ref_no_1
                                  ,ref_no_2
                                  ,ref_no_3
                                  ,ref_no_4
                                  ,ref_no_5
                                  ,SYSDATE
                                  ,SYSDATE
                                  ,USER
                                  ,SYSDATE
                                  ,USER
                              FROM gtt_fm_tran_data
                             WHERE tran_code     = L_get_tran_code.tran_code
                               AND fiscal_doc_id = I_fiscal_doc_id);
      elsif L_count = 0 then
         select fm_tran_id_seq.nextval
         into L_seq_no
         from dual;
         INSERT INTO fm_tran_data( TRAN_ID
                                  ,TRAN_CODE
                                  ,FISCAL_DOC_ID
                                  ,FISCAL_DOC_NO
                                  ,SUPPLIER
                                  ,DEPT
                                  ,CLASS
                                  ,SUBCLASS
                                  ,ITEM
                                  ,LOCATION_TYPE
                                  ,LOCATION
                                  ,TOTAL_COST
                                  ,REF_NO_1
                                  ,REF_NO_2
                                  ,REF_NO_3
                                  ,REF_NO_4
                                  ,REF_NO_5
                                  ,TRAN_DATE
                                  ,CREATE_DATETIME
                                  ,CREATE_ID
                                  ,LAST_UPDATE_DATETIME
                                  ,LAST_UPDATE_ID
                                  )
                          (SELECT  fm_tran_id_seq.nextval
                                  ,b.* 
                             from ( select tran_code
                                          ,fiscal_doc_id
                                          ,fiscal_doc_no
                                          ,supplier
                                          ,dept
                                          ,class
                                          ,subclass
                                          ,item
                                          ,location_type
                                          ,location
                                          ,sum(total_cost)
                                          ,decode(ref_no_1,'NT',null,ref_no_1)
                                          ,decode(ref_no_2,'NT',null,ref_no_2)
                                          ,decode(ref_no_3,'NT',null,ref_no_3)
                                          ,decode(ref_no_4,'NT',null,ref_no_4)
                                          ,decode(ref_no_5,'NT',null,ref_no_5)
                                          ,SYSDATE SYSDATE_1
                                          ,SYSDATE SYSDATE_2
                                          ,USER USER_1
                                          ,SYSDATE SYSDATE_3
                                          ,USER USER_2
                                      FROM gtt_fm_tran_data stg
                                     WHERE tran_code     = L_get_tran_code.tran_code
                                       AND fiscal_doc_id = I_fiscal_doc_id
                                       AND (stg.ref_no_1 = 'NT' 
                                              or stg.ref_no_2 = 'NT' 
                                              or stg.ref_no_3 = 'NT' 
                                              or stg.ref_no_4 = 'NT' 
                                              or stg.ref_no_5 = 'NT')
                                  GROUP BY tran_code,fiscal_doc_id,fiscal_doc_no,supplier
                                          ,dept,class,subclass,item,location_type,location
                                          ,ref_no_1,ref_no_2,ref_no_3,ref_no_4,ref_no_5) b);
--
      end if;
--
   END LOOP;
--
   return TRUE;
EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      
      return FALSE;
--
END MOVE_TRAN_STG_BASE;
--
----------------------------------------------------------------------------------
END FM_TRANDATA_POSTING_SQL;
/