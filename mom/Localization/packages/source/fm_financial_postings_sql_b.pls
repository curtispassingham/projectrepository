CREATE OR REPLACE PACKAGE BODY FM_FINANCIAL_POSTING_SQL as
----------------------------------------------------------------------------------
L_yes varchar2(1):='Y';
L_no  varchar2(1):='N';
L_err varchar2(1):='E';
L_st varchar2(1) :='S';
L_wh varchar2(1) :='W';
L_db varchar2(2) :='DB';
L_cr varchar2(2) :='CR';
---
TYPE L_TEMP_REC is RECORD ( sequence1         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE1%TYPE,
                            sequence2         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE2%TYPE,
                            sequence3         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE3%TYPE,
                            sequence4         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE4%TYPE,
                            sequence5         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE5%TYPE,
                            sequence6         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE6%TYPE,
                            sequence7         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE7%TYPE,
                            sequence8         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE8%TYPE,
                            sequence9         GTT_FM_FINANCIAL_POSTINGS.SEQUENCE9%TYPE,
                            sequence10        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE10%TYPE,
                            sequence11        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE11%TYPE,
                            sequence12        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE12%TYPE,
                            sequence13        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE13%TYPE,
                            sequence14        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE14%TYPE,
                            sequence15        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE15%TYPE,
                            sequence16        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE16%TYPE,
                            sequence17        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE17%TYPE,
                            sequence18        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE18%TYPE,
                            sequence19        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE19%TYPE,
                            sequence20        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE20%TYPE,
                            sequence21        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE21%TYPE,
                            sequence22        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE22%TYPE,
                            sequence23        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE23%TYPE,
                            sequence24        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE24%TYPE,
                            sequence25        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE25%TYPE,
                            sequence26        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE26%TYPE,
                            sequence27        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE27%TYPE,
                            sequence28        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE28%TYPE,
                            sequence29        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE29%TYPE,
                            sequence30        GTT_FM_FINANCIAL_POSTINGS.SEQUENCE30%TYPE,
                            credit_debit      GTT_FM_FINANCIAL_POSTINGS.CREDIT_DEBIT%TYPE,
                            set_of_books_id   GTT_FM_FINANCIAL_POSTINGS.SET_OF_BOOKS_ID%TYPE,
                            org_unit          GTT_FM_FINANCIAL_POSTINGS.ORG_UNIT%TYPE,
                            tran_date         GTT_FM_FINANCIAL_POSTINGS.TRAN_DATE%TYPE,
                            account_id        GTT_FM_FINANCIAL_POSTINGS.ACCOUNT_ID%TYPE,
                            natural_acct_seg  GTT_FM_FINANCIAL_POSTINGS.SEQUENCE1%TYPE,
                            total_amount      NUMBER);
----------------------------------------------------------------------------------
FUNCTION FINANCIAL_ROLL_UP(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id    IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                          )
   return BOOLEAN is

   L_program             VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.FINANCIAL_ROLL_UP';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '|| I_fiscal_doc_id;
   ---
   L_gl_cross_ref_seq_no FM_GL_CROSS_REF.SEQ_NO%TYPE       := NULL;
   L_sob_id              FM_SOB_SETUP.SET_OF_BOOKS_ID%TYPE := NULL;
   L_coa_id              FM_COA_SETUP.COA_ID%TYPE          := NULL;
   L_tsf_entity_id       STORE.TSF_ENTITY_ID%TYPE          := NULL;
   L_org_unit_id         STORE.ORG_UNIT_ID%TYPE            := NULL;
   L_seq_no              NUMBER(10);
   ---
   I_debit_seg_rec    SEG_TYPE := NULL;
   I_credit_seg_rec   SEG_TYPE := NULL;
   seg_rec            SEG_TYPE := NULL;
   ---
   L_dynamic_ind       VARCHAR2(1) := NULL;
   L_coa_segment_count NUMBER(2)   := 0;
   ---
   CURSOR C_GET_TRAN_DATA is
      select ftd.fiscal_doc_no,
             ftd.tran_code,
             ftd.dept,
             ftd.class,
             ftd.subclass,
             ftd.location,
             ftd.location_type,
             ftd.ref_no_1,
             ftd.ref_no_2,
             ftd.ref_no_3,
             ftd.ref_no_4,
             ftd.ref_no_5,
             ftd.tran_date,
             sum(ftd.total_cost) as total_amount
        from fm_tran_data ftd,
             fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = ftd.fiscal_doc_id
         and fdh.fiscal_doc_id = I_fiscal_doc_id
    group by ftd.tran_code,
             ftd.dept,
             ftd.class,
             ftd.subclass,
             ftd.location,
             ftd.location_type,
             ftd.ref_no_1,
             ftd.ref_no_2,
             ftd.ref_no_3,
             ftd.ref_no_4,
             ftd.ref_no_5,
             ftd.fiscal_doc_no,
             ftd.tran_date
    order by ftd.tran_code;
   ---
   CURSOR C_GET_SOB is
      select fss.set_of_books_id
        from fm_sob_setup fss,
             mv_loc_sob sob,
             fm_fiscal_doc_header fdh
       where fss.set_of_books_id = sob.set_of_books_id
         and sob.location in (select primary_vwh from wh where wh=fdh.location_id and fdh.fiscal_doc_id = I_fiscal_doc_id
                              union select store from store where store=fdh.location_id and fdh.fiscal_doc_id = I_fiscal_doc_id)
         and fdh.location_type   = sob.location_type
         and fdh.fiscal_doc_id   = I_fiscal_doc_id;
   ---
   CURSOR C_GET_COA_ID (P_sob_id FM_SOB_SETUP.SET_OF_BOOKS_ID%TYPE) is
      select fss.coa_id
        from fm_sob_setup fss,
             fm_coa_setup fcs
       where fss.set_of_books_id  = P_sob_id
         and fcs.coa_id           = fss.coa_id;
   ---
   CURSOR C_GET_TSF_ENTITY is
      select distinct decode((select fdh.location_type
                                from fm_fiscal_doc_header
                               where fiscal_doc_id = I_fiscal_doc_id),L_st,st.tsf_entity_id,L_wh,wh.tsf_entity_id) as tsf_entity_id,
                      decode((select fdh.location_type
                                from fm_fiscal_doc_header
                               where fiscal_doc_id = I_fiscal_doc_id),L_st,st.org_unit_id,L_wh,wh.org_unit_id) as org_unit_id
               from store st,
                    wh wh,
                    fm_fiscal_doc_header fdh
              where fdh.fiscal_doc_id   = I_fiscal_doc_id
                and ((fdh.location_type = L_wh and fdh.location_id = wh.wh)
                  or (fdh.location_type = L_st and fdh.location_id = st.store));
   ---
   CURSOR C_GET_CROSS_REF(P_tran_code FM_TRAN_DATA.TRAN_CODE%TYPE,
                          P_ref_no_1 FM_TRAN_DATA.REF_NO_1%TYPE,
                          P_ref_no_2 FM_TRAN_DATA.REF_NO_2%TYPE,
                          P_ref_no_3 FM_TRAN_DATA.REF_NO_3%TYPE,
                          P_ref_no_4 FM_TRAN_DATA.REF_NO_4%TYPE,
                          P_ref_no_5 FM_TRAN_DATA.REF_NO_5%TYPE) is
      select seq_no , L_yes as dynamic_ind
        from fm_gl_cross_ref fgc
       where fgc.tran_code       = P_tran_code
         and fgc.set_of_books_id = L_sob_id
         and ((P_ref_no_1 is not null and fgc.ref_no_1 = P_ref_no_1) or (P_ref_no_1 is null and fgc.ref_no_1 is null))
         and ((P_ref_no_2 is not null and fgc.ref_no_2 = P_ref_no_2) or (P_ref_no_2 is null and fgc.ref_no_2 is null))
         and ((P_ref_no_3 is not null and fgc.ref_no_3 = P_ref_no_3) or (P_ref_no_3 is null and fgc.ref_no_3 is null))
         and ((P_ref_no_4 is not null and fgc.ref_no_4 = P_ref_no_4) or (P_ref_no_4 is null and fgc.ref_no_4 is null))
         and ((P_ref_no_5 is not null and fgc.ref_no_5 = P_ref_no_5) or (P_ref_no_5 is null and fgc.ref_no_5 is null))
         and seq_no in (select header_seq_no from fm_gl_dynamic_attributes)
      union
      select seq_no, L_no as dynamic_ind
        from fm_gl_cross_ref fgc
       where fgc.tran_code       = P_tran_code
         and fgc.set_of_books_id = L_sob_id
         and ((P_ref_no_1 is not null and fgc.ref_no_1 = P_ref_no_1) or (P_ref_no_1 is null and fgc.ref_no_1 is null))
         and ((P_ref_no_2 is not null and fgc.ref_no_2 = P_ref_no_2) or (P_ref_no_2 is null and fgc.ref_no_2 is null))
         and ((P_ref_no_3 is not null and fgc.ref_no_3 = P_ref_no_3) or (P_ref_no_3 is null and fgc.ref_no_3 is null))
         and ((P_ref_no_4 is not null and fgc.ref_no_4 = P_ref_no_4) or (P_ref_no_4 is null and fgc.ref_no_4 is null))
         and ((P_ref_no_5 is not null and fgc.ref_no_5 = P_ref_no_5) or (P_ref_no_5 is null and fgc.ref_no_5 is null))
         and not exists (select 1
                           from fm_gl_cross_ref fgc
                          where fgc.tran_code       = P_tran_code
                            and fgc.set_of_books_id = L_sob_id
                            and ((P_ref_no_1 is not null and fgc.ref_no_1 = P_ref_no_1) or (P_ref_no_1 is null and fgc.ref_no_1 is null))
                            and ((P_ref_no_2 is not null and fgc.ref_no_2 = P_ref_no_2) or (P_ref_no_2 is null and fgc.ref_no_2 is null))
                            and ((P_ref_no_3 is not null and fgc.ref_no_3 = P_ref_no_3) or (P_ref_no_3 is null and fgc.ref_no_3 is null))
                            and ((P_ref_no_4 is not null and fgc.ref_no_4 = P_ref_no_4) or (P_ref_no_4 is null and fgc.ref_no_4 is null))
                            and ((P_ref_no_5 is not null and fgc.ref_no_5 = P_ref_no_5) or (P_ref_no_5 is null and fgc.ref_no_5 is null))
                            and seq_no in (select header_seq_no from fm_gl_dynamic_attributes))
         and GET_DB_CR_COUNT(seq_no,L_coa_segment_count,L_cr) = L_coa_segment_count
         and GET_DB_CR_COUNT(seq_no,L_coa_segment_count,L_db) = L_coa_segment_count
      union
      select seq_no, L_no as dynamic_ind
        from fm_gl_cross_ref fgc
       where not exists (select 1
                           from fm_gl_cross_ref fgc
                          where fgc.tran_code = P_tran_code
                            and fgc.set_of_books_id = L_sob_id
                            and ((P_ref_no_1 is not null and fgc.ref_no_1 = P_ref_no_1) or (P_ref_no_1 is null and fgc.ref_no_1 is null))
                            and ((P_ref_no_2 is not null and fgc.ref_no_2 = P_ref_no_2) or (P_ref_no_2 is null and fgc.ref_no_2 is null))
                            and ((P_ref_no_3 is not null and fgc.ref_no_3 = P_ref_no_3) or (P_ref_no_3 is null and fgc.ref_no_3 is null))
                            and ((P_ref_no_4 is not null and fgc.ref_no_4 = P_ref_no_4) or (P_ref_no_4 is null and fgc.ref_no_4 is null))
                            and ((P_ref_no_5 is not null and fgc.ref_no_5 = P_ref_no_5) or (P_ref_no_5 is null and fgc.ref_no_5 is null)))
         and fgc.tran_code       = P_tran_code
         and fgc.set_of_books_id = L_sob_id
         and ref_no_1 is null
         and ref_no_2 is null
         and ref_no_3 is null
         and ref_no_4 is null
         and ref_no_5 is null;
   ---
   BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_SOB', 'fm_sob_setup/fm_fiscal_doc_header', L_key);
   open C_GET_SOB;
   SQL_LIB.SET_MARK('FETCH','C_GET_SOB', 'fm_sob_setup/fm_fiscal_doc_header', L_key);
   fetch C_GET_SOB into L_sob_id;
   SQL_LIB.SET_MARK('FETCH','C_GET_SOB', 'fm_sob_setup/fm_fiscal_doc_header', L_key);
   close C_GET_SOB;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_COA_ID', 'fm_coa_Setup/fm_sob_setup', 'P_sob_id' || L_sob_id);
   OPEN C_GET_COA_ID(L_sob_id);
   SQL_LIB.SET_MARK('FETCH','C_GET_COA_ID', 'fm_sob_setup/fm_sob_setup', 'P_sob_id' || L_sob_id);
   FETCH C_GET_COA_ID into L_coa_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_COA_ID', 'fm_sob_setup/fm_sob_setup', 'P_sob_id' || L_sob_id);
   close C_GET_COA_ID;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_TSF_ENTITY', 'fm_fiscal_doc_header', L_key);
   open C_GET_TSF_ENTITY;
   SQL_LIB.SET_MARK('OPEN','C_GET_TSF_ENTITY', 'fm_fiscal_doc_header', L_key);
   fetch C_GET_TSF_ENTITY into L_tsf_entity_id,L_org_unit_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_ENTITY', 'fm_fiscal_doc_header', L_key);
   close C_GET_TSF_ENTITY;
   ---
   L_coa_segment_count := FM_FINANCIAL_POSTING_SQL.GET_COA_SEGMENT_COUNT(O_error_message,L_coa_id);
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_DATA', 'fm_fiscal_doc_header', L_key);

   FOR L_get_tran_data IN c_get_tran_data
   LOOP
      L_gl_cross_ref_seq_no := NULL;
      open C_GET_CROSS_REF(L_get_tran_data.tran_code,
                           L_get_tran_data.ref_no_1,
                           L_get_tran_data.ref_no_2,
                           L_get_tran_data.ref_no_3,
                           L_get_tran_data.ref_no_4,
                           L_get_tran_data.ref_no_5);
      fetch C_GET_CROSS_REF into L_gl_cross_ref_seq_no, L_dynamic_ind;
      close C_GET_CROSS_REF;
      
      if L_gl_cross_ref_seq_no is not null then
         if FM_FINANCIAL_POSTING_SQL.GET_DB_CR_SEGMENTS(O_error_message      => O_error_message,
                                                        O_credit_seg_val_rec => I_credit_seg_rec,
                                                        O_debit_seg_val_rec  => I_debit_seg_rec,
                                                        I_tran_code          => L_get_tran_data.tran_code,
                                                        I_ref_no_1           => L_get_tran_data.ref_no_1,
                                                        I_ref_no_2           => L_get_tran_data.ref_no_2,
                                                        I_ref_no_3           => L_get_tran_data.ref_no_3,
                                                        I_ref_no_4           => L_get_tran_data.ref_no_4,
                                                        I_ref_no_5           => L_get_tran_data.ref_no_5,
                                                        I_seq_no             => L_gl_cross_ref_seq_no,
                                                        I_dynamic_ind        => L_dynamic_ind,
                                                        I_sob_id             => L_sob_id,
                                                        I_dept               => L_get_tran_data.dept,
                                                        I_class              => L_get_tran_data.class,
                                                        I_subclass           => L_get_tran_data.subclass,
                                                        I_location           => L_get_tran_data.location,
                                                        I_tsf_entity         => L_tsf_entity_id)= FALSE then
            return FALSE;
         end if;
         ---
         for i in 1..2 loop
            if i=1 then
               seg_rec := I_credit_seg_rec;
            else
               seg_rec := I_debit_seg_rec;
            end if;
            ---
            if FM_FINANCIAL_POSTING_SQL.INSERT_TEMP_TABLE(O_error_message => O_error_message
                                                         ,I_seq_no        => L_seq_no
                                                         ,I_sob_id        => L_sob_id
                                                         ,I_coa_id        => L_coa_id
                                                         ,I_org_unit_id   => L_org_unit_id
                                                         ,I_tran_code     => L_get_tran_data.tran_code
                                                         ,I_fiscal_doc_id => I_fiscal_doc_id
                                                         ,I_fiscal_doc_no => L_get_tran_data.fiscal_doc_no
                                                         ,I_dept          => L_get_tran_data.dept
                                                         ,I_class         => L_get_tran_data.class
                                                         ,I_subclass      => L_get_tran_data.subclass
                                                         ,I_location_type => L_get_tran_data.location_type
                                                         ,I_location      => L_get_tran_data.location
                                                         ,I_tsf_entity_id => L_tsf_entity_id
                                                         ,I_total_cost    => L_get_tran_data.total_amount
                                                         ,I_ref_no_1      => L_get_tran_data.ref_no_1
                                                         ,I_ref_no_2      => L_get_tran_data.ref_no_2
                                                         ,I_ref_no_3      => L_get_tran_data.ref_no_3
                                                         ,I_ref_no_4      => L_get_tran_data.ref_no_4
                                                         ,I_ref_no_5      => L_get_tran_data.ref_no_5
                                                         ,I_tran_date     => L_get_tran_data.tran_date
                                                         ,I_segment       => seg_rec) = FALSE then
               return FALSE;
            end if;
            ---
            if FM_FINANCIAL_POSTING_SQL.GET_ACCOUNT_ID(O_error_message => O_error_message
                                                      ,I_seq_no        => L_seq_no) = FALSE then
               return FALSE;
            end if;
            ---
         end loop;
         ---
      end if;
      ---
   end loop;
   ---
   
   if L_coa_id is not null then
      if FM_FINANCIAL_POSTING_SQL.POST_AP_FINANCIALS(O_error_message,
                                                     I_fiscal_doc_id,
                                                     L_coa_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
  return TRUE;

EXCEPTION
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      if C_GET_TRAN_DATA%ISOPEN then
         close C_GET_TRAN_DATA;
      end if;
      --
      if C_GET_SOB%ISOPEN then
         close C_GET_SOB;
      end if;
      --
      if C_GET_COA_ID%ISOPEN then
         close C_GET_COA_ID;
      end if;
      --
      if C_GET_TSF_ENTITY%ISOPEN then
         close C_GET_TSF_ENTITY;
      end if;
      --
      if C_GET_CROSS_REF%ISOPEN then
         close C_GET_CROSS_REF;
      end if;
      --
      return FALSE;

END FINANCIAL_ROLL_UP;
----------------------------------------------------------------------------------
FUNCTION GET_SEGMENT_VALUE (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_coa_id          IN FM_COA_SETUP.COA_ID%TYPE,
                            I_segment_no      IN NUMBER,
                            I_entity_name     IN FM_DYNAMIC_SEGMENT_SETUP.ENTITY_NAME%TYPE,
                            I_merch_entity_id IN FM_DYNAMIC_SEGMENT_SETUP.MERCH_ENTITY_ID%TYPE,
                            I_dept            IN FM_DYNAMIC_SEGMENT_SETUP.DEPT%TYPE,
                            I_class           IN FM_DYNAMIC_SEGMENT_SETUP.CLASS%TYPE)
   RETURN VARCHAR2 is
   ---
   L_program             VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.GET_SEGMENT_VALUE';
   L_segment_val         FM_DYNAMIC_SEGMENT_SETUP.SEGMENT_VALUE%TYPE;
   ---
   cursor C_GET_SEGMENT_VALUE is
      select segment_value
        from fm_dynamic_segment_setup
       where segment_no      = I_segment_no
         and coa_id          = I_coa_id
          or (entity_name     = I_entity_name
              and merch_entity_id = I_merch_entity_id
              and dept is NULL
              and class is NULL)
          or (entity_name     = I_entity_name
              and merch_entity_id = I_merch_entity_id
              and dept = I_dept
              and class is NULL)
          or (entity_name     = I_entity_name
              and merch_entity_id = I_merch_entity_id
              and dept = I_dept
              and class = I_class);
   ---
BEGIN
   ---
  open C_GET_SEGMENT_VALUE;
  fetch C_GET_SEGMENT_VALUE into L_segment_val;
  close C_GET_SEGMENT_VALUE;
   ---
   return L_segment_val;
   ---
EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      return L_err;
--
END GET_SEGMENT_VALUE;
----------------------------------------------------------------------------------
FUNCTION INSERT_TEMP_TABLE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_seq_no         IN OUT NUMBER
                          ,I_sob_id         IN     FM_SOB_SETUP.SET_OF_BOOKS_ID%TYPE
                          ,I_coa_id         IN OUT FM_COA_SETUP.COA_ID%TYPE
                          ,I_org_unit_id    IN     STORE.ORG_UNIT_ID%TYPE
                          ,I_tran_code      IN     FM_TRAN_DATA.TRAN_CODE%TYPE
                          ,I_fiscal_doc_id  IN     FM_TRAN_DATA.FISCAL_DOC_ID%TYPE
                          ,I_fiscal_doc_no  IN     FM_TRAN_DATA.FISCAL_DOC_NO%TYPE
                          ,I_dept           IN     FM_TRAN_DATA.DEPT%TYPE
                          ,I_class          IN     FM_TRAN_DATA.CLASS%TYPE
                          ,I_subclass       IN     FM_TRAN_DATA.SUBCLASS%TYPE
                          ,I_location_type  IN     FM_TRAN_DATA.LOCATION_TYPE%TYPE
                          ,I_location       IN     FM_TRAN_DATA.LOCATION%TYPE
                          ,I_tsf_entity_id  IN     STORE.TSF_ENTITY_ID%TYPE
                          ,I_total_cost     IN     FM_TRAN_DATA.TOTAL_COST%TYPE
                          ,I_ref_no_1       IN     FM_TRAN_DATA.REF_NO_1%TYPE
                          ,I_ref_no_2       IN     FM_TRAN_DATA.REF_NO_2%TYPE
                          ,I_ref_no_3       IN     FM_TRAN_DATA.REF_NO_3%TYPE
                          ,I_ref_no_4       IN     FM_TRAN_DATA.REF_NO_4%TYPE
                          ,I_ref_no_5       IN     FM_TRAN_DATA.REF_NO_5%TYPE
                          ,I_tran_date      IN     FM_TRAN_DATA.TRAN_DATE%TYPE
                          ,I_segment        IN OUT SEG_TYPE)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.INSERT_TEMP_TABLE';

   L_seq_no            NUMBER(10);
   L_segment           seg_type := NULL;
   L_coa_segment_count NUMBER(2) := 0;
   ---
   cursor C_SEQ_FIN_POSTING is
      select FM_GTT_FIN_SEQ.NEXTVAL seq_no
        from dual;
   ---
BEGIN
--
if (I_total_cost <> 0) then
   open C_SEQ_FIN_POSTING;
   fetch C_SEQ_FIN_POSTING into L_seq_no;
   close C_SEQ_FIN_POSTING;
   ---
   L_coa_segment_count := FM_FINANCIAL_POSTING_SQL.GET_COA_SEGMENT_COUNT(O_error_message,I_coa_id);
   ---
   if L_coa_segment_count > 0 then
      for i in (L_coa_segment_count+1)..30 loop
         I_segment.L_seg_val_tab(i).segment_value := NULL;
      end loop;
   end if;
   ---
   INSERT INTO GTT_FM_FINANCIAL_POSTINGS(SEQ_NO
                                        ,SET_OF_BOOKS_ID
                                        ,ORG_UNIT
                                        ,TRAN_CODE
                                        ,FISCAL_DOC_ID
                                        ,FISCAL_DOC_NO
                                        ,DEPT
                                        ,CLASS
                                        ,SUBCLASS
                                        ,LOCATION_TYPE
                                        ,LOCATION
                                        ,TSF_ENTITY_ID
                                        ,TOTAL_COST
                                        ,REF_NO_1
                                        ,REF_NO_2
                                        ,REF_NO_3
                                        ,REF_NO_4
                                        ,REF_NO_5
                                        ,CREDIT_DEBIT
                                        ,SEQUENCE1
                                        ,SEQUENCE2
                                        ,SEQUENCE3
                                        ,SEQUENCE4
                                        ,SEQUENCE5
                                        ,SEQUENCE6
                                        ,SEQUENCE7
                                        ,SEQUENCE8
                                        ,SEQUENCE9
                                        ,SEQUENCE10
                                        ,SEQUENCE11
                                        ,SEQUENCE12
                                        ,SEQUENCE13
                                        ,SEQUENCE14
                                        ,SEQUENCE15
                                        ,SEQUENCE16
                                        ,SEQUENCE17
                                        ,SEQUENCE18
                                        ,SEQUENCE19
                                        ,SEQUENCE20
                                        ,SEQUENCE21
                                        ,SEQUENCE22
                                        ,SEQUENCE23
                                        ,SEQUENCE24
                                        ,SEQUENCE25
                                        ,SEQUENCE26
                                        ,SEQUENCE27
                                        ,SEQUENCE28
                                        ,SEQUENCE29
                                        ,SEQUENCE30
                                        ,TRAN_DATE
                                        ,CREATE_DATETIME
                                        ,CREATE_ID
                                        ,LAST_UPDATE_DATETIME
                                        ,LAST_UPDATE_ID
                                        )VALUES
                                        (L_seq_no
                                        ,I_sob_id
                                        ,I_org_unit_id
                                        ,I_tran_code
                                        ,I_fiscal_doc_id
                                        ,I_fiscal_doc_no
                                        ,I_dept
                                        ,I_class
                                        ,I_subclass
                                        ,I_location_type
                                        ,I_location
                                        ,I_tsf_entity_id
                                        ,I_total_cost
                                        ,I_ref_no_1
                                        ,I_ref_no_2
                                        ,I_ref_no_3
                                        ,I_ref_no_4
                                        ,I_ref_no_5
                                        ,I_segment.credit_debit
                                        ,I_segment.L_seg_val_tab(1).segment_value
                                        ,I_segment.L_seg_val_tab(2).segment_value
                                        ,I_segment.L_seg_val_tab(3).segment_value
                                        ,I_segment.L_seg_val_tab(4).segment_value
                                        ,I_segment.L_seg_val_tab(5).segment_value
                                        ,I_segment.L_seg_val_tab(6).segment_value
                                        ,I_segment.L_seg_val_tab(7).segment_value
                                        ,I_segment.L_seg_val_tab(8).segment_value
                                        ,I_segment.L_seg_val_tab(9).segment_value
                                        ,I_segment.L_seg_val_tab(10).segment_value
                                        ,I_segment.L_seg_val_tab(11).segment_value
                                        ,I_segment.L_seg_val_tab(12).segment_value
                                        ,I_segment.L_seg_val_tab(13).segment_value
                                        ,I_segment.L_seg_val_tab(14).segment_value
                                        ,I_segment.L_seg_val_tab(15).segment_value
                                        ,I_segment.L_seg_val_tab(16).segment_value
                                        ,I_segment.L_seg_val_tab(17).segment_value
                                        ,I_segment.L_seg_val_tab(18).segment_value
                                        ,I_segment.L_seg_val_tab(19).segment_value
                                        ,I_segment.L_seg_val_tab(20).segment_value
                                        ,I_segment.L_seg_val_tab(21).segment_value
                                        ,I_segment.L_seg_val_tab(22).segment_value
                                        ,I_segment.L_seg_val_tab(23).segment_value
                                        ,I_segment.L_seg_val_tab(24).segment_value
                                        ,I_segment.L_seg_val_tab(25).segment_value
                                        ,I_segment.L_seg_val_tab(26).segment_value
                                        ,I_segment.L_seg_val_tab(27).segment_value
                                        ,I_segment.L_seg_val_tab(28).segment_value
                                        ,I_segment.L_seg_val_tab(29).segment_value
                                        ,I_segment.L_seg_val_tab(30).segment_value
                                        ,I_tran_date
                                        ,sysdate
                                        ,user
                                        ,sysdate
                                        ,user);
 end if;
--
   I_seq_no := L_seq_no;

   return TRUE;
EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END INSERT_TEMP_TABLE;
----------------------------------------------------------------------------------
FUNCTION UPDATE_NF_STATUS_FP( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                            ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.UPDATE_NF_STATUS_F';
   L_key                 VARCHAR2(80) := 'fiscal_doc_id = '||I_fiscal_doc_id;
--
   L_financials_posted           VARCHAR2(2):= 'FP';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   l_dummy        NUMBER;
   L_count        NUMBER;
   L_schedule     FM_SCHEDULE.SCHEDULE_NO%TYPE;
--
   CURSOR C_UPDATE_NF is
      SELECT 1
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.fiscal_doc_id = I_fiscal_doc_id
       for update nowait;
--
   CURSOR c_update_schedule is
      SELECT count(1)
            ,fdh.schedule_no
        FROM fm_fiscal_doc_header fdh
       WHERE fdh.schedule_no = (SELECT schedule_no FROM fm_fiscal_doc_header WHERE fiscal_doc_id = I_fiscal_doc_id)
         AND status != L_financials_posted
    GROUP BY schedule_no;
--
BEGIN
--
   SQL_LIB.SET_MARK('OPEN','C_UPDATE_NF', 'fm_fiscal_doc_header', L_key);
   OPEN C_UPDATE_NF;
   SQL_LIB.SET_MARK('FETCH','C_UPDATE_NF', 'fm_fiscal_doc_header', L_key);
   FETCH C_UPDATE_NF INTO l_dummy;
   SQL_LIB.SET_MARK('CLOSE','C_UPDATE_NF', 'fm_fiscal_doc_header', L_key);
   CLOSE C_UPDATE_NF;
--
   UPDATE fm_fiscal_doc_header
      SET status        = L_financials_posted
    WHERE fiscal_doc_id = I_fiscal_doc_id;
--
   SQL_LIB.SET_MARK('OPEN','C_UPDATE_SCHEDULE', 'fm_fiscal_doc_header', L_key);
   OPEN C_UPDATE_SCHEDULE;
   SQL_LIB.SET_MARK('FETCH','C_UPDATE_SCHEDULE', 'fm_fiscal_doc_header', L_key);
   FETCH C_UPDATE_SCHEDULE INTO L_count, L_schedule;
   SQL_LIB.SET_MARK('CLOSE','C_UPDATE_SCHEDULE', 'fm_fiscal_doc_header', L_key);
   CLOSE C_UPDATE_SCHEDULE;
--
   if nvl(L_count,0) < 1 then
      UPDATE fm_schedule
         SET status      = L_financials_posted
       WHERE schedule_no = (SELECT schedule_no FROM fm_fiscal_doc_header WHERE fiscal_doc_id = I_fiscal_doc_id);
   end if;
--
   return TRUE;
--
EXCEPTION
---
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'fm_fiscal_doc_header',
                                            I_fiscal_doc_id,
                                            NULL);
      return FALSE;
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_UPDATE_NF%ISOPEN then
         close C_UPDATE_NF;
      end if;
--
      return FALSE;
END UPDATE_NF_STATUS_FP;
----------------------------------------------------------------------------------
FUNCTION GET_COA_SEGMENT_COUNT(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_coa_id           IN OUT FM_COA_SETUP.COA_ID%TYPE)
   return NUMBER is

   L_program   VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.GET_COA_SEGMENT_COUNT';
   L_coa_id    FM_COA_SETUP.COA_ID%TYPE;
   L_seg_count NUMBER :=0;
   i           NUMBER;
   L_segment   VARCHAR2(1000);
   type refcursor is REF CURSOR;
   C_GET_SEGMENT   refcursor;
   ---
   TYPE SEG_ARRAY IS VARRAY(30) of FM_COA_SETUP.SEGMENT1_DESC%TYPE;
   SEG_REC SEG_ARRAY := SEG_ARRAY();
   ---
BEGIN
   for i in 1..30 loop
      L_segment  := 'SELECT SEGMENT'||i||'_DESC FROM FM_COA_SETUP WHERE COA_ID='||I_coa_id;
      open C_GET_SEGMENT
       for L_segment;
      SEG_REC.extend;
      fetch C_GET_SEGMENT into SEG_REC(i);
      close C_GET_SEGMENT;
      L_seg_count := L_seg_count + 1;
      exit when SEG_REC(i) is NULL;
   end loop;
   return (L_seg_count-1);
--
EXCEPTION
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--

      if C_GET_SEGMENT%ISOPEN then
         close C_GET_SEGMENT;
      end if;
--
      return -1;
--
END GET_COA_SEGMENT_COUNT;
------------------------------------------------------------------------------------------------------------
FUNCTION GET_DB_CR_COUNT(I_seq_no           IN     FM_GL_CROSS_REF.SEQ_NO%TYPE,
                         I_coa_seg_count    IN     NUMBER,
                         I_cr_db_type         IN     VARCHAR2)
   return NUMBER is

   L_program   VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.GET_DB_CR_COUNT';

   i           NUMBER;
   L_count     NUMBER(2) := 0;
   L_segment   FM_GL_CROSS_REF.DB_SEQUENCE1%TYPE := NULL;
   L_select    VARCHAR2(1000);
   ---
   type refcursor is REF CURSOR;
   C_GET_SEGMENT   refcursor;
   ---
BEGIN
   
   for i in 1..I_coa_seg_count loop
      L_segment := NULL;
      L_select  := NULL;
      L_select  := 'SELECT '|| I_cr_db_type ||'_SEQUENCE'||i||' FROM FM_GL_CROSS_REF WHERE seq_no = '|| I_seq_no;
       open C_GET_SEGMENT for L_select;
      fetch C_GET_SEGMENT into L_segment;
      ---
      
      if L_segment is not null then
         L_count := L_count + 1;
      end if;
      ---
      close C_GET_SEGMENT;
   end loop;
   ---
   
   return L_count;
--
EXCEPTION
---
   when OTHERS then
      if C_GET_SEGMENT%ISOPEN then
         close C_GET_SEGMENT;
      end if;
--
   return -1;
--
END GET_DB_CR_COUNT;
------------------------------------------------------------------------------------------------------------
FUNCTION GET_DB_CR_SEGMENTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_credit_seg_val_rec IN OUT SEG_TYPE,
                            O_debit_seg_val_rec  IN OUT SEG_TYPE,
                            I_tran_code          IN     FM_TRAN_DATA.TRAN_CODE%TYPE,
                            I_ref_no_1           IN     FM_TRAN_DATA.REF_NO_1%TYPE,
                            I_ref_no_2           IN     FM_TRAN_DATA.REF_NO_2%TYPE,
                            I_ref_no_3           IN     FM_TRAN_DATA.REF_NO_3%TYPE,
                            I_ref_no_4           IN     FM_TRAN_DATA.REF_NO_4%TYPE,
                            I_ref_no_5           IN     FM_TRAN_DATA.REF_NO_5%TYPE,
                            I_seq_no             IN OUT FM_GL_CROSS_REF.SEQ_NO%TYPE,
                            I_dynamic_ind        IN     VARCHAR2,
                            I_sob_id             IN     FM_GL_CROSS_REF.SET_OF_BOOKS_ID%TYPE,
                            I_dept               IN     FM_TRAN_DATA.DEPT%TYPE,
                            I_class              IN     FM_TRAN_DATA.CLASS%TYPE,
                            I_subclass           IN     FM_TRAN_DATA.CLASS%TYPE,
                            I_location           IN     FM_TRAN_DATA.LOCATION%TYPE,
                            I_tsf_entity         IN     STORE.TSF_ENTITY_ID%TYPE)
   return BOOLEAN is

   L_program            VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.GET_DB_CR_SEGMENTS';
   L_entity_name        FM_GL_DYNAMIC_ATTRIBUTES.ENTITY_NAME%type  := NULL;
   L_seq_no             NUMBER := 0;
   L_coa_segment_count  NUMBER := 0;
   ---
   CURSOR C_CHK_DYNAMIC is
      select fgda.credit_debit,
             fgda.segment_no,
             fgda.entity_name,
             fgr.tran_code
        from fm_gl_dynamic_attributes fgda,
             fm_gl_cross_ref fgr
       where fgr.seq_no = fgda.header_seq_no
         and fgr.seq_no = I_seq_no;
   ---
   CURSOR C_GET_COA_ID is
      select fss.coa_id
        from fm_sob_setup fss,
             fm_coa_setup fcs
       where fss.set_of_books_id  = I_sob_id
         and fcs.coa_id           = fss.coa_id;
   ---
   CURSOR C_GET_DYNAMIC_ATTRIB is
      select fgo.dynamic_attribute
        from fm_gl_options fgo,
             fm_gl_dynamic_attributes fgda
       where fgo.entity_name    = fgda.entity_name
         and fgo.entity_name    = L_entity_name
         and fgo.dynamic_ind    = L_yes
         and fgda.header_seq_no = I_seq_no;
   ---
   CURSOR C_GET_SEGMENT_VALUES (P_seg_count number,P_Credit_debit varchar2,P_seq_no number) is
      select decode(P_seg_count,1,db_sequence1,
                                2,db_sequence2,
                                3,db_sequence3,
                                4,db_sequence4,
                                5,db_sequence5,
                                6,db_sequence6,
                                7,db_sequence7,
                                8,db_sequence8,
                                9,db_sequence9,
                                10,db_sequence10,
                                11,db_sequence11,
                                12,db_sequence12,
                                13,db_sequence13,
                                14,db_sequence14,
                                15,db_sequence15,
                                16,db_sequence16,
                                17,db_sequence17,
                                18,db_sequence18,
                                19,db_sequence19,
                                20,db_sequence20,
                                21,db_sequence21,
                                22,db_sequence22,
                                23,db_sequence23,
                                24,db_sequence24,
                                25,db_sequence25,
                                26,db_sequence26,
                                27,db_sequence27,
                                28,db_sequence28,
                                29,db_sequence29,
                                30,db_sequence30) as segment
      from fm_gl_cross_ref
     where seq_no = P_seq_no
       and P_Credit_debit = 'D'
     union
     select decode(P_seg_count,1,cr_sequence1,
                               2,cr_sequence2,
                               3,cr_sequence3,
                               4,cr_sequence4,
                               5,cr_sequence5,
                               6,cr_sequence6,
                               7,cr_sequence7,
                               8,cr_sequence8,
                               9,cr_sequence9,
                               10,cr_sequence10,
                               11,cr_sequence11,
                               12,cr_sequence12,
                               13,cr_sequence13,
                               14,cr_sequence14,
                               15,cr_sequence15,
                               16,cr_sequence16,
                               17,cr_sequence17,
                               18,cr_sequence18,
                               19,cr_sequence19,
                               20,cr_sequence20,
                               21,cr_sequence21,
                               22,cr_sequence22,
                               23,cr_sequence23,
                               24,cr_sequence24,
                               25,cr_sequence25,
                               26,cr_sequence26,
                               27,cr_sequence27,
                               28,cr_sequence28,
                               29,cr_sequence29,
                               30,cr_sequence30) as segment
       from fm_gl_cross_ref
      where seq_no = P_seq_no
        and P_Credit_debit = 'C';
   ---
   cursor C_GET_GENERIC_ACCT_SEQ is
      select seq_no
        from fm_gl_cross_ref fgc
       where fgc.tran_code       = I_tran_code
         and fgc.set_of_books_id = I_sob_id
         and ((I_ref_no_1 is not null and fgc.ref_no_1 = I_ref_no_1) or (I_ref_no_1 is null and fgc.ref_no_1 is null))
         and ((I_ref_no_2 is not null and fgc.ref_no_2 = I_ref_no_2) or (I_ref_no_2 is null and fgc.ref_no_2 is null))
         and ((I_ref_no_3 is not null and fgc.ref_no_3 = I_ref_no_3) or (I_ref_no_3 is null and fgc.ref_no_3 is null))
         and ((I_ref_no_4 is not null and fgc.ref_no_4 = I_ref_no_4) or (I_ref_no_4 is null and fgc.ref_no_4 is null))
         and ((I_ref_no_5 is not null and fgc.ref_no_5 = I_ref_no_5) or (I_ref_no_5 is null and fgc.ref_no_5 is null))
         and seq_no not in (select seq_no
                           from fm_gl_cross_ref fgc
                          where fgc.tran_code       = I_tran_code
                            and fgc.set_of_books_id = I_sob_id
                            and ((I_ref_no_1 is not null and fgc.ref_no_1 = I_ref_no_1) or (I_ref_no_1 is null and fgc.ref_no_1 is null))
                            and ((I_ref_no_2 is not null and fgc.ref_no_2 = I_ref_no_2) or (I_ref_no_2 is null and fgc.ref_no_2 is null))
                            and ((I_ref_no_3 is not null and fgc.ref_no_3 = I_ref_no_3) or (I_ref_no_3 is null and fgc.ref_no_3 is null))
                            and ((I_ref_no_4 is not null and fgc.ref_no_4 = I_ref_no_4) or (I_ref_no_4 is null and fgc.ref_no_4 is null))
                            and ((I_ref_no_5 is not null and fgc.ref_no_5 = I_ref_no_5) or (I_ref_no_5 is null and fgc.ref_no_5 is null))
                            and seq_no in (select header_seq_no from fm_gl_dynamic_attributes))
         and GET_DB_CR_COUNT(seq_no,L_coa_segment_count,L_cr) = L_coa_segment_count
         and GET_DB_CR_COUNT(seq_no,L_coa_segment_count,L_db) = L_coa_segment_count
      union
      select seq_no
        from fm_gl_cross_ref fgc
       where not exists (select 1
                           from fm_gl_cross_ref fgc
                          where fgc.tran_code       = I_tran_code
                            and fgc.set_of_books_id = I_sob_id
                            and ((I_ref_no_1 is not null and fgc.ref_no_1 = I_ref_no_1) or (I_ref_no_1 is null and fgc.ref_no_1 is null))
                            and ((I_ref_no_2 is not null and fgc.ref_no_2 = I_ref_no_2) or (I_ref_no_2 is null and fgc.ref_no_2 is null))
                            and ((I_ref_no_3 is not null and fgc.ref_no_3 = I_ref_no_3) or (I_ref_no_3 is null and fgc.ref_no_3 is null))
                            and ((I_ref_no_4 is not null and fgc.ref_no_4 = I_ref_no_4) or (I_ref_no_4 is null and fgc.ref_no_4 is null))
                            and ((I_ref_no_5 is not null and fgc.ref_no_5 = I_ref_no_5) or (I_ref_no_5 is null and fgc.ref_no_5 is null)))
         and fgc.tran_code       = I_tran_code
         and fgc.set_of_books_id = I_sob_id
         and ref_no_1 is null
         and ref_no_2 is null
         and ref_no_3 is null
         and ref_no_4 is null
         and ref_no_5 is null;

   L_credit_debit       FM_GL_DYNAMIC_ATTRIBUTES.CREDIT_DEBIT%type    := NULL;
   L_segment_no         FM_GL_DYNAMIC_ATTRIBUTES.SEGMENT_NO%type      := NULL;
   L_coa_id             FM_COA_SETUP.COA_ID%TYPE                      := NULL;
   L_seg_val            FM_DYNAMIC_SEGMENT_SETUP.SEGMENT_VALUE%TYPE   := NULL;
   L_tran_Code          FM_TRAN_CODES.TRAN_CODE%TYPE                  := NULL;
   L_dynamic_attribute  FM_GL_OPTIONS.DYNAMIC_ATTRIBUTE%TYPE          := NULL;
   L_merch_entity_id    FM_DYNAMIC_SEGMENT_SETUP.MERCH_ENTITY_ID%TYPE := NULL;
   ---
   L_credit_seg_val_rec SEG_TYPE;
   L_debit_seg_val_rec  SEG_TYPE;

BEGIN

    open C_GET_COA_ID;
   fetch C_GET_COA_ID into L_coa_id;
   close C_GET_COA_ID;

   L_coa_segment_count := FM_FINANCIAL_POSTING_SQL.GET_COA_SEGMENT_COUNT(O_error_message,L_coa_id);

   L_credit_seg_val_rec.credit_debit := 'C';
   L_debit_seg_val_rec.credit_debit  := 'D';
   L_seq_no := I_seq_no;
   if I_dynamic_ind = L_yes then
      open C_CHK_DYNAMIC;
      LOOP

         L_credit_debit := NULL;
         L_segment_no   := NULL;
         L_entity_name  := NULL;

      fetch C_CHK_DYNAMIC into L_credit_debit,L_segment_no,L_entity_name,L_tran_Code;
      exit when C_CHK_DYNAMIC%NOTFOUND;
      ---
      if C_CHK_DYNAMIC%FOUND then
          open C_GET_DYNAMIC_ATTRIB;
         fetch C_GET_DYNAMIC_ATTRIB into L_dynamic_attribute;
         close C_GET_DYNAMIC_ATTRIB;

            if L_dynamic_attribute = 'DEPT' then
               L_merch_entity_id  := I_dept;
            elsif L_dynamic_attribute = 'CLASS' then
               L_merch_entity_id  := I_class;
            elsif L_dynamic_attribute  = 'SUBC' then
               L_merch_entity_id  := I_subclass;
            elsif L_dynamic_attribute  = 'LOCN' then
               L_merch_entity_id  := I_location;
            elsif L_dynamic_attribute  = 'TSFENT' then
               L_merch_entity_id  := I_tsf_entity;
            end if;
            ---
            L_seg_val := GET_SEGMENT_VALUE(O_error_message,
                                           L_coa_id,
                                           L_segment_no,
                                           L_entity_name,
                                           L_merch_entity_id,
                                           I_dept,
                                           I_class);
            if L_seg_val != L_err and L_seg_val is not null then
               if L_credit_debit = 'C' then
                  L_credit_seg_val_rec.L_seg_val_tab(L_segment_no).segment_value := L_seg_val;
               elsif L_credit_debit = 'D' then
                  L_debit_seg_val_rec.L_seg_val_tab(L_segment_no).segment_value := L_seg_val;
               end if;
               ---
            elsif L_seg_val is null then
               L_credit_seg_val_rec.L_seg_val_tab.DELETE;
               L_debit_seg_val_rec.L_seg_val_tab.DELETE;
                open C_GET_GENERIC_ACCT_SEQ;
               fetch C_GET_GENERIC_ACCT_SEQ into L_seq_no;
               close C_GET_GENERIC_ACCT_SEQ;
               exit when L_seg_val is NULL;
            elsif L_seg_val = L_err then
               L_credit_seg_val_rec.L_seg_val_tab.DELETE;
               L_debit_seg_val_rec.L_seg_val_tab.DELETE;
               return FALSE;
            end if;
            ---
      end if;
      ---
      END LOOP;
      close C_CHK_DYNAMIC;
      ---
   end if;
   ---
   if L_coa_segment_count > 0 then
      for i in 1..L_coa_segment_count loop
         if not L_credit_seg_val_rec.L_seg_val_tab.exists(i) then
            open C_GET_SEGMENT_VALUES(i,'C',L_seq_no);
           fetch C_GET_SEGMENT_VALUES into L_credit_seg_val_rec.L_seg_val_tab(i).segment_value;
           close C_GET_SEGMENT_VALUES;
         end if;
         ---
      end loop;
      ---
      for i in 1..L_coa_segment_count loop
         if not L_debit_seg_val_rec.L_seg_val_tab.exists(i) then
            open C_GET_SEGMENT_VALUES(i,'D',L_seq_no);
           fetch C_GET_SEGMENT_VALUES into L_debit_seg_val_rec.L_seg_val_tab(i).segment_value;
           close C_GET_SEGMENT_VALUES;
         end if;
         ---
      end loop;
      ---
   end if;
   ---
   ---
   O_credit_seg_val_rec := L_credit_seg_val_rec;
   O_debit_seg_val_rec  := L_debit_seg_val_rec;
   ---
   L_credit_seg_val_rec.L_seg_val_tab.DELETE;
   L_debit_seg_val_rec.L_seg_val_tab.DELETE;

   return TRUE;
--
EXCEPTION
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_CHK_DYNAMIC%ISOPEN then
         close C_CHK_DYNAMIC;
      end if;
--
      if C_GET_COA_ID%ISOPEN then
         close C_GET_COA_ID;
      end if;
--
      if C_GET_DYNAMIC_ATTRIB%ISOPEN then
         close C_GET_DYNAMIC_ATTRIB;
      end if;
--
      if C_GET_SEGMENT_VALUES%ISOPEN then
         close C_GET_SEGMENT_VALUES;
      end if;
--
      return FALSE;
--
END GET_DB_CR_SEGMENTS;
------------------------------------------------------------------------------------------------------------
FUNCTION GET_ACCOUNT_ID(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                       ,I_seq_no         IN NUMBER)
   return BOOLEAN is
--
   L_program             VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.GET_ACCOUNT_ID';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100) := 'GTT_FM_FINANCIAL_POSTONGS';
   L_key          VARCHAR2(100) := 'seq_no ' || TO_CHAR(I_seq_no);
   ---

   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   i                     NUMBER :=0;

   CURSOR C_GET_FIN_POSTINGS is
      select credit_debit,
             sequence1,
             sequence2,
             sequence3,
             sequence4,
             sequence5,
             sequence6,
             sequence7,
             sequence8,
             sequence9,
             sequence10,
             sequence11,
             sequence12,
             sequence13,
             sequence14,
             sequence15,
             sequence16,
             sequence17,
             sequence18,
             sequence19,
             sequence20,
             sequence21,
             sequence22,
             sequence23,
             sequence24,
             sequence25,
             sequence26,
             sequence27,
             sequence28,
             sequence29,
             sequence30
        from gtt_fm_financial_postings
       where seq_no = I_seq_no;

   L_fin_data C_GET_FIN_POSTINGS%ROWTYPE;

  cursor C_GET_ACCOUNT_ID is
      select account_id
        from fm_account_setup
       where ((L_fin_data.sequence1 is null and attribute1 is null) or (L_fin_data.sequence1 is not null and attribute1 is not null and attribute1 = L_fin_data.sequence1))
         and ((L_fin_data.sequence2 is null and attribute2 is null) or (L_fin_data.sequence2 is not null and attribute2 is not null and attribute2 = L_fin_data.sequence2))
         and ((L_fin_data.sequence3 is null and attribute3 is null) or (L_fin_data.sequence3 is not null and attribute3 is not null and attribute3 = L_fin_data.sequence3))
         and ((L_fin_data.sequence4 is null and attribute4 is null) or (L_fin_data.sequence4 is not null and attribute4 is not null and attribute4 = L_fin_data.sequence4))
         and ((L_fin_data.sequence5 is null and attribute5 is null) or (L_fin_data.sequence5 is not null and attribute5 is not null and attribute5 = L_fin_data.sequence5))
         and ((L_fin_data.sequence6 is null and attribute6 is null) or (L_fin_data.sequence6 is not null and attribute6 is not null and attribute6 = L_fin_data.sequence6))
         and ((L_fin_data.sequence7 is null and attribute7 is null) or (L_fin_data.sequence7 is not null and attribute7 is not null and attribute7 = L_fin_data.sequence7))
         and ((L_fin_data.sequence8 is null and attribute8 is null) or (L_fin_data.sequence8 is not null and attribute8 is not null and attribute8 = L_fin_data.sequence8))
         and ((L_fin_data.sequence9 is null and attribute9 is null) or (L_fin_data.sequence9 is not null and attribute9 is not null and attribute9 = L_fin_data.sequence9))
         and ((L_fin_data.sequence10 is null and attribute10 is null) or (L_fin_data.sequence10 is not null and attribute10 is not null and attribute10 = L_fin_data.sequence10))
         and ((L_fin_data.sequence11 is null and attribute11 is null) or (L_fin_data.sequence11 is not null and attribute11 is not null and attribute11 = L_fin_data.sequence11))
         and ((L_fin_data.sequence12 is null and attribute12 is null) or (L_fin_data.sequence12 is not null and attribute12 is not null and attribute12 = L_fin_data.sequence12))
         and ((L_fin_data.sequence13 is null and attribute13 is null) or (L_fin_data.sequence13 is not null and attribute13 is not null and attribute13 = L_fin_data.sequence13))
         and ((L_fin_data.sequence14 is null and attribute14 is null) or (L_fin_data.sequence14 is not null and attribute14 is not null and attribute14 = L_fin_data.sequence14))
         and ((L_fin_data.sequence15 is null and attribute15 is null) or (L_fin_data.sequence15 is not null and attribute15 is not null and attribute15 = L_fin_data.sequence15))
         and ((L_fin_data.sequence16 is null and attribute16 is null) or (L_fin_data.sequence16 is not null and attribute16 is not null and attribute16 = L_fin_data.sequence16))
         and ((L_fin_data.sequence17 is null and attribute17 is null) or (L_fin_data.sequence17 is not null and attribute17 is not null and attribute17 = L_fin_data.sequence17))
         and ((L_fin_data.sequence18 is null and attribute18 is null) or (L_fin_data.sequence18 is not null and attribute18 is not null and attribute18 = L_fin_data.sequence18))
         and ((L_fin_data.sequence19 is null and attribute19 is null) or (L_fin_data.sequence19 is not null and attribute19 is not null and attribute19 = L_fin_data.sequence19))
         and ((L_fin_data.sequence20 is null and attribute20 is null) or (L_fin_data.sequence20 is not null and attribute20 is not null and attribute20 = L_fin_data.sequence20))
         and ((L_fin_data.sequence21 is null and attribute21 is null) or (L_fin_data.sequence21 is not null and attribute21 is not null and attribute21 = L_fin_data.sequence21))
         and ((L_fin_data.sequence22 is null and attribute22 is null) or (L_fin_data.sequence22 is not null and attribute22 is not null and attribute22 = L_fin_data.sequence22))
         and ((L_fin_data.sequence23 is null and attribute23 is null) or (L_fin_data.sequence23 is not null and attribute23 is not null and attribute23 = L_fin_data.sequence23))
         and ((L_fin_data.sequence24 is null and attribute24 is null) or (L_fin_data.sequence24 is not null and attribute24 is not null and attribute24 = L_fin_data.sequence24))
         and ((L_fin_data.sequence25 is null and attribute25 is null) or (L_fin_data.sequence25 is not null and attribute25 is not null and attribute25 = L_fin_data.sequence25))
         and ((L_fin_data.sequence26 is null and attribute26 is null) or (L_fin_data.sequence26 is not null and attribute26 is not null and attribute26 = L_fin_data.sequence26))
         and ((L_fin_data.sequence27 is null and attribute27 is null) or (L_fin_data.sequence27 is not null and attribute27 is not null and attribute27 = L_fin_data.sequence27))
         and ((L_fin_data.sequence28 is null and attribute28 is null) or (L_fin_data.sequence28 is not null and attribute28 is not null and attribute28 = L_fin_data.sequence28))
         and ((L_fin_data.sequence29 is null and attribute29 is null) or (L_fin_data.sequence29 is not null and attribute29 is not null and attribute29 = L_fin_data.sequence29))
         and ((L_fin_data.sequence30 is null and attribute30 is null) or (L_fin_data.sequence30 is not null and attribute30 is not null and attribute30 = L_fin_data.sequence30));

   L_acct_id FM_ACCOUNT_SETUP.ACCOUNT_ID%TYPE;

   cursor C_LOCK_TEMP_TABLE is
      select 'X'
        from gtt_fm_financial_postings
       where seq_no = I_seq_no
         for update nowait;

BEGIN

   L_fin_data := NULL;

   open C_GET_FIN_POSTINGS;
   fetch C_GET_FIN_POSTINGS into L_fin_data;
   close C_GET_FIN_POSTINGS;

   L_acct_id := NULL;

   open C_GET_ACCOUNT_ID;
   fetch C_GET_ACCOUNT_ID into L_acct_id;
   close C_GET_ACCOUNT_ID;
   ---
   if L_acct_id is null then
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_NO_ACCT_EXISTS',NULL,NULL,NULL);
      return FALSE;
   else
      L_cursor := 'C_LOCK_TEMP_TABLE';
      ---
      SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
      open C_LOCK_TEMP_TABLE;
      SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
      close C_LOCK_TEMP_TABLE;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
      ---
      update gtt_fm_financial_postings
         set account_id    = L_acct_id
       where seq_no        = I_seq_no;
   end if;
   ---
   return TRUE;

EXCEPTION
--
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      ---
      return FALSE;
--
END GET_ACCOUNT_ID;
----------------------------------------------------------------------------------
FUNCTION POST_AP_FINANCIALS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id      IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           ,I_coa_id             IN     FM_COA_SETUP.COA_ID%TYPE)
   return BOOLEAN is

   L_program             VARCHAR2(80) := 'FM_FINANCIAL_POSTING_SQL.POST_AP_FINANCIALS';

   cursor C_GET_NATURAL_ACCT_SEG is
      select fcs.natural_acct_segment
        from fm_coa_setup fcs
       where fcs.coa_id = I_coa_id;
   ---
   cursor C_SEQ_AP_STAGE_HEAD is
      select fm_ap_stage_head_seq.Nextval ap_head_seq_no
        from dual;
   ---
   cursor C_SEQ_AP_STAGE_DETAIL is
      select fm_ap_stage_detail_seq.NEXTVAL ap_detail_seq_no
        from dual;
   ---
   cursor C_GET_DOC_NO is
      select fdh.fiscal_doc_no,
             fdh.key_value_1
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;
   ---
   CURSOR C_GET_ORDER_DETAILS is
      select oh.currency_code
        from ordhead oh,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where fdh.fiscal_doc_id  = I_fiscal_doc_id
         and fdd.fiscal_doc_id  = fdh.fiscal_doc_id
         and fdd.requisition_no = oh.order_no
         and rownum<2;

   CURSOR C_GET_FINANCIAL_AP is
      select financial_ap,consolidation_ind
        from system_options;

   type refcursor is REF CURSOR;
   C_GET_TEMP_DATA   refcursor;
      ---

   L_natural_acct_seg   FM_COA_SETUP.NATURAL_ACCT_SEGMENT%TYPE := NULL;
   L_select             VARCHAR2(3000);
   L_sequence           FM_ACCOUNT_SETUP.ATTRIBUTE1%TYPE := NULL;
   L_total_cost         FM_TRAN_DATA.TOTAL_COST%TYPE :=NULL;
   L_fiscal_doc_no      FM_FISCAL_DOC_HEADER.FISCAL_DOC_NO%TYPE := NULL;
   L_key_value_1        FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE   := NULL;
   L_financial_ap       SYSTEM_OPTIONS.FINANCIAL_AP%TYPE        := NULL;
   L_consolidation_ind  SYSTEM_OPTIONS.CONSOLIDATION_IND%TYPE   := NULL;
   R_get_temp_data      L_TEMP_REC;
   L_ap_head_seq_no     FM_AP_STAGE_HEAD.SEQ_NO%TYPE := NULL;
   L_ap_detail_seq_no   FM_AP_STAGE_HEAD.SEQ_NO%TYPE := NULL;
   L_exchange_rate      FM_AP_STAGE_HEAD.EXCHANGE_RATE%TYPE := NULL;
   L_currency_code      FM_AP_STAGE_HEAD.CURRENCY_CODE%TYPE := NULL;
   L_exchange_type      FM_AP_STAGE_HEAD.EXCHANGE_RATE_TYPE%TYPE := NULL;
   C_CHAR_TYPE             CONSTANT FM_SYSTEM_OPTIONS.VARIABLE_TYPE%TYPE        := 'CHAR';
   C_DEFAULT_CURR          CONSTANT FM_SYSTEM_OPTIONS.VARIABLE%TYPE             := 'DEFAULT_CURRENCY';
   L_dummy_desc       FM_SYSTEM_OPTIONS.DESCRIPTION%TYPE;
   L_dummy_date       FM_SYSTEM_OPTIONS.DATE_VALUE%TYPE;
   L_dummy_number     FM_SYSTEM_OPTIONS.NUMBER_VALUE%TYPE;


   BEGIN

    open C_GET_DOC_NO;
   fetch C_GET_DOC_NO into L_fiscal_doc_no,L_key_value_1;
   close C_GET_DOC_NO;

    open C_GET_NATURAL_ACCT_SEG;
   fetch C_GET_NATURAL_ACCT_SEG into L_natural_acct_seg;
   close C_GET_NATURAL_ACCT_SEG;
   L_select := 'select gfp.sequence1,
                       gfp.sequence2,
                       gfp.sequence3,
                       gfp.sequence4,
                       gfp.sequence5,
                       gfp.sequence6,
                       gfp.sequence7,
                       gfp.sequence8,
                       gfp.sequence9,
                       gfp.sequence10,
                       gfp.sequence11,
                       gfp.sequence12,
                       gfp.sequence13,
                       gfp.sequence14,
                       gfp.sequence15,
                       gfp.sequence16,
                       gfp.sequence17,
                       gfp.sequence18,
                       gfp.sequence19,
                       gfp.sequence20,
                       gfp.sequence21,
                       gfp.sequence22,
                       gfp.sequence23,
                       gfp.sequence24,
                       gfp.sequence25,
                       gfp.sequence26,
                       gfp.sequence27,
                       gfp.sequence28,
                       gfp.sequence29,
                       gfp.sequence30,
                       gfp.credit_debit,
                       gfp.set_of_books_id,
                       gfp.org_unit,
                       gfp.tran_date,
                       gfp.account_id,
                       gfp.sequence'||L_natural_acct_seg||' as natural_acct_seg,
                       sum(gfp.total_cost) as total_amount
                  from gtt_fm_financial_postings gfp
                 where gfp.fiscal_doc_id = '||I_fiscal_doc_id ||
            ' group by gfp.sequence1,
                       gfp.sequence2,
                       gfp.sequence3,
                       gfp.sequence4,
                       gfp.sequence5,
                       gfp.sequence6,
                       gfp.sequence7,
                       gfp.sequence8,
                       gfp.sequence9,
                       gfp.sequence10,
                       gfp.sequence11,
                       gfp.sequence12,
                       gfp.sequence13,
                       gfp.sequence14,
                       gfp.sequence15,
                       gfp.sequence16,
                       gfp.sequence17,
                       gfp.sequence18,
                       gfp.sequence19,
                       gfp.sequence20,
                       gfp.sequence21,
                       gfp.sequence22,
                       gfp.sequence23,
                       gfp.sequence24,
                       gfp.sequence25,
                       gfp.sequence26,
                       gfp.sequence27,
                       gfp.sequence28,
                       gfp.sequence29,
                       gfp.sequence30,
                       gfp.credit_debit,
                       gfp.set_of_books_id,
                       gfp.org_unit,
                       gfp.account_id,
                       gfp.tran_date';

   open C_GET_FINANCIAL_AP;
   fetch C_GET_FINANCIAL_AP into L_financial_ap,L_consolidation_ind;
   close C_GET_FINANCIAL_AP;

      if L_consolidation_ind = 'Y' then
         L_exchange_type := 'C';
      else
         L_exchange_type := 'O';
      end if;
   if L_financial_ap is not null then
      OPEN C_GET_TEMP_DATA for L_select;
      loop
      fetch C_GET_TEMP_DATA into R_get_temp_data;
      exit when C_GET_TEMP_DATA%NOTFOUND;

      if R_get_temp_data.natural_acct_seg = 'TAP' then
          open C_SEQ_AP_STAGE_HEAD;
         fetch C_SEQ_AP_STAGE_HEAD into L_ap_head_seq_no;
         close C_SEQ_AP_STAGE_HEAD;

         open C_GET_ORDER_DETAILS;
         fetch C_GET_ORDER_DETAILS into L_currency_code;
         close C_GET_ORDER_DETAILS;
         ---
         if L_currency_code is null then
          if FM_SYSTEM_OPTIONS_SQL.GET_VARIABLE(O_error_message,
                                            L_dummy_desc,
                                            L_dummy_number,
                                            L_currency_code,
                                            L_dummy_date,
                                            C_CHAR_TYPE,
                                            C_DEFAULT_CURR) = FALSE then
            return FALSE;
            end if;
         end if;
         
         if L_currency_code is not null then
            if CURRENCY_SQL.GET_RATE (O_error_message,
                                      L_exchange_rate,
                                      L_currency_code,
                                      NULL,
                                      R_get_temp_data.tran_date) = FALSE then
               return FALSE;
            end if;
        end if;  
         ---
         
         insert into fm_ap_stage_head (seq_no,
                                       fiscal_doc_id,
                                       fiscal_type_lookup_code,
                                       fiscal_doc_no,
                                       vendor,
                                       oracle_site_id,
                                       currency_code,
                                       exchange_rate,
                                       exchange_rate_type,
                                       tran_date,
                                       amount,
                                       best_terms,
                                       best_terms_date,
                                       segment1,
                                       segment2,
                                       segment3,
                                       segment4,
                                       segment5,
                                       segment6,
                                       segment7,
                                       segment8,
                                       segment9,
                                       segment10,
                                       segment11,
                                       segment12,
                                       segment13,
                                       segment14,
                                       segment15,
                                       segment16,
                                       segment17,
                                       segment18,
                                       segment19,
                                       segment20,
                                       segment21,
                                       segment22,
                                       segment23,
                                       segment24,
                                       segment25,
                                       segment26,
                                       segment27,
                                       segment28,
                                       segment29,
                                       segment30,
                                       account_id,
                                       set_of_books_id,
                                       org_unit,
                                       create_date_time,
                                       create_id,
                                       last_update_datetime,
                                       last_update_id,
                                       reference_id)
                               values (L_ap_head_seq_no,
                                       I_fiscal_doc_id,
                                       decode(R_get_temp_data.credit_debit, 'C','CREDIT','D','STANDARD'),
                                       L_fiscal_doc_no,
                                       L_key_value_1,
                                       NULL,--L_oracle_site_id,
                                       L_currency_code,
                                       L_exchange_rate,
                                       L_exchange_type,
                                       R_get_temp_data.tran_date,
                                       R_get_temp_data.total_amount,
                                       NULL,--best_terms,
                                       NULL,--best_terms_date,
                                       R_get_temp_data.sequence1,
                                       R_get_temp_data.sequence2,
                                       R_get_temp_data.sequence3,
                                       R_get_temp_data.sequence4,
                                       R_get_temp_data.sequence5,
                                       R_get_temp_data.sequence6,
                                       R_get_temp_data.sequence7,
                                       R_get_temp_data.sequence8,
                                       R_get_temp_data.sequence9,
                                       R_get_temp_data.sequence10,
                                       R_get_temp_data.sequence11,
                                       R_get_temp_data.sequence12,
                                       R_get_temp_data.sequence13,
                                       R_get_temp_data.sequence14,
                                       R_get_temp_data.sequence15,
                                       R_get_temp_data.sequence16,
                                       R_get_temp_data.sequence17,
                                       R_get_temp_data.sequence18,
                                       R_get_temp_data.sequence19,
                                       R_get_temp_data.sequence20,
                                       R_get_temp_data.sequence21,
                                       R_get_temp_data.sequence22,
                                       R_get_temp_data.sequence23,
                                       R_get_temp_data.sequence24,
                                       R_get_temp_data.sequence25,
                                       R_get_temp_data.sequence26,
                                       R_get_temp_data.sequence27,
                                       R_get_temp_data.sequence28,
                                       R_get_temp_data.sequence29,
                                       R_get_temp_data.sequence30,
                                       R_get_temp_data.account_id,
                                       R_get_temp_data.set_of_books_id,
                                       R_get_temp_data.org_unit,
                                       SYSDATE,
                                       USER,
                                       SYSDATE,
                                       USER,
                                       NULL);--reference_id
      else
          open C_SEQ_AP_STAGE_DETAIL;
         fetch C_SEQ_AP_STAGE_DETAIL into L_ap_detail_seq_no;
         close C_SEQ_AP_STAGE_DETAIL;
         insert into fm_ap_stage_detail (seq_no,
                                         fiscal_doc_id,
                                         line_type_lookup_code,
                                         amount,
                                         segment1,
                                         segment2,
                                         segment3,
                                         segment4,
                                         segment5,
                                         segment6,
                                         segment7,
                                         segment8,
                                         segment9,
                                         segment10,
                                         segment11,
                                         segment12,
                                         segment13,
                                         segment14,
                                         segment15,
                                         segment16,
                                         segment17,
                                         segment18,
                                         segment19,
                                         segment20,
                                         segment21,
                                         segment22,
                                         segment23,
                                         segment24,
                                         segment25,
                                         segment26,
                                         segment27,
                                         segment28,
                                         segment29,
                                         segment30,
                                         account_id,
                                         set_of_books_id,
                                         org_unit,
                                         tax_code,
                                         tax_rate,
                                         create_datetime,
                                         create_id,
                                         last_update_datetime,
                                         last_update_id,
                                         reference_id)
                                 values (L_ap_detail_seq_no,
                                         I_fiscal_doc_id,
                                         NULL,--line_type_lookup_code,
                                         R_get_temp_data.total_amount,
                                         R_get_temp_data.sequence1,
                                         R_get_temp_data.sequence2,
                                         R_get_temp_data.sequence3,
                                         R_get_temp_data.sequence4,
                                         R_get_temp_data.sequence5,
                                         R_get_temp_data.sequence6,
                                         R_get_temp_data.sequence7,
                                         R_get_temp_data.sequence8,
                                         R_get_temp_data.sequence9,
                                         R_get_temp_data.sequence10,
                                         R_get_temp_data.sequence11,
                                         R_get_temp_data.sequence12,
                                         R_get_temp_data.sequence13,
                                         R_get_temp_data.sequence14,
                                         R_get_temp_data.sequence15,
                                         R_get_temp_data.sequence16,
                                         R_get_temp_data.sequence17,
                                         R_get_temp_data.sequence18,
                                         R_get_temp_data.sequence19,
                                         R_get_temp_data.sequence20,
                                         R_get_temp_data.sequence21,
                                         R_get_temp_data.sequence22,
                                         R_get_temp_data.sequence23,
                                         R_get_temp_data.sequence24,
                                         R_get_temp_data.sequence25,
                                         R_get_temp_data.sequence26,
                                         R_get_temp_data.sequence27,
                                         R_get_temp_data.sequence28,
                                         R_get_temp_data.sequence29,
                                         R_get_temp_data.sequence30,
                                         R_get_temp_data.account_id,
                                         R_get_temp_data.set_of_books_id,
                                         R_get_temp_data.org_unit,
                                         NULL,--vat_code,
                                         NULL,--vat_rate,
                                         SYSDATE,
                                         USER,
                                         SYSDATE,
                                         USER,
                                         NULL);--reference_id
      end if;
   end loop;
   close C_GET_TEMP_DATA;
   ---
   end if;
   return TRUE;

EXCEPTION
---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SUBSTR(SQLERRM, 1,254),
                                             L_program,
                                             TO_CHAR(SQLCODE));
--
      if C_GET_TEMP_DATA%ISOPEN then
         close C_GET_TEMP_DATA;
      end if;
--
      if C_GET_NATURAL_ACCT_SEG%ISOPEN then
         close C_GET_NATURAL_ACCT_SEG;
      end if;
--
      return FALSE;
END POST_AP_FINANCIALS;
----------------------------------------------------------------------------------
END FM_FINANCIAL_POSTING_SQL;
/