CREATE OR REPLACE PACKAGE BODY FM_INVADJUST_SQL is

--------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS --
--------------------------------------------------------------------------------
FUNCTION PARSE_INVADJUSTDTL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_seq_no          IN       NUMBER,
                            I_invtl_tbl       IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_orderable_TBL   OUT      "RIB_InvAdjustDtl_TBL",
                             I_sellable_TBL    IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_sellable_TBL     OUT      "RIB_InvAdjustDtl_TBL",
                     O_detail_TBL       OUT      "RIB_InvAdjustDtl_TBL",
                     I_rib_detail_TBL   IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION CHECK_INVADJ(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_adj_qty            IN OUT   INV_ADJ.ADJ_QTY%TYPE,
                      I_from_disposition   IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                      I_to_disposition     IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_orderable_TBL   OUT      "RIB_InvAdjustDtl_TBL",
                             I_sellable_TBL    IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN IS

   L_program                VARCHAR2(62) := 'FM_INVADJUST_SQL.GET_ORDERABLE_ITEMS';
   
   L_orderable_detail_TBL   "RIB_InvAdjustDtl_TBL" := NULL;
   L_orditem_TBL            bts_orditem_qty_tbl := NULL;
   L_sell_items             ITEM_TBL;
   L_sell_qty               QTY_TBL;
   L_detail_no              NUMBER := 0;

   cursor C_SELL_ITEMS is
      select item_id,
             unit_qty
        from TABLE(cast(I_sellable_TBL AS "RIB_InvAdjustDtl_TBL"));

   cursor C_ORD_ITEM_QTY (I_item  IN ITEM_MASTER.ITEM%TYPE) is
      select orderable_item,qty
        from TABLE(cast(L_orditem_TBL AS bts_orditem_qty_tbl))
       where sellable_item = I_item;

BEGIN

   open C_SELL_ITEMS;
   fetch C_SELL_ITEMS BULK COLLECT into L_sell_items,
                                        L_sell_qty;
   close C_SELL_ITEMS;

   if ITEM_XFORM_SQL.ORDERABLE_ITEM_INFO(O_error_message,
                                         L_orditem_TBL,  -- output table
                                         L_sell_items,
                                         L_sell_qty) = FALSE then
      return FALSE;
   end if;

   L_orderable_detail_TBL := "RIB_InvAdjustDtl_TBL"();

   FOR i in I_sellable_TBL.FIRST..I_sellable_TBL.LAST LOOP
      FOR item_rec in C_ORD_ITEM_QTY(I_sellable_TBL(i).item_id) LOOP
         L_detail_no := L_detail_no + 1;
         L_orderable_detail_TBL.EXTEND;
         L_orderable_detail_TBL(L_detail_no) := I_sellable_TBL(i); --- Get all message values
         L_orderable_detail_TBL(L_detail_no).item_id := item_rec.orderable_item;
         L_orderable_detail_TBL(L_detail_no).unit_qty := item_rec.qty;
      END LOOP;
   END LOOP;

   O_orderable_TBL := L_orderable_detail_TBL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ORDERABLE_ITEMS;
----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_sellable_TBL     OUT      "RIB_InvAdjustDtl_TBL",
                     O_detail_TBL       OUT      "RIB_InvAdjustDtl_TBL",
                     I_rib_detail_TBL   IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN IS

   L_program      VARCHAR2(62) := 'FM_INVADJUST_SQL.CHECK_ITEMS';

   L_dtl_count    NUMBER := 0;
   L_sell_count   NUMBER := 0;
   L_item_rec     ITEM_MASTER%ROWTYPE;

BEGIN
   O_sellable_TBL := "RIB_InvAdjustDtl_TBL"();
   O_detail_TBL := "RIB_InvAdjustDtl_TBL"();
   ---
   FOR i in 1..I_rib_detail_TBL.COUNT LOOP
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item_rec,  --- output record
                                          I_rib_detail_TBL(i).item_id) = FALSE then
         return FALSE;
      end if;

      if L_item_rec.sellable_ind = 'Y'   and
         L_item_rec.orderable_ind = 'N'  and
         L_item_rec.item_xform_ind = 'Y' then
         --- add item to sellable table
         L_sell_count := L_sell_count + 1;

         O_sellable_TBL.EXTEND;
         O_sellable_TBL(L_sell_count) := I_rib_detail_TBL(i);
      else
         --- add item to a new detail table
         L_dtl_count := L_dtl_count + 1;
         O_detail_TBL.EXTEND;
         O_detail_TBL(L_dtl_count) := I_rib_detail_TBL(i);
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_ITEMS;
----------------------------------------------------------------------------------------
FUNCTION CHECK_INVADJ(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_adj_qty            IN OUT   INV_ADJ.ADJ_QTY%TYPE,
                      I_from_disposition   IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                      I_to_disposition     IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(62) := 'FM_INVADJUST_SQL.CHECK_INVADJ';

   L_from_inv_status    INV_ADJ.INV_STATUS%TYPE;
   L_to_inv_status      INV_ADJ.INV_STATUS%TYPE;
   L_adj_qty            INV_ADJ.ADJ_QTY%TYPE;   

BEGIN
   if I_from_disposition is NOT NULL then
      if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                   L_from_inv_status,
                                   I_from_disposition) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_to_disposition is NOT NULL then
      if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                   L_to_inv_status,
                                   I_to_disposition) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_from_disposition is NOT NULL and I_to_disposition is NOT NULL then
      if  NVL(L_from_inv_status,-999) = NVL(L_to_inv_status,-999) then
         return TRUE;   -- no disposition change, done
      end if;
      --
      if L_to_inv_status is NULL then
         L_adj_qty := -1;
      elsif L_from_inv_status is NULL then
         L_adj_qty := +1;
      end if;
   elsif I_from_disposition is NULL or I_to_disposition is NULL then
      if I_from_disposition is NULL then
         L_adj_qty    := +1;
      elsif I_to_disposition is NULL then
         L_adj_qty    := -1;
      end if;
      ---
   end if;
   ---
   L_adj_qty := L_adj_qty * ABS(O_adj_qty);
   
   if (I_from_disposition is NOT NULL and I_to_disposition is NOT NULL) and
      L_from_inv_status is NULL then
      L_adj_qty := -1 * L_adj_qty;
   end if;
   
   O_adj_qty := L_adj_qty;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_INVADJ;
------------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
-- Called by FM_INTEGRATION_SQL.CONSUME_INVADJUST when the message type corresponds
-- to an Inventory Adjustment
------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_seq_no          IN OUT   FM_STG_ADJUST_DESC.SEQ_NO%TYPE,
                 I_message         IN       RIB_OBJECT,
                 I_message_type    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program              VARCHAR2(62)                            := 'FM_INVADJUST_SQL.CONSUME';
   L_key                  VARCHAR2(150)                           := NULL;
   L_table                VARCHAR2(255)                           := NULL;
   L_cursor               VARCHAR2(32)                            := NULL;
   L_status_code          VARCHAR2(1);
   L_reason_util_exists   VARCHAR2(1)                             := 'N';
   L_seq_no               NUMBER                                  := NULL;
   L_rms_call             BOOLEAN                                 := FALSE;
   L_batch_running        RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE;

   L_invDesc_rec          "RIB_InvAdjustDesc_REC"                 := NULL;
   L_invadj_detail_TBL    "RIB_InvAdjustDtl_TBL"                  := NULL;
   L_detail_TBL           "RIB_InvAdjustDtl_TBL"                  := NULL;
   L_sellable_TBL         "RIB_InvAdjustDtl_TBL"                  := NULL;
   L_orderable_TBL        "RIB_InvAdjustDtl_TBL"                  := NULL;

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_SEQ_NO is
      select fm_stg_adjust_desc_seq.nextval
        from dual;

   cursor C_REASON_UTIL_EXISTS is
      select 'Y'
        from TABLE(CAST(L_invDesc_rec.InvAdjustDtl_TBL AS "RIB_InvAdjustDtl_TBL")) iadtl,
                   fm_util_invadj_reason fuir
       where fuir.reason = iadtl.adjustment_reason_code
         and rownum = 1;
   
   cursor C_LOCK_STG_ADJUST_DESC(P_seq_no NUMBER) is
      select 'X'
        from fm_stg_adjust_desc fsad
       where fsad.seq_no = P_seq_no
         for update nowait;
   
BEGIN

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_message',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_message_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_message_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   L_invDesc_rec := TREAT(I_message as "RIB_InvAdjustDesc_REC");
   L_invadj_detail_TBL := L_invDesc_rec.InvAdjustDtl_TBL;
   ---  
   open C_SEQ_NO;
   fetch C_SEQ_NO into L_seq_no;
   close C_SEQ_NO;
   ---
   -- Perform basic validation of the RIB message before table insert
   if FM_STG_VALIDATION_SQL.PROCESS_VALIDATION(O_error_message,
                                               I_message,
                                               I_message_type,
                                               'STOCK') = FALSE then
      return FALSE;
   end if;
   ---
   L_key   := 'seq_no: '||L_seq_no;
   L_table := 'FM_STG_ADJUST_DESC';   
   SQL_LIB.SET_MARK('INSERT', NULL, L_table, L_key);
   insert into fm_stg_adjust_desc (seq_no,
                                   dc_dest_id,
                                   status,
                                   create_datetime,
                                   create_id,
                                   last_update_datetime,
                                   last_update_id)
                            values(L_seq_no,
                                   L_invDesc_rec.dc_dest_id,
                                   'U',
                                   sysdate,
                                   user,
                                   sysdate,
                                   user);
   
   ---
   L_table  := 'L_invDesc_rec.InvAdjustDtl_TBL/FM_UTIL_INVADJ_REASON';
   L_cursor := 'C_REASON_UTIL_EXISTS';
   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, NULL);
   open C_REASON_UTIL_EXISTS;
   SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, NULL);
   fetch C_REASON_UTIL_EXISTS into L_reason_util_exists;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, NULL);
   close C_REASON_UTIL_EXISTS;
   ---
   if CHECK_ITEMS(O_error_message,
                  L_sellable_TBL,
                  L_detail_TBL,
                  L_invadj_detail_TBL) = FALSE then
      return FALSE;
   end if;
   if L_sellable_TBL is NOT NULL and L_sellable_TBL.COUNT > 0 then

      L_orderable_TBL := "RIB_InvAdjustDtl_TBL"();
      if GET_ORDERABLE_ITEMS(O_error_message,
                             L_orderable_TBL, --- Output RIB table of orderable items
                             L_sellable_TBL) = FALSE then
         return FALSE;
      end if;

      if L_orderable_TBL is NOT NULL and L_orderable_TBL.COUNT > 0 then
         FOR i in 1..L_orderable_TBL.COUNT LOOP
            if FM_INVADJUST_SQL.CHECK_INVADJ(O_error_message,
                                             L_orderable_TBL(i).unit_qty,
                                             L_orderable_TBL(i).from_disposition,
                                             L_orderable_TBL(i).to_disposition) = FALSE then
               return FALSE;                                             
            end if;         
            if L_orderable_TBL(i).unit_qty < 0 and L_reason_util_exists = 'Y' then       
                if PARSE_INVADJUSTDTL(O_error_message, 
                                      L_seq_no, 
                                      L_orderable_TBL) = FALSE then
                  return FALSE;
               end if;
            else
               L_rms_call := TRUE;
            end if;
         END LOOP;
      end if;

   end if;

   if L_detail_TBL is NOT NULL and L_detail_TBL.COUNT > 0 then   
      FOR i in 1..L_detail_TBL.COUNT LOOP
         if FM_INVADJUST_SQL.CHECK_INVADJ(O_error_message,
                                          L_detail_TBL(i).unit_qty,
                                          L_detail_TBL(i).from_disposition,
                                          L_detail_TBL(i).to_disposition) = FALSE then
             return FALSE;
         end if;
         if L_detail_TBL(i).unit_qty < 0 and L_reason_util_exists = 'Y' then       
            if PARSE_INVADJUSTDTL(O_error_message, 
                                   L_seq_no, 
                                   L_detail_TBL) = FALSE then
               return FALSE;
            end if;
         else         
            L_rms_call := TRUE;
         end if;             
      END LOOP;
   end if;
   ---
   if L_rms_call then
      -- Check if RMS batch is running
      if RMS_BATCH_STATUS_SQL.GET_BATCH_RUNNING_IND(O_error_message,
                                                    L_batch_running) = FALSE then
         return FALSE;
      end if;
      if L_batch_running = 'N' then
         RMSSUB_INVADJUST.CONSUME_INVADJ(L_status_code,
                                         O_error_message,
                                         I_message,
                                         I_message_type,
                                         'N');

         if L_status_code = API_CODES.UNHANDLED_ERROR or O_error_message is NOT NULL then
            return FALSE;
         else
            L_table  := 'FM_STG_ADJUST_DESC';
            L_cursor := 'C_LOCK_STG_ADJUST_DESC';
            SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
            open C_LOCK_STG_ADJUST_DESC(L_seq_no);
            SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
            close C_LOCK_STG_ADJUST_DESC;
            
            L_cursor := NULL;
            SQL_LIB.SET_MARK('DELETE', L_cursor, L_table, L_key);
            delete from fm_stg_adjust_desc
                  where seq_no = L_seq_no;  
         end if;
      else  -- L_batch_running = 'Y'      
         -- Stage all InvAdjust details in the FM_STG_ADJUST_DTL table     
         if PARSE_INVADJUSTDTL(O_error_message,
                               L_seq_no,
                               L_invadj_detail_TBL) = FALSE then
            return FALSE;
         end if;
         -- Update status on FM_STG_ADJUST_DESC to 'H'old status
         if FM_INTEGRATION_SQL.UPDATE_STG_STATUS(O_error_message,
                                                 'H',
                                                 'STOCK',
                                                 L_seq_no) = FALSE then
            return FALSE;
         end if;                               
      end if;
   else
      O_seq_no      := L_seq_no;
   end if;

   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(L_seq_no),
                                             NULL);  
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
        return FALSE;
END CONSUME;

------------------------------------------------------------------------------------
-- PRIVATE FUNCTION --
-- Called by CONSUME
------------------------------------------------------------------------------------
FUNCTION PARSE_INVADJUSTDTL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_seq_no          IN       NUMBER,
                             I_invtl_tbl       IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN IS

   L_program      VARCHAR2(62)            := 'FM_INVADJUST_SQL.PARSE_INVADJUSTDTL';
   L_key          VARCHAR2(150)           := NULL;
   L_table        VARCHAR2(255)           := NULL;
   L_cursor       VARCHAR2(32)            := NULL;
   L_invdtl_rec   "RIB_InvAdjustDtl_REC"  := NULL;
   L_dtl_seq_no   NUMBER                  := NULL;
   
   cursor C_GET_NEXT_SEQ_DTL is
      select fm_stg_adjust_dtl_seq.nextval
        from dual;

BEGIN
   
   L_table := 'FM_STG_ADJUST_DTL';
   
   FOR i in I_invtl_tbl.FIRST..I_invtl_tbl.LAST LOOP
      
      open C_GET_NEXT_SEQ_DTL;
      fetch C_GET_NEXT_SEQ_DTL into L_dtl_seq_no;
      close C_GET_NEXT_SEQ_DTL;

      L_invdtl_rec := I_invtl_tbl(i);
      L_key        := 'seq_no: '||L_dtl_seq_no||', desc_seq_no: '||I_seq_no;
      SQL_LIB.SET_MARK('INSERT', NULL, L_table, L_key);
      --Insert only adjustment with the negative value
      insert into fm_stg_adjust_dtl(seq_no,
                                    desc_seq_no,
                                    item_id,
                                    adjustment_reason_code,
                                    unit_qty,
                                    transshipment_nbr,
                                    from_disposition,
                                    to_disposition,
                                    from_trouble_code,
                                    to_trouble_code,
                                    from_wip_code,
                                    to_wip_code,
                                    transaction_code,
                                    user_id,
                                    create_date,
                                    po_nbr,
                                    doc_type,
                                    aux_reason_code,
                                    weight,
                                    weight_uom,
                                    ebc_cost,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                             values(L_dtl_seq_no,
                                    I_seq_no,
                                    L_invdtl_rec.item_id,
                                    L_invdtl_rec.adjustment_reason_code,
                                    L_invdtl_rec.unit_qty ,
                                    L_invdtl_rec.transshipment_nbr,
                                    L_invdtl_rec.from_disposition,
                                    L_invdtl_rec.to_disposition,
                                    L_invdtl_rec.from_trouble_code,
                                    L_invdtl_rec.to_trouble_code,
                                    L_invdtl_rec.from_wip_code,
                                    L_invdtl_rec.to_wip_code,
                                    L_invdtl_rec.transaction_code,
                                    L_invdtl_rec.user_id,
                                    L_invdtl_rec.create_date,
                                    L_invdtl_rec.po_nbr,
                                    L_invdtl_rec.doc_type,
                                    L_invdtl_rec.aux_reason_code,
                                    L_invdtl_rec.weight,
                                    L_invdtl_rec.weight_uom,
                                    L_invdtl_rec.unit_cost,
                                    sysdate,
                                    user,
                                    sysdate,
                                    user);
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PARSE_INVADJUSTDTL;
------------------------------------------------------------------------------------------------------------------
END FM_INVADJUST_SQL;
/
