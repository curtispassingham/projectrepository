CREATE OR REPLACE PACKAGE FM_SCHEDULE_VAL_SQL is
---------------------------------------------------------------------------------
-- CREATE DATE           - 17-04-2007
-- CREATE USER           ? JLD
-- PROJECT / PT / TRD    - 10481 / /001
-- DESCRIPTION           ? This package manipulate the form orfm_schedule.fmb
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
-- CHANGE SEQ  - 001 - Enabler Latin America
-- CHANGE DATE - 10-JUN-2008
-- CHANGE USER - BCA
-- PROJECT / PT / TRD - 10481 - Oracle Retail Localization
-- DESCRIPTION - Create function VALIDATE_APROVE_SCHED (BUG FIX ID-8).
-------------------------------------------------------------------------------
RECORD_LOCKED        EXCEPTION;
PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
---------------------------------------------------------------------------------
-- Function Name: LOV_SCHEDULE_NO
-- Purpose:       This function returns the query for record_grup.
---------------------------------------------------------------------------------
FUNCTION LOV_SCHEDULE_NO(O_error_message  IN OUT VARCHAR2,
                         O_query          IN OUT VARCHAR2,
                         I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE,
                         I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE,
                         I_where_clause   IN     VARCHAR2)

return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXISTS_SCHEDULE
-- Purpose:       This function verify if exists the schedule no.
---------------------------------------------------------------------------------
FUNCTION EXISTS_SCHEDULE(O_error_message  IN OUT VARCHAR2,
                         O_exists         IN OUT BOOLEAN,
                         I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE,
                         I_mode_type      IN     FM_SCHEDULE.MODE_TYPE%TYPE,
                         I_location       IN     FM_SCHEDULE.LOCATION_ID%TYPE   DEFAULT NULL,
                         I_loc_type       IN     FM_SCHEDULE.LOCATION_TYPE%TYPE DEFAULT NULL)

return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_SCHEDULE_NUMBER_SEQ
-- Purpose:       This function get the next receiving number.
---------------------------------------------------------------------------------
FUNCTION GET_SCHEDULE_NUMBER_SEQ(O_error_message   IN OUT VARCHAR2,
                                 O_seq_schedule_no IN OUT NUMBER )

return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: FISCAL_NO_DOC_EXISTS
-- Purpose:       This function verifies if the fiscal number already exits.
---------------------------------------------------------------------------------
FUNCTION FISCAL_NO_DOC_EXISTS(O_error_message  IN OUT VARCHAR2,
                              O_exists         IN OUT BOOLEAN,
                              I_schedule_no    IN     NUMBER)

return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: UPDATE_SCHD_STATUS
-- Purpose:       This function updates the status of the schedule
---------------------------------------------------------------------------------
FUNCTION UPDATE_SCHD_STATUS(O_error_message  IN OUT VARCHAR2,
                            I_schedule_no    IN     NUMBER,
                            I_status         IN     FM_SCHEDULE.STATUS%TYPE)

return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: FISCAL_NO_DOC_WI
-- Purpose:       This function verifies if all the fiscal number are in Worksheet or Inactive status
---------------------------------------------------------------------------------
FUNCTION FISCAL_NO_DOC_WI(O_error_message  IN OUT VARCHAR2,
                          O_exists         IN OUT BOOLEAN,
                          I_schedule_no    IN     NUMBER)

return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: UPDATE_STATUS_FISCAL_DOC
-- Purpose:       This function does the update on table FM_FISCAL_DOC_HEADER
--                where status = 'I' .
---------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS_FISCAL_DOC(O_error_message  IN OUT VARCHAR2,
                                  I_location       IN     ITEM_LOC.LOC%TYPE,
                                  I_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                                  I_schedule_no    IN     FM_FISCAL_DOC_HEADER.SCHEDULE_NO%TYPE,
                                  I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                                  I_status         IN     FM_FISCAL_DOC_HEADER.STATUS%TYPE )

return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: WORKSHEET_SCHED
-- Purpose:       This function returns the schedule to worksheet status.
----------------------------------------------------------------------------------
FUNCTION WORKSHEET_SCHED(O_error_message  IN OUT VARCHAR2,
                         I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: ALL_FISCAL_DOC_INACTIVE
-- Purpose:       This function finds if all the fiscal docs under the schedule are in inactive status
----------------------------------------------------------------------------------
FUNCTION ALL_FISCAL_DOC_INACTIVE(O_error_message  IN OUT VARCHAR2,
                                 O_exists         IN OUT BOOLEAN,
                                 I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_SCHED
-- Purpose:       This function does the validation on schedule level.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_SCHED(O_error_message  IN OUT VARCHAR2,
                        O_status         IN OUT FM_SCHEDULE.STATUS%TYPE,
                        I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: MATCH_SCHED
-- Purpose:       This function does the match on schedule level.
----------------------------------------------------------------------------------
FUNCTION MATCH_SCHED(O_error_message  IN OUT VARCHAR2,
                     O_status         IN OUT FM_SCHEDULE.STATUS%TYPE,
                     I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: APPROVE_SCHED
-- Purpose:       This function does the match on schedule level.
----------------------------------------------------------------------------------
FUNCTION APPROVE_SCHED(O_error_message  IN OUT VARCHAR2,
                       I_schedule_no    IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: INSERT_NON_EXIST_DOCS
-- Purpose:       This function insert non exixtent documents on fm_printer_queue table.
----------------------------------------------------------------------------------
FUNCTION INSERT_NON_EXIST_DOCS(O_error_message IN OUT VARCHAR2,
                               I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- Function Name: SET_WHERE_CLAUSE
-- Purpose:       This function returns the query for where clause of block.
----------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message IN OUT VARCHAR2,
                          O_where         IN OUT VARCHAR2,
                          I_location_id   IN FM_SCHEDULE.LOCATION_ID%TYPE,
                          I_location_type IN FM_SCHEDULE.LOCATION_TYPE%TYPE,
                          I_mode_type     IN FM_SCHEDULE.MODE_TYPE%TYPE,
                          I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE,
--                          I_schedule_date IN FM_SCHEDULE.SCHEDULE_DATE%TYPE,
			  I_start_date    IN FM_SCHEDULE.SCHEDULE_DATE%TYPE,
			  I_end_date      IN FM_SCHEDULE.SCHEDULE_DATE%TYPE,
                          I_status        IN FM_SCHEDULE.STATUS%TYPE
                          )
return BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: VALIDATE_REQ_TYPE
-- Purpose:       This function does the validation on requisition type.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_REQ_TYPE(O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           I_schedule_no   IN FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;

-- 001 - Begin
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_APROVE_SCHED
-- Purpose:       This function does the validate match on schedule level.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_APROVE_SCHED(O_error_message IN OUT VARCHAR2,
                               O_status        IN OUT BOOLEAN,
                               I_schedule_no   IN     FM_SCHEDULE.SCHEDULE_NO%TYPE)
return BOOLEAN;
-- 001 - End


END FM_SCHEDULE_VAL_SQL;
/