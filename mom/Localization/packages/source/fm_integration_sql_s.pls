CREATE OR REPLACE PACKAGE FM_INTEGRATION_SQL is
----------------------------------------------------------------------------------
RECEIPT_ADD            CONSTANT  VARCHAR2(30) := 'receiptcre';
----------------------------------------------------------------------------------
FUNCTION UPDATE_STG_STATUS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_status             IN       VARCHAR2 DEFAULT NULL,
                           I_requisition_type   IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                           I_seq_no             IN       FM_STG_ASNOUT_DESC.SEQ_NO%TYPE,
                           I_fiscal_doc_id      IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CONSUME_RTV(O_status_code     IN OUT   VARCHAR2,
                     O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message         IN       RIB_OBJECT,
                     I_message_type    IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CONSUME_SHIPMENT(O_status_code     IN OUT   VARCHAR2,
                          O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       RIB_OBJECT,
                          I_message_type    IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CONSUME_INVADJUST(O_status_code     IN OUT   VARCHAR2,
                           O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       RIB_OBJECT,
                           I_message_type    IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CONSUME_RECEIPT(O_status_code     IN OUT   VARCHAR2,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_OBJECT",
                         I_message_type    IN       VARCHAR2)

RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CONSUME_WASTE_ADJ(O_status_code     IN OUT   VARCHAR2,
                           O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN OUT   FM_STG_ADJUST_DTL%ROWTYPE,
                           I_location        IN       FM_STG_ADJUST_DESC.DC_DEST_ID%TYPE,
                           I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
END FM_INTEGRATION_SQL;
/