DROP TYPE OBJ_ITEM_TAX_CRITERIA_TBL FORCE
/

DROP TYPE OBJ_ITEM_TAX_CRITERIA_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_ITEM_TAX_CRITERIA_REC AS OBJECT
(
  ITEM	VARCHAR2(25),
  SUPPLIER  NUMBER(10),
  LOCATION	NUMBER(10),
  EFF_DATE	DATE,
  UNIT_COST	NUMBER(20, 4),
  MRP	NUMBER(20, 4),
  RETAIL	NUMBER(20, 4),
  FREIGHT	NUMBER(20, 4),
  TAX_TYPE	VARCHAR2(6)
)
/


CREATE OR REPLACE TYPE OBJ_ITEM_TAX_CRITERIA_TBL AS TABLE OF OBJ_ITEM_TAX_CRITERIA_REC
/





