drop type IM_DOC_SEARCH_CRITERIA_REC force;

create or replace type im_doc_search_criteria_rec as object (
   in_cost_review_ind         varchar2(1),
   in_qty_review_ind          varchar2(1),
   consignment_ind            varchar2(1),
   deal_ind                   varchar2(1),
   deal_type                  varchar2(1),
   prepay_invoice_ind         varchar2(1),
   detail_exist_ind           varchar2(1),
   receipts_exist_ind         varchar2(1),
   doc_hold_ind               varchar2(1),
   start_document_date        date,
   end_document_date          date,
   start_due_date             date,
   end_due_date               date,
   loc_type                   varchar2(1),
   invc_exist_ind             varchar2(1),
   receipt_cost_and_qty_ind   varchar2(1),
   start_receipt_date         date,
   end_receipt_date           date,
   from_doc_cost              number(20,4),
   to_doc_cost                number(20,4),
   from_reciept_cost          number(20,4),
   to_receipt_cost            number(20,4),
   dept                       number(4),
   class                      number(4),
   subclass                   number(4),
   start_resolve_by_date      date,
   end_resolve_by_date        date,
   from_discrep_cost          number(20,4),
   to_discrep_cost            number(20,4),
   from_discrep_qty           number(20,4),
   to_discrep_qty             number(20,4),
   start_routing_by_date      date,
   end_routing_by_date        date,
   cash_disc_ind              varchar2(1),
   manual_group_id            number(10),
   start_match_date           date,
   end_match_date             date,
   matchable_credit_note_ind  varchar2(1),
   rtv_ind                    varchar2(1),
   debit_memo_reversed_ind    varchar2(1),
   --
   doc_type                   OBJ_VARCHAR_ID_TABLE,
   ext_doc_id                 OBJ_VARCHAR_DESC_TABLE,
   ref_doc                    OBJ_NUMERIC_ID_TABLE,
   supplier_group_id          OBJ_NUMERIC_ID_TABLE,
   vendor_type                OBJ_VARCHAR_ID_TABLE,
   vendor                     OBJ_VARCHAR_ID_TABLE,
   vendor_name                OBJ_VARCHAR_DESC_TABLE,
   supplier_site_id           OBJ_NUMERIC_ID_TABLE,
   supplier_site_name         OBJ_VARCHAR_DESC_TABLE,
   status                     OBJ_VARCHAR_ID_TABLE,
   rtv_order_no               OBJ_NUMERIC_ID_TABLE,
   deal_id                    OBJ_NUMERIC_ID_TABLE,
   deal_detail_id             OBJ_NUMERIC_ID_TABLE,
   loc                        OBJ_NUMERIC_ID_TABLE,
   loc_name                   OBJ_VARCHAR_DESC_TABLE,
   loc_list                   OBJ_NUMERIC_ID_TABLE,
   loc_trait                  OBJ_NUMERIC_ID_TABLE,
   store_grade                OBJ_VARCHAR_DESC_TABLE,
   order_no                   OBJ_NUMERIC_ID_TABLE,
   item                       OBJ_VARCHAR_ID_TABLE,
   item_desc                  OBJ_VARCHAR_DESC_TABLE,
   vpn                        OBJ_VARCHAR_DESC_TABLE,
   ap_reviewer                OBJ_VARCHAR_ID_TABLE,
   currency                   OBJ_VARCHAR_ID_TABLE,
   payment_terms              OBJ_VARCHAR_ID_TABLE,
   freight_payment_terms      OBJ_VARCHAR_ID_TABLE,
   doc_source                 OBJ_VARCHAR_ID_TABLE,
   receipt                    OBJ_NUMERIC_ID_TABLE,
   shipment_invc_match_status OBJ_VARCHAR_ID_TABLE,
   match_id                   OBJ_VARCHAR_ID_TABLE,
   ref_cnr_ext_doc_id         OBJ_VARCHAR_DESC_TABLE,
   ref_inv_ext_doc_id         OBJ_VARCHAR_DESC_TABLE,
   group_id                   OBJ_NUMERIC_ID_TABLE
);
/
