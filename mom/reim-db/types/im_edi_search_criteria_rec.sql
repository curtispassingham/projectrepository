drop type im_edi_search_criteria_rec force;

create or replace type im_edi_search_criteria_rec as object (
   start_document_date        date,
   end_document_date          date,
   loc_type                   varchar2(1),
   --
   inject_source              OBJ_VARCHAR_DESC_TABLE,
   rule                       OBJ_VARCHAR_DESC_TABLE,
   doc_type                   OBJ_VARCHAR_ID_TABLE,
   ext_doc_id                 OBJ_VARCHAR_DESC_TABLE,
   vendor_type                OBJ_VARCHAR_ID_TABLE,
   vendor                     OBJ_VARCHAR_ID_TABLE,
   vendor_name                OBJ_VARCHAR_DESC_TABLE,
   supplier_site_id           OBJ_NUMERIC_ID_TABLE,
   supplier_site_name         OBJ_VARCHAR_DESC_TABLE,
   loc                        OBJ_NUMERIC_ID_TABLE,
   loc_name                   OBJ_VARCHAR_DESC_TABLE,
   order_no                   OBJ_NUMERIC_ID_TABLE,
   ap_reviewer                OBJ_VARCHAR_ID_TABLE,
   group_id                   OBJ_NUMERIC_ID_TABLE
);
/




















