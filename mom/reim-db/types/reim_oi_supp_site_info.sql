DROP TYPE REIM_OI_SUPP_SITE_INFO_REC FORCE;

CREATE OR REPLACE TYPE REIM_OI_SUPP_SITE_INFO_REC as OBJECT(
   SUPPLIER_SITE_NAME VARCHAR2(240),
   CONTACT_NAME       VARCHAR2(120),
   CONTACT_PHONE      VARCHAR2(20),
   CONTACT_EMAIL      VARCHAR2(100),
   CONTACT_ADDRESS    VARCHAR2(1000)
);
/
