--------------------------------------------------------
-- Copyright (c) 2008, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.6 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TYPES CREATED:				OBJ_REIM_POSTING_ACCT_TBL
--							OBJ_REIM_POSTING_ACCT_REC
----------------------------------------------------------------------------
--------------------------------------
--       Dropping TYPES
--------------------------------------
PROMPT Dropping 'OBJ_REIM_POSTING_ACCT_TBL'
DROP TYPE OBJ_REIM_POSTING_ACCT_TBL;

PROMPT Dropping 'OBJ_REIM_POSTING_ACCT_REC'
DROP TYPE OBJ_REIM_POSTING_ACCT_REC;

PROMPT Creating 'OBJ_REIM_POSTING_ACCT_REC'
CREATE OR REPLACE TYPE OBJ_REIM_POSTING_ACCT_REC AS OBJECT
(
    SEGMENT1           VARCHAR2(25),
    SEGMENT2           VARCHAR2(25),
    SEGMENT3           VARCHAR2(25),
    SEGMENT4           VARCHAR2(25),
    SEGMENT5           VARCHAR2(25),
    SEGMENT6           VARCHAR2(25),
    SEGMENT7           VARCHAR2(25),
    SEGMENT8           VARCHAR2(25),
    SEGMENT9           VARCHAR2(25),
    SEGMENT10          VARCHAR2(25),
    SEGMENT11          VARCHAR2(25),
    SEGMENT12          VARCHAR2(25),
    SEGMENT13          VARCHAR2(25),
    SEGMENT14          VARCHAR2(25),
    SEGMENT15          VARCHAR2(25),
    SEGMENT16          VARCHAR2(25),
    SEGMENT17          VARCHAR2(25),
    SEGMENT18          VARCHAR2(25),
    SEGMENT19          VARCHAR2(25),
    SEGMENT20          VARCHAR2(25),  
    SET_OF_BOOKS_ID    NUMBER(15),
    ACCOUNT_TYPE       VARCHAR2(6),
    ACCOUNT_CODE       VARCHAR2(20),
    TAX_CODE           VARCHAR2(6),
    STATUS             VARCHAR2(25),
    POSTING_AMT_ID     NUMBER(10)
)
/

CREATE OR REPLACE TYPE OBJ_REIM_POSTING_ACCT_TBL AS TABLE OF OBJ_REIM_POSTING_ACCT_REC
/
