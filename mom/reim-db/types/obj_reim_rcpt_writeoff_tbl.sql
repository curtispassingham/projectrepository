DROP TYPE obj_reim_rcpt_writeoff_tbl FORCE;
/

DROP TYPE obj_reim_rcpt_writeoff_rec FORCE;
/

CREATE OR REPLACE TYPE obj_reim_rcpt_writeoff_rec AS OBJECT
(
  shipment NUMBER(12),
  shipdate DATE,
  item VARCHAR2(25),
  unmatched_amt NUMBER(20,4),
  order_no NUMBER(12),
  location NUMBER(10),
  loc_type VARCHAR2(1)
)
/

CREATE OR REPLACE TYPE obj_reim_rcpt_writeoff_tbl AS TABLE OF obj_reim_rcpt_writeoff_rec
/
