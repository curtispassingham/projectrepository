DROP TYPE OBJ_ITEM_TAX_CALC_OVRD_TBL FORCE
/

DROP TYPE OBJ_ITEM_TAX_CALC_OVRD_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_ITEM_TAX_CALC_OVRD_REC AS OBJECT
(
  ITEM	VARCHAR2(25),
  SUPPLIER  NUMBER(10),
  LOCATION	NUMBER(10),
  TAX_CODE  VARCHAR2(6),
  TAX_RATE  NUMBER(20,10),
  TAX_BASIS_FORMULA  VARCHAR2(500),
  APPLICATION_ORDER  NUMBER(6)
)
/


CREATE OR REPLACE TYPE OBJ_ITEM_TAX_CALC_OVRD_TBL AS TABLE OF OBJ_ITEM_TAX_CALC_OVRD_REC
/
