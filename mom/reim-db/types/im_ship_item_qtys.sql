DROP TYPE im_ship_item_qtys_tbl FORCE;

DROP TYPE im_ship_item_qtys_rec FORCE;

CREATE OR REPLACE TYPE im_ship_item_qtys_rec AS OBJECT
(
 SHIPMENT      NUMBER(12),
 ITEM          VARCHAR2(25),
 QTY_MATCHED   NUMBER(12,4)
);
/

CREATE OR REPLACE TYPE im_ship_item_qtys_tbl AS TABLE OF im_ship_item_qtys_rec;
/
