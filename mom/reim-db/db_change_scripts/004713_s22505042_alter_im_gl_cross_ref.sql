--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
WHENEVER SQLERROR EXIT FAILURE

--------------------------------------
--       CREATE BACKUP TABLE
--------------------------------------
CREATE TABLE IM_GL_CROSS_REF_BKP AS SELECT * FROM IM_GL_CROSS_REF;

--------------------------------------
--       DROP OLD TABLE
--------------------------------------
DROP TABLE IM_GL_CROSS_REF;

--------------------------------------
--       CREATE NEW TABLE
--------------------------------------
CREATE TABLE IM_GL_CROSS_REF(
  GL_CROSS_REF_ID   NUMBER(10)   NOT NULL,
  ACCOUNT_TYPE      VARCHAR2(6)  NOT NULL,
  ACCOUNT_CODE      VARCHAR2(20) NOT NULL,
  SET_OF_BOOKS_ID   NUMBER(15,0) NOT NULL,
  TAX_CODE          VARCHAR2(6)  NULL,
  CREATED_BY        VARCHAR2(60) DEFAULT USER NOT NULL,
  CREATION_DATE     DATE         DEFAULT SYSDATE NOT NULL,
  LAST_UPDATED_BY   VARCHAR2(60) DEFAULT USER NOT NULL,
  LAST_UPDATE_DATE  DATE         DEFAULT SYSDATE NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1 NOT NULL,
  SEGMENT1          VARCHAR2(25) NULL,
  SEGMENT2          VARCHAR2(25) NULL,
  SEGMENT3          VARCHAR2(25) NULL,
  SEGMENT4          VARCHAR2(25) NULL,
  SEGMENT5          VARCHAR2(25) NULL,
  SEGMENT6          VARCHAR2(25) NULL,
  SEGMENT7          VARCHAR2(25) NULL,
  SEGMENT8          VARCHAR2(25) NULL,
  SEGMENT9          VARCHAR2(25) NULL,
  SEGMENT10         VARCHAR2(25) NULL,
  SEGMENT11         VARCHAR2(25) NULL,
  SEGMENT12         VARCHAR2(25) NULL,
  SEGMENT13         VARCHAR2(25) NULL,
  SEGMENT14         VARCHAR2(25) NULL,
  SEGMENT15         VARCHAR2(25) NULL,
  SEGMENT16         VARCHAR2(25) NULL,
  SEGMENT17         VARCHAR2(25) NULL,
  SEGMENT18         VARCHAR2(25) NULL,
  SEGMENT19         VARCHAR2(25) NULL,
  SEGMENT20         VARCHAR2(25) NULL
  );

COMMENT ON COLUMN im_gl_cross_ref.gl_cross_ref_id IS 'Unique identifier of the Cross Reference entry.';
COMMENT ON COLUMN im_gl_cross_ref.account_type IS 'categories of account codes that describe the invoice matching operations.';
COMMENT ON COLUMN im_gl_cross_ref.account_code IS 'codes for invoice matching operations within the category designated in the account_type column.';
COMMENT ON COLUMN im_gl_cross_ref.set_of_books_id IS 'set of books identifier';
COMMENT ON COLUMN im_gl_cross_ref.tax_code IS 'Tax code for account segment';
COMMENT ON COLUMN im_gl_cross_ref.created_by IS 'The username identifier of the user who created a record.';
COMMENT ON COLUMN im_gl_cross_ref.creation_date IS 'The date a record was created.';
COMMENT ON COLUMN im_gl_cross_ref.last_updated_by IS 'The username identifier of the user who last updated a record.';
COMMENT ON COLUMN im_gl_cross_ref.last_update_date IS 'The date a record was last updated.';
COMMENT ON COLUMN im_gl_cross_ref.object_version_id IS 'The version identifier for a record. Unique for each state of a single record.';
COMMENT ON COLUMN im_gl_cross_ref.segment1 IS 'alphanumeric value for segment 1 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment2 IS 'alphanumeric value for segment 2 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment3 IS 'alphanumeric value for segment 3 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment4 IS 'alphanumeric value for segment 4 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment5 IS 'alphanumeric value for segment 5 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment6 IS 'alphanumeric value for segment 6 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment7 IS 'alphanumeric value for segment 7 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment8 IS 'alphanumeric value for segment 8 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment9 IS 'alphanumeric value for segment 9 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment10 IS 'alphanumeric value for segment 10 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment11 IS 'alphanumeric value for segment 11 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment12 IS 'alphanumeric value for segment 12 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment13 IS 'alphanumeric value for segment 13 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment14 IS 'alphanumeric value for segment 14 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment15 IS 'alphanumeric value for segment 15 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment16 IS 'alphanumeric value for segment 16 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment17 IS 'alphanumeric value for segment 17 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment18 IS 'alphanumeric value for segment 18 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment19 IS 'alphanumeric value for segment 19 of financials gl account.';
COMMENT ON COLUMN im_gl_cross_ref.segment20 IS 'alphanumeric value for segment 20 of financials gl account.';
  
--------------------------------------
--       CREATE PRIMARY KEY
--------------------------------------  
ALTER TABLE IM_GL_CROSS_REF
  ADD CONSTRAINT PK_IM_GL_CROSS_REF UNIQUE (GL_CROSS_REF_ID);  

--------------------------------------
--       CREATE UNIQUE KEY
--------------------------------------  
ALTER TABLE IM_GL_CROSS_REF
  ADD CONSTRAINT UK_IM_GL_CROSS_REF UNIQUE (ACCOUNT_TYPE,
                                            ACCOUNT_CODE,
                                            SET_OF_BOOKS_ID,
                                            TAX_CODE);
  
--------------------------------------
--       INSERT DATA FROM BACKUP TABLE
--------------------------------------  
 INSERT INTO IM_GL_CROSS_REF(GL_CROSS_REF_ID,
                             ACCOUNT_TYPE,
                             ACCOUNT_CODE,
                             SEGMENT1,
                             SEGMENT2,
                             SEGMENT3,
                             SEGMENT4,
                             SEGMENT5,
                             SEGMENT6,
                             SEGMENT7,
                             SEGMENT8,
                             SEGMENT9,
                             SEGMENT10,
                             SEGMENT11,
                             SEGMENT12,
                             SEGMENT13,
                             SEGMENT14,
                             SEGMENT15,
                             SEGMENT16,
                             SEGMENT17,
                             SEGMENT18,
                             SEGMENT19,
                             SEGMENT20,
                             SET_OF_BOOKS_ID,
                             TAX_CODE,
                             CREATED_BY,
                             CREATION_DATE,
                             LAST_UPDATED_BY,
                             LAST_UPDATE_DATE,
                             OBJECT_VERSION_ID)
SELECT IM_GL_CROSS_REF_SEQ.NEXTVAL,
       ACCOUNT_TYPE,
       ACCOUNT_CODE,
       SEGMENT1,
       SEGMENT2,
       SEGMENT3,
       SEGMENT4,
       SEGMENT5,
       SEGMENT6,
       SEGMENT7,
       SEGMENT8,
       SEGMENT9,
       SEGMENT10,
       SEGMENT11,
       SEGMENT12,
       SEGMENT13,
       SEGMENT14,
       SEGMENT15,
       SEGMENT16,
       SEGMENT17,
       SEGMENT18,
       SEGMENT19,
       SEGMENT20,
       SET_OF_BOOKS_ID,
       TAX_CODE,
       USER CREATED_BY,
       SYSDATE CREATION_DATE,
       USER LAST_UPDATED_BY,
       SYSDATE LAST_UPDATE_DATE,
       1 OBJECT_VERSION_ID               
  FROM (SELECT ACCOUNT_TYPE,
               ACCOUNT_CODE,
               SET_OF_BOOKS_ID,
               DECODE(TAX_CODE,
			          'NOCODE', NULL,
					  TAX_CODE) TAX_CODE,
               SEGMENT_NO,
               Max(SEGMENT_VALUE) SEGMENT_VALUE
          FROM IM_GL_CROSS_REF_BKP
         GROUP BY ACCOUNT_TYPE,
                  ACCOUNT_CODE,
                  SET_OF_BOOKS_ID,
                  TAX_CODE,
                  SEGMENT_NO)
 PIVOT (MAX(SEGMENT_VALUE) FOR SEGMENT_NO IN (1 SEGMENT1, 2 SEGMENT2, 3 SEGMENT3, 4 SEGMENT4, 5 SEGMENT5,
                                              6 SEGMENT6, 7 SEGMENT7, 8 SEGMENT8, 9 SEGMENT9, 10 SEGMENT10,
                                              11 SEGMENT11, 12 SEGMENT12, 13 SEGMENT13, 14 SEGMENT14, 15 SEGMENT15,
                                              16 SEGMENT16, 17 SEGMENT17, 18 SEGMENT18, 19 SEGMENT19, 20 SEGMENT20)
       ); 
	   
--------------------------------------
--       DROP BACKUP TABLE
--------------------------------------
DROP TABLE IM_GL_CROSS_REF_BKP
/
