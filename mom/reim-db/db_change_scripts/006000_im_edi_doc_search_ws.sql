--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_EDI_DOC_SEARCH_WS'
ALTER TABLE IM_EDI_DOC_SEARCH_WS ADD GROUP_ID NUMBER (10) NULL
/

COMMENT ON COLUMN IM_EDI_DOC_SEARCH_WS.GROUP_ID is 'The Group Id of the document'
/

