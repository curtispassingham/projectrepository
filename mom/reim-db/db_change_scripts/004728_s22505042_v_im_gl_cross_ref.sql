--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Created : 		 V_IM_GL_CROSS_REF
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Creating View               
--------------------------------------
CREATE OR REPLACE VIEW V_IM_GL_CROSS_REF
AS
WITH GL_CROSS_REF AS (SELECT SET_OF_BOOKS_ID,
                             ACCOUNT_TYPE,
                             ACCOUNT_CODE,
                             TAX_CODE,
                             SEGMENT_NO,
                             SEGMENT_VALUE
                        FROM IM_GL_CROSS_REF
                             UNPIVOT INCLUDE NULLS (SEGMENT_VALUE FOR SEGMENT_NO IN (SEGMENT1 AS 1, SEGMENT2 AS 2, SEGMENT3 AS 3, SEGMENT4 AS 4,
                                                                                     SEGMENT5 AS 5, SEGMENT6 AS 6, SEGMENT7 AS 7, SEGMENT8 AS 8,
                                                                                     SEGMENT9 AS 9, SEGMENT10 AS 10, SEGMENT11 AS 11, SEGMENT12 AS 12,
                                                                                     SEGMENT13 AS 13, SEGMENT14 AS 14, SEGMENT15 AS 15, SEGMENT16 AS 16,
                                                                                     SEGMENT17 AS 17, SEGMENT18 AS 18, SEGMENT19 AS 19, SEGMENT20 AS 20))),
     GL_OPTIONS AS (SELECT SET_OF_BOOKS_ID,
                           MAX(SEGMENT_NO) MAX_SEGMENT_NO
                      FROM IM_GL_OPTIONS
                     GROUP BY SET_OF_BOOKS_ID)
SELECT GCR.SET_OF_BOOKS_ID,
       GCR.ACCOUNT_TYPE,
       GCR.ACCOUNT_CODE,
       GCR.TAX_CODE,
       GCR.SEGMENT_NO,
       GCR.SEGMENT_VALUE
  FROM GL_CROSS_REF GCR,
       GL_OPTIONS GO
 WHERE GCR.SET_OF_BOOKS_ID = GO.SET_OF_BOOKS_ID
   AND GCR.SEGMENT_NO     <= GO.MAX_SEGMENT_NO;
   