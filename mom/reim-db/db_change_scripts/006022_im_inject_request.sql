--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_INJECT_REQUEST
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_INJECT_REQUEST'
CREATE TABLE IM_INJECT_REQUEST
 (INJECT_ID NUMBER(10,0) NOT NULL,
  TEMPLATE_TYPE VARCHAR2(255 ) NOT NULL,
  TEMPLATE_KEY VARCHAR2(255 ) NOT NULL,
  REQUEST_DESC VARCHAR2(4000 ) NOT NULL,
  SOURCE_FILE VARCHAR2(300 ) NOT NULL,
  REJECT_FILE VARCHAR2(300 ),
  LANG NUMBER(6,0) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_INJECT_REQUEST is 'Holds the request for invoice induction'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.INJECT_ID is 'Inject ID, unique for each injecting process'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.TEMPLATE_TYPE is 'Stores the template type of the request'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.TEMPLATE_KEY is 'Stores the template of the request'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.REQUEST_DESC is 'The description for this request'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.SOURCE_FILE is 'Name of the Input file'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.REJECT_FILE is 'Name of the output file'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.LANG is 'Stores the user lanugage'
/


PROMPT Creating Primary Key on 'IM_INJECT_REQUEST'
ALTER TABLE IM_INJECT_REQUEST
 ADD CONSTRAINT PK_IM_INJECT_REQUEST PRIMARY KEY
  (INJECT_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'IM_INJECT_REQUEST'
 ALTER TABLE IM_INJECT_REQUEST
  ADD CONSTRAINT FK_IM_INJECT_REQUEST
  FOREIGN KEY (INJECT_ID)
 REFERENCES IM_INJECT_STATUS (INJECT_ID)
/

