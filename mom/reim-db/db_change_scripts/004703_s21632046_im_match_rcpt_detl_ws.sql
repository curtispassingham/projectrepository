--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT TRUNCATING Table 'IM_MATCH_RCPT_DETL_WS'
TRUNCATE TABLE IM_MATCH_RCPT_DETL_WS
/

PROMPT Modifying Table 'IM_MATCH_RCPT_DETL_WS'
ALTER TABLE IM_MATCH_RCPT_DETL_WS MODIFY UNIT_COST NOT NULL
/

PROMPT Modifying Primary Key on 'IM_MATCH_RCPT_DETL_WS'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'PK_IM_MATCH_RCPT_DETL_WS'
   AND constraint_TYPE = 'P';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE IM_MATCH_RCPT_DETL_WS DROP CONSTRAINT PK_IM_MATCH_RCPT_DETL_WS DROP INDEX';
end if;
end;
/

ALTER TABLE IM_MATCH_RCPT_DETL_WS
 ADD CONSTRAINT PK_IM_MATCH_RCPT_DETL_WS PRIMARY KEY
  (SHIPMENT,
   ITEM,
   WORKSPACE_ID,
   UNIT_COST
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

