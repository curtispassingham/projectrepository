--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_INJECT_DOC_ROW
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_INJECT_DOC_ROW'
CREATE TABLE IM_INJECT_DOC_ROW
 (INJECT_ID NUMBER(10,0) NOT NULL,
  TRANSACTION_ID NUMBER(10,0) NOT NULL,
  INJECT_DOC_ID NUMBER(10,0) NOT NULL,
  WKSHT_KEY VARCHAR2(255 ) NOT NULL,
  RECORD_NO NUMBER(10,0) NOT NULL,
  S9T_ROW BLOB NOT NULL,
  EXTRACTED VARCHAR2(1 ) NOT NULL,
  CREATED_BY VARCHAR2(60 ) NOT NULL,
  CREATION_DATE DATE NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) NOT NULL,
  LAST_UPDATE_DATE DATE NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_INJECT_DOC_ROW is 'This table stores staged document raw records.'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.INJECT_ID is 'Inject ID, unique for each injecting process'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.TRANSACTION_ID is 'Transaction ID, unique for each document per inecting process. May not be unique globally'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.INJECT_DOC_ID is 'Injector document ID, unique for each documents that is unique globally'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.WKSHT_KEY is 'Holds the document worksheet key'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.RECORD_NO is 'Line number order of the record in the raw document'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.S9T_ROW is 'Holds the row information'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.EXTRACTED is 'Flag indicating if the data has been extracted to an outbound data destination'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.CREATED_BY is 'The username identifier of the user who created a record.'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.CREATION_DATE is 'The date a record was created.'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.LAST_UPDATED_BY is 'The username identifier of the user who last updated a record.'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.LAST_UPDATE_DATE is 'The date a record was last updated.'
/

COMMENT ON COLUMN IM_INJECT_DOC_ROW.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/

