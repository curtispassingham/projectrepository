--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_DISCREPANCY_LIST_WS'
ALTER TABLE IM_DISCREPANCY_LIST_WS ADD AP_REVIEWER VARCHAR2 (30 ) NULL
/

COMMENT ON COLUMN IM_DISCREPANCY_LIST_WS.AP_REVIEWER is 'This column identifies the ap reviewer associated with the discrepancy'
/

