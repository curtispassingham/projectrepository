--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_REASON_TRAN_CODE_MAP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_REASON_TRAN_CODE_MAP'
CREATE TABLE IM_REASON_TRAN_CODE_MAP
 (REASON_TRAN_CODE_MAP_ID NUMBER(10,0) NOT NULL,
  REASON_CODE_ID VARCHAR2(20 ) NOT NULL,
  TRAN_TYPE VARCHAR2(6 ) NOT NULL,
  TRAN_CODE VARCHAR2(20 ) NOT NULL,
  SET_OF_BOOKS_ID NUMBER(15,0) NOT NULL,
  CREATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE  NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE  NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1  NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_REASON_TRAN_CODE_MAP is 'This table holds the mapping between Reason codes and Tran codes.'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.REASON_TRAN_CODE_MAP_ID is 'Self generated Numeric Identifier for a reason-tran code mapping'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.REASON_CODE_ID is 'Identifier of the Reason code'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.TRAN_TYPE is 'Category of transaction code that the reason code is mapped to. (RCA)'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.TRAN_CODE is 'The tran code which the reason code is mapped to.'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.SET_OF_BOOKS_ID is 'The Set of books Identifier'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.CREATED_BY is 'The username identifier of the user who created a record.'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.CREATION_DATE is 'The date a record was created.'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.LAST_UPDATED_BY is 'The username identifier of the user who last updated a record.'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.LAST_UPDATE_DATE is 'The date a record was last updated.'
/

COMMENT ON COLUMN IM_REASON_TRAN_CODE_MAP.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/


PROMPT Creating Primary Key on 'IM_REASON_TRAN_CODE_MAP'
ALTER TABLE IM_REASON_TRAN_CODE_MAP
 ADD CONSTRAINT PK_IM_REASON_TRAN_CODE_MAP PRIMARY KEY
  (REASON_TRAN_CODE_MAP_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'IM_REASON_TRAN_CODE_MAP'
ALTER TABLE IM_REASON_TRAN_CODE_MAP
 ADD CONSTRAINT UK_IM_TRAN_CODES UNIQUE
  (REASON_CODE_ID,
   TRAN_CODE,
   SET_OF_BOOKS_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'IM_REASON_TRAN_CODE_MAP'
 ALTER TABLE IM_REASON_TRAN_CODE_MAP
  ADD CONSTRAINT IRTCM_IRC_FK
  FOREIGN KEY (REASON_CODE_ID)
 REFERENCES IM_REASON_CODES (REASON_CODE_ID)
/


PROMPT Creating FK on 'IM_REASON_TRAN_CODE_MAP'
 ALTER TABLE IM_REASON_TRAN_CODE_MAP
  ADD CONSTRAINT IRTCM_ITC_FK
  FOREIGN KEY (TRAN_TYPE, TRAN_CODE)
 REFERENCES IM_TRAN_CODE (TRAN_TYPE, TRAN_CODE)
/




-- ======================================================
-- Data Migration Script for Reason Code Maintenance
-- ======================================================

DELETE FROM GTT_6_NUM_6_STR_6_DATE;

DELETE FROM IM_REASON_TRAN_CODE_MAP;

DELETE FROM IM_TRAN_CODE WHERE TRAN_TYPE = 'RCA';



INSERT INTO GTT_6_NUM_6_STR_6_DATE(VARCHAR2_1,  --NEW_TRAN_CODE
                                   VARCHAR2_2,  --OLD_REASON_CODE
                                   VARCHAR2_3)  --SET_OF_BOOKS_ID
SELECT IM_TRAN_CODE_SEQ.NEXTVAL,
       IGCR.ACCOUNT_CODE,
       IGCR.SET_OF_BOOKS_ID
  FROM IM_GL_CROSS_REF IGCR
 WHERE IGCR.ACCOUNT_TYPE = 'RCA'
  AND EXISTS (SELECT 'x'
                FROM IM_REASON_CODES IRC
               WHERE IRC.REASON_CODE_ID = IGCR.ACCOUNT_CODE);


INSERT INTO IM_TRAN_CODE(TRAN_ID,
                         TRAN_TYPE,
                         TRAN_TYPE_DESCRIPTION,
                         TRAN_CODE,
                         TRAN_CODE_DESCRIPTION)
                  SELECT IM_TRAN_ID_SEQ.NEXTVAL,
				         'RCA' TRAN_TYPE,
                         'Reason Code Action' TRAN_TYPE_DESCRIPTION,
                         GTT.VARCHAR2_1 TRAN_CODE,
                         'Generated tran code for reason code ' || GTT.VARCHAR2_2 || ' for Set of Books ID ' || GTT.VARCHAR2_3 TRAN_CODE_DESCRIPTION
                    FROM GTT_6_NUM_6_STR_6_DATE GTT;


INSERT INTO IM_REASON_TRAN_CODE_MAP(REASON_TRAN_CODE_MAP_ID,
                                    REASON_CODE_ID,
									TRAN_TYPE,
                                    TRAN_CODE,
                                    SET_OF_BOOKS_ID)
                             SELECT IM_REASON_TRAN_CODE_MAP_SEQ.NEXTVAL,
                                    IRC.REASON_CODE_ID,
									'RCA',
                                    GTT.VARCHAR2_1,
                                    GTT.VARCHAR2_3
                               FROM IM_REASON_CODES IRC,
                                    GTT_6_NUM_6_STR_6_DATE GTT
                              WHERE IRC.REASON_CODE_ID = GTT.VARCHAR2_2;

MERGE INTO IM_GL_CROSS_REF TGT
USING (SELECT GTT.VARCHAR2_1 NEW_ACCOUNT_CODE,
              IGCR.GL_CROSS_REF_ID
		 FROM GTT_6_NUM_6_STR_6_DATE GTT,
		      IM_GL_CROSS_REF IGCR
	    WHERE IGCR.ACCOUNT_CODE    = GTT.VARCHAR2_2
		  AND IGCR.SET_OF_BOOKS_ID = GTT.VARCHAR2_3) SRC
ON (TGT.GL_CROSS_REF_ID = SRC.GL_CROSS_REF_ID)
WHEN MATCHED THEN
   UPDATE
      SET TGT.ACCOUNT_CODE = SRC.NEW_ACCOUNT_CODE;

COMMIT;
/





