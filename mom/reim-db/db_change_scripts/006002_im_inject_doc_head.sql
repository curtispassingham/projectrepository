--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_INJECT_DOC_HEAD'
ALTER TABLE IM_INJECT_DOC_HEAD ADD MATCH_ID VARCHAR2 (30 ) NULL
/

COMMENT ON COLUMN IM_INJECT_DOC_HEAD.MATCH_ID is 'This column holds the id of the user that matched the document.'
/

