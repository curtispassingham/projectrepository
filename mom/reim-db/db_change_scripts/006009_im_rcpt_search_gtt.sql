--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_RCPT_SEARCH_GTT'
ALTER TABLE IM_RCPT_SEARCH_GTT ADD SHIP_DATE DATE  NULL
/

COMMENT ON COLUMN IM_RCPT_SEARCH_GTT.SHIP_DATE is 'This field contains the date the PO was shipped.'
/

