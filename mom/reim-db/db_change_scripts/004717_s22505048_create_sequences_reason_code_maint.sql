--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:			        IM_SEC_GRP_RC_SEQ
--						IM_REASON_TRAN_CODE_MAP_SEQ
--						IM_TRAN_CODE_SEQ
--						IM_TRAN_ID_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       ADDING SEQUENCE
--------------------------------------
CREATE SEQUENCE IM_SEC_GRP_RC_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/

CREATE SEQUENCE IM_REASON_TRAN_CODE_MAP_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/

CREATE SEQUENCE IM_TRAN_CODE_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/

CREATE SEQUENCE IM_TRAN_ID_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/