--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_UNMTCH_DOC_DETL_COMMENTS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_UNMTCH_DOC_DETL_COMMENTS'
CREATE TABLE IM_UNMTCH_DOC_DETL_COMMENTS
 (UNMTCH_ID NUMBER(10,0) NOT NULL,
  COMMENT_ID NUMBER(10,0) NOT NULL,
  COMMENT_TYPE VARCHAR2(6 ) NOT NULL,
  TEXT VARCHAR2(2000 ) NOT NULL,
  DOC_ID NUMBER(10,0) NOT NULL,
  ITEM VARCHAR2(25 ),
  DISCREPANCY_TYPE VARCHAR2(6 ),
  REASON_CODE_ID VARCHAR2(20 ),
  DEBIT_REASON_CODE VARCHAR2(20 ),
  CREATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE  NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1  NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_UNMTCH_DOC_DETL_COMMENTS is 'This table stores the records from IM_DOC_DETAIL_COMMENTS that were deleted by the unmatch process.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.UNMTCH_ID is 'Unique Identifier of the Unmatch Action'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.COMMENT_ID is 'This column holds the unique id of each comment in the system.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.COMMENT_TYPE is 'This column holds the type that determines whether the comment is external or internal'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.TEXT is 'This column holds user entered comment text'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.DOC_ID is 'This column holds the unique id of the document.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.ITEM is 'This column will hold the item Identifier.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.DISCREPANCY_TYPE is 'The discrepancy type column will be populated when comments are associated with a discrepancy resolution for the item on the document.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.REASON_CODE_ID is 'The reason code corresponding to a variance resolution for either a cost or quantity discrepancy. '
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.DEBIT_REASON_CODE is 'This column is used to check discrepancy comments from cost review details screen for debit memo documents'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.CREATED_BY is 'The username identifier of the user who created a record.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.CREATION_DATE is 'The date a record was created.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.LAST_UPDATED_BY is 'The username identifier of the user who last updated a record.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.LAST_UPDATE_DATE is 'The date a record was last updated.'
/

COMMENT ON COLUMN IM_UNMTCH_DOC_DETL_COMMENTS.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/

