--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_INJECT_DOC_ERROR'
ALTER TABLE IM_INJECT_DOC_ERROR ADD INJECT_ID NUMBER (10,0) DEFAULT -1 NOT NULL
/

COMMENT ON COLUMN IM_INJECT_DOC_ERROR.INJECT_ID is 'Unique Identifier of the Injection process'
/

