--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_TRAN_CODE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_TRAN_CODE'
CREATE TABLE IM_TRAN_CODE
 (TRAN_ID NUMBER(10,0) NOT NULL,
  TRAN_TYPE VARCHAR2(6 ) NOT NULL,
  TRAN_TYPE_DESCRIPTION VARCHAR2(250 ) NOT NULL,
  TRAN_CODE VARCHAR2(20 ) NOT NULL,
  TRAN_CODE_DESCRIPTION VARCHAR2(250 ) NOT NULL,
  CREATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE  NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE  NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1  NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_TRAN_CODE is 'This table holds the tran codes used by the system.'
/

COMMENT ON COLUMN IM_TRAN_CODE.TRAN_ID is 'Unique Identifier for the row'
/

COMMENT ON COLUMN IM_TRAN_CODE.TRAN_TYPE is 'Categories of transaction codes that describe the invoice matching operations.'
/

COMMENT ON COLUMN IM_TRAN_CODE.TRAN_TYPE_DESCRIPTION is 'Description of the tran type'
/

COMMENT ON COLUMN IM_TRAN_CODE.TRAN_CODE is 'Codes for invoice matching operations within the category designated in the tran_type column.'
/

COMMENT ON COLUMN IM_TRAN_CODE.TRAN_CODE_DESCRIPTION is 'Description of the tran code'
/

COMMENT ON COLUMN IM_TRAN_CODE.CREATED_BY is 'The username identifier of the user who created a record.'
/

COMMENT ON COLUMN IM_TRAN_CODE.CREATION_DATE is 'The date a record was created.'
/

COMMENT ON COLUMN IM_TRAN_CODE.LAST_UPDATED_BY is 'The username identifier of the user who last updated a record.'
/

COMMENT ON COLUMN IM_TRAN_CODE.LAST_UPDATE_DATE is 'The date a record was last updated.'
/

COMMENT ON COLUMN IM_TRAN_CODE.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/


PROMPT Creating Primary Key on 'IM_TRAN_CODE'
ALTER TABLE IM_TRAN_CODE
 ADD CONSTRAINT PK_IM_TRAN_CODE PRIMARY KEY
  (TRAN_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'IM_TRAN_CODE'
ALTER TABLE IM_TRAN_CODE
 ADD CONSTRAINT UK_IM_TRAN_CODE UNIQUE
  (TRAN_TYPE,
   TRAN_CODE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

ALTER TABLE IM_TRAN_CODE ADD CONSTRAINT CHK_ITC_TRAN_TYPE CHECK (TRAN_TYPE IN ('BT','NMC','RCA'))
/
