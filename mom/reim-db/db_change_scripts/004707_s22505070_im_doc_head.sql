--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_DOC_HEAD'
COMMENT ON COLUMN IM_DOC_HEAD.STATUS is 'Status describes the position of the document within the matching process and payment processes.  this column is mandatory not null.  the valid values are:  worksheet group (wgrp): documents in worksheet group status are members of document groups that are not approved.  documents in worksheet group status can not be accessed individually and must be accessed through their group.  ready for match (rmtch): documents in ready for match status have not been matched to a receipt(s) based on common supplier/po/location.  unmatched(unmtch)  unresolved match (urmtch): documents in unresolved match status have been matched to a receipt(s) based on common supplier/po/location, but the summary cost and/or summary quantity cannot be matched within defined vendor tolerance(s).  matched (mtch): documents in unresolved match status have been matched to a receipt(s) based on common supplier/po/location and the summary cost and summary quantity variances fall within defined supplier tolerance(s).  deleted (delete) documents in deleted status have been deleted by front end users.  these documents (and their supporting details) will be removed from the database by a batch purge process.  approved (apprve) documents in approved status are not considered by the automatch process.  these documents (for example, non-merchandise invoices, etc) are marked as approved so that they are picked up for payment.  posted (posted) documents in posted status have been paid by being exported to the financials system.'
/

PROMPT MODIFYING COLUMN 'STATUS' IN 'IM_DOC_HEAD'
UPDATE IM_DOC_HEAD SET STATUS = 'URMTCH' WHERE STATUS = 'MURMTH'
/

PROMPT COMMITTING COLUMN 'STATUS' IN 'IM_DOC_HEAD'
COMMIT
/

PROMPT MODIFYING CONSTRAINT 'CHK_IM_DOC_HEAD_STATUS'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_IM_DOC_HEAD_STATUS'
     AND constraint_TYPE = 'C';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE IM_DOC_HEAD DROP CONSTRAINT CHK_IM_DOC_HEAD_STATUS';
  end if;
end;
/

ALTER TABLE IM_DOC_HEAD ADD CONSTRAINT
 CHK_IM_DOC_HEAD_STATUS CHECK (STATUS IN ('RMTCH', 'URMTCH', 'MTCH', 'APPRVE', 'POSTED','DELETE','VOID','DISPUT','CONVT','SUBMIT','TAXDIS', 'WKSHT'))
/
