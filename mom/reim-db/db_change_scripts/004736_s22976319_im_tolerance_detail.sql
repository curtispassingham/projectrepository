--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

------------------------------------------------
--       Modifying Table IM_TOLERANCE_DETAIL              
------------------------------------------------
PROMPT DROPPING CONSTARINTS ON TABLE 'IM_TOLERANCE_DETAIL'
 DECLARE
  L_constraints_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraints_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ITD_MATCH_LEVEL_SL'
     AND constraint_TYPE = 'C';

  if (L_constraints_exists != 0) then
      execute immediate 'ALTER TABLE IM_TOLERANCE_DETAIL DROP CONSTRAINT CHK_ITD_MATCH_LEVEL_SL';
  end if;
end;
/

PROMPT ADDING CONSTARINTS ON TABLE 'IM_TOLERANCE_DETAIL'
ALTER TABLE IM_TOLERANCE_DETAIL ADD CONSTRAINT CHK_ITD_MATCH_LEVEL CHECK (MATCH_LEVEL IN ('S', 'L', 'P'))
/

COMMENT ON COLUMN IM_TOLERANCE_DETAIL.MATCH_LEVEL IS 'The matching level which a tolerance detail applies to. Valid values are S (Summary) and L (Line Item) and P (Parent Item).'
/

UPDATE IM_TOLERANCE_DETAIL SET auto_res_value = NULL, reason_code_id = NULL WHERE match_level IN ('S','P')
/

PROMPT ADDING CONSTARINTS ON TABLE 'IM_TOLERANCE_DETAIL'
ALTER TABLE IM_TOLERANCE_DETAIL ADD CONSTRAINT CHK_ITD_MATCH_LEVEL_AUTO_RES CHECK (MATCH_LEVEL IN ('S', 'P') AND AUTO_RES_VALUE IS NULL AND REASON_CODE_ID IS NULL OR MATCH_LEVEL = 'L')
/
  