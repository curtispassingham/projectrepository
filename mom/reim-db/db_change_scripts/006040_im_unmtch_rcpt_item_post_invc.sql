--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_UNMTCH_RCPT_ITEM_POST_INVC
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_UNMTCH_RCPT_ITEM_POST_INVC'
CREATE TABLE IM_UNMTCH_RCPT_ITEM_POST_INVC
 (UNMTCH_ID NUMBER(10,0) NOT NULL,
  SEQ_NO NUMBER(10,0) NOT NULL,
  DOC_ID NUMBER(10,0) NOT NULL,
  STATUS VARCHAR2(1 ),
  CREATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1 NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_UNMTCH_RCPT_ITEM_POST_INVC is 'This table stores the records from IM_RCPT_ITEM_POSTING_INVOICE that were deleted by the unmatch process.'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.UNMTCH_ID is 'Unique Identifier of the Unmatch Action'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.SEQ_NO is 'Sequence Number from Receipt Item Posting table.'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.DOC_ID is 'The Identifier of the document that is getting unmatched.'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.STATUS is 'Status of the Unmatch Action.'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.CREATED_BY is 'Identifier of the User who Originally performed the match'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.CREATION_DATE is 'Time at which the match was performed.'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.LAST_UPDATED_BY is 'Identifier of the User who Originally performed the match'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.LAST_UPDATE_DATE is 'Time at which the match was performed.'
/

COMMENT ON COLUMN IM_UNMTCH_RCPT_ITEM_POST_INVC.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/

