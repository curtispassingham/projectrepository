--------------------------------------------------------
-- Copyright (c) 2010, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
--------------------------------------------------------------------------------------
--	SEQUENCE CREATION:	IM_GL_CROSS_REF_SEQ	
--------------------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATE NEW SEQUENCE
--------------------------------------
CREATE SEQUENCE IM_GL_CROSS_REF_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/

