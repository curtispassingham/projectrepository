--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:				IM_PAYLOAD_ID_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       ADDING SEQUENCE
--------------------------------------
CREATE SEQUENCE IM_PAYLOAD_ID_SEQ
  INCREMENT BY 1
  START WITH 1000
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/
