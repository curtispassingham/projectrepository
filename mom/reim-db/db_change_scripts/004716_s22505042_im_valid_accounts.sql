--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_VALID_ACCOUNTS'
ALTER TABLE IM_VALID_ACCOUNTS MODIFY TAX_CODE NULL
/

COMMENT ON COLUMN IM_VALID_ACCOUNTS.TAX_CODE is 'Tax code for account segment'
/

PROMPT updating Table 'IM_VALID_ACCOUNTS'
UPDATE IM_VALID_ACCOUNTS SET TAX_CODE = NULL WHERE TAX_CODE = 'NOCODE'
/
