--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_DYNAMIC_SEGMENT_DEPT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_DYNAMIC_SEGMENT_DEPT'
CREATE TABLE IM_DYNAMIC_SEGMENT_DEPT
 (DYN_SEG_DEPT_ID NUMBER(10,0) NOT NULL,
  DEPT NUMBER(4,0) NOT NULL,
  SET_OF_BOOKS_ID NUMBER(15,0) NOT NULL,
  DEPT_SEGMENT VARCHAR2(25 ) NOT NULL,
  CREATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE  NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE  NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1  NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_DYNAMIC_SEGMENT_DEPT is 'This table holds the dynamic account segments for departments'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.DYN_SEG_DEPT_ID is 'unique identifier of the row.'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.DEPT is 'this column holds the department whose account segements are defined.'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.SET_OF_BOOKS_ID is 'set of books identifier.'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.DEPT_SEGMENT is 'this column holds the departments account segement.  this value will be used to build account numbers by the posting process.'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.CREATED_BY is 'the username identifier of the user who created a record.'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.CREATION_DATE is 'the date a record was created.'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.LAST_UPDATED_BY is 'the username identifier of the user who last updated a record.'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.LAST_UPDATE_DATE is 'the date a record was last updated.'
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_DEPT.OBJECT_VERSION_ID is 'the version identifier for a record. unique for each state of a single record.'
/


PROMPT Creating Primary Key on 'IM_DYNAMIC_SEGMENT_DEPT'
ALTER TABLE IM_DYNAMIC_SEGMENT_DEPT
 ADD CONSTRAINT PK_IM_DYNAMIC_SEGMENT_DEPT PRIMARY KEY
  (DYN_SEG_DEPT_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'IM_DYNAMIC_SEGMENT_DEPT'
ALTER TABLE IM_DYNAMIC_SEGMENT_DEPT
 ADD CONSTRAINT UK_IM_DYNAMIC_SEGMENT_DEPT UNIQUE
  (DEPT,
   SET_OF_BOOKS_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


INSERT INTO IM_DYNAMIC_SEGMENT_DEPT(DYN_SEG_DEPT_ID,
                                    DEPT,
                                    SET_OF_BOOKS_ID,
                                    DEPT_SEGMENT,
                                    CREATED_BY,
                                    CREATION_DATE,
                                    LAST_UPDATED_BY,
                                    LAST_UPDATE_DATE,
                                    OBJECT_VERSION_ID)
SELECT IM_DYN_SEG_DEPT_SEQ.NEXTVAL,
       DEPT,
       SET_OF_BOOKS_ID,
       DEPT_SEGMENT,
       USER CREATED_BY,
       SYSDATE CREATION_DATE,
       USER LAST_UPDATED_BY,
       SYSDATE LAST_UPDATE_DATE,
       1 OBJECT_VERSION_ID
  FROM (SELECT DEPT,
               SET_OF_BOOKS_ID,
               MAX(DEPT_SEGMENT) DEPT_SEGMENT 
          FROM IM_DYNAMIC_SEGMENT_DEPT_CLASS
         GROUP BY DEPT,
                  SET_OF_BOOKS_ID);

COMMIT;
/
