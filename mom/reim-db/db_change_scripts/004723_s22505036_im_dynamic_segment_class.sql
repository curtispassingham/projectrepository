--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

alter table IM_DYNAMIC_SEGMENT_DEPT_CLASS rename to IM_DYNAMIC_SEGMENT_CLASS
/


PROMPT Modifying Table 'IM_DYNAMIC_SEGMENT_CLASS'
ALTER TABLE IM_DYNAMIC_SEGMENT_CLASS ADD DYN_SEG_CLASS_ID NUMBER (10,0) DEFAULT IM_DYN_SEG_CLASS_SEQ.NEXTVAL  NOT NULL
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_CLASS.DYN_SEG_CLASS_ID is 'unique identifier of the row.'
/

ALTER TABLE IM_DYNAMIC_SEGMENT_CLASS DROP COLUMN DEPT_SEGMENT
/

ALTER TABLE IM_DYNAMIC_SEGMENT_CLASS MODIFY DEPT
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_CLASS.DEPT is 'this column holds the department of the class whose account segments are defined.'
/


PROMPT Modifying Primary Key on 'IM_DYNAMIC_SEGMENT_CLASS'
ALTER TABLE IM_DYNAMIC_SEGMENT_CLASS
 DROP CONSTRAINT PK_IM_DYNAMIC_SEGMENT_DEPT_CLA DROP INDEX
/


PROMPT Creating Primary Key on 'IM_DYNAMIC_SEGMENT_CLASS'
ALTER TABLE IM_DYNAMIC_SEGMENT_CLASS
 ADD CONSTRAINT PK_IM_DYNAMIC_SEGMENT_CLASS PRIMARY KEY
  (DYN_SEG_CLASS_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'IM_DYNAMIC_SEGMENT_CLASS'
ALTER TABLE IM_DYNAMIC_SEGMENT_CLASS
 ADD CONSTRAINT UK_IM_DYNAMIC_SEGMENT_CLASS UNIQUE
  (DEPT,
   CLASS,
   SET_OF_BOOKS_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

