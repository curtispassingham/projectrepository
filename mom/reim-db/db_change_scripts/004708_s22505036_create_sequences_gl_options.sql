--------------------------------------------------------
-- Copyright (c) 2010, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
--------------------------------------------------------------------------------------
--	SEQUENCE CREATION:	IM_DYN_SEG_LOC_SEQ,	IM_DYN_SEG_DEPT_SEQ	,IM_DYN_SEG_CLASS_SEQ	
--------------------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATE SEQUENCES
--------------------------------------

CREATE SEQUENCE IM_DYN_SEG_LOC_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/

CREATE SEQUENCE IM_DYN_SEG_DEPT_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/

CREATE SEQUENCE IM_DYN_SEG_CLASS_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE 
  NOORDER
/
