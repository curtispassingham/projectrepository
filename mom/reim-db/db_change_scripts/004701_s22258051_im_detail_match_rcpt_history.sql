--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_DETAIL_MATCH_RCPT_HISTORY'
ALTER TABLE IM_DETAIL_MATCH_RCPT_HISTORY MODIFY ITEM
/

COMMENT ON COLUMN IM_DETAIL_MATCH_RCPT_HISTORY.ITEM is 'This column will hold the the invoice item if substitution was performed during matching else the actual receipt item.'
/

ALTER TABLE IM_DETAIL_MATCH_RCPT_HISTORY MODIFY SUBSTITUTE_ITEM
/

COMMENT ON COLUMN IM_DETAIL_MATCH_RCPT_HISTORY.SUBSTITUTE_ITEM is 'This column will hold the actual receipt item if substitution was performed during matching else will be empty.'
/

PROMPT Modifying Primary Key on 'IM_DETAIL_MATCH_RCPT_HISTORY'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME ='PK_IM_DETAIL_MATCH_RCPT_HIST'
     AND constraint_TYPE = 'P';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE IM_DETAIL_MATCH_RCPT_HISTORY DROP CONSTRAINT PK_IM_DETAIL_MATCH_RCPT_HIST DROP INDEX';
  end if;
end;
/

PROMPT Creating Unique Key on 'IM_DETAIL_MATCH_RCPT_HISTORY'
ALTER TABLE IM_DETAIL_MATCH_RCPT_HISTORY
 ADD CONSTRAINT UK_IM_DETAIL_MATCH_RCPT_HIST UNIQUE
  (MATCH_ID,
   SHIPMENT,
   ITEM,
   SUBSTITUTE_ITEM
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

