--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_INVC_ITEM_VIEW_WS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_INVC_ITEM_VIEW_WS'
CREATE TABLE IM_INVC_ITEM_VIEW_WS
 (INVC_ITEM_VIEW_WS_ID NUMBER(10,0) NOT NULL,
  WORKSPACE_ID NUMBER(10,0) NOT NULL,
  INVOICE_ID NUMBER(10,0),
  EXT_DOC_ID VARCHAR2(150 ),
  ITEM VARCHAR2(25 ),
  ITEM_DESCRIPTION VARCHAR2(250 ),
  ITEM_PARENT VARCHAR2(25 ),
  DEPT NUMBER(12,0),
  CLASS NUMBER(12,0),
  SUBCLASS NUMBER(12,0),
  CURRENCY_CODE VARCHAR2(3 ),
  SUPPLIER_SITE_ID NUMBER(10,0),
  SUPPLIER_SITE_NAME VARCHAR2(240 ),
  SUPPLIER NUMBER(10,0),
  SUPPLIER_NAME VARCHAR2(240 ),
  AP_REVIEWER VARCHAR2(30 ),
  LOCATION NUMBER(10,0),
  LOC_TYPE VARCHAR2(1 ),
  LOCATION_NAME VARCHAR2(150 ),
  INVOICE_TOTAL_COST NUMBER(20,4),
  CASH_DISCOUNT VARCHAR2(1 ),
  MANUAL_GROUP_ID NUMBER(10,0),
  ORDER_NO NUMBER(12,0),
  PO_SUPPLIER NUMBER(10,0),
  PO_SUPPLIER_SITE_ID NUMBER(10,0),
  ORDER_UNIT_COST NUMBER(20,4),
  ORIG_ORDER_UNIT_COST NUMBER(20,4),
  ORDER_QTY NUMBER(12,4),
  INVOICE_UNIT_COST NUMBER(20,4),
  INVOICE_QTY NUMBER(12,4),
  INVOICE_EXT_COST NUMBER(20,4),
  COST_MATCHED VARCHAR2(1 ),
  QTY_MATCHED VARCHAR2(1 ),
  RECEIPT_ID NUMBER(12,0),
  RECEIPT_UNIT_COST NUMBER(20,4),
  RECEIPT_RECEIVED_QTY NUMBER(12,4),
  RECEIPT_AVAIL_QTY NUMBER(12,4),
  RECEIPT_EXT_COST NUMBER(20,4),
  UNIT_COST_VARIANCE NUMBER(20,4),
  UNIT_COST_VARIANCE_PCT NUMBER(20,10),
  COST_IN_TOLERANCE VARCHAR2(1 ),
  QTY_VARIANCE NUMBER(12,4),
  QTY_VARIANCE_PCT NUMBER(20,10),
  QTY_IN_TOLERANCE VARCHAR2(1 ),
  IN_TOLERANCE VARCHAR2(1 ),
  RECEIPT_COUNT NUMBER(10,0),
  CHOICE_FLAG VARCHAR2(1 ),
  SKU_DETAIL_MATCH_WS_ID NUMBER(10,0),
  INVC_DETAIL_MATCH_WS_ID NUMBER(10,0)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_INVC_ITEM_VIEW_WS is ''
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.INVC_ITEM_VIEW_WS_ID is 'Unique identifier for the row.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.WORKSPACE_ID is 'Identifies the workspace id for the UI session.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.INVOICE_ID is 'The invoice ID.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.EXT_DOC_ID is 'The external ID for the document.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.ITEM is 'The item.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.ITEM_DESCRIPTION is 'The description of the item.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.ITEM_PARENT is 'The parent of the item.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.DEPT is 'The department of the item.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.CLASS is 'The class of the item.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.SUBCLASS is 'The subclass of the item.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.CURRENCY_CODE is 'The currency code of the discrepancy.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.SUPPLIER_SITE_ID is 'The supplier site.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.SUPPLIER_SITE_NAME is 'The supplier site name.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.SUPPLIER is 'The Supplier Identifier.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.SUPPLIER_NAME is 'The supplier name.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.AP_REVIEWER is 'The accounts payable employee who is the default reviewer for this invoice '
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.LOCATION is 'The location of the discrepancy.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.LOC_TYPE is 'The type of location.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.LOCATION_NAME is 'The name of the location.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.INVOICE_TOTAL_COST is 'The total cost of the invoice.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.CASH_DISCOUNT is 'The cash discount amount.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.MANUAL_GROUP_ID is 'The identifier of the Manual group the invoice belongs to'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.ORDER_NO is 'The order number on the invoice.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.PO_SUPPLIER is 'The PO supplier.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.PO_SUPPLIER_SITE_ID is 'The PO supplier site.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.ORDER_UNIT_COST is 'The unit cost on the order.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.ORIG_ORDER_UNIT_COST is 'The original order cost'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.ORDER_QTY is 'The order quantity.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.INVOICE_UNIT_COST is 'The unit cost on the invoice.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.INVOICE_QTY is 'The qty on the invoice.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.INVOICE_EXT_COST is 'The extended cost of the invoice.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.COST_MATCHED is 'Cost match indicator.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.QTY_MATCHED is 'Quantity match indicator'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.RECEIPT_ID is 'The receipt identifier.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.RECEIPT_UNIT_COST is 'The unit cost on the receipt.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.RECEIPT_RECEIVED_QTY is 'The qty that has been received.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.RECEIPT_AVAIL_QTY is 'The available quantity on the receipt.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.RECEIPT_EXT_COST is 'The extended cost on the receipt.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.UNIT_COST_VARIANCE is 'Variance between Unit costs of Receipt and Invoice'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.UNIT_COST_VARIANCE_PCT is 'Variance between Unit costs of Receipt and Invoice in percent'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.COST_IN_TOLERANCE is 'Indicates whether the invoice and the receipt(s) unit cost is within tolerance'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.QTY_VARIANCE is 'Variance between Quantities of Receipt and Invoice'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.QTY_VARIANCE_PCT is 'Variance between Quantities of Receipt and Invoice in percent'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.QTY_IN_TOLERANCE is 'Indicates whether the invoice and the receipt(s) qty is within tolerance'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.IN_TOLERANCE is 'Indicates whether the invoice and the receipt(s) qty and cost are within tolerance'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.RECEIPT_COUNT is 'The number of receipts associated with this invoice / sku combination.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.CHOICE_FLAG is 'Indicates selection in the UI.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.SKU_DETAIL_MATCH_WS_ID is 'The item detail match workspace the list was generated from.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_WS.INVC_DETAIL_MATCH_WS_ID is 'The invoice detail match workspace the list was generated from.'
/


PROMPT Creating Primary Key on 'IM_INVC_ITEM_VIEW_WS'
ALTER TABLE IM_INVC_ITEM_VIEW_WS
 ADD CONSTRAINT PK_IM_INVC_ITEM_VIEW_WS PRIMARY KEY
  (INVC_ITEM_VIEW_WS_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Index on 'IM_INVC_ITEM_VIEW_WS'
 CREATE INDEX IM_INVC_ITEM_VIEW_WS_I1 on IM_INVC_ITEM_VIEW_WS
 (WORKSPACE_ID,
   INVOICE_ID,
   ITEM
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

