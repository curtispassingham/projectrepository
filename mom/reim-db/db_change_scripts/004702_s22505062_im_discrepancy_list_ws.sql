--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_DISCREPANCY_LIST_WS'
ALTER TABLE IM_DISCREPANCY_LIST_WS ADD COST_IN_TOLERANCE VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN IM_DISCREPANCY_LIST_WS.COST_IN_TOLERANCE is 'Indicates whether the invoice and the receipt(s) unit cost is within tolerance'
/

ALTER TABLE IM_DISCREPANCY_LIST_WS ADD QTY_IN_TOLERANCE VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN IM_DISCREPANCY_LIST_WS.QTY_IN_TOLERANCE is 'Indicates whether the invoice and the receipt(s) qty is within tolerance'
/

