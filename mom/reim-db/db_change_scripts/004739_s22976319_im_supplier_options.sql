--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_SUPPLIER_OPTIONS'
ALTER TABLE IM_SUPPLIER_OPTIONS ADD ONLINE_PARENT_MATCH_IND VARCHAR2 (1 ) DEFAULT 'N' NOT NULL
/

COMMENT ON COLUMN IM_SUPPLIER_OPTIONS.ONLINE_PARENT_MATCH_IND is 'Indicator that controls Style Level matching on the User Interface'
/

PROMPT ADDING CONSTRAINT 'CHK_ISO_ONLN_PAR_MTCH_IND'
ALTER TABLE IM_SUPPLIER_OPTIONS ADD CONSTRAINT CHK_ISO_ONLN_PAR_MTCH_IND CHECK (ONLINE_PARENT_MATCH_IND IN ('N','Y'))
/
