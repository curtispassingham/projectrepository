--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_DYNAMIC_SEGMENT_LOC'
ALTER TABLE IM_DYNAMIC_SEGMENT_LOC ADD DYN_SEG_LOC_ID NUMBER (10,0) DEFAULT IM_DYN_SEG_LOC_SEQ.NEXTVAL NOT NULL
/

COMMENT ON COLUMN IM_DYNAMIC_SEGMENT_LOC.DYN_SEG_LOC_ID is 'unique identifier of the row.'
/


PROMPT Modifying Primary Key on 'IM_DYNAMIC_SEGMENT_LOC'
ALTER TABLE IM_DYNAMIC_SEGMENT_LOC
 DROP CONSTRAINT PK_IM_DYNAMIC_SEGMENT_LOC DROP INDEX
/

ALTER TABLE IM_DYNAMIC_SEGMENT_LOC
 ADD CONSTRAINT PK_IM_DYNAMIC_SEGMENT_LOC PRIMARY KEY
  (DYN_SEG_LOC_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'IM_DYNAMIC_SEGMENT_LOC'
ALTER TABLE IM_DYNAMIC_SEGMENT_LOC
 ADD CONSTRAINT UK_IM_DYNAMIC_SEGMENT_LOC UNIQUE
  (LOCATION
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

