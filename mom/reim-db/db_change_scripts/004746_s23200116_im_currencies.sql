--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_CURRENCIES
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_CURRENCIES'
CREATE TABLE IM_CURRENCIES
 (CURRENCY_CODE VARCHAR2(3 ) NOT NULL,
  CURRENCY_COST_DEC NUMBER(1) NOT NULL,
  CREATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE  NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT 'USER ' NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE  NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1  NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_CURRENCIES is 'Amount aggregation precision per currency code.'
/

COMMENT ON COLUMN IM_CURRENCIES.CURRENCY_CODE is 'currency code'
/

COMMENT ON COLUMN IM_CURRENCIES.CURRENCY_COST_DEC is 'aggregation amount  precision'
/

COMMENT ON COLUMN IM_CURRENCIES.CREATED_BY is 'the username identifier of the user who created a record.'
/

COMMENT ON COLUMN IM_CURRENCIES.CREATION_DATE is 'the date a record was created.'
/

COMMENT ON COLUMN IM_CURRENCIES.LAST_UPDATED_BY is 'the username identifier of the user who last updated a record.'
/

COMMENT ON COLUMN IM_CURRENCIES.LAST_UPDATE_DATE is 'the date a record was last updated.'
/

COMMENT ON COLUMN IM_CURRENCIES.OBJECT_VERSION_ID is 'the version identifier for a record. unique for each state of a single record.'
/


PROMPT Creating Primary Key on 'IM_CURRENCIES'
ALTER TABLE IM_CURRENCIES
 ADD CONSTRAINT PK_IM_CURRENCIES PRIMARY KEY
  (CURRENCY_CODE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

ALTER TABLE IM_CURRENCIES ADD CONSTRAINT CHK_CURRENCY_COST_DEC CHECK (CURRENCY_COST_DEC BETWEEN 0 AND 4)
/

