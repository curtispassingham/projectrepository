--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_INJECT_STATUS'
ALTER TABLE IM_INJECT_STATUS MODIFY INJECT_ID NUMBER (10,0)
/

COMMENT ON COLUMN IM_INJECT_STATUS.INJECT_ID is 'Inject ID, unique for each injecting process'
/


PROMPT Creating Primary Key on 'IM_INJECT_STATUS'
ALTER TABLE IM_INJECT_STATUS
 ADD CONSTRAINT PK_IM_INJECT_STATUS PRIMARY KEY
  (INJECT_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

