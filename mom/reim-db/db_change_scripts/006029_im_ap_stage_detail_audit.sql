--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_AP_STAGE_DETAIL_AUDIT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_AP_STAGE_DETAIL_AUDIT'
CREATE TABLE IM_AP_STAGE_DETAIL_AUDIT
 (DOC_ID NUMBER(10) NOT NULL,
  SEQ_NO NUMBER(10) NOT NULL,
  TRAN_CODE VARCHAR2(6 ) NOT NULL,
  LINE_TYPE_LOOKUP_CODE VARCHAR2(25 ) NOT NULL,
  ORIGINAL_AMOUNT NUMBER(20,4) NOT NULL,
  ADJUSTED_AMOUNT NUMBER(20,4) NOT NULL,
  CREATED_BY VARCHAR2(60 ) NOT NULL,
  CREATION_DATE DATE NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_AP_STAGE_DETAIL_AUDIT is 'This table acts an audit table on IM_AP_STAGE_DETAIL. It is used to record the amount calculated by the application and the amount entered by the user on the screen. '
/

COMMENT ON COLUMN IM_AP_STAGE_DETAIL_AUDIT.DOC_ID is 'The Identifier of the Document whose amounts got adjusted.'
/

COMMENT ON COLUMN IM_AP_STAGE_DETAIL_AUDIT.SEQ_NO is 'Internally generated Sequence Number for the posting amount'
/

COMMENT ON COLUMN IM_AP_STAGE_DETAIL_AUDIT.TRAN_CODE is 'Transaction Code of the posting amount'
/

COMMENT ON COLUMN IM_AP_STAGE_DETAIL_AUDIT.LINE_TYPE_LOOKUP_CODE is 'Line type Lookup code of the posting amount.'
/

COMMENT ON COLUMN IM_AP_STAGE_DETAIL_AUDIT.ORIGINAL_AMOUNT is 'Original amount calculated through the batch.'
/

COMMENT ON COLUMN IM_AP_STAGE_DETAIL_AUDIT.ADJUSTED_AMOUNT is 'User entered value for the amount'
/

COMMENT ON COLUMN IM_AP_STAGE_DETAIL_AUDIT.CREATED_BY is 'The Identifier of the User who adjusted the amount.'
/

COMMENT ON COLUMN IM_AP_STAGE_DETAIL_AUDIT.CREATION_DATE is 'The date and time the adjustment was performed.'
/


PROMPT Creating Primary Key on 'IM_AP_STAGE_DETAIL_AUDIT'
ALTER TABLE IM_AP_STAGE_DETAIL_AUDIT
 ADD CONSTRAINT PK_IM_AP_STAGE_DETL_AUDIT PRIMARY KEY
  (DOC_ID,
   SEQ_NO
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

