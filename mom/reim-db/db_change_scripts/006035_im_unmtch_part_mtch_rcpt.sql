--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_UNMTCH_PART_MTCH_RCPT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_UNMTCH_PART_MTCH_RCPT'
CREATE TABLE IM_UNMTCH_PART_MTCH_RCPT
 (UNMTCH_ID NUMBER(10,0) NOT NULL,
  SHIPMENT NUMBER(12,0) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  QTY_MATCHED NUMBER(20,4) NOT NULL,
  CREATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1 NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_UNMTCH_PART_MTCH_RCPT is 'This table stores the records from IM_PARTIALLY_MATCHED_RECEIPTS that were deleted by the unmatch process.'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.UNMTCH_ID is 'Unique Identifier of the Unmatch Action'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.SHIPMENT is 'The Identifier of the shipment that is getting unmatched.'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.ITEM is 'The Item that is getting unmatched'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.QTY_MATCHED is 'The Unit Qty of the item that is getting unmatched.'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.CREATED_BY is 'Identifier of the User who Originally performed the match'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.CREATION_DATE is 'Time at which the match was performed.'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.LAST_UPDATED_BY is 'Identifier of the User who Originally performed the match'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.LAST_UPDATE_DATE is 'Time at which the match was performed.'
/

COMMENT ON COLUMN IM_UNMTCH_PART_MTCH_RCPT.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/

