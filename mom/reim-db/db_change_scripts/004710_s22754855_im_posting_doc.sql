-------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_POSTING_DOC'
ALTER TABLE IM_POSTING_DOC ADD SET_OF_BOOKS_ID NUMBER (15) NULL
/

COMMENT ON COLUMN IM_POSTING_DOC.SET_OF_BOOKS_ID is 'The Identifier of the Set of Books the document needs to post accounts to.'
/

ALTER TABLE IM_POSTING_DOC DROP COLUMN SUMMARY_MATCH_PRIMARY_DOC
/

