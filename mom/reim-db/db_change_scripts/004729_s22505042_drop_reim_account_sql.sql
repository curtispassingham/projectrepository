--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Dropping Package :                           REIM_ACCOUNT_SQL
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT Dropping package 'REIM_ACCOUNT_SQL'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'REIM_ACCOUNT_SQL';

  if (L_table_exists != 0) then
      execute immediate 'DROP PACKAGE REIM_ACCOUNT_SQL';
  end if;
end;
/
