--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_MATCH_GROUP_HEAD_WS'
ALTER TABLE IM_MATCH_GROUP_HEAD_WS ADD ITEM_PARENT VARCHAR2 (25 ) NULL
/

COMMENT ON COLUMN IM_MATCH_GROUP_HEAD_WS.ITEM_PARENT is 'Item Parent used for Detail Match at Parent level'
/

