--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_POSTING_DOC_ACCOUNTS'

PROMPT Creating Index 'IM_POSTING_DOC_ACCOUNTS_I1'
CREATE INDEX IM_POSTING_DOC_ACCOUNTS_I1 on IM_POSTING_DOC_ACCOUNTS
  (POSTING_AMT_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

