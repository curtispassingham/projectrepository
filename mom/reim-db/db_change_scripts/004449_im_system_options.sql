--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_SYSTEM_OPTIONS'
ALTER TABLE IM_SYSTEM_OPTIONS ADD INJECTOR_STAGED_HIST_DAYS NUMBER (3) DEFAULT 30 NOT NULL
/

COMMENT ON COLUMN IM_SYSTEM_OPTIONS.INJECTOR_STAGED_HIST_DAYS is 'The number of days the staged data is available before archive'
/

ALTER TABLE IM_SYSTEM_OPTIONS ADD BATCH_STATUS_HIST_DAYS NUMBER (3) DEFAULT 60 NOT NULL
/

COMMENT ON COLUMN IM_SYSTEM_OPTIONS.BATCH_STATUS_HIST_DAYS is 'The number of days the staged status data is available before archive'
/

ALTER TABLE IM_SYSTEM_OPTIONS ADD BATCH_THREAD_TIMEOUT_MS NUMBER (10) DEFAULT 3600000 NOT NULL
/

COMMENT ON COLUMN IM_SYSTEM_OPTIONS.BATCH_THREAD_TIMEOUT_MS is 'The Max number of milli seconds the main thread should wait for the child threads to complete the processing'
/

