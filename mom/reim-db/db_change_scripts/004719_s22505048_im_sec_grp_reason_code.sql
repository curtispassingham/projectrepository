--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_SEC_GRP_REASON_CODE'
ALTER TABLE IM_SEC_GRP_REASON_CODE ADD SEC_GRP_REASON_CODE_ID NUMBER (10,0) DEFAULT IM_SEC_GRP_RC_SEQ.NEXTVAL NOT NULL
/

COMMENT ON COLUMN IM_SEC_GRP_REASON_CODE.SEC_GRP_REASON_CODE_ID is 'Self generated Numeric Identifier for a security group - reason code mapping'
/


PROMPT Modifying Primary Key on 'IM_SEC_GRP_REASON_CODE'
ALTER TABLE IM_SEC_GRP_REASON_CODE
 DROP CONSTRAINT PK_IM_SEC_GRP_REASON_CODE DROP INDEX
/

ALTER TABLE IM_SEC_GRP_REASON_CODE
 ADD CONSTRAINT PK_IM_SEC_GRP_REASON_CODE PRIMARY KEY
  (SEC_GRP_REASON_CODE_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

