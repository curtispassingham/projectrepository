--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:				IM_GROUP_ID_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       ADDING SEQUENCE
--------------------------------------
PROMPT Creating Sequence 'IM_GROUP_ID_SEQ'
DECLARE

L_max_group_id NUMBER(10);

BEGIN

SELECT Nvl((SELECT Max(Nvl(group_id, 0)) FROM im_doc_head), 0) + 100  INTO L_max_group_id FROM dual;

EXECUTE IMMEDIATE 'CREATE SEQUENCE IM_GROUP_ID_SEQ INCREMENT BY 1 START WITH ' || To_Char(L_max_group_id) || ' MAXVALUE 9999999999 CACHE 100 CYCLE NOORDER';

END;
/