--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
ALTER TABLE IM_OI_INVOICE_REPORT RENAME COLUMN SUPPLIER TO SUPPLIER_SITE_ID
/

ALTER TABLE IM_OI_INVOICE_REPORT RENAME COLUMN SUP_NAME TO SUPPLIER_SITE_NAME
/

ALTER TABLE IM_OI_INVOICE_REPORT ADD SUPPLIER NUMBER(10,0)
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.SUPPLIER is 'Identifier of the Supplier on the Invoice.'
/
