--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Modified: 		 IM_INJECT_REQUEST
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Renaming Table 
--------------------------------------
PROMPT Renaming TABLE IM_INJECT_REQUEST
RENAME IM_INJECT_REQUEST TO DBC_IM_INJECT_REQUEST
/

-------------------------------------------
--       Creating Table IM_INJECT_REQUEST
-------------------------------------------

PROMPT Creating Table 'IM_INJECT_REQUEST'
CREATE TABLE IM_INJECT_REQUEST
 (INJECT_ID NUMBER(10,0) NOT NULL,
  TEMPLATE_TYPE VARCHAR2(255) NOT NULL,
  TEMPLATE_KEY VARCHAR2(255) NOT NULL,
  REQUEST_DESC VARCHAR2(4000) NOT NULL,
  SOURCE_FILE VARCHAR2(300) NOT NULL,
  B_SOURCE_FILE BLOB NOT NULL,
  REJECT_FILE VARCHAR2(300),
  B_REJECT_FILE BLOB,
  LANG VARCHAR2(10) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_INJECT_REQUEST is 'Holds the request for invoice induction'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.INJECT_ID is 'Inject ID, unique for each injecting process'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.TEMPLATE_TYPE is 'Stores the template type of the request'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.TEMPLATE_KEY is 'Stores the template of the request'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.REQUEST_DESC is 'The description for this request'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.SOURCE_FILE is 'Name of the Input file'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.REJECT_FILE is 'Name of the output file'
/

COMMENT ON COLUMN IM_INJECT_REQUEST.LANG is 'Stores the user lanugage'
/


--------------------------------------
--       Inserting Data
--------------------------------------
PROMPT Inserting Data...
INSERT INTO IM_INJECT_REQUEST
(INJECT_ID,    
 TEMPLATE_TYPE,
 TEMPLATE_KEY, 
 REQUEST_DESC, 
 SOURCE_FILE,
 B_SOURCE_FILE,  
 REJECT_FILE,  
 B_REJECT_FILE,
 LANG
)
SELECT
 INJECT_ID,    
 TEMPLATE_TYPE,
 TEMPLATE_KEY, 
 REQUEST_DESC, 
 SOURCE_FILE,  
 UTL_RAW.CAST_TO_RAW(SOURCE_FILE),
 REJECT_FILE, 
 UTL_RAW.CAST_TO_RAW(REJECT_FILE),
 LANG 
FROM DBC_IM_INJECT_REQUEST
/ 

COMMIT
/

--------------------------------------
--       Dropping Backup Table
--------------------------------------
DROP TABLE DBC_IM_INJECT_REQUEST
/

------------------------------------------------------------
--       Adding PK and FK constraints to IM_INJECT_REQUEST
------------------------------------------------------------

PROMPT Creating Primary Key on 'IM_INJECT_REQUEST'
ALTER TABLE IM_INJECT_REQUEST
 ADD CONSTRAINT PK_IM_INJECT_REQUEST PRIMARY KEY
  (INJECT_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'IM_INJECT_REQUEST'
 ALTER TABLE IM_INJECT_REQUEST
  ADD CONSTRAINT FK_IM_INJECT_REQUEST
  FOREIGN KEY (INJECT_ID)
 REFERENCES IM_INJECT_STATUS (INJECT_ID)
/


----------------------------------------------------------
--       Dropping Columns SOURCE_FILE and REJECT_FILE
----------------------------------------------------------
ALTER TABLE IM_INJECT_REQUEST DROP COLUMN SOURCE_FILE
/

ALTER TABLE IM_INJECT_REQUEST DROP COLUMN REJECT_FILE
/

-----------------------------------------------------------------
--       Renaming Columns back to SOURCE_FILE and REJECT_FILE 
-----------------------------------------------------------------
ALTER TABLE IM_INJECT_REQUEST RENAME COLUMN B_SOURCE_FILE TO SOURCE_FILE
/

COMMENT ON COLUMN IM_INJECT_REQUEST.SOURCE_FILE is 'Name of the Input file'
/

ALTER TABLE IM_INJECT_REQUEST RENAME COLUMN B_REJECT_FILE TO REJECT_FILE
/

COMMENT ON COLUMN IM_INJECT_REQUEST.REJECT_FILE is 'Name of the output file'
/
