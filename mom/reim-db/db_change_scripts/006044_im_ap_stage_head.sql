--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_AP_STAGE_HEAD'

PROMPT Creating Index 'IM_AP_STAGE_HEAD_I1'
CREATE INDEX IM_AP_STAGE_HEAD_I1 on IM_AP_STAGE_HEAD
  (DOC_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

