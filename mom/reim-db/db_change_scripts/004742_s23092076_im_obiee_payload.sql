--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_OBIEE_PAYLOAD
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_OBIEE_PAYLOAD'
CREATE TABLE IM_OBIEE_PAYLOAD
 (PAYLOAD_ID NUMBER(10) NOT NULL,
  PAYLOAD_TYPE VARCHAR2(30 ) NOT NULL,
  ATTRIB_KEY VARCHAR2(50 ) NOT NULL,
  ATTRIB_VALUE VARCHAR2(250 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_OBIEE_PAYLOAD is 'This table is used for staging payload for OBIEE'
/

COMMENT ON COLUMN IM_OBIEE_PAYLOAD.PAYLOAD_ID is 'Identifier of the payload to be staged for OBIEE'
/

COMMENT ON COLUMN IM_OBIEE_PAYLOAD.PAYLOAD_TYPE is 'Type of the payload'
/

COMMENT ON COLUMN IM_OBIEE_PAYLOAD.ATTRIB_KEY is 'Payload Attribute Key'
/

COMMENT ON COLUMN IM_OBIEE_PAYLOAD.ATTRIB_VALUE is 'Payload Attribute Value'
/

