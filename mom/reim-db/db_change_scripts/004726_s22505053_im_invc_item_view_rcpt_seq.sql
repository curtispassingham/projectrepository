--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:			        IM_INVC_ITEM_VIEW_RCPT_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       ADDING SEQUENCE
--------------------------------------
CREATE SEQUENCE IM_INVC_ITEM_VIEW_RCPT_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 1000
  CYCLE 
  NOORDER
/
