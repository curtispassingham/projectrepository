--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_MATCH_INVC_WS'
ALTER TABLE IM_MATCH_INVC_WS ADD PRE_PAID_IND VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN IM_MATCH_INVC_WS.PRE_PAID_IND is 'the column will indicate whether the invoice was paid prior to matching.  valid values are Y yes and N no.'
/

ALTER TABLE IM_MATCH_INVC_WS ADD CUSTOM_DOC_REF_1 VARCHAR2 (90 ) NULL
/

COMMENT ON COLUMN IM_MATCH_INVC_WS.CUSTOM_DOC_REF_1 is 'this column holds any custom information that the retailer would like to hold at a document level.'
/

ALTER TABLE IM_MATCH_INVC_WS ADD CUSTOM_DOC_REF_2 VARCHAR2 (90 ) NULL
/

COMMENT ON COLUMN IM_MATCH_INVC_WS.CUSTOM_DOC_REF_2 is 'this column holds any custom information that the retailer would like to hold at a document level.'
/

ALTER TABLE IM_MATCH_INVC_WS ADD CUSTOM_DOC_REF_3 VARCHAR2 (90 ) NULL
/

COMMENT ON COLUMN IM_MATCH_INVC_WS.CUSTOM_DOC_REF_3 is 'this column holds any custom information that the retailer would like to hold at a document level.'
/

ALTER TABLE IM_MATCH_INVC_WS ADD CUSTOM_DOC_REF_4 VARCHAR2 (90 ) NULL
/

COMMENT ON COLUMN IM_MATCH_INVC_WS.CUSTOM_DOC_REF_4 is 'this column holds any custom information that the retailer would like to hold at a document level.'
/

