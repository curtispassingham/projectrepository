--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_DISCREPANCY_LIST_WS'
ALTER TABLE IM_DISCREPANCY_LIST_WS ADD VPN VARCHAR2 (30 ) NULL
/

COMMENT ON COLUMN IM_DISCREPANCY_LIST_WS.VPN is 'VPN of the Item'
/

ALTER TABLE IM_DISCREPANCY_LIST_WS ADD DEALS_EXIST VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN IM_DISCREPANCY_LIST_WS.DEALS_EXIST is 'Indicator to denote if there are any deals on the PO/Item/Location'
/

