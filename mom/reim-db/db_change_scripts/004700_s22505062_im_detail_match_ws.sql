--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_DETAIL_MATCH_WS'
ALTER TABLE IM_DETAIL_MATCH_WS ADD MATCH_KEY_ID NUMBER (10) NULL
/

COMMENT ON COLUMN IM_DETAIL_MATCH_WS.MATCH_KEY_ID is 'The identifier of the match key, populated when a match is attempted from the discrepancy review list.'
/

