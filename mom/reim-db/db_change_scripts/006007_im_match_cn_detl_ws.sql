--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_MATCH_CN_DETL_WS'
ALTER TABLE IM_MATCH_CN_DETL_WS ADD VPN VARCHAR2 (30 ) NULL
/

COMMENT ON COLUMN IM_MATCH_CN_DETL_WS.VPN is 'VPN of the Item'
/

