--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_UNMTCH_SMRY_MATCH_INVC_HIST
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_UNMTCH_SMRY_MATCH_INVC_HIST'
CREATE TABLE IM_UNMTCH_SMRY_MATCH_INVC_HIST
 (UNMTCH_ID NUMBER(10,0) NOT NULL,
  MATCH_ID NUMBER(10,0) NOT NULL,
  DOC_ID NUMBER(10,0) NOT NULL,
  CREATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1 NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_UNMTCH_SMRY_MATCH_INVC_HIST is 'This table stores the records from IM_SUMMARY_MATCH_INVC_HISTORY that were deleted by the unmatch process.'
/

COMMENT ON COLUMN IM_UNMTCH_SMRY_MATCH_INVC_HIST.UNMTCH_ID is 'Unique Identifier of the Unmatch Action'
/

COMMENT ON COLUMN IM_UNMTCH_SMRY_MATCH_INVC_HIST.MATCH_ID is 'Unique Identifier of the Detail Match.'
/

COMMENT ON COLUMN IM_UNMTCH_SMRY_MATCH_INVC_HIST.DOC_ID is 'The Identifier of the document that is getting unmatched.'
/

COMMENT ON COLUMN IM_UNMTCH_SMRY_MATCH_INVC_HIST.CREATED_BY is 'Identifier of the User who Originally performed the match'
/

COMMENT ON COLUMN IM_UNMTCH_SMRY_MATCH_INVC_HIST.CREATION_DATE is 'Time at which the match was performed.'
/

COMMENT ON COLUMN IM_UNMTCH_SMRY_MATCH_INVC_HIST.LAST_UPDATED_BY is 'Identifier of the User who Originally performed the match'
/

COMMENT ON COLUMN IM_UNMTCH_SMRY_MATCH_INVC_HIST.LAST_UPDATE_DATE is 'Time at which the match was performed.'
/

COMMENT ON COLUMN IM_UNMTCH_SMRY_MATCH_INVC_HIST.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/

