--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_INJECT_DOC_RULE'
ALTER TABLE IM_INJECT_DOC_RULE ADD RULE_GROUP VARCHAR (500) NULL
/

COMMENT ON COLUMN IM_INJECT_DOC_RULE.RULE_GROUP is 'The worksheet , screen or record on which the rule failed'
/

