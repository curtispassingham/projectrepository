--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_MATCH_STGY_DETAIL'
COMMENT ON COLUMN IM_MATCH_STGY_DETAIL.MATCH_TYPE is 'Regular or Best or Parent (''R'',''B'',''P'')'
/

PROMPT DROPPING CONSTRAINT 'CHK_MATCH_TYPE'
 DECLARE
  L_constraints_exists_1 number := 0;
BEGIN
  SELECT count(*) INTO L_constraints_exists_1
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_MATCH_TYPE'
     AND constraint_TYPE = 'C';

  if (L_constraints_exists_1 != 0) then
      execute immediate 'ALTER TABLE IM_MATCH_STGY_DETAIL DROP CONSTRAINT CHK_MATCH_TYPE';
  end if;
end;
/

PROMPT ADDING CONSTRAINT 'CHK_IMSD_MATCH_TYPE'
ALTER TABLE IM_MATCH_STGY_DETAIL ADD CONSTRAINT CHK_IMSD_MATCH_TYPE CHECK (MATCH_TYPE IN ('R','B','P'))
/

PROMPT ADDING CONSTRAINT 'CHK_IMSD_MATCH_LEVEL_TYPE'
ALTER TABLE IM_MATCH_STGY_DETAIL ADD CONSTRAINT CHK_IMSD_MATCH_LEVEL_TYPE CHECK((MATCH_LEVEL ='SA' AND MATCH_TYPE = 'R') OR (MATCH_LEVEL ='SO' AND MATCH_TYPE IN ('R','B')) OR (MATCH_LEVEL = 'D' AND MATCH_TYPE IN ('R','B','P')))
/
