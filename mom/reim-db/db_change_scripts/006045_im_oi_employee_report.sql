--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_OI_EMPLOYEE_REPORT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_OI_EMPLOYEE_REPORT'
CREATE TABLE IM_OI_EMPLOYEE_REPORT
 (SESSION_ID NUMBER(15,0) NOT NULL,
  USER_ID VARCHAR2(30 ) NOT NULL,
  COMPLETE_PAST_DUE NUMBER(20,0),
  REMAIN_PAST_DUE NUMBER(20,0),
  COMPLETE_TODAY NUMBER(20,0),
  REMAIN_TODAY NUMBER(20,0),
  COMPLETE_TMRW NUMBER(20,0),
  REMAIN_TMRW NUMBER(20,0),
  COMPLETE_2DAYS_OUT NUMBER(20,0),
  REMAIN_2DAYS_OUT NUMBER(20,0),
  COMPLETE_3DAYS_OUT NUMBER(20,0),
  REMAIN_3DAYS_OUT NUMBER(20,0)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_OI_EMPLOYEE_REPORT is 'This table stores data for Employee Performance Report on ReIM OI dashboard.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.SESSION_ID is 'OI session ID of the current user session.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.USER_ID is 'The Identifier of the User whose statistics is shown.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.COMPLETE_PAST_DUE is 'Number of matched Invoices whose due date has passed.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.REMAIN_PAST_DUE is 'Number of yet to be matched Invoices whose due date has passed.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.COMPLETE_TODAY is 'Number of matched Invoices which are due today.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.REMAIN_TODAY is 'Number of yet to be matched Invoices which are due today.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.COMPLETE_TMRW is 'Number of matched Invoices which are due tomorrow.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.REMAIN_TMRW is 'Number of yet to be matched Invoices which are due tomorrow.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.COMPLETE_2DAYS_OUT is 'Number of matched Invoices which are due two days from today.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.REMAIN_2DAYS_OUT is 'Number of yet to be matched Invoices which due two days from today.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.COMPLETE_3DAYS_OUT is 'Number of matched Invoices which are due due three days from today.'
/

COMMENT ON COLUMN IM_OI_EMPLOYEE_REPORT.REMAIN_3DAYS_OUT is 'Number of yet to be matched Invoices which are due due three days from today.'
/

