--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Sequence Added: 		 IM_UNMTCH_ID_SEQ
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating SEQUENCE                
--------------------------------------
PROMPT Creating SEQUENCE 'IM_UNMTCH_ID_SEQ'
CREATE SEQUENCE IM_UNMTCH_ID_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  CACHE 100
  CYCLE
  NOORDER
  /
