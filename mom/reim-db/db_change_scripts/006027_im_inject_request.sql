--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_INJECT_REQUEST'
ALTER TABLE IM_INJECT_REQUEST ADD DATE_FORMAT VARCHAR (30 ) NULL
/

COMMENT ON COLUMN IM_INJECT_REQUEST.DATE_FORMAT is 'Date format of the locale'
/

ALTER TABLE IM_INJECT_REQUEST ADD DECIMAL_FORMAT VARCHAR (30 ) NULL
/

COMMENT ON COLUMN IM_INJECT_REQUEST.DECIMAL_FORMAT is 'Decimal format of the locale'
/

ALTER TABLE IM_INJECT_REQUEST ADD INTEGER_FORMAT VARCHAR (30 ) NULL
/

COMMENT ON COLUMN IM_INJECT_REQUEST.INTEGER_FORMAT is 'Integer format of the locale'
/

ALTER TABLE IM_INJECT_REQUEST ADD COUNTRY VARCHAR (2 ) NULL
/

COMMENT ON COLUMN IM_INJECT_REQUEST.COUNTRY is 'country of the language in locale'
/

