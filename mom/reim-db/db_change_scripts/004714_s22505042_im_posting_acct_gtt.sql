--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT truncating Table 'IM_POSTING_ACCT_GTT'
TRUNCATE TABLE IM_POSTING_ACCT_GTT
/

PROMPT Modifying Table 'IM_POSTING_ACCT_GTT'
ALTER TABLE IM_POSTING_ACCT_GTT MODIFY TAX_CODE NULL
/

COMMENT ON COLUMN IM_POSTING_ACCT_GTT.TAX_CODE is 'Tax code for account segment'
/

