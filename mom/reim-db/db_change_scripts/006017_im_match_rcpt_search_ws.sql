--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_MATCH_RCPT_SEARCH_WS'
ALTER TABLE IM_MATCH_RCPT_SEARCH_WS ADD SHIP_DATE DATE  NULL
/

COMMENT ON COLUMN IM_MATCH_RCPT_SEARCH_WS.SHIP_DATE is 'This field contains the date the PO was shipped.'
/

