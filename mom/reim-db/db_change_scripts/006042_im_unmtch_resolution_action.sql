--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_UNMTCH_RESOLUTION_ACTION
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_UNMTCH_RESOLUTION_ACTION'
CREATE TABLE IM_UNMTCH_RESOLUTION_ACTION
 (UNMTCH_ID NUMBER(10,0) NOT NULL,
  DOC_ID NUMBER(10,0) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  REASON_CODE_ID VARCHAR2(20 ) NOT NULL,
  ACTION VARCHAR2(6 ) NOT NULL,
  QTY NUMBER(12,4),
  UNIT_COST NUMBER(20,4),
  EXTENDED_COST NUMBER(20,4),
  STATUS VARCHAR2(1 ) NOT NULL,
  SHIPMENT NUMBER(12,0),
  CREATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1 NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_UNMTCH_RESOLUTION_ACTION is 'This table stores the records from IM_RESOLUTION_ACTION that were deleted by the unmatch process.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.UNMTCH_ID is 'Unique Identifier of the Unmatch Action'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.DOC_ID is 'The Identifier of the document that is getting unmatched.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.ITEM is 'The Identifier of the Item that is getting unmatched.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.REASON_CODE_ID is 'The Reason code used during matching.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.ACTION is 'The action associated with the Reason Code.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.QTY is 'The Adjustment Qty for the reason code.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.UNIT_COST is 'This column holds the unit cost difference or the new unit cost if action is a receiver cost adjustment.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.EXTENDED_COST is 'This column will hold the extened total cost adjustment for the resolution reason.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.STATUS is 'This column will hold the rollup status of the resolution.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.SHIPMENT is 'This column holds the receipt when it is associated with a discrepancy action of receiver adjustment.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.CREATED_BY is 'Identifier of the User who Originally performed the match'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.CREATION_DATE is 'Time at which the match was performed.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.LAST_UPDATED_BY is 'Identifier of the User who Originally performed the match'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.LAST_UPDATE_DATE is 'Time at which the match was performed.'
/

COMMENT ON COLUMN IM_UNMTCH_RESOLUTION_ACTION.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/

