--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_UNMTCH_STATUS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_UNMTCH_STATUS'
CREATE TABLE IM_UNMTCH_STATUS
 (UNMTCH_ID NUMBER(10,0) NOT NULL,
  UNMTCH_LVL VARCHAR2(1 ) NOT NULL,
  DOC_ID NUMBER(10,0),
  ITEM VARCHAR2(25 ),
  STATUS VARCHAR2(8 ) NOT NULL,
  CREATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  CREATION_DATE DATE DEFAULT SYSDATE NOT NULL,
  LAST_UPDATED_BY VARCHAR2(60 ) DEFAULT USER NOT NULL,
  LAST_UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  OBJECT_VERSION_ID NUMBER(15,0) DEFAULT 1 NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_UNMTCH_STATUS is 'This table stores the status of Unmatch Action'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.UNMTCH_ID is 'Unique Identifier of the Unmatch Action'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.UNMTCH_LVL is 'Level at which the unmatching is performed. Possible Values - ''D'' - Document, ''I'' - Item'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.DOC_ID is 'The Identifier of the document that is getting unmatched.'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.ITEM is 'The item that is getting unmatched.'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.STATUS is 'Status of the Unmatch Action.'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.CREATED_BY is 'Identifier of the User who performed the unmatch action'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.CREATION_DATE is 'Time at which the unmatch action is performed.'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.LAST_UPDATED_BY is 'Identifier of the User who performed the unmatch action'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.LAST_UPDATE_DATE is 'Time at which the unmatch action is performed.'
/

COMMENT ON COLUMN IM_UNMTCH_STATUS.OBJECT_VERSION_ID is 'The version identifier for a record. Unique for each state of a single record.'
/

