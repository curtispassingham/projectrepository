--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_OI_INVOICE_REPORT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_OI_INVOICE_REPORT'
CREATE TABLE IM_OI_INVOICE_REPORT
 (SESSION_ID NUMBER(15,0) NOT NULL,
  USER_ID VARCHAR2(30 ) NOT NULL,
  PRIORITY NUMBER(10,0),
  DOC_ID NUMBER(10,0),
  EXT_DOC_ID VARCHAR2(150 ),
  ORDER_NO NUMBER(12,0),
  SUPPLIER NUMBER(10,0),
  SUP_NAME VARCHAR2(240 ),
  LOCATION NUMBER(10,0),
  LOC_TYPE VARCHAR2(1 ),
  DOC_DATE DATE,
  DUE_DATE DATE,
  ITEMS NUMBER(10,0),
  TOTAL_COST NUMBER(20,4),
  TOTAL_COST_SY_CURR NUMBER(20,4),
  INVOICE_CURR VARCHAR2(3 ),
  SYSTEM_CURR VARCHAR2(3 ),
  CASH_DISCOUNT_IND VARCHAR2(1 ),
  COST_DISC_IND VARCHAR2(1 ),
  QTY_DISC_IND VARCHAR2(1 ),
  TAX_DISC_IND VARCHAR2(1 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_OI_INVOICE_REPORT is 'This table stores data for Invoices Report on ReIM OI dashboard.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.SESSION_ID is 'OI session ID of the current user session.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.USER_ID is 'The Identifier of the User who is responsible for the Invoices.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.PRIORITY is 'The priority of the Invoice. It is calculated by taking the due date, discount, discrepant status and the total amount of the invoice into consideration.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.DOC_ID is 'Internal Identifier of the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.EXT_DOC_ID is 'Vendor Document Number of the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.ORDER_NO is 'Order Number on the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.SUPPLIER is 'Identifier of the Supplier Site on the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.SUP_NAME is 'Name of the Supplier Site on the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.LOCATION is 'Location on the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.LOC_TYPE is 'The type of the location on the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.DOC_DATE is 'The date that the Invoice was generated.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.DUE_DATE is 'The due date of the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.ITEMS is 'Number of items on the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.TOTAL_COST is 'Total Amount Inclusive of Tax on the Invoice.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.TOTAL_COST_SY_CURR is 'Total Amount Inclusive of Tax on the Invoice (In System Currency)'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.INVOICE_CURR is 'Invoice Currency.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.SYSTEM_CURR is 'Primary Currency of the system.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.CASH_DISCOUNT_IND is 'Indicates if the Invoice is eligible for a cash discount.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.COST_DISC_IND is 'Indicates if any of the items on the Invoice is Cost discrepant.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.QTY_DISC_IND is 'Indicates if any of the items on the Invoice is Qty discrepant.'
/

COMMENT ON COLUMN IM_OI_INVOICE_REPORT.TAX_DISC_IND is 'Indicates if any of the items on the Invoice is Tax discrepant.'
/

