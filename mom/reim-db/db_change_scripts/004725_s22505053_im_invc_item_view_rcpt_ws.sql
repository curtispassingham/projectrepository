--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_INVC_ITEM_VIEW_RCPT_WS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_INVC_ITEM_VIEW_RCPT_WS'
CREATE TABLE IM_INVC_ITEM_VIEW_RCPT_WS
 (INVC_ITEM_VIEW_RCPT_WS_ID NUMBER(10,0) NOT NULL,
  INVC_ITEM_VIEW_WS_ID NUMBER(10,0) NOT NULL,
  WORKSPACE_ID NUMBER(10,0) NOT NULL,
  RCPT_DETAIL_MATCH_WS_ID NUMBER(10,0)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_INVC_ITEM_VIEW_RCPT_WS is ''
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_RCPT_WS.INVC_ITEM_VIEW_RCPT_WS_ID is 'Unique identifier for the row.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_RCPT_WS.INVC_ITEM_VIEW_WS_ID is 'The im_invc_item_view_ws row the receipts belong to.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_RCPT_WS.WORKSPACE_ID is 'Identifies the workspace id for the UI session.'
/

COMMENT ON COLUMN IM_INVC_ITEM_VIEW_RCPT_WS.RCPT_DETAIL_MATCH_WS_ID is 'The invoice detail match workspace the list was generated from.'
/


PROMPT Creating Primary Key on 'IM_INVC_ITEM_VIEW_RCPT_WS'
ALTER TABLE IM_INVC_ITEM_VIEW_RCPT_WS
 ADD CONSTRAINT PK_IM_INVC_ITEM_VIEW_RCPT_WS PRIMARY KEY
  (INVC_ITEM_VIEW_RCPT_WS_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Index on 'IM_INVC_ITEM_VIEW_RCPT_WS'
 CREATE INDEX IM_INVC_ITEM_VIEW_RCPT_WS_I1 on IM_INVC_ITEM_VIEW_RCPT_WS
 (INVC_ITEM_VIEW_WS_ID,
   WORKSPACE_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

