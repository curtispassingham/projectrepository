--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_TAX_REVIEW_LIST_SEARCH_WS'
ALTER TABLE IM_TAX_REVIEW_LIST_SEARCH_WS ADD CURRENCY_CODE VARCHAR2 (3 ) NULL
/

COMMENT ON COLUMN IM_TAX_REVIEW_LIST_SEARCH_WS.CURRENCY_CODE is 'The currency code associated with the document.'
/

