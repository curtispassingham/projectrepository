--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT truncating Table 'IM_POSTING_DOC_ACCOUNTS'
TRUNCATE TABLE IM_POSTING_DOC_ACCOUNTS
/

PROMPT Modifying Table 'IM_POSTING_DOC_ACCOUNTS'
ALTER TABLE IM_POSTING_DOC_ACCOUNTS MODIFY TAX_CODE NULL
/

COMMENT ON COLUMN IM_POSTING_DOC_ACCOUNTS.TAX_CODE is 'Tax Code for account segment'
/

