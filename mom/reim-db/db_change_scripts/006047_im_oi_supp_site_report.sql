--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 IM_OI_SUPP_SITE_REPORT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'IM_OI_SUPP_SITE_REPORT'
CREATE TABLE IM_OI_SUPP_SITE_REPORT
 (SESSION_ID NUMBER(15,0) NOT NULL,
  SUPPLIER NUMBER(10,0) NOT NULL,
  SUP_NAME VARCHAR2(240 ),
  COMPLIANCE_SCARD NUMBER(1,0),
  AUTOMATCH_COUNT NUMBER(15,0),
  AUTOMATCH_RATE NUMBER(5,2),
  COST_DISC_COUNT NUMBER(15,0),
  QTY_DISC_COUNT NUMBER(15,0),
  EARLY_SHIP_INVC_COUNT NUMBER(15,0),
  LATE_SHIP_INVC_COUNT NUMBER(15,0),
  INVOICE_COUNT NUMBER(15,0),
  TOTAL_AMOUNT NUMBER(20,4),
  SUPP_CURRENCY_CODE VARCHAR2(3 ),
  FILTER_IND VARCHAR2(1 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE IM_OI_SUPP_SITE_REPORT is 'This table stores the Supplier Site scorecard information for ReIM OI dashboard.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.SESSION_ID is 'OI session ID of the current user session.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.SUPPLIER is 'The Supplier Site Identifier'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.SUP_NAME is 'Supplier Site Name'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.COMPLIANCE_SCARD is 'Compliance Score card of the Supplier Site. Values between 1 and 5.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.AUTOMATCH_COUNT is 'Number of Invoices from the Supplier Site which were Automatched.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.AUTOMATCH_RATE is 'Percentage of Invoices from the Supplier Site which were Automatched.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.COST_DISC_COUNT is 'Number of Invoices from the Supplier Site which have/had cost discrepancies.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.QTY_DISC_COUNT is 'Number of Invoices from the Supplier Site which have/had qty discrepancies.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.EARLY_SHIP_INVC_COUNT is 'Number of Invoices from the Supplier Site which had early shipments.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.LATE_SHIP_INVC_COUNT is 'Number of Invoices from the Supplier Site which had delayed shipments.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.INVOICE_COUNT is 'Total Number of Invoices from the Supplier Site'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.TOTAL_AMOUNT is 'Total of amounts for all the Invoices from the Supplier Site'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.SUPP_CURRENCY_CODE is 'Contains a code identifying the currency the supplier uses for business transactions.'
/

COMMENT ON COLUMN IM_OI_SUPP_SITE_REPORT.FILTER_IND is 'Indicator used for filtering records in the UI.'
/

