--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Drop Table              im_inject_doc_lock 
--------------------------------------
PROMPT dropping primary key 'PK_IM_INJECT_DOC_LOCK'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'PK_IM_INJECT_DOC_LOCK'
     AND constraint_TYPE = 'P';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE IM_INJECT_DOC_LOCK DROP CONSTRAINT PK_IM_INJECT_DOC_LOCK';
  end if;
end;
/

PROMPT dropping Table 'IM_INJECT_DOC_LOCK'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'IM_INJECT_DOC_LOCK';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE IM_INJECT_DOC_LOCK';
  end if;
end;
/

