--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'IM_SYSTEM_OPTIONS'
ALTER TABLE IM_SYSTEM_OPTIONS ADD S9T_PURGE_DAYS NUMBER (2,0) DEFAULT 30 NOT NULL
/

COMMENT ON COLUMN IM_SYSTEM_OPTIONS.S9T_PURGE_DAYS is 'Number of days after which stage data for document Induction would be purged'
/


PROMPT ADDING CONSTRAINT 'CHK_IM_SYS_OPT_S9T_PURGE_DAYS'
ALTER TABLE IM_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_IM_SYS_OPT_S9T_PURGE_DAYS CHECK (S9T_PURGE_DAYS<= 30)
/
