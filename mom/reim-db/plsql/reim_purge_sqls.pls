CREATE OR REPLACE PACKAGE REIM_PURGE_SQL AS
------------------------------------------------------------------
FUNCTION CLEAR_DOCUMENT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------
FUNCTION CLEAR_WORKSPACES(O_error_message IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------
END REIM_PURGE_SQL;
/
