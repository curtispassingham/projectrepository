WHENEVER SQLERROR EXIT FAILURE ROLLBACK
 
CREATE OR REPLACE PACKAGE REIM_WRAPPER_SQL AS
-------------------------------------------------------------------------------------------
-- Name:    CONVERT
-- Purpose: Converts a value from one currency to another.
--          If NULL is passed in for I_currency or I_currency_out
--          the function will assume the primary currency.
--          For I_cost_retail_ind, pass 'C' if passing a cost,
--          'R' if passing a retail that does not need to be converted
--          to a 'smart' retail or 'P' for a retail that needs to be
--          'smart' by passing the value through calc_adjust.
--          If NULL is passed in for I_effective_date the function
--          will use today's date, otherwise pass in the date of
--          the currency rate to be used in the conversion.
--          If NULL is passed in for I_exchange_type,
--          the default on system_options will be used. If the
--          system_options.consolidation_ind = 'Y', use the
--          consolidated rate'C', otherwise use 'O' for the
--          operational rate.  If NULL is passed in for I_exchange_rate_in
--          or I_exchange_rate_out, then the exchange rate between
--          the in/out currency and the primary currency will be
--          retrieved and used.
-- This is a wrapper for RMS CURRENCY_SQL.CONVERT function.
-- It returns 0 if error occurs, 1 if successful.
-------------------------------------------------------------------------------------------
ADDR_TYPE_6                VARCHAR2(2) := '06';
MODULE_SUPP                VARCHAR2(4) := 'SUPP';

FUNCTION CONVERT (O_error_message        IN OUT  VARCHAR2,
                  I_currency_value       IN      NUMBER,
                  I_currency             IN      CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  I_currency_out         IN      CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  O_currency_value       IN OUT  NUMBER,
                  I_cost_retail_ind      IN      VARCHAR2,
                  I_effective_date       IN      CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                  I_exchange_type        IN      CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                  I_in_exchange_rate     IN      CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_out_exchange_rate    IN      CURRENCY_RATES.EXCHANGE_RATE%TYPE)
	RETURN NUMBER;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_INVOICE_MATCH_STATUS
-- Purpose:       This function serves as an API to allow
--                external systems to update the invoice matching
--                status for a shipment.
-------------------------------------------------------------------------------------------
FUNCTION UPDATE_INVOICE_MATCH_STATUS(O_error_message OUT VARCHAR2,
                                     I_shipments     IN  SHIPMENT_IDS,
                                     I_new_status    IN  VARCHAR2)
   RETURN NUMBER;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_INVOICE_QTY_MATCHED 
-- Purpose:       This function serves as an API to allow
--                external systems to update the invoice qty
--                matched for a shipment.
-------------------------------------------------------------------------------------------
FUNCTION UPDATE_INVOICE_QTY_MATCHED(O_error_message OUT VARCHAR2,
                                     I_shipment_item_qtys     IN  SHIPMENT_MATCHED_QTY_ARRAY)
   RETURN NUMBER;
-------------------------------------------------------------------------------------------
-- Function Name: CLEAR_STAGED_SHIPMENTS
-- Purpose      : This function removes all data from the stage_purged_shipments
--                and the stage_purged_shipksus tables for shipments not specified.
----------------------------------------------------------------------------------
FUNCTION CLEAR_STAGED_SHIPMENTS(O_error_message OUT VARCHAR2,
                                I_shipments     IN  SHIPMENT_IDS)
   RETURN NUMBER;
-------------------------------------------------------------------------------------------
-- Function Name: GET_ORACLE_VENDOR_SITE_ID
-- Purpose:       This function calls the ORG_UNIT_SQL.GET_LOC_VENDOR_SITE_ASSOC
--                PL/SQL package function within RMS, in order to retrieve the
--                Oracle Vendor Site Id associated to the Org Unit attribute of
--                the input location (store or warehouse).
-------------------------------------------------------------------------------------------
FUNCTION GET_ORACLE_VENDOR_SITE_ID(O_error_message         OUT VARCHAR2,
                                   O_oracle_vendor_site_id OUT NUMBER,
                                   I_location              IN  NUMBER,
						I_vendor_id			   IN  VARCHAR2,
						I_order_no              IN  NUMBER,
						I_rtv_ind 				IN varchar2)
   RETURN NUMBER;
-------------------------------------------------------------------------------------------
-- Function Name: UPDATE_SUPPLIER_SITE_ID
-- Purpose:       This function updates supplier site id on documents which do not have supplier site id populated.
-------------------------------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER_SITE_ID(O_error_message    OUT VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------------
END REIM_WRAPPER_SQL;
/
