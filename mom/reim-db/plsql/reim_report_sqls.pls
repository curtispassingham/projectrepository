CREATE OR REPLACE PACKAGE REIM_REPORT_SQL AS

/***********************
-- Parameters used in the BIP invoice match reports has to be declared in a package.
-- This package will define all the parameters used in the BIP reports. 
-- Currently only two parameters are used in the reports which
-- might be extended in the future.
****************/
   PM_doc_id             IM_DOC_HEAD.DOC_ID%TYPE;
   PM_receipt_id         IM_RWO_SHIPMENT_HIST.SHIPMENT%TYPE;
   
END REIM_REPORT_SQL;
/