CREATE OR REPLACE PACKAGE REIM_EDI_MAINT_SQL AS
----------------------------------------------------------------
VALID_SUPPLIER_EXISTS CONSTANT VARCHAR2(25) := 'VALID_SUPPLIER_EXISTS';
----------------------------------------------------------------
FUNCTION UPDATE_CHOICE_FLAG_SEARCH_WS(O_error_message    OUT VARCHAR2,
                                      I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                                      I_choice_flag   IN     IM_EDI_DOC_SEARCH_WS.CHOICE_FLAG%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION MARK_FOR_DELETE(O_error_message    OUT VARCHAR2,
                         I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                         I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE DEFAULT NULL)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION UPDATE_ORDER_NO(O_error_message        OUT VARCHAR2,
                         O_updated_doc_count    OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                         I_workspace_id      IN OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                         I_old_order_no      IN OUT IM_EDI_DOC_SEARCH_WS.ORDER_NO%TYPE,
                         I_new_order_no      IN OUT IM_EDI_DOC_SEARCH_WS.ORDER_NO%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION UPDATE_ITEM(O_error_message        OUT VARCHAR2,
                     O_updated_doc_count    OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                     I_workspace_id      IN OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                     I_old_item          IN OUT IM_INJECT_DOC_DETAIL.ITEM%TYPE,
                     I_new_item          IN OUT IM_INJECT_DOC_DETAIL.ITEM%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION SEARCH(O_error_message    OUT VARCHAR2,
                O_workspace_id         OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                O_max_rows_exceeded    OUT NUMBER,
                I_search_criteria   IN OUT IM_EDI_SEARCH_CRITERIA_REC)
RETURN NUMBER;
----------------------------------------------------------------
-- Name:    VALIDATE_DOCUMENT
-- Purpose: This function performs validations on an inject document.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
----------------------------------------------------------------
FUNCTION VALIDATE_DOCUMENT(O_error_message IN OUT VARCHAR2,
                           I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                           I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                           I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
-- Name:    LOAD_DOCUMENT
-- Purpose: This function creates a copy of the inject document for users to edit online.
----------------------------------------------------------------
FUNCTION LOAD_DOCUMENT(O_error_message IN OUT VARCHAR2,
					   O_inject_id        OUT IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                       O_inject_doc_id    OUT IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                       I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                       I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
-- Name:    SAVE_DOCUMENT
-- Purpose: This function saves Inject Document to Operational tables, marks the original inject document's errors to delete status.
----------------------------------------------------------------
FUNCTION SAVE_DOCUMENT(O_error_message IN OUT VARCHAR2,
                       I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                       I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
-- Name:    REJECT_DOCUMENT
-- Purpose: This function updates the Inject document's status such that the Injector batch picks them up for sending back to the vendor.
----------------------------------------------------------------
FUNCTION REJECT_DOCUMENT(O_error_message    OUT VARCHAR2,
                         I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                         I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
END REIM_EDI_MAINT_SQL;
/
