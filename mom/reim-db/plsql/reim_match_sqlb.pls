CREATE OR REPLACE PACKAGE BODY REIM_MATCH_SQL AS
----------------------------------------------------------------------------------
--PRIVATE PROTOTYPES
--
PROCEDURE UPDATE_ALL_RESULT_TXTS(I_ERROR_MESSAGE   IN VARCHAR2,
                                 I_PROCESS_ID      IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                 I_CONFIG_ID       IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                 I_SUPPLIER_ID     IN SUPS.SUPPLIER%TYPE,
                                 I_MATCH_LEVEL     IN IM_MATCH_POOL_RESULTS.MATCH_LEVEL%TYPE);
---
FUNCTION GET_MATCH_TYPE(I_CONFIG_ID IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE)
RETURN VARCHAR2;

----------------------------------------------------------------------------------

--PUBLIC FUNCTIONS

/***********************************
 Determines the applicable match tolerances for each
 match candidate document.  Match tolerances are defined
 at either supplier or system level (default).

 Supplier tolerances should be used when they exist.
 System default will be used when supplier tolerances do not exist

 This procedure will resolve which level to use per
 candidate match document.

 Requires:
 - IM_MATCH_DOC populated.

 Results:
 - IM_MATCH_DOC_TOLERANCES rows for each IM_MATCH_DOC.

***********************************/
PROCEDURE DETERMINE_MATCH_TOLERANCES (O_STATUS          OUT NUMBER,
                                      O_ERROR_MESSAGE   OUT VARCHAR2,
                                      I_PROCESS_ID      IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                      I_CONFIG_ID       IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                      I_SUPPLIER_ID     IN SUPS.SUPPLIER%TYPE)
IS

   L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.DETERMINE_MATCH_TOLERANCES';
   L_exists        varchar2(2)  := 'N';
   L_start_time    TIMESTAMP := SYSTIMESTAMP;

BEGIN

   select 'Y' 
     into L_exists
     from im_match_pool_config impc
    where impc.config_id = I_config_id;

   if L_exists = 'N' then
      O_STATUS := SUCCESS;
      RETURN;
   end if;

   insert into im_match_pool_tolerances (process_id,
                                         pool_key,
                                         tol_owner_type,
                                         tol_owner,
                                         tol_key,
                                         supplier,
                                         cost_quantity_ind,
                                         summary_line_ind,
                                         tolerance_document_type,
                                         lower_limit_inclusive,
                                         upper_limit_exclusive,
                                         favor_of,
                                         tol_value_type,
                                         tol_value,
                                         config_id,
                                         created_by,
                                         creation_date,
                                         last_updated_by,
                                         last_update_date,
                                         object_version_id)
   select i2.process_id,
          i2.pool_key,
          i2.tol_owner_type,
          i2.tol_owner,
          i2.tol_key,
          i2.supplier,
          i2.cost_quantity_ind,
          i2.summary_line_ind,
          i2.tolerance_document_type,
          i2.lower_limit_inclusive,
          i2.upper_limit_exclusive,
          decode(i2.favor_of,
                 REIM_CONSTANTS.TLR_FAVOR_OF_RETL, FAVOR_OF_RETAILER,
                 FAVOR_OF_SUPPLIER) favor_of,
          decode(i2.tol_value_type,
                 REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT, 'PCT',
                 'AMT') tol_value_type,
          i2.tol_value,
          i2.config_id,
          user    created_by,
          sysdate creation_date,
          user    last_updated_by,
          sysdate last_update_date,
          0       object_version_id
     from (select i.process_id,
                  i.pool_key,
                  i.tol_owner_type,
                  i.tol_owner,
                  i.tol_key,
                  i.supplier,
                  i.cost_quantity_ind,
                  i.summary_line_ind,
                  i.tolerance_document_type,
                  i.lower_limit_inclusive,
                  i.upper_limit_exclusive,
                  i.favor_of,
                  i.tol_value_type,
                  i.tol_value,
                  i.config_id,
                  i.lvl,
                  min(i.lvl) over (partition by i.process_id,
                                                i.pool_key,
                                                i.supplier,
                                                i.cost_quantity_ind,
                                                i.summary_line_ind,
                                                i.favor_of) min_lvl
             from (select distinct md.process_id,
                          md.pool_key,
                          'SUPPLIER' tol_owner_type,
                          md.supplier tol_owner,
                          td.tolerance_detail_id tol_key,
                          md.supplier,
                          td.match_type cost_quantity_ind,
                          td.match_level summary_line_ind,
                          md.doc_type tolerance_document_type,
                          td.lower_limit lower_limit_inclusive,
                          td.upper_limit upper_limit_exclusive,
                          td.favor_of favor_of,
                          td.tolerance_value_type tol_value_type,
                          td.tolerance_value tol_value,
                          md.config_id,
                          1 lvl
                     from im_match_doc md,
                          im_tolerance_level_map tm,
                          im_tolerance_detail td
                    where md.process_id           = I_process_id
                      and md.config_id            = I_config_id
                      and md.supplier             = I_supplier_id
                      --and tm.tolerance_level_type = REIM_CONSTANTS.TLR_LVL_TYPE_SUPP
                      and tm.tolerance_level_type = 'S'
                      and tm.tolerance_level_id   = md.supplier
                      and tm.tolerance_id         = td.tolerance_id
                   union all
                   select distinct md.process_id,
                          md.pool_key,
                          'SYSTEM' tol_owner_type,
                          null tol_owner,
                          td.tolerance_detail_id tol_key, 
                          md.supplier,
                          td.match_type cost_quantity_ind,
                          td.match_level summary_line_ind,
                          md.doc_type tolerance_document_type,
                          td.lower_limit lower_limit_inclusive,
                          td.upper_limit upper_limit_exclusive,
                          td.favor_of favor_of,
                          td.tolerance_value_type tol_value_type,
                          td.tolerance_value tol_value,
                          md.config_id,
                          2 lvl
                     from im_match_doc md,
                          im_tolerance_head th,
                          im_tolerance_detail td
                    where md.process_id           = I_process_id
                      and md.config_id            = I_config_id
                      and md.supplier             = I_supplier_id
                      and th.system_default       = 'Y' 
                      and th.tolerance_id         = td.tolerance_id) i) i2
    where i2.lvl = i2.min_lvl;

   LOGGER.LOG_INFORMATION(L_PROGRAM||' insert im_match_pool_tolerances- SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   O_STATUS := SUCCESS;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

   RETURN;

EXCEPTION
   WHEN OTHERS THEN
      O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')||
                         SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
      O_STATUS := FAIL;
END DETERMINE_MATCH_TOLERANCES;
----------------------------------------------------------------------------------


    /***********************************

     Performs summary match of documents for a given supplier.

     Requires:
     -  IM_MATCH_DOC populated with candidate match for given process_id, config_id, supplier_id

     Results:
     -  Successful summary matches (exact or within tolerance) will be reflected as follows:
        - IM_MATCH_DOC - match id assigned

    ***********************************/
PROCEDURE INIT_SUMMARY_MATCH(O_STATUS             OUT NUMBER,
                             O_ERROR_MESSAGE      OUT VARCHAR2,
                             I_PROCESS_ID      IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                             I_CONFIG_ID       IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                             I_SUPPLIER_ID     IN     SUPS.SUPPLIER%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.INIT_SUMMARY_MATCH';
        L_MATCH_TYPE    IM_MATCH_POOL_CONFIG.MATCH_TYPE%TYPE;
        L_CURRDATE      PERIOD.VDATE%TYPE;
        L_CALC_TOL      IM_SYSTEM_OPTIONS.CALC_TOLERANCE%TYPE;
        L_start_time    TIMESTAMP := SYSTIMESTAMP;
    BEGIN

        --------------------------
        -- Determine the current date from the PERIOD table...
        --------------------------
        SELECT VDATE INTO L_CURRDATE FROM PERIOD;

        --------------------------
        -- Calc tolerance
        --------------------------
        SELECT CALC_TOLERANCE INTO L_CALC_TOL FROM IM_SYSTEM_OPTIONS;

        --------------------------
        -- Determine the type of matching to be done (e.g. credit note with credit note requests, invoice with receipt)
        --------------------------
        L_MATCH_TYPE := GET_MATCH_TYPE(I_CONFIG_ID);

        --------------------------
        -- POPULATE IM_MATCH_POOL_RESULTS for the SUMMARY match session.
        --
        -- Match the "left hand side" (e.g. credit notes) with
        -- the "right hand side" (e.g. credit note requests) of each pool_key...

        -- IM_MATCH_P0OL_RESULTS is meant to capture each "side's" cost and qty total,
        -- determine the variance between them, and finally indicate
        -- which party the variance favors (retailer or supplier)...
        --------------------------
        INSERT INTO IM_MATCH_POOL_RESULTS (
            MATCH_POOL_RESULT_ID,
            PROCESS_ID,
            SUPPLIER,
            CONFIG_ID,
            POOL_KEY,
            MATCH_LEVEL,
            MATCH_DATE,
            ITEM,
            LHS_DOC_ID,
            LHS_TOTAL_COST,
            LHS_TOTAL_QTY,
            RHS_TOTAL_COST,
            RHS_TOTAL_QTY,
            TOTAL_COST_VAR,
            TOTAL_QTY_VAR,
            COST_VAR_FAVOR_OF,
            QTY_VAR_FAVOR_OF,
            IS_COST_VWT,
            IS_QTY_VWT,
            SUCCESS_MATCH_ID,
            RESULT_TXT
        )
        SELECT
                IM_MATCH_POOL_RESULT_ID_SEQ.NEXTVAL AS MATCH_POOL_RESULT_ID,
                I_PROCESS_ID            AS PROCESS_ID,
                I_SUPPLIER_ID           AS SUPPLIER,
                I_CONFIG_ID             AS CONFIG_ID,
                LHS.POOL_KEY            AS POOL_KEY,
                MATCH_LEVEL_SUMMARY     AS MATCH_LEVEL,
                L_CURRDATE              AS MATCH_DATE,
                NULL                    AS ITEM,
                NULL                    AS LHS_DOC_ID,
                LHS.COST_MATCH_TOTAL    AS LHS_TOTAL_COST,
                LHS.QTY_MATCH_TOTAL     AS LHS_TOTAL_QTY,
                RHS.COST_MATCH_TOTAL    AS RHS_TOTAL_COST,
                RHS.QTY_MATCH_TOTAL     AS RHS_TOTAL_QTY,
                RHS.COST_MATCH_TOTAL-LHS.COST_MATCH_TOTAL   AS TOTAL_COST_VAR,
                RHS.QTY_MATCH_TOTAL-LHS.QTY_MATCH_TOTAL     AS TOTAL_QTY_VAR,
                GET_FAVOR_OF(RHS.COST_MATCH_TOTAL, LHS.COST_MATCH_TOTAL, L_MATCH_TYPE) AS COST_FAVOR_OF,
                GET_FAVOR_OF(RHS.QTY_MATCH_TOTAL, LHS.QTY_MATCH_TOTAL, L_MATCH_TYPE) AS QTY_FAVOR_OF,
                Decode (RHS.COST_MATCH_TOTAL - LHS.COST_MATCH_TOTAL, 0, YN_YES, YN_NO) AS IS_COST_VWT,
                Decode (RHS.QTY_MATCH_TOTAL  - LHS.QTY_MATCH_TOTAL,  0, YN_YES, YN_NO) AS IS_QTY_VWT,
                NULL                    AS SUCCESS_MATCH_ID,
                NULL                    AS RESULT_TXT
        FROM
        (
                -- Subquery 1.1:  Select all credit notes for the process / supplier by pool_key
                -- and call it the left hand side of the match
                SELECT  IMD.POOL_KEY,
                        l_MATCH_TYPE         AS MATCH_TYPE,
                        Sum(

                            CASE
                            WHEN L_CALC_TOL IS NOT NULL AND L_CALC_TOL > 0 THEN
                                (SELECT Sum(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY) FROM IM_DOC_DETAIL_REASON_CODES IDDRC WHERE IDDRC.DOC_ID = IMD.DOC_REF)

                            ELSE
                                IDH.TOTAL_COST - Nvl((SELECT Sum(non_merch_amt) FROM im_doc_non_merch WHERE doc_id = IMD.DOC_REF ), 0)
                            END

                        ) AS COST_MATCH_TOTAL,
                        SUM(IDH.TOTAL_QTY) AS QTY_MATCH_TOTAL
                FROM    IM_MATCH_DOC IMD,
                        IM_DOC_HEAD IDH
                WHERE   IMD.PROCESS_ID    = I_PROCESS_ID
                AND     IMD.SUPPLIER      = I_SUPPLIER_ID
                AND     IMD.CONFIG_ID     = I_CONFIG_ID
                AND     IDH.DOC_ID        = IMD.DOC_REF
                AND     IDH.TYPE          = IMD.DOC_TYPE
                AND     IDH.TYPE          IN (DOC_TYPE_CREDIT_NOTE)
                AND     L_MATCH_TYPE      = MATCH_TYPE_CRDNT_CRDNRQ
                AND     IMD.MATCH_LEVEL   IS NULL
                AND     ((IDH.STATUS = 'POSTED' AND IDH.HOLD_STATUS = 'N') OR (IDH.STATUS = 'APPRVE' AND IDH.HOLD_STATUS IN ('H','R')))
                GROUP BY IMD.POOL_KEY
                --
                -- TODO:  invoice.match.support:  "union" similar subquery to selct invoices if P_MATCH_TYPE is MATCH_TYPE_MRCHI_RCPT
                --
        ) LHS,  -- as in "left-hand-side" of the match
        (
                -- Subquery 1.2:  Select all credit note requests for the process / supplier by pool_key
                -- and call it the right hand side of the match
                SELECT  IMD.POOL_KEY,
                        l_MATCH_TYPE         AS MATCH_TYPE,
                        Sum(

                            CASE
                            WHEN L_CALC_TOL IS NOT NULL AND L_CALC_TOL > 0 THEN
                                (SELECT Sum(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY) FROM IM_DOC_DETAIL_REASON_CODES IDDRC WHERE IDDRC.DOC_ID = IMD.DOC_REF)

                            ELSE
                                IDH.TOTAL_COST - Nvl((SELECT Sum(non_merch_amt) FROM im_doc_non_merch WHERE doc_id = IMD.DOC_REF ), 0)
                            END

                        ) AS COST_MATCH_TOTAL,
                        SUM(IDH.TOTAL_QTY) AS QTY_MATCH_TOTAL
                FROM    IM_MATCH_DOC IMD,
                        IM_DOC_HEAD IDH
                WHERE   IMD.PROCESS_ID    = I_PROCESS_ID
                AND     IMD.SUPPLIER      = I_SUPPLIER_ID
                AND     IMD.CONFIG_ID     = I_CONFIG_ID
                AND     IDH.DOC_ID        = IMD.DOC_REF
                AND     IDH.TYPE          = IMD.DOC_TYPE
                AND     IS_CREDIT_NOTE_REQUEST(IDH.TYPE) = YN_YES
                AND     L_MATCH_TYPE      = MATCH_TYPE_CRDNT_CRDNRQ
                AND     IMD.MATCH_LEVEL   IS NULL
                AND     IDH.STATUS NOT IN ('MTCH', 'POSTED')
                GROUP BY IMD.POOL_KEY
                --
                -- TODO:  invoice.match.support:  "union" similar subquery to selct receipts if P_MATCH_TYPE is MATCH_TYPE_MRCHI_RCPT
                --
        ) RHS  -- as in "right-hand-side" of the match
        WHERE LHS.POOL_KEY = RHS.POOL_KEY;

        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        O_STATUS := SUCCESS;

        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;
    END INIT_SUMMARY_MATCH;
----------------------------------------------------------------------------------




    /******************************

    Validates the summary match results.

    This function will set IM_MATCH_POOL_RESULTS.RESULT_TXT
    if a summary match cannot be accomplished due to:

    -  Cost and Quantity Discrepancies are out of tolerance.

    *******************************/
    PROCEDURE VALIDATE_SUMMARY_MATCH (O_STATUS         OUT NUMBER,
                                O_ERROR_MESSAGE  OUT VARCHAR2,
                                I_PROCESS_ID  IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID   IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID IN SUPS.SUPPLIER%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.VALIDATE_SUMMARY_MATCH';
        L_start_time    TIMESTAMP := SYSTIMESTAMP;
    BEGIN
        O_STATUS := SUCCESS;

        ---------------------------------
        -- Do general validation of match pool results
        ---------------------------------
        VALIDATE_MATCH_RESULTS( O_STATUS,
                                O_ERROR_MESSAGE,
                                I_PROCESS_ID,
                                I_CONFIG_ID,
                                I_SUPPLIER_ID);

        IF O_STATUS <> SUCCESS THEN
          RETURN;
        END IF;


        --------------------------
        -- Write a result comment to all summary matches that
        -- that has either cost or quantity variances outside of tolerance.
        --------------------------
        UPDATE IM_MATCH_POOL_RESULTS IMPR
        SET   IMPR.SUCCESS_MATCH_ID = NULL,
              IMPR.RESULT_TXT = AS_ERROR('Cost or quantity variance exists outside of tolerance')
        WHERE IMPR.PROCESS_ID = I_PROCESS_ID
        AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
        AND   IMPR.CONFIG_ID  = I_CONFIG_ID
        AND   IS_ERROR(IMPR.RESULT_TXT) = YN_NO
        AND   NOT(IMPR.IS_COST_VWT = YN_YES AND IMPR.IS_QTY_VWT = YN_YES)
        AND   IMPR.MATCH_LEVEL = MATCH_LEVEL_SUMMARY;

        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;

          RETURN;
    END VALIDATE_SUMMARY_MATCH;
----------------------------------------------------------------------------------



    /***********************************

     Performs one-to-one match of documents for a given supplier.

     Requires:
     -  IM_MATCH_DOC populated with candidate match for given process_id, config_id, supplier_id

     Results:
     -  Successful summary matches (exact or within tolerance) will be reflected as follows:
        - IM_MATCH_DOC - match id assigned

    ***********************************/
    PROCEDURE INIT_ONE_TO_ONE_MATCH (O_STATUS          OUT NUMBER,
                                      O_ERROR_MESSAGE   OUT VARCHAR2,
                                      I_PROCESS_ID      IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                      I_CONFIG_ID       IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                      I_SUPPLIER_ID     IN SUPS.SUPPLIER%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.INIT_ONE_TO_ONE_MATCH';
        L_MATCH_TYPE    IM_MATCH_POOL_CONFIG.MATCH_TYPE%TYPE;
        L_CURRDATE      PERIOD.VDATE%TYPE;
        L_CALC_TOL      IM_SYSTEM_OPTIONS.CALC_TOLERANCE%TYPE;
        L_start_time    TIMESTAMP := SYSTIMESTAMP;

    BEGIN

        --------------------------
        -- Determine the current date from the PERIOD table...
        --------------------------
        SELECT VDATE INTO L_CURRDATE FROM PERIOD;

        --------------------------
        -- Calc tolerance
        --------------------------
        SELECT CALC_TOLERANCE INTO L_CALC_TOL FROM IM_SYSTEM_OPTIONS;


        --------------------------
        -- Determine the type of matching to be done (e.g. credit note with credit note requests, invoice with receipt)
        --------------------------
        L_MATCH_TYPE := GET_MATCH_TYPE(I_CONFIG_ID);

        --------------------------
        -- POPULATE IM_MATCH_POOL_RESULTS for the one-to-one match session.
        --------------------------
        INSERT INTO IM_MATCH_POOL_RESULTS (
            MATCH_POOL_RESULT_ID,
            PROCESS_ID,
            SUPPLIER,
            CONFIG_ID,
            POOL_KEY,
            MATCH_LEVEL,
            MATCH_DATE,
            ITEM,
            LHS_DOC_ID,
            LHS_TOTAL_COST,
            LHS_TOTAL_QTY,
            RHS_DOC_ID,
            RHS_TOTAL_COST,
            RHS_TOTAL_QTY,
            TOTAL_COST_VAR,
            TOTAL_QTY_VAR,
            COST_VAR_FAVOR_OF,
            QTY_VAR_FAVOR_OF,
            IS_COST_VWT,
            IS_QTY_VWT,
            SUCCESS_MATCH_ID,
            RESULT_TXT
        )
        SELECT
                IM_MATCH_POOL_RESULT_ID_SEQ.NEXTVAL AS MATCH_POOL_RESULT_ID,
                I_PROCESS_ID            AS PROCESS_ID,
                I_SUPPLIER_ID           AS SUPPLIER,
                I_CONFIG_ID             AS CONFIG_ID,
                LHS.POOL_KEY            AS POOL_KEY,
                MATCH_LEVEL_ONE_TO_ONE  AS MATCH_LEVEL,
                L_CURRDATE              AS MATCH_DATE,
                NULL                    AS ITEM,
                LHS.DOC_ID              AS LHS_DOC_ID,
                LHS.COST_MATCH_TOTAL    AS LHS_TOTAL_COST,
                LHS.QTY_MATCH_TOTAL     AS LHS_TOTAL_QTY,
                RHS.DOC_ID              AS RHS_DOC_ID,
                RHS.COST_MATCH_TOTAL    AS RHS_TOTAL_COST,
                RHS.QTY_MATCH_TOTAL     AS RHS_TOTAL_QTY,
                RHS.COST_MATCH_TOTAL-LHS.COST_MATCH_TOTAL   AS TOTAL_COST_VAR,
                RHS.QTY_MATCH_TOTAL-LHS.QTY_MATCH_TOTAL     AS TOTAL_QTY_VAR,
                GET_FAVOR_OF(RHS.COST_MATCH_TOTAL, LHS.COST_MATCH_TOTAL, L_MATCH_TYPE) AS COST_FAVOR_OF,
                GET_FAVOR_OF(RHS.QTY_MATCH_TOTAL, LHS.QTY_MATCH_TOTAL, L_MATCH_TYPE) AS QTY_FAVOR_OF,
                Decode (RHS.COST_MATCH_TOTAL - LHS.COST_MATCH_TOTAL, 0, YN_YES, YN_NO) AS IS_COST_VWT,
                Decode (RHS.QTY_MATCH_TOTAL  - LHS.QTY_MATCH_TOTAL,  0, YN_YES, YN_NO) AS IS_QTY_VWT,
                NULL                    AS SUCCESS_MATCH_ID,
                  NULL                    AS RESULT_TXT
        FROM
        (
                -- Subquery 1.1:  Select all credit notes for the process / supplier by pool_key
                -- and call it the left hand side of the match
                SELECT  UNIQUE IMD.POOL_KEY,
                        l_MATCH_TYPE            AS MATCH_TYPE,
                        IDH.DOC_ID              AS DOC_ID,
                        CASE
                        WHEN L_CALC_TOL IS NOT NULL AND L_CALC_TOL > 0 THEN
                            (SELECT Sum(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY) FROM IM_DOC_DETAIL_REASON_CODES IDDRC WHERE IDDRC.DOC_ID = IMD.DOC_REF)

                        ELSE
                            IDH.TOTAL_COST - Nvl((SELECT Sum(non_merch_amt) FROM im_doc_non_merch WHERE doc_id = IMD.DOC_REF ), 0)
                        END

                        AS COST_MATCH_TOTAL,
                        IDH.TOTAL_QTY           AS QTY_MATCH_TOTAL
                FROM    IM_MATCH_DOC IMD,
                        IM_DOC_HEAD IDH
                WHERE   IMD.PROCESS_ID    = I_PROCESS_ID
                AND     IMD.SUPPLIER      = I_SUPPLIER_ID
                AND     IMD.CONFIG_ID     = I_CONFIG_ID
                AND     IDH.DOC_ID        = IMD.DOC_REF
                AND     IDH.TYPE          = IMD.DOC_TYPE
                AND     IDH.TYPE          IN (DOC_TYPE_CREDIT_NOTE)
                AND     L_MATCH_TYPE      = MATCH_TYPE_CRDNT_CRDNRQ
                AND     IMD.MATCH_LEVEL   IS NULL
                AND     ((IDH.STATUS = 'POSTED' AND IDH.HOLD_STATUS = 'N') OR (IDH.STATUS = 'APPRVE' AND IDH.HOLD_STATUS IN ('H','R')))

                --
                -- TODO:  invoice.match.support:  "union" similar subquery to selct invoices if P_MATCH_TYPE is MATCH_TYPE_MRCHI_RCPT
                --
        ) LHS,  -- as in "left-hand-side" of the match
        (
                -- Subquery 1.2:  Select all credit note requests for the process / supplier by pool_key
                -- and call it the right hand side of the match
                SELECT  UNIQUE IMD.POOL_KEY,
                        l_MATCH_TYPE            AS MATCH_TYPE,
                        IDH.DOC_ID              AS DOC_ID,
                        CASE
                        WHEN L_CALC_TOL IS NOT NULL AND L_CALC_TOL > 0 THEN
                            (SELECT Sum(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY) FROM IM_DOC_DETAIL_REASON_CODES IDDRC WHERE IDDRC.DOC_ID = IMD.DOC_REF)

                        ELSE
                            IDH.TOTAL_COST - Nvl((SELECT Sum(non_merch_amt) FROM im_doc_non_merch WHERE doc_id = IMD.DOC_REF ), 0)
                        END

                        AS COST_MATCH_TOTAL,
                        IDH.TOTAL_QTY           AS QTY_MATCH_TOTAL
                FROM    IM_MATCH_DOC IMD,
                        IM_DOC_HEAD IDH
                WHERE   IMD.PROCESS_ID    = I_PROCESS_ID
                AND     IMD.SUPPLIER      = I_SUPPLIER_ID
                AND     IMD.CONFIG_ID     = I_CONFIG_ID
                AND     IDH.DOC_ID        = IMD.DOC_REF
                AND     IDH.TYPE          = IMD.DOC_TYPE
                AND     IS_CREDIT_NOTE_REQUEST(IDH.TYPE) = YN_YES
                AND     L_MATCH_TYPE      = MATCH_TYPE_CRDNT_CRDNRQ
                AND     IMD.MATCH_LEVEL   IS NULL
                AND     IDH.STATUS NOT IN  ('MTCH','POSTED')

                --
                -- TODO:  invoice.match.support:  "union" similar subquery to selct receipts if P_MATCH_TYPE is MATCH_TYPE_MRCHI_RCPT
                --
        ) RHS  -- as in "right-hand-side" of the match
        WHERE LHS.POOL_KEY = RHS.POOL_KEY;   -- Force a cartesian product of documents on left and right hand sides PER pool key.

        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        O_STATUS := SUCCESS;

        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;
    END INIT_ONE_TO_ONE_MATCH;
----------------------------------------------------------------------------------


    /******************************

    Validates the summary match results.

    This function will set IM_MATCH_POOL_RESULTS.RESULT_TXT
    if a summary match cannot be accomplished due to:

    -  Cost and Quantity Discrepancies are out of tolerance.

    *******************************/
    PROCEDURE VALIDATE_ONE_TO_ONE_MATCH (O_STATUS         OUT NUMBER,
                                O_ERROR_MESSAGE  OUT VARCHAR2,
                                I_PROCESS_ID  IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID   IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID IN SUPS.SUPPLIER%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.VALIDATE_ONE_TO_ONE_MATCH';
        L_start_time    TIMESTAMP := SYSTIMESTAMP;
    BEGIN

        --------------------------
        -- Eliminate non-matches
        --------------------------

        ---------------------------------
        -- Do general validation of match pool results
        ---------------------------------
        VALIDATE_MATCH_RESULTS( O_STATUS,
                                O_ERROR_MESSAGE,
                                I_PROCESS_ID,
                                I_CONFIG_ID,
                                I_SUPPLIER_ID);

        IF O_STATUS <> SUCCESS THEN
          RETURN;
        END IF;


        --------------------------
        -- Any doc-to-doc matches out of tolerance are out of the game.
        --------------------------
        UPDATE IM_MATCH_POOL_RESULTS IMPR
        SET    IMPR.RESULT_TXT  = AS_ERROR('Cost or quantity variances are not within tolerance.')
        WHERE IMPR.PROCESS_ID   = I_PROCESS_ID
        AND   IMPR.CONFIG_ID    = I_CONFIG_ID
        AND   IMPR.SUPPLIER     = I_SUPPLIER_ID
        AND   IMPR.MATCH_LEVEL  = MATCH_LEVEL_ONE_TO_ONE
        AND   (IMPR.IS_COST_VWT = YN_NO
               OR
               IMPR.IS_QTY_VWT  = YN_NO);


        --------------------------
        -- When there's a document on one side that matches to
        -- two or more documents on the other side,  they're out.
        --------------------------
        UPDATE IM_MATCH_POOL_RESULTS IMPR
        SET   IMPR.RESULT_TXT  =  AS_ERROR(
                                  'Some documents on one side of the pool matches to two or more documents on the other side.')
        WHERE IMPR.PROCESS_ID  = I_PROCESS_ID
        AND   IMPR.SUPPLIER    = I_SUPPLIER_ID
        AND   IMPR.CONFIG_ID   = I_CONFIG_ID
        AND   IMPR.IS_COST_VWT = YN_YES
        AND   IMPR.IS_QTY_VWT  = YN_YES
        AND   IMPR.MATCH_LEVEL = MATCH_LEVEL_ONE_TO_ONE
        AND   EXISTS (
              SELECT IMPR2.LHS_DOC_ID, Count(*)
              FROM   IM_MATCH_POOL_RESULTS IMPR2
              WHERE  IMPR2.PROCESS_ID  = I_PROCESS_ID
              AND    IMPR2.SUPPLIER    = I_SUPPLIER_ID
              AND    IMPR2.CONFIG_ID   = I_CONFIG_ID
              AND    IMPR2.IS_COST_VWT = YN_YES
              AND    IMPR2.IS_QTY_VWT  = YN_YES
              AND    IMPR2.MATCH_LEVEL = MATCH_LEVEL_ONE_TO_ONE
              AND    IMPR2.POOL_KEY    = IMPR.POOL_KEY
              GROUP BY IMPR2.LHS_DOC_ID
              HAVING Count(*) > 1
              UNION  ALL
              SELECT IMPR2.RHS_DOC_ID, Count(*)
              FROM   IM_MATCH_POOL_RESULTS IMPR2
              WHERE  IMPR2.PROCESS_ID  = I_PROCESS_ID
              AND    IMPR2.SUPPLIER    = I_SUPPLIER_ID
              AND    IMPR2.CONFIG_ID   = I_CONFIG_ID
              AND    IMPR2.IS_COST_VWT = YN_YES
              AND    IMPR2.IS_QTY_VWT  = YN_YES
              AND    IMPR2.MATCH_LEVEL = MATCH_LEVEL_ONE_TO_ONE
              AND    IMPR2.POOL_KEY    = IMPR.POOL_KEY
              GROUP BY IMPR2.RHS_DOC_ID
              HAVING Count(*) > 1
        );

        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        O_STATUS := SUCCESS;


        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;

          RETURN;
    END VALIDATE_ONE_TO_ONE_MATCH;
----------------------------------------------------------------------------------



    /***********************************

     Performs detail match of documents for a given supplier.

     Requires:
     -  IM_MATCH_DOC populated with candidate match for given process_id, config_id, supplier_id

     Results:
     -  Successful summary matches (exact or within tolerance) will be reflected as follows:
        - IM_MATCH_DOC - match id assigned

    ***********************************/
    PROCEDURE INIT_DETAIL_MATCH (O_STATUS              OUT NUMBER,
                                      O_ERROR_MESSAGE      OUT VARCHAR2,
                                      I_PROCESS_ID         IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                      I_CONFIG_ID          IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                      I_SUPPLIER_ID        IN SUPS.SUPPLIER%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.INIT_DETAIL_MATCH';
        L_MATCH_TYPE    IM_MATCH_POOL_CONFIG.MATCH_TYPE%TYPE;
        L_CURRDATE      PERIOD.VDATE%TYPE;
        L_ANY_COST_VOT  VARCHAR2(1) := YN_NO;
        L_ANY_QTY_VOT   VARCHAR2(1) := YN_NO;
        L_CBC_REASON    IM_SYSTEM_OPTIONS.DFLT_COST_OVERBILL_RC%TYPE  := NULL;
        L_CBQ_REASON    IM_SYSTEM_OPTIONS.DFLT_QTY_OVERBILL_RC%TYPE   := NULL;
        L_CMC_REASON    IM_SYSTEM_OPTIONS.DFLT_COST_UNDERBILL_RC%TYPE := NULL;
        L_CMQ_REASON    IM_SYSTEM_OPTIONS.DFLT_QTY_UNDERBILL_RC%TYPE  := NULL;
        L_start_time    TIMESTAMP := SYSTIMESTAMP;
    BEGIN

        --------------------------
        -- Retrieve and validate the default reason codes setup in system options
        --------------------------
        GET_VALID_DEFAULT_REASON_CODES(O_STATUS,
                                          O_ERROR_MESSAGE,
                                          L_CBC_REASON,
                                          L_CBQ_REASON,
                                          L_CMC_REASON,
                                          L_CMQ_REASON);
        IF O_STATUS = FAIL THEN
            RETURN;
        END IF;

        --------------------------
        -- Determine the current date from the PERIOD table...
        --------------------------
        SELECT VDATE INTO L_CURRDATE FROM PERIOD;

        --------------------------
        -- Determine the type of matching to be done (e.g. credit note with credit note requests, invoice with receipt)
        --------------------------
        L_MATCH_TYPE := GET_MATCH_TYPE(I_CONFIG_ID);



        --------------------------
        -- POPULATE IM_MATCH_POOL_RESULTS for the detail match session.
        --------------------------
        INSERT INTO IM_MATCH_POOL_RESULTS (
            MATCH_POOL_RESULT_ID,
            PROCESS_ID,
            SUPPLIER,
            CONFIG_ID,
            POOL_KEY,
            MATCH_LEVEL,
            MATCH_DATE,
            ITEM,
            LHS_DOC_ID,
            LHS_TOTAL_COST,
            LHS_TOTAL_QTY,
            RHS_DOC_ID,
            RHS_TOTAL_COST,
            RHS_TOTAL_QTY,
            TOTAL_COST_VAR,
            TOTAL_QTY_VAR,
            COST_VAR_FAVOR_OF,
            QTY_VAR_FAVOR_OF,
            IS_COST_VWT,
            IS_QTY_VWT,
            SUCCESS_MATCH_ID,
            RESULT_TXT
        )
        SELECT IM_MATCH_POOL_RESULT_ID_SEQ.NEXTVAL AS MATCH_POOL_RESULT_ID, A.* FROM(
        SELECT  I_PROCESS_ID            AS PROCESS_ID,
                I_SUPPLIER_ID           AS SUPPLIER,
                I_CONFIG_ID             AS CONFIG_ID,
                LHS.POOL_KEY            AS POOL_KEY,
                MATCH_LEVEL_DETAIL      AS MATCH_LEVEL,
                L_CURRDATE              AS MATCH_DATE,
                LHS.ITEM                AS ITEM,
                LHS.DOC_ID              AS LHS_DOC_ID,
                LHS.COST_MATCH_TOTAL    AS LHS_TOTAL_COST,
                LHS.QTY_MATCH_TOTAL     AS LHS_TOTAL_QTY,
                NULL                    AS RHS_DOC_ID,
                Nvl(RHS.COST_MATCH_TOTAL, 0)                          AS RHS_TOTAL_COST,
                Nvl(RHS.QTY_MATCH_TOTAL,  0)                          AS RHS_TOTAL_QTY,
                Nvl(RHS.COST_MATCH_TOTAL, 0) - LHS.COST_MATCH_TOTAL   AS TOTAL_COST_VAR,
                Nvl(RHS.QTY_MATCH_TOTAL,  0) - LHS.QTY_MATCH_TOTAL    AS TOTAL_QTY_VAR,
                GET_FAVOR_OF(RHS.COST_MATCH_TOTAL, LHS.COST_MATCH_TOTAL, L_MATCH_TYPE) AS COST_FAVOR_OF,
                GET_FAVOR_OF(RHS.QTY_MATCH_TOTAL, LHS.QTY_MATCH_TOTAL, L_MATCH_TYPE) AS QTY_FAVOR_OF,
                Decode (Nvl(RHS.COST_MATCH_TOTAL, 0) - LHS.COST_MATCH_TOTAL, 0, YN_YES, YN_NO) AS IS_COST_VWT,
                Decode (Nvl(RHS.QTY_MATCH_TOTAL,  0)  - LHS.QTY_MATCH_TOTAL,  0, YN_YES, YN_NO) AS IS_QTY_VWT,
                NULL                    AS SUCCESS_MATCH_ID,
                NULL                    AS RESULT_TXT
        FROM
        (
                -- Subquery 1.1:  Select all credit note ITEMS for the process / supplier by pool_key
                -- and call it the left hand side of the match
                SELECT  IMD.POOL_KEY,
                        l_MATCH_TYPE            AS MATCH_TYPE,
                        IDH.DOC_ID              AS DOC_ID,
                        IDDRC.ITEM              AS ITEM,
                        IDDRC.orig_UNIT_COST AS COST_MATCH_TOTAL,
                        Sum(IDDRC.orig_QTY)   AS QTY_MATCH_TOTAL
                FROM    IM_MATCH_DOC IMD,
                        IM_DOC_HEAD IDH,
                        IM_DOC_DETAIL_REASON_CODES  IDDRC
                WHERE   IMD.PROCESS_ID    = I_PROCESS_ID
                AND     IMD.SUPPLIER      = I_SUPPLIER_ID
                AND     IMD.CONFIG_ID     = I_CONFIG_ID
                AND     IDH.DOC_ID        = IMD.DOC_REF
                AND     IDH.TYPE          = IMD.DOC_TYPE
                AND     GET_MATCH_SIDE(IDH.TYPE) = MATCH_SIDE_LEFT
                AND     IDDRC.DOC_ID      = IDH.DOC_ID
                AND     L_MATCH_TYPE      = MATCH_TYPE_CRDNT_CRDNRQ
                AND     IMD.MATCH_LEVEL IS NULL
                AND     ((IDH.STATUS = 'POSTED' AND IDH.HOLD_STATUS = 'N') OR (IDH.STATUS = 'APPRVE' AND IDH.HOLD_STATUS IN ('H','R')))

                GROUP BY IMD.POOL_KEY, L_MATCH_TYPE, IDH.DOC_ID, IDDRC.ITEM, IDDRC.orig_UNIT_COST
                --
                -- TODO:  invoice.match.support:  "union" similar subquery to selct invoices if P_MATCH_TYPE is MATCH_TYPE_MRCHI_RCPT
                --
        ) LHS,  -- as in "left-hand-side" of the match
        (
                -- Subquery 1.2:  Select all credit note request ITEMS  for the process / supplier by pool_key
                -- and call it the right hand side of the match
                SELECT  IMD.POOL_KEY,
                        l_MATCH_TYPE             AS MATCH_TYPE,
                        NULL                     AS DOC_ID,
                        IDDRC.ITEM               AS ITEM,
                        IDDRC.orig_UNIT_COST AS COST_MATCH_TOTAL,
                        Sum(IDDRC.orig_QTY)  AS QTY_MATCH_TOTAL
                FROM    IM_MATCH_DOC IMD,
                        IM_DOC_HEAD IDH,
                        IM_DOC_DETAIL_REASON_CODES  IDDRC
                WHERE   IMD.PROCESS_ID    = I_PROCESS_ID
                AND     IMD.SUPPLIER      = I_SUPPLIER_ID
                AND     IMD.CONFIG_ID     = I_CONFIG_ID
                AND     IDH.DOC_ID        = IMD.DOC_REF
                AND     IDH.TYPE          = IMD.DOC_TYPE
                AND     IDDRC.DOC_ID      = IDH.DOC_ID
                AND     GET_MATCH_SIDE(IDH.TYPE) = MATCH_SIDE_RIGHT
                AND     L_MATCH_TYPE      = MATCH_TYPE_CRDNT_CRDNRQ
                AND     IMD.MATCH_LEVEL IS NULL
                AND     IDH.STATUS NOT IN ('MTCH','POSTED')

                GROUP BY IMD.POOL_KEY, L_MATCH_TYPE, IDDRC.ITEM, IDDRC.orig_UNIT_COST
                --
                -- TODO:  invoice.match.support:  "union" similar subquery to selct receipts if P_MATCH_TYPE is MATCH_TYPE_MRCHI_RCPT
                --
        ) RHS  -- as in "right-hand-side" of the match
        WHERE LHS.POOL_KEY = RHS.POOL_KEY(+)
        AND   LHS.ITEM     = RHS.ITEM(+)
        AND   LHS.POOL_KEY IN (

                -- Detail match only pools where there's only 1
                -- document remaining on the left-hand-side of the
                -- the match.

                SELECT POOL_KEY
                FROM(
                SELECT imd2.pool_key, Count(*) CNT
                FROM   im_match_doc imd2
                WHERE  IMD2.PROCESS_ID = I_PROCESS_ID
                AND    IMD2.SUPPLIER = I_SUPPLIER_ID
                AND    IMD2.CONFIG_ID = I_CONFIG_ID
                AND    IMD2.MATCH_LEVEL IS NULL
                AND    GET_MATCH_SIDE(IMD2.DOC_TYPE) = MATCH_SIDE_LEFT
                GROUP BY IMD2.POOL_KEY
                ) WHERE CNT = 1

        )
        ) A WHERE A.POOL_KEY IS NOT NULL;

        -- NOTE WHERE CLAUSE above:
        -- We're using Left outer join scheme t o ensure that if an item on the left hand side (e.g. credit note)
        -- cannot be matched to an item on the right hand side (e.g. credit note request),  then the cost
        -- and quantity on the left hand side item will become entirely discrepant.


        -- On the flip-side:   The left outer join can pick up a left hand side document (credit note)
        -- that does not match to any right hand side document (credit note requests).    It will
        -- force the creation of orphan detail lines of left hand document on the match pool results.

        -- Example:  A credit note with no credit note requests will be picked up and matched with all
        -- its items treated as orphan lines.

        -- We don't want this to happen.
        -- The 'delete' below ensures that for every process, supplier, config, pool, and document,  there
        -- should be at least 1 item in that group that matched to a credit note request.

        DELETE FROM im_match_pool_results tgt
        WHERE (process_id, supplier, config_id, pool_key, lhs_doc_id) IN (
            SELECT process_id, supplier, config_id, pool_key, lhs_doc_id
            FROM (
                  SELECT process_id, supplier, config_id, pool_key, lhs_doc_id, sum(rhs_total_qty)
                  FROM   im_match_pool_results impr
                  WHERE  impr.process_id = I_PROCESS_ID
                  AND    IMPR.SUPPLIER = I_SUPPLIER_ID
                  AND    IMPR.CONFIG_ID = I_CONFIG_ID
                  GROUP BY process_id, supplier, config_id, pool_key, lhs_doc_id
                  HAVING Sum(rhs_total_qty) = 0
            )
        );



        -------------------------
        -- Expose the actual item detail unit cost and quantities
        -- to be used for matching.    Details may be from IM_DOC_DETAIL_REASON_CODES
        -- or IM_INVOICE_DETAILS
        -------------------------
        INSERT INTO IM_MATCH_POOL_ITEM
        (
            PROCESS_ID,
            MATCH_POOL_RESULT_ID,
            MATCH_SIDE,
            DOC_TYPE,
            DOC_ID,
            ITEM,
            UNIT_COST,
            QTY,
            REASON_CODE
        )
        SELECT  I_PROCESS_ID,
                IMPR.MATCH_POOL_RESULT_ID,
                MATCH_SIDE_LEFT,
                IDH.TYPE,
                IDDRC.DOC_ID,
                IDDRC.ITEM,
                iddrc.orig_unit_cost,
                iddrc.orig_qty,
                iddrc.reason_code_id
        FROM  IM_MATCH_POOL_RESULTS IMPR,
              IM_DOC_DETAIL_REASON_CODES IDDRC,
              IM_DOC_HEAD IDH
        WHERE IMPR.PROCESS_ID = I_PROCESS_ID
        AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
        AND   IMPR.CONFIG_ID  = I_CONFIG_ID
        AND   IMPR.MATCH_LEVEL= MATCH_LEVEL_DETAIL
        AND   IDDRC.DOC_ID    = IMPR.LHS_DOC_ID
        AND   IDDRC.ITEM      = IMPR.ITEM
        AND   IDH.DOC_ID      = IDDRC.DOC_ID
        AND   L_MATCH_TYPE    = MATCH_TYPE_CRDNT_CRDNRQ
        --
        -- TODO:  invoice.match.support:  "union" similar subquery to selct INVOICE LINES if P_MATCH_TYPE is MATCH_TYPE_MRCHI_RCPT
        --
        UNION ALL
        SELECT  I_PROCESS_ID,
                IMPR.MATCH_POOL_RESULT_ID,
                MATCH_SIDE_RIGHT,
                IDH.TYPE,
                IDDRC.DOC_ID,
                IDDRC.ITEM,
                iddrc.orig_unit_cost,
                iddrc.orig_qty,
                iddrc.reason_code_id
        FROM  IM_MATCH_POOL_RESULTS IMPR,
              IM_MATCH_DOC IMD,
              IM_DOC_DETAIL_REASON_CODES IDDRC,
              IM_DOC_HEAD IDH
        WHERE
        --- Find detail match results
              IMPR.PROCESS_ID = I_PROCESS_ID
        AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
        AND   IMPR.CONFIG_ID  = I_CONFIG_ID
        AND   IMPR.MATCH_LEVEL= MATCH_LEVEL_DETAIL
        --- Find the candidate documents on the right hand side of the match
        AND   IMD.PROCESS_ID  = IMPR.PROCESS_ID
        AND   IMD.SUPPLIER    = IMPR.SUPPLIER
        AND   IMD.CONFIG_ID   = IMPR.CONFIG_ID
        AND   IMD.POOL_KEY    = IMPR.POOL_KEY
        AND   IS_CREDIT_NOTE_REQUEST(IMD.DOC_TYPE) = YN_YES
        AND   IMD.MATCH_LEVEL IS NULL
        --- Extract the details
        AND   IDDRC.DOC_ID    = IMD.DOC_REF
        AND   IDDRC.ITEM      = IMPR.ITEM
        AND   IDH.DOC_ID      = IDDRC.DOC_ID
        --- Only valid for credit note matching...
        AND   L_MATCH_TYPE    = MATCH_TYPE_CRDNT_CRDNRQ;
        --
        -- TODO:  invoice.match.support:  "union" similar subquery to selct RECEIPTLINES if P_MATCH_TYPE is MATCH_TYPE_MRCHI_RCPT

        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        O_STATUS := SUCCESS;

        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;
    END INIT_DETAIL_MATCH;
----------------------------------------------------------------------------------

    /******************************

    Validates the summary match results.

    This function will set IM_MATCH_POOL_RESULTS.RESULT_TXT
    if a summary match cannot be accomplished due to:

    -  Cost and Quantity Discrepancies are out of tolerance.

    *******************************/
PROCEDURE CREATE_DETAIL_DISCREPANCIES (O_status           OUT NUMBER,
                                       O_error_message    OUT VARCHAR2,
                                       I_process_id    IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                       I_config_id     IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                       I_supplier_id   IN     SUPS.SUPPLIER%TYPE)
IS

   L_program VARCHAR2(45) := 'REIM_MATCH_SQL.CREATE_DETAIL_DISCREPANCIES';
   L_start_time    TIMESTAMP := SYSTIMESTAMP;

BEGIN

   -- Calculate quantity discrepancies for each item

   insert into im_match_qty_var (process_id,
                                 match_pool_result_id,
                                 qty,
                                 unit_cost,
                                 unit_cost_src_type,
                                 within_tolerance)
      select process_id,
             match_pool_result_id,
             qty_diff,
             unit_cost,
             match_side_right,
             is_qty_vwt
        from (select I_process_id process_id,
                     lhs.match_pool_result_id,
                     lhs.is_qty_vwt,
                     NVL(rhs.unit_cost, 0) unit_cost,
                     (NVL(rhs.qty, 0) - lhs.qty) qty_diff
                from (select impi.match_pool_result_id,
                             impi.item,
                             impi.unit_cost,
                             SUM(impi.qty) qty,
                             impr.is_qty_vwt
                        from im_match_pool_item impi,
                             im_match_pool_results impr
                       where impr.process_id           = I_process_id
                         and impr.supplier             = I_supplier_id
                         and impr.config_id            = I_config_id
                         and impr.match_level          = MATCH_LEVEL_DETAIL
                         and impi.match_pool_result_id = impr.match_pool_result_id
                         and impi.match_side           = MATCH_SIDE_LEFT
                         and (   impr.result_txt is NULL
                              or IS_ERROR(impr.result_txt) = YN_NO)
                       group by impi.match_pool_result_id,
                                impi.item,
                                impi.unit_cost,
                                impr.is_qty_vwt) LHS,
                     (select impi.match_pool_result_id,
                             impi.item,
                             impi.unit_cost,
                             SUM(impi.qty) qty
                        from im_match_pool_item impi,
                             im_match_pool_results impr
                       where impr.process_id           = I_process_id
                         and impr.supplier             = I_supplier_id
                         and impr.config_id            = I_config_id
                         and impr.match_level          = MATCH_LEVEL_DETAIL
                         and impi.match_pool_result_id = impr.match_pool_result_id
                         and impi.match_side           = MATCH_SIDE_RIGHT
                         and (   impr.result_txt is NULL
                              or IS_ERROR(impr.result_txt) = YN_NO)
                       group by impi.match_pool_result_id,
                                impi.item,
                                impi.unit_cost) rhs
               where lhs.match_pool_result_id = rhs.match_pool_result_id (+)
                 and lhs.item  = rhs.item (+))
       where qty_diff <> 0
         and qty_diff * unit_cost <> 0;


        -----------------------
        -- Calculate cost discrepancies for each item
        -----------------------
        INSERT INTO IM_MATCH_COST_VAR
        (
            process_id,
            match_pool_result_id,
            quantity,
            amount,
            qty_src_type,
            within_tolerance
        )
        SELECT PROCESS_ID,
                MATCH_POOL_RESULT_ID,
                QTY,
                COST_DIFF,
                MATCH_SIDE_LEFT,
                is_cost_vwt
        FROM (
              SELECT  I_PROCESS_ID AS PROCESS_ID,
                      LHS.MATCH_POOL_RESULT_ID AS MATCH_POOL_RESULT_ID,
                      LHS.QTY AS QTY,
                      Nvl(RHS.UNIT_COST,0) - LHS.UNIT_COST AS COST_DIFF,
                      LHS.IS_COST_VWT
              FROM
              (
                  SELECT IMPI.MATCH_POOL_RESULT_ID,
                        IMPI.ITEM,
                        IMPI.UNIT_COST,
                        Sum(IMPI.QTY) AS QTY,
                        impr.is_cost_vwt
                  FROM   IM_MATCH_POOL_ITEM IMPI,
                        IM_MATCH_POOL_RESULTS IMPR
                  WHERE  IMPR.PROCESS_ID = I_PROCESS_ID
                  AND    IMPR.SUPPLIER   = I_SUPPLIER_ID
                  AND    IMPR.CONFIG_ID  = I_CONFIG_ID
                  AND    IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                  AND    IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
                  AND    IMPI.MATCH_SIDE = MATCH_SIDE_LEFT
                  AND    (IMPR.RESULT_TXT IS NULL
                          OR
                          IS_ERROR(IMPR.RESULT_TXT) = YN_NO )
                  GROUP BY IMPI.MATCH_POOL_RESULT_ID, IMPI.ITEM, IMPI.UNIT_COST, impr.is_cost_vwt
              )LHS,
              (
                  SELECT IMPI.MATCH_POOL_RESULT_ID,
                        IMPI.ITEM,
                        IMPI.UNIT_COST,
                        Sum(IMPI.QTY) AS QTY
                  FROM   IM_MATCH_POOL_ITEM IMPI,
                        IM_MATCH_POOL_RESULTS IMPR
                  WHERE  IMPR.PROCESS_ID = I_PROCESS_ID
                  AND    IMPR.SUPPLIER   = I_SUPPLIER_ID
                  AND    IMPR.CONFIG_ID  = I_CONFIG_ID
                  AND    IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                  AND    IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
                  AND    IMPI.MATCH_SIDE = MATCH_SIDE_RIGHT
                  AND    (IMPR.RESULT_TXT IS NULL
                          OR
                          IS_ERROR(IMPR.RESULT_TXT) = YN_NO )
                  GROUP BY IMPI.MATCH_POOL_RESULT_ID, IMPI.ITEM, IMPI.UNIT_COST
              )RHS
              WHERE LHS.MATCH_POOL_RESULT_ID = RHS.MATCH_POOL_RESULT_ID(+)
              AND   LHS.ITEM  = RHS.ITEM(+)
        ) WHERE COST_DIFF <> 0
        AND  COST_DIFF * QTY <> 0;

        VALIDATE_DISCREPANCIES(O_STATUS,
                                O_ERROR_MESSAGE,
                                I_PROCESS_ID,
                                I_CONFIG_ID,
                                I_SUPPLIER_ID,
                                MATCH_LEVEL_DETAIL);
        
        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        RETURN;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;

          RETURN;
    END CREATE_DETAIL_DISCREPANCIES;
----------------------------------------------------------------------------------




    /**************************

      Eliminates detail matches which has the following characteristics:

      - Detail match that potentially yields a resolution action without
        the corresponding default reason code setup in im_system options , OR
        user has no access to the default reason code.


    **************************/
    PROCEDURE VALIDATE_DETAIL_MATCH (O_STATUS    OUT NUMBER,
                                O_ERROR_MESSAGE  OUT VARCHAR2,
                                I_PROCESS_ID  IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID   IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID IN SUPS.SUPPLIER%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.VALIDATE_DETAIL_MATCH';
        L_CBC_REASON    IM_SYSTEM_OPTIONS.DFLT_COST_OVERBILL_RC%TYPE := NULL;
        L_CBQ_REASON    IM_SYSTEM_OPTIONS.DFLT_QTY_OVERBILL_RC%TYPE := NULL;
        L_CMC_REASON    IM_SYSTEM_OPTIONS.DFLT_COST_UNDERBILL_RC%TYPE := NULL;
        L_CMQ_REASON    IM_SYSTEM_OPTIONS.DFLT_QTY_UNDERBILL_RC%TYPE := NULL;
        L_DFLT_REASON_ERRORS VARCHAR2(500);
        L_MATCH_TYPE    IM_MATCH_POOL_CONFIG.MATCH_TYPE%TYPE;
	L_USER_ID  IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
        L_start_time    TIMESTAMP := SYSTIMESTAMP;
		
    BEGIN
        O_STATUS := SUCCESS;
		L_USER_ID := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
		if L_USER_ID is null then
			L_USER_ID := get_user;
		end if;
        ---------------------------------
        -- Do general validation of match pool results
        ---------------------------------
        VALIDATE_MATCH_RESULTS( O_STATUS,
                                O_ERROR_MESSAGE,
                                I_PROCESS_ID,
                                I_CONFIG_ID,
                                I_SUPPLIER_ID);

        IF O_STATUS <> SUCCESS THEN
          RETURN;
        END IF;



        L_MATCH_TYPE := GET_MATCH_TYPE(I_CONFIG_ID);
        --------------------------
        -- Validate TAX codes on details.
        --------------------------

        UPDATE IM_MATCH_POOL_RESULTS TGT
        SET TGT.RESULT_TXT = AS_ERROR('TAX code or rate differences found')
        WHERE (TGT.PROCESS_ID, TGT.SUPPLIER,TGT.CONFIG_ID,TGT.POOL_KEY,TGT.MATCH_LEVEL)
        IN (
            SELECT UNIQUE PROCESS_ID, SUPPLIER, CONFIG_ID, POOL_KEY, MATCH_LEVEL
            	FROM
            		(
            		---------------------------------
        			-- Get all the unique tax codes from both the LHS and RHS sides
        			---------------------------------
            		SELECT UNIQUE LHS.PROCESS_ID, LHS.SUPPLIER, LHS.CONFIG_ID, LHS.POOL_KEY, LHS.MATCH_LEVEL,LHS.TAX_CODE,
                        LHS.TAX_RATE,
                        LHS.TAX_FORMULA FROM (
                		SELECT UNIQUE IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.MATCH_LEVEL,
                        		IDDRT.TAX_CODE  AS TAX_CODE,
                        		IDDRT.TAX_RATE AS TAX_RATE,
                        		IDDRT.TAX_FORMULA AS TAX_FORMULA
                		FROM    IM_MATCH_POOL_RESULTS IMPR,
                        		IM_MATCH_POOL_ITEM    IMPI,
                        		IM_DOC_DETAIL_REASON_CODES  IDDRC,
                        		IM_DOC_DETAIL_RC_TAX  IDDRT
                		WHERE   IMPR.PROCESS_ID = I_PROCESS_ID
                		AND     IMPR.SUPPLIER = I_SUPPLIER_ID
                		AND     IMPR.CONFIG_ID = I_CONFIG_ID
                		AND     IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                		--
                		AND     IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
                		AND     IMPI.MATCH_SIDE = MATCH_SIDE_LEFT
                		--
                		AND     IDDRC.IM_DOC_DETAIL_REASON_CODES_ID = IDDRT.IM_DOC_DETAIL_REASON_CODES_ID
                		AND     IDDRC.DOC_ID = IMPI.DOC_ID
                		AND     IDDRC.REASON_CODE_ID = IMPI.REASON_CODE
                		AND     IDDRC.ITEM = IMPI.ITEM
                		AND     L_MATCH_TYPE = MATCH_TYPE_CRDNT_CRDNRQ
            			) LHS,
            			(
                		SELECT UNIQUE IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.MATCH_LEVEL,
                        		IDDRT.TAX_CODE  AS TAX_CODE,
                        		IDDRT.TAX_RATE AS TAX_RATE,
                        		IDDRT.TAX_FORMULA AS TAX_FORMULA
                		FROM    IM_MATCH_POOL_RESULTS IMPR,
                        		IM_MATCH_POOL_ITEM    IMPI,
                        		IM_DOC_DETAIL_REASON_CODES  IDDRC,
                        		IM_DOC_DETAIL_RC_TAX  IDDRT
                		WHERE   IMPR.PROCESS_ID = I_PROCESS_ID
                		AND     IMPR.SUPPLIER = I_SUPPLIER_ID
                		AND     IMPR.CONFIG_ID = I_CONFIG_ID
                		AND     IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                		--
                		AND     IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
                		AND     IMPI.MATCH_SIDE = MATCH_SIDE_RIGHT
                		--
                		AND     IDDRC.IM_DOC_DETAIL_REASON_CODES_ID = IDDRT.IM_DOC_DETAIL_REASON_CODES_ID
                		AND     IDDRC.DOC_ID = IMPI.DOC_ID
                		AND     IDDRC.REASON_CODE_ID = IMPI.REASON_CODE
                		AND     IDDRC.ITEM = IMPI.ITEM
                		) RHS
                		WHERE LHS.PROCESS_ID = RHS.PROCESS_ID
                		AND   LHS.SUPPLIER = RHS.SUPPLIER
                		AND   LHS.POOL_KEY = RHS.POOL_KEY
                		AND   LHS.MATCH_LEVEL = RHS.MATCH_LEVEL
                		AND   L_MATCH_TYPE = MATCH_TYPE_CRDNT_CRDNRQ


                	MINUS -- (All tax records - All matching tax records) = All unmatched tax records

                	---------------------------------
        			-- Get all the matching unique tax records from both the LHS and RHS sides
        			---------------------------------
                	SELECT UNIQUE LHS.PROCESS_ID, LHS.SUPPLIER, LHS.CONFIG_ID, LHS.POOL_KEY, LHS.MATCH_LEVEL,LHS.TAX_CODE,
                        LHS.TAX_RATE,
                        LHS.TAX_FORMULA
            			FROM (
                		SELECT UNIQUE IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.MATCH_LEVEL,
                        		IDDRT.TAX_CODE  AS TAX_CODE,
                        		IDDRT.TAX_RATE AS TAX_RATE,
                        		IDDRT.TAX_FORMULA AS TAX_FORMULA
                		FROM    IM_MATCH_POOL_RESULTS IMPR,
                        		IM_MATCH_POOL_ITEM    IMPI,
                        		IM_DOC_DETAIL_REASON_CODES  IDDRC,
                        		IM_DOC_DETAIL_RC_TAX  IDDRT
                		WHERE   IMPR.PROCESS_ID = I_PROCESS_ID
                		AND     IMPR.SUPPLIER = I_SUPPLIER_ID
                		AND     IMPR.CONFIG_ID = I_CONFIG_ID
                		AND     IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                		--
                		AND     IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
                		AND     IMPI.MATCH_SIDE = MATCH_SIDE_LEFT
                		--
                		AND     IDDRC.IM_DOC_DETAIL_REASON_CODES_ID = IDDRT.IM_DOC_DETAIL_REASON_CODES_ID
                		AND     IDDRC.DOC_ID = IMPI.DOC_ID
                		AND     IDDRC.REASON_CODE_ID = IMPI.REASON_CODE
                		AND     IDDRC.ITEM = IMPI.ITEM
                		AND     L_MATCH_TYPE = MATCH_TYPE_CRDNT_CRDNRQ
            			) LHS,
            			(
                		SELECT UNIQUE IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.MATCH_LEVEL,
                        		IDDRT.TAX_CODE  AS TAX_CODE,
                        		IDDRT.TAX_RATE AS TAX_RATE,
                        		IDDRT.TAX_FORMULA AS TAX_FORMULA
                		FROM    IM_MATCH_POOL_RESULTS IMPR,
                        		IM_MATCH_POOL_ITEM    IMPI,
                        		IM_DOC_DETAIL_REASON_CODES  IDDRC,
                        		IM_DOC_DETAIL_RC_TAX  IDDRT
                		WHERE   IMPR.PROCESS_ID = I_PROCESS_ID
                		AND     IMPR.SUPPLIER = I_SUPPLIER_ID
                		AND     IMPR.CONFIG_ID = I_CONFIG_ID
                		AND     IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                		--
                		AND     IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
                		AND     IMPI.MATCH_SIDE = MATCH_SIDE_RIGHT
                		--
                		AND     IDDRC.IM_DOC_DETAIL_REASON_CODES_ID = IDDRT.IM_DOC_DETAIL_REASON_CODES_ID
                		AND     IDDRC.DOC_ID = IMPI.DOC_ID
                		AND     IDDRC.REASON_CODE_ID = IMPI.REASON_CODE
                		AND     IDDRC.ITEM = IMPI.ITEM
                		) RHS
                	WHERE LHS.PROCESS_ID = RHS.PROCESS_ID
                	AND   LHS.SUPPLIER = RHS.SUPPLIER
                	AND   LHS.POOL_KEY = RHS.POOL_KEY
                	AND   LHS.MATCH_LEVEL = RHS.MATCH_LEVEL
                	AND   L_MATCH_TYPE = MATCH_TYPE_CRDNT_CRDNRQ
                	AND   LHS.TAX_CODE = RHS.TAX_CODE
                	AND   LHS.TAX_RATE = RHS.TAX_RATE
                	AND   LHS.TAX_FORMULA = RHS.TAX_FORMULA
					)
         );


        --------------------------
        -- Cost discrepancy items' dept/class roles are defined.
        --------------------------
        UPDATE IM_MATCH_POOL_RESULTS TGT
        SET TGT.RESULT_TXT = AS_ERROR('Permission not defined for one or more cost discrepant items (FILTER_GROUP_MERCH).')
        WHERE (TGT.PROCESS_ID, TGT.SUPPLIER,TGT.CONFIG_ID,TGT.POOL_KEY,TGT.MATCH_LEVEL, TGT.ITEM)
        IN (
              SELECT impr.process_id, impr.supplier, impr.config_id, impr.pool_key, impr.match_level, impr.item
              FROM  IM_MATCH_POOL_RESULTS IMPR,
                    ITEM_MASTER IM
              WHERE IMPR.PROCESS_ID = I_PROCESS_ID
              AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
              AND   IMPR.CONFIG_ID  = I_CONFIG_ID
              AND   IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
              AND   IMPR.IS_COST_VWT = YN_NO
              --
              AND   IM.ITEM = IMPR.ITEM
              --
              AND   NOT EXISTS (SELECT 1 FROM FILTER_GROUP_MERCH FGM,SEC_USER SU,SEC_USER_GROUP SUG
					WHERE 
					SU.USER_SEQ = SUG.USER_SEQ 
					AND FGM.SEC_GROUP_ID = SUG.GROUP_ID
					AND SU.APPLICATION_USER_ID = L_USER_ID AND FGM.FILTER_MERCH_LEVEL ='D' AND FGM.FILTER_MERCH_ID=IM.DEPT
					UNION ALL
					SELECT 1  FROM FILTER_GROUP_MERCH FGM,SEC_USER SU,SEC_USER_GROUP SUG
					WHERE 
					SU.USER_SEQ = SUG.USER_SEQ
					AND FGM.SEC_GROUP_ID = SUG.GROUP_ID
					AND SU.APPLICATION_USER_ID = L_USER_ID AND FGM.FILTER_MERCH_LEVEL ='C' AND FGM.FILTER_MERCH_ID_CLASS =IM.CLASS
				)
        );


        --------------------------
        -- Quantity discrepancy location roles are defined.
        --------------------------
        UPDATE IM_MATCH_POOL_RESULTS TGT
        SET TGT.RESULT_TXT = AS_ERROR('Permission not defined for one or more quanity discrepant document (SEC_GROUP_LOC_MATRIX).')
        WHERE (TGT.PROCESS_ID, TGT.SUPPLIER,TGT.CONFIG_ID,TGT.POOL_KEY,TGT.MATCH_LEVEL, TGT.ITEM)
        IN (
              SELECT impr.process_id, impr.supplier, impr.config_id, impr.pool_key, impr.match_level, impr.item
              FROM  IM_MATCH_POOL_RESULTS IMPR,
                    IM_MATCH_POOL_ITEM IMPI,
                    IM_DOC_HEAD IDH
              WHERE IMPR.PROCESS_ID = I_PROCESS_ID
              AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
              AND   IMPR.CONFIG_ID  = I_CONFIG_ID
              AND   IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
              AND   IMPR.IS_QTY_VWT = YN_NO
              --
              AND   IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
              AND   IMPI.ITEM = IMPR.ITEM
              AND   IMPI.MATCH_SIDE = MATCH_SIDE_LEFT
              --
              AND   IDH.DOC_ID = IMPI.DOC_ID
              AND   IDH.TYPE = IMPI.DOC_TYPE
              --
              AND   NOT EXISTS (SELECT 1 FROM 
											(
												SELECT LM.LOCATION LOCATION, SUG.USER_ID BUSINESS_USER FROM 
												(
													SELECT SGLM.GROUP_ID GROUP_ID,SGLM.STORE LOCATION,SGLM.SELECT_IND SELECT_IND 
													FROM SEC_GROUP SG,SEC_GROUP_LOC_MATRIX SGLM 
													WHERE 
													SGLM.GROUP_ID = SG.GROUP_ID
													UNION
													SELECT SGLM.GROUP_ID GROUP_ID,SGLM.WH LOCATION,SGLM.SELECT_IND SELECT_IND 
													FROM SEC_GROUP SG,SEC_GROUP_LOC_MATRIX SGLM 
													WHERE 
													SGLM.GROUP_ID = SG.GROUP_ID
												) LM ,
												(
													SELECT SEC_USER_GROUP.GROUP_ID GROUP_ID ,SEC_USER.APPLICATION_USER_ID USER_ID 
													FROM SEC_USER ,SEC_USER_GROUP
													WHERE SEC_USER.USER_SEQ = SEC_USER_GROUP.USER_SEQ 
												) SUG
													WHERE LM.GROUP_ID = SUG.GROUP_ID 
											) SEC_QUERY
											WHERE SEC_QUERY.LOCATION = IDH.LOCATION AND SEC_QUERY.BUSINESS_USER = L_USER_ID
								)
        );


        --------------------------
        -- Retrieve and validate the default reason codes setup in system options
        --------------------------
        GET_VALID_DEFAULT_REASON_CODES(O_STATUS,
                                       L_DFLT_REASON_ERRORS,
                                       L_CBC_REASON,
                                       L_CBQ_REASON,
                                       L_CMC_REASON,
                                       L_CMQ_REASON);
        IF O_STATUS = FAIL THEN
            RETURN;
        END IF;

        --------------------------
        -- No default chargeback cost reason defined.
        -- Invalidate match pools that have cost chargebacks
        --------------------------
        IF L_CBC_REASON IS NULL THEN
              UPDATE IM_MATCH_POOL_RESULTS TGT
              SET TGT.RESULT_TXT = AS_ERROR('Missing or no access to default chargeback COST reason code.')
              WHERE (TGT.PROCESS_ID, TGT.SUPPLIER,TGT.CONFIG_ID,TGT.POOL_KEY,TGT.MATCH_LEVEL)
              IN (
                      SELECT  PROCESS_ID, SUPPLIER, CONFIG_ID,POOL_KEY, MATCH_LEVEL
                      FROM (
                            SELECT  IMPR.PROCESS_ID,
                                    IMPR.SUPPLIER,
                                    IMPR.CONFIG_ID,
                                    IMPR.POOL_KEY,
                                    IMPR.MATCH_LEVEL,
                                    Sum(CASE
                                    WHEN  IMPR.IS_COST_VWT = YN_NO
                                          AND IMPR.TOTAL_COST_VAR <> 0
                                          AND IMPR.COST_VAR_FAVOR_OF = FAVOR_OF_SUPPLIER
                                    THEN 1
                                    ELSE 0
                                    END) AS CBC
                            FROM  IM_MATCH_POOL_RESULTS IMPR
                            WHERE IMPR.PROCESS_ID = I_PROCESS_ID
                            AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
                            AND   IMPR.CONFIG_ID  = I_CONFIG_ID
                            AND   IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                            GROUP BY IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.MATCH_LEVEL
                      ) WHERE CBC > 0
              );
        END IF;


        --------------------------
        -- No default chargeback qty reason defined.
        -- Invalidate match pools that have qty chargebacks
        --------------------------
        IF L_CBQ_REASON IS NULL THEN
              UPDATE IM_MATCH_POOL_RESULTS TGT
              SET TGT.RESULT_TXT = AS_ERROR('Missing or no access to default chargeback QUANTITY reason code.')
              WHERE (TGT.PROCESS_ID,TGT.SUPPLIER,TGT.CONFIG_ID,TGT.POOL_KEY,TGT.MATCH_LEVEL)
              IN (
                      SELECT  PROCESS_ID,SUPPLIER, CONFIG_ID, POOL_KEY, MATCH_LEVEL
                      FROM (
                            SELECT  IMPR.PROCESS_ID,
                                    IMPR.SUPPLIER,
                                    IMPR.CONFIG_ID,
                                    IMPR.POOL_KEY,
                                    IMPR.MATCH_LEVEL,
                                    Sum(CASE
                                    WHEN IMPR.IS_QTY_VWT = YN_NO
                                          AND IMPR.TOTAL_QTY_VAR <> 0
                                          AND IMPR.QTY_VAR_FAVOR_OF = FAVOR_OF_SUPPLIER
                                    THEN 1
                                    ELSE 0
                                    END) AS CBQ
                            FROM  IM_MATCH_POOL_RESULTS IMPR
                            WHERE IMPR.PROCESS_ID = I_PROCESS_ID
                            AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
                            AND   IMPR.CONFIG_ID  = I_CONFIG_ID
                            AND   IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                            GROUP BY IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.MATCH_LEVEL
                      ) WHERE CBQ > 0
              );
        END IF;

        --------------------------
        -- No default credit memo cost reason defined.
        -- Invalidate match pools that have credit memo cost.
        --------------------------
        IF L_CMC_REASON IS NULL THEN
              UPDATE IM_MATCH_POOL_RESULTS TGT
              SET TGT.RESULT_TXT = AS_ERROR('Missing or no access to default credit memo COST reason code.')
              WHERE (TGT.PROCESS_ID, TGT.SUPPLIER,TGT.CONFIG_ID,TGT.POOL_KEY,TGT.MATCH_LEVEL)
              IN (
                      SELECT  PROCESS_ID, SUPPLIER, CONFIG_ID,POOL_KEY, MATCH_LEVEL
                      FROM (
                            SELECT  IMPR.PROCESS_ID,
                                    IMPR.SUPPLIER,
                                    IMPR.CONFIG_ID,
                                    IMPR.POOL_KEY,
                                    IMPR.MATCH_LEVEL,
                                    Sum(CASE
                                    WHEN IMPR.IS_COST_VWT = YN_NO
                                          AND IMPR.TOTAL_COST_VAR <> 0
                                          AND IMPR.COST_VAR_FAVOR_OF = FAVOR_OF_RETAILER
                                    THEN 1
                                    ELSE 0
                                    END) AS CMC
                            FROM  IM_MATCH_POOL_RESULTS IMPR
                            WHERE IMPR.PROCESS_ID = I_PROCESS_ID
                            AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
                            AND   IMPR.CONFIG_ID  = I_CONFIG_ID
                            AND   IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                            GROUP BY IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.MATCH_LEVEL
                      ) WHERE CMC > 0
              );
        END IF;

        --------------------------
        -- No default credit memo qty reason defined.
        -- Invalidate match pools that have credit memo qty.
        --------------------------
        IF L_CMQ_REASON IS NULL THEN
              UPDATE IM_MATCH_POOL_RESULTS TGT
              SET TGT.RESULT_TXT = AS_ERROR('Missing or no access to default credit memo QUANTITY reason code.')
              WHERE (TGT.PROCESS_ID, TGT.SUPPLIER,TGT.CONFIG_ID,TGT.POOL_KEY,TGT.MATCH_LEVEL)
              IN (
                      SELECT  PROCESS_ID, SUPPLIER, CONFIG_ID,POOL_KEY, MATCH_LEVEL
                      FROM (
                            SELECT  IMPR.PROCESS_ID,
                                    IMPR.SUPPLIER,
                                    IMPR.CONFIG_ID,
                                    IMPR.POOL_KEY,
                                    IMPR.MATCH_LEVEL,
                                    Sum(CASE
                                    WHEN IMPR.IS_QTY_VWT = YN_NO
                                          AND IMPR.TOTAL_QTY_VAR <> 0
                                          AND IMPR.QTY_VAR_FAVOR_OF = FAVOR_OF_RETAILER
                                    THEN 1
                                    ELSE 0
                                    END) AS CMQ
                            FROM  IM_MATCH_POOL_RESULTS IMPR
                            WHERE IMPR.PROCESS_ID = I_PROCESS_ID
                            AND   IMPR.SUPPLIER   = I_SUPPLIER_ID
                            AND   IMPR.CONFIG_ID  = I_CONFIG_ID
                            AND   IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
                            GROUP BY IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.MATCH_LEVEL
                      ) WHERE CMQ > 0
              );
        END IF;
        --

        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;

          RETURN;
    END VALIDATE_DETAIL_MATCH ;
----------------------------------------------------------------------------------


    /**************************

      RETRIEVES the default reason codes from IM_SYSTEM_OPTIONS
      for cost / quantity discrepancies in chargeback and credit
      memo scenarios.

      These reason codes will be validated against the given
      business role name to ensure that the role the user assumes
      has access to the reason codes.

    **************************/
    PROCEDURE GET_VALID_DEFAULT_REASON_CODES(O_STATUS     OUT NUMBER,
                                            O_ERROR_MESSAGE   OUT VARCHAR2,
                                            O_CBC_REASON    OUT IM_SYSTEM_OPTIONS.DFLT_COST_OVERBILL_RC%TYPE,
                                            O_CBQ_REASON    OUT IM_SYSTEM_OPTIONS.DFLT_QTY_OVERBILL_RC%TYPE,
                                            O_CMC_REASON    OUT IM_SYSTEM_OPTIONS.DFLT_COST_UNDERBILL_RC%TYPE,
                                            O_CMQ_REASON    OUT IM_SYSTEM_OPTIONS.DFLT_QTY_UNDERBILL_RC%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.GET_VALID_DEFAULT_REASON_CODES';
        L_SUMMARY_OR_LINE  VARCHAR2(1);
        L_IS_CBQ_ACCESSIBLE  VARCHAR2(1):=YN_YES;
        L_IS_CBC_ACCESSIBLE  VARCHAR2(1):=YN_YES;
        L_IS_CMQ_ACCESSIBLE  VARCHAR2(1):=YN_YES;
        L_IS_CMC_ACCESSIBLE  VARCHAR2(1):=YN_YES;
	L_USER_ID  IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
        L_start_time    TIMESTAMP := SYSTIMESTAMP;
		
    BEGIN
        O_STATUS := SUCCESS;
		L_USER_ID := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
		if L_USER_ID is null then
			L_USER_ID := get_user;
		end if;
		
        SELECT  ISO.DFLT_QTY_OVERBILL_RC,
                NVL(
                    (SELECT YN_YES FROM (SELECT RC.REASON_CODE_ID  REASON_CODE_ID FROM SEC_USER U , SEC_USER_GROUP UG, IM_SEC_GRP_REASON_CODE RC
					 WHERE U.APPLICATION_USER_ID = L_USER_ID
					 AND U.USER_SEQ = UG.USER_SEQ
					 AND UG.GROUP_ID = RC.GROUP_ID)BUSINESS_ROLE
                    WHERE BUSINESS_ROLE.REASON_CODE_ID = ISO.DFLT_QTY_OVERBILL_RC),
                    YN_NO),
                ----------
                ISO.DFLT_COST_OVERBILL_RC,
                NVL(
                    (SELECT YN_YES FROM (SELECT RC.REASON_CODE_ID  REASON_CODE_ID FROM SEC_USER U , SEC_USER_GROUP UG, IM_SEC_GRP_REASON_CODE RC
					 WHERE U.APPLICATION_USER_ID = L_USER_ID
					 AND U.USER_SEQ = UG.USER_SEQ
					 AND UG.GROUP_ID = RC.GROUP_ID)BUSINESS_ROLE
                    WHERE BUSINESS_ROLE.REASON_CODE_ID = ISO.DFLT_COST_OVERBILL_RC),
                    YN_NO),
                ----------
                ISO.DFLT_QTY_UNDERBILL_RC,
                NVL(
                    (SELECT YN_YES FROM (SELECT RC.REASON_CODE_ID  REASON_CODE_ID FROM SEC_USER U , SEC_USER_GROUP UG, IM_SEC_GRP_REASON_CODE RC
					 WHERE U.APPLICATION_USER_ID = L_USER_ID
					 AND U.USER_SEQ = UG.USER_SEQ
					 AND UG.GROUP_ID = RC.GROUP_ID)BUSINESS_ROLE
                    WHERE BUSINESS_ROLE.REASON_CODE_ID = ISO.DFLT_QTY_UNDERBILL_RC),
                    YN_NO),
                ----------
                ISO.DFLT_COST_UNDERBILL_RC,
                NVL(
                    (SELECT YN_YES FROM (SELECT RC.REASON_CODE_ID  REASON_CODE_ID FROM SEC_USER U , SEC_USER_GROUP UG, IM_SEC_GRP_REASON_CODE RC
					 WHERE U.APPLICATION_USER_ID = L_USER_ID
					 AND U.USER_SEQ = UG.USER_SEQ
					 AND UG.GROUP_ID = RC.GROUP_ID)BUSINESS_ROLE
                    WHERE BUSINESS_ROLE.REASON_CODE_ID =  ISO.DFLT_COST_UNDERBILL_RC),
                    YN_NO)
        INTO    O_CBQ_REASON, L_IS_CBQ_ACCESSIBLE,
                O_CBC_REASON, L_IS_CBC_ACCESSIBLE,
                O_CMQ_REASON, L_IS_CMQ_ACCESSIBLE,
                O_CMC_REASON, L_IS_CMC_ACCESSIBLE
        FROM    IM_SYSTEM_OPTIONS ISO;

        --------------

        IF O_CBQ_REASON IS NULL THEN
            O_ERROR_MESSAGE := O_ERROR_MESSAGE||' Default chargeback reason code for quantity discrepancies is not defined in application system options.';
        END IF;

        IF  O_CBQ_REASON IS NOT NULL AND L_IS_CBQ_ACCESSIBLE = YN_NO THEN
            O_CBQ_REASON := NULL;
            O_ERROR_MESSAGE := O_ERROR_MESSAGE||' Business role '||L_USER_ID||' does not have permission to use default chargeback reason code '||O_CBQ_REASON||'.';
        END IF;

        ---------------
        IF O_CBC_REASON IS NULL THEN
            O_ERROR_MESSAGE := O_ERROR_MESSAGE||' Default chargeback reason code for cost discrepancies is not defined in application system options.';
        END IF;

        IF  O_CBC_REASON IS NOT NULL AND L_IS_CBC_ACCESSIBLE = YN_NO THEN
            O_CBC_REASON := NULL;
            O_ERROR_MESSAGE := O_ERROR_MESSAGE||' Business role '||L_USER_ID||' does not have permission to use default chargeback reason code '||O_CBC_REASON||'.';
        END IF;

        --------------

        IF O_CMQ_REASON IS NULL THEN
            O_ERROR_MESSAGE := O_ERROR_MESSAGE||' Default credit memo reason code for quantity discrepancies is not defined in application system options.';
        END IF;

        IF  O_CMQ_REASON IS NOT NULL AND L_IS_CMQ_ACCESSIBLE = YN_NO THEN
            O_CMQ_REASON := NULL;
            O_ERROR_MESSAGE := O_ERROR_MESSAGE||' Business role '||L_USER_ID||' does not have permission to use default credit memo reason code '||O_CMQ_REASON||'.';
        END IF;


        ---------------
        IF O_CMC_REASON IS NULL THEN
            O_ERROR_MESSAGE := O_ERROR_MESSAGE||' Default credit memo reason code for cost discrepancies is not defined in application system options.';
        END IF;

        IF  O_CMC_REASON IS NOT NULL AND L_IS_CMC_ACCESSIBLE = YN_NO THEN
            O_CMC_REASON := NULL;
            O_ERROR_MESSAGE := O_ERROR_MESSAGE||' Business role '||L_USER_ID||' does not have permission to use default credit memo reason code '||O_CMC_REASON||'.';
        END IF;

        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;
          RETURN;
    END GET_VALID_DEFAULT_REASON_CODES;
----------------------------------------------------------------------------------
PROCEDURE UPDATE_ALL_RESULT_TXTS(I_ERROR_MESSAGE   IN VARCHAR2,
                                 I_PROCESS_ID      IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                 I_CONFIG_ID       IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                 I_SUPPLIER_ID     IN SUPS.SUPPLIER%TYPE,
                                 I_MATCH_LEVEL     IN IM_MATCH_POOL_RESULTS.MATCH_LEVEL%TYPE)
IS
BEGIN

   update im_match_pool_results impr
      set result_txt = as_error(i_error_message)
    where impr.process_id = I_process_id
      and impr.config_id  = I_config_id
      and impr.supplier   = I_supplier_id
      and impr.match_level= I_match_level;

END;
----------------------------------------------------------------------------------
/**************************
DETERMINE if cost and quantity variances are WITHIN tolerance.

Select the tolerance range applicable
for the cost/qty variance.     If no range selected, then
cost/qty variance is NOT within any tolerance.
If a tolerance range applies,  then cost/qty variance is
within tolerance only if the applicable tolerance value
is greater than or equal to the cost/qty variance.
**************************/

PROCEDURE APPLY_MATCH_TOLERANCES(O_STATUS           OUT NUMBER,
                                 O_ERROR_MESSAGE    OUT VARCHAR2,
                                 I_PROCESS_ID    IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                 I_CONFIG_ID     IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                 I_SUPPLIER_ID   IN     SUPS.SUPPLIER%TYPE,
                                 I_MATCH_LEVEL   IN     IM_MATCH_POOL_RESULTS.MATCH_LEVEL%TYPE)
IS
   L_PROGRAM             VARCHAR2(50) := 'REIM_MATCH_SQL.APPLY_MATCH_TOLERANCES';

   L_SUMMARY_OR_LINE     VARCHAR2(1);
   L_VDATE               DATE := GET_VDATE;
   L_MATCH_TOTAL_QTY_IND VARCHAR2(3);
   L_start_time          TIMESTAMP := SYSTIMESTAMP;

BEGIN

   select min(v.match_total_qty_ind)
     into L_MATCH_TOTAL_QTY_IND
     from v_im_supp_site_attrib_expl v,
          sups s
    where v.supplier        = s.supplier
      and s.supplier_parent = I_SUPPLIER_ID;

   IF I_MATCH_LEVEL IN (MATCH_LEVEL_SUMMARY, MATCH_LEVEL_ONE_TO_ONE) THEN
      L_SUMMARY_OR_LINE := 'S';
   ELSE
      L_SUMMARY_OR_LINE := 'L';
   END IF;

   --All results with zero variances are flagged as within tolerance...
   UPDATE IM_MATCH_POOL_RESULTS IMPR
      SET IS_COST_VWT = (CASE WHEN TOTAL_COST_VAR <> 0 THEN 
                                 YN_NO
                              ELSE 
                                 YN_YES
                         END),
          IS_QTY_VWT  = (CASE WHEN TOTAL_QTY_VAR <> 0 THEN
                                 -- There's a quantity variance.  Check if it matters based on supplier option..
                                 (CASE WHEN I_MATCH_LEVEL = MATCH_LEVEL_DETAIL THEN
                                          YN_NO
                                        WHEN YN_YES = L_MATCH_TOTAL_QTY_IND THEN
                                          YN_NO  -- Supplier options says we should check qty variances, so treat qty variance as outside of tolerance!
                                        ELSE
                                          YN_YES  -- Supplier doesnt care about qty variances, let the qty variance be in within tolerance status.
                                  END)
                              ELSE 
                                 YN_YES
                         END)
    WHERE IMPR.PROCESS_ID  = I_PROCESS_ID
      AND IMPR.SUPPLIER    = I_SUPPLIER_ID
      AND IMPR.CONFIG_ID   = I_CONFIG_ID
      AND IMPR.MATCH_LEVEL = I_MATCH_LEVEL;

   LOGGER.LOG_INFORMATION(L_program||' merge IM_MATCH_POOL_RESULTS exact match- SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Check tolerance table to see if we can pass some cost variances as within tolerance...
   UPDATE IM_MATCH_POOL_RESULTS IMPR
      SET IS_COST_VWT = YN_YES
    WHERE IMPR.ROWID IN (
                SELECT RESULT_ROWID
                  FROM (SELECT IMPR.ROWID           AS RESULT_ROWID,
                               IDH.CURRENCY_CODE    AS DOC_CURRENCY,
                               IMPT.LOWER_LIMIT_INCLUSIVE,
                               IMPT.UPPER_LIMIT_EXCLUSIVE,
                               IMPT.TOL_VALUE_TYPE,
                               IMPT.TOL_VALUE,
                               IMPR.LHS_TOTAL_COST, 
                               IMPR.TOTAL_COST_VAR, 
                               --
                               (SELECT EXCHANGE_RATE
                                  FROM MV_CURRENCY_CONVERSION_RATES
                                 WHERE FROM_CURRENCY  = IDH.CURRENCY_CODE
                                   AND TO_CURRENCY    = TH.CURRENCY_CODE
                                   AND EXCHANGE_TYPE  = 'C'
                                   AND EFFECTIVE_DATE = (SELECT Max(EFFECTIVE_DATE)
                                                           FROM MV_CURRENCY_CONVERSION_RATES
                                                          WHERE FROM_CURRENCY   = IDH.CURRENCY_CODE
                                                            AND TO_CURRENCY     = TH.CURRENCY_CODE
                                                            AND EXCHANGE_TYPE   = 'C'
                                                            AND EFFECTIVE_DATE <= L_VDATE)) EXCHANGE_RATE,
                               --
                               IMPR.TOTAL_COST_VAR AS ORIG_COST_VAR,
                               IMPR.RHS_TOTAL_COST AS RHS_TOTAL_COST
                          FROM IM_MATCH_POOL_RESULTS IMPR,
                               IM_MATCH_POOL_TOLERANCES IMPT,
                               IM_DOC_HEAD IDH,
                               IM_TOLERANCE_DETAIL TD,
                               IM_TOLERANCE_HEAD TH
                         WHERE IMPR.PROCESS_ID       = I_PROCESS_ID
                           AND IMPR.SUPPLIER         = I_SUPPLIER_ID
                           AND IMPR.CONFIG_ID        = I_CONFIG_ID
                           AND IMPT.PROCESS_ID       = I_PROCESS_ID
                           AND IMPT.SUPPLIER         = I_SUPPLIER_ID
                           AND IMPT.CONFIG_ID        = I_CONFIG_ID
                           AND IMPT.POOL_KEY         = IMPR.POOL_KEY
                           AND IMPT.FAVOR_OF         = IMPR.COST_VAR_FAVOR_OF
                           AND IMPT.SUMMARY_LINE_IND = L_SUMMARY_OR_LINE
                           AND IDH.DOC_ID            = (SELECT DOC_REF
                                                          FROM IM_MATCH_DOC IMD
                                                         WHERE IMD.PROCESS_ID = I_PROCESS_ID
                                                           AND IMD.SUPPLIER   = I_SUPPLIER_ID
                                                           AND IMD.CONFIG_ID  = I_CONFIG_ID
                                                           AND IMD.POOL_KEY   = IMPR.POOL_KEY
                                                           AND IMD.DOC_TYPE   IN (DOC_TYPE_INVOICE, DOC_TYPE_CREDIT_NOTE)
                                                           AND ROWNUM         < 2)
                           AND IMPR.MATCH_LEVEL      = I_MATCH_LEVEL
                           AND IMPR.IS_COST_VWT      = YN_NO
                           AND IMPT.TOL_KEY          = TD.TOLERANCE_DETAIL_ID
                           AND TD.TOLERANCE_ID       = TH.TOLERANCE_ID)
                 WHERE ABS(LHS_TOTAL_COST*EXCHANGE_RATE) >= LOWER_LIMIT_INCLUSIVE
                   AND ABS(LHS_TOTAL_COST*EXCHANGE_RATE) <  UPPER_LIMIT_EXCLUSIVE
                   AND YN_YES = (CASE WHEN TOL_VALUE_TYPE = 'PCT' THEN
                                         CASE WHEN Abs(ORIG_COST_VAR) <= Abs(RHS_TOTAL_COST*TOL_VALUE/100) THEN 
                                                 YN_YES
                                              ELSE 
                                                 YN_NO
                                              END
                                      ELSE
                                         CASE WHEN Abs(TOTAL_COST_VAR*EXCHANGE_RATE) <= Abs(TOL_VALUE) THEN
                                                 YN_YES
                                              ELSE
                                                 YN_NO
                                              END
                                      END));

   LOGGER.LOG_INFORMATION(L_program||' merge IM_MATCH_POOL_RESULTS is_cost_vwt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Check tolerance table to see if we can pass some qty variances
   -- as within tolerance...
   UPDATE IM_MATCH_POOL_RESULTS IMPR
      SET IS_QTY_VWT = YN_YES
    WHERE IMPR.ROWID IN (SELECT RESULT_ROWID
                           FROM (SELECT IMPR.ROWID AS RESULT_ROWID,
                                        IMPT.LOWER_LIMIT_INCLUSIVE,
                                        IMPT.UPPER_LIMIT_EXCLUSIVE,
                                        IMPT.TOL_VALUE_TYPE,
                                        IMPT.TOL_VALUE,
                                        IMPR.TOTAL_QTY_VAR,
                                        IMPR.RHS_TOTAL_QTY,
                                        IMPR.LHS_TOTAL_QTY
                                   FROM IM_MATCH_POOL_RESULTS IMPR,
                                        IM_MATCH_POOL_TOLERANCES IMPT
                                  WHERE IMPR.PROCESS_ID       = I_PROCESS_ID
                                    AND IMPR.SUPPLIER         = I_SUPPLIER_ID
                                    AND IMPR.CONFIG_ID        = I_CONFIG_ID
                                    AND IMPT.PROCESS_ID       = I_PROCESS_ID
                                    AND IMPT.SUPPLIER         = I_SUPPLIER_ID
                                    AND IMPT.POOL_KEY         = IMPR.POOL_KEY
                                    AND IMPT.CONFIG_ID        = IMPR.CONFIG_ID
                                    AND IMPT.FAVOR_OF         = IMPR.QTY_VAR_FAVOR_OF
                                    AND IMPT.SUMMARY_LINE_IND = L_SUMMARY_OR_LINE
                                    AND IMPR.MATCH_LEVEL      = I_MATCH_LEVEL
                                    AND IMPR.IS_QTY_VWT       = YN_NO)
                          WHERE Abs(LHS_TOTAL_QTY) >= LOWER_LIMIT_INCLUSIVE
                            AND Abs(LHS_TOTAL_QTY) <  UPPER_LIMIT_EXCLUSIVE
                            AND   YN_YES = (CASE WHEN TOL_VALUE_TYPE = 'PCT' THEN
                                                    CASE WHEN Abs(TOTAL_QTY_VAR) <= Abs(RHS_TOTAL_QTY*TOL_VALUE/100) THEN 
                                                            YN_YES
                                                         ELSE
                                                            YN_NO
                                                         END
                                                 ELSE
                                                    CASE WHEN Abs(TOTAL_QTY_VAR) <= Abs(TOL_VALUE) THEN
                                                            YN_YES
                                                         ELSE
                                                            YN_NO
                                                         END
                                                 END));

   LOGGER.LOG_INFORMATION(L_program||' merge IM_MATCH_POOL_RESULTS is_qty_vwt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   O_STATUS := SUCCESS;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

   RETURN;

EXCEPTION
   WHEN OTHERS THEN
      O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')||
                            SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
     UPDATE_ALL_RESULT_TXTS(O_ERROR_MESSAGE,I_PROCESS_ID,I_CONFIG_ID,I_SUPPLIER_ID,I_MATCH_LEVEL);
     O_STATUS := FAIL;

     RETURN;
END APPLY_MATCH_TOLERANCES;
----------------------------------------------------------------------------------


    /**************************
     EVALUATE and do the necessary data associations
     for successful matches.  Mainly:

     - Creating MATCH_ID on successful matches on IM_MATCH_POOL_RESULTS.
     - Updating the RESULT_TXT on failed matches on IM_MATCH_POOL_RESULTS.

    **************************/
    PROCEDURE PROCESS_SUCCESSFUL_MATCHES(O_STATUS       OUT NUMBER,
                                O_ERROR_MESSAGE   OUT VARCHAR2,
                                I_PROCESS_ID      IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID       IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID     IN SUPS.SUPPLIER%TYPE,
                                I_MATCH_LEVEL     IN IM_MATCH_POOL_RESULTS.MATCH_LEVEL%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.PROCESS_SUCCESSFUL_MATCHES';
        L_start_time          TIMESTAMP := SYSTIMESTAMP;
    BEGIN
        --------------------------
        -- Create successful match ID's for SUMMARY and ONE-TO-ONE matches
        --------------------------
        UPDATE IM_MATCH_POOL_RESULTS IMPR
        SET    IMPR.SUCCESS_MATCH_ID = IM_MATCH_SEQ.NEXTVAL
        WHERE  IMPR.PROCESS_ID  = I_PROCESS_ID
        AND    IMPR.SUPPLIER    = I_SUPPLIER_ID
        AND    IMPR.CONFIG_ID   = I_CONFIG_ID
        AND    IMPR.IS_COST_VWT = YN_YES
        AND    IMPR.IS_QTY_VWT  = YN_YES
        AND    IMPR.MATCH_LEVEL = I_MATCH_LEVEL
        AND    I_MATCH_LEVEL IN (MATCH_LEVEL_SUMMARY, MATCH_LEVEL_ONE_TO_ONE)
        AND    (IMPR.RESULT_TXT IS NULL
                OR
                IS_ERROR(IMPR.RESULT_TXT) = YN_NO );

        --------------------------
        -- Create successful match ID's for DETAIL level matches
        --------------------------
        FOR REC IN (

              -- Make sure we only pick documents with no detail
              -- match errors on any of their items.   For detail
              -- match,  1 pool guaranteed to have 1 lhs document (cn or invoice).

              SELECT  D.PROCESS_ID,
                      D.SUPPLIER,
                      D.CONFIG_ID,
                      D.POOL_KEY
              FROM (
                    SELECT  IMPR.PROCESS_ID,
                            IMPR.SUPPLIER,
                            IMPR.CONFIG_ID,
                            IMPR.POOL_KEY,
                            Sum(
                                  CASE
                                    WHEN IMPR.RESULT_TXT IS NOT NULL AND IS_ERROR(IMPR.RESULT_TXT) = YN_YES  THEN 1
                                    ELSE 0
                                  END
                            ) AS HAS_ERROR
                    FROM  IM_MATCH_POOL_ITEM IMPI,
                          IM_MATCH_POOL_RESULTS  IMPR
                    WHERE IMPR.PROCESS_ID   = I_PROCESS_ID
                    AND   IMPR.SUPPLIER     = I_SUPPLIER_ID
                    AND   IMPR.CONFIG_ID    = I_CONFIG_ID
                    AND   IMPR.MATCH_LEVEL  = MATCH_LEVEL_DETAIL
                    AND   IMPI.PROCESS_ID   = I_PROCESS_ID
                    AND   IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
                    GROUP BY IMPR.PROCESS_ID, IMPR.SUPPLIER, IMPR.CONFIG_ID, IMPR.POOL_KEY, IMPR.ITEM) D
              WHERE D.HAS_ERROR = 0 ) LOOP


              -- Do the update.  Each detail item level match pool result gets a
              -- successful match id.
              UPDATE IM_MATCH_POOL_RESULTS
              SET    SUCCESS_MATCH_ID = IM_MATCH_SEQ.NEXTVAL
              WHERE  PROCESS_ID = REC.PROCESS_ID
              AND    SUPPLIER   = REC.SUPPLIER
              AND    CONFIG_ID  = REC.CONFIG_ID
              AND    POOL_KEY   = REC.POOL_KEY
              AND    MATCH_LEVEL  = MATCH_LEVEL_DETAIL;

        END LOOP;



        --------------------------
        -- Now that we've created success_match_id's for successful matches at the
        -- given level,   make sure we tag the candidate documents with the
        -- level they were matched at to inform succeeding match attemp processed NOT to
        -- pick up the documents again.
        --------------------------

        MERGE INTO IM_MATCH_DOC TGT
        USING (
              SELECT UNIQUE IMD.ROWID AS IMD_ROWID
              FROM  IM_MATCH_DOC IMD,
                    IM_MATCH_POOL_RESULTS IMPR
              WHERE IMPR.PROCESS_ID   = I_PROCESS_ID
              AND   IMPR.SUPPLIER     = I_SUPPLIER_ID
              AND   IMPR.CONFIG_ID    = I_CONFIG_ID
              ---
              AND    IMPR.PROCESS_ID  = IMD.PROCESS_ID
              AND    IMPR.SUPPLIER    = IMD.SUPPLIER
              AND    IMPR.CONFIG_ID   = IMD.CONFIG_ID
              AND    IMPR.POOL_KEY    = IMD.POOL_KEY
              AND    IMPR.MATCH_LEVEL = I_MATCH_LEVEL
              AND    IMPR.SUCCESS_MATCH_ID IS NOT NULL  -- << only those tagged as successful matches!
              AND    (
                        (
                            -- For summary matches,  all documents within the pools will be updated
                            I_MATCH_LEVEL = MATCH_LEVEL_SUMMARY
                            AND
                            IMPR.LHS_DOC_ID IS NULL AND IMPR.RHS_DOC_ID IS NULL
                        )
                        OR
                        (
                            -- For one-to-one matches, we need to update specific documents.
                            I_MATCH_LEVEL = MATCH_LEVEL_ONE_TO_ONE
                            AND
                            IMD.DOC_REF IN (IMPR.LHS_DOC_ID, IMPR.RHS_DOC_ID)
                        )
                        OR
                        (
                            I_MATCH_LEVEL = MATCH_LEVEL_DETAIL
                            AND
                            (IMD.DOC_TYPE, IMD.DOC_REF)  IN (
                                SELECT IMPI.DOC_TYPE, IMPI.DOC_ID
                                FROM   IM_MATCH_POOL_ITEM IMPI
                                WHERE IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
                            )
                        )
                    )

        ) SRC
        ON (TGT.ROWID = SRC.IMD_ROWID)
        WHEN MATCHED THEN UPDATE SET TGT.MATCH_LEVEL = I_MATCH_LEVEL;

        REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);

        O_STATUS := SUCCESS;

        RETURN;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));

          UPDATE_ALL_RESULT_TXTS(O_ERROR_MESSAGE,I_PROCESS_ID,I_CONFIG_ID,I_SUPPLIER_ID,I_MATCH_LEVEL);

          O_STATUS := FAIL;

          RETURN;
    END PROCESS_SUCCESSFUL_MATCHES;
----------------------------------------------------------------------------------



    --------------------------------------------
    FUNCTION GET_MATCH_SIDE (I_DOC_TYPE VARCHAR2)
    RETURN VARCHAR2
    IS
    BEGIN
       IF I_DOC_TYPE IN (DOC_TYPE_INVOICE, DOC_TYPE_CREDIT_NOTE) THEN
          RETURN MATCH_SIDE_LEFT;
       END IF;

       RETURN MATCH_SIDE_RIGHT;
    END;

    /**************************
    Calculates the resolution date of documents.
    **************************/

    FUNCTION GET_RESOLUTION_DATE(I_DUE_DATE  DATE,
                                 I_CURRENT_DATE DATE,
                                 I_DISCREPANCY_TYPE VARCHAR2)
    RETURN DATE
    IS
        RESOLVE_DATE  DATE := I_CURRENT_DATE;
        L_RESOLUTION_DAYS NUMBER;

        CURSOR C_GET_RESLN_DUE_DAYS
        IS
        SELECT Nvl(RESOLUTION_DUE_DAYS, 1)
          FROM IM_SYSTEM_OPTIONS;

    BEGIN

        OPEN C_GET_RESLN_DUE_DAYS;
        FETCH C_GET_RESLN_DUE_DAYS INTO L_RESOLUTION_DAYS;
        CLOSE C_GET_RESLN_DUE_DAYS;

        RESOLVE_DATE := RESOLVE_DATE+ L_RESOLUTION_DAYS;


        IF RESOLVE_DATE >= I_DUE_DATE THEN
            RESOLVE_DATE := I_DUE_DATE-1;

        END IF;

        RETURN RESOLVE_DATE;
    END;



    /**
    Given an amount from a supplier an another amount from the retailer,
    determine if the difference favors one party or the other.
    **/
    FUNCTION GET_FAVOR_OF(I_RETAILER_AMT       NUMBER,
                          I_SUPPLIER_AMT       NUMBER,
                          I_MATCH_TYPE         VARCHAR2)
    RETURN VARCHAR2
    IS
    BEGIN

        IF I_MATCH_TYPE IN (MATCH_TYPE_CRDNT_CRDNRQ) THEN

            IF (I_RETAILER_AMT - I_SUPPLIER_AMT) < 0 THEN

                -- Retailer requests credit for -100
                -- Supplier sends -80
                -- Difference = -100 - (-80) = -20
                -- Retailer underpaid by -20.
                -- Favor Of = SUPPLIER

                RETURN FAVOR_OF_SUPPLIER;

            ELSE

                -- Retailer requests credit for -80
                -- Supplier sends -100
                -- Difference = -80 - (-100) = +20
                -- Retailer OVERpaid by +20.
                -- Favor Of = RETAILER


                RETURN FAVOR_OF_RETAILER;

            END IF;

        ELSIF I_MATCH_TYPE IN (MATCH_TYPE_MRCHI_RCPT) THEN

            IF (I_RETAILER_AMT - I_SUPPLIER_AMT) < 0 THEN

                -- Retailer receives merchandise worth 80
                -- Supplier invoices retailer for 100
                -- Difference = 80 - 100 = -20
                -- Retailer OVERBILLED by -20.
                -- Favor Of = SUPPLIER

                RETURN FAVOR_OF_SUPPLIER;

            ELSE

                -- Retailer receives merchandise worth 100
                -- Supplier invoices retailer for 80
                -- Difference = 100 - 80 = +20
                -- Retailer UNDERBILLED by 20.
                -- Favor Of = RETAILER

                RETURN FAVOR_OF_RETAILER;

            END IF;
        END IF;

        RETURN FAVOR_OF_RETAILER;

    END;



----------------------------------------------------------------------------------
    FUNCTION AS_ERROR(I_MSG  IM_MATCH_POOL_RESULTS.RESULT_TXT%TYPE)
    RETURN VARCHAR2
    IS
    BEGIN
         RETURN SubStr(MATCH_ERROR_PREFIX||' '||I_MSG, 1, 150);
    END;


----------------------------------------------------------------------------------
    FUNCTION IS_ERROR(I_TXT IM_MATCH_POOL_RESULTS.RESULT_TXT%TYPE)
    RETURN VARCHAR2
    IS
    BEGIN
        IF I_TXT IS NOT NULL AND I_TXT LIKE '%'||MATCH_ERROR_PREFIX||'%'
        THEN RETURN YN_YES;
        END IF;

        RETURN YN_NO;
    END;


----------------------------------------------------------------------------------


    /**
    GETS the match type for the given config.

    Returns either MATCH_TYPE_CRDNT_CRDNRQ or MATCH_TYPE_INVC_RCPT
    **/
    FUNCTION GET_MATCH_TYPE(I_CONFIG_ID IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE )
    RETURN VARCHAR2
    IS
        L_MATCH_TYPE   IM_MATCH_POOL_CONFIG.MATCH_TYPE%TYPE;
        CURSOR C_MATCH_TYPE
        IS
        SELECT MATCH_TYPE
          FROM IM_MATCH_POOL_CONFIG
         WHERE CONFIG_ID = I_CONFIG_ID;

    BEGIN
        OPEN C_MATCH_TYPE;
        FETCH C_MATCH_TYPE INTO L_MATCH_TYPE;
        CLOSE C_MATCH_TYPE;

        RETURN L_MATCH_TYPE;
    END;


----------------------------------------------------------------------------------

    /**
    VALIDATES the generated cost and quantity discrepancies for each pool_key item,
    to make sure that the left and right hand side costs balance with the discrepancies.
    **/
    PROCEDURE VALIDATE_DISCREPANCIES(O_STATUS       OUT NUMBER,
                                O_ERROR_MESSAGE   OUT VARCHAR2,
                                I_PROCESS_ID      IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID       IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID     IN SUPS.SUPPLIER%TYPE,
                                I_MATCH_LEVEL     IN IM_MATCH_POOL_RESULTS.MATCH_LEVEL%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.VALIDATE_DISCREPANCIES';

        L_MATCH_TYPE    IM_MATCH_POOL_CONFIG.MATCH_TYPE%TYPE;

    BEGIN

        L_MATCH_TYPE := GET_MATCH_TYPE(I_CONFIG_ID);

        MERGE INTO  IM_MATCH_POOL_RESULTS TGT
        USING(
        SELECT
              L_COST.ROW_ID,
              CASE
                  WHEN L_COST.AMOUNT <> R_COST.AMOUNT - (QD.AMOUNT+CD.AMOUNT) THEN
                      'LHS Cost '||L_COST.AMOUNT||' <> RHS Cost '||R_COST.AMOUNT
                      ||' - ( Qty Var '||qd.amount||' + Cost Var '||cd.amount||')'
                  ELSE NULL
              END
              AS BALANCE_ERROR
        FROM (
              SELECT IMPR.ROWID ROW_ID,
                     Sum(impi.unit_cost * impi.qty) AS AMOUNT
              FROM   IM_MATCH_POOL_RESULTS IMPR,
                    IM_MATCH_POOL_ITEM    IMPI
              WHERE  IMPR.PROCESS_ID = I_PROCESS_ID
              AND    IMPR.CONFIG_ID  = I_CONFIG_ID
              AND    IMPR.SUPPLIER = I_SUPPLIER_ID
              AND    IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
              AND    L_MATCH_TYPE     = MATCH_TYPE_CRDNT_CRDNRQ
              ---
              AND    IMPI.PROCESS_ID = I_PROCESS_ID
              AND    IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
              AND    IMPI.ITEM = IMPR.ITEM
              AND    IMPI.MATCH_SIDE = MATCH_SIDE_LEFT
            GROUP BY IMPR.ROWID
        ) L_COST
        ,
        (
              SELECT IMPR.ROWID ROW_ID,
                    Sum(impi.unit_cost * impi.qty) AS   AMOUNT
              FROM   IM_MATCH_POOL_RESULTS IMPR,
                    IM_MATCH_POOL_ITEM    IMPI
              WHERE  IMPR.PROCESS_ID = I_PROCESS_ID
              AND    IMPR.CONFIG_ID  = I_CONFIG_ID
              AND    IMPR.SUPPLIER = I_SUPPLIER_ID
              AND    IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
              AND    L_MATCH_TYPE     = MATCH_TYPE_CRDNT_CRDNRQ
              ---
              AND    IMPI.PROCESS_ID = I_PROCESS_ID
              AND    IMPI.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
              AND    IMPI.ITEM = IMPR.ITEM
              AND    IMPI.MATCH_SIDE = MATCH_SIDE_RIGHT
            GROUP BY IMPR.ROWID
        ) R_COST
        ,
        (
              SELECT IMPR.ROWID ROW_ID,
                    Sum(IMQD.QTY * IMQD.UNIT_COST) AS AMOUNT
              FROM   IM_MATCH_POOL_RESULTS IMPR,
                    IM_MATCH_QTY_VAR     IMQD
              WHERE  IMPR.PROCESS_ID = I_PROCESS_ID
              AND    IMPR.CONFIG_ID  = I_CONFIG_ID
              AND    IMPR.SUPPLIER = I_SUPPLIER_ID
              AND    IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
              --
              AND    IMQD.PROCESS_ID = I_PROCESS_ID
              AND    IMQD.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
            GROUP BY IMPR.ROWID
        ) QD
        ,
        (
              SELECT IMPR.ROWID ROW_ID,
                    Sum(IMCD.QUANTITY * IMCD.AMOUNT) AS AMOUNT
              FROM   IM_MATCH_POOL_RESULTS IMPR,
                    IM_MATCH_COST_VAR     IMCD
              WHERE  IMPR.PROCESS_ID = I_PROCESS_ID
              AND    IMPR.CONFIG_ID  = I_CONFIG_ID
              AND    IMPR.SUPPLIER = I_SUPPLIER_ID
              AND    IMPR.MATCH_LEVEL = MATCH_LEVEL_DETAIL
              --
              AND    IMCD.PROCESS_ID = I_PROCESS_ID
              AND    IMCD.MATCH_POOL_RESULT_ID = IMPR.MATCH_POOL_RESULT_ID
            GROUP BY IMPR.ROWID
        )  CD
        WHERE L_COST.ROW_ID = R_COST.ROW_ID(+)
        ---
        AND   L_COST.ROW_ID = QD.ROW_ID(+)
        ---
        AND   L_COST.ROW_ID = CD.ROW_ID(+)
        ) SRC
        ON (TGT.ROWID = SRC.ROW_ID AND  SRC.BALANCE_ERROR IS NOT NULL)
        WHEN MATCHED THEN UPDATE SET TGT.RESULT_TXT = AS_ERROR(SRC.BALANCE_ERROR);

        O_STATUS := SUCCESS;

        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));

          O_STATUS := FAIL;

          RETURN;
    END VALIDATE_DISCREPANCIES;

----------------------------------------------------------------------------------------
    FUNCTION IS_CREDIT_NOTE_REQUEST(DOC_TYPE    IM_DOC_HEAD.TYPE%TYPE)
    RETURN VARCHAR2
    IS
    BEGIN
        IF DOC_TYPE IN (DOC_TYPE_CREDIT_NOTE_RQ_QTY, DOC_TYPE_CREDIT_NOTE_RQ_COST , DOC_TYPE_CREDIT_NOTE_RQ_TAX) THEN
          RETURN YN_YES;
        END IF;

        RETURN YN_NO;
    END;



    /******************************

    Validates match results - regardless of level.

    *******************************/
    PROCEDURE VALIDATE_MATCH_RESULTS (O_STATUS         OUT NUMBER,
                                O_ERROR_MESSAGE  OUT VARCHAR2,
                                I_PROCESS_ID  IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID   IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID IN SUPS.SUPPLIER%TYPE)
    IS
        L_PROGRAM       VARCHAR2(50) := 'REIM_MATCH_SQL.VALIDATE_MATCH_RESULTS';
    BEGIN
        O_STATUS := SUCCESS;

        ---------------------------
        -- Match pools with different currencies
        -- will not be matched
        ---------------------------
        UPDATE IM_MATCH_POOL_RESULTS TGT
        SET   TGT.SUCCESS_MATCH_ID = NULL,
              TGT.RESULT_TXT = AS_ERROR('Match pool documents have different currencies.')
        WHERE (TGT.PROCESS_ID, TGT.SUPPLIER, TGT.CONFIG_ID, TGT.POOL_KEY) IN
        (
              SELECT PROCESS_ID, SUPPLIER, CONFIG_ID, POOL_KEY
              FROM (
                  -- Count the currencies of all docs within each pool
                  -- and expose those with more than 1 currency
                  SELECT PROCESS_ID, SUPPLIER, CONFIG_ID, POOL_KEY, Count(*)
                  FROM (
                          -- Identify currencies of all docs within each pool...
                          SELECT  UNIQUE
                                  IMPR.PROCESS_ID,
                                  IMPR.SUPPLIER,
                                  IMPR.CONFIG_ID,
                                  IMPR.POOL_KEY,
                                  IDH.CURRENCY_CODE
                          FROM IM_MATCH_DOC IMD,
                              IM_DOC_HEAD IDH,
                              IM_MATCH_POOL_RESULTS IMPR
                          WHERE IMPR.PROCESS_ID = I_PROCESS_ID
                          AND   IMPR.SUPPLIER = I_SUPPLIER_ID
                          AND   IMPR.CONFIG_ID = I_CONFIG_ID
                          AND   IMD.PROCESS_ID = IMPR.PROCESS_ID
                          AND   IMD.SUPPLIER = IMPR.SUPPLIER
                          AND   IMD.CONFIG_ID = IMPR.CONFIG_ID
                          AND   IMD.POOL_KEY  = IMPR.POOL_KEY
                          AND   IDH.DOC_ID = IMD.DOC_REF
                          AND   IDH.TYPE = IMD.DOC_TYPE
                  ) GROUP BY PROCESS_ID, SUPPLIER, CONFIG_ID, POOL_KEY
                  HAVING Count(*) > 1
              )
        );


        RETURN;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := Nvl(O_ERROR_MESSAGE,'')
                                  ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;

          RETURN;
    END VALIDATE_MATCH_RESULTS;
----------------------------------------------------------------------------------
	
END REIM_MATCH_SQL;
/
