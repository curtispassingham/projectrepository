CREATE OR REPLACE PACKAGE REIM_ONLINE_MATCH_CN_SQL AS
----------------------------------------------------------------
FUNCTION CREATE_MATCH_CN_DETAIL_WS(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN OUT IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
-- The public function used for Initializing Credit Note Match Data for Online matching.
-- Output param: O_match_workspace_id (The ID of the Match workspace, new one if I_match_workspace_id is not passed in)
--
-- Input param: I_search_wspace_id (The ID of the Search workspace)
--              I_match_wspace_id (The ID of the Match workspace, if search is performed on the match screen)
--
FUNCTION INIT_ONLINE_MATCH_DATA(O_error_message       OUT VARCHAR2,
                                O_match_wspace_id     OUT IM_MATCH_CN_WS.WORKSPACE_ID%TYPE,
                                I_search_wspace_id IN     IM_CN_SEARCH_WS.WORKSPACE_ID%TYPE,
                                I_match_wspace_id  IN     IM_MATCH_CN_WS.WORKSPACE_ID%TYPE DEFAULT NULL)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION PERSIST_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                 I_workspace_id  IN OUT IM_MATCH_CN_DETL_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
-- The public function used for Refreshing Credit Note Match Data for Online matching.
-- Input param: I_workspace_id (The ID of the Match workspace)
--              I_skip_matched (Identifier to unselect Matched documents)
----------------------------------------------------------------
FUNCTION REFRESH_MTCH_WSPACE(O_error_message    OUT VARCHAR2,
                             I_workspace_id  IN     IM_MATCH_CN_DETL_WS.WORKSPACE_ID%TYPE,
                             I_skip_matched  IN     VARCHAR2)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION DIRTY_LOCK_CHECKS(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION PERFORM_SMRY_MATCH_ONLINE(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION SUGGEST_MATCH(O_error_message    OUT VARCHAR2,
                       O_match_count      OUT NUMBER,
                       I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
-- The public function used for Validating Tax compliance
-- Input param: I_workspace_id (The ID of the Match workspace)
----------------------------------------------------------------
FUNCTION VALIDATE_TAX(O_error_message    OUT VARCHAR2,
                      O_tax_compliant    OUT VARCHAR2,
                      O_tax_disc_lvl     OUT VARCHAR2,
                      I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION PERFORM_ONLINE_RESLN(O_error_message          OUT VARCHAR2,
                              I_workspace_id        IN OUT IM_DETAIL_MATCH_CN_WS.WORKSPACE_ID%TYPE,
                              I_cost_decision       IN     IM_DOC_DETAIL_REASON_CODES.COST_MATCHED%TYPE,
                              I_qty_decision        IN     IM_DOC_DETAIL_REASON_CODES.QTY_MATCHED%TYPE,
                              I_resln_action_rc_tbl IN     IM_RESLN_ACTION_RC_TBL)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION PERFORM_ONLINE_TAX_RESLN(O_error_message     OUT VARCHAR2,
                                  I_workspace_id   IN OUT IM_DETAIL_MATCH_CN_WS.WORKSPACE_ID%TYPE,
                                  I_tax_decision   IN     VARCHAR2,
                                  I_tax_resln_rc   IN     IM_REASON_CODES.REASON_CODE_ID%TYPE,
                                  I_tax_resln_cmnt IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
END REIM_ONLINE_MATCH_CN_SQL;
/
