CREATE OR REPLACE PACKAGE BODY REIM_EDI_MAINT_SQL AS
----------------------------------------------------------------
----------------------------------------------------------------

LP_doc_max_count NUMBER(4) := 300;

----------------------------------------------------------------

LP_doc_with varchar2(5000) := q'[
   with sc_scalars as (
      select :1 as start_document_date,
             :2 as end_document_date,
             :3 as loc_type
        from dual),
   sc_inject_sources as (select value(in_inject_sources) inject_source
                       from table(cast(:4 as OBJ_VARCHAR_DESC_TABLE)) in_inject_sources),
   sc_rules          as (select value(in_rules) rule
                       from table(cast(:5 as OBJ_VARCHAR_DESC_TABLE)) in_rules),
   sc_doc_types      as (select value(in_doc_types) doc_type
                       from table(cast(:6 as OBJ_VARCHAR_ID_TABLE)) in_doc_types),
   sc_ext_doc_ids    as (select value(in_ext_doc_ids) ext_doc_id
                       from table(cast(:7 as OBJ_VARCHAR_DESC_TABLE)) in_ext_doc_ids),
   sc_vendor_types   as (select value(in_vendor_types) vendor_type
                       from table(cast(:8 as OBJ_VARCHAR_ID_TABLE)) in_vendor_types),
   sc_vendors        as (select value(in_vendors) vendor
                       from table(cast(:9 as OBJ_VARCHAR_ID_TABLE)) in_vendors),
   sc_vendor_names   as (select value(in_vendor_names) vendor_name
                       from table(cast(:10 as OBJ_VARCHAR_DESC_TABLE)) in_vendor_names),
   sc_supplier_sites as (select value(in_supplier_sites) supplier_site
                       from table(cast(:11 as OBJ_NUMERIC_ID_TABLE)) in_supplier_sites),
   sc_sup_site_names as (select value(in_sup_site_names) sup_site_name
                       from table(cast(:11 as OBJ_VARCHAR_DESC_TABLE)) in_sup_site_names),
   sc_locs           as (select value(in_locs) loc
                       from table(cast(:12 as OBJ_NUMERIC_ID_TABLE)) in_locs),
   sc_loc_names      as (select value(in_loc_names) loc_name
                       from table(cast(:13 as OBJ_VARCHAR_DESC_TABLE)) in_loc_names),
   sc_order_nos      as (select value(in_order_nos) order_no
                       from table(cast(:14 as OBJ_NUMERIC_ID_TABLE)) in_order_nos),
   sc_ap_reviewers   as (select value(in_ap_reviewers) ap_reviewer
                       from table(cast(:15 as OBJ_VARCHAR_ID_TABLE)) in_ap_reviewers),
   sc_group_ids      as (select value(in_group_ids) group_id
                       from table(cast(:16 as OBJ_NUMERIC_ID_TABLE)) in_group_ids)
   ]';

----------------------------------------------------------------

FUNCTION COMMON_DOC_SEARCH(O_error_message     IN OUT VARCHAR2,
                           I_search_criteria   IN OUT IM_EDI_SEARCH_CRITERIA_REC,
                           I_workspace_id      IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION SETUP_DOC_FROM(O_error_message     IN OUT VARCHAR2,
                        IO_from             IN OUT VARCHAR2,
                        I_search_criteria   IN     IM_EDI_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
---
FUNCTION SETUP_DOC_WHERE(O_error_message     IN OUT VARCHAR2,
                         IO_where            IN OUT VARCHAR2,
                         I_search_criteria   IN     IM_EDI_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
---
FUNCTION REFRESH_WORKSPACE(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
---
FUNCTION DIRTY_LOCK_CHECKS(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                           I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
---
FUNCTION LOG_SEARCH_CRITERIA(O_error_message     IN OUT VARCHAR2,
                             I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                             I_search_criteria   IN     IM_EDI_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
---
FUNCTION UPDATE_SUCCESSFUL_DOC(O_error_message    OUT VARCHAR2,
                               I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                               I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION UPDATE_CHOICE_FLAG_SEARCH_WS(O_error_message    OUT VARCHAR2,
                                      I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                                      I_choice_flag   IN     IM_EDI_DOC_SEARCH_WS.CHOICE_FLAG%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.UPDATE_CHOICE_FLAG_SEARCH_WS';
   L_start_time   TIMESTAMP := SYSTIMESTAMP;

BEGIN

   update im_edi_doc_search_ws
      set choice_flag = I_choice_flag
    where qbe_filter_ind = 'N';

   LOGGER.LOG_INFORMATION(L_program||' Update im_edi_doc_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UPDATE_CHOICE_FLAG_SEARCH_WS;
----------------------------------------------------------------
FUNCTION MARK_FOR_DELETE(O_error_message    OUT VARCHAR2,
                         I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                         I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE DEFAULT NULL)
RETURN NUMBER IS

   L_program      VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.MARK_FOR_DELETE';
   L_start_time   TIMESTAMP := SYSTIMESTAMP;

   L_user IM_DOC_HEAD.MATCH_ID%TYPE := get_user;

BEGIN

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id,
                        I_inject_doc_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date(number_1)
      select ws.inject_doc_id
        from im_edi_doc_search_ws ws
       where ws.workspace_id   = I_workspace_id
         and (   (    I_inject_doc_id is NOT NULL
                  and ws.inject_doc_id = I_inject_doc_id)
              or (    ws.choice_flag    = 'Y'
                  and ws.qbe_filter_ind = 'N'));

   update im_inject_doc_head dh
      set dh.last_updated_by   = L_user,
          dh.last_update_date  = sysdate,
          dh.object_version_id = dh.object_version_id + REIM_CONSTANTS.ONE
    where dh.inject_doc_id in (select gtt.number_1
                                 from gtt_num_num_str_str_date_date gtt);

   update im_inject_doc_error de
      set de.status            = REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_DELETE,
          de.last_updated_by   = L_user,
          de.last_update_date  = sysdate,
          de.object_version_id = de.object_version_id + REIM_CONSTANTS.ONE
    where de.inject_doc_id in (select gtt.number_1
                                 from gtt_num_num_str_str_date_date gtt);

   update im_inject_doc_record dr
      set dr.extracted         = REIM_CONSTANTS.INJ_REC_EXTRCT_STAT_QUAL,
          dr.last_updated_by   = L_user,
          dr.last_update_date  = sysdate,
          dr.object_version_id = dr.object_version_id + REIM_CONSTANTS.ONE
    where dr.inject_doc_id in (select gtt.number_1
                                 from gtt_num_num_str_str_date_date gtt);

   delete from im_edi_doc_search_ws ws
    where ws.workspace_id  =  I_workspace_id
      and ws.inject_doc_id in (select gtt.number_1
                                 from gtt_num_num_str_str_date_date gtt);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END MARK_FOR_DELETE;
----------------------------------------------------------------
FUNCTION UPDATE_ORDER_NO(O_error_message        OUT VARCHAR2,
                         O_updated_doc_count    OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                         I_workspace_id      IN OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                         I_old_order_no      IN OUT IM_EDI_DOC_SEARCH_WS.ORDER_NO%TYPE,
                         I_new_order_no      IN OUT IM_EDI_DOC_SEARCH_WS.ORDER_NO%TYPE)
RETURN NUMBER IS

   L_program        VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.UPDATE_ORDER_NO';
   L_start_time     TIMESTAMP := SYSTIMESTAMP;
   L_user           IM_DOC_HEAD.MATCH_ID%TYPE := get_user;
   L_supplier_check NUMBER(10) := REIM_CONSTANTS.ZERO;
   L_count          NUMBER(10) := REIM_CONSTANTS.ZERO;

   cursor c_valid_supplier_check is
      select count(1)
        from im_edi_doc_search_ws ws
       where ws.workspace_id   = I_workspace_id
         and ws.choice_flag    = 'Y'
         and ws.qbe_filter_ind = 'N'
         and ws.order_no       = I_old_order_no
         and ws.rule           = 'VALID_SUPPLIER';

   cursor c_docs is
      select i.inject_doc_id,
             im_trial_id_seq.nextval trial_id
        from (select distinct ws.inject_doc_id
                from im_edi_doc_search_ws ws
               where ws.workspace_id   = I_workspace_id
                 and ws.choice_flag    = 'Y'
                 and ws.qbe_filter_ind = 'N'
                 and ws.order_no       = I_new_order_no) i;

BEGIN

   if I_old_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_old_order_no',
                                            'NULL',
                                            'NOT NULL');
      return REIM_CONSTANTS.FAIL;
   end if;

   if I_new_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_new_order_no',
                                            'NULL',
                                            'NOT NULL');
      return REIM_CONSTANTS.FAIL;
   end if;

   --stop if VALID_SUPPLIER errors exist
   open c_valid_supplier_check;
   fetch c_valid_supplier_check into L_supplier_check;
   close c_valid_supplier_check;

   if L_supplier_check > REIM_CONSTANTS.ZERO then
      O_error_message := REIM_EDI_MAINT_SQL.VALID_SUPPLIER_EXISTS;
      return REIM_CONSTANTS.FAIL;
   end if;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id,
                        NULL) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --update old errors to fixed
   merge into im_inject_doc_error target
   using (select distinct ws.inject_doc_id
            from im_edi_doc_search_ws ws
           where ws.workspace_id   = I_workspace_id
             and ws.choice_flag    = 'Y'
             and ws.qbe_filter_ind = 'N'
             and ws.order_no       = I_old_order_no) use_this
   on (target.inject_doc_id = use_this.inject_doc_id)
   when matched then update
    set target.status            = REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_FIXED,
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Update im_inject_doc_error - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --update im_inject_doc_head
   merge into im_inject_doc_head target
   using (select distinct ws.inject_doc_id
            from im_edi_doc_search_ws ws
           where ws.workspace_id   = I_workspace_id
             and ws.choice_flag    = 'Y'
             and ws.qbe_filter_ind = 'N'
             and ws.order_no       = I_old_order_no
   ) use_this
   on (target.inject_doc_id  = use_this.inject_doc_id)
   when matched then update
    set target.order_no          = I_new_order_no,
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Update im_inject_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --update search ws is:really needed since refresh?
   update im_edi_doc_search_ws ws
      set ws.order_no       = I_new_order_no
    where ws.workspace_id   = I_workspace_id
      and ws.choice_flag    = 'Y'
      and ws.qbe_filter_ind = 'N'
      and ws.order_no       = I_old_order_no;

   LOGGER.LOG_INFORMATION(L_program||' Update im_edi_doc_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --validate docs and move to operation tables if valid
   O_updated_doc_count := 0;
   for rec in c_docs loop

      if REIM_DOCUMENT_SQL.VALIDATE_DOCUMENT(O_error_message,
                                             rec.inject_doc_id,
                                             rec.trial_id) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

      merge into im_inject_doc_error tgt
      using (select iidr.rule,
                    iidr.fixable_default
               from im_inject_doc_rule iidr) src
      on (    tgt.inject_doc_id = rec.inject_doc_id
          and tgt.rule          = src.rule)
      when MATCHED then
         update
            set tgt.fixable = src.fixable_default;

      O_updated_doc_count := O_updated_doc_count + 1;

      select count(*)
        into L_count
        from im_inject_doc_error de
       where de.inject_doc_id = rec.inject_doc_id
         and de.trial_id      = rec.trial_id;

      LOGGER.LOG_INFORMATION(L_program||' validate loop with inject_doc_id:'||rec.inject_doc_id||' error_count:'||L_count);

      if L_count = 0 then

         if REIM_INJECTOR_SQL.INJECT_VALID_TRANSACTIONS(O_error_message,
                                                        NULL, --I_inject_id
                                                        rec.inject_doc_id,
                                                        L_user) = REIM_CONSTANTS.FAIL then
            return REIM_CONSTANTS.FAIL;
         end if;

         if UPDATE_SUCCESSFUL_DOC(O_error_message,
                                  I_workspace_id,
                                  rec.inject_doc_id) =  REIM_CONSTANTS.FAIL then
            return REIM_CONSTANTS.FAIL;
         end if;

      end if;

   end loop;

   --refresh workspace --is: only do if docs were actually updated?
   if REFRESH_WORKSPACE(O_error_message,
                        I_workspace_id) =  REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UPDATE_ORDER_NO;
----------------------------------------------------------------
FUNCTION UPDATE_ITEM(O_error_message        OUT VARCHAR2,
                     O_updated_doc_count    OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                     I_workspace_id      IN OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                     I_old_item          IN OUT IM_INJECT_DOC_DETAIL.ITEM%TYPE,
                     I_new_item          IN OUT IM_INJECT_DOC_DETAIL.ITEM%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.UPDATE_ITEM';
   L_start_time   TIMESTAMP := SYSTIMESTAMP;
   L_user           IM_DOC_HEAD.MATCH_ID%TYPE := get_user;

   L_supplier_check NUMBER(10) := 0;
   L_count          NUMBER(10) := 0;

   cursor c_valid_supplier_check is
      select count(1)
        from im_edi_doc_search_ws ws,
             im_inject_doc_detail dd
       where ws.workspace_id   = I_workspace_id
         and ws.choice_flag    = 'Y'
         and ws.qbe_filter_ind = 'N'
         and ws.rule           = 'VALID_SUPPLIER'
         and ws.inject_doc_id  = dd.inject_doc_id
         and dd.item           = I_old_item;

   cursor c_docs is
      select i.inject_doc_id,
             im_trial_id_seq.nextval trial_id
        from (select distinct ws.inject_doc_id
                from im_edi_doc_search_ws ws,
                     im_inject_doc_detail dd
               where ws.workspace_id   = I_workspace_id
                 and ws.choice_flag    = 'Y'
                 and ws.qbe_filter_ind = 'N'
                 and ws.inject_doc_id  = dd.inject_doc_id
                 and dd.item           = I_new_item) i;

BEGIN

   if I_old_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_old_item',
                                            'NULL',
                                            'NOT NULL');
      return REIM_CONSTANTS.FAIL;
   end if;

   if I_new_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_new_item',
                                            'NULL',
                                            'NOT NULL');
      return REIM_CONSTANTS.FAIL;
   end if;

   --stop if VALID_SUPPLIER errors exist
   open c_valid_supplier_check;
   fetch c_valid_supplier_check into L_supplier_check;
   close c_valid_supplier_check;

   if L_supplier_check > REIM_CONSTANTS.ZERO then
      O_error_message := REIM_EDI_MAINT_SQL.VALID_SUPPLIER_EXISTS;
      return REIM_CONSTANTS.FAIL;
   end if;

   --verion check locking
   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id,
                        NULL) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --update old errors to fixed
   merge into im_inject_doc_error target
   using (select distinct ws.inject_doc_id
            from im_edi_doc_search_ws ws,
                 im_inject_doc_detail dd
           where ws.workspace_id   = I_workspace_id
             and ws.choice_flag    = 'Y'
             and ws.qbe_filter_ind = 'N'
             and ws.inject_doc_id  = dd.inject_doc_id
             and dd.item           = I_old_item) use_this
   on (target.inject_doc_id = use_this.inject_doc_id)
   when matched then update
    set target.status            = REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_FIXED,
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Update im_inject_doc_error - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --update im_inject_doc_detail
   merge into im_inject_doc_detail target
   using (select distinct ws.inject_doc_id,
                          dd.detail_id
            from im_edi_doc_search_ws ws,
                 im_inject_doc_detail dd
           where ws.workspace_id   = I_workspace_id
             and ws.choice_flag    = 'Y'
             and ws.qbe_filter_ind = 'N'
             and ws.inject_doc_id  = dd.inject_doc_id
             and dd.item           = I_old_item) use_this
   on (    target.inject_doc_id = use_this.inject_doc_id
       and target.detail_id     = use_this.detail_id)
   when matched then update
    set target.item              = I_new_item,
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Update im_inject_doc_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);


   --validate docs and move to operation tables if valid
   O_updated_doc_count := 0;
   for rec in c_docs loop

      if REIM_DOCUMENT_SQL.VALIDATE_DOCUMENT(O_error_message,
                                             rec.inject_doc_id,
                                             rec.trial_id) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

      merge into im_inject_doc_error tgt
      using (select iidr.rule,
                    iidr.fixable_default
               from im_inject_doc_rule iidr) src
      on (    tgt.inject_doc_id = rec.inject_doc_id
          and tgt.rule          = src.rule)
      when MATCHED then
         update
            set tgt.fixable = src.fixable_default;

      O_updated_doc_count := O_updated_doc_count + 1;

      select count(*)
        into L_count
        from im_inject_doc_error de
       where de.inject_doc_id = rec.inject_doc_id
         and de.trial_id      = rec.trial_id;

      LOGGER.LOG_INFORMATION(L_program||' validate loop with inject_doc_id:'||rec.inject_doc_id||' error_count:'||L_count);

      --push to ops table, update status on inject table if no validation errors
      if L_count = 0 then

         if REIM_INJECTOR_SQL.INJECT_VALID_TRANSACTIONS(O_error_message,
                                                        NULL, --I_inject_id
                                                        rec.inject_doc_id,
                                                        L_user) = REIM_CONSTANTS.FAIL then
            return REIM_CONSTANTS.FAIL;
         end if;

         if UPDATE_SUCCESSFUL_DOC(O_error_message,
                                  I_workspace_id,
                                  rec.inject_doc_id) =  REIM_CONSTANTS.FAIL then
            return REIM_CONSTANTS.FAIL;
         end if;

      end if;

   end loop;

   --refresh workspace --is: only do if docs were actually updated?
   if REFRESH_WORKSPACE(O_error_message,
                        I_workspace_id) =  REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UPDATE_ITEM;
----------------------------------------------------------------
FUNCTION SEARCH(O_error_message    OUT VARCHAR2,
                O_workspace_id         OUT IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                O_max_rows_exceeded    OUT NUMBER,
                I_search_criteria   IN OUT IM_EDI_SEARCH_CRITERIA_REC)
RETURN NUMBER IS

   L_program      VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.SEARCH';
   L_start_time   TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');

   O_workspace_id := im_workspace_id_seq.nextval;

   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_DOC_SEARCH(O_error_message,
                        I_search_criteria,
                        O_workspace_id) = FALSE then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END SEARCH;
----------------------------------------------------------------
FUNCTION COMMON_DOC_SEARCH(O_error_message     IN OUT VARCHAR2,
                           I_search_criteria   IN OUT IM_EDI_SEARCH_CRITERIA_REC,
                           I_workspace_id      IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_EDI_MAINT_SQL.COMMON_DOC_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

   L_insert VARCHAR2(2000)  :=
      q'[insert into im_edi_doc_search_ws (workspace_id,
                                           inject_id,
                                           inject_doc_id,
                                           inject_source,
                                           ext_doc_id,
                                           group_id,
                                           doc_type,
                                           status,
                                           doc_date,
                                           due_date,
                                           rule,
                                           error_context,
                                           location,
                                           loc_type,
                                           loc_name,
                                           order_no,
                                           vendor_type,
                                           vendor,
                                           vendor_name,
                                           supplier_site_id,
                                           supplier_site_name,
                                           currency_code,
                                           total_cost,
                                           total_qty,
                                           inject_doc_head_version_id,
                                           qbe_filter_ind,
                                           choice_flag)
                                        ]';

   L_query VARCHAR2(3000)   :=
      q'[select i.workspace_id,
                i.inject_id,
                i.inject_doc_id,
                i.inject_source,
                i.ext_doc_id,
                i.group_id,
                i.doc_type,
                i.status,
                i.doc_date,
                i.due_date,
                i.rule,
                i.error_context,
                i.location,
                i.loc_type,
                i.loc_name,
                i.order_no,
                i.vendor_type,
                i.vendor,
                i.vendor_name,
                i.supplier_site_id,
                i.supplier_site_name,
                i.currency_code,
                i.total_cost,
                i.total_qty,
                i.inject_doc_head_version_id,
                i.qbe_filter_ind,
                i.choice_flag
           from (select :16 workspace_id,
                        idh.inject_id,
                        idh.inject_doc_id,
                        ins.inject_source,
                        idh.ext_doc_id,
                        idh.group_id,
                        idh.doc_type,
                        idh.status,
                        idh.doc_date,
                        idh.due_date,
                        ide.rule,
                        ide.error_context,
                        idh.location,
                        idh.loc_type,
                        vl.loc_name,
                        idh.order_no,
                        --
                        idh.vendor_type,
                        idh.ref_vendor vendor,
                        vn1.vendor_name vendor_name,
                        --
                        idh.vendor supplier_site_id,
                        vn2.vendor_name supplier_site_name,
                        --
                        idh.currency_code,
                        idh.total_cost,
                        idh.total_qty,
                        idh.object_version_id inject_doc_head_version_id,
                        'N' qbe_filter_ind,
                        'N' choice_flag,
                        nvl(ide.trial_id,0) trial_id,
                        max(nvl(ide.trial_id,0)) over (partition by idh.inject_doc_id) max_trial_id]';

   L_from VARCHAR2(5000)  :=
      q'[ from
               (select s.store loc, s.store_name loc_name
                  from v_store s
                union all
                select w.wh loc, w.wh_name loc_name
                  from v_wh w
               ) vl,
               (select p.partner_id vendor, p.partner_desc vendor_name,
                       p.partner_type vendor_type
                  from partner p
                union all
                select to_char(s.supplier) vendor, s.sup_name vendor_name,
                       'SUPP' vendor_type
                  from sups s
               ) vn1,
               (select p.partner_id vendor, p.partner_desc vendor_name,
                       p.partner_type vendor_type
                  from partner p
                union all
                select to_char(s.supplier) vendor, s.sup_name vendor_name,
                       'SUPP' vendor_type
                  from sups s
               ) vn2,
               im_inject_doc_head idh,
               im_inject_status ins,
               im_inject_doc_error ide
        ]';

   L_where VARCHAR2(5000)  := null;

BEGIN

   delete from im_doc_search_gtt;

   if SETUP_DOC_FROM(O_error_message,
                     L_from,
                     I_search_criteria) = FALSE then
      return FALSE;
   end if;

   if SETUP_DOC_WHERE(O_error_message,
                      L_where,
                      I_search_criteria) = FALSE then
      return FALSE;
   end if;

   EXECUTE IMMEDIATE L_insert||LP_doc_with||L_query||L_from||L_where||q'[) i where i.trial_id = i.max_trial_id]'
      using I_search_criteria.start_document_date,
            I_search_criteria.end_document_date,
            I_search_criteria.loc_type,
            I_search_criteria.inject_source,
            I_search_criteria.rule,
            I_search_criteria.doc_type,
            I_search_criteria.ext_doc_id,
            I_search_criteria.vendor_type,
            I_search_criteria.vendor,
            I_search_criteria.vendor_name,
            I_search_criteria.supplier_site_id,
            I_search_criteria.supplier_site_name,
            I_search_criteria.loc,
            I_search_criteria.loc_name,
            I_search_criteria.order_no,
            I_search_criteria.ap_reviewer,
            I_search_criteria.group_id,
            I_workspace_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert im_edi_doc_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --filter ap reviewer
   if (I_search_criteria.ap_reviewer is not null and I_search_criteria.ap_reviewer.count >= 1) then

      delete from im_edi_doc_search_ws ws
       where ws.workspace_id = I_workspace_id
         and not exists (select 'x'
                           from v_im_supp_site_attrib_expl so,
                                table(cast(I_search_criteria.ap_reviewer as OBJ_VARCHAR_ID_TABLE)) in_ap_reviewers
                          where ws.vendor_type        = 'SUPP'
                            and ws.supplier_site_id   = so.supplier
                            and lower(so.ap_reviewer) like('%'||lower(value(in_ap_reviewers))||'%'));

      LOGGER.LOG_INFORMATION(L_program||' filter ap_reviewer im_edi_doc_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COMMON_DOC_SEARCH;
----------------------------------------------------------------
FUNCTION SETUP_DOC_FROM(O_error_message     IN OUT VARCHAR2,
                        IO_from             IN OUT VARCHAR2,
                        I_search_criteria   IN     IM_EDI_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_EDI_MAINT_SQL.SETUP_DOC_FROM';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   IO_from := IO_from || ',sc_scalars';

   if (I_search_criteria.inject_source is not null and I_search_criteria.inject_source.count >= 1) then
      IO_from := IO_from ||',sc_inject_sources';
   end if;
   if (I_search_criteria.rule is not null and I_search_criteria.rule.count >= 1) then
      IO_from := IO_from ||',sc_rules';
   end if;
   if (I_search_criteria.doc_type is not null and I_search_criteria.doc_type.count >= 1) then
      IO_from := IO_from ||',sc_doc_types';
   end if;
   if (I_search_criteria.ext_doc_id is not null and I_search_criteria.ext_doc_id.count >= 1) then
      IO_from := IO_from ||',sc_ext_doc_ids';
   end if;
   if (I_search_criteria.vendor_type is not null and I_search_criteria.vendor_type.count >= 1) then
      IO_from := IO_from ||',sc_vendor_types';
   end if;
   if (I_search_criteria.vendor is not null and I_search_criteria.vendor.count >= 1) then
      IO_from := IO_from ||',sc_vendors';
   end if;
   if (I_search_criteria.vendor_name is not null and I_search_criteria.vendor_name.count >= 1) then
      IO_from := IO_from ||',sc_vendor_names';
   end if;
   if (I_search_criteria.supplier_site_id is not null and I_search_criteria.supplier_site_id.count >= 1) then
      IO_from := IO_from ||',sc_supplier_sites';
   end if;
   if (I_search_criteria.supplier_site_name is not null and I_search_criteria.supplier_site_name.count >= 1) then
      IO_from := IO_from ||',sc_sup_site_names';
   end if;
   if (I_search_criteria.loc is not null and I_search_criteria.loc.count >= 1) then
      IO_from := IO_from ||',sc_locs';
   end if;
   if (I_search_criteria.loc_name is not null and I_search_criteria.loc_name.count >= 1) then
      IO_from := IO_from ||',sc_loc_names';
   end if;
   if (I_search_criteria.order_no is not null and I_search_criteria.order_no.count >= 1) then
      IO_from := IO_from ||',sc_order_nos';
   end if;
   if (I_search_criteria.group_id is not null and I_search_criteria.group_id.count >= 1) then
      IO_from := IO_from ||',sc_group_ids';
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SETUP_DOC_FROM;
----------------------------------------------------------------
FUNCTION SETUP_DOC_WHERE(O_error_message     IN OUT VARCHAR2,
                         IO_where            IN OUT VARCHAR2,
                         I_search_criteria   IN     IM_EDI_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_EDI_MAINT_SQL.SETUP_DOC_WHERE';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;
   L_join_cnd           VARCHAR2(8)    := null;

BEGIN

   L_join_cnd := ' AND ';
   IO_where   := q'[ where idh.workspace_type   = 'EDI_INJECT' --REIM_CONSTANTS.INJECT_WORKSPACE_EDI_INJECT
                       and idh.location         = vl.loc(+)
                       and idh.vendor           = vn2.vendor(+)
                       and 'SUPP'               = vn2.vendor_type(+)
                       and idh.ref_vendor       = vn1.vendor(+)
                       and idh.vendor_type      = vn1.vendor_type(+)
                       and idh.inject_doc_id    = ide.inject_doc_id
                       and ide.status           = 'N'
                       and ide.fixable          = 'Y'
                       and idh.inject_id        = ins.inject_id(+)
                   ]';

   if I_search_criteria.start_document_date is not null or I_search_criteria.end_document_date is not null then
      IO_where := IO_where||L_join_cnd||'idh.doc_date between '
         ||'nvl(sc_scalars.start_document_date,idh.doc_date) and '
         ||'nvl(sc_scalars.end_document_date,idh.doc_date)';
   end if;
   if I_search_criteria.loc_type is not null then
      IO_where := IO_where||L_join_cnd||'idh.loc_type = sc_scalars.loc_type';
   end if;
   if (I_search_criteria.inject_source is not null and I_search_criteria.inject_source.count >= 1) then
      IO_where := IO_where||L_join_cnd||'ins.inject_source = sc_inject_sources.inject_source';
   end if;
   if (I_search_criteria.rule is not null and I_search_criteria.rule.count >= 1) then
      IO_where := IO_where||L_join_cnd||'ide.rule = sc_rules.rule';
   end if;
   if (I_search_criteria.doc_type is not null and I_search_criteria.doc_type.count >= 1) then
      IO_where := IO_where||L_join_cnd||'idh.doc_type = sc_doc_types.doc_type';
   end if;
   if (I_search_criteria.ext_doc_id is not null and I_search_criteria.ext_doc_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(idh.ext_doc_id) like('%'||lower(nvl(sc_ext_doc_ids.ext_doc_id,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.vendor_type is not null and I_search_criteria.vendor_type.count >= 1) then
      IO_where := IO_where||L_join_cnd||'idh.vendor_type = sc_vendor_types.vendor_type';
   end if;
   if (I_search_criteria.vendor is not null and I_search_criteria.vendor.count >= 1) then
      IO_where := IO_where||L_join_cnd||'idh.ref_vendor = sc_vendors.vendor';
   end if;
   if (I_search_criteria.vendor_name is not null and I_search_criteria.vendor_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vn2.vendor_name) like('%'||lower(nvl(sc_vendor_names.vendor_name,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.supplier_site_id is not null and I_search_criteria.supplier_site_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'idh.vendor = sc_supplier_sites.supplier_site';
   end if;
   if (I_search_criteria.supplier_site_name is not null and I_search_criteria.supplier_site_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vn1.vendor_name) like('%'||lower(nvl(sc_sup_site_names.sup_site_name,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.loc is not null and I_search_criteria.loc.count >= 1) then
      IO_where := IO_where||L_join_cnd||'idh.location = sc_locs.loc';
   end if;
   if (I_search_criteria.loc_name is not null and I_search_criteria.loc_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vl.loc_name) like('%'||lower(nvl(sc_loc_names.loc_name,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.order_no is not null and I_search_criteria.order_no.count >= 1) then
      IO_where := IO_where||L_join_cnd||'idh.order_no = sc_order_nos.order_no';
   end if;
   if (I_search_criteria.group_id is not null and I_search_criteria.group_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'idh.group_id = sc_group_ids.group_id';
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SETUP_DOC_WHERE;
----------------------------------------------------------------
FUNCTION REFRESH_WORKSPACE(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program    VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.REFRESH_WORKSPACE';
   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete
     from im_edi_doc_search_ws iedsw
    where iedsw.workspace_id = I_workspace_id
      and NOT EXISTS (select 'x'
                        from im_inject_doc_error iide
                       where iide.inject_doc_id = iedsw.inject_doc_id
                         and iide.status        = REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW
                         and iide.fixable       = 'Y');

   LOGGER.LOG_INFORMATION(L_program||' Delete Fixed or Rejected documents from Search Workspace - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REFRESH_WORKSPACE;
----------------------------------------------------------------
FUNCTION DIRTY_LOCK_CHECKS(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                           I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.DIRTY_LOCK_CHECKS';
   L_start_time  TIMESTAMP := SYSTIMESTAMP;

   L_table_name         VARCHAR2(30) := NULL;
   L_dirty_object       VARCHAR2(30) := NULL;
   L_dirty_rec_count    NUMBER       := REIM_CONSTANTS.ZERO;

   dirty_records  EXCEPTION;
   records_locked EXCEPTION;
   PRAGMA         EXCEPTION_INIT(records_locked, -54);

   cursor C_CHK_INJ_DOC_HEAD is
      select count(1)
        from im_edi_doc_search_ws ds,
             im_inject_doc_head idh
       where ds.workspace_id                = I_workspace_id
         and (   (    I_inject_doc_id is NOT NULL
                  and ds.inject_doc_id = I_inject_doc_id)
              or (    ds.choice_flag    = 'Y'
                  and ds.qbe_filter_ind = 'N'))
         and ds.inject_doc_id  = idh.inject_doc_id
         and ds.inject_doc_head_version_id  <> idh.object_version_id;

   cursor C_LOCK_INJ_DOC_HEAD is
      select 'x'
        from im_inject_doc_head idh
       where EXISTS (select 'x'
                       from im_edi_doc_search_ws ds
                      where ds.workspace_id   = I_workspace_id
                        and (   (    I_inject_doc_id is NOT NULL
                                 and ds.inject_doc_id = I_inject_doc_id)
                             or (    ds.choice_flag    = 'Y'
                                 and ds.qbe_filter_ind = 'N'))
                        and ds.inject_doc_id  = idh.inject_doc_id)
         FOR UPDATE NOWAIT;

BEGIN

   L_dirty_object := 'INJECT_DOC_ID';
   open C_CHK_INJ_DOC_HEAD;
   fetch C_CHK_INJ_DOC_HEAD into L_dirty_rec_count;
   close C_CHK_INJ_DOC_HEAD;
   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;

   L_table_name := 'IM_INJECT_DOC_HEAD';
   open C_LOCK_INJ_DOC_HEAD;
   close C_LOCK_INJ_DOC_HEAD;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when records_locked then
      O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED', --constant
                                            L_table_name,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when dirty_records then
      O_error_message := SQL_LIB.CREATE_MSG('DIRTY_RECORDS', --constant
                                            L_dirty_object,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DIRTY_LOCK_CHECKS;
----------------------------------------------------------------
FUNCTION LOG_SEARCH_CRITERIA(O_error_message     IN OUT VARCHAR2,
                             I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                             I_search_criteria   IN     IM_EDI_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS
   L_program            VARCHAR2(61) := 'REIM_EDI_MAINT_SQL.LOG_SEARCH_CRITERIA';
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--START_DOCUMENT_DATE:'||I_search_criteria.START_DOCUMENT_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--END_DOCUMENT_DATE:'||I_search_criteria.END_DOCUMENT_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--LOC_TYPE:'||I_search_criteria.LOC_TYPE);
   --
   if I_search_criteria.INJECT_SOURCE is not null and I_search_criteria.INJECT_SOURCE.COUNT > 0 then
   for i in 1..I_search_criteria.INJECT_SOURCE.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--INJECT_SOURCE('||i||'):'||I_search_criteria.INJECT_SOURCE(i));
   end loop; end if;
   --
   if I_search_criteria.RULE is not null and I_search_criteria.RULE.COUNT > 0 then
   for i in 1..I_search_criteria.RULE.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--RULE('||i||'):'||I_search_criteria.RULE(i));
   end loop; end if;
   --
   if I_search_criteria.DOC_TYPE is not null and I_search_criteria.DOC_TYPE.COUNT > 0 then
   for i in 1..I_search_criteria.DOC_TYPE.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--DOC_TYPE('||i||'):'||I_search_criteria.DOC_TYPE(i));
   end loop; end if;
   --
   if I_search_criteria.EXT_DOC_ID is not null and I_search_criteria.EXT_DOC_ID.COUNT > 0 then
   for i in 1..I_search_criteria.EXT_DOC_ID.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--EXT_DOC_ID('||i||'):'||I_search_criteria.EXT_DOC_ID(i));
   end loop; end if;
   --
   if I_search_criteria.VENDOR_TYPE is not null and I_search_criteria.VENDOR_TYPE.COUNT > 0 then
   for i in 1..I_search_criteria.VENDOR_TYPE.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--VENDOR_TYPE('||i||'):'||I_search_criteria.VENDOR_TYPE(i));
   end loop; end if;
   --
   if I_search_criteria.VENDOR is not null and I_search_criteria.VENDOR.COUNT > 0 then
   for i in 1..I_search_criteria.VENDOR.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--VENDOR('||i||'):'||I_search_criteria.VENDOR(i));
   end loop; end if;
   --
   if I_search_criteria.VENDOR_NAME is not null and I_search_criteria.VENDOR_NAME.COUNT > 0 then
   for i in 1..I_search_criteria.VENDOR_NAME.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--VENDOR_NAME('||i||'):'||I_search_criteria.VENDOR_NAME(i));
   end loop; end if;
   --
   if I_search_criteria.SUPPLIER_SITE_ID is not null and I_search_criteria.SUPPLIER_SITE_ID.COUNT > 0 then
   for i in 1..I_search_criteria.SUPPLIER_SITE_ID.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--SUPPLIER_SITE_ID('||i||'):'||I_search_criteria.SUPPLIER_SITE_ID(i));
   end loop; end if;
   --
   if I_search_criteria.SUPPLIER_SITE_NAME is not null and I_search_criteria.SUPPLIER_SITE_NAME.COUNT > 0 then
   for i in 1..I_search_criteria.SUPPLIER_SITE_NAME.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--SUPPLIER_SITE_NAME('||i||'):'||I_search_criteria.SUPPLIER_SITE_NAME(i));
   end loop; end if;
   --
   if I_search_criteria.LOC is not null and I_search_criteria.LOC.COUNT > 0 then
   for i in 1..I_search_criteria.LOC.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--LOC('||i||'):'||I_search_criteria.LOC(i));
   end loop; end if;
   --
   if I_search_criteria.LOC_NAME is not null and I_search_criteria.LOC_NAME.COUNT > 0 then
   for i in 1..I_search_criteria.LOC_NAME.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--LOC_NAME('||i||'):'||I_search_criteria.LOC_NAME(i));
   end loop; end if;
   --
   if I_search_criteria.ORDER_NO is not null and I_search_criteria.ORDER_NO.COUNT > 0 then
   for i in 1..I_search_criteria.ORDER_NO.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--ORDER_NO('||i||'):'||I_search_criteria.ORDER_NO(i));
   end loop; end if;
   --
   if I_search_criteria.AP_REVIEWER is not null and I_search_criteria.AP_REVIEWER.COUNT > 0 then
   for i in 1..I_search_criteria.AP_REVIEWER.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--AP_REVIEWER('||i||'):'||I_search_criteria.AP_REVIEWER(i));
   end loop; end if;
   --
   if I_search_criteria.GROUP_ID is not null and I_search_criteria.GROUP_ID.COUNT > 0 then
   for i in 1..I_search_criteria.GROUP_ID.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--GROUP_ID('||i||'):'||I_search_criteria.GROUP_ID(i));
   end loop; end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOG_SEARCH_CRITERIA;
----------------------------------------------------------------
-- Name:    VALIDATE_DOCUMENT
-- Purpose: This function performs validations on an inject document.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
----------------------------------------------------------------
FUNCTION VALIDATE_DOCUMENT(O_error_message IN OUT VARCHAR2,
                           I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                           I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                           I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.VALIDATE_DOCUMENT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   cursor C_FETCH_ORIG_INJECT_DOC_ID is
      select iidh.src_doc_id
        from im_inject_doc_head iidh
       where iidh.inject_doc_id = I_inject_doc_id;

   L_orig_inject_doc_id IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE := NULL;

BEGIN

   open C_FETCH_ORIG_INJECT_DOC_ID;
   fetch C_FETCH_ORIG_INJECT_DOC_ID into L_orig_inject_doc_id;
   close C_FETCH_ORIG_INJECT_DOC_ID;

   update im_inject_doc_error iide
      set iide.status = REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_FIXED
    where iide.inject_doc_id = I_inject_doc_id;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id,
                        L_orig_inject_doc_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   merge into im_inject_doc_error tgt
   using (select iidr.rule,
                 iidr.fixable_default
            from im_inject_doc_rule iidr) src
   on (    tgt.inject_doc_id = I_inject_doc_id
       and tgt.rule          = src.rule)
   when MATCHED then
      update
         set tgt.fixable = src.fixable_default;

   if REIM_DOCUMENT_SQL.VALIDATE_DOCUMENT(O_error_message,
                                          I_inject_doc_id,
                                          I_trial_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   merge into im_inject_doc_error tgt
   using (select iidr.rule,
                 iidr.fixable_default
            from im_inject_doc_rule iidr) src
   on (    tgt.inject_doc_id = I_inject_doc_id
       and tgt.rule          = src.rule)
   when MATCHED then
      update
         set tgt.fixable = src.fixable_default;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END VALIDATE_DOCUMENT;
----------------------------------------------------------------
-- Name:    SAVE_DOCUMENT
-- Purpose: This function saves Inject Document to Operational tables, marks the original inject document's errors to delete status.
----------------------------------------------------------------
FUNCTION SAVE_DOCUMENT(O_error_message IN OUT VARCHAR2,
                       I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                       I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.SAVE_DOCUMENT';

   L_start_time TIMESTAMP                 := SYSTIMESTAMP;
   L_user       IM_DOC_HEAD.MATCH_ID%TYPE := get_user;

   cursor C_FETCH_ORIG_INJECT_DOC_ID is
      select iidh.src_doc_id
        from im_inject_doc_head iidh
       where iidh.inject_doc_id = I_inject_doc_id;

   L_orig_inject_doc_id IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE := NULL;

BEGIN

   open C_FETCH_ORIG_INJECT_DOC_ID;
   fetch C_FETCH_ORIG_INJECT_DOC_ID into L_orig_inject_doc_id;
   close C_FETCH_ORIG_INJECT_DOC_ID;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id,
                        L_orig_inject_doc_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REIM_INJECTOR_SQL.INJECT_VALID_TRANSACTIONS(O_error_message,
                                                  NULL, --I_inject_id
                                                  I_inject_doc_id,
                                                  L_user) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if UPDATE_SUCCESSFUL_DOC(O_error_message,
                            I_workspace_id,
                            L_orig_inject_doc_id) =  REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REFRESH_WORKSPACE(O_error_message,
                        I_workspace_id) =  REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END SAVE_DOCUMENT;
----------------------------------------------------------------
FUNCTION UPDATE_SUCCESSFUL_DOC(O_error_message    OUT VARCHAR2,
                               I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                               I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.UPDATE_SUCCESSFUL_DOC';

   L_start_time TIMESTAMP := SYSTIMESTAMP;
   L_user       IM_DOC_HEAD.MATCH_ID%TYPE := get_user;

BEGIN

   update im_inject_doc_head dh
      set dh.last_updated_by   = L_user,
          dh.last_update_date  = sysdate,
          dh.object_version_id = dh.object_version_id + REIM_CONSTANTS.ONE
    where dh.inject_doc_id = I_inject_doc_id;

   update im_inject_doc_error de
      set de.status            = REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_FIXED,
          de.last_updated_by   = L_user,
          de.last_update_date  = sysdate,
          de.object_version_id = de.object_version_id + REIM_CONSTANTS.ONE
    where de.inject_doc_id = I_inject_doc_id;

   delete from im_edi_doc_search_ws ws
    where ws.workspace_id  = I_workspace_id
      and ws.inject_doc_id = I_inject_doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Update im_edi_doc_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UPDATE_SUCCESSFUL_DOC;
----------------------------------------------------------------
-- Name:    REJECT_DOCUMENT
-- Purpose: This function updates the Inject document's status such that the Injector batch picks them up for sending back to the vendor.
----------------------------------------------------------------
FUNCTION REJECT_DOCUMENT(O_error_message    OUT VARCHAR2,
                         I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                         I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.REJECT_DOCUMENT';
   L_start_time   TIMESTAMP := SYSTIMESTAMP;

   cursor C_FETCH_ORIG_INJECT_DOC_ID is
      select iidh.src_doc_id
        from im_inject_doc_head iidh
       where iidh.inject_doc_id = I_inject_doc_id;

   L_orig_inject_doc_id IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE := NULL;

BEGIN

   open C_FETCH_ORIG_INJECT_DOC_ID;
   fetch C_FETCH_ORIG_INJECT_DOC_ID into L_orig_inject_doc_id;
   close C_FETCH_ORIG_INJECT_DOC_ID;

   if MARK_FOR_DELETE(O_error_message,
                      I_workspace_id,
                      L_orig_inject_doc_id) =  REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REJECT_DOCUMENT;
----------------------------------------------------------------
-- Name:    LOAD_DOCUMENT
-- Purpose: This function creates a copy of the inject document for users to edit online.
----------------------------------------------------------------
FUNCTION LOAD_DOCUMENT(O_error_message IN OUT VARCHAR2,
                       O_inject_id        OUT IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                       O_inject_doc_id    OUT IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                       I_workspace_id  IN     IM_EDI_DOC_SEARCH_WS.WORKSPACE_ID%TYPE,
                       I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER IS
   L_program      VARCHAR2(50) := 'REIM_EDI_MAINT_SQL.LOAD_DOCUMENT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;
   L_user       IM_DOC_HEAD.MATCH_ID%TYPE := get_user;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          'I_workspace_id: ' || I_workspace_id ||
                          'I_inject_doc_id: ' || I_inject_doc_id);

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id,
                        I_inject_doc_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   O_inject_doc_id := im_inject_doc_seq.NEXTVAL;
   O_inject_id     := im_inject_seq.NEXTVAL;

   insert into im_inject_doc_head(inject_id,
                                  transaction_id,
                                  inject_doc_id,
                                  ext_doc_id,
                                  doc_type,
                                  order_no,
                                  rtv_order_no,
                                  location,
                                  loc_type,
                                  vendor,
                                  vendor_type,
                                  ref_vendor,
                                  currency_code,
                                  total_cost,
                                  total_qty,
                                  total_discount,
                                  doc_date,
                                  due_date,
                                  terms,
                                  total_tax_amount,
                                  rtv_ind,
                                  manually_paid_ind,
                                  exchange_rate,
                                  deal_id,
                                  deal_approval_ind,
                                  freight_type,
                                  merch_type,
                                  payment_method,
                                  custom_doc_ref1,
                                  custom_doc_ref2,
                                  custom_doc_ref3,
                                  custom_doc_ref4,
                                  cross_ref_doc,
                                  group_id,
                                  multi_loc_ind,
                                  reverse_vat_ind,
                                  thread_id,
                                  object_version_id,
                                  status,
                                  terms_dscnt_pct,
                                  approval_id,
                                  approval_date,
                                  pre_paid_ind,
                                  pre_paid_id,
                                  post_date,
                                  ref_auth_no,
                                  deal_type,
                                  hold_status,
                                  total_cost_inc_tax,
                                  header_only,
                                  src_doc_id,
                                  ref_object_version_id,
                                  last_updated_by,
                                  last_update_date,
                                  created_by,
                                  creation_date,
                                  doc_source,
                                  best_terms_source,
                                  best_terms,
                                  best_terms_date,
                                  match_date,
                                  workspace_type,
                                  deal_detail_id,
                                  ref_cnr_ext_doc_id,
                                  ref_inv_ext_doc_id)
                           select O_inject_id,
                                  REIM_CONSTANTS.ONE transaction_id,
                                  O_inject_doc_id,
                                  iidh.ext_doc_id,
                                  iidh.doc_type,
                                  iidh.order_no,
                                  iidh.rtv_order_no,
                                  iidh.location,
                                  iidh.loc_type,
                                  iidh.vendor,
                                  iidh.vendor_type,
                                  iidh.ref_vendor,
                                  iidh.currency_code,
                                  iidh.total_cost,
                                  iidh.total_qty,
                                  iidh.total_discount,
                                  iidh.doc_date,
                                  iidh.due_date,
                                  iidh.terms,
                                  iidh.total_tax_amount,
                                  iidh.rtv_ind,
                                  iidh.manually_paid_ind,
                                  iidh.exchange_rate,
                                  iidh.deal_id,
                                  iidh.deal_approval_ind,
                                  iidh.freight_type,
                                  iidh.merch_type,
                                  iidh.payment_method,
                                  iidh.custom_doc_ref1,
                                  iidh.custom_doc_ref2,
                                  iidh.custom_doc_ref3,
                                  iidh.custom_doc_ref4,
                                  iidh.cross_ref_doc,
                                  iidh.group_id,
                                  iidh.multi_loc_ind,
                                  iidh.reverse_vat_ind,
                                  REIM_CONSTANTS.ONE thread_id,
                                  REIM_CONSTANTS.ONE object_version_id,
                                  iidh.status,
                                  iidh.terms_dscnt_pct,
                                  iidh.approval_id,
                                  iidh.approval_date,
                                  iidh.pre_paid_ind,
                                  iidh.pre_paid_id,
                                  iidh.post_date,
                                  iidh.ref_auth_no,
                                  iidh.deal_type,
                                  iidh.hold_status,
                                  iidh.total_cost + NVL(iidh.total_tax_amount, REIM_CONSTANTS.ZERO) total_cost_inc_tax,
                                  iidh.header_only,
                                  iidh.inject_doc_id src_doc_id,
                                  iidh.object_version_id ref_object_version_id,
                                  L_user last_updated_by,
                                  sysdate last_update_date,
                                  L_user created_by,
                                  sysdate creation_date,
                                  iidh.doc_source,
                                  iidh.best_terms_source,
                                  iidh.best_terms,
                                  iidh.best_terms_date,
                                  iidh.match_date,
                                  REIM_CONSTANTS.INJECT_WORKSPACE_INJECT_COPY,
                                  iidh.deal_detail_id,
                                  iidh.ref_cnr_ext_doc_id,
                                  iidh.ref_inv_ext_doc_id
                             from im_inject_doc_head iidh
                            where iidh.inject_doc_id = I_inject_doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_HEAD - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_doc_tax(inject_id,
                                 transaction_id,
                                 inject_doc_id,
                                 tax_code,
                                 tax_rate,
                                 tax_basis,
                                 tax_amount,
                                 ref_object_version_id,
                                 object_version_id,
                                 last_updated_by,
                                 last_update_date,
                                 created_by,
                                 creation_date)
                          select O_inject_id,
                                 REIM_CONSTANTS.ONE transaction_id,
                                 O_inject_doc_id,
                                 iidt.tax_code,
                                 iidt.tax_rate,
                                 iidt.tax_basis,
                                 round((iidt.tax_basis * (iidt.tax_rate/100)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) tax_amount,
                                 iidt.object_version_id ref_object_version_id,
                                 REIM_CONSTANTS.ONE object_version_id,
                                 L_user last_updated_by,
                                 sysdate last_update_date,
                                 L_user created_by,
                                 sysdate creation_date
                            from im_inject_doc_tax iidt
                           where iidt.inject_doc_id = I_inject_doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_TAX - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_doc_non_merch(inject_id,
                                       transaction_id,
                                       inject_doc_id,
                                       non_merch_code,
                                       non_merch_amount,
                                       service_performed,
                                       store,
                                       object_version_id,
                                       ref_object_version_id,
                                       last_updated_by,
                                       last_update_date,
                                       created_by,
                                       creation_date)
                                select O_inject_id,
                                       REIM_CONSTANTS.ONE transaction_id,
                                       O_inject_doc_id,
                                       iidnm.non_merch_code,
                                       iidnm.non_merch_amount,
                                       iidnm.service_performed,
                                       iidnm.store,
                                       REIM_CONSTANTS.ONE object_version_id,
                                       iidnm.object_version_id ref_object_version_id,
                                       L_user last_updated_by,
                                       sysdate last_update_date,
                                       L_user created_by,
                                       sysdate creation_date
                                  from im_inject_doc_non_merch iidnm
                                 where iidnm.inject_doc_id = I_inject_doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_NON_MERCH - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_doc_non_merch_tax(inject_doc_id,
                                           inject_id,
                                           non_merch_code,
                                           tax_code,
                                           tax_rate,
                                           tax_basis,
                                           object_version_id,
                                           tax_amount,
                                           ref_object_version_id,
                                           last_updated_by,
                                           last_update_date,
                                           created_by,
                                           creation_date)
                                    select O_inject_doc_id,
                                           O_inject_id,
                                           iidnmt.non_merch_code,
                                           iidnmt.tax_code,
                                           iidnmt.tax_rate,
                                           iidnmt.tax_basis,
                                           REIM_CONSTANTS.ONE object_version_id,
                                           round((iidnmt.tax_basis * (iidnmt.tax_rate/100)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) tax_amount,
                                           iidnmt.object_version_id ref_object_version_id,
                                           L_user last_updated_by,
                                           sysdate last_update_date,
                                           L_user created_by,
                                           sysdate creation_date
                                      from im_inject_doc_non_merch_tax iidnmt
                                     where iidnmt.inject_doc_id = I_inject_doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_NON_MERCH_TAX - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date(number_1, --orig_detl_id
                                             number_2) --new_detl_id
                                      select iidd.detail_id,
                                             im_inject_doc_detail_seq.NEXTVAL
                                        from im_inject_doc_detail iidd
                                       where iidd.inject_doc_id = I_inject_doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_doc_detail(inject_id,
                                    transaction_id,
                                    inject_doc_id,
                                    detail_id,
                                    item,
                                    vpn,
                                    upc,
                                    item_source,
                                    upc_supplement,
                                    qty,
                                    unit_cost,
                                    total_allowance,
                                    object_version_id,
                                    status,
                                    tax_discrepancy_ind,
                                    reason_code_id,
                                    ref_object_version_id,
                                    last_updated_by,
                                    last_update_date,
                                    created_by,
                                    creation_date)
                             select O_inject_id,
                                    REIM_CONSTANTS.ONE transaction_id,
                                    O_inject_doc_id,
                                    gtt.number_2, --new_detl_id
                                    iidd.item,
                                    iidd.vpn,
                                    iidd.upc,
                                    iidd.item_source,
                                    iidd.upc_supplement,
                                    iidd.qty,
                                    iidd.unit_cost,
                                    iidd.total_allowance,
                                    REIM_CONSTANTS.ONE object_version_id,
                                    iidd.status,
                                    iidd.tax_discrepancy_ind,
                                    iidd.reason_code_id,
                                    iidd.object_version_id ref_object_version_id,
                                    L_user last_updated_by,
                                    sysdate last_update_date,
                                    L_user created_by,
                                    sysdate creation_date
                               from gtt_num_num_str_str_date_date gtt,
                                    im_inject_doc_detail iidd
                              where iidd.detail_id = gtt.number_1;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_DETAIL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_doc_detail_tax(detail_id,
                                        tax_code,
                                        tax_rate,
                                        tax_basis,
                                        reverse_vat_ind,
                                        object_version_id,
                                        tax_amount,
                                        ref_object_version_id,
                                        last_updated_by,
                                        last_update_date,
                                        created_by,
                                        creation_date)
                                 select gtt.number_2,
                                        iiddt.tax_code,
                                        iiddt.tax_rate,
                                        iiddt.tax_basis,
                                        iiddt.reverse_vat_ind,
                                        REIM_CONSTANTS.ONE object_version_id,
                                        iiddt.tax_amount,
                                        iiddt.object_version_id ref_object_version_id,
                                        L_user last_updated_by,
                                        sysdate last_update_date,
                                        L_user created_by,
                                        sysdate creation_date
                                   from gtt_num_num_str_str_date_date gtt,
                                        im_inject_doc_detail_tax iiddt
                                  where iiddt.detail_id = gtt.number_1;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_DETAIL_TAX - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_doc_detail_allowance(detail_id,
                                              allowance_code,
                                              allowance_amount,
                                              last_updated_by,
                                              last_update_date,
                                              created_by,
                                              creation_date,
                                              object_version_id)
                                       select gtt.number_2,
                                              iidda.allowance_code,
                                              iidda.allowance_amount,
                                              L_user last_updated_by,
                                              sysdate last_update_date,
                                              L_user created_by,
                                              sysdate creation_date,
                                              REIM_CONSTANTS.ONE object_version_id
                                         from gtt_num_num_str_str_date_date gtt,
                                              im_inject_doc_detail_allowance iidda
                                        where iidda.detail_id = gtt.number_1;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_DETAIL_ALLOWANCE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_doc_detail_allow_tax(detail_id,
                                              allowance_code,
                                              tax_code,
                                              tax_rate,
                                              tax_basis,
                                              last_updated_by,
                                              last_update_date,
                                              created_by,
                                              creation_date,
                                              object_version_id)
                                       select gtt.number_2,
                                              iiddat.allowance_code,
                                              iiddat.tax_code,
                                              iiddat.tax_rate,
                                              iiddat.tax_basis,
                                              L_user last_updated_by,
                                              sysdate last_update_date,
                                              L_user created_by,
                                              sysdate creation_date,
                                              REIM_CONSTANTS.ONE object_version_id
                                         from gtt_num_num_str_str_date_date gtt,
                                              im_inject_doc_detail_allow_tax iiddat
                                        where iiddat.detail_id = gtt.number_1;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_DETAIL_ALLOW_TAX - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_tax_discrepancy(inject_doc_id,
                                         order_no,
                                         item,
                                         tax_code,
                                         doc_tax_rate,
                                         doc_tax_amount,
                                         verify_tax_rate,
                                         verify_tax_code,
                                         verify_tax_amount,
                                         verify_tax_src,
                                         verify_tax_formula,
                                         verify_tax_order,
                                         last_updated_by,
                                         last_update_date,
                                         created_by,
                                         creation_date,
                                         object_version_id)
                                  select O_inject_doc_id,
                                         iitd.order_no,
                                         iitd.item,
                                         iitd.tax_code,
                                         iitd.doc_tax_rate,
                                         iitd.doc_tax_amount,
                                         iitd.verify_tax_rate,
                                         iitd.verify_tax_code,
                                         iitd.verify_tax_amount,
                                         iitd.verify_tax_src,
                                         iitd.verify_tax_formula,
                                         iitd.verify_tax_order,
                                         L_user last_updated_by,
                                         sysdate last_update_date,
                                         L_user created_by,
                                         sysdate creation_date,
                                         REIM_CONSTANTS.ONE object_version_id
                                    from im_inject_tax_discrepancy iitd
                                   where iitd.inject_doc_id = I_inject_doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_TAX_DISCREPANCY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_inject_doc_error(inject_id,
                                   inject_doc_id,
                                   detail_id,
                                   non_merch_code,
                                   rule,
                                   error_context,
                                   fixable,
                                   status,
                                   action_date,
                                   last_updated_by,
                                   last_update_date,
                                   created_by,
                                   creation_date,
                                   object_version_id,
                                   trial_id)
                            select O_inject_id,
                                   O_inject_doc_id,
                                   iide.detail_id,
                                   iide.non_merch_code,
                                   iide.rule,
                                   iide.error_context,
                                   iide.fixable,
                                   iide.status,
                                   iide.action_date,
                                   L_user last_updated_by,
                                   sysdate last_update_date,
                                   L_user created_by,
                                   sysdate creation_date,
                                   REIM_CONSTANTS.ONE object_version_id,
                                   REIM_CONSTANTS.ONE trial_id
                              from im_inject_doc_error iide
                             where iide.inject_doc_id = I_inject_doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_INJECT_DOC_ERROR - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   LOGGER.LOG_INFORMATION('End ' || L_program );
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END LOAD_DOCUMENT;
----------------------------------------------------------------
END REIM_EDI_MAINT_SQL;
/
