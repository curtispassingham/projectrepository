CREATE OR REPLACE PACKAGE BODY REIM_UNDO_MATCH_SQL AS
----------------------------------------------------------------------------------------------

--LP_expand_group_level NUMBER(4) := 3;

----------------------------------------------------------------------------------------------
/**
 *  -------- PRIVATE PROCS AND FUNCTIONS SPECS --------------
 */
----------------------------------------------------------------------------------------------
/**
 * The private procedure used for logging Unmatch status information in IM_UNMTCH_STATUS.
 */
PROCEDURE LOG_UNMTCH_STATUS(O_error_message    OUT VARCHAR2,
                            I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                            I_status        IN     IM_UNMTCH_STATUS.STATUS%TYPE,
                            I_unmtch_lvl    IN     IM_UNMTCH_STATUS.UNMTCH_LVL%TYPE,
                            I_unmtch_doc    IN     IM_UNMTCH_STATUS.DOC_ID%TYPE,
                            I_doc_item_tbl  IN     OBJ_NUM_NUM_STR_TBL);
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Check for dirty records in the Detail Match Item View screen
 *              b. Check for DB locks on OPS tables.
 * Input param: I_workspace_id (The ID of the Detail match workspace)
 */
FUNCTION DIRTY_LOCK_CHECK_DETL(O_error_message    OUT VARCHAR2,
                               I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                               I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Check for dirty records in the match Inquiry screen
 *              b. Check for DB locks on OPS tables.
 * Input param: I_doc_id (The ID of the Invoice that needs to be unmatched)
 */
FUNCTION DIRTY_LOCK_CHECK_DOC(O_error_message    OUT VARCHAR2,
                              I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                              I_doc_id        IN     IM_DOC_HEAD.DOC_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Validate if Unmatch is Possible
 * Input param: I_doc_item_tbl (Table of Document Items)
 *              I_dmatch_match_id_tbl (Table of Detail Match IDs)
 *              I_smatch_match_id_tbl (Table of Summary Match IDs)
 *              I_irip_seq_tbl (Table of Receipt Item Posting Sequences)
 *              I_unmtch_lvl (Doc/Item)
 *              I_unmtch_doc_id
 * 
 * Output param: O_unmtch_comments (List of Validation comments)
 */
FUNCTION VALIDATE_UNMTCH(O_error_message           OUT VARCHAR2,
                         O_unmtch_comments        OUT OBJ_VARCHAR_ID_TABLE,
                         I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                         I_unmtch_lvl          IN     IM_UNMTCH_STATUS.UNMTCH_LVL%TYPE,
                         I_unmtch_doc_id       IN     IM_DOC_HEAD.DOC_ID%TYPE,
                         I_doc_item_tbl        IN     OBJ_NUM_NUM_STR_TBL,
                         I_dmatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE,
                         I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE,
                         I_irip_seq_tbl        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Revert Receiver Cost and Unit Adjustments
 * Input param: I_doc_item_tbl (Table of Document-Item)
 *              I_irip_seq_tbl (Table of Sequence Numbers)
 */
FUNCTION REVERT_RECEIVER_ADJ(O_error_message    OUT VARCHAR2,
                             I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                             I_doc_item_tbl  IN     OBJ_NUM_NUM_STR_TBL,
                             I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Move records from im_resolution_action, im_doc_detail_comments tables to im_unmtch_resln_action and im_unmtch_doc_detail_comnts
 *              b. Updates the status of resolutions (that were performed on invoice-items which are not part of the match which is getting unmatched) to unrolled
 * Input param: I_doc_item_tbl (Table of Document-Item)
 */
FUNCTION REVERT_RESLN_DATA(O_error_message          OUT VARCHAR2,
                           I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                           I_unmtch_lvl          IN     IM_UNMTCH_STATUS.UNMTCH_LVL%TYPE,
                           I_unmtch_doc_id       IN     IM_DOC_HEAD.DOC_ID%TYPE,
                           I_doc_item_tbl        IN     OBJ_NUM_NUM_STR_TBL,
                           I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Move records from im_detail_match_%_history tables to im_unmtch_dmatch_%_history
 * Input param: I_dmatch_match_id_tbl (Table of Detail Match IDs)
 */
FUNCTION REVERT_DMATCH_HISTORY(O_error_message          OUT VARCHAR2,
                               I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                               I_dmatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Move records from im_summary_match_%_history tables to im_unmtch_smatch_%_history
 * Input param: I_smatch_match_id_tbl (Table of Summary Match IDs)
 */
FUNCTION REVERT_SMATCH_HISTORY(O_error_message          OUT VARCHAR2,
                               I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                               I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Move records from im_receipt_item_posting/invc tables to im_unmtch_rcpt_item_posting/invc
 * Input param: I_irip_seq_tbl (Table of Sequence Numbers)
 */
FUNCTION REVERT_RCPT_ITEM_POSTING(O_error_message    OUT VARCHAR2,
                                  I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                                  I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Update im_partially_matched_receipts
 * Input param: I_irip_seq_tbl (Table of Receipt Item Posting Sequemce Numbers)
 */
FUNCTION REVERT_PART_MTCH_RCPT(O_error_message    OUT VARCHAR2,
                               I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                               I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Update Shipsku and Shipment
 * Input param: I_irip_seq_tbl (Table of Receipt item Posting Sequence Numbers)
 */
FUNCTION REVERT_SHIPMENT(O_error_message    OUT VARCHAR2,
                         I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                         I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Update Invoice detail and head tables
 * Input param: I_dmatch_match_id_tbl (Table of Detail Match IDs)
 *              I_smatch_match_id_tbl (Table of Summary Match IDs)
 *              I_unmtch_lvl (Doc/Item)
 *              I_unmtch_doc_id
 */
FUNCTION REVERT_INVOICE(O_error_message          OUT VARCHAR2,
                        I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                        I_unmtch_lvl          IN     IM_UNMTCH_STATUS.UNMTCH_LVL%TYPE,
                        I_unmtch_doc_id       IN     IM_DOC_HEAD.DOC_ID%TYPE,
                        I_dmatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE,
                        I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Update Resolution Documents to DELETE
 * Input param: I_dmatch_match_id_tbl (Table of Detail Match IDs)
 *              I_smatch_match_id_tbl (Table of Summary Match IDs)
 */
FUNCTION DELETE_RESLN_DOCS(O_error_message          OUT VARCHAR2,
                           I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                           I_dmatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE,
                           I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Add the unmatched receipt to the matching workspace
 * Input param: I_irip_seq_tbl (Table of Receipt Item Posting Sequence Numbers)
 */
FUNCTION ADD_UNMTCH_SHIPMENT(O_error_message    OUT VARCHAR2,
                             I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                             I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                             I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 *  --------- PUBLIC PROCS AND FUNCTIONS BODIES --------------
 */
----------------------------------------------------------------------------------------------
/**
 * The public function used to check if unmatch is possible and Perform Unmatch from Detail Match Item View Screen.
 *
 * Input param: I_workspace_id (The ID of the Detail match workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_UNMTCH_DETL(O_error_message      OUT VARCHAR2,
                             O_unmtch_comments    OUT OBJ_VARCHAR_ID_TABLE,
                             I_workspace_id    IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'REIM_UNDO_MATCH_SQL.PERFORM_UNMTCH_DETL';
   
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;
   L_unmtch_id    NUMBER(10)   := im_unmtch_id_seq.NEXTVAL;

   L_unmtch_item_tbl     OBJ_VARCHAR_ID_TABLE := NULL;
   L_doc_item_tbl        OBJ_NUM_NUM_STR_TBL  := NULL;
   L_irip_seq_tbl        OBJ_NUMERIC_ID_TABLE := NULL;
   L_dmatch_match_id_tbl OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_FETCH_DOC_ITEM is
      select OBJ_NUM_NUM_STR_REC(idmwi.invoice_id,
                                 NULL,
                                 idmwi.item)
        from im_detail_match_ws idmws,
             im_detail_match_ws idmwi
       where idmws.workspace_id    = I_workspace_id
         and idmws.entity_type     = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
         and idmws.matched_filter  = 'N'
         and idmws.choice_flag     = 'Y'
         and idmws.ui_filter_ind   = 'N'
         and idmwi.workspace_id    = I_workspace_id
         and idmwi.entity_type     = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
         and idmwi.ancestor_id     = idmws.detail_match_ws_id
         and idmwi.match_status    = 'M';

   cursor C_FETCH_DMATCH_IDS is
      select /*+ CARDINALITY(di, 10) */ distinct match_id
        from TABLE(CAST(L_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) di,
             im_detail_match_invc_history idmih
       where idmih.doc_id = di.number_1
         and idmih.item   = di.string;

   cursor C_FETCH_IRIP_SEQ_NOS is
      select /*+ CARDINALITY(ids, 10) */ distinct irip.seq_no
        from TABLE(CAST(L_dmatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
             im_detail_match_invc_history idmih,
             im_detail_match_rcpt_history idmrh,
             im_receipt_item_posting irip,
             im_rcpt_item_posting_invoice iripi
       where idmih.match_id = value(ids)
         and idmrh.match_id = idmih.match_id
         and irip.shipment  = idmrh.shipment
         and irip.item      = NVL(idmrh.substitute_item, idmrh.item)
         and iripi.seq_no   = irip.seq_no
         and iripi.doc_id   = idmih.doc_id;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_workspace_id:'|| I_workspace_id);

   open C_FETCH_DOC_ITEM;
   fetch C_FETCH_DOC_ITEM BULK COLLECT into L_doc_item_tbl;
   close C_FETCH_DOC_ITEM;

   LOG_UNMTCH_STATUS(O_error_message,
                     L_unmtch_id,
                     STATUS_INIT,
                     REIM_CONSTANTS.UNMTCH_LVL_ITEM,
                     NULL,
                     L_doc_item_tbl);

   if DIRTY_LOCK_CHECK_DETL(O_error_message,
                            L_unmtch_id,
                            I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   open C_FETCH_DMATCH_IDS;
   fetch C_FETCH_DMATCH_IDS BULK COLLECT into L_dmatch_match_id_tbl;
   close C_FETCH_DMATCH_IDS;

   open C_FETCH_IRIP_SEQ_NOS;
   fetch C_FETCH_IRIP_SEQ_NOS BULK COLLECT into L_irip_seq_tbl;
   close C_FETCH_IRIP_SEQ_NOS;

   -----------------------
   --Validate
   -----------------------

   if VALIDATE_UNMTCH(O_error_message,
                      O_unmtch_comments,
                      L_unmtch_id,
                      REIM_CONSTANTS.UNMTCH_LVL_ITEM,
                      NULL,
                      L_doc_item_tbl,
                      L_dmatch_match_id_tbl,
                      NULL,
                      L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if O_unmtch_comments is NOT NULL and O_unmtch_comments.COUNT > REIM_CONSTANTS.ZERO then
      LOG_UNMTCH_STATUS(O_error_message,
                        L_unmtch_id,
                        STATUS_FAILED,
                        REIM_CONSTANTS.UNMTCH_LVL_ITEM,
                        NULL,
                        NULL);
      REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);
      LOGGER.LOG_INFORMATION('End ' || L_program );
      return REIM_CONSTANTS.SUCCESS;
   end if;

   -----------------------
   --Revert
   -----------------------

   if REVERT_SHIPMENT(O_error_message,
                      L_unmtch_id,
                      L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_INVOICE(O_error_message,
                     L_unmtch_id,
                     REIM_CONSTANTS.UNMTCH_LVL_ITEM,
                     NULL,
                     L_dmatch_match_id_tbl,
                     NULL) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_PART_MTCH_RCPT(O_error_message,
                            L_unmtch_id,
                            L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_RECEIVER_ADJ(O_error_message,
                          L_unmtch_id,
                          L_doc_item_tbl,
                          L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if DELETE_RESLN_DOCS(O_error_message,
                        L_unmtch_id,
                        L_dmatch_match_id_tbl,
                        NULL) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_RESLN_DATA(O_error_message,
                        L_unmtch_id,
                        REIM_CONSTANTS.UNMTCH_LVL_ITEM,
                        NULL,
                        L_doc_item_tbl,
                        NULL) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_DMATCH_HISTORY(O_error_message,
                            L_unmtch_id,
                            L_dmatch_match_id_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if ADD_UNMTCH_SHIPMENT(O_error_message,
                          L_unmtch_id,
                          I_workspace_id,
                          L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_RCPT_ITEM_POSTING(O_error_message,
                               L_unmtch_id,
                               L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REIM_ONLINE_MATCH_SQL.REFRESH_MTCH_WSPACE(O_error_message,
                                                I_workspace_id,
                                                'N') = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REIM_ONLINE_MATCH_SQL.CREATE_DETAIL_MATCH_WS(O_error_message,
                                                   I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   LOG_UNMTCH_STATUS(O_error_message,
                     L_unmtch_id,
                     STATUS_SUCCESS,
                     REIM_CONSTANTS.UNMTCH_LVL_ITEM,
                     NULL,
                     NULL);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        L_unmtch_id,
                        STATUS_FAILED,
                        REIM_CONSTANTS.UNMTCH_LVL_ITEM,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END PERFORM_UNMTCH_DETL;
----------------------------------------------------------------------------------------------
/**
 * The public function used to check if unmatch is possible and Perform Unmatch from Match Inquiry Screen.
 *
 * Input param: I_doc_id (The ID of the Invoice that needs to be unmatched)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_UNMTCH_DOC(O_error_message      OUT VARCHAR2,
                            O_unmtch_comments    OUT OBJ_VARCHAR_ID_TABLE,
                            I_doc_id          IN     IM_DOC_HEAD.DOC_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'REIM_UNDO_MATCH_SQL.PERFORM_UNMTCH_DOC';

   L_start_time TIMESTAMP  := SYSTIMESTAMP;
   L_unmtch_id  NUMBER(10) := im_unmtch_id_seq.NEXTVAL;

   L_doc_item_tbl        OBJ_NUM_NUM_STR_TBL  := NULL;
   L_irip_seq_tbl        OBJ_NUMERIC_ID_TABLE := NULL;
   L_dmatch_match_id_tbl OBJ_NUMERIC_ID_TABLE := NULL;
   L_smatch_match_id_tbl OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_FETCH_DOC_ITEM is
      select OBJ_NUM_NUM_STR_REC(inner.doc_id,
                                 NULL,
                                 inner.item)
        from (select distinct idmih_mtch.doc_id,
                              idmih_mtch.item
                from im_detail_match_invc_history idmih_doc,
                     im_detail_match_invc_history idmih_mtch
               where idmih_doc.doc_id    = I_doc_id
                 and idmih_mtch.match_id = idmih_doc.match_id  
              union all
              select distinct iid.doc_id,
                              iid.item
                from im_summary_match_invc_history ismih_doc,
                     im_summary_match_invc_history ismih_mtch,
                     im_invoice_detail iid
               where ismih_doc.doc_id    = I_doc_id
                 and ismih_mtch.match_id = ismih_doc.match_id
                 and iid.doc_id          = ismih_mtch.doc_id
                 and NOT EXISTS (select 'x'
                                   from im_detail_match_invc_history idmih
                                  where idmih.doc_id = ismih_mtch.doc_id
                                    and idmih.item   = iid.item)) inner;

   cursor C_FETCH_DMATCH_IDS is
      select distinct match_id
        from im_detail_match_invc_history idmih
       where idmih.doc_id = I_doc_id;

   cursor C_FETCH_SMATCH_IDS is
      select distinct match_id
        from im_summary_match_invc_history ismih
       where ismih.doc_id = I_doc_id;

   cursor C_FETCH_IRIP_SEQ_NOS is
      select distinct iripi.seq_no
        from im_rcpt_item_posting_invoice iripi
       where iripi.doc_id   = I_doc_id;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_doc_id: '|| I_doc_id);

   LOG_UNMTCH_STATUS(O_error_message,
                     L_unmtch_id,
                     STATUS_INIT,
                     REIM_CONSTANTS.UNMTCH_LVL_DOC,
                     I_doc_id,
                     NULL);

   if DIRTY_LOCK_CHECK_DOC(O_error_message,
                           L_unmtch_id,
                           I_doc_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   open C_FETCH_DOC_ITEM;
   fetch C_FETCH_DOC_ITEM BULK COLLECT into L_doc_item_tbl;
   close C_FETCH_DOC_ITEM;

   open C_FETCH_DMATCH_IDS;
   fetch C_FETCH_DMATCH_IDS BULK COLLECT into L_dmatch_match_id_tbl;
   close C_FETCH_DMATCH_IDS;

   open C_FETCH_SMATCH_IDS;
   fetch C_FETCH_SMATCH_IDS BULK COLLECT into L_smatch_match_id_tbl;
   close C_FETCH_SMATCH_IDS;

   open C_FETCH_IRIP_SEQ_NOS;
   fetch C_FETCH_IRIP_SEQ_NOS BULK COLLECT into L_irip_seq_tbl;
   close C_FETCH_IRIP_SEQ_NOS;

   -----------------------
   --Validate
   -----------------------

   if VALIDATE_UNMTCH(O_error_message,
                      O_unmtch_comments,
                      L_unmtch_id,
                      REIM_CONSTANTS.UNMTCH_LVL_DOC,
                      I_doc_id,
                      L_doc_item_tbl,
                      L_dmatch_match_id_tbl,
                      L_smatch_match_id_tbl,
                      L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if O_unmtch_comments is NOT NULL and O_unmtch_comments.COUNT > REIM_CONSTANTS.ZERO then
      LOG_UNMTCH_STATUS(O_error_message,
                        L_unmtch_id,
                        STATUS_FAILED,
                        REIM_CONSTANTS.UNMTCH_LVL_DOC,
                        I_doc_id,
                        NULL);
      REIM_PKG_UTIL_SQL.LOG_TIME(L_program, L_start_time);
      LOGGER.LOG_INFORMATION('End ' || L_program );
      return REIM_CONSTANTS.SUCCESS;
   end if;

   -----------------------
   --Revert
   -----------------------

   if REVERT_SHIPMENT(O_error_message,
                      L_unmtch_id,
                      L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_INVOICE(O_error_message,
                     L_unmtch_id,
                     REIM_CONSTANTS.UNMTCH_LVL_DOC,
                     I_doc_id,
                     L_dmatch_match_id_tbl,
                     L_smatch_match_id_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_PART_MTCH_RCPT(O_error_message,
                            L_unmtch_id,
                            L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_RECEIVER_ADJ(O_error_message,
                          L_unmtch_id,
                          L_doc_item_tbl,
                          L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if DELETE_RESLN_DOCS(O_error_message,
                        L_unmtch_id,
                        L_dmatch_match_id_tbl,
                        L_smatch_match_id_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_RESLN_DATA(O_error_message,
                        L_unmtch_id,
                        REIM_CONSTANTS.UNMTCH_LVL_DOC,
                        I_doc_id,
                        L_doc_item_tbl,
                        L_smatch_match_id_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_DMATCH_HISTORY(O_error_message,
                            L_unmtch_id,
                            L_dmatch_match_id_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_SMATCH_HISTORY(O_error_message,
                            L_unmtch_id,
                            L_smatch_match_id_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REVERT_RCPT_ITEM_POSTING(O_error_message,
                               L_unmtch_id,
                               L_irip_seq_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   LOG_UNMTCH_STATUS(O_error_message,
                     L_unmtch_id,
                     STATUS_SUCCESS,
                     REIM_CONSTANTS.UNMTCH_LVL_DOC,
                     I_doc_id,
                     NULL);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        L_unmtch_id,
                        STATUS_FAILED,
                        REIM_CONSTANTS.UNMTCH_LVL_DOC,
                        I_doc_id,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END PERFORM_UNMTCH_DOC;
----------------------------------------------------------------------------------------------
/**
 *  --------- PRIVATE PROCS AND FUNCTIONS BODIES --------------
 */
----------------------------------------------------------------------------------------------
/**
 * The private procedure used for logging Unmatch status information in IM_UNMTCH_STATUS.
 */
PROCEDURE LOG_UNMTCH_STATUS(O_error_message    OUT VARCHAR2,
                            I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                            I_status        IN     IM_UNMTCH_STATUS.STATUS%TYPE,
                            I_unmtch_lvl    IN     IM_UNMTCH_STATUS.UNMTCH_LVL%TYPE,
                            I_unmtch_doc    IN     IM_UNMTCH_STATUS.DOC_ID%TYPE,
                            I_doc_item_tbl  IN     OBJ_NUM_NUM_STR_TBL)
IS
   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.LOG_UNMTCH_STATUS';

   L_user IM_UNMTCH_STATUS.CREATED_BY%TYPE := get_user;

BEGIN

   if I_status = STATUS_INIT then

      insert into im_unmtch_status(unmtch_id,
                                   unmtch_lvl,
                                   doc_id,
                                   item,
                                   status,
                                   created_by,
                                   creation_date,
                                   last_updated_by,
                                   last_update_date,
                                   object_version_id)
                            select I_unmtch_id,
                                   I_unmtch_lvl,
                                   I_unmtch_doc,
                                   NULL,
                                   I_status,
                                   L_user,
                                   sysdate,
                                   L_user,
                                   sysdate,
                                   REIM_CONSTANTS.ONE
                              from dual
                             where I_unmtch_lvl = REIM_CONSTANTS.UNMTCH_LVL_DOC
                            union all
                            select I_unmtch_id,
                                   I_unmtch_lvl,
                                   doc_item.number_1,
                                   doc_item.string,
                                   I_status,
                                   L_user,
                                   sysdate,
                                   L_user,
                                   sysdate,
                                   REIM_CONSTANTS.ONE
                              from TABLE(CAST(I_doc_item_tbl AS OBJ_NUM_NUM_STR_TBL)) doc_item
                             where I_unmtch_lvl = REIM_CONSTANTS.UNMTCH_LVL_ITEM;

   else

      update im_unmtch_status
         set status            = I_status,
             last_updated_by   = L_user,
             last_update_date  = sysdate,
             object_version_id = object_version_id + REIM_CONSTANTS.ONE
       where unmtch_id = I_unmtch_id;

   end if;

   COMMIT;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

END LOG_UNMTCH_STATUS;
------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Check for dirty records in the Detail Match Item View screen
 *              b. Check for DB locks on OPS tables.
 * Input param: I_workspace_id (The ID of the Detail match workspace)
 */
FUNCTION DIRTY_LOCK_CHECK_DETL(O_error_message    OUT VARCHAR2,
                               I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                               I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.DIRTY_LOCK_CHECK_DETL';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_table_name         VARCHAR2(30) := NULL;
   L_dirty_object       VARCHAR2(30) := NULL;
   L_dirty_rec_count    NUMBER       := REIM_CONSTANTS.ZERO;

   dirty_records  EXCEPTION;
   records_locked EXCEPTION;
   PRAGMA         EXCEPTION_INIT(records_locked, -54);

   cursor C_CHK_INVOICE is
      select count(1)
        from im_detail_match_ws idmws,
             im_detail_match_ws idmwi,
             im_match_invc_ws imiw,
             im_doc_head idh
       where idmws.workspace_id       = I_workspace_id
         and idmws.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
         and idmws.matched_filter     = 'Y'
         and idmws.choice_flag        = 'Y'
         and idmws.ui_filter_ind      = 'N'
         and idmwi.workspace_id       = I_workspace_id
         and idmwi.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
         and idmwi.ancestor_id        = idmws.detail_match_ws_id
         and idmwi.match_status       = 'M'
         and imiw.workspace_id        = I_workspace_id
         and imiw.doc_id              = idmwi.invoice_id
         and imiw.choice_flag         = 'Y'
         and imiw.doc_id              = idh.doc_id
         and imiw.doc_head_version_id <> idh.object_version_id;

   cursor C_CHK_SHIPMENT is
      select count(1)
        from im_detail_match_ws idmws,
             im_detail_match_ws idmwr,
             im_match_rcpt_ws imrw,
             shipment sh
       where idmws.workspace_id   = I_workspace_id
         and idmws.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
         and idmws.matched_filter = 'Y'
         and idmws.choice_flag    = 'Y'
         and idmws.ui_filter_ind  = 'N'
         and idmwr.workspace_id   = I_workspace_id
         and idmwr.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
         and idmwr.ancestor_id    = idmws.detail_match_ws_id
         and imrw.workspace_id    = I_workspace_id
         and imrw.shipment        = idmwr.receipt_id
         and imrw.choice_flag     = 'Y'
         and sh.shipment          = imrw.shipment
         and sh.invc_match_status <> imrw.invc_match_status;

   cursor C_CHK_SHIPSKU is
      with shipsku_ws as (select ss.shipment,
                                 ss.item,
                                 SUM(NVL(ss.qty_received, 0) - NVL(ss.qty_matched, 0)) ss_qty_available,
                                 ss.unit_cost ss_unit_cost,
                                 NVL(ss.invc_match_status, REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH) invc_match_status
                            from im_detail_match_ws idmws,
                                 im_detail_match_ws idmwr,
                                 shipsku ss
                           where idmws.workspace_id   = I_workspace_id
                             and idmws.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                             and idmws.matched_filter = 'Y'
                             and idmws.choice_flag    = 'Y'
                             and idmws.ui_filter_ind  = 'N'
                             and idmwr.workspace_id   = I_workspace_id
                             and idmwr.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                             and idmwr.ancestor_id    = idmws.detail_match_ws_id
                             and ss.shipment          = idmwr.receipt_id
                             and ss.item              = idmwr.item
                           GROUP BY ss.shipment,
                                    ss.item,
                                    ss.unit_cost,
                                    ss.invc_match_status)
      select count(1)
        from (select imrw.workspace_id,
                     imrdw.shipment,
                     imrdw.item ws_item,
                     imrdw.invc_match_status,
                     ss_ws.invc_match_status ss_invc_match_status,
                     imrdw.qty_available_nc,
                     ss_ws.ss_qty_available,
                     imrdw.unit_cost_nc,
                     ss_ws.ss_unit_cost
                from im_match_rcpt_ws imrw,
                     shipsku_ws ss_ws,
                     im_match_rcpt_detl_ws imrdw
               where imrw.workspace_id  = I_workspace_id
                 and imrw.choice_flag   = 'Y'
                 and imrdw.workspace_id = imrw.workspace_id
                 and imrdw.shipment     = imrw.shipment
                 and ss_ws.shipment     = imrw.shipment
                 and imrdw.shipment (+) = ss_ws.shipment
                 and imrdw.item (+)     = ss_ws.item)
       where ws_item is NULL
          or invc_match_status <> ss_invc_match_status
          or qty_available_nc  <> ss_qty_available
          or unit_cost_nc      <> ss_unit_cost;

   cursor C_LOCK_DOC_HEAD is
      select 'x'
        from im_doc_head idh
       where EXISTS (select 'x'
                       from im_detail_match_ws idmws,
                            im_detail_match_ws idmwi
                      where idmws.workspace_id   = I_workspace_id
                        and idmws.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                        and idmws.matched_filter = 'Y'
                        and idmws.choice_flag    = 'Y'
                        and idmws.ui_filter_ind  = 'N'
                        and idmwi.workspace_id   = I_workspace_id
                        and idmwi.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                        and idmwi.ancestor_id    = idmws.detail_match_ws_id
                        and idmwi.match_status   = 'M'
                        and idmwi.invoice_id     = idh.doc_id)
         FOR UPDATE NOWAIT;

   cursor C_LOCK_INVC_DETAIL is
      select 'x'
        from im_invoice_detail iid
       where EXISTS (select 'x'
                       from im_detail_match_ws idmws,
                            im_detail_match_ws idmwi
                      where idmws.workspace_id   = I_workspace_id
                        and idmws.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                        and idmws.matched_filter = 'Y'
                        and idmws.choice_flag    = 'Y'
                        and idmws.ui_filter_ind  = 'N'
                        and idmwi.workspace_id   = I_workspace_id
                        and idmwi.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                        and idmwi.ancestor_id    = idmws.detail_match_ws_id
                        and idmwi.match_status   = 'M'
                        and idmwi.invoice_id     = iid.doc_id
                        and idmwi.item           = iid.item)
         FOR UPDATE NOWAIT;

   cursor C_LOCK_SHIPMENT is
      select 'x'
        from shipment sh
       where EXISTS (select 'x'
                       from im_detail_match_ws idmws,
                            im_detail_match_ws idmwi,
                            im_detail_match_invc_history idmih,
                            im_detail_match_rcpt_history idmrh
                      where idmws.workspace_id   = I_workspace_id
                        and idmws.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                        and idmws.matched_filter = 'Y'
                        and idmws.choice_flag    = 'Y'
                        and idmws.ui_filter_ind  = 'N'
                        and idmwi.workspace_id   = I_workspace_id
                        and idmwi.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                        and idmwi.ancestor_id    = idmws.detail_match_ws_id
                        and idmwi.match_status   = 'M'
                        and idmih.doc_id         = idmwi.invoice_id
                        and idmih.item           = idmwi.item
                        and idmrh.match_id       = idmih.match_id
                        and idmrh.shipment       = sh.shipment)
         FOR UPDATE NOWAIT;

   cursor C_LOCK_SHIPSKU is
      select 'x'
        from shipsku ss
       where EXISTS (select 'x'
                       from im_detail_match_ws idmws,
                            im_detail_match_ws idmwi,
                            im_detail_match_invc_history idmih,
                            im_detail_match_rcpt_history idmrh
                      where idmws.workspace_id   = I_workspace_id
                        and idmws.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                        and idmws.matched_filter = 'Y'
                        and idmws.choice_flag    = 'Y'
                        and idmws.ui_filter_ind  = 'N'
                        and idmwi.workspace_id   = I_workspace_id
                        and idmwi.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                        and idmwi.ancestor_id    = idmws.detail_match_ws_id
                        and idmwi.match_status   = 'M'
                        and idmih.doc_id         = idmwi.invoice_id
                        and idmih.item           = idmwi.item
                        and idmrh.match_id       = idmih.match_id
                        and ss.shipment          = idmrh.shipment
                        and ss.item              = NVL(idmrh.substitute_item,idmrh.item))
         FOR UPDATE NOWAIT;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_workspace_id:'|| I_workspace_id);

   --DIRTY CHECKS
   L_dirty_object := 'INVOICE'; --constant
   open C_CHK_INVOICE;
   fetch C_CHK_INVOICE into L_dirty_rec_count;
   close C_CHK_INVOICE;

   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;

   L_dirty_object := 'SHIPMENT'; --constant
   open C_CHK_SHIPMENT;
   fetch C_CHK_SHIPMENT into L_dirty_rec_count;
   close C_CHK_SHIPMENT;

   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;

   open C_CHK_SHIPSKU;
   fetch C_CHK_SHIPSKU into L_dirty_rec_count;
   close C_CHK_SHIPSKU;

   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;

   --CHECK LOCKS
   L_table_name := 'IM_DOC_HEAD'; --constant
   open C_LOCK_DOC_HEAD;
   close C_LOCK_DOC_HEAD;

   L_table_name := 'IM_INVOICE_DETAIL'; --constant
   open C_LOCK_INVC_DETAIL;
   close C_LOCK_INVC_DETAIL;

   L_table_name := 'SHIPMENT'; --constant
   open C_LOCK_SHIPMENT;
   close C_LOCK_SHIPMENT;
   LOGGER.LOG_INFORMATION(L_program||' lock shipment');

   L_table_name := 'SHIPSKU'; --constant
   open C_LOCK_SHIPSKU;
   close C_LOCK_SHIPSKU;
   LOGGER.LOG_INFORMATION(L_program||' lock shipsku');

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when records_locked then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED', --constant
                                            L_table_name,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when dirty_records then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG('DIRTY_RECORDS', --constant
                                            L_dirty_object,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DIRTY_LOCK_CHECK_DETL;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Check for dirty records in the match Inquiry screen
 *              b. Check for DB locks on OPS tables.
 * Input param: I_doc_id (The ID of the Invoice that needs to be unmatched)
 */
FUNCTION DIRTY_LOCK_CHECK_DOC(O_error_message    OUT VARCHAR2,
                              I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                              I_doc_id        IN     IM_DOC_HEAD.DOC_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.DIRTY_LOCK_CHECK_DOC';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_table_name         VARCHAR2(30) := NULL;
   L_dirty_object       VARCHAR2(30) := NULL;
   L_dirty_rec_count    NUMBER       := REIM_CONSTANTS.ZERO;

   dirty_records  EXCEPTION;
   records_locked EXCEPTION;
   PRAGMA         EXCEPTION_INIT(records_locked, -54);

   cursor C_LOCK_DOC_HEAD is
      select 'x'
        from im_doc_head idh
       where doc_id = I_doc_id
         FOR UPDATE NOWAIT;

   cursor C_LOCK_INVC_DETAIL is
      select 'x'
        from im_invoice_detail iid
       where doc_id = I_doc_id
         FOR UPDATE NOWAIT;

   cursor C_LOCK_SHIPMENT is
      select 'x'
        from shipment sh
       where EXISTS (select 'x'
                       from im_rcpt_item_posting_invoice iripi,
                            im_receipt_item_posting irip
                      where iripi.doc_id  = I_doc_id
                        and irip.seq_no   = iripi.seq_no
                        and irip.shipment = sh.shipment)
         FOR UPDATE NOWAIT;

   cursor C_LOCK_SHIPSKU is
      select 'x'
        from shipsku ss
       where EXISTS (select 'x'
                       from im_rcpt_item_posting_invoice iripi,
                            im_receipt_item_posting irip
                      where iripi.doc_id  = I_doc_id
                        and irip.seq_no   = iripi.seq_no
                        and irip.shipment = ss.shipment
                        and irip.item     = ss.item)
         FOR UPDATE NOWAIT;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_doc_id: ' || I_doc_id);

--   --DIRTY CHECKS
--   L_dirty_object := 'INVOICE'; --constant
--   open C_CHK_INVOICE;
--   fetch C_CHK_INVOICE into L_dirty_rec_count;
--   close C_CHK_INVOICE;
--
--   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
--      RAISE dirty_records;
--   end if;
--
--   L_dirty_object := 'SHIPMENT'; --constant
--   open C_CHK_SHIPMENT;
--   fetch C_CHK_SHIPMENT into L_dirty_rec_count;
--   close C_CHK_SHIPMENT;
--
--   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
--      RAISE dirty_records;
--   end if;
--
--   open C_CHK_SHIPSKU;
--   fetch C_CHK_SHIPSKU into L_dirty_rec_count;
--   close C_CHK_SHIPSKU;
--
--   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
--      RAISE dirty_records;
--   end if;

   --CHECK LOCKS
   L_table_name := 'IM_DOC_HEAD'; --constant
   open C_LOCK_DOC_HEAD;
   close C_LOCK_DOC_HEAD;

   L_table_name := 'IM_INVOICE_DETAIL'; --constant
   open C_LOCK_INVC_DETAIL;
   close C_LOCK_INVC_DETAIL;

   L_table_name := 'SHIPMENT'; --constant
   open C_LOCK_SHIPMENT;
   close C_LOCK_SHIPMENT;
   LOGGER.LOG_INFORMATION(L_program||' lock shipment');

   L_table_name := 'SHIPSKU'; --constant
   open C_LOCK_SHIPSKU;
   close C_LOCK_SHIPSKU;
   LOGGER.LOG_INFORMATION(L_program||' lock shipsku');

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when records_locked then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED', --constant
                                            L_table_name,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when dirty_records then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG('DIRTY_RECORDS', --constant
                                            L_dirty_object,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DIRTY_LOCK_CHECK_DOC;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Move records from im_detail_match_%_history tables to im_unmtch_detl_match_%_history
 * Input param: I_dmatch_match_id_tbl (Table of Detail Match IDs)
 */
FUNCTION REVERT_DMATCH_HISTORY(O_error_message          OUT VARCHAR2,
                               I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                               I_dmatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.REVERT_DMATCH_HISTORY';

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id);

   insert into im_unmtch_detl_match_hist(unmtch_id,
                                         match_id,
                                         auto_matched,
                                         exact_match,
                                         created_by,
                                         creation_date,
                                         last_updated_by,
                                         last_update_date,
                                         object_version_id)
                                  select /*+ CARDINALITY(ids, 10) */ I_unmtch_id,
                                         idmh.match_id,
                                         idmh.auto_matched,
                                         idmh.exact_match,
                                         idmh.created_by,
                                         idmh.creation_date,
                                         idmh.last_updated_by,
                                         idmh.last_update_date,
                                         idmh.object_version_id
                                    from TABLE(CAST(I_dmatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                         im_detail_match_history idmh
                                   where idmh.match_id = value(ids);

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_detl_match_hist - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_unmtch_detl_match_invc_hist(unmtch_id,
                                              match_id,
                                              doc_id,
                                              item,
                                              created_by,
                                              creation_date,
                                              last_updated_by,
                                              last_update_date,
                                              object_version_id)
                                       select /*+ CARDINALITY(ids, 10) */ I_unmtch_id,
                                              idmih.match_id,
                                              idmih.doc_id,
                                              idmih.item,
                                              idmih.created_by,
                                              idmih.creation_date,
                                              idmih.last_updated_by,
                                              idmih.last_update_date,
                                              idmih.object_version_id
                                         from TABLE(CAST(I_dmatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                              im_detail_match_invc_history idmih
                                        where idmih.match_id = value(ids);

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_detl_match_invc_hist - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_unmtch_detl_match_rcpt_hist(unmtch_id,
                                              match_id,
                                              shipment,
                                              item,
                                              substitute_item,
                                              created_by,
                                              creation_date,
                                              last_updated_by,
                                              last_update_date,
                                              object_version_id)
                                       select /*+ CARDINALITY(ids, 10) */ I_unmtch_id,
                                              idmrh.match_id,
                                              idmrh.shipment,
                                              idmrh.item,
                                              idmrh.substitute_item,
                                              idmrh.created_by,
                                              idmrh.creation_date,
                                              idmrh.last_updated_by,
                                              idmrh.last_update_date,
                                              idmrh.object_version_id
                                         from TABLE(CAST(I_dmatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                              im_detail_match_rcpt_history idmrh
                                        where idmrh.match_id = value(ids);

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_detl_match_rcpt_hist - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_detail_match_rcpt_history idmrh
    where idmrh.match_id IN (select /*+ CARDINALITY(ids, 10) */ value(ids)
                               from TABLE(CAST(I_dmatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_detail_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_detail_match_invc_history idmih
    where idmih.match_id IN (select /*+ CARDINALITY(ids, 10) */ value(ids)
                               from TABLE(CAST(I_dmatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_detail_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_detail_match_history idmh
    where idmh.match_id IN (select /*+ CARDINALITY(ids, 10) */ value(ids)
                              from TABLE(CAST(I_dmatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_detail_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REVERT_DMATCH_HISTORY;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Move records from im_summary_match_%_history tables to im_unmtch_smry_match_%_history
 * Input param: I_smatch_match_id_tbl (Table of Summary Match IDs)
 */
FUNCTION REVERT_SMATCH_HISTORY(O_error_message          OUT VARCHAR2,
                               I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                               I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.REVERT_SMATCH_HISTORY';

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id);

   insert into im_unmtch_smry_match_hist(unmtch_id,
                                         match_id,
                                         auto_matched,
                                         exact_match,
                                         created_by,
                                         creation_date,
                                         last_updated_by,
                                         last_update_date,
                                         object_version_id)
                                  select /*+ CARDINALITY(ids, 10) */ I_unmtch_id,
                                         ismh.match_id,
                                         ismh.auto_matched,
                                         ismh.exact_match,
                                         ismh.created_by,
                                         ismh.creation_date,
                                         ismh.last_updated_by,
                                         ismh.last_update_date,
                                         ismh.object_version_id
                                    from TABLE(CAST(I_smatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                         im_summary_match_history ismh
                                   where ismh.match_id = value(ids);

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_smry_match_hist - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_unmtch_smry_match_invc_hist(unmtch_id,
                                              match_id,
                                              doc_id,
                                              created_by,
                                              creation_date,
                                              last_updated_by,
                                              last_update_date,
                                              object_version_id)
                                       select /*+ CARDINALITY(ids, 10) */ I_unmtch_id,
                                              ismih.match_id,
                                              ismih.doc_id,
                                              ismih.created_by,
                                              ismih.creation_date,
                                              ismih.last_updated_by,
                                              ismih.last_update_date,
                                              ismih.object_version_id
                                         from TABLE(CAST(I_smatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                              im_summary_match_invc_history ismih
                                        where ismih.match_id = value(ids);

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_smry_match_invc_hist - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_unmtch_smry_match_rcpt_hist(unmtch_id,
                                              match_id,
                                              shipment,
                                              created_by,
                                              creation_date,
                                              last_updated_by,
                                              last_update_date,
                                              object_version_id)
                                       select /*+ CARDINALITY(ids, 10) */ I_unmtch_id,
                                              ismrh.match_id,
                                              ismrh.shipment,
                                              ismrh.created_by,
                                              ismrh.creation_date,
                                              ismrh.last_updated_by,
                                              ismrh.last_update_date,
                                              ismrh.object_version_id
                                         from TABLE(CAST(I_smatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                              im_summary_match_rcpt_history ismrh
                                        where ismrh.match_id = value(ids);

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_smry_match_rcpt_hist - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_summary_match_rcpt_history ismrh
    where ismrh.match_id IN (select /*+ CARDINALITY(ids, 10) */ value(ids)
                               from TABLE(CAST(I_smatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_summary_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_summary_match_invc_history ismih
    where ismih.match_id IN (select /*+ CARDINALITY(ids, 10) */ value(ids)
                               from TABLE(CAST(I_smatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_summary_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_summary_match_history ismh
    where ismh.match_id IN (select /*+ CARDINALITY(ids, 10) */ value(ids)
                              from TABLE(CAST(I_smatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_summary_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REVERT_SMATCH_HISTORY;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Move records from im_receipt_item_posting, im_rcpt_item_posting_invoice  tables to im_unmtch_rcpt_item_posting, im_unmtch_rcpt_item_post_invc
 * Input param: I_irip_seq_tbl (Table of Receipt Item Posting Sequemce Numbers)
 */
FUNCTION REVERT_RCPT_ITEM_POSTING(O_error_message    OUT VARCHAR2,
                                  I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                                  I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.REVERT_RCPT_ITEM_POSTING';

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id);

   insert into im_unmtch_rcpt_item_posting(unmtch_id,
                                           seq_no,
                                           shipment,
                                           item,
                                           qty_matched,
                                           qty_posted,
                                           created_by,
                                           creation_date,
                                           last_updated_by,
                                           last_update_date,
                                           object_version_id)
                                    select /*+ CARDINALITY(ids, 10) */ I_unmtch_id,
                                           irip.seq_no,
                                           irip.shipment,
                                           irip.item,
                                           irip.qty_matched,
                                           irip.qty_posted,
                                           irip.created_by,
                                           irip.creation_date,
                                           irip.last_updated_by,
                                           irip.last_update_date,
                                           irip.object_version_id
                                      from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                           im_receipt_item_posting irip
                                     where irip.seq_no = value(ids);

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_rcpt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_unmtch_rcpt_item_post_invc(unmtch_id,
                                             seq_no,
                                             doc_id,
                                             status,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
                                     select /*+ CARDINALITY(ids, 10) */ I_unmtch_id,
                                             iripi.seq_no,
                                             iripi.doc_id,
                                             iripi.status,
                                             iripi.created_by,
                                             iripi.creation_date,
                                             iripi.last_updated_by,
                                             iripi.last_update_date,
                                             iripi.object_version_id
                                        from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                             im_rcpt_item_posting_invoice iripi
                                       where iripi.seq_no = value(ids);

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_rcpt_item_post_invc - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_rcpt_item_posting_invoice iripi
    where iripi.seq_no IN (select /*+ CARDINALITY(ids, 10) */ value(ids)
                            from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_rcpt_item_posting_invoice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_receipt_item_posting irip
    where irip.seq_no IN (select /*+ CARDINALITY(ids, 10) */ value(ids)
                           from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_receipt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REVERT_RCPT_ITEM_POSTING;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Move records from im_resolution_action, im_doc_detail_comments tables to im_unmtch_resln_action and im_unmtch_doc_detail_comnts
 *              b. Updates the status of resolutions (that were performed on invoice-items which are not part of the match which is getting unmatched) to unrolled
 * Input param: I_doc_item_tbl (Table of Document-Item)
 */
FUNCTION REVERT_RESLN_DATA(O_error_message          OUT VARCHAR2,
                           I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                           I_unmtch_lvl          IN     IM_UNMTCH_STATUS.UNMTCH_LVL%TYPE,
                           I_unmtch_doc_id       IN     IM_DOC_HEAD.DOC_ID%TYPE,
                           I_doc_item_tbl        IN     OBJ_NUM_NUM_STR_TBL,
                           I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.REVERT_RESLN_DATA';

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id);

   insert into im_unmtch_resolution_action(unmtch_id,
                                           doc_id,
                                           item,
                                           reason_code_id,
                                           action,
                                           qty,
                                           unit_cost,
                                           extended_cost,
                                           status,
                                           shipment,
                                           created_by,
                                           creation_date,
                                           last_updated_by,
                                           last_update_date,
                                           object_version_id)
                                    select /*+ CARDINALITY(doc_item, 10) */ I_unmtch_id,
                                           ira.doc_id,
                                           ira.item,
                                           ira.reason_code_id,
                                           ira.action,
                                           ira.qty,
                                           ira.unit_cost,
                                           ira.extended_cost,
                                           ira.status,
                                           ira.shipment,
                                           ira.created_by,
                                           ira.creation_date,
                                           ira.last_updated_by,
                                           ira.last_update_date,
                                           ira.object_version_id
                                      from TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item,
                                           im_resolution_action ira
                                     where ira.doc_id = doc_item.number_1
                                       and ira.item   = doc_item.string
                                       and ira.action <> REIM_CONSTANTS.RC_ACTION_UIT;

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_resolution_action - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_unmtch_doc_detl_comments(unmtch_id,
                                           comment_id,
                                           comment_type,
                                           text,
                                           doc_id,
                                           item,
                                           discrepancy_type,
                                           reason_code_id,
                                           debit_reason_code,
                                           created_by,
                                           creation_date,
                                           last_updated_by,
                                           last_update_date,
                                           object_version_id)
                                    select /*+ CARDINALITY(doc_item, 10) */ I_unmtch_id,
                                           iddc.comment_id,
                                           iddc.comment_type,
                                           iddc.text,
                                           iddc.doc_id,
                                           iddc.item,
                                           iddc.discrepancy_type,
                                           iddc.reason_code_id,
                                           iddc.debit_reason_code,
                                           iddc.created_by,
                                           iddc.creation_date,
                                           iddc.last_updated_by,
                                           iddc.last_update_date,
                                           iddc.object_version_id
                                      from TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item,
                                           im_doc_detail_comments iddc
                                     where iddc.doc_id           = doc_item.number_1
                                       and iddc.item             = doc_item.string
                                       and iddc.reason_code_id   is NOT NULL
                                       and iddc.discrepancy_type <> 'TAX';

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_doc_detl_comments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_doc_detail_comments iddc
    where (iddc.doc_id, iddc.item) IN (select /*+ CARDINALITY(doc_item, 10) */ doc_item.number_1,
                                              doc_item.string
                                         from TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item)
      and iddc.reason_code_id   is NOT NULL
      and iddc.discrepancy_type <> 'TAX';

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_doc_detail_comments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_resolution_action ira
    where (ira.doc_id, ira.item) IN (select /*+ CARDINALITY(doc_item, 10) */ doc_item.number_1,
                                            doc_item.string
                                       from TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item)
      and ira.action             <> REIM_CONSTANTS.RC_ACTION_UIT;

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_resolution_action - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   update im_resolution_action ira
      set ira.status = REIM_CONSTANTS.RCA_STATUS_UNROLLED
    where I_unmtch_lvl = REIM_CONSTANTS.UNMTCH_LVL_DOC
      and ira.doc_id   IN (select /*+ CARDINALITY(ids, 10) */ ismih.doc_id
                             from TABLE(CAST(I_smatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                                  im_summary_match_invc_history ismih
                            where ismih.match_id = value(ids)
                              and ismih.doc_id   <> I_unmtch_doc_id)
      and ira.status   = REIM_CONSTANTS.RCA_STATUS_ROLLEDUP
      and ira.action   NOT IN (REIM_CONSTANTS.RC_ACTION_UIT,
                               REIM_CONSTANTS.RC_ACTION_RUA,
                               REIM_CONSTANTS.RC_ACTION_RCA,
                               REIM_CONSTANTS.RC_ACTION_RCAS,
                               REIM_CONSTANTS.RC_ACTION_RCAMR,
                               REIM_CONSTANTS.RC_ACTION_RCAMRS);

   LOGGER.LOG_INFORMATION(L_program||' Update status of resolutions to unrolled - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REVERT_RESLN_DATA;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Revert Receiver Cost and Unit Adjustments
 * Input param: I_doc_item_tbl (Table of Document-Item)
 *              I_irip_seq_tbl (Table of Sequence Numbers)
 */
FUNCTION REVERT_RECEIVER_ADJ(O_error_message    OUT VARCHAR2,
                             I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                             I_doc_item_tbl  IN     OBJ_NUM_NUM_STR_TBL,
                             I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.REVERT_RECEIVER_ADJ';

   L_user  IM_DOC_HEAD.CREATED_BY%TYPE := get_user;
   L_vdate DATE                        := get_vdate;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id);

   merge into im_receiver_cost_adjust tgt
   using (select /*+ CARDINALITY(doc_item, 10) CARDINALITY(ids, 10) */ irca.order_no,
                 irca.item,
                 irca.location,
                 irca.adjusted_unit_cost - ira.unit_cost new_adj_unit_cost
            from TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item,
                 TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                 im_resolution_action ira,
                 im_rcpt_item_posting_invoice iripi,
                 im_receipt_item_posting irip,
                 im_receiver_cost_adjust irca
           where ira.doc_id          = doc_item.number_1
             and ira.item            = doc_item.string
             and ira.action          IN (REIM_CONSTANTS.RC_ACTION_RCA,
                                         REIM_CONSTANTS.RC_ACTION_RCAS,
                                         REIM_CONSTANTS.RC_ACTION_RCAMR,
                                         REIM_CONSTANTS.RC_ACTION_RCAMRS)
             and iripi.seq_no        = value(ids)
             and irip.seq_no         = iripi.seq_no
             and ira.doc_id          = iripi.doc_id
             and irca.shipment       = irip.shipment
             and irca.item           = irip.item
             and irca.reason_code_id = ira.reason_code_id
             and irca.item           = ira.item) src
   on (    tgt.order_no = src.order_no
       and tgt.item     = src.item
       and tgt.location = src.location)
   when MATCHED then
      update
         set tgt.adjusted_unit_cost = src.new_adj_unit_cost;

   LOGGER.LOG_INFORMATION(L_program||' Update New cost for RCAs into im_receiver_cost_adjust - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_receiver_unit_adjust(shipment,
                                       item,
                                       seq_no,
                                       adjusted_item_qty,
                                       comments,
                                       reason_code_id,
                                       created_by,
                                       location,
                                       creation_date,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
                                select /*+ CARDINALITY(doc_item, 10) CARDINALITY(ids, 10) */ irua.shipment,
                                       irua.item,
                                       irua.seq_no + REIM_CONSTANTS.ONE,
                                       (-1) * irua.adjusted_item_qty,
                                       irua.comments,
                                       irua.reason_code_id,
                                       L_user,
                                       irua.location,
                                       L_vdate,
                                       L_user,
                                       L_vdate,
                                       REIM_CONSTANTS.ONE
                                  from TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item,
                                       TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                       im_resolution_action ira,
                                       im_rcpt_item_posting_invoice iripi,
                                       im_receipt_item_posting irip,
                                       im_receiver_unit_adjust irua
                                 where ira.doc_id          = doc_item.number_1
                                   and ira.item            = doc_item.string
                                   and ira.action          = REIM_CONSTANTS.RC_ACTION_RUA
                                   and iripi.seq_no        = value(ids)
                                   and irip.seq_no         = iripi.seq_no
                                   and ira.doc_id          = iripi.doc_id
                                   and irua.shipment       = irip.shipment
                                   and irua.item           = irip.item
                                   and irua.reason_code_id = ira.reason_code_id
                                   and irua.item           = ira.item;

   LOGGER.LOG_INFORMATION(L_program||' Insert new RUAs into im_receiver_unit_adjust - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REVERT_RECEIVER_ADJ;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Update im_partially_matched_receipts and move unmatched amounts to im_unmtch_part_mtch_rcpt
 * Input param: I_irip_seq_tbl (Table of Receipt Item Posting Sequemce Numbers)
 */
FUNCTION REVERT_PART_MTCH_RCPT(O_error_message    OUT VARCHAR2,
                               I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                               I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.REVERT_PART_MTCH_RCPT';

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id);

   insert into im_unmtch_part_mtch_rcpt(unmtch_id,
                                        shipment,
                                        item,
                                        qty_matched,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
                                 select I_unmtch_id,
                                        ri.shipment,
                                        ri.item,
                                        ri.qty_matched,
                                        ipmr.created_by,
                                        ipmr.creation_date,
                                        ipmr.last_updated_by,
                                        ipmr.last_update_date,
                                        ipmr.object_version_id
                                 from (select /*+ CARDINALITY(ids, 10) */ irip.shipment,
                                              irip.item,
                                              SUM(irip.qty_matched) qty_matched
                                         from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                              im_receipt_item_posting irip
                                        where irip.seq_no = value(ids)
                                        GROUP BY irip.shipment,
                                                 irip.item) ri,
                                      im_partially_matched_receipts ipmr
                                where ipmr.shipment = ri.shipment
                                  and ipmr.item     = ri.item;

   LOGGER.LOG_INFORMATION(L_program||' Insert into im_unmtch_part_mtch_rcpt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_partially_matched_receipts tgt
   using (select /*+ CARDINALITY(ids, 10) */ irip.shipment,
                                             irip.item,
                                             SUM(qty_matched) qty_matched
            from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                 im_receipt_item_posting irip
           where irip.seq_no = value(ids)
           GROUP BY irip.shipment,
                    irip.item) src
   on (    src.shipment = tgt.shipment
       and src.item     = tgt.item)
   when MATCHED then
      update
         set tgt.qty_matched = tgt.qty_matched - src.qty_matched;

   LOGGER.LOG_INFORMATION(L_program||' Update Qty matched in im_partially_matched_receipts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_partially_matched_receipts ipmr
    where qty_matched = REIM_CONSTANTS.ZERO
      and exists (select /*+ CARDINALITY(ids, 10) */ 'x'
                    from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                         im_receipt_item_posting irip
                   where irip.seq_no   = value(ids)
                     and ipmr.shipment = irip.shipment
                     and ipmr.item     = irip.item);

   LOGGER.LOG_INFORMATION(L_program||' Delete from im_partially_matched_receipts when qty_matched is Zero - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REVERT_PART_MTCH_RCPT;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Update Shipsku and Shipment
 * Input param: I_irip_seq_tbl (Table of Receipt item Posting Sequence Numbers)
 */
FUNCTION REVERT_SHIPMENT(O_error_message    OUT VARCHAR2,
                         I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                         I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.REVERT_SHIPMENT';

   L_ship_item_qtys_tbl IM_SHIP_ITEM_QTYS_TBL := NULL;

   cursor C_FETCH_SHIP_ITEM_QTY is
      select IM_SHIP_ITEM_QTYS_REC(inner.shipment,
                                   inner.item,
                                   inner.qty_matched)
        from (select /*+ CARDINALITY(ids, 10) */ irip.shipment,
                     irip.item,
                     SUM(irip.qty_matched) qty_matched
                from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                     im_receipt_item_posting irip
               where irip.seq_no   = value(ids)
               GROUP BY irip.shipment,
                        irip.item) inner;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id);

   open C_FETCH_SHIP_ITEM_QTY;
   fetch C_FETCH_SHIP_ITEM_QTY BULK COLLECT into L_ship_item_qtys_tbl;
   close C_FETCH_SHIP_ITEM_QTY;

   if SHIPMENT_API_SQL.DEDUCT_INVOICE_QTY_MATCHED(O_error_message,
                                                  L_ship_item_qtys_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   LOGGER.LOG_INFORMATION(L_program ||' Update Shipsku ');

   update shipment sh
      set sh.invc_match_status = REIM_CONSTANTS.SHIP_STATUS_UNMTCH,
          sh.invc_match_date   = NULL
    where EXISTS (select /*+ CARDINALITY(ship_item, 10) */ 'x'
                    from TABLE(CAST(L_ship_item_qtys_tbl as IM_SHIP_ITEM_QTYS_TBL)) ship_item
                   where ship_item.shipment = sh.shipment);

   LOGGER.LOG_INFORMATION(L_program||' Update Shipment Invoice Match Status - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REVERT_SHIPMENT;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Update Invoice detail and head tables
 * Input param: I_dmatch_match_id_tbl (Table of Detail Match IDs)
 *              I_smatch_match_id_tbl (Table of Summary Match IDs)
 *              I_unmtch_lvl (Doc/Item)
 *              I_unmtch_doc_id
 */
FUNCTION REVERT_INVOICE(O_error_message          OUT VARCHAR2,
                        I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                        I_unmtch_lvl          IN     IM_UNMTCH_STATUS.UNMTCH_LVL%TYPE,
                        I_unmtch_doc_id       IN     IM_DOC_HEAD.DOC_ID%TYPE,
                        I_dmatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE,
                        I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.REVERT_INVOICE';

   L_user IM_DOC_HEAD.CREATED_BY%TYPE := get_user;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id
                                    ||' I_unmtch_lvl: '        || I_unmtch_lvl
                                    ||' I_unmtch_doc_id: '     || I_unmtch_doc_id);

   merge into im_doc_head tgt
   using (with doc_item as (select /*+ CARDINALITY(ids, 10) */ distinct idmih.doc_id,
                                   idmih.item
                             from TABLE(CAST(I_dmatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                                  im_detail_match_invc_history idmih
                            where idmih.match_id = value(ids))
          select di.doc_id,
                 SUM((iid.resolution_adjusted_unit_cost * (iid.qty - iid.resolution_adjusted_qty)) + (iid.qty * (iid.unit_cost - iid.resolution_adjusted_unit_cost))) adj_total_cost,
                 SUM(iid.qty - iid.resolution_adjusted_qty) adj_total_qty
            from doc_item di,
                 im_invoice_detail iid
           where iid.doc_id = di.doc_id
             and iid.item   = di.item
           GROUP BY di.doc_id) src
   on (src.doc_id = tgt.doc_id)
   when MATCHED then
      update
         set tgt.status                         = REIM_CONSTANTS.DOC_STATUS_URMTCH,
             tgt.match_id                       = NULL,
             tgt.match_date                     = NULL,
             tgt.match_type                     = NULL,
             tgt.resolution_adjusted_total_cost = tgt.resolution_adjusted_total_cost + src.adj_total_cost,
             tgt.resolution_adjusted_total_qty  = tgt.resolution_adjusted_total_qty + src.adj_total_qty,
             tgt.hold_status                    = 'N',
             tgt.last_updated_by                = L_user,
             tgt.last_update_date               = sysdate,
             tgt.object_version_id              = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Update im_doc_head for Detail Matches - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select /*+ CARDINALITY(ids, 10) */ distinct ismih.doc_id
            from TABLE(CAST(I_smatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                 im_summary_match_invc_history ismih
           where ismih.match_id = value(ids)) src
   on (src.doc_id = tgt.doc_id)
   when MATCHED then
      update
         set tgt.status                         = REIM_CONSTANTS.DOC_STATUS_URMTCH,
             tgt.match_id                       = NULL,
             tgt.match_date                     = NULL,
             tgt.match_type                     = NULL,
             tgt.detail_matched                 = DECODE(tgt.doc_id, I_unmtch_doc_id, 'N', tgt.detail_matched),
             tgt.variance_within_tolerance      = NULL,
             tgt.resolution_adjusted_total_cost = DECODE(tgt.doc_id, I_unmtch_doc_id, tgt.total_cost, tgt.resolution_adjusted_total_cost),
             tgt.resolution_adjusted_total_qty  = DECODE(tgt.doc_id, I_unmtch_doc_id, tgt.total_qty, tgt.resolution_adjusted_total_qty), 
             tgt.hold_status                    = 'N',
             tgt.last_updated_by                = L_user,
             tgt.last_update_date               = sysdate,
             tgt.object_version_id              = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Update im_doc_head for Summary Matches - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_invoice_detail tgt
   using (select /*+ CARDINALITY(ids, 10) */ distinct idmih.doc_id,
                          idmih.item
            from TABLE(CAST(I_dmatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                 im_detail_match_invc_history idmih
           where idmih.match_id = value(ids)  
          union all
          select /*+ CARDINALITY(ids, 10) */ distinct iid.doc_id,
                          iid.item
            from TABLE(CAST(I_smatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                 im_summary_match_invc_history ismih,
                 im_invoice_detail iid
           where ismih.match_id = value(ids)
             and iid.doc_id     = ismih.doc_id
             and NOT EXISTS (select 'x'
                               from im_detail_match_invc_history idmih
                              where idmih.doc_id = ismih.doc_id
                                and idmih.item   = iid.item)) src
   on (    src.doc_id = tgt.doc_id
       and src.item   = tgt.item)
   when MATCHED then
      update
         set tgt.resolution_adjusted_unit_cost  = tgt.unit_cost,
             tgt.resolution_adjusted_qty        = tgt.qty,
             tgt.status                         = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH,
             tgt.cost_matched                   = 'N',
             tgt.qty_matched                    = 'N',
             tgt.cost_variance_within_tolerance = NULL,
             tgt.qty_variance_within_tolerance  = NULL,
             tgt.adjustment_pending             = 'N',
             tgt.last_updated_by                = L_user,
             tgt.last_update_date               = sysdate,
             tgt.object_version_id              = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Update im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (with docs as (select distinct inner.doc_id
                          from (select /*+ CARDINALITY(ids, 10) */ idmih.doc_id
                                  from TABLE(CAST(I_dmatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                                       im_detail_match_invc_history idmih
                                 where idmih.match_id = value(ids)
                                union all
                                select /*+ CARDINALITY(ids, 10) */ ismih.doc_id
                                  from TABLE(CAST(I_smatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                                       im_summary_match_invc_history ismih
                                 where ismih.match_id = value(ids)) inner)
          select d.doc_id,
                 SUM(DECODE(iid.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, REIM_CONSTANTS.ONE,
                            REIM_CONSTANTS.ZERO)) item_mtch_count
            from docs d,
                 im_invoice_detail iid
           where iid.doc_id = d.doc_id
           GROUP BY d.doc_id) src
   on (src.doc_id = tgt.doc_id)
   when MATCHED then
      update
         set tgt.detail_matched = DECODE(src.item_mtch_count,
                                         REIM_CONSTANTS.ZERO, 'N',
                                         'Y');

   LOGGER.LOG_INFORMATION(L_program||' Update detail_matched on im_doc_head for Doc Level unmatching - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REVERT_INVOICE;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Update Resolution Documents to DELETE
 * Input param: I_dmatch_match_id_tbl (Table of Detail Match IDs)
 *              I_smatch_match_id_tbl (Table of Summary Match IDs)
 */
FUNCTION DELETE_RESLN_DOCS(O_error_message          OUT VARCHAR2,
                           I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                           I_dmatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE,
                           I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.DELETE_RESLN_DOCS';

   L_user IM_DOC_HEAD.CREATED_BY%TYPE := get_user;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id);

   update im_doc_head idh
      set idh.status = REIM_CONSTANTS.DOC_STATUS_DELETE
    where idh.type    NOT IN (REIM_CONSTANTS.DOC_TYPE_DEBMET,
                              REIM_CONSTANTS.DOC_TYPE_CRDNRT)
      and idh.ref_doc IN (select /*+ CARDINALITY(ids, 10) */ idmih.doc_id
                            from TABLE(CAST(I_dmatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                                 im_detail_match_invc_history idmih
                           where idmih.match_id = value(ids)
                          union all
                          select /*+ CARDINALITY(ids, 10) */ distinct ismih.doc_id
                            from TABLE(CAST(I_smatch_match_id_tbl AS OBJ_NUMERIC_ID_TABLE)) ids,
                                 im_summary_match_invc_history ismih
                           where ismih.match_id = value(ids));

   LOGGER.LOG_INFORMATION(L_program||' Update Resln documents status to DELETE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DELETE_RESLN_DOCS;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Validate if Unmatch is Possible
 * Input param: I_doc_item_tbl (Table of Document Items)
 *              I_dmatch_match_id_tbl (Table of Detail Match IDs)
 *              I_smatch_match_id_tbl (Table of Summary Match IDs)
 *              I_irip_seq_tbl (Table of Receipt Item Posting Sequences)
 *              I_unmtch_lvl (Doc/Item)
 *              I_unmtch_doc_id
 * 
 * Output param: O_unmtch_comments (List of Validation comments)
 */
FUNCTION VALIDATE_UNMTCH(O_error_message           OUT VARCHAR2,
                         O_unmtch_comments        OUT OBJ_VARCHAR_ID_TABLE,
                         I_unmtch_id           IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                         I_unmtch_lvl          IN     IM_UNMTCH_STATUS.UNMTCH_LVL%TYPE,
                         I_unmtch_doc_id       IN     IM_DOC_HEAD.DOC_ID%TYPE,
                         I_doc_item_tbl        IN     OBJ_NUM_NUM_STR_TBL,
                         I_dmatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE,
                         I_smatch_match_id_tbl IN     OBJ_NUMERIC_ID_TABLE,
                         I_irip_seq_tbl        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.VALIDATE_UNMTCH';

   L_im_ship_item_qtys_tbl IM_SHIP_ITEM_QTYS_TBL := NULL;

   L_posted_invc     CONSTANT VARCHAR2(30) := 'POSTED_INVOICE';
   L_delete_invc     CONSTANT VARCHAR2(30) := 'DELETE_INVOICE';
   L_matched_outside CONSTANT VARCHAR2(30) := 'MATCHED_OUTSIDE';
   L_tax_resolved    CONSTANT VARCHAR2(30) := 'TAX_RESOLVED';
   L_child_posted    CONSTANT VARCHAR2(30) := 'CHILD_POSTED';
   L_child_matched   CONSTANT VARCHAR2(30) := 'CHILD_MATCHED';
   L_child_dwnloaded CONSTANT VARCHAR2(30) := 'CHILD_DWNLD';
   L_rca_mismatch    CONSTANT VARCHAR2(30) := 'RCA_MISMATCH';
   L_rua_negative    CONSTANT VARCHAR2(30) := 'RUA_NEGATIVE';
   L_smry_mtch       CONSTANT VARCHAR2(30) := 'SMRY_MATCHED';
   L_rcpt_writeoff   CONSTANT VARCHAR2(30) := 'RCPT_WRITEOFF';

   cursor C_FETCH_SHIP_ITEMS is
      select IM_SHIP_ITEM_QTYS_REC(inner.shipment,
                                   inner.item,
                                   inner.qty_matched)
        from (select /*+ CARDINALITY(seq, 10) */ irip.shipment,
                     irip.item,
                     SUM(irip.qty_matched) qty_matched
                from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) seq,
                     im_receipt_item_posting irip
               where irip.seq_no = value(seq)
               GROUP BY irip.shipment,
                        irip.item) inner;

   cursor C_FETCH_UNMTCH_CMNTS is
--      with docs as (select doc_id
--                      from TABLE(CAST(I_dmatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) d_ids
--                           im_detail_match_invc_history idmih
--                     where idmih.match_id = value(d_ids)
--                    union all
--                    select doc_id
--                      from TABLE(CAST(I_smatch_match_id_tbl as OBJ_NUMERIC_ID_TABLE)) s_ids
--                           im_summary_match_invc_history ismih
--                     where ismih.match_id = value(s_ids))
      with docs as (select /*+ CARDINALITY(doc_item, 10) */ distinct number_1 doc_id
                      from TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item
                    union all
                    select I_unmtch_doc_id doc_id
                      from dual)
      select L_posted_invc cmnt
        from dual
       where EXISTS (select 'x'
                       from docs d,
                            im_doc_head idh
                      where idh.doc_id = d.doc_id
                        and idh.status = REIM_CONSTANTS.DOC_STATUS_POSTED)
      union all
      select L_delete_invc cmnt
        from dual
       where EXISTS (select 'x'
                       from docs d,
                            im_doc_head idh
                      where idh.doc_id = d.doc_id
                        and idh.status = REIM_CONSTANTS.DOC_STATUS_DELETE)
      union all
      select L_matched_outside cmnt
        from dual
       where EXISTS (select 'x'
                       from docs d,
                            im_doc_head idh
                      where idh.doc_id     = d.doc_id
                        and idh.match_type NOT IN (REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                   REIM_CONSTANTS.MATCH_TYPE_MANUAL))
      union all
      select L_tax_resolved cmnt
        from dual
       where EXISTS (select /*+ CARDINALITY(doc_item, 10) */ 'x'
                       from TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item,
                            im_tax_discrepancy_history itdh,
                            im_resolution_action ira
                      where itdh.doc_id = doc_item.number_1
                        and itdh.item   = doc_item.string
                        and ira.doc_id  = itdh.doc_id
                        and ira.item    = itdh.item
                        and ira.action  IN ('DMTI',
                                            'DMTF',
                                            'CNRTI',
                                            'CNRTF')
                     union all
                     select 'x'
                       from im_tax_discrepancy_history itdh,
                            im_resolution_action ira
                      where I_unmtch_lvl = REIM_CONSTANTS.UNMTCH_LVL_DOC
                        and itdh.doc_id  = I_unmtch_doc_id
                        and ira.doc_id   = itdh.doc_id
                        and ira.item     = itdh.item
                        and ira.action   IN ('DMTI',
                                             'DMTF',
                                             'CNRTI',
                                             'CNRTF'))
      union all
      select L_child_posted cmnt
        from dual
       where EXISTS (select 'x'
                       from docs d,
                            im_doc_head idh
                      where idh.ref_doc = d.doc_id
                        and idh.status  = REIM_CONSTANTS.DOC_STATUS_POSTED)
      union all
      select L_child_matched cmnt
        from dual
       where EXISTS (select 'x'
                       from docs d,
                            im_doc_head idh
                      where idh.ref_doc = d.doc_id
                        and idh.status  = REIM_CONSTANTS.DOC_STATUS_MTCH)
      union all
      select L_child_dwnloaded cmnt
        from dual
       where EXISTS (select 'x'
                       from docs d,
                            im_doc_head idh
                      where idh.ref_doc          = d.doc_id
                        and idh.edi_download_ind = 'Y')
      union all
      select L_smry_mtch cmnt
        from dual
       where I_unmtch_lvl = REIM_CONSTANTS.UNMTCH_LVL_ITEM
         and EXISTS (select 'x'
                       from docs d,
                            im_summary_match_invc_history ismih
                      where ismih.doc_id = d.doc_id)
      union all
      select L_rcpt_writeoff cmnt
        from dual
       where EXISTS (select /*+ CARDINALITY(seq, 10) */ 'x'
                       from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) seq,
                            im_receipt_item_posting irip,
                            shipment sh
                      where irip.seq_no          = value(seq)
                        and sh.shipment          = irip.shipment
                        and sh.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_CLOSED)
      union all
      select L_rca_mismatch cmnt
        from dual
       where EXISTS (select /*+ CARDINALITY(doc_item, 10) CARDINALITY(ids, 10) */ 'x'
                       from im_transform_shipsku_gtt gtt,
                            TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item,
                            TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_resolution_action ira,
                            im_rcpt_item_posting_invoice iripi,
                            im_receipt_item_posting irip,
                            im_receiver_cost_adjust irca
                      where ira.doc_id              = doc_item.number_1
                        and ira.item                = doc_item.string
                        and ira.action              IN (REIM_CONSTANTS.RC_ACTION_RCA,
                                                        REIM_CONSTANTS.RC_ACTION_RCAS,
                                                        REIM_CONSTANTS.RC_ACTION_RCAMR,
                                                        REIM_CONSTANTS.RC_ACTION_RCAMRS)
                        and iripi.seq_no            = value(ids)
                        and irip.seq_no             = iripi.seq_no
                        and ira.doc_id              = iripi.doc_id
                        and irca.shipment           = irip.shipment
                        and irca.item               = irip.item
                        and irca.reason_code_id     = ira.reason_code_id
                        and irip.shipment           = gtt.shipment
                        and irip.item               = gtt.item
                        and irca.adjusted_unit_cost <> gtt.transform_unit_cost)
      union all
      select L_rua_negative cmnt
        from dual
       where EXISTS (select /*+ CARDINALITY(doc_item, 10) CARDINALITY(ids, 10) */ 'x'
                       from (select gtt.shipment,
                                    gtt.item,
                                    SUM(NVL(gtt.transform_qty_received, 0)) qty_received
                               from im_transform_shipsku_gtt gtt
                              GROUP BY gtt.shipment,
                                       gtt.item) sh_dest,
                            TABLE(CAST(I_doc_item_tbl as OBJ_NUM_NUM_STR_TBL)) doc_item,
                            TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_resolution_action ira,
                            im_rcpt_item_posting_invoice iripi,
                            im_receipt_item_posting irip,
                            im_receiver_unit_adjust irua
                      where ira.doc_id          = doc_item.number_1
                        and ira.item            = doc_item.string
                        and ira.action          = REIM_CONSTANTS.RC_ACTION_RUA
                        and iripi.seq_no        = value(ids)
                        and irip.seq_no         = iripi.seq_no
                        and ira.doc_id          = iripi.doc_id
                        and irua.shipment       = irip.shipment
                        and irua.item           = irip.item
                        and irua.reason_code_id = ira.reason_code_id
                        and irip.shipment       = sh_dest.shipment
                        and irip.item           = sh_dest.item
                        and REIM_CONSTANTS.ZERO > sh_dest.qty_received - irua.adjusted_item_qty);

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id
                                    ||' I_unmtch_lvl: '        || I_unmtch_lvl
                                    ||' I_unmtch_doc_id: '     || I_unmtch_doc_id);

   open C_FETCH_SHIP_ITEMS;
   fetch C_FETCH_SHIP_ITEMS BULK COLLECT into L_im_ship_item_qtys_tbl;
   close C_FETCH_SHIP_ITEMS;

   if REIM_XFORM_SHIPSKU_SQL.POP_TRANSFORM_SHIPSKU_GTT(O_error_message,
                                                       L_im_ship_item_qtys_tbl) = REIM_CONSTANTS.FAIL then
       return REIM_CONSTANTS.FAIL;
   end if;

   open C_FETCH_UNMTCH_CMNTS;
   fetch C_FETCH_UNMTCH_CMNTS BULK COLLECT into O_unmtch_comments;
   close C_FETCH_UNMTCH_CMNTS;

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END VALIDATE_UNMTCH;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Add the unmatched receipt to the matching workspace
 * Input param: I_irip_seq_tbl (Table of Receipt Item Posting Sequence Numbers)
 */
FUNCTION ADD_UNMTCH_SHIPMENT(O_error_message    OUT VARCHAR2,
                             I_unmtch_id     IN     IM_UNMTCH_STATUS.UNMTCH_ID%TYPE,
                             I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                             I_irip_seq_tbl  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_UNDO_MATCH_SQL.ADD_UNMTCH_SHIPMENT';

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_unmtch_id: ' || I_unmtch_id
                                    ||' I_workspace_id: '       || I_workspace_id);


   merge into im_match_rcpt_ws tgt
   using (with irip_ship as (select /*+ CARDINALITY(ids, 10) */ distinct irip.shipment
                               from TABLE(CAST(I_irip_seq_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                    im_receipt_item_posting irip
                              where irip.shipment = value(ids)),
               locs as (select s.store loc,
                               vs.store_name loc_name,
                               s.vat_region
                          from store s,
                               v_store_tl vs
                         where vs.store = s.store
                        union all
                        select w.wh loc,
                               vw.wh_name loc_name,
                               w.vat_region
                          from wh w,
                               v_wh_tl vw
                         where vw.wh = w.wh),
               set_of_books as (select mls.set_of_books_id,
                                       ol.order_no
                                  from mv_loc_sob mls,
                                       (select order_no,
                                               MIN(location) location
                                          from ordloc
                                         GROUP BY order_no) ol
                                 where mls.location      = ol.location
                                   and mls.location_type in ('W','S'))
          select I_workspace_id workspace_id,
                 sh.shipment,
                 s.supplier_parent supplier,
                 tl_s.sup_name supplier_name,
                 oh.supplier supplier_site_id,
                 tl_s.sup_name supplier_site_name,
                 sh.order_no,
                 sh.asn,
                 sh.bill_to_loc,
                 sh.bill_to_loc_type,
                 bl.loc_name bill_to_loc_name,
                 sh.to_loc ship_to_loc,
                 sh.to_loc_type ship_to_loc_type,
                 sl.loc_name ship_to_loc_name,
                 sh.status_code,
                 sh.invc_match_status,
                 sh.receive_date,
                 oh.currency_code,
                 0 total_avail_cost,--merge later
                 0 total_avail_qty,--merge later
                 0 merch_amount,--merge later
                 0 total_qty,--merge later
                 'Y' choice_flag,
                 sob.set_of_books_id loc_set_of_books_id,
                 bl.vat_region loc_vat_region,
                 s.vat_region supplier_site_vat_region,
                 sh.ship_date
            from irip_ship irips,
                 shipment sh,
                 ordhead oh,
                 sups s,
                 v_sups_tl tl_s,
                 v_sups_tl tl_ss,
                 locs bl,
                 locs sl,
                 set_of_books sob
           where sh.shipment    = irips.shipment
             and oh.order_no    = sh.order_no
             and s.supplier     = oh.supplier
             and tl_s.supplier  = s.supplier_parent
             and tl_ss.supplier = s.supplier
             and bl.loc         = sh.bill_to_loc
             and sl.loc         = sh.to_loc
             and sob.order_no   = oh.order_no) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment)
   when NOT MATCHED THEN
      insert (workspace_id,
              shipment,
              supplier,
              supplier_name,
              supplier_site_id,
              supplier_site_name,
              order_no,
              asn,
              bill_to_loc,
              bill_to_loc_type,
              bill_to_loc_name,
              ship_to_loc,
              ship_to_loc_type,
              ship_to_loc_name,
              status_code,
              invc_match_status,
              receive_date,
              currency_code,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              choice_flag,
              loc_set_of_books_id,
              loc_vat_region,
              supplier_site_vat_region,
              ship_date)
      values (src.workspace_id,
              src.shipment,
              src.supplier,
              src.supplier_name,
              src.supplier_site_id,
              src.supplier_site_name,
              src.order_no,
              src.asn,
              src.bill_to_loc,
              src.bill_to_loc_type,
              src.bill_to_loc_name,
              src.ship_to_loc,
              src.ship_to_loc_type,
              src.ship_to_loc_name,
              src.status_code,
              src.invc_match_status,
              src.receive_date,
              src.currency_code,
              src.total_avail_cost,
              src.total_avail_qty,
              src.merch_amount,
              src.total_qty,
              src.choice_flag,
              src.loc_set_of_books_id,
              src.loc_vat_region,
              src.supplier_site_vat_region,
              src.ship_date);

   LOGGER.LOG_INFORMATION(L_program||'Insert selected reecipts from SEARCH_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      LOG_UNMTCH_STATUS(O_error_message,
                        I_unmtch_id,
                        STATUS_FAILED,
                        NULL,
                        NULL,
                        NULL);
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END ADD_UNMTCH_SHIPMENT;
----------------------------------------------------------------------------------------------
END REIM_UNDO_MATCH_SQL;
/
