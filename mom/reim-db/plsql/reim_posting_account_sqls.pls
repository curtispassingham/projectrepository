CREATE OR REPLACE PACKAGE REIM_POSTING_ACCOUNT_SQL AS
--------------------------------------------------------------------------------
/**    
 * The public function used for loading Posting accounts. Dynamic segments are evaluated if required. 
 *
 * Input param: I_posting_id (The ID of the Posting Process)
 *              I_force_dynamic (Indicator which instructs the function to load dynamic values regardless of IM_GL_OPTIONS' Dynamic Indicator)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION LOAD_POSTING_ACCOUNTS(O_error_message    OUT VARCHAR2,
                               I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                               I_force_dynamic IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
/**
 * The public function used to retrieve accounts for validation. 
 * It then Validates the accounts against IM_VALID_ACCOUNTS.
 * If not VALID, collects them and sends them back for validation  
 *
 * Output param: O_accounts (Collection of accounts that require validation)
 *
 * Input param: I_posting_id (The ID of the Posting Process)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION RETRIEVE_ACCTS_FOR_VALIDATION(O_error_message    OUT VARCHAR2,
                                       O_accounts         OUT OBJ_REIM_POSTING_ACCT_TBL,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
/**    
 * The public function used to update status of accounts in IM_POSTING_DOC_ACCOUNTS based on input collection of accounts.
 * It raises a posting error if the status is not VALID
 *
 * Input param: I_posting_id (The ID of the Posting Process)
 *              I_accounts (Collection of accounts used to update IM_POSTING_DOC_ACCOUNTS)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION UPDATE_POSTING_ACCOUNTS_STATUS(O_error_message    OUT VARCHAR2,
                                        I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                        I_accounts      IN     OBJ_REIM_POSTING_ACCT_TBL)

RETURN NUMBER;
--------------------------------------------------------------------------------
END REIM_POSTING_ACCOUNT_SQL;
/
