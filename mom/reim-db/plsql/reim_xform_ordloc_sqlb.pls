CREATE OR REPLACE PACKAGE BODY REIM_XFORM_ORDLOC_SQL AS

/**
 * The public function used for Transforming Shipsku records to be used in ReIM.
 * Uses im_transform_ordloc_gtt.
 *
 * Returns 1 on Success
 *         0 on Failure
 */

FUNCTION TRANSFORM_ORDLOC_GTT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_XFORM_ORDLOC_SQL.TRANSFORM_ORDLOC_GTT';
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;

/* Assumes the im_transform_ordloc_gtt table is populated prior to calling this function.

 Name                    Null?    Type
 ----------------------- -------- ----------------
 ORDER_NO                         NUMBER(12)
 ITEM                             VARCHAR2(25)
 ITEM_DESC                        VARCHAR2(250)
 LOC                              NUMBER(10)
 LOC_TYPE                         VARCHAR2(1)
 QTY_ORDERED                      NUMBER(12,4)
 UNIT_COST                        NUMBER(20,4)
 UNIT_COST_INIT                   NUMBER(20,4)
 COST_SOURCE                      VARCHAR2(4)
 SUP_QTY_LEVEL                    VARCHAR2(6)
 CATCH_WEIGHT_TYPE                VARCHAR2(2)
 IMPORT_LOC                       NUMBER(10)
 IMPORT_LOC_TYPE                  VARCHAR2(1)
 TRANSFORM_QTY_ORDERED            NUMBER(12,4)
 TRANSFORM_UNIT_COST              NUMBER(20,4)
 TRANSFORM_UNIT_COST_INI          NUMBER(20,4)
*/

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program);

   --catch_weight_type 3,1,NULL and sup_qty_level = 'CA'/'EA'
   merge into im_transform_ordloc_gtt target
   using (select gtt.order_no,
                 gtt.item,
                 gtt.loc,
                 --
                 NVL(oh.import_id, gtt.loc) import_loc,
                 --
                 case when oh.import_id is not null then
                    case when oh.import_type in ('M','X','F') then
                       'W'
                    else
                       oh.import_type
                    end
                 else
                    gtt.loc_type
                 end import_loc_type,
                 --
                 (gtt.qty_ordered)    / decode(gtt.sup_qty_level, 'CA', os.supp_pack_size, 1) transform_qty_ordered,
                 (gtt.unit_cost)      * decode(gtt.sup_qty_level, 'CA', os.supp_pack_size, 1) transform_unit_cost,
                 (gtt.unit_cost_init) * decode(gtt.sup_qty_level, 'CA', os.supp_pack_size, 1) transform_unit_cost_init
            from im_transform_ordloc_gtt gtt,
                 ordhead oh,
                 ordsku os
           where (   gtt.catch_weight_type IN ('3',
                                               '1')
                  or gtt.catch_weight_type is NULL)
             and gtt.order_no            = oh.order_no
             and gtt.order_no            = os.order_no
             and gtt.item                = os.item
   ) use_this
   on (    target.order_no = use_this.order_no
       and target.item     = use_this.item
       and target.loc      = use_this.loc)
   when matched then update
    set target.transform_qty_ordered    = use_this.transform_qty_ordered,
        target.transform_unit_cost      = use_this.transform_unit_cost,
        target.transform_unit_cost_init = use_this.transform_unit_cost_init,
        target.import_loc               = use_this.import_loc,
        target.import_loc_type          = use_this.import_loc_type;


   --catch_weight_type 4
   merge into im_transform_ordloc_gtt target
   using (select gtt.order_no,
                 gtt.item,
                 gtt.loc,
                 --
                 NVL(oh.import_id, gtt.loc) import_loc,
                 --
                 case when oh.import_id is not null then
                    case when oh.import_type in ('M','X','F') then
                       'W'
                    else
                       oh.import_type
                    end
                 else
                    gtt.loc_type
                 end import_loc_type,
                 --
                 (gtt.qty_ordered    * iscd.net_weight / NVL (isc.supp_pack_size, 1) ) transform_qty_ordered,
                 (gtt.unit_cost      / NVL (iscd.net_weight, 1) * isc.supp_pack_size ) transform_unit_cost,
                 (gtt.unit_cost_init / NVL (iscd.net_weight, 1) * isc.supp_pack_size ) transform_unit_cost_init
            from im_transform_ordloc_gtt gtt,
                 packitem pi,
                 ordhead oh,
                 ordsku os,
                 item_supp_country_dim iscd,
                 item_supp_country isc
           where gtt.catch_weight_type   = '4'
             and gtt.item                = pi.pack_no
             and gtt.order_no            = oh.order_no
             and gtt.order_no            = os.order_no
             and gtt.item                = os.item
             --
             and oh.supplier             = isc.supplier
             and os.item                 = isc.item
             and os.origin_country_id    = isc.origin_country_id
             --
             and iscd.supplier           = oh.supplier
             and iscd.item               = os.item
             and iscd.origin_country     = os.origin_country_id
             and iscd.dim_object         = 'CA'
   ) use_this
   on (    target.order_no = use_this.order_no
       and target.item     = use_this.item
       and target.loc      = use_this.loc)
   when matched then update
    set target.transform_qty_ordered    = use_this.transform_qty_ordered,
        target.transform_unit_cost      = use_this.transform_unit_cost,
        target.transform_unit_cost_init = use_this.transform_unit_cost_init,
        target.import_loc               = use_this.import_loc,
        target.import_loc_type          = use_this.import_loc_type;

   --catch_weight_type 2
   merge into im_transform_ordloc_gtt target
   using (select gtt.order_no,
                 gtt.item,
                 gtt.loc,
                 --
                 NVL(oh.import_id, gtt.loc) import_loc,
                 --
                 case when oh.import_id is not null then
                    case when oh.import_type in ('M','X','F') then
                       'W'
                    else
                       oh.import_type
                    end
                 else
                    gtt.loc_type
                 end import_loc_type,
                 --
                 (gtt.qty_ordered    * pi.pack_qty) transform_qty_ordered,
                 (gtt.unit_cost      / pi.pack_qty) transform_unit_cost,
                 (gtt.unit_cost_init / pi.pack_qty) transform_unit_cost_init
            from im_transform_ordloc_gtt gtt,
                 packitem pi,
                 ordhead oh
           where gtt.catch_weight_type   = '2'
             and gtt.item                = pi.pack_no
             and gtt.order_no            = oh.order_no
   ) use_this
   on (    target.order_no = use_this.order_no
       and target.item     = use_this.item
       and target.loc      = use_this.loc)
   when matched then update
    set target.transform_qty_ordered    = use_this.transform_qty_ordered,
        target.transform_unit_cost      = use_this.transform_unit_cost,
        target.transform_unit_cost_init = use_this.transform_unit_cost_init,
        target.import_loc               = use_this.import_loc,
        target.import_loc_type          = use_this.import_loc_type;

   LOGGER.LOG_INFORMATION('End ' || L_program);
   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;

END TRANSFORM_ORDLOC_GTT;
------------------------------------------------------------------------------------------
/**
 * The public function used for Transforming Shipsku records to be used in ReIM.
 * Uses im_transform_ordloc_gtt.
 *
 * Returns 1 on Success
 *         0 on Failure
 */

FUNCTION TRANSFORM_ORDLOC_GTT(O_error_message     IN OUT VARCHAR2,
                              I_order_sup_items   IN     OBJ_NUM_NUM_STR_TBL)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_XFORM_ORDLOC_SQL.TRANSFORM_ORDLOC_GTT';
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;

/*--------------------------------------
 * INPUT:
 *   OBJ_NUM_NUM_STR_TBL TABLE OF OBJ_NUM_NUM_STR_REC
 *   Name          Type
 *   -------------------------
 *   NUMBER_1      NUMBER(30)     --ORDER_NO
 *   NUMBER_2      NUMBER(30)     --SUPPLIER
 *   STRING        VARCHAR2(255)  --ITEM
 *-------------------------------------*/
BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program);

   delete from im_transform_ordloc_gtt;

   insert into im_transform_ordloc_gtt (order_no,
                                        item,
                                        item_desc,
                                        loc,
                                        loc_type,
                                        qty_ordered,
                                        unit_cost,
                                        unit_cost_init,
                                        cost_source,
                                        sup_qty_level,
                                        catch_weight_type,
                                        import_loc,
                                        import_loc_type,
                                        transform_qty_ordered,
                                        transform_unit_cost,
                                        transform_unit_cost_init)
   select distinct ol.order_no,
          ol.item,
          im.item_desc,
          ol.location,
          ol.loc_type,
          min(ol.qty_ordered)    over (partition by ol.order_no, ol.item, ol.location) qty_ordered,
          min(ol.unit_cost)      over (partition by ol.order_no, ol.item, ol.location) unit_cost,
          min(ol.unit_cost_init) over (partition by ol.order_no, ol.item, ol.location) unit_cost_init,
          min(ol.cost_source)    over (partition by ol.order_no, ol.item, ol.location) cost_source,
          s.sup_qty_level,
          im.catch_weight_type,
          ol.location,
          ol.loc_type,
          min(ol.qty_ordered)    over (partition by ol.order_no, ol.item, ol.location) tsf_qty_ordered,
          min(ol.unit_cost)      over (partition by ol.order_no, ol.item, ol.location) tsf_unit_cost,
          min(ol.unit_cost_init) over (partition by ol.order_no, ol.item, ol.location) tsf_unit_cost_init
     from table(cast(I_order_sup_items as OBJ_NUM_NUM_STR_TBL)) order_sup_items,
          ordloc ol,
          sups s,
          item_master im
    where order_sup_items.number_1 = ol.order_no
      and order_sup_items.number_2 = s.supplier
      and order_sup_items.string   = ol.item
      and ol.item                  = im.item;

   LOGGER.LOG_INFORMATION(L_program||'insert into im_transform_ordloc_gtt - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   if REIM_XFORM_ORDLOC_SQL.TRANSFORM_ORDLOC_GTT(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   LOGGER.LOG_INFORMATION('End ' || L_program);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;

END TRANSFORM_ORDLOC_GTT;
------------------------------------------------------------------------------------------
END REIM_XFORM_ORDLOC_SQL;
/
