create or replace PACKAGE BODY       REIM_AUTO_MATCH_SQL AS

/**
 *  -------- PRIVATE PROCS AND FUNCTIONS SPECS --------------
 */
------------------------------------------------------------------------------------------
/**
 * The private procedure used for logging Auto match status information in IM_AUTO_MATCH_STATUS.
 */
PROCEDURE LOG_AUTO_MATCH_STATUS(O_error_message IN OUT VARCHAR2,
                                I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                I_status        IN     IM_AUTO_MATCH_STATUS.STATUS%TYPE);
------------------------------------------------------------------------------------------
/**
 * The private procedure used for logging Auto match task status information in IM_AUTO_MATCH_TASK_STATUS.
 */
PROCEDURE LOG_AUTO_MATCH_TASK_STATUS(O_error_message       IN OUT VARCHAR2,
                                     I_workspace_id        IN     IM_AUTO_MATCH_TASK_STATUS.WORKSPACE_ID%TYPE,
                                     I_match_stgy_dtl_id   IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_STGY_DTL_ID%TYPE,
                                     I_chunk_num           IN     IM_AUTO_MATCH_TASK_STATUS.CHUNK_NUM%TYPE,
                                     I_match_luw_id        IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                     I_task_level          IN     IM_AUTO_MATCH_TASK_STATUS.TASK_LEVEL%TYPE,
                                     I_task_type           IN     IM_AUTO_MATCH_TASK_STATUS.TASK_TYPE%TYPE,
                                     I_doc_count           IN     IM_AUTO_MATCH_TASK_STATUS.DOC_COUNT%TYPE,
                                     I_summary_match_count IN     IM_AUTO_MATCH_TASK_STATUS.SUMMARY_MATCH_COUNT%TYPE,
                                     I_detail_match_count  IN     IM_AUTO_MATCH_TASK_STATUS.DETAIL_MATCH_COUNT%TYPE,
                                     I_disc_item_count     IN     IM_AUTO_MATCH_TASK_STATUS.DISC_ITEM_COUNT%TYPE,
                                     I_disc_reslvd_count   IN     IM_AUTO_MATCH_TASK_STATUS.DISC_RESLVD_COUNT%TYPE,
                                     I_status              IN     IM_AUTO_MATCH_TASK_STATUS.STATUS%TYPE);
------------------------------------------------------------------------------------------
/**
 * The private function that rolls up cost and qty from child tables to group head table.
 * Used for Summary One to Many (Batch) and Suggest Match (Online) functionalities.
 */
FUNCTION ROLLUP_COST_AND_QTY(O_error_message IN OUT VARCHAR2,
                             I_workspace_id  IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                             I_match_luw_id  IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                             I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                             I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * Performs Cost pre match for invoices.
 */
FUNCTION PERFORM_COST_PRE_MATCH(O_error_message    OUT VARCHAR2,
                                I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
FUNCTION UPD_TAXDIS_SMRY(O_error_message     IN OUT VARCHAR2,
                         I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                         I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                         I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                         I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
FUNCTION PERFORM_SKU_LVL_DETL_MATCH(O_error_message        OUT VARCHAR2,
                                    I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                    I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                    I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE, -- 'R'-Regular  or 'B'-Best Match
                                    I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                    I_chunk_num         IN     IM_AUTO_MATCH_TASK_STATUS.CHUNK_NUM%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
FUNCTION PERFORM_STYLE_LVL_DETL_MATCH(O_error_message        OUT VARCHAR2,
                                      I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                      I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                      I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE, -- 'P'- Parent
                                      I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                      I_chunk_num         IN     IM_AUTO_MATCH_TASK_STATUS.CHUNK_NUM%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
FUNCTION PERSIST_SKU_LVL_DETL_MATCH(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                    I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
FUNCTION PERSIST_STYLE_LVL_DETL_MATCH(O_error_message    OUT VARCHAR2,
                                      I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                      I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
FUNCTION PRORATE_STYLE_LVL_QTY_VAR(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                   I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 *  --------- PUBLIC PROCS AND FUNCTIONS BODIES --------------
 */

/**
 * The public function used for Initializing the Match Process
 * Input param: I_supplier_id (The Supplier which needs to be included/excluded)
 *              I_incl_excl (Indicates whether to 'I'nclude or 'E'xclude)
 *              I_cost_pre_match_ind (Indicates whether to perform cost pre match logic or not. Y/N)
 *
 * Output param: O_workspace_id (Workspace id for the match process)
 *               O_auto_match_chunk_tbl (Table of LUWs and Chunks to be processed by the Batch)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION INIT_MATCH(O_error_message           OUT VARCHAR2,
                    O_workspace_id            OUT IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                    O_auto_match_chunk_tbl    OUT IM_AUTO_MATCH_LUW_TBL,
                    I_supplier_id          IN     SUPS.SUPPLIER%TYPE,
                    I_incl_excl            IN     VARCHAR2,
                    I_cost_pre_match_ind   IN     VARCHAR2 DEFAULT 'Y')
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.INIT_MATCH';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   program_exception EXCEPTION;

   L_vdate                 PERIOD.VDATE%TYPE                     := GET_VDATE;
   L_default_match_stgy_id IM_MATCH_STGY_HEAD.MATCH_STGY_ID%TYPE := NULL;
   L_luw_auto_match        IM_SYSTEM_OPTIONS.LUW_AUTO_MATCH%TYPE := NULL;
   L_multi_curr_match_keys OBJ_NUMERIC_ID_TABLE                  := NULL;
   L_num_incl_suppliers    IM_BATCH_CONFIG.SUPPLIER%TYPE         := NULL;
   L_auto_match_chunk_tbl  IM_AUTO_MATCH_LUW_TBL                 := NULL;
   L_doc_count             NUMBER(10)                            := 0;

   cursor C_FETCH_DEFAULT_MATCH_STGY is
      select match_stgy_id
        from im_match_stgy_head
       where system_default = 'Y'
         and rownum         = REIM_CONSTANTS.ONE;

   cursor C_LUW_CHUNK is
      select IM_AUTO_MATCH_LUW_REC(gtt1.workspace_id,
                                   gtt1.match_stgy_id,
                                   gtt1.match_luw_id,
                                   CAST(MULTISET(select IM_AUTO_MATCH_CHUNK_REC(1) from dual) as IM_AUTO_MATCH_CHUNK_TBL))
        from (select distinct gtt.workspace_id,
                              gtt.match_luw_id,
                              gtt.match_stgy_id
                from im_match_chunk_1_gtt gtt
               where rownum > 0) gtt1;

   cursor C_FETCH_LUWS_WITH_STGY is
   SELECT IM_AUTO_MATCH_LUW_REC(luw_tbl.WORKSPACE_ID,
                                luw_tbl.MATCH_STGY_ID,
                                luw_tbl.MATCH_LUW_ID,
                                luw_tbl.chunk_tbl)
     from TABLE(CAST(L_auto_match_chunk_tbl AS IM_AUTO_MATCH_LUW_TBL)) luw_tbl,
          TABLE(luw_tbl.CHUNK_TBL) chunks_tbl
    WHERE luw_tbl.match_stgy_id is NOT NULL;


BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_supplier_id: ' || I_supplier_id
                          || ' I_incl_excl: ' || I_incl_excl);


   O_workspace_id := IM_WORKSPACE_ID_SEQ.NEXTVAL;
   LOGGER.LOG_INFORMATION('O_workspace_id is '||O_workspace_id);

   LOG_AUTO_MATCH_STATUS(O_error_message,
                         O_workspace_id,
                         STATUS_INIT);

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              O_workspace_id,
                              NULL, NULL, NULL,
                              TASK_LVL_BATCH,
                              TASK_TYPE_INIT,
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_STARTED);

   open C_FETCH_DEFAULT_MATCH_STGY;
   fetch C_FETCH_DEFAULT_MATCH_STGY into L_default_match_stgy_id;
   close C_FETCH_DEFAULT_MATCH_STGY;

   select luw_auto_match
     into L_luw_auto_match
     from im_system_options;

   -- setup list of valid suppliers to be processed in this run.
   delete from gtt_6_num_6_str_6_date;

   insert into gtt_6_num_6_str_6_date (number_1,         --supplier_site
                                       number_2,         --supplier_parent
                                       number_3,         --MATCH_STGY_ID
                                       number_4,         --GROUP_ID
                                       varchar2_1,       --MATCH_KEY
                                       varchar2_2,       --TOTAL_QTY_REQUIRED_IND
                                       varchar2_3,       --MATCH_TOTAL_QTY_IND
                                       number_5)       --SKU_COMP_PERCENT
   select distinct sups.supplier,
          sups.supplier_parent,
          iso.match_stgy_id,
          iso.group_id,
          iso.match_key,
          iso.total_qty_required_ind,
          iso.match_total_qty_ind,
          iso.sku_comp_percent
     from im_batch_config ibc,
          sups,
          v_im_supp_site_attrib_expl iso
    where UPPER(ibc.batch_name) = 'REIMAUTOMATCH'
      and ibc.scope             = 'I'
      and ibc.processed         = 'N'
      and ibc.supplier          = sups.supplier_parent
      and sups.supplier         = iso.supplier;

   select COUNT(*)
     into L_num_incl_suppliers
     from gtt_6_num_6_str_6_date;

   if L_num_incl_suppliers = 0 then

      insert into gtt_6_num_6_str_6_date (number_1,         --supplier_site
                                          number_2,         --supplier_parent
                                          number_3,         --MATCH_STGY_ID
                                          number_4,         --GROUP_ID
                                          varchar2_1,       --MATCH_KEY
                                          varchar2_2,       --TOTAL_QTY_REQUIRED_IND
                                          varchar2_3,       --MATCH_TOTAL_QTY_IND
                                          number_5)       --SKU_COMP_PERCENT
      select distinct sups.supplier,
             sups.supplier_parent,
             iso.match_stgy_id,
             iso.group_id,
             iso.match_key,
             iso.total_qty_required_ind,
             iso.match_total_qty_ind,
             iso.sku_comp_percent
        from sups,
             v_im_supp_site_attrib_expl iso
       where supplier_parent is NOT NULL
         and sups.supplier   = iso.supplier;

   end if;

   delete from gtt_6_num_6_str_6_date
    where number_2 in(select supplier
                        from im_batch_config ibc
                       where UPPER(ibc.batch_name) = 'REIMAUTOMATCH'
                         and ibc.scope             = 'E'
                         and ibc.processed         = 'N');

   delete from im_match_invc_gtt;

   insert into im_match_invc_gtt(workspace_id,
                                 doc_id,
                                 doc_head_version_id, --to be taken from im_doc_head.object_version_id
                                 match_stgy_id,
                                 match_key,
                                 status,
                                 doc_date,
                                 due_date,
                                 supplier_group_id,
                                 supplier,
                                 po_supplier,
                                 supplier_site_id,
                                 po_supplier_site_id,
                                 currency_code,
                                 order_no,
                                 location,
                                 loc_type,
                                 --location_name,
                                 total_avail_cost,
                                 total_avail_qty,
                                 merch_amount,
                                 total_qty,
                                 cost_pre_match,
                                 header_only,
                                 summary_mtch_elig,
                                 detail_mtch_elig,
                                 qty_required,
                                 qty_match_required,
                                 sku_comp_percent,
                                 choice_flag)
      select O_workspace_id,
             idh.doc_id,
             idh.object_version_id doc_head_version_id,
             NVL(incl_sups.number_3, L_default_match_stgy_id),
             incl_sups.varchar2_1 match_key,
             DECODE(idh.status,
                    REIM_CONSTANTS.DOC_STATUS_TAXDIS, REIM_CONSTANTS.DOC_STATUS_RMTCH,
                    idh.status) status,
             idh.doc_date,
             idh.due_date,
             incl_sups.number_4 group_id,
             idh.vendor,
             incl_sups.number_2 supplier_parent,
             idh.supplier_site_id,
             oh.supplier,
             idh.currency_code,
             idh.order_no,
             idh.location,
             idh.loc_type,
             --loc.loc_name,
             idh.total_cost total_avail_cost, -- merge later
             idh.total_qty total_avail_qty, --merge later
             --idh.total_cost_inc_tax,
             idh.total_cost merch_amount, -- merge later
             idh.total_qty,
             idh.cost_pre_match,
             'Y', -- header_only, merge later
             'Y', -- summary_mtch_elig, (Yes initially)
             'Y', -- detail_mtch_elig, merge later
             NVL(incl_sups.varchar2_2, 'N'), -- to be confirmed
             NVL(incl_sups.varchar2_3, 'N'), -- to be confirmed
             NVL(incl_sups.number_5, REIM_CONSTANTS.ZERO),
             'Y'  -- choice_flag
        from gtt_6_num_6_str_6_date incl_sups,
             ordhead oh,
             im_doc_head idh,
             (select store loc,
                     REIM_CONSTANTS.LOC_TYPE_STORE loc_type
                from store
              union all
              select wh loc,
                     REIM_CONSTANTS.LOC_TYPE_WH loc_type
                from wh
               where wh = physical_wh) loc
       where idh.type                    = REIM_CONSTANTS.DOC_TYPE_MRCHI
         and (   idh.status              IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                             REIM_CONSTANTS.DOC_STATUS_URMTCH)
              or (    idh.status         = REIM_CONSTANTS.DOC_STATUS_TAXDIS
                  and idh.header_only    = 'Y'))
         and idh.total_cost              > REIM_CONSTANTS.ZERO
         and oh.order_no                 = idh.order_no
         and incl_sups.number_1          = oh.supplier
         and loc.loc                     = idh.location
         and loc.loc_type                = idh.loc_type
         and NOT EXISTS (select 'x'
                           from im_manual_group_invoices imgi
                          where imgi.doc_id = idh.doc_id);

   LOGGER.LOG_INFORMATION('Insert im_match_invc_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_gtt tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 SUM(idnm.non_merch_amt) non_merch_amt
            from im_match_invc_gtt imiw,
                 im_doc_non_merch idnm
           where imiw.workspace_id = O_workspace_id
             and idnm.doc_id = imiw.doc_id
           group by imiw.workspace_id,
                    imiw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.merch_amount     = tgt.merch_amount - src.non_merch_amt,
             tgt.total_avail_cost = tgt.total_avail_cost - src.non_merch_amt;

   LOGGER.LOG_INFORMATION('Merge im_match_invc_gtt NON_MERCH_AMT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_key_gtt;

   insert into im_match_key_gtt(workspace_id,
                                match_key_value,
                                match_key_id)
     select workspace_id,
            match_key_value,
            IM_MATCH_KEY_ID_SEQ.NEXTVAL match_key_id
       from (select distinct imiw.workspace_id,
                    imiw.match_key || '^' || DECODE(imiw.match_key,
                                                    REIM_CONSTANTS.MATCH_KEY_PO_LOC, imiw.order_no||'^'||imiw.location,
                                                    REIM_CONSTANTS.MATCH_KEY_PO,imiw.order_no,
                                                    REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC, imiw.po_supplier_site_id||'^'||imiw.location,
                                                    REIM_CONSTANTS.MATCH_KEY_SUPP_SITE, imiw.po_supplier_site_id,
                                                    REIM_CONSTANTS.MATCH_KEY_SUPP_LOC, imiw.po_supplier||'^'||imiw.location,
                                                    REIM_CONSTANTS.MATCH_KEY_SUPPLIER, imiw.po_supplier,
                                                    REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, imiw.supplier_group_id||'^'||imiw.location,
                                                    REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP, imiw.supplier_group_id) match_key_value
               from im_match_invc_gtt imiw
              where imiw.workspace_id = O_workspace_id);

   LOGGER.LOG_INFORMATION('Insert im_match_key_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_gtt tgt
   using (select gtt.workspace_id,
                 gtt.match_key_value,
                 gtt.match_key_id
            from im_match_key_gtt gtt) src
   on (    tgt.workspace_id                                                                        = src.workspace_id
       and tgt.match_key ||'^'||DECODE(tgt.match_key,
                                       REIM_CONSTANTS.MATCH_KEY_PO_LOC, tgt.order_no||'^'||tgt.location,
                                       REIM_CONSTANTS.MATCH_KEY_PO, tgt.order_no,
                                       REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC, tgt.po_supplier_site_id||'^'||tgt.location,
                                       REIM_CONSTANTS.MATCH_KEY_SUPP_SITE, tgt.po_supplier_site_id,
                                       REIM_CONSTANTS.MATCH_KEY_SUPP_LOC, tgt.po_supplier||'^'||tgt.location,
                                       REIM_CONSTANTS.MATCH_KEY_SUPPLIER, tgt.po_supplier,
                                       REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, tgt.supplier_group_id||'^'||tgt.location,
                                       REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP, tgt.supplier_group_id) = src.match_key_value)
   when MATCHED then
      update
         set tgt.match_key_id = src.match_key_id;

   LOGGER.LOG_INFORMATION('Update im_match_invc_gtt MATCH_KEY_ID - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_gtt tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 RANK() OVER (PARTITION BY match_key_id
                                  ORDER BY imiw.due_date,
                                           imiw.merch_amount desc,
                                           imiw.doc_id) rank_in_match_key
            from im_match_invc_gtt imiw
           where imiw.workspace_id = O_workspace_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.rank_in_match_key = src.rank_in_match_key;

   LOGGER.LOG_INFORMATION('Rank im_match_invc_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_invc_key_gtt;
   insert into im_match_invc_key_gtt(workspace_id,
                                     match_key,
                                     match_key_id,
                                     supplier_group_id,
                                     po_supplier,
                                     po_supplier_site_id,
                                     order_no,
                                     location)
                              select distinct imiw.workspace_id,
                                              imiw.match_key,
                                              imiw.match_key_id,
                                              DECODE(imiw.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP,     imiw.supplier_group_id,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, imiw.supplier_group_id,
                                                     NULL) supplier_group_id,
                                              DECODE(imiw.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPPLIER, imiw.po_supplier,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_LOC, imiw.po_supplier,
                                                     NULL) po_supplier,
                                              DECODE(imiw.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_SITE,     imiw.po_supplier_site_id,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC, imiw.po_supplier_site_id,
                                                     NULL) po_supplier_site_id,
                                              DECODE(imiw.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_PO_LOC, imiw.order_no,
                                                     REIM_CONSTANTS.MATCH_KEY_PO,     imiw.order_no,
                                                     NULL) order_no,
                                              DECODE(imiw.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_PO_LOC,         imiw.location,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC,  imiw.location,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_LOC,       imiw.location,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, imiw.location,
                                                     NULL) location
                                from im_match_invc_gtt imiw
                               where imiw.workspace_id = O_workspace_id;

   LOGGER.LOG_INFORMATION('Insert im_match_invc_key_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_rcpt_gtt(workspace_id,
                                  shipment,
                                  match_key_id,
                                  order_no,
                                  supplier_site_id,
                                  bill_to_loc,
                                  bill_to_loc_type,
                                  invc_match_status,
                                  receive_date,
                                  currency_code,
                                  total_avail_cost,
                                  total_avail_qty,
                                  merch_amount,
                                  total_qty,
                                  choice_flag)
              with order_ws as (select imiw.workspace_id,
                                       imiw.match_key_id,
                                       oh.order_no,
                                       oh.supplier,
                                       DECODE(imiw.match_key,
                                              REIM_CONSTANTS.MATCH_KEY_PO_LOC, imiw.location,
                                              NULL) location,
                                       oh.currency_code
                                  from im_match_invc_key_gtt imiw,
                                       ordhead oh
                                 where imiw.order_no  = oh.order_no
                                   and imiw.match_key IN (REIM_CONSTANTS.MATCH_KEY_PO,
                                                          REIM_CONSTANTS.MATCH_KEY_PO_LOC)
                                union all
                                select imiw.workspace_id,
                                       imiw.match_key_id,
                                       oh.order_no,
                                       oh.supplier,
                                       DECODE(imiw.match_key,
                                              REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC, imiw.location,
                                              NULL) location,
                                       oh.currency_code
                                  from im_match_invc_key_gtt imiw,
                                       ordhead oh
                                 where oh.supplier     = imiw.po_supplier_site_id
                                   and imiw.match_key IN (REIM_CONSTANTS.MATCH_KEY_SUPP_SITE,
                                                          REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC)
                                union all
                                select imiw.workspace_id,
                                       imiw.match_key_id,
                                       oh.order_no,
                                       oh.supplier,
                                       DECODE(imiw.match_key,
                                              REIM_CONSTANTS.MATCH_KEY_SUPP_LOC, imiw.location,
                                              NULL) location,
                                       oh.currency_code
                                  from im_match_invc_key_gtt imiw,
                                       sups s,
                                       ordhead oh
                                 where s.supplier_parent = imiw.po_supplier
                                   and oh.supplier       = s.supplier
                                   and imiw.match_key    IN (REIM_CONSTANTS.MATCH_KEY_SUPPLIER,
                                                             REIM_CONSTANTS.MATCH_KEY_SUPP_LOC)
                                union all
                                select imiw.workspace_id,
                                       imiw.match_key_id,
                                       oh.order_no,
                                       oh.supplier,
                                       DECODE(imiw.match_key,
                                              REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, imiw.location,
                                              NULL) location,
                                       oh.currency_code
                                  from im_match_invc_key_gtt imiw,
                                       im_supplier_group_members isgm,
                                       sups s,
                                       ordhead oh
                                 where isgm.group_id     = imiw.supplier_group_id
                                   and s.supplier_parent = isgm.supplier
                                   and oh.supplier       = s.supplier
                                   and imiw.match_key    IN (REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP,
                                                             REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC))
                         select ows.workspace_id,
                                sh.shipment,
                                ows.match_key_id,
                                ows.order_no,
                                ows.supplier,
                                sh.bill_to_loc,
                                sh.bill_to_loc_type,
                                sh.invc_match_status,
                                sh.receive_date,
                                ows.currency_code,
                                0 total_avail_cost, --to be merged later
                                0 total_avail_qty, --to be merged later
                                0 merch_amount,  -- to be merged later
                                0 total_qty,     -- to be merged later
                                'Y'
                           from order_ws ows,
                                shipment sh
                          where sh.order_no          = ows.order_no
                            and sh.bill_to_loc       = NVL(ows.location, sh.bill_to_loc)
                            and sh.status_code       IN (REIM_CONSTANTS.SHIP_STATUS_UNMTCH,
                                                         REIM_CONSTANTS.SHIP_STATUS_RECEIVED,
                                                         REIM_CONSTANTS.SHIP_STATUS_CANCELLED)
                            and sh.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                            and sh.receive_date      is NOT NULL
                            and NOT EXISTS (select 'x'
                                              from im_manual_group_receipts imgr
                                             where imgr.shipment = sh.shipment);


   LOGGER.LOG_INFORMATION('Insert im_match_rcpt_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_invc_detl_gtt(workspace_id,
                                     doc_id,
                                     item,
                                     invc_detl_version_id, --to be taken from im_invoice_detail.object_version_id
                                     match_key_id,
                                     chunk_num,
                                     match_luw_id,
                                     status,
                                     unit_cost,
                                     invoice_qty,
                                     cost_matched,
                                     qty_matched,
                                     choice_flag,
                                     ordloc_unit_cost)
                              --
                              --SHIPMENT EXISTS
                              select distinct imiw.workspace_id,
                                     imiw.doc_id,
                                     iid.item,
                                     nvl(iid.object_version_id,0) invc_detl_version_id,
                                     imiw.match_key_id,
                                     imiw.chunk_num,
                                     imiw.match_luw_id,
                                     iid.status,
                                     iid.resolution_adjusted_unit_cost,
                                     iid.resolution_adjusted_qty invoice_qty,
                                     iid.cost_matched,
                                     iid.qty_matched,
                                     'Y',
                                     min(ol.unit_cost) over (partition by imiw.order_no, iid.item, imiw.location)
                                from im_match_invc_gtt imiw,
                                     ordhead oh,
                                     shipment sh,
                                     im_invoice_detail iid,
                                     (select store loc, store phy_loc from store
                                      union all
                                      select wh loc, physical_wh phy_loc from wh) loc,
                                     ordloc ol
                               where imiw.workspace_id = O_workspace_id
                                 and imiw.order_no     = oh.order_no(+)
                                 and oh.import_id      is null
                                 and sh.order_no       = oh.order_no(+)
                                 and sh.bill_to_loc    = imiw.location
                                 --
                                 and iid.doc_id        = imiw.doc_id
                                 and iid.status        = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                                 --
                                 and imiw.match_key   != REIM_CONSTANTS.MATCH_KEY_PO
                                 --
                                 and imiw.order_no     = ol.order_no(+)
                                 and imiw.location     = loc.phy_loc
                                 and loc.loc           = ol.location(+)
                                 and iid.item          = ol.item(+)
                                 and exists (select 'x' from shipment where order_no = oh.order_no)
                              --
                              --NO SHIPMENT
                              union all
                              select distinct imiw.workspace_id,
                                     imiw.doc_id,
                                     iid.item,
                                     nvl(iid.object_version_id,0) invc_detl_version_id,
                                     imiw.match_key_id,
                                     imiw.chunk_num,
                                     imiw.match_luw_id,
                                     iid.status,
                                     iid.resolution_adjusted_unit_cost,
                                     iid.resolution_adjusted_qty invoice_qty,
                                     iid.cost_matched,
                                     iid.qty_matched,
                                     'Y',
                                     min(ol.unit_cost) over (partition by imiw.order_no, iid.item, imiw.location)
                                from im_match_invc_gtt imiw,
                                     ordhead oh,
                                     im_invoice_detail iid,
                                     ordloc ol
                               where imiw.workspace_id = O_workspace_id
                                 and imiw.order_no     = oh.order_no
                                 and oh.import_id      is null
                                 --
                                 and iid.doc_id        = imiw.doc_id
                                 and iid.status        = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                                 --
                                 and imiw.match_key   != REIM_CONSTANTS.MATCH_KEY_PO
                                 --
                                 and imiw.order_no     = ol.order_no
                                 and iid.item          = ol.item
                                 and not exists (select 'x' from shipment where order_no = oh.order_no)
                              --IMPORT
                              union all
                              select distinct imiw.workspace_id,
                                     imiw.doc_id,
                                     iid.item,
                                     nvl(iid.object_version_id,0) invc_detl_version_id,
                                     imiw.match_key_id,
                                     imiw.chunk_num,
                                     imiw.match_luw_id,
                                     iid.status,
                                     iid.resolution_adjusted_unit_cost,
                                     iid.resolution_adjusted_qty invoice_qty,
                                     iid.cost_matched,
                                     iid.qty_matched,
                                     'Y',
                                     min(ol.unit_cost) over (partition by imiw.order_no, iid.item, imiw.location)
                                from im_match_invc_gtt imiw,
                                     ordhead oh,
                                     im_invoice_detail iid,
                                     ordloc ol
                               where imiw.workspace_id = O_workspace_id
                                 and imiw.order_no     = oh.order_no
                                 and oh.import_id      is not null
                                 and iid.doc_id        = imiw.doc_id
                                 and iid.status        = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                                 --
                                 and imiw.match_key   != REIM_CONSTANTS.MATCH_KEY_PO
                                 --
                                 and imiw.order_no     = ol.order_no
                                 and iid.item          = ol.item
                              --
                              --PO match key
                              union all
                              select imiw.workspace_id,
                                     imiw.doc_id,
                                     iid.item,
                                     nvl(iid.object_version_id,0) invc_detl_version_id,
                                     imiw.match_key_id,
                                     imiw.chunk_num,
                                     imiw.match_luw_id,
                                     iid.status,
                                     iid.resolution_adjusted_unit_cost,
                                     iid.resolution_adjusted_qty invoice_qty,
                                     iid.cost_matched,
                                     iid.qty_matched,
                                     'Y',
                                     null ordloc_unit_cost
                                from im_match_invc_gtt imiw,
                                     im_invoice_detail iid
                               where imiw.workspace_id = O_workspace_id
                                 and iid.doc_id        = imiw.doc_id
                                 and iid.status        = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                                 and imiw.match_key    = REIM_CONSTANTS.MATCH_KEY_PO;

   LOGGER.LOG_INFORMATION('Insert im_match_invc_detl_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_detl_gtt tgt
   using (select distinct gtt.workspace_id,
                 gtt.doc_id,
                 gtt.item,
                 min(ol.unit_cost) over (partition by gtt.workspace_id, gtt.doc_id, gtt.item) ordloc_unit_cost,
                 count(distinct ol.unit_cost) over (partition by gtt.workspace_id, gtt.doc_id, gtt.item) cost_cnt
            from im_match_invc_detl_gtt gtt,
                 im_match_invc_gtt imiw,
                 ordloc ol
           where gtt.workspace_id = O_workspace_id
             and gtt.workspace_id = imiw.workspace_id
             and gtt.doc_id       = imiw.doc_id
             and imiw.match_key   = REIM_CONSTANTS.MATCH_KEY_PO
             and imiw.order_no    = ol.order_no
             and gtt.item         = ol.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item
       and src.cost_cnt     = 1)
   when MATCHED then update
         set tgt.ordloc_unit_cost = src.ordloc_unit_cost;

   LOGGER.LOG_INFORMATION('Merge im_match_invc_detl_gtt PO ordloc_unit_cost - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_detl_gtt tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 iid.item,
                 iid.object_version_id invc_detl_version_id,
                 imiw.match_key_id,
                 imiw.chunk_num,
                 imiw.match_luw_id,
                 iid.status,
                 iid.resolution_adjusted_unit_cost unit_cost,
                 iid.resolution_adjusted_qty invoice_qty,
                 iid.cost_matched,
                 iid.qty_matched,
                 'Y' choice_flag,
                 min(ol.unit_cost) ordloc_unit_cost
            from im_match_invc_gtt imiw,
                 im_invoice_detail iid,
                 ordloc ol,
                 item_master im_iid,
                 item_master im_ol
           where imiw.workspace_id  = O_workspace_id
             and iid.doc_id         = imiw.doc_id
             and iid.status         = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
             and ol.order_no        = imiw.order_no
             and iid.item           = im_iid.item
             and ol.item            = im_ol.item
             and im_iid.item_parent = im_ol.item_parent
           GROUP BY imiw.workspace_id,
                    imiw.doc_id,
                    iid.item,
                    iid.object_version_id,
                    imiw.match_key_id,
                    imiw.chunk_num,
                    imiw.match_luw_id,
                    iid.status,
                    iid.resolution_adjusted_unit_cost,
                    iid.resolution_adjusted_qty,
                    iid.cost_matched,
                    iid.qty_matched
             HAVING count(distinct ol.unit_cost) = 1) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              item,
              invc_detl_version_id,
              match_key_id,
              chunk_num,
              match_luw_id,
              status,
              unit_cost,
              invoice_qty,
              cost_matched,
              qty_matched,
              choice_flag,
              ordloc_unit_cost)
      values (src.workspace_id,
              src.doc_id,
              src.item,
              src.invc_detl_version_id,
              src.match_key_id,
              src.chunk_num,
              src.match_luw_id,
              src.status,
              src.unit_cost,
              src.invoice_qty,
              src.cost_matched,
              src.qty_matched,
              src.choice_flag,
              src.ordloc_unit_cost);

   LOGGER.LOG_INFORMATION('Merge im_match_invc_detl_gtt for items not on PO (Style Level ordloc_unit_cost) - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   
   merge into im_match_invc_detl_gtt tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 iid.item,
                 iid.object_version_id invc_detl_version_id,
                 imiw.match_key_id,
                 imiw.chunk_num,
                 imiw.match_luw_id,
                 iid.status,
                 iid.resolution_adjusted_unit_cost unit_cost,
                 iid.resolution_adjusted_qty invoice_qty,
                 iid.cost_matched,
                 iid.qty_matched,
                 'Y' choice_flag,
                 isc.unit_cost ordloc_unit_cost
            from im_match_invc_gtt imiw,
                 im_invoice_detail iid,
                 im_match_invc_detl_gtt imid,
                 item_supp_country isc
           where imiw.workspace_id  = O_workspace_id
             and iid.doc_id         = imiw.doc_id
             and iid.status         = 'UNMTCH'
             and iid.item           = isc.item
             and imiw.supplier_site_id  = isc.supplier
             and imid.workspace_id  = imiw.workspace_id
             and imid.doc_id        = imiw.doc_id
             and imid.item          = iid.item
             and nvl(imid.ordloc_unit_cost,0)=0
          ) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item )
          when MATCHED then update
         set tgt.ordloc_unit_cost = src.ordloc_unit_cost;
         
         LOGGER.LOG_INFORMATION('Merge im_match_invc_detl_gtt for items not on PO  - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_invc_detl_gtt gtt
    where gtt.rowid in(select g.rowid
                         from im_match_invc_detl_gtt g,
                              im_match_invc_gtt imiw
                        where g.workspace_id     = O_workspace_id
                          and g.workspace_id     = imiw.workspace_id
                          and g.doc_id           = imiw.doc_id
                          and imiw.match_key     = REIM_CONSTANTS.MATCH_KEY_PO
                          and g.ordloc_unit_cost is null);

   LOGGER.LOG_INFORMATION('delete im_match_invc_detl_gtt no ordloc_unit_cost - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(number_1)
      select distinct i2.match_key_id
        from (select match_key_id,
                     COUNT(distinct i.currency_code) OVER (PARTITION BY i.match_key_id) curr_count,
                     COUNT(distinct i.vat_region) OVER (PARTITION BY i.match_key_id) vat_count,
                     COUNT(distinct i.set_of_books_id) OVER (PARTITION BY i.match_key_id) sob_count
                from (with loc as (select store loc,
                                          REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                          vat_region
                                     from store
                                   union all
                                   select wh loc,
                                          REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                          vat_region
                                     from wh
                                    where wh = physical_wh),
                       ord_loc as (select order_no,
                                          MIN(location) location
                                     from ordloc
                                    GROUP BY order_no)
                      select distinct
                             iw.match_key_id,
                             iw.currency_code,
                             nvl(l.vat_region,-1) vat_region,
                             nvl(so.set_of_books_id,-1) set_of_books_id
                        from im_match_invc_gtt iw,
                             loc l,
                             ord_loc ol,
                             mv_loc_sob so
                       where iw.workspace_id = O_workspace_id
                         and iw.location     = l.loc
                         and iw.order_no     = ol.order_no
                         and ol.location     = so.location
                      union all
                      select distinct
                             rw.match_key_id,
                             rw.currency_code,
                             nvl(l.vat_region,-1) vat_region,
                             nvl(so.set_of_books_id,-1) set_of_books_id
                        from im_match_rcpt_gtt rw,
                             loc l,
                             ord_loc ol,
                             mv_loc_sob so
                       where rw.workspace_id = O_workspace_id
                         and rw.bill_to_loc  = l.loc
                         and rw.order_no     = ol.order_no
                         and ol.location     = so.location) i) i2
       where curr_count > 1
          or vat_count  > 1
          or sob_count  > 1;

   LOGGER.LOG_INFORMATION('Identify Multi currency/vat_region/SOB Match Keys - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_match_invc_detl_gtt
    where match_key_id in(select number_1 from gtt_num_num_str_str_date_date);

   LOGGER.LOG_INFORMATION('Delete Multi currency/vat_region/SOB Match Keys from INVC_DETL_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_match_invc_gtt
    where match_key_id in(select number_1 from gtt_num_num_str_str_date_date);

   LOGGER.LOG_INFORMATION('Delete Multi currency/vat_region/SOB Match Keys from INVC_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_match_rcpt_gtt
    where match_key_id in(select number_1 from gtt_num_num_str_str_date_date);

   LOGGER.LOG_INFORMATION('Delete Multi currency/vat_region/SOB Match Keys from RCPT_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_gtt tgt
   using (select imidw.workspace_id,
                 imidw.doc_id,
                 SUM(DECODE(imidw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imidw.invoice_qty,
                            REIM_CONSTANTS.ZERO) * imidw.unit_cost) unmatch_item_amt,
                 SUM(DECODE(imidw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imidw.invoice_qty,
                            REIM_CONSTANTS.ZERO)) unmatch_item_qty
            from im_match_invc_detl_gtt imidw
           where imidw.workspace_id  = O_workspace_id
           GROUP BY imidw.workspace_id,
                    imidw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.unmatch_item_amt,
             tgt.total_avail_qty  = src.unmatch_item_qty,
             tgt.header_only  = 'N';

   LOGGER.LOG_INFORMATION('Merge im_match_invc_gtt AVAILABLE COST AND QTY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_gtt tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 case
                    when imiw.header_only = 'Y' then
                       'N'
                    else
                       case
                          when L_vdate >= (imiw.due_date - sys_opt.days_before_due_date) then
                             'Y'
                          else
                             case
                                when L_vdate >= (imiw.doc_date + supp_opt.qty_disc_day_before_rte) then
                                   'Y'
                                else
                                   'N'
                             end
                       end
                 end detail_mtch_elig
            from im_match_invc_gtt imiw,
                 im_system_options sys_opt,
                 im_supplier_options supp_opt
           where imiw.workspace_id = O_workspace_id
             and supp_opt.supplier = imiw.supplier) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.detail_mtch_elig = src.detail_mtch_elig;

   LOGGER.LOG_INFORMATION('Merge im_match_invc_gtt DETAIL_MTCH_ELIG - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_transform_shipsku_gtt;

   insert /*+ PARALLEL */ into im_transform_shipsku_gtt (shipment,
                                         item,
                                         seq_no,
                                         ss_qty_received,
                                         ss_qty_matched,
                                         ss_unit_cost,
                                         weight_received,
                                         weight_received_uom,
                                         carton,
                                         catch_weight_type,
                                         order_no,
                                         supplier,
                                         sup_qty_level,
                                         transform_qty_received,
                                         transform_qty_matched,
                                         transform_unit_cost)
                                  select
                     imrw.shipment,
                                         ss.item,
                                         ss.seq_no,
                                         ss.qty_received,
                                         ss.qty_matched,
                                         ss.unit_cost,
                                         ss.weight_received,
                                         ss.weight_received_uom,
                                         ss.carton,
                                         im.catch_weight_type,
                                         oh.order_no,
                                         oh.supplier,
                                         s.sup_qty_level,
                                         ss.qty_received,
                                         ss.qty_matched,
                                         ss.unit_cost
                                    from im_match_rcpt_gtt imrw,
                                         shipsku ss,
                                         ordhead oh,
                                         sups s,
                                         item_master im
                                   where imrw.workspace_id                                               = O_workspace_id
                                     and ss.shipment                                                     = imrw.shipment
                                     and nvl(ss.invc_match_status, REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH) = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
                                     and oh.order_no                                                     = imrw.order_no
                                     and s.supplier                                                      = oh.supplier
                                     and im.item                                                         = ss.item;

   LOGGER.LOG_INFORMATION('Insert Shipsku xform gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_XFORM_SHIPSKU_SQL.TRANSFORM_SHIPSKU_GTT(O_error_message) = REIM_CONSTANTS.FAIL then
       raise program_exception;
   end if;

   insert into im_match_rcpt_detl_gtt(workspace_id,
                                       shipment,
                                       item,
                                       match_key_id,
                                       invc_match_status,
                                       unit_cost,
                                       qty_received,
                                       qty_available,
                                       unit_cost_nc,
                                       qty_available_nc,
                                       catch_weight_type,
                                       choice_flag)
        with im_xform_shipsku_gtt as (select gtt.shipment,
                                             gtt.item,
                                             gtt.transform_unit_cost,
                                             SUM(NVL(gtt.transform_qty_received, 0)) qty_received,
                                             SUM(NVL(gtt.transform_qty_received, 0) - NVL(ipmr.qty_matched, 0)) qty_available,
                                             gtt.ss_unit_cost unit_cost_nc,
                                             SUM(NVL(gtt.ss_qty_received, 0) - NVL(gtt.ss_qty_matched, 0)) qty_available_nc,
                                             gtt.catch_weight_type
                                        from im_transform_shipsku_gtt gtt,
                                             im_partially_matched_receipts ipmr
                                       where ipmr.shipment (+) = gtt.shipment
                                         and ipmr.item (+)     = gtt.item
                                       GROUP BY gtt.shipment,
                                                gtt.item,
                                                gtt.transform_unit_cost,
                                                gtt.ss_unit_cost,
                                                gtt.catch_weight_type)
                              select imrw.workspace_id,
                                     imrw.shipment,
                                     gtt.item,
                                     imrw.match_key_id,
                                     REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH,
                                     gtt.transform_unit_cost,
                                     gtt.qty_received,
                                     gtt.qty_available,
                                     gtt.unit_cost_nc,
                                     gtt.qty_available_nc,
                                     gtt.catch_weight_type,
                                     'Y'
                                from im_match_rcpt_gtt imrw,
                                     im_xform_shipsku_gtt gtt
                               where imrw.workspace_id = O_workspace_id
                                 and gtt.shipment      = imrw.shipment;

   LOGGER.LOG_INFORMATION('Insert im_match_rcpt_detl_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_gtt tgt
   using (select imrdw.workspace_id,
                 imrdw.shipment,
                 SUM(imrdw.unit_cost * imrdw.qty_available) total_avail_cost,
                 SUM(imrdw.qty_available) total_avail_qty
            from im_match_rcpt_detl_gtt imrdw
           where imrdw.workspace_id = O_workspace_id
           GROUP BY imrdw.workspace_id,
                    imrdw.shipment) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.total_avail_cost,
             tgt.total_avail_qty  = src.total_avail_qty;

   LOGGER.LOG_INFORMATION('Merge cost-qty im_match_rcpt_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_gtt tgt
   using (with im_mtch_invc_ws as (select distinct imiw.workspace_id,
                                          imiw.match_key_id,
                                          imiw.match_key,
                                          imiw.supplier_group_id,
                                          imiw.currency_code,
                                          Decode(imiw.match_key,
                                                 REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP,     NULL,
                                                 REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, NULL,
                                                 imiw.po_supplier) supplier,
                                          Decode(imiw.match_key,
                                                 REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP,     NULL,
                                                 REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, NULL,
                                                 REIM_CONSTANTS.MATCH_KEY_SUPPLIER,       NULL,
                                                 REIM_CONSTANTS.MATCH_KEY_SUPP_LOC,       NULL,
                                                 REIM_CONSTANTS.MATCH_KEY_PO,             imiw.po_supplier_site_id,
                                                 REIM_CONSTANTS.MATCH_KEY_PO_LOC,         imiw.po_supplier_site_id,
                                                 imiw.po_supplier_site_id) supplier_site_id,
                                          Decode(imiw.match_key,
                                                 REIM_CONSTANTS.MATCH_KEY_PO,     oh.dept,
                                                 REIM_CONSTANTS.MATCH_KEY_PO_LOC, oh.dept,
                                                 NULL) dept
                                     from im_match_invc_gtt imiw,
                                          ordhead oh
                                    where imiw.workspace_id = O_workspace_id
                                      and oh.order_no       = imiw.order_no),
               im_tolerance_ws as (select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          decode(tolerance_level_type,
                                                 REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_SITE, REIM_CONSTANTS.TLR_RANK_SUPP_SITE,
                                                 REIM_CONSTANTS.TLR_LVL_TYPE_SUPP,      REIM_CONSTANTS.TLR_RANK_SUPP,
                                                 REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_GRP,  REIM_CONSTANTS.TLR_RANK_SUPP_GRP,
                                                 REIM_CONSTANTS.TLR_LVL_TYPE_DEPT,      REIM_CONSTANTS.TLR_RANK_DEPT) tolerance_rank
                                     from im_tolerance_level_map
                                   union all
                                   select NULL tolerance_level_type,
                                          NULL tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SYSTEM tolerance_rank
                                     from im_tolerance_head
                                    where system_default = 'Y'),
               im_curr_conv_ws as (select from_currency,
                                          to_currency,
                                          effective_date,
                                          exchange_rate,
                                          exchange_type
                                     from (select from_currency,
                                                  to_currency,
                                                  effective_date,
                                                  exchange_rate,
                                                  exchange_type,
                                                  RANK() OVER (PARTITION BY from_currency,
                                                                            to_currency,
                                                                            exchange_type
                                                                   ORDER BY effective_date desc) date_rank
                                             from mv_currency_conversion_rates
                                            where exchange_type  = REIM_CONSTANTS.EXCH_TYPE_CONSOLIDATED
                                              and effective_date <= L_vdate)
                                     where date_rank = REIM_CONSTANTS.ONE)
          select inner.workspace_id,
                 inner.match_key_id,
                 inner.tolerance_id,
                 RANK() OVER (PARTITION BY inner.match_key_id
                                  ORDER BY inner.tolerance_rank desc) tolerance_rank,
                 ith.currency_code tolerance_currency_code,
                 iccw.exchange_rate tolerance_exchange_rate -- Add conversion logic
            from (select imiw.workspace_id,
                         imiw.match_key_id,
                         itw.tolerance_id,
                         imiw.currency_code doc_currency_code,
                         itw.tolerance_rank
                    from im_tolerance_ws itw,
                         im_mtch_invc_ws imiw
                   where itw.tolerance_level_id = DECODE(itw.tolerance_level_type,
                                                         REIM_CONSTANTS.TLR_LVL_TYPE_DEPT,      imiw.dept,
                                                         REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_SITE, imiw.supplier_site_id,
                                                         REIM_CONSTANTS.TLR_LVL_TYPE_SUPP,      imiw.supplier,
                                                         REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_GRP,  imiw.supplier_group_id)
                  union all
                  select imiw.workspace_id,
                         imiw.match_key_id,
                         itw.tolerance_id,
                         imiw.currency_code doc_currency_code,
                         itw.tolerance_rank
                    from im_tolerance_ws itw,
                         im_mtch_invc_ws imiw
                   where itw.tolerance_rank = REIM_CONSTANTS.ONE) inner,
                 im_tolerance_head ith,
                 im_curr_conv_ws iccw
           where inner.tolerance_id        = ith.tolerance_id
             and iccw.from_currency        = inner.doc_currency_code
             and iccw.to_currency          = ith.currency_code) src
   on (    tgt.workspace_id   = src.workspace_id
       and tgt.match_key_id   = src.match_key_id
       and src.tolerance_rank = REIM_CONSTANTS.ONE)
   when MATCHED then
      update
         set tgt.tolerance_id            = src.tolerance_id,
             tgt.tolerance_currency_code = src.tolerance_currency_code,
             tgt.tolerance_exchange_rate = src.tolerance_exchange_rate;

   LOGGER.LOG_INFORMATION('Merge Tolerance im_match_invc_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_chunk_1_gtt;

   insert into im_match_chunk_1_gtt (workspace_id,
                                     match_stgy_id,
                                     match_key_id,
                                     match_key_count,
                                     match_luw_id)
   select workspace_id,
          match_stgy_id,
          match_key_id,
          total_count,
          match_luw_id
     from (select imiw.workspace_id,
                  imiw.match_stgy_id,
                  imiw.match_key_id,
                  imiw.invc_count,
                  imrdw.rcpt_detl_count,
                  invc_count * rcpt_detl_count total_count
             from (select imiw.workspace_id,
                          imiw.match_key_id,
                          imiw.match_stgy_id,
                          count(1) invc_count
                     from im_match_invc_gtt imiw
                    where imiw.workspace_id = O_workspace_id
                    GROUP BY imiw.workspace_id,
                             imiw.match_key_id,
                             imiw.match_stgy_id) imiw,
                  (select imrdw.workspace_id,
                          imrdw.match_key_id,
                          count(1) rcpt_detl_count
                     from im_match_rcpt_detl_gtt imrdw
                    where imrdw.workspace_id = O_workspace_id
                    GROUP BY imrdw.match_key_id,
                             imrdw.workspace_id) imrdw
            where imiw.match_key_id = imrdw.match_key_id) ws
    model
       dimension by (ROW_NUMBER() OVER (ORDER BY ws.workspace_id,
                                                 ws.match_stgy_id,
                                                 ws.match_key_id) rn)
       measures (SUM(ws.invc_count * ws.rcpt_detl_count) OVER (PARTITION BY ws.workspace_id,
                                                                            ws.match_stgy_id,
                                                                            ws.match_key_id) unit_count,
                 ws.workspace_id,
                 ws.match_stgy_id,
                 ws.match_key_id,
                 ws.total_count,
                 ws.invc_count,
                 ws.rcpt_detl_count,
                 1 run_total_luw,
                 1 match_luw_id)
       rules (run_total_luw[rn] =
                 case
                    when CV(rn) = 1 then
                       unit_count[CV(rn)]
                    else
                       case
                          when match_stgy_id[CV(rn)] <> match_stgy_id[CV(rn) - 1] then
                             unit_count[CV(rn)]
                          else
                             case
                                when (unit_count[CV(rn)] + run_total_luw[CV(rn) - 1]) > L_luw_auto_match then
                                   case
                                      when match_key_id[CV(rn)]  = match_key_id[CV(rn) - 1] then
                                         run_total_luw[CV(rn) - 1]
                                      else
                                         unit_count[CV(rn)]
                                   end
                                else
                                   unit_count[CV(rn)] + run_total_luw[CV(rn) - 1]
                             end
                       end
                 end,
              match_luw_id[rn] =
                 case
                    when CV(rn) = 1 then
                       1
                    else
                       case
                          when match_stgy_id[CV(rn)] <> match_stgy_id[CV(rn) - 1] then
                             match_luw_id[CV(rn) - 1] + 1
                          else
                             case
                                when (unit_count[CV(rn)] + run_total_luw[CV(rn) - 1]) > L_luw_auto_match then
                                   case
                                      when match_key_id[CV(rn)]  = match_key_id[CV(rn) - 1] then
                                         match_luw_id[CV(rn) - 1]
                                      else
                                         match_luw_id[CV(rn) - 1] + 1
                                   end
                                else
                                   match_luw_id[CV(rn) - 1]
                             end
                       end
                 end);

   LOGGER.LOG_INFORMATION('Insert LUW im_match_chunk_1_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_invc_ws (workspace_id,
                                 doc_id,
                                 doc_head_version_id,
                                 match_stgy_id,
                                 match_key,
                                 match_key_id,
                                 rank_in_match_key,
                                 status,
                                 doc_date,
                                 due_date,
                                 supplier_group_id,
                                 supplier,
                                 supplier_name,
                                 supplier_phone,
                                 po_supplier,
                                 po_supplier_site_id,
                                 supplier_site_id,
                                 supplier_site_name,
                                 currency_code,
                                 order_no,
                                 location,
                                 loc_type,
                                 location_name,
                                 total_avail_cost,
                                 total_avail_qty,
                                 merch_amount,
                                 total_qty,
                                 cost_pre_match,
                                 header_only,
                                 summary_mtch_elig,
                                 detail_mtch_elig,
                                 qty_required,
                                 qty_match_required,
                                 chunk_num,
                                 match_luw_id,
                                 tolerance_id,
                                 tolerance_currency_code,
                                 tolerance_exchange_rate,
                                 sku_comp_percent,
                                 manual_group_id,
                                 choice_flag)
   select distinct
          tmp.workspace_id,
          tmp.doc_id,
          tmp.doc_head_version_id,
          tmp.match_stgy_id,
          tmp.match_key,
          tmp.match_key_id,
          tmp.rank_in_match_key,
          tmp.status,
          tmp.doc_date,
          tmp.due_date,
          tmp.supplier_group_id,
          tmp.supplier,
          tmp.supplier_name,
          tmp.supplier_phone,
          tmp.po_supplier,
          tmp.po_supplier_site_id,
          tmp.supplier_site_id,
          tmp.supplier_site_name,
          tmp.currency_code,
          tmp.order_no,
          tmp.location,
          tmp.loc_type,
          tmp.location_name,
          tmp.total_avail_cost,
          tmp.total_avail_qty,
          tmp.merch_amount,
          tmp.total_qty,
          tmp.cost_pre_match,
          tmp.header_only,
          tmp.summary_mtch_elig,
          tmp.detail_mtch_elig,
          tmp.qty_required,
          tmp.qty_match_required,
          null chunk_num,
          gtt.match_luw_id,
          tmp.tolerance_id,
          tmp.tolerance_currency_code,
          tmp.tolerance_exchange_rate,
          tmp.sku_comp_percent,
          tmp.manual_group_id,
          tmp.choice_flag
     from (select g.workspace_id,
                  g.match_stgy_id,
                  g.match_key_id,
                  g.match_luw_id
            from im_match_chunk_1_gtt g) gtt,
          im_match_invc_gtt tmp
    where tmp.workspace_id  = O_workspace_id
      and tmp.workspace_id  = gtt.workspace_id(+)
      and tmp.match_stgy_id = gtt.match_stgy_id(+)
      and tmp.match_key_id  = gtt.match_key_id(+);

   LOGGER.LOG_INFORMATION('Insert LUW im_match_invc_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_invc_detl_ws (workspace_id,
                                      doc_id,
                                      item,
                                      invc_detl_version_id,
                                      match_key_id,
                                      chunk_num,
                                      match_luw_id,
                                      status,
                                      unit_cost,
                                      invoice_qty,
                                      cost_matched,
                                      qty_matched,
                                      choice_flag,
                                      ordloc_unit_cost)
                   select tmp.workspace_id,
                          tmp.doc_id,
                          tmp.item,
                          tmp.invc_detl_version_id,
                          tmp.match_key_id,
                          gtt.chunk_num,
                          gtt.match_luw_id,
                          tmp.status,
                          tmp.unit_cost,
                          tmp.invoice_qty,
                          tmp.cost_matched,
                          tmp.qty_matched,
                          tmp.choice_flag,
                          tmp.ordloc_unit_cost
                     from (select g.workspace_id,
                                  g.match_key_id,
                                  g.match_luw_id,
                                  1 chunk_num
                             from im_match_chunk_1_gtt g) gtt,
                          im_match_invc_detl_gtt tmp
                    where tmp.workspace_id  = O_workspace_id
                      and tmp.workspace_id  = gtt.workspace_id(+)
                      and tmp.match_key_id  = gtt.match_key_id(+);

   LOGGER.LOG_INFORMATION('Insert LUW im_match_invc_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_rcpt_ws (workspace_id,
                                 shipment,
                                 match_key_id,
                                 supplier,
                                 supplier_name,
                                 supplier_site_id,
                                 supplier_site_name,
                                 order_no,
                                 asn,
                                 bill_to_loc,
                                 bill_to_loc_type,
                                 bill_to_loc_name,
                                 ship_to_loc,
                                 ship_to_loc_type,
                                 ship_to_loc_name,
                                 status_code,
                                 invc_match_status,
                                 receive_date,
                                 currency_code,
                                 total_avail_cost,
                                 total_avail_qty,
                                 merch_amount,
                                 total_qty,
                                 chunk_num,
                                 match_luw_id,
                                 choice_flag)
   select tmp.workspace_id,
          tmp.shipment,
          tmp.match_key_id,
          tmp.supplier,
          tmp.supplier_name,
          tmp.supplier_site_id,
          tmp.supplier_site_name,
          tmp.order_no,
          tmp.asn,
          tmp.bill_to_loc,
          tmp.bill_to_loc_type,
          tmp.bill_to_loc_name,
          tmp.ship_to_loc,
          tmp.ship_to_loc_type,
          tmp.ship_to_loc_name,
          tmp.status_code,
          tmp.invc_match_status,
          tmp.receive_date,
          tmp.currency_code,
          tmp.total_avail_cost,
          tmp.total_avail_qty,
          tmp.merch_amount,
          tmp.total_qty,
          gtt.chunk_num,
          gtt.match_luw_id,
          tmp.choice_flag
     from (select g.workspace_id,
                  g.match_key_id,
                  g.match_luw_id,
                  1 chunk_num
            from im_match_chunk_1_gtt g) gtt,
          im_match_rcpt_gtt tmp
    where tmp.workspace_id  = gtt.workspace_id
      and tmp.match_key_id  = gtt.match_key_id;

   LOGGER.LOG_INFORMATION('Insert LUW im_match_rcpt_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_rcpt_detl_ws (workspace_id,
                                      shipment,
                                      item,
                                      substitute_item,
                                      match_key_id,
                                      chunk_num,
                                      match_luw_id,
                                      invc_match_status,
                                      unit_cost,
                                      qty_received,
                                      qty_available,
                                      unit_cost_nc,
                                      qty_available_nc,
                                      catch_weight_type,
                                      choice_flag)
   select tmp.workspace_id,
          tmp.shipment,
          tmp.item,
          tmp.substitute_item,
          tmp.match_key_id,
          gtt.chunk_num,
          gtt.match_luw_id,
          tmp.invc_match_status,
          tmp.unit_cost,
          tmp.qty_received,
          tmp.qty_available,
          tmp.unit_cost_nc,
          tmp.qty_available_nc,
          tmp.catch_weight_type,
          tmp.choice_flag
     from (select g.workspace_id,
                  g.match_key_id,
                  g.match_luw_id,
                  1 chunk_num
             from im_match_chunk_1_gtt g
            where g.match_key_count <= L_luw_auto_match) gtt,
          im_match_rcpt_detl_gtt tmp
    where tmp.workspace_id  = gtt.workspace_id
      and tmp.match_key_id  = gtt.match_key_id;

   LOGGER.LOG_INFORMATION('Insert LUW im_match_rcpt_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_cost_pre_match_ind = 'Y' then
      if PERFORM_COST_PRE_MATCH(O_error_message,
                                O_workspace_id) = REIM_CONSTANTS.FAIL then
         raise program_exception;
      end if;
   end if;

   open C_LUW_CHUNK;
   fetch C_LUW_CHUNK BULK COLLECT into L_auto_match_chunk_tbl;
   close C_LUW_CHUNK;

   LOGGER.LOG_INFORMATION('Fetch C_LUW_CHUNK - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_FETCH_LUWS_WITH_STGY;
   fetch C_FETCH_LUWS_WITH_STGY BULK COLLECT into O_auto_match_chunk_tbl;
   close C_FETCH_LUWS_WITH_STGY;

   LOGGER.LOG_INFORMATION('Fetch C_FETCH_LUWS_WITH_STGY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into L_doc_count
     from im_match_invc_ws
    where workspace_id = O_workspace_id;

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              O_workspace_id,
                              NULL, NULL, NULL,
                              TASK_LVL_BATCH,
                              TASK_TYPE_INIT,
                              L_doc_count, NULL, NULL, NULL, NULL,
                              TASK_STATUS_SUCCESS);

   LOG_AUTO_MATCH_STATUS(O_error_message,
                         O_workspace_id,
                         STATUS_PROCESS);

   LOGGER.LOG_INFORMATION('End ' || L_program
                          || ' O_workspace_id: ' || O_workspace_id);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||O_workspace_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when program_exception then
      LOG_AUTO_MATCH_STATUS(O_error_message,
                            O_workspace_id,
                            STATUS_FAILED);
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 O_workspace_id,
                                 NULL, NULL, NULL,
                                 TASK_LVL_BATCH,
                                 TASK_TYPE_INIT,
                                 NULL, NULL, NULL, NULL, NULL,
                                 TASK_STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      LOG_AUTO_MATCH_STATUS(O_error_message,
                            O_workspace_id,
                            STATUS_FAILED);
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 O_workspace_id,
                                 NULL, NULL, NULL,
                                 TASK_LVL_BATCH,
                                 TASK_TYPE_INIT,
                                 NULL, NULL, NULL, NULL, NULL,
                                 TASK_STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
END INIT_MATCH;
------------------------------------------------------------------------------------------
/**
 * The public function used to perform Summary All to All Match
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_stgy_dtl_id (Match Strategy Detail ID)
 *              I_match_luw_id (ID of the Match LUW)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_SMRY_ALL_2_ALL(O_error_message        OUT VARCHAR2,
                                I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER IS
   L_program         VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERFORM_SMRY_ALL_2_ALL';
   L_start_time      TIMESTAMP    := SYSTIMESTAMP;
   L_vdate           DATE         := get_vdate;
   L_doc_count       NUMBER(10)   := 0;
   L_mtch_count      NUMBER(10)   := 0;
   program_exception EXCEPTION;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:'      || I_workspace_id ||
                          ' I_match_luw_id:'      || I_match_luw_id ||
                          ' I_match_stgy_dtl_id:' || I_match_stgy_dtl_id);

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              I_match_stgy_dtl_id, NULL, I_match_luw_id,
                              TASK_LVL_LUW,
                              TASK_TYPE_MATCH,
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_STARTED);

   --assign a match_group_id to each match_key_id in the workspace
   --number_1   = match_key_id
   --number_2   = match_group_id
   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date (number_1, number_2)
   select inner.match_key_id,
          im_match_group_seq.nextval match_group_id
     from (select distinct iw.match_key_id
             from im_match_invc_ws iw
            where iw.workspace_id = I_workspace_id
              and iw.match_luw_id = I_match_luw_id) inner;

   LOGGER.LOG_INFORMATION(L_program||' Insert match_group_id to gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --create im_match_group_head_ws rows for unmatched data
   insert into im_match_group_head_ws (workspace_id,
                                       match_group_id,
                                       match_stgy_id,
                                       match_stgy_dtl_id,
                                       chunk_num,
                                       match_luw_id,
                                       invc_group_id,
                                       rcpt_group_id,
                                       match_type,
                                       match_key_id,
                                       item,
                                       cost_tolerance_dtl_id,
                                       qty_tolerance_dtl_id,
                                       invc_group_cost,
                                       invc_group_qty,
                                       rcpt_group_cost,
                                       rcpt_group_qty,
                                       cost_variance,
                                       qty_variance,
                                       cost_discrepant,
                                       qty_discrepant,
                                       cost_disc_resolved,
                                       qty_disc_resolved,
                                       cost_adjust_rc,
                                       qty_adjust_rc,
                                       sku_compliant,
                                       tax_compliant,
                                       match_available,
                                       match_status,
                                       persist_status,
                                       error_code,
                                       error_context)
     select /*+ INDEX(IW IM_MATCH_INVC_WS_I4) */
            distinct iw.workspace_id,
            gtt.number_2 match_group_id,
            iw.match_stgy_id,
            I_match_stgy_dtl_id match_stgy_dtl_id,
            -1,  --iw.chunk_num,
            iw.match_luw_id,
            null invc_group_id,
            null rcpt_group_id,
            REIM_CONSTANTS.MATCH_LEVEL_SUMM_ALL_2_ALL match_type,
            iw.match_key_id,
            null item,
            nvl(tdc.tolerance_detail_id,-1) cost_tolerance_dtl_id,
            nvl(tdq.tolerance_detail_id,-1) qty_tolerance_dtl_id,
            invc.invc_group_cost,
            invc.invc_group_qty,
            rcpt.rcpt_group_cost,
            rcpt.rcpt_group_qty,
            (rcpt.rcpt_group_cost - invc.invc_group_cost) cost_variance,
            (rcpt.rcpt_group_qty - invc.invc_group_qty) qty_variance,
            --
            case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when tdc.tolerance_value < ABS(100 * (invc.invc_group_cost - rcpt.rcpt_group_cost) / rcpt.rcpt_group_cost) then
                            'Y'
                         else
                            'N'
                    end
                 when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when tdc.tolerance_value < ABS((invc.invc_group_cost - rcpt.rcpt_group_cost) * iw.tolerance_exchange_rate) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when invc.invc_group_cost = rcpt.rcpt_group_cost then
                            'N'
                         else
                            'Y'
                    end
            end cost_discrepant, --target (y/n)
            --
            case when iw.qty_match_required = 'N' then
                    'N'
                 when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when tdq.tolerance_value < ABS(100 * (invc.invc_group_qty - rcpt.rcpt_group_qty) / rcpt.rcpt_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when tdq.tolerance_value < ABS(invc.invc_group_qty - rcpt.rcpt_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when invc.invc_group_qty = rcpt.rcpt_group_qty then
                            'N'
                         else
                            'Y'
                    end
            end qty_discrepant, --target (y/n)
            --
            null cost_disc_resolved,
            null qty_disc_resolved,
            null cost_adjust_rc,
            null qty_adjust_rc,
            'N' sku_compliant,
            'Y' tax_compliant,
            'Y' match_available,
            --
            case when
               case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                       case when tdc.tolerance_value < ABS(100 * (invc.invc_group_cost - rcpt.rcpt_group_cost) / rcpt.rcpt_group_cost) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                       case when tdc.tolerance_value < ABS((invc.invc_group_cost - rcpt.rcpt_group_cost) * iw.tolerance_exchange_rate) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    else  --no tolerance found, exact match
                       case when invc.invc_group_cost = rcpt.rcpt_group_cost then
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                       end
               end = REIM_CONSTANTS.DOC_STATUS_MTCH
               --
               and
               --
               case when iw.qty_match_required = 'N' then
                       REIM_CONSTANTS.DOC_STATUS_MTCH
                    when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                       case when tdq.tolerance_value < ABS(100 * (invc.invc_group_qty - rcpt.rcpt_group_qty) / rcpt.rcpt_group_qty) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                       case when tdq.tolerance_value < ABS(invc.invc_group_qty - rcpt.rcpt_group_qty) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    else  --no tolerance found, exact match
                       case when invc.invc_group_qty = rcpt.rcpt_group_qty then
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                       end
               end = REIM_CONSTANTS.DOC_STATUS_MTCH then
                  REIM_CONSTANTS.DOC_STATUS_MTCH
               else
                  REIM_CONSTANTS.DOC_STATUS_URMTCH
            end match_status,
            --
            'N' persist_status,
            null error_code,
            null error_context
       from im_match_invc_ws iw,
            gtt_num_num_str_str_date_date gtt,
            --
            (select inner.match_key_id,
                    sum(inner.invc_group_cost) invc_group_cost,
                    sum(inner.invc_group_qty) invc_group_qty
               from (select /*+ FULL(ID) INDEX(IW PK_IM_MATCH_INVC_WS) */
                        id.match_key_id,
                            sum(id.unit_cost * id.invoice_qty) invc_group_cost,
                            sum(id.invoice_qty) invc_group_qty
                      from im_match_invc_ws iw,
                           im_match_invc_detl_ws id
                     where iw.workspace_id    = I_workspace_id
                       and iw.match_luw_id    = I_match_luw_id
                       and iw.header_only     = 'N'
                       and iw.workspace_id    = id.workspace_id
                       and iw.match_luw_id    = id.match_luw_id
                       and iw.doc_id          = id.doc_id
                       and id.status          = 'UNMTCH' --constant
                      group by id.match_key_id
                     union all
                     select iw.match_key_id,
                            sum(iw.merch_amount) invc_group_cost,
                            sum(iw.total_qty) invc_group_qty
                      from im_match_invc_ws iw
                     where iw.workspace_id    = I_workspace_id
                       and iw.match_luw_id    = I_match_luw_id
                       and iw.header_only     = 'Y'
                       and iw.status          <> 'MTCH' --constant
                      group by iw.match_key_id) inner
              group by inner.match_key_id) invc,
            --
            (select /*+ FULL(RD) */ rd.match_key_id,
                    sum(rd.unit_cost * NVL(rd.qty_available, 0)) rcpt_group_cost,
                    sum(rd.qty_available) rcpt_group_qty
              from im_match_rcpt_detl_ws rd
             where rd.workspace_id      = I_workspace_id
               and rd.match_luw_id      = I_match_luw_id
               and rd.invc_match_status = 'U'  --constant
             group by rd.match_key_id) rcpt,
            --
            im_tolerance_detail tdc,
            im_tolerance_detail tdq
      where iw.workspace_id       = I_workspace_id
        and iw.match_luw_id       = I_match_luw_id
        --
        and iw.match_key_id       = gtt.number_1
        and iw.match_key_id       = invc.match_key_id
        and iw.match_key_id       = rcpt.match_key_id
        --
        and rcpt.rcpt_group_cost  <> REIM_CONSTANTS.ZERO
        and rcpt.rcpt_group_qty   <> REIM_CONSTANTS.ZERO
        --
        and iw.tolerance_id       = tdc.tolerance_id(+)
        and tdc.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
        and tdc.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
        and tdc.lower_limit(+)   <= (invc.invc_group_cost * iw.tolerance_exchange_rate)
        and tdc.upper_limit(+)    > (invc.invc_group_cost * iw.tolerance_exchange_rate)
        and tdc.favor_of(+)       = case when invc.invc_group_cost > rcpt.rcpt_group_cost
                                         then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                         else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                    end  --constant
        --
        and iw.tolerance_id       = tdq.tolerance_id(+)
        and tdq.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
        and tdq.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY --constants
        and tdq.lower_limit(+)   <= invc.invc_group_qty
        and tdq.upper_limit(+)    > invc.invc_group_qty
        and tdq.favor_of(+)       = case when invc.invc_group_qty > rcpt.rcpt_group_qty
                                         then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                         else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                    end;  --constant

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_MATCH_GROUP_HEAD_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --sku tolerance rcpt to invc
   merge /*+ INDEX(TARGET IM_MATCH_GROUP_HEAD_WS_I5) */ into im_match_group_head_ws target
   using (with iw as (select i2.workspace_id,
                             i2.match_key_id,
                             i2.sku_comp_percent
                        from (select i.workspace_id,
                                     i.match_key_id,
                                     i.sku_comp_percent,
                                     max(i.header_only) header_only
                                from im_match_invc_ws i
                               where i.workspace_id = I_workspace_id
                                 and i.match_luw_id = I_match_luw_id
                               group by i.workspace_id,
                                        i.match_key_id,
                                        i.sku_comp_percent) i2
                       where i2.header_only = 'N'),
               rcpt as (select distinct
                               rd.workspace_id,
                               rd.match_key_id,
                               rd.item
                         from im_match_rcpt_detl_ws rd
                        where rd.workspace_id      = I_workspace_id
                          and rd.match_luw_id = I_match_luw_id
                          and rd.invc_match_status = 'U'),
               invc as (select /*+ FULL(ID) */ distinct
                               id.workspace_id,
                               id.match_key_id,
                               id.item
                         from im_match_invc_detl_ws id
                        where id.workspace_id = I_workspace_id
                          and id.match_luw_id = I_match_luw_id
                          and id.status       = 'UNMTCH')
          select inner.workspace_id,
                 inner.match_key_id,
                 min(inner.sku_compliant) sku_compliant
            from (select rcpt.workspace_id,
                         rcpt.match_key_id,
                         case when sum(decode(rcpt.item, invc.item, 1, 0)) / count(rcpt.item) <
                                      (iw.sku_comp_percent/100) then
                                 'N'
                              else
                                 'Y'
                         end sku_compliant
                    from iw,
                         rcpt,
                         invc
                   where iw.workspace_id = rcpt.workspace_id
                     and iw.match_key_id = rcpt.match_key_id
                     and rcpt.item       = invc.item(+)
                   group by rcpt.workspace_id,
                            rcpt.match_key_id,
                            iw.sku_comp_percent
                   union all
                  select invc.workspace_id,
                         invc.match_key_id,
                         case when sum(decode(invc.item, rcpt.item, 1, 0)) / count(invc.item) <
                                      (iw.sku_comp_percent/100) then
                                 'N'
                              else
                                 'Y'
                         end sku_compliant
                    from iw,
                         rcpt,
                         invc
                   where iw.workspace_id   = invc.workspace_id
                     and iw.match_key_id   = invc.match_key_id
                     and invc.workspace_id = rcpt.workspace_id(+)
                     and invc.match_key_id = rcpt.match_key_id(+)
                     and invc.item         = rcpt.item(+)
                   group by invc.workspace_id,
                            invc.match_key_id,
                            iw.sku_comp_percent) inner
           group by inner.workspace_id,
                    inner.match_key_id) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id      = use_this.match_key_id
       and target.match_stgy_dtl_id = I_match_stgy_dtl_id)
   when matched then update
    set target.sku_compliant    = use_this.sku_compliant,
        target.match_status     = DECODE(use_this.sku_compliant,
                                         'N', REIM_CONSTANTS.DOC_STATUS_URMTCH,
                                         target.match_status),
        target.error_code       = DECODE(use_this.sku_compliant,
                                         'N', 'sku_comp',               --constant
                                         NULL),
        target.error_context    = DECODE(use_this.sku_compliant,
                                         'N', 'sku_comp_context',               --constant
                                         NULL);

   LOGGER.LOG_INFORMATION('Merge IM_MATCH_GROUP_HEAD_WS SKU_COMPLIANCE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if CHECK_TAX_COMPLIANCE(O_error_message,
                           I_workspace_id,
                           I_match_luw_id,
                           null,
                           I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   if UPD_TAXDIS_SMRY(O_error_message,
                      I_workspace_id,
                      I_match_luw_id,
                      NULL,
                      I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   --push matched info to workspace.

   merge /*+ FULL(TARGET) */
   into im_match_invc_ws target
   using (select /*+ FULL(GH) */
                 gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id
            from im_match_group_head_ws gh
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
   ) use_this
   on (    target.match_key_id   = use_this.match_key_id
       and target.workspace_id   = use_this.workspace_id )
   when matched then update
    set target.status         = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.match_group_id = use_this.match_group_id
    where target.status         != REIM_CONSTANTS.DOC_STATUS_MTCH
      and target.match_group_id is null;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_invc_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ FULL(TARGET) */ into im_match_invc_detl_ws target
   using (select /*+ FULL(GH) */ gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id
             from im_match_group_head_ws gh
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id)
   when matched then update
    set target.status         = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.match_group_id = use_this.match_group_id
    where target.match_group_id   is null
      and target.status           != REIM_CONSTANTS.DOC_STATUS_MTCH;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_invc_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ FULL(TARGET) */
   into im_match_rcpt_ws target
   using (select /*+ FULL(GH) */
                 gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id
            from im_match_group_head_ws gh
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
   ) use_this
   on (    target.workspace_id   = use_this.workspace_id
       and target.match_key_id   = use_this.match_key_id)
   when matched then update
    set target.invc_match_status = 'M', --constant
        target.match_group_id    = use_this.match_group_id
    where target.invc_match_status != 'M'
      and target.match_group_id    is null;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_rcpt_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ FULL(TARGET) */ into im_match_rcpt_detl_ws target
   using (select /*+ FULL(GH) */ gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id
             from im_match_group_head_ws gh
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id)
   when matched then update
    set target.invc_match_status = 'M', --constant
        target.match_group_id    = use_this.match_group_id
    where target.invc_match_status != 'M'
      and target.match_group_id    is null;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_rcpt_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('End '||L_program );

   select count(*) doc_count, sum(decode(status,REIM_CONSTANTS.DOC_STATUS_MTCH,1,0)) mtch_status
     into L_doc_count, L_mtch_count
     from im_match_invc_ws
    where workspace_id = I_workspace_id
      and match_luw_id = I_match_luw_id;

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              I_match_stgy_dtl_id, NULL, I_match_luw_id,
                              TASK_LVL_LUW,
                              TASK_TYPE_MATCH,
                              L_doc_count, L_mtch_count, NULL, NULL, NULL,
                              STATUS_SUCCESS);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when program_exception then
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 I_match_stgy_dtl_id, NULL, I_match_luw_id,
                                 TASK_LVL_LUW,
                                 TASK_TYPE_MATCH,
                                 NULL, NULL, NULL, NULL, NULL,
                                 STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 I_match_stgy_dtl_id, NULL, I_match_luw_id,
                                 TASK_LVL_LUW,
                                 TASK_TYPE_MATCH,
                                 NULL, NULL, NULL, NULL, NULL,
                                 STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
END PERFORM_SMRY_ALL_2_ALL;
------------------------------------------------------------------------------------------
/**
 * The public function used to perform Summary One to Many Match.
 * Invoices would get grouped into groups of one invoice each.
 * Best Match: All combinations of receipts (for each group key) are formed as groups.
 * If the number of receipts exceed five then the best match switches to regular match which is an One to One Match.
 * Group header table holds all combinations of invoice and receipt groups
 *
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_stgy_dtl_id (Match Strategy Detail ID)
 *              I_match_luw_id (ID of the Match LUW)
 *              I_match_type (Match Type - Can be 'R'-Regular  or 'B'-Best Match)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_SMRY_ONE_2_MANY(O_error_message        OUT VARCHAR2,
                                 I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                 I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                 I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE, -- Match_type can be 'R'-Regular  or 'B'-Best Match
                                 I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERFORM_SMRY_ONE_2_MANY';
   L_start_time      TIMESTAMP := SYSTIMESTAMP;
   L_doc_count       NUMBER(10)   := 0;
   L_mtch_count      NUMBER(10)   := 0;
   program_exception EXCEPTION;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program
                          || ' I_workspace_id: '      || I_workspace_id
                          || ' I_match_stgy_dtl_id: ' || I_match_stgy_dtl_id
                          || ' I_match_luw_id: '      || I_match_luw_id
                          || ' I_match_type: '        || I_match_type);

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              I_match_stgy_dtl_id, NULL, I_match_luw_id,
                              TASK_LVL_LUW,
                              TASK_TYPE_MATCH,
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_STARTED);

   insert into im_match_group_invc_ws(invc_group_id,
                                      doc_id,
                                      workspace_id,
                                      match_stgy_id,
                                      match_stgy_dtl_id,
                                      chunk_num,
                                      match_luw_id,
                                      match_key_id,
                                      due_date)
                               select im_match_invc_group_seq.NEXTVAL,
                                      inner.doc_id,
                                      inner.workspace_id,
                                      inner.match_stgy_id,
                                      I_match_stgy_dtl_id,
                                      NULL, -- chunk_num
                                      inner.match_luw_id,
                                      inner.match_key_id,
                                      inner.due_date
                                 from (select imiw.doc_id,
                                              imiw.workspace_id,
                                              imiw.match_stgy_id,
                                              imiw.match_luw_id,
                                              imiw.match_key_id,
                                              imiw.due_date,
                                              imiw.rank_in_match_key
                                         from im_match_invc_ws imiw
                                        where imiw.workspace_id = I_workspace_id
                                          and imiw.match_luw_id = I_match_luw_id
                                        ORDER BY imiw.match_luw_id, --required to order according to rank_in_match_key
                                                 imiw.match_key_id,
                                                 imiw.rank_in_match_key) inner;

   LOGGER.LOG_INFORMATION('Create Invoice Group - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if CREATE_RCPT_GROUPS(O_error_message,
                         I_workspace_id,
                         I_match_luw_id,
                         NULL, --match_key_id
                         I_match_stgy_dtl_id,
                         I_match_type) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   if CREATE_MTCH_GROUP_HEAD(O_error_message,
                             I_workspace_id,
                             I_match_luw_id,
                             NULL, --match_key_id
                             I_match_stgy_dtl_id,
                             REIM_CONSTANTS.MATCH_LEVEL_SUMM_1_2_MANY) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   if MATCH_GROUP_HEAD(O_error_message,
                       I_workspace_id,
                       I_match_luw_id,
                       NULL, --match_key_id
                       I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   if CHECK_SKU_COMPLIANCE(O_error_message,
                           I_workspace_id,
                           I_match_luw_id,
                           NULL, --match_key_id
                           I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   if I_match_type = REIM_CONSTANTS.MATCH_TYPE_BEST then

      if DETERMINE_BEST_MATCH(O_error_message,
                              I_workspace_id,
                              I_match_luw_id,
                              NULL, --match_key_id
                              I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
         raise program_exception;
      end if;

   else

      --update to URMTCH is there are more than one match available
      update im_match_group_head_ws
      set match_status    = REIM_CONSTANTS.DOC_STATUS_URMTCH
      where invc_group_id IN (select /*+ INDEX(GH IM_MATCH_GROUP_HEAD_WS_I4) */
                                     gh.invc_group_id
                                from im_match_group_head_ws gh
                               where gh.workspace_id = I_workspace_id
                                 and gh.match_luw_id = I_match_luw_id
                                 and gh.match_status = REIM_CONSTANTS.DOC_STATUS_MTCH
                               group by gh.invc_group_id
                              having count(1) > 1);

      LOGGER.LOG_INFORMATION(L_program||' merge 1 multi match to URMTCH - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      update im_match_group_head_ws
      set match_status    = REIM_CONSTANTS.DOC_STATUS_URMTCH
      where rcpt_group_id IN (select /*+ INDEX(GH IM_MATCH_GROUP_HEAD_WS_I4) */
                                     gh.rcpt_group_id
                                from im_match_group_head_ws gh
                               where gh.workspace_id = I_workspace_id
                                 and gh.match_luw_id = I_match_luw_id
                                 and gh.match_status = REIM_CONSTANTS.DOC_STATUS_MTCH
                               group by gh.rcpt_group_id
                              having count(1) > 1);

      LOGGER.LOG_INFORMATION(L_program||' merge 2 multi match to URMTCH - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if CHECK_TAX_COMPLIANCE(O_error_message,
                           I_workspace_id,
                           I_match_luw_id,
                           null,
                           I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   if UPD_WS_SMRY_MTCH_DATA(O_error_message,
                            I_workspace_id,
                            I_match_luw_id,
                            NULL, --match_key_id
                            I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;


   select count(*) doc_count, sum(decode(status,REIM_CONSTANTS.DOC_STATUS_MTCH,1,0)) mtch_status
     into L_doc_count, L_mtch_count
     from im_match_invc_ws
    where workspace_id = I_workspace_id
      and match_luw_id = I_match_luw_id;

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              I_match_stgy_dtl_id, NULL, I_match_luw_id,
                              TASK_LVL_LUW,
                              TASK_TYPE_MATCH,
                              L_doc_count, L_mtch_count, NULL, NULL, NULL,
                              TASK_STATUS_SUCCESS);

   LOGGER.LOG_INFORMATION('End ' || L_program );
   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when program_exception then
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 I_match_stgy_dtl_id, NULL, I_match_luw_id,
                                 TASK_LVL_LUW,
                                 TASK_TYPE_MATCH,
                                 NULL, NULL, NULL, NULL, NULL,
                                 STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 I_match_stgy_dtl_id, NULL, I_match_luw_id,
                                 TASK_LVL_LUW,
                                 TASK_TYPE_MATCH,
                                 NULL, NULL, NULL, NULL, NULL,
                                 STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
END PERFORM_SMRY_ONE_2_MANY;
------------------------------------------------------------------------------------------
/**
 * The public function that performs Detail Matching on the Line items of the invoices in the chunk.
 * Match_type: Regular would match each invoice line item with the sum of all available receipt line item(same item)
 *             If more than one Invoice item matches to the sum of receipt items (same item) all invoice items would be marked as discrepant.
 * Match_type: Best Match would match each invoice line item with the sum of all available receipt line item(same item)
 *             If more than one Invoice item matches to the sum of receipt items (same item), best match is decided based on criteria.
 *             If undecided even after applying criteria, all invoice items would be marked as discrepant.
 * Group header table holds combinations of invoice and receipt groups(for the same item).
 *
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_stgy_dtl_id (Match Strategy Detail ID)
 *              I_chunk_num (ID of the Chunk of Match Data)
 *              I_match_type (Match Type - Can be 'R'-Regular  or 'B'-Best Match)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
 FUNCTION PERFORM_DETAIL_MATCHING(O_error_message        OUT VARCHAR2,
                                 I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                 I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                 I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE, -- 'R'-Regular  or 'B'-Best Match or 'P'-Parent
                                 I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                 I_chunk_num         IN     IM_AUTO_MATCH_TASK_STATUS.CHUNK_NUM%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERFORM_DETAIL_MATCHING';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   if I_match_type IN (REIM_CONSTANTS.MATCH_TYPE_BEST,
                       REIM_CONSTANTS.MATCH_TYPE_REGULAR) then

      if PERFORM_SKU_LVL_DETL_MATCH(O_error_message,
                                    I_workspace_id,
                                    I_match_stgy_dtl_id,
                                    I_match_type,
                                    I_match_luw_id,
                                    I_chunk_num) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

   elsif I_match_type = REIM_CONSTANTS.MATCH_TYPE_PARENT then

      if PERFORM_STYLE_LVL_DETL_MATCH(O_error_message,
                                      I_workspace_id,
                                      I_match_stgy_dtl_id,
                                      I_match_type,
                                      I_match_luw_id,
                                      I_chunk_num) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERFORM_DETAIL_MATCHING;
------------------------------------------------------------------------------------------
/**
 * The public function that persists Match data at a LUW level.(Used by Auto Match Batch)
 * It calls PERSIST_DETAIL and PERSIST_SUMMARY functions in the same order.
 *
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_luw_id (ID of the Match LUW)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERSIST_MATCH_DATA(O_error_message    OUT VARCHAR2,
                            I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                            I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERSIST_MATCH_DATA';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   program_exception EXCEPTION;

BEGIN

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              NULL, NULL, I_match_luw_id,
                              TASK_LVL_LUW,
                              TASK_TYPE_PERSIST,
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_STARTED);


   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: ' || I_workspace_id
                          || ' I_match_luw_id: ' || I_match_luw_id);

   LOGGER.LOG_INFORMATION(' Calling Persist SKU level Detail Match Data function...');

   if PERSIST_SKU_LVL_DETL_MATCH(O_error_message,
                                 I_workspace_id,
                                 I_match_luw_id) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   LOGGER.LOG_INFORMATION(' Calling Persist Style level Detail Match Data function...');

   if PERSIST_STYLE_LVL_DETL_MATCH(O_error_message,
                                   I_workspace_id,
                                   I_match_luw_id) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   LOGGER.LOG_INFORMATION(' Calling Persist Summary Match Data function...');

   if PERSIST_SUMMARY_MATCH_DATA(O_error_message,
                                 I_workspace_id,
                                 I_match_luw_id,
                                 NULL) = REIM_CONSTANTS.FAIL then
      raise program_exception;
   end if;

   LOGGER.LOG_INFORMATION('End ' || L_program );

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              NULL, NULL, I_match_luw_id,
                              TASK_LVL_LUW,
                              TASK_TYPE_PERSIST,
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_SUCCESS);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when program_exception then
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 NULL, NULL, I_match_luw_id,
                                 TASK_LVL_LUW,
                                 TASK_TYPE_PERSIST,
                                 NULL, NULL, NULL, NULL, NULL,
                                 TASK_STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 NULL, NULL, I_match_luw_id,
                                 TASK_LVL_LUW,
                                 TASK_TYPE_PERSIST,
                                 NULL, NULL, NULL, NULL, NULL,
                                 TASK_STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
END PERSIST_MATCH_DATA;
------------------------------------------------------------------------------------------
/**
 * The public function that persists match data produced by summary level matches
 * Update Invoice Header and Shipment based on their details' statuses (for detail matches).
 * Update Invoice Header and Shipment for successful summary matches.
 * Create Summary Match History Data.
 * Create Receipt Item Posting Data for summary Matches
 *
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_luw_id (ID of the Match LUW)
 *              I_match_key_id (ID of the Match Key -- Optional)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERSIST_SUMMARY_MATCH_DATA(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                    I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                    I_match_key_id  IN     IM_MATCH_INVC_WS.MATCH_KEY_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERSIST_SUMMARY_MATCH_DATA';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_vdate   DATE         := get_vdate;
   L_user    IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: ' || I_workspace_id
                          || ' I_match_luw_id: ' || I_match_luw_id
                          || ' I_match_key_id: ' || I_match_key_id);

   if L_user is null then
      L_user := get_user;
   end if;

   delete from gtt_num_num_str_str_date_date;

   --number_1   match_key_id
   --number_2   match_id
   --varchar2_1 match_group_id
   insert into gtt_num_num_str_str_date_date (number_1, number_2, varchar2_1)
   select gh.match_key_id,
          im_summary_match_history_seq.nextval,
          gh.match_group_id
     from im_match_group_head_ws gh
    where gh.workspace_id = I_workspace_id
      and gh.match_luw_id = I_match_luw_id
      and gh.match_key_id = nvl(I_match_key_id,gh.match_key_id)
      and gh.match_status = REIM_CONSTANTS.DOC_STATUS_MTCH
      and gh.match_type   IN (REIM_CONSTANTS.MATCH_LEVEL_SUMM_ALL_2_ALL,
                              REIM_CONSTANTS.MATCH_LEVEL_SUMM_1_2_MANY);

/*
For All Invoices where MATCH_AVAILABLE = 'Y' and ERROR_CODE is NULL set  IM_MATCH_GROUP_HEAD.STATUS to 'MTCH'.
All invoices for the chunk from IM_MATCH_GROUP_HEAD should be updated in
IM_DOC_HEAD, IM_INVOICE_DETAIL, IM_AUTO_MATCH_INVC_WS.
Also the corresponding Receipts belonging to the group should be updated to matched in
 SHIPMENT, SHIPSKU tables.
Other tables that needs to be populated /updated includes, IM_SUMMARY_MATCH_HISTORY, IM_SUMMARY_MATCH_INVC_HISTORY, IM_SUMMARY_MATCH_RCPT_HISTORY,
IM_RECEIPT_ITEM_POSTING,  IM_RCPT_ITEM_POSTING_INVOICE,
IM_AUTO_MATCH_STGY_STATUS.
*/

   --Update Im Partially Matched Receipt with matched quantity
   merge into im_partially_matched_receipts target
   using (select rd.shipment,
                 rd.item,
                 rd.qty_available qty_matched
            from gtt_num_num_str_str_date_date gtt,
                 im_match_rcpt_detl_ws rd
           where gtt.varchar2_1                    = rd.match_group_id
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item)
   when matched then update
    set target.qty_matched       = target.qty_matched + use_this.qty_matched,
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE
   when not matched then insert (shipment,
                                 item,
                                 qty_matched,
                                 created_by,
                                 creation_date,
                                 last_updated_by,
                                 last_update_date,
                                 object_version_id)
                         values (use_this.shipment,
                                 use_this.item,
                                 use_this.qty_matched,
                                 L_user,
                                 sysdate,
                                 L_user,
                                 sysdate,
                                 REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' insert im_partially_matched_receipts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from gtt_6_num_6_str_6_date;

   --number_1   seq_no
   --number_2   match_group_id
   --number_3   shipment
   --number_4   qty_matched
   --varchar2_1 item
   insert into gtt_6_num_6_str_6_date (number_1,
                                       number_2,
                                       number_3,
                                       number_4,
                                       varchar2_1)
      select im_receipt_item_posting_seq.nextval,
             rd.match_group_id,
             rd.shipment,
             rd.qty_available,
             rd.item
        from gtt_num_num_str_str_date_date gtt,
             im_match_rcpt_detl_ws rd
       where gtt.varchar2_1 = rd.match_group_id;

   insert into im_receipt_item_posting (seq_no,
                                        shipment,
                                        item,
                                        qty_matched,
                                        qty_posted,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
   select gtt.number_1,
          gtt.number_3,
          gtt.varchar2_1,
          gtt.number_4,
          NULL,
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_6_num_6_str_6_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_rcpt_item_posting_invoice (seq_no,
                                             doc_id,
                                             status,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             imiw.doc_id,
          'M',
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt,
             im_match_invc_ws imiw
       where imiw.match_group_id = gtt.number_2;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting_invoice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head target
   using (select /*+ ORDERED FULL(GH) INDEX(MI IM_MATCH_INVC_WS_I4) INDEX(DH PK_IM_DOC_HEAD) */
                 mi.doc_id,
                 gh.cost_variance,
                 RANK() OVER (PARTITION BY gh.match_key_id,
                                           gh.match_group_id
                                  ORDER BY mi.merch_amount desc,
                                           mi.doc_id) variance_rnk
            from gtt_num_num_str_str_date_date gtt,
                 im_match_group_head_ws gh,
                 im_match_invc_ws mi,
                 im_doc_head dh
           where gtt.number_1      = gh.match_key_id
             and gtt.varchar2_1    = gh.match_group_id
             and gh.match_key_id   = mi.match_key_id
             and gh.match_group_id = mi.match_group_id
             and mi.doc_id         = dh.doc_id
             and dh.status        != REIM_CONSTANTS.DOC_STATUS_MTCH
   ) use_this
   on (target.doc_id  = use_this.doc_id)
   when matched then update
    set target.status                    = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.variance_within_tolerance = DECODE(use_this.variance_rnk,
                                                  REIM_CONSTANTS.ONE, use_this.cost_variance,
                                                  NULL),
        target.match_id                  = L_user,
        target.match_date                = L_vdate,
        target.match_type                = DECODE(I_match_key_id,
                                                  NULL, REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                  REIM_CONSTANTS.MATCH_TYPE_MANUAL),
        target.last_updated_by           = L_user,
        target.last_update_date          = sysdate,
        target.object_version_id         = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge to im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_invoice_detail target
   using (select id.doc_id,
                 id.item
            from gtt_num_num_str_str_date_date gtt,
                 im_match_invc_detl_ws id,
                 im_invoice_detail iid
           where gtt.varchar2_1 = id.match_group_id
             and id.doc_id      = iid.doc_id
             and id.item        = iid.item
             and iid.status     != REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status            = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched      = 'Y',
        target.qty_matched       = 'Y',
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge to im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ---------------

   merge into shipment target
   using (select /*+ INDEX(SH PK_SHIPSKU) */ mr.shipment
            from gtt_num_num_str_str_date_date gtt,
                 im_match_rcpt_ws mr,
                 shipment sh
           where gtt.varchar2_1                    = mr.match_group_id
             and mr.shipment                       = sh.shipment
             and nvl(sh.invc_match_status,'-999') != REIM_CONSTANTS.SHIP_IM_STATUS_MTCH
   ) use_this
   on (target.shipment           = use_this.shipment)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_MTCH,
        target.invc_match_date   = L_vdate;

   LOGGER.LOG_INFORMATION(L_program||' merge to shipment - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into shipsku target
   using (select rd.shipment,
                 rd.item
          from gtt_num_num_str_str_date_date gtt,
               im_match_rcpt_detl_ws rd
          where gtt.varchar2_1 = rd.match_group_id) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
        target.qty_matched       = nvl(target.qty_received,0) - nvl(target.qty_matched,0)
    where nvl(target.invc_match_status,'-999') != REIM_CONSTANTS.SSKU_IM_STATUS_MTCH;

   LOGGER.LOG_INFORMATION(L_program||' merge to shipsku - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ---------------

   insert into im_summary_match_history (match_id,
                                         auto_matched,
                                         exact_match,
                                         created_by,
                                         creation_date,
                                         last_updated_by,
                                         last_update_date,
                                         object_version_id)
   select /*+ ORDERED FULL(GH) INDEX(MI IM_MATCH_INVC_WS_I4) */
          gtt.number_2,
          DECODE(I_match_key_id,
                 NULL, 'Y',
                 'N'),
          decode(sum(nvl(gh.cost_variance,0)), 0, 'Y', 'N'),
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_num_num_str_str_date_date gtt,
          im_match_group_head_ws gh,
          im_match_invc_ws mi
    where gtt.number_1      = gh.match_key_id
      and gh.match_key_id   = mi.match_key_id
      and gh.match_group_id = mi.match_group_id
    group by gtt.number_2;

   LOGGER.LOG_INFORMATION(L_program||' insert im_summary_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_summary_match_invc_history(match_id,
                                             doc_id,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
   select /*+ ORDERED FULL(GH) INDEX(MI IM_MATCH_INVC_WS_I4) */
          gtt.number_2,
          mi.doc_id,
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_num_num_str_str_date_date gtt,
          im_match_group_head_ws gh,
          im_match_invc_ws mi
    where gh.workspace_id   = I_workspace_id
      and gh.match_status   = 'MTCH'
      and gh.workspace_id   = mi.workspace_id
      and gh.match_key_id   = mi.match_key_id
      and gh.match_group_id = mi.match_group_id
      and gh.match_key_id   = gtt.number_1
      and gh.match_group_id = gtt.varchar2_1
      and gh.match_group_id = mi.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_summary_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_summary_match_rcpt_history(match_id,
                                             shipment,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
   select /*+ ORDERED FULL(GH) FULL(MR) */
          gtt.number_2,
          mr.shipment,
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from im_match_group_head_ws gh,
          im_match_rcpt_ws mr,
          gtt_num_num_str_str_date_date gtt
    where gh.workspace_id   = I_workspace_id
      and gh.match_status   = 'MTCH'
      and gh.workspace_id   = mr.workspace_id
      and gh.match_key_id   = mr.match_key_id
      and gh.match_group_id = mr.match_group_id
      and gh.match_key_id   = gtt.number_1
      and gh.match_group_id = gtt.varchar2_1
      and gh.match_group_id = mr.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_summary_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('End ' || L_program );

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERSIST_SUMMARY_MATCH_DATA;
------------------------------------------------------------------------------------------
/**
 * Performs Cost pre match for invoices.
 */
FUNCTION PERFORM_COST_PRE_MATCH(O_error_message    OUT VARCHAR2,
                                I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERFORM_COST_PRE_MATCH';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: ' || I_workspace_id);

   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1)
   select inner.doc_id,
          inner.item
     from (select d.doc_id,
                  d.item,
                  --
                  case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                          case when tdc.tolerance_value < ABS(100 * (d.unit_cost - d.ordloc_unit_cost) / d.ordloc_unit_cost) then
                                  'Y'
                               else
                                  'N'
                          end
                       when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                          case when tdc.tolerance_value < ABS((d.unit_cost - d.ordloc_unit_cost) * i.tolerance_exchange_rate) then
                                  'Y'
                               else
                                  'N'
                          end
                       else  --no tolerance found, exact match
                          case when d.unit_cost = d.ordloc_unit_cost then
                                  'N'
                               else
                                  'Y'
                          end
                  end cost_discrepant
                  --
             from im_match_invc_ws i,
                  im_match_invc_detl_ws d,
                  im_tolerance_detail tdc
            where i.workspace_id        = I_workspace_id
              and i.match_luw_id        is null
              and i.cost_pre_match      = 'N'
              --
              and d.workspace_id        = i.workspace_id
              and d.doc_id              = i.doc_id
              and d.match_luw_id        is null
              --
              and i.tolerance_id        = tdc.tolerance_id(+)
              and tdc.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
              and tdc.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
              and tdc.lower_limit(+)   <= (d.unit_cost * i.tolerance_exchange_rate)
              and tdc.upper_limit(+)    > (d.unit_cost * i.tolerance_exchange_rate)
              and tdc.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
          ) inner
    where inner.cost_discrepant = 'Y';


   LOGGER.LOG_INFORMATION('Insert gtt cost pre match - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_invoice_detail tgt
   using (select gtt.number_1 doc_id,
                 gtt.varchar2_1 item
            from gtt_num_num_str_str_date_date gtt) src
   on (    tgt.doc_id = src.doc_id
       and tgt.item   = src.item)
   when MATCHED then
      update set tgt.cost_matched = 'D';

   LOGGER.LOG_INFORMATION('Merge im_invoice_detail cost pre match - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select distinct gtt.number_1 doc_id
            from gtt_num_num_str_str_date_date gtt) src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update set tgt.status = REIM_CONSTANTS.DOC_STATUS_URMTCH;

   LOGGER.LOG_INFORMATION('Merge im_doc_head cost pre match status - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select i.doc_id
            from im_match_invc_ws i
           where i.workspace_id   = I_workspace_id
             and i.match_luw_id   is null
             and i.cost_pre_match = 'N') src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update set tgt.cost_pre_match = 'Y';

   LOGGER.LOG_INFORMATION('Merge im_doc_head cost pre match ind - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('End ' || L_program );

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id, L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END PERFORM_COST_PRE_MATCH;
------------------------------------------------------------------------------------------
/**
 * The public function used to perform the necessary actions to end the automatch batch.
 *      1. Updates Invoices which dont have receipts to URMTCH.
 *      2. Updates discrepant status of Invoice items which do not have corresponding receipt items provided the invoice is not matched at a summary level.
 *      3. Updates discrepant status of Invoice items which have receipts but not matched.
 * Input param: I_workspace_id (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION END_MATCH(O_error_message    OUT VARCHAR2,
                   I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.END_MATCH';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_user             IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');

BEGIN

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              NULL, NULL, NULL,
                              TASK_LVL_BATCH,
                              TASK_TYPE_PERSIST, --is:
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_STARTED);

   LOGGER.LOG_INFORMATION(L_program || ' I_workspace_id: ' || I_workspace_id);

   if L_user is null then
      L_user := get_user;
   end if;

   merge into im_doc_head target
   using (select mi.doc_id,
                 DECODE(mi.status,
                        REIM_CONSTANTS.DOC_STATUS_RMTCH, REIM_CONSTANTS.DOC_STATUS_URMTCH,
                        mi.status) status
            from im_match_invc_ws mi,
                 im_doc_head idh
           where mi.workspace_id  = I_workspace_id
             and mi.status        <> REIM_CONSTANTS.DOC_STATUS_MTCH
             and idh.doc_id       = mi.doc_id
             and idh.status       = REIM_CONSTANTS.DOC_STATUS_RMTCH
             and mi.match_stgy_id is NOT NULL
   ) use_this
   on (target.doc_id  = use_this.doc_id)
   when matched then update
    set target.status                    = use_this.status,
        target.last_updated_by           = L_user,
        target.last_update_date          = sysdate,
        target.object_version_id         = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_DOC_HEAD unmatched status - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   update im_batch_config ibc
      set ibc.processed='Y', ibc.processed_date=sysdate
    where upper(ibc.batch_name) = 'REIMAUTOMATCH'
      and ibc.processed = 'N';

   LOGGER.LOG_INFORMATION(L_program||' Update im_batch_config to processed- SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOG_AUTO_MATCH_STATUS(O_error_message,
                         I_workspace_id,
                         STATUS_SUCCESS);
   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              NULL, NULL, NULL,
                              TASK_LVL_BATCH,
                              TASK_TYPE_PERSIST, --is:
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_SUCCESS);

   LOGGER.LOG_INFORMATION('End ' || L_program );

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      LOG_AUTO_MATCH_STATUS(O_error_message,
                            I_workspace_id,
                            STATUS_FAILED);
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 NULL, NULL, NULL,
                                 TASK_LVL_BATCH,
                                 TASK_TYPE_PERSIST, --is:
                                 NULL, NULL, NULL, NULL, NULL,
                                 TASK_STATUS_FAILED);
      return REIM_CONSTANTS.FAIL;
END END_MATCH;
------------------------------------------------------------------------------------------
/**
 * The function used for Creating Combinations(rcpt groups) of receipts.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION CREATE_RCPT_GROUPS(O_error_message     IN OUT VARCHAR2,
                            I_workspace_id      IN     IM_MATCH_GROUP_RCPT_WS.WORKSPACE_ID%TYPE,
                            I_match_luw_id      IN     IM_MATCH_GROUP_RCPT_WS.MATCH_LUW_ID%TYPE,
                            I_match_key_id      IN     IM_MATCH_GROUP_RCPT_WS.MATCH_KEY_ID%TYPE,
                            I_match_stgy_dtl_id IN     IM_MATCH_GROUP_RCPT_WS.MATCH_STGY_DTL_ID%TYPE,
                            I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE)
RETURN NUMBER IS

   L_program    VARCHAR2(61) := 'REIM_AUTO_MATCH_SQL.CREATE_RCPT_GROUPS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_best_mtch_limit NUMBER := 5;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_stgy_dtl_id:' || I_match_stgy_dtl_id ||
                          ' I_match_type:' || I_match_type ||
                          ' I_match_key_id:' || I_match_key_id);

   if I_match_key_id is NOT NULL then
      --Automatch has a limit of 5 and the online match 20
      L_best_mtch_limit := 20;
   end if;

   if I_match_type = REIM_CONSTANTS.MATCH_TYPE_REGULAR then

      insert into im_match_group_rcpt_ws(rcpt_group_id,
                                         shipment,
                                         workspace_id,
                                         --match_stgy_id,
                                         match_stgy_dtl_id,
                                         chunk_num,
                                         match_luw_id,
                                         match_key_id)
                                  select im_match_rcpt_group_seq.NEXTVAL,
                                         imrw.shipment,
                                         imrw.workspace_id,
                                         --I_match_stgy_id,
                                         I_match_stgy_dtl_id,
                                         REIM_CONSTANTS.ONE, --to be nulled
                                         imrw.match_luw_id,
                                         imrw.match_key_id
                                    from im_match_rcpt_ws imrw
                                   where imrw.workspace_id = I_workspace_id
                                     and imrw.match_luw_id = I_match_luw_id
                                     and imrw.match_key_id      = NVL(I_match_key_id, imrw.match_key_id)
                                     and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH;

      LOGGER.LOG_INFORMATION('Insert im_match_group_rcpt_ws regular - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   else

      --Insert for match keys exceeding best match limit
      insert into im_match_group_rcpt_ws(rcpt_group_id,
                                         shipment,
                                         workspace_id,
                                         --match_stgy_id,
                                         match_stgy_dtl_id,
                                         chunk_num,
                                         match_luw_id,
                                         match_key_id)
                 with im_mtch_rcpt_ws as (select workspace_id,
                                                 match_luw_id,
                                                 match_key_id,
                                                 shipment,
                                                 match_key_rank,
                                                 max_match_key_rank
                                            from (select imrw.workspace_id,
                                                         imrw.match_luw_id,
                                                         imrw.match_key_id,
                                                         imrw.shipment,
                                                         ROW_NUMBER() OVER (PARTITION BY imrw.workspace_id,
                                                                                         imrw.match_luw_id,
                                                                                         imrw.match_key_id
                                                                                ORDER BY imrw.shipment) match_key_rank,
                                                         count(1) OVER (PARTITION BY imrw.workspace_id,
                                                                                     imrw.match_luw_id,
                                                                                     imrw.match_key_id) max_match_key_rank
                                                    from im_match_rcpt_ws imrw
                                                   where imrw.workspace_id = I_workspace_id
                                                     and imrw.match_luw_id = I_match_luw_id
                                                     and imrw.match_key_id      = NVL(I_match_key_id, imrw.match_key_id)
                                                     and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH)
                                           where max_match_key_rank > L_best_mtch_limit)
                                  select im_match_rcpt_group_seq.NEXTVAL,
                                         imrw_ws.shipment,
                                         imrw_ws.workspace_id,
                                         --I_match_stgy_id,
                                         I_match_stgy_dtl_id,
                                         REIM_CONSTANTS.ONE, --to be nulled
                                         imrw_ws.match_luw_id,
                                         imrw_ws.match_key_id
                                    from im_mtch_rcpt_ws imrw_ws;

      delete from gtt_10_num_10_str_10_date;
      insert into gtt_10_num_10_str_10_date(number_1,  -- workspace_id
                                            number_2,  -- match_luw_id
                                            number_3,  -- match_key_id
                                            number_4,  -- match_stgy_dtl_id
                                            number_5,  -- shipment
                                            number_6)  -- match_key_rank
         select workspace_id,
                match_luw_id,
                match_key_id,
                match_stgy_dtl_id,
                shipment,
                match_key_rank
           from (select imrw.workspace_id,
                        imrw.match_luw_id,
                        imrw.match_key_id,
                        I_match_stgy_dtl_id match_stgy_dtl_id,
                        imrw.shipment,
                        ROW_NUMBER() OVER (PARTITION BY imrw.workspace_id,
                                                        imrw.match_luw_id,
                                                        imrw.match_key_id
                                               ORDER BY imrw.shipment) match_key_rank,
                        count(1) OVER (PARTITION BY imrw.workspace_id,
                                                    imrw.match_luw_id,
                                                    imrw.match_key_id) max_match_key_rank
                   from im_match_rcpt_ws imrw
                  where I_match_type           = REIM_CONSTANTS.MATCH_TYPE_BEST
                    and imrw.workspace_id      = I_workspace_id
                    and imrw.match_luw_id      = I_match_luw_id
                    and imrw.match_key_id      = NVL(I_match_key_id, imrw.match_key_id)
                    and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH)
          where max_match_key_rank <= L_best_mtch_limit;

      for L_match_key_rank in 1 .. L_best_mtch_limit loop

         if L_match_key_rank > REIM_CONSTANTS.ONE then

            delete from gtt_6_num_6_str_6_date;
            insert into gtt_6_num_6_str_6_date(number_1,  -- old_group_id
                                               number_2,  -- new_group_id
                                               number_3,  -- workspace_id
                                               number_4,  -- match_luw_id
                                               number_5,  -- match_key_id
                                               number_6)  -- match_stgy_dtl_id
                                        select rcpt_group_id,
                                               im_match_rcpt_group_seq.NEXTVAL,
                                               workspace_id,
                                               match_luw_id,
                                               match_key_id,
                                               match_stgy_dtl_id
                                          from (select distinct rcpt_group_id,
                                                       gtt.number_1 workspace_id,
                                                       gtt.number_2 match_luw_id,
                                                       gtt.number_3 match_key_id,
                                                       gtt.number_4 match_stgy_dtl_id
                                                  from gtt_10_num_10_str_10_date gtt,
                                                       im_match_group_rcpt_ws imgrw
                                                 where gtt.number_1            = I_workspace_id
                                                   and gtt.number_2            = I_match_luw_id
                                                   and gtt.number_3            = NVL(I_match_key_id, gtt.number_3)
                                                   and gtt.number_4            = I_match_stgy_dtl_id
                                                   and gtt.number_6            = L_match_key_rank
                                                   and imgrw.workspace_id      = gtt.number_1
                                                   and imgrw.match_luw_id      = gtt.number_2
                                                   and imgrw.match_key_id      = gtt.number_3
                                                   and imgrw.match_stgy_dtl_id = gtt.number_4);

            insert into im_match_group_rcpt_ws(rcpt_group_id,
                                               shipment,
                                               workspace_id,
                                               match_stgy_dtl_id,
                                               chunk_num,
                                               match_luw_id,
                                               match_key_id)
                                        select gtt.number_2 rcpt_group_id, --new_group_id
                                               imgrw.shipment,
                                               imgrw.workspace_id,
                                               imgrw.match_stgy_dtl_id,
                                               imgrw.chunk_num,
                                               imgrw.match_luw_id,
                                               imgrw.match_key_id
                                          from gtt_6_num_6_str_6_date gtt,
                                               im_match_group_rcpt_ws imgrw
                                         where imgrw.rcpt_group_id     = gtt.number_1     --old_group_id
                                           and imgrw.workspace_id      = gtt.number_3 --workspace_id
                                           and imgrw.match_luw_id      = gtt.number_4 --match_luw_id
                                           and imgrw.match_key_id      = gtt.number_5 --match_key_id
                                           and imgrw.match_stgy_dtl_id = gtt.number_6; --match_stgy_dtl_id

            insert into im_match_group_rcpt_ws(rcpt_group_id,
                                               shipment,
                                               workspace_id,
                                               match_stgy_dtl_id,
                                               chunk_num,
                                               match_luw_id,
                                               match_key_id)
                                        select gtt6.number_2 rcpt_group_id, --new_group_id
                                               gtt10.number_5 shipment,
                                               gtt10.number_1 workspace_id,
                                               gtt10.number_4 match_stgy_dtl_id,
                                               REIM_CONSTANTS.ONE, --to be nulled
                                               gtt10.number_2 match_luw_id,
                                               gtt10.number_3 match_key_id
                                          from gtt_10_num_10_str_10_date gtt10,
                                               gtt_6_num_6_str_6_date gtt6
                                         where gtt10.number_1 = I_workspace_id
                                           and gtt10.number_2 = I_match_luw_id
                                           and gtt10.number_3 = NVL(I_match_key_id, gtt10.number_3)
                                           and gtt10.number_4 = I_match_stgy_dtl_id
                                           and gtt10.number_6 = L_match_key_rank
                                           and gtt6.number_3  = gtt10.number_1
                                           and gtt6.number_4  = gtt10.number_2
                                           and gtt6.number_5  = gtt10.number_3
                                           and gtt6.number_6  = gtt10.number_4;

         end if;

         insert into im_match_group_rcpt_ws(rcpt_group_id,
                                            shipment,
                                            workspace_id,
                                            match_stgy_dtl_id,
                                            chunk_num,
                                            match_luw_id,
                                            match_key_id)
            select im_match_rcpt_group_seq.NEXTVAL,
                   gtt10.number_5 shipment,
                   gtt10.number_1 workspace_id,
                   gtt10.number_4 match_stgy_dtl_id,
                                            REIM_CONSTANTS.ONE, --to be nulled
                   gtt10.number_2 match_luw_id,
                   gtt10.number_3 match_key_id
              from gtt_10_num_10_str_10_date gtt10
             where gtt10.number_1 = I_workspace_id
               and gtt10.number_2 = I_match_luw_id
               and gtt10.number_3 = NVL(I_match_key_id, gtt10.number_3)
               and gtt10.number_4 = I_match_stgy_dtl_id
               and gtt10.number_6 = L_match_key_rank;

      end loop;

   end if;

   LOGGER.LOG_INFORMATION('End ' || L_program);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CREATE_RCPT_GROUPS;
------------------------------------------------------------------------------------------
/**
 * The function used for Creating Group head for One to Many Match.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION CREATE_MTCH_GROUP_HEAD(O_error_message     IN OUT VARCHAR2,
                                I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                                I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                                I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                                I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE,
                                I_match_type        IN     IM_MATCH_GROUP_HEAD_WS.MATCH_TYPE%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'REIM_AUTO_MATCH_SQL.CREATE_MTCH_GROUP_HEAD';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_key_id:' || I_match_key_id);

   --create im_match_group_head_ws rows for unmatched data
   insert into im_match_group_head_ws (workspace_id,
                                       match_group_id,
                                       match_stgy_id,
                                       match_stgy_dtl_id,
                                       match_luw_id,
                                       invc_group_id,
                                       rcpt_group_id,
                                       match_type,
                                       match_key_id,
                                       cost_discrepant,
                                       qty_discrepant,
                                       sku_compliant,
                                       match_available,
                                       match_status,
                                       persist_status,
                                       error_code,
                                       error_context)
        with im_mtch_group_rcpt_ws as (select distinct workspace_id,
                                                       match_luw_id,
                                                       match_key_id,
                                                       match_stgy_dtl_id,
                                                       rcpt_group_id
                                                  from im_match_group_rcpt_ws
                                                 where workspace_id      = I_workspace_id
                                                   and match_luw_id      = I_match_luw_id
                                                   and match_stgy_dtl_id = I_match_stgy_dtl_id
                                                   and match_key_id      = NVL(I_match_key_id, match_key_id)),
             im_mtch_group_invc_ws as (select distinct workspace_id,
                                                       match_luw_id,
                                                       match_key_id,
                                                       match_stgy_id,
                                                       match_stgy_dtl_id,
                                                       invc_group_id
                                                  from im_match_group_invc_ws
                                                 where workspace_id      = I_workspace_id
                                                   and match_luw_id      = I_match_luw_id
                                                   and match_stgy_dtl_id = I_match_stgy_dtl_id
                                                   and match_key_id      = NVL(I_match_key_id, match_key_id))
                                select I_workspace_id,
                                       im_match_group_seq.nextval,
                                       imgiw.match_stgy_id,
                                       imgiw.match_stgy_dtl_id,
                                       imgiw.match_luw_id,
                                       imgiw.invc_group_id,
                                       imgrw.rcpt_group_id,
                                       I_match_type match_type,
                                       imgiw.match_key_id,
                                       'N' cost_discrepant,
                                       'N' qty_discrepant,
                                       'N' sku_compliant,
                                       'N' match_available,
                                       REIM_CONSTANTS.DOC_STATUS_URMTCH match_status,
                                       'N' persist_status,
                                       NULL error_code,
                                       NULL error_context
                                  from im_mtch_group_invc_ws imgiw,
                                       im_mtch_group_rcpt_ws imgrw
                                 where imgiw.workspace_id      = I_workspace_id
                                   and imgiw.match_luw_id      = I_match_luw_id
                                   and imgiw.match_stgy_dtl_id = I_match_stgy_dtl_id
                                   and imgiw.match_key_id      = NVL(I_match_key_id, imgiw.match_key_id)
                                   and imgrw.workspace_id      = imgiw.workspace_id
                                   and imgrw.match_luw_id      = imgiw.match_luw_id
                                   and imgrw.match_key_id      = imgiw.match_key_id
                                   and imgrw.match_stgy_dtl_id = imgiw.match_stgy_dtl_id;

   LOGGER.LOG_INFORMATION('Insert IM_MATCH_GROUP_HEAD_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if ROLLUP_COST_AND_QTY(O_error_message,
                          I_workspace_id,
                          I_match_luw_id,
                          I_match_key_id,
                          I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   LOGGER.LOG_INFORMATION('End ' || L_program);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CREATE_MTCH_GROUP_HEAD;
------------------------------------------------------------------------------------------
/**
 * The function used for Matching Group head based on tolerance for One to Many Match.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION MATCH_GROUP_HEAD(O_error_message     IN OUT VARCHAR2,
                          I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                          I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                          I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                          I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'REIM_AUTO_MATCH_SQL.MATCH_GROUP_HEAD';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_key_id:' || I_match_key_id);

   merge into im_match_group_head_ws tgt
   using (with im_mtch_group_head_ws as (select gh.workspace_id,
                                                gh.match_group_id,
                                                gh.invc_group_cost,
                                                gh.rcpt_group_cost,
                                                gh.invc_group_qty,
                                                gh.rcpt_group_qty,
                                                gh.tolerance_id,
                                                gh.qty_match_required,
                                                gh.tolerance_exchange_rate,
                                                case when gh.invc_group_cost > gh.rcpt_group_cost
                                                    then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                                    else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                                end cost_favor_of,
                                                case when gh.invc_group_qty > gh.rcpt_group_qty
                                                    then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                                    else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                                end qty_favor_of
                                           from im_match_group_head_ws gh
                                          where gh.workspace_id      = I_workspace_id
                                            and gh.match_luw_id      = I_match_luw_id
                                            and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
                                            and gh.match_stgy_dtl_id = I_match_stgy_dtl_id),
                    im_tol_detl_cost as (select itd.*
                                           from im_tolerance_detail itd
                                          where itd.match_level      = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
                                            and itd.match_type       = REIM_CONSTANTS.TLR_MTCH_TYPE_COST),
                    im_tol_detl_qty as (select itd.*
                                           from im_tolerance_detail itd
                                          where itd.match_level      = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
                                            and itd.match_type       = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY)
          select imghw.workspace_id,
                 imghw.match_group_id,
                 (imghw.rcpt_group_cost - imghw.invc_group_cost) cost_variance,
                 (imghw.rcpt_group_qty - imghw.invc_group_qty) qty_variance,
                 case when itd_cost.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when itd_cost.tolerance_value < ABS(100 * (imghw.invc_group_cost - imghw.rcpt_group_cost) / imghw.rcpt_group_cost) then
                            'Y'
                         else
                            'N'
                    end
                 when itd_cost.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when itd_cost.tolerance_value < ABS((imghw.invc_group_cost - imghw.rcpt_group_cost) * imghw.tolerance_exchange_rate) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when imghw.invc_group_cost = imghw.rcpt_group_cost then
                            'N'
                         else
                            'Y'
                    end
                 end cost_discrepant,
                 --
                 case when imghw.qty_match_required = 'N' then
                    'N'
                 when itd_qty.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when itd_qty.tolerance_value < ABS(100 * (imghw.invc_group_qty - imghw.rcpt_group_qty) / imghw.rcpt_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 when itd_qty.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when itd_qty.tolerance_value < ABS(imghw.invc_group_qty - imghw.rcpt_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when imghw.invc_group_qty = imghw.rcpt_group_qty then
                            'N'
                         else
                            'Y'
                    end
                 end qty_discrepant,
                 --
                 case when
                    case when itd_cost.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                            case when itd_cost.tolerance_value < ABS(100 * (imghw.invc_group_cost - imghw.rcpt_group_cost) / imghw.rcpt_group_cost) then
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                            end
                         when itd_cost.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                            case when itd_cost.tolerance_value < ABS((imghw.invc_group_cost - imghw.rcpt_group_cost) * imghw.tolerance_exchange_rate) then
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                            end
                         else  --no tolerance found, exact match
                            case when imghw.invc_group_cost = imghw.rcpt_group_cost then
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                            end
                    end = REIM_CONSTANTS.DOC_STATUS_MTCH
                    --
                    and
                    --
                    case when imghw.qty_match_required = 'N' then
                            REIM_CONSTANTS.DOC_STATUS_MTCH
                         when itd_qty.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                            case when itd_qty.tolerance_value < ABS(100 * (imghw.invc_group_qty - imghw.rcpt_group_qty) / imghw.rcpt_group_qty) then
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                            end
                         when itd_qty.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                            case when itd_qty.tolerance_value < ABS(imghw.invc_group_qty - imghw.rcpt_group_qty) then
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                            end
                         else  --no tolerance found, exact match
                            case when imghw.invc_group_qty = imghw.rcpt_group_qty then
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                            end
                    end = REIM_CONSTANTS.DOC_STATUS_MTCH then
                       REIM_CONSTANTS.DOC_STATUS_MTCH
                    else
                       REIM_CONSTANTS.DOC_STATUS_URMTCH
                 end match_status,
                 itd_cost.tolerance_detail_id cost_tolerance_dtl_id,
                 itd_qty.tolerance_detail_id qty_tolerance_dtl_id
            from im_mtch_group_head_ws imghw,
                 im_tol_detl_cost itd_cost,
                 im_tol_detl_qty itd_qty
           where imghw.rcpt_group_qty     <> REIM_CONSTANTS.ZERO
             and imghw.rcpt_group_cost    <> REIM_CONSTANTS.ZERO
             --
             and itd_cost.tolerance_id(+) = imghw.tolerance_id
             and itd_cost.lower_limit(+) <= (imghw.invc_group_cost * imghw.tolerance_exchange_rate)
             and itd_cost.upper_limit(+)  > (imghw.invc_group_cost * imghw.tolerance_exchange_rate)
             and itd_cost.favor_of(+)     = imghw.cost_favor_of
             --
             and itd_qty.tolerance_id(+) = imghw.tolerance_id
             and itd_qty.lower_limit(+)  <= imghw.invc_group_qty
             and itd_qty.upper_limit(+)  > imghw.invc_group_qty
             and itd_qty.favor_of(+)     = imghw.qty_favor_of
             --
             ) src
   on (tgt.match_group_id = src.match_group_id)
   when MATCHED then
      update
         set tgt.cost_variance         = src.cost_variance,
             tgt.qty_variance          = src.qty_variance,
             tgt.cost_discrepant       = src.cost_discrepant,
             tgt.qty_discrepant        = src.qty_discrepant,
             tgt.sku_compliant         = DECODE(src.match_status,
                                                REIM_CONSTANTS.DOC_STATUS_MTCH, 'Y',
                                                'N'),
             tgt.match_available       = DECODE(src.match_status,
                                                REIM_CONSTANTS.DOC_STATUS_MTCH, 'Y',
                                                'N'),
             tgt.match_status          = src.match_status,
             tgt.cost_tolerance_dtl_id = src.cost_tolerance_dtl_id,
             tgt.qty_tolerance_dtl_id  = src.qty_tolerance_dtl_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_MATCH_GROUP_HEAD_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('End ' || L_program);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END MATCH_GROUP_HEAD;
------------------------------------------------------------------------------------------
/**
 * The function used for Checking SKU compliance.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION CHECK_SKU_COMPLIANCE(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                              I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                              I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                              I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'REIM_AUTO_MATCH_SQL.CHECK_SKU_COMPLIANCE';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_key_id:' || I_match_key_id);

   merge into im_match_group_head_ws target
   using (with gh as (select /*+ FULL(GH) */ gh.workspace_id,
                             gh.match_key_id,
                             gh.match_group_id,
                             gh.rcpt_group_id,
                             gh.invc_group_id,
                             gh.sku_comp_percent
                        from im_match_group_head_ws gh
                       where gh.workspace_id      = I_workspace_id
                         and gh.match_luw_id      = I_match_luw_id
                         and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
                         and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
                         and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH),
               rcpt as (select /*+ FULL(GR) INDEX( RD IM_MATCH_RCPT_DETL_WS_I3) */ distinct
                             gr.workspace_id,
                             gr.match_key_id,
                             gr.rcpt_group_id,
                               rd.item
                        from im_match_group_rcpt_ws gr,
                             im_match_rcpt_detl_ws rd
                       where gr.workspace_id      = I_workspace_id
                         and gr.match_luw_id      = I_match_luw_id
                         and gr.match_key_id      = NVL(I_match_key_id, gr.match_key_id)
                         and gr.match_stgy_dtl_id = I_match_stgy_dtl_id
                         --
                         and rd.workspace_id      = gr.workspace_id
                         and rd.match_luw_id      = gr.match_luw_id
                         and rd.match_key_id      = gr.match_key_id
                         and rd.shipment          = gr.shipment
                         and rd.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH),
               invc as (select /*+ FULL(GI) INDEX(ID IM_MATCH_INVC_DETL_WS_I3) */ distinct
                             gi.workspace_id,
                             gi.match_key_id,
                             gi.invc_group_id,
                               id.item
                        from im_match_group_invc_ws gi,
                             im_match_invc_detl_ws id
                       where gi.workspace_id      = I_workspace_id
                         and gi.match_luw_id      = I_match_luw_id
                         and gi.match_key_id      = NVL(I_match_key_id, gi.match_key_id)
                         and gi.match_stgy_dtl_id = I_match_stgy_dtl_id
                         --
                         and id.workspace_id      = gi.workspace_id
                         and id.match_luw_id      = gi.match_luw_id
                         and id.match_key_id      = gi.match_key_id
                         and id.doc_id            = gi.doc_id
                           and id.status       = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH)
          select inner.workspace_id,
                 inner.match_key_id,
                 inner.match_group_id,
                 min(inner.sku_compliant) sku_compliant
            from (select gh.workspace_id,
                         gh.match_key_id,
                         gh.match_group_id,
                         case when (SUM(DECODE(rcpt.item,
                                               invc.item, REIM_CONSTANTS.ONE,
                                               REIM_CONSTANTS.ZERO)) / count(rcpt.item)) * 100 <
                                      gh.sku_comp_percent then
                                 'N'
                              else
                                 'Y'
                         end sku_compliant
                    from gh,
                         rcpt,
                         invc
                   where gh.workspace_id   = rcpt.workspace_id
                     and gh.match_key_id   = rcpt.match_key_id
                     and gh.rcpt_group_id  = rcpt.rcpt_group_id
                     and gh.workspace_id   = invc.workspace_id
                     and gh.match_key_id   = invc.match_key_id
                     and gh.invc_group_id  = invc.invc_group_id
                     and rcpt.item         = invc.item (+)
                   group by gh.workspace_id,
                            gh.match_key_id,
                            gh.match_group_id,
                            gh.sku_comp_percent
                  union all
                  select gh.workspace_id,
                         gh.match_key_id,
                         gh.match_group_id,
                         case when (SUM(DECODE(invc.item,
                                               rcpt.item, REIM_CONSTANTS.ONE,
                                               REIM_CONSTANTS.ZERO)) / count(invc.item)) * 100 <
                                      gh.sku_comp_percent then
                                 'N'
                              else
                                 'Y'
                         end sku_compliant
                    from gh,
                         invc,
                         rcpt
                   where gh.workspace_id   = invc.workspace_id
                     and gh.match_key_id   = invc.match_key_id
                     and gh.invc_group_id  = invc.invc_group_id
                     and gh.workspace_id   = rcpt.workspace_id
                     and gh.match_key_id   = rcpt.match_key_id
                     and gh.rcpt_group_id  = rcpt.rcpt_group_id
                     and invc.item         = rcpt.item (+)
                   group by gh.workspace_id,
                            gh.match_key_id,
                            gh.match_group_id,
                            gh.sku_comp_percent) inner
           group by inner.workspace_id,
                    inner.match_key_id,
                    inner.match_group_id) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id   = use_this.match_key_id
       and target.match_group_id = use_this.match_group_id)
   when matched then
      update
    set target.sku_compliant    = use_this.sku_compliant,
        target.match_status     = DECODE(use_this.sku_compliant,
                                         'N', REIM_CONSTANTS.DOC_STATUS_URMTCH,
                                         target.match_status),
        target.error_code       = DECODE(use_this.sku_compliant,
                                         'N', 'sku_comp',               --constant
                                         NULL),
        target.error_context    = DECODE(use_this.sku_compliant,
                                         'N', 'sku_comp_context',               --constant
                                         NULL);

   LOGGER.LOG_INFORMATION('Merge IM_MATCH_GROUP_HEAD_WS SKU_COMPLIANCE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('End ' || L_program);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CHECK_SKU_COMPLIANCE;
------------------------------------------------------------------------------------------
/**
 * The function used for Determining the best match for the invoices.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION DETERMINE_BEST_MATCH(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                              I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                              I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                              I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(61) := 'REIM_AUTO_MATCH_SQL.DETERMINE_BEST_MATCH';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_match_key_rank     NUMBER := REIM_CONSTANTS.ZERO;
   L_max_match_key_rank NUMBER := REIM_CONSTANTS.ZERO;


BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_key_id:' || I_match_key_id);

   --get the max match key rank and Loop through.
        -- Find the best match for the invc_group (for the current match kay rank) and mark other matches as URMTCH.
        -- If there are more than one leave them as URMTCH.
        -- Mark other matches in the same key which have atleast one of the receipts participating in the match selected in the current match key rank.

   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(number_1, -- match_key_rank
                                             number_2, -- invc_group_id
                                             varchar2_1) -- match_key_id
      select RANK() OVER (PARTITION BY match_key_id
                              ORDER BY invc_group_id),
             invc_group_id,
             match_key_id
        from (select distinct imghw.invc_group_id,
                              imghw.match_key_id
                from im_match_group_head_ws imghw
               where imghw.workspace_id      = I_workspace_id
                 and imghw.match_luw_id      = I_match_luw_id
                 and imghw.match_stgy_dtl_id = I_match_stgy_dtl_id
                 and imghw.match_key_id      = NVL(I_match_key_id, imghw.match_key_id));

   LOGGER.LOG_INFORMATION('Insert Match Key ranks into GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select NVL(MAX(number_1), REIM_CONSTANTS.ZERO)
     into L_max_match_key_rank
     from gtt_num_num_str_str_date_date;

   for L_match_key_rank in 1 .. L_max_match_key_rank loop

      -- Best Match
      merge into im_match_group_head_ws tgt
      using (select gh.match_group_id,
                    RANK() OVER (PARTITION BY gh.workspace_id,
                                              gh.match_luw_id,
                                              gh.match_key_id,
                                              gh.match_stgy_id,
                                              gh.match_stgy_dtl_id,
                                              gh.invc_group_id
                                     ORDER BY ABS(gh.cost_variance),
                                              gh.cost_variance desc,
                                              DECODE(gh.qty_variance,
                                                     NULL, REIM_CONSTANTS.ZERO,
                                                     ABS(gh.qty_variance))) best_match_rank
               from gtt_num_num_str_str_date_date gtt,
                    im_match_group_head_ws gh
              where gtt.number_1     = L_match_key_rank
                and gh.invc_group_id = gtt.number_2
                and gh.match_key_id  = gtt.varchar2_1
                and gh.match_status  = REIM_CONSTANTS.DOC_STATUS_MTCH) src
      on (tgt.match_group_id = src.match_group_id)
      when MATCHED then
         update
            set tgt.match_status = DECODE(src.best_match_rank,
                                          REIM_CONSTANTS.ONE, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                          REIM_CONSTANTS.DOC_STATUS_URMTCH);

      LOGGER.LOG_INFORMATION('Merge for Best Match - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --Update multi best matches to URMTCH
      merge into im_match_group_head_ws tgt
      using (select gh.invc_group_id
               from gtt_num_num_str_str_date_date gtt,
                    im_match_group_head_ws gh
              where gtt.number_1     = L_match_key_rank
                and gh.invc_group_id = gtt.number_2
                and gh.match_key_id  = gtt.varchar2_1
                and gh.match_status  = REIM_CONSTANTS.DOC_STATUS_MTCH
              GROUP BY gh.invc_group_id
              HAVING COUNT(1) > REIM_CONSTANTS.ONE) src
      on (tgt.invc_group_id = src.invc_group_id)
      when MATCHED then
         update
            set tgt.match_status = REIM_CONSTANTS.DOC_STATUS_URMTCH;

      LOGGER.LOG_INFORMATION('Merge Multi Best Match to URMTCH - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --Update Matches for other invoice groups within the match key based on the current invoice group's match status.
      merge into im_match_group_head_ws tgt
      using (with rcpt_groups as (select distinct gr_sh.rcpt_group_id
                                    from gtt_num_num_str_str_date_date gtt,
                                         im_match_group_head_ws gh,
                                         im_match_group_rcpt_ws gr_gh,
                                         im_match_group_rcpt_ws gr_sh
                                   where gtt.number_1     = L_match_key_rank
                                     and gh.invc_group_id = gtt.number_2
                                     and gh.match_key_id  = gtt.varchar2_1
                                     and gh.match_status  = REIM_CONSTANTS.DOC_STATUS_MTCH
                                     and gr_gh.rcpt_group_id = gh.rcpt_group_id
                                     and gr_sh.workspace_id  = gr_gh.workspace_id
                                     and gr_sh.match_luw_id  = gr_gh.match_luw_id
                                     and gr_sh.match_key_id  = gr_gh.match_key_id
                                     and gr_sh.shipment      = gr_gh.shipment)
             select gh.match_group_id
               from gtt_num_num_str_str_date_date gtt,
                    im_match_group_head_ws gh,
                    rcpt_groups rg
              where gtt.number_1     > L_match_key_rank
                and gh.invc_group_id = gtt.number_2
                and gh.match_key_id  = gtt.varchar2_1
                and gh.match_status  = REIM_CONSTANTS.DOC_STATUS_MTCH
                and rg.rcpt_group_id = gh.rcpt_group_id) src
      on (tgt.match_group_id = src.match_group_id)
      when MATCHED then
         update
            set tgt.match_status = REIM_CONSTANTS.DOC_STATUS_URMTCH;

      LOGGER.LOG_INFORMATION('Merge Higher Match Keys to URMTCH - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end loop; -- Match Key Rank

   LOGGER.LOG_INFORMATION('End ' || L_program);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DETERMINE_BEST_MATCH;
------------------------------------------------------------------------------------------
/**
 * Update Workspace tables with Summary match results.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION UPD_WS_SMRY_MTCH_DATA(O_error_message     IN OUT VARCHAR2,
                               I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                               I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                               I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                               I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(61) := 'REIM_AUTO_MATCH_SQL.UPD_WS_SMRY_MTCH_DATA';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_key_id:' || I_match_key_id);

   merge into im_match_invc_ws target
   using (select /*+ ORDERED INDEX(GI IM_MATCH_GROUP_INVC_WS_I6) INDEX(MI IM_MATCH_INVC_WS_I22) */
                 gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 mi.doc_id
            from im_match_group_head_ws gh,
                 im_match_group_invc_ws gi,
                 im_match_invc_ws mi
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.invc_group_id     = gi.invc_group_id
             and gh.match_key_id      = gi.match_key_id
             and gi.doc_id            = mi.doc_id
             and gi.match_key_id      = mi.match_key_id
             and mi.status           != REIM_CONSTANTS.DOC_STATUS_MTCH
             and mi.match_group_id    is null
   ) use_this
   on (    target.workspace_id   = use_this.workspace_id
       and target.match_key_id   = use_this.match_key_id
       and target.doc_id         = use_this.doc_id)
   when matched then update
    set target.status         = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.match_group_id = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_invc_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ FULL(TARGET) */
   into im_match_invc_detl_ws target
   using (select /*+ ORDERED FULL(ID) INDEX(GH IM_MATCH_GROUP_HEAD_WS_I5) INDEX(GI IM_MATCH_GROUP_INVC_WS_I22) */
                 gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 id.doc_id,
                 id.item
            from im_match_group_head_ws gh,
                 im_match_group_invc_ws gi,
                 im_match_invc_detl_ws id
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.invc_group_id     = gi.invc_group_id
             and gh.match_key_id      = gi.match_key_id
             and gi.doc_id            = id.doc_id
             and gi.match_key_id      = id.match_key_id
             and id.status           != REIM_CONSTANTS.DOC_STATUS_MTCH
             and id.match_group_id    is null
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id
       and target.doc_id       = use_this.doc_id
       and target.item         = use_this.item)
   when matched then update
    set target.status         = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.cost_matched   = 'Y',
        target.qty_matched    = 'Y',
        target.match_group_id = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_invc_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ INDEX(TARGET IM_MATCH_RCPT_WS_I3) */
   into im_match_rcpt_ws target
   using (select /*+ ORDERED INDEX(GH IM_MATCH_GROUP_HEAD_WS_I4) INDEX(GR IM_MATCH_GROUP_RCPT_WS_I6) */
                 gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 ri.shipment
            from im_match_group_head_ws gh,
                 im_match_group_rcpt_ws gr,
                 im_match_rcpt_ws ri
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.rcpt_group_id     = gr.rcpt_group_id
             and gh.match_key_id      = gr.match_key_id
             and gr.shipment          = ri.shipment
             and gr.match_key_id      = ri.match_key_id
             and ri.invc_match_status = REIM_CONSTANTS.SHIP_STATUS_UNMTCH
             and ri.match_group_id    is null
   ) use_this
   on (    target.workspace_id   = use_this.workspace_id
       and target.match_key_id   = use_this.match_key_id
       and target.shipment       = use_this.shipment)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_MTCH,
        target.match_group_id    = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_rcpt_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ INDEX(TARGET IM_MATCH_RCPT_DETL_WS_I3) */
   into im_match_rcpt_detl_ws target
   using (select /*+ORDERED */
                 gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 rd.shipment,
                 rd.item
            from im_match_group_head_ws gh,
                 im_match_group_rcpt_ws gr,
                 im_match_rcpt_detl_ws rd
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.rcpt_group_id     = gr.rcpt_group_id
             and gh.match_key_id      = gr.match_key_id
             and gr.workspace_id      = rd.workspace_id
             and gr.shipment          = rd.shipment
             and gr.match_key_id      = rd.match_key_id
             and rd.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
             and rd.match_group_id    is null
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id
       and target.shipment     = use_this.shipment
       and target.item         = use_this.item)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
        target.match_group_id    = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_rcpt_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if UPD_TAXDIS_SMRY(O_error_message,
                      I_workspace_id,
                      I_match_luw_id,
                      I_match_key_id,
                      I_match_stgy_dtl_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UPD_WS_SMRY_MTCH_DATA;
------------------------------------------------------------------------------------------
FUNCTION CHECK_TAX_COMPLIANCE(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                              I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                              I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                              I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER IS

   L_program            VARCHAR2(61)   := 'REIM_AUTO_MATCH_SQL.CHECK_TAX_COMPLIANCE';
   L_start_time         TIMESTAMP      := SYSTIMESTAMP;

   L_status             NUMBER(1);
   L_item_tax_criteria  OBJ_ITEM_TAX_CRITERIA_TBL  := null;
   L_item_tax_calc_ovrd OBJ_ITEM_TAX_CALC_OVRD_TBL := null;
   L_item_tax_results   OBJ_ITEM_TAX_BREAK_TBL     := null;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_key_id:' || I_match_key_id ||
                          ' I_match_stgy_dtl_id:' || I_match_stgy_dtl_id);

   delete from im_match_invc_tax_ws w
    where w.workspace_id      = I_workspace_id
      and w.match_luw_id      = I_match_luw_id
      and w.match_key_id      = nvl(I_match_key_id,w.match_key_id)
      and w.match_stgy_dtl_id = I_match_stgy_dtl_id;

   LOGGER.LOG_INFORMATION(L_program||' delete im_match_invc_tax_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_rcpt_tax_ws w
    where w.workspace_id      = I_workspace_id
      and w.match_luw_id      = I_match_luw_id
      and w.match_key_id      = nvl(I_match_key_id,w.match_key_id)
      and w.match_stgy_dtl_id = I_match_stgy_dtl_id;

   LOGGER.LOG_INFORMATION(L_program||' delete im_match_rcpt_tax_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from gtt_6_num_6_str_6_date;

   insert into gtt_6_num_6_str_6_date(number_1,
                                      number_2,
                                      number_3,
                                      number_4,
                                      number_5,
                                      number_6,
                                      varchar2_1,
                                      varchar2_2)
   select i.workspace_id,
          i.match_luw_id,
          i.match_key_id,
          i.match_group_id,
          i.invc_group_id,
          i.rcpt_group_id,
          i.doc_id,
          i.header_only
     from (select /*+ FULL(IW) INDEX(GH IM_MATCH_GROUP_HEAD_WS_I5) */
                  gh.workspace_id,
                  gh.match_luw_id,
                  gh.match_key_id,
                  gh.match_group_id,
                  gh.invc_group_id,
                  gh.rcpt_group_id,
                  iw.doc_id,
                  iw.header_only,
                  max(iw.header_only) over (partition by gh.workspace_id, gh.match_key_id, gh.match_group_id,gh.invc_group_id) max_header_only
             from im_match_group_head_ws gh,
                  im_match_group_invc_ws gi,
                  im_match_invc_ws iw
            where gh.workspace_id      = I_workspace_id
              and gh.match_luw_id      = I_match_luw_id
              and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
              and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
              and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
              and gh.invc_group_id     = gi.invc_group_id(+)
              and gh.workspace_id      = iw.workspace_id
              and gh.match_luw_id      = iw.match_luw_id
              and gh.match_key_id      = iw.match_key_id
              and nvl(gi.doc_id, iw.doc_id) = iw.doc_id) i
    where i.max_header_only = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' Insert gtt_6_num_6_str_6_date header only - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_invc_tax_ws (workspace_id,
                                     match_luw_id,
                                     match_key_id,
                                     match_stgy_dtl_id,
                                     tax_code,
                                     tax_rate,
                                     tax_basis,
                                     tax_amount)
   with gtt as (select number_1   workspace_id,
                       number_2   match_luw_id,
                       number_3   match_key_id,
                       number_4   match_group_id,
                       number_5   invc_group_id,
                       number_6   rcpt_group_id,
                       varchar2_1 doc_id,
                       varchar2_2 header_only
                  from gtt_6_num_6_str_6_date)
   select gtt.workspace_id,
          gtt.match_luw_id,
          gtt.match_key_id,
          I_match_stgy_dtl_id,
          td.tax_code,
          td.tax_rate,
          td.tax_basis,
          td.tax_amount
     from gtt,
          im_match_invc_detl_ws mid,
          im_invoice_detail_tax td
    where gtt.workspace_id  = mid.workspace_id
      and gtt.match_luw_id  = mid.match_luw_id
      and gtt.match_key_id  = mid.match_key_id
      and gtt.doc_id        = mid.doc_id
      and gtt.header_only   = 'N'
      and mid.doc_id        = td.doc_id
      and mid.item          = td.item
   union all
   select dtax.workspace_id,
          dtax.match_luw_id,
          dtax.match_key_id,
          I_match_stgy_dtl_id,
          dtax.tax_code,
          dtax.tax_rate,
          dtax.tax_basis,
          --
          dtax.tax_amount - nvl(nmtax.tax_amount,0) final_amt
     from (select gtt.workspace_id,
                  gtt.match_luw_id,
                  gtt.match_key_id,
                  gtt.doc_id,
                  dt.tax_code,
                  dt.tax_rate,
                  dt.tax_basis,
                  sum(dt.tax_amount) tax_amount
             from gtt,
                  im_doc_tax dt
            where gtt.header_only = 'Y'
              and gtt.doc_id      = dt.doc_id
            group by gtt.workspace_id,
                     gtt.match_luw_id,
                     gtt.match_key_id,
                     gtt.doc_id,
                     dt.tax_code,
                     dt.tax_rate,
                     dt.tax_basis) dtax,
          (select gtt.workspace_id,
                  gtt.match_luw_id,
                  gtt.match_key_id,
                  gtt.doc_id,
                  mt.tax_code,
                  mt.tax_rate,
                  mt.tax_basis,
                  sum(mt.tax_amount) tax_amount
             from gtt,
                  im_doc_non_merch_tax mt
            where gtt.header_only = 'Y'
              and gtt.doc_id      = mt.doc_id
            group by gtt.workspace_id,
                     gtt.match_luw_id,
                     gtt.match_key_id,
                     gtt.doc_id,
                     mt.tax_code,
                     mt.tax_rate,
                     mt.tax_basis) nmtax
    where dtax.workspace_id = nmtax.workspace_id(+)
      and dtax.match_luw_id = nmtax.match_luw_id(+)
      and dtax.match_key_id = nmtax.match_key_id(+)
      and dtax.doc_id       = nmtax.doc_id(+)
      and dtax.tax_code     = nmtax.tax_code(+)
      and dtax.tax_rate     = nmtax.tax_rate(+)
      and (nmtax.tax_amount  is null
          or
          dtax.tax_amount - nmtax.tax_amount > 0);

   LOGGER.LOG_INFORMATION(L_program||' Insert im_match_invc_tax_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select OBJ_ITEM_TAX_CRITERIA_REC(ITEM        => i.item,
                                    SUPPLIER    => i.supplier_site_id,
                                    LOCATION    => i.bill_to_loc,
                                    EFF_DATE    => i.receive_date,
                                    UNIT_COST   => i.ext_cost,
                                    MRP         => null,
                                    RETAIL      => null,
                                    FREIGHT     => null,
                                    TAX_TYPE    => REIM_CONSTANTS.TAX_TYPE_COST)
     bulk collect into L_item_tax_criteria
     from (select distinct mrd.item,
                           mr.supplier_site_id,
                           mr.bill_to_loc,
                           mr.receive_date,
                           mrd.unit_cost * mrd.qty_available ext_cost
                      from (select distinct number_1   workspace_id,
                                            number_2   match_luw_id,
                                            number_3   match_key_id,
                                            number_4   match_group_id,
                                            number_5   invc_group_id,
                                            number_6   rcpt_group_id,
                                            varchar2_2 header_only
                              from gtt_6_num_6_str_6_date) gtt,
                           im_match_group_rcpt_ws rg,
                           im_match_rcpt_ws mr,
                           im_match_rcpt_detl_ws mrd
                     where gtt.rcpt_group_id            = rg.rcpt_group_id(+)
                       and nvl(rg.shipment,mr.shipment) = mr.shipment
                       and mr.workspace_id              = gtt.workspace_id
                       and mr.match_luw_id              = gtt.match_luw_id
                       and mrd.match_key_id             = gtt.match_key_id
                       --
                       and mr.workspace_id              = mrd.workspace_id
                       and mr.match_key_id              = mrd.match_key_id
                       and mr.shipment                  = mrd.shipment) i;

   LOGGER.LOG_INFORMATION(L_program||' Get L_item_tax_criteria - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_TAX_WRAPPER_SQL.CALCULATE_ITEM_TAXES_SINGLETAX(L_status,
                                                       O_error_message,
                                                       L_item_tax_criteria,
                                                       L_item_tax_calc_ovrd,
                                                       L_item_tax_results);
   if L_status = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   insert into im_match_rcpt_tax_ws (workspace_id,
                                     match_luw_id,
                                     match_key_id,
                                     match_stgy_dtl_id,
                                     tax_code,
                                     tax_rate,
                                     tax_basis,
                                     tax_amount)
   select /*+ FULL(MR) FULL(MRD) */ mr.workspace_id,
          mr.match_luw_id,
          mr.match_key_id,
          I_match_stgy_dtl_id,
          r.tax_code,
          r.tax_rate,
          r.tax_basis,
          r.tax_amount
     from TABLE(CAST(L_item_tax_results as OBJ_ITEM_TAX_BREAK_TBL)) r,
          im_match_rcpt_ws mr,
          im_match_rcpt_detl_ws mrd
    where r.item          = mrd.item
      and r.supplier      = mr.supplier_site_id
      and r.location      = mr.bill_to_loc
      and r.eff_date      = mr.receive_date
      and r.tax_basis     = mrd.unit_cost * qty_available
      --
      and mr.workspace_id = I_workspace_id
      and mr.match_luw_id = I_match_luw_id
      and mr.match_key_id = NVL(I_match_key_id, mr.match_key_id)
      and mr.match_key_id is not null
      --
      and mr.workspace_id = mrd.workspace_id
      and mr.match_luw_id = mrd.match_luw_id
      and mr.shipment     = mrd.shipment;

   LOGGER.LOG_INFORMATION(L_program||' insert im_match_rcpt_tax_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_group_head_ws target
   using (select distinct inner.workspace_id,
                          inner.match_key_id,
                          inner.match_stgy_dtl_id
            from (
                  (select idoc.workspace_id,
                          idoc.match_key_id,
                          idoc.match_stgy_dtl_id,
                          idoc.tax_code,
                          idoc.tax_rate
                     from im_match_invc_tax_ws idoc
                    where idoc.workspace_id      = I_workspace_id
                      and idoc.match_luw_id      = I_match_luw_id
                      and idoc.match_key_id      = NVL(I_match_key_id, idoc.match_key_id)
                      and idoc.match_stgy_dtl_id = I_match_stgy_dtl_id
                   minus
                   select rcpt.workspace_id,
                          rcpt.match_key_id,
                          rcpt.match_stgy_dtl_id,
                          rcpt.tax_code,
                          rcpt.tax_rate
                     from im_match_rcpt_tax_ws rcpt
                    where rcpt.workspace_id      = I_workspace_id
                      and rcpt.match_luw_id      = I_match_luw_id
                      and rcpt.match_key_id      = NVL(I_match_key_id, rcpt.match_key_id)
                      and rcpt.match_stgy_dtl_id = I_match_stgy_dtl_id)
                  union all
                  (select rcpt.workspace_id,
                          rcpt.match_key_id,
                          rcpt.match_stgy_dtl_id,
                          rcpt.tax_code,
                          rcpt.tax_rate
                     from im_match_rcpt_tax_ws rcpt
                    where rcpt.workspace_id      = I_workspace_id
                      and rcpt.match_luw_id      = I_match_luw_id
                      and rcpt.match_key_id      = NVL(I_match_key_id, rcpt.match_key_id)
                      and rcpt.match_stgy_dtl_id = I_match_stgy_dtl_id
                   minus
                   select idoc.workspace_id,
                          idoc.match_key_id,
                          idoc.match_stgy_dtl_id,
                          idoc.tax_code,
                          idoc.tax_rate
                     from im_match_invc_tax_ws idoc
                    where idoc.workspace_id      = I_workspace_id
                      and idoc.match_luw_id      = I_match_luw_id
                      and idoc.match_key_id      = NVL(I_match_key_id, idoc.match_key_id)
                      and idoc.match_stgy_dtl_id = I_match_stgy_dtl_id)
              ) inner
   ) use_this
   on (    target.workspace_id      = use_this.workspace_id
       and target.match_key_id      = use_this.match_key_id
       and target.match_stgy_dtl_id = use_this.match_stgy_dtl_id)
   when matched then update
    set target.tax_compliant    = 'N',
        target.match_status     = decode(target.match_status,
                                         REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.DOC_STATUS_TAXDIS,
                                         target.match_status);

   LOGGER.LOG_INFORMATION(L_program||' merge tax compliant im_match_group_head_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CHECK_TAX_COMPLIANCE;
------------------------------------------------------------------------------------------
/**
 *  --------- PRIVATE PROCS AND FUNCTIONS BODIES --------------
 */
----------------------------------------------------------------------------------------------
-- The private procedure used for logging Auto match status information in IM_AUTO_MATCH_STATUS.
----------------------------------------------------------------------------------------------
PROCEDURE LOG_AUTO_MATCH_STATUS(O_error_message IN OUT VARCHAR2,
                                I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                I_status        IN     IM_AUTO_MATCH_STATUS.STATUS%TYPE)
IS
   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.LOG_AUTO_MATCH_STATUS';

BEGIN

   if I_status = STATUS_INIT then

      insert into IM_AUTO_MATCH_STATUS(workspace_id,
                                       start_datetime,
                                       status)
                               values (I_workspace_id,
                                       sysdate,
                                       STATUS_INIT);

   else

      update IM_AUTO_MATCH_STATUS
         set status       = I_status,
             end_datetime = DECODE(I_status,
                                   STATUS_FAILED, SYSDATE,
                                   STATUS_SUCCESS, SYSDATE,
                                   NULL),
             error_msg    = O_error_message
       where workspace_id = I_workspace_id;

   end if;

   COMMIT;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

END LOG_AUTO_MATCH_STATUS;
------------------------------------------------------------------------------------------
--The private procedure used for logging Auto match task status information in IM_AUTO_MATCH_TASK_STATUS.
----------------------------------------------------------------------------------------------
PROCEDURE LOG_AUTO_MATCH_TASK_STATUS(O_error_message       IN OUT VARCHAR2,
                                     I_workspace_id        IN     IM_AUTO_MATCH_TASK_STATUS.WORKSPACE_ID%TYPE,
                                     I_match_stgy_dtl_id   IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_STGY_DTL_ID%TYPE,
                                     I_chunk_num           IN     IM_AUTO_MATCH_TASK_STATUS.CHUNK_NUM%TYPE,
                                     I_match_luw_id        IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                     I_task_level          IN     IM_AUTO_MATCH_TASK_STATUS.TASK_LEVEL%TYPE,
                                     I_task_type           IN     IM_AUTO_MATCH_TASK_STATUS.TASK_TYPE%TYPE,
                                     I_doc_count           IN     IM_AUTO_MATCH_TASK_STATUS.DOC_COUNT%TYPE,
                                     I_summary_match_count IN     IM_AUTO_MATCH_TASK_STATUS.SUMMARY_MATCH_COUNT%TYPE,
                                     I_detail_match_count  IN     IM_AUTO_MATCH_TASK_STATUS.DETAIL_MATCH_COUNT%TYPE,
                                     I_disc_item_count     IN     IM_AUTO_MATCH_TASK_STATUS.DISC_ITEM_COUNT%TYPE,
                                     I_disc_reslvd_count   IN     IM_AUTO_MATCH_TASK_STATUS.DISC_RESLVD_COUNT%TYPE,
                                     I_status              IN     IM_AUTO_MATCH_TASK_STATUS.STATUS%TYPE)
IS

   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program        VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.LOG_AUTO_MATCH_TASK_STATUS';
   L_match_stgy_id  im_match_stgy_detail.match_stgy_id%TYPE := null;

BEGIN

  if I_match_stgy_dtl_id is not null then
     select match_stgy_id
       into L_match_stgy_id
       from im_match_stgy_detail
      where match_stgy_dtl_id = I_match_stgy_dtl_id;
  end if;

   merge into im_auto_match_task_status tgt
   using (select I_workspace_id workspace_id,
                 L_match_stgy_id match_stgy_id,
                 I_match_stgy_dtl_id match_stgy_dtl_id,
                 I_chunk_num chunk_num,
                 I_match_luw_id match_luw_id,
                 I_task_level task_level,
                 I_task_type task_type,
                 I_doc_count doc_count,
                 I_summary_match_count summary_match_count,
                 I_detail_match_count detail_match_count,
                 I_disc_item_count disc_item_count,
                 I_disc_reslvd_count disc_reslvd_count,
                 I_status status,
                 O_error_message error_msg
            from dual) src
   on (    tgt.workspace_id                 = src.workspace_id
       and NVL(tgt.match_stgy_dtl_id, -999) = NVL(src.match_stgy_dtl_id, -999)
       and NVL(tgt.chunk_num, -999)         = NVL(src.chunk_num, -999)
       and NVL(tgt.match_luw_id, -999)      = NVL(src.match_luw_id, -999)
       and tgt.task_level                   = src.task_level
       and tgt.task_type                    = src.task_type)
   when MATCHED then
      update
         set tgt.doc_count           = NVL(src.doc_count, tgt.doc_count),
             tgt.summary_match_count = NVL(src.summary_match_count, tgt.summary_match_count),
             tgt.detail_match_count  = NVL(src.detail_match_count, tgt.detail_match_count),
             tgt.disc_item_count     = NVL(src.disc_item_count, tgt.disc_item_count),
             tgt.disc_reslvd_count   = NVL(src.disc_reslvd_count, tgt.disc_reslvd_count),
             tgt.status              = src.status,
             tgt.error_msg           = src.error_msg,
             tgt.end_datetime        = SYSDATE
   when NOT MATCHED then
      insert (task_id,
              workspace_id,
              match_stgy_id,
              match_stgy_dtl_id,
              chunk_num,
              match_luw_id,
              task_level,
              task_type,
              doc_count,
              summary_match_count,
              detail_match_count,
              disc_item_count,
              disc_reslvd_count,
              start_datetime,
              status,
              error_msg)
      values (IM_TASK_ID_SEQ.NEXTVAL,
              src.workspace_id,
              src.match_stgy_id,
              src.match_stgy_dtl_id,
              src.chunk_num,
              src.match_luw_id,
              src.task_level,
              src.task_type,
              src.doc_count,
              src.summary_match_count,
              src.detail_match_count,
              src.disc_item_count,
              src.disc_reslvd_count,
              SYSDATE,
              src.status,
              src.error_msg);

   COMMIT;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
END LOG_AUTO_MATCH_TASK_STATUS;
------------------------------------------------------------------------------------------
/**
 * The private function that rolls up cost and qty from child tables to group head table.
 * Used for Summary One to Many (Batch) and Suggest Match (Online) functionalities.
 */
FUNCTION ROLLUP_COST_AND_QTY(O_error_message IN OUT VARCHAR2,
                             I_workspace_id  IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                             I_match_luw_id  IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                             I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                             I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER IS

   L_program            VARCHAR2(61)   := 'REIM_AUTO_MATCH_SQL.ROLLUP_COST_AND_QTY';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_key_id:'    || I_match_key_id);

   merge /*+ INDEX(TARGET IM_MATCH_GROUP_HEAD_WS_I5) */
   into im_match_group_head_ws target
   using (select inner.workspace_id,
                 inner.match_stgy_id,
                 inner.match_stgy_dtl_id,
                 inner.match_key_id,
                 inner.invc_group_id,
                 SUM(inner.invc_group_cost) invc_group_cost,
                 SUM(inner.invc_group_qty) invc_group_qty,
                 MAX(inner.tolerance_id) tolerance_id,
                 MAX(inner.tolerance_exchange_rate) tolerance_exchange_rate,
                 MAX(inner.tolerance_currency_code) tolerance_currency_code,
                 MIN(inner.qty_match_required) qty_match_required,
                 DECODE(MAX(inner.header_only),
                        'N', MAX(inner.sku_comp_percent),
                        REIM_CONSTANTS.ZERO) sku_comp_percent
            from (select /*+ FULL(GI) INDEX(IW PK_IM_MATCH_INVC_WS) INDEX(ID PK_IM_MATCH_INVC_DETL_WS) */
                         gi.workspace_id,
                         gi.match_stgy_id,
                         gi.match_stgy_dtl_id,
                         gi.match_key_id,
                         gi.invc_group_id,
                         SUM(id.unit_cost * DECODE(id.qty_matched,
                                                   'N', id.invoice_qty,
                                                   'D', id.invoice_qty,
                                                   REIM_CONSTANTS.ZERO)) invc_group_cost,
                         SUM(DECODE(id.qty_matched,
                                    'N', id.invoice_qty,
                                    'D', id.invoice_qty,
                                    REIM_CONSTANTS.ZERO)) invc_group_qty,
                         MAX(iw.tolerance_id) tolerance_id,
                         MAX(iw.tolerance_exchange_rate) tolerance_exchange_rate,
                         MAX(iw.tolerance_currency_code) tolerance_currency_code,
                         MIN(iw.qty_match_required) qty_match_required,
                         MAX(iw.sku_comp_percent) sku_comp_percent,
                         'N' header_only
                    from im_match_invc_detl_ws id,
                         im_match_invc_ws iw,
                         im_match_group_invc_ws gi
                   where gi.workspace_id      = I_workspace_id
                     and gi.match_luw_id      = I_match_luw_id
                     and gi.match_stgy_dtl_id = I_match_stgy_dtl_id
                     and gi.match_key_id      = NVL(I_match_key_id, gi.match_key_id)
                     and iw.workspace_id      = gi.workspace_id
                     and iw.doc_id            = gi.doc_id
                     and iw.header_only       = 'N'
                     and id.workspace_id      = iw.workspace_id
                     and id.doc_id            = iw.doc_id
                   GROUP BY gi.workspace_id,
                            gi.match_stgy_id,
                            gi.match_stgy_dtl_id,
                            gi.match_key_id,
                            gi.invc_group_id
                      union all
                  select  /*+ FULL(GI) INDEX(IW PK_IM_MATCH_INVC_WS) */
                         gi.workspace_id,
                         gi.match_stgy_id,
                         gi.match_stgy_dtl_id,
                         gi.match_key_id,
                         gi.invc_group_id,
                         SUM(DECODE(iw.status,
                                    REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                    iw.total_avail_cost)) invc_group_cost,
                         SUM(DECODE(iw.status,
                                    REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                    iw.total_avail_qty)) invc_group_qty,
                         MAX(iw.tolerance_id) tolerance_id,
                         MAX(iw.tolerance_exchange_rate) tolerance_exchange_rate,
                         MAX(iw.tolerance_currency_code) tolerance_currency_code,
                         MIN(iw.qty_match_required) qty_match_required,
                         MAX(iw.sku_comp_percent) sku_comp_percent,
                         'Y' header_only
                    from im_match_group_invc_ws gi,
                         im_match_invc_ws iw
                   where gi.workspace_id      = I_workspace_id
                     and gi.match_luw_id      = I_match_luw_id
                     and gi.match_stgy_dtl_id = I_match_stgy_dtl_id
                     and gi.match_key_id      = NVL(I_match_key_id, gi.match_key_id)
                     and iw.workspace_id      = gi.workspace_id
                     and iw.doc_id            = gi.doc_id
                     and iw.header_only       = 'Y'
                   GROUP BY gi.workspace_id,
                            gi.match_stgy_id,
                            gi.match_stgy_dtl_id,
                            gi.match_key_id,
                            gi.invc_group_id) inner
           GROUP BY inner.workspace_id,
                    inner.match_stgy_id,
                    inner.match_stgy_dtl_id,
                    inner.match_key_id,
                    inner.invc_group_id
   ) use_this
   on (    target.workspace_id      = use_this.workspace_id
       and target.match_stgy_dtl_id = use_this.match_stgy_dtl_id
       and target.match_key_id      = use_this.match_key_id
       and target.invc_group_id     = use_this.invc_group_id)
   when MATCHED then
      update set target.invc_group_cost          = use_this.invc_group_cost,
                 target.invc_group_qty           = use_this.invc_group_qty,
                 target.tolerance_id             = use_this.tolerance_id,
                 target.tolerance_exchange_rate  = use_this.tolerance_exchange_rate,
                 target.tolerance_currency_code  = use_this.tolerance_currency_code,
                 target.qty_match_required       = use_this.qty_match_required,
                 target.sku_comp_percent         = use_this.sku_comp_percent;

   merge /*+ INDEX(TARGET IM_MATCH_GROUP_HEAD_WS_I5) */
         into im_match_group_head_ws target
   using (select /*+ FULL(RD) INDEX(GR PK_IM_MATCH_GROUP_RCPT_WS) */
                 gr.workspace_id,
                 --gr.match_stgy_id,
                 gr.match_stgy_dtl_id,
                 gr.match_key_id,
                 gr.rcpt_group_id,
                 SUM(rd.unit_cost * DECODE(rd.invc_match_status,
                                           REIM_CONSTANTS.SSKU_IM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                           rd.qty_available)) rcpt_group_cost,
                 SUM(DECODE(rd.invc_match_status,
                            REIM_CONSTANTS.SSKU_IM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            rd.qty_available)) rcpt_group_qty
           from im_match_group_rcpt_ws gr,
                im_match_rcpt_detl_ws rd
          where gr.workspace_id      = I_workspace_id
            and gr.match_luw_id      = I_match_luw_id
            and gr.match_stgy_dtl_id = I_match_stgy_dtl_id
            and gr.match_key_id      = NVL(I_match_key_id, gr.match_key_id)
            and rd.workspace_id      = gr.workspace_id
            and rd.shipment          = gr.shipment
          GROUP BY gr.workspace_id,
                   --gr.match_stgy_id,
                   gr.match_stgy_dtl_id,
                   gr.match_key_id,
                   gr.rcpt_group_id
   ) use_this
   on (    target.workspace_id      = use_this.workspace_id
       and target.match_stgy_dtl_id = use_this.match_stgy_dtl_id
       and target.match_key_id      = use_this.match_key_id
       and target.rcpt_group_id     = use_this.rcpt_group_id)
   when MATCHED then
      update set target.rcpt_group_cost = use_this.rcpt_group_cost,
                 target.rcpt_group_qty  = use_this.rcpt_group_qty;

   LOGGER.LOG_INFORMATION('End ' || L_program);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END ROLLUP_COST_AND_QTY;
------------------------------------------------------------------------------------------
FUNCTION UPD_TAXDIS_SMRY(O_error_message     IN OUT VARCHAR2,
                         I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                         I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                         I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                         I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(61) := 'REIM_AUTO_MATCH_SQL.UPD_TAXDIS_SMRY';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_match_stgy_id         im_match_stgy_detail.match_stgy_id%TYPE;
   L_match_stgy_rank       im_match_stgy_detail.match_stgy_rank%TYPE;
   L_match_level           im_match_stgy_detail.match_level%TYPE;
   L_max_match_stgy_rank   im_match_stgy_detail.match_stgy_rank%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_luw_id:' || I_match_luw_id ||
                          ' I_match_key_id:' || I_match_key_id ||
                          ' I_match_stgy_dtl_id:' || I_match_stgy_dtl_id);

   select sd.match_stgy_id, sd.match_stgy_rank, sd.match_level
     into L_match_stgy_id, L_match_stgy_rank, L_match_level
     from im_match_stgy_detail sd
    where sd.match_stgy_dtl_id = I_match_stgy_dtl_id;

   select max(match_stgy_rank)
     into L_max_match_stgy_rank
     from im_match_stgy_detail
    where match_stgy_id = L_match_stgy_id;

   --if last detail in strategy, push any TAXDIS to workspace
   if L_match_stgy_rank = L_max_match_stgy_rank and
      I_match_key_id is null then

      if L_match_level = REIM_CONSTANTS.MATCH_LEVEL_SUMM_1_2_MANY then
         merge into im_match_invc_detl_ws target
         using (select /*+ ORDERED */ distinct gh.workspace_id,
                       gh.match_key_id,
                       gi.doc_id,
                       gh.match_status
                  from im_match_group_head_ws gh,
                       im_match_group_invc_ws gi
                 where gh.workspace_id      = I_workspace_id
                   and gh.match_luw_id      = I_match_luw_id
                   and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
                   and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
                   and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_TAXDIS
                   and gh.invc_group_id     = gi.invc_group_id
         ) use_this
         on (    target.workspace_id = use_this.workspace_id
             and target.match_key_id = use_this.match_key_id
             and target.doc_id       = use_this.doc_id)
         when matched then update
          set target.status = decode(target.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, target.status, REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH);

         LOGGER.LOG_INFORMATION(L_program||' Merge 1_2_many TAXDIS to im_match_invc_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         merge into im_match_invc_ws target
         using (select /*+ ORDERED */ distinct gh.workspace_id,
                       gh.match_key_id,
                       gi.doc_id,
                       gh.match_status
                  from im_match_group_head_ws gh,
                       im_match_group_invc_ws gi
                 where gh.workspace_id      = I_workspace_id
                   and gh.match_luw_id      = I_match_luw_id
                   and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
                   and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
                   and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_TAXDIS
                   and gh.invc_group_id     = gi.invc_group_id
         ) use_this
         on (    target.workspace_id = use_this.workspace_id
             and target.match_key_id = use_this.match_key_id
             and target.doc_id       = use_this.doc_id)
         when matched then update
          set target.status = decode(target.status,
                                     REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, target.status,
                                     decode(target.header_only,
                                            'Y', REIM_CONSTANTS.DOC_STATUS_TAXDIS,
                                            REIM_CONSTANTS.DOC_STATUS_URMTCH));

         LOGGER.LOG_INFORMATION(L_program||' Merge 1_2_many TAXDIS to im_match_invc_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      elsif L_match_level = REIM_CONSTANTS.MATCH_LEVEL_SUMM_ALL_2_ALL then

         merge into im_match_invc_detl_ws target
         using (select /*+ ORDERED */ distinct gh.workspace_id,
                       gh.match_key_id,
                       mi.doc_id,
                       gh.match_status
                  from im_match_group_head_ws gh,
                       im_match_invc_ws mi
                 where gh.workspace_id      = I_workspace_id
                   and gh.match_luw_id      = I_match_luw_id
                   and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
                   and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
                   and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_TAXDIS
                   --
                   and mi.workspace_id      = I_workspace_id
                   and mi.match_luw_id      = I_match_luw_id
                   and mi.match_key_id      = NVL(I_match_key_id, mi.match_key_id)
         ) use_this
         on (    target.workspace_id = use_this.workspace_id
             and target.match_key_id = use_this.match_key_id
             and target.doc_id       = use_this.doc_id)
         when matched then update
          set target.status = decode(target.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, target.status, REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH);

         LOGGER.LOG_INFORMATION(L_program||' ALL 2 ALL Merge TAXDIS to im_match_invc_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         merge into im_match_invc_ws target
         using (select /*+ ORDERED */ distinct gh.workspace_id,
                       gh.match_key_id,
                       mi.doc_id,
                       gh.match_status
                  from im_match_group_head_ws gh,
                       im_match_invc_ws mi
                 where gh.workspace_id      = I_workspace_id
                   and gh.match_luw_id      = I_match_luw_id
                   and gh.match_key_id      = NVL(I_match_key_id, gh.match_key_id)
                   and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
                   and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_TAXDIS
                   --
                   and mi.workspace_id      = I_workspace_id
                   and mi.match_luw_id      = I_match_luw_id
                   and mi.match_key_id      = NVL(I_match_key_id, mi.match_key_id)
         ) use_this
         on (    target.workspace_id = use_this.workspace_id
             and target.match_key_id = use_this.match_key_id
             and target.doc_id       = use_this.doc_id)
         when matched then update
          set target.status = decode(target.status,
                                     REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, target.status,
                                     decode(target.header_only,
                                            'Y', REIM_CONSTANTS.DOC_STATUS_TAXDIS,
                                            REIM_CONSTANTS.DOC_STATUS_URMTCH));

         LOGGER.LOG_INFORMATION(L_program||' ALL 2 ALL Merge TAXDIS to im_match_invc_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      end if; --L_match_level

   end if; -- last strategy detail

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UPD_TAXDIS_SMRY;
------------------------------------------------------------------------------------------
/**
 * The private function that performs Detail Matching on the Line items of the invoices in the chunk. - Tran Level
 * Match_type: Regular would match each invoice line item with the sum of all available receipt line item(same item)
 *             If more than one Invoice item matches to the sum of receipt items (same item) all invoice items would be marked as discrepant.
 * Match_type: Best Match would match each invoice line item with the sum of all available receipt line item(same item)
 *             If more than one Invoice item matches to the sum of receipt items (same item), best match is decided based on criteria.
 *             If undecided even after applying criteria, all invoice items would be marked as discrepant.
 * Group header table holds combinations of invoice and receipt groups(for the same item).
 *
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_stgy_dtl_id (Match Strategy Detail ID)
 *              I_chunk_num (ID of the Chunk of Match Data)
 *              I_match_type (Match Type - Can be 'R'-Regular  or 'B'-Best Match)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_SKU_LVL_DETL_MATCH(O_error_message        OUT VARCHAR2,
                                    I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                    I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                    I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE, -- 'R'-Regular  or 'B'-Best Match
                                    I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                    I_chunk_num         IN     IM_AUTO_MATCH_TASK_STATUS.CHUNK_NUM%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERFORM_SKU_LVL_DETL_MATCH';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_delay_detl_match IM_SYSTEM_OPTIONS.DELAY_LN_MTCH_UNTL_ROUTE_DATE%TYPE;

   L_match_count           NUMBER(10) := 0;
   L_disc_count            NUMBER(10) := 0;
   L_rslv_count            NUMBER(10) := 0;

BEGIN

   LOGGER.LOG_INFORMATION('Start '||L_program
                          || ' I_workspace_id: '      || I_workspace_id
                          || ' I_match_stgy_dtl_id: ' || I_match_stgy_dtl_id
                          || ' I_match_type: '        || I_match_type
                          || ' I_match_luw_id: '      || I_match_luw_id
                          || ' I_chunk_num: '         || I_chunk_num);

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              I_match_stgy_dtl_id, I_chunk_num, I_match_luw_id,
                              TASK_LVL_CHUNK,
                              TASK_TYPE_MATCH,
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_STARTED);

   select delay_ln_mtch_untl_route_date
     into L_delay_detl_match
     from im_system_options;

   delete from im_match_detail_rcpt_gtt gtt;
   insert into im_match_detail_rcpt_gtt(match_key_id,
                                        rcpt_group_id,
                                        item,
                                        unit_cost_nc)
   select mr2.match_key_id,
          im_match_rcpt_group_seq.nextval rcpt_group_id,
          mr2.item,
          mr2.unit_cost_nc
     from (select distinct mr.match_key_id,
                           mr.item,
                           mr.unit_cost_nc
             from im_match_rcpt_detl_ws mr
            where mr.workspace_id      = I_workspace_id
              and mr.match_luw_id      = I_match_luw_id
              and mr.chunk_num         = I_chunk_num
              and mr.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH) mr2;

   delete from im_match_detail_ids_gtt;
   insert into im_match_detail_ids_gtt (match_key_id,
                                        doc_id,
                                        item,
                                        item_ctr,
                                        match_group_id,
                                        invc_group_id,
                                        rcpt_group_id,
                                        unit_cost_nc)
   select inner.match_key_id,
          inner.doc_id,
          inner.item,
          inner.item_ctr,
          im_match_group_seq.nextval match_group_id,
          im_match_invc_group_seq.nextval invc_group_id,
          inner.rcpt_group_id,
          inner.unit_cost_nc
     from (select i.match_key_id,
                  i.doc_id,
                  i.item,
                  i.item_ctr,
                  gtt.rcpt_group_id,
                  gtt.unit_cost_nc
             from (select /*+ FULL(IW) INDEX(MIW IM_MATCH_INVC_WS_I22 */
                          iw.match_key_id,
                          iw.doc_id,
                          iw.item,
                          miw.due_date,
                          iw.unit_cost,
                          iw.ordloc_unit_cost,
                          case when count(distinct iw.unit_cost) over (partition by iw.match_key_id,
                                                                                    iw.item) > REIM_CONSTANTS.ONE then
                                  REIM_CONSTANTS.ONE
                          else
                             count(iw.doc_id) over (partition by iw.match_key_id,
                                                                 iw.item,
                                                                 iw.unit_cost)
                          end item_ctr
                     from im_match_invc_detl_ws iw,
                          im_match_invc_ws miw
                    where iw.workspace_id      = I_workspace_id
                      and iw.match_luw_id      = I_match_luw_id
                      and iw.chunk_num         = I_chunk_num
                      and iw.status            = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                      and iw.workspace_id      = miw.workspace_id
                      and iw.doc_id            = miw.doc_id
                      and (   miw.detail_mtch_elig = 'Y'
                           or L_delay_detl_match   = 'N')) i,
                  im_match_detail_rcpt_gtt gtt
            where i.match_key_id     = gtt.match_key_id
              and i.item             = gtt.item
              and i.ordloc_unit_cost = gtt.unit_cost_nc
            order by i.item, i.due_date, gtt.unit_cost_nc
          ) inner;

   LOGGER.LOG_INFORMATION(L_program||' Insert im_match_detail_ids_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date(number_1,    --invc_group_id
                                             number_2,    --match_group_id
                                             varchar2_1,  --item
                                             varchar2_2)  --match_key_id
      select im_match_invc_group_seq.nextval invc_group_id,
             im_match_group_seq.nextval match_group_id,
             inner.item,
             inner.match_key_id
        from (select distinct match_key_id,
                     item
                from im_match_detail_ids_gtt
               where I_match_type = REIM_CONSTANTS.MATCH_TYPE_BEST
                 and item_ctr     > REIM_CONSTANTS.ONE) inner;

   LOGGER.LOG_INFORMATION(L_program||' Identify multi document item from im_match_detail_ids_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_detail_ids_gtt (match_key_id,
                                        doc_id,
                                        item,
                                        item_ctr,
                                        match_group_id,
                                        invc_group_id,
                                        rcpt_group_id,
                                        unit_cost_nc)
   select multi_doc.varchar2_2 match_key_id,
          gtt.doc_id,
          multi_doc.varchar2_1 item,
          gtt.item_ctr,
          multi_doc.number_2 match_group_id,
          multi_doc.number_1 invc_group_id,
          gtt.rcpt_group_id,
          gtt.unit_cost_nc
     from gtt_num_num_str_str_date_date multi_doc,
          im_match_detail_ids_gtt gtt
    where gtt.match_key_id = multi_doc.varchar2_2
      and gtt.item         = multi_doc.varchar2_1;

   LOGGER.LOG_INFORMATION(L_program||' Insert im_match_detail_ids_gtt for multi document items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_group_invc_ws(invc_group_id,
                                      doc_id,
                                      workspace_id,
                                      match_stgy_id,
                                      match_stgy_dtl_id,
                                      chunk_num,
                                      match_luw_id,
                                      match_key_id,
                                      due_date)
   select /*+ INDEX(MI IM_MATCH_INVC_WS_I22) */
          gtt.invc_group_id,
          gtt.doc_id,
          I_workspace_id,
          mi.match_stgy_id,
          I_match_stgy_dtl_id,
          I_chunk_num,
          I_match_luw_id,
          gtt.match_key_id,
          mi.due_date
     from im_match_detail_ids_gtt gtt,
          im_match_invc_ws mi
    where mi.workspace_id = I_workspace_id
      and mi.doc_id       = gtt.doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert im_match_group_invc_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_group_rcpt_ws(rcpt_group_id,
                                      shipment,
                                      workspace_id,
                                      match_stgy_id,
                                      match_stgy_dtl_id,
                                      chunk_num,
                                      match_luw_id,
                                      match_key_id)
   select /*+ INDEX(MR IM_MATCH_RCPT_DETL_WS_I3) */ distinct gtt.rcpt_group_id,
                   mr.shipment,
                   I_workspace_id,
                   -1 match_stgy_id,
                   I_match_stgy_dtl_id,
                   I_chunk_num,
                   I_match_luw_id,
                   gtt.match_key_id
     from (select distinct match_key_id,
                           rcpt_group_id,
                           item
             from im_match_detail_ids_gtt) gtt,
          im_match_rcpt_detl_ws mr
    where mr.workspace_id      = I_workspace_id
      and mr.match_luw_id      = I_match_luw_id
      and mr.chunk_num         = I_chunk_num
      and mr.match_key_id      = gtt.match_key_id
      and mr.item              = gtt.item
      and mr.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH;

   LOGGER.LOG_INFORMATION(L_program||' Insert im_match_group_rcpt_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --create im_match_group_head_ws rows for unmatched data
   insert into im_match_group_head_ws (workspace_id,
                                       match_group_id,
                                       match_stgy_id,
                                       match_stgy_dtl_id,
                                       chunk_num,
                                       match_luw_id,
                                       invc_group_id,
                                       rcpt_group_id,
                                       match_type,
                                       match_key_id,
                                       item,
                                       cost_tolerance_dtl_id,
                                       qty_tolerance_dtl_id,
                                       tolerance_currency_code,
                                       tolerance_exchange_rate,
                                       invc_group_cost,
                                       invc_group_qty,
                                       rcpt_group_cost,
                                       rcpt_group_qty,
                                       cost_variance,
                                       qty_variance,
                                       cost_discrepant,
                                       qty_discrepant,
                                       cost_disc_resolved,
                                       qty_disc_resolved,
                                       cost_adjust_rc,
                                       qty_adjust_rc,
                                       sku_compliant,
                                       tax_compliant,
                                       match_available,
                                       match_status,
                                       persist_status,
                                       error_code,
                                       error_context)
     select iw.workspace_id,
            invc.match_group_id,
            iw.match_stgy_id,
            I_match_stgy_dtl_id match_stgy_dtl_id,
            I_chunk_num,
            I_match_luw_id,
            invc.invc_group_id,
            rcpt.rcpt_group_id,
            REIM_CONSTANTS.MATCH_LEVEL_DETAIL match_type,
            invc.match_key_id,
            invc.item,
            nvl(tdc.tolerance_detail_id,-1) cost_tolerance_dtl_id,
            nvl(tdq.tolerance_detail_id,-1) qty_tolerance_dtl_id,
            iw.tolerance_currency_code,
            iw.tolerance_exchange_rate,
            invc.invc_group_cost,
            invc.invc_group_qty,
            rcpt.rcpt_group_cost,
            rcpt.rcpt_group_qty,
            (rcpt.rcpt_group_cost - invc.invc_group_cost) cost_variance,
            (rcpt.rcpt_group_qty - invc.invc_group_qty) qty_variance,
            --
            case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when tdc.tolerance_value < ABS(100 * (invc.invc_group_cost - rcpt.rcpt_group_cost) / rcpt.rcpt_group_cost) then
                            'Y'
                         else
                            'N'
                    end
                 when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when tdc.tolerance_value < ABS((invc.invc_group_cost - rcpt.rcpt_group_cost) * iw.tolerance_exchange_rate) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when invc.invc_group_cost = rcpt.rcpt_group_cost then
                            'N'
                         else
                            'Y'
                    end
            end cost_discrepant, --target (y/n)
            --
            case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when tdq.tolerance_value < ABS(100 * (invc.invc_group_qty - rcpt.rcpt_group_qty) / rcpt.rcpt_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when tdq.tolerance_value < ABS(invc.invc_group_qty - rcpt.rcpt_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when invc.invc_group_qty = rcpt.rcpt_group_qty then
                            'N'
                         else
                            'Y'
                    end
            end qty_discrepant, --target (y/n)
            --
            null cost_disc_resolved,
            null qty_disc_resolved,
            null cost_adjust_rc,
            null qty_adjust_rc,
            'N' sku_compliant,
            'Y' tax_compliant,
            'Y' match_available,
            --
            case when
               case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                       case when tdc.tolerance_value < ABS(100 * (invc.invc_group_cost - rcpt.rcpt_group_cost) / rcpt.rcpt_group_cost) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                       case when tdc.tolerance_value < ABS((invc.invc_group_cost - rcpt.rcpt_group_cost) * iw.tolerance_exchange_rate) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    else  --no tolerance found, exact match
                       case when invc.invc_group_cost = rcpt.rcpt_group_cost then
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                       end
               end = REIM_CONSTANTS.DOC_STATUS_MTCH
               --
               and
               --
               case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                       case when tdq.tolerance_value < ABS(100 * (invc.invc_group_qty - rcpt.rcpt_group_qty) / rcpt.rcpt_group_qty) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                       case when tdq.tolerance_value < ABS(invc.invc_group_qty - rcpt.rcpt_group_qty) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    else  --no tolerance found, exact match
                       case when invc.invc_group_qty = rcpt.rcpt_group_qty then
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                       end
               end = REIM_CONSTANTS.DOC_STATUS_MTCH then
                  REIM_CONSTANTS.DOC_STATUS_MTCH
               else
                  REIM_CONSTANTS.DOC_STATUS_URMTCH
            end match_status,
            --
            'N' persist_status,
            null error_code,
            null error_context
       from (select /*+ ORDERED INDEX(ID PK_IM_MATCH_INVC_DETL_WS) INDEX(MIW PK_IM_MATCH_INVC_WS) */
                    gtt.match_group_id,
                    gtt.invc_group_id,
                    gtt.match_key_id,
                    id.item,
                    min(id.unit_cost) invc_group_cost,
                    sum(id.invoice_qty) invc_group_qty
              from im_match_detail_ids_gtt gtt,
                   im_match_invc_ws miw,
                   im_match_invc_detl_ws id
             where miw.match_key_id = gtt.match_key_id
               and miw.doc_id       = gtt.doc_id
               and miw.workspace_id = I_workspace_id
               and miw.match_luw_id = I_match_luw_id
               and id.workspace_id  = miw.workspace_id
               and id.match_luw_id  = miw.match_luw_id
               and id.match_key_id  = miw.match_key_id
               and id.chunk_num     = I_chunk_num
               and id.doc_id        = miw.doc_id
               and id.status        = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
               and id.item          = gtt.item
               and rownum           > 0
             group by gtt.match_group_id,
                      gtt.invc_group_id,
                      gtt.match_key_id,
                      id.item) invc,
            --
            (select /*+ ORDERED INDEX(IMGRW IM_MATCH_GROUP_RCPT_WS_I6) INDEX(RD IM_MATCH_RCPT_DETL_WS_I3) */
                    imgrw.rcpt_group_id,
                    rd.match_key_id,
                    rd.item,
                    min(rd.unit_cost) rcpt_group_cost,
                    sum(rd.qty_available) rcpt_group_qty
              from (select distinct match_key_id, rcpt_group_id, item from im_match_detail_ids_gtt) gtt,
                   im_match_group_rcpt_ws imgrw,
                   im_match_rcpt_detl_ws rd
             where imgrw.match_key_id      = gtt.match_key_id
               and imgrw.rcpt_group_id     = gtt.rcpt_group_id
               and imgrw.workspace_id      = I_workspace_id
               and imgrw.match_luw_id      = I_match_luw_id
               and imgrw.chunk_num         = I_chunk_num
               and imgrw.match_stgy_dtl_id = I_match_stgy_dtl_id
               and rd.workspace_id         = imgrw.workspace_id
               and rd.match_luw_id         = imgrw.match_luw_id
               and rd.chunk_num            = imgrw.chunk_num
               and rd.match_key_id         = imgrw.match_key_id
               and rd.shipment             = imgrw.shipment
               and rd.invc_match_status    = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
               and rd.item                 = gtt.item
             group by imgrw.rcpt_group_id,
                      rd.match_key_id,
                      rd.item) rcpt,
            --
            (select /*+ FULL(IMIW) */ imiw.workspace_id,
                    imiw.match_luw_id,
                    imiw.match_key_id,
                    max(imiw.match_stgy_id) match_stgy_id,
                    max(imiw.tolerance_id) tolerance_id,
                    max(imiw.tolerance_currency_code) tolerance_currency_code,
                    max(imiw.tolerance_exchange_rate) tolerance_exchange_rate
               from im_match_invc_ws imiw
              where imiw.workspace_id = I_workspace_id
                and imiw.match_luw_id = I_match_luw_id
              group by imiw.workspace_id,
                       imiw.match_luw_id,
                       imiw.match_key_id) iw,
            --
            im_tolerance_detail tdc,
            im_tolerance_detail tdq
        --
      where invc.match_key_id     = rcpt.match_key_id
        and invc.item             = rcpt.item
        --
        and iw.workspace_id       = I_workspace_id
        and iw.match_luw_id       = I_match_luw_id
        and iw.match_key_id       = invc.match_key_id
        --
        and rcpt.rcpt_group_qty   <> REIM_CONSTANTS.ZERO
        and rcpt.rcpt_group_cost  <> REIM_CONSTANTS.ZERO
        --
        and iw.tolerance_id       = tdc.tolerance_id(+)
        and tdc.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
        and tdc.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
        and tdc.lower_limit(+)   <= (invc.invc_group_cost * iw.tolerance_exchange_rate)
        and tdc.upper_limit(+)    > (invc.invc_group_cost * iw.tolerance_exchange_rate)
        and tdc.favor_of(+)       = case when invc.invc_group_cost > rcpt.rcpt_group_cost
                                         then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                         else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                    end
        --
        and iw.tolerance_id       = tdq.tolerance_id(+)
        and tdq.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
        and tdq.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
        and tdq.lower_limit(+)   <= invc.invc_group_qty
        and tdq.upper_limit(+)    > invc.invc_group_qty
        and tdq.favor_of(+)       = case when invc.invc_group_qty > rcpt.rcpt_group_qty
                                         then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                         else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                    end;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_MATCH_GROUP_HEAD_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_match_type = REIM_CONSTANTS.MATCH_TYPE_REGULAR then

      --set multi unresolved.
      merge into im_match_group_head_ws target
      using (select i.workspace_id,
                    i.match_luw_id,
                    i.chunk_num,
                    i.match_key_id,
                    i.item
               from (select gh.workspace_id,
                            gh.match_luw_id,
                            gh.chunk_num,
                            gh.match_key_id,
                            gh.item,
                            count(*) match_cnt
                       from im_match_group_head_ws gh
                      where gh.workspace_id      = I_workspace_id
                        and gh.match_luw_id      = I_match_luw_id
                        and gh.chunk_num         = I_chunk_num
                        and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
                      group by gh.workspace_id,
                             gh.match_luw_id,
                             gh.chunk_num,
                             gh.match_key_id,
                             gh.item) i
              where i.match_cnt > 1
      ) use_this
      on (    target.workspace_id   = use_this.workspace_id
          and target.match_luw_id   = use_this.match_luw_id
          and target.chunk_num      = use_this.chunk_num
          and target.match_key_id   = use_this.match_key_id
          and target.item           = use_this.item)
      when matched then update
       set target.match_status = REIM_CONSTANTS.DOC_ITEM_STATUS_MURMTH;

      LOGGER.LOG_INFORMATION(L_program||' Merge IM_MATCH_GROUP_HEAD_WS Regular Match MURMTH - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --auto resolution
      insert into im_resolution_action_ws (workspace_id,
                                           match_luw_id,
                                           match_key_id,
                                           chunk_num,
                                           match_group_id,
                                           doc_id,
                                           item,
                                           reason_code_id,
                                           action,
                                           qty,
                                           unit_cost,
                                           extended_cost,
                                           status,
                                           shipment)
      ------
      with autores as (
         select /*+ ORDERED INDEX(G IM_MATCH_GROUP_HEAD_WS_I4) INDEX(I IM_MATCH_GROUP_INVC_WS_I21) INDEX(IW IM_MATCH_INVC_WS_I22) */
                g.workspace_id,
                g.match_luw_id,
                g.match_key_id,
                g.chunk_num,
                --
                i.doc_id,
                g.item,
                g.match_group_id,
                g.invc_group_id,
                g.rcpt_group_id,
                g.invc_group_cost,
                g.rcpt_group_cost,
                g.invc_group_qty,
                g.rcpt_group_qty,
                g.cost_discrepant,
                g.qty_discrepant,
                tdc.reason_code_id cost_reason_code_id,
                tdq.reason_code_id qty_reason_code_id,
                --
                case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                        case when tdc.auto_res_value < ABS(100 * (g.invc_group_cost - g.rcpt_group_cost) / g.rcpt_group_cost) then
                                'N'
                             else
                                'Y'
                        end
                     when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                        case when tdc.auto_res_value < ABS((g.invc_group_cost - g.rcpt_group_cost) * g.tolerance_exchange_rate) then
                                'N'
                             else
                                'Y'
                        end
                     else  --no tolerance found, exact match
                        case when g.invc_group_cost = g.rcpt_group_cost then
                                'Y'
                             else
                                'N'
                        end
                end cost_resolution_ind, --target (y/n)
                --
                case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                        case when tdq.auto_res_value < ABS(100 * (g.invc_group_qty - g.rcpt_group_qty) / g.rcpt_group_qty) then
                                'N'
                             else
                                'Y'
                        end
                     when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                        case when tdq.auto_res_value < ABS(g.invc_group_qty - g.rcpt_group_qty) then
                                'N'
                             else
                                'Y'
                        end
                     else  --no tolerance found, exact match
                        case when g.invc_group_qty = g.rcpt_group_qty then
                                'Y'
                             else
                                'N'
                        end
                end qty_resolution_ind --target (y/n)
                --
           from im_match_group_head_ws g,
                im_match_group_invc_ws i,
                im_match_invc_ws iw,
                im_tolerance_detail tdc,
                im_tolerance_detail tdq
          where g.workspace_id             = I_workspace_id
            and g.match_luw_id             = I_match_luw_id
            and g.chunk_num                = I_chunk_num
            and g.match_stgy_dtl_id        = I_match_stgy_dtl_id
            and g.invc_group_id            = i.invc_group_id
            and iw.doc_id                  = i.doc_id
            and iw.workspace_id            = i.workspace_id
            and iw.detail_mtch_elig        = 'Y'
            and tdc.tolerance_detail_id(+) = g.cost_tolerance_dtl_id
            and tdq.tolerance_detail_id(+) = g.qty_tolerance_dtl_id
            and g.match_status             NOT IN (REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
                                                   REIM_CONSTANTS.DOC_ITEM_STATUS_MURMTH)
            and g.rcpt_group_qty           <> REIM_CONSTANTS.ZERO
            and g.rcpt_group_cost          <> REIM_CONSTANTS.ZERO)
      ------
      select a.workspace_id,
             a.match_luw_id,
             a.match_key_id,
             a.chunk_num,
             a.match_group_id,
             --
             a.doc_id,
             a.item,
             a.cost_reason_code_id,
             rc.action,
             null qty,
             (a.rcpt_group_cost - a.invc_group_cost) unit_cost,
             (a.rcpt_group_cost - a.invc_group_cost) * a.invc_group_qty extended_unit_cost,
             'U' status, --unrolled
             null shipment
        from autores a,
             im_reason_codes rc
       where a.cost_discrepant     = 'Y'
         and a.cost_resolution_ind = 'Y'
         and a.cost_reason_code_id = rc.reason_code_id
         and rc.reason_code_type   = 'C'
         --
         and ((a.qty_discrepant = 'Y' and a.qty_resolution_ind = 'Y')
              or
              (a.qty_discrepant = 'N'))
      union all
      select a.workspace_id,
             a.match_luw_id,
             a.match_key_id,
             a.chunk_num,
             a.match_group_id,
             --
             a.doc_id,
             a.item,
             a.qty_reason_code_id,
             rc.action,
             (a.rcpt_group_qty - a.invc_group_qty),
             null unit_cost,
             null extended_unit_cost,
             'U' status, --unrolled
             min(r.shipment)
        from autores a,
             im_match_group_rcpt_ws r,
             im_reason_codes rc
       where a.qty_discrepant     = 'Y'
         and a.qty_resolution_ind = 'Y'
         and a.rcpt_group_id      = r.rcpt_group_id
         and a.qty_reason_code_id = rc.reason_code_id
         and rc.reason_code_type  = 'Q'
         and ((a.cost_discrepant = 'Y' and a.cost_resolution_ind = 'Y')
              or
              (a.cost_discrepant = 'N'))
       group by a.workspace_id,
                a.match_luw_id,
                a.match_key_id,
                a.chunk_num,
                a.match_group_id,
                --
                a.doc_id,
                a.item,
                a.qty_reason_code_id,
                rc.action,
                a.invc_group_qty,
                a.rcpt_group_qty;

      LOGGER.LOG_INFORMATION(L_program||' Insert im_resolution_action_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into im_match_group_head_ws target
      using (select a.workspace_id,
                    a.match_group_id,
                    max(a.unit_cost) unit_cost,
                    max(a.qty) qty
               from im_resolution_action_ws a
              where a.workspace_id = I_workspace_id
                and a.match_luw_id = I_match_luw_id
                and a.chunk_num    = I_chunk_num
              group by a.workspace_id,
                       a.match_group_id
      ) use_this
      on (    target.workspace_id   = use_this.workspace_id
          and target.match_group_id = use_this.match_group_id)
      when matched then update
       set target.match_status  = REIM_CONSTANTS.DOC_STATUS_MTCH,
           target.cost_variance = decode(use_this.unit_cost, null, target.cost_variance, 0),
           target.qty_variance  = decode(use_this.qty,       null, target.qty_variance,  0);

      LOGGER.LOG_INFORMATION(L_program||' Merge im_match_group_head_ws auto resolved - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   elsif I_match_type = REIM_CONSTANTS.MATCH_TYPE_BEST then

      merge into im_match_group_head_ws target
      using (select i2.match_key_id,
                    i2.match_group_id,
                    i2.invc_group_id,
                    i2.item,
                    i2.workspace_id,
                    i2.match_luw_id,
                    i2.chunk_num,
                    i2.match_status,
                    i2.cost_variance,
                    i2.acv,
                    i2.scv,
                    i2.match_rank,
                    i2.match_cnt
               from (select i.match_key_id,
                            i.match_group_id,
                            i.invc_group_id,
                            i.item,
                            i.workspace_id,
                            i.match_luw_id,
                            i.chunk_num,
                            i.match_status,
                            i.cost_variance,
                            i.acv,
                            i.scv,
                            i.match_rank,
                            sum(i.match_cnt) over (partition by i.match_key_id,
                                                                i.item,
                                                                i.workspace_id,
                                                                i.match_luw_id,
                                                                i.chunk_num) match_cnt
                       from (select gh.match_key_id,
                                    gh.match_group_id,
                                    gh.invc_group_id,
                                    gh.item,
                                    gh.workspace_id,
                                    gh.match_luw_id,
                                    gh.chunk_num,
                                    gh.match_status,
                                    gh.cost_variance,
                                    abs(gh.cost_variance) acv,
                                    sign(gh.cost_variance) scv,
                                    rank() over (partition by gh.match_key_id,
                                                              gh.item,
                                                              gh.workspace_id,
                                                              gh.match_luw_id,
                                                              gh.chunk_num
                                                     order by abs(gh.cost_variance),
                                                              sign(gh.cost_variance),
                                                              abs(gh.qty_variance),
                                                              sign(gh.qty_variance)) match_rank,
                                    decode(rank() over (partition by gh.match_key_id,
                                                                     gh.item,
                                                                     gh.workspace_id,
                                                                     gh.match_luw_id,
                                                                     gh.chunk_num
                                                            order by abs(gh.cost_variance),
                                                                     sign(gh.cost_variance),
                                                                     abs(gh.qty_variance),
                                                                     sign(gh.qty_variance)),
                                           1, 1, 0) match_cnt
                               from im_match_group_head_ws gh
                              where gh.workspace_id      = I_workspace_id
                                and gh.match_luw_id      = I_match_luw_id
                                and gh.chunk_num         = I_chunk_num
                                and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
                                and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
                            ) i
                    ) i2
              where (i2.match_cnt > 1) or (i2.match_rank != 1 and i2.match_cnt = 1)
      ) use_this
      on (    target.workspace_id   = use_this.workspace_id
          and target.match_group_id = use_this.match_group_id)
      when matched then update
       set target.match_status = REIM_CONSTANTS.DOC_STATUS_URMTCH;

      LOGGER.LOG_INFORMATION(L_program||' Merge IM_MATCH_GROUP_HEAD_WS Best Match - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   --push matched info to workspace.

   merge /* INDEX(TARGET IM_MATCH_INVC_DETL_WS_I22) */
   into im_match_invc_detl_ws target
   using (select /*+ FULL(GH) INDEX(GI IM_MATCH_GROUP_INVC_WS_I21) */ gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 gi.doc_id,
                 gh.item,
                 gh.match_status
            from im_match_group_head_ws gh,
                 im_match_group_invc_ws gi
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.chunk_num         = I_chunk_num
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.invc_group_id     = gi.invc_group_id
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id
       and target.doc_id       = use_this.doc_id
       and target.item         = use_this.item)
   when matched then update
    set target.status         = use_this.match_status,
        target.match_group_id = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_invc_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ INDEX(TARGET IM_MATCH_RCPT_DETL_WS_I3) */
   into im_match_rcpt_detl_ws target
   using (select /*+ ORDERED */ gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 gr.shipment,
                 gh.item,
                 gh.match_status
            from im_match_group_head_ws gh,
                 im_match_group_rcpt_ws gr
           where gh.workspace_id      = I_workspace_id
             and gh.match_luw_id      = I_match_luw_id
             and gh.match_stgy_dtl_id = I_match_stgy_dtl_id
             and gh.chunk_num         = I_chunk_num
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.rcpt_group_id     = gr.rcpt_group_id
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id
       and target.shipment     = use_this.shipment
       and target.item         = use_this.item)
   when matched then update
    set target.invc_match_status = 'M', --constant
        target.match_group_id    = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_rcpt_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select sum(decode(status,'MTCH',1,0)) match_count,
          sum(decode(cost_matched,'N',1,
                     decode(qty_matched,'N',1,0),
                     0)) disc_count
     into L_match_count, L_disc_count
     from im_match_invc_detl_ws
    where workspace_id = I_workspace_id
      and match_luw_id = I_match_luw_id
      and chunk_num    = I_chunk_num;

   select count(distinct doc_id||'_=+'||item)
    into L_rslv_count
    from im_match_invc_detl_ws
    where workspace_id = I_workspace_id
      and match_luw_id = I_match_luw_id
      and chunk_num    = I_chunk_num;

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              I_match_stgy_dtl_id, I_chunk_num, I_match_luw_id,
                              TASK_LVL_CHUNK,
                              TASK_TYPE_MATCH,
                              NULL, NULL, L_match_count, L_disc_count, L_rslv_count,
                              TASK_STATUS_SUCCESS);

   LOGGER.LOG_INFORMATION('End ' || L_program );
   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);


   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 I_match_stgy_dtl_id, I_chunk_num, I_match_luw_id,
                                 TASK_LVL_CHUNK,
                                 TASK_TYPE_MATCH,
                                 NULL, NULL, NULL, NULL, NULL,
                                 TASK_STATUS_FAILED);

      return REIM_CONSTANTS.FAIL;

END PERFORM_SKU_LVL_DETL_MATCH;
------------------------------------------------------------------------------------------
FUNCTION PERFORM_STYLE_LVL_DETL_MATCH(O_error_message        OUT VARCHAR2,
                                      I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                      I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                      I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE, -- 'P'-Parent
                                      I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                      I_chunk_num         IN     IM_AUTO_MATCH_TASK_STATUS.CHUNK_NUM%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERFORM_STYLE_LVL_DETL_MATCH';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_match_count NUMBER(10) := 0;
   L_disc_count  NUMBER(10) := 0;

BEGIN

   LOGGER.LOG_INFORMATION('Start '||L_program
                          || ' I_workspace_id: '      || I_workspace_id
                          || ' I_match_stgy_dtl_id: ' || I_match_stgy_dtl_id
                          || ' I_match_type: '        || I_match_type
                          || ' I_match_luw_id: '      || I_match_luw_id
                          || ' I_chunk_num: '         || I_chunk_num);

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              I_match_stgy_dtl_id, I_chunk_num, I_match_luw_id,
                              TASK_LVL_CHUNK,
                              TASK_TYPE_MATCH,
                              NULL, NULL, NULL, NULL, NULL,
                              TASK_STATUS_STARTED);

   merge into im_match_invc_detl_ws tgt
   using (select imidw.workspace_id,
                 imidw.match_luw_id,
                 imidw.chunk_num,
                 imidw.match_key_id,
                 imidw.doc_id,
                 im.item,
                 im.item_parent,
                 COUNT(distinct imidw.unit_cost)
                    OVER (PARTITION BY imidw.workspace_id, imidw.match_luw_id, imidw.chunk_num, imidw.match_key_id, im.item_parent) cost_count
            from im_match_invc_detl_ws imidw,
                 item_master im,
                 im_match_invc_ws imiw
           where imidw.workspace_id = I_workspace_id
             and imidw.match_luw_id = I_match_luw_id
             and imidw.chunk_num    = I_chunk_num
             and imidw.item         = im.item
             and im.item_parent     is NOT NULL
             and imidw.workspace_id = imiw.workspace_id
             and imidw.doc_id       = imiw.doc_id) src
   on (    tgt.workspace_id   = src.workspace_id
       and tgt.match_luw_id   = src.match_luw_id
       and tgt.chunk_num      = src.chunk_num
       and tgt.match_key_id   = src.match_key_id
       and tgt.item           = src.item
       and tgt.doc_id         = src.doc_id
       and src.cost_count     = 1)
   when matched then update
    set tgt.item_parent  = src.item_parent;

   LOGGER.LOG_INFORMATION(L_program||' Merge im_match_invc_detl_ws item_parent - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_detl_ws tgt
   using (select imrdw.workspace_id,
                 imrdw.match_luw_id,
                 imrdw.chunk_num,
                 imrdw.match_key_id,
                 imrdw.shipment,
                 im.item,
                 im.item_parent,
                 COUNT(distinct imrdw.unit_cost_nc)
                    OVER (PARTITION BY imrdw.workspace_id, imrdw.match_luw_id, imrdw.chunk_num, imrdw.match_key_id, im.item_parent) cost_count
            from im_match_rcpt_detl_ws imrdw,
                 item_master im
           where imrdw.workspace_id = I_workspace_id
             and imrdw.match_luw_id = I_match_luw_id
             and imrdw.chunk_num    = I_chunk_num
             and imrdw.item         = im.item
             and im.item_parent     is NOT NULL) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.match_luw_id = src.match_luw_id
       and tgt.chunk_num    = src.chunk_num
       and tgt.match_key_id = src.match_key_id
       and tgt.item         = src.item
       and tgt.shipment     = src.shipment
       and src.cost_count   = 1)
   when matched then update
    set tgt.item_parent  = src.item_parent;

   LOGGER.LOG_INFORMATION(L_program||' Merge im_match_rcpt_detl_ws item_parent - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --assign a match_group_id to each match_key_id in the workspace
   --number_1   = match_key_id
   --number_2   = match_group_id
   --varchar2_1 = item_parent
   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2,
                                              varchar2_1)
   select inner.match_key_id,
          im_match_group_seq.nextval match_group_id,
          inner.item_parent
     from (select imidw.match_key_id,
                  imidw.item_parent
             from im_match_invc_detl_ws imidw
            where imidw.workspace_id = I_workspace_id
              and imidw.match_luw_id = I_match_luw_id
              and imidw.chunk_num    = I_chunk_num
              and imidw.item_parent  is NOT NULL
            GROUP BY imidw.match_key_id,
                     imidw.item_parent) inner;

   LOGGER.LOG_INFORMATION(L_program||' Insert match_group_id to gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --create im_match_group_head_ws rows for unmatched data
   insert into im_match_group_head_ws (workspace_id,
                                       match_group_id,
                                       match_stgy_id,
                                       match_stgy_dtl_id,
                                       chunk_num,
                                       match_luw_id,
                                       invc_group_id,
                                       rcpt_group_id,
                                       match_type,
                                       match_key_id,
                                       item_parent,
                                       cost_tolerance_dtl_id,
                                       qty_tolerance_dtl_id,
                                       invc_group_cost,
                                       invc_group_qty,
                                       rcpt_group_cost,
                                       rcpt_group_qty,
                                       cost_variance,
                                       qty_variance,
                                       cost_discrepant,
                                       qty_discrepant,
                                       cost_disc_resolved,
                                       qty_disc_resolved,
                                       cost_adjust_rc,
                                       qty_adjust_rc,
                                       sku_compliant,
                                       tax_compliant,
                                       match_available,
                                       match_status,
                                       persist_status,
                                       error_code,
                                       error_context)
     select /*+ INDEX(IW IM_MATCH_INVC_WS_I4) */
            distinct imiw.workspace_id,
            gtt.number_2 match_group_id,
            imiw.match_stgy_id,
            I_match_stgy_dtl_id match_stgy_dtl_id,
            I_chunk_num,
            imiw.match_luw_id,
            null invc_group_id,
            null rcpt_group_id,
            REIM_CONSTANTS.MATCH_LEVEL_DETAIL match_type,
            imiw.match_key_id,
            invc.item_parent,
            nvl(tdc.tolerance_detail_id,-1) cost_tolerance_dtl_id,
            nvl(tdq.tolerance_detail_id,-1) qty_tolerance_dtl_id,
            invc.invc_unit_cost,
            invc.invc_group_qty,
            rcpt.rcpt_unit_cost,
            rcpt.rcpt_group_qty,
            (rcpt.rcpt_unit_cost - invc.invc_unit_cost) cost_variance,
            (rcpt.rcpt_group_qty - invc.invc_group_qty) qty_variance,
            --
            case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when tdc.tolerance_value < ABS(100 * (invc.invc_unit_cost - rcpt.rcpt_unit_cost) / rcpt.rcpt_unit_cost) then
                            'Y'
                         else
                            'N'
                    end
                 when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when tdc.tolerance_value < ABS((invc.invc_unit_cost - rcpt.rcpt_unit_cost) * imiw.tolerance_exchange_rate) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when invc.invc_unit_cost = rcpt.rcpt_unit_cost then
                            'N'
                         else
                            'Y'
                    end
            end cost_discrepant, --target (y/n)
            --
            case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when tdq.tolerance_value < ABS(100 * (invc.invc_group_qty - rcpt.rcpt_group_qty) / rcpt.rcpt_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when tdq.tolerance_value < ABS(invc.invc_group_qty - rcpt.rcpt_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when invc.invc_group_qty = rcpt.rcpt_group_qty then
                            'N'
                         else
                            'Y'
                    end
            end qty_discrepant, --target (y/n)
            --
            null cost_disc_resolved,
            null qty_disc_resolved,
            null cost_adjust_rc,
            null qty_adjust_rc,
            'N' sku_compliant,
            'Y' tax_compliant,
            'Y' match_available,
            --
            case when
               case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                       case when tdc.tolerance_value < ABS(100 * (invc.invc_unit_cost - rcpt.rcpt_unit_cost) / rcpt.rcpt_unit_cost) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                       case when tdc.tolerance_value < ABS((invc.invc_unit_cost - rcpt.rcpt_unit_cost) * imiw.tolerance_exchange_rate) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    else  --no tolerance found, exact match
                       case when invc.invc_unit_cost = rcpt.rcpt_unit_cost then
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                       end
               end = REIM_CONSTANTS.DOC_STATUS_MTCH
               --
               and
               --
               case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                       case when tdq.tolerance_value < ABS(100 * (invc.invc_group_qty - rcpt.rcpt_group_qty) / rcpt.rcpt_group_qty) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                       end
                    when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                       case when tdq.tolerance_value < ABS(invc.invc_group_qty - rcpt.rcpt_group_qty) then
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_MTCH end
                    else  --no tolerance found, exact match
                       case when invc.invc_group_qty = rcpt.rcpt_group_qty then
                               REIM_CONSTANTS.DOC_STATUS_MTCH
                            else
                               REIM_CONSTANTS.DOC_STATUS_URMTCH
                       end
               end = REIM_CONSTANTS.DOC_STATUS_MTCH then
                  REIM_CONSTANTS.DOC_STATUS_MTCH
               else
                  REIM_CONSTANTS.DOC_STATUS_URMTCH
            end match_status,
            --
            'N' persist_status,
            null error_code,
            null error_context
       from gtt_num_num_str_str_date_date gtt,
            im_match_invc_ws imiw,
            --
            (select /*+ FULL(IMIDW) INDEX(IMIW PK_IM_MATCH_INVC_WS) */
                    imidw.match_key_id,
                    imidw.item_parent,
                    max(imidw.unit_cost) invc_unit_cost,
                    sum(imidw.invoice_qty) invc_group_qty
              from im_match_invc_ws imiw,
                   im_match_invc_detl_ws imidw
             where imiw.workspace_id    = I_workspace_id
               and imiw.match_luw_id    = I_match_luw_id
               and imiw.workspace_id    = imidw.workspace_id
               and imiw.match_luw_id    = imidw.match_luw_id
               and imiw.doc_id          = imidw.doc_id
               and imidw.status         = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
               and imidw.chunk_num      = I_chunk_num
               and imidw.item_parent    is NOT NULL
             GROUP BY imidw.match_key_id,
                      imidw.item_parent) invc,
            --
            (select /*+ FULL(RD) */
                    imrdw.match_key_id,
                    imrdw.item_parent,
                    max(imrdw.unit_cost) rcpt_unit_cost,
                    sum(imrdw.qty_available) rcpt_group_qty
              from im_match_rcpt_detl_ws imrdw
             where imrdw.workspace_id      = I_workspace_id
               and imrdw.match_luw_id      = I_match_luw_id
               and imrdw.chunk_num         = I_chunk_num
               and imrdw.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
               and imrdw.item_parent       is NOT NULL
             GROUP BY imrdw.match_key_id,
                      imrdw.item_parent) rcpt,
            --
            im_tolerance_detail tdc,
            im_tolerance_detail tdq
      where imiw.workspace_id     = I_workspace_id
        and imiw.match_luw_id     = I_match_luw_id
        --
        and imiw.match_key_id     = gtt.number_1
        and imiw.match_key_id     = invc.match_key_id
        and imiw.match_key_id     = rcpt.match_key_id
        and gtt.varchar2_1        = invc.item_parent
        and invc.item_parent      = rcpt.item_parent
        --
        and rcpt.rcpt_unit_cost   <> REIM_CONSTANTS.ZERO
        and rcpt.rcpt_group_qty   <> REIM_CONSTANTS.ZERO
        --
        and imiw.tolerance_id     = tdc.tolerance_id(+)
        and tdc.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_PARENT
        and tdc.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
        and tdc.lower_limit(+)   <= (invc.invc_unit_cost * imiw.tolerance_exchange_rate)
        and tdc.upper_limit(+)    > (invc.invc_unit_cost * imiw.tolerance_exchange_rate)
        and tdc.favor_of(+)       = case when invc.invc_unit_cost > rcpt.rcpt_unit_cost
                                         then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                         else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                    end
        --
        and imiw.tolerance_id     = tdq.tolerance_id(+)
        and tdq.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_PARENT
        and tdq.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
        and tdq.lower_limit(+)   <= invc.invc_group_qty
        and tdq.upper_limit(+)    > invc.invc_group_qty
        and tdq.favor_of(+)       = case when invc.invc_group_qty > rcpt.rcpt_group_qty
                                         then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                         else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                    end;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_MATCH_GROUP_HEAD_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --push matched info to workspace.
   merge into im_match_invc_detl_ws tgt
   using (select imghw.workspace_id,
                 imghw.match_key_id,
                 imghw.match_luw_id,
                 imghw.chunk_num,
                 imghw.match_group_id,
                 imghw.match_status,
                 imghw.item_parent
            from im_match_group_head_ws imghw
           where imghw.workspace_id      = I_workspace_id
             and imghw.match_luw_id      = I_match_luw_id
             and imghw.match_stgy_dtl_id = I_match_stgy_dtl_id
             and imghw.chunk_num         = I_chunk_num
             and imghw.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.match_key_id = src.match_key_id
       and tgt.match_luw_id = src.match_luw_id
       and tgt.chunk_num    = src.chunk_num
       and tgt.item_parent  = src.item_parent)
   when matched then
      update
         set tgt.status         = src.match_status,
             tgt.match_group_id = src.match_group_id
       where tgt.status = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_invc_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_detl_ws tgt
   using (select imghw.workspace_id,
                 imghw.match_key_id,
                 imghw.match_luw_id,
                 imghw.chunk_num,
                 imghw.match_group_id,
                 imghw.match_status,
                 imghw.item_parent
            from im_match_group_head_ws imghw
           where imghw.workspace_id      = I_workspace_id
             and imghw.match_luw_id      = I_match_luw_id
             and imghw.match_stgy_dtl_id = I_match_stgy_dtl_id
             and imghw.chunk_num         = I_chunk_num
             and imghw.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.match_key_id = src.match_key_id
       and tgt.match_luw_id = src.match_luw_id
       and tgt.chunk_num    = src.chunk_num
       and tgt.item_parent  = src.item_parent)
   when matched then
      update
         set tgt.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
             tgt.match_group_id    = src.match_group_id
       where tgt.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH;

   LOGGER.LOG_INFORMATION(L_program||' Merge Match to im_match_rcpt_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select sum(decode(status,'MTCH',1,0)) match_count,
          sum(decode(cost_matched,'N',1,
                     decode(qty_matched,'N',1,0),
                     0)) disc_count
     into L_match_count,
          L_disc_count
     from im_match_invc_detl_ws
    where workspace_id = I_workspace_id
      and match_luw_id = I_match_luw_id
      and chunk_num    = I_chunk_num;

   LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                              I_workspace_id,
                              I_match_stgy_dtl_id, I_chunk_num, I_match_luw_id,
                              TASK_LVL_CHUNK,
                              TASK_TYPE_MATCH,
                              NULL, NULL, L_match_count, L_disc_count, NULL,
                              TASK_STATUS_SUCCESS);

   LOGGER.LOG_INFORMATION('End ' || L_program );
   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      LOG_AUTO_MATCH_TASK_STATUS(O_error_message,
                                 I_workspace_id,
                                 I_match_stgy_dtl_id, I_chunk_num, I_match_luw_id,
                                 TASK_LVL_CHUNK,
                                 TASK_TYPE_MATCH,
                                 NULL, NULL, NULL, NULL, NULL,
                                 TASK_STATUS_FAILED);

      return REIM_CONSTANTS.FAIL;

END PERFORM_STYLE_LVL_DETL_MATCH;
------------------------------------------------------------------------------------------
/**
 * The public function that persists data at a chunk level which include,
 * Update Invoice-detail and Shipsku Data for successful matches at summary and detail level.
 * Perform Auto resolution and mark invoice_details to match.
 * Create Detail Match History and Receipt Item Posting data for detail matches.
 * Mark items in invoice_detail as discrepant for unsuccessful detail matches
 *
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_luw_id (ID of the Match LUW)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERSIST_SKU_LVL_DETL_MATCH(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                    I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERSIST_SKU_LVL_DETL_MATCH';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_user    IM_DOC_HEAD.MATCH_ID%TYPE := get_user;
   L_vdate   DATE                      := get_vdate;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: ' || I_workspace_id
                          || ' I_match_luw_id: ' || I_match_luw_id);

   LOGGER.LOG_INFORMATION('End ' || L_program );

   --------------------------------------
   --write match history
   --------------------------------------

   delete from gtt_6_num_6_str_6_date;

   --number_1   detail_match_history_seq
   --number_2   match_group_id
   --number_3   invc_group_id
   --number_4   rcpt_group_id
   --varchar2_1 item
   --varchar2_2 exact_match
   insert into gtt_6_num_6_str_6_date(number_1,
                                      number_2,
                                      number_3,
                                      number_4,
                                      varchar2_1,
                                      varchar2_2)
   select im_detail_match_history_seq.nextval,
          imghw.match_group_id,
          imghw.invc_group_id,
          imghw.rcpt_group_id,
          imghw.item,
          case when imghw.qty_variance = 0 and imghw.cost_variance = 0 then 'Y' else 'N' end
     from im_match_group_head_ws imghw,
          im_match_stgy_detail imsd
    where imghw.workspace_id      = I_workspace_id
      and imghw.match_luw_id      = I_match_luw_id
      and imghw.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
      and imghw.match_type        = REIM_CONSTANTS.MATCH_LEVEL_DETAIL
      and imghw.match_stgy_dtl_id = imsd.match_stgy_dtl_id
      and imsd.match_type         IN (REIM_CONSTANTS.MATCH_TYPE_REGULAR,
                                      REIM_CONSTANTS.MATCH_TYPE_BEST);

   insert into im_detail_match_history (match_id,
                                        auto_matched,
                                        exact_match,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             'Y',
             gtt.varchar2_2,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_6_num_6_str_6_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_invc_history (match_id,
                                             doc_id,
                                             item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             i.doc_id,
             gtt.varchar2_1,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_6_num_6_str_6_date gtt,
             im_match_group_invc_ws i
       where i.invc_group_id = gtt.number_3;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_rcpt_history (match_id,
                                             shipment,
                                             item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             r.shipment,
             gtt.varchar2_1,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_6_num_6_str_6_date gtt,
             im_match_group_rcpt_ws r
       where r.rcpt_group_id = gtt.number_4;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to posting
   --------------------------------------

   delete from gtt_num_num_str_str_date_date;

   --number_1   seq_no
   --number_2   receipt_id
   --varchar2_1 item
   --varchar2_2 match_group_id
   insert into gtt_num_num_str_str_date_date(number_1,
                                             number_2,
                                             varchar2_1,
                                             varchar2_2)
      select im_receipt_item_posting_seq.nextval,
             rd.shipment,
             rd.item,
             rd.match_group_id
        from gtt_6_num_6_str_6_date gtt,
             im_match_rcpt_detl_ws rd
       where rd.match_group_id = gtt.number_2;

   insert into im_receipt_item_posting (seq_no,
                                        shipment,
                                        item,
                                        qty_matched,
                                        qty_posted,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
   select gtt.number_1,
          gtt.number_2,
          gtt.varchar2_1,
          rd.qty_available,
          null,
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_num_num_str_str_date_date gtt,
          im_match_rcpt_detl_ws rd
    where rd.match_group_id = gtt.varchar2_2
      and rd.shipment       = gtt.number_2
      and rd.item           = gtt.varchar2_1;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_rcpt_item_posting_invoice (seq_no,
                                             doc_id,
                                             status,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
   select gtt.number_1,
          id.doc_id,
          'M',
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_num_num_str_str_date_date gtt,
          im_match_invc_detl_ws id
    where id.match_group_id = gtt.varchar2_2
      and id.item           = gtt.varchar2_1;

   LOGGER.LOG_INFORMATION(L_program||' insert im_rcpt_item_posting_invoice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to partially matched receipts
   --------------------------------------

   merge into im_partially_matched_receipts target
   using (select rd.shipment,
                 rd.item,
                 rd.qty_available qty_matched
            from gtt_num_num_str_str_date_date gtt,
                 im_match_rcpt_detl_ws rd
           where rd.match_group_id = gtt.varchar2_2
             and rd.shipment       = gtt.number_2
             and rd.item           = gtt.varchar2_1
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item)
   when matched then update
    set target.qty_matched       = target.qty_matched + use_this.qty_matched,
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE
   when not matched then insert (shipment,
                                 item,
                                 qty_matched,
                                 created_by,
                                 creation_date,
                                 last_updated_by,
                                 last_update_date,
                                 object_version_id)
                         values (use_this.shipment,
                                 use_this.item,
                                 use_this.qty_matched,
                                 L_user,
                                 sysdate,
                                 L_user,
                                 sysdate,
                                 REIM_CONSTANTS.ONE);

   --------------------------------------
   --write to doc entities
   --------------------------------------

   --Matched details
   merge into im_invoice_detail target
   using (select /*+ ORDERED INDEX(H IM_MATCH_GROUP_HEAD_WS_I22) INDEX(I IM_MATCH_GROUP_INVC_WS_I21) */
                 i.doc_id,
                 h.item,
                 h.cost_variance,
                 h.qty_variance,
                 rank() over (partition by i.invc_group_id order by i.doc_id) variance_rnk
            from gtt_6_num_6_str_6_date gtt,
                 im_match_group_head_ws h,
                 im_match_group_invc_ws i
           where gtt.number_2       = h.match_group_id
             and h.invc_group_id    = i.invc_group_id
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status                         = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched                   = 'Y',
        target.qty_matched                    = 'Y',
        target.cost_variance_within_tolerance = use_this.cost_variance,
        target.qty_variance_within_tolerance  = DECODE(use_this.variance_rnk,
                                                       REIM_CONSTANTS.ONE, use_this.qty_variance,
                                                       target.qty_variance_within_tolerance),
        target.last_updated_by                = L_user,
        target.last_update_date               = sysdate,
        target.object_version_id              = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_INVOICE_DETAIL matched - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_invoice_detail target
   using (select a.doc_id,
                 a.item,
                 a.unit_cost,
                 a.action
            from im_resolution_action_ws a
           where a.workspace_id = I_workspace_id
             and a.match_luw_id = I_match_luw_id
             and a.unit_cost    is not null) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
      set target.cost_matched                   = REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD,
          target.resolution_adjusted_unit_cost  = decode(use_this.action,
                                                         'RCA',    target.resolution_adjusted_unit_cost,
                                                         'RCAS',   target.resolution_adjusted_unit_cost,
                                                         'RCAMR',  target.resolution_adjusted_unit_cost,
                                                         'RCASMR', target.resolution_adjusted_unit_cost,
                                                         'RUA',    target.resolution_adjusted_unit_cost,
                                                         target.resolution_adjusted_unit_cost + use_this.unit_cost);

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_INVOICE_DETAIL matched autores cost - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_invoice_detail target
   using (select a.doc_id,
                 a.item,
                 a.qty,
                 a.action
            from im_resolution_action_ws a
           where a.workspace_id = I_workspace_id
             and a.match_luw_id = I_match_luw_id
             and a.qty          is not null) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
      set target.qty_matched              = REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD,
          target.resolution_adjusted_qty  = decode(use_this.action,
                                                   'RCA',    target.resolution_adjusted_qty,
                                                   'RCAS',   target.resolution_adjusted_qty,
                                                   'RCAMR',  target.resolution_adjusted_qty,
                                                   'RCASMR', target.resolution_adjusted_qty,
                                                   'RUA',    target.resolution_adjusted_qty,
                                                   target.resolution_adjusted_qty + use_this.qty);

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_INVOICE_DETAIL matched autores qty - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Discrepant details
   merge into im_invoice_detail target
   using (select /*+ FULL(IMIDW) FULL(IMIW) */
                 imiw.workspace_id,
                 imiw.doc_id,
                 imidw.item
            from im_match_invc_ws imiw,
                 im_match_invc_detl_ws imidw
           where imiw.workspace_id     = I_workspace_id
             and imiw.match_luw_id     = I_match_luw_id
             and imiw.status           <> REIM_CONSTANTS.DOC_STATUS_MTCH
             and imiw.match_stgy_id    is NOT NULL
             and imiw.header_only      = 'N'
             and imiw.detail_mtch_elig = 'Y'
             --
             and imidw.workspace_id    = imiw.workspace_id
             and imidw.doc_id          = imiw.doc_id
             and imidw.status          <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
             --
             and EXISTS (select 'x'
                           from im_match_stgy_detail imsd
                          where imsd.match_stgy_id = imiw.match_stgy_id
                            and imsd.match_level   = REIM_CONSTANTS.MATCH_LEVEL_DETAIL
                            and imsd.match_type    <> REIM_CONSTANTS.MATCH_TYPE_PARENT)
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status            = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH,
        target.qty_matched       = REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC,
        target.cost_matched      = REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC,
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_INVOICE_DETAIL unmatched - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Fully matched documents
   merge into im_doc_head target
   using (select /*+ ORDERED */
                 id.doc_id,
                 SUM(DECODE(id.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) status_cnt
            from gtt_6_num_6_str_6_date gtt,
                 im_match_group_head_ws h,
                 im_match_group_invc_ws i,
                 im_match_invc_detl_ws iw,
                 im_invoice_detail id
           where gtt.number_2    = h.match_group_id
             and h.invc_group_id = i.invc_group_id
             --
             and iw.workspace_id = I_workspace_id
             and iw.match_luw_id = I_match_luw_id
             and iw.doc_id       = i.doc_id
             --
             and i.doc_id        = id.doc_id
             and iw.item         = id.item
           GROUP BY id.doc_id
   ) use_this
   on (target.doc_id  = use_this.doc_id)
   when matched then update
    set target.status                    = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                  REIM_CONSTANTS.DOC_STATUS_URMTCH),
        target.match_id                  = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, L_user,
                                                  NULL),
        target.match_date                = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, L_vdate,
                                                  NULL),
        target.match_type                = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                  NULL),
        target.detail_matched            = 'Y',
        target.last_updated_by           = L_user,
        target.last_update_date          = sysdate,
        target.object_version_id         = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --autores to doc head
   merge into im_doc_head target
   using (select id.doc_id,
                 sum(id.resolution_adjusted_unit_cost * id.resolution_adjusted_qty) res_cost,
                 sum(id.resolution_adjusted_qty) res_qty
            from (select distinct a.doc_id
                    from im_resolution_action_ws a
                   where a.workspace_id = I_workspace_id
                     and a.match_luw_id = I_match_luw_id) ra,
                 im_invoice_detail id
             where ra.doc_id       = id.doc_id
           group by id.doc_id) use_this
   on (target.doc_id = use_this.doc_id)
   when matched then update
      set target.resolution_adjusted_total_cost = use_this.res_cost,
          target.resolution_adjusted_total_qty  = use_this.res_qty;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_head resolution adj - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --auto resolution/partially matched
   --------------------------------------

   insert into im_resolution_action (doc_id,
                                     item,
                                     reason_code_id,
                                     action,
                                     qty,
                                     unit_cost,
                                     extended_cost,
                                     status,
                                     shipment,
                                     created_by,
                                     creation_date,
                                     last_updated_by,
                                     last_update_date,
                                     object_version_id)
   select ra.doc_id,
          ra.item,
          ra.reason_code_id,
          ra.action,
          ra.qty,
          ra.unit_cost,
          ra.extended_cost,
          ra.status,
          ra.shipment,
          L_user,
          sysdate,
          L_user,
          sysdate,
          0
     from im_resolution_action_ws ra
    where ra.workspace_id = I_workspace_id
      and ra.match_luw_id = I_match_luw_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_resolution_action - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_receiver_cost_adjust (order_no,
                                        item,
                                        location,
                                        supplier,
                                        adjusted_unit_cost,
                                        currency_code,
                                        comments,
                                        TYPE,
                                        reason_code_id,
                                        created_by,
                                        shipment,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
   select distinct oh.order_no,
          gh.item,
          min(ol.location) over (partition by sh.shipment, oh.order_no, gh.item, irc.reason_code_id),
          oh.supplier,
          gh.rcpt_group_cost - ra.unit_cost,
          oh.currency_code,
          NULL, --comment
          DECODE(irc.action,
                  'RCA', 'POR',
                  'RCAS', 'PORS',
                  'RCAMR', 'POMR',
                  'RCAMRS', 'POMRS'),
          irc.reason_code_id,
          L_user,
          sh.shipment,
          L_vdate,
          L_user,
          L_vdate,
          0
     from im_resolution_action_ws ra,
          im_match_group_head_ws gh,
          im_match_group_rcpt_ws gr,
          im_reason_codes irc,
          --
          (select wh loc, physical_wh phy_loc, 'W' loc_type from wh where physical_wh != wh
           union all
           select store loc, store phy_loc, 'S' loc_type from store) loc,
          --
          shipment sh,
          ordhead oh,
          ordloc ol
    where ra.workspace_id      = I_workspace_id
      and ra.match_luw_id      = I_match_luw_id
      and ra.workspace_id      = gh.workspace_id
      and ra.match_group_id    = gh.match_group_id
      and ra.match_luw_id      = gh.match_luw_id
      and gh.workspace_id      = gr.workspace_id
      and gh.rcpt_group_id     = gr.rcpt_group_id
      and gh.match_luw_id      = gr.match_luw_id
      --
      and ra.reason_code_id    = irc.reason_code_id
      and irc.reason_code_type = 'C'
      and irc.action           in ('RCA', 'RCAS','RCAMR','RCAMRS')
      --
      and gr.shipment          = sh.shipment
      and oh.order_no          = sh.order_no
      and sh.to_loc            = loc.phy_loc
      and loc.loc              = ol.location
      and ol.order_no          = oh.order_no
      and ol.item              = gh.item;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receiver_cost_adjust - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_receiver_unit_adjust (shipment,
                                        item,
                                        seq_no,
                                        adjusted_item_qty,
                                        comments,
                                        reason_code_id,
                                        created_by,
                                        location,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
   select inner.shipment,
          inner.item,
          1,
          inner.adjusted_item_qty,
          NULL,
          inner.reason_code_id,
          L_user,
          inner.to_loc,
          L_vdate,
          L_user,
          L_vdate,
          0
     from (select sh.shipment,
                  gh.item,
                  (-1) * ra.qty adjusted_item_qty,
                  irc.reason_code_id,
                  sh.to_loc,
                  rank() over (partition by gh.workspace_id, gh.match_group_id, gh.rcpt_group_id, gh.item
                                   order by gr.shipment) ship_rank
             from im_resolution_action_ws ra,
                  im_match_group_head_ws gh,
                  im_match_group_rcpt_ws gr,
                  --
                  im_reason_codes irc,
                  shipment sh
            where ra.workspace_id      = I_workspace_id
              and ra.match_luw_id      = I_match_luw_id
              and ra.workspace_id      = gh.workspace_id
              and ra.match_group_id    = gh.match_group_id
              and ra.match_luw_id      = gh.match_luw_id
              and gh.workspace_id      = gr.workspace_id
              and gh.rcpt_group_id     = gr.rcpt_group_id
              and gh.match_luw_id      = gr.match_luw_id
              --
              and ra.reason_code_id    = irc.reason_code_id
              and irc.reason_code_type = 'Q'
              and irc.action           = 'RUA'
              --
              and gr.shipment          = sh.shipment) inner
    where inner.ship_rank = REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receiver_unit_adjust - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to shipment entities
   --------------------------------------

   merge into shipsku target
   using (select /*+ ORDERED INDEX(SS PK_SHIPSKU) INDEX(H IM_MATCH_GROUP_HEAD_WS_I22) INDEX(R IM_MATCH_GROUP_RCPT_WS_I21) */
                 ss.shipment,
                 ss.item,
                 ss.seq_no,
                 nvl(ss.qty_received,0) - nvl(ss.qty_matched,0) qty_matched
            from gtt_6_num_6_str_6_date gtt,
                 im_match_group_head_ws h,
                 im_match_group_rcpt_ws r,
                 shipsku ss
           where gtt.number_2                  = h.match_group_id
             and h.rcpt_group_id               = r.rcpt_group_id
             --
             and r.shipment                    = ss.shipment
             and h.item                        = ss.item
             and nvl(ss.invc_match_status,'U') != REIM_CONSTANTS.SSKU_IM_STATUS_MTCH
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item
       and target.seq_no   = use_this.seq_no)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
        target.qty_matched       = NVL(target.qty_matched, REIM_CONSTANTS.ZERO) + use_this.qty_matched;

   LOGGER.LOG_INFORMATION(L_program||' merge shipsku - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into shipment target
   using (select /*+ INDEX(SS PK_SHIPMENT) */
                 ss.shipment,
                 count(distinct nvl(ss.invc_match_status,'null'||rownum)) status_cnt
            from gtt_6_num_6_str_6_date gtt,
                 im_match_group_rcpt_ws r,
                 shipsku ss
           where r.rcpt_group_id = gtt.number_4
             and r.shipment      = ss.shipment
           group by ss.shipment
   ) use_this
   on (    target.shipment     = use_this.shipment
       and use_this.status_cnt = 1)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_MTCH,
        target.invc_match_date   = L_vdate;

   LOGGER.LOG_INFORMATION(L_program||' merge shipment - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);


   LOGGER.LOG_INFORMATION('End ' || L_program );

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERSIST_SKU_LVL_DETL_MATCH;
------------------------------------------------------------------------------------------
FUNCTION PERSIST_STYLE_LVL_DETL_MATCH(O_error_message    OUT VARCHAR2,
                                      I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                      I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PERSIST_STYLE_LVL_DETL_MATCH';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_user  IM_DOC_HEAD.MATCH_ID%TYPE := get_user;
   L_vdate DATE                      := get_vdate;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: ' || I_workspace_id
                          || ' I_match_luw_id: ' || I_match_luw_id);

   LOGGER.LOG_INFORMATION('End ' || L_program );

   --number_1   detail_match_history_seq
   --number_2   match_group_id
   --varchar2_1 item_parent
   --varchar2_2 exact_match
   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date(number_1,
                                             number_2,
                                             varchar2_1,
                                             varchar2_2)
   select im_detail_match_history_seq.nextval,
          imghw.match_group_id,
          imghw.item_parent,
          case when imghw.qty_variance = 0 and imghw.cost_variance = 0 then 'Y' else 'N' end
     from im_match_group_head_ws imghw,
          im_match_stgy_detail imsd
    where imghw.workspace_id      = I_workspace_id
      and imghw.match_luw_id      = I_match_luw_id
      and imghw.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
      and imghw.match_type        = REIM_CONSTANTS.MATCH_LEVEL_DETAIL
      and imghw.match_stgy_dtl_id = imsd.match_stgy_dtl_id
      and imsd.match_type         = REIM_CONSTANTS.MATCH_TYPE_PARENT;

   insert into im_detail_match_history (match_id,
                                        auto_matched,
                                        exact_match,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             'Y',
             gtt.varchar2_2,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_invc_history (match_id,
                                             doc_id,
                                             item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             imidw.doc_id,
             imidw.item,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt,
             im_match_invc_detl_ws imidw
       where gtt.number_2   = imidw.match_group_id
         and gtt.varchar2_1 = imidw.item_parent;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_rcpt_history (match_id,
                                             shipment,
                                             item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             imrdw.shipment,
             imrdw.item,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt,
             im_match_rcpt_detl_ws imrdw
       where gtt.number_2   = imrdw.match_group_id
         and gtt.varchar2_1 = imrdw.item_parent;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to partially matched receipts
   --------------------------------------

   merge into im_partially_matched_receipts tgt
   using (select imrdw.shipment,
                 imrdw.item,
                 imrdw.qty_available qty_matched
            from gtt_num_num_str_str_date_date gtt,
                 im_match_rcpt_detl_ws imrdw
           where gtt.number_2   = imrdw.match_group_id
             and gtt.varchar2_1 = imrdw.item_parent) src
   on (    tgt.shipment = src.shipment
       and tgt.item     = src.item)
   when matched then update
    set tgt.qty_matched       = tgt.qty_matched + src.qty_matched,
        tgt.last_updated_by   = L_user,
        tgt.last_update_date  = sysdate,
        tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE
   when not matched then insert (shipment,
                                 item,
                                 qty_matched,
                                 created_by,
                                 creation_date,
                                 last_updated_by,
                                 last_update_date,
                                 object_version_id)
                         values (src.shipment,
                                 src.item,
                                 src.qty_matched,
                                 L_user,
                                 sysdate,
                                 L_user,
                                 sysdate,
                                 REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' Merge im_partially_matched_receipts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --number_1   seq_no
   --number_2   match_group_id
   --number_3   shipment
   --number_4   qty_matched
   --varchar2_1 item
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date (number_1,
                                       number_2,
                                       number_3,
                                       number_4,
                                       varchar2_1)
      select im_receipt_item_posting_seq.nextval,
             imrdw.match_group_id,
             imrdw.shipment,
             imrdw.qty_available,
             imrdw.item
        from gtt_num_num_str_str_date_date gtt,
             im_match_rcpt_detl_ws imrdw
       where gtt.number_2 = imrdw.match_group_id;

   insert into im_receipt_item_posting (seq_no,
                                        shipment,
                                        item,
                                        qty_matched,
                                        qty_posted,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
   select gtt.number_1,
          gtt.number_3,
          gtt.varchar2_1,
          gtt.number_4,
          NULL,
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_6_num_6_str_6_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_rcpt_item_posting_invoice (seq_no,
                                             doc_id,
                                             status,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select distinct gtt.number_1,
             imiw.doc_id,
             'M',
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt,
             im_match_invc_detl_ws imiw
       where imiw.match_group_id = gtt.number_2;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting_invoice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if PRORATE_STYLE_LVL_QTY_VAR(O_error_message,
                                I_workspace_id,
                                I_match_luw_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   merge into im_invoice_detail target
   using (select gtt.varchar2_1 doc_id,
                 gtt.varchar2_2 item,
                 gtt.number_1   cost_variance,
                 gtt.number_2   qty_variance
            from gtt_10_num_10_str_10_date gtt
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status                         = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched                   = 'Y',
        target.qty_matched                    = 'Y',
        target.cost_variance_within_tolerance = use_this.cost_variance,
        target.qty_variance_within_tolerance  = use_this.qty_variance,
        target.last_updated_by                = L_user,
        target.last_update_date               = sysdate,
        target.object_version_id              = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_INVOICE_DETAIL matched - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Fully matched documents
   merge into im_doc_head target
   using (select /*+ ORDERED */
                 id.doc_id,
                 SUM(DECODE(id.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) status_cnt
            from gtt_6_num_6_str_6_date gtt,
                 im_match_group_head_ws h,
                 im_match_invc_detl_ws iw,
                 im_invoice_detail id
           where gtt.number_2     = h.match_group_id
             and h.match_group_id = iw.match_group_id
             --
             and iw.doc_id        = id.doc_id
             and iw.item          = id.item
           GROUP BY id.doc_id
   ) use_this
   on (target.doc_id  = use_this.doc_id)
   when matched then update
    set target.status                    = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                  REIM_CONSTANTS.DOC_STATUS_URMTCH),
        target.match_id                  = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, L_user,
                                                  NULL),
        target.match_date                = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, L_vdate,
                                                  NULL),
        target.match_type                = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                  NULL),
        target.detail_matched            = 'Y',
        target.last_updated_by           = L_user,
        target.last_update_date          = sysdate,
        target.object_version_id         = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into shipsku target
   using (select ss.shipment,
                 ss.item,
                 ss.seq_no,
                 nvl(ss.qty_received,0) - nvl(ss.qty_matched,0) qty_matched
            from gtt_num_num_str_str_date_date gtt,
                 im_match_group_head_ws h,
                 im_match_rcpt_detl_ws r,
                 shipsku ss
           where gtt.number_2                  = h.match_group_id
             and h.match_group_id              = r.match_group_id
             --
             and r.shipment                    = ss.shipment
             and r.item                        = ss.item
             and nvl(ss.invc_match_status,'U') != REIM_CONSTANTS.SSKU_IM_STATUS_MTCH
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item
       and target.seq_no   = use_this.seq_no)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
        target.qty_matched       = NVL(target.qty_matched, REIM_CONSTANTS.ZERO) + use_this.qty_matched;

   LOGGER.LOG_INFORMATION(L_program||' merge shipsku - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into shipment target
   using (select /*+ INDEX(SS PK_SHIPMENT) */
                 ss.shipment,
                 count(distinct nvl(ss.invc_match_status,'null'||rownum)) status_cnt
            from gtt_num_num_str_str_date_date gtt,
                 im_match_rcpt_detl_ws r,
                 shipsku ss
           where r.match_group_id = gtt.number_2
             and r.shipment       = ss.shipment
           group by ss.shipment
   ) use_this
   on (    target.shipment     = use_this.shipment
       and use_this.status_cnt = 1)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_MTCH,
        target.invc_match_date   = L_vdate;

   LOGGER.LOG_INFORMATION(L_program||' merge shipment - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('End ' || L_program );
   REIM_PKG_UTIL_SQL.LOG_TIME (L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERSIST_STYLE_LVL_DETL_MATCH;
------------------------------------------------------------------------------------------
FUNCTION PRORATE_STYLE_LVL_QTY_VAR(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                   I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(50) := 'REIM_AUTO_MATCH_SQL.PRORATE_STYLE_LVL_QTY_VAR';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete from gtt_10_num_10_str_10_date;
   insert into gtt_10_num_10_str_10_date (varchar2_1, --doc_id
                                          varchar2_2, --item
                                          number_1,   --cost_variance
                                          number_2)   --qty_variance
   select doc_id,
          item,
          cost_variance,
          qty_variance
     from (select imidw.doc_id,
                  imidw.item,
                  imidw.item_parent,
                  imghw.cost_variance,
                  imghw.qty_variance total_qty_variance,
                  SIGN(imghw.qty_variance) * CEIL(ABS(imghw.qty_variance) * RATIO_TO_REPORT(imidw.invoice_qty) OVER (PARTITION BY imidw.item_parent)) doc_qty_variance,
                  imidw.invoice_qty
             from gtt_num_num_str_str_date_date gtt,
                  im_match_group_head_ws imghw,
                  im_match_invc_detl_ws imidw
            where gtt.number_2         = imghw.match_group_id
              and imghw.match_group_id = imidw.match_group_id
              and imghw.item_parent    = imidw.item_parent)inner
    model
       partition by (item_parent)
       dimension by (ROW_NUMBER() over (PARTITION BY item_parent ORDER BY item_parent, invoice_qty desc, doc_id) rn)
       measures (doc_id,
                 item,
                 item_parent,
                 cost_variance,
                 total_qty_variance,
                 doc_qty_variance,
                 SUM(NVL(doc_qty_variance,0)) OVER (PARTITION BY item_parent
                                                        ORDER BY item_parent, invoice_qty desc, doc_id) run_total,
                 0 qty_variance)
       rules sequential order(qty_variance[rn] = case
                                                    when total_qty_variance[CV(rn)] = REIM_CONSTANTS.ZERO THEN
                                                       REIM_CONSTANTS.ZERO
                                                    when Abs(run_total[CV(rn)]) <= Abs(total_qty_variance[CV(rn)]) then
                                                       doc_qty_variance[CV(rn)]
                                                    when Abs(run_total[CV(rn) - 1]) < Abs(total_qty_variance[CV(rn)]) then
                                                       total_qty_variance[CV(rn)] - run_total[CV(rn) - 1]
                                                    else
                                                       REIM_CONSTANTS.ZERO
                                                 end);

   REIM_PKG_UTIL_SQL.LOG_TIME (L_program ||'-'||I_workspace_id||'-'||I_match_luw_id, L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PRORATE_STYLE_LVL_QTY_VAR;
------------------------------------------------------------------------------------------
END REIM_AUTO_MATCH_SQL;
/