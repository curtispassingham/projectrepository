CREATE OR REPLACE PACKAGE BODY REIM_POSTING_SQL AS

/**
 *  -------- PRIVATE PROCS AND FUNCTIONS SPECS --------------
 */
TB CONSTANT VARCHAR2(1) := CHR(9);

PROCEDURE LOG_DEBUG
    (I_POSTING_ID IN IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
     I_MESSAGE IN IM_POSTING_DOC_ERRORS.ERROR_MSG%TYPE);
----------------------------------------------------------------------------------------------
PROCEDURE LOG_DEBUG_PRORATE_WORKSPACE
       (I_POSTING_ID NUMBER,
        I_PRORATE_WORKSPACE_ID NUMBER,
        I_COMMENT VARCHAR2);
----------------------------------------------------------------------------------------------
PROCEDURE GET_DOC_AMOUNTS
       (I_POSTING_ID NUMBER,
        C_DOC_AMOUNTS IN OUT SYS_REFCURSOR);
----------------------------------------------------------------------------------------------
FUNCTION PRORATE_DOC_AMOUNT
       (O_ERROR_MESSAGE  OUT VARCHAR2,
        I_PROGRAM        IN VARCHAR2,
        I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE,
        I_AMOUNT_TYPE    IN VARCHAR2)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to Create/update/delete manual groups.
 * Used for Online Resolutions.
 */
FUNCTION UPDATE_POSTING_DOC_SOB(O_error_message IN OUT VARCHAR2,
                                I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------

    /**
     *  --------- PUBLIC PROCS AND FUNCTIONS BODIES --------------
     */


    /************************************************************
     * Begins the posting process.   Mainly inserts a new
     * row into IM_POSTING_STATUS
     ************************************************************/
    FUNCTION BEGIN_POSTING_PROCESS
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_STATUS.POSTING_ID%TYPE,
            I_DESCRIPTION    IN IM_POSTING_STATUS.DESCRIPTION%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.BEGIN_POSTING_PROCESS';
    BEGIN

        LOG_DEBUG
            (I_POSTING_ID,
             'BEGIN'|| TDELIM || L_PROGRAM);

        INSERT INTO IM_POSTING_STATUS
                     (POSTING_ID,
                        STATUS,
                        HAS_ERRORS,
                        DESCRIPTION,
                        START_DATE,
                        END_DATE,
                        CREATED_BY)
        VALUES     (I_POSTING_ID,
                        REIM_POSTING_SQL.POSTING_STATUS_NEW,
                        YN_NO,
                        SUBSTR(I_DESCRIPTION, 0, 49),
                        SYSDATE,
                        NULL,
                        USER);
       LOG_DEBUG
            (I_POSTING_ID,
             'END'|| TDELIM || L_PROGRAM);

        RETURN SUCCESS;


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END BEGIN_POSTING_PROCESS;



/*******************

Identifies Matched Invoices eligible for posting.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION PREPARE_MATCHED_INVOICES(O_ERROR_MESSAGE  OUT VARCHAR2,
                                      I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                      I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                      I_SYS_CURR_CODE IN SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS

        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_MATCHED_INVOICES';
        L_DOC_ID_COUNT  NUMBER(6) ;
        Status NUMBER(10);

    BEGIN

       LOG_DEBUG
            (I_POSTING_ID,
             'BEGIN'|| TDELIM || L_PROGRAM);

        IF I_DOC_ID_LIST IS NOT NULL
        THEN
        L_DOC_ID_COUNT := I_DOC_ID_LIST.COUNT;
        ELSE
        L_DOC_ID_COUNT := 0;
        END IF;

        -- Identify the invoices and gather information that
        -- would help in the calculation of posting values later...
        INSERT INTO IM_POSTING_DOC
                     (POSTING_ID,
                        DOC_ID,
                        DOC_TYPE,
                        CALC_BASIS,
                        MATCH_TYPE,
                        RESOLUTION_COUNT,
                        DOC_TAX_COUNT,
                        PARENT_ID,
                        DOC_DATE,
                        VENDOR_TYPE,
                        VENDOR,
                        ORDER_NO,
                        BEST_TERMS,
                        BEST_TERMS_DATE,
                        MANUALLY_PAID_IND,
                        PRE_PAID_IND,
                        DEAL_ID,
                        EXT_DOC_ID,
                        CURRENCY_CODE,
                        LOCATION,
                        LOC_TYPE,
                        DECIMAL_DIGITS,
                        SYS_CURRENCY_CODE,
                        SYS_TAX_IND
                       )
        (SELECT I_POSTING_ID,
                  IDH.DOC_ID,
                  IDH.TYPE,
                  -- Calculations are mostly based
                  -- on whether a document has details or not.
                  CASE
                    WHEN (SELECT COUNT(*)
                            FROM   IM_INVOICE_DETAIL
                            WHERE  DOC_ID = IDH.DOC_ID) > 0 THEN REIM_POSTING_SQL.BASIS_DETAILS
                    ELSE REIM_POSTING_SQL.BASIS_HEADER
                  END CALC_BASIS,
                  -- We need to know if the document
                  -- was matched at summary (group or one-to-one) OR detail levels.
                  CASE
                    WHEN (SELECT COUNT(* )
                            FROM   IM_SUMMARY_MATCH_INVC_HISTORY
                            WHERE  DOC_ID = IDH.DOC_ID) > 0 THEN REIM_POSTING_SQL.MATCH_TYPE_SUMMARY
                    ELSE   REIM_POSTING_SQL.MATCH_TYPE_DETAIL
                  END MATCH_TYPE,
                  --
                  (SELECT COUNT(* )
                   FROM   IM_RESOLUTION_ACTION
                   WHERE  DOC_ID = IDH.DOC_ID),
                  --
                  (SELECT COUNT(* )
                   FROM   IM_DOC_TAX
                   WHERE  DOC_ID = IDH.DOC_ID),

                  IDH.PARENT_ID,
                  IDH.DOC_DATE,
                  IDH.VENDOR_TYPE,
                  IDH.VENDOR,
                  IDH.ORDER_NO,
                  IDH.BEST_TERMS,
                  IDH.BEST_TERMS_DATE,
                  IDH.MANUALLY_PAID_IND,
                  IDH.PRE_PAID_IND,
                  IDH.DEAL_ID,
                  IDH.EXT_DOC_ID,
                  IDH.CURRENCY_CODE,
                  IDH.LOCATION,
                  IDH.LOC_TYPE,
                  NVL((SELECT MAX(ICL.CURRENCY_COST_DEC) FROM IM_CURRENCIES ICL WHERE ICL.CURRENCY_CODE = IDH.CURRENCY_CODE), REIM_POSTING_SQL.DEFAULT_DIGITS),
                  I_SYS_CURR_CODE,
                  DECODE(NVL(REVERSE_VAT_IND, REIM_POSTING_SQL.YN_NO),
                         REIM_POSTING_SQL.YN_YES, REIM_POSTING_SQL.SYS_TAX_IND_REV_CHRG_VAT,
                         REIM_POSTING_SQL.YN_NO)
         FROM   IM_DOC_HEAD IDH
         WHERE  IDH.TYPE = REIM_POSTING_SQL.DOC_TYPE_MERCH_INVOICE
                  AND IDH.STATUS IN (REIM_POSTING_SQL.DOC_STATUS_MATCHED)
                  AND IDH.HOLD_STATUS NOT IN (DOC_HELD)
                  AND NOT EXISTS (SELECT 1
                                        FROM   IM_RESOLUTION_ACTION IRA
                                        WHERE  IRA.DOC_ID = IDH.DOC_ID
                                                 AND IRA.STATUS = REASON_STATUS_UNROLLED)
                  AND NOT EXISTS (SELECT 1
                                        FROM   IM_INVOICE_DETAIL IID
                                        WHERE  IID.DOC_ID = IDH.DOC_ID
                                                 AND IID.ADJUSTMENT_PENDING = YN_YES)
                  AND (
                         (L_DOC_ID_COUNT > 0
                          AND IDH.DOC_ID IN (
                              SELECT * FROM TABLE(CAST(I_DOC_ID_LIST AS OBJ_NUMERIC_ID_TABLE))
                         )
                      )
                      OR
                      (
                          L_DOC_ID_COUNT = 0
                      )
                  )
        );

        IF LOAD_DOC_MERCH_COMPONENTS(O_ERROR_MESSAGE,
                                     I_POSTING_ID) = FAIL then
           RETURN FAIL;
        END IF;

        IF UPDATE_POSTING_DOC_SOB(O_ERROR_MESSAGE,
                                  I_POSTING_ID) = FAIL then
           RETURN FAIL;
        END IF;

        RETURN SUCCESS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END PREPARE_MATCHED_INVOICES;


    /************************************************************
     * Identifies all the pre-paid invoices to be posted.
     * Mainly inserts into IM_POSTING_DOC
     ************************************************************/
    FUNCTION PREPARE_PREPAID_INVOICES(O_ERROR_MESSAGE  OUT VARCHAR2,
                                      I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                      I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                      I_SYS_CURR_CODE IN SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS

        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_PREPAID_INVOICES';
        L_DOC_ID_COUNT  NUMBER(6) ;

    BEGIN
        -- Identify the invoices and gather information that
        -- would help in the calculation of posting values later...
        INSERT INTO IM_POSTING_DOC
                     (POSTING_ID,
                        DOC_ID,
                        DOC_TYPE,
                        CALC_BASIS,
                        MATCH_TYPE,
                        RESOLUTION_COUNT,
                        DOC_TAX_COUNT,
                        PARENT_ID,
                        DOC_DATE,
                        VENDOR_TYPE,
                        VENDOR,
                        ORDER_NO,
                        BEST_TERMS,
                        BEST_TERMS_DATE,
                        MANUALLY_PAID_IND,
                        PRE_PAID_IND,
                        DEAL_ID,
                        EXT_DOC_ID,
                        CURRENCY_CODE,
                        LOCATION,
                        LOC_TYPE,
                        DECIMAL_DIGITS,
                        SYS_CURRENCY_CODE)
        (SELECT I_POSTING_ID,
                  IDH.DOC_ID,
                  IDH.TYPE,
                  -- Calculations are mostly based
                  -- on whether a document has details or not.
                  CASE
                    WHEN (SELECT COUNT(*)
                            FROM   IM_INVOICE_DETAIL
                            WHERE  DOC_ID = IDH.DOC_ID) > 0 THEN REIM_POSTING_SQL.BASIS_DETAILS
                    ELSE REIM_POSTING_SQL.BASIS_HEADER
                  END CALC_BASIS,
                  -- We need to know if the document
                  -- was matched at summary (group or one-to-one) OR detail levels.
                  CASE
                    WHEN (SELECT COUNT(* )
                            FROM   IM_SUMMARY_MATCH_INVC_HISTORY
                            WHERE  DOC_ID = IDH.DOC_ID) > 0 THEN REIM_POSTING_SQL.MATCH_TYPE_SUMMARY
                    ELSE   REIM_POSTING_SQL.MATCH_TYPE_DETAIL
                  END MATCH_TYPE,
                  --
                  (SELECT COUNT(* )
                   FROM   IM_RESOLUTION_ACTION
                   WHERE  DOC_ID = IDH.DOC_ID),
                  --
                  (SELECT COUNT(* )
                   FROM   IM_DOC_TAX
                   WHERE  DOC_ID = IDH.DOC_ID),

                  IDH.PARENT_ID,
                  IDH.DOC_DATE,
                  IDH.VENDOR_TYPE,
                  IDH.VENDOR,
                  IDH.ORDER_NO,
                  IDH.BEST_TERMS,
                  IDH.BEST_TERMS_DATE,
                  IDH.MANUALLY_PAID_IND,
                  IDH.PRE_PAID_IND,
                  IDH.DEAL_ID,
                  IDH.EXT_DOC_ID,
                  IDH.CURRENCY_CODE,
                  IDH.LOCATION,
                  IDH.LOC_TYPE,
                  NVL((SELECT MAX(ICL.CURRENCY_COST_DEC) FROM IM_CURRENCIES ICL WHERE ICL.CURRENCY_CODE = IDH.CURRENCY_CODE), REIM_POSTING_SQL.DEFAULT_DIGITS),
                  I_SYS_CURR_CODE
         FROM   IM_DOC_HEAD IDH
         WHERE  IDH.TYPE = REIM_POSTING_SQL.DOC_TYPE_MERCH_INVOICE
                AND IDH.HOLD_STATUS NOT IN (DOC_HELD)
                AND (IDH.DOC_ID IN (SELECT * FROM TABLE(CAST(I_DOC_ID_LIST AS OBJ_NUMERIC_ID_TABLE))))
        );

        IF UPDATE_POSTING_DOC_SOB(O_ERROR_MESSAGE,
                                  I_POSTING_ID) = FAIL then
           RETURN FAIL;
        END IF;

    RETURN SUCCESS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END;


    /************************************************************
     * Validates matched invoices to be posted.
     * Disqualifies any invoice loaded into IM_POSTING_DOC
     ************************************************************/
    FUNCTION VALIDATE_INVOICES_TO_POST
           (O_ERROR_MESSAGE                   OUT VARCHAR2,
            I_POSTING_ID                        IN IM_POSTING_DOC.POSTING_ID%TYPE,
            I_HEADER_TO_DETAIL_TOLERANCE  IN NUMBER)
    RETURN NUMBER
    IS

        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.VALIDATE_INVOICES_TO_POST';
        STATUS      NUMBER(10)  := SUCCESS;

        --- CHECK:   Summary matches with no details require
        --- that receipt tracking information must be present. (IM_RCPT_ITEM_POSTING_INVOICE)
        CURSOR RCPT_INFO_PRESENT IS
        SELECT UNIQUE IPD.POSTING_ID,
                 IPD.DOC_ID,
                 'Summary match does not have receipt tracking information' AS MSG
        FROM   IM_POSTING_DOC IPD
        WHERE  POSTING_ID = I_POSTING_ID
                 AND NOT EXISTS (SELECT 1
                                     FROM   IM_RCPT_ITEM_POSTING_INVOICE IRIP
                                     WHERE  IRIP.DOC_ID = IPD.DOC_ID)
                 AND IPD.MATCH_TYPE = REIM_POSTING_SQL.MATCH_TYPE_SUMMARY
                 AND IPD.CALC_BASIS = REIM_POSTING_SQL.BASIS_HEADER;

        --- CHECK:  Appropriate IM_DOC_VAT are present when needed.
        CURSOR DOC_TAX_PRESENT IS
        SELECT UNIQUE IPD.POSTING_ID,
                 IPD.DOC_ID,
                 'Total cost and total cost inc tax are different but no IM_DOC_TAX entries exist.' AS MSG
        FROM   IM_POSTING_DOC IPD,
               IM_DOC_HEAD IDH
        WHERE  POSTING_ID = I_POSTING_ID
        AND    IDH.DOC_ID = IPD.DOC_ID
        AND    IDH.TOTAL_COST_INC_TAX <> IDH.TOTAL_COST
        AND    NOT EXISTS (SELECT 1 FROM IM_DOC_TAX WHERE DOC_ID = IDH.DOC_ID);

        -- CHECK:  Items are valid?
        CURSOR ITEMS_STILL_VALID IS
        SELECT UNIQUE POSTING_ID,
               IPD.DOC_ID,
               'One or more items in the document does not exist in ITEM_MASTER.' AS MSG
        FROM IM_POSTING_DOC IPD
        WHERE POSTING_ID = I_POSTING_ID
        AND  EXISTS (
                SELECT 1
                FROM IM_INVOICE_DETAIL IID
                WHERE IID.DOC_ID = IPD.DOC_ID
                AND NOT EXISTS (SELECT 1 FROM ITEM_MASTER IM WHERE IM.ITEM = IID.ITEM)
            );

        -- CHECK:  Reason Codes have tran code mapping?
        CURSOR C_REASON_TRAN_CODE_MAP IS
        SELECT DISTINCT POSTING_ID,
               IPD.DOC_ID,
               'One or more reason codes is not mapped to a tran code.' AS MSG
        FROM IM_POSTING_DOC IPD,
             IM_RESOLUTION_ACTION IRA
        WHERE IPD.POSTING_ID                                         = I_POSTING_ID
          AND IRA.DOC_ID                                             = IPD.DOC_ID
          AND REIM_POSTING_SQL.IS_ACCOUNTABLE_RESOLUTION(IRA.ACTION) = YN_YES
          AND NOT EXISTS(SELECT 'X'
                           FROM IM_REASON_TRAN_CODE_MAP IRTCM
                          WHERE IRTCM.REASON_CODE_ID  = IRA.REASON_CODE_ID
                            AND IRTCM.SET_OF_BOOKS_ID = IPD.SET_OF_BOOKS_ID);


    BEGIN

        FOR REC IN RCPT_INFO_PRESENT
        LOOP
            STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE, REC.POSTING_ID, REC.DOC_ID, SEVERITY_ERROR, ERROR_CAT_VALIDATION, REC.MSG);
        END LOOP;

        FOR REC IN DOC_TAX_PRESENT
        LOOP
            STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR
                            (O_ERROR_MESSAGE,
                            REC.POSTING_ID,
                            REC.DOC_ID,
                            SEVERITY_ERROR,
                            ERROR_CAT_VALIDATION,
                            REC.MSG);
        END LOOP;


        FOR REC IN ITEMS_STILL_VALID
        LOOP
            STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR
                            (O_ERROR_MESSAGE,
                            REC.POSTING_ID,
                            REC.DOC_ID,
                            SEVERITY_ERROR,
                            ERROR_CAT_VALIDATION,
                            REC.MSG);
        END LOOP;

        FOR REC IN C_REASON_TRAN_CODE_MAP
        LOOP
            STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR
                            (O_ERROR_MESSAGE,
                            REC.POSTING_ID,
                            REC.DOC_ID,
                            SEVERITY_ERROR,
                            ERROR_CAT_VALIDATION,
                            REC.MSG);
        END LOOP;

        RETURN SUCCESS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END VALIDATE_INVOICES_TO_POST;


    /************************************************************
     * Calculates non-merchandise costs.
     * Mainly inserts into IM_POSTING_DOC_AMOUNTS
     ************************************************************/
    FUNCTION CALC_NON_MERCH_COSTS
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE)
    RETURN NUMBER
    IS

        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_NON_MERCH_COSTS';
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

    BEGIN
        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
          INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

        INSERT INTO IM_POSTING_DOC_AMOUNTS IPDA
                       (POSTING_AMT_ID,
                        POSTING_ID,
                        DOC_ID,
                        LOCATION,
                        ORG_UNIT,
                        SET_OF_BOOKS_ID,
                        AMOUNT_TYPE,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        AMOUNT,
                        ACCOUNT_TYPE,
                        ACCOUNT_CODE,
                        --removed in accordance with the TCD for configurable VAT
                        --AMOUNT_VAT,
                        PRIM_CURRENCY_CODE)


        SELECT          IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                        POSTING_ID,
                        DOC_ID,
                        LOC,
                        (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                                   UNION ALL
                                                   SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                           ELSE
                                                           (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                           END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                                  ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
                        SET_OF_BOOKS_ID,
                        AMOUNT_TYPE,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        AMOUNT,
                        NON_MERCH_CODES     ACCOUNT_TYPE,
                        ACCOUNT_CODE,
                        --removed in accordance with the TCD for configurable VAT
                        --AMOUNT_VAT,
                        NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
                FROM
                (SELECT POSTING_ID,
                        DOC_ID,
                        LOCATION LOC,
                        SET_OF_BOOKS_ID,
                        SYS_CURRENCY_CODE,
                        AMOUNT_TYPE,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        ROUND(SUM(AMOUNT), DECIMAL_DIGITS) AMOUNT,
                        ACCOUNT_CODE
                        --SUM(AMOUNT_VAT) AMOUNT_VAT
        FROM (
                SELECT   IPD.POSTING_ID,
                          IPD.DOC_ID,
                          IPD.LOCATION,
                          IPD.SET_OF_BOOKS_ID,
                          IPD.SYS_CURRENCY_CODE,
                          IPD.DECIMAL_DIGITS,
                          REIM_POSTING_SQL.AMT_NON_MERCHANDISE_COST           AS AMOUNT_TYPE,
                          NULL AS DEPT,
                          NULL AS CLASS,
                          NULL AS SUBCLASS,
                          DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDNMT.TAX_CODE)  AS TAX_CODE,
                          DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDNMT.TAX_RATE)  AS TAX_RATE,

                          /**************/
                          -- Custom grouping...
                          IDNM.NON_MERCH_CODE     AS  GROUP_BY_1,
                          NO_VALUE                AS  GROUP_BY_2,
                          NO_VALUE                AS  GROUP_BY_3,
                          NO_VALUE                AS  GROUP_BY_4,
                          NO_VALUE                AS  GROUP_BY_5,
                          NO_VALUE                AS  GROUP_BY_6,
                          NO_VALUE                AS  GROUP_BY_7,
                          NO_VALUE                AS  GROUP_BY_8,
                          NO_VALUE                AS  GROUP_BY_9,
                          /**************/
                          IDNM.NON_MERCH_AMT      AS AMOUNT,
                          IDNM.NON_MERCH_CODE     ACCOUNT_CODE
                          --ROUND(IDNM.NON_MERCH_AMT * IDNM.VAT_RATE / 100, ipd.DECIMAL_digits) AS AMOUNT_VAT
                FROM     IM_POSTING_DOC IPD,
                        IM_DOC_HEAD IDH,
                        IM_DOC_NON_MERCH IDNM,
                        IM_DOC_NON_MERCH_TAX IDNMT
                WHERE   IPD.POSTING_ID = I_POSTING_ID
                AND IDNM.DOC_ID = IDNMT.DOC_ID (+)
                        AND IDNM.NON_MERCH_CODE = IDNMT.NON_MERCH_CODE (+)
                        AND IDH.DOC_ID = IPD.DOC_ID

                        AND IDNM.DOC_ID = IPD.DOC_ID

        )
        GROUP BY POSTING_ID,
                        DOC_ID,
                        LOCATION,
                        SET_OF_BOOKS_ID,
                        SYS_CURRENCY_CODE,
                        AMOUNT_TYPE,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        DECIMAL_DIGITS
        );

        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END CALC_NON_MERCH_COSTS;


    /************************************************************
     * Calculates matched document resolution costs (i.e.
     * IM_RESOLUTION_ACTIONs associated per document to be posted).
     * Mainly inserts into IM_POSTING_DOC_AMOUNTS
     ************************************************************/
    FUNCTION CALC_INVOICE_RESOLUTION_COSTS
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE)
    RETURN NUMBER
    IS

        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_INVOICE_RESOLUTION_COSTS';
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

    BEGIN
        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
          INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

        INSERT INTO IM_POSTING_DOC_AMOUNTS
                       (POSTING_AMT_ID,
                        POSTING_ID,
                        DOC_ID,
                        AMOUNT_TYPE,
                        LOCATION,
                        ORG_UNIT,
                        SET_OF_BOOKS_ID,
                        TAX_CODE,
                        TAX_RATE,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        AMOUNT,
                        ACCOUNT_TYPE,
                        ACCOUNT_CODE,
                        --removed in accordance with the TCD for configurable VAT
                        --AMOUNT_VAT,
                        PRIM_CURRENCY_CODE)

        SELECT   IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                        POSTING_ID,
                   DOC_ID,
                   TYPE,
                   LOC,
                  (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                                   UNION ALL
                                                   SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                           ELSE
                                                           (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                           END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                                  ) WHERE LOCATION = LOC AND ROWNUM < 2 ) AS ORG_UNIT,
                  SET_OF_BOOKS_ID,
                  TAX_CODE,
                  TAX_RATE,
                  DEPT,
                  CLASS,
                  SUBCLASS,
                  GROUP_BY_1,
                  GROUP_BY_2,
                  GROUP_BY_3,
                  GROUP_BY_4,
                  GROUP_BY_5,
                  GROUP_BY_6,
                  GROUP_BY_7,
                  GROUP_BY_8,
                  EXTENDED_COST,
                  ACCOUNT_TYPE,
                  ACCOUNT_CODE,
                  --EXT_COST_VAT,
                  NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                    WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                            (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                          WAREHOUSE.WH
                                                     ELSE
                                                          (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                          WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                     END)
                                            FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                         ELSE
                                            MLS.LOCATION
                                         END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
         FROM
        (SELECT
                  POSTING_ID,
                   DOC_ID,
                   TYPE,
                   LOCATION LOC,
                   SET_OF_BOOKS_ID,
                   SYS_CURRENCY_CODE,
                   TAX_CODE,
                   TAX_RATE,
                    DEPT,
                    CLASS,
                    SUBCLASS,
                    GROUP_BY_1,
                    GROUP_BY_2,
                    GROUP_BY_3,
                    GROUP_BY_4,
                    GROUP_BY_5,
                    GROUP_BY_6,
                    GROUP_BY_7,
                    GROUP_BY_8,
                    round(Sum(EXTENDED_COST),decimal_digits) EXTENDED_COST,
                    ACCOUNT_TYPE,
                    ACCOUNT_CODE
                    --SUM(EXT_COST_VAT) EXT_COST_VAT,
        FROM     (SELECT IPD.POSTING_ID,
                             IPD.DOC_ID,
                             REIM_POSTING_SQL.AMT_RESOLUTION_ACTION
                             TYPE,
                             IPD.LOCATION,
                             IPD.SET_OF_BOOKS_ID,
                             IPD.SYS_CURRENCY_CODE,
                             DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IIDT.TAX_CODE) AS TAX_CODE,
                             --DECODE(IID.VAT_CODE,NULL,NULL, IID.VAT_RATE)   AS TAX_RATE,
                             DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IIDT.TAX_RATE) AS TAX_RATE,
                             IRA.ACTION    AS GROUP_BY_1,
                             IRA.REASON_CODE_ID   AS GROUP_BY_2,
                             no_value AS group_by_3,
                             no_value AS group_by_4,
                             NO_VALUE AS GROUP_BY_5,
                             NO_VALUE AS GROUP_BY_6,
                             NO_VALUE AS GROUP_BY_7,
                             NO_VALUE AS GROUP_BY_8,
                             IM.DEPT,
                             IM.CLASS,
                             NULL AS SUBCLASS,
                             CALC_RESOLUTION_AMOUNT(IRA.ROWID,  IID.ROWID) AS EXTENDED_COST,
                             REASON_CODE_ACTIONS         ACCOUNT_TYPE,
                             IRTCM.TRAN_CODE             ACCOUNT_CODE,
                             ipd.decimal_digits
                    FROM     IM_POSTING_DOC IPD,
                             IM_RESOLUTION_ACTION IRA,
                             IM_REASON_TRAN_CODE_MAP IRTCM,
                             IM_INVOICE_DETAIL IID,
                                IM_INVOICE_DETAIL_TAX IIDT,
                             ITEM_MASTER IM
                    WHERE  IPD.POSTING_ID = I_POSTING_ID
                             AND IPD.RESOLUTION_COUNT > 0

                             AND IID.DOC_ID = IPD.DOC_ID
                             AND IRA.DOC_ID = IPD.DOC_ID
                             AND IRA.ITEM = IID.ITEM
                             AND IRTCM.REASON_CODE_ID  = IRA.REASON_CODE_ID
                             AND IRTCM.SET_OF_BOOKS_ID = IPD.SET_OF_BOOKS_ID
                             AND IM.ITEM = IID.ITEM
                                AND IID.ITEM = IIDT.ITEM (+)
                             AND IID.DOC_ID = IIDT.DOC_ID (+)
                             AND IS_ACCOUNTABLE_RESOLUTION(IRA.ACTION) = YN_YES)
        GROUP BY POSTING_ID,DOC_ID,TYPE,LOCATION,SET_OF_BOOKS_ID,SYS_CURRENCY_CODE,TAX_CODE,
                   TAX_RATE,DEPT, CLASS, SUBCLASS, GROUP_BY_1,
                   GROUP_BY_2, GROUP_BY_3, GROUP_BY_4,
                   GROUP_BY_5, GROUP_BY_6, GROUP_BY_7,
                   GROUP_BY_8, DECIMAL_DIGITS,ACCOUNT_TYPE, ACCOUNT_CODE)
                   ;

        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END CALC_INVOICE_RESOLUTION_COSTS;



    /************************************************************
     * Calculates the merchandise portion of documents.
     * Mainly inserts into IM_POSTING_DOC_AMOUNTS
     ************************************************************/
    FUNCTION CALC_DOC_MERCH_COST
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_DOC_MERCH_COST';
        L_PRORATE_BY_TAX_ID IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

    BEGIN
        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
          INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

        INSERT INTO IM_POSTING_DOC_AMOUNTS IPDA
                     (  POSTING_AMT_ID,
                        POSTING_ID,
                        DOC_ID,
                        AMOUNT_TYPE,
                        LOCATION,
                        ORG_UNIT,
                        SET_OF_BOOKS_ID,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        AMOUNT,
                        ACCOUNT_TYPE,
                        ACCOUNT_CODE,
                        --removed in accordance with the TCD for configurable VAT
                        --AMOUNT_VAT,
                        PRIM_CURRENCY_CODE)


        SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOC,
                (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                                   UNION ALL
                                                   SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                           ELSE
                                                           (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                           END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                                  ) WHERE LOCATION = LOC AND ROWNUM < 2 ) AS ORG_UNIT,
                SET_OF_BOOKS_ID,
                DEPT,
                CLASS,
                SUBCLASS,
                TAX_CODE,
                TAX_RATE,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE,
                --removed in accordance with the TCD for configurable VAT
                --AMOUNT_VAT,
                NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
        FROM
        (SELECT  POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION LOC,
                SET_OF_BOOKS_ID,
                SYS_CURRENCY_CODE,
                DEPT,
                CLASS,
                SUBCLASS,
                TAX_CODE,
                TAX_RATE,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                SUM(AMOUNT) AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE
                --removed in accordance with the TCD for configurable VAT
                --SUM(AMOUNT_VAT)

        FROM (
                  /*
                  * The following sub queries will take care of all cases when a VAT basis exists,
                  * and the document has Details.
                  * The query to Calculate amount from VAT basis for Header Only invoices (i.e. no details)
                  * is done in a separate query at the end.
                  */

                  -- With Details (Invoices)
                  SELECT IPD.POSTING_ID,
                          IPD.DOC_ID,
                          REIM_POSTING_SQL.AMT_MERCHANDISE_COST  AS  AMOUNT_TYPE,
                          IPD.LOCATION,
                          IPD.SET_OF_BOOKS_ID,
                          IPD.SYS_CURRENCY_CODE,
                          IM.DEPT AS DEPT,
                          IM.CLASS AS CLASS,
                          NULL AS SUBCLASS,
                          --IID.VAT_CODE AS TAX_CODE,
                          DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IIDT.TAX_CODE)  AS TAX_CODE,
                          DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IIDT.TAX_RATE)  AS TAX_RATE,
                          --DECODE(IID.VAT_CODE,NULL,NULL,IID.VAT_RATE) AS TAX_RATE,
                          /**** *CUSTOM GROUPING *****/
                          NO_VALUE AS GROUP_BY_1,
                          NO_VALUE AS GROUP_BY_2,
                          NO_VALUE                AS  GROUP_BY_3,
                          NO_VALUE                AS  GROUP_BY_4,
                          NO_VALUE AS GROUP_BY_5,
                          NO_VALUE AS GROUP_BY_6,
                          NO_VALUE AS GROUP_BY_7,
                          NO_VALUE AS GROUP_BY_8,
                          /**** *CUSTOM GROUPING *****/
                          ROUND(IID.UNIT_COST * IID.QTY , ipd.decimal_digits) AS AMOUNT,
                          BASIC_TRANSACTIONS ACCOUNT_TYPE,
                          UNMATCHED_RECEIPT ACCOUNT_CODE
                          --removed in accordance with configurable VAT
                          --ROUND(IID.UNIT_COST * IID.QTY * IID.VAT_RATE / 100 , ipd.decimal_digits) AS AMOUNT_VAT
                  FROM    IM_POSTING_DOC IPD,
                          IM_DOC_HEAD IDH,
                          IM_INVOICE_DETAIL IID,
              IM_INVOICE_DETAIL_TAX IIDT,
                          ITEM_MASTER IM
                  WHERE    IPD.POSTING_ID = I_POSTING_ID

                          AND IDH.DOC_ID = IPD.DOC_ID
                          AND IID.DOC_ID = IPD.DOC_ID
                          AND IPD.CALC_BASIS = REIM_POSTING_SQL.BASIS_DETAILS
                          AND IID.ITEM = IM.ITEM
              AND IID.DOC_ID = IIDT.DOC_ID (+)
                          AND IID.ITEM = IIDT.ITEM (+)
                  UNION ALL
                  -- With Details (Non Invoices)
                  SELECT  IPD.POSTING_ID,
                          IPD.DOC_ID,
                          REIM_POSTING_SQL.AMT_MERCHANDISE_COST  AS AMOUNT_TYPE,
                          IPD.LOCATION,
                          IPD.SET_OF_BOOKS_ID,
                          IPD.SYS_CURRENCY_CODE,
                          IM.DEPT AS DEPT,
                          IM.CLASS AS CLASS,
                          NULL AS SUBCLASS,
                          DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDDRCT.TAX_CODE)  AS TAX_CODE,
                          --DECODE(IDDRC.VAT_CODE,NULL,NULL,IDDRC.VAT_RATE) AS TAX_RATE,
                          DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDDRCT.TAX_RATE)  AS TAX_RATE,
                          /**** *CUSTOM GROUPING *****/
                          NO_VALUE AS GROUP_BY_1,
                          NO_VALUE AS GROUP_BY_2,
                          NO_VALUE                AS  GROUP_BY_3,
                          NO_VALUE                AS  GROUP_BY_4,

                          NO_VALUE AS GROUP_BY_5,
                          NO_VALUE AS GROUP_BY_6,
                          NO_VALUE AS GROUP_BY_7,
                          NO_VALUE AS GROUP_BY_8,
                          /**** *CUSTOM GROUPING *****/
                          ROUND(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY, ipd.decimal_digits) AS AMOUNT,
                          BASIC_TRANSACTIONS ACCOUNT_TYPE,
                          Decode(IDH.TYPE,DOC_TYPE_CREDIT_NOTE,CREDIT_NOTE,UNMATCHED_RECEIPT) ACCOUNT_CODE
                          --removed in accordance with configurable VAT
                          --ROUND(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY * IDDRC.VAT_RATE / 100, ipd.decimal_digits)  AS AMOUNT_VAT
                  FROM     IM_POSTING_DOC IPD,
                          IM_DOC_HEAD IDH,
                          IM_DOC_DETAIL_REASON_CODES  IDDRC,
              IM_DOC_DETAIL_RC_TAX IDDRCT,
                          ITEM_MASTER IM
                  WHERE    IPD.POSTING_ID = I_POSTING_ID

                          AND IDH.DOC_ID = IPD.DOC_ID
                          AND IDDRC.DOC_ID = IPD.DOC_ID
              AND IDDRC.IM_DOC_DETAIL_REASON_CODES_ID = IDDRCT.IM_DOC_DETAIL_REASON_CODES_ID (+)
                          AND IPD.CALC_BASIS = REIM_POSTING_SQL.BASIS_DETAILS
                          AND IDDRC.ITEM = IM.ITEM
                          AND IDDRC.STATUS = REIM_POSTING_SQL.DOC_STATUS_APPROVED
                          AND (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0)
                  UNION ALL
                  -- Receipt write-offs
                  SELECT  IPD.POSTING_ID,
                          IPD.DOC_ID,
                          REIM_POSTING_SQL.AMT_MERCHANDISE_COST  AS AMOUNT_TYPE,
                          S.TO_LOC LOCATION,
                          MLS.SET_OF_BOOKS_ID,
                          IPD.SYS_CURRENCY_CODE,
                          IPDDC.DEPT AS DEPT,
                          IPDDC.CLASS AS CLASS,
                          NULL AS SUBCLASS,
                          --IPDDC.VAT_CODE and IPDDC.TAX_RATE are being
                          --removed temporarily in antocipation of configurable VAT
                          --IPDDC.VAT_CODE,
                          --DECODE(IPDDC.VAT_CODE,NULL,NULL,IPDDC.VAT_RATE) AS VAT_RATE,
                          NULL AS TAX_CODE,
                          NULL AS TAX_RATE,
                          TO_CHAR(S.SHIPMENT)                AS GROUP_BY_1,
                          TO_CHAR(S.RECEIVE_DATE, 'YYYY-MM-DD')            AS GROUP_BY_2,
                          TO_CHAR(S.ORDER_NO)  AS GROUP_BY_3,
                          S.TO_LOC_TYPE||'^'||TO_CHAR(S.TO_LOC)                  AS GROUP_BY_4,
                          'SUPP'||'^'||DECODE(SUPS.SUPPLIER_PARENT, NULL,SUPS.SUPPLIER,SUPS.SUPPLIER_PARENT ) AS GROUP_BY_5,
                          IPDDC.CURRENCY_CODE||'^'||TO_CHAR(IPDDC.EXCHANGE_RATE)||'^'||IPDDC.EXCHANGE_TYPE  AS GROUP_BY_6,
                          IPDDC.PRIM_CURRENCY_CODE AS GROUP_BY_7,
                          NO_VALUE AS GROUP_BY_8,
                          ROUND(IPDDC.EXT_COST, IPD.DECIMAL_DIGITS) AS AMOUNT,
                          BASIC_TRANSACTIONS ACCOUNT_TYPE,
                          UNMATCHED_RECEIPT ACCOUNT_CODE
                          --removed in accordance with TCD for configurable VAT
                          --0 AS AMOUNT_VAT -- this was being calculated from IPDDC.VAT_AMOUNT
                  FROM    IM_POSTING_DOC IPD,
                          IM_POSTING_DOC_DEPTCLASS IPDDC,
                          SHIPMENT S,
                          ORDHEAD OH,
                                SUPS,
                                MV_LOC_SOB MLS,
                                (SELECT ORDER_NO,
                                  MIN(LOCATION) LOCATION
                             FROM ORDLOC
                            GROUP BY ORDER_NO) OL
                  WHERE   IPD.POSTING_ID = I_POSTING_ID

                          AND IPDDC.POSTING_ID = IPD.POSTING_ID
                          AND IPDDC.DOC_ID = IPD.DOC_ID
                          AND IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF
                          AND S.SHIPMENT = IPDDC.MATCH_ID
                          AND OH.ORDER_NO = S.ORDER_NO
                                AND SUPS.SUPPLIER = OH.SUPPLIER
                          AND OL.ORDER_NO = OH.ORDER_NO
                          AND MLS.LOCATION = OL.LOCATION
                          AND MLS.LOCATION_TYPE IN (REIM_CONSTANTS.LOC_TYPE_WH, REIM_CONSTANTS.LOC_TYPE_STORE)

                  UNION ALL
                 --Credit notes without details and tax info created through group entry screen - bug 11723032
                  SELECT  IPD.POSTING_ID,
                          IPD.DOC_ID,
                          REIM_POSTING_SQL.AMT_MERCHANDISE_COST  AS AMOUNT_TYPE,
                          IPD.LOCATION,
                          IPD.SET_OF_BOOKS_ID,
                          IPD.SYS_CURRENCY_CODE,
                          NULL AS DEPT,
                          NULL AS CLASS,
                          NULL AS SUBCLASS,
                          NULL AS TAX_CODE,
                          NULL AS TAX_RATE,
                          /**** *CUSTOM GROUPING *****/
                          NO_VALUE AS GROUP_BY_1,
                          NO_VALUE AS GROUP_BY_2,
                          NO_VALUE AS  GROUP_BY_3,
                          NO_VALUE AS  GROUP_BY_4,
                          NO_VALUE AS GROUP_BY_5,
                          NO_VALUE AS GROUP_BY_6,
                          NO_VALUE AS GROUP_BY_7,
                          NO_VALUE AS GROUP_BY_8,
                          ROUND(IDH.TOTAL_COST - NVL((SELECT SUM(IDNM.NON_MERCH_AMT)
                                           FROM   IM_DOC_NON_MERCH IDNM
                                           WHERE  IDNM.DOC_ID = IDH.DOC_ID),0), ipd.decimal_digits) AS AMOUNT,
                          BASIC_TRANSACTIONS ACCOUNT_TYPE,
                         CASE
                         WHEN  IDH.ORDER_NO IS NOT NULL
                              THEN CREDIT_NOTE
                         WHEN  IDH.ORDER_NO IS  NULL
                              THEN CREDIT_NOTE_NON_DYNAMIC
                         END AS ACCOUNT_CODE
                  FROM    IM_POSTING_DOC IPD,
                          IM_DOC_HEAD IDH

                  WHERE   IPD.POSTING_ID = I_POSTING_ID

                          AND IDH.DOC_ID = IPD.DOC_ID
                          AND NOT EXISTS(SELECT 'X' FROM IM_DOC_DETAIL_REASON_CODES WHERE DOC_ID = IDH.DOC_ID)
                          AND NOT EXISTS(SELECT 'X' FROM IM_DOC_TAX WHERE DOC_ID = IDH.DOC_ID)
                          AND IDH.STATUS = DOC_STATUS_APPROVED
                          AND IDH.TYPE = DOC_TYPE_CREDIT_NOTE
                          AND (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0)
                 UNION ALL
                 --Credit notes without details but with tax info created through group entry screen
                  SELECT  IPD.POSTING_ID,
                          IPD.DOC_ID,
                          REIM_POSTING_SQL.AMT_MERCHANDISE_COST  AS AMOUNT_TYPE,
                          IPD.LOCATION,
                          IPD.SET_OF_BOOKS_ID,
                          IPD.SYS_CURRENCY_CODE,
                          NULL AS DEPT,
                          NULL AS CLASS,
                          NULL AS SUBCLASS,
                          DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDT.TAX_CODE) AS TAX_CODE,
                          DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDT.TAX_RATE) AS TAX_RATE,
                          /**** *CUSTOM GROUPING *****/
                          NO_VALUE AS GROUP_BY_1,
                          NO_VALUE AS GROUP_BY_2,
                          NO_VALUE AS  GROUP_BY_3,
                          NO_VALUE AS  GROUP_BY_4,
                          NO_VALUE AS GROUP_BY_5,
                          NO_VALUE AS GROUP_BY_6,
                          NO_VALUE AS GROUP_BY_7,
                          NO_VALUE AS GROUP_BY_8,
                          ROUND(IDT.TAX_BASIS - NVL((SELECT SUM(IDNMT.TAX_BASIS)
                                                        FROM   IM_DOC_NON_MERCH_TAX IDNMT
                                                        WHERE  IDNMT.DOC_ID = IDH.DOC_ID
                                                        AND IDNMT.TAX_CODE = IDT.TAX_CODE),0), ipd.decimal_digits) AS AMOUNT,
                          BASIC_TRANSACTIONS ACCOUNT_TYPE,
                         CASE
                         WHEN  IDH.ORDER_NO IS NOT NULL
                              THEN CREDIT_NOTE
                         WHEN  IDH.ORDER_NO IS  NULL
                              THEN CREDIT_NOTE_NON_DYNAMIC
                         END AS ACCOUNT_CODE
                  FROM    IM_POSTING_DOC IPD,
                          IM_DOC_HEAD IDH,
                          IM_DOC_TAX IDT

                  WHERE   IPD.POSTING_ID = I_POSTING_ID

                          AND IDH.DOC_ID = IPD.DOC_ID
                          AND IDH.DOC_ID = IDT.DOC_ID
                          AND NOT EXISTS(SELECT 'X' FROM IM_DOC_DETAIL_REASON_CODES WHERE DOC_ID = IDH.DOC_ID)
                          AND IDH.STATUS = DOC_STATUS_APPROVED
                          AND IDH.TYPE = DOC_TYPE_CREDIT_NOTE
                          AND (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0)

                          ) GROUP BY POSTING_ID,
                            DOC_ID,
                    AMOUNT_TYPE,
                LOCATION,
                SET_OF_BOOKS_ID,
                SYS_CURRENCY_CODE,
                DEPT,
             CLASS,
                SUBCLASS,
                TAX_CODE,
                TAX_RATE,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                ACCOUNT_TYPE,
                ACCOUNT_CODE);

       -- To accomodate Header only invoices with tax Basis and no details
       L_PRORATE_BY_TAX_ID := REIM_PRORATE_SQL.GET_NEXT_PRORATE_WORKSPACE_ID;

       INSERT INTO IM_PRORATE_WORKSPACE
           (PRORATE_WORKSPACE_ID,
            PRORATE_GROUP_KEY,
            DECIMAL_SCALE,
            AMOUNT_TO_PRORATE_1,
            SHARE_IN_GROUP_1,
            REF_ROWID_1,
            REF_ROWID_2)

        SELECT *
        FROM   (SELECT L_PRORATE_BY_TAX_ID                    AS PRORATE_WORKSPACE_ID,
               IPD.POSTING_ID
               ||'^'
               ||IPD.DOC_ID                                   AS PRORATE_GROUP_KEY,
               IPD.DECIMAL_DIGITS                             AS DECIMAL_DIGITS,
               ROUND(IDH.TOTAL_COST - NVL((SELECT SUM(IDNM.NON_MERCH_AMT)
                                           FROM   IM_DOC_NON_MERCH IDNM
                                           WHERE  IDNM.DOC_ID = IDT.DOC_ID),0),
                                          IPD.DECIMAL_DIGITS) AS AMOUNT_TO_PRORATE_1,--invoice amount
               ROUND(IDT.TAX_AMOUNT - NVL((SELECT SUM(IDNMT.TAX_AMOUNT)
                                          FROM   IM_DOC_NON_MERCH_TAX IDNMT
                                          WHERE  IDNMT.DOC_ID = IDT.DOC_ID
                                                AND IDNMT.TAX_CODE = IDT.TAX_CODE),
                                       0),IPD.DECIMAL_DIGITS) AS SHARE_IN_GROUP_1,
                  IPD.ROWID                         AS REF_ROWID_1,
                  IDT.ROWID                         AS REF_ROWID_2
          FROM   IM_POSTING_DOC IPD,
               IM_DOC_HEAD IDH,
               IM_DOC_TAX IDT
          WHERE  IPD.POSTING_ID = I_POSTING_ID

                AND IDH.DOC_ID = IPD.DOC_ID
                AND IDT.DOC_ID = IPD.DOC_ID
                AND IDH.TYPE = DOC_TYPE_MERCH_INVOICE
                AND IPD.CALC_BASIS = REIM_POSTING_SQL.BASIS_HEADER
                AND IPD.DOC_TAX_COUNT > 0
                AND (IDH.DEAL_ID IS NULL
                     OR IDH.DEAL_ID = 0))
                WHERE  SHARE_IN_GROUP_1 <> 0;

          REIM_PRORATE_SQL.DO_PRORATION(L_PRORATE_BY_TAX_ID);

          INSERT INTO IM_POSTING_DOC_AMOUNTS
           (POSTING_AMT_ID,
            POSTING_ID,
            DOC_ID,
            AMOUNT_TYPE,
            LOCATION,
            ORG_UNIT,
            SET_OF_BOOKS_ID,
            TAX_CODE,
            TAX_RATE,
            DEPT,
            CLASS,
            SUBCLASS,
            GROUP_BY_1,
            GROUP_BY_2,
            GROUP_BY_3,
            GROUP_BY_4,
            GROUP_BY_5,
            GROUP_BY_6,
            GROUP_BY_7,
            GROUP_BY_8,
            AMOUNT,
            ACCOUNT_TYPE,
            ACCOUNT_CODE,
            --remved in accordance with the TCD for configurable VAT
            --AMOUNT_VAT,
            PRIM_CURRENCY_CODE)
        -- Any document which has a tax Basis but NOT any invoice details nor reason codes:
        -- Document Value = document total cost without non-merch
        -- Document tax Basis is prorated w.r.t the Non MRCHI amount of the Doc
        SELECT IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
               IPD.POSTING_ID,
               IPD.DOC_ID,
               REIM_POSTING_SQL.AMT_MERCHANDISE_COST AS AMOUNT_TYPE,
               IPD.LOCATION,
              (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = IPD.LOCATION AND ROWNUM < 2 ) AS ORG_UNIT,
              IPD.SET_OF_BOOKS_ID,
         DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDV.TAX_CODE)  AS TAX_CODE,
         DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDV.TAX_RATE)  AS TAX_RATE,
         NULL                                                                      AS DEPT,
         NULL                                                                      AS CLASS,
         NULL                                                                      AS SUBCLASS,
         NO_VALUE                                                                  AS GROUP_BY_1,
         NO_VALUE                                                                  AS GROUP_BY_2,
         NO_VALUE                                                                  AS GROUP_BY_3,
         NO_VALUE                                                                  AS GROUP_BY_4,
         NO_VALUE                                                                  AS GROUP_BY_5,
         NO_VALUE                                                                  AS GROUP_BY_6,
         NO_VALUE                                                                  AS GROUP_BY_7,
         NO_VALUE                                                                  AS GROUP_BY_8,
         ROUND(IPW.RESULT_PRORATED_AMOUNT_1,IPD.DECIMAL_DIGITS)                    AS AMOUNT,
         BASIC_TRANSACTIONS    ACCOUNT_TYPE,
         UNMATCHED_RECEIPT     ACCOUNT_CODE,
         --removed in accordance with TCD for configurable VAT
         --ROUND((IPW.RESULT_PRORATED_AMOUNT_1 * NVL(IDV.VAT_RATE,0) / 100),
         --    IPD.DECIMAL_DIGITS) AS AMOUNT_VAT,
         NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = IPD.LOCATION) AND ROWNUM < 2)) PRIM_CURRENCY_CODE

        FROM   IM_POSTING_DOC IPD,
               IM_DOC_HEAD IDH,
               IM_DOC_TAX IDV,
               IM_PRORATE_WORKSPACE IPW
        WHERE  IPD.POSTING_ID = I_POSTING_ID
             AND IPW.PRORATE_WORKSPACE_ID = L_PRORATE_BY_TAX_ID -- the prorate workspace ID

             AND IPD.ROWID = IPW.REF_ROWID_1
             AND IDV.ROWID = IPW.REF_ROWID_2
             AND IDH.DOC_ID = IPD.DOC_ID
             AND IDV.DOC_ID = IPD.DOC_ID
             AND (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0);



            --remove the proration work space
        REIM_PRORATE_SQL.END_PRORATION(L_PRORATE_BY_TAX_ID);

        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END;


/*******************

Calculates document TOTAL TAX.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL


*******************/

    FUNCTION CALC_DOCUMENT_TAX
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE,
            I_ISPREPAY IN VARCHAR2)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_DOCUMENT_TAX';
        L_PRORATE_TAX_ID IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;

        STATUS NUMBER := SUCCESS;



    BEGIN


    /*
    *For Header only invoices
    ** tax amount for each tax code should be prorated accross departments
    */


     L_PRORATE_TAX_ID := REIM_PRORATE_SQL.GET_NEXT_PRORATE_WORKSPACE_ID;

     INSERT INTO IM_PRORATE_WORKSPACE(PRORATE_WORKSPACE_ID,
                                         PRORATE_GROUP_KEY,
                                         DECIMAL_SCALE,
                                         AMOUNT_TO_PRORATE_1,
                                         SHARE_IN_GROUP_1,
                                         AMOUNT_TO_PRORATE_2,
                                         SHARE_IN_GROUP_2,
                                         REF_ROWID_1,
                                         REF_ROWID_2
                                         )

        SELECT L_PRORATE_TAX_ID   AS PRORATE_WORKSPACE_ID,
               'byTax'||'^'||IPD.POSTING_ID||'^'||IPDDC.MATCH_ID||'^'||IPD.DOC_ID ||'^'||IDT.TAX_CODE  AS PRORATE_GROUP_KEY,
               IPD.DECIMAL_DIGITS AS DECIMAL_DIGITS,
               NVL((IDT.TAX_AMOUNT-IDNMT.TAX_AMOUNT),IDT.TAX_AMOUNT) AS AMOUNT_TO_PRORATE_1,
               IPDDC.EXT_COST AS SHARE_IN_GROUP_1,
               NULL AS AMOUNT_TO_PRORATE_2,
               NULL AS SHARE_IN_GROUP_2,
               IDT.ROWID,
               IPDDC.ROWID
        FROM IM_POSTING_DOC IPD,
             IM_DOC_TAX IDT,
			 IM_DOC_NON_MERCH_TAX IDNMT,
             IM_POSTING_DOC_DEPTCLASS IPDDC
         WHERE IPD.POSTING_ID = I_POSTING_ID
               AND IPDDC.POSTING_ID = IPD.POSTING_ID
               AND IPD.CALC_BASIS = BASIS_HEADER
               AND IDT.DOC_ID = IPD.DOC_ID
               AND IPDDC.DOC_ID = IDT.DOC_ID
			   AND IDT.DOC_ID = IDNMT.DOC_ID(+)
			   AND IDT.TAX_CODE = IDNMT.TAX_CODE(+)
			   AND NVL((IDT.TAX_BASIS-IDNMT.TAX_BASIS),IDT.TAX_BASIS) <> 0;




     REIM_PRORATE_SQL.DO_PRORATION(L_PRORATE_TAX_ID);


     INSERT INTO IM_POSTING_DOC_AMOUNTS
                     (  POSTING_AMT_ID,
                        POSTING_ID,
                        DOC_ID,
                        AMOUNT_TYPE,
                        LOCATION,
                        ORG_UNIT,
                        SET_OF_BOOKS_ID,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        AMOUNT,
                        ACCOUNT_TYPE,
                        ACCOUNT_CODE,
                        PRIM_CURRENCY_CODE)


    SELECT IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
           POSTING_ID,
           DOC_ID,
           DECODE(I_ISPREPAY,YN_YES,AMT_PPA_COST,AMT_TAX) AMOUNT_TYPE,
           LOC,
           (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
           SET_OF_BOOKS_ID,
           DEPT,
           CLASS,
           SUBCLASS,
           DECODE(I_ISPREPAY,YN_YES,NULL,TAX_CODE),
           DECODE(I_ISPREPAY,YN_YES,NULL,TAX_RATE),
           NO_VALUE,
           NO_VALUE,
           NO_VALUE,
           NO_VALUE,
           NO_VALUE,
           NO_VALUE,
           NO_VALUE,
           DECODE(I_ISPREPAY,YN_YES,YN_YES,NO_VALUE),
           AMOUNT,
           BASIC_TRANSACTIONS     ACCOUNT_TYPE,
           DECODE(I_ISPREPAY,YN_YES,PRE_PAID_ASSET,TAX) ACCOUNT_CODE,
           NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE

    FROM

         (
         SELECT  IPD.POSTING_ID               POSTING_ID,
                 IPD.DOC_ID                   DOC_ID,
                 IPD.LOCATION                 LOC,
                 IPD.SET_OF_BOOKS_ID          SET_OF_BOOKS_ID,
                 IPD.SYS_CURRENCY_CODE        SYS_CURRENCY_CODE,
                 IPDDC.DEPT                   DEPT,
                 IPDDC.CLASS                  CLASS,
                 NULL                         SUBCLASS,
                 IDT.TAX_CODE                 TAX_CODE,
                 IDT.TAX_RATE                 TAX_RATE,
                 IPW.RESULT_PRORATED_AMOUNT_1 AMOUNT

         FROM  IM_POSTING_DOC IPD,
               IM_PRORATE_WORKSPACE IPW,
               IM_DOC_TAX IDT,
               IM_POSTING_DOC_DEPTCLASS IPDDC

               WHERE IPD.POSTING_ID = I_POSTING_ID
               AND IPD.MATCH_TYPE = MATCH_TYPE_SUMMARY
               AND IPD.POSTING_ID = IPDDC.POSTING_ID

               AND IDT.DOC_ID = IPD.DOC_ID
               AND IPDDC.DOC_ID = IDT.DOC_ID
               AND IPW.REF_ROWID_1 = IDT.ROWID
               AND IPW.REF_ROWID_2 = IPDDC.ROWID
               AND IPW.PRORATE_WORKSPACE_ID = L_PRORATE_TAX_ID

               UNION ALL
              -- SUBQUERY FOR  HEADER ONLY RESOLUTION DOCUMENTS
               SELECT  IPD.POSTING_ID         POSTING_ID,
                 IPD.DOC_ID                   DOC_ID,
                 IPD.LOCATION                 LOC,
                 IPD.SET_OF_BOOKS_ID          SET_OF_BOOKS_ID,
                 IPD.SYS_CURRENCY_CODE        SYS_CURRENCY_CODE,
                 NULL                         DEPT,
                 NULL                         CLASS,
                 NULL                         SUBCLASS,
                 IDT.TAX_CODE                 TAX_CODE,
                 IDT.TAX_RATE                 TAX_RATE,
                 (IDT.TAX_AMOUNT - NVL((SELECT SUM(TAX_AMOUNT) FROM IM_DOC_NON_MERCH_TAX IDNX WHERE IDNX.DOC_ID = IDT.DOC_ID
                                                                                  AND   IDNX.TAX_CODE = IDT.TAX_CODE
                                                                                  AND   IDNX.TAX_RATE = IDT.TAX_RATE),0)) AS AMOUNT

               FROM  IM_POSTING_DOC IPD,
                     IM_DOC_TAX IDT,
                     IM_DOC_HEAD IDH

                     WHERE IPD.POSTING_ID = I_POSTING_ID
                     AND   IPD.CALC_BASIS = BASIS_HEADER

                     AND   IDT.DOC_ID = IPD.DOC_ID
                     AND   IDH.DOC_ID = IDT.DOC_ID
                     AND   IDH.TYPE NOT IN(DOC_TYPE_MERCH_INVOICE,DOC_TYPE_NONMERCH_INVOICE)
                     AND   (IDH.DEAL_ID = 0 OR IDH.DEAL_ID IS NULL)
                UNION ALL
                -- SUBQUERY FOR HEADER ONLY INVOICES PREPAYMENT
                SELECT  IPD.POSTING_ID               POSTING_ID,
                 IPD.DOC_ID                   DOC_ID,
                 IPD.LOCATION                 LOC,
                 IPD.SET_OF_BOOKS_ID          SET_OF_BOOKS_ID,
                 IPD.SYS_CURRENCY_CODE        SYS_CURRENCY_CODE,
                 NULL                   DEPT,
                 NULL                  CLASS,
                 NULL                         SUBCLASS,
                 IDT.TAX_CODE                 TAX_CODE,
                 IDT.TAX_RATE                 TAX_RATE,
                 (IDT.TAX_AMOUNT - NVL((SELECT SUM(TAX_AMOUNT) FROM IM_DOC_NON_MERCH_TAX IDNX WHERE IDNX.DOC_ID = IDT.DOC_ID
                                                                                  AND   IDNX.TAX_CODE = IDT.TAX_CODE
                                                                                  AND   IDNX.TAX_RATE = IDT.TAX_RATE),0)) AS AMOUNT

               FROM  IM_POSTING_DOC IPD,
                     IM_DOC_TAX IDT,
                     IM_DOC_HEAD IDH

                     WHERE IPD.POSTING_ID = I_POSTING_ID
                     AND   IPD.CALC_BASIS = BASIS_HEADER

                     AND   IDT.DOC_ID = IPD.DOC_ID
                     AND   IDH.DOC_ID = IDT.DOC_ID
                     AND   IDH.STATUS <> REIM_POSTING_SQL.DOC_STATUS_MATCHED
                     AND   IDH.TYPE = DOC_TYPE_MERCH_INVOICE

                );


    REIM_PRORATE_SQL.END_PRORATION(L_PRORATE_TAX_ID);


    /*PRORATION IS NOT REQUIRED FOR INVOICE WITH DETAILS*/

     INSERT INTO IM_POSTING_DOC_AMOUNTS
                     (  POSTING_AMT_ID,
                        POSTING_ID,
                        DOC_ID,
                        AMOUNT_TYPE,
                        LOCATION,
                        ORG_UNIT,
                        SET_OF_BOOKS_ID,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        AMOUNT,
                        ACCOUNT_TYPE,
                        ACCOUNT_CODE,
                        PRIM_CURRENCY_CODE)
    SELECT IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
           POSTING_ID,
           DOC_ID,
           DECODE(I_ISPREPAY,YN_YES,AMT_PPA_COST,AMOUNT_TYPE) AMOUNT_TYPE,
           LOC,
           (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
           SET_OF_BOOKS_ID,
           DEPT,
           CLASS,
           SUBCLASS,
           DECODE(I_ISPREPAY,YN_YES,NULL,TAX_CODE),
           DECODE(I_ISPREPAY,YN_YES,NULL,TAX_RATE),
           NO_VALUE GROUP_BY_1,
           NO_VALUE GROUP_BY_2,
           NO_VALUE GROUP_BY_3,
           NO_VALUE GROUP_BY_4,
           NO_VALUE GROUP_BY_5,
           NO_VALUE GROUP_BY_6,
           NO_VALUE GROUP_BY_7,
           DECODE(I_ISPREPAY,YN_YES,YN_YES,NO_VALUE) GROUP_BY_8,
           AMOUNT,
           BASIC_TRANSACTIONS   ACCOUNT_TYPE,
           DECODE(I_ISPREPAY,YN_YES,PRE_PAID_ASSET,ACCOUNT_CODE) ACCOUNT_CODE,
           NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION =  LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE

           FROM
           (
            SELECT   POSTING_ID,
                     DOC_ID,
                     AMOUNT_TYPE,
                 LOCATION LOC,
                 SET_OF_BOOKS_ID,
                 DEPT,
                 CLASS,
                 SUBCLASS,
                 TAX_CODE,
                 TAX_RATE,
                 ACCOUNT_CODE,
                 SYS_CURRENCY_CODE,
                 ROUND(SUM(AMOUNT),DECIMAL_DIGITS) AMOUNT
            FROM(
                        --tax component of invoices (dynamic)
                                SELECT IPD.POSTING_ID POSTING_ID,
                                       IPD.DOC_ID DOC_ID,
                                       DECODE(NVL(IPD.SYS_TAX_IND, YN_NO),
                                              SYS_TAX_IND_ACQ_VAT, AMT_ACQ_VAT,
                                              AMT_TAX) AMOUNT_TYPE,
                                       IPD.LOCATION LOCATION,
                                       IPD.SET_OF_BOOKS_ID,
                                       IM.DEPT DEPT,
                                       IM.CLASS CLASS,
                                       IM.SUBCLASS SUBCLASS,
                                       IIDT.TAX_CODE TAX_CODE,
                                       IIDT.TAX_RATE TAX_RATE,
                                       DECODE(NVL(IPD.SYS_TAX_IND, YN_NO),
                                              SYS_TAX_IND_ACQ_VAT, TAX_ACQUISITION,
                                              TAX) ACCOUNT_CODE,
                                       IPD.SYS_CURRENCY_CODE SYS_CURRENCY_CODE,
                                       IPD.DECIMAL_DIGITS,
                                       IIDT.TAX_AMOUNT AMOUNT
                                    FROM  IM_POSTING_DOC IPD,
                                          IM_INVOICE_DETAIL IID,
                                          ITEM_MASTER IM,
                                          IM_INVOICE_DETAIL_TAX IIDT
                                    WHERE IPD.POSTING_ID = I_POSTING_ID
                                        AND IPD.CALC_BASIS = BASIS_DETAILS

                                        AND (I_ISPREPAY = YN_NO OR NVL(IPD.SYS_TAX_IND, YN_NO) = YN_NO)
                                        AND IID.DOC_ID = IPD.DOC_ID
                                        AND IIDT.DOC_ID = IID.DOC_ID
                                        AND IIDT.ITEM = IID.ITEM
                                        AND IM.ITEM = IID.ITEM
                                        AND (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)

                        UNION ALL

                                --reverse vat component of invoices (dynamic)
                                SELECT IPD.POSTING_ID POSTING_ID,
                                       IPD.DOC_ID DOC_ID,
                                       AMT_REV_CHRG_VAT AMOUNT_TYPE,
                                       IPD.LOCATION LOCATION,
                                       IPD.SET_OF_BOOKS_ID,
                                       IM.DEPT DEPT,
                                       IM.CLASS CLASS,
                                       IM.SUBCLASS SUBCLASS,
                                       VI.VAT_CODE TAX_CODE,
                                       VI.VAT_RATE TAX_RATE,
                                       TAX_REVERSE_CHARGE ACCOUNT_CODE,
                                       IPD.SYS_CURRENCY_CODE SYS_CURRENCY_CODE,
                                       IPD.DECIMAL_DIGITS,
                                       IIDT.TAX_BASIS * (VI.VAT_RATE/100) AMOUNT
                                    FROM  IM_POSTING_DOC IPD,
                                          IM_INVOICE_DETAIL_TAX IIDT,
                                          ITEM_MASTER IM,
                                          (SELECT WH LOCATION,VAT_REGION FROM WH
                                           UNION ALL
                                           SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS,
                                          VAT_ITEM VI
                                    WHERE I_ISPREPAY     = YN_NO
                                        AND IPD.POSTING_ID = I_POSTING_ID
                                        AND IPD.SYS_TAX_IND = REIM_POSTING_SQL.SYS_TAX_IND_REV_CHRG_VAT
                                        AND IPD.CALC_BASIS = BASIS_DETAILS

                                        AND (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)
                                        AND IIDT.DOC_ID = IPD.DOC_ID
                                        AND IM.ITEM = IIDT.ITEM
                                        AND LOCS.LOCATION = IPD.LOCATION
                                        AND VI.ITEM = IIDT.ITEM
                                        AND VI.REVERSE_VAT_IND = YN_YES
                                        AND VI.VAT_REGION     = LOCS.VAT_REGION
                                        AND VI.VAT_TYPE       IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                        AND VI.ACTIVE_DATE    = (SELECT MAX(ACTIVE_DATE)
                                                                   FROM VAT_ITEM V
                                                                  WHERE V.ITEM = VI.ITEM
                                                                    AND V.VAT_REGION = VI.VAT_REGION
                                                                    AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                                    AND V.ACTIVE_DATE <= IPD.DOC_DATE)


                        UNION ALL

                                --tax component of resolution documents (dynamic)
                                SELECT IPD.POSTING_ID POSTING_ID,
                                       IPD.DOC_ID DOC_ID,
                                       DECODE(NVL(IPD.SYS_TAX_IND, YN_NO),
                                              SYS_TAX_IND_ACQ_VAT, AMT_ACQ_VAT,
                                              AMT_TAX) AMOUNT_TYPE,
                                       IPD.LOCATION LOCATION,
                                       IPD.SET_OF_BOOKS_ID,
                                       IM.DEPT DEPT,
                                       IM.CLASS CLASS,
                                       IM.SUBCLASS SUBCLASS,
                                       IDDRT.TAX_CODE TAX_CODE,
                                       IDDRT.TAX_RATE TAX_RATE,
                                       DECODE(NVL(IPD.SYS_TAX_IND, YN_NO),
                                              SYS_TAX_IND_ACQ_VAT, TAX_ACQUISITION,
                                              TAX) ACCOUNT_CODE,
                                       IPD.SYS_CURRENCY_CODE SYS_CURRENCY_CODE,
                                       IPD.DECIMAL_DIGITS,
                                       IDDRT.TAX_AMOUNT AMOUNT
                                FROM   IM_POSTING_DOC IPD,
                                       IM_DOC_DETAIL_REASON_CODES IDDRC,
                                       ITEM_MASTER IM,
                                       IM_DOC_DETAIL_RC_TAX IDDRT
                                 WHERE IPD.POSTING_ID = I_POSTING_ID

                                       AND IDDRC.DOC_ID = IPD.DOC_ID
                                       AND IDDRT.IM_DOC_DETAIL_REASON_CODES_ID = IDDRC.IM_DOC_DETAIL_REASON_CODES_ID
                                       AND IDDRC.STATUS = REIM_POSTING_SQL.DOC_STATUS_APPROVED
                                 AND   IM.ITEM = IDDRC.ITEM
                                 AND   (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)


                        UNION ALL

                                --reverse vat component of resolution documents (dynamic)
                                SELECT IPD.POSTING_ID POSTING_ID,
                                       IPD.DOC_ID DOC_ID,
                                       AMT_REV_CHRG_VAT AMOUNT_TYPE,
                                       IPD.LOCATION LOCATION,
                                       IPD.SET_OF_BOOKS_ID,
                                       IM.DEPT DEPT,
                                       IM.CLASS CLASS,
                                       IM.SUBCLASS SUBCLASS,
                                       VI.VAT_CODE TAX_CODE,
                                       VI.VAT_RATE TAX_RATE,
                                       TAX_REVERSE_CHARGE ACCOUNT_CODE,
                                       IPD.SYS_CURRENCY_CODE SYS_CURRENCY_CODE,
                                       IPD.DECIMAL_DIGITS,
                                       IDDRT.TAX_BASIS * (VI.VAT_RATE/100) AMOUNT
                                FROM   IM_POSTING_DOC IPD,
                                       IM_DOC_DETAIL_REASON_CODES IDDRC,
                                       IM_DOC_DETAIL_RC_TAX IDDRT,
                                       ITEM_MASTER IM,
                                       (SELECT WH LOCATION,VAT_REGION FROM WH
                                           UNION ALL
                                           SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS,
                                       VAT_ITEM VI
                                 WHERE I_ISPREPAY     = YN_NO
                                   AND IPD.POSTING_ID = I_POSTING_ID
                                   AND IPD.SYS_TAX_IND = REIM_POSTING_SQL.SYS_TAX_IND_REV_CHRG_VAT

                                   AND (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)
                                   AND IDDRC.DOC_ID = IPD.DOC_ID
                                   AND IDDRC.STATUS = REIM_POSTING_SQL.DOC_STATUS_APPROVED
                                   AND IDDRT.IM_DOC_DETAIL_REASON_CODES_ID = IDDRC.IM_DOC_DETAIL_REASON_CODES_ID
                                   AND IM.ITEM = IDDRC.ITEM
                                   AND LOCS.LOCATION = IPD.LOCATION
                                   AND VI.ITEM = IDDRC.ITEM
                                   AND VI.REVERSE_VAT_IND = YN_YES
                                   AND VI.VAT_REGION     = LOCS.VAT_REGION
                                   AND VI.VAT_TYPE       IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                   AND VI.ACTIVE_DATE    = (SELECT MAX(ACTIVE_DATE)
                                                              FROM VAT_ITEM V
                                                             WHERE V.ITEM = VI.ITEM
                                                               AND V.VAT_REGION = VI.VAT_REGION
                                                               AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                               AND V.ACTIVE_DATE <= IPD.DOC_DATE)


                        UNION ALL

                                  --tax component of fixed deal  (dynamic)
                                SELECT IPD.POSTING_ID POSTING_ID,
                                       IPD.DOC_ID DOC_ID,
                                       AMT_TAX AMOUNT_TYPE,
                                       IFDD.LOCATION LOCATION,
                                       IPD.SET_OF_BOOKS_ID,
                                       IFDD.DEPT DEPT,
                                       IFDD.CLASS CLASS,
                                       IFDD.SUBCLASS SUBCLASS,
                                       IFDDT.TAX_CODE TAX_CODE,
                                       IFDDT.TAX_RATE TAX_RATE,
                                       TAX AS ACCOUNT_CODE,
                                       IPD.SYS_CURRENCY_CODE SYS_CURRENCY_CODE,
                                       IPD.DECIMAL_DIGITS,
                                       IFDDT.TAX_AMOUNT AMOUNT
                          FROM  IM_POSTING_DOC IPD,
                                        IM_FIXED_DEAL_DETAIL IFDD,
                                IM_FIXED_DEAL_DETAIL_TAX IFDDT
                                WHERE IPD.POSTING_ID = I_POSTING_ID

                                      AND IFDD.DOC_ID = IPD.DOC_ID
                          AND IFDD.DOC_ID = IFDDT.DOC_ID
                              AND IFDD.SEQ_NO = IFDDT.SEQ_NO

                              UNION ALL

                                --tax component of complex deal (dynamic)
                                SELECT IPD.POSTING_ID POSTING_ID,
                                       IPD.DOC_ID DOC_ID,
                                       DECODE(NVL(IPD.SYS_TAX_IND, YN_NO),
                                              SYS_TAX_IND_ACQ_VAT, AMT_ACQ_VAT,
                                              AMT_TAX) AMOUNT_TYPE,
                                       ICDD.LOCATION LOCATION,
                                       IPD.SET_OF_BOOKS_ID,
                                       IM.DEPT DEPT,
                                       IM.CLASS CLASS,
                                       IM.SUBCLASS SUBCLASS,
                                       ICDDT.TAX_CODE TAX_CODE,
                                       ICDDT.TAX_RATE TAX_RATE,
                                       DECODE(NVL(IPD.SYS_TAX_IND, YN_NO),
                                              SYS_TAX_IND_ACQ_VAT, TAX_ACQUISITION,
                                              TAX) ACCOUNT_CODE,
                                       IPD.SYS_CURRENCY_CODE SYS_CURRENCY_CODE,
                                       IPD.DECIMAL_DIGITS,
                                       ICDDT.TAX_AMOUNT AMOUNT
                                    FROM  IM_POSTING_DOC IPD,
                                          IM_COMPLEX_DEAL_DETAIL ICDD,
                                          IM_COMPLEX_DEAL_DETAIL_TAX ICDDT,
                                          ITEM_MASTER IM
                                    WHERE IPD.POSTING_ID = I_POSTING_ID

                                        AND ICDD.DOC_ID = IPD.DOC_ID
                                        AND ICDD.DOC_ID = ICDDT.DOC_ID
                                        AND ICDDT.ITEM = ICDD.ITEM
                                        AND IM.ITEM = ICDD.ITEM

                              UNION ALL
                                    --tax component of non-merch cost (non-dynamic)
                                SELECT IPD.POSTING_ID          POSTING_ID,
                                       IPD.DOC_ID              DOC_ID,
                                       AMT_TAX AMOUNT_TYPE,
                                       IPD.LOCATION            LOCATION,
                                       IPD.SET_OF_BOOKS_ID,
                                       NULL AS DEPT,
                                       NULL AS CLASS,
                                       NULL AS SUBCLASS,
                                       IDNMT.TAX_CODE          TAX_CODE,
                                       IDNMT.TAX_RATE          TAX_RATE,
                                       TAX_NON_DYNAMIC         ACCOUNT_CODE,
                                       IPD.SYS_CURRENCY_CODE   SYS_CURRENCY_CODE,
                                       IPD.DECIMAL_DIGITS,
                                   IDNMT.TAX_AMOUNT   AMOUNT
                                    FROM  IM_POSTING_DOC IPD,
                                          IM_DOC_NON_MERCH_TAX IDNMT
                                  WHERE IPD.POSTING_ID = I_POSTING_ID
                                      AND IDNMT.DOC_ID   = IPD.DOC_ID

                )
                     GROUP BY POSTING_ID,
                             DOC_ID,
                             AMOUNT_TYPE,
                             LOCATION,
                             SET_OF_BOOKS_ID,
                             DEPT,
                            CLASS,
                              SUBCLASS,
                             TAX_CODE,
                              TAX_RATE,
                             ACCOUNT_CODE,
                             SYS_CURRENCY_CODE,
                                    DECIMAL_DIGITS
                    );


     /* Insert Offsets for Acquisition and reverse charge vats*/
     INSERT INTO IM_POSTING_DOC_AMOUNTS
                     (  POSTING_AMT_ID,
                        POSTING_ID,
                        DOC_ID,
                        AMOUNT_TYPE,
                        LOCATION,
                        ORG_UNIT,
                        SET_OF_BOOKS_ID,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        AMOUNT,
                        ACCOUNT_TYPE,
                        ACCOUNT_CODE,
                        PRIM_CURRENCY_CODE)
                 SELECT IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                        POSTING_ID,
                        DOC_ID,
                        DECODE(AMOUNT_TYPE,
                               REIM_POSTING_SQL.AMT_ACQ_VAT, REIM_POSTING_SQL.AMT_ACQ_VAT_OFFSET,
                               REIM_POSTING_SQL.AMT_REV_CHRG_VAT, REIM_POSTING_SQL.AMT_REV_CHRG_VAT_OFFSET) AMOUNT_TYPE,
                        LOCATION,
                        ORG_UNIT,
                        SET_OF_BOOKS_ID,
                        DEPT,
                        CLASS,
                        SUBCLASS,
                        TAX_CODE,
                        TAX_RATE,
                        GROUP_BY_1,
                        GROUP_BY_2,
                        GROUP_BY_3,
                        GROUP_BY_4,
                        GROUP_BY_5,
                        GROUP_BY_6,
                        GROUP_BY_7,
                        GROUP_BY_8,
                        -1 * AMOUNT, -- negate the amount
                        ACCOUNT_TYPE,
                        DECODE(AMOUNT_TYPE,
                               REIM_POSTING_SQL.AMT_ACQ_VAT, REIM_POSTING_SQL.TAX_ACQUISITION_OFFSET,
                               REIM_POSTING_SQL.AMT_REV_CHRG_VAT, REIM_POSTING_SQL.TAX_REVERSE_CHARGE_OFFSET) ACCOUNT_CODE,
                        PRIM_CURRENCY_CODE
                   FROM IM_POSTING_DOC_AMOUNTS
                  WHERE POSTING_ID = I_POSTING_ID
                    AND AMOUNT_TYPE IN (REIM_POSTING_SQL.AMT_ACQ_VAT,
                                        REIM_POSTING_SQL.AMT_REV_CHRG_VAT);

   RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);


     EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END;




/*******************

Calculates the Variance Within Tolerance for Invoices.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION CALC_INVOICE_VWT
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_INVOICE_VWT';
       STATUS NUMBER := SUCCESS;
    BEGIN

        STATUS := PRORATE_DOC_AMOUNT(O_ERROR_MESSAGE, L_PROGRAM, I_POSTING_ID, AMT_VARIANCE_WITHIN_TOLERANCE);

        IF (STATUS <> SUCCESS) THEN
            RETURN STATUS;
        ELSE
            RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
        END IF;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END;

/*******************

Calculates the matched receipt cost for invoices.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION CALC_INVOICE_MATCHED_RECEIPTS
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_INVOICE_MATCHED_RECEIPTS';
        STATUS NUMBER := SUCCESS;
    BEGIN
        STATUS := PRORATE_DOC_AMOUNT(O_ERROR_MESSAGE, L_PROGRAM, I_POSTING_ID, AMT_MATCHED_RECEIPT_COST);

        IF (STATUS <> SUCCESS) THEN
            RETURN STATUS;
        ELSE
            RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END CALC_INVOICE_MATCHED_RECEIPTS;

/*******************

Calculates the Document PPA cost (i.e total header cost) to be posted.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
   FUNCTION CALC_DOCUMENT_PPA_VALUE(O_ERROR_MESSAGE  OUT VARCHAR2,
                                I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE
                                )

    RETURN NUMBER
    IS

          L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_DOCUMENT_PPA_VALUE';
          L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);
    BEGIN
    SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
              INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

    INSERT INTO IM_POSTING_DOC_AMOUNTS
               (POSTING_AMT_ID,
                POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION,
                ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE,
                PRIM_CURRENCY_CODE)

        SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                POSTING_ID,
                DOC_ID,
                AMT_PPA_COST    AMOUNT_TYPE,
                LOC,
                (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                NULL  DEPT,
                NULL  CLASS,
                NULL  SUBCLASS,
                NO_VALUE  GROUP_BY_1,
                NO_VALUE  GROUP_BY_2,
                NO_VALUE  GROUP_BY_3,
                NO_VALUE  GROUP_BY_4,
                NO_VALUE  GROUP_BY_5,
                NO_VALUE  GROUP_BY_6,
                NO_VALUE  GROUP_BY_7,
                YN_YES    GROUP_BY_8,
                AMOUNT,
                BASIC_TRANSACTIONS       ACCOUNT_TYPE,
                PRE_PAID_ASSET           ACCOUNT_CODE,
                NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
                 FROM (


        -- Query #1:  Any invoice without invoice details
        -- Document Value = document total cost WHICH includes non-merch
        -- Fix for HPQC defect 4511 13.2 System Test.
        -- Fetching total cost inclusive of tax .Removing the
        -- not exists clause for im_doc_detail_reason_codes as
        -- prepay can be done only for Merchandise Documents which
        -- does not hold statuses like MTCH,TAXDIS,POSTED
        -- Part a: For documents which do not have entry in im_doc_tax
        SELECT  IPD.POSTING_ID,
                IPD.DOC_ID,
                AMT_PPA_COST AMOUNT_TYPE,
                IPD.LOCATION LOC,
                IPD.SET_OF_BOOKS_ID,
                IPD.SYS_CURRENCY_CODE,
                NULL AS TAX_CODE,
                NULL AS TAX_RATE,
                NULL AS DEPT,
                NULL AS CLASS,
                NULL AS SUBCLASS,
                NO_VALUE AS GROUP_BY_1,
                NO_VALUE AS GROUP_BY_2,
                NO_VALUE AS GROUP_BY_3,
                NO_VALUE AS GROUP_BY_4,
                NO_VALUE AS GROUP_BY_5,
                NO_VALUE AS GROUP_BY_6,
                NO_VALUE AS GROUP_BY_7,
                YN_YES AS GROUP_BY_8,
                ROUND(IDH.TOTAL_COST_INC_TAX, IPD.DECIMAL_DIGITS)  AS AMOUNT
        FROM    IM_POSTING_DOC IPD,
                IM_DOC_HEAD    IDH,
                IM_SYSTEM_OPTIONS ISO
        WHERE   IPD.POSTING_ID = I_POSTING_ID

        AND     IDH.DOC_ID =  IPD.DOC_ID
        AND     IDH.TYPE = DOC_TYPE_MERCH_INVOICE
        AND     IDH.STATUS NOT IN (DOC_STATUS_MATCHED,
                                   DOC_STATUS_POSTED,
                                   DOC_STATUS_TAX_DISCREPANCY)
        AND     NOT EXISTS (SELECT 1 FROM im_invoice_detail iid WHERE iid.doc_id =idh.doc_id)
        AND     NOT EXISTS (SELECT 1 FROM im_doc_tax idt WHERE idt.doc_id = idh.doc_id)
        UNION ALL

        -- Query #1:  Any invoice without details
        -- Document Value = document total cost WHICH includes non-merch
        -- Part b: For documents which have entries in im_doc_tax
        SELECT  POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOC,
                SET_OF_BOOKS_ID,
                SYS_CURRENCY_CODE,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                ROUND(SUM(AMOUNT), DECIMAL_DIGITS)  AS AMOUNT
        FROM (
              SELECT  IPD.POSTING_ID,
                      IPD.DOC_ID,
                      AMT_PPA_COST AMOUNT_TYPE,
                      IPD.LOCATION LOC,
                      IPD.SET_OF_BOOKS_ID,
                      IPD.SYS_CURRENCY_CODE,
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDT.TAX_CODE) AS TAX_CODE,
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDT.TAX_RATE) AS TAX_RATE,
                      NULL AS DEPT,
                      NULL AS CLASS,
                      NULL AS SUBCLASS,
                      NO_VALUE AS GROUP_BY_1,
                      NO_VALUE AS GROUP_BY_2,
                      NO_VALUE AS GROUP_BY_3,
                      NO_VALUE AS GROUP_BY_4,
                      NO_VALUE AS GROUP_BY_5,
                      NO_VALUE AS GROUP_BY_6,
                      NO_VALUE AS GROUP_BY_7,
                      YN_YES AS GROUP_BY_8,
                      IDT.TAX_BASIS AS AMOUNT,
                      IPD.DECIMAL_DIGITS DECIMAL_DIGITS
              FROM    IM_POSTING_DOC IPD,
                      IM_DOC_HEAD    IDH,
                      IM_DOC_TAX IDT
              WHERE   IPD.POSTING_ID = I_POSTING_ID

              AND     IDH.TYPE = DOC_TYPE_MERCH_INVOICE
              AND     IDH.DOC_ID =  IPD.DOC_ID
              AND     IDH.DOC_ID = IDT.DOC_ID
              AND     NOT EXISTS (SELECT 1 FROM im_invoice_detail iid WHERE iid.doc_id =idh.doc_id)
        )
        GROUP BY POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOC,
                SET_OF_BOOKS_ID,
                SYS_CURRENCY_CODE,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                DECIMAL_DIGITS
         UNION ALL

        -- Query #2:  Following query will take care of all scenerios where document have details .
        --There is a seperate subquery for each scenerio.
        SELECT  POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION LOC,
                SET_OF_BOOKS_ID,
                SYS_CURRENCY_CODE,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                ROUND(SUM(AMOUNT), DECIMAL_DIGITS) AMOUNT
        FROM(
              -- SUBQUERY #1:
              -- will have to explicitly query for the non-merch components  to combine with SUBQuery#2.
              SELECT  IPD.POSTING_ID,
                      IPD.DOC_ID,
                      IPD.DOC_TYPE,
                      AMT_PPA_COST AMOUNT_TYPE,
                      IPD.LOCATION LOCATION,
                      IPD.SET_OF_BOOKS_ID,
                      IPD.SYS_CURRENCY_CODE,
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDNMT.TAX_CODE) AS TAX_CODE,
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDNMT.TAX_RATE) AS TAX_RATE,
                      NULL AS DEPT,
                      NULL AS CLASS,
                      NULL AS SUBCLASS,
                      NO_VALUE                AS  GROUP_BY_1,
                      NO_VALUE                AS  GROUP_BY_2,
                      NO_VALUE                AS  GROUP_BY_3,
                      NO_VALUE                AS  GROUP_BY_4,
                      NO_VALUE                AS  GROUP_BY_5,
                      NO_VALUE                AS  GROUP_BY_6,
                      NO_VALUE                AS  GROUP_BY_7,
                      YN_YES AS GROUP_BY_8,
                      IDNM.NON_MERCH_AMT  AMOUNT,
                      IPD.DECIMAL_DIGITS
                FROM  IM_POSTING_DOC IPD,
                      IM_DOC_HEAD IDH,
                      IM_DOC_NON_MERCH IDNM,
                      IM_SYSTEM_OPTIONS ISO,
                      IM_DOC_NON_MERCH_TAX IDNMT
              WHERE IPD.POSTING_ID = I_POSTING_ID
              AND   IDH.DOC_ID = IPD.DOC_ID

              AND   IDNM.DOC_ID = IDH.DOC_ID
              AND   IDH.TYPE = DOC_TYPE_MERCH_INVOICE
              AND   IDNM.DOC_ID = IDNMT.DOC_ID (+)
              AND   IDNM.NON_MERCH_CODE = IDNMT.NON_MERCH_CODE (+)
              AND   IDH.STATUS NOT IN (DOC_STATUS_MATCHED,
                                       DOC_STATUS_POSTED,
                                       DOC_STATUS_TAX_DISCREPANCY)
              AND   EXISTS (SELECT 1 FROM IM_INVOICE_DETAIL IID WHERE IID.DOC_ID = IDH.DOC_ID)
              UNION ALL
                -- Subquery2:  Calculate detail level amount from invoice details
              SELECT  IPD.POSTING_ID,
                      IPD.DOC_ID,
                      IPD.DOC_TYPE,
                      AMT_PPA_COST AS AMOUNT_TYPE,
                      IPD.LOCATION AS LOCATION,
                      IPD.SET_OF_BOOKS_ID,
                      IPD.SYS_CURRENCY_CODE,
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IIDT.TAX_CODE) AS TAX_CODE,
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IIDT.TAX_RATE) AS TAX_RATE,
                      NULL AS DEPT,
                      NULL AS CLASS,
                      NULL AS SUBCLASS,
                      NO_VALUE AS GROUP_BY_1,
                      NO_VALUE AS GROUP_BY_2,
                      NO_VALUE AS GROUP_BY_3,
                      NO_VALUE AS GROUP_BY_4,
                      NO_VALUE AS GROUP_BY_5,
                      NO_VALUE AS GROUP_BY_6,
                      NO_VALUE AS GROUP_BY_7,
                      YN_YES AS GROUP_BY_8,
                      (IID.UNIT_COST * IID.QTY) AMOUNT,
                      ipd.decimal_digits
              FROM    IM_POSTING_DOC IPD,
                      IM_DOC_HEAD    IDH,
                      IM_INVOICE_DETAIL IID,
                      IM_SYSTEM_OPTIONS ISO,
                      IM_INVOICE_DETAIL_TAX IIDT
              WHERE   IPD.POSTING_ID = I_POSTING_ID

              AND     IDH.DOC_ID =  IPD.DOC_ID
              AND     IDH.TYPE   = DOC_TYPE_MERCH_INVOICE
              AND     IDH.STATUS NOT IN (DOC_STATUS_MATCHED,
                                         DOC_STATUS_POSTED,
                                         DOC_STATUS_TAX_DISCREPANCY)
              AND     IID.DOC_ID = IDH.DOC_ID
              AND     IID.DOC_ID = IIDT.DOC_ID (+)
              AND     IID.ITEM = IIDT.ITEM (+)
              )
              GROUP BY POSTING_ID,
                      DOC_ID,
                      DOC_TYPE,
                      AMOUNT_TYPE,
                      LOCATION,
                      SET_OF_BOOKS_ID,
                      SYS_CURRENCY_CODE,
                      TAX_CODE,
                      TAX_RATE,
                      DEPT,
                      CLASS,
                      SUBCLASS,
                      GROUP_BY_1,
                      GROUP_BY_2,
                      GROUP_BY_3,
                      GROUP_BY_4,
                      GROUP_BY_5,
                      GROUP_BY_6,
                      GROUP_BY_7,
                      GROUP_BY_8,
                      decimal_digits);

    RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;

    END;

    /************************************************************
     * Calculates invoice UNR and VWT amounts and prorates them
     * across applicable departments, classes and VAT codes.
     *
     * The procedure will prorate two values depending on the
     * value of I_AMOUNT_TYPE :  the UNR (matched receipt)
     * amount and the VWT (variance within tolerance) amount.
     *
     * VWT Calculation
     * ---------------
     *
     *    VWT = Document header VWT
     *          + Sum of Variances within tolerance on the document detail.
     *
     * UNR Calculation
     * ---------------
     *
     * If the document has no details:
     *
     *    UNR = Document Total Cost
     *          - Non-merchandise Cost
     *          + Calculated VWT
     *          + Sum of Resolution Action extended costs
     *
     * If the document has details:
     *
     *    UNR = Sum of detail extended costs
     *          + Calculated VWT
     *          + Sum of Resolution Action extended costs
     *
     *
     * Proration
     * ----------
     *
     * 1.  Prorate by department/class using information from IM_RECEIPT_ITEM_POSTING
     *     /IM_RCPT_ITEM_POSTING_INVOICE
     *
     * 2.  For each prorated dept/class amount, prorate further per invoice
     *     vat code (IM_DOC_VAT.VAT_BASIS).
     *
     *
     * Example:
     * --------
     *              S U M M A R Y   M A T C H
     *    ---------------------------------------------------------
     *    Invoice_1                        Receipt
     *    ---------------------------------------------------------
     *    - InvcTotal:  1000               - RcptTotal : 800
     *    - Vat Breakdown:                 - Item Breakdown:
     *              C  20%  800  = 160            Item A Dep_1  600
     *              S  10%  200  =  20            Item B Dep_2  200
     *    - TaxTotal = 180
     *
     *    VWT = 800 - 1000 = -200
     *    UNR = 1000 + VWT = 1000 - 200 = 800
     *
     *    Prorate UNR
     *    -----------
     *
     *        UNR x Dep_1/RcptTotal = 800 x 600/800 = 600
     *
     *                  600 x C/InvcTotal = 600 x 800/1000 = 480  (Dep_1, Vat C)
     *                      Tax = 480 x .20 = 96
     *
     *                  600 x S/InvcTotal = 600 x 200/1000 = 120  (Dep_1, Vat S)
     *                      Tax = 120 x .10 = 12
     *
     *        UNR x Dep_2/RcptTotal = 800 x 200/800 = 200
     *
     *                  200 x C/InvcTotal = 200 x 800/1000 = 160  (Dep_2, Vat C)
     *                      Tax = 160 x .20 = 32
     *
     *                  200 x S/InvcTotal = 200 x 200/1000 =  40  (Dep_2, Vat S)
     *                      Tax = 40 x .10 =  4
     *
     *
     *    Prorate VWT
     *    -----------
     *
     *        VWT x Dep_1/RcptTotal = 200 x 600/800 = 150
     *
     *                  150 x C/InvcTotal = 150 x 800/1000 = 120 (Dep_1, Vat C)
     *                      Tax = 120 x .20 = 24
     *
     *                  150 x S/InvcTotal = 150 x 200/1000 =  30  (Dep_1, Vat S)
     *                      Tax = 30 x .10 = 3
     *
     *        VWT x Dep_2/RcptTotal = 200 x 200/800 = 50
     *
     *                  50  x C/InvcTotal = 50  x 800/1000 =  40  (Dep_2, Vat C)
     *                      Tax = 40 x .20 =  8
     *
     *                  50  x S/InvcTotal = 50  x 200/1000 =  10  (Dep_2, Vat S)
     *                      Tax = 10 x .10 =  1
     *
     *
     *    Check:  Sum of all prorated amounts == Invoice Total
     *            Sum of all taxes == Invoice Tax Total
     *
     *
     *
     * Mainly inserts into IM_POSTING_DOC_AMOUNTS
     ************************************************************/
    FUNCTION PRORATE_DOC_AMOUNT
        (O_ERROR_MESSAGE  OUT VARCHAR2,
         I_PROGRAM        IN VARCHAR2,
         I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE,
         I_AMOUNT_TYPE    IN VARCHAR2)
    RETURN NUMBER
    IS
        L_BY_DEPTCLASS IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;
        L_PRORATE_TAX_ID IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;
        --L_EXTCOST_BY_DOC_ORD_ID IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;

        STATUS NUMBER := SUCCESS;

        C_DOC_AMOUNTS SYS_REFCURSOR;

        L_POSTING_ID NUMBER;
        L_DOC_ID NUMBER;
        L_DECIMAL_DIGITS NUMBER;
        L_UNR NUMBER;
        L_DOC_COST NUMBER;
        L_DOC_VWT NUMBER;
        L_RESOLUTION_COST NUMBER;
        L_NON_MERCH_COST NUMBER;
        L_DEPT NUMBER;
        L_CLASS NUMBER;
        L_EXT_COST NUMBER;
        L_ZERO_CHK NUMBER;

        L_AMT_TO_PRORATE NUMBER;

        L_CALC_STRING VARCHAR2(500);
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PRORATE_DOC_AMOUNT';
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

    BEGIN
        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM || ' called from ' ||I_PROGRAM);

        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
          INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

        GET_DOC_AMOUNTS(I_POSTING_ID, C_DOC_AMOUNTS);
        /**
          CALCULATE:   UNR PER DOCUMENT AND SPLIT THEM ACROSS DEPARTMENTS
        **/
        L_BY_DEPTCLASS := REIM_PRORATE_SQL.GET_NEXT_PRORATE_WORKSPACE_ID;

        LOG_DEBUG
            (I_POSTING_ID,
             L_PROGRAM||TDELIM||'Calculating document UNR / VWT...');

        LOOP
            FETCH C_DOC_AMOUNTS INTO
                       L_POSTING_ID,
                       L_DOC_ID,
                       L_DECIMAL_DIGITS,
                       L_UNR,
                       L_DOC_COST,
                       L_DOC_VWT,
                       L_RESOLUTION_COST,
                       L_NON_MERCH_COST,
                       L_DEPT,
                       L_CLASS,
                       L_EXT_COST;
            EXIT WHEN C_DOC_AMOUNTS%NOTFOUND;

            L_CALC_STRING := 'doc='||L_DOC_ID||TB||
                 'unr='||L_UNR||TB||
                 'doc_cost='||L_DOC_COST||TB||
                 'vwt='||L_DOC_VWT||TB||
                 'res='||L_RESOLUTION_COST||TB||
                 'nmc='||L_NON_MERCH_COST||TB||
                 'dept='||L_DEPT||TB||
                 'class='||L_CLASS||TB||
                 'dc_value='||L_EXT_COST;
                 L_ZERO_CHK  := 555;

            LOG_DEBUG
                (I_POSTING_ID,
                 L_PROGRAM||TDELIM||
                 'Calc doc amounts'||TDELIM||L_CALC_STRING);

            -- Determine the amount to be prorated.
            IF I_AMOUNT_TYPE = AMT_MATCHED_RECEIPT_COST THEN
                L_AMT_TO_PRORATE := L_UNR;
            ELSIF I_AMOUNT_TYPE = AMT_VARIANCE_WITHIN_TOLERANCE THEN
                L_AMT_TO_PRORATE := L_DOC_VWT;
            ELSE
                L_AMT_TO_PRORATE := 0;
            END IF;


                    IF ((I_AMOUNT_TYPE = AMT_VARIANCE_WITHIN_TOLERANCE) AND (L_AMT_TO_PRORATE = 0)) THEN
               L_ZERO_CHK := 100;
            END IF;

            IF L_ZERO_CHK = 555 THEN

                INSERT INTO IM_PRORATE_WORKSPACE(PRORATE_WORKSPACE_ID,
                                                 PRORATE_GROUP_KEY,
                                                 DECIMAL_SCALE,
                                                 AMOUNT_TO_PRORATE_1,
                                                 SHARE_IN_GROUP_1,
                                                 SHARE_DATA_1)
                VALUES(
                    L_BY_DEPTCLASS,
                    -- Prorate UNR within the document...
                    L_POSTING_ID || TDELIM || L_DOC_ID,
                    L_DECIMAL_DIGITS,
                    L_AMT_TO_PRORATE,
                    L_EXT_COST,
                    -- Keep key information per prorated amount...
                    /* 1 */  L_POSTING_ID || TDELIM ||
                    /* 2 */  L_DOC_ID || TDELIM ||
                    /* 3 */  L_DEPT || TDELIM ||
                    /* 4 */  L_CLASS
                );

            END IF;

        END LOOP;

        CLOSE C_DOC_AMOUNTS;

        REIM_PRORATE_SQL.DO_PRORATION(L_BY_DEPTCLASS);

        LOG_DEBUG_PRORATE_WORKSPACE
            (I_POSTING_ID,
             L_BY_DEPTCLASS,
             L_PROGRAM||TDELIM||I_AMOUNT_TYPE||' by Dept/Class proration ');


        INSERT INTO IM_POSTING_DOC_AMOUNTS IPDA
            ( POSTING_AMT_ID,
            POSTING_ID,
            DOC_ID,
            AMOUNT_TYPE,
            LOCATION,
            ORG_UNIT,
            SET_OF_BOOKS_ID,
            DEPT,
            CLASS,
            TAX_CODE,
            TAX_RATE,
            GROUP_BY_1,
            GROUP_BY_2,
            GROUP_BY_3,
            GROUP_BY_4,
            GROUP_BY_5,
            GROUP_BY_6,
            GROUP_BY_7,
            GROUP_BY_8,
            AMOUNT,
            ACCOUNT_TYPE,
            ACCOUNT_CODE,
            --AMOUNT_VAT,
            PRIM_CURRENCY_CODE)
        SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                        IPD.POSTING_ID,
                IPD.DOC_ID,
                I_AMOUNT_TYPE,
                        IPD.LOCATION,
              (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = IPD.LOCATION AND ROWNUM < 2  ) AS ORG_UNIT,
                IPD.SET_OF_BOOKS_ID,
                IPW.DEPT,
                IPW.CLASS,
                NULL AS TAX_CODE,
                NULL AS TAX_RATE,
                NO_VALUE AS GROUP_BY_1,
                NO_VALUE AS GROUP_BY_2,
                NO_VALUE AS GROUP_BY_3,
                NO_VALUE AS GROUP_BY_4,
                NO_VALUE AS GROUP_BY_5,
                NO_VALUE AS GROUP_BY_6,
                NO_VALUE AS GROUP_BY_7,
                NO_VALUE AS GROUP_BY_8,
                IPW.RESULT_PRORATED_AMOUNT_1 AS AMOUNT,
                BASIC_TRANSACTIONS AS ACCOUNT_TYPE,
                DECODE(I_AMOUNT_TYPE,AMT_MATCHED_RECEIPT_COST,UNMATCHED_RECEIPT,VARIANCE_WITHIN_TOLERANCE) AS  ACCOUNT_CODE,
                --ROUND(IPW.RESULT_PRORATED_AMOUNT_1 * IPW.VAT_RATE / 100, ipd.DECIMAL_digits) AS AMOUNT_VAT,
                        NVL(IPD.SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = IPD.LOCATION) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
        FROM (
                SELECT  REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 1) AS POSTING_ID,
                        REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 2) AS DOC_ID,
                        REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 3) AS DEPT,
                        REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 4) AS CLASS,
                        RESULT_PRORATED_AMOUNT_1
                FROM IM_PRORATE_WORKSPACE
                WHERE PRORATE_WORKSPACE_ID = L_BY_DEPTCLASS
                ) IPW,
             IM_POSTING_DOC IPD
        WHERE L_PRORATE_ACROSS_TAX_CODES = REIM_POSTING_SQL.YN_NO
        AND   IPW.POSTING_ID = I_POSTING_ID
        AND   IPD.POSTING_ID = IPW.POSTING_ID
        AND   IPD.DOC_ID     = IPW.DOC_ID
        ;

--new proration across tax codes

        L_PRORATE_TAX_ID := REIM_PRORATE_SQL.GET_NEXT_PRORATE_WORKSPACE_ID;
        INSERT INTO IM_PRORATE_WORKSPACE(PRORATE_WORKSPACE_ID,
                                         PRORATE_GROUP_KEY,
                                         DECIMAL_SCALE,
                                         AMOUNT_TO_PRORATE_1,
                                         SHARE_IN_GROUP_1,
                                         SHARE_DATA_1)
        SELECT *
        FROM (
                -- subquery:  when there's IM_DOC_TAX...
                SELECT L_PRORATE_TAX_ID   AS PRORATE_WORKSPACE_ID,
                       IPW.SHARE_DATA_1   AS PRORATE_GROUP_KEY,
                       IPD.DECIMAL_DIGITS AS DECIMAL_DIGITS,
                       IPW.RESULT_PRORATED_AMOUNT_1 AS AMOUNT_TO_PRORATE_1,
                       IDT.TAX_BASIS
                           - NVL((
                                SELECT SUM(IDNMT.TAX_BASIS)
                                FROM IM_DOC_NON_MERCH_TAX IDNMT
                                WHERE IDNMT.DOC_ID = IDT.DOC_ID
                                AND IDNMT.TAX_CODE = IDT.TAX_CODE
                                ), 0)
                           + NVL((
                                SELECT
                                    SUM(((CASE
                                        WHEN IRA.ACTION IN ('DMVI', 'DMVF') THEN
                                            -1
                                        ELSE
                                            1 END))*(
                                        CASE
                                        WHEN IRA.EXTENDED_COST IS NOT NULL THEN
                                            IRA.EXTENDED_COST
                                        WHEN IRA.QTY IS NOT NULL THEN
                                            IRA.QTY * IID.RESOLUTION_ADJUSTED_UNIT_COST
                                        END)
                                    ) AS RES_COST
                                FROM    IM_INVOICE_DETAIL IID,
                                        IM_INVOICE_DETAIL_TAX IIDT,
                                        IM_RESOLUTION_ACTION IRA
                                WHERE IID.DOC_ID   = IDT.DOC_ID
                                AND   IID.DOC_ID = IIDT.DOC_ID
                                AND   IID.ITEM = IIDT.ITEM
                                AND   IIDT.TAX_CODE = IDT.TAX_CODE
                                AND   IRA.DOC_ID   = IID.DOC_ID
                                AND   IRA.ITEM     = IID.ITEM
                                AND   IS_ACCOUNTABLE_RESOLUTION(IRA.ACTION) = YN_YES
                                ), 0)
                       AS SHARE_IN_GROUP_1,
                         IPD.POSTING_ID||TDELIM||
                         IPD.DOC_ID||TDELIM||
                         REIM_PKG_UTIL_SQL.GET_TOKEN(IPW.SHARE_DATA_1, TDELIM, 3)||TDELIM||  -- DEPARTMENT
                         REIM_PKG_UTIL_SQL.GET_TOKEN(IPW.SHARE_DATA_1, TDELIM, 4)||TDELIM|| -- CLASS
                         IDT.TAX_CODE||TDELIM||
                         IDT.TAX_RATE AS SHARE_DATA_1
                FROM IM_POSTING_DOC IPD,
                     IM_PRORATE_WORKSPACE IPW,
                     IM_DOC_TAX IDT
                WHERE L_PRORATE_ACROSS_TAX_CODES = REIM_POSTING_SQL.YN_YES
                AND   IPW.PRORATE_WORKSPACE_ID = L_BY_DEPTCLASS
                AND   IPD.POSTING_ID           = I_POSTING_ID
                AND   IPD.POSTING_ID           = REIM_PKG_UTIL_SQL.GET_TOKEN(IPW.SHARE_DATA_1, TDELIM, 1)
                AND   IPD.DOC_ID               = REIM_PKG_UTIL_SQL.GET_TOKEN(IPW.SHARE_DATA_1, TDELIM, 2)
                AND   IDT.DOC_ID               = IPD.DOC_ID
        ) WHERE SHARE_IN_GROUP_1 <> 0
        UNION ALL
        -- subquery:  when there's NO IM_DOC_TAX
        SELECT L_PRORATE_TAX_ID   AS PRORATE_WORKSPACE_ID,
               IPW.SHARE_DATA_1   AS PRORATE_GROUP_KEY,
               IPD.DECIMAL_DIGITS AS DECIMAL_DIGITS,
               IPW.RESULT_PRORATED_AMOUNT_1 AS AMOUNT_TO_PRORATE_1,
               1 AS SHARE_IN_GROUP_1,
                 IPD.POSTING_ID||TDELIM||
                 IPD.DOC_ID||TDELIM||
                 REIM_PKG_UTIL_SQL.GET_TOKEN(IPW.SHARE_DATA_1, TDELIM, 3)||TDELIM||  -- DEPARTMENT
                 REIM_PKG_UTIL_SQL.GET_TOKEN(IPW.SHARE_DATA_1, TDELIM, 4)||TDELIM|| -- CLASS
                 ''||TDELIM||
                 0 AS SHARE_DATA_1
        FROM IM_POSTING_DOC IPD,
             IM_PRORATE_WORKSPACE IPW
        WHERE L_PRORATE_ACROSS_TAX_CODES = REIM_POSTING_SQL.YN_YES
        AND   IPW.PRORATE_WORKSPACE_ID = L_BY_DEPTCLASS
        AND   IPD.POSTING_ID           = I_POSTING_ID
        AND   IPD.POSTING_ID           = REIM_PKG_UTIL_SQL.GET_TOKEN(IPW.SHARE_DATA_1, TDELIM, 1)
        AND   IPD.DOC_ID               = REIM_PKG_UTIL_SQL.GET_TOKEN(IPW.SHARE_DATA_1, TDELIM, 2)
        AND   NOT EXISTS (SELECT 1 FROM IM_DOC_TAX IDT WHERE IDT.DOC_ID = IPD.DOC_ID)
        ;

        REIM_PRORATE_SQL.DO_PRORATION(L_PRORATE_TAX_ID);

        LOG_DEBUG_PRORATE_WORKSPACE
            (I_POSTING_ID,
             L_PRORATE_TAX_ID,
             L_PROGRAM||TDELIM||I_AMOUNT_TYPE||' by TAX Proration ');


        INSERT INTO IM_POSTING_DOC_AMOUNTS IPDA
            ( POSTING_AMT_ID,
            POSTING_ID,
            DOC_ID,
            AMOUNT_TYPE,
            LOCATION,
            ORG_UNIT,
            SET_OF_BOOKS_ID,
            DEPT,
            CLASS,
            TAX_CODE,
            TAX_RATE,
            GROUP_BY_1,
            GROUP_BY_2,
            GROUP_BY_3,
            GROUP_BY_4,
            GROUP_BY_5,
            GROUP_BY_6,
            GROUP_BY_7,
            GROUP_BY_8,
            AMOUNT,
            ACCOUNT_TYPE,
            ACCOUNT_CODE,
            --AMOUNT_VAT,
            PRIM_CURRENCY_CODE)
        SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                        IPD.POSTING_ID,
                IPD.DOC_ID,
                I_AMOUNT_TYPE,
                        IPD.LOCATION,
              (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = IPD.LOCATION AND ROWNUM < 2  ) AS ORG_UNIT,
                IPD.SET_OF_BOOKS_ID,
                IPW.DEPT,
                IPW.CLASS,
                IPW.TAX_CODE,
                IPW.TAX_RATE,
                NO_VALUE AS GROUP_BY_1,
                NO_VALUE AS GROUP_BY_2,
                NO_VALUE AS GROUP_BY_3,
                NO_VALUE AS GROUP_BY_4,
                NO_VALUE AS GROUP_BY_5,
                NO_VALUE AS GROUP_BY_6,
                NO_VALUE AS GROUP_BY_7,
                NO_VALUE AS GROUP_BY_8,
                IPW.RESULT_PRORATED_AMOUNT_1 AS AMOUNT,
                BASIC_TRANSACTIONS AS ACCOUNT_TYPE,
                DECODE(I_AMOUNT_TYPE,AMT_MATCHED_RECEIPT_COST,UNMATCHED_RECEIPT,VARIANCE_WITHIN_TOLERANCE) AS  ACCOUNT_CODE,
                NVL(IPD.SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = IPD.LOCATION) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
        FROM (
                SELECT  REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 1) AS POSTING_ID,
                        REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 2) AS DOC_ID,
                        REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 3) AS DEPT,
                        REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 4) AS CLASS,
                        REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 5) AS TAX_CODE,
                        TO_NUMBER(REIM_PKG_UTIL_SQL.GET_TOKEN(SHARE_DATA_1, TDELIM, 6)) AS TAX_RATE,
                        RESULT_PRORATED_AMOUNT_1
                FROM IM_PRORATE_WORKSPACE
                WHERE PRORATE_WORKSPACE_ID = L_PRORATE_TAX_ID
               ) IPW,
             IM_POSTING_DOC IPD
        WHERE L_PRORATE_ACROSS_TAX_CODES = REIM_POSTING_SQL.YN_YES
        AND   IPW.POSTING_ID = I_POSTING_ID
        AND   IPD.POSTING_ID = IPW.POSTING_ID
        AND   IPD.DOC_ID     = IPW.DOC_ID
        ;

        REIM_PRORATE_SQL.END_PRORATION(L_BY_DEPTCLASS);
        REIM_PRORATE_SQL.END_PRORATION(L_PRORATE_TAX_ID);

        LOG_DEBUG
            (I_POSTING_ID,
             L_PROGRAM||TDELIM||'END PRORATE_DOC_AMOUNT');

        RETURN SUCCESS;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;
    END PRORATE_DOC_AMOUNT;


    /************************************************************
     * Supports PRORATE_DOC_AMOUNT.
     * This a system ref cursor for a query against all postable documents
     * to calculate UNR and VWT for each and join them against the departments
     * and classes they will be distributing to.
     ************************************************************/
    PROCEDURE GET_DOC_AMOUNTS (I_POSTING_ID NUMBER, C_DOC_AMOUNTS IN OUT SYS_REFCURSOR)
    IS
       L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.GET_DOC_AMOUNTS';

    BEGIN

    LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        OPEN C_DOC_AMOUNTS FOR
        SELECT  DOC_UNRS.*, DOC_DC.DEPT, DOC_DC.CLASS, DOC_DC.EXT_COST AS DEPT_CLASS_COST
        FROM
        -- Subquery:  Calculates the UNR amount by document (unr = doc amount - nonmerch + header vwt + detail vwt + resolution costs )
        (
                SELECT POSTING_ID,
                       DOC_ID,
                       DECIMAL_DIGITS,
                       DECODE(DETAIL_COST, 0, (HEADER_COST-NON_MERCH_COST), DETAIL_COST) +  HEADER_VWT + DETAIL_VWT + RESOLUTION_COST AS UNR,
                       DECODE(DETAIL_COST, 0, (HEADER_COST-NON_MERCH_COST), DETAIL_COST) AS DOC_COST,
                       (HEADER_VWT + DETAIL_VWT) AS DOC_VWT,
                       RESOLUTION_COST,
                       NON_MERCH_COST
                FROM (
                        -- Subquery:  Exposes the header total, detail total,  header vwt , detail vwt, and resolution amounts PER document...
                        SELECT   IPD.POSTING_ID,
                                 IPD.DOC_ID,
                                 IPD.DECIMAL_DIGITS,
                                 IDH.TOTAL_COST AS HEADER_COST,
                                 NVL((SELECT SUM(QTY * UNIT_COST) FROM IM_INVOICE_DETAIL WHERE DOC_ID = IPD.DOC_ID), 0) DETAIL_COST,
                                 NVL((SELECT SUM(NON_MERCH_AMT) FROM IM_DOC_NON_MERCH WHERE DOC_ID = IPD.DOC_ID), 0) NON_MERCH_COST,
                                 NVL(IDH.VARIANCE_WITHIN_TOLERANCE, 0) HEADER_VWT,
                                 NVL((
                                    SELECT SUM(
                                                (NVL(IID.COST_VARIANCE_WITHIN_TOLERANCE,0) * IID.QTY) +
                                                (NVL(IID.QTY_VARIANCE_WITHIN_TOLERANCE,0) * IID.RESOLUTION_ADJUSTED_UNIT_COST)
                                              )
                                    FROM IM_INVOICE_DETAIL IID
                                    WHERE DOC_ID = IPD.DOC_ID), 0) DETAIL_VWT,
                                 NVL((
                                    SELECT SUM(
                                    (CASE
                                    WHEN IRA.ACTION IN ('DMTI', 'DMTF') THEN
                                    -1
                                    ELSE
                                    1 END)*REIM_POSTING_SQL.CALC_RESOLUTION_AMOUNT(IRA.ROWID, NULL))
                                    FROM IM_RESOLUTION_ACTION IRA
                                    WHERE IRA.DOC_ID = IPD.DOC_ID
                                    AND   IS_ACCOUNTABLE_RESOLUTION(IRA.ACTION) = YN_YES), 0) AS RESOLUTION_COST
                        FROM     IM_POSTING_DOC IPD,
                                 IM_DOC_HEAD    IDH
                        WHERE    IPD.POSTING_ID = I_POSTING_ID

                        AND      IDH.DOC_ID = IPD.DOC_ID
                )
        ) DOC_UNRS,
        -- Subquery:  Exposes the departments and classes each document will distribute its UNR to...
        (
                SELECT  POSTING_ID,
                        DOC_ID,
                        DEPT,
                        CLASS,
                        SUM(QTY_MATCHED*UNIT_COST) EXT_COST
                FROM (

                    SELECT  UNIQUE IPD.POSTING_ID,
                            IPD.DOC_ID,
                            IRIP.ITEM,
                            IM.DEPT,
                            IM.CLASS,
                            -- Unit cost from shipsku
                            (
                                SELECT UNIT_COST
                                FROM V_IM_SHIPSKU
                                WHERE SHIPMENT = IRIP.SHIPMENT
                                AND ITEM = IRIP.ITEM
                                AND ROWNUM = 1
                            ) AS UNIT_COST,
                            -- qty_matched from IRIP.  If not available, try summing shipsku qty_matched / qty_received
                            CASE
                               WHEN IRIP.QTY_MATCHED IS NULL OR IRIP.QTY_MATCHED = 0 THEN
                                    NVL(
                                            (
                                                SELECT Sum(IID.RESOLUTION_ADJUSTED_QTY)
                                                FROM IM_INVOICE_DETAIL IID
                                                WHERE IID.DOC_ID = IPD.DOC_ID
                                                AND   IID.ITEM = IRIP.ITEM
                                            ),

                                            (
                                                SELECT SUM(NVL(SS.QTY_MATCHED, SS.QTY_RECEIVED))
                                                FROM SHIPSKU SS
                                                WHERE SS.SHIPMENT = IRIP.SHIPMENT
                                                AND   SS.ITEM     = IRIP.ITEM
                                            )
                                        )
                               ELSE
                                    IRIP.QTY_MATCHED
                            END AS QTY_MATCHED
                    FROM IM_POSTING_DOC IPD,
                         IM_RCPT_ITEM_POSTING_INVOICE IRIPI,
                         IM_RECEIPT_ITEM_POSTING IRIP,
                         ITEM_MASTER IM
                    WHERE IPD.POSTING_ID = I_POSTING_ID

                    AND   IRIPI.DOC_ID   = IPD.DOC_ID
                    AND   IRIP.SEQ_NO    = IRIPI.SEQ_NO
                    AND   IM.ITEM        = IRIP.ITEM
                ) GROUP BY POSTING_ID, DOC_ID, DEPT, CLASS
        ) DOC_DC
        WHERE DOC_UNRS.POSTING_ID = DOC_DC.POSTING_ID
         AND   DOC_UNRS.DOC_ID = DOC_DC.DOC_ID;

    LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM);

    END;


/*******************

Checks to make sure that the DEBIT amounts = CREDIT amounts in IM_FINANCIALS_STAGE.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID:         Optional document ID to be checked.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION CHK_DB_CR_BALANCE_GL_STAGE
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE,
            I_DOC_ID           IN IM_POSTING_DOC.DOC_ID%TYPE DEFAULT NULL)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CHK_DB_CR_BALANCE_GL_STAGE';

        STATUS NUMBER(10) := SUCCESS;
    L_BALANCE_TOLERANCE IM_SYSTEM_OPTIONS.BALANCING_TOLERANCE%TYPE;
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

        CURSOR CHK_BALANCE_RS IS
                  SELECT POSTING_ID,
                           DOC_ID,
                           SIGNED_AMT,
                           TRANS_AMOUNT,
                           PRIM_TRANS_AMOUNT,
                           'Sum of Debit and Credit amounts must be zero.  Posted debit/credit differ by '
                           ||SIGNED_AMT
                           ||' for untaxed amount,'
                           ||TRANS_AMOUNT
                           ||' for signed transaction amount and by'
                           ||PRIM_TRANS_AMOUNT
                           ||' for signed transaction amount in primary currency' MSG
                  FROM   (SELECT   IFS.POSTING_ID,
                                         IFS.DOC_ID,
                                         IPD.DECIMAL_DIGITS,
                                         SUM(DECODE(IFS.DEBIT_CREDIT_IND,'DEBIT',IFS.AMOUNT,
                                                                                   -1 * IFS.AMOUNT)) AS SIGNED_AMT,
                                         SUM(TRANS_AMOUNT) AS TRANS_AMOUNT,
                                         SUM(PRIM_TRANS_AMOUNT) AS PRIM_TRANS_AMOUNT
                            FROM     IM_FINANCIALS_STAGE IFS,
                                         IM_POSTING_DOC IPD
                            WHERE    IFS.POSTING_ID = IPD.POSTING_ID
                                         AND IPD.POSTING_ID = I_POSTING_ID
                                         AND IPD.DOC_ID = NVL(I_DOC_ID,IPD.DOC_ID)
                                         AND IFS.DOC_ID = NVL(I_DOC_ID,IFS.DOC_ID)
                                         AND IFS.DOC_ID = IPD.DOC_ID
                            GROUP BY IFS.POSTING_ID,IFS.DOC_ID, IPD.DECIMAL_DIGITS) BALANCE
                  WHERE
                  ABS(ROUND(BALANCE.SIGNED_AMT, BALANCE.DECIMAL_DIGITS)) >= 1/POWER(10, BALANCE.DECIMAL_DIGITS)
                  OR ABS(ROUND(BALANCE.TRANS_AMOUNT, BALANCE.DECIMAL_DIGITS)) >= 1/POWER(10, BALANCE.DECIMAL_DIGITS)
                  OR ABS(ROUND(BALANCE.PRIM_TRANS_AMOUNT, BALANCE.DECIMAL_DIGITS)) >= 1/POWER(10, BALANCE.DECIMAL_DIGITS);


        CURSOR C_ORDER_REC IS
        SELECT * FROM (
                  SELECT ROWID, DOC_ID, ORDER_NO, RECEIPT_ID, VAT_CODE, DECODE(TRAN_CODE,REIM_POSTING_SQL.TAX,TAX_ENTRY,REIM_POSTING_SQL.TAX_NON_DYNAMIC,TAX_ENTRY,'NMRCH',TAX_ENTRY,REIM_POSTING_SQL.YN_NO) TAX_ENTRY,
                  DECODE(TRAN_CODE, 'NMRCH','TAXN',TRAN_CODE) TRAN_CODE, SEQ_NO FROM  IM_FINANCIALS_STAGE
                  WHERE POSTING_ID = I_POSTING_ID)
                  ORDER BY DOC_ID, ORDER_NO, RECEIPT_ID, VAT_CODE, TAX_ENTRY, TRAN_CODE, SEQ_NO;

    BEGIN
      SELECT NVL((SELECT BALANCING_TOLERANCE FROM IM_SYSTEM_OPTIONS), 0), NVL((SELECT PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS),REIM_POSTING_SQL.YN_NO)
            INTO L_BALANCE_TOLERANCE, L_PRORATE_ACROSS_TAX_CODES
            FROM DUAL;

          --Order financials_stage lines when prorate_across_tax_codes is set to 'Y'
            IF (L_PRORATE_ACROSS_TAX_CODES = REIM_POSTING_SQL.YN_YES) THEN

                FOR REC IN C_ORDER_REC LOOP

                    UPDATE IM_FINANCIALS_STAGE
                            SET SEQ_NO = IM_FINANCIALS_STAGE_SEQ.NEXTVAL,
                                VAT_CODE = DECODE(TRAN_CODE,
                                                  REIM_POSTING_SQL.TAX, VAT_CODE,
                                                  REIM_POSTING_SQL.TAX_NON_DYNAMIC, VAT_CODE,
                                                  REIM_POSTING_SQL.TAX_ACQUISITION, VAT_CODE,
                                                  REIM_POSTING_SQL.TAX_ACQUISITION_OFFSET, VAT_CODE,
                                                  REIM_POSTING_SQL.TAX_REVERSE_CHARGE, VAT_CODE,
                                                  REIM_POSTING_SQL.TAX_REVERSE_CHARGE_OFFSET, VAT_CODE,
                                                  NULL),
                                VAT_RATE = DECODE(TRAN_CODE,
                                                  REIM_POSTING_SQL.TAX, VAT_RATE,
                                                  REIM_POSTING_SQL.TAX_NON_DYNAMIC, VAT_RATE,
                                                  REIM_POSTING_SQL.TAX_ACQUISITION, VAT_RATE,
                                                  REIM_POSTING_SQL.TAX_ACQUISITION_OFFSET, VAT_RATE,
                                                  REIM_POSTING_SQL.TAX_REVERSE_CHARGE, VAT_RATE,
                                                  REIM_POSTING_SQL.TAX_REVERSE_CHARGE_OFFSET, VAT_RATE,
                                                  NULL),
                                TAX_ENTRY = DECODE(TRAN_CODE,
                                                  REIM_POSTING_SQL.TAX,TAX_ENTRY,
                                                  REIM_POSTING_SQL.TAX_NON_DYNAMIC, TAX_ENTRY,
                                                  REIM_POSTING_SQL.TAX_ACQUISITION, TAX_ENTRY,
                                                  REIM_POSTING_SQL.TAX_ACQUISITION_OFFSET, TAX_ENTRY,
                                                  REIM_POSTING_SQL.TAX_REVERSE_CHARGE, TAX_ENTRY,
                                                  REIM_POSTING_SQL.TAX_REVERSE_CHARGE_OFFSET, TAX_ENTRY,
                                                  REIM_POSTING_SQL.YN_NO)
                            WHERE SEQ_NO = REC.SEQ_NO;

                END LOOP;

            END IF;

            FOR REC IN CHK_BALANCE_RS
            LOOP
                IF  (abs(REC.SIGNED_AMT)        >= L_BALANCE_TOLERANCE OR
                     abs(REC.TRANS_AMOUNT)      >= L_BALANCE_TOLERANCE OR
                     abs(REC.PRIM_TRANS_AMOUNT) >= L_BALANCE_TOLERANCE) THEN
                  STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE, REC.POSTING_ID, REC.DOC_ID, SEVERITY_ERROR, ERROR_CAT_BALANCING, REC.MSG);
                ELSE
                  STATUS := REIM_POSTING_SQL.ADJ_VARIANCE_GL_STAGE(O_ERROR_MESSAGE, REC.POSTING_ID, REC.DOC_ID, L_BALANCE_TOLERANCE, REC.SIGNED_AMT, REC.TRANS_AMOUNT, REC.PRIM_TRANS_AMOUNT);
                END IF;
            END LOOP;

            RETURN STATUS;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END CHK_DB_CR_BALANCE_GL_STAGE;

    /*******************

    Adjusts the balancing variance with the record having max amount.

      Parameters:
      O_ERROR_MESSAGE:  Error message output
      I_POSTING_ID:     ID reference to the new posting process.
      I_DOC_ID:         Optional document ID to be checked.

      Returns:
      SUCCESS/FAIL

    *******************/
    FUNCTION ADJ_VARIANCE_GL_STAGE ( O_ERROR_MESSAGE      OUT  VARCHAR2,
                                     I_POSTING_ID         IN   IM_POSTING_DOC.POSTING_ID%TYPE,
                                     I_DOC_ID             IN   IM_POSTING_DOC.DOC_ID%TYPE DEFAULT NULL,
                                     I_BALANCE_TOLERANCE  IN   IM_SYSTEM_OPTIONS.BALANCING_TOLERANCE%TYPE,
                                     I_SIGNED_AMT         IN   IM_FINANCIALS_STAGE.AMOUNT%TYPE,
                                     I_TRANS_AMOUNT       IN   IM_FINANCIALS_STAGE.TRANS_AMOUNT%TYPE,
                                     I_PRIM_TRANS_AMOUNT  IN   IM_FINANCIALS_STAGE.PRIM_TRANS_AMOUNT%TYPE)
        RETURN NUMBER
        IS

            L_PROGRAM VARCHAR2(50)  :=  'REIM_POSTING_SQL.ADJ_VARIANCE_GL_STAGE';
            L_MAX_AMT                   IM_FINANCIALS_STAGE.AMOUNT%TYPE;
            L_TRAN_CODE                 IM_FINANCIALS_STAGE.TRAN_CODE%TYPE;
            L_DEBIT_CREDIT_IND          IM_FINANCIALS_STAGE.DEBIT_CREDIT_IND%TYPE;
            STATUS NUMBER(10)       :=  SUCCESS;

            CURSOR C_MAX_AMT_RS IS
                SELECT AMOUNT,TRAN_CODE, DEBIT_CREDIT_IND
                FROM IM_FINANCIALS_STAGE IFS
                WHERE  IFS.TRAN_CODE <> 'TAP'
                AND IFS.TAX_ENTRY = 'N'
                AND IFS.DOC_ID = Nvl(I_DOC_ID, IFS.DOC_ID)
                AND IFS.POSTING_ID = I_POSTING_ID
                ORDER BY AMOUNT DESC;

        BEGIN

            FOR REC IN C_MAX_AMT_RS
            LOOP

                L_MAX_AMT          :=   REC.AMOUNT;
                L_TRAN_CODE        :=   REC.TRAN_CODE;
                L_DEBIT_CREDIT_IND :=   REC.DEBIT_CREDIT_IND;
                EXIT;

            END LOOP;

            UPDATE  IM_FINANCIALS_STAGE
            SET
                AMOUNT = DECODE(L_DEBIT_CREDIT_IND, 'DEBIT', AMOUNT - (I_SIGNED_AMT), AMOUNT + (I_SIGNED_AMT)),
                TRANS_AMOUNT = DECODE(L_DEBIT_CREDIT_IND, 'DEBIT', TRANS_AMOUNT + (I_TRANS_AMOUNT), TRANS_AMOUNT - (I_TRANS_AMOUNT)),
                PRIM_TRANS_AMOUNT = DECODE(L_DEBIT_CREDIT_IND, 'DEBIT', PRIM_TRANS_AMOUNT + (I_PRIM_TRANS_AMOUNT), PRIM_TRANS_AMOUNT - (I_PRIM_TRANS_AMOUNT))
                WHERE  AMOUNT           =  L_MAX_AMT
                AND TRAN_CODE           =  L_TRAN_CODE
                AND DEBIT_CREDIT_IND    =  L_DEBIT_CREDIT_IND
                AND DOC_ID              =  Nvl(I_DOC_ID, DOC_ID)
                AND POSTING_ID          =  I_POSTING_ID
                AND TRAN_CODE           <> 'TAP'
                AND TAX_ENTRY           =  'N';

            STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE,
                               I_POSTING_ID,
                               I_DOC_ID,
                               SEVERITY_WARNING,
                               ERROR_CAT_BALANCING,
                               '(GL CREDIT - DEBIT amounts) <= balancing tolerance '||I_BALANCE_TOLERANCE||
                               '.  IM_FINANCIALS_STAGE entry for TRAN_CODE '||L_TRAN_CODE||
                               ' was adjusted by '|| I_SIGNED_AMT || ' from the original amount of '|| L_MAX_AMT ||'.');


            RETURN STATUS;


        EXCEPTION
            WHEN OTHERS THEN
            O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                           ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                        TO_CHAR(SQLCODE));

            RETURN FAIL;


        END ADJ_VARIANCE_GL_STAGE;

/*******************

Checks to make sure that the header amount = detail amounts for IM_AP_STAGE_HEAD/DETAIL.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID:         Optional document ID to check.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION CHK_DB_CR_BALANCE_AP_STAGE
           (O_ERROR_MESSAGE  OUT VARCHAR2,
            I_POSTING_ID     IN IM_POSTING_DOC.POSTING_ID%TYPE,
            I_DOC_ID           IN IM_POSTING_DOC.DOC_ID%TYPE DEFAULT NULL)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CHK_DB_CR_BALANCE_AP_STAGE';
        STATUS NUMBER(10) := SUCCESS;

        L_CALC_TOLERANCE NUMBER := 0.0 ;

        L_ADJUSTED_DETAIL_ROW ROWID;
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

        L_PRE_DOC_ID IM_AP_STAGE_DETAIL.DOC_ID%TYPE := 0;

        L_PRE_TRAN_CODE IM_AP_STAGE_DETAIL.TRAN_CODE%TYPE := NULL;

        L_LINE_GROUP_NO IM_AP_STAGE_DETAIL.LINE_GROUP_NO%TYPE := NULL;

        CURSOR CHK_ORCL_SITE_ID IS
          SELECT IASH.POSTING_ID,
          IASH.DOC_ID,
          OH.IMPORT_ID IMPORT_ID,
          IDH.VENDOR IDH_SUPP
          FROM IM_AP_STAGE_HEAD IASH,
          IM_DOC_HEAD IDH,
          ORDHEAD OH
          WHERE OH.ORDER_NO = IDH.ORDER_NO
          AND OH.IMPORT_ID IS NOT NULL
          AND IDH.DOC_ID = IASH.DOC_ID
          AND IASH.POSTING_ID = I_POSTING_ID
          AND IASH.ORACLE_SITE_ID IS NULL;



        L_BALANCE_TOLERANCE IM_SYSTEM_OPTIONS.BALANCING_TOLERANCE%TYPE;
        CURSOR CHK_BALANCE_RS IS
                SELECT IASH.POSTING_ID,
                        IASH.DOC_ID,
                        ' Header amount of '
                        ||IASH.AMOUNT
                        ||' does not equal to total detail amount of '
                        ||DETAIL_AMT AS MSG,
                        Abs(IASH.AMOUNT - DETAILS.DETAIL_AMT) Variance,
                        DETAILS.DECIMAL_DIGITS
                FROM   (SELECT POSTING_ID, DOC_ID, SUM(AMOUNT) AS AMOUNT FROM IM_AP_STAGE_HEAD GROUP BY POSTING_ID, DOC_ID) IASH,
                        (SELECT   IASD.POSTING_ID    AS POSTING_ID,
                                    IASD.DOC_ID,
                                    SUM(IASD.AMOUNT)   DETAIL_AMT,
                                    ipd.decimal_digits
                          FROM     IM_AP_STAGE_DETAIL IASD,
                                    IM_POSTING_DOC IPD
                          WHERE    IPD.POSTING_ID = I_POSTING_ID
                                    AND IPD.DOC_ID = NVL(I_DOC_ID,IPD.DOC_ID)
                                    AND IASD.POSTING_ID = IPD.POSTING_ID
                                    AND IASD.DOC_ID = IPD.DOC_ID
                          GROUP BY IASD.POSTING_ID,IASD.DOC_ID, ipd.decimal_digits) DETAILS
                WHERE  IASH.POSTING_ID = DETAILS.POSTING_ID
                        AND IASH.DOC_ID = DETAILS.DOC_ID
                        AND ROUND(ABS(IASH.AMOUNT - DETAILS.DETAIL_AMT), DETAILS.DECIMAL_DIGITS)>= 1 / POWER(10, DETAILS.DECIMAL_DIGITS);

         CURSOR CHK_ACCTS_PAYABLE IS
                SELECT IASH.POSTING_ID,
                        IASH.DOC_ID,
                        'Posted TAP of '
                        ||IASH.AMOUNT
                        ||' does not equal to document amount of  '
                        ||IDH.TOTAL_COST_INC_TAX
                        ||' (Variance = '
                        ||ABS(  ROUND(IASH.AMOUNT, ipd.decimal_digits) - ROUND(IDH.TOTAL_COST_INC_TAX, ipd.decimal_digits))
                        ||' ) '
                        AS MSG
                FROM IM_POSTING_DOC IPD,
                     IM_AP_STAGE_HEAD IASH,
                     IM_DOC_HEAD IDH
                WHERE IPD.POSTING_ID = I_POSTING_ID
                AND IPD.DOC_ID = NVL(I_DOC_ID,IPD.DOC_ID)
                AND IASH.POSTING_ID = IPD.POSTING_ID
                AND IASH.DOC_ID = IPD.DOC_ID
                AND IDH.DOC_ID = IASH.DOC_ID
                AND ABS(  ROUND(IASH.AMOUNT, ipd.decimal_digits) - ROUND(IDH.TOTAL_COST_INC_TAX, ipd.decimal_digits)) <> 0;

         CURSOR C_ORDER_REC IS
                SELECT ROWID, DOC_ID, VAT_CODE,LINE_TYPE_LOOKUP_CODE,TRAN_CODE,SEQ_NO
                  FROM (SELECT ROWID, DOC_ID, VAT_CODE, Decode(TRAN_CODE,'NMRCH','TAX',LINE_TYPE_LOOKUP_CODE) AS LINE_TYPE_LOOKUP_CODE,
                               Decode(TRAN_CODE,'NMRCH','TAXN',TRAN_CODE) AS TRAN_CODE, SEQ_NO
                        FROM  IM_AP_STAGE_DETAIL
                        WHERE POSTING_ID = I_POSTING_ID)
                        ORDER BY DOC_ID, VAT_CODE, LINE_TYPE_LOOKUP_CODE, TRAN_CODE, SEQ_NO;

         CURSOR C_GET_SYS_OPTIONS IS
                SELECT NVL(BALANCING_TOLERANCE, 0),
                       NVL(PRORATE_ACROSS_TAX_CODES, REIM_POSTING_SQL.YN_NO)
                  FROM IM_SYSTEM_OPTIONS;


    BEGIN

            OPEN C_GET_SYS_OPTIONS;
            FETCH C_GET_SYS_OPTIONS INTO L_BALANCE_TOLERANCE, L_PRORATE_ACROSS_TAX_CODES;
            CLOSE C_GET_SYS_OPTIONS;

            --Order ap_stage_detail lines when prorate_across_tax_codes is set to 'Y'
            IF (L_PRORATE_ACROSS_TAX_CODES = REIM_POSTING_SQL.YN_YES) THEN
                FOR REC IN C_ORDER_REC
                    LOOP

                        IF (REC.DOC_ID <> L_PRE_DOC_ID OR
                            (REC.TRAN_CODE NOT IN (REIM_POSTING_SQL.TAX,
                                                   REIM_POSTING_SQL.TAX_NON_DYNAMIC,
                                                   REIM_POSTING_SQL.TAX_ACQUISITION,
                                                   REIM_POSTING_SQL.TAX_ACQUISITION_OFFSET,
                                                   REIM_POSTING_SQL.TAX_REVERSE_CHARGE_OFFSET) AND
                             L_PRE_TRAN_CODE IN (REIM_POSTING_SQL.TAX,
                                                 REIM_POSTING_SQL.TAX_NON_DYNAMIC,
                                                 REIM_POSTING_SQL.TAX_ACQUISITION,
                                                 REIM_POSTING_SQL.TAX_ACQUISITION_OFFSET,
                                                 REIM_POSTING_SQL.TAX_REVERSE_CHARGE_OFFSET))) THEN
                                L_LINE_GROUP_NO := IM_AP_STG_DTL_LINE_GROUP_SEQ.NEXTVAL;
                        END IF;

                        UPDATE IM_AP_STAGE_DETAIL
                        SET SEQ_NO = IM_AP_STAGE_DETAIL_SEQ.NEXTVAL,
                            LINE_GROUP_NO = L_LINE_GROUP_NO,
                            VAT_CODE = DECODE(LINE_TYPE_LOOKUP_CODE,
                                              REIM_POSTING_SQL.TAX, VAT_CODE,
                                              NULL),
                            VAT_RATE = DECODE(LINE_TYPE_LOOKUP_CODE,
                                              REIM_POSTING_SQL.TAX, VAT_RATE,
                                              NULL),
                            PRORATE_ACROSS_FLAG = DECODE(TRAN_CODE,
                                                         REIM_POSTING_SQL.TAX, REIM_POSTING_SQL.YN_YES,
                                                         REIM_POSTING_SQL.TAX_NON_DYNAMIC, REIM_POSTING_SQL.YN_YES,
                                                         REIM_POSTING_SQL.TAX_ACQUISITION, REIM_POSTING_SQL.YN_YES,
                                                         REIM_POSTING_SQL.TAX_ACQUISITION_OFFSET, REIM_POSTING_SQL.YN_YES,
                                                         REIM_POSTING_SQL.TAX_REVERSE_CHARGE, REIM_POSTING_SQL.YN_YES,
                                                         REIM_POSTING_SQL.TAX_REVERSE_CHARGE_OFFSET, REIM_POSTING_SQL.YN_YES,
                                                         REIM_POSTING_SQL.YN_NO)
                        WHERE ROWID = REC.ROWID;

                        L_PRE_DOC_ID := REC.DOC_ID;
                        L_PRE_TRAN_CODE := REC.TRAN_CODE;

                    END LOOP;
             END IF;

            FOR REC IN CHK_ORCL_SITE_ID LOOP

              STATUS := REIM_POSTING_SQL.POPULATE_SITE_ID(O_ERROR_MESSAGE, REC.POSTING_ID, REC.DOC_ID,  REC.IMPORT_ID, REC.IDH_SUPP);

            END LOOP;



            SELECT calc_tolerance INTO L_CALC_TOLERANCE FROM im_system_options ;


            FOR REC IN CHK_BALANCE_RS
            LOOP
                  IF ( abs(REC.VARIANCE) > L_BALANCE_TOLERANCE) THEN
                      STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE,
                                                                       REC.POSTING_ID,
                                                                       REC.DOC_ID,
                                                                       SEVERITY_ERROR,
                                                                       ERROR_CAT_BALANCING,
                                                                       REC.MSG);
                  ELSE
                      -- Look for a tax row to adjust
                      SELECT
                          (SELECT ROWID
                          FROM IM_AP_STAGE_DETAIL IASD
                          WHERE IASD.POSTING_ID = I_POSTING_ID
                          AND   IASD.DOC_ID = REC.doc_id
                          AND   IASD.LINE_TYPE_LOOKUP_CODE = 'TAX'
                          AND   IASD.AMOUNT <> 0
                          AND   ROWNUM < 2)
                      INTO L_ADJUSTED_DETAIL_ROW
                      FROM DUAL;

                      --  If no tax row exists,  pick any detail...
                      IF L_ADJUSTED_DETAIL_ROW IS NULL THEN

                          SELECT
                              (SELECT ROWID
                              FROM IM_AP_STAGE_DETAIL IASD
                              WHERE IASD.POSTING_ID = I_POSTING_ID
                              AND   IASD.DOC_ID = REC.doc_id
                              AND   IASD.AMOUNT <> 0
                              AND   ROWNUM < 2)
                          INTO L_ADJUSTED_DETAIL_ROW
                          FROM DUAL;

                  END IF;

                      IF L_ADJUSTED_DETAIL_ROW IS NOT NULL THEN

                          FOR ADJREC IN (SELECT * FROM IM_AP_STAGE_DETAIL WHERE ROWID = L_ADJUSTED_DETAIL_ROW) LOOP
                              STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE,
                                             ADJREC.POSTING_ID,
                                             ADJREC.DOC_ID,
                                             SEVERITY_WARNING,
                                             ERROR_CAT_BALANCING,
                                             '(A/P Header - total detail amounts) <= balancing tolerance of '|| L_BALANCE_TOLERANCE||
                                             '.  IM_AP_STAGE_DETAIL entry for TRAN_CODE '||ADJREC.TRAN_CODE||', LINE_TYPE_LOOKUP_CODE '||ADJREC.LINE_TYPE_LOOKUP_CODE||', and SEQ_NO '||ADJREC.SEQ_NO||
                                             ' was adjusted by '||rec.variance|| ' from the original amount of '||ADJREC.AMOUNT||'.');
            END LOOP;

                          UPDATE IM_AP_STAGE_DETAIL IASD
                          SET IASD.AMOUNT = IASD.AMOUNT + REC.VARIANCE
                          WHERE ROWID = L_ADJUSTED_DETAIL_ROW;

                      END IF;
                  END IF;
            END LOOP;


            IF L_CALC_TOLERANCE = 0.0 THEN

                FOR REC IN CHK_ACCTS_PAYABLE
                LOOP
                      STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE, REC.POSTING_ID, REC.DOC_ID, SEVERITY_ERROR, ERROR_CAT_BALANCING, REC.MSG);
                END LOOP;
            END IF;

            LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM ||' DOC ID - > '|| I_DOC_ID );

            RETURN STATUS;

            LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM ||' DOC ID - > '|| I_DOC_ID );

        RETURN SUCCESS;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM||' DOC ID - > '|| I_DOC_ID,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END CHK_DB_CR_BALANCE_AP_STAGE;


FUNCTION POPULATE_SITE_ID(O_ERROR_MESSAGE  OUT VARCHAR2,
                             I_POSTING_ID  IN IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
                             I_DOC_ID  IN IM_POSTING_DOC_ERRORS.DOC_ID%TYPE,
                             I_IMPORT_ID IN ORDHEAD.IMPORT_ID%TYPE,
                             I_IDH_SUPP IN IM_DOC_HEAD.VENDOR%TYPE
                             )
    RETURN NUMBER
    IS
        L_STATUS NUMBER(10);
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.POPULATE_SITE_ID';
        L_ORACLE_SITE_ID IM_AP_STAGE_HEAD.ORACLE_SITE_ID%TYPE;
        L_ORG_UNIT IM_AP_STAGE_HEAD.ORG_UNIT%TYPE;


    BEGIN


        -- get all the supplier sites for the supplier in im_DOC_HEAD;
        -- get the org unit from wh for the V_wh of the im_doc_head's location
        -- for the vendor from im_doc_head get all the supplier sites
        -- from Partner_org_unit get the supplier site id and org_unit_id for the matching org_unit Id from the wh table.

        SELECT (CASE
                             WHEN basic.primary_pay_site = 'Y' THEN
                                  basic.partner
                             WHEN basic.primary_pay_site = 'N' THEN
                                  (SELECT /*+ FIRST_ROWS */
                                          TO_NUMBER(key_value_1)
                                     FROM ADDR ad
                                    WHERE basic.partner = TO_NUMBER(ad.key_value_1)
                                      AND ad.addr_type  = REIM_WRAPPER_SQL.ADDR_TYPE_6
                                      AND ad.module     = REIM_WRAPPER_SQL.MODULE_SUPP
                                      AND ROWNUM < 2)
                             END) AS ORACLE_SITE_ID,
                BASIC.ORG_UNIT_ID
        INTO  L_ORACLE_SITE_ID, L_ORG_UNIT
        FROM (
        SELECT POU.PARTNER, POU.ORG_UNIT_ID,
        NVL(pou.PRIMARY_PAY_SITE,'N') PRIMARY_PAY_SITE
        FROM SUPS, WH, PARTNER_ORG_UNIT POU
        WHERE SUPS.SUPPLIER_PARENT = I_IDH_SUPP
        AND WH.WH = I_IMPORT_ID
        AND SUPS.SUPPLIER = POU.PARTNER
        AND POU.ORG_UNIT_ID = WH.ORG_UNIT_ID AND ROWNUM<2

        UNION

        SELECT POU.PARTNER, POU.ORG_UNIT_ID,
        NVL(pou.PRIMARY_PAY_SITE,'N') PRIMARY_PAY_SITE
        FROM SUPS, STORE, PARTNER_ORG_UNIT POU
        WHERE SUPS.SUPPLIER_PARENT = I_IDH_SUPP
        AND STORE = I_IMPORT_ID
        AND SUPS.SUPPLIER = POU.PARTNER
        AND POU.ORG_UNIT_ID = STORE.ORG_UNIT_ID AND ROWNUM<2
         ) basic;

        UPDATE IM_AP_STAGE_HEAD IASH
        SET ORACLE_SITE_ID = L_ORACLE_SITE_ID,
         ORG_UNIT = L_ORG_UNIT
        WHERE  IASH.POSTING_ID = I_POSTING_ID
        AND IASH.DOC_ID = I_DOC_ID;

        UPDATE IM_AP_STAGE_DETAIL IASD SET ORG_UNIT = L_ORG_UNIT
        WHERE  IASD.POSTING_ID = I_POSTING_ID
        AND IASD.DOC_ID = I_DOC_ID;


        RETURN SUCCESS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END POPULATE_SITE_ID;



/*******************

Adds an entry in IM_POSTING_DOC_ERRORS table for the given posting ID and/or document ID.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the posting process.
  I_DOC_ID:         Document ID
  I_SEVERITY:       'ERROR', 'WARN', 'DEBUG'
  I_ERROR_MSG:      Meaningful message describing the error.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE  OUT VARCHAR2,
                             I_POSTING_ID  IN IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
                             I_DOC_ID  IN IM_POSTING_DOC_ERRORS.DOC_ID%TYPE ,
                             I_SEVERITY IN IM_POSTING_DOC_ERRORS.SEVERITY%TYPE,
                             I_CATEGORY IN IM_POSTING_DOC_ERRORS.CATEGORY%TYPE,
                             I_ERROR_MSG IN IM_POSTING_DOC_ERRORS.ERROR_MSG%TYPE)
    RETURN NUMBER
    IS
        L_STATUS NUMBER(10);
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR';
    BEGIN

        L_STATUS := ADD_ERROR_AUTO_TRAN (O_ERROR_MESSAGE, I_POSTING_ID, I_DOC_ID, I_SEVERITY, I_CATEGORY, I_ERROR_MSG);

        IF I_SEVERITY = SEVERITY_ERROR THEN

            IF I_DOC_ID IS NULL THEN
                UPDATE IM_POSTING_DOC SET HAS_ERRORS = YN_YES WHERE POSTING_ID = I_POSTING_ID;
            ELSE
                UPDATE IM_POSTING_DOC SET HAS_ERRORS = YN_YES WHERE POSTING_ID = I_POSTING_ID
                AND DOC_ID = I_DOC_ID;
            END IF;

            UPDATE IM_POSTING_STATUS SET HAS_ERRORS = YN_YES WHERE POSTING_ID = I_POSTING_ID;
        END IF;

        RETURN L_STATUS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM||' DOC ID - > '|| I_DOC_ID,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ADD_POSTING_DOC_ERROR;




/*******************

Adds an entry in IM_POSTING_DOC_ERRORS table for the given posting ID and/or document ID.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the posting process.
  I_DOC_ID:         Document ID
  I_SEVERITY:       'ERROR', 'WARN', 'DEBUG'
  I_ERROR_MSG:      Meaningful message describing the error.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION ADD_ERROR_AUTO_TRAN(O_ERROR_MESSAGE  OUT VARCHAR2,
                             I_POSTING_ID  IN IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
                             I_DOC_ID  IN IM_POSTING_DOC_ERRORS.DOC_ID%TYPE ,
                             I_SEVERITY IN IM_POSTING_DOC_ERRORS.SEVERITY%TYPE,
                             I_CATEGORY IN IM_POSTING_DOC_ERRORS.CATEGORY%TYPE,
                             I_ERROR_MSG IN IM_POSTING_DOC_ERRORS.ERROR_MSG%TYPE)
    RETURN NUMBER
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.ADD_ERROR_AUTO_TRAN';
    BEGIN

        INSERT INTO IM_POSTING_DOC_ERRORS (POSTING_ID, DOC_ID, SEVERITY, CATEGORY, CREATION_DATE, ERROR_MSG)
        VALUES(I_POSTING_ID, NVL(I_DOC_ID, -999999), I_SEVERITY, I_CATEGORY, SYSDATE, I_ERROR_MSG);
        COMMIT;

        RETURN SUCCESS;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM||' DOC ID - > '|| I_DOC_ID,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ADD_ERROR_AUTO_TRAN;



/*******************

Removes entries in IM_POSTING_DOC_ERRORS

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     Optional ID reference to the new posting process.
  I_DOC_ID:         Optional document ID criteria for deletion.
  I_SEVERITY:       optional severity criteria for deletion (ERROR, WARN, DEBUG).

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION DELETE_POSTING_DOC_ERROR(O_ERROR_MESSAGE  OUT VARCHAR2,
                                      I_POSTING_ID  IN IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
                                      I_DOC_ID  IN IM_POSTING_DOC_ERRORS.DOC_ID%TYPE DEFAULT NULL,
                                      I_SEVERITY IN IM_POSTING_DOC_ERRORS.SEVERITY%TYPE DEFAULT NULL)
    RETURN NUMBER
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.DELETE_POSTING_DOC_ERROR';
    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM ||' DOC ID - > '|| I_DOC_ID );

        DELETE FROM IM_POSTING_DOC_ERRORS IPDE
        WHERE IPDE.POSTING_ID = I_POSTING_ID
        AND IPDE.DOC_ID = NVL(I_DOC_ID, IPDE.DOC_ID)
        AND IPDE.SEVERITY = NVL(I_SEVERITY, IPDE.SEVERITY);
        COMMIT;

        LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM ||' DOC ID - > '|| I_DOC_ID );

        RETURN SUCCESS;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM||' DOC ID - > '|| I_DOC_ID,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END DELETE_POSTING_DOC_ERROR;





/*******************

Identifies Resolution Type documents for posting.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.
  I_DOC_TYPE_LIST:  Optional document type list to select
  I_DOC_STATUS_LIST:  Optional document status value selection criteria.
  I_DOC_HOLD_STATUS:  Optional hold status value selection criteria.
  I_DOC_RESOLUTION_ACTIONS:  Optional resolution action selection criteria.
  I_ROLLUP_STATUS:           Optional Rollup status selection criteria.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION PREPARE_RESOLUTIONS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                          I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                          I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                          I_DOC_TYPE_LIST IN OBJ_VARCHAR_ID_TABLE,
                                          I_DOC_STATUS_LIST IN OBJ_VARCHAR_ID_TABLE,
                                          I_DOC_HOLD_STATUS IN OBJ_VARCHAR_ID_TABLE,
                                          I_PRIM_CURRENCY_CODE IN SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                                          I_DOC_RESOLUTION_ACTIONS IN OBJ_VARCHAR_ID_TABLE DEFAULT NULL,
                                          I_ROLLUP_STATUS IN VARCHAR2 DEFAULT NULL)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_RESOLUTIONS';
        L_DOC_ID_COUNT  NUMBER(6);
    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        IF I_DOC_ID_LIST IS NULL
        THEN
          L_DOC_ID_COUNT := 0;
        ELSE
          L_DOC_ID_COUNT := I_DOC_ID_LIST.COUNT;
        END IF;

        INSERT INTO IM_POSTING_DOC
                     (POSTING_ID,
                        DOC_ID,
                        DOC_TYPE,
                        CALC_BASIS,
                        MATCH_TYPE,
                        RESOLUTION_COUNT,
                        DOC_TAX_COUNT,
                        PARENT_ID,
                        DOC_DATE,
                        VENDOR_TYPE,
                        VENDOR,
                        ORDER_NO,
                        BEST_TERMS,
                        BEST_TERMS_DATE,
                        MANUALLY_PAID_IND,
                        PRE_PAID_IND,
                        DEAL_ID,
                        EXT_DOC_ID,
                        CURRENCY_CODE,
                        LOCATION,
                        LOC_TYPE,
                        DECIMAL_DIGITS,
                        SYS_CURRENCY_CODE,
                        SYS_TAX_IND)
        (SELECT I_POSTING_ID,
                  IDH.DOC_ID,
                  IDH.TYPE,
                  -- Calculations are mostly based
                  -- on whether a document has details or not.
                  CASE
                    WHEN (SELECT COUNT(*)
                            FROM   IM_DOC_DETAIL_REASON_CODES IDDRC
                            WHERE  IDDRC.DOC_ID = IDH.DOC_ID) > 0 THEN REIM_POSTING_SQL.BASIS_DETAILS
                    ELSE REIM_POSTING_SQL.BASIS_HEADER
                  END CALC_BASIS,
                  MATCH_TYPE_UNKNOWN AS MATCH_TYPE,
                  NVL ((SELECT COUNT(*)
                            FROM   IM_DOC_DETAIL_REASON_CODES IDDRC
                            WHERE  IDDRC.DOC_ID = IDH.DOC_ID), 0)   AS RESOLUTION_COUNT,
                  NVL((SELECT COUNT(* )
                   FROM   IM_DOC_TAX
                   WHERE  DOC_ID = IDH.DOC_ID), 0) AS doc_tax_count,
                  IDH.PARENT_ID,
                  IDH.DOC_DATE,
                  IDH.VENDOR_TYPE,
                  IDH.VENDOR,
                  (case when idh.rtv_ind = 'Y' then idh.rtv_order_no else IDH.ORDER_NO end) order_no,
                  IDH.BEST_TERMS,
                  IDH.BEST_TERMS_DATE,
                  IDH.MANUALLY_PAID_IND,
                  IDH.PRE_PAID_IND,
                  IDH.DEAL_ID,
                  IDH.EXT_DOC_ID,
                  IDH.CURRENCY_CODE,
                  IDH.LOCATION,
                  IDH.LOC_TYPE,
                  NVL((SELECT MAX(ICL.CURRENCY_COST_DEC) FROM IM_CURRENCIES ICL WHERE ICL.CURRENCY_CODE = IDH.CURRENCY_CODE), REIM_POSTING_SQL.DEFAULT_DIGITS),
                  I_PRIM_CURRENCY_CODE,
                  DECODE(NVL(IDH.REVERSE_VAT_IND, REIM_POSTING_SQL.YN_NO),
                         REIM_POSTING_SQL.YN_YES, REIM_POSTING_SQL.SYS_TAX_IND_REV_CHRG_VAT,
                         REIM_POSTING_SQL.YN_NO)
         FROM   IM_DOC_HEAD IDH
         WHERE  IDH.TYPE IN   (SELECT * FROM TABLE(CAST(I_DOC_TYPE_LIST   AS OBJ_VARCHAR_ID_TABLE)))
         AND    IDH.STATUS IN (SELECT * FROM TABLE(CAST(I_DOC_STATUS_LIST AS OBJ_VARCHAR_ID_TABLE)))
         AND    IDH.HOLD_STATUS  <> DOC_HELD
         AND  (
                    (
                    ---  IF Hold criteria NOT specified - then at least there shouldn't
                    --   be an associated invoice ON HOLD.
                    I_DOC_HOLD_STATUS IS NULL AND NOT EXISTS (
                                      SELECT 1 FROM IM_INV_DOCUMENT IID , IM_DOC_HEAD INVOICE
                                      WHERE IID.REF_DOC_ID = IDH.DOC_ID
                                      AND IID.inv_id = INVOICE.DOC_ID
                                      AND INVOICE.HOLD_STATUS = DOC_HELD
                                      )
                    )
                    OR
                    (
                    --- IF Hold Criteria IS specified...
                    I_DOC_HOLD_STATUS IS NOT NULL
                    AND (
                          ---  If there are associated invoices - their hold statuses should match the provided criteria
                          EXISTS (
                                SELECT 1 FROM IM_INV_DOCUMENT IID , IM_DOC_HEAD INVOICE
                                WHERE IID.REF_DOC_ID = IDH.DOC_ID
                                AND IID.inv_id = INVOICE.DOC_ID
                                AND INVOICE.HOLD_STATUS IN (SELECT * FROM TABLE(CAST(I_DOC_HOLD_STATUS   AS OBJ_VARCHAR_ID_TABLE)))
                                )
                          ---  OR.. if there NO associated invoices .
                          OR  (NOT EXISTS (
                                SELECT 1 FROM IM_INV_DOCUMENT IID WHERE IID.REF_DOC_ID = IDH.DOC_ID
                              ) AND IDH.HOLD_STATUS IN (SELECT * FROM TABLE(CAST(I_DOC_HOLD_STATUS   AS OBJ_VARCHAR_ID_TABLE))))


                          )
                  )
              )

         AND    (I_DOC_RESOLUTION_ACTIONS IS NULL
                                                 OR
                    (
                      I_DOC_RESOLUTION_ACTIONS IS NOT NULL
                      AND EXISTS (SELECT 1
                                  FROM   IM_RESOLUTION_ACTION IRA
                                  WHERE  IRA.DOC_ID = IDH.DOC_ID
                                  AND IRA.ACTION IN (SELECT * FROM TABLE(CAST(I_DOC_RESOLUTION_ACTIONS AS OBJ_VARCHAR_ID_TABLE)))
                                  AND (I_ROLLUP_STATUS IS NULL
                                          OR
                                        (I_ROLLUP_STATUS IS NOT NULL AND IRA.STATUS = I_ROLLUP_STATUS)
                                      )
                                   )
                    )
                )
         AND    (
                      (
                          L_DOC_ID_COUNT > 0
                          AND IDH.DOC_ID IN (SELECT * FROM TABLE(CAST(I_DOC_ID_LIST AS OBJ_NUMERIC_ID_TABLE)))
                      )
                      OR
                      (
                          L_DOC_ID_COUNT = 0
                      )
                )
         );

        IF UPDATE_POSTING_DOC_SOB(O_ERROR_MESSAGE,
                                  I_POSTING_ID) = FAIL then
           RETURN FAIL;
        END IF;

        RETURN SUCCESS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;
    END ;


/*******************

Identifies APPROVED debit memos for posting.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION PREPARE_APPROVED_DEBIT_MEMOS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                          I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                          I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                          I_SYS_CURR_CODE SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_APPROVED_DEBIT_MEMOS';
        L_DOC_ID_COUNT  NUMBER(6);
        L_DOC_TYPE_LIST    OBJ_VARCHAR_ID_TABLE;
        L_DOC_STATUS_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_HOLD_STATUS  OBJ_VARCHAR_ID_TABLE;
    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        L_DOC_TYPE_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_TYPE_DEBIT_MEMO_QTY,
                                                    DOC_TYPE_DEBIT_MEMO_COST,
                                                    DOC_TYPE_DEBIT_MEMO_TAX);

        L_DOC_STATUS_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_STATUS_APPROVED);

        L_DOC_HOLD_STATUS := NEW OBJ_VARCHAR_ID_TABLE(DOC_NOT_HELD, DOC_RELEASED_FROM_HOLD);

        RETURN PREPARE_RESOLUTIONS(O_ERROR_MESSAGE,
                                            I_POSTING_ID,
                                            I_DOC_ID_LIST,
                                            L_DOC_TYPE_LIST,
                                            L_DOC_STATUS_LIST,
                                            L_DOC_HOLD_STATUS,
                                            I_SYS_CURR_CODE);

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;


    END ;



/*******************

Identifies APPROVED credit notes for posting.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION PREPARE_APPROVED_CREDIT_NOTES(O_ERROR_MESSAGE  OUT VARCHAR2,
                                          I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                          I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                          I_SYS_CURR_CODE SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_APPROVED_CREDIT_NOTES';
        L_DOC_ID_COUNT  NUMBER(6);
        L_DOC_TYPE_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_STATUS_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_HOLD_STATUS  OBJ_VARCHAR_ID_TABLE;
    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        L_DOC_TYPE_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_TYPE_CREDIT_NOTE);

        L_DOC_STATUS_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_STATUS_APPROVED);

        -- We only post APPROVED credit notes that are not at all HELD or Released.

        L_DOC_HOLD_STATUS := OBJ_VARCHAR_ID_TABLE(DOC_NOT_HELD,DOC_RELEASED_FROM_HOLD);

        RETURN PREPARE_RESOLUTIONS(O_ERROR_MESSAGE,
                                            I_POSTING_ID,
                                            I_DOC_ID_LIST,
                                            L_DOC_TYPE_LIST,
                                            L_DOC_STATUS_LIST,
                                            L_DOC_HOLD_STATUS,
                                            I_SYS_CURR_CODE);


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;


    END ;



 /*******************

Updates Approved and Posted credit note hold status to P.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.


  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION UPDATE_CREDIT_NOTE_HOLD_STATUS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                          I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.UPDATE_CREDIT_NOTE_HOLD_STATUS';

    BEGIN

        UPDATE IM_DOC_HEAD IDH
        SET IDH.HOLD_STATUS = DOC_RELEASED_AND_POSTED
        WHERE IDH.DOC_ID IN(  SELECT DOC_ID FROM IM_POSTING_DOC IPD
                              WHERE IPD.POSTING_ID = I_POSTING_ID

                              AND   IPD.DOC_TYPE = DOC_TYPE_CREDIT_NOTE)
        AND   IDH.STATUS = DOC_STATUS_APPROVED
        AND   IDH.HOLD_STATUS = DOC_RELEASED_FROM_HOLD;


        RETURN SUCCESS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;


    END ;




/*******************

Identifies APPROVED Credit memos for posting.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION PREPARE_APPROVED_CREDIT_MEMOS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                          I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                          I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                          I_SYS_CURR_CODE SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_APPROVED_CREDIT_MEMOS';
        L_DOC_ID_COUNT  NUMBER(6);
        L_DOC_TYPE_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_STATUS_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_HOLD_STATUS  OBJ_VARCHAR_ID_TABLE;
    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        L_DOC_TYPE_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_TYPE_CREDIT_MEMO_QTY,
                                                    DOC_TYPE_CREDIT_MEMO_COST);

        L_DOC_STATUS_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_STATUS_APPROVED);

        L_DOC_HOLD_STATUS := NEW OBJ_VARCHAR_ID_TABLE(DOC_NOT_HELD, DOC_RELEASED_FROM_HOLD);

        LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM);

        RETURN PREPARE_RESOLUTIONS(O_ERROR_MESSAGE,
                                            I_POSTING_ID,
                                            I_DOC_ID_LIST,
                                            L_DOC_TYPE_LIST,
                                            L_DOC_STATUS_LIST,
                                            L_DOC_HOLD_STATUS,
                                            I_SYS_CURR_CODE
                                            );


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;


    END ;


/*******************

Identifies credit notes that were released for posting.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION PREPARE_RELEASED_CREDIT_NOTES(O_ERROR_MESSAGE  OUT VARCHAR2,
                                          I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                          I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                          I_SYS_CURR_CODE SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_RELEASED_CREDIT_NOTES';
        L_DOC_ID_COUNT  NUMBER(6);
        L_DOC_TYPE_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_STATUS_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_HOLD_STATUS  OBJ_VARCHAR_ID_TABLE;
    BEGIN
        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        L_DOC_TYPE_LIST   := NEW OBJ_VARCHAR_ID_TABLE(DOC_TYPE_CREDIT_NOTE);

        L_DOC_STATUS_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_STATUS_MATCHED, DOC_STATUS_APPROVED);

        L_DOC_HOLD_STATUS := NEW OBJ_VARCHAR_ID_TABLE(DOC_RELEASED_FROM_HOLD);

        LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM);

        RETURN PREPARE_RESOLUTIONS(O_ERROR_MESSAGE,
                                            I_POSTING_ID,
                                            I_DOC_ID_LIST,
                                            L_DOC_TYPE_LIST,
                                            L_DOC_STATUS_LIST,
                                            L_DOC_HOLD_STATUS,
                                            I_SYS_CURR_CODE);


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;


    END ;

/*******************

Identifies credit notes that were MATCHED but NEVER HELD.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION PREPARE_NON_HELD_CREDIT_NOTES(O_ERROR_MESSAGE  OUT VARCHAR2,
                                          I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                          I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                          I_SYS_CURR_CODE SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_NON_HELD_CREDIT_NOTES';
        L_DOC_ID_COUNT  NUMBER(6);
        L_DOC_TYPE_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_STATUS_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_HOLD_STATUS  OBJ_VARCHAR_ID_TABLE;
    BEGIN
        L_DOC_TYPE_LIST   := NEW OBJ_VARCHAR_ID_TABLE(DOC_TYPE_CREDIT_NOTE);

        L_DOC_STATUS_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_STATUS_MATCHED);

        L_DOC_HOLD_STATUS := NEW OBJ_VARCHAR_ID_TABLE(DOC_NOT_HELD);

        LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM);

        RETURN PREPARE_RESOLUTIONS(O_ERROR_MESSAGE,
                                            I_POSTING_ID,
                                            I_DOC_ID_LIST,
                                            L_DOC_TYPE_LIST,
                                            L_DOC_STATUS_LIST,
                                            L_DOC_HOLD_STATUS,
                                            I_SYS_CURR_CODE);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;


    END ;

/*******************

Performs validation of resolution docs prior to posting.   Documents
that don't pass validation will be registered in IM_POSTING_DOC_ERRORS.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION VALIDATE_RESOLUTION_DOCS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                       I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE)
    RETURN NUMBER
    IS

        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.VALIDATE_RESOLUTION_DOCS_TO_POST';
        STATUS      NUMBER(10)  := SUCCESS;

        --- Resolution documents without reason codes.
        CURSOR REASON_CODES_NULL IS
        SELECT UNIQUE IPD.POSTING_ID,
               IPD.DOC_ID,
               'One or more resolution document detail has no reason code.' AS MSG
        FROM   IM_POSTING_DOC IPD,
               IM_DOC_HEAD IDH,
               IM_DOC_DETAIL_REASON_CODES IDDRC
        WHERE  IPD.POSTING_ID = I_POSTING_ID
        AND    IPD.DOC_ID = IDH.DOC_ID
        AND    IDH.TYPE IN (
                              DOC_TYPE_DEBIT_MEMO_QTY,
                              DOC_TYPE_DEBIT_MEMO_COST,
                              DOC_TYPE_DEBIT_MEMO_TAX,
                              DOC_TYPE_CREDIT_MEMO_QTY,
                              DOC_TYPE_CREDIT_MEMO_COST
                    )
        AND    IDDRC.DOC_ID = IDH.DOC_ID
        AND    IDDRC.REASON_CODE_ID IS NULL
        AND    (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0);

        -- CHECK:  Reason Codes for resolution documents have tran code mapping?
        CURSOR C_REASON_TRAN_CODE_MAP IS
        SELECT DISTINCT POSTING_ID,
               IPD.DOC_ID,
               'One or more reason codes is not mapped to a tran code.' AS MSG
        FROM IM_POSTING_DOC IPD,
             IM_DOC_DETAIL_REASON_CODES IDDRC
        WHERE IPD.POSTING_ID = I_POSTING_ID
          AND IPD.DOC_TYPE   IN (DOC_TYPE_DEBIT_MEMO_QTY,
                                 DOC_TYPE_DEBIT_MEMO_COST,
                                 DOC_TYPE_DEBIT_MEMO_TAX,
                                 DOC_TYPE_CREDIT_MEMO_QTY,
                                 DOC_TYPE_CREDIT_MEMO_COST)
          AND (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)
          AND IDDRC.DOC_ID      = IPD.DOC_ID
          AND NOT EXISTS(SELECT 'X'
                           FROM IM_REASON_TRAN_CODE_MAP IRTCM
                          WHERE IRTCM.REASON_CODE_ID  = IDDRC.REASON_CODE_ID
                            AND IRTCM.SET_OF_BOOKS_ID = IPD.SET_OF_BOOKS_ID);

    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        FOR REC IN REASON_CODES_NULL
        LOOP
            STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE, REC.POSTING_ID, REC.DOC_ID, SEVERITY_ERROR, ERROR_CAT_VALIDATION, REC.MSG);
        END LOOP;

        FOR REC IN C_REASON_TRAN_CODE_MAP
        LOOP
            STATUS := REIM_POSTING_SQL.ADD_POSTING_DOC_ERROR(O_ERROR_MESSAGE, REC.POSTING_ID, REC.DOC_ID, SEVERITY_ERROR, ERROR_CAT_VALIDATION, REC.MSG);
        END LOOP;

        LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM);

        RETURN SUCCESS;


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END VALIDATE_RESOLUTION_DOCS;



/*******************

Calculates fixed deal amounts for debit or credit memos.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION CALC_FIXED_DEALS(O_ERROR_MESSAGE  OUT VARCHAR2,
                              I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE)

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_FIXED_DEALS';
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

    BEGIN
        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
              INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

        INSERT INTO IM_POSTING_DOC_AMOUNTS
               (POSTING_AMT_ID,
                POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION,
                ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE,
                --removed in accordance with TCD for Configurable VAT
                --AMOUNT_VAT,
                PRIM_CURRENCY_CODE)
        SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                IPD.POSTING_ID,
                IPD.DOC_ID,
                AMT_FIXED_DEAL,
                IFDD.LOCATION,
              (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = IFDD.LOCATION AND ROWNUM < 2  ) AS ORG_UNIT,
                IPD.SET_OF_BOOKS_ID,
                Decode(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IFDDT.TAX_CODE) AS TAX_CODE,
                Decode(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IFDDT.TAX_RATE) AS TAX_RATE,
                IFDD.DEPT,
                IFDD.CLASS,
                NULL AS SUBCLASS,
                /**************/
                NO_VALUE                 AS  GROUP_BY_1,
                NO_VALUE                AS  GROUP_BY_2,
                NO_VALUE                AS  GROUP_BY_3,
                NO_VALUE                AS  GROUP_BY_4,
                IFDD.LOCATION           AS  GROUP_BY_5,
                IFDD.LOC_TYPE           AS  GROUP_BY_6,
                NO_VALUE                AS  GROUP_BY_7,
                NO_VALUE                AS  GROUP_BY_8,
                /**************/

                ROUND(IFDD.INCOME ,IPD.DECIMAL_DIGITS),
                BASIC_TRANSACTIONS              ACCOUNT_TYPE,
                DEAL_INCOME_RECEIVABLE_FIXED    ACCOUNT_CODE,
                --removed in accordance with TCD for Configurable VAT
                --ROUND(IFDD.INCOME * IFDD.VAT_RATE/100, ipd.decimal_digits),
                NVL(IPD.SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = IFDD.LOCATION) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
        FROM  IM_POSTING_DOC IPD,
              IM_DOC_HEAD IDH,
              IM_FIXED_DEAL_DETAIL IFDD,
              IM_FIXED_DEAL_DETAIL_TAX IFDDT
        WHERE IPD.POSTING_ID = I_POSTING_ID

        AND   IDH.DOC_ID = IPD.DOC_ID
        AND   IFDD.DOC_ID = IFDDT.DOC_ID (+)
        AND   IFDD.SEQ_NO = IFDDT.SEQ_NO (+)
        AND   IDH.DEAL_TYPE = DEAL_TYPE_FIXED
        AND   IFDD.DOC_ID  IN (IDH.DOC_ID,
                                  (CASE
                                      WHEN IPD.DOC_TYPE IN (DOC_TYPE_DEBIT_MEMO_COST)
                                      THEN IDH.REF_DOC
                                      ELSE NULL
                                  END)
                              );

        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ;

/*******************

Calculates Complex Deal income amounts for debit/credit memos.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION CALC_COMPLEX_DEALS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE)

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_COMPLEX_DEALS';
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

    BEGIN

        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
              INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

        INSERT INTO IM_POSTING_DOC_AMOUNTS
               (POSTING_AMT_ID,
                POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION,
                ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE,
                --removed in accordance with TCD for Configurable VAT
                --AMOUNT_VAT,
                PRIM_CURRENCY_CODE)
       SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                   POSTING_ID,
                   DOC_ID,
                   AMOUNT_TYPE,
                   LOCATION,
                   ORG_UNIT,
                   SET_OF_BOOKS_ID,
                   TAX_CODE ,
                   TAX_RATE,
                   DEPT,
                   CLASS,
                   SUBCLASS,
                   GROUP_BY_1,
                   GROUP_BY_2,
                   GROUP_BY_3,
                   GROUP_BY_4,
                   GROUP_BY_5,
                   GROUP_BY_6,
                   GROUP_BY_7,
                   GROUP_BY_8,
                   AMOUNT,
                   ACCOUNT_TYPE,
                   ACCOUNT_CODE,
                   PRIM_CURRENCY_CODE
            FROM (
                  SELECT --IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL ,
                            IPD.POSTING_ID,
                            IPD.DOC_ID,
                            AMT_COMPLEX_DEAL AS AMOUNT_TYPE,
                            ICDD.LOCATION,
                           (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                                    UNION ALL
                                                    SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                            ELSE
                                                           (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                            END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                                    ) WHERE LOCATION = ICDD.LOCATION AND ROWNUM < 2  ) AS ORG_UNIT,
                           IPD.SET_OF_BOOKS_ID,
                            NULL   AS TAX_CODE,
                            NULL   AS TAX_RATE,
                            IM.DEPT,
                            IM.CLASS,
                            NULL AS SUBCLASS,
                            NO_VALUE AS GROUP_BY_1,
                            NO_VALUE AS GROUP_BY_2,
                            ICDD.LOCAL_CURRENCY AS GROUP_BY_3,
                            SUM(ICDD.INCOME_LOCAL_CURR) AS GROUP_BY_4,
                            ICDD.LOCATION AS  GROUP_BY_5,
                            ICDD.LOC_TYPE AS  GROUP_BY_6,
                            NO_VALUE AS GROUP_BY_7,
                            NO_VALUE AS GROUP_BY_8,
                            SUM(ROUND(ICDD.INCOME_DEAL_CURR, IPD.DECIMAL_DIGITS)) AS AMOUNT,
                            BASIC_TRANSACTIONS               ACCOUNT_TYPE,
                            DEAL_INCOME_RECEIVABLE_COMPLEX    ACCOUNT_CODE,
                            --removed in accordance with TCD for Configurable VAT
                            --ROUND(ICDD.INCOME_DEAL_CURR * ICDD.VAT_RATE/100, ipd.decimal_digits),
                            NVL(IPD.SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                                        WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                                          (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                                        WAREHOUSE.WH
                                                                   ELSE
                                                                        (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                                        WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                                   END)
                                                          FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                                       ELSE
                                                          MLS.LOCATION
                                                       END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = ICDD.LOCATION) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
                    FROM  IM_POSTING_DOC IPD,
                          IM_DOC_HEAD IDH,
                          IM_COMPLEX_DEAL_DETAIL ICDD,
                          ITEM_MASTER IM
                    WHERE IPD.POSTING_ID = I_POSTING_ID

                    AND   IDH.DOC_ID = IPD.DOC_ID
                    AND   IDH.DEAL_TYPE = DEAL_TYPE_COMPLEX
                    AND   IM.ITEM = ICDD.ITEM
                    AND   ICDD.DOC_ID  IN (IDH.DOC_ID,
                                              (CASE
                                                  WHEN IPD.DOC_TYPE IN (DOC_TYPE_DEBIT_MEMO_COST)
                                                  THEN IDH.REF_DOC
                                                  ELSE NULL
                                              END)
                                          )
                    GROUP BY  IPD.POSTING_ID, IPD.DOC_ID,  ICDD.LOCATION,  IPD.SET_OF_BOOKS_ID, IM.DEPT,  IM.CLASS, ICDD.LOCAL_CURRENCY, ICDD.LOC_TYPE,IPD.SYS_CURRENCY_CODE
                  );



        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ;



/*******************

Calculates the detail level cost of resolution documents (as opposed to the
resolution doc's header total cost).

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION CALC_RESOLUTION_DOC_DETAIL_AMT(O_ERROR_MESSAGE  OUT VARCHAR2,
                              I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE)

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_RESOLUTION_DOC_DETAIL_AMT';
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

    BEGIN
        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
              INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

           INSERT INTO IM_POSTING_DOC_AMOUNTS
                      (POSTING_AMT_ID,
                        POSTING_ID,
                       DOC_ID,
                       AMOUNT_TYPE,
                       LOCATION,
                       ORG_UNIT,
                       SET_OF_BOOKS_ID,
                       TAX_CODE,
                       TAX_RATE,
                       DEPT,
                       CLASS,
                       SUBCLASS,
                       GROUP_BY_1,
                       GROUP_BY_2,
                       GROUP_BY_3,
                       GROUP_BY_4,
                       GROUP_BY_5,
                       GROUP_BY_6,
                       GROUP_BY_7,
                       GROUP_BY_8,
                       AMOUNT,
                       ACCOUNT_TYPE,
                       ACCOUNT_CODE,
                       --removed in accordance with TCD for Configurable VAT
                       --AMOUNT_VAT,
                       PRIM_CURRENCY_CODE)
        SELECT   IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                        POSTING_ID,
                        DOC_ID,
                        AMT_RESOLUTION_ACTION AS AMOUNT_TYPE,
                        LOC,
                      (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
                       SET_OF_BOOKS_ID,
                       TAX_CODE,
                       TAX_RATE,
                       DEPT,
                       CLASS,
                       SUBCLASS,
                       GROUP_BY_1,
                       GROUP_BY_2,
                       GROUP_BY_3,
                       GROUP_BY_4,
                       GROUP_BY_5,
                       GROUP_BY_6,
                       GROUP_BY_7,
                       GROUP_BY_8,
                       AMOUNT,
                       ACCOUNT_TYPE,
                       ACCOUNT_CODE,
                       --removed in accordance with TCD for Configurable VAT
                       --AMOUNT_VAT,
                        NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
                         FROM (

          SELECT        IPD.POSTING_ID  AS POSTING_ID,
                        IPD.DOC_ID  AS DOC_ID,
                        AMT_RESOLUTION_ACTION AS AMOUNT_TYPE,
                        IPD.LOCATION LOC,
                        IPD.SET_OF_BOOKS_ID,
                        IPD.SYS_CURRENCY_CODE,
                        Decode(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDDRCT.TAX_CODE) AS TAX_CODE,
                        Decode(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,IDDRCT.TAX_RATE) AS TAX_RATE,
                        DEPT AS DEPT,
                        CLASS AS CLASS,
                        SUBCLASS AS SUBCLASS,
                        IRC.ACTION           AS GROUP_BY_1,
                        IDDRC.REASON_CODE_ID AS GROUP_BY_2,
                        IDH.DEAL_TYPE             AS GROUP_BY_3,
                        IDH.CONSIGNMENT_IND       AS GROUP_BY_4,
                        NO_VALUE       AS GROUP_BY_5,
                        NO_VALUE       AS GROUP_BY_6,
                        NO_VALUE       AS GROUP_BY_7,
                        NO_VALUE       AS GROUP_BY_8,
                        ROUND(SUM(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY), IPD.DECIMAL_DIGITS) AS AMOUNT,
                        REASON_CODE_ACTIONS         ACCOUNT_TYPE,
                        IRTCM.TRAN_CODE         ACCOUNT_CODE
                        --removed in accordance with TCD for Configurable VAT
                        --SUM(ROUND(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY * IDDRC.VAT_RATE/100, IPD.DECIMAL_DIGITS)) AS AMOUNT_VAT
          FROM  IM_POSTING_DOC IPD,
                IM_DOC_DETAIL_REASON_CODES IDDRC,
                IM_REASON_TRAN_CODE_MAP IRTCM,
                IM_DOC_DETAIL_RC_TAX IDDRCT,
                IM_DOC_HEAD IDH,
                IM_REASON_CODES  IRC,
                ITEM_MASTER IM
          WHERE IPD.POSTING_ID = I_POSTING_ID

          AND   IDDRC.DOC_ID   = IPD.DOC_ID
          AND   IRTCM.REASON_CODE_ID  = IDDRC.REASON_CODE_ID
          AND   IRTCM.SET_OF_BOOKS_ID = IPD.SET_OF_BOOKS_ID
          AND   IM.ITEM        = IDDRC.ITEM
          AND   IRC.REASON_CODE_ID = IDDRC.REASON_CODE_ID
          AND   IDDRC.IM_DOC_DETAIL_REASON_CODES_ID = IDDRCT.IM_DOC_DETAIL_REASON_CODES_ID (+)
          AND   IDH.DOC_ID     = IPD.DOC_ID
          AND   IDDRC.STATUS = REIM_POSTING_SQL.DOC_STATUS_APPROVED
          AND   (IDH.CONSIGNMENT_IND IS NULL OR IDH.CONSIGNMENT_IND = YN_NO)
          AND   (IDH.DEAL_TYPE IS NULL   OR
                (IDH.DEAL_TYPE IS NOT NULL AND IDH.DEAL_TYPE = DEAL_TYPE_NON_MERCH))
          GROUP BY      IPD.POSTING_ID,
                        IPD.DOC_ID,
                        IPD.LOCATION,
                        IPD.SET_OF_BOOKS_ID,
                        IPD.SYS_CURRENCY_CODE,
                        IDDRCT.TAX_CODE,
                        IDDRCT.TAX_RATE,
                        IM.DEPT,
                        IM.CLASS,
                        IM.SUBCLASS,
                        IRC.ACTION,           -- GROUP_BY_1,
                        IDDRC.REASON_CODE_ID, -- GROUP_BY_2,
                        IDH.DEAL_TYPE, -- GROUP_BY_3,
                        IDH.CONSIGNMENT_IND, -- GROUP_BY_4,
                        NO_VALUE, -- GROUP_BY_5,
                        NO_VALUE, -- GROUP_BY_6,
                        NO_VALUE, -- GROUP_BY_7,
                        NO_VALUE,
                        REASON_CODE_ACTIONS,
                        IRTCM.TRAN_CODE,
                        IPD.DECIMAL_DIGITS); -- GROUP_BY_8;


        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ;

/*******************

Calculates the document value (i.e. total cost inclusive of tax) of documents to be posted.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
   FUNCTION CALC_DOCUMENT_VALUE(O_ERROR_MESSAGE  OUT VARCHAR2,
                                I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE)

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_DOCUMENT_VALUE';


        /**FOR CONFIGURABLE TAX FUNCTIONALITY
        there should be a single TAP entry per document and amount should be  inclusive of tax.
        **/

    BEGIN



         INSERT INTO IM_POSTING_DOC_AMOUNTS
               (POSTING_AMT_ID,
                POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION,
                ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE,
                --removed in accordance with TCD for Configurable VAT
                --AMOUNT_VAT,
                PRIM_CURRENCY_CODE)

        SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                        POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOC,
               (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE,
                --BASIC_TRANSACTIONS       ACCOUNT_TYPE,
                --TRADE_ACCOUNTS_PAYABLE   ACCOUNT_CODE,
                --removed in accordance with TCD for Configurable VAT
                --AMOUNT_VAT,
                NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
                 FROM (
        -- Query #1:   Receipt Write-Offs
        -- Document Value = 0  (but in order to fit receipt writeoffs
        -- into the posting infrastructure,   we need to calculate at least
        -- a row to represent 0 document.
        (SELECT IPD.POSTING_ID,
                IPD.DOC_ID,
                AMT_DOCUMENT_HEADER_COST AMOUNT_TYPE,
                S.TO_LOC LOC,
                MLS.SET_OF_BOOKS_ID,
                IPD.SYS_CURRENCY_CODE,
                --empty in accordance with TCD for Configurable VAT
                NULL AS TAX_CODE,
                --empty in accordance with TCD for Configurable VAT
                NULL AS TAX_RATE,
                NULL AS DEPT,
                NULL AS CLASS,
                NULL AS SUBCLASS,
                TO_CHAR(S.SHIPMENT)                AS GROUP_BY_1,
                TO_CHAR(S.RECEIVE_DATE, 'YYYY-MM-DD')            AS GROUP_BY_2,
                TO_CHAR(S.ORDER_NO)  AS GROUP_BY_3,
                S.TO_LOC_TYPE||'^'||TO_CHAR(S.TO_LOC)                  AS GROUP_BY_4,
                'SUPP'||'^'||DECODE(SUPS.SUPPLIER_PARENT, NULL,SUPS.SUPPLIER,SUPS.SUPPLIER_PARENT ) AS GROUP_BY_5,
                IPDDC.CURRENCY_CODE||'^'||TO_CHAR(IPDDC.EXCHANGE_RATE)||'^'||IPDDC.EXCHANGE_TYPE  AS GROUP_BY_6,
                IPDDC.PRIM_CURRENCY_CODE AS GROUP_BY_7,
                NO_VALUE AS GROUP_BY_8,
                0.0 AS AMOUNT,
                BASIC_TRANSACTIONS ACCOUNT_TYPE,
                TRADE_ACCOUNTS_PAYABLE ACCOUNT_CODE
        FROM    IM_POSTING_DOC IPD,
                IM_POSTING_DOC_DEPTCLASS IPDDC,
                SHIPMENT S,
                ORDHEAD OH,
                    SUPS,
                     MV_LOC_SOB MLS,
                     (SELECT ORDER_NO,
                        MIN(LOCATION) LOCATION
                   FROM ORDLOC
                  GROUP BY ORDER_NO) OL
        WHERE IPD.POSTING_ID = I_POSTING_ID
          AND IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF

          AND IPDDC.POSTING_ID = I_POSTING_ID
          AND IPDDC.DOC_ID =  IPD.DOC_ID
          AND S.SHIPMENT = IPDDC.MATCH_ID
          AND OH.ORDER_NO = S.ORDER_NO
            AND SUPS.SUPPLIER = OH.SUPPLIER
          AND OL.ORDER_NO = OH.ORDER_NO
          AND MLS.LOCATION = OL.LOCATION
          AND MLS.LOCATION_TYPE IN (REIM_CONSTANTS.LOC_TYPE_WH, REIM_CONSTANTS.LOC_TYPE_STORE)
        GROUP BY IPD.POSTING_ID, IPD.DOC_ID, --IPDDC.VAT_CODE,IPDDC.VAT_RATE,
                S.SHIPMENT, S.RECEIVE_DATE, S.ORDER_NO, S.TO_LOC, MLS.SET_OF_BOOKS_ID, S.TO_LOC_TYPE, IPD.SYS_CURRENCY_CODE,
                'SUPP'||'^'||DECODE(SUPS.SUPPLIER_PARENT, NULL,SUPS.SUPPLIER,SUPS.SUPPLIER_PARENT ), IPDDC.PRIM_CURRENCY_CODE, IPDDC.CURRENCY_CODE, IPDDC.EXCHANGE_RATE, IPDDC.EXCHANGE_TYPE

        UNION ALL
        -- Query #2:  Pre-paid
        -- Document Value = 0
        SELECT  IPD.POSTING_ID,
                IPD.DOC_ID,
                AMT_DOCUMENT_HEADER_COST AMOUNT_TYPE,
                IPD.LOCATION LOC,
                IPD.SET_OF_BOOKS_ID,
                IPD.SYS_CURRENCY_CODE,
                NULL AS TAX_CODE,
                NULL AS TAX_RATE,
                NULL AS DEPT,
                NULL AS CLASS,
                NULL AS SUBCLASS,
                NO_VALUE AS GROUP_BY_1,
                NO_VALUE AS GROUP_BY_2,
                NO_VALUE AS GROUP_BY_3,
                NO_VALUE AS GROUP_BY_4,
                NO_VALUE AS GROUP_BY_5,
                NO_VALUE AS GROUP_BY_6,
                NO_VALUE AS GROUP_BY_7,
                NO_VALUE AS GROUP_BY_8,
                0.0  AS AMOUNT,
                BASIC_TRANSACTIONS ACCOUNT_TYPE,
                TRADE_ACCOUNTS_PAYABLE ACCOUNT_CODE
        FROM    IM_POSTING_DOC IPD,
                IM_DOC_HEAD    IDH
        WHERE   IPD.POSTING_ID = I_POSTING_ID

        AND     IDH.DOC_ID =  IPD.DOC_ID
        AND     (IDH.PRE_PAID_IND = YN_YES OR IDH.MANUALLY_PAID_IND = YN_YES)

        UNION ALL

        -- Query #3:  Any document without invoice details, reason code details
        -- Document Value = document total cost WHICH includes non-merch
        -- Non-merch invoices are excluded from this calculation
        SELECT  IPD.POSTING_ID,
                IPD.DOC_ID,
                AMT_DOCUMENT_HEADER_COST AMOUNT_TYPE,
                IPD.LOCATION LOC,
                IPD.SET_OF_BOOKS_ID,
                IPD.SYS_CURRENCY_CODE,
                NULL AS TAX_CODE,
                NULL AS TAX_RATE,
                NULL AS DEPT,
                NULL AS CLASS,
                NULL AS SUBCLASS,
                NO_VALUE AS GROUP_BY_1,
                NO_VALUE AS GROUP_BY_2,
                NO_VALUE AS GROUP_BY_3,
                NO_VALUE AS GROUP_BY_4,
                NO_VALUE AS GROUP_BY_5,
                NO_VALUE AS GROUP_BY_6,
                NO_VALUE AS GROUP_BY_7,
                NO_VALUE AS GROUP_BY_8,
                ROUND(IDH.TOTAL_COST_INC_TAX, IPD.DECIMAL_DIGITS)  AS AMOUNT,
                BASIC_TRANSACTIONS ACCOUNT_TYPE,
                CASE WHEN IDH.PRE_PAID_IND = YN_YES OR IDH.MANUALLY_PAID_IND = YN_YES THEN
                PRE_PAID_ASSET
                ELSE
                Decode(IDH.TYPE,DOC_TYPE_MERCH_INVOICE,TRADE_ACCOUNTS_PAYABLE,TAP_NON_DYNAMIC)
                END AS ACCOUNT_CODE
                --removed in accordance with TCD for Configurable VAT
                --ROUND(IDH.TOTAL_COST_INC_TAX-IDH.TOTAL_COST, IPD.DECIMAL_DIGITS) AS AMOUNT_VAT
        FROM    IM_POSTING_DOC IPD,
                IM_DOC_HEAD    IDH
        WHERE   IPD.POSTING_ID = I_POSTING_ID

        AND     IDH.DOC_ID =  IPD.DOC_ID
        AND     IDH.TYPE <> DOC_TYPE_NONMERCH_INVOICE
        AND     NOT EXISTS (SELECT 1 FROM im_invoice_detail iid WHERE iid.doc_id =idh.doc_id)
        AND     NOT EXISTS (SELECT 1 FROM IM_DOC_DETAIL_REASON_CODES iddrc WHERE iddrc.doc_id = idh.doc_id)
        --AND     NOT EXISTS (SELECT 1 FROM IM_DOC_TAX IDT WHERE IDT.DOC_ID = IDH.DOC_ID)
        AND     (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0)

        UNION ALL

        -- Query #4:  Following query will take care of all scenerios where document have details or reason codes or a header document
        --with vat basis.There is a seperate subquery for each scenerio.
        SELECT  POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION LOC,
                SET_OF_BOOKS_ID,
                SYS_CURRENCY_CODE,
                TAX_CODE                               AS TAX_CODE,
                DECODE(TAX_CODE, NULL, NULL, TAX_RATE) AS TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                ROUND(SUM(AMOUNT), DECIMAL_DIGITS) AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE
        FROM(

              -- SUBQUERY #1:  Any document that might have details, reason codes or deal id
              -- will have to explicitly query for the non-merch components  to combine with SUBQuery#3,4.
              -- Non-merch invoice will be calculated by this subquery
              SELECT  IPD.POSTING_ID,
                      IPD.DOC_ID,
                      IPD.DOC_TYPE,
                      AMT_DOCUMENT_HEADER_COST AMOUNT_TYPE,
                      IPD.LOCATION LOCATION,
                      IPD.SET_OF_BOOKS_ID,
                      IPD.SYS_CURRENCY_CODE,
                      NULL AS TAX_CODE,
                      NULL AS TAX_RATE,
                      NULL AS DEPT,
                      NULL AS CLASS,
                      NULL AS SUBCLASS,
                      NO_VALUE AS  GROUP_BY_1,
                      NO_VALUE AS  GROUP_BY_2,
                      NO_VALUE AS  GROUP_BY_3,
                      NO_VALUE AS  GROUP_BY_4,
                      NO_VALUE AS  GROUP_BY_5,
                      NO_VALUE AS  GROUP_BY_6,
                      NO_VALUE AS  GROUP_BY_7,
                      NO_VALUE AS GROUP_BY_8,
                      (IDNM.NON_MERCH_AMT + NVL((SELECT SUM(TAX_AMOUNT)
                                                        FROM IM_DOC_NON_MERCH_TAX IDNMT
                                                        WHERE IDNMT.DOC_ID = IDNM.DOC_ID
                                                        AND   IDNMT.NON_MERCH_CODE = IDNM.NON_MERCH_CODE),0)) AMOUNT,
                      IPD.DECIMAL_DIGITS,
                      BASIC_TRANSACTIONS ACCOUNT_TYPE,
                                CASE
                                    WHEN IDH.PRE_PAID_IND = YN_YES OR IDH.MANUALLY_PAID_IND = YN_YES THEN
                                         PRE_PAID_ASSET
                                    WHEN IDH.TYPE = DOC_TYPE_NONMERCH_INVOICE THEN
                                         TAP_NON_DYNAMIC
                                    ELSE
                                         Decode(IDH.DEAL_TYPE,
                                                DEAL_TYPE_NON_MERCH, TAP_NON_DYNAMIC,
                                                TRADE_ACCOUNTS_PAYABLE)
                                END AS ACCOUNT_CODE
                FROM  IM_POSTING_DOC IPD,
                      IM_DOC_HEAD IDH,
                      IM_DOC_NON_MERCH IDNM
              WHERE IPD.POSTING_ID = I_POSTING_ID
              AND   IDH.DOC_ID = IPD.DOC_ID

              AND   IDNM.DOC_ID = IDH.DOC_ID
              AND   (EXISTS (SELECT 1 FROM IM_INVOICE_DETAIL IID WHERE IID.DOC_ID = IDH.DOC_ID)
                    OR EXISTS (SELECT 1 FROM IM_DOC_DETAIL_REASON_CODES iddrc WHERE iddrc.doc_id = idh.doc_id)
                  --  OR EXISTS (SELECT 1 FROM IM_DOC_TAX IDT WHERE IDT.DOC_ID = IDH.DOC_ID)
                    OR (IDH.DEAL_ID IS NOT NULL AND IDH.DEAL_ID <> 0)
                    OR IDH.TYPE = DOC_TYPE_NONMERCH_INVOICE)

              UNION ALL
              -- Subquery2:  Calculate detail level amount from invoice details
              SELECT  IPD.POSTING_ID,
                      IPD.DOC_ID,
                      IPD.DOC_TYPE,
                      AMT_DOCUMENT_HEADER_COST AS AMOUNT_TYPE,
                      IPD.LOCATION AS LOCATION,
                      IPD.SET_OF_BOOKS_ID,
                      IPD.SYS_CURRENCY_CODE,
                      NULL AS TAX_CODE,
                      NULL AS TAX_RATE,
                      NULL AS DEPT,
                      NULL AS CLASS,
                      NULL AS SUBCLASS,
                      NO_VALUE AS GROUP_BY_1,
                      NO_VALUE AS GROUP_BY_2,
                      NO_VALUE AS GROUP_BY_3,
                      NO_VALUE AS GROUP_BY_4,
                      NO_VALUE AS GROUP_BY_5,
                      NO_VALUE AS GROUP_BY_6,
                      NO_VALUE AS GROUP_BY_7,
                      NO_VALUE AS GROUP_BY_8,
                      ((IID.UNIT_COST * IID.QTY) + DECODE(IPD.SYS_TAX_IND,
                                                                  SYS_TAX_IND_ACQ_VAT, 0,
                                                                  NVL((SELECT SUM(TAX_AMOUNT)
                                                                        FROM IM_INVOICE_DETAIL_TAX IIDT
                                                                      WHERE IIDT.DOC_ID = IID.DOC_ID
                                                                          AND   IIDT.ITEM = IID.ITEM),0))) AMOUNT,
                      ipd.decimal_digits,
                      BASIC_TRANSACTIONS ACCOUNT_TYPE,
                      CASE WHEN IDH.PRE_PAID_IND = YN_YES OR IDH.MANUALLY_PAID_IND = YN_YES THEN
                        PRE_PAID_ASSET
                      ELSE
                        TRADE_ACCOUNTS_PAYABLE
                      END AS ACCOUNT_CODE
              FROM    IM_POSTING_DOC IPD,
                      IM_DOC_HEAD    IDH,
                      IM_INVOICE_DETAIL IID

              WHERE   IPD.POSTING_ID = I_POSTING_ID

              AND     IDH.DOC_ID =  IPD.DOC_ID
              AND     IDH.TYPE <> DOC_TYPE_NONMERCH_INVOICE
              AND     IID.DOC_ID = IDH.DOC_ID
              AND     NOT EXISTS (SELECT 1 FROM IM_DOC_DETAIL_REASON_CODES iddrc WHERE iddrc.doc_id = idh.doc_id)
              AND     (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0)

              UNION ALL

              -- Subquery3:  Calculate detail level amount from reason code details.
              SELECT  IPD.POSTING_ID,
                      IPD.DOC_ID,
                      IPD.DOC_TYPE,
                      AMT_DOCUMENT_HEADER_COST AS AMOUNT_TYPE,
                      IPD.LOCATION AS LOCATION,
                      IPD.SET_OF_BOOKS_ID,
                      IPD.SYS_CURRENCY_CODE,
                      NULL AS TAX_CODE,
                      NULL AS TAX_RATE,
                      NULL AS DEPT,
                      NULL AS CLASS,
                      NULL AS SUBCLASS,
                      NO_VALUE AS GROUP_BY_1,
                      NO_VALUE AS GROUP_BY_2,
                      NO_VALUE AS GROUP_BY_3,
                      NO_VALUE AS GROUP_BY_4,
                      NO_VALUE AS GROUP_BY_5,
                      NO_VALUE AS GROUP_BY_6,
                      NO_VALUE AS GROUP_BY_7,
                      NO_VALUE AS GROUP_BY_8,
                      (ROUND(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY, ipd.decimal_digits) + DECODE(IPD.SYS_TAX_IND,
                                                                                                 SYS_TAX_IND_ACQ_VAT, 0,
                                                                                                 NVL((SELECT ROUND(SUM(TAX_AMOUNT), ipd.decimal_digits)
                                                                                                        FROM IM_DOC_DETAIL_RC_TAX IDDRT
                                                                                                       WHERE IDDRT.IM_DOC_DETAIL_REASON_CODES_ID = IDDRC.IM_DOC_DETAIL_REASON_CODES_ID),0))) AMOUNT,
                      ipd.decimal_digits,
                      BASIC_TRANSACTIONS ACCOUNT_TYPE,
                      TRADE_ACCOUNTS_PAYABLE ACCOUNT_CODE
              FROM    IM_POSTING_DOC IPD,
                      IM_DOC_HEAD    IDH,
                      IM_DOC_DETAIL_REASON_CODES IDDRC
              WHERE   IPD.POSTING_ID = I_POSTING_ID

              AND     IDH.DOC_ID =  IPD.DOC_ID
              AND     IDH.TYPE <> DOC_TYPE_NONMERCH_INVOICE
              AND     IDDRC.DOC_ID = IDH.DOC_ID
              AND     IDDRC.STATUS = REIM_POSTING_SQL.DOC_STATUS_APPROVED
              AND     NOT EXISTS (SELECT 1 FROM IM_INVOICE_DETAIL IID WHERE IID.doc_id = idh.doc_id)
              AND     (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0)
            UNION ALL

            -- Subquery:   Calculate fixed deal level
                  SELECT  IPD.POSTING_ID,
                          IPD.DOC_ID,
                          IPD.DOC_TYPE,
                          AMT_DOCUMENT_HEADER_COST AS AMOUNT_TYPE,
                          IFDD.LOCATION AS LOCATION,
                          IPD.SET_OF_BOOKS_ID,
                          IPD.SYS_CURRENCY_CODE,
                          NULL AS TAX_CODE,
                          NULL AS TAX_RATE,
                          NULL AS DEPT,
                          NULL AS CLASS,
                          NULL AS SUBCLASS,
                          NO_VALUE AS GROUP_BY_1,
                          NO_VALUE AS GROUP_BY_2,
                          NO_VALUE AS GROUP_BY_3,
                          NO_VALUE AS GROUP_BY_4,
                          NO_VALUE AS GROUP_BY_5,
                          NO_VALUE AS GROUP_BY_6,
                          NO_VALUE AS GROUP_BY_7,
                          NO_VALUE AS GROUP_BY_8,
                          IFDD.INCOME + NVL((SELECT SUM(TAX_AMOUNT) FROM IM_FIXED_DEAL_DETAIL_TAX IFDDT
                                              WHERE IFDD.DOC_ID = IFDDT.DOC_ID AND IFDD.SEQ_NO = IFDDT.SEQ_NO),0) AS  AMOUNT,
                          --removed in accordance with TCD for Configurable VAT
                          --(IFDD.INCOME * NVL(IFDD.VAT_RATE, 0)/100) AS VAT_AMOUNT,
                          ipd.decimal_digits,
                          BASIC_TRANSACTIONS ACCOUNT_TYPE,
                          TRADE_ACCOUNTS_PAYABLE ACCOUNT_CODE
                  FROM    IM_POSTING_DOC IPD,
                          IM_DOC_HEAD    IDH,
                          IM_FIXED_DEAL_DETAIL IFDD
                  WHERE   IPD.POSTING_ID = I_POSTING_ID

                  AND     IDH.DOC_ID =  IPD.DOC_ID
                  AND     IDH.DOC_ID =  IFDD.DOC_ID
                  UNION ALL

                  -- Subquery:  Calculate complex deal level
                  SELECT  IPD.POSTING_ID,
                          IPD.DOC_ID,
                          IPD.DOC_TYPE,
                          AMT_DOCUMENT_HEADER_COST AS AMOUNT_TYPE,
                          ICDD.LOCATION AS LOCATION,
                          IPD.SET_OF_BOOKS_ID,
                          IPD.SYS_CURRENCY_CODE,
                          NULL AS TAX_CODE,
                          NULL AS TAX_RATE,
                          NULL AS DEPT,
                          NULL AS CLASS,
                          NULL AS SUBCLASS,
                          NO_VALUE AS GROUP_BY_1,
                          NO_VALUE AS GROUP_BY_2,
                          NO_VALUE AS GROUP_BY_3,
                          NO_VALUE AS GROUP_BY_4,
                          NO_VALUE AS GROUP_BY_5,
                          NO_VALUE AS GROUP_BY_6,
                          NO_VALUE AS GROUP_BY_7,
                          NO_VALUE AS GROUP_BY_8,
                          ICDD.INCOME_DEAL_CURR + DECODE(IPD.SYS_TAX_IND,
                                                         SYS_TAX_IND_ACQ_VAT , 0,
                                                         NVL((SELECT SUM(TAX_AMOUNT) FROM IM_COMPLEX_DEAL_DETAIL_TAX ICDDT
                                                               WHERE ICDD.DOC_ID = ICDDT.DOC_ID AND ICDD.SEQ_NO = ICDDT.SEQ_NO),0)) AS  AMOUNT,
                          --removed in accordance with TCD for Configurable VAT
                          --(ICDD.INCOME_DEAL_CURR * NVL(ICDD.VAT_RATE, 0)/100) AS VAT_AMOUNT,
                          ipd.decimal_digits,
                          BASIC_TRANSACTIONS ACCOUNT_TYPE,
                          TRADE_ACCOUNTS_PAYABLE ACCOUNT_CODE
                  FROM    IM_POSTING_DOC IPD,
                          IM_DOC_HEAD    IDH,
                          IM_COMPLEX_DEAL_DETAIL ICDD
                  WHERE   IPD.POSTING_ID = I_POSTING_ID

                  AND     IDH.DOC_ID =  IPD.DOC_ID
                  AND     IDH.DOC_ID =  ICDD.DOC_ID

        )
        GROUP BY POSTING_ID,
                      DOC_ID,
                      DOC_TYPE,
                      AMOUNT_TYPE,
                      LOCATION,
                      SET_OF_BOOKS_ID,
                      SYS_CURRENCY_CODE,
                      TAX_CODE,
                      TAX_RATE,
                      DEPT,
                      CLASS,
                      SUBCLASS,
                      GROUP_BY_1,
                      GROUP_BY_2,
                      GROUP_BY_3,
                      GROUP_BY_4,
                      GROUP_BY_5,
                      GROUP_BY_6,
                      GROUP_BY_7,
                      GROUP_BY_8,
                      decimal_digits,ACCOUNT_CODE,ACCOUNT_TYPE));


        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ;



/*******************

Populates the table IM_POSTING_DOC_DEPTCLASS with the extende cost totals
PER department class from the associated document items.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION LOAD_DOC_MERCH_COMPONENTS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                       I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE)

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.LOAD_DOC_MERCH_COMPONENTS';
        L_PRORATE_ID NUMBER(10,0);
    BEGIN

          LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

          INSERT INTO IM_POSTING_DOC_DEPTCLASS (
                POSTING_ID,
                DOC_ID,
                TYPE,
                DEPT,
                CLASS,
                EXT_COST,
                --removed in accordance with TCD for Configurable VAT
                --VAT_CODE,
                --VAT_RATE,
                --VAT_AMOUNT,
                MATCH_ID
          )
          -- QUERY:  Sum(merchandise detail cost) PER department/class
          SELECT MERCHS.POSTING_ID,
                MERCHS.DOC_ID,
                MERCHS.TYPE,
                IM.DEPT,
                IM.CLASS,
                SUM(MERCHS.QUANTITY * MERCHS.UNIT_COST) AS EXT_COST,
                --removed in accordance with TCD for Configurable VAT
                --MERCHS.VAT_CODE,
                --MERCHS.VAT_RATE,
                --SUM(ROUND( ROUND(MERCHS.QUANTITY * MERCHS.UNIT_COST * MERCHS.VAT_RATE/100, MERCHS.DECIMAL_DIGITS), merchs.decimal_digits))  AS VAT_AMOUNT,
                MATCH_ID
          FROM (
                    /****
                    Any doc with detail reason codes ...
                    *****/
                    SELECT   IPD.POSTING_ID AS POSTING_ID,
                            IPD.DOC_ID AS DOC_ID,
                            -1*IPD.DOC_ID   AS MATCH_ID,
                            'resolution'   AS TYPE,
                            IDRC.ITEM AS ITEM,
                            IDRC.ORIG_QTY AS QUANTITY,
                            IDRC.ORIG_UNIT_COST AS UNIT_COST,
                            --removed in accordance with TCD for Configurable VAT
                            --IDRC.VAT_CODE AS VAT_CODE,
                            --DECODE(IDRC.VAT_CODE, NULL, NULL, IDRC.VAT_RATE) AS VAT_RATE,
                            IPD.DECIMAL_DIGITS
                    FROM     IM_POSTING_DOC IPD,
                            IM_DOC_DETAIL_REASON_CODES IDRC
                    WHERE    IPD.POSTING_ID = I_POSTING_ID

                    AND      IPD.MATCH_TYPE <> MATCH_TYPE_SUMMARY
                    AND      IPD.DOC_ID = IDRC.DOC_ID
                    UNION ALL
                    /****
                    Any matched invoices...
                    Go back to the receipt items for dept class.
                    For detail matches,  obtain vat code/rate from invoice detail.
                    For summary matches,  vat code/rate indeterminate.  Proration across invoice tax basis required.
                    *****/
                    SELECT  UNIQUE IPD.POSTING_ID,
                                          IPD.DOC_ID,
                                NULL AS MATCH_ID,
                                'receipt' AS TYPE,
                                          IRIP.ITEM AS ITEM,
                                -- qty_matched from IRIP.  If not available, try summing shipsku qty_matched / qty_received
                                          CASE
                                              WHEN IRIP.QTY_MATCHED IS NULL OR IRIP.QTY_MATCHED = 0 THEN
                                    NVL(
                                            (
                                                SELECT Sum(IID.RESOLUTION_ADJUSTED_QTY)
                                                FROM IM_INVOICE_DETAIL IID
                                                WHERE IID.DOC_ID = IPD.DOC_ID
                                                AND   IID.ITEM = IRIP.ITEM
                                            ),

                                            (
                                                SELECT SUM(NVL(SS.QTY_MATCHED, SS.QTY_RECEIVED))
                                                FROM V_IM_SHIPSKU SS
                                                WHERE SS.SHIPMENT = IRIP.SHIPMENT
                                                AND   SS.ITEM     = IRIP.ITEM
                                            )
                                        )
                                              ELSE
                                    IRIP.QTY_MATCHED
                            END AS QUANTITY,
                            -- Unit cost from shipsku
                            (
                                SELECT UNIT_COST
                                FROM V_IM_SHIPSKU
                                WHERE SHIPMENT = IRIP.SHIPMENT
                                AND ITEM = IRIP.ITEM
                                AND ROWNUM = 1
                            ) AS UNIT_COST,
                                IPD.DECIMAL_DIGITS
                                FROM      IM_POSTING_DOC IPD,
                                          IM_RCPT_ITEM_POSTING_INVOICE IRIPI,
                                          IM_RECEIPT_ITEM_POSTING IRIP
                                WHERE     IPD.POSTING_ID = I_POSTING_ID
                                AND IPD.CALC_BASIS = BASIS_HEADER
                                AND NOT EXISTS (SELECT '1' FROM IM_DOC_DETAIL_REASON_CODES IDDRC WHERE IDDRC.DOC_ID = IPD.DOC_ID)

                                AND IRIPI.DOC_ID = IPD.DOC_ID
                                AND IRIP.SEQ_NO = IRIPI.SEQ_NO
          )   MERCHS,
          ITEM_MASTER IM
          WHERE MERCHS.ITEM = IM.ITEM
          GROUP BY MERCHS.POSTING_ID,
                MERCHS.DOC_ID,
                MERCHS.TYPE,
                IM.DEPT,
                IM.CLASS,
                --MERCHS.VAT_CODE,
                --MERCHS.VAT_RATE,
                MERCHS.MATCH_ID  ;


        RETURN SUCCESS;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ;



/*******************

Identifies approved non-merchandise type documents

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION PREPARE_APPROVED_NONMERCH(O_ERROR_MESSAGE  OUT VARCHAR2,
                                          I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                          I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                          I_SYS_CURR_CODE IN SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_APPROVED_NONMERCH';
        L_DOC_ID_COUNT  NUMBER(6);
    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BGIN'|| TDELIM || L_PROGRAM);

        IF I_DOC_ID_LIST IS NULL
        THEN
          L_DOC_ID_COUNT := 0;
        ELSE
          L_DOC_ID_COUNT := I_DOC_ID_LIST.COUNT;
        END IF;

        INSERT INTO IM_POSTING_DOC
                     (POSTING_ID,
                        DOC_ID,
                        DOC_TYPE,
                        CALC_BASIS,
                        MATCH_TYPE,
                        RESOLUTION_COUNT,
                        DOC_TAX_COUNT,
                        PARENT_ID,
                        DOC_DATE,
                        VENDOR_TYPE,
                        VENDOR,
                        ORDER_NO,
                        BEST_TERMS,
                        BEST_TERMS_DATE,
                        MANUALLY_PAID_IND,
                        PRE_PAID_IND,
                        DEAL_ID,
                        EXT_DOC_ID,
                        CURRENCY_CODE,
                        LOCATION,
                        LOC_TYPE,
                        DECIMAL_DIGITS,
                        SYS_CURRENCY_CODE)
        (SELECT I_POSTING_ID,
                  IDH.DOC_ID,
                  IDH.TYPE,
                  BASIS_HEADER AS CALC_BASIS,
                  MATCH_TYPE_UNKNOWN AS MATCH_TYPE,
                  0   AS RESOLUTION_COUNT,
                  NVL((SELECT COUNT(* )
                   FROM   IM_DOC_TAX
                   WHERE  DOC_ID = IDH.DOC_ID), 0) AS doc_tax_count,
                  IDH.PARENT_ID,
                  IDH.DOC_DATE,
                  IDH.VENDOR_TYPE,
                  IDH.VENDOR,
                  IDH.ORDER_NO,
                  IDH.BEST_TERMS,
                  IDH.BEST_TERMS_DATE,
                  IDH.MANUALLY_PAID_IND,
                  IDH.PRE_PAID_IND,
                  IDH.DEAL_ID,
                  IDH.EXT_DOC_ID,
                  IDH.CURRENCY_CODE,
                  IDH.LOCATION,
                  IDH.LOC_TYPE,
                  NVL((SELECT MAX(ICL.CURRENCY_COST_DEC) FROM IM_CURRENCIES ICL WHERE ICL.CURRENCY_CODE = IDH.CURRENCY_CODE), REIM_POSTING_SQL.DEFAULT_DIGITS),
                  I_SYS_CURR_CODE
         FROM   IM_DOC_HEAD IDH
         WHERE  IDH.TYPE IN (DOC_TYPE_NONMERCH_INVOICE)
         AND IDH.STATUS IN (REIM_POSTING_SQL.DOC_STATUS_APPROVED)
         AND (
                      (L_DOC_ID_COUNT > 0
                          AND IDH.DOC_ID IN (
                              SELECT * FROM TABLE(CAST(I_DOC_ID_LIST AS OBJ_NUMERIC_ID_TABLE))
                              )
                      )
                      OR
                      (
                          L_DOC_ID_COUNT = 0
                      )
                  )
         );

        IF UPDATE_POSTING_DOC_SOB(O_ERROR_MESSAGE,
                                  I_POSTING_ID) = FAIL then
           RETURN FAIL;
        END IF;

        RETURN SUCCESS;
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;
    END ;




/*******************

Identifies receipt write-offs for posting.

NOTE:  Dealing with receipt write-offs is a bit complicated in trying
to fit it into the current posting process.   Receipt write-offs don't
have documents that's why they are being written off.   However,
in order to make this process work,   we need to pretend
that there is a document with zero (0) value to reuse a lot of our
posting functions.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_RECEIPT_WRITE_OFF:    Required list of receipt ID's to be written off.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION PREPARE_RECEIPT_WRITEOFFS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                      I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                      I_RCPT_WRITE_OFFS  IN OBJ_REIM_RCPT_WRITEOFF_TBL,
                                      I_SYS_CURR_CODE IN SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_RECEIPT_WRITEOFFS';
        L_DUMMY_DOC_ID NUMBER(10);
        L_WO OBJ_REIM_RCPT_WRITEOFF_TBL := I_RCPT_WRITE_OFFS;
    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        SELECT IM_DOC_HEAD_SEQ.NEXTVAL INTO L_DUMMY_DOC_ID FROM DUAL;

        INSERT INTO IM_POSTING_DOC
                     (POSTING_ID,
                        DOC_ID,
                        DOC_TYPE,
                        CALC_BASIS,
                        MATCH_TYPE,
                        RESOLUTION_COUNT,
                        DOC_TAX_COUNT,
                        PARENT_ID,
                        DOC_DATE,
                        VENDOR_TYPE,
                        VENDOR,
                        ORDER_NO,
                        BEST_TERMS,
                        BEST_TERMS_DATE,
                        MANUALLY_PAID_IND,
                        PRE_PAID_IND,
                        DEAL_ID,
                        EXT_DOC_ID,
                        CURRENCY_CODE,
                        LOCATION,
                        LOC_TYPE,
                        DECIMAL_DIGITS,
                        SYS_CURRENCY_CODE)
         VALUES  (I_POSTING_ID,
                  L_DUMMY_DOC_ID,
                  DOC_TYPE_RECEIPT_WRITEOFF,
                  BASIS_DETAILS,
                  MATCH_TYPE_UNKNOWN,
                  0,
                  0,
                  NULL, --  AS PARENT_ID,
                  NULL, --  AS DOC_DATE,
                  NULL, --  AS vendor_type,
                  NULL, --  AS VENDOR,
                  NULL, --  AS ORDER_NO,
                  NULL, --  AS BEST_TERMS,
                  NULL, --  AS BEST_TERMS_DATE,
                  YN_NO, --  AS MANUALLY_PAID_IND,
                  YN_NO, --  AS PRE_PAID_IND,
                  NULL, --  AS DEAL_ID,
                  NULL, --  AS EXT_DOC_ID,
                  NULL, --  AS CURRENCY_CODE,
                  NULL, --  AS LOCATION,
                  NULL, --  AS LOC_TYPE,
                  REIM_POSTING_SQL.DEFAULT_DIGITS, --  AS DECIMAL_DIGITS
                  I_SYS_CURR_CODE
         );

        RETURN LOAD_RECEIPT_MERCH (O_ERROR_MESSAGE,
                                   I_POSTING_ID,
                                   L_DUMMY_DOC_ID,
                                   I_RCPT_WRITE_OFFS,
                                   I_SYS_CURR_CODE);

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;
    END ;



/*******************

Populates IM_POSTING_DOC_DEPTCLASS with receipt merchandise costs
per department and class.

NOTE:  See comment on PREPARE_RECEIPT_WRITEOFFS.

This function works like LOAD_DOC_MERCH_COMPONENTS in that they
both impact the same table.   However,   this function is driven
by the input receipt list rather than by any of REIM's match tables.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID:         The "fake" doc id associated with the receipt write offs.
  I_RCPT_WRITE_OFFS: The list of receipt's to be written off.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION LOAD_RECEIPT_MERCH (O_ERROR_MESSAGE  OUT VARCHAR2,
                                 I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                 I_DOC_ID IN IM_DOC_HEAD.DOC_ID%TYPE,
                                 I_RCPT_WRITE_OFFS IN OBJ_REIM_RCPT_WRITEOFF_TBL,
                                 I_SYS_CURR_CODE IN SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.LOAD_RECEIPT_MERCH';
        L_PRORATE_ID NUMBER(10,0);
    BEGIN

          LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

          INSERT INTO IM_POSTING_DOC_DEPTCLASS (
                POSTING_ID,
                DOC_ID,
                TYPE,
                DEPT,
                CLASS,
                EXT_COST,
                --removed in accordance with TCD for Configurable VAT
                --VAT_CODE,
                --VAT_RATE,
                --VAT_AMOUNT,
                MATCH_ID,
                SHIPMENT,
                PRIM_CURRENCY_CODE
          )
          SELECT MERCHS.POSTING_ID,
                MERCHS.DOC_ID,
                MERCHS.TYPE,
                IM.DEPT,
                IM.CLASS,
                SUM(MERCHS.UNMATCHED_AMT) AS EXT_COST,
                --MERCHS.VAT_CODE,
                --MERCHS.VAT_RATE,
                --SUM(ROUND( ROUND(MERCHS.UNMATCHED_AMT * MERCHS.VAT_RATE/100, MERCHS.DECIMAL_DIGITS), merchs.decimal_digits))  AS VAT_AMOUNT,
                MERCHS.MATCH_ID,
                MERCHS.RECEIPT_ID,
                PRIM_CURRENCY_CODE
          FROM (
                    SELECT  Q.POSTING_ID AS POSTING_ID,
                            Q.DOC_ID AS DOC_ID,
                            'receipt' AS TYPE,
                            Q.ITEM AS ITEM,
                            Q.UNMATCHED_AMT,
                            Q.DECIMAL_DIGITS AS DECIMAL_DIGITS,
                            Q.SHIPMENT AS MATCH_ID,
                            Q.SHIPMENT AS RECEIPT_ID,
                            Q.PRIM_CURRENCY_CODE AS PRIM_CURRENCY_CODE
                    FROM     (
                                SELECT DISTINCT IPD.POSTING_ID,
                                          IPD.DOC_ID,
                                          SS.ITEM AS ITEM,
                                          ((Sum(SS.QTY_RECEIVED) over (PARTITION BY ss.shipment,ss.item) - Nvl((SELECT Sum(qty_matched) FROM im_partially_matched_receipts ipmr WHERE ipmr.shipment = ss.shipment AND ipmr.item = ss.item),0))* SS.UNIT_COST) UNMATCHED_AMT,
                                          SP.VAT_REGION,
                                          RWO.SHIPMENT,
                                          NVL((SELECT MAX(ICL.CURRENCY_COST_DEC) FROM IM_CURRENCIES ICL WHERE ICL.CURRENCY_CODE = OH.CURRENCY_CODE), REIM_POSTING_SQL.DEFAULT_DIGITS) AS decimal_digits,
                                          NVL(IPD.SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = S.TO_LOC) AND ROWNUM < 2)) AS PRIM_CURRENCY_CODE
                                FROM      IM_POSTING_DOC IPD,
                                          TABLE (CAST (I_RCPT_WRITE_OFFS AS OBJ_REIM_RCPT_WRITEOFF_TBL)) RWO,
                                          SHIPMENT S,
                                          v_im_SHIPSKU SS,
                                          ORDHEAD OH,
                                          SUPS SP
                                WHERE     IPD.POSTING_ID = I_POSTING_ID
                                          AND ipd.doc_id = I_DOC_ID

                                          AND S.SHIPMENT = RWO.SHIPMENT
                                          AND OH.ORDER_NO = S.ORDER_NO
                                          AND SP.SUPPLIER = OH.SUPPLIER
                                          AND SS.SHIPMENT = S.SHIPMENT
                                          AND SS.ITEM = NVL(RWO.ITEM, SS.ITEM)) Q
          )   MERCHS,
          ITEM_MASTER IM
          WHERE MERCHS.ITEM = IM.ITEM
          GROUP BY MERCHS.POSTING_ID,
                MERCHS.DOC_ID,
                MERCHS.TYPE,
                IM.DEPT,
                IM.CLASS,
                --MERCHS.VAT_CODE,
                --MERCHS.VAT_RATE,
                MERCHS.MATCH_ID,
                PRIM_CURRENCY_CODE;


        --RETURN SUCCESS;
        RETURN UPDATE_CURR_EXCH_RWO(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ;

   /**************************************
CALCULATE RWO COST FOR A DOCUMNET WHICH SHOULD BE INCLUSIVE OF TAX AMOUNT

Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.


  Returns:
  SUCCESS/FAIL


***************************************/

FUNCTION CALC_RWO_COST    (O_ERROR_MESSAGE  OUT VARCHAR2,
                           I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE )

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_RWO_COST';
        L_TRANSACTIONS     OBJ_NUMERIC_ID_TABLE;
        L_LOCATIONS        OBJ_NUMERIC_ID_TABLE;
        L_ITEMS           OBJ_VARCHAR_ID_TABLE;
        L_TAX_BREAKUPS     OBJ_ITEM_TAX_BREAKUP_TBL;
        L_VDATE DATE;
        L_STATUS NUMBER(1);

        CURSOR C_TRANSACTIONS IS
              SELECT UNIQUE MATCH_ID
                    FROM    IM_POSTING_DOC IPD,
                            IM_POSTING_DOC_DEPTCLASS IPDDC
                    WHERE IPD.POSTING_ID = I_POSTING_ID
                        AND   IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF
                        AND   IPD.HAS_ERRORS  = YN_NO
                        AND   IPDDC.DOC_ID = IPD.DOC_ID;


        CURSOR C_LOCATIONS IS
              SELECT  UNIQUE S.TO_LOC
              FROM    IM_POSTING_DOC IPD,
                      IM_POSTING_DOC_DEPTCLASS IPDDC,
                      SHIPMENT S
              WHERE IPD.POSTING_ID = I_POSTING_ID
              AND   IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF
              AND   IPD.HAS_ERRORS  = YN_NO
              AND   IPDDC.DOC_ID = IPD.DOC_ID
              AND   S.SHIPMENT = IPDDC.MATCH_ID;

       CURSOR C_ITEMS IS
              SELECT  UNIQUE SS.ITEM
              FROM    IM_POSTING_DOC IPD,
                      IM_POSTING_DOC_DEPTCLASS IPDDC,
                      SHIPMENT S,
                      V_IM_SHIPSKU SS
              WHERE IPD.POSTING_ID = I_POSTING_ID
              AND   IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF
              AND   IPD.HAS_ERRORS  = YN_NO
              AND   IPDDC.DOC_ID = IPD.DOC_ID
              AND   S.SHIPMENT = IPDDC.MATCH_ID
              AND   SS.SHIPMENT = S.SHIPMENT
              AND   (SS.QTY_RECEIVED - DECODE(SS.QTY_MATCHED, NULL, 0,SS.QTY_MATCHED)) > 0;

    BEGIN

    L_STATUS:=SUCCESS;

    OPEN C_TRANSACTIONS;
        FETCH C_TRANSACTIONS BULK COLLECT INTO L_TRANSACTIONS;
    CLOSE C_TRANSACTIONS;

    OPEN C_LOCATIONS;
            FETCH C_LOCATIONS BULK COLLECT INTO L_LOCATIONS;
    CLOSE C_LOCATIONS;

    OPEN C_ITEMS;
            FETCH C_ITEMS BULK COLLECT INTO L_ITEMS;
    CLOSE C_ITEMS;

     SELECT VDATE INTO L_VDATE FROM PERIOD;

    IF L_STATUS = REIM_POSTING_SQL.FAIL THEN
      RETURN L_STATUS;
    END IF;



    INSERT INTO IM_POSTING_DOC_AMOUNTS
               (POSTING_AMT_ID,
                POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION,
                ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE,
                PRIM_CURRENCY_CODE)

        SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                POSTING_ID,
                DOC_ID,
                AMT_RWO_COST  AMOUNT_TYPE,
                LOC,
               (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
                SET_OF_BOOKS_ID,
                NULL AS TAX_CODE,
                NULL AS TAX_RATE,
                DEPT,
                CLASS,
                NULL AS SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                BASIC_TRANSACTIONS  ACCOUNT_TYPE,
                RECEIPT_WRITE_OFF   ACCOUNT_CODE,
                NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE

                 FROM
                 (

                    SELECT POSTING_ID,
                           DOC_ID,
                           DEPT,
                           CLASS,
                           SUBCLASS,
                           SYS_CURRENCY_CODE,
                           LOC,
                           SET_OF_BOOKS_ID,
                           GROUP_BY_1,
                           GROUP_BY_2,
                           GROUP_BY_3,
                           GROUP_BY_4,
                           GROUP_BY_5,
                           GROUP_BY_6,
                           GROUP_BY_7,
                           GROUP_BY_8,
                           ROUND(SUM(AMOUNT),DECIMAL_DIGITS) AS AMOUNT
                    FROM(
                          --SubQuery to fetch receipt tax information
                          SELECT IPD.POSTING_ID,
                                 IPD.DOC_ID,
                                 IM.DEPT,
                                 IM.CLASS,
                                 NULL AS SUBCLASS,
                                 IPD.SYS_CURRENCY_CODE,
                                 S.TO_LOC LOC,
                                 MLS.SET_OF_BOOKS_ID,
                                 TO_CHAR(S.SHIPMENT)                      AS GROUP_BY_1,
                                 TO_CHAR(S.RECEIVE_DATE, 'YYYY-MM-DD')    AS GROUP_BY_2,
                                 TO_CHAR(S.ORDER_NO)                      AS GROUP_BY_3,
                                 S.TO_LOC_TYPE||'^'||TO_CHAR(S.TO_LOC)    AS GROUP_BY_4,
                                 'SUPP'||'^'||DECODE(SUPS.SUPPLIER_PARENT, NULL,SUPS.SUPPLIER,SUPS.SUPPLIER_PARENT ) AS GROUP_BY_5,
                                 IPDDC.CURRENCY_CODE||'^'||TO_CHAR(IPDDC.EXCHANGE_RATE)||'^'||IPDDC.EXCHANGE_TYPE  AS GROUP_BY_6,
                                 IPDDC.PRIM_CURRENCY_CODE AS GROUP_BY_7,
                                 NO_VALUE AS GROUP_BY_8,
                                 NVL((TAX_INFO.TAX_AMOUNT * (SS.QTY_RECEIVED - NVL(M.QTY_MATCHED, 0))),0) AS   AMOUNT,
                                 IPD.DECIMAL_DIGITS
                           FROM  IM_POSTING_DOC IPD,
                                 IM_POSTING_DOC_DEPTCLASS IPDDC,
                                 SHIPMENT S,
                                 V_IM_SHIPSKU SS,
                                 ITEM_MASTER IM,
                                 ORDHEAD OH,
                                         SUPS,
                                 IM_PARTIALLY_MATCHED_RECEIPTS M,
                                 TABLE (CAST (L_TAX_BREAKUPS AS OBJ_ITEM_TAX_BREAKUP_TBL)) TAX_INFO,
                                       MV_LOC_SOB MLS,
                                       (SELECT ORDER_NO,
                                         MIN(LOCATION) LOCATION
                                    FROM ORDLOC
                                   GROUP BY ORDER_NO) OL
                          WHERE IPD.POSTING_ID = I_POSTING_ID
                            AND IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF
                            AND IPD.HAS_ERRORS  = YN_NO
                            AND IPDDC.DOC_ID = IPD.DOC_ID
                            AND S.SHIPMENT = IPDDC.MATCH_ID
                            AND OH.ORDER_NO = S.ORDER_NO
                            AND SS.SHIPMENT = S.SHIPMENT
                            AND TAX_INFO.TRAN_NUMBER = S.SHIPMENT
                            AND TAX_INFO.ITEM = SS.ITEM
                            AND IM.ITEM = TAX_INFO.ITEM
                            AND IM.DEPT = IPDDC.DEPT
                            AND IM.CLASS = IPDDC.CLASS
                            AND SS.SHIPMENT = M.SHIPMENT  (+)
                            AND SS.ITEM = M.ITEM (+)
                                   AND SUPS.SUPPLIER = OH.SUPPLIER
                            AND OL.ORDER_NO = OH.ORDER_NO
                            AND MLS.LOCATION = OL.LOCATION
                            AND MLS.LOCATION_TYPE IN (REIM_CONSTANTS.LOC_TYPE_WH, REIM_CONSTANTS.LOC_TYPE_STORE)

                            UNION ALL

                            --SubQuery to fetch the receipt extended cost
                      SELECT  IPD.POSTING_ID,
                              IPD.DOC_ID,
                              IPDDC.DEPT AS DEPT,
                              IPDDC.CLASS AS CLASS,
                              NULL AS SUBCLASS,
                              IPD.SYS_CURRENCY_CODE,
                              S.TO_LOC LOC,
                             MLS.SET_OF_BOOKS_ID,
                              TO_CHAR(S.SHIPMENT)                                   AS GROUP_BY_1,
                              TO_CHAR(S.RECEIVE_DATE, 'YYYY-MM-DD')                     AS GROUP_BY_2,
                              TO_CHAR(S.ORDER_NO)                           AS GROUP_BY_3,
                              S.TO_LOC_TYPE||'^'||TO_CHAR(S.TO_LOC)                         AS GROUP_BY_4,
                              'SUPP'||'^'||DECODE(SUPS.SUPPLIER_PARENT, NULL,SUPS.SUPPLIER,SUPS.SUPPLIER_PARENT ) AS GROUP_BY_5,
                              IPDDC.CURRENCY_CODE||'^'||TO_CHAR(IPDDC.EXCHANGE_RATE)||'^'||IPDDC.EXCHANGE_TYPE  AS GROUP_BY_6,
                              IPDDC.PRIM_CURRENCY_CODE                      AS GROUP_BY_7,
                              NO_VALUE                              AS GROUP_BY_8,
                              IPDDC.EXT_COST AS AMOUNT,
                              IPD.DECIMAL_DIGITS
                      FROM    IM_POSTING_DOC IPD,
                              IM_POSTING_DOC_DEPTCLASS IPDDC,
                              SHIPMENT S,
                              ORDHEAD OH,
                                    SUPS,
                                   MV_LOC_SOB MLS,
                                   (SELECT ORDER_NO,
                                     MIN(LOCATION) LOCATION
                                FROM ORDLOC
                               GROUP BY ORDER_NO) OL
                        WHERE IPD.POSTING_ID = I_POSTING_ID

                          AND IPDDC.POSTING_ID = IPD.POSTING_ID
                          AND IPDDC.DOC_ID = IPD.DOC_ID
                          AND IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF
                          AND S.SHIPMENT = IPDDC.MATCH_ID
                          AND OH.ORDER_NO = S.ORDER_NO
                                AND SUPS.SUPPLIER = OH.SUPPLIER
                         AND OL.ORDER_NO = OH.ORDER_NO
                         AND MLS.LOCATION = OL.LOCATION
                         AND MLS.LOCATION_TYPE IN (REIM_CONSTANTS.LOC_TYPE_WH, REIM_CONSTANTS.LOC_TYPE_STORE)

                )GROUP BY POSTING_ID,
                          DOC_ID,
                          DEPT,
                          CLASS,
                          SUBCLASS,
                          SYS_CURRENCY_CODE,
                          LOC,
                          SET_OF_BOOKS_ID,
                          GROUP_BY_1,
                          GROUP_BY_2,
                          GROUP_BY_3,
                          GROUP_BY_4,
                          GROUP_BY_5,
                          GROUP_BY_6,
                          GROUP_BY_7,
                          GROUP_BY_8,
                          DECIMAL_DIGITS
                          );


        RETURN UPDATE_CURR_EXCH_RWO(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ;



/**************************************
Load the receipt tax amounts.

Parameters:
  O_ERROR_MESSAGE:  Error message output.
  I_TAX_BREAKUPS: Item level taxes.
  I_POSTING_ID:     ID reference to the new posting process.


  Returns:
  SUCCESS/FAIL


***************************************/

FUNCTION LOAD_RWO_TAX_AMOUNT (O_ERROR_MESSAGE  OUT VARCHAR2,
                              I_TAX_BREAKUPS     OBJ_ITEM_TAX_BREAKUP_TBL,
                              I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE )

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.LOAD_RWO_TAX_AMOUNT';

    BEGIN

     INSERT INTO IM_POSTING_DOC_AMOUNTS
               (POSTING_AMT_ID,
                POSTING_ID,
                DOC_ID,
                AMOUNT_TYPE,
                LOCATION,
                ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                ACCOUNT_TYPE,
                ACCOUNT_CODE,
                PRIM_CURRENCY_CODE)

        SELECT  IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                POSTING_ID,
                DOC_ID,
                AMT_TAX  AMOUNT_TYPE,
                LOC,
               (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
                SET_OF_BOOKS_ID,
                TAX_CODE,
                TAX_RATE,
                DEPT,
                CLASS,
                NULL AS SUBCLASS,
                GROUP_BY_1,
                GROUP_BY_2,
                GROUP_BY_3,
                GROUP_BY_4,
                GROUP_BY_5,
                GROUP_BY_6,
                GROUP_BY_7,
                GROUP_BY_8,
                AMOUNT,
                BASIC_TRANSACTIONS  ACCOUNT_TYPE,
                TAX   ACCOUNT_CODE,
                NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE

                 FROM
                 (

                        -- receipt tax information
                         SELECT POSTING_ID,
                                DOC_ID,
                                DEPT,
                                CLASS,
                                SUBCLASS,
                                TAX_CODE,
                                TAX_RATE,
                                SYS_CURRENCY_CODE,
                                LOC,
                                SET_OF_BOOKS_ID,
                                GROUP_BY_1,
                                GROUP_BY_2,
                                GROUP_BY_3,
                                GROUP_BY_4,
                                GROUP_BY_5,
                                GROUP_BY_6,
                                GROUP_BY_7,
                                GROUP_BY_8,
                                ROUND(SUM(AMOUNT),DECIMAL_DIGITS) AS AMOUNT
                          FROM(
                          SELECT IPD.POSTING_ID,
                                 IPD.DOC_ID,
                                 IM.DEPT,
                                 IM.CLASS,
                                 NULL SUBCLASS,
                                 TAX_INFO.TAX_CODE,
                                 TAX_INFO.TAX_RATE,
                                 IPD.SYS_CURRENCY_CODE,
                                 S.TO_LOC LOC,
                                 MLS.SET_OF_BOOKS_ID,
                                 TO_CHAR(S.SHIPMENT)                AS GROUP_BY_1,
                                 TO_CHAR(S.RECEIVE_DATE, 'YYYY-MM-DD')            AS GROUP_BY_2,
                                 TO_CHAR(S.ORDER_NO)  AS GROUP_BY_3,
                                 S.TO_LOC_TYPE||'^'||TO_CHAR(S.TO_LOC)                  AS GROUP_BY_4,
                                 'SUPP'||'^'||TO_CHAR(OH.SUPPLIER)                      AS GROUP_BY_5,
                                 IPDDC.CURRENCY_CODE||'^'||TO_CHAR(IPDDC.EXCHANGE_RATE)||'^'||IPDDC.EXCHANGE_TYPE  AS GROUP_BY_6,
                                 IPDDC.PRIM_CURRENCY_CODE AS GROUP_BY_7,
                                 NO_VALUE AS GROUP_BY_8,
                                 TAX_INFO.TAX_AMOUNT * (SS.QTY_RECEIVED - NVL(M.QTY_MATCHED, 0)) AS AMOUNT,
                                 IPD.DECIMAL_DIGITS
                           FROM  IM_POSTING_DOC IPD,
                                 IM_POSTING_DOC_DEPTCLASS IPDDC,
                                 SHIPMENT S,
                                 V_IM_SHIPSKU SS,
                                 ORDHEAD OH,
                                 ITEM_MASTER IM,
                                 IM_PARTIALLY_MATCHED_RECEIPTS M,
                                 TABLE (CAST (I_TAX_BREAKUPS AS OBJ_ITEM_TAX_BREAKUP_TBL)) TAX_INFO,
                                       MV_LOC_SOB MLS,
                                       (SELECT ORDER_NO,
                                         MIN(LOCATION) LOCATION
                                    FROM ORDLOC
                                   GROUP BY ORDER_NO) OL
                           WHERE IPD.POSTING_ID = I_POSTING_ID
                             AND IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF
                             AND IPD.HAS_ERRORS  = YN_NO
                             AND IPDDC.DOC_ID = IPD.DOC_ID
                             AND S.SHIPMENT = IPDDC.MATCH_ID
                             AND OH.ORDER_NO = S.ORDER_NO
                             AND SS.SHIPMENT = S.SHIPMENT
                             AND TAX_INFO.TRAN_NUMBER = S.SHIPMENT
                             AND TAX_INFO.ITEM = SS.ITEM
                             AND IM.ITEM = TAX_INFO.ITEM
                             AND IM.DEPT = IPDDC.DEPT
                             AND IM.CLASS = IPDDC.CLASS
                             AND SS.SHIPMENT = M.SHIPMENT  (+)
                             AND SS.ITEM = M.ITEM (+)
                             AND OL.ORDER_NO = OH.ORDER_NO
                             AND MLS.LOCATION = OL.LOCATION
                             AND MLS.LOCATION_TYPE IN (REIM_CONSTANTS.LOC_TYPE_WH, REIM_CONSTANTS.LOC_TYPE_STORE)
                            )

                            GROUP BY POSTING_ID,
                                    DOC_ID,
                                    DEPT,
                                    CLASS,
                                    SUBCLASS,
                                    TAX_CODE,
                                    TAX_RATE,
                                    SYS_CURRENCY_CODE,
                                    LOC,
                                    SET_OF_BOOKS_ID,
                                    GROUP_BY_1,
                                    GROUP_BY_2,
                                    GROUP_BY_3,
                                    GROUP_BY_4,
                                    GROUP_BY_5,
                                    GROUP_BY_6,
                                    GROUP_BY_7,
                                    GROUP_BY_8,
                                    DECIMAL_DIGITS
               );

             RETURN SUCCESS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END ;

/*******************

Prepares Credit Notes for Discrepancy Write Off.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.
  I_DOC_ID_LIST:    Optional list of document ID's to be selected for posting.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION  PREPARE_DWO_CREDIT_NOTES(O_ERROR_MESSAGE  OUT VARCHAR2,
                                      I_POSTING_ID  IN IM_POSTING_DOC.POSTING_ID%TYPE,
                                      I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE,
                                      I_SYS_CURR_CODE SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)

        RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.PREPARE_DWO_CREDIT_NOTES';
        L_DOC_ID_COUNT  NUMBER(6);
        L_DOC_TYPE_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_STATUS_LIST  OBJ_VARCHAR_ID_TABLE;
        L_DOC_HOLD_STATUS  OBJ_VARCHAR_ID_TABLE;
        L_DOC_RESOLUTION_ACTIONS OBJ_VARCHAR_ID_TABLE;
    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

        L_DOC_TYPE_LIST   := NEW OBJ_VARCHAR_ID_TABLE(DOC_TYPE_CREDIT_NOTE);

        L_DOC_STATUS_LIST := NEW OBJ_VARCHAR_ID_TABLE(DOC_STATUS_MATCHED);

        L_DOC_HOLD_STATUS := NEW OBJ_VARCHAR_ID_TABLE(DOC_NOT_HELD);

        L_DOC_RESOLUTION_ACTIONS := NEW OBJ_VARCHAR_ID_TABLE(REASON_DISCREPANCY_WRITEOFF);

        LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM);

        RETURN PREPARE_RESOLUTIONS(O_ERROR_MESSAGE,
                                            I_POSTING_ID,
                                            I_DOC_ID_LIST,
                                            L_DOC_TYPE_LIST,
                                            L_DOC_STATUS_LIST,
                                            L_DOC_HOLD_STATUS,
                                            I_SYS_CURR_CODE,
                                            L_DOC_RESOLUTION_ACTIONS,
                                            REASON_STATUS_UNROLLED);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

        RETURN FAIL;


    END ;





/*******************

Calculates the Discrepancy Write Off cost of credit notes.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION CALC_CREDIT_NOTE_DWO (O_ERROR_MESSAGE  OUT  VARCHAR2,
                                   I_POSTING_ID     IN   IM_POSTING_DOC.POSTING_ID%TYPE)
    RETURN NUMBER
    IS

        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_CREDIT_NOTE_DWO';
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

        CURSOR DWO_CREDIT_NOTES IS
            SELECT POSTING_ID,
                  DOC_ID,
                  LOC,
               (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = LOC AND ROWNUM < 2  ) AS ORG_UNIT,
                  SET_OF_BOOKS_ID,
                  REASON_CODE_ID,
                  ACCOUNT_CODE,
                  ACTION,
                  DEPT,
                  CLASS,
                  TAX_CODE,
                  TAX_RATE,
                  RQ_REASON,
                  RQ_ACTION,
                  AMOUNT,
                  TAX_AMOUNT,
                  NVL(SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = LOC) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
                   FROM (
            SELECT POSTING_ID,
                  DOC_ID,
                  LOCATION LOC,
                  SET_OF_BOOKS_ID,
                  SYS_CURRENCY_CODE,
                  REASON_CODE_ID,
                  ACCOUNT_CODE,
                  ACTION,
                  DEPT,
                  CLASS,
                  TAX_CODE,
                  TAX_RATE,
                  RQ_REASON,
                  RQ_ACTION,
                  SUM(AMOUNT) AS AMOUNT,
                  SUM(TAX_AMOUNT) AS TAX_AMOUNT
            FROM (
                  SELECT UNIQUE DWOS.POSTING_ID,
                        DWOS.DOC_ID,
                        RQS.LOCATION,
                        RQS.SYS_CURRENCY_CODE,
                        DWOS.REASON_CODE_ID,  -- dwo cn reason
                        DWOS.ACCOUNT_CODE,
                        DWOS.ACTION,       -- dwo cn action
                        DWOS.AMOUNT,       -- dwo cn amount
                        DWOS.TAX_AMOUNT,
                        DWOS.TAX_CODE,
                        DWOS.TAX_RATE,
                        DWOS.ITEM,
                        DWOS.SET_OF_BOOKS_ID,
                        IM.DEPT,
                        IM.CLASS,
                        RQS.RQ_REASON,  -- reason from cn request matched to cn
                        RQS.RQ_ACTION,  -- action from cn request matched to cn
                        RQ_DOC_TOTAL
                        ,Rank() over (PARTITION BY DWOS.POSTING_ID,
                                                    DWOS.DOC_ID,
                                                    DWOS.REASON_CODE_ID,
                                                    DWOS.ACTION,
                                                    DWOS.AMOUNT,
                                                    DWOS.TAX_AMOUNT,
                                                    DWOS.TAX_CODE,
                                                    DWOS.TAX_RATE,
                                                    DWOS.ITEM
                                      ORDER BY RQS.RQ_DOC_TOTAL DESC)  AS rank
                  FROM
                          (
                                --- Collect all CN-DWO details
                                --- .... for unit cost matched details...
                                SELECT  IPD.POSTING_ID,
                                        IPD.DOC_ID,
                                        IPD.SET_OF_BOOKS_ID,
                                        R.REASON_CODE_ID,
                                        IRTCM.TRAN_CODE ACCOUNT_CODE,
                                        R.ACTION,
                                        ROUND(SUM(R.EXTENDED_COST), IPD.DECIMAL_DIGITS) AS AMOUNT,
                                        ROUND(SUM(R.EXTENDED_COST * DT.TAX_RATE/100), IPD.DECIMAL_DIGITS) AS TAX_AMOUNT,
                                        DT.TAX_CODE AS TAX_CODE,
                                        DT.TAX_RATE AS TAX_RATE,
                                        D.ITEM
                                FROM    IM_RESOLUTION_ACTION R,
                                        IM_DOC_DETAIL_REASON_CODES D,
                                        IM_DOC_DETAIL_RC_TAX DT,
                                        IM_POSTING_DOC IPD,
                                        IM_REASON_TRAN_CODE_MAP IRTCM
                                WHERE   IPD.POSTING_ID = I_POSTING_ID
                                        AND D.DOC_ID = IPD.DOC_ID
                                        AND R.DOC_ID = D.DOC_ID
                                        AND DT.IM_DOC_DETAIL_REASON_CODES_ID = D.IM_DOC_DETAIL_REASON_CODES_ID
                                        AND R.ACTION = REASON_DISCREPANCY_WRITEOFF
                                        AND R.ITEM = D.ITEM
                                        AND R.UNIT_COST IS NOT NULL
                                        AND IRTCM.REASON_CODE_ID  = R.REASON_CODE_ID
                                        AND IRTCM.SET_OF_BOOKS_ID = IPD.SET_OF_BOOKS_ID
                                GROUP BY IPD.POSTING_ID, IPD.DOC_ID, IPD.SET_OF_BOOKS_ID, R.REASON_CODE_ID, IRTCM.TRAN_CODE, R.ACTION,D.ITEM, DT.TAX_CODE,DT.TAX_RATE, IPD.DECIMAL_DIGITS
                                UNION ALL
                                --- .... for quantity matched details...
                                SELECT  IPD.POSTING_ID,
                                        IPD.DOC_ID,
                                        IPD.SET_OF_BOOKS_ID,
                                        R.REASON_CODE_ID,
                                        IRTCM.TRAN_CODE ACCOUNT_CODE,
                                        R.ACTION,
                                        ROUND(SUM(R.QTY * D.ADJUSTED_UNIT_COST), IPD.DECIMAL_DIGITS) AS AMOUNT,
                                        ROUND(SUM(R.QTY * D.ADJUSTED_UNIT_COST * DT.TAX_RATE/100), IPD.DECIMAL_DIGITS) AS TAX_AMOUNT,
                                        DT.TAX_CODE AS TAX_CODE,
                                        DT.TAX_RATE AS TAX_RATE,
                                        D.ITEM
                                FROM    IM_RESOLUTION_ACTION R,
                                        IM_DOC_DETAIL_REASON_CODES D,
                                        IM_DOC_DETAIL_RC_TAX DT,
                                        IM_POSTING_DOC IPD,
                                        IM_REASON_TRAN_CODE_MAP IRTCM
                                WHERE   IPD.POSTING_ID = I_POSTING_ID
                                        AND D.DOC_ID = IPD.DOC_ID
                                        AND R.DOC_ID = D.DOC_ID
                                        AND DT.IM_DOC_DETAIL_REASON_CODES_ID = D.IM_DOC_DETAIL_REASON_CODES_ID
                                        AND R.ACTION = REASON_DISCREPANCY_WRITEOFF
                                        AND R.ITEM = D.ITEM
                                        AND R.QTY IS NOT NULL
                                        AND IRTCM.REASON_CODE_ID  = R.REASON_CODE_ID
                                        AND IRTCM.SET_OF_BOOKS_ID = IPD.SET_OF_BOOKS_ID
                              GROUP BY IPD.POSTING_ID, IPD.DOC_ID, IPD.SET_OF_BOOKS_ID, R.REASON_CODE_ID, IRTCM.TRAN_CODE, R.ACTION,D.ITEM, DT.TAX_CODE,DT.TAX_RATE, IPD.DECIMAL_DIGITS

                          ) DWOS,

                          (
                              --- Collect All CNR doc, item, reason associated with
                              --- each postable CN-DWO.
                              SELECT  IPD.DOC_ID          AS NT_DOC_ID,
                                      CNR.DOC_ID          AS RQ_DOC_ID,
                                      CNR.ITEM            AS RQ_ITEM,
                                      CNR.REASON_CODE_ID  AS RQ_REASON,
                                      IRTCM.TRAN_CODE     AS ACCOUNT_CODE,
                                      IRC.ACTION          AS RQ_ACTION,
                                      IDH.TOTAL_COST      AS RQ_DOC_TOTAL,
                                      IPD.LOCATION AS LOCATION,
                                      IPD.SYS_CURRENCY_CODE,
                                      CNR.MATCH_ID
                              FROM    IM_POSTING_DOC IPD,
                                      IM_CN_DETAIL_MATCH_HIS CNR,
                                      IM_DOC_HEAD IDH,
                                      IM_REASON_CODES IRC,
                                      IM_REASON_TRAN_CODE_MAP IRTCM
                              WHERE IPD.POSTING_ID = I_POSTING_ID
                              AND   CNR.MATCH_ID IN (SELECT ICDMH.MATCH_ID FROM IM_CN_DETAIL_MATCH_HIS ICDMH WHERE ICDMH.DOC_ID = IPD.DOC_ID)
                              AND   CNR.DOC_ID <> IPD.DOC_ID
                              AND   IDH.DOC_ID = CNR.DOC_ID
                              AND   CNR.REASON_CODE_ID = IRC.REASON_CODE_ID
                              AND   IRTCM.REASON_CODE_ID  = IRC.REASON_CODE_ID
                              AND   IRTCM.SET_OF_BOOKS_ID = IPD.SET_OF_BOOKS_ID

                          ) RQS,
                          item_master im
                  WHERE DWOS.DOC_ID = RQS.NT_DOC_ID
                  AND   DWOS.ITEM = RQS.RQ_ITEM
                  AND   IM.ITEM = DWOS.ITEM) dwo
            WHERE dwo.Rank = 1
            GROUP BY POSTING_ID,
                  DOC_ID,
                  LOCATION,
                  SET_OF_BOOKS_ID,
                  SYS_CURRENCY_CODE,
                  REASON_CODE_ID,
                  ACCOUNT_CODE,
                  ACTION,
                  DEPT,
                  CLASS,
                  TAX_CODE,
                  TAX_RATE,
                  RQ_REASON,
                  RQ_ACTION);

    BEGIN
        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
              INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

        FOR REC IN DWO_CREDIT_NOTES
        LOOP
              -- Insert actual discrepancy write-off
              INSERT INTO IM_POSTING_DOC_AMOUNTS IPDA
                          (POSTING_AMT_ID,
                            POSTING_ID,
                              DOC_ID,
                              LOCATION,
                              ORG_UNIT,
                              SET_OF_BOOKS_ID,
                              AMOUNT_TYPE,
                              DEPT,
                              CLASS,
                              SUBCLASS,
                              TAX_CODE,
                              TAX_RATE,
                              GROUP_BY_1,
                              GROUP_BY_2,
                              GROUP_BY_3,
                              GROUP_BY_4,
                              GROUP_BY_5,
                              GROUP_BY_6,
                              GROUP_BY_7,
                              GROUP_BY_8,
                              AMOUNT,
                              ACCOUNT_TYPE,
                              ACCOUNT_CODE,
                              PRIM_CURRENCY_CODE)
              VALUES (   IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                            REC.POSTING_ID,
                            REC.DOC_ID,
                            REC.LOC,
                            REC.ORG_UNIT,
                            REC.SET_OF_BOOKS_ID,
                            AMT_CREDIT_NOTE_DWO,
                            REC.DEPT,
                            REC.CLASS,
                            NULL,
                            DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,REC.TAX_CODE),
                            DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,REC.TAX_RATE),
                            REC.ACTION, -- GROUP_BY_1,
                            REC.REASON_CODE_ID,      -- GROUP_BY_2,
                            NO_VALUE,        --GROUP_BY_3,
                            NO_VALUE,        --  GROUP_BY_4,
                            NO_VALUE,        --  GROUP_BY_5,
                            NO_VALUE,        -- GROUP_BY_6,
                            NO_VALUE,        --  GROUP_BY_7,
                            NO_VALUE,        --  GROUP_BY_8,
                            REC.AMOUNT,
                            REASON_CODE_ACTIONS,
                            REC.ACCOUNT_CODE,
                            REC.PRIM_CURRENCY_CODE
              );


              --  Insert offset to discrepancy write-off
              INSERT INTO IM_POSTING_DOC_AMOUNTS IPDA
                          (POSTING_AMT_ID,
                            POSTING_ID,
                              DOC_ID,
                              LOCATION,
                              ORG_UNIT,
                              SET_OF_BOOKS_ID,
                              AMOUNT_TYPE,
                              DEPT,
                              CLASS,
                              SUBCLASS,
                              TAX_CODE,
                              TAX_RATE,
                              GROUP_BY_1,
                              GROUP_BY_2,
                              GROUP_BY_3,
                              GROUP_BY_4,
                              GROUP_BY_5,
                              GROUP_BY_6,
                              GROUP_BY_7,
                              GROUP_BY_8,
                              AMOUNT,
                              ACCOUNT_TYPE,
                              ACCOUNT_CODE,
                              PRIM_CURRENCY_CODE)
              VALUES ( IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                            REC.POSTING_ID,
                            REC.DOC_ID,
                            REC.LOC,
                            REC.ORG_UNIT,
                            REC.SET_OF_BOOKS_ID,
                            AMT_CREDIT_NOTE_DWO_OFFSET,
                            REC.DEPT,
                            REC.CLASS,
                            NULL,
                            DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,REC.TAX_CODE),
                            DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,REC.TAX_RATE),
                            REC.RQ_ACTION, -- GROUP_BY_1,
                            REC.RQ_REASON,  -- GROUP_BY_2,
                            NO_VALUE,        --GROUP_BY_3,
                            NO_VALUE,        --  GROUP_BY_4,
                            NO_VALUE,        --  GROUP_BY_5,
                            NO_VALUE,        -- GROUP_BY_6,
                            NO_VALUE,        --  GROUP_BY_7,
                            NO_VALUE,        --  GROUP_BY_8,
                            (REC.AMOUNT) * -1,
                            REASON_CODE_ACTIONS,
                            REC.ACCOUNT_CODE,
                            REC.PRIM_CURRENCY_CODE
              );

        END LOOP;


        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END;





/*******************

Calculates the Variance within Tolerance of matched credit notes.

  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION CALC_CREDIT_NOTE_VWT (O_ERROR_MESSAGE  OUT  VARCHAR2,
                                   I_POSTING_ID     IN   IM_POSTING_DOC.POSTING_ID%TYPE)
   RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_CREDIT_NOTE_VWT';

        L_VWT_SUMMARY_BY_MATCH IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;
        L_VWT_SUMMARY_LEVEL  IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;
        L_VWT_DETAIL_LEVEL   IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;
        L_PRORATE_ACROSS_TAX_CODES  VARCHAR2(1);

    BEGIN
        SELECT NVL(PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO)
              INTO L_PRORATE_ACROSS_TAX_CODES FROM IM_SYSTEM_OPTIONS;

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);
        ---- Step 1 and  2:   Prorate total variance across CREDIT notes within a match.

        LOG_DEBUG(I_POSTING_ID,'Prorate total variance across CREDIT notes within a match');

        L_VWT_SUMMARY_BY_MATCH := REIM_PRORATE_SQL.GET_NEXT_PRORATE_WORKSPACE_ID;

        INSERT INTO IM_PRORATE_WORKSPACE(PRORATE_WORKSPACE_ID,
                                         PRORATE_GROUP_KEY,
                                         DECIMAL_SCALE,
                                         AMOUNT_TO_PRORATE_1,
                                         SHARE_IN_GROUP_1,
                                         REF_ROWID_1)
        SELECT L_VWT_SUMMARY_BY_MATCH                   AS PRORATE_WORKSPACE_ID,
               IPD.POSTING_ID||'^'||ISMIH.MATCH_ID  AS PRORATE_GROUP_KEY,
               IPD.DECIMAL_DIGITS                   AS DECIMAL_SCALE,
               SUM(IDH.VARIANCE_WITHIN_TOLERANCE) OVER(PARTITION BY ISMIH.MATCH_ID ) AS AMOUNT_TO_PRORATE_1,
               IDH.TOTAL_COST AS SHARE_IN_GROUP_1,
               IPD.ROWID                            AS REF_ROWID_1
        FROM   IM_DOC_HEAD IDH,
               im_cn_summary_match_his ISMIH,
               IM_POSTING_DOC IPD
        WHERE  IDH.DOC_ID = ISMIH.DOC_ID
               AND IDH.DOC_ID = IPD.DOC_ID
               AND IPD.POSTING_ID = I_POSTING_ID;


        REIM_PRORATE_SQL.DO_PRORATION(L_VWT_SUMMARY_BY_MATCH);

        LOG_DEBUG(I_POSTING_ID,'Prorate variance of a CREDIT NOTE across vat codes within the credit note');

        --- Step 3 :  Prorate variance of a CREDIT NOTE across vat codes within the credit note.

        L_VWT_SUMMARY_LEVEL := REIM_PRORATE_SQL.GET_NEXT_PRORATE_WORKSPACE_ID;

        INSERT INTO IM_PRORATE_WORKSPACE(PRORATE_WORKSPACE_ID,
                                         PRORATE_GROUP_KEY,
                                         DECIMAL_SCALE,
                                         AMOUNT_TO_PRORATE_1,
                                         SHARE_IN_GROUP_1,
                                         REF_ROWID_1,
                                         REF_ROWID_2)
        SELECT L_VWT_SUMMARY_LEVEL            AS PRORATE_WORKSPACE_ID,
               IPD.POSTING_ID||'^'||IPD.DOC_ID   AS PRORATE_GROUP_KEY,
               IPD.DECIMAL_DIGITS                AS DECIMAL_SCALE,
               IPW.RESULT_PRORATED_AMOUNT_1      AS AMOUNT_TO_PRORATE_1,
               NVL(IDT.TAX_BASIS, 1)             AS SHARE_IN_GROUP_1,
               IPD.ROWID                         AS REF_ROWID_1,
               IDT.ROWID                         AS REF_ROWID_2
        FROM   IM_PRORATE_WORKSPACE IPW,
               IM_POSTING_DOC IPD,
               IM_DOC_TAX IDT
        WHERE  IPW.PRORATE_WORKSPACE_ID = L_VWT_SUMMARY_BY_MATCH
        AND    IPD.ROWID = IPW.REF_ROWID_1
        AND    IDT.DOC_ID(+) = IPD.DOC_ID;


        REIM_PRORATE_SQL.DO_PRORATION(L_VWT_SUMMARY_LEVEL);


        /**********************************

        DEALING WITH DETAIL MATCHES

        This process merely involves going through the details and calculating
        the VWT for each doc based on the provided cost and qty vwt components.

        ************************************/


        --- Step 3 :  Prorate variance of a CREDIT NOTE across vat codes within the credit note.

        L_VWT_DETAIL_LEVEL := REIM_PRORATE_SQL.GET_NEXT_PRORATE_WORKSPACE_ID;

        INSERT INTO IM_PRORATE_WORKSPACE(PRORATE_WORKSPACE_ID,
                                         PRORATE_GROUP_KEY,
                                         DECIMAL_SCALE,
                                         AMOUNT_TO_PRORATE_1,
                                         SHARE_IN_GROUP_1,
                                         REF_ROWID_1,
                                         REF_ROWID_2)
        SELECT L_VWT_DETAIL_LEVEL            AS PRORATE_WORKSPACE_ID,
               IPD.POSTING_ID||'^'||IPD.DOC_ID   AS PRORATE_GROUP_KEY,
               IPD.DECIMAL_DIGITS                AS DECIMAL_SCALE,
               IDH.VARIANCE_WITHIN_TOLERANCE     AS AMOUNT_TO_PRORATE_1,
               NVL(IDT.TAX_BASIS, 1)             AS SHARE_IN_GROUP_1,
               IPD.ROWID                         AS REF_ROWID_1,
               IDT.ROWID                         AS REF_ROWID_2
        FROM   IM_DOC_HEAD IDH,
               IM_POSTING_DOC IPD,
               IM_DOC_TAX IDT
        WHERE  IPD.POSTING_ID = I_POSTING_ID
        AND    IDH.DOC_ID = IPD.DOC_ID
        AND    IDT.DOC_ID(+) = IDH.DOC_ID
        AND    IDH.VARIANCE_WITHIN_TOLERANCE <> 0
        AND    IDH.TYPE = DOC_TYPE_CREDIT_NOTE
        AND    EXISTS (SELECT 1 FROM IM_CN_DETAIL_MATCH_HIS ICDMH WHERE ICDMH.DOC_ID = IDH.DOC_ID);


        REIM_PRORATE_SQL.DO_PRORATION(L_VWT_DETAIL_LEVEL);


        FOR REC IN (SELECT IPD.POSTING_ID,
                          IPD.DOC_ID,
                          IPD.LOCATION,
                          (SELECT ORG_UNIT_ID FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                        UNION ALL
                                        SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL THEN WH.ORG_UNIT_ID
                                                ELSE
                                               (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH )
                                                END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH
                                        ) WHERE LOCATION = IPD.LOCATION AND ROWNUM < 2 ) AS ORG_UNIT,
                          IPD.SET_OF_BOOKS_ID,
                          IDT.TAX_CODE AS TAX_CODE,
                          IDT.TAX_RATE AS TAX_RATE,
                          NULL  AS DEPT,
                          NULL  AS CLASS,
                          NULL  AS  SUBCLASS,
                          IPW.RESULT_PRORATED_AMOUNT_1 AS amount,
                          ROUND(IPW.RESULT_PRORATED_AMOUNT_1 * (IDT.TAX_RATE/100), IPD.DECIMAL_DIGITS) AS tax_amount,
                          NO_VALUE   AS  GROUP_BY_1,
                          NO_VALUE   AS  GROUP_BY_2,
                          NO_VALUE   AS  GROUP_BY_3,
                          NO_VALUE   AS  GROUP_BY_4,
                          NO_VALUE   AS  GROUP_BY_5,
                          NO_VALUE   AS  GROUP_BY_6,
                          NO_VALUE   AS  GROUP_BY_7,
                          NO_VALUE   AS  GROUP_BY_8,
                          NVL(IPD.SYS_CURRENCY_CODE, (SELECT LOC_CURRENCY FROM MV_LOC_SOB LOC_SOB
                                            WHERE LOC_SOB.LOCATION IN (SELECT (CASE MLS.LOCATION_TYPE WHEN 'W' THEN
                                              (SELECT (CASE WHEN WAREHOUSE.PRIMARY_VWH IS NULL THEN
                                                            WAREHOUSE.WH
                                                       ELSE
                                                            (SELECT V_WAREHOUSE.WH FROM WH V_WAREHOUSE
                                                            WHERE WAREHOUSE.PRIMARY_VWH = V_WAREHOUSE.WH)
                                                       END)
                                              FROM WH WAREHOUSE WHERE WAREHOUSE.WH = MLS.LOCATION)
                                           ELSE
                                              MLS.LOCATION
                                           END) AS REAL_LOC FROM MV_LOC_SOB MLS WHERE MLS.LOCATION = IPD.LOCATION) AND ROWNUM < 2)) PRIM_CURRENCY_CODE
                    FROM  IM_PRORATE_WORKSPACE IPW,
                          IM_POSTING_DOC IPD,
                          IM_DOC_TAX IDT
                    WHERE  IPW.PRORATE_WORKSPACE_ID IN (L_VWT_SUMMARY_LEVEL, L_VWT_DETAIL_LEVEL)
                    AND    IPW.RESULT_PRORATED_AMOUNT_1 <> 0
                    AND    IPD.ROWID = IPW.REF_ROWID_1
                    AND    IDT.ROWID(+) = IPW.REF_ROWID_2)
        LOOP
              INSERT INTO IM_POSTING_DOC_AMOUNTS IPDA(
                                              POSTING_AMT_ID,
                                              POSTING_ID,
                                              DOC_ID,
                                              LOCATION,
                                              ORG_UNIT,
                                              SET_OF_BOOKS_ID,
                                              AMOUNT_TYPE,
                                              TAX_CODE,
                                              TAX_RATE,
                                              DEPT,
                                              CLASS,
                                              SUBCLASS,
                                              AMOUNT,
                                              ACCOUNT_TYPE,
                                              ACCOUNT_CODE,
                                              --removed in accordance with TCD for Configurable VAT
                                              --AMOUNT_VAT,
                                              GROUP_BY_1,
                                              GROUP_BY_2,
                                              GROUP_BY_3,
                                              GROUP_BY_4,
                                              GROUP_BY_5,
                                              GROUP_BY_6,
                                              GROUP_BY_7,
                                              GROUP_BY_8,
                                              PRIM_CURRENCY_CODE
                                                )
              VALUES ( IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                      REC.POSTING_ID,
                      REC.DOC_ID,
                      REC.LOCATION,
                      REC.ORG_UNIT,
                      REC.SET_OF_BOOKS_ID,
                      AMT_CREDIT_NOTE_VWT,
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,REC.TAX_CODE),
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,REC.TAX_RATE),
                      REC.DEPT,
                      REC.CLASS,
                      REC.SUBCLASS,
                      REC.AMOUNT,
                      BASIC_TRANSACTIONS,
                      VARIANCE_WITHIN_TOLERANCE,
                      --removed in accordance with TCD for Configurable VAT
                      --REC.VAT_AMOUNT * -1,
                      REC.GROUP_BY_1,
                      REC.GROUP_BY_2,
                      REC.GROUP_BY_3,
                      REC.GROUP_BY_4,
                      REC.GROUP_BY_5,
                      REC.GROUP_BY_6,
                      REC.GROUP_BY_7,
                      REC.GROUP_BY_8,
                      REC.PRIM_CURRENCY_CODE

              );

              INSERT INTO IM_POSTING_DOC_AMOUNTS IPDA(POSTING_AMT_ID,
                                                      POSTING_ID,
                                                      DOC_ID,
                                                      LOCATION,
                                                      ORG_UNIT,
                                                      SET_OF_BOOKS_ID,
                                                      AMOUNT_TYPE,
                                                      TAX_CODE,
                                                      TAX_RATE,
                                                      DEPT,
                                                      CLASS,
                                                      SUBCLASS,
                                                      AMOUNT,
                                                      ACCOUNT_TYPE,
                                                      ACCOUNT_CODE,
                                                      --removed in accordance with TCD for Configurable VAT
                                                      --AMOUNT_VAT,
                                                      GROUP_BY_1,
                                                      GROUP_BY_2,
                                                      GROUP_BY_3,
                                                      GROUP_BY_4,
                                                      GROUP_BY_5,
                                                      GROUP_BY_6,
                                                      GROUP_BY_7,
                                                      GROUP_BY_8,
                                                      PRIM_CURRENCY_CODE
                                                      )
              VALUES ( IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                      REC.POSTING_ID,
                      REC.DOC_ID,
                      REC.LOCATION,
                      REC.ORG_UNIT,
                      REC.SET_OF_BOOKS_ID,
                      AMT_CREDIT_NOTE_VWT_OFFSET,
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,REC.TAX_CODE),
                      DECODE(L_PRORATE_ACROSS_TAX_CODES,REIM_POSTING_SQL.YN_NO,NULL,REC.TAX_RATE),
                      REC.DEPT,
                      REC.CLASS,
                      REC.SUBCLASS,
                      REC.AMOUNT * -1,
                      BASIC_TRANSACTIONS,
                      CREDIT_NOTE,
                      --REC.VAT_AMOUNT,
                      REC.GROUP_BY_1,
                      REC.GROUP_BY_2,
                      REC.GROUP_BY_3,
                      REC.GROUP_BY_4,
                      REC.GROUP_BY_5,
                      REC.GROUP_BY_6,
                      REC.GROUP_BY_7,
                      REC.GROUP_BY_8,
                      REC.PRIM_CURRENCY_CODE

              );

        END LOOP;



        REIM_PRORATE_SQL.END_PRORATION(L_VWT_DETAIL_LEVEL);
        REIM_PRORATE_SQL.END_PRORATION(L_VWT_SUMMARY_LEVEL);
        REIM_PRORATE_SQL.END_PRORATION(L_VWT_SUMMARY_BY_MATCH);

        RETURN UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE, I_POSTING_ID);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    END;


/*******************
  Utility method to calculate the CURRENCY RATE for the documents to be posted
  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION UPDATE_CURRENCY_EXCHANGE_RATE(O_ERROR_MESSAGE  OUT VARCHAR2,
                                  I_POSTING_ID IN IM_POSTING_DOC.POSTING_ID%TYPE)

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.UPDATE_CURRENCY_EXCHANGE_RATE';

        L_CONSOLIDATION_IND SYSTEM_CONFIG_OPTIONS.CONSOLIDATION_IND%TYPE := NULL;

    BEGIN

      SELECT CONSOLIDATION_IND
        INTO L_CONSOLIDATION_IND
        FROM SYSTEM_CONFIG_OPTIONS;

    --calculate and insert the currency rate for the particular POSTING_ID
         MERGE INTO IM_POSTING_DOC_AMOUNTS TGT
                USING (
                        SELECT DISTINCT IPDA.POSTING_ID,
                        IPDA.DOC_ID DOC_ID,
                        IPDA.LOCATION,
                        DECODE(IDH.CURRENCY_CODE,LOCS.CURRENCY_CODE,1,IDH.EXCHANGE_RATE/CR.EXCHANGE_RATE) AS EXCHANGE_RATE,
                        CR.EXCHANGE_TYPE,
                        IPDA.POSTING_AMT_ID
                 FROM   IM_POSTING_DOC IPD,
                        IM_POSTING_DOC_AMOUNTS IPDA,
                        IM_DOC_HEAD IDH,
                        ORDHEAD OD,
                        CURRENCY_RATES CR,
                        (SELECT STORE LOCATION,CURRENCY_CODE, 'S' LOC_TYPE FROM STORE
                        UNION ALL
                        SELECT WH, CURRENCY_CODE,'W' LOC_TYPE FROM WH) LOCS
                WHERE   IDH.DOC_ID = IPD.DOC_ID AND
                        IPD.POSTING_ID = I_POSTING_ID AND
                        IDH.ORDER_NO = OD.ORDER_NO AND
                        LOCS.LOCATION = IDH.LOCATION AND
                        LOCS.LOC_TYPE = IDH.LOC_TYPE AND
                        IPD.POSTING_ID = IPDA.POSTING_ID AND
                        CR.CURRENCY_CODE = LOCS.CURRENCY_CODE AND
                        IDH.DOC_ID = IPDA.DOC_ID AND
                        DECODE(L_CONSOLIDATION_IND,'Y','C','O') = CR.EXCHANGE_TYPE AND
                        CR.EFFECTIVE_DATE = (SELECT MAX(CR.EFFECTIVE_DATE)
                                               FROM CURRENCY_RATES CR
                                              WHERE CR.CURRENCY_CODE = LOCS.CURRENCY_CODE
                                                AND OD.WRITTEN_DATE >= CR.EFFECTIVE_DATE)) SRC
                    ON ( SRC.POSTING_ID = TGT.POSTING_ID -- param
                    AND SRC.DOC_ID = TGT.DOC_ID
                    AND SRC.LOCATION = TGT.LOCATION
                    AND SRC.POSTING_AMT_ID = TGT.POSTING_AMT_ID)

                    WHEN MATCHED THEN UPDATE SET TGT.CURRENCY_RATE = SRC.EXCHANGE_RATE,
                                                 TGT.CURRENCY_RATE_TYPE = SRC.EXCHANGE_TYPE

                    WHEN NOT MATCHED THEN INSERT (TGT.POSTING_ID,
                                                  TGT.DOC_ID)
                                                  VALUES (SRC.POSTING_ID,
                                                          SRC.DOC_ID);
                                                          
 /** Adding one more merge to handle if Order number is null mostly in case of Non Merchindise Invoice , Debit and Credit Memos**/
		
 	 MERGE INTO IM_POSTING_DOC_AMOUNTS TGT USING
	  ( SELECT DISTINCT ipd.posting_id, idh.doc_id, locs.location, 
	    DECODE(IDH.CURRENCY_CODE, LOCS.CURRENCY_CODE,1,IDH.EXCHANGE_RATE/CR.EXCHANGE_RATE) AS EXCHANGE_RATE,
	    CR.EXCHANGE_TYPE
	      FROM 
	    IM_POSTING_DOC IPD,
	    (SELECT STORE LOCATION,CURRENCY_CODE, 'S' LOC_TYPE FROM STORE
	      UNION ALL
	      SELECT WH, CURRENCY_CODE,'W' LOC_TYPE FROM WH
	      ) LOCS,
	      IM_DOC_HEAD IDH,
	      CURRENCY_RATES CR,
	      SYSTEM_OPTIONS SYSOP
	     -- ORDHEAD OD
	 WHERE IDH.DOC_ID      = IPD.DOC_ID
	    and IDH.order_no is null
	    AND IPD.POSTING_ID    = I_POSTING_ID
	    AND LOCS.LOCATION     = IDH.LOCATION
	    AND LOCS.LOC_TYPE     = IDH.LOC_TYPE
	    AND CR.CURRENCY_CODE  = LOCS.CURRENCY_CODE
	    and CR.EXCHANGE_TYPE  = DECODE(SYSOP.CONSOLIDATION_IND,'Y','C','O')
	    AND CR.EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_DATE)  FROM CURRENCY_RATES  WHERE CURRENCY_CODE = LOCS.CURRENCY_CODE )
	  ) 
	  SRC ON ( SRC.POSTING_ID = TGT.POSTING_ID AND SRC.DOC_ID = TGT.DOC_ID AND SRC.LOCATION = TGT.LOCATION)
	WHEN MATCHED THEN
	  UPDATE
	  SET TGT.CURRENCY_RATE    = SRC.EXCHANGE_RATE,
	    TGT.CURRENCY_RATE_TYPE = SRC.EXCHANGE_TYPE ;
	    --WHEN NOT MATCHED THEN
	 -- INSERT
	  --  (TGT.POSTING_ID, TGT.DOC_ID
	  ---  ) VALUES
	  --  (SRC.POSTING_ID, SRC.DOC_ID
	  --  );

      LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM);

      RETURN SUCCESS;


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;
    END;


/*******************
  Utility method to calculate the CURRENCY RATE for the documents to be posted
  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
    FUNCTION UPDATE_CURR_EXCH_RWO(O_ERROR_MESSAGE  OUT VARCHAR2,
                                  I_POSTING_ID IN IM_POSTING_DOC.POSTING_ID%TYPE)

    RETURN NUMBER
    IS
        L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.UPDATE_CURR_EXCH_RECEIPT_WO';

        L_CONSOLIDATION_IND SYSTEM_CONFIG_OPTIONS.CONSOLIDATION_IND%TYPE := NULL;


    BEGIN

        LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);
    --calculate and insert the currency rate for the particular POSTING_ID

      SELECT CONSOLIDATION_IND
        INTO L_CONSOLIDATION_IND
        FROM SYSTEM_CONFIG_OPTIONS;

      MERGE INTO IM_POSTING_DOC_DEPTCLASS TGT
                      USING (
                      SELECT DISTINCT IPDD.SHIPMENT,IPDD.POSTING_ID,IPDD.DOC_ID,
                                    OH.CURRENCY_CODE,
                                    OH.EXCHANGE_RATE,
                                    DECODE(L_CONSOLIDATION_IND,YN_YES,EXCHANGE_TYPE_CONSOLIDATED,EXCHANGE_TYPE_OPERATIONAL) EXCHANGE_TYPE
                              FROM  IM_POSTING_DOC_DEPTCLASS IPDD,
                                    SHIPMENT RECEIPT,
                                    ORDHEAD OH
                                    WHERE IPDD.POSTING_ID  = I_POSTING_ID
                                    AND IPDD.SHIPMENT    = RECEIPT.SHIPMENT
                                    AND OH.ORDER_NO        = RECEIPT.ORDER_NO
                              )
                              SRC
                      ON (
                              SRC.POSTING_ID = TGT.POSTING_ID --will be the param
                              AND SRC.SHIPMENT = TGT.SHIPMENT
                      )
                      WHEN MATCHED THEN UPDATE SET TGT.CURRENCY_CODE = SRC.CURRENCY_CODE,
                                                  TGT.EXCHANGE_RATE = SRC.EXCHANGE_RATE,
                                                  TGT.EXCHANGE_TYPE = SRC.EXCHANGE_TYPE
                      WHEN NOT MATCHED THEN INSERT (TGT.POSTING_ID,
                                                    TGT.DOC_ID,
                                                    TGT.SHIPMENT,
                                                    TYPE)
                                                    VALUES
                                                    (SRC.POSTING_ID,SRC.DOC_ID, SRC.SHIPMENT,
                                                  'receipt');--dummy values


      LOG_DEBUG(I_POSTING_ID,'END'|| TDELIM || L_PROGRAM);

      RETURN SUCCESS;


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;
    END;

    /************************************************************
     * Writes to the logging table IM_POSTING_DOC_ERRORS as DEBUG entries.
     ************************************************************/
    PROCEDURE LOG_DEBUG
        (I_POSTING_ID IN IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
         I_MESSAGE IN IM_POSTING_DOC_ERRORS.ERROR_MSG%TYPE)
    IS
        ERR_MSG VARCHAR2(300);
        STATUS NUMBER;
    BEGIN
        IF DEBUG_ENABLED <> 'Y' THEN
            RETURN;
        END IF;
        STATUS := ADD_POSTING_DOC_ERROR(ERR_MSG,
                             I_POSTING_ID,
                             NULL,
                             SEVERITY_DEBUG,
                             ERROR_CAT_OTHER,
                             I_MESSAGE);


    END;

    /************************************************************
     * Writes contents from IM_PRORATE_WORKSPACE into
     * the logging table IM_POSTING_DOC_ERRORS as DEBUG entries.
     ************************************************************/
    PROCEDURE LOG_DEBUG_PRORATE_WORKSPACE
        (I_POSTING_ID NUMBER,
         I_PRORATE_WORKSPACE_ID NUMBER,
         I_COMMENT VARCHAR2)
    IS
    BEGIN
        IF DEBUG_ENABLED <> 'Y' THEN
            RETURN;
        END IF;
        for REC in (SELECT PRORATE_GROUP_KEY,
                           DECIMAL_SCALE,
                           AMOUNT_TO_PRORATE_1,
                           SHARE_IN_GROUP_1,
                           RESULT_PRORATED_AMOUNT_1,
                           share_data_1
                    FROM IM_PRORATE_WORKSPACE WHERE PRORATE_WORKSPACE_ID = I_PRORATE_WORKSPACE_ID)
        LOOP
            REIM_POSTING_SQL.LOG_DEBUG
                (I_POSTING_ID,
                 I_COMMENT||TB||
                 'prorate_grp='||REC.PRORATE_GROUP_KEY||TB||
                 'amt_to_prorate='||REC.AMOUNT_TO_PRORATE_1||TB||
                 'share_in_grp='||REC.SHARE_IN_GROUP_1||TB||
                 'prorated_amt='||REC.RESULT_PRORATED_AMOUNT_1||TB||
                 'share_data='||REC.SHARE_DATA_1);
        end loop;

        RETURN;
    END;

    /************************************************************
     * Returns TRUE if the input resolution action string
     * the logging table IM_POSTING_DOC_ERRORS as DEBUG entries.
     ************************************************************/
    FUNCTION IS_ACCOUNTABLE_RESOLUTION
            (I_RESOLUTION_ACTION IN IM_RESOLUTION_ACTION.ACTION%TYPE)
    RETURN VARCHAR2
    IS
    BEGIN
        IF I_RESOLUTION_ACTION IN ('DD',
                                   'MR',
                                   'RCA',
                                   'RCAMR',
								   'RCASMR',
                                   'RCAS',
                                   'RCD',
                                   'RQD',
                                   'RUA',
                                   'SR',
                                   'UIT') THEN
            RETURN YN_NO;
        END IF;

        RETURN YN_YES;
    END;



    /************************************************************
     * This function provides a consistent calculation of
     * resolution amounts.
     * Given an IM_RESOLUTION_ACTION (IRA) row,  the resolution amount
     * is = to IRA.EXTENDED_COST if populated.   Otherwise,  the
     * following calculations occur:
     *
     *     Resolution Amt = Quantity Res Amt + Cost Res Amt
     *
     * Where:
     *
     *     Quantity Resolution Amount = (IRA.QUANTITY) x (SHIPSKU.UNIT_COST)
     *
     *     (If SHIPSKU.UNIT_COST is not available,   the associated invoice detail
     *      resolution adjusted unit cost will be used)
     *
     *     Cost Resolution Amount = (IRA.UNIT_COST) x (IM_INVOICE_DETAIL.QTY)
     *
     ************************************************************/
    FUNCTION CALC_RESOLUTION_AMOUNT(IM_RESOLUTION_ACTION_ROWID ROWID,
                                    IM_INVOICE_DETAIL_ROWID ROWID)
    RETURN NUMBER
    IS
        L_AMOUNT NUMBER := 0;
    BEGIN
        SELECT
        (
                SELECT
                        CASE
                            WHEN IRA.EXTENDED_COST IS NULL OR IRA.EXTENDED_COST = 0 THEN
                                (
                                    NVL(IRA.QTY, 0) *
                                    NVL( (
                                               SELECT SS.UNIT_COST
                                               FROM IM_RCPT_ITEM_POSTING_INVOICE IRIPI,
                                                    IM_RECEIPT_ITEM_POSTING IRIP,
                                                    V_IM_SHIPSKU SS
                                               WHERE iripi.doc_id = ira.doc_id
                                               AND   irip.seq_no = iripi.seq_no
                                               AND   irip.item = iid.item
                                               AND   SS.SHIPMENT = IRIP.SHIPMENT
                                               AND   SS.ITEM = IRIP.ITEM
                                               AND ROWNUM < 2
                                         ),
                                         IID.RESOLUTION_ADJUSTED_UNIT_COST)
                                )
                                +
                                (
                                    NVL(IRA.UNIT_COST, 0) *
                                    IID.QTY
                                )
                            ELSE
                                IRA.EXTENDED_COST
                        END
                FROM IM_RESOLUTION_ACTION IRA,
                     IM_INVOICE_DETAIL IID
                WHERE IRA.ROWID  = IM_RESOLUTION_ACTION_ROWID
                AND  (
                          (
                              IM_INVOICE_DETAIL_ROWID IS NOT NULL
                              AND IID.ROWID = IM_INVOICE_DETAIL_ROWID
                          )
                          OR
                          (
                              IM_INVOICE_DETAIL_ROWID IS NULL
                              AND   IID.DOC_ID = IRA.DOC_ID
                              AND   IID.ITEM   = IRA.ITEM
                          )
                     )
        )
        INTO L_AMOUNT
        FROM DUAL;

        RETURN NVL(L_AMOUNT, 0);

    EXCEPTION
       WHEN SYS_INVALID_ROWID THEN
           RETURN 0;
    END;

/*******************
  Utility method to update the default department and class for the documents to be posted
  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
   FUNCTION UPDATE_DEFAULT_DEPT_CLASS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                     I_POSTING_ID IN IM_POSTING_DOC.POSTING_ID%TYPE)
   RETURN NUMBER
    IS

    L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.UPDATE_DEFAULT_DEPT_CLASS';

    BEGIN

    /* Update the default department and class for documents*/
    FOR REC IN (SELECT DOC_ID FROM IM_POSTING_DOC
                              WHERE POSTING_ID=I_POSTING_ID
                                    AND DOC_TYPE <> DOC_TYPE_RECEIPT_WRITEOFF
                                    AND HAS_ERRORS <> YN_YES) LOOP

    UPDATE IM_POSTING_DOC SET (DEFAULT_DEPT,DEFAULT_CLASS) =(SELECT DEPT, CLASS FROM (
                                                                                   SELECT IM.DEPT, IM.CLASS FROM RTV_DETAIL RD, ITEM_MASTER IM, IM_DOC_HEAD IDH
                                                                                          WHERE IDH.DOC_ID = REC.DOC_ID
                                                                                                AND IDH.ORDER_NO IS NOT NULL
                                                                                                AND IDH.RTV_IND = YN_YES
                                                                                                AND RD.RTV_ORDER_NO = IDH.RTV_ORDER_NO
                                                                                                AND RD.ITEM = IM.ITEM
                                                                                          GROUP BY im.dept, im.class HAVING Min(rd.seq_no) = 1
                                                                          UNION ALL
                                                                                  SELECT dept, class FROM (SELECT IM.DEPT,IM.CLASS FROM ORDSKU OS, ITEM_MASTER IM, IM_DOC_HEAD IDH
                                                                                                                    WHERE IDH.DOC_ID = REC.DOC_ID
                                                                                                                          AND IDH.ORDER_NO IS NOT NULL
                                                                                                                          AND IDH.RTV_IND = YN_NO
                                                                                                                          AND OS.ORDER_NO = IDH.ORDER_NO
                                                                                                                          AND OS.ITEM = IM.ITEM
                                                                                                                    ORDER BY im.dept) WHERE ROWNUM < 2
                                                                          UNION ALL
                                                                                  SELECT dept, class FROM ( SELECT IM.DEPT,IM.CLASS FROM IM_INVOICE_DETAIL IID, ITEM_MASTER IM, IM_DOC_HEAD IDH
                                                                                                                                        WHERE IID.ITEM = IM.ITEM
                                                                                                                                              AND iid.doc_id = IDH.DOC_ID
                                                                                                                                              AND IDH.DOC_ID = REC.DOC_ID
                                                                                                                                        ORDER BY IM.dept) WHERE ROWNUM < 2
                                                                          UNION ALL
                                                                                  SELECT dept, class FROM  ( SELECT IM.DEPT, IM.CLASS FROM IM_DOC_DETAIL_REASON_CODES IDDRC,ITEM_MASTER IM, IM_DOC_HEAD IDH
                                                                                                                                          WHERE IDH.DOC_ID = REC.DOC_ID
                                                                                                                                                AND IDDRC.DOC_ID = IDH.DOC_ID
                                                                                                                                                AND IDDRC.ITEM = IM.ITEM
                                                                                                                                          ORDER BY IM.dept) WHERE ROWNUM < 2
                                                                          UNION ALL
                                                                                  SELECT  DEPT,CLASS FROM   (SELECT DISTINCT IFDD.DEPT, IFDD.CLASS  FROM IM_FIXED_DEAL_DETAIL IFDD, IM_DOC_HEAD IDH
                                                                                                                                                          WHERE IDH.DOC_ID = REC.DOC_ID
                                                                                                                                                                AND IFDD.DOC_ID = IDH.DOC_ID
                                                                                                                                                          ORDER BY IFDD.DEPT) where rownum < 2
                                                                          UNION ALL
                                                                                  SELECT DEPT, CLASS FROM (SELECT   DISTINCT IM.DEPT,IM.CLASS FROM IM_COMPLEX_DEAL_DETAIL IFDD,ITEM_MASTER IM,IM_DOC_HEAD IDH
                                                                                                                                                  WHERE   IDH.DOC_ID = REC.DOC_ID
                                                                                                                                                          AND IFDD.DOC_ID = IDH.DOC_ID
                                                                                                                                                          AND IM.ITEM = IFDD.ITEM
                                                                                                                                                  ORDER BY IM.DEPT) where rownum < 2
                                                                          ) WHERE ROWNUM < 2 )
    WHERE POSTING_ID = I_POSTING_ID
          AND DOC_ID = REC.DOC_ID;
    END LOOP;

    /*Update default department class for receipts */
    UPDATE IM_POSTING_DOC IPD_TGT SET (DEFAULT_DEPT,DEFAULT_CLASS) =
                    (SELECT IPDDC.DEPT,IPDDC.CLASS FROM IM_POSTING_DOC IPD, IM_POSTING_DOC_DEPTCLASS IPDDC
                    WHERE IPD.POSTING_ID=I_POSTING_ID
                    AND IPD.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF
                    AND IPD.HAS_ERRORS <> YN_YES
                    AND IPD.POSTING_ID = IPDDC.POSTING_ID
                    AND IPD.DOC_ID = IPDDC.DOC_ID
                    AND ROWNUM < 2)
    WHERE IPD_TGT.POSTING_ID=I_POSTING_ID
    AND IPD_TGT.HAS_ERRORS <> YN_YES
    AND IPD_TGT.DOC_TYPE = DOC_TYPE_RECEIPT_WRITEOFF;
  RETURN SUCCESS;


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;
    END;


/*******************
  Utility method to update the location for orders to importer warehouses for the documents to be posted
  Parameters:
  O_ERROR_MESSAGE:  Error message output
  I_POSTING_ID:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/
   FUNCTION UPDATE_IMPORTER_LOCATIONS(O_ERROR_MESSAGE  OUT VARCHAR2,
                                     I_POSTING_ID IN IM_POSTING_DOC.POSTING_ID%TYPE)
   RETURN NUMBER
    IS

    L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.UPDATE_IMPORTER_LOCATIONS';

    BEGIN

    /* Update the importer location for documents for im_posting_doc */
    MERGE INTO IM_POSTING_DOC TGT USING
      ( SELECT IPD.DOC_ID, IDH.LOCATION  FROM IM_DOC_HEAD IDH, IM_POSTING_DOC IPD, ORDHEAD OH
         WHERE IPD.POSTING_ID = I_POSTING_ID

           AND IDH.DOC_ID = IPD.DOC_ID
           AND IDH.ORDER_NO = OH.ORDER_NO
           AND OH.IMPORT_ID IS NOT NULL ) SRC
    ON ( SRC.DOC_ID = TGT.DOC_ID AND TGT.POSTING_ID = I_POSTING_ID)
      WHEN MATCHED THEN UPDATE SET TGT.LOCATION = SRC.LOCATION;

    /* Update the importer location for documents for im_posting_doc_amounts */
    MERGE INTO IM_POSTING_DOC_AMOUNTS TGT USING
      ( SELECT IPD.DOC_ID, IDH.LOCATION,
                ( SELECT ORG_UNIT_ID
                   FROM
                    ( SELECT ORG_UNIT_ID, STORE AS LOCATION FROM STORE
                      UNION ALL
                      SELECT ORG_UNIT_ID, WH AS LOCATION FROM WH
                    ) WHERE LOCATION = OH.IMPORT_ID AND ROWNUM < 2
                ) AS ORG_UNIT,
                (SELECT MAX(LOC_SOB.SET_OF_BOOKS_ID)
                  FROM MV_LOC_SOB LOC_SOB
                  WHERE LOC_SOB.LOCATION = OH.IMPORT_ID) SET_OF_BOOKS_ID
        FROM IM_DOC_HEAD IDH, IM_POSTING_DOC IPD, ORDHEAD OH
         WHERE IPD.POSTING_ID = I_POSTING_ID

           AND IDH.DOC_ID = IPD.DOC_ID
           AND IDH.ORDER_NO = OH.ORDER_NO
           AND OH.IMPORT_ID IS NOT NULL ) SRC
    ON ( SRC.DOC_ID = TGT.DOC_ID AND TGT.POSTING_ID = I_POSTING_ID)
      WHEN MATCHED THEN UPDATE SET TGT.LOCATION = SRC.LOCATION,
                                   TGT.ORG_UNIT = SRC.ORG_UNIT,
                                   TGT.SET_OF_BOOKS_ID = SRC.SET_OF_BOOKS_ID;

  RETURN SUCCESS;


    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;
    END;  /* End update import locations */


/*******************

Calculates the Variance Calc Tolerance and Variance Calc Tax Tolerance for all types of documents.

  Parameters:
  O_error_message:  Error message output
  I_posting_id:     ID reference to the new posting process.

  Returns:
  SUCCESS/FAIL

*******************/

    FUNCTION CALC_DOCUMENT_VCT (O_error_message    OUT VARCHAR2,
                                I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                I_is_prepay     IN     VARCHAR2 DEFAULT REIM_CONSTANTS.YN_NO)
    RETURN NUMBER
    IS

        L_program  VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_DOCUMENT_VCT';

        L_posting_doc_tax_amount IM_POSTING_DOC_AMOUNTS.AMOUNT%TYPE;
        L_doc_tax_amount         IM_DOC_TAX.TAX_AMOUNT%TYPE;

        cursor C_VARIANCE is
            select inner.posting_id,
                   inner.doc_id,
                   inner.decimal_digits,
                   (inner.idh_total_cost_inc_tax - inner.ipda_total_cost_inc_tax) amount_variance
              from (select ipda.posting_id,
                           ipda.doc_id,
                           ipd.decimal_digits,
                           MAX(idh.total_cost_inc_tax) idh_total_cost_inc_tax,
                           SUM(ipda.amount) ipda_total_cost_inc_tax
                      from im_posting_doc ipd,
                           im_doc_head idh,
                           im_posting_doc_amounts ipda
                     where ipd.posting_id = I_posting_id
                       and idh.doc_id = ipd.doc_id
                       and ipda.posting_id = ipd.posting_id
                       and ipda.doc_id = ipd.doc_id
                       and ipda.amount_type = AMT_DOCUMENT_HEADER_COST
                     group by ipda.posting_id,
                              ipda.doc_id,
                              ipd.decimal_digits) inner
             where (inner.idh_total_cost_inc_tax - inner.ipda_total_cost_inc_tax) <> 0;

        cursor C_FETCH_POSTING_DOC_TAX_AMOUNT (I_posting_id IM_POSTING_DOC_AMOUNTS.POSTING_ID%TYPE,
                                               I_doc_id     IM_POSTING_DOC_AMOUNTS.DOC_ID%TYPE)is
            select sum(ipda.amount) OVER (PARTITION BY ipda.posting_id, ipda.doc_id) ipda_total_tax_amount
              from im_posting_doc_amounts ipda
             where ipda.posting_id = I_posting_id
               and ipda.doc_id = I_doc_id
               and ipda.amount_type = AMT_TAX;

        cursor C_FETCH_DOC_TAX_AMOUNT (I_doc_id IM_DOC_TAX.DOC_ID%TYPE)is
            select sum(idt.tax_amount) OVER (PARTITION BY idt.doc_id) idt_tax_amount
              from im_doc_tax idt
             where idt.doc_id = I_doc_id;
    BEGIN

        for rec IN C_variance loop

            if (I_is_prepay = REIM_CONSTANTS.YN_YES) then

               insert into im_posting_doc_amounts(posting_id,
                                                  doc_id,
                                                  amount_type,
                                                  tax_code,
                                                  tax_rate,
                                                  dept,
                                                  class,
                                                  subclass,
                                                  amount,
                                                  group_by_1,
                                                  group_by_2,
                                                  group_by_3,
                                                  group_by_4,
                                                  group_by_5,
                                                  group_by_6,
                                                  group_by_7,
                                                  group_by_8,
                                                  posting_amt_id,
                                                  set_of_books_id,
                                                  org_unit,
                                                  location,
                                                  prim_currency_code,
                                                  currency_rate_type,
                                                  currency_rate,
                                                  account_type,
                                                  account_code)
                 select ipda.posting_id,
                        ipda.doc_id,
                        AMT_PPA_COST,
                        ipda.tax_code,
                        ipda.tax_rate,
                        ipda.dept,
                        ipda.class,
                        ipda.subclass,
                        round(rec.amount_variance, rec.decimal_digits) variance_amount,
                        group_by_1,
                        group_by_2,
                        group_by_3,
                        group_by_4,
                        group_by_5,
                        group_by_6,
                        group_by_7,
                        YN_YES group_by_8,
                        IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                        ipda.set_of_books_id,
                        ipda.org_unit,
                        ipda.location,
                        ipda.prim_currency_code,
                        ipda.currency_rate_type,
                        ipda.currency_rate,
                        BASIC_TRANSACTIONS,
                        PRE_PAID_ASSET
                   from im_posting_doc_amounts ipda
                  where ipda.posting_id = rec.posting_id
                    and ipda.doc_id = rec.doc_id
                    and ipda.amount_type = AMT_PPA_COST
                    and rownum < 2;

            else

                open C_FETCH_POSTING_DOC_TAX_AMOUNT(rec.posting_id,
                                                    rec.doc_id);
                fetch C_FETCH_POSTING_DOC_TAX_AMOUNT into L_posting_doc_tax_amount;
                close C_FETCH_POSTING_DOC_TAX_AMOUNT;

                open C_FETCH_DOC_TAX_AMOUNT(rec.doc_id);
                fetch C_FETCH_DOC_TAX_AMOUNT into L_doc_tax_amount;
                close C_FETCH_DOC_TAX_AMOUNT;

                insert into im_posting_doc_amounts(posting_id,
                                                   doc_id,
                                                   amount_type,
                                                   tax_code,
                                                   tax_rate,
                                                   dept,
                                                   class,
                                                   subclass,
                                                   amount,
                                                   group_by_1,
                                                   group_by_2,
                                                   group_by_3,
                                                   group_by_4,
                                                   group_by_5,
                                                   group_by_6,
                                                   group_by_7,
                                                   group_by_8,
                                                   posting_amt_id,
                                                   set_of_books_id,
                                                   org_unit,
                                                   location,
                                                   prim_currency_code,
                                                   currency_rate_type,
                                                   currency_rate,
                                                   account_type,
                                                   account_code)
                                            select ipda.posting_id,
                                                   ipda.doc_id,
                                                   AMT_VAR_CALC_TAX_TOLERANCE,
                                                   ipda.tax_code,
                                                   ipda.tax_rate,
                                                   ipda.dept,
                                                   ipda.class,
                                                   ipda.subclass,
                                                   --Round off VCTT to the document's decimal digits
                                                   round((NVL(L_doc_tax_amount, 0) - NVL(L_posting_doc_tax_amount, 0)), rec.decimal_digits) vctt_amount,
                                                   group_by_1,
                                                   group_by_2,
                                                   group_by_3,
                                                   group_by_4,
                                                   group_by_5,
                                                   group_by_6,
                                                   group_by_7,
                                                   group_by_8,
                                                   IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                                                   ipda.set_of_books_id,
                                                   ipda.org_unit,
                                                   ipda.location,
                                                   ipda.prim_currency_code,
                                                   ipda.currency_rate_type,
                                                   ipda.currency_rate,
                                                   BASIC_TRANSACTIONS,
                                                   VARIANCE_CALC_TAX_TOLERANCE
                                              from im_posting_doc_amounts ipda
                                             where NVL(L_posting_doc_tax_amount, 0) <> 0
                                               and ipda.posting_id = rec.posting_id
                                               and ipda.doc_id = rec.doc_id
                                               and ipda.amount_type = AMT_DOCUMENT_HEADER_COST
                                               and round((NVL(L_doc_tax_amount, 0) - NVL(L_posting_doc_tax_amount, 0)), rec.decimal_digits) <> 0
                                               and rownum < 2;

                insert into im_posting_doc_amounts(posting_id,
                                                   doc_id,
                                                   amount_type,
                                                   tax_code,
                                                   tax_rate,
                                                   dept,
                                                   class,
                                                   subclass,
                                                   amount,
                                                   group_by_1,
                                                   group_by_2,
                                                   group_by_3,
                                                   group_by_4,
                                                   group_by_5,
                                                   group_by_6,
                                                   group_by_7,
                                                   group_by_8,
                                                   posting_amt_id,
                                                   set_of_books_id,
                                                   org_unit,
                                                   location,
                                                   prim_currency_code,
                                                   currency_rate_type,
                                                   currency_rate,
                                                   account_type,
                                                   account_code)
                                            select ipda.posting_id,
                                                   ipda.doc_id,
                                                   AMT_VARIANCE_CALC_TOLERANCE,
                                                   ipda.tax_code,
                                                   ipda.tax_rate,
                                                   ipda.dept,
                                                   ipda.class,
                                                   ipda.subclass,
                                                   --Use the rounded off value of VCTT's offset to compensate the loss due to precision rounding while calculating VCTT
                                                   round(rec.amount_variance - (NVL(L_doc_tax_amount, 0) - NVL(L_posting_doc_tax_amount, 0)), rec.decimal_digits) vct_amount,
                                                   group_by_1,
                                                   group_by_2,
                                                   group_by_3,
                                                   group_by_4,
                                                   group_by_5,
                                                   group_by_6,
                                                   group_by_7,
                                                   group_by_8,
                                                   IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
                                                   ipda.set_of_books_id,
                                                   ipda.org_unit,
                                                   ipda.location,
                                                   ipda.prim_currency_code,
                                                   ipda.currency_rate_type,
                                                   ipda.currency_rate,
                                                   BASIC_TRANSACTIONS,
                                                   VARIANCE_CALC_TOLERANCE
                                              from im_posting_doc_amounts ipda
                                             where ipda.posting_id = rec.posting_id
                                               and ipda.doc_id = rec.doc_id
                                               and ipda.amount_type = AMT_DOCUMENT_HEADER_COST
                                               and round(rec.amount_variance - (NVL(L_doc_tax_amount, 0) - NVL(L_posting_doc_tax_amount, 0)), rec.decimal_digits) <> 0
                                               and rownum < 2;

            end if;

            update im_posting_doc_amounts ipda
               set ipda.amount = round((ipda.amount + rec.amount_variance), rec.decimal_digits)
             where ipda.posting_id = rec.posting_id
               and ipda.doc_id = rec.doc_id
               and ipda.amount_type = AMT_DOCUMENT_HEADER_COST
               and ipda.amount <> 0
               and rownum < 2;

        end loop;

        return SUCCESS;

    EXCEPTION

        when OTHERS then
            O_error_message := O_error_message || SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_program,TO_CHAR(SQLCODE));
        return FAIL;

    END CALC_DOCUMENT_VCT;

/*******************
  THIS FUNCTION IS USED TO UPDATE/GENERATE DOCUMENT TAXES FOR THE FOLLOWING:
    1. INTRA_REGION_TAX
    2. ACQUISITION VAT
  THIS FUNCTION RE-CALCULATES THE TAX FROM VAT ITEM

  PARAMETERS:
  O_ERROR_MESSAGE : ERROR MESSAGE OUTPUT,
  I_POSTING_ID : POSTING ID OF THE PROCESS

  RETURNS:
  SUCCESS/FAIL

*******************/
  FUNCTION UPDATE_POSTING_DOC_TAX (O_ERROR_MESSAGE OUT VARCHAR2,
                                   I_POSTING_ID IN IM_POSTING_DOC.POSTING_ID%TYPE)

  RETURN NUMBER
  IS
  L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.UPDATE_POSTING_DOC_TAX';

  L_TOTAL_INVOICE_COST    IM_DOC_HEAD.TOTAL_COST%TYPE;
  L_TOTAL_TAX_BASIS       IM_DOC_TAX.TAX_BASIS%TYPE;
  L_INTRA_REGION_POST_IND IM_SYSTEM_OPTIONS.POST_INTRA_REGION_TAX_IND%TYPE;

  --FIND ALL DOC ID(S) FOR A GIVEN POSTING ID WHERE THE VAT REGION IS DIFFERENT FOR SUPPLIER AND LOCATION (HAVING DETAILS)
  CURSOR C_FETCH_INTRA_REGION_TAX_IND IS
  SELECT POST_INTRA_REGION_TAX_IND
    FROM IM_SYSTEM_OPTIONS;

  --FIND ALL DOC ID(S) FOR A GIVEN POSTING ID WHERE THE VAT REGION IS DIFFERENT FOR SUPPLIER AND LOCATION (HAVING DETAILS)
  CURSOR C_INV_WITH_DETL_INTER_VR IS
  SELECT IPD.POSTING_ID POSTING_ID,
          IDH.DOC_ID DOC_ID,
          IDT.TAX_CODE TAX_CODE,
          IDT.TAX_RATE TAX_RATE,
          IDH.TOTAL_COST,
          IPD.DECIMAL_DIGITS,
          CASE
            WHEN VR_LOC.ACQUISITION_VAT_IND = REIM_POSTING_SQL.YN_YES
             AND VR_SUP.VAT_REGION_TYPE    IN (REIM_POSTING_SQL.TAX_REGION_EU_BASE, REIM_POSTING_SQL.TAX_REGION_EU_MEMBER) THEN REIM_POSTING_SQL.YN_YES
            ELSE REIM_POSTING_SQL.YN_NO
          END ACQ_VAT_IND
  FROM IM_DOC_HEAD IDH,
          IM_POSTING_DOC IPD,
          IM_DOC_TAX IDT,
          SUPS S,
          (SELECT WH LOCATION,VAT_REGION FROM WH
           UNION ALL
           SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS,
          VAT_REGION VR_LOC,
          VAT_REGION VR_SUP
   WHERE IDH.DOC_ID = IPD.DOC_ID AND
         IDT.DOC_ID = IDH.DOC_ID AND
         IPD.DOC_TYPE = REIM_POSTING_SQL.DOC_TYPE_MERCH_INVOICE AND
         IPD.CALC_BASIS = REIM_POSTING_SQL.BASIS_DETAILS AND
         IPD.POSTING_ID = I_POSTING_ID AND
         S.VAT_REGION <> LOCS.VAT_REGION AND
         VR_LOC.VAT_REGION = LOCS.VAT_REGION AND
         VR_SUP.VAT_REGION = S.VAT_REGION AND
         NVL(IDH.SUPPLIER_SITE_ID,IDH.VENDOR) = S.SUPPLIER AND
         IPD.LOCATION = LOCS.LOCATION AND
         (L_INTRA_REGION_POST_IND = REIM_POSTING_SQL.YN_YES
          OR (    VR_LOC.ACQUISITION_VAT_IND = REIM_POSTING_SQL.YN_YES
              AND VR_SUP.VAT_REGION_TYPE    IN (REIM_POSTING_SQL.TAX_REGION_EU_BASE, REIM_POSTING_SQL.TAX_REGION_EU_MEMBER)));

  --FIND ALL DOC ID(S) FOR A GIVEN POSTING ID WHERE THE VAT REGION IS DIFFERENT FOR SUPPLIER AND LOCATION (HEADER ONLY)
   CURSOR C_INV_HEADER_ONLY IS
  SELECT IPD.POSTING_ID POSTING_ID,
          IDH.DOC_ID DOC_ID,
          IDT.TAX_CODE TAX_CODE,
          IDT.TAX_RATE TAX_RATE,
          IDH.TOTAL_COST
  FROM IM_DOC_HEAD IDH,
          IM_POSTING_DOC IPD,
          IM_DOC_TAX IDT,
          SUPS S,
          (SELECT WH LOCATION,VAT_REGION FROM WH
           UNION ALL
           SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS
   WHERE IDH.DOC_ID = IPD.DOC_ID AND
           IDT.DOC_ID = IDH.DOC_ID AND
           IPD.CALC_BASIS = REIM_POSTING_SQL.BASIS_HEADER AND
           IPD.DOC_TYPE = REIM_POSTING_SQL.DOC_TYPE_MERCH_INVOICE AND
           IPD.POSTING_ID = I_POSTING_ID AND
           S.VAT_REGION <> LOCS.VAT_REGION AND
           NVL(IDH.SUPPLIER_SITE_ID,IDH.VENDOR) = S.SUPPLIER AND
           IPD.LOCATION = LOCS.LOCATION AND
           L_INTRA_REGION_POST_IND = REIM_POSTING_SQL.YN_YES;

   --FIND ALL DOC_ID FOR OTHER DOCUMENTS WHERE VAT_REGION IS DIFFERENT FROM SUPPLIER AND LOCATION
   CURSOR C_DOC_WITH_DETL_INTER_VR IS
   SELECT IPD.POSTING_ID POSTING_ID,
          IDH.DOC_ID DOC_ID,
          IDT.TAX_CODE TAX_CODE,
          IDT.TAX_RATE TAX_RATE,
          IDH.TOTAL_COST,
          IPD.DECIMAL_DIGITS,
          CASE
            WHEN VR_LOC.ACQUISITION_VAT_IND = REIM_POSTING_SQL.YN_YES
             AND VR_SUP.VAT_REGION_TYPE    IN (REIM_POSTING_SQL.TAX_REGION_EU_BASE, REIM_POSTING_SQL.TAX_REGION_EU_MEMBER) THEN REIM_POSTING_SQL.YN_YES
            ELSE REIM_POSTING_SQL.YN_NO
          END ACQ_VAT_IND
     FROM IM_DOC_HEAD IDH,
          IM_POSTING_DOC IPD,
          IM_DOC_TAX IDT,
          SUPS S,
          (SELECT WH LOCATION,VAT_REGION FROM WH
           UNION ALL
           SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS,
          VAT_REGION VR_LOC,
          VAT_REGION VR_SUP
   WHERE IDH.DOC_ID = IPD.DOC_ID AND
         (IDH.DEAL_ID IS NULL OR IDH.DEAL_ID = 0) AND
         IDT.DOC_ID = IDH.DOC_ID AND
         IPD.CALC_BASIS = REIM_POSTING_SQL.BASIS_DETAILS AND
         IPD.DOC_TYPE NOT IN (REIM_POSTING_SQL.DOC_TYPE_MERCH_INVOICE,
                              REIM_POSTING_SQL.DOC_TYPE_NONMERCH_INVOICE) AND
         IPD.POSTING_ID = I_POSTING_ID AND
         S.VAT_REGION <> LOCS.VAT_REGION AND
         VR_LOC.VAT_REGION = LOCS.VAT_REGION AND
         VR_SUP.VAT_REGION = S.VAT_REGION AND
         NVL(IDH.SUPPLIER_SITE_ID,IDH.VENDOR) = S.SUPPLIER AND
         IPD.LOCATION = LOCS.LOCATION AND
         (L_INTRA_REGION_POST_IND = REIM_POSTING_SQL.YN_YES
          OR (    VR_LOC.ACQUISITION_VAT_IND = REIM_POSTING_SQL.YN_YES
              AND VR_SUP.VAT_REGION_TYPE    IN (REIM_POSTING_SQL.TAX_REGION_EU_BASE, REIM_POSTING_SQL.TAX_REGION_EU_MEMBER)));

   --FIND ALL DOC_ID FOR OTHER DOCUMENTS WHERE VAT_REGION IS DIFFERENT FROM SUPPLIER AND LOCATION
   CURSOR C_DEALS_INTER_VR IS
   SELECT IPD.POSTING_ID POSTING_ID,
          IDH.DOC_ID DOC_ID,
          IDT.TAX_CODE TAX_CODE,
          IDT.TAX_RATE TAX_RATE,
          IDH.TOTAL_COST,
          IPD.DECIMAL_DIGITS,
          CASE
            WHEN VR_LOC.ACQUISITION_VAT_IND = REIM_POSTING_SQL.YN_YES
             AND VR_SUP.VAT_REGION_TYPE    IN (REIM_POSTING_SQL.TAX_REGION_EU_BASE, REIM_POSTING_SQL.TAX_REGION_EU_MEMBER) THEN REIM_POSTING_SQL.YN_YES
            ELSE REIM_POSTING_SQL.YN_NO
          END ACQ_VAT_IND
     FROM IM_DOC_HEAD IDH,
          IM_POSTING_DOC IPD,
          IM_DOC_TAX IDT,
          SUPS S,
          (SELECT WH LOCATION,VAT_REGION FROM WH
           UNION ALL
           SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS,
          VAT_REGION VR_LOC,
          VAT_REGION VR_SUP
   WHERE IDH.DOC_ID = IPD.DOC_ID AND
         IDH.DEAL_TYPE = DEAL_TYPE_COMPLEX AND
         IDT.DOC_ID = IDH.DOC_ID AND
         IPD.CALC_BASIS = REIM_POSTING_SQL.BASIS_DETAILS AND
         IPD.DOC_TYPE NOT IN (REIM_POSTING_SQL.DOC_TYPE_MERCH_INVOICE,
                              REIM_POSTING_SQL.DOC_TYPE_NONMERCH_INVOICE) AND
         IPD.POSTING_ID = I_POSTING_ID AND
         S.VAT_REGION <> LOCS.VAT_REGION AND
         VR_LOC.VAT_REGION = LOCS.VAT_REGION AND
         VR_SUP.VAT_REGION = S.VAT_REGION AND
         NVL(IDH.SUPPLIER_SITE_ID,IDH.VENDOR) = S.SUPPLIER AND
         IPD.LOCATION = LOCS.LOCATION AND
         (    VR_LOC.ACQUISITION_VAT_IND = REIM_POSTING_SQL.YN_YES
          AND VR_SUP.VAT_REGION_TYPE    IN (REIM_POSTING_SQL.TAX_REGION_EU_BASE, REIM_POSTING_SQL.TAX_REGION_EU_MEMBER));

   TYPE CUR_WITH_DETAILS_TYPE IS TABLE OF C_INV_WITH_DETL_INTER_VR%ROWTYPE;
   TYPE CUR_HEADER_ONLY_TYPE IS TABLE OF C_INV_HEADER_ONLY%ROWTYPE;
   TYPE CUR_DOCUMENT_TYPE IS TABLE OF C_DOC_WITH_DETL_INTER_VR%ROWTYPE;
   TYPE CUR_DEAL_TYPE IS TABLE OF C_DEALS_INTER_VR%ROWTYPE;


   REC_WITH_DETAILS CUR_WITH_DETAILS_TYPE;
   REC_HEADER_ONLY CUR_HEADER_ONLY_TYPE;
   REC_DOCUMENT CUR_DOCUMENT_TYPE;
   REC_DEAL CUR_DEAL_TYPE;

   BEGIN

   OPEN C_FETCH_INTRA_REGION_TAX_IND;
   FETCH C_FETCH_INTRA_REGION_TAX_IND INTO L_INTRA_REGION_POST_IND;
   CLOSE C_FETCH_INTRA_REGION_TAX_IND;

   OPEN C_INV_WITH_DETL_INTER_VR;
   LOOP
        FETCH C_INV_WITH_DETL_INTER_VR BULK COLLECT INTO REC_WITH_DETAILS LIMIT 10000;
        FOR INDX IN 1..REC_WITH_DETAILS.COUNT
        LOOP
            INSERT INTO IM_INTRA_TAX_TMP(POSTING_ID,DOC_ID,TAX_CODE,TAX_RATE,TOTAL_COST)
            VALUES (REC_WITH_DETAILS(INDX).POSTING_ID,
            REC_WITH_DETAILS(INDX).DOC_ID,
            REC_WITH_DETAILS(INDX).TAX_CODE,
            REC_WITH_DETAILS(INDX).TAX_RATE,
            REC_WITH_DETAILS(INDX).TOTAL_COST);

            DELETE FROM IM_INVOICE_DETAIL_TAX
            WHERE DOC_ID = REC_WITH_DETAILS(INDX).DOC_ID;

            DELETE FROM IM_DOC_TAX
            WHERE DOC_ID = REC_WITH_DETAILS(INDX).DOC_ID;

            INSERT INTO IM_INVOICE_DETAIL_TAX (DOC_ID,
                                               ITEM,
                                               TAX_CODE,
                                               TAX_RATE,
                                               TAX_BASIS,
                                               TAX_AMOUNT,
                                               TAX_FORMULA,
                                               TAX_ORDER,
                                               TAX_FORMULA_TYPE)
            SELECT IDH.DOC_ID DOC_ID,
                    IID.ITEM ITEM,
                    VI.VAT_CODE TAX_CODE,
                    VI.VAT_RATE TAX_RATE,
                    SUM(UNIT_COST * QTY) TAX_BASIS,
                    ROUND(SUM(UNIT_COST * QTY) * DECODE(REC_WITH_DETAILS(INDX).ACQ_VAT_IND,
                                                         REIM_POSTING_SQL.YN_YES,(VI.VAT_RATE/100),
                                                         0), REC_WITH_DETAILS(INDX).DECIMAL_DIGITS) TAX_AMOUNT,
                    'CO' TAX_FORMULA,
                    1 TAX_ORDER,
                    'CO' TAX_FORMULA_TYPE
            FROM IM_DOC_HEAD IDH,
                    IM_INVOICE_DETAIL IID,
                    VAT_ITEM VI,
                    (SELECT WH LOCATION,VAT_REGION FROM WH
                     UNION ALL
                    SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS
            WHERE IDH.DOC_ID = REC_WITH_DETAILS(INDX).DOC_ID AND
                    IDH.DOC_ID = IID.DOC_ID AND
                    VI.ITEM = IID.ITEM AND
                    VI.VAT_REGION = LOCS.VAT_REGION AND
                    LOCS.LOCATION = IDH.LOCATION AND
                    VI.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH) AND
                    VI.ACTIVE_DATE = (SELECT MAX(ACTIVE_DATE)
                                        FROM VAT_ITEM V
                                       WHERE V.ITEM = VI.ITEM
                                         AND V.VAT_REGION = VI.VAT_REGION
                                         AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                         AND V.ACTIVE_DATE <= IDH.DOC_DATE)
                    GROUP BY IDH.DOC_ID,IID.ITEM,VI.VAT_CODE,VI.VAT_RATE;


            INSERT INTO IM_DOC_TAX (DOC_ID,TAX_CODE,TAX_RATE ,TAX_BASIS ,TAX_AMOUNT)
            SELECT DOC_ID,
                   TAX_CODE,
                   TAX_RATE,
                   SUM(TAX_BASIS),
                   SUM(TAX_AMOUNT)
              FROM (SELECT IDH.DOC_ID DOC_ID,
                            IIDT.TAX_CODE TAX_CODE,
                            IIDT.TAX_RATE TAX_RATE,
                            SUM(IIDT.TAX_BASIS) TAX_BASIS,
                            ROUND(SUM(IIDT.TAX_BASIS) * DECODE(REC_WITH_DETAILS(INDX).ACQ_VAT_IND,
                                                               REIM_POSTING_SQL.YN_YES,(IIDT.TAX_RATE/100),
                                                               0), REC_WITH_DETAILS(INDX).DECIMAL_DIGITS) TAX_AMOUNT
                    FROM IM_DOC_HEAD IDH,
                         IM_INVOICE_DETAIL_TAX IIDT
                    WHERE IDH.DOC_ID = REC_WITH_DETAILS(INDX).DOC_ID AND
                          IDH.DOC_ID = IIDT.DOC_ID
                    GROUP BY IDH.DOC_ID,IIDT.TAX_CODE,IIDT.TAX_RATE
                    UNION ALL
                    SELECT IDH.DOC_ID DOC_ID,
                            IDNMT.TAX_CODE TAX_CODE,
                            IDNMT.TAX_RATE TAX_RATE,
                            SUM(IDNMT.TAX_BASIS) TAX_BASIS,
                            0 TAX_AMOUNT
                    FROM IM_DOC_HEAD IDH,
                         IM_DOC_NON_MERCH_TAX IDNMT
                   WHERE IDH.DOC_ID = REC_WITH_DETAILS(INDX).DOC_ID AND
                         IDH.DOC_ID = IDNMT.DOC_ID
                   GROUP BY IDH.DOC_ID,IDNMT.TAX_CODE,IDNMT.TAX_RATE)
             GROUP BY DOC_ID,
                      TAX_CODE,
                      TAX_RATE;

           UPDATE IM_POSTING_DOC IPD
              SET IPD.SYS_TAX_IND = DECODE(REC_WITH_DETAILS(INDX).ACQ_VAT_IND,
                                                    REIM_POSTING_SQL.YN_YES, REIM_POSTING_SQL.SYS_TAX_IND_ACQ_VAT,
                                                    REIM_POSTING_SQL.SYS_TAX_IND_INTRA_VAT_REG)
            WHERE IPD.POSTING_ID = I_POSTING_ID
              AND IPD.DOC_ID     = REC_WITH_DETAILS(INDX).DOC_ID;

        END LOOP;

    EXIT WHEN C_INV_WITH_DETL_INTER_VR%NOTFOUND;

  END LOOP;

  OPEN C_INV_HEADER_ONLY;
  LOOP

    FETCH C_INV_HEADER_ONLY BULK COLLECT INTO REC_HEADER_ONLY LIMIT 10000;
    FOR INDX IN 1..REC_HEADER_ONLY.COUNT
    LOOP
        INSERT INTO IM_INTRA_TAX_TMP(POSTING_ID,DOC_ID,TAX_CODE,TAX_RATE,TOTAL_COST)
            VALUES (REC_HEADER_ONLY(INDX).POSTING_ID,
            REC_HEADER_ONLY(INDX).DOC_ID,
            REC_HEADER_ONLY(INDX).TAX_CODE,
            REC_HEADER_ONLY(INDX).TAX_RATE,
            REC_HEADER_ONLY(INDX).TOTAL_COST);

        DELETE FROM IM_DOC_TAX
        WHERE DOC_ID = REC_HEADER_ONLY(INDX).DOC_ID;

        INSERT INTO IM_DOC_TAX (DOC_ID,TAX_CODE,TAX_RATE ,TAX_BASIS ,TAX_AMOUNT)
                SELECT IDH.DOC_ID DOC_ID,
                IIDT.TAX_CODE TAX_CODE,
                IIDT.TAX_RATE TAX_RATE,
                SUM(IIDT.TAX_BASIS) TAX_BASIS,
                0 TAX_AMOUNT
                FROM
                IM_DOC_HEAD IDH,
                IM_DOC_NON_MERCH_TAX IIDT
                WHERE IDH.DOC_ID = REC_HEADER_ONLY(INDX).DOC_ID AND
                IDH.DOC_ID = IIDT.DOC_ID
                GROUP BY IDH.DOC_ID,IIDT.TAX_CODE,IIDT.TAX_RATE
                UNION
        SELECT DOC_ID,
        TAX_CODE,
        TAX_RATE,
        ROUND((TOTAL_ITEM_COST/TOTAL_RECEIPT_COST)*TOTAL_COST,DECIMAL_DIGITS) TAX_BASIS,
    0
        FROM (
        SELECT IDH.DOC_ID DOC_ID,
        IDH.TOTAL_COST TOTAL_COST,
        IPD.DECIMAL_DIGITS DECIMAL_DIGITS,
        VI.VAT_CODE TAX_CODE,
        VI.VAT_RATE TAX_RATE,
        SUM(SS.UNIT_COST * IRIP.QTY_MATCHED) OVER (PARTITION BY IDH.DOC_ID,VI.VAT_CODE) TOTAL_ITEM_COST,
        SUM(SS.UNIT_COST * IRIP.QTY_MATCHED) OVER (PARTITION BY IDH.DOC_ID) TOTAL_RECEIPT_COST,
        0 TAX_AMOUNT
        FROM IM_DOC_HEAD IDH,
        IM_POSTING_DOC IPD,
        VAT_ITEM VI,
        IM_RECEIPT_ITEM_POSTING IRIP,
        IM_RCPT_ITEM_POSTING_INVOICE IRIPI,
        V_IM_SHIPSKU SS,
        (SELECT WH LOCATION,VAT_REGION FROM WH
        UNION ALL
        SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS
        WHERE IDH.DOC_ID = IRIPI.DOC_ID AND
        IPD.DOC_ID = IDH.DOC_ID AND
        IDH.DOC_ID = REC_HEADER_ONLY(INDX).DOC_ID AND
        IRIP.ITEM = SS.ITEM AND
        IRIP.SEQ_NO = IRIPI.SEQ_NO AND
        SS.SHIPMENT = IRIP.SHIPMENT AND
        VI.ITEM = SS.ITEM AND
        VI.VAT_REGION = LOCS.VAT_REGION AND
        LOCS.LOCATION = IDH.LOCATION AND
        VI.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH) AND
        VI.ACTIVE_DATE = (SELECT MAX(ACTIVE_DATE) FROM VAT_ITEM V
        WHERE V.ITEM = VI.ITEM
        AND V.VAT_REGION = VI.VAT_REGION
        AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
        AND V.ACTIVE_DATE <= IDH.DOC_DATE)
        GROUP BY IDH.DOC_ID,VI.VAT_CODE,VI.VAT_RATE,IDH.TOTAL_COST,IPD.DECIMAL_DIGITS,SS.UNIT_COST,IRIP.QTY_MATCHED);

    --ADJUSTING THE VARIANCE TO AVOID ROUNDING ISSUES.
        SELECT IDH.TOTAL_COST,
        SUM(IDT.TAX_BASIS) TAX_BASIS
        INTO L_TOTAL_INVOICE_COST,
         L_TOTAL_TAX_BASIS
        FROM IM_DOC_HEAD IDH,
        IM_DOC_TAX IDT
        WHERE IDH.DOC_ID = IDT.DOC_ID AND
        IDH.DOC_ID = REC_HEADER_ONLY(INDX).DOC_ID
        GROUP BY IDH.TOTAL_COST;

        IF (L_TOTAL_INVOICE_COST - L_TOTAL_TAX_BASIS) <> 0 THEN
            UPDATE IM_DOC_TAX SET TAX_BASIS = TAX_BASIS + (L_TOTAL_INVOICE_COST - L_TOTAL_TAX_BASIS)
            WHERE DOC_ID = REC_HEADER_ONLY(INDX).DOC_ID AND
            ROWNUM < 2;
        END IF;

       UPDATE IM_POSTING_DOC IPD
          SET IPD.SYS_TAX_IND = REIM_POSTING_SQL.SYS_TAX_IND_INTRA_VAT_REG
            WHERE IPD.POSTING_ID = I_POSTING_ID
              AND IPD.DOC_ID     = REC_HEADER_ONLY(INDX).DOC_ID;

    END LOOP;

    EXIT WHEN C_INV_HEADER_ONLY%NOTFOUND;

  END LOOP;

  OPEN C_DOC_WITH_DETL_INTER_VR;
  LOOP
  FETCH C_DOC_WITH_DETL_INTER_VR BULK COLLECT INTO REC_DOCUMENT LIMIT 10000;
  FOR INDX IN 1..REC_DOCUMENT.COUNT
  LOOP
      INSERT INTO IM_INTRA_TAX_TMP(POSTING_ID,DOC_ID,TAX_CODE,TAX_RATE,TOTAL_COST)
            VALUES
      (REC_DOCUMENT(INDX).POSTING_ID,
            REC_DOCUMENT(INDX).DOC_ID,
            REC_DOCUMENT(INDX).TAX_CODE,
            REC_DOCUMENT(INDX).TAX_RATE,
            REC_DOCUMENT(INDX).TOTAL_COST);

      DELETE FROM IM_DOC_DETAIL_RC_TAX
      WHERE IM_DOC_DETAIL_REASON_CODES_ID IN
      (SELECT IM_DOC_DETAIL_REASON_CODES_ID FROM IM_DOC_DETAIL_REASON_CODES
      WHERE DOC_ID = REC_DOCUMENT(INDX).DOC_ID);

      DELETE FROM IM_DOC_TAX
      WHERE doc_id = REC_DOCUMENT(INDX).DOC_ID;

      INSERT INTO IM_DOC_DETAIL_RC_TAX (IM_DOC_DETAIL_REASON_CODES_ID,
                                        TAX_CODE,TAX_RATE,TAX_BASIS,TAX_AMOUNT,
                                        TAX_FORMULA,TAX_ORDER,TAX_FORMULA_TYPE)
      SELECT IDDRC.IM_DOC_DETAIL_REASON_CODES_ID IM_DOC_DETAIL_REASON_CODES_ID,
            VI.VAT_CODE TAX_CODE,
            VI.VAT_RATE TAX_RATE,
            SUM(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY) TAX_BASIS,
            ROUND(SUM(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY) * DECODE(REC_DOCUMENT(INDX).ACQ_VAT_IND,
                                                                      REIM_POSTING_SQL.YN_YES,(VI.VAT_RATE/100),
                                                                      0), REC_DOCUMENT(INDX).DECIMAL_DIGITS) TAX_AMOUNT,
            'CO' TAX_FORMULA,
            1 TAX_ORDER,
            'CO' TAX_FORMULA_TYPE
            FROM IM_DOC_HEAD IDH,
            IM_DOC_DETAIL_REASON_CODES IDDRC,
            VAT_ITEM VI,
            (SELECT WH LOCATION,VAT_REGION FROM WH
            UNION ALL
            SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS
            WHERE IDH.DOC_ID = REC_DOCUMENT(INDX).DOC_ID AND
                    IDH.DOC_ID = IDDRC.DOC_ID AND
                    VI.ITEM = IDDRC.ITEM AND
                    VI.VAT_REGION = LOCS.VAT_REGION AND
                    LOCS.LOCATION = IDH.LOCATION AND
                    VI.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH) AND
                    VI.ACTIVE_DATE = (SELECT MAX(ACTIVE_DATE) FROM VAT_ITEM V
                                        WHERE V.ITEM = VI.ITEM
                                        AND V.VAT_REGION = VI.VAT_REGION
                                        AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                        AND V.ACTIVE_DATE <= IDH.DOC_DATE)
            GROUP BY IDDRC.IM_DOC_DETAIL_REASON_CODES_ID,VI.VAT_CODE,VI.VAT_RATE;


      INSERT INTO IM_DOC_TAX (DOC_ID,TAX_CODE,TAX_RATE ,TAX_BASIS ,TAX_AMOUNT)
      SELECT DOC_ID,
             TAX_CODE,
             TAX_RATE,
             SUM(TAX_BASIS),
             SUM(TAX_AMOUNT)
      FROM (SELECT IDH.DOC_ID DOC_ID,
            IDDRT.TAX_CODE TAX_CODE,
            IDDRT.TAX_RATE TAX_RATE,
            SUM(IDDRT.TAX_BASIS) TAX_BASIS,
            ROUND(SUM(IDDRT.TAX_BASIS) * DECODE(REC_DOCUMENT(INDX).ACQ_VAT_IND,
                                                REIM_POSTING_SQL.YN_YES,(IDDRT.TAX_RATE/100),
                                                0), REC_DOCUMENT(INDX).DECIMAL_DIGITS) TAX_AMOUNT
            FROM IM_DOC_HEAD IDH,
                 IM_DOC_DETAIL_RC_TAX IDDRT,
                 IM_DOC_DETAIL_REASON_CODES IDDRC
           WHERE IDH.DOC_ID = REC_DOCUMENT(INDX).DOC_ID AND
                 IDH.DOC_ID = IDDRC.DOC_ID AND
                 IDDRT.IM_DOC_DETAIL_REASON_CODES_ID = IDDRC.IM_DOC_DETAIL_REASON_CODES_ID
        GROUP BY IDH.DOC_ID,IDDRT.TAX_CODE,IDDRT.TAX_RATE
          UNION
          SELECT IDH.DOC_ID DOC_ID,
                 IDNMT.TAX_CODE TAX_CODE,
                 IDNMT.TAX_RATE TAX_RATE,
                 SUM(IDNMT.TAX_BASIS) TAX_BASIS,
                 0 TAX_AMOUNT
          FROM IM_DOC_HEAD IDH,
               IM_DOC_NON_MERCH_TAX IDNMT
          WHERE IDH.DOC_ID = REC_DOCUMENT(INDX).DOC_ID AND
                IDH.DOC_ID = IDNMT.DOC_ID
          GROUP BY IDH.DOC_ID,IDNMT.TAX_CODE,IDNMT.TAX_RATE)
      GROUP BY DOC_ID,
               TAX_CODE,
               TAX_RATE;

      UPDATE IM_POSTING_DOC IPD
        SET IPD.SYS_TAX_IND = DECODE(REC_DOCUMENT(INDX).ACQ_VAT_IND,
                                                  REIM_POSTING_SQL.YN_YES, REIM_POSTING_SQL.SYS_TAX_IND_ACQ_VAT,
                                                  REIM_POSTING_SQL.SYS_TAX_IND_INTRA_VAT_REG)
      WHERE IPD.POSTING_ID = I_POSTING_ID
        AND IPD.DOC_ID     = REC_DOCUMENT(INDX).DOC_ID;

   END LOOP;

   EXIT WHEN C_DOC_WITH_DETL_INTER_VR%NOTFOUND;

  END LOOP;

-- Complex deals Acquisition tax updates
  OPEN C_DEALS_INTER_VR;
  LOOP
  FETCH C_DEALS_INTER_VR BULK COLLECT INTO REC_DEAL LIMIT 10000;
  FOR INDX IN 1..REC_DEAL.COUNT
  LOOP
      INSERT INTO IM_INTRA_TAX_TMP(POSTING_ID,DOC_ID,TAX_CODE,TAX_RATE,TOTAL_COST)
            VALUES
      (REC_DEAL(INDX).POSTING_ID,
            REC_DEAL(INDX).DOC_ID,
            REC_DEAL(INDX).TAX_CODE,
            REC_DEAL(INDX).TAX_RATE,
            REC_DEAL(INDX).TOTAL_COST);

      DELETE FROM IM_DOC_TAX
      WHERE DOC_ID = REC_DEAL(INDX).DOC_ID;

      DELETE FROM IM_DOC_DETAIL_RC_TAX
      WHERE IM_DOC_DETAIL_REASON_CODES_ID
         IN (SELECT IM_DOC_DETAIL_REASON_CODES_ID
               FROM IM_DOC_DETAIL_REASON_CODES
              WHERE DOC_ID = REC_DEAL(INDX).DOC_ID);

      DELETE FROM IM_COMPLEX_DEAL_DETAIL_TAX
       WHERE DOC_ID = REC_DEAL(INDX).DOC_ID;

      INSERT INTO IM_DOC_DETAIL_RC_TAX (IM_DOC_DETAIL_REASON_CODES_ID,
                                        TAX_CODE,TAX_RATE,TAX_BASIS,TAX_AMOUNT,
                                        TAX_FORMULA,TAX_ORDER,TAX_FORMULA_TYPE)
      SELECT IDDRC.IM_DOC_DETAIL_REASON_CODES_ID IM_DOC_DETAIL_REASON_CODES_ID,
            VI.VAT_CODE TAX_CODE,
            VI.VAT_RATE TAX_RATE,
            SUM(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY) TAX_BASIS,
            ROUND(SUM(IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY) * DECODE(REC_DEAL(INDX).ACQ_VAT_IND,
                                                                      REIM_POSTING_SQL.YN_YES,(VI.VAT_RATE/100),
                                                                      0), REC_DEAL(INDX).DECIMAL_DIGITS) TAX_AMOUNT,
            'CO' TAX_FORMULA,
            1 TAX_ORDER,
            'CO' TAX_FORMULA_TYPE
            FROM IM_DOC_HEAD IDH,
            IM_DOC_DETAIL_REASON_CODES IDDRC,
            VAT_ITEM VI,
            (SELECT WH LOCATION,VAT_REGION FROM WH
            UNION ALL
            SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS
            WHERE IDH.DOC_ID = REC_DEAL(INDX).DOC_ID AND
                    IDH.DOC_ID = IDDRC.DOC_ID AND
                    VI.ITEM = IDDRC.ITEM AND
                    VI.VAT_REGION = LOCS.VAT_REGION AND
                    LOCS.LOCATION = IDH.LOCATION AND
                    VI.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH) AND
                    VI.ACTIVE_DATE = (SELECT MAX(ACTIVE_DATE) FROM VAT_ITEM V
                                        WHERE V.ITEM = VI.ITEM
                                        AND V.VAT_REGION = VI.VAT_REGION
                                        AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                        AND V.ACTIVE_DATE <= IDH.DOC_DATE)
            GROUP BY IDDRC.IM_DOC_DETAIL_REASON_CODES_ID,VI.VAT_CODE,VI.VAT_RATE;

      INSERT INTO IM_COMPLEX_DEAL_DETAIL_TAX (DOC_ID,
                                              SEQ_NO,
                                              ITEM,
                                              TAX_CODE,
                                              TAX_RATE,
                                              TAX_BASIS,
                                              TAX_AMOUNT)
        SELECT ICDD.DOC_ID,
               ICDD.SEQ_NO,
               ICDD.ITEM,
               VI.VAT_CODE TAX_CODE,
               VI.VAT_RATE TAX_RATE,
               ICDD.INCOME_DEAL_CURR TAX_BASIS,
               ROUND(ICDD.INCOME_DEAL_CURR * DECODE(REC_DEAL(INDX).ACQ_VAT_IND,
                                                    REIM_POSTING_SQL.YN_YES,(VI.VAT_RATE/100),
                                                    0), REC_DEAL(INDX).DECIMAL_DIGITS) TAX_AMOUNT
         FROM IM_DOC_HEAD IDH,
              IM_COMPLEX_DEAL_DETAIL ICDD,
              VAT_ITEM VI,
              (SELECT WH LOCATION,VAT_REGION FROM WH
               UNION ALL
               SELECT STORE LOCATION,VAT_REGION FROM STORE) LOCS
       WHERE IDH.DOC_ID = REC_DEAL(INDX).DOC_ID AND
             IDH.DOC_ID = ICDD.DOC_ID AND
             VI.ITEM = ICDD.ITEM AND
             VI.VAT_REGION = LOCS.VAT_REGION AND
             LOCS.LOCATION = IDH.LOCATION AND
             VI.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH) AND
             VI.ACTIVE_DATE = (SELECT MAX(ACTIVE_DATE) FROM VAT_ITEM V
                                WHERE V.ITEM = VI.ITEM
                                  AND V.VAT_REGION = VI.VAT_REGION
                                  AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                  AND V.ACTIVE_DATE <= IDH.DOC_DATE);


      INSERT INTO IM_DOC_TAX (DOC_ID,
                              TAX_CODE,
                              TAX_RATE,
                              TAX_BASIS,
                              TAX_AMOUNT)
      SELECT ICDDT.DOC_ID,
             ICDDT.TAX_CODE,
             ICDDT.TAX_RATE,
             SUM(ICDDT.TAX_BASIS),
             SUM(ICDDT.TAX_AMOUNT)
        FROM IM_DOC_HEAD IDH,
             IM_COMPLEX_DEAL_DETAIL_TAX ICDDT
       WHERE IDH.DOC_ID = REC_DEAL(INDX).DOC_ID
         AND IDH.DOC_ID = ICDDT.DOC_ID
       GROUP BY ICDDT.DOC_ID,
                ICDDT.TAX_CODE,
                ICDDT.TAX_RATE;

      UPDATE IM_POSTING_DOC IPD
        SET IPD.SYS_TAX_IND = DECODE(REC_DEAL(INDX).ACQ_VAT_IND,
                                     REIM_POSTING_SQL.YN_YES, REIM_POSTING_SQL.SYS_TAX_IND_ACQ_VAT,
                                     REIM_POSTING_SQL.SYS_TAX_IND_INTRA_VAT_REG)
      WHERE IPD.POSTING_ID = I_POSTING_ID
        AND IPD.DOC_ID     = REC_DEAL(INDX).DOC_ID;

   END LOOP;

   EXIT WHEN C_DEALS_INTER_VR%NOTFOUND;

  END LOOP;


  RETURN SUCCESS;

   EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;

END UPDATE_POSTING_DOC_TAX;


/*******************
  THIS FUNCTION IS USED TO LOAD THE TAX COMPONENT BACK TO IM_DOC_TAX AND IM_INVOICE_DETAIL_TAX AFTER INTRA POSTING CALCULATION.

  PARAMETERS:
  O_ERROR_MESSAGE : ERROR MESSAGE OUTPUT,
  I_POSTING_ID : POSTING ID OF THE PROCESS

  RETURNS:
  SUCCESS/FAIL

*******************/

FUNCTION LOAD_ACTUAL_TAX (O_ERROR_MESSAGE OUT VARCHAR2,
  I_POSTING_ID IN IM_POSTING_DOC.POSTING_ID%TYPE)

  RETURN NUMBER
  IS
  L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.LOAD_ACTUAL_TAX';

  BEGIN

  DELETE FROM IM_DOC_TAX WHERE DOC_ID IN (
  SELECT DOC_ID FROM IM_INTRA_TAX_TMP WHERE POSTING_ID = I_POSTING_ID);

    INSERT INTO IM_DOC_TAX (DOC_ID,TAX_CODE,TAX_RATE,TAX_BASIS,TAX_AMOUNT)
    SELECT IITT.DOC_ID,
    IITT.TAX_CODE,
    IITT.TAX_RATE,
    IITT.TOTAL_COST,
    0
    FROM IM_INTRA_TAX_TMP IITT,
  IM_DOC_HEAD IDH
    WHERE IITT.POSTING_ID = I_POSTING_ID AND
  IDH.DOC_ID = IITT.DOC_ID;

  DELETE FROM IM_INVOICE_DETAIL_TAX WHERE DOC_ID IN (
  SELECT DOC_ID FROM IM_INTRA_TAX_TMP WHERE POSTING_ID = I_POSTING_ID);

    INSERT INTO IM_INVOICE_DETAIL_TAX(DOC_ID ,ITEM ,  TAX_CODE ,  TAX_RATE ,  TAX_BASIS ,  TAX_AMOUNT ,  TAX_FORMULA  ,  TAX_ORDER ,  TAX_FORMULA_TYPE)
    SELECT IITT.DOC_ID,
    IID.ITEM,
    IITT.TAX_CODE,
    IITT.TAX_RATE,
    IID.UNIT_COST * IID.QTY,
    0,
    'CO',
    1,
    'CO'
    FROM IM_INTRA_TAX_TMP IITT,
    IM_INVOICE_DETAIL IID
    WHERE IITT.DOC_ID = IID.DOC_ID  AND
    IITT.POSTING_ID = I_POSTING_ID;

  DELETE FROM IM_DOC_DETAIL_RC_TAX
  WHERE IM_DOC_DETAIL_REASON_CODES_ID IN (
  SELECT IM_DOC_DETAIL_REASON_CODES_ID FROM
  IM_DOC_DETAIL_REASON_CODES IDDRC,
  IM_INTRA_TAX_TMP IITT
  WHERE IDDRC.DOC_ID = IITT.DOC_ID AND
  IITT.POSTING_ID = I_POSTING_ID);

  INSERT INTO IM_DOC_DETAIL_RC_TAX  (IM_DOC_DETAIL_REASON_CODES_ID,TAX_CODE,TAX_RATE,TAX_BASIS,TAX_AMOUNT,
  TAX_FORMULA,TAX_ORDER,TAX_FORMULA_TYPE)
  SELECT IDDRC.IM_DOC_DETAIL_REASON_CODES_ID,
  IITT.TAX_CODE,
  IITT.TAX_RATE,
  IDDRC.ORIG_UNIT_COST * IDDRC.ORIG_QTY,
  0,
  'CO',
  1,
  'CO'
  FROM
  IM_DOC_DETAIL_REASON_CODES IDDRC,
  IM_INTRA_TAX_TMP IITT
  WHERE IDDRC.DOC_ID = IITT.DOC_ID AND
  IITT.POSTING_ID = I_POSTING_ID;

  DELETE FROM IM_COMPLEX_DEAL_DETAIL_TAX
  WHERE DOC_ID IN (SELECT DOC_ID
                     FROM IM_INTRA_TAX_TMP
                    WHERE POSTING_ID = I_POSTING_ID);

  INSERT INTO IM_COMPLEX_DEAL_DETAIL_TAX (DOC_ID,
                                          SEQ_NO,
                                          ITEM,
                                          TAX_CODE,
                                          TAX_RATE,
                                          TAX_BASIS,
                                          TAX_AMOUNT)
        SELECT ICDD.DOC_ID,
               ICDD.SEQ_NO,
               ICDD.ITEM,
               IITT.TAX_CODE,
               IITT.TAX_RATE,
               ICDD.INCOME_DEAL_CURR TAX_BASIS,
               0 TAX_AMOUNT
         FROM IM_DOC_HEAD IDH,
              IM_COMPLEX_DEAL_DETAIL ICDD,
              IM_INTRA_TAX_TMP IITT
       WHERE IITT.POSTING_ID = I_POSTING_ID
         AND IDH.DOC_ID = IITT.DOC_ID
         AND IDH.DOC_ID = ICDD.DOC_ID;

  DELETE FROM IM_INTRA_TAX_TMP;

  RETURN SUCCESS;

   EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;

END LOAD_ACTUAL_TAX;

/*******************
  THIS FUNCTION IS USED TO LOG SYSTEM GENERATED TAXES TO IM_DOC_SYS_POSTING.

  PARAMETERS:
  O_ERROR_MESSAGE : ERROR MESSAGE OUTPUT,
  I_POSTING_ID : POSTING ID OF THE PROCESS

  RETURNS:
  SUCCESS/FAIL

*******************/

FUNCTION POST_SYSTEM_GEN_TAX(O_ERROR_MESSAGE OUT VARCHAR2,
                             I_POSTING_ID IN IM_POSTING_DOC.POSTING_ID%TYPE)

  RETURN NUMBER
  IS
  L_PROGRAM  VARCHAR2(50) := 'REIM_POSTING_SQL.POST_SYSTEM_GEN_TAX';

  BEGIN

    INSERT INTO IM_DOC_SYS_TAX_POSTING (DOC_ID,
                                        TAX_BASIS,
                                        TAX_CODE,
                                        TAX_RATE,
                                        DEPT,
                                        CLASS,
                                        AMOUNT,
                                        TAX_TYPE)
    SELECT DOC_ID,
           SUM(TAX_BASIS),
           TAX_CODE,
           TAX_RATE,
           DEPT,
           CLASS,
           SUM(AMOUNT),
           TAX_TYPE
      FROM (--Acquisition tax for invoices
            SELECT IPD.DOC_ID DOC_ID,
                   IIDT.TAX_BASIS TAX_BASIS,
                   VI.VAT_CODE TAX_CODE,
                   VI.VAT_RATE TAX_RATE,
                   IM.DEPT DEPT,
                   IM.CLASS CLASS,
                   ROUND(IIDT.TAX_BASIS * (VI.VAT_RATE/100), IPD.DECIMAL_DIGITS) AMOUNT,
                   AMT_ACQ_VAT TAX_TYPE
             FROM  IM_POSTING_DOC IPD,
                   IM_INVOICE_DETAIL_TAX IIDT,
                   ITEM_MASTER IM,
                   (SELECT WH LOCATION,VAT_REGION, 'W' LOC_TYPE FROM WH
                    UNION ALL
                    SELECT STORE LOCATION,VAT_REGION, 'S' LOC_TYPE FROM STORE) LOCS,
                   VAT_ITEM VI
            WHERE IPD.POSTING_ID           = I_POSTING_ID
              AND IPD.DOC_TYPE             = DOC_TYPE_MERCH_INVOICE
              AND IPD.CALC_BASIS           = BASIS_DETAILS
              AND IPD.HAS_ERRORS           = YN_NO
              AND NVL(IPD.SYS_TAX_IND,'N') = SYS_TAX_IND_ACQ_VAT
              AND IIDT.DOC_ID              = IPD.DOC_ID
              AND IM.ITEM                  = IIDT.ITEM
              AND (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)
              AND LOCS.LOCATION            = IPD.LOCATION
              AND LOCS.LOC_TYPE            = IPD.LOC_TYPE
              AND VI.VAT_REGION            = LOCS.VAT_REGION
              AND VI.ITEM                  = IIDT.ITEM
              AND VI.VAT_TYPE              IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
              AND VI.ACTIVE_DATE    = (SELECT MAX(ACTIVE_DATE)
                                         FROM VAT_ITEM V
                                        WHERE V.ITEM = VI.ITEM
                                          AND V.VAT_REGION = VI.VAT_REGION
                                          AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                          AND V.ACTIVE_DATE <= IPD.DOC_DATE)
           UNION ALL
           --reverse charge vat component of invoices
           SELECT IPD.DOC_ID DOC_ID,
                  IIDT.TAX_BASIS TAX_BASIS,
                  VI.VAT_CODE TAX_CODE,
                  VI.VAT_RATE TAX_RATE,
                  IM.DEPT DEPT,
                  IM.CLASS CLASS,
                  ROUND(IIDT.TAX_BASIS * (VI.VAT_RATE/100), IPD.DECIMAL_DIGITS) AMOUNT,
                  AMT_REV_CHRG_VAT TAX_TYPE
            FROM  IM_POSTING_DOC IPD,
                  IM_INVOICE_DETAIL_TAX IIDT,
                  ITEM_MASTER IM,
                  (SELECT WH LOCATION,VAT_REGION, 'W' LOC_TYPE FROM WH
                   UNION ALL
                   SELECT STORE LOCATION,VAT_REGION, 'S' LOC_TYPE FROM STORE) LOCS,
                  VAT_ITEM VI
            WHERE IPD.POSTING_ID     = I_POSTING_ID
              AND IPD.DOC_TYPE       = DOC_TYPE_MERCH_INVOICE
              AND IPD.SYS_TAX_IND    = REIM_POSTING_SQL.SYS_TAX_IND_REV_CHRG_VAT
              AND IPD.CALC_BASIS     = BASIS_DETAILS
              AND IPD.HAS_ERRORS     = YN_NO
              AND (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)
              AND IIDT.DOC_ID        = IPD.DOC_ID
              AND IM.ITEM            = IIDT.ITEM
              AND LOCS.LOCATION      = IPD.LOCATION
              AND LOCS.LOC_TYPE      = IPD.LOC_TYPE
              AND VI.ITEM            = IIDT.ITEM
              AND VI.REVERSE_VAT_IND = YN_YES
              AND VI.VAT_REGION      = LOCS.VAT_REGION
              AND VI.VAT_TYPE        IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
              AND VI.ACTIVE_DATE    = (SELECT MAX(ACTIVE_DATE)
                                         FROM VAT_ITEM V
                                        WHERE V.ITEM = VI.ITEM
                                          AND V.VAT_REGION = VI.VAT_REGION
                                          AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                          AND V.ACTIVE_DATE <= IPD.DOC_DATE)
           UNION ALL
           --Acquisition tax component of resolution documents
           SELECT IPD.DOC_ID DOC_ID,
                  IDDRT.TAX_BASIS TAX_BASIS,
                  VI.VAT_CODE TAX_CODE,
                  VI.VAT_RATE TAX_RATE,
                  IM.DEPT DEPT,
                  IM.CLASS CLASS,
                  ROUND(IDDRT.TAX_BASIS * (VI.VAT_RATE/100), IPD.DECIMAL_DIGITS) AMOUNT,
                  AMT_ACQ_VAT TAX_TYPE
             FROM IM_POSTING_DOC IPD,
                  IM_DOC_DETAIL_REASON_CODES IDDRC,
                  ITEM_MASTER IM,
                  IM_DOC_DETAIL_RC_TAX IDDRT,
                  (SELECT WH LOCATION,VAT_REGION, 'W' LOC_TYPE FROM WH
                   UNION ALL
                   SELECT STORE LOCATION,VAT_REGION, 'S' LOC_TYPE FROM STORE) LOCS,
                  VAT_ITEM VI
            WHERE IPD.POSTING_ID                      = I_POSTING_ID
              AND IPD.HAS_ERRORS                      = YN_NO
              AND NVL(IPD.SYS_TAX_IND,'N')            = SYS_TAX_IND_ACQ_VAT
              AND IDDRC.DOC_ID                        = IPD.DOC_ID
              AND IDDRT.IM_DOC_DETAIL_REASON_CODES_ID = IDDRC.IM_DOC_DETAIL_REASON_CODES_ID
              AND IDDRC.STATUS                        = REIM_POSTING_SQL.DOC_STATUS_APPROVED
              AND IM.ITEM                             = IDDRC.ITEM
              AND (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)
              AND LOCS.LOCATION                       = IPD.LOCATION
              AND LOCS.LOC_TYPE                       = IPD.LOC_TYPE
              AND VI.VAT_REGION                       = LOCS.VAT_REGION
              AND VI.ITEM                             = IDDRC.ITEM
              AND VI.VAT_TYPE                        IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
              AND VI.ACTIVE_DATE    = (SELECT MAX(ACTIVE_DATE)
                                         FROM VAT_ITEM V
                                        WHERE V.ITEM = VI.ITEM
                                          AND V.VAT_REGION = VI.VAT_REGION
                                          AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                          AND V.ACTIVE_DATE <= IPD.DOC_DATE)
              UNION ALL
              --reverse vat component of resolution documents
              SELECT IPD.DOC_ID DOC_ID,
                     IDDRT.TAX_BASIS TAX_BASIS,
                     VI.VAT_CODE TAX_CODE,
                     VI.VAT_RATE TAX_RATE,
                     IM.DEPT DEPT,
                     IM.CLASS CLASS,
                     ROUND(IDDRT.TAX_BASIS * (VI.VAT_RATE/100), IPD.DECIMAL_DIGITS) AMOUNT,
                     AMT_REV_CHRG_VAT TAX_TYPE
                FROM IM_POSTING_DOC IPD,
                     IM_DOC_DETAIL_REASON_CODES IDDRC,
                     IM_DOC_DETAIL_RC_TAX IDDRT,
                     ITEM_MASTER IM,
                     (SELECT WH LOCATION,VAT_REGION, 'W' LOC_TYPE FROM WH
                      UNION ALL
                      SELECT STORE LOCATION,VAT_REGION, 'S' LOC_TYPE FROM STORE) LOCS,
                     VAT_ITEM VI
               WHERE IPD.POSTING_ID                      = I_POSTING_ID
                 AND IPD.SYS_TAX_IND                     = REIM_POSTING_SQL.SYS_TAX_IND_REV_CHRG_VAT
                 AND IPD.HAS_ERRORS                      = YN_NO
                 AND (IPD.DEAL_ID IS NULL OR IPD.DEAL_ID = 0)
                 AND IDDRC.DOC_ID                        = IPD.DOC_ID
                 AND IDDRC.STATUS                        = REIM_POSTING_SQL.DOC_STATUS_APPROVED
                 AND IDDRT.IM_DOC_DETAIL_REASON_CODES_ID = IDDRC.IM_DOC_DETAIL_REASON_CODES_ID
                 AND IM.ITEM                             = IDDRC.ITEM
                 AND LOCS.LOCATION                       = IPD.LOCATION
                 AND LOCS.LOC_TYPE                       = IPD.LOC_TYPE
                 AND VI.ITEM                             = IDDRC.ITEM
                 AND VI.REVERSE_VAT_IND                  = YN_YES
                 AND VI.VAT_REGION                       = LOCS.VAT_REGION
                 AND VI.VAT_TYPE       IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                 AND VI.ACTIVE_DATE    = (SELECT MAX(ACTIVE_DATE)
                                            FROM VAT_ITEM V
                                           WHERE V.ITEM = VI.ITEM
                                             AND V.VAT_REGION = VI.VAT_REGION
                                             AND V.VAT_TYPE IN (REIM_CONSTANTS.TAX_TYPE_COST, REIM_CONSTANTS.TAX_TYPE_BOTH)
                                             AND V.ACTIVE_DATE <= IPD.DOC_DATE))
    GROUP BY DOC_ID,
             TAX_CODE,
             TAX_RATE,
             DEPT,
             CLASS,
             TAX_TYPE;

    --Create Offset records
    INSERT INTO IM_DOC_SYS_TAX_POSTING (DOC_ID,
                                        TAX_BASIS,
                                        TAX_CODE,
                                        TAX_RATE,
                                        DEPT,
                                        CLASS,
                                        AMOUNT,
                                        TAX_TYPE)
    SELECT IDSTP.DOC_ID,
           IDSTP.TAX_BASIS,
           IDSTP.TAX_CODE,
           IDSTP.TAX_RATE,
           IDSTP.DEPT,
           IDSTP.CLASS,
           -1 * IDSTP.AMOUNT,
           DECODE(IDSTP.TAX_TYPE,
                  REIM_POSTING_SQL.AMT_ACQ_VAT, REIM_POSTING_SQL.AMT_ACQ_VAT_OFFSET,
                  REIM_POSTING_SQL.AMT_REV_CHRG_VAT, REIM_POSTING_SQL.AMT_REV_CHRG_VAT_OFFSET) TAX_TYPE
      FROM IM_POSTING_DOC IPD,
           IM_DOC_SYS_TAX_POSTING IDSTP
     WHERE IPD.POSTING_ID = I_POSTING_ID
       AND IDSTP.DOC_ID   = IPD.DOC_ID
       AND IDSTP.TAX_TYPE IN (REIM_POSTING_SQL.AMT_ACQ_VAT,
                              REIM_POSTING_SQL.AMT_REV_CHRG_VAT);

  RETURN SUCCESS;

   EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;

END POST_SYSTEM_GEN_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION PREPAY_MRCHI(O_error_message   IN OUT VARCHAR2,
                      O_prepay_comments    OUT OBJ_VARCHAR_ID_TABLE,
                      O_posting_id         OUT  NUMBER,
                      I_workspace_id    IN     IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'REIM_DOCUMENT_SQL.PREPAY_MRCHI';

   L_vdate DATE := GET_VDATE;

   L_description IM_POSTING_STATUS.DESCRIPTION%TYPE := 'PREPAY INVOICE';

   L_dirty_record CONSTANT VARCHAR2(30) := 'DIRTY_RECORD';

   L_doc_id_list OBJ_NUMERIC_ID_TABLE := NULL;

   L_currency_code            VARCHAR2(5) := NULL;
   L_num_tax_allow            VARCHAR2(1) := NULL;
   L_post_based_on_doc_header VARCHAR2(1) := NULL;

   cursor C_FETCH_PREPAY_CMNTS is
      select distinct prepay_comment
        from (with im_doc_search_ws as (select doc_id,
                                               doc_head_version_id
                                          from im_doc_invc_search_ws idisw
                                         where idisw.workspace_id = I_workspace_id
                                           and idisw.choice_flag  = REIM_CONSTANTS.YN_YES)
              --dirty check (check for object_version_id between WS and ops tables)
              select idsw.doc_id,
                     L_dirty_record prepay_comment
                from im_doc_search_ws idsw,
                     im_doc_head idh
               where idh.doc_id            = idsw.doc_id
                 and idh.object_version_id <> idsw.doc_head_version_id);

   cursor C_LOCK_DOC_HEAD is
      select 'x'
        from im_doc_head idh
       where EXISTS (select 'x'
                       from im_doc_invc_search_ws idisw
                      where idisw.workspace_id = I_workspace_id
                        and idisw.choice_flag  = REIM_CONSTANTS.YN_YES
                        and idisw.doc_id       = idh.doc_id)
         for UPDATE NOWAIT;

   cursor C_DOC_LIST is
      select doc_id
        from im_doc_invc_search_ws
       where workspace_id = I_workspace_id
         and choice_flag  = REIM_CONSTANTS.YN_YES;

BEGIN

   O_posting_id:= IM_POSTING_DOC_SEQ.NEXTVAL;
   LOGGER.LOG_INFORMATION('Start ' || L_program);
   LOGGER.TIME_RESET;
   LOGGER.TIME_START('PREPAY_MRCHI');

   open C_FETCH_PREPAY_CMNTS;
   fetch C_FETCH_PREPAY_CMNTS BULK COLLECT into O_prepay_comments;
   close C_FETCH_PREPAY_CMNTS;

   LOGGER.LOG_INFORMATION(L_program||' Generate error object - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if O_prepay_comments is NOT NULL and
      O_prepay_comments.COUNT > REIM_CONSTANTS.ZERO then
      ---
      LOGGER.TIME_STOP('PREPAY_MRCHI');
      LOGGER.LOG_INFORMATION('End ' || L_program );
      return REIM_CONSTANTS.SUCCESS;
   end if;

   open C_LOCK_DOC_HEAD;
   close C_LOCK_DOC_HEAD;

   select num_tax_allow,
          post_on_doc_header
     into L_num_tax_allow,
          L_post_based_on_doc_header
     from im_system_options;

   select currency_code
     into L_currency_code
     from system_config_options;

   -- insert into IM_POSTING_STATUS
   if BEGIN_POSTING_PROCESS(O_error_message,
                            O_posting_id,
                            L_description) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   open C_DOC_LIST;
   fetch C_DOC_LIST BULK COLLECT into L_doc_id_list;
   close C_DOC_LIST;
   
   --update best_terms,best_terms_date in im_doc_head
   if UPDATE_BEST_TERMS_PREPAY_INV(O_error_message,
                                   L_doc_id_list)= REIM_CONSTANTS.FAIL then                               
      return REIM_CONSTANTS.FAIL;
   end if;

   -- insert into IM_POSTING_DOC based on the doc_id_list
   if PREPARE_PREPAID_INVOICES(O_error_message,
                               O_posting_id,
                               L_doc_id_list,
                               L_currency_code) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   -- insert into tax tables depending on how vat_region differs from location and supplier
   if UPDATE_POSTING_DOC_TAX(O_error_message,
                             O_posting_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   -- insert into IM_POSTING_DOC_AMOUNTS
   if CALC_DOCUMENT_VALUE(O_error_message,
                          O_posting_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   -- insert into IM_POSTING_DOC_AMOUNTS
   if CALC_DOCUMENT_PPA_VALUE(O_error_message,
                              O_posting_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if L_num_tax_allow <> REIM_CONSTANTS.NUM_TAX_ALLOW_NONE then
      -- insert into IM_POSTING_DOC_AMOUNTS
      if CALC_DOCUMENT_TAX(O_error_message,
                           O_posting_id,
                           REIM_CONSTANTS.YN_YES) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;
   end if;

   if L_post_based_on_doc_header = REIM_CONSTANTS.YN_YES then
      -- insert into IM_POSTING_DOC_AMOUNTS
      if CALC_DOCUMENT_VCT(O_error_message,
                           O_posting_id,
                           REIM_CONSTANTS.YN_YES) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;
   end if;

   -- delete and insert into tax tables
   if LOAD_ACTUAL_TAX(O_error_message,
                      O_posting_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   LOGGER.LOG_INFORMATION(L_program||' Generate error object - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.TIME_STOP('PREPAY_MRCHI');
   LOGGER.LOG_INFORMATION('End ' || L_program );

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;

END PREPAY_MRCHI;
-------------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_VOID_CNR(O_error_message         OUT VARCHAR2,
                          I_posting_id         IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                          I_doc_id_list        IN     OBJ_NUMERIC_ID_TABLE,
                          I_prim_currency_code IN     VARCHAR2)
RETURN NUMBER
IS

   L_program      VARCHAR2(40) := 'REIM_POSTING_SQL.PREPARE_VOID_CNR';
   L_doc_id_count NUMBER(6)    := NULL;

BEGIN

   LOG_DEBUG(I_POSTING_ID,'BEGIN'|| TDELIM || L_PROGRAM);

   if I_doc_id_list is NULL then
     L_doc_id_count := 0;
   else
     L_doc_id_count := I_doc_id_list.COUNT;
   end if;

   --insert all CNRs that have been voided and are unrolled
   insert into im_posting_doc (posting_id,
                               doc_id,
                               doc_type,
                               calc_basis,
                               match_type,
                               parent_id,
                               doc_date,
                               vendor_type,
                               vendor,
                               order_no,
                               best_terms,
                               best_terms_date,
                               manually_paid_ind,
                               pre_paid_ind,
                               deal_id,
                               ext_doc_id,
                               currency_code,
                               location,
                               loc_type,
                               decimal_digits,
                               sys_currency_code,
                               sys_tax_ind)
      select I_posting_id,
             idh.doc_id,
             idh.type,
             -- calculations are mostly based
             -- on whether a document has details or not.
             REIM_POSTING_SQL.BASIS_DETAILS calc_basis,
             match_type_unknown match_type,
             idh.parent_id,
             idh.doc_date,
             idh.vendor_type,
             idh.vendor,
             idh.order_no,
             idh.best_terms,
             idh.best_terms_date,
             idh.manually_paid_ind,
             idh.pre_paid_ind,
             idh.deal_id,
             idh.ext_doc_id,
             idh.currency_code,
             idh.location,
             idh.loc_type,
             NVL((select MAX(icl.currency_cost_dec)
                    from im_currencies icl
                   where icl.currency_code = idh.currency_code), REIM_POSTING_SQL.DEFAULT_DIGITS),
             I_prim_currency_code,
             DECODE(NVL(idh.reverse_vat_ind, REIM_POSTING_SQL.YN_NO),
                    REIM_POSTING_SQL.YN_YES, REIM_POSTING_SQL.SYS_TAX_IND_REV_CHRG_VAT,
                    REIM_POSTING_SQL.YN_NO)
        from im_doc_head idh
       where idh.status    = REIM_CONSTANTS.DOC_STATUS_VOID
         and idh.type      IN (REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                               REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                               REIM_CONSTANTS.DOC_TYPE_CRDNRT)
         and idh.deal_type is NULL
         and EXISTS (select 'x'
                       from im_resolution_action ira
                      where ira.doc_id = idh.doc_id
                        and ira.status = REIM_CONSTANTS.RCA_STATUS_UNROLLED)
         and (   (    L_doc_id_count > 0
                  and idh.doc_id IN (select VALUE(ids)
                                       from table(cast(I_doc_id_list as OBJ_NUMERIC_ID_TABLE)) ids))
              or (L_doc_id_count = 0));

   if UPDATE_POSTING_DOC_SOB(O_error_message,
                             I_posting_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := O_error_message||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,
                                                             SQLERRM,
                                                             L_PROGRAM,
                                                             TO_CHAR(SQLCODE));

   return REIM_CONSTANTS.FAIL;

END PREPARE_VOID_CNR;
-------------------------------------------------------------------------------------------------------------
FUNCTION CALC_VOID_CNR_AMOUNT(O_error_message    OUT VARCHAR2,
                              I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_POSTING_SQL.CALC_VOID_CNR_AMOUNT';

BEGIN

   --insert into im_posting_doc_amounts from im_resolution_actions (DWO)
   insert into im_posting_doc_amounts (posting_amt_id,
                                       posting_id,
                                       doc_id,
                                       amount_type,
                                       location,
                                       org_unit,
                                       set_of_books_id,
                                       dept,
                                       class,
                                       subclass,
                                       group_by_1,
                                       group_by_2,
                                       group_by_3,
                                       group_by_4,
                                       group_by_5,
                                       group_by_6,
                                       group_by_7,
                                       group_by_8,
                                       amount,
                                       account_type,
                                       account_code,
                                       prim_currency_code)
      select IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
             posting_id,
             doc_id,
             REIM_POSTING_SQL.AMT_RESOLUTION_ACTION amount_type,
             loc,
             (select org_unit_id
                from (select org_unit_id,
                             store location
                        from store
                      union all
                      select case
                                when wh.primary_vwh is NULL then
                                   wh.org_unit_id
                                else
                                   (select org_unit_id
                                      from wh warehouse
                                     where warehouse.wh = wh.primary_vwh )
                             end org_unit_id,
                             wh.wh location
                        from wh)
               where location = loc
                 and rownum < 2) org_unit,
             set_of_books_id,
             dept,
             class,
             subclass,
             group_by_1,
             group_by_2,
             group_by_3,
             group_by_4,
             group_by_5,
             group_by_6,
             group_by_7,
             group_by_8,
             amount,
             account_type,
             account_code,
             NVL(sys_currency_code, (select loc_currency
                                       from mv_loc_sob loc_sob
                                      where loc_sob.location IN (select case mls.location_type
                                                                           when REIM_CONSTANTS.LOC_TYPE_WH then
                                                                              (select case
                                                                                         when warehouse.primary_vwh is null then
                                                                                            warehouse.wh
                                                                                         else
                                                                                            (select v_warehouse.wh
                                                                                               from wh v_warehouse
                                                                                              where warehouse.primary_vwh = v_warehouse.wh)
                                                                                      end
                                                                                 from wh warehouse
                                                                                where warehouse.wh = mls.location)
                                                                           else
                                                                              mls.location
                                                                        end real_loc
                                                                   from mv_loc_sob mls
                                                                  where mls.location = loc)
                                        and rownum < 2)) prim_currency_code
        from (select ipd.posting_id,
                     ipd.doc_id,
                     ipd.location loc,
                     ipd.set_of_books_id,
                     ipd.sys_currency_code,
                     dept,
                     class,
                     subclass,
                     irc.action group_by_1,
                     ira.reason_code_id group_by_2,
                     idh.deal_type group_by_3,
                     idh.consignment_ind group_by_4,
                     no_value group_by_5,
                     no_value group_by_6,
                     no_value group_by_7,
                     no_value group_by_8,
                     --This should be a negative value for the write off
                     ROUND(SUM(ira.unit_cost * ira.qty), ipd.decimal_digits) amount,
                     REASON_CODE_ACTIONS account_type,
                     irtcm.tran_code account_code
                from im_posting_doc ipd,
                     im_doc_head idh,
                     im_resolution_action ira,
                     im_reason_codes irc,
                     item_master im,
                     im_reason_tran_code_map irtcm
               where ipd.posting_id        = I_posting_id
                 and ipd.has_errors        = REIM_CONSTANTS.YN_NO
                 and idh.doc_id            = ipd.doc_id
                 and idh.status            = REIM_CONSTANTS.DOC_STATUS_VOID
                 and ira.doc_id            = ipd.doc_id
                 and irc.reason_code_id    = ira.reason_code_id
                 and im.item               = ira.item
                 and irtcm.reason_code_id  = ira.reason_code_id
                 and irtcm.set_of_books_id = ipd.set_of_books_id
               group by ipd.posting_id,
                        ipd.doc_id,
                        ipd.location,
                        ipd.set_of_books_id,
                        ipd.sys_currency_code,
                        im.dept,
                        im.class,
                        im.subclass,
                        irc.action,           -- group_by_1,
                        ira.reason_code_id,   -- group_by_2,
                        idh.deal_type,        -- group_by_3,
                        idh.consignment_ind,  -- group_by_4,
                        no_value,             -- group_by_5,
                        no_value,             -- group_by_6,
                        no_value,             -- group_by_7,
                        no_value,             -- group_by_8;
                        irtcm.tran_code,
                        ipd.decimal_digits);

   --insert into im_posting_doc_amounts from im_doc_detail_reason_codes (CBC, CBQ, etc)
   insert into im_posting_doc_amounts (posting_amt_id,
                                       posting_id,
                                       doc_id,
                                       amount_type,
                                       location,
                                       org_unit,
                                       set_of_books_id,
                                       dept,
                                       class,
                                       subclass,
                                       group_by_1,
                                       group_by_2,
                                       group_by_3,
                                       group_by_4,
                                       group_by_5,
                                       group_by_6,
                                       group_by_7,
                                       group_by_8,
                                       amount,
                                       account_type,
                                       account_code,
                                       prim_currency_code)
      select IM_POSTING_DOC_AMOUNTS_SEQ.NEXTVAL,
             posting_id,
             doc_id,
             AMT_RESOLUTION_ACTION_OFFSET amount_type,
             loc,
             (select org_unit_id
                from (select org_unit_id,
                             store location
                        from store
                      union all
                      select case
                                when wh.primary_vwh is NULL then
                                   wh.org_unit_id
                                else
                                   (select org_unit_id
                                      from wh warehouse
                                     where warehouse.wh = wh.primary_vwh )
                             end org_unit_id,
                             wh.wh location
                        from wh)
               where location = loc
                 and rownum < 2) org_unit,
             set_of_books_id,
             dept,
             class,
             subclass,
             group_by_1,
             group_by_2,
             group_by_3,
             group_by_4,
             group_by_5,
             group_by_6,
             group_by_7,
             group_by_8,
             amount,
             account_type,
             account_code,
             NVL(sys_currency_code, (select loc_currency
                                       from mv_loc_sob loc_sob
                                      where loc_sob.location IN (select case mls.location_type
                                                                           when REIM_CONSTANTS.LOC_TYPE_WH then
                                                                              (select case
                                                                                         when warehouse.primary_vwh is null then
                                                                                            warehouse.wh
                                                                                         else
                                                                                            (select v_warehouse.wh
                                                                                               from wh v_warehouse
                                                                                              where warehouse.primary_vwh = v_warehouse.wh)
                                                                                      end
                                                                                 from wh warehouse
                                                                                where warehouse.wh = mls.location)
                                                                           else
                                                                              mls.location
                                                                        end real_loc
                                                                   from mv_loc_sob mls
                                                                  where mls.location = loc)
                                        and rownum < 2)) prim_currency_code
        from (select ipd.posting_id,
                     ipd.doc_id,
                     ipd.location loc,
                     ipd.set_of_books_id,
                     ipd.sys_currency_code,
                     dept,
                     class,
                     subclass,
                     irc.action group_by_1,
                     iddrc.reason_code_id group_by_2,
                     idh.deal_type group_by_3,
                     idh.consignment_ind group_by_4,
                     no_value group_by_5,
                     no_value group_by_6,
                     no_value group_by_7,
                     no_value group_by_8,
                     --This should be a postive value because it is being voided
                     --This amount washes the DWO
                     ROUND(SUM((-1) *iddrc.adjusted_qty * iddrc.adjusted_unit_cost), ipd.decimal_digits) amount,
                     --Account Type of RCA
                     REASON_CODE_ACTIONS account_type,
                     irtcm.tran_code account_code
                from im_posting_doc ipd,
                     im_doc_head idh,
                     im_doc_detail_reason_codes iddrc,
                     im_reason_codes irc,
                     item_master im,
                     im_reason_tran_code_map irtcm
               where ipd.posting_id        = I_posting_id
                 and ipd.has_errors        = REIM_CONSTANTS.YN_NO
                 and idh.doc_id            = ipd.doc_id
                 and iddrc.doc_id          = ipd.doc_id
                 and irc.reason_code_id    = iddrc.reason_code_id
                 and im.item               = iddrc.item
                 and irtcm.reason_code_id  = iddrc.reason_code_id
                 and irtcm.set_of_books_id = ipd.set_of_books_id
               group by ipd.posting_id,
                        ipd.doc_id,
                        ipd.location,
                        ipd.set_of_books_id,
                        ipd.sys_currency_code,
                        im.dept,
                        im.class,
                        im.subclass,
                        irc.action,           -- group_by_1,
                        iddrc.reason_code_id, -- group_by_2,
                        idh.deal_type,        -- group_by_3,
                        idh.consignment_ind,  -- group_by_4,
                        no_value,             -- group_by_5,
                        no_value,             -- group_by_6,
                        no_value,             -- group_by_7,
                        no_value,             -- group_by_8;
                        irtcm.tran_code,
                        ipd.decimal_digits);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_ERROR_MESSAGE := O_ERROR_MESSAGE||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,
                                                             SQLERRM,
                                                             L_PROGRAM,
                                                             TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END CALC_VOID_CNR_AMOUNT;
-------------------------------------------------------------------------------------------------------------
FUNCTION TRUNCATE_FINANCIALS_STAGE(O_error_message    OUT VARCHAR2)
RETURN NUMBER
IS

   L_program VARCHAR2(60) := 'REIM_POSTING_SQL.TRUNCATE_FINANCIALS_STAGE';

BEGIN

   EXECUTE IMMEDIATE 'truncate table IM_FINANCIALS_STAGE';

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_ERROR_MESSAGE := O_ERROR_MESSAGE||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,
                                                             SQLERRM,
                                                             L_PROGRAM,
                                                             TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END TRUNCATE_FINANCIALS_STAGE;
-------------------------------------------------------------------------------------------------------------
FUNCTION TRUNCATE_AP_STAGE(O_error_message    OUT VARCHAR2)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_POSTING_SQL.TRUNCATE_AP_STAGE';

BEGIN

   EXECUTE IMMEDIATE 'truncate table IM_AP_STAGE_DETAIL';
   EXECUTE IMMEDIATE 'truncate table IM_AP_STAGE_HEAD';

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_ERROR_MESSAGE := O_ERROR_MESSAGE||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,
                                                             SQLERRM,
                                                             L_PROGRAM,
                                                             TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END TRUNCATE_AP_STAGE;
-------------------------------------------------------------------------------------------------------------
/**
 * The private function used to fetch the Set of Books, the document should get posted to.
 */
FUNCTION UPDATE_POSTING_DOC_SOB(O_error_message IN OUT VARCHAR2,
                                I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_POSTING_SQL.UPDATE_POSTING_DOC_SOB';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_posting_id: ' || I_posting_id);

   merge into im_posting_doc tgt
   using (select ipd.posting_id,
                 ipd.doc_id,
                 (select set_of_books_id
                    from mv_loc_sob mls
                   where mls.location_type in ('W', 'S')
                     and location = DECODE(ipd.order_no,
                                           NULL, DECODE(mls.location_type,
                                                        'W', (select wh.primary_vwh
                                                                from wh
                                                               where wh.wh = ipd.location),
                                                        'S', ipd.location),
                                           (select min(location)
                                              from ordloc ol
                                             where ol.order_no = ipd.order_no))) set_of_books_id
            from im_posting_doc ipd
           where ipd.posting_id = I_posting_id) src
   on (    tgt.posting_id = src.posting_id
       and tgt.doc_id     = src.doc_id)
   when MATCHED then
      update
         set tgt.set_of_books_id = src.set_of_books_id;

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_ERROR_MESSAGE := O_ERROR_MESSAGE||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,
                                                             SQLERRM,
                                                             L_PROGRAM,
                                                             TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END UPDATE_POSTING_DOC_SOB;
-------------------------------------------------------------------------------------------------------------
FUNCTION ADJUST_AP_STAGE_DETL(O_error_message       OUT VARCHAR2,
                              I_ap_stage_amt_tbl IN     OBJ_REIM_AP_STAGE_AMT_TBL)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_POSTING_SQL.ADJUST_AP_STAGE_DETL';

   L_user    IM_DOC_HEAD.CREATED_BY%TYPE := get_user;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start ');

   insert into im_ap_stage_detail_audit(doc_id,
                                        seq_no,
                                        tran_code,
                                        line_type_lookup_code,
                                        original_amount,
                                        adjusted_amount,
                                        created_by,
                                        creation_date)
                                 select iasde.doc_id,
                                        iasde.seq_no,
                                        iasde.tran_code,
                                        iasde.line_type_lookup_code,
                                        iasde.amount,
                                        tbl.amount,
                                        L_user,
                                        sysdate
                                   from TABLE(CAST(I_ap_stage_amt_tbl as OBJ_REIM_AP_STAGE_AMT_TBL)) tbl,
                                        im_ap_stage_detail_error iasde
                                  where iasde.doc_id = tbl.doc_id
                                    and iasde.seq_no = tbl.seq_no;

   LOGGER.LOG_INFORMATION(L_program||' Insert Audit Records - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_ap_stage_head(seq_no,
                                doc_id,
                                invc_type_lookup_code,
                                invoice_number,
                                vendor,
                                oracle_site_id,
                                currency_code,
                                exchange_rate,
                                exchange_rate_type,
                                doc_date,
                                amount,
                                best_terms_date,
                                segment1,
                                segment2,
                                segment3,
                                segment4,
                                segment5,
                                segment6,
                                segment7,
                                segment8,
                                segment9,
                                segment10,
                                create_date_time,
                                best_terms,
                                set_of_books_id,
                                posting_id,
                                org_unit,
                                segment11,
                                segment12,
                                segment13,
                                segment14,
                                segment15,
                                segment16,
                                segment17,
                                segment18,
                                segment19,
                                segment20,
                                odi_session_num,
                                reference_id)
                         select seq_no,
                                doc_id,
                                invc_type_lookup_code,
                                invoice_number,
                                vendor,
                                oracle_site_id,
                                currency_code,
                                exchange_rate,
                                exchange_rate_type,
                                doc_date,
                                amount,
                                best_terms_date,
                                segment1,
                                segment2,
                                segment3,
                                segment4,
                                segment5,
                                segment6,
                                segment7,
                                segment8,
                                segment9,
                                segment10,
                                create_date_time,
                                best_terms,
                                set_of_books_id,
                                posting_id,
                                org_unit,
                                segment11,
                                segment12,
                                segment13,
                                segment14,
                                segment15,
                                segment16,
                                segment17,
                                segment18,
                                segment19,
                                segment20,
                                odi_session_num,
                                reference_id
                           from im_ap_stage_head_error iashe
                          where iashe.doc_id IN (select tbl.doc_id
                                                   from TABLE(CAST(I_ap_stage_amt_tbl as OBJ_REIM_AP_STAGE_AMT_TBL)) tbl);

   LOGGER.LOG_INFORMATION(L_program||' Insert AP_STAGE_HEAD Record - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_ap_stage_detail(doc_id,
                                  seq_no,
                                  tran_code,
                                  line_type_lookup_code,
                                  amount,
                                  segment1,
                                  segment2,
                                  segment3,
                                  segment4,
                                  segment5,
                                  segment6,
                                  segment7,
                                  segment8,
                                  segment9,
                                  segment10,
                                  create_date_time,
                                  set_of_books_id,
                                  posting_id,
                                  org_unit,
                                  segment11,
                                  segment12,
                                  segment13,
                                  segment14,
                                  segment15,
                                  segment16,
                                  segment17,
                                  segment18,
                                  segment19,
                                  segment20,
                                  odi_session_num,
                                  reference_id,
                                  vat_code,
                                  vat_rate,
                                  line_group_no,
                                  prorate_across_flag)
                           select iasde.doc_id,
                                  iasde.seq_no,
                                  iasde.tran_code,
                                  iasde.line_type_lookup_code,
                                  tbl.amount, --modified amount
                                  iasde.segment1,
                                  iasde.segment2,
                                  iasde.segment3,
                                  iasde.segment4,
                                  iasde.segment5,
                                  iasde.segment6,
                                  iasde.segment7,
                                  iasde.segment8,
                                  iasde.segment9,
                                  iasde.segment10,
                                  iasde.create_date_time,
                                  iasde.set_of_books_id,
                                  iasde.posting_id,
                                  iasde.org_unit,
                                  iasde.segment11,
                                  iasde.segment12,
                                  iasde.segment13,
                                  iasde.segment14,
                                  iasde.segment15,
                                  iasde.segment16,
                                  iasde.segment17,
                                  iasde.segment18,
                                  iasde.segment19,
                                  iasde.segment20,
                                  iasde.odi_session_num,
                                  iasde.reference_id,
                                  iasde.vat_code,
                                  iasde.vat_rate,
                                  iasde.line_group_no,
                                  iasde.prorate_across_flag
                           from TABLE(CAST(I_ap_stage_amt_tbl as OBJ_REIM_AP_STAGE_AMT_TBL)) tbl,
                                im_ap_stage_detail_error iasde
                          where iasde.doc_id = tbl.doc_id
                            and iasde.seq_no = tbl.seq_no;

   LOGGER.LOG_INFORMATION(L_program||' Insert AP_STAGE_DETAIL Records - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_ap_stage_detail_error
    where doc_id IN (select tbl.doc_id
                       from TABLE(CAST(I_ap_stage_amt_tbl as OBJ_REIM_AP_STAGE_AMT_TBL)) tbl);

   LOGGER.LOG_INFORMATION(L_program||' Delete AP_STAGE_DETAIL_ERROR Records - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_ap_stage_head_error
    where doc_id IN (select tbl.doc_id
                       from TABLE(CAST(I_ap_stage_amt_tbl as OBJ_REIM_AP_STAGE_AMT_TBL)) tbl);

   LOGGER.LOG_INFORMATION(L_program||' Delete AP_STAGE_HEAD_ERROR Records - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   update im_doc_head
      set status            = REIM_CONSTANTS.DOC_STATUS_POSTED,
          post_date         = get_vdate,
          last_updated_by   = L_user,
          last_update_date  = sysdate,
          object_version_id = object_version_id + REIM_CONSTANTS.ONE
    where doc_id IN (select tbl.doc_id
                       from TABLE(CAST(I_ap_stage_amt_tbl as OBJ_REIM_AP_STAGE_AMT_TBL)) tbl);

   LOGGER.LOG_INFORMATION(L_program||' Update document status to POSTED - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION(L_program||' End ');
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_ERROR_MESSAGE := O_ERROR_MESSAGE||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,
                                                             SQLERRM,
                                                             L_PROGRAM,
                                                             TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END ADJUST_AP_STAGE_DETL;
-------------------------------------------------------------------------------------------------------------
/************************************************************
     * Updates best terms and best_terms_date for all the  invoices to be pre-paid.
     * Mainly updates into IM_DOC_HEAD
     ************************************************************/
    FUNCTION UPDATE_BEST_TERMS_PREPAY_INV(O_ERROR_MESSAGE  OUT VARCHAR2,
                                      I_DOC_ID_LIST IN OBJ_NUMERIC_ID_TABLE
                                      )
    RETURN NUMBER
    IS

        L_PROGRAM  VARCHAR2(60) := 'REIM_POSTING_SQL.UPDATE_BEST_TERMS_PREPAID_INVOICES';
        L_DOC_ID_COUNT  NUMBER(6) ;

    BEGIN
        -- Update best_terms and best_terms_source in IM_DOC_HEAD
       
       MERGE INTO IM_DOC_HEAD DH USING
		(SELECT * FROM
			( WITH TERMS_RANKING_DATA AS
				(SELECT UNIQUE TH.TERMS AS RANKING_TERMS ,
					CASE WHEN ENABLED_FLAG = 'Y'
						THEN TH.RANK
						ELSE 9999
					END AS RANK ,
					ENABLED_TD.PERCENT
					FROM TERMS_HEAD TH ,
					(SELECT terms,	
							percent,
							enabled_flag
					FROM
      (SELECT terms,
        percent,
        enabled_flag,
        row_number() over (partition BY terms order by NVL(START_DATE_ACTIVE,TO_DATE('1000','YYYY')) DESC,NVL(END_DATE_ACTIVE ,TO_DATE('3000','YYYY'))) AS row_number
      FROM terms_detail
      WHERE enabled_flag='Y'
      )
    WHERE row_number=1
    ORDER BY terms
    ) ENABLED_TD
  WHERE TH.TERMS = ENABLED_TD.TERMS ( + )
  ORDER BY TH.TERMS
  )
SELECT UNIQUE DOCUMENT.DOC_ID ,
  CASE
    WHEN SO.USE_INVOICE_TERMS_IND = 'Y'
    THEN 'DOC'
    WHEN SO.USE_INVOICE_TERMS_IND = 'N'
    THEN (
      CASE
        WHEN PO_TERMS_RANKING.RANK < DOCUMENT_TERMS_RANKING.RANK
        THEN 'ORDER'
        WHEN ( DOCUMENT_TERMS_RANKING.RANK = 9999
        AND PO_TERMS_RANKING.RANK          = 9999 )
        THEN NULL
        ELSE 'DOC'
      END )
  END AS BEST_TERMS_SOURCE ,
  CASE
    WHEN SO.USE_INVOICE_TERMS_IND = 'Y'
    THEN DOCUMENT.TERMS
    WHEN SO.USE_INVOICE_TERMS_IND = 'N'
    THEN (
      CASE
        WHEN PO_TERMS_RANKING.RANK < DOCUMENT_TERMS_RANKING.RANK
        THEN PO.TERMS
        WHEN ( DOCUMENT_TERMS_RANKING.RANK = 9999
        AND PO_TERMS_RANKING.RANK          = 9999 )
        THEN NULL
        ELSE DOCUMENT.TERMS
      END )
  END AS BEST_TERMS ,
  CASE
    WHEN SO.USE_INVOICE_TERMS_IND = 'Y'
    THEN DOCUMENT_TERMS_RANKING.PERCENT
    WHEN SO.USE_INVOICE_TERMS_IND = 'N'
    THEN (
      CASE
        WHEN PO_TERMS_RANKING.RANK < DOCUMENT_TERMS_RANKING.RANK
        THEN PO_TERMS_RANKING.PERCENT
        WHEN ( DOCUMENT_TERMS_RANKING.RANK = 9999
        AND PO_TERMS_RANKING.RANK          = 9999 )
        THEN DOCUMENT.TERMS_DSCNT_PCT
        ELSE DOCUMENT_TERMS_RANKING.PERCENT
      END )
  END AS TERMS_DISCNT_PCT
FROM IM_SUPPLIER_OPTIONS SO ,
  ORDHEAD PO ,
  IM_DOC_HEAD DOCUMENT ,
  TERMS_RANKING_DATA DOCUMENT_TERMS_RANKING ,
  TERMS_RANKING_DATA PO_TERMS_RANKING
WHERE DOCUMENT.ORDER_NO                  = PO.ORDER_NO
AND DOCUMENT_TERMS_RANKING.RANKING_TERMS = DOCUMENT.TERMS
AND PO_TERMS_RANKING.RANKING_TERMS       = PO.TERMS
AND DOCUMENT.VENDOR                      = SO.SUPPLIER
AND DOCUMENT.BEST_TERMS                 IS NULL
AND DOCUMENT.BEST_TERMS_SOURCE          IS NULL
AND DOCUMENT.TYPE                        = 'MRCHI'
AND (DOCUMENT.DOC_ID IN (SELECT * FROM TABLE(CAST(I_DOC_ID_LIST AS OBJ_NUMERIC_ID_TABLE))))              
  )
) SRC ON ( SRC.DOC_ID = DH.DOC_ID )
WHEN MATCHED THEN
  UPDATE
  SET DH.BEST_TERMS      = SRC.BEST_TERMS ,
    DH.BEST_TERMS_SOURCE = SRC.BEST_TERMS_SOURCE,
    DH.TERMS_DSCNT_PCT   = SRC.TERMS_DISCNT_PCT;
    
    -- Update best_terms_date and best_terms_date_source in IM_DOC_HEAD

  MERGE INTO IM_DOC_HEAD DH USING
(SELECT *
FROM
  (SELECT DH.DOC_ID ,
    DH.FREIGHT_TYPE ,
    SO.SUPPLIER ,
    SO.USE_INVOICE_TERMS_IND ,
    SO.ROG_DATE_ALLOWED_IND ,
    ROG_DATES.ROG_DATE ,
    CASE
      WHEN ( DH.FREIGHT_TYPE IS NULL
      OR ( DH.FREIGHT_TYPE   IS NOT NULL
      AND DH.FREIGHT_TYPE    != 'CC' ) )
      THEN (
        CASE
          WHEN ( SO.USE_INVOICE_TERMS_IND != 'Y'
          AND SO.ROG_DATE_ALLOWED_IND      = 'Y' )
          THEN (
            CASE
              WHEN ROG_DATES.ROG_DATE >= DH.DOC_DATE
              THEN 'ROG'
              ELSE 'DOC'
            END )
          ELSE 'DOC'
        END )
      ELSE 'DOC'
    END AS BEST_TERMS_DATE_SOURCE ,
    CASE
      WHEN ( DH.FREIGHT_TYPE IS NULL
      OR ( DH.FREIGHT_TYPE   IS NOT NULL
      AND DH.FREIGHT_TYPE    != 'CC' ) )
      THEN (
        CASE
          WHEN ( SO.USE_INVOICE_TERMS_IND != 'Y'
          AND SO.ROG_DATE_ALLOWED_IND      = 'Y' )
          THEN (
            CASE
              WHEN ROG_DATES.ROG_DATE >= DH.DOC_DATE
              THEN ROG_DATES.ROG_DATE
              ELSE ROG_DATES.DOC_DATE
            END )
          ELSE ROG_DATES.DOC_DATE
        END )
      ELSE ROG_DATES.DOC_DATE
    END AS BEST_TERMS_DATE
  FROM IM_DOC_HEAD DH ,
    IM_SUPPLIER_OPTIONS SO ,
    (SELECT DOC_ID ,
      NVL ( MAX ( RECEIVE_DATE ) , DOC_DATE ) AS ROG_DATE ,
      DOC_DATE
    FROM
      (SELECT UNIQUE INVOICE_RECEIPTS.DOC_ID ,
        S.RECEIVE_DATE ,
        INVOICE_RECEIPTS.DOC_DATE
      FROM SHIPMENT S ,
        ( WITH DM_INVC_RCPT AS
        (SELECT UNIQUE DMIH.DOC_ID AS DOC_ID ,
          DMRH.SHIPMENT            AS RECEIPT_ID
        FROM IM_DETAIL_MATCH_INVC_HISTORY DMIH ,
          IM_DETAIL_MATCH_RCPT_HISTORY DMRH
        WHERE DMIH.MATCH_ID = DMRH.MATCH_ID ( + )
        ) ,
        SM_INVC_RCPT AS
        (SELECT UNIQUE SMIH.DOC_ID AS DOC_ID ,
          SMRH.SHIPMENT            AS RECEIPT_ID
        FROM IM_SUMMARY_MATCH_INVC_HISTORY SMIH ,
          IM_SUMMARY_MATCH_RCPT_HISTORY SMRH
        WHERE SMIH.MATCH_ID = SMRH.MATCH_ID ( + )
        )
      SELECT UNIQUE DH.DOC_ID ,
        NVL ( DIR.RECEIPT_ID , - 1 ) AS SHIPMENT ,
        DH.DOC_DATE
      FROM IM_DOC_HEAD DH ,
        DM_INVC_RCPT DIR
      WHERE DH.DOC_ID = DIR.DOC_ID ( + )
      AND DH.TYPE     = 'MRCHI'
      UNION
      SELECT UNIQUE DH.DOC_ID ,
        NVL ( SIR.RECEIPT_ID , - 1 ) AS SHIPMENT ,
        DH.DOC_DATE
      FROM IM_DOC_HEAD DH ,
        SM_INVC_RCPT SIR
      WHERE DH.DOC_ID = SIR.DOC_ID ( + )
      AND DH.TYPE     = 'MRCHI'
       ) INVOICE_RECEIPTS
      WHERE INVOICE_RECEIPTS.SHIPMENT = S.SHIPMENT ( + )
      ORDER BY INVOICE_RECEIPTS.DOC_ID ,
        S.RECEIVE_DATE
      )
    GROUP BY DOC_ID ,
      DOC_DATE
    ) ROG_DATES
  WHERE DH.VENDOR                = SO.SUPPLIER
  AND DH.TYPE                    = 'MRCHI'
  AND DH.BEST_TERMS_DATE_SOURCE IS NULL
  AND DH.BEST_TERMS_DATE        IS NULL
  AND ROG_DATES.DOC_ID           = DH.DOC_ID
  AND (DH.DOC_ID IN (SELECT * FROM TABLE(CAST(I_DOC_ID_LIST AS OBJ_NUMERIC_ID_TABLE)))) 
  )
) SRC ON ( SRC.DOC_ID = DH.DOC_ID )
WHEN MATCHED THEN
  UPDATE
  SET DH.BEST_TERMS_DATE_SOURCE = SRC.BEST_TERMS_DATE_SOURCE ,
    DH.BEST_TERMS_DATE          = SRC.BEST_TERMS_DATE;
   
	RETURN SUCCESS;

    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                                   ||SQL_LIB.CREATE_MSG(REIM_POSTING_SQL.PACKAGE_ERROR,SQLERRM,L_PROGRAM,
                                                                TO_CHAR(SQLCODE));

          RETURN FAIL;


    

    END UPDATE_BEST_TERMS_PREPAY_INV;
	-----------------------------------------------------------
END REIM_POSTING_SQL;
/