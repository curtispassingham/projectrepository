CREATE OR REPLACE PACKAGE REIM_UNDO_MATCH_SQL AS

   STATUS_INIT    CONSTANT VARCHAR2(8) := 'INIT';
   STATUS_SUCCESS CONSTANT VARCHAR2(8) := 'SUCCESS';
   STATUS_FAILED  CONSTANT VARCHAR2(8) := 'FAILED';

--------------------------------------------------------------------
/**
 * The public function used to check if unmatch is possible and Perform Unmatch from Detail Match Item View Screen.
 *
 * Input param: I_workspace_id (The ID of the Detail match workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_UNMTCH_DETL(O_error_message      OUT VARCHAR2,
                             O_unmtch_comments    OUT OBJ_VARCHAR_ID_TABLE,
                             I_workspace_id    IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
/**
 * The public function used to check if unmatch is possible and Perform Unmatch from Match Inquiry Screen.
 *
 * Input param: I_doc_id (The ID of the Invoice that needs to be unmatched)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_UNMTCH_DOC(O_error_message      OUT VARCHAR2,
                            O_unmtch_comments    OUT OBJ_VARCHAR_ID_TABLE,
                            I_doc_id          IN     IM_DOC_HEAD.DOC_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
END REIM_UNDO_MATCH_SQL;
/
