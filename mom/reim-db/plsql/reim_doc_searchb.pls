CREATE OR REPLACE PACKAGE BODY REIM_SEARCH_SQL AS
--------------------------------------------------------------------

LP_doc_max_count NUMBER(4) := 300;
LP_expand_group_level NUMBER(4) := 3;

--------------------------------------------------------------------

LP_doc_with varchar2(5000) := q'[
   with sc_scalars as (
      select :1 as consignment_ind,
             :2 as deal_ind,
             :3 as deal_type,
             :4 as prepay_invoice_ind,
             :5 as detail_exist_ind,
             :6 as receipts_exist_ind,
             :7 as doc_hold_ind,
             :8 as start_document_date,
             :9 as end_document_date,
             :10 as start_due_date,
             :11 as end_due_date,
             :12 as start_routing_by_date,
             :13 as end_routing_by_date,
             :14 as start_resolve_by_date,
             :15 as end_resolve_by_date,
             :16 as loc_type,
             :17 as from_doc_cost,
             :18 as to_doc_cost,
             :19 as cash_disc_ind,
             :20 as manual_group_id,
             :21 as start_match_date,
             :22 as end_match_date,
             :23 as matchable_credit_note_ind,
             :24 as rtv_ind
        from dual),
   sc_doc_types      as (select value(in_doc_types) doc_type
                       from table(cast(:25 as OBJ_VARCHAR_ID_TABLE)) in_doc_types),
   sc_ext_doc_ids    as (select value(in_ext_doc_ids) ext_doc_id
                       from table(cast(:26 as OBJ_VARCHAR_DESC_TABLE)) in_ext_doc_ids),
   sc_vendor_types   as (select value(in_vendor_types) vendor_type
                       from table(cast(:27 as OBJ_VARCHAR_ID_TABLE)) in_vendor_types),
   sc_vendors        as (select value(in_vendors) vendor
                       from table(cast(:28 as OBJ_VARCHAR_ID_TABLE)) in_vendors),
   sc_vendor_names   as (select value(in_vendor_names) vendor_name
                       from table(cast(:29 as OBJ_VARCHAR_DESC_TABLE)) in_vendor_names),
   sc_supplier_sites as (select value(in_supplier_sites) supplier_site
                       from table(cast(:30 as OBJ_NUMERIC_ID_TABLE)) in_supplier_sites),
   sc_sup_site_names as (select value(in_sup_site_names) sup_site_name
                       from table(cast(:31 as OBJ_VARCHAR_DESC_TABLE)) in_sup_site_names),
   sc_statuses       as (select value(in_statuses) status
                       from table(cast(:32 as OBJ_VARCHAR_ID_TABLE)) in_statuses),
   sc_deal_ids       as (select value(in_deal_ids) deal_id
                       from table(cast(:33 as OBJ_NUMERIC_ID_TABLE)) in_deal_ids),
   sc_deal_detail_ids as (select value(in_deal_detail_ids) deal_detail_id
                       from table(cast(:34 as OBJ_NUMERIC_ID_TABLE)) in_deal_detail_ids),
   sc_locs           as (select value(in_locs) loc
                       from table(cast(:35 as OBJ_NUMERIC_ID_TABLE)) in_locs),
   sc_loc_names      as (select value(in_loc_names) loc_name
                       from table(cast(:36 as OBJ_VARCHAR_DESC_TABLE)) in_loc_names),
   sc_order_nos      as (select value(in_order_nos) order_no
                       from table(cast(:37 as OBJ_NUMERIC_ID_TABLE)) in_order_nos),
   sc_rtv_order_nos  as (select value(in_rtv_order_nos) rtv_order_no
                       from table(cast(:38 as OBJ_NUMERIC_ID_TABLE)) in_rtv_order_nos),
   sc_currencies     as (select value(in_currencies) currency
                       from table(cast(:39 as OBJ_VARCHAR_ID_TABLE)) in_currencies),
   sc_payment_terms  as (select value(in_payment_terms) payment_term
                       from table(cast(:40 as OBJ_VARCHAR_ID_TABLE)) in_payment_terms),
   sc_doc_sources    as (select value(in_doc_sources) doc_source
                       from table(cast(:41 as OBJ_VARCHAR_ID_TABLE)) in_doc_sources),
   sc_ref_docs       as (select value(in_ref_docs) ref_doc
                       from table(cast(:42 as OBJ_NUMERIC_ID_TABLE)) in_ref_docs),
   sc_match_ids      as (select value(in_match_ids) match_id
                       from table(cast(:43 as OBJ_VARCHAR_ID_TABLE)) in_match_ids),
   sc_ref_cnr_ext_doc_ids as (select value(ref_cnr_ext_doc_ids) ref_cnr_ext_doc_id
                                from table(cast(:44 as OBJ_VARCHAR_DESC_TABLE)) ref_cnr_ext_doc_ids),
   sc_ref_inv_ext_doc_ids as (select value(ref_inv_ext_doc_ids) ref_inv_ext_doc_id
                                from table(cast(:45 as OBJ_VARCHAR_DESC_TABLE)) ref_inv_ext_doc_ids),
   sc_group_ids as (select value(in_group_ids) group_id
                      from table(cast(:46 as OBJ_NUMERIC_ID_TABLE)) in_group_ids),
   sc_ap_reviewers as (select value(in_ap_reviewers) ap_reviewer
                      from table(cast(:47 as OBJ_VARCHAR_ID_TABLE)) in_ap_reviewers)
   ]';


LP_rcpt_with varchar2(5000) := q'[
   with sc_scalars as (
      select :1 as invc_driven_sources_only_ind,
             :2 as receipts_exist_ind,
             :3 as receipt_cost_and_qty_ind,
             :4 as start_receipt_date,
             :5 as end_receipt_date,
             :6 as from_reciept_cost,
             :7 as to_receipt_cost,
             :8 as loc_type,
             :9 as manual_group_id
        from dual),
   sc_sup_groups     as (select value(in_sup_groups) vendor
                       from table(cast(:10 as OBJ_NUMERIC_ID_TABLE)) in_sup_groups),
   sc_vendors        as (select value(in_vendors) vendor
                       from table(cast(:11 as OBJ_VARCHAR_ID_TABLE)) in_vendors),
   sc_vendor_names   as (select value(in_vendor_names) vendor_name
                       from table(cast(:12 as OBJ_VARCHAR_DESC_TABLE)) in_vendor_names),
   sc_supplier_sites as (select value(in_supplier_sites) supplier_site
                       from table(cast(:13 as OBJ_NUMERIC_ID_TABLE)) in_supplier_sites),
   sc_sup_site_names as (select value(in_sup_site_names) sup_site_name
                       from table(cast(:14 as OBJ_VARCHAR_DESC_TABLE)) in_sup_site_names),
   sc_locs           as (select value(in_locs) loc
                       from table(cast(:15 as OBJ_NUMERIC_ID_TABLE)) in_locs),
   sc_loc_names      as (select value(in_loc_names) loc_name
                       from table(cast(:16 as OBJ_VARCHAR_DESC_TABLE)) in_loc_names),
   sc_order_nos      as (select value(in_order_nos) order_no
                       from table(cast(:17 as OBJ_NUMERIC_ID_TABLE)) in_order_nos),
   sc_currencies     as (select value(in_currencies) currency
                       from table(cast(:18 as OBJ_VARCHAR_ID_TABLE)) in_currencies),
   sc_shipments      as (select value(in_shipments) shipment
                       from table(cast(:19 as OBJ_NUMERIC_ID_TABLE)) in_shipments),
   sc_invc_match_statuses as (select value(in_invc_match_statuses) invc_match_status
                            from table(cast(:20 as OBJ_VARCHAR_ID_TABLE)) in_invc_match_statuses)
   ]';

--------------------------------------------------------------------
FUNCTION COMMON_DOC_SEARCH(O_error_message     IN OUT VARCHAR2,
                           I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC,
                           I_discrepancy_privs IN     VARCHAR2,
                           I_client            IN     VARCHAR2)
RETURN BOOLEAN;
---
FUNCTION GET_DOC_SET_OF_BOOKS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
---
FUNCTION GET_RCPT_SET_OF_BOOKS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
---
FUNCTION GET_SUP_INFO(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
---
FUNCTION GET_LOCS(O_error_message     IN OUT VARCHAR2,
                  I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC,
                  IO_locs             IN OUT OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;
---
FUNCTION SETUP_DOC_FROM(O_error_message     IN OUT VARCHAR2,
                        IO_from             IN OUT VARCHAR2,
                        I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
---
FUNCTION SETUP_DOC_WHERE(O_error_message     IN OUT VARCHAR2,
                         IO_where            IN OUT VARCHAR2,
                         I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
---
FUNCTION FILTER_DOC_DETAIL(O_error_message     IN OUT VARCHAR2,
                           I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC,
                           I_client            IN     VARCHAR2)
RETURN BOOLEAN;
---
FUNCTION FILTER_DOC_INVC_DETAIL(O_error_message     IN OUT VARCHAR2,
                                I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC,
                                I_discrepancy_privs IN     VARCHAR2)
RETURN BOOLEAN;
-----------------------
FUNCTION COMMON_RCPT_SEARCH(O_error_message     IN OUT VARCHAR2,
                            I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
---
FUNCTION SETUP_RCPT_FROM(O_error_message     IN OUT VARCHAR2,
                         IO_from             IN OUT VARCHAR2,
                         I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
---
FUNCTION SETUP_RCPT_WHERE(O_error_message     IN OUT VARCHAR2,
                          IO_where            IN OUT VARCHAR2,
                          I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
---
FUNCTION FILTER_RCPT_DETAIL(O_error_message     IN OUT VARCHAR2,
                            I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
-----------------------
FUNCTION WRITE_DOC_RESULTS(O_error_message     IN OUT VARCHAR2,
                           O_max_rows_exceeded    OUT NUMBER,
                           I_workspace_id      IN     IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
-----------------------
FUNCTION WRITE_MANUAL_RESULTS(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_MATCH_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                              I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC,
                              I_choice_flag       IN     IM_MATCH_INVC_SEARCH_WS.CHOICE_FLAG%TYPE)
RETURN BOOLEAN;
-----------------------
FUNCTION WRITE_MATCH_INQ_RESULTS(O_error_message     IN OUT VARCHAR2,
                                 O_max_rows_exceeded    OUT NUMBER,
                                 I_workspace_id      IN     IM_MATCH_INQ_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
-----------------------
FUNCTION WRITE_CNR_RESULTS(O_error_message     IN OUT VARCHAR2,
                           I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
-----------------------
FUNCTION WRITE_CN_RESULTS(O_error_message     IN OUT VARCHAR2,
                          I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
-----------------------
FUNCTION WRITE_TAX_REV_LIST_RESULTS(O_error_message     IN OUT VARCHAR2,
                                    O_max_rows_exceeded    OUT NUMBER,
                                    I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
-----------------------
FUNCTION EXPAND_DOC_TO_GROUP(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
-----------------------
FUNCTION EXPAND_CN_TO_GROUP(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
-----------------------
FUNCTION EXPAND_RCPT_TO_GROUP(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
-----------------------
FUNCTION DOC_MATCHKEY_TO_DOC_MATCHKEY(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
-----------------------
FUNCTION DOC_GROUP_TO_RCPT_GROUP(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
-----------------------
FUNCTION DOC_MATCHKEY_TO_RCPT_MATCHKEY(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------
FUNCTION CALC_MATCH_KEY_GET_RCPT(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------
FUNCTION LOG_SEARCH_CRITERIA(O_error_message     IN OUT VARCHAR2,
                             I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                             I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------
FUNCTION LOG_SEARCH_CRITERIA(O_error_message     IN OUT VARCHAR2,
                             I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                             I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS
   L_program            VARCHAR2(61) := 'REIM_SEARCH_SQL.LOG_SEARCH_CRITERIA';
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--IN_COST_REVIEW_IND:'||I_search_criteria.IN_COST_REVIEW_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--IN_QTY_REVIEW_IND:'||I_search_criteria.IN_QTY_REVIEW_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--CONSIGNMENT_IND:'||I_search_criteria.CONSIGNMENT_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--DEAL_IND:'||I_search_criteria.DEAL_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--PREPAY_INVOICE_IND:'||I_search_criteria.PREPAY_INVOICE_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--DETAIL_EXIST_IND:'||I_search_criteria.DETAIL_EXIST_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--RECEIPTS_EXIST_IND:'||I_search_criteria.RECEIPTS_EXIST_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--DOC_HOLD_IND:'||I_search_criteria.DOC_HOLD_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--START_DOCUMENT_DATE:'||I_search_criteria.START_DOCUMENT_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--END_DOCUMENT_DATE:'||I_search_criteria.END_DOCUMENT_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--START_DUE_DATE:'||I_search_criteria.START_DUE_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--END_DUE_DATE:'||I_search_criteria.END_DUE_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--LOC_TYPE:'||I_search_criteria.LOC_TYPE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--INVC_EXIST_IND:'||I_search_criteria.INVC_EXIST_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--RECEIPT_COST_AND_QTY_IND:'||I_search_criteria.RECEIPT_COST_AND_QTY_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--START_RECEIPT_DATE:'||I_search_criteria.START_RECEIPT_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--END_RECEIPT_DATE:'||I_search_criteria.END_RECEIPT_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--FROM_DOC_COST:'||I_search_criteria.FROM_DOC_COST);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--TO_DOC_COST:'||I_search_criteria.TO_DOC_COST);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--FROM_RECIEPT_COST:'||I_search_criteria.FROM_RECIEPT_COST);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--TO_RECEIPT_COST:'||I_search_criteria.TO_RECEIPT_COST);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--DEPT:'||I_search_criteria.DEPT);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--CLASS:'||I_search_criteria.CLASS);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--SUBCLASS:'||I_search_criteria.SUBCLASS);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--START_RESOLVE_BY_DATE:'||I_search_criteria.START_RESOLVE_BY_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--END_RESOLVE_BY_DATE:'||I_search_criteria.END_RESOLVE_BY_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--FROM_DISCREP_COST:'||I_search_criteria.FROM_DISCREP_COST);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--TO_DISCREP_COST:'||I_search_criteria.TO_DISCREP_COST);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--FROM_DISCREP_QTY:'||I_search_criteria.FROM_DISCREP_QTY);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--TO_DISCREP_QTY:'||I_search_criteria.TO_DISCREP_QTY);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--START_ROUTING_BY_DATE:'||I_search_criteria.START_ROUTING_BY_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--END_ROUTING_BY_DATE:'||I_search_criteria.END_ROUTING_BY_DATE);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--CASH_DISC_IND:'||I_search_criteria.CASH_DISC_IND);
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--MANUAL_GROUP_ID:'||I_search_criteria.MANUAL_GROUP_ID);
   --
   if I_search_criteria.DOC_TYPE is not null and I_search_criteria.DOC_TYPE.COUNT > 0 then
   for i in 1..I_search_criteria.DOC_TYPE.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--DOC_TYPE('||i||'):'||I_search_criteria.DOC_TYPE(i));
   end loop; end if;
   --
   if I_search_criteria.EXT_DOC_ID is not null and I_search_criteria.EXT_DOC_ID.COUNT > 0 then
   for i in 1..I_search_criteria.EXT_DOC_ID.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--EXT_DOC_ID('||i||'):'||I_search_criteria.EXT_DOC_ID(i));
   end loop; end if;
   --
   if I_search_criteria.REF_DOC is not null and I_search_criteria.REF_DOC.COUNT > 0 then
   for i in 1..I_search_criteria.REF_DOC.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--REF_DOC('||i||'):'||I_search_criteria.REF_DOC(i));
   end loop; end if;
   --
   if I_search_criteria.SUPPLIER_GROUP_ID is not null and I_search_criteria.SUPPLIER_GROUP_ID.COUNT > 0 then
   for i in 1..I_search_criteria.SUPPLIER_GROUP_ID.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--SUPPLIER_GROUP_ID('||i||'):'||I_search_criteria.SUPPLIER_GROUP_ID(i));
   end loop; end if;
   --
   if I_search_criteria.VENDOR_TYPE is not null and I_search_criteria.VENDOR_TYPE.COUNT > 0 then
   for i in 1..I_search_criteria.VENDOR_TYPE.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--VENDOR_TYPE('||i||'):'||I_search_criteria.VENDOR_TYPE(i));
   end loop; end if;
   --
   if I_search_criteria.VENDOR is not null and I_search_criteria.VENDOR.COUNT > 0 then
   for i in 1..I_search_criteria.VENDOR.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--VENDOR('||i||'):'||I_search_criteria.VENDOR(i));
   end loop; end if;
   --
   if I_search_criteria.VENDOR_NAME is not null and I_search_criteria.VENDOR_NAME.COUNT > 0 then
   for i in 1..I_search_criteria.VENDOR_NAME.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--VENDOR_NAME('||i||'):'||I_search_criteria.VENDOR_NAME(i));
   end loop; end if;
   --
   if I_search_criteria.SUPPLIER_SITE_ID is not null and I_search_criteria.SUPPLIER_SITE_ID.COUNT > 0 then
   for i in 1..I_search_criteria.SUPPLIER_SITE_ID.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--SUPPLIER_SITE_ID('||i||'):'||I_search_criteria.SUPPLIER_SITE_ID(i));
   end loop; end if;
   --
   if I_search_criteria.SUPPLIER_SITE_NAME is not null and I_search_criteria.SUPPLIER_SITE_NAME.COUNT > 0 then
   for i in 1..I_search_criteria.SUPPLIER_SITE_NAME.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--SUPPLIER_SITE_NAME('||i||'):'||I_search_criteria.SUPPLIER_SITE_NAME(i));
   end loop; end if;
   --
   if I_search_criteria.STATUS is not null and I_search_criteria.STATUS.COUNT > 0 then
   for i in 1..I_search_criteria.STATUS.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--STATUS('||i||'):'||I_search_criteria.STATUS(i));
   end loop; end if;
   --
   if I_search_criteria.RTV_ORDER_NO is not null and I_search_criteria.RTV_ORDER_NO.COUNT > 0 then
   for i in 1..I_search_criteria.RTV_ORDER_NO.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--RTV_ORDER_NO('||i||'):'||I_search_criteria.RTV_ORDER_NO(i));
   end loop; end if;
   --
   if I_search_criteria.LOC is not null and I_search_criteria.LOC.COUNT > 0 then
   for i in 1..I_search_criteria.LOC.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--LOC('||i||'):'||I_search_criteria.LOC(i));
   end loop; end if;
   --
   if I_search_criteria.LOC_NAME is not null and I_search_criteria.LOC_NAME.COUNT > 0 then
   for i in 1..I_search_criteria.LOC_NAME.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--LOC_NAME('||i||'):'||I_search_criteria.LOC_NAME(i));
   end loop; end if;
   --
   if I_search_criteria.LOC_LIST is not null and I_search_criteria.LOC_LIST.COUNT > 0 then
   for i in 1..I_search_criteria.LOC_LIST.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--LOC_LIST('||i||'):'||I_search_criteria.LOC_LIST(i));
   end loop; end if;
   --
   if I_search_criteria.LOC_TRAIT is not null and I_search_criteria.LOC_TRAIT.COUNT > 0 then
   for i in 1..I_search_criteria.LOC_TRAIT.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--LOC_TRAIT('||i||'):'||I_search_criteria.LOC_TRAIT(i));
   end loop; end if;
   --
   if I_search_criteria.STORE_GRADE is not null and I_search_criteria.STORE_GRADE.COUNT > 0 then
   for i in 1..I_search_criteria.STORE_GRADE.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--STORE_GRADE('||i||'):'||I_search_criteria.STORE_GRADE(i));
   end loop; end if;
   --
   if I_search_criteria.ORDER_NO is not null and I_search_criteria.ORDER_NO.COUNT > 0 then
   for i in 1..I_search_criteria.ORDER_NO.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--ORDER_NO('||i||'):'||I_search_criteria.ORDER_NO(i));
   end loop; end if;
   --
   if I_search_criteria.ITEM is not null and I_search_criteria.ITEM.COUNT > 0 then
   for i in 1..I_search_criteria.ITEM.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--ITEM('||i||'):'||I_search_criteria.ITEM(i));
   end loop; end if;
   --
   if I_search_criteria.ITEM_DESC is not null and I_search_criteria.ITEM_DESC.COUNT > 0 then
   for i in 1..I_search_criteria.ITEM_DESC.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--ITEM_DESC('||i||'):'||I_search_criteria.ITEM_DESC(i));
   end loop; end if;
   --
   if I_search_criteria.VPN is not null and I_search_criteria.VPN.COUNT > 0 then
   for i in 1..I_search_criteria.VPN.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--VPN('||i||'):'||I_search_criteria.VPN(i));
   end loop; end if;
   --
   if I_search_criteria.AP_REVIEWER is not null and I_search_criteria.AP_REVIEWER.COUNT > 0 then
   for i in 1..I_search_criteria.AP_REVIEWER.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--AP_REVIEWER('||i||'):'||I_search_criteria.AP_REVIEWER(i));
   end loop; end if;
   --
   if I_search_criteria.CURRENCY is not null and I_search_criteria.CURRENCY.COUNT > 0 then
   for i in 1..I_search_criteria.CURRENCY.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--CURRENCY('||i||'):'||I_search_criteria.CURRENCY(i));
   end loop; end if;
   --
   if I_search_criteria.PAYMENT_TERMS is not null and I_search_criteria.PAYMENT_TERMS.COUNT > 0 then
   for i in 1..I_search_criteria.PAYMENT_TERMS.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--PAYMENT_TERMS('||i||'):'||I_search_criteria.PAYMENT_TERMS(i));
   end loop; end if;
   --
   if I_search_criteria.FREIGHT_PAYMENT_TERMS is not null and I_search_criteria.FREIGHT_PAYMENT_TERMS.COUNT > 0 then
   for i in 1..I_search_criteria.FREIGHT_PAYMENT_TERMS.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--FREIGHT_PAYMENT_TERMS('||i||'):'||I_search_criteria.FREIGHT_PAYMENT_TERMS(i));
   end loop; end if;
   --
   if I_search_criteria.DOC_SOURCE is not null and I_search_criteria.DOC_SOURCE.COUNT > 0 then
   for i in 1..I_search_criteria.DOC_SOURCE.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--DOC_SOURCE('||i||'):'||I_search_criteria.DOC_SOURCE(i));
   end loop; end if;
   --
   if I_search_criteria.RECEIPT is not null and I_search_criteria.RECEIPT.COUNT > 0 then
   for i in 1..I_search_criteria.RECEIPT.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--RECEIPT('||i||'):'||I_search_criteria.RECEIPT(i));
   end loop; end if;
   --
   if I_search_criteria.SHIPMENT_INVC_MATCH_STATUS is not null and I_search_criteria.SHIPMENT_INVC_MATCH_STATUS.COUNT > 0 then
   for i in 1..I_search_criteria.SHIPMENT_INVC_MATCH_STATUS.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--SHIPMENT_INVC_MATCH_STATUS('||i||'):'||I_search_criteria.SHIPMENT_INVC_MATCH_STATUS(i));
   end loop; end if;
   --
   if I_search_criteria.GROUP_ID is not null and I_search_criteria.GROUP_ID.COUNT > 0 then
   for i in 1..I_search_criteria.GROUP_ID.COUNT loop
   LOGGER.LOG_INFORMATION(L_program||' Search criteria for Workspace:'||I_workspace_id||'--GROUP_ID('||i||'):'||I_search_criteria.GROUP_ID(i));
   end loop; end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOG_SEARCH_CRITERIA;
--------------------------------------------------------------------
FUNCTION DOC_SEARCH(O_error_message     IN OUT VARCHAR2,
                    O_workspace_id         OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                    O_max_rows_exceeded    OUT NUMBER,
                    I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'REIM_SEARCH_SQL.DOC_SEARCH';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   O_workspace_id := im_workspace_id_seq.nextval;
   LOGGER.LOG_INFORMATION(L_program||' START DOC_SEARCH workspace:'||O_workspace_id);

   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_DOC_SEARCH(O_error_message,
                        I_search_criteria,
                        REIM_SEARCH_SQL.DISCREPANT_SEARCH_CRITERIA,
                        L_program) = FALSE then
      return 0;
   end if;

   if WRITE_DOC_RESULTS(O_error_message,
                        O_max_rows_exceeded,
                        O_workspace_id) = FALSE then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||O_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END DOC_SEARCH;
--------------------------------------------------------------------------
FUNCTION CONTEXT_SEARCH(O_error_message     IN OUT VARCHAR2,
                       O_workspace_id         OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                       O_max_rows_exceeded    OUT NUMBER,
                       I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'REIM_SEARCH_SQL.CONTEXT_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   O_workspace_id := im_workspace_id_seq.nextval;
   LOGGER.LOG_INFORMATION(L_program||' START CONTEXT_SEARCH workspace:'||O_workspace_id);

   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_DOC_SEARCH(O_error_message,
                        I_search_criteria,
                        REIM_SEARCH_SQL.DISCREPANT_SEARCH_CRITERIA,
                        L_program) = FALSE then
      return 0;
   end if;

   if EXPAND_DOC_TO_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   --expand to match key if not in a manual group
   if DOC_MATCHKEY_TO_DOC_MATCHKEY(O_error_message) = FALSE then
      return 0;
   end if;

   --populate receipt side for manual groups
   if DOC_GROUP_TO_RCPT_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   --populate receipt side for match key of no manual groups
   if DOC_MATCHKEY_TO_RCPT_MATCHKEY(O_error_message) = FALSE then
      return 0;
   end if;

   if WRITE_MANUAL_RESULTS(O_error_message,
                           O_workspace_id,
                           I_search_criteria,
                           'N') = FALSE then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||O_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END CONTEXT_SEARCH;
--------------------------------------------------------------------------
FUNCTION MANUAL_SEARCH(O_error_message     IN OUT VARCHAR2,
                       O_workspace_id         OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                       O_max_rows_exceeded    OUT NUMBER,
                       I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'REIM_SEARCH_SQL.MANUAL_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   O_workspace_id := im_workspace_id_seq.nextval;
   LOGGER.LOG_INFORMATION(L_program||' START MANUAL_SEARCH');

   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_DOC_SEARCH(O_error_message,
                        I_search_criteria,
                        REIM_SEARCH_SQL.DISCREPANT_SEARCH_CRITERIA,
                        L_program) = FALSE then
      return 0;
   end if;

   if EXPAND_DOC_TO_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   /*TODO:not needed?*/if EXPAND_CN_TO_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   if COMMON_RCPT_SEARCH(O_error_message,
                         I_search_criteria) = FALSE then
      return 0;
   end if;

   if EXPAND_RCPT_TO_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   if WRITE_MANUAL_RESULTS(O_error_message,
                           O_workspace_id,
                           I_search_criteria,
                           'N') = FALSE then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||O_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END MANUAL_SEARCH;
--------------------------------------------------------------------------
FUNCTION CREDIT_NOTE_SEARCH(O_error_message        IN OUT VARCHAR2,
                            O_workspace_id            OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                            O_max_rows_exceeded       OUT NUMBER,
                            I_cnr_search_criteria  IN OUT IM_DOC_SEARCH_CRITERIA_REC,
                            I_cn_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'REIM_SEARCH_SQL.CREDIT_NOTE_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   O_workspace_id := im_workspace_id_seq.nextval;
   LOGGER.LOG_INFORMATION(L_program||' START CREDIT_NOTE_SEARCH workspace:'||O_workspace_id);

   LOGGER.LOG_INFORMATION('Start log I_cnr_search_criteria');
   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_cnr_search_criteria) = FALSE then
      return 0;
   end if;
   LOGGER.LOG_INFORMATION('End log I_cnr_search_criteria');

   if COMMON_DOC_SEARCH(O_error_message,
                        I_cnr_search_criteria,
                        REIM_SEARCH_SQL.DISCREPANT_SEARCH_CRITERIA,
                        L_program) = FALSE then
      return 0;
   end if;

   if EXPAND_CN_TO_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   if WRITE_CNR_RESULTS(O_error_message,
                        O_workspace_id) = FALSE then
      return 0;
   end if;

   --------------------------

   LOGGER.LOG_INFORMATION('Start log I_cn_search_criteria');
   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_cn_search_criteria) = FALSE then
      return 0;
   end if;
   LOGGER.LOG_INFORMATION('End log I_cn_search_criteria');

   if COMMON_DOC_SEARCH(O_error_message,
                        I_cn_search_criteria,
                        REIM_SEARCH_SQL.DISCREPANT_SEARCH_CRITERIA,
                        L_program) = FALSE then
      return 0;
   end if;

   if EXPAND_CN_TO_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   if WRITE_CN_RESULTS(O_error_message,
                       O_workspace_id) = FALSE then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||O_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END CREDIT_NOTE_SEARCH;
--------------------------------------------------------------------------
FUNCTION MATCH_INQ_SEARCH(O_error_message     IN OUT VARCHAR2,
                          O_workspace_id         OUT IM_MATCH_INQ_SEARCH_WS.WORKSPACE_ID%TYPE,
                          O_max_rows_exceeded    OUT NUMBER,
                          I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'REIM_SEARCH_SQL.MATCH_INQ_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   O_workspace_id := im_workspace_id_seq.nextval;
   LOGGER.LOG_INFORMATION(L_program||' START MATCH_INQ_SEARCH workspace:'||O_workspace_id);

   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_DOC_SEARCH(O_error_message,
                        I_search_criteria,
                        REIM_SEARCH_SQL.DISCREPANT_SEARCH_CRITERIA,
                        L_program) = FALSE then
      return 0;
   end if;

   if EXPAND_DOC_TO_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   if WRITE_MATCH_INQ_RESULTS(O_error_message,
                              O_max_rows_exceeded,
                              O_workspace_id) = FALSE then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||O_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END MATCH_INQ_SEARCH;
--------------------------------------------------------------------------
FUNCTION DISCREPANCY_SEARCH(O_error_message     IN OUT VARCHAR2,
                            O_workspace_id         OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                            O_max_rows_exceeded    OUT NUMBER,
                            I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC,
                            I_discrepancy_privs IN     VARCHAR2)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'REIM_SEARCH_SQL.DISCREPANCY_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   O_workspace_id := im_workspace_id_seq.nextval;
   LOGGER.LOG_INFORMATION(L_program||' START DISCREPANCY_SEARCH workspace:'||O_workspace_id);

   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_DOC_SEARCH(O_error_message,
                        I_search_criteria,
                        I_discrepancy_privs,
                        L_program) = FALSE then
      return 0;
   end if;

   if EXPAND_DOC_TO_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   delete from im_rcpt_search_gtt;

   if DOC_GROUP_TO_RCPT_GROUP(O_error_message) = FALSE then
      return 0;
   end if;

   if CALC_MATCH_KEY_GET_RCPT(O_error_message) = FALSE then
      return 0;
   end if;

   if WRITE_MANUAL_RESULTS(O_error_message,
                           O_workspace_id,
                           I_search_criteria,
                           'Y') = FALSE then
      return 0;
   end if;

   if REIM_ONLINE_MATCH_SQL.INIT_ONLINE_MATCH_DATA(O_error_message,
                                                   O_workspace_id,
                                                   O_workspace_id,
                                                   NULL,
                                                   L_program) = 0 then
      return 0;
   end if;

   if REIM_ONLINE_MATCH_SQL.CREATE_DETAIL_MATCH_WS(O_error_message,
                                                   O_workspace_id) = 0 then
      return 0;
   end if;

   if REIM_ONLINE_MATCH_SQL.CREATE_DISCREPANCY_LIST_WS(O_error_message,
                                                       O_workspace_id) = 0 then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||O_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END DISCREPANCY_SEARCH;
--------------------------------------------------------------------------
FUNCTION TAX_REVIEW_LIST_SEARCH(O_error_message     IN OUT VARCHAR2,
                                O_workspace_id         OUT IM_MATCH_INQ_SEARCH_WS.WORKSPACE_ID%TYPE,
                                O_max_rows_exceeded    OUT NUMBER,
                                I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'REIM_SEARCH_SQL.TAX_REVIEW_LIST_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   O_workspace_id := im_workspace_id_seq.nextval;
   LOGGER.LOG_INFORMATION(L_program||' START TAX_REVIEW_LIST_SEARCH workspace:'||O_workspace_id);

   if LOG_SEARCH_CRITERIA(O_error_message,
                          O_workspace_id,
                          I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_DOC_SEARCH(O_error_message,
                        I_search_criteria,
                        REIM_SEARCH_SQL.DISCREPANT_SEARCH_CRITERIA,
                        L_program) = FALSE then
      return 0;
   end if;

   if WRITE_TAX_REV_LIST_RESULTS(O_error_message,
                                 O_max_rows_exceeded,
                                 O_workspace_id) = FALSE then
      return 0;
   end if;


   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||O_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END TAX_REVIEW_LIST_SEARCH;
--------------------------------------------------------------------------
FUNCTION COMMON_DOC_SEARCH(O_error_message     IN OUT VARCHAR2,
                           I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC,
                           I_discrepancy_privs IN     VARCHAR2,
                           I_client            IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.COMMON_DOC_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

   L_locs               OBJ_NUMERIC_ID_TABLE;

   L_insert VARCHAR2(2500)  :=
      q'[insert into im_doc_search_gtt (doc_id,
                                        ext_doc_id,
                                        doc_source,
                                        type,
                                        status,
                                        order_no,
                                        location,
                                        loc_type,
                                        loc_name,
                                        loc_vat_region,
                                        vendor_type,
                                        vendor,
                                        vendor_name,
                                        vendor_phone,
                                        supplier_site_id,
                                        supplier_site_name,
                                        supplier_site_vat_region,
                                        po_supplier,
                                        po_supplier_site,
                                        doc_date,
                                        due_date,
                                        freight_type,
                                        consignment_ind,
                                        currency_code,
                                        cost_pre_match,
                                        ap_reviewer,
                                        total_cost,
                                        total_qty,
                                        doc_head_version_id,
                                        match_id,
                                        match_date,
                                        hold_status,
                                        pre_paid_ind,
                                        rtv_order_no,
                                        rtv_ind,
                                        manually_paid_ind,
                                        group_id,
                                        custom_doc_ref_1,
                                        custom_doc_ref_2,
                                        custom_doc_ref_3,
                                        custom_doc_ref_4)
                                        ]';

   L_query VARCHAR2(2000)   :=
      q'[select dh.doc_id,
                dh.ext_doc_id,
                dh.doc_source,
                dh.type,
                dh.status,
                dh.order_no,
                dh.location,
                dh.loc_type,
                vl.loc_name,
                vl.vat_region,
                dh.vendor_type,
                dh.vendor,
                vn.vendor_name,
                vn.vendor_phone,
                dh.supplier_site_id,
                vn2.vendor_name,
                vn2.vat_region,
                null po_supplier,
                null po_supplier_site,
                dh.doc_date,
                dh.due_date,
                dh.freight_type,
                dh.consignment_ind,
                dh.currency_code,
                dh.cost_pre_match,
                null ap_reviewer,
                dh.total_cost,
                dh.total_qty,
                dh.object_version_id,
                dh.match_id,
                dh.match_date,
                dh.hold_status,
                dh.pre_paid_ind,
                dh.rtv_order_no,
                dh.rtv_ind,
                dh.manually_paid_ind,
                dh.group_id,
                dh.custom_doc_ref_1,
                dh.custom_doc_ref_2,
                dh.custom_doc_ref_3,
                dh.custom_doc_ref_4]';

   L_from VARCHAR2(5000)  :=
      q'[ from
               (select s.store loc,
                       s.store_name loc_name,
                       s.vat_region
                  from v_store s
                union all
                select w.wh loc,
                       w.wh_name loc_name,
                       w.vat_region
                  from v_wh w
               ) vl,
               (select p.partner_id vendor, p.partner_desc vendor_name,
                       p.partner_type vendor_type, p.contact_phone vendor_phone
                  from partner p
                union all
                select to_char(s.supplier) vendor, s.sup_name vendor_name,
                       'SUPP' vendor_type, s.contact_phone vendor_phone
                  from sups s
               ) vn,
               (select to_char(s2.supplier) vendor, s2.sup_name vendor_name,
                       'SUPP' vendor_type, s2.contact_phone vendor_phone,
                       s2.vat_region
                  from sups s2
               ) vn2,
               im_doc_head dh,
               im_system_options so,
               v_im_supp_site_attrib_expl sa
        ]';

   L_where VARCHAR2(5000)  := null;

   cursor C_FETCH_ITEM_SUPPLIER is
      select distinct supplier
        from (select /*+ CARDINALITY(items, 10) */ supplier
                from table(cast(I_search_criteria.item as OBJ_VARCHAR_ID_TABLE)) items,
                     item_supplier its
               where its.item = value(items)
              union all
              select /*+ CARDINALITY(items, 10) */ s_mem.supplier
                from table(cast(I_search_criteria.item as OBJ_VARCHAR_ID_TABLE)) items,
                     item_supplier its,
                     sups s_item,
                     im_supplier_group_members isgm_item,
                     im_supplier_group_members isgm_mem,
                     sups s_mem
               where its.item = value(items)
                and s_item.supplier       = its.supplier
                and isgm_item.supplier    = s_item.supplier_parent
                and isgm_mem.group_id     = isgm_item.group_id
                and s_mem.supplier_parent = isgm_mem.supplier);

BEGIN

   delete from im_doc_search_gtt;

   if I_search_criteria.supplier_site_id is NULL and I_search_criteria.item is NOT NULL then

      open C_FETCH_ITEM_SUPPLIER;
      fetch C_FETCH_ITEM_SUPPLIER BULK COLLECT into I_search_criteria.supplier_site_id;
      close C_FETCH_ITEM_SUPPLIER;

   end if;

   if GET_LOCS(O_error_message,
               I_search_criteria,
               I_search_criteria.loc) = FALSE then
      return FALSE;
   end if;

   if SETUP_DOC_FROM(O_error_message,
                     L_from,
                     I_search_criteria) = FALSE then
      return FALSE;
   end if;

   if SETUP_DOC_WHERE(O_error_message,
                      L_where,
                      I_search_criteria) = FALSE then
      return FALSE;
   end if;

   EXECUTE IMMEDIATE L_insert||LP_doc_with||L_query||L_from||L_where
      using I_search_criteria.consignment_ind,
            I_search_criteria.deal_ind,
            I_search_criteria.deal_type,
            I_search_criteria.prepay_invoice_ind,
            I_search_criteria.detail_exist_ind,
            I_search_criteria.receipts_exist_ind,
            I_search_criteria.doc_hold_ind,
            I_search_criteria.start_document_date,
            I_search_criteria.end_document_date,
            I_search_criteria.start_due_date,
            I_search_criteria.end_due_date,
            I_search_criteria.start_routing_by_date,
            I_search_criteria.end_routing_by_date,
            I_search_criteria.start_resolve_by_date,
            I_search_criteria.end_resolve_by_date,
            I_search_criteria.loc_type,
            I_search_criteria.from_doc_cost,
            I_search_criteria.to_doc_cost,
            I_search_criteria.cash_disc_ind,
            I_search_criteria.manual_group_id,
            I_search_criteria.start_match_date,
            I_search_criteria.end_match_date,
            I_search_criteria.matchable_credit_note_ind,
            I_search_criteria.rtv_ind,
            --
            I_search_criteria.doc_type,
            I_search_criteria.ext_doc_id,
            I_search_criteria.vendor_type,
            I_search_criteria.vendor,
            I_search_criteria.vendor_name,
            I_search_criteria.supplier_site_id,
            I_search_criteria.supplier_site_name,
            I_search_criteria.status,
            I_search_criteria.deal_id,
            I_search_criteria.deal_detail_id,
            I_search_criteria.loc,
            I_search_criteria.loc_name,
            I_search_criteria.order_no,
            I_search_criteria.rtv_order_no,
            I_search_criteria.currency,
            I_search_criteria.payment_terms,
            I_search_criteria.doc_source,
            I_search_criteria.ref_doc,
            I_search_criteria.match_id,
            I_search_criteria.ref_cnr_ext_doc_id,
            I_search_criteria.ref_inv_ext_doc_id,
            I_search_criteria.group_id,
            I_search_criteria.ap_reviewer;

   LOGGER.LOG_INFORMATION(L_program||' Insert im_doc_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if GET_DOC_SET_OF_BOOKS(O_error_message) = FALSE then
      return FALSE;
   end if;

   if GET_SUP_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   if FILTER_DOC_DETAIL(O_error_message,
                        I_search_criteria,
                        I_client) = FALSE then
      return FALSE;
   end if;

   if FILTER_DOC_INVC_DETAIL(O_error_message,
                             I_search_criteria,
                             I_discrepancy_privs) = FALSE then
      return FALSE;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COMMON_DOC_SEARCH;
--------------------------------------------------------------------------
FUNCTION GET_DOC_SET_OF_BOOKS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program    VARCHAR2(61)   := 'REIM_SEARCH_SQL.GET_DOC_SET_OF_BOOKS';
   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   merge into im_doc_search_gtt tgt
   using (select mlss.set_of_books_id,
                 gtts.doc_id
            from im_doc_search_gtt gtts,
                 mv_loc_sob mlss
           where mlss.location      = gtts.location
             and mlss.location_type = 'S'
             and gtts.order_no      is NULL
          union all
          select mlsw.set_of_books_id,
                 gttw.doc_id
            from im_doc_search_gtt gttw,
                 mv_loc_sob mlsw,
                 wh
           where wh.physical_wh     = gttw.location
             and wh.primary_vwh     = mlsw.location
             and mlsw.location_type = 'W'
             and gttw.order_no      is NULL
          union all
          select mlso.set_of_books_id,
                 gtto.doc_id
            from mv_loc_sob mlso,
                 (select ol.order_no,
                         Min(ol.location) location,
                         igtt.doc_id
                    from ordloc ol,
                         im_doc_search_gtt igtt
                   where igtt.order_no = ol.order_no
                   GROUP BY ol.order_no,
                            igtt.doc_id) gtto
           where mlso.location      = gtto.location
             and mlso.location_type IN ('W','S')) src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update
         set tgt.loc_set_of_books_id = src.set_of_books_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge SOB info im_doc_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DOC_SET_OF_BOOKS;
--------------------------------------------------------------------------
FUNCTION GET_SUP_INFO(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.GET_SUP_INFO';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   --figure out supplier info for supplier documents
   --get PO supplier and site
   update im_doc_search_gtt target
      set (ap_reviewer,
           match_key,
           supplier_group_id,
           po_supplier,
           po_supplier_site) =
           (select so.ap_reviewer ap_reviewer,
                   so.match_key match_key,
                   so.group_id group_id,
                   s.supplier_parent po_supplier,
                   oh.supplier po_supplier_site
              from v_im_supp_site_attrib_expl so,
                   ordhead oh,
                   sups s
             where oh.supplier = so.supplier
               and oh.supplier = s.supplier
               and oh.order_no = target.order_no)
    where target.match_key   is null
      and target.vendor_type = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER;

   LOGGER.LOG_INFORMATION(L_program||' Merge sup info im_doc_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SUP_INFO;
--------------------------------------------------------------------------
FUNCTION GET_LOCS(O_error_message     IN OUT VARCHAR2,
                  I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC,
                  IO_locs             IN OUT OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.GET_LOCS';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;
   L_query              VARCHAR2(4001) := null;
   L_cnt                NUMBER(4)      := 0;
   L_locs               OBJ_NUMERIC_ID_TABLE;

BEGIN

   delete from gtt_num_num_str_str_date_date;

   if ((I_search_criteria.loc is null         or I_search_criteria.loc.count         = 0) and
       (I_search_criteria.loc_list is null    or I_search_criteria.loc_list.count    = 0) and
       (I_search_criteria.loc_trait is null   or I_search_criteria.loc_trait.count   = 0) and
       (I_search_criteria.store_grade is null or I_search_criteria.store_grade.count = 0)) then
      return TRUE;
   end if;

   --put all the input locs into the gtt

   insert into gtt_num_num_str_str_date_date (number_1, number_2)
      select 1, value(in_locs) loc
        from table(cast(I_search_criteria.loc as OBJ_NUMERIC_ID_TABLE)) in_locs
      union all
      select 2, ld.location
        from table(cast(I_search_criteria.loc_list as OBJ_NUMERIC_ID_TABLE)) in_loc_lists,
             loc_list_detail ld
       where value(in_loc_lists) = ld.loc_list
      union all
      select 3, lt.store
        from table(cast(I_search_criteria.loc_trait as OBJ_NUMERIC_ID_TABLE)) in_loc_traits,
             loc_traits_matrix lt
       where value(in_loc_traits) = lt.loc_trait
      union all
      select 4, sg.store
        from table(cast(I_search_criteria.store_grade as OBJ_VARCHAR_DESC_TABLE)) in_store_grades,
             store_grade_store sg
       where value(in_store_grades) = sg.store_grade;

   --do not include virtual whs.
   delete from gtt_num_num_str_str_date_date
    where number_2 in(select wh from wh where wh != physical_wh);

   --run an intersect query across the differnt loc input criteria

   L_query := q'[select i.number_2 from ( ]';
   if (I_search_criteria.loc is not null and I_search_criteria.loc.count >= 1) then
      L_query := L_query || q'[ select number_2 from gtt_num_num_str_str_date_date where number_1 = 1 ]';
      L_cnt := L_cnt + 1;
   end if;

   if (I_search_criteria.loc_list is not null and I_search_criteria.loc_list.count >= 1) then
      if L_cnt > 0 then L_query := L_query || q'[ INTERSECT ]'; end if;
      L_query := L_query || q'[ select number_2 from gtt_num_num_str_str_date_date where number_1 = 2 ]';
      L_cnt := L_cnt + 1;
   end if;

   if (I_search_criteria.loc_trait is not null and I_search_criteria.loc_trait.count >= 1) then
      if L_cnt > 0 then L_query := L_query || q'[ INTERSECT ]'; end if;
      L_query := L_query || q'[ select number_2 from gtt_num_num_str_str_date_date where number_1 = 3 ]';
      L_cnt := L_cnt + 1;
   end if;

   if (I_search_criteria.store_grade is not null and I_search_criteria.store_grade.count >= 1) then
      if L_cnt > 0 then L_query := L_query || q'[ INTERSECT ]'; end if;
      L_query := L_query || q'[ select number_2 from gtt_num_num_str_str_date_date where number_1 = 4 ]';
      L_cnt := L_cnt + 1;
   end if;

   L_query := L_query || q'[ ) i ]';

   EXECUTE IMMEDIATE L_query BULK COLLECT INTO L_locs;
   if L_locs.count > 0 then
      IO_locs := L_locs;
   else
      IO_locs := OBJ_NUMERIC_ID_TABLE(-909090909);
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LOCS;
--------------------------------------------------------------------------
FUNCTION SETUP_DOC_FROM(O_error_message     IN OUT VARCHAR2,
                        IO_from             IN OUT VARCHAR2,
                        I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.SETUP_DOC_FROM';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   IO_from := IO_from || ',sc_scalars';

   if (I_search_criteria.manual_group_id is not null) then
      IO_from := IO_from ||',im_manual_group_invoices mgi';
   end if;
   if (I_search_criteria.doc_type is not null and I_search_criteria.doc_type.count >= 1) then
      IO_from := IO_from ||',sc_doc_types';
   end if;
   if (I_search_criteria.ext_doc_id is not null and I_search_criteria.ext_doc_id.count >= 1) then
      IO_from := IO_from ||',sc_ext_doc_ids';
   end if;
   if (I_search_criteria.vendor_type is not null and I_search_criteria.vendor_type.count >= 1) then
      IO_from := IO_from ||',sc_vendor_types';
   end if;
   if (I_search_criteria.vendor is not null and I_search_criteria.vendor.count >= 1) then
      IO_from := IO_from ||',sc_vendors';
   end if;
   if (I_search_criteria.vendor_name is not null and I_search_criteria.vendor_name.count >= 1) then
      IO_from := IO_from ||',sc_vendor_names';
   end if;
   if (I_search_criteria.supplier_site_id is not null and I_search_criteria.supplier_site_id.count >= 1) then
      IO_from := IO_from ||',sc_supplier_sites';
   end if;
   if (I_search_criteria.supplier_site_name is not null and I_search_criteria.supplier_site_name.count >= 1) then
      IO_from := IO_from ||',sc_sup_site_names';
   end if;
   if (I_search_criteria.status is not null and I_search_criteria.status.count >= 1) then
      IO_from := IO_from ||',sc_statuses';
   end if;
   if (I_search_criteria.deal_id is not null and I_search_criteria.deal_id.count >= 1) then
      IO_from := IO_from ||',sc_deal_ids';
   end if;
   if (I_search_criteria.deal_detail_id is not null and I_search_criteria.deal_detail_id.count >= 1) then
      IO_from := IO_from ||',sc_deal_detail_ids';
   end if;
   if (I_search_criteria.loc is not null and I_search_criteria.loc.count >= 1) then
      IO_from := IO_from ||',sc_locs';
   end if;
   if (I_search_criteria.loc_name is not null and I_search_criteria.loc_name.count >= 1) then
      IO_from := IO_from ||',sc_loc_names';
   end if;
   if (I_search_criteria.order_no is not null and I_search_criteria.order_no.count >= 1) then
      IO_from := IO_from ||',sc_order_nos';
   end if;
   if (I_search_criteria.rtv_order_no is not null and I_search_criteria.rtv_order_no.count >= 1) then
      IO_from := IO_from ||',sc_rtv_order_nos';
   end if;
   if (I_search_criteria.currency is not null and I_search_criteria.currency.count >= 1) then
      IO_from := IO_from ||',sc_currencies';
   end if;
   if (I_search_criteria.payment_terms is not null and I_search_criteria.payment_terms.count >= 1) then
      IO_from := IO_from ||',sc_payment_terms';
   end if;
   if (I_search_criteria.doc_source is not null and I_search_criteria.doc_source.count >= 1) then
      IO_from := IO_from ||',sc_doc_sources';
   end if;
   if (I_search_criteria.ref_doc is not null and I_search_criteria.ref_doc.count >= 1) then
      IO_from := IO_from ||',sc_ref_docs';
   end if;
   if (I_search_criteria.match_id is not null and I_search_criteria.match_id.count >= 1) then
      IO_from := IO_from ||',sc_match_ids';
   end if;
   if (I_search_criteria.ref_cnr_ext_doc_id is not null and I_search_criteria.ref_cnr_ext_doc_id.count >= 1) then
      IO_from := IO_from ||',sc_ref_cnr_ext_doc_ids';
   end if;
   if (I_search_criteria.ref_inv_ext_doc_id is not null and I_search_criteria.ref_inv_ext_doc_id.count >= 1) then
      IO_from := IO_from ||',sc_ref_inv_ext_doc_ids';
   end if;
   if (I_search_criteria.group_id is not null and I_search_criteria.group_id.count >= 1) then
      IO_from := IO_from ||',sc_group_ids';
   end if;
   if (I_search_criteria.ap_reviewer is not null and I_search_criteria.ap_reviewer.count >= 1) then
      IO_from := IO_from ||',sc_ap_reviewers';
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SETUP_DOC_FROM;
--------------------------------------------------------------------------
FUNCTION SETUP_DOC_WHERE(O_error_message     IN OUT VARCHAR2,
                         IO_where            IN OUT VARCHAR2,
                         I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.SETUP_DOC_WHERE';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;
   L_join_cnd           VARCHAR2(8)    := null;

BEGIN

   L_join_cnd := ' AND ';
   IO_where   := q'[ where dh.location                      = vl.loc
                       and dh.vendor                        = vn.vendor
                       and dh.vendor_type                   = vn.vendor_type
                       and dh.status                       != 'DELETE'
                       and dh.supplier_site_id              = vn2.vendor(+)
                       and 'SUPP'                           = vn2.vendor_type(+)
                       and dh.supplier_site_id              = sa.supplier(+)
                   ]';

   if (I_search_criteria.manual_group_id is not null) then
      IO_where := IO_where||L_join_cnd||q'[mgi.group_id = sc_scalars.manual_group_id]';
      IO_where := IO_where||L_join_cnd||q'[dh.doc_id = mgi.invoice_id]';
   end if;

   if I_search_criteria.consignment_ind is not null then
      IO_where := IO_where||L_join_cnd||'dh.consignment_ind = sc_scalars.consignment_ind';
   end if;
   if I_search_criteria.deal_ind is not null then
      IO_where := IO_where||L_join_cnd||'(( dh.deal_id is not null and sc_scalars.deal_ind = ''Y'' ) or '
                                      ||' ( dh.deal_id is null and sc_scalars.deal_ind = ''N'' ))';
   end if;
   if I_search_criteria.deal_type is not null then
      IO_where := IO_where||L_join_cnd||'dh.deal_type = sc_scalars.deal_type';
   end if;
   if I_search_criteria.prepay_invoice_ind is not null then
      IO_where := IO_where||L_join_cnd||'dh.pre_paid_ind = sc_scalars.prepay_invoice_ind';
   end if;
   if I_search_criteria.matchable_credit_note_ind = 'Y' then
      if I_search_criteria.doc_hold_ind is not null or
         (I_search_criteria.status is not null and I_search_criteria.status.count >= 1) then
         O_error_message := SQL_LIB.CREATE_MSG('MATCHABLE_CREDIT_NOTE_IND_CS_ERR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      IO_where := IO_where||L_join_cnd||
         q'[((dh.status = 'POSTED' and dh.hold_status = 'N') or (dh.status = 'APPRVE' and dh.hold_status in ('H', 'R')))]';
   else
      if I_search_criteria.doc_hold_ind is not null then
         IO_where := IO_where||L_join_cnd||'(( dh.hold_status = ''H'' and sc_scalars.doc_hold_ind = ''Y'' ) or '
                                         ||' ( dh.hold_status != ''H'' and sc_scalars.doc_hold_ind = ''N'' ))';
      end if;
      if (I_search_criteria.status is not null and I_search_criteria.status.count >= 1) then
         IO_where := IO_where||L_join_cnd||'dh.status = sc_statuses.status';
      else
         IO_where := IO_where||L_join_cnd||'dh.status = dh.status';
      end if;
   end if;
   if I_search_criteria.rtv_ind is not null then
      IO_where := IO_where||L_join_cnd||'dh.rtv_ind = sc_scalars.rtv_ind';
   end if;
   if I_search_criteria.start_document_date is not null or I_search_criteria.end_document_date is not null then
      IO_where := IO_where||L_join_cnd||'dh.doc_date between '
         ||'nvl(sc_scalars.start_document_date,dh.doc_date) and '
         ||'nvl(sc_scalars.end_document_date,dh.doc_date)';
   end if;
   if I_search_criteria.start_due_date is not null or I_search_criteria.end_due_date is not null then
      IO_where := IO_where||L_join_cnd||'dh.due_date between '
         ||'nvl(sc_scalars.start_due_date,dh.due_date) and '
         ||'nvl(sc_scalars.end_due_date,dh.due_date)';
   end if;
   if I_search_criteria.start_routing_by_date is not null or I_search_criteria.end_routing_by_date is not null then
      IO_where := IO_where||L_join_cnd
         ||'((dh.due_date - so.days_before_due_date) between '
         ||'  nvl(sc_scalars.start_routing_by_date,(dh.due_date - so.days_before_due_date)) and '
         ||'  nvl(sc_scalars.end_routing_by_date,(dh.due_date - so.days_before_due_date))'
         ||' or '
         ||' (dh.doc_date + sa.qty_disc_day_before_rte) between '
         ||'  nvl(sc_scalars.start_routing_by_date,(dh.doc_date + sa.qty_disc_day_before_rte)) and '
         ||'  nvl(sc_scalars.end_routing_by_date,(dh.doc_date + sa.qty_disc_day_before_rte)))';
   end if;
   if I_search_criteria.start_resolve_by_date is not null or I_search_criteria.end_resolve_by_date is not null then
      IO_where := IO_where||L_join_cnd||'(dh.due_date - so.resolution_due_days) between '
         ||'nvl(sc_scalars.start_resolve_by_date,(dh.due_date-so.resolution_due_days)) and '
         ||'nvl(sc_scalars.end_resolve_by_date,(dh.due_date-so.resolution_due_days))';
   end if;
   if I_search_criteria.loc_type is not null then
      IO_where := IO_where||L_join_cnd||'dh.loc_type = sc_scalars.loc_type';
   end if;
   if I_search_criteria.start_match_date is not null or I_search_criteria.end_match_date is not null then
      IO_where := IO_where||L_join_cnd||'dh.match_date between '
         ||'nvl(sc_scalars.start_match_date,dh.match_date) and '
         ||'nvl(sc_scalars.end_match_date,dh.match_date)';
   end if;

   ----

   if (I_search_criteria.payment_terms is not null and I_search_criteria.payment_terms.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.terms = sc_payment_terms.payment_term';
   end if;
   if I_search_criteria.cash_disc_ind = 'Y' then
      IO_where := IO_where||L_join_cnd||'dh.total_discount is not null and dh.total_discount != 0';
   end if;
   if I_search_criteria.cash_disc_ind = 'N' then
      IO_where := IO_where||L_join_cnd||'(dh.total_discount is null or dh.total_discount = 0)';
   end if;
   if (I_search_criteria.doc_source is not null and I_search_criteria.doc_source.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.doc_source = sc_doc_sources.doc_source';
   end if;
   if (I_search_criteria.doc_type is not null and I_search_criteria.doc_type.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.type = sc_doc_types.doc_type';
   end if;
   if (I_search_criteria.ext_doc_id is not null and I_search_criteria.ext_doc_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(dh.ext_doc_id) like('%'||lower(nvl(sc_ext_doc_ids.ext_doc_id,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.vendor_type is not null and I_search_criteria.vendor_type.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.vendor_type = sc_vendor_types.vendor_type';
   end if;
   if (I_search_criteria.vendor is not null and I_search_criteria.vendor.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.vendor = sc_vendors.vendor';
   end if;
   if (I_search_criteria.vendor_name is not null and I_search_criteria.vendor_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vn.vendor_name) like('%'||lower(nvl(sc_vendor_names.vendor_name,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.supplier_site_id is not null and I_search_criteria.supplier_site_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.supplier_site_id = sc_supplier_sites.supplier_site';
   end if;
   if (I_search_criteria.supplier_site_name is not null and I_search_criteria.supplier_site_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vn2.vendor_name) like('%'||lower(nvl(sc_sup_site_names.sup_site_name,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.deal_id is not null and I_search_criteria.deal_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.deal_id = sc_deal_ids.deal_id';
   end if;
   if (I_search_criteria.deal_detail_id is not null and I_search_criteria.deal_detail_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.deal_detail_id = sc_deal_detail_ids.deal_detail_id';
   end if;
   if (I_search_criteria.loc is not null and I_search_criteria.loc.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.location = sc_locs.loc';
   end if;
   if (I_search_criteria.loc_name is not null and I_search_criteria.loc_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vl.loc_name) like('%'||lower(nvl(sc_loc_names.loc_name,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.order_no is not null and I_search_criteria.order_no.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.order_no = sc_order_nos.order_no';
   end if;
   if (I_search_criteria.rtv_order_no is not null and I_search_criteria.rtv_order_no.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.rtv_order_no = sc_rtv_order_nos.rtv_order_no';
   end if;
   if (I_search_criteria.currency is not null and I_search_criteria.currency.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.currency_code = sc_currencies.currency';
   end if;
   if I_search_criteria.from_doc_cost is not null or I_search_criteria.to_doc_cost is not null then
      IO_where := IO_where||L_join_cnd||'dh.total_cost between '
         ||'nvl(sc_scalars.from_doc_cost,dh.total_cost) and '
         ||'nvl(sc_scalars.to_doc_cost,dh.total_cost)';
   end if;
   if (I_search_criteria.ref_doc is not null and I_search_criteria.ref_doc.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.ref_doc = sc_ref_docs.ref_doc';
   end if;
   if (I_search_criteria.match_id is not null and I_search_criteria.match_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.match_id = sc_match_ids.match_id';
   end if;
   if (I_search_criteria.ref_cnr_ext_doc_id is not null and I_search_criteria.ref_cnr_ext_doc_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.ref_cnr_ext_doc_id = sc_ref_cnr_ext_doc_ids.ref_cnr_ext_doc_id';
   end if;
   if (I_search_criteria.ref_inv_ext_doc_id is not null and I_search_criteria.ref_inv_ext_doc_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.ref_inv_ext_doc_id = sc_ref_inv_ext_doc_ids.ref_inv_ext_doc_id';
   end if;
   if (I_search_criteria.group_id is not null and I_search_criteria.group_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'dh.group_id = sc_group_ids.group_id';
   end if;
   if (I_search_criteria.ap_reviewer is not null and I_search_criteria.ap_reviewer.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(sa.ap_reviewer) like('%'||lower(nvl(sc_ap_reviewers.ap_reviewer,'-90909'))||'%')]';
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SETUP_DOC_WHERE;
--------------------------------------------------------------------------
FUNCTION FILTER_DOC_INVC_DETAIL(O_error_message     IN OUT VARCHAR2,
                                I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC,
                                I_discrepancy_privs IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.FILTER_DOC_INVC_DETAIL';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

   L_delete VARCHAR2(2000)  := q'[delete from im_doc_search_gtt gtt where not exists ( ]';

   L_with varchar2(5000) := q'[
      with sc_scalars as (
         select :1 as dept,
                :2 as class,
                :3 as subclass,
                :4 as in_cost_review_ind,
                :5 as in_qty_review_ind,
                :6 as debit_memo_reversed_ind
           from dual),
      sc_items          as (select value(in_items) item
                          from table(cast(:7 as OBJ_VARCHAR_ID_TABLE)) in_items),
      sc_item_descs     as (select value(in_item_descs) item_desc
                          from table(cast(:8 as OBJ_VARCHAR_DESC_TABLE)) in_item_descs),
      sc_vpns           as (select value(in_vpns) vpn
                          from table(cast(:9 as OBJ_VARCHAR_DESC_TABLE)) in_vpns)
   ]';

   L_select    varchar2(100)  := q'[ select 'x' ]';
   L_from      varchar2(100)  := q'[ from item_master im, im_invoice_detail id ]';
   L_from_2    varchar2(100)  := q'[ from item_master im, im_doc_detail_reason_codes id ]';
   L_from_add  varchar2(5000) := null;

   L_join_cnd  varchar2(8)    := ' and ';
   L_where     varchar2(100)  := q'[ where id.doc_id = gtt.doc_id and id.item = im.item ]';
   L_where_add varchar2(5000) := null;

   L_where_dm_invc    varchar2(5000) := null;
   L_where_dm_reason  varchar2(5000) := null;

BEGIN

   --setup from
   L_from_add := L_from_add || q'[,sc_scalars]';
   if (I_search_criteria.item is not null and I_search_criteria.item.count >= 1) then
      L_from_add := L_from_add ||q'[,sc_items]';
   end if;
   if (I_search_criteria.item_desc is not null and I_search_criteria.item_desc.count >= 1) then
      L_from_add := L_from_add ||q'[,sc_item_descs]';
   end if;
   if (I_search_criteria.vpn is not null and I_search_criteria.vpn.count >= 1) then
      L_from_add := L_from_add ||q'[,sc_vpns]';
      L_from_add := L_from_add ||q'[,item_supplier isup]';
   end if;

   --setup where
   if (I_search_criteria.item is not null and I_search_criteria.item.count >= 1) then
      L_where_add := L_where_add||L_join_cnd||q'[im.item = sc_items.item]';
   end if;
   if (I_search_criteria.item_desc is not null and I_search_criteria.item_desc.count >= 1) then
      L_where_add := L_where_add||L_join_cnd||q'[lower(im.item_desc) like('%'||lower(sc_item_descs.item_desc)||'%')]';
   end if;
   if I_search_criteria.dept is not null then
      L_where_add := L_where_add||L_join_cnd||q'[    im.dept = sc_scalars.dept
                                                 and im.class = nvl(sc_scalars.class, im.class)
                                                 and im.subclass = nvl(sc_scalars.subclass, im.subclass)]';
   end if;

   if I_discrepancy_privs = REIM_SEARCH_SQL.DISCREPANT_SEARCH_CRITERIA then
      if I_search_criteria.in_cost_review_ind is not null and I_search_criteria.in_cost_review_ind = 'Y' then
         L_where_add := L_where_add||L_join_cnd||q'[id.cost_matched in('M', 'R')]';
      end if;
      if I_search_criteria.in_cost_review_ind is not null and I_search_criteria.in_cost_review_ind = 'N' then
         L_where_add := L_where_add||L_join_cnd||q'[id.cost_matched in('N', 'D')]';
      end if;
      if I_search_criteria.in_qty_review_ind is not null and I_search_criteria.in_qty_review_ind = 'Y' then
         L_where_add := L_where_add||L_join_cnd||q'[id.qty_matched in('M', 'R')]';
      end if;
      if I_search_criteria.in_qty_review_ind is not null and I_search_criteria.in_qty_review_ind = 'N' then
         L_where_add := L_where_add||L_join_cnd||q'[id.qty_matched in('N', 'D')]';
      end if;
   elsif I_discrepancy_privs = REIM_SEARCH_SQL.DISCREPANT_COST_IND then
      L_where_add := L_where_add||L_join_cnd||q'[id.cost_matched = 'D']';
   elsif I_discrepancy_privs = REIM_SEARCH_SQL.DISCREPANT_QTY_IND then
      L_where_add := L_where_add||L_join_cnd||q'[id.qty_matched = 'D']';
   elsif I_discrepancy_privs = REIM_SEARCH_SQL.DISCREPANT_BOTH_IND then
      L_where_add := L_where_add||L_join_cnd||q'[(id.cost_matched = 'D' or id.qty_matched = 'D')]';
   end if;

   if I_search_criteria.detail_exist_ind = 'Y' then
      --force delete to run with out any extra join conditions
      L_where_add := L_where_add||L_join_cnd||q'[1=1]';
   end if;
   if (I_search_criteria.vpn is not null and I_search_criteria.vpn.count >= 1) then
      L_where_add := L_where_add||L_join_cnd||q'[isup.supplier = nvl(gtt.supplier_site_id, gtt.vendor)]';
      L_where_add := L_where_add||L_join_cnd||q'[isup.item = id.item]';
      L_where_add := L_where_add||L_join_cnd||q'[lower(isup.vpn) like('%'||lower(sc_vpns.vpn)||'%')]';
   end if;

   if I_search_criteria.debit_memo_reversed_ind is not null then
      L_where_dm_invc   := L_where_dm_invc||L_join_cnd||q'[1=2]';

      if I_search_criteria.debit_memo_reversed_ind = 'Y' then
         L_where_dm_reason := L_where_dm_reason||L_join_cnd||q'[(id.reversed_qty is not null or id.reversed_unit_cost is not null)]';
      elsif I_search_criteria.debit_memo_reversed_ind = 'N' then
         L_where_dm_reason := L_where_dm_reason||L_join_cnd||q'[(id.reversed_qty is null and id.reversed_unit_cost is null)]';
      end if;

   end if;

   --if nothing added to where return with out running the delete
   if L_where_add is null and L_where_dm_invc is null then
      return TRUE;
   end if;

   EXECUTE IMMEDIATE L_delete||L_with||q'[(]'||
                     L_select||L_from||L_from_add||L_where||L_where_add||L_where_dm_invc||
                     q'[ union all ]'||
                     L_select||L_from_2||L_from_add||L_where||L_where_add||L_where_dm_reason||q'[))]'
      using I_search_criteria.dept,
            I_search_criteria.class,
            I_search_criteria.subclass,
            I_search_criteria.in_cost_review_ind,
            I_search_criteria.in_qty_review_ind,
            I_search_criteria.debit_memo_reversed_ind,
            I_search_criteria.item,
            I_search_criteria.item_desc,
            I_search_criteria.vpn;

   LOGGER.LOG_INFORMATION(L_program||' delete im_doc_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FILTER_DOC_INVC_DETAIL;
--------------------------------------------------------------------------
FUNCTION FILTER_DOC_DETAIL(O_error_message     IN OUT VARCHAR2,
                           I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC,
                           I_client            IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.FILTER_DOC_DETAIL';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   if I_search_criteria.detail_exist_ind = 'N' then

      delete from im_doc_search_gtt gtt
       where exists (select 'x'
                       from im_invoice_detail id
                      where gtt.doc_id = id.doc_id
                     union all
                     select 'x'
                       from im_doc_detail_reason_codes rc
                      where gtt.doc_id = rc.doc_id);

      LOGGER.LOG_INFORMATION(L_program||' filter detail_exist_ind - %ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if (I_search_criteria.supplier_group_id is not null and I_search_criteria.supplier_group_id.count >= 1) then

      delete from im_doc_search_gtt gtt
       where not exists (select 'x'
                           from im_supplier_group_members sg,
                                table(cast(I_search_criteria.supplier_group_id as OBJ_NUMERIC_ID_TABLE)) in_sup_group
                          where sg.group_id    = value(in_sup_group)
                            and sg.supplier    = gtt.vendor);

      LOGGER.LOG_INFORMATION(L_program||' filter supplier_group_id - %ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if I_search_criteria.receipts_exist_ind is not null then

      delete from gtt_num_num_str_str_date_date;
      --
      if I_client = 'REIM_SEARCH_SQL.MANUAL_SEARCH' then
         insert into gtt_num_num_str_str_date_date(varchar2_1) values ('U');
      else
         insert into gtt_num_num_str_str_date_date(varchar2_1) values ('U');
         insert into gtt_num_num_str_str_date_date(varchar2_1) values ('M');
         insert into gtt_num_num_str_str_date_date(varchar2_1) values ('C');
      end if;

   end if;

   if I_search_criteria.receipts_exist_ind = 'Y' then

      delete from im_doc_search_gtt gtt
       where gtt.match_key in('SG', 'SGL')
         and not exists (select 'x'
                           from im_supplier_group_members sgm,
                                sups s,
                                ordhead oh,
                                shipment sh
                          where gtt.supplier_group_id            = sgm.group_id
                            and sgm.supplier                     = s.supplier_parent
                            and oh.supplier                      = s.supplier
                            and oh.order_no                      = sh.order_no
                            and sh.bill_to_loc                   = decode(gtt.match_key,'SGL', gtt.location, sh.bill_to_loc)
                            and sh.status_code                   in('U','R','C')
                            and nvl(sh.invc_match_status,'U')    in(select varchar2_1 from gtt_num_num_str_str_date_date));

      LOGGER.LOG_INFORMATION(L_program||' filter receipts_exist_ind Y SG/SGL - %ROWCOUNT: ' || SQL%ROWCOUNT);

      delete from im_doc_search_gtt gtt
       where gtt.match_key in('S', 'SL')
         and not exists (select 'x'
                           from sups s,
                                ordhead oh,
                                shipment sh
                          where s.supplier_parent                = gtt.vendor
                            and oh.supplier                      = s.supplier
                            and oh.order_no                      = sh.order_no
                            and sh.bill_to_loc                   = decode(gtt.match_key, 'SL',  gtt.location, sh.bill_to_loc)
                            and sh.status_code                   in('U','R','C')
                            and nvl(sh.invc_match_status,'U')    in(select varchar2_1 from gtt_num_num_str_str_date_date));

      LOGGER.LOG_INFORMATION(L_program||' filter receipts_exist_ind Y S/SL - %ROWCOUNT: ' || SQL%ROWCOUNT);

      delete from im_doc_search_gtt gtt
       where gtt.match_key in('SS', 'SSL')
         and not exists (select 'x'
                           from ordhead oh,
                                shipment sh
                          where oh.supplier                      = gtt.supplier_site_id
                            and oh.order_no                      = sh.order_no
                            and sh.bill_to_loc                   = decode(gtt.match_key, 'SSL', gtt.location, sh.bill_to_loc)
                            and sh.status_code                   in('U','R','C')
                            and nvl(sh.invc_match_status,'U')    in(select varchar2_1 from gtt_num_num_str_str_date_date));

      LOGGER.LOG_INFORMATION(L_program||' filter receipts_exist_ind Y SS/SSL - %ROWCOUNT: ' || SQL%ROWCOUNT);

      delete from im_doc_search_gtt gtt
       where gtt.match_key in('PO', 'POL')
         and not exists (select 'x'
                           from shipment sh
                          where sh.order_no                      = gtt.order_no
                            and sh.bill_to_loc                   = decode(gtt.match_key,'POL', gtt.location, sh.bill_to_loc)
                            and sh.status_code                   in('U','R','C')
                            and nvl(sh.invc_match_status,'U')    in(select varchar2_1 from gtt_num_num_str_str_date_date));

      LOGGER.LOG_INFORMATION(L_program||' filter receipts_exist_ind Y PO/POL - %ROWCOUNT: ' || SQL%ROWCOUNT);


   elsif I_search_criteria.receipts_exist_ind = 'N' then

      delete from im_doc_search_gtt gtt
       where gtt.match_key in('SG', 'SGL')
         and exists (select 'x'
                       from im_supplier_group_members sgm,
                            sups s,
                            ordhead oh,
                            shipment sh
                      where gtt.supplier_group_id            = sgm.group_id
                        and sgm.supplier                     = s.supplier_parent
                        and oh.supplier                      = s.supplier
                        and oh.order_no                      = sh.order_no
                        and sh.bill_to_loc                   = decode(gtt.match_key,'SGL', gtt.location, sh.bill_to_loc)
                        and sh.status_code                   in('U','R','C')
                        and nvl(sh.invc_match_status,'U')    in(select varchar2_1 from gtt_num_num_str_str_date_date));

      LOGGER.LOG_INFORMATION(L_program||' filter receipts_exist_ind N SG/SGL - %ROWCOUNT: ' || SQL%ROWCOUNT);

      delete from im_doc_search_gtt gtt
       where gtt.match_key in('S', 'SL')
         and exists (select 'x'
                       from sups s,
                            ordhead oh,
                            shipment sh
                      where s.supplier_parent                = gtt.vendor
                        and oh.supplier                      = s.supplier
                        and oh.order_no                      = sh.order_no
                        and sh.bill_to_loc                   = decode(gtt.match_key, 'SL',  gtt.location, sh.bill_to_loc)
                        and sh.status_code                   in('U','R','C')
                        and nvl(sh.invc_match_status,'U')    in(select varchar2_1 from gtt_num_num_str_str_date_date));

      LOGGER.LOG_INFORMATION(L_program||' filter receipts_exist_ind N S/SL - %ROWCOUNT: ' || SQL%ROWCOUNT);

      delete from im_doc_search_gtt gtt
       where gtt.match_key in('SS', 'SSL')
         and exists (select 'x'
                       from ordhead oh,
                            shipment sh
                      where oh.supplier                      = gtt.supplier_site_id
                        and oh.order_no                      = sh.order_no
                        and sh.bill_to_loc                   = decode(gtt.match_key, 'SSL', gtt.location, sh.bill_to_loc)
                        and sh.status_code                   in('U','R','C')
                        and nvl(sh.invc_match_status,'U')    in(select varchar2_1 from gtt_num_num_str_str_date_date));

      LOGGER.LOG_INFORMATION(L_program||' filter receipts_exist_ind N SS/SSL - %ROWCOUNT: ' || SQL%ROWCOUNT);

      delete from im_doc_search_gtt gtt
       where gtt.match_key in('PO', 'POL')
         and exists (select 'x'
                       from shipment sh
                      where sh.order_no                      = gtt.order_no
                        and sh.bill_to_loc                   = decode(gtt.match_key,'POL', gtt.location, sh.bill_to_loc)
                        and sh.status_code                   in('U','R','C')
                        and nvl(sh.invc_match_status,'U')    in(select varchar2_1 from gtt_num_num_str_str_date_date));

      LOGGER.LOG_INFORMATION(L_program||' filter receipts_exist_ind N PO/POL - %ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if (I_search_criteria.freight_payment_terms is not null and I_search_criteria.freight_payment_terms.count >= 1) then

      delete from im_doc_search_gtt gtt
       where not exists
         (select 'x'
            from table(cast(I_search_criteria.freight_payment_terms as OBJ_VARCHAR_ID_TABLE)) in_freight_term,
                 ordhead oh
           where oh.order_no      = gtt.order_no
             and oh.freight_terms = value(in_freight_term));

      LOGGER.LOG_INFORMATION(L_program||' filter freight_payment_terms - %ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if (I_search_criteria.receipt is not null and I_search_criteria.receipt.count >= 1) then

      delete from im_doc_search_gtt gtt
       where gtt.doc_id not in(
          select i.doc_id
            from im_detail_match_invc_history i
           where i.match_id in(select r.match_id
                                 from im_detail_match_rcpt_history r,
                                      table(cast(I_search_criteria.receipt as OBJ_NUMERIC_ID_TABLE)) in_receipt
                                where r.shipment = value(in_receipt))
          union all
          select i.doc_id
            from im_summary_match_invc_history i
           where i.match_id in(select r.match_id
                                 from im_summary_match_rcpt_history r,
                                      table(cast(I_search_criteria.receipt as OBJ_NUMERIC_ID_TABLE)) in_receipt
                                where r.shipment = value(in_receipt))
       );

      LOGGER.LOG_INFORMATION(L_program||' filter receipt - %ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FILTER_DOC_DETAIL;
--------------------------------------------------------------------------
FUNCTION COMMON_RCPT_SEARCH(O_error_message     IN OUT VARCHAR2,
                            I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.COMMON_RCPT_SEARCH';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

   L_insert VARCHAR2(2000)  :=
      q'[insert into im_rcpt_search_gtt (shipment,
                                         order_no,
                                         asn,
                                         status_code,
                                         invc_match_status,
                                         location,
                                         location_type,
                                         location_name,
                                         bill_to_loc,
                                         bill_to_loc_type,
                                         bill_to_loc_name,
                                         loc_vat_region,
                                         ship_date,
                                         receive_date,
                                         supplier,
                                         supplier_name,
                                         supplier_site_id,
                                         supplier_site_name,
                                         supplier_site_vat_region,
                                         currency_code,
                                         total_cost,
                                         total_qty,
                                         choice_flag) ]';

   L_query VARCHAR2(2000)   :=
      q'[select distinct sh.shipment,
                         oh.order_no,
                         sh.asn,
                         sh.status_code,
                         NVL(sh.invc_match_status,'U'),
                         sh.to_loc,
                         sh.to_loc_type,
                         vl.loc_name,
                         sh.bill_to_loc,
                         sh.bill_to_loc_type,
                         vl2.loc_name,
                         vl2.vat_region,
                         sh.ship_date,
                         sh.receive_date,
                         vn2.vendor,
                         vn2.vendor_name,
                         oh.supplier,
                         vn.vendor_name,
                         vn.vat_region,
                         oh.currency_code,
                         null total_cost,
                         null total_qty,
                         'N' choice_flag ]';

   L_from VARCHAR2(5000)  :=
      q'[ from ordhead oh, shipment sh,
               (select s.store loc,
                       s.store_name loc_name
                  from v_store s
                union all
                select w.wh loc,
                       w.wh_name loc_name
                  from v_wh w
               ) vl,
               (select s.store loc,
                       s.store_name loc_name,
                       s.vat_region
                  from store s
                union all
                select w.wh loc,
                       w.wh_name loc_name,
                       w.vat_region
                  from wh w
               ) vl2,
               (select to_char(s.supplier) vendor, s.sup_name vendor_name,
                       'SUPP' vendor_type, s.supplier_parent,
                       s.vat_region
                  from sups s
               ) vn,
               (select to_char(s2.supplier) vendor, s2.sup_name vendor_name,
                       'SUPP' vendor_type, s2.supplier_parent
                  from sups s2
               ) vn2
        ]';

   L_where VARCHAR2(5000)  := null;

BEGIN

   delete from im_rcpt_search_gtt;

   if GET_LOCS(O_error_message,
               I_search_criteria,
               I_search_criteria.loc) = FALSE then
      return FALSE;
   end if;

   if SETUP_RCPT_FROM(O_error_message,
                      L_from,
                      I_search_criteria) = FALSE then
      return FALSE;
   end if;

   if SETUP_RCPT_WHERE(O_error_message,
                       L_where,
                       I_search_criteria) = FALSE then
      return FALSE;
   end if;

   EXECUTE IMMEDIATE L_insert||LP_rcpt_with||L_query||L_from||L_where
      using I_search_criteria.invc_exist_ind,
            I_search_criteria.receipts_exist_ind,
            I_search_criteria.receipt_cost_and_qty_ind,
            I_search_criteria.start_receipt_date,
            I_search_criteria.end_receipt_date,
            I_search_criteria.from_reciept_cost,
            I_search_criteria.to_receipt_cost,
            I_search_criteria.loc_type,
            I_search_criteria.manual_group_id,
            --
            I_search_criteria.supplier_group_id,
            I_search_criteria.vendor,
            I_search_criteria.vendor_name,
            I_search_criteria.supplier_site_id,
            I_search_criteria.supplier_site_name,
            I_search_criteria.loc,
            I_search_criteria.loc_name,
            I_search_criteria.order_no,
            I_search_criteria.currency,
            I_search_criteria.receipt,
            I_search_criteria.shipment_invc_match_status;

   LOGGER.LOG_INFORMATION(L_program||' Insert im_rcpt_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if FILTER_RCPT_DETAIL(O_error_message,
                         I_search_criteria) = FALSE then
      return FALSE;
   end if;

   if GET_RCPT_SET_OF_BOOKS(O_error_message) = FALSE then
      return FALSE;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COMMON_RCPT_SEARCH;
--------------------------------------------------------------------------
FUNCTION SETUP_RCPT_FROM(O_error_message     IN OUT VARCHAR2,
                         IO_from             IN OUT VARCHAR2,
                         I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.SETUP_RCPT_FROM';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   IO_from := IO_from || ',sc_scalars';

   if (I_search_criteria.manual_group_id is not null) then
      IO_from := IO_from ||',im_manual_group_receipts mgr';
   end if;
   if (I_search_criteria.vendor is not null and I_search_criteria.vendor.count >= 1) then
      IO_from := IO_from ||',sc_vendors';
   end if;
   if (I_search_criteria.vendor_name is not null and I_search_criteria.vendor_name.count >= 1) then
      IO_from := IO_from ||',sc_vendor_names';
   end if;
   if (I_search_criteria.supplier_site_id is not null and I_search_criteria.supplier_site_id.count >= 1) then
      IO_from := IO_from ||',sc_supplier_sites';
   end if;
   if (I_search_criteria.supplier_site_name is not null and I_search_criteria.supplier_site_name.count >= 1) then
      IO_from := IO_from ||',sc_sup_site_names';
   end if;
   if (I_search_criteria.order_no is not null and I_search_criteria.order_no.count >= 1) then
      IO_from := IO_from ||',sc_order_nos';
   end if;
   if (I_search_criteria.currency is not null and I_search_criteria.currency.count >= 1) then
      IO_from := IO_from ||',sc_currencies';
   end if;
   if (I_search_criteria.receipt is not null and I_search_criteria.receipt.count >= 1) then
      IO_from := IO_from ||',sc_shipments';
   end if;
   if (I_search_criteria.loc is not null and I_search_criteria.loc.count >= 1) then
      IO_from := IO_from ||',sc_locs';
   end if;
   if (I_search_criteria.loc_name is not null and I_search_criteria.loc_name.count >= 1) then
      IO_from := IO_from ||',sc_loc_names';
   end if;
   if (I_search_criteria.shipment_invc_match_status is not null and I_search_criteria.shipment_invc_match_status.count >= 1) then
      IO_from := IO_from ||',sc_invc_match_statuses';
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SETUP_RCPT_FROM;
--------------------------------------------------------------------------
FUNCTION SETUP_RCPT_WHERE(O_error_message     IN OUT VARCHAR2,
                          IO_where            IN OUT VARCHAR2,
                          I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.SETUP_RCPT_WHERE';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;
   L_join_cnd           VARCHAR2(8)    := null;

BEGIN

   L_join_cnd := ' AND ';
   IO_where   := q'[ where oh.supplier        = vn.vendor
                       and vn.supplier_parent = vn2.vendor
                       and oh.order_no        = sh.order_no
                       and sh.to_loc          = vl.loc
                       and sh.bill_to_loc     = vl2.loc
                       --
                       and sh.status_code     IN ('U', /*REIM_CONSTANTS.SHIP_STATUS_UNMTCH*/
                                                  'R', /*REIM_CONSTANTS.SHIP_STATUS_RECEIVED*/
                                                  'C') /*REIM_CONSTANTS.SHIP_STATUS_CANCELLED*/
                       and sh.receive_date    is not null
                       and nvl(sh.invc_match_status,'U') = 'U' /*REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH*/
                   ]';

   if (I_search_criteria.manual_group_id is not null) then
      IO_where := IO_where||L_join_cnd||'mgr.group_id = sc_scalars.manual_group_id';
      IO_where := IO_where||L_join_cnd||'sh.shipment = mgr.shipment';
   end if;
   if (I_search_criteria.vendor is not null and I_search_criteria.vendor.count >= 1) then
      IO_where := IO_where||L_join_cnd||'vn2.vendor = sc_vendors.vendor';
   end if;
   if (I_search_criteria.vendor_name is not null and I_search_criteria.vendor_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vn2.vendor_name) like('%'||lower(nvl(sc_vendor_names.vendor_name,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.supplier_site_id is not null and I_search_criteria.supplier_site_id.count >= 1) then
      IO_where := IO_where||L_join_cnd||'oh.supplier = sc_supplier_sites.supplier_site';
   end if;
   if (I_search_criteria.supplier_site_name is not null and I_search_criteria.supplier_site_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vn.vendor_name) like('%'||lower(nvl(sc_sup_site_names.sup_site_name,'-90909'))||'%')]';
   end if;
   if (I_search_criteria.order_no is not null and I_search_criteria.order_no.count >= 1) then
      IO_where := IO_where||L_join_cnd||'oh.order_no = sc_order_nos.order_no';
   end if;
   if (I_search_criteria.currency is not null and I_search_criteria.currency.count >= 1) then
      IO_where := IO_where||L_join_cnd||'oh.currency_code = sc_currencies.currency';
   end if;
   if (I_search_criteria.receipt is not null and I_search_criteria.receipt.count >= 1) then
      IO_where := IO_where||L_join_cnd||'sh.shipment = sc_shipments.shipment';
   end if;
   if (I_search_criteria.loc is not null and I_search_criteria.loc.count >= 1) then
      IO_where := IO_where||L_join_cnd||'sh.to_loc = sc_locs.loc';
   end if;
   if (I_search_criteria.loc_name is not null and I_search_criteria.loc_name.count >= 1) then
      IO_where := IO_where||L_join_cnd||
                  q'[lower(vl.loc_name) like('%'||lower(nvl(sc_loc_names.loc_name,'-90909'))||'%')]';
   end if;
   if I_search_criteria.loc_type is not null then
      IO_where := IO_where||L_join_cnd||'sh.to_loc_type = sc_scalars.loc_type';
   end if;
   if I_search_criteria.start_receipt_date is not null or I_search_criteria.end_receipt_date is not null then
      IO_where := IO_where||L_join_cnd||'sh.receive_date between '
         ||'nvl(sc_scalars.start_receipt_date,sh.receive_date) and '
         ||'nvl(sc_scalars.end_receipt_date,sh.receive_date)';
   end if;
   if (I_search_criteria.shipment_invc_match_status is not null and I_search_criteria.shipment_invc_match_status.count >= 1) then
      IO_where := IO_where||L_join_cnd||'sh.invc_match_status = sc_invc_match_statuses.invc_match_status';
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SETUP_RCPT_WHERE;
--------------------------------------------------------------------------
FUNCTION FILTER_RCPT_DETAIL(O_error_message     IN OUT VARCHAR2,
                            I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.FILTER_RCPT_DETAIL';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

   L_delete VARCHAR2(2000)  := q'[delete from im_rcpt_search_gtt gtt where not exists ( ]';

   L_with varchar2(5000) := q'[with
      sc_items      as (select value(in_items) item
                      from table(cast(:1 as OBJ_VARCHAR_ID_TABLE)) in_items),
      sc_item_descs as (select value(in_item_descs) item_desc
                      from table(cast(:2 as OBJ_VARCHAR_DESC_TABLE)) in_item_descs),
      sc_vpns       as (select value(in_vpns) vpn
                      from table(cast(:3 as OBJ_VARCHAR_DESC_TABLE)) in_vpns)
   ]';

   L_select    varchar2(100)  := q'[ select 'x' ]';
   L_from      varchar2(100)  := q'[ from shipsku ss ]';
   L_from_add  varchar2(5000) := null;

   L_join_cnd  varchar2(8)    := ' and ';
   L_where     varchar2(100)  := q'[ where gtt.shipment = ss.shipment ]';
   L_where_add varchar2(5000) := null;

BEGIN

   if (I_search_criteria.supplier_group_id is not null and I_search_criteria.supplier_group_id.count >= 1) then

      delete from im_rcpt_search_gtt gtt
       where not exists (select 'x'
                           from im_supplier_group_members sg,
                                table(cast(I_search_criteria.supplier_group_id as OBJ_NUMERIC_ID_TABLE)) in_sup_group
                          where sg.group_id = value(in_sup_group)
                            and sg.supplier = gtt.supplier);

      LOGGER.LOG_INFORMATION(L_program||' filter supplier_group_id - %ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   --setup from
   if (I_search_criteria.item is not null and I_search_criteria.item.count >= 1) then
      L_from_add := L_from_add ||q'[,sc_items]';
   end if;
   if (I_search_criteria.item_desc is not null and I_search_criteria.item_desc.count >= 1) then
      L_from_add := L_from_add ||q'[,sc_item_descs]';
      L_from_add := L_from_add ||q'[,item_master im]';
   end if;
   if (I_search_criteria.vpn is not null and I_search_criteria.vpn.count >= 1) then
      L_from_add := L_from_add ||q'[,sc_vpns]';
      L_from_add := L_from_add ||q'[,item_supplier isup]';
   end if;

   --setup where
   if (I_search_criteria.item is not null and I_search_criteria.item.count >= 1) then
      L_where_add := L_where_add||L_join_cnd||q'[ss.item = sc_items.item]';
   end if;
   if (I_search_criteria.item_desc is not null and I_search_criteria.item_desc.count >= 1) then
      L_where_add := L_where_add||L_join_cnd||q'[ss.item = im.item]';
      L_where_add := L_where_add||L_join_cnd||q'[lower(im.item_desc) like('%'||lower(sc_item_descs.item_desc)||'%')]';
   end if;
   if (I_search_criteria.vpn is not null and I_search_criteria.vpn.count >= 1) then
      L_where_add := L_where_add||L_join_cnd||q'[isup.item = ss.item]';
      L_where_add := L_where_add||L_join_cnd||q'[lower(isup.vpn) like('%'||lower(sc_vpns.vpn)||'%')]';
   end if;

   --if nothing added to where return with out running the delete
   if L_where_add is null then
      return TRUE;
   end if;

   EXECUTE IMMEDIATE L_delete||L_with||L_select||L_from||L_from_add||L_where||L_where_add||q'[)]'
      using I_search_criteria.item,
            I_search_criteria.item_desc,
            I_search_criteria.vpn;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FILTER_RCPT_DETAIL;
--------------------------------------------------------------------------
FUNCTION GET_RCPT_SET_OF_BOOKS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program    VARCHAR2(61)   := 'REIM_SEARCH_SQL.GET_RCPT_SET_OF_BOOKS';
   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   merge into im_rcpt_search_gtt tgt
   using (select mlso.set_of_books_id,
                 gtto.shipment
            from mv_loc_sob mlso,
                 (select ol.order_no,
                         Min(ol.location) location,
                         igtt.shipment
                    from im_rcpt_search_gtt igtt,
                         ordloc ol
                   where igtt.order_no = ol.order_no
                   GROUP BY ol.order_no,
                            igtt.shipment) gtto
           where mlso.location      = gtto.location
             and mlso.location_type IN ('W','S')) src
   on (tgt.shipment = src.shipment)
   when MATCHED then
      update
         set tgt.loc_set_of_books_id = src.set_of_books_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge SOB info im_rcpt_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_RCPT_SET_OF_BOOKS;
--------------------------------------------------------------------------
FUNCTION WRITE_DOC_RESULTS(O_error_message     IN OUT VARCHAR2,
                           O_max_rows_exceeded    OUT NUMBER,
                           I_workspace_id      IN     IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.WRITE_DOC_RESULTS';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;
   L_row_count          NUMBER(4)      := 0;

BEGIN

   delete from im_doc_invc_search_ws
    where workspace_id = I_workspace_id;

   insert into im_doc_invc_search_ws (workspace_id,
                                      doc_id,
                                      ext_doc_id,
                                      doc_source,
                                      type,
                                      status,
                                      order_no,
                                      location,
                                      loc_type,
                                      loc_name,
                                      vendor_type,
                                      vendor,
                                      vendor_name,
                                      supplier_site_id,
                                      supplier_site_name,
                                      doc_date,
                                      due_date,
                                      freight_type,
                                      consignment_ind,
                                      currency_code,
                                      ap_reviewer,
                                      total_cost,
                                      total_qty,
                                      choice_flag,
                                      doc_head_version_id,
                                      hold_status,
                                      pre_paid_ind,
                                      manually_paid_ind,
                                      group_id)
   select I_workspace_id workspace_id,
          gtt.doc_id,
          gtt.ext_doc_id,
          gtt.doc_source,
          gtt.type,
          gtt.status,
          gtt.order_no,
          gtt.location,
          gtt.loc_type,
          gtt.loc_name,
          gtt.vendor_type,
          gtt.vendor,
          gtt.vendor_name,
          gtt.supplier_site_id,
          gtt.supplier_site_name,
          gtt.doc_date,
          gtt.due_date,
          gtt.freight_type,
          gtt.consignment_ind,
          gtt.currency_code,
          gtt.ap_reviewer,
          gtt.total_cost,
          gtt.total_qty,
          'N',
          gtt.doc_head_version_id,
          gtt.hold_status,
          gtt.pre_paid_ind,
          gtt.manually_paid_ind,
          gtt.group_id
     from im_doc_search_gtt gtt
    where rownum <= LP_doc_max_count;

   if SQL%RowCount = LP_doc_max_count then
      O_max_rows_exceeded := 1;
   else
      O_max_rows_exceeded := 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program||'- workspace_id'||'-'||I_workspace_id,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_DOC_RESULTS;
--------------------------------------------------------------------------
FUNCTION WRITE_MANUAL_RESULTS(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_MATCH_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                              I_search_criteria   IN     IM_DOC_SEARCH_CRITERIA_REC,
                              I_choice_flag       IN     IM_MATCH_INVC_SEARCH_WS.CHOICE_FLAG%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.WRITE_MANUAL_RESULTS';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

   L_shipments          OBJ_NUMERIC_ID_TABLE;

BEGIN

   delete from im_match_invc_search_ws where workspace_id = I_workspace_id;
   delete from im_match_rcpt_search_ws where workspace_id = I_workspace_id;

   insert into im_match_invc_search_ws (workspace_id,
                                        doc_id,
                                        ext_doc_id,
                                        status,
                                        doc_date,
                                        due_date,
                                        order_no,
                                        location,
                                        location_name,
                                        loc_type,
                                        loc_vat_region,
                                        loc_set_of_books_id,
                                        supplier,
                                        supplier_name,
                                        supplier_phone,
                                        supplier_site_id,
                                        supplier_site_name,
                                        supplier_site_vat_region,
                                        currency_code,
                                        cost_pre_match,
                                        supplier_group_id,
                                        total_cost,
                                        total_qty,
                                        choice_flag,
                                        doc_head_version_id,
                                        match_key_id,
                                        pre_paid_ind,
                                        custom_doc_ref_1,
                                        custom_doc_ref_2,
                                        custom_doc_ref_3,
                                        custom_doc_ref_4)
   select I_workspace_id workspace_id,
          gtt.doc_id,
          gtt.ext_doc_id,
          gtt.status,
          gtt.doc_date,
          gtt.due_date,
          gtt.order_no,
          gtt.location,
          gtt.loc_name,
          gtt.loc_type,
          gtt.loc_vat_region,
          gtt.loc_set_of_books_id,
          gtt.vendor,
          gtt.vendor_name,
          gtt.vendor_phone,
          gtt.supplier_site_id,
          gtt.supplier_site_name,
          gtt.supplier_site_vat_region,
          gtt.currency_code,
          gtt.cost_pre_match,
          gtt.supplier_group_id,
          gtt.total_cost,
          gtt.total_qty,
          I_choice_flag choice_flag,
          gtt.doc_head_version_id,
          gtt.match_key_id,
          gtt.pre_paid_ind,
          gtt.custom_doc_ref_1,
          gtt.custom_doc_ref_2,
          gtt.custom_doc_ref_3,
          gtt.custom_doc_ref_4
     from im_doc_search_gtt gtt;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_MATCH_INVC_SEARCH_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select shipment bulk collect into L_shipments from im_rcpt_search_gtt;

   if REIM_XFORM_SHIPSKU_SQL.POP_TRANSFORM_SHIPSKU_GTT(O_error_message,
                                                       L_shipments) = REIM_CONSTANTS.FAIL then
      return FALSE;
   end if;

   insert into im_match_rcpt_search_ws (workspace_id,
                                        shipment,
                                        order_no,
                                        asn,
                                        status_code,
                                        invc_match_status,
                                        location,
                                        location_name,
                                        loc_type,
                                        bill_to_loc,
                                        bill_to_loc_name,
                                        bill_to_loc_type,
                                        loc_vat_region,
                                        loc_set_of_books_id,
                                        ship_date,
                                        receive_date,
                                        supplier,
                                        supplier_name,
                                        supplier_site_id,
                                        supplier_site_name,
                                        supplier_site_vat_region,
                                        currency_code,
                                        total_avail_cost,
                                        total_avail_qty,
                                        merch_amount,
                                        total_qty,
                                        choice_flag,
                                        match_key_id)
   select I_workspace_id workspace_id,
          gtt.shipment,
          gtt.order_no,
          gtt.asn,
          gtt.status_code,
          gtt.invc_match_status,
          gtt.location,
          gtt.location_name,
          gtt.location_type,
          gtt.bill_to_loc,
          gtt.bill_to_loc_name,
          gtt.bill_to_loc_type,
          gtt.loc_vat_region,
          gtt.loc_set_of_books_id,
          gtt.ship_date,
          gtt.receive_date,
          gtt.supplier,
          gtt.supplier_name,
          gtt.supplier_site_id,
          gtt.supplier_site_name,
          gtt.supplier_site_vat_region,
          gtt.currency_code,
          sum((ss.transform_qty_received - nvl(pm.qty_matched,0)) * (ss.transform_unit_cost)) total_avail_cost,
          sum(ss.transform_qty_received) - sum(nvl(pm.qty_matched,0)) total_avail_qty,
          sum(ss.transform_qty_received * ss.transform_unit_cost) merch_amount,
          sum(ss.transform_qty_received) total_qty,
          I_choice_flag choice_flag,
          gtt.match_key_id
     from im_rcpt_search_gtt gtt,
          im_transform_shipsku_gtt ss,
          im_partially_matched_receipts pm
    where gtt.shipment = ss.shipment
      and gtt.shipment = pm.shipment(+)
      and ss.item      = pm.item(+)
    group by gtt.shipment,
             gtt.order_no,
             gtt.asn,
             gtt.status_code,
             gtt.invc_match_status,
             gtt.location,
             gtt.location_name,
             gtt.location_type,
             gtt.bill_to_loc,
             gtt.bill_to_loc_name,
             gtt.bill_to_loc_type,
             gtt.loc_vat_region,
             gtt.loc_set_of_books_id,
             gtt.ship_date,
             gtt.receive_date,
             gtt.supplier,
             gtt.supplier_name,
             gtt.supplier_site_id,
             gtt.supplier_site_name,
             gtt.supplier_site_vat_region,
             gtt.currency_code,
             gtt.match_key_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert IM_MATCH_RCPT_SEARCH_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_search_criteria.from_reciept_cost is not null or I_search_criteria.to_receipt_cost is not null then
      delete from im_match_rcpt_search_ws ws
       where ws.total_avail_cost not between nvl(I_search_criteria.from_reciept_cost, ws.total_avail_cost)
                                         and nvl(I_search_criteria.to_receipt_cost, ws.total_avail_cost);

      LOGGER.LOG_INFORMATION(L_program||' Filter Rcpt Cost im_match_rcpt_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if I_search_criteria.invc_exist_ind = 'Y' then

      delete from im_match_rcpt_search_ws ws
       where ws.workspace_id = I_workspace_id
         and ws.shipment not in(
          with grp as (select gtt.shipment,
                              sh.order_no,
                              sh.bill_to_loc,
                              s1.supplier,
                              s2.supplier supplier_parent,
                              so.match_key,
                              so.group_id
                         from im_rcpt_search_gtt gtt,
                              shipment sh,
                              ordhead oh,
                              sups s1,
                              sups s2,
                              v_im_supp_site_attrib_expl so
                        where gtt.shipment       = sh.shipment
                          and sh.order_no        = oh.order_no
                          and oh.supplier        = s1.supplier
                          and s1.supplier_parent = s2.supplier_parent
                          and oh.supplier        = so.supplier)
          select distinct grp.shipment
            from grp,
                 im_doc_head dh
           where grp.match_key in('PO','POL')
             and grp.order_no  = dh.order_no
             and dh.location   = decode(grp.match_key,'POL', grp.bill_to_loc, dh.location)
          union all
          select distinct grp.shipment
            from grp,
                 im_doc_head dh
           where grp.match_key in('SS','SSL')
             and grp.supplier  = dh.supplier_site_id
             and dh.location   = decode(grp.match_key,'SSL', grp.bill_to_loc, dh.location)
          union all
          select distinct grp.shipment
            from grp,
                 im_doc_head dh
           where grp.match_key       in('S','SL')
             and grp.supplier_parent = dh.vendor
             and dh.type             = 'MRCHI'
             and dh.location         = decode(grp.match_key,'SL', grp.bill_to_loc, dh.location)
          union all
          select distinct grp.shipment
            from grp,
                 im_supplier_group_members sgm,
                 im_doc_head dh
           where grp.match_key in('SG','SGL')
             and grp.group_id  = sgm.group_id
             and sgm.supplier  = dh.vendor
             and dh.type       = 'MRCHI'
             and dh.location   = decode(grp.match_key,'SGL', grp.bill_to_loc, dh.location));

      LOGGER.LOG_INFORMATION(L_program||' Filter invc_exist_ind Y im_match_rcpt_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   elsif I_search_criteria.invc_exist_ind = 'N' then

      delete from im_match_rcpt_search_ws ws
       where ws.workspace_id = I_workspace_id
         and ws.shipment in(
          with grp as (select gtt.shipment,
                              sh.order_no,
                              sh.bill_to_loc,
                              s1.supplier,
                              s2.supplier supplier_parent,
                              so.match_key,
                              so.group_id
                         from im_rcpt_search_gtt gtt,
                              shipment sh,
                              ordhead oh,
                              sups s1,
                              sups s2,
                              v_im_supp_site_attrib_expl so
                        where gtt.shipment       = sh.shipment
                          and sh.order_no        = oh.order_no
                          and oh.supplier        = s1.supplier
                          and s1.supplier_parent = s2.supplier_parent
                          and oh.supplier        = so.supplier)
          select distinct grp.shipment
            from grp,
                 im_doc_head dh
           where grp.match_key in('PO','POL')
             and grp.order_no  = dh.order_no
             and dh.location   = decode(grp.match_key,'POL', grp.bill_to_loc, dh.location)
          union all
          select distinct grp.shipment
            from grp,
                 im_doc_head dh
           where grp.match_key in('SS','SSL')
             and grp.supplier  = dh.supplier_site_id
             and dh.location   = decode(grp.match_key,'SSL', grp.bill_to_loc, dh.location)
          union all
          select distinct grp.shipment
            from grp,
                 im_doc_head dh
           where grp.match_key       in('S','SL')
             and grp.supplier_parent = dh.vendor
             and dh.type             = 'MRCHI'
             and dh.location         = decode(grp.match_key,'SL', grp.bill_to_loc, dh.location)
          union all
          select distinct grp.shipment
            from grp,
                 im_supplier_group_members sgm,
                 im_doc_head dh
           where grp.match_key in('SG','SGL')
             and grp.group_id  = sgm.group_id
             and sgm.supplier  = dh.vendor
             and dh.type       = 'MRCHI'
             and dh.location   = decode(grp.match_key,'SGL', grp.bill_to_loc, dh.location));

      LOGGER.LOG_INFORMATION(L_program||' Filter invc_exist_ind N im_match_rcpt_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program||'- workspace_id'||'-'||I_workspace_id,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_MANUAL_RESULTS;
--------------------------------------------------------------------------
FUNCTION WRITE_MATCH_INQ_RESULTS(O_error_message     IN OUT VARCHAR2,
                                 O_max_rows_exceeded    OUT NUMBER,
                                 I_workspace_id      IN     IM_MATCH_INQ_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.WRITE_MATCH_INQ_RESULTS';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete from im_match_inq_search_ws where workspace_id = I_workspace_id;

   insert into im_match_inq_search_ws (workspace_id,
                                       doc_id,
                                       ext_doc_id,
                                       status,
                                       doc_date,
                                       due_date,
                                       order_no,
                                       supplier,
                                       supplier_name,
                                       supplier_site_id,
                                       supplier_site_name,
                                       location,
                                       location_name,
                                       loc_type,
                                       currency_code,
                                       total_cost,
                                       total_qty,
                                       ap_reviewer,
                                       match_id,
                                       match_date,
                                       summary_match_ind,
                                       detail_match_ind)
   select I_workspace_id workspace_id,
          gtt.doc_id,
          gtt.ext_doc_id,
          gtt.status,
          gtt.doc_date,
          gtt.due_date,
          gtt.order_no,
          gtt.vendor,
          gtt.vendor_name,
          gtt.supplier_site_id,
          gtt.supplier_site_name,
          gtt.location,
          gtt.loc_name,
          gtt.loc_type,
          gtt.currency_code,
          gtt.total_cost,
          gtt.total_qty,
          gtt.ap_reviewer,
          gtt.match_id,
          gtt.match_date,
          'N',
          'N'
     from im_doc_search_gtt gtt
    where rownum <= LP_doc_max_count;

   if SQL%RowCount = LP_doc_max_count then
      O_max_rows_exceeded := 1;
   else
      O_max_rows_exceeded := 0;
   end if;

   merge into im_match_inq_search_ws target
   using (select distinct I_workspace_id workspace_id,
                          gtt.doc_id
            from im_doc_search_gtt gtt,
                 im_summary_match_invc_history ih
           where gtt.doc_id = ih.doc_id) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.doc_id       = use_this.doc_id)
   when matched then update
      set target.summary_match_ind = 'Y';

   merge into im_match_inq_search_ws target
   using (select distinct I_workspace_id workspace_id,
                          gtt.doc_id
            from im_doc_search_gtt gtt,
                 im_detail_match_invc_history ih
           where gtt.doc_id = ih.doc_id) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.doc_id       = use_this.doc_id)
   when matched then update
      set target.detail_match_ind = 'Y';

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program||'- workspace_id'||'-'||I_workspace_id,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_MATCH_INQ_RESULTS;
--------------------------------------------------------------------------
FUNCTION WRITE_CNR_RESULTS(O_error_message     IN OUT VARCHAR2,
                           I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.WRITE_CNR_RESULTS';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete from im_cnr_search_ws where workspace_id = I_workspace_id;

   insert into im_cnr_search_ws (workspace_id,
                                 doc_id,
                                 ext_doc_id,
                                 type,
                                 status,
                                 order_no,
                                 rtv_order_no,
                                 location,
                                 loc_type,
                                 loc_name,
                                 loc_vat_region,
                                 loc_set_of_books_id,
                                 vendor_type,
                                 vendor,
                                 vendor_name,
                                 supplier_site_id,
                                 supplier_site_name,
                                 supplier_site_vat_region,
                                 doc_date,
                                 currency_code,
                                 total_cost,
                                 total_qty,
                                 doc_head_version_id,
                                 choice_flag)
   select I_workspace_id,
          gtt.doc_id,
          gtt.ext_doc_id,
          gtt.type,
          gtt.status,
          gtt.order_no,
          gtt.rtv_order_no,
          gtt.location,
          gtt.loc_type,
          gtt.loc_name,
          gtt.loc_vat_region,
          gtt.loc_set_of_books_id,
          gtt.vendor_type,
          gtt.vendor,
          gtt.vendor_name,
          gtt.supplier_site_id,
          gtt.supplier_site_name,
          gtt.supplier_site_vat_region,
          gtt.doc_date,
          gtt.currency_code,
          gtt.total_cost,
          gtt.total_qty,
          gtt.doc_head_version_id,
          'N' choice_flag
     from im_doc_search_gtt gtt
    where gtt.type in (REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                       REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                       REIM_CONSTANTS.DOC_TYPE_CRDNRT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program||'- workspace_id'||'-'||I_workspace_id,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_CNR_RESULTS;
--------------------------------------------------------------------------
FUNCTION WRITE_CN_RESULTS(O_error_message     IN OUT VARCHAR2,
                          I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.WRITE_CN_RESULTS';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete from im_cn_search_ws  where workspace_id = I_workspace_id;

   insert into im_cn_search_ws (workspace_id,
                                doc_id,
                                ext_doc_id,
                                type,
                                status,
                                order_no,
                                rtv_order_no,
                                location,
                                loc_type,
                                loc_name,
                                loc_vat_region,
                                loc_set_of_books_id,
                                vendor_type,
                                vendor,
                                vendor_name,
                                supplier_site_id,
                                supplier_site_name,
                                supplier_site_vat_region,
                                doc_date,
                                currency_code,
                                total_cost,
                                total_qty,
                                doc_head_version_id,
                                choice_flag)
   select I_workspace_id,
          gtt.doc_id,
          gtt.ext_doc_id,
          gtt.type,
          gtt.status,
          gtt.order_no,
          gtt.rtv_order_no,
          gtt.location,
          gtt.loc_type,
          gtt.loc_name,
          gtt.loc_vat_region,
          gtt.loc_set_of_books_id,
          gtt.vendor_type,
          gtt.vendor,
          gtt.vendor_name,
          gtt.supplier_site_id,
          gtt.supplier_site_name,
          gtt.supplier_site_vat_region,
          gtt.doc_date,
          gtt.currency_code,
          gtt.total_cost,
          gtt.total_qty,
          gtt.doc_head_version_id,
          'N' choice_flag
     from im_doc_search_gtt gtt
    where gtt.type = REIM_CONSTANTS.DOC_TYPE_CRDNT;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program||'- workspace_id'||'-'||I_workspace_id,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_CN_RESULTS;
--------------------------------------------------------------------------
FUNCTION WRITE_TAX_REV_LIST_RESULTS(O_error_message     IN OUT VARCHAR2,
                                    O_max_rows_exceeded    OUT NUMBER,
                                    I_workspace_id      IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.WRITE_TAX_REV_LIST_RESULTS';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete from im_tax_review_list_search_ws where workspace_id = I_workspace_id;

   insert into im_tax_review_list_search_ws(workspace_id,
                                            doc_id,
                                            ext_doc_id,
                                            supplier_id,
                                            supplier_name,
                                            supplier_site_id,
                                            supplier_site_name,
                                            order_no,
                                            item,
                                            item_desc,
                                            vpn,
                                            location,
                                            location_name,
                                            loc_type,
                                            tax_code_discrepancy_ind,
                                            tax_rate_discrepancy_ind,
                                            doc_tax_code,
                                            doc_tax_rate,
                                            doc_tax_amount,
                                            verify_tax_code,
                                            verify_tax_rate,
                                            verify_tax_amount,
                                            doc_date,
                                            currency_code,
                                            choice_flag)
   select I_workspace_id,
          gtt.doc_id,
          gtt.ext_doc_id,
          gtt.vendor,
          gtt.vendor_name,
          gtt.supplier_site_id,
          gtt.supplier_site_name,
          gtt.order_no,
          td.item,
          im.item_desc,
          its.vpn,
          gtt.location,
          gtt.loc_name,
          gtt.loc_type,
          decode(td.tax_code, td.verify_tax_code, 'N', 'Y') tax_code_discrepancy_ind,
          decode(td.doc_tax_rate, td.verify_tax_rate, 'N', 'Y') tax_rate_discrepancy_ind,
          td.tax_code doc_tax_code,
          td.doc_tax_rate,
          td.doc_tax_amount,
          td.verify_tax_code,
          td.verify_tax_rate,
          td.verify_tax_amount,
          gtt.doc_date,
          gtt.currency_code,
          'N' choice_flag
     from im_doc_search_gtt gtt,
          im_tax_discrepancy td,
          item_master im,
          item_supplier its
    where td.doc_id        = gtt.doc_id
      and im.item          = td.item
      and its.item (+)     = im.item
      and its.supplier (+) = gtt.supplier_site_id
      and rownum           <= LP_doc_max_count;

   if SQL%RowCount = LP_doc_max_count then
      O_max_rows_exceeded := 1;
   else
      O_max_rows_exceeded := 0;
   end if;

   LOGGER.LOG_INFORMATION(L_program||' Insert im_tax_review_list_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program||'- workspace_id'||'-'||I_workspace_id,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_TAX_REV_LIST_RESULTS;
--------------------------------------------------------------------------
FUNCTION EXPAND_DOC_TO_GROUP(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.EXPAND_DOC_TO_GROUP';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   for x in 1..LP_expand_group_level loop

      merge into im_doc_search_gtt target
      using (select distinct dh.doc_id,
                    dh.ext_doc_id,
                    dh.type,
                    dh.status,
                    dh.order_no,
                    dh.location,
                    dh.loc_type,
                    vl.loc_name loc_name,
                    vl.vat_region loc_vat_region,
                    ws.loc_set_of_books_id,
                    dh.vendor_type,
                    dh.vendor,
                    vn.vendor_name,
                    vn.vendor_phone,
                    dh.supplier_site_id,
                    vn2.vendor_name supplier_site_name,
                    vn2.vat_region supplier_site_vat_region,
                    dh.doc_date,
                    dh.due_date,
                    dh.freight_type,
                    dh.consignment_ind,
                    dh.currency_code,
                    dh.cost_pre_match,
                    so.ap_reviewer,
                    dh.total_cost,
                    dh.total_qty,
                    dh.doc_source,
                    dh.object_version_id doc_head_version_id,
                    dh.match_id,
                    dh.match_date
               from im_doc_search_gtt ws,
                    im_manual_group_invoices ig1,
                    im_manual_group_invoices ig2,
                    im_manual_group_receipts rg1,
                    im_manual_group_receipts rg2,
                    im_doc_head dh,
                    (select s.store loc, s.store_name loc_name, s.vat_region
                       from store s
                     union all
                     select wh.wh loc, wh.wh_name loc_name, wh.vat_region
                       from wh) vl,
                    (select p.partner_id vendor, p.partner_desc vendor_name,
                            p.partner_type vendor_type, p.contact_phone vendor_phone
                       from partner p
                     union all
                     select to_char(s.supplier) vendor, s.sup_name vendor_name,
                            'SUPP' vendor_type, s.contact_phone vendor_phone
                       from sups s
                    ) vn,
                    (select to_char(s2.supplier) vendor, s2.sup_name vendor_name,
                            'SUPP' vendor_type, s2.contact_phone vendor_phone, s2.vat_region
                       from sups s2
                    ) vn2,
                    v_im_supp_site_attrib_expl so
              where ws.doc_id           = ig1.doc_id
                and ig1.group_id        = rg1.group_id
                and rg1.shipment        = rg2.shipment
                and rg2.group_id        = ig2.group_id
                and ig2.doc_id          = dh.doc_id
                and ig2.doc_id         != ws.doc_id
                --
                and dh.location         = vl.loc
                and dh.vendor           = vn.vendor
                and dh.vendor_type      = vn.vendor_type
                and dh.supplier_site_id = vn2.vendor(+)
                and vn2.vendor_type(+)  = 'SUPP'
                and so.supplier         = dh.supplier_site_id
      ) use_this
      --
      on (target.doc_id = use_this.doc_id)
      --
      when not matched then
      insert (doc_id,
              ext_doc_id,
              type,
              status,
              order_no,
              location,
              loc_type,
              loc_name,
              loc_vat_region,
              loc_set_of_books_id,
              vendor_type,
              vendor,
              vendor_name,
              vendor_phone,
              supplier_site_id,
              supplier_site_name,
              supplier_site_vat_region,
              doc_date,
              due_date,
              freight_type,
              consignment_ind,
              currency_code,
              cost_pre_match,
              ap_reviewer,
              total_cost,
              total_qty,
              doc_source,
              doc_head_version_id,
              match_id,
              match_date)
      values (use_this.doc_id,
              use_this.ext_doc_id,
              use_this.type,
              use_this.status,
              use_this.order_no,
              use_this.location,
              use_this.loc_type,
              use_this.loc_name,
              use_this.loc_vat_region,
              use_this.loc_set_of_books_id,
              use_this.vendor_type,
              use_this.vendor,
              use_this.vendor_name,
              use_this.vendor_phone,
              use_this.supplier_site_id,
              use_this.supplier_site_name,
              use_this.supplier_site_vat_region,
              use_this.doc_date,
              use_this.due_date,
              use_this.freight_type,
              use_this.consignment_ind,
              use_this.currency_code,
              use_this.cost_pre_match,
              use_this.ap_reviewer,
              use_this.total_cost,
              use_this.total_qty,
              use_this.doc_source,
              use_this.doc_head_version_id,
              use_this.match_id,
              use_this.match_date);

      LOGGER.LOG_INFORMATION(L_program||' Expand Doc im_doc_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end loop;

   if GET_SUP_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPAND_DOC_TO_GROUP;
--------------------------------------------------------------------------
FUNCTION EXPAND_CN_TO_GROUP(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.EXPAND_CN_TO_GROUP';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date (number_1, number_2)
   select distinct gcn2.doc_id,
          gtt.loc_set_of_books_id
     from im_cn_manual_groups gcn,
          im_cn_manual_groups gcn2,
          im_doc_search_gtt gtt
    where gtt.doc_id   = gcn.doc_id
      and gcn.group_id = gcn2.group_id
   minus
   select gtt.doc_id,
          gtt.loc_set_of_books_id
     from im_doc_search_gtt gtt;

   merge into im_doc_search_gtt target
   using (select dh.doc_id,
                 dh.ext_doc_id,
                 dh.type,
                 dh.status,
                 dh.order_no,
                 dh.location,
                 dh.loc_type,
                 vl.loc_name loc_name,
                 vl.vat_region loc_vat_region,
                 gtt.number_2 loc_set_of_books_id,
                 dh.vendor_type,
                 dh.vendor,
                 vn.vendor_name vendor_name,
                 dh.supplier_site_id,
                 vn2.vendor_name supplier_site_name,
                 vn2.vat_region supplier_site_vat_region,
                 dh.doc_date,
                 dh.due_date,
                 dh.freight_type,
                 dh.consignment_ind,
                 dh.currency_code,
                 so.ap_reviewer,
                 dh.total_cost,
                 dh.total_qty,
                 dh.doc_source,
                 dh.object_version_id doc_head_version_id,
                 dh.match_id,
                 dh.match_date
            from gtt_num_num_str_str_date_date gtt,
                 im_doc_head dh,
                 (select s.store loc, s.store_name loc_name, s.vat_region
                    from store s
                  union all
                  select wh.wh loc, wh.wh_name loc_name, wh.vat_region
                    from wh) vl,
                 (select p.partner_id vendor, p.partner_desc vendor_name,
                         p.partner_type vendor_type, p.contact_phone vendor_phone
                    from partner p
                  union all
                  select to_char(s.supplier) vendor, s.sup_name vendor_name,
                         'SUPP' vendor_type, s.contact_phone vendor_phone
                    from sups s
                 ) vn,
                 (select to_char(s2.supplier) vendor, s2.sup_name vendor_name,
                         'SUPP' vendor_type, s2.contact_phone vendor_phone,
                         s2.vat_region
                    from sups s2
                 ) vn2,
                 v_im_supp_site_attrib_expl so
           where gtt.number_1        = dh.doc_id
             and dh.location         = vl.loc
             and dh.vendor           = vn.vendor
             and dh.vendor_type      = vn.vendor_type
             and dh.supplier_site_id = vn2.vendor(+)
             and vn2.vendor_type(+)  = 'SUPP'
             and so.supplier         = dh.supplier_site_id
   ) use_this
   --
   on (target.doc_id = use_this.doc_id)
   --
   when not matched then
   insert (doc_id,
           ext_doc_id,
           type,
           status,
           order_no,
           location,
           loc_type,
           loc_name,
           loc_vat_region,
           loc_set_of_books_id,
           vendor_type,
           vendor,
           vendor_name,
           supplier_site_id,
           supplier_site_name,
           supplier_site_vat_region,
           doc_date,
           due_date,
           freight_type,
           consignment_ind,
           currency_code,
           ap_reviewer,
           total_cost,
           total_qty,
           doc_source,
           doc_head_version_id,
           match_id,
           match_date)
   values (use_this.doc_id,
           use_this.ext_doc_id,
           use_this.type,
           use_this.status,
           use_this.order_no,
           use_this.location,
           use_this.loc_type,
           use_this.loc_name,
           use_this.loc_vat_region,
           use_this.loc_set_of_books_id,
           use_this.vendor_type,
           use_this.vendor,
           use_this.vendor_name,
           use_this.supplier_site_id,
           use_this.supplier_site_name,
           use_this.supplier_site_vat_region,
           use_this.doc_date,
           use_this.due_date,
           use_this.freight_type,
           use_this.consignment_ind,
           use_this.currency_code,
           use_this.ap_reviewer,
           use_this.total_cost,
           use_this.total_qty,
           use_this.doc_source,
           use_this.doc_head_version_id,
           use_this.match_id,
           use_this.match_date);

   LOGGER.LOG_INFORMATION(L_program||' Expand credit note im_doc_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if GET_SUP_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPAND_CN_TO_GROUP;
--------------------------------------------------------------------------
FUNCTION EXPAND_RCPT_TO_GROUP(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.EXPAND_RCPT_TO_GROUP';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   for x in 1..LP_expand_group_level loop

      merge into im_rcpt_search_gtt target
      using (select sh.shipment,
                    sh.order_no,
                    sh.asn,
                    sh.status_code,
                    sh.invc_match_status,
                    sh.to_loc location,
                    vl.loc_name location_name,
                    sh.to_loc_type location_type,
                    --
                    sh.bill_to_loc,
                    vl2.loc_name bill_to_loc_name,
                    sh.bill_to_loc_type,
                    vl2.vat_region loc_vat_region,
                    ws.loc_set_of_books_id,
                    --
                    sh.receive_date,
                    s.supplier,
                    s.sup_name supplier_name,
                    oh.supplier supplier_site_id,
                    vn.vendor_name supplier_site_name,
                    vn.vat_region supplier_site_vat_region,
                    oh.currency_code,
                    null total_cost,
                    null total_qty,
                    'N' choice_flag
               from im_rcpt_search_gtt ws,
                    im_manual_group_receipts ig,
                    im_manual_group_receipts ig2,
                    shipment sh,
                    ordhead oh,
                    (select s.store loc, s.store_name loc_name
                       from store s
                     union all
                     select w.wh loc, w.wh_name loc_name
                       from wh w
                    ) vl,
                    (select s.store loc,
                            s.store_name loc_name,
                            s.vat_region
                       from store s
                     union all
                     select w.wh loc,
                            w.wh_name loc_name,
                            w.vat_region
                       from wh w
                    ) vl2,
                    (select to_char(s.supplier) vendor, s.sup_name vendor_name,
                            'SUPP' vendor_type, s.supplier_parent, s.vat_region
                       from sups s
                    ) vn,
                    sups s
              where ws.shipment        = ig.shipment
                and ig.group_id        = ig2.group_id
                and ig2.shipment       = sh.shipment
                and sh.order_no        = oh.order_no
                and sh.to_loc          = vl.loc
                and sh.bill_to_loc     = vl2.loc
                and oh.supplier        = vn.vendor
                and vn.supplier_parent = s.supplier
      ) use_this
      --
      on (target.shipment = use_this.shipment)
      --
      when not matched then
      insert (shipment,
              order_no,
              asn,
              status_code,
              invc_match_status,
              location,
              location_type,
              location_name,
              bill_to_loc,
              bill_to_loc_name,
              bill_to_loc_type,
              loc_vat_region,
              loc_set_of_books_id,
              receive_date,
              supplier,
              supplier_name,
              supplier_site_id,
              supplier_site_name,
              supplier_site_vat_region,
              currency_code,
              total_cost,
              total_qty,
              choice_flag)
      values (use_this.shipment,
              use_this.order_no,
              use_this.asn,
              use_this.status_code,
              use_this.invc_match_status,
              use_this.location,
              use_this.location_type,
              use_this.location_name,
              use_this.bill_to_loc,
              use_this.bill_to_loc_name,
              use_this.bill_to_loc_type,
              use_this.loc_vat_region,
              use_this.loc_set_of_books_id,
              use_this.receive_date,
              use_this.supplier,
              use_this.supplier_name,
              use_this.supplier_site_id,
              use_this.supplier_site_name,
              use_this.supplier_site_vat_region,
              use_this.currency_code,
              use_this.total_cost,
              use_this.total_qty,
              use_this.choice_flag);

   end loop;

   LOGGER.LOG_INFORMATION(L_program||' Expand Rcpt im_rcpt_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPAND_RCPT_TO_GROUP;
--------------------------------------------------------------------------
FUNCTION DOC_MATCHKEY_TO_DOC_MATCHKEY(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.DOC_MATCHKEY_TO_DOC_MATCHKEY';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   merge into im_doc_search_gtt target
   using (with mk as (select distinct
                             dh.doc_id,
                             dh.ext_doc_id,
                             dh.type,
                             dh.status,
                             dh.order_no,
                             dh.location,
                             dh.loc_type,
                             dh.vendor_type,
                             dh.vendor,
                             dh.supplier_site_id,
                             dh.doc_date,
                             dh.due_date,
                             dh.freight_type,
                             dh.consignment_ind,
                             dh.currency_code,
                             dh.cost_pre_match,
                             dh.total_cost,
                             dh.total_qty,
                             dh.doc_source,
                             dh.object_version_id doc_head_version_id,
                             dh.match_id,
                             dh.match_date,
                             'SG-SGL'
                        from im_doc_search_gtt gtt,
                             im_supplier_group_members sgm,
                             im_doc_head dh
                       where gtt.match_key         IN ('SG', 'SGL')
                         and gtt.supplier_group_id = sgm.group_id
                         and sgm.supplier          = dh.vendor
                         and dh.location           = DECODE(gtt.match_key,'SGL', gtt.location, dh.location)
                         and dh.status             IN ('RMTCH', 'URMTCH')
                         and dh.type               = 'MRCHI'
                         --
                         and gtt.doc_id not in(select gtt.doc_id
                                                 from im_manual_group_invoices mgi
                                                where mgi.doc_id = gtt.doc_id)
                         and dh.doc_id not in(select dh.doc_id
                                                from im_manual_group_invoices mgi2
                                               where mgi2.doc_id = dh.doc_id)
                      union all
                      select distinct
                             dh.doc_id,
                             dh.ext_doc_id,
                             dh.type,
                             dh.status,
                             dh.order_no,
                             dh.location,
                             dh.loc_type,
                             dh.vendor_type,
                             dh.vendor,
                             dh.supplier_site_id,
                             dh.doc_date,
                             dh.due_date,
                             dh.freight_type,
                             dh.consignment_ind,
                             dh.currency_code,
                             dh.cost_pre_match,
                             dh.total_cost,
                             dh.total_qty,
                             dh.doc_source,
                             dh.object_version_id doc_head_version_id,
                             dh.match_id,
                             dh.match_date,
                             'S-SL'
                        from im_doc_search_gtt gtt,
                             im_doc_head dh
                       where gtt.match_key IN ('S', 'SL')
                         and gtt.vendor    = dh.vendor
                         and dh.location   = DECODE(gtt.match_key,'SL', gtt.location, dh.location)
                         and dh.status     IN ('RMTCH', 'URMTCH')
                         and dh.type       = 'MRCHI'
                         --
                         and gtt.doc_id not in(select gtt.doc_id
                                                 from im_manual_group_invoices mgi
                                                where mgi.doc_id = gtt.doc_id)
                         and dh.doc_id not in(select dh.doc_id
                                                from im_manual_group_invoices mgi2
                                               where mgi2.doc_id = dh.doc_id)
                      union all
                      select distinct
                             dh.doc_id,
                             dh.ext_doc_id,
                             dh.type,
                             dh.status,
                             dh.order_no,
                             dh.location,
                             dh.loc_type,
                             dh.vendor_type,
                             dh.vendor,
                             dh.supplier_site_id,
                             dh.doc_date,
                             dh.due_date,
                             dh.freight_type,
                             dh.consignment_ind,
                             dh.currency_code,
                             dh.cost_pre_match,
                             dh.total_cost,
                             dh.total_qty,
                             dh.doc_source,
                             dh.object_version_id doc_head_version_id,
                             dh.match_id,
                             dh.match_date,
                             'Ss-SSL'
                        from im_doc_search_gtt gtt,
                             im_doc_head dh
                       where gtt.match_key        IN ('SS', 'SSL')
                         and gtt.supplier_site_id = dh.supplier_site_id
                         and dh.location          = DECODE(gtt.match_key,'SSL', gtt.location, dh.location)
                         and dh.status            IN ('RMTCH', 'URMTCH')
                         and dh.type              = 'MRCHI'
                         --
                         and gtt.doc_id not in(select gtt.doc_id
                                                 from im_manual_group_invoices mgi
                                                where mgi.doc_id = gtt.doc_id)
                         and dh.doc_id not in(select dh.doc_id
                                                from im_manual_group_invoices mgi2
                                               where mgi2.doc_id = dh.doc_id)
                      union all
                      select distinct
                             dh.doc_id,
                             dh.ext_doc_id,
                             dh.type,
                             dh.status,
                             dh.order_no,
                             dh.location,
                             dh.loc_type,
                             dh.vendor_type,
                             dh.vendor,
                             dh.supplier_site_id,
                             dh.doc_date,
                             dh.due_date,
                             dh.freight_type,
                             dh.consignment_ind,
                             dh.currency_code,
                             dh.cost_pre_match,
                             dh.total_cost,
                             dh.total_qty,
                             dh.doc_source,
                             dh.object_version_id doc_head_version_id,
                             dh.match_id,
                             dh.match_date,
                             'PO-POL'
                        from im_doc_search_gtt gtt,
                             im_doc_head dh
                       where gtt.match_key        IN ('PO', 'POL')
                         and gtt.order_no         = dh.order_no
                         and dh.location          = DECODE(gtt.match_key,'POL', gtt.location, dh.location)
                         and dh.status            IN ('RMTCH', 'URMTCH')
                         and dh.type              = 'MRCHI'
                         --
                         and gtt.doc_id not in(select gtt.doc_id
                                                 from im_manual_group_invoices mgi
                                                where mgi.doc_id = gtt.doc_id)
                         and dh.doc_id not in(select dh.doc_id
                                                from im_manual_group_invoices mgi2
                                               where mgi2.doc_id = dh.doc_id))
          select mk.doc_id,
                 mk.ext_doc_id,
                 mk.type,
                 mk.status,
                 mk.order_no,
                 mk.location,
                 mk.loc_type,
                 vl.loc_name loc_name,
                 vl.vat_region loc_vat_region,
                 sb.set_of_books_id loc_set_of_books_id,
                 mk.vendor_type,
                 mk.vendor,
                 vn.vendor_name vendor_name,
                 vn.vendor_phone,
                 mk.supplier_site_id,
                 vn2.vendor_name supplier_site_name,
                 vn2.vat_region supplier_site_vat_region,
                 mk.doc_date,
                 mk.due_date,
                 mk.freight_type,
                 mk.consignment_ind,
                 mk.currency_code,
                 mk.cost_pre_match,
                 so.ap_reviewer,
                 mk.total_cost,
                 mk.total_qty,
                 mk.doc_source,
                 mk.doc_head_version_id,
                 mk.match_id,
                 mk.match_date
            from mk,
                 (select s.store loc, s.store_name loc_name, s.vat_region
                    from store s
                  union all
                  select wh.wh loc, wh.wh_name loc_name, wh.vat_region
                    from wh) vl,
                 (select mls.set_of_books_id,
                         ol.order_no
                    from mv_loc_sob mls,
                         (select order_no,
                                 MIN(location) location
                            from ordloc
                           GROUP BY order_no) ol
                   where mls.location      = ol.location
                     and mls.location_type in ('W','S')
                 ) sb,
                 (select p.partner_id vendor, p.partner_desc vendor_name,
                         p.partner_type vendor_type, p.contact_phone vendor_phone
                    from partner p
                  union all
                  select to_char(s.supplier) vendor, s.sup_name vendor_name,
                         'SUPP' vendor_type, s.contact_phone vendor_phone
                    from sups s
                 ) vn,
                 (select to_char(s2.supplier) vendor, s2.sup_name vendor_name,
                         'SUPP' vendor_type, s2.contact_phone vendor_phone, s2.vat_region
                    from sups s2
                 ) vn2,
                 v_im_supp_site_attrib_expl so
           where mk.location         = vl.loc
             and mk.order_no         = sb.order_no
             and mk.vendor           = vn.vendor
             and mk.vendor_type      = vn.vendor_type
             and mk.supplier_site_id = vn2.vendor(+)
             and vn2.vendor_type(+)  = 'SUPP'
             and so.supplier         = mk.supplier_site_id
   ) use_this
   --
   on (target.doc_id = use_this.doc_id)
   --
   when not matched then
   insert (doc_id,
           ext_doc_id,
           type,
           status,
           order_no,
           location,
           loc_type,
           loc_name,
           loc_vat_region,
           loc_set_of_books_id,
           vendor_type,
           vendor,
           vendor_name,
           vendor_phone,
           supplier_site_id,
           supplier_site_name,
           supplier_site_vat_region,
           doc_date,
           due_date,
           freight_type,
           consignment_ind,
           currency_code,
           cost_pre_match,
           ap_reviewer,
           total_cost,
           total_qty,
           doc_source,
           doc_head_version_id,
           match_id,
           match_date)
   values (use_this.doc_id,
           use_this.ext_doc_id,
           use_this.type,
           use_this.status,
           use_this.order_no,
           use_this.location,
           use_this.loc_type,
           use_this.loc_name,
           use_this.loc_vat_region,
           use_this.loc_set_of_books_id,
           use_this.vendor_type,
           use_this.vendor,
           use_this.vendor_name,
           use_this.vendor_phone,
           use_this.supplier_site_id,
           use_this.supplier_site_name,
           use_this.supplier_site_vat_region,
           use_this.doc_date,
           use_this.due_date,
           use_this.freight_type,
           use_this.consignment_ind,
           use_this.currency_code,
           use_this.cost_pre_match,
           use_this.ap_reviewer,
           use_this.total_cost,
           use_this.total_qty,
           use_this.doc_source,
           use_this.doc_head_version_id,
           use_this.match_id,
           use_this.match_date);

   LOGGER.LOG_INFORMATION(L_program||' Expand DOC to Match Key im_rcpt_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if GET_SUP_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DOC_MATCHKEY_TO_DOC_MATCHKEY;
--------------------------------------------------------------------------
FUNCTION DOC_GROUP_TO_RCPT_GROUP(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.DOC_GROUP_TO_RCPT_GROUP';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   merge into im_rcpt_search_gtt target
   using (select distinct sh.shipment,
                 sh.order_no,
                 sh.asn,
                 sh.status_code,
                 sh.invc_match_status,
                 sh.to_loc location,
                 vl.loc_name location_name,
                 sh.to_loc_type location_type,
                 --
                 sh.bill_to_loc,
                 vl2.loc_name bill_to_loc_name,
                 sh.bill_to_loc_type,
                 vl2.vat_region loc_vat_region,
                 grp.loc_set_of_books_id,
                 --
                 sh.receive_date,
                 s.supplier,
                 s.sup_name supplier_name,
                 oh.supplier supplier_site_id,
                 vn.vendor_name supplier_site_name,
                 vn.vat_region supplier_site_vat_region,
                 oh.currency_code,
                 null total_cost,
                 null total_qty,
                 'N' choice_flag
            from (select distinct ig.group_id,
                         gtt.loc_set_of_books_id
                    from im_doc_search_gtt gtt,
                         im_manual_group_invoices ig
                   where gtt.doc_id = ig.doc_id) grp,
                 im_manual_group_receipts rg,
                 shipment sh,
                 ordhead oh,
                 (select s.store loc, s.store_name loc_name
                    from store s
                  union all
                  select w.wh loc, w.wh_name loc_name
                    from wh w
                 ) vl,
                 (select s.store loc,
                         s.store_name loc_name,
                         s.vat_region
                    from store s
                  union all
                  select w.wh loc,
                         w.wh_name loc_name,
                         w.vat_region
                    from wh w
                 ) vl2,
                 (select to_char(s.supplier) vendor, s.sup_name vendor_name,
                         'SUPP' vendor_type, s.supplier_parent, s.vat_region
                    from sups s
                 ) vn,
                 sups s
           where grp.group_id       = rg.group_id
             and rg.shipment        = sh.shipment
             and sh.order_no        = oh.order_no
             and sh.to_loc          = vl.loc
             and sh.bill_to_loc     = vl2.loc
             and oh.supplier        = vn.vendor
             and vn.supplier_parent = s.supplier) use_this
   --
   on (target.shipment = use_this.shipment)
   --
   when not matched then
   insert (shipment,
           order_no,
           asn,
           status_code,
           invc_match_status,
           location,
           location_type,
           location_name,
           bill_to_loc,
           bill_to_loc_name,
           bill_to_loc_type,
           loc_vat_region,
           loc_set_of_books_id,
           receive_date,
           supplier,
           supplier_name,
           supplier_site_id,
           supplier_site_name,
           supplier_site_vat_region,
           currency_code,
           total_cost,
           total_qty,
           choice_flag)
   values (use_this.shipment,
           use_this.order_no,
           use_this.asn,
           use_this.status_code,
           use_this.invc_match_status,
           use_this.location,
           use_this.location_type,
           use_this.location_name,
           use_this.bill_to_loc,
           use_this.bill_to_loc_name,
           use_this.bill_to_loc_type,
           use_this.loc_vat_region,
           use_this.loc_set_of_books_id,
           use_this.receive_date,
           use_this.supplier,
           use_this.supplier_name,
           use_this.supplier_site_id,
           use_this.supplier_site_name,
           use_this.supplier_site_vat_region,
           use_this.currency_code,
           use_this.total_cost,
           use_this.total_qty,
           use_this.choice_flag);

   LOGGER.LOG_INFORMATION(L_program||' doc group to rcpt group im_rcpt_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DOC_GROUP_TO_RCPT_GROUP;
--------------------------------------------------------------------------
FUNCTION DOC_MATCHKEY_TO_RCPT_MATCHKEY(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.DOC_MATCHKEY_TO_RCPT_MATCHKEY';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   merge into im_rcpt_search_gtt target
   using (
           with ship as (select distinct
                                sh.shipment,
                                sh.order_no,
                                sh.asn,
                                sh.status_code,
                                sh.invc_match_status,
                                sh.to_loc,
                                sh.to_loc_type,
                                sh.bill_to_loc,
                                sh.bill_to_loc_type,
                                sh.receive_date,
                                oh.supplier,
                                oh.currency_code
                           from im_doc_search_gtt gtt,
                                im_supplier_group_members sgm,
                                sups s,
                                ordhead oh,
                                shipment sh
                          where gtt.match_key         in('SG', 'SGL')
                            and gtt.supplier_group_id = sgm.group_id
                            and sgm.supplier          = s.supplier_parent
                            and oh.supplier           = s.supplier
                            and oh.order_no           = sh.order_no
                            and sh.bill_to_loc        = decode(gtt.match_key,'SGL', gtt.location, sh.bill_to_loc)
                            and sh.status_code       IN (REIM_CONSTANTS.SHIP_STATUS_UNMTCH,
                                                         REIM_CONSTANTS.SHIP_STATUS_RECEIVED,
                                                         REIM_CONSTANTS.SHIP_STATUS_CANCELLED)
                            and sh.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                            and gtt.doc_id not in(select gtt.doc_id
                                                    from im_manual_group_invoices mgi
                                                   where mgi.doc_id = gtt.doc_id)
                            and sh.shipment not in(select sh.shipment
                                                     from im_manual_group_receipts mgr
                                                    where mgr.shipment = sh.shipment)
                         union all
                         select distinct
                                sh.shipment,
                                sh.order_no,
                                sh.asn,
                                sh.status_code,
                                sh.invc_match_status,
                                sh.to_loc,
                                sh.to_loc_type,
                                sh.bill_to_loc,
                                sh.bill_to_loc_type,
                                sh.receive_date,
                                oh.supplier,
                                oh.currency_code
                           from im_doc_search_gtt gtt,
                                sups s,
                                ordhead oh,
                                shipment sh
                          where gtt.match_key        in('S', 'SL')
                            and s.supplier_parent    = gtt.po_supplier
                            and oh.supplier          = s.supplier
                            and oh.order_no          = sh.order_no
                            and sh.bill_to_loc       = decode(gtt.match_key, 'SL',  gtt.location, sh.bill_to_loc)
                            and sh.status_code       IN (REIM_CONSTANTS.SHIP_STATUS_UNMTCH,
                                                         REIM_CONSTANTS.SHIP_STATUS_RECEIVED,
                                                         REIM_CONSTANTS.SHIP_STATUS_CANCELLED)
                            and sh.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                            and gtt.doc_id not in(select gtt.doc_id
                                                    from im_manual_group_invoices mgi
                                                   where mgi.doc_id = gtt.doc_id)
                            and sh.shipment not in(select sh.shipment
                                                     from im_manual_group_receipts mgr
                                                    where mgr.shipment = sh.shipment)
                         union all
                         select distinct
                                sh.shipment,
                                sh.order_no,
                                sh.asn,
                                sh.status_code,
                                sh.invc_match_status,
                                sh.to_loc,
                                sh.to_loc_type,
                                sh.bill_to_loc,
                                sh.bill_to_loc_type,
                                sh.receive_date,
                                oh.supplier,
                                oh.currency_code
                           from im_doc_search_gtt gtt,
                                ordhead oh,
                                shipment sh
                          where gtt.match_key        in('SS', 'SSL')
                            and oh.supplier          = gtt.po_supplier_site
                            and oh.order_no          = sh.order_no
                            and sh.bill_to_loc       = decode(gtt.match_key, 'SSL', gtt.location, sh.bill_to_loc)
                            and sh.status_code       IN (REIM_CONSTANTS.SHIP_STATUS_UNMTCH,
                                                         REIM_CONSTANTS.SHIP_STATUS_RECEIVED,
                                                         REIM_CONSTANTS.SHIP_STATUS_CANCELLED)
                            and sh.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                            and gtt.doc_id not in(select gtt.doc_id
                                                    from im_manual_group_invoices mgi
                                                   where mgi.doc_id = gtt.doc_id)
                            and sh.shipment not in(select sh.shipment
                                                     from im_manual_group_receipts mgr
                                                    where mgr.shipment = sh.shipment)
                         union all
                         select distinct
                                sh.shipment,
                                sh.order_no,
                                sh.asn,
                                sh.status_code,
                                sh.invc_match_status,
                                sh.to_loc,
                                sh.to_loc_type,
                                sh.bill_to_loc,
                                sh.bill_to_loc_type,
                                sh.receive_date,
                                oh.supplier,
                                oh.currency_code
                           from im_doc_search_gtt gtt,
                                shipment sh,
                                ordhead oh
                          where gtt.match_key        in('PO', 'POL')
                            and sh.order_no          = gtt.order_no
                            and sh.bill_to_loc       = decode(gtt.match_key,'POL', gtt.location, sh.bill_to_loc)
                            and sh.status_code       IN (REIM_CONSTANTS.SHIP_STATUS_UNMTCH,
                                                         REIM_CONSTANTS.SHIP_STATUS_RECEIVED,
                                                         REIM_CONSTANTS.SHIP_STATUS_CANCELLED)
                            and sh.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                            and sh.order_no          = oh.order_no
                            and gtt.doc_id not in(select gtt.doc_id
                                                    from im_manual_group_invoices mgi
                                                   where mgi.doc_id = gtt.doc_id)
                            and sh.shipment not in(select sh.shipment
                                                     from im_manual_group_receipts mgr
                                                    where mgr.shipment = sh.shipment))
          select ship.shipment,
                 ship.order_no,
                 ship.asn,
                 ship.status_code,
                 ship.invc_match_status,
                 ship.to_loc location,
                 vl.loc_name location_name,
                 ship.to_loc_type location_type,
                 --
                 ship.bill_to_loc,
                 vl2.loc_name bill_to_loc_name,
                 ship.bill_to_loc_type,
                 vl2.vat_region loc_vat_region,
                 --
                 ship.receive_date,
                 s.supplier,
                 s.sup_name supplier_name,
                 ship.supplier supplier_site_id,
                 vn.vendor_name supplier_site_name,
                 vn.vat_region supplier_site_vat_region,
                 ship.currency_code,
                 null total_cost,
                 null total_qty,
                 'N' choice_flag
            from ship,
                 (select s.store loc, s.store_name loc_name
                    from store s
                  union all
                  select w.wh loc, w.wh_name loc_name
                    from wh w
                 ) vl,
                 (select s.store loc,
                         s.store_name loc_name,
                         s.vat_region
                    from store s
                  union all
                  select w.wh loc,
                         w.wh_name loc_name,
                         w.vat_region
                    from wh w
                 ) vl2,
                 (select to_char(s.supplier) vendor, s.sup_name vendor_name,
                         'SUPP' vendor_type, s.supplier_parent, s.vat_region
                    from sups s
                 ) vn,
                 sups s
           where ship.to_loc          = vl.loc
             and ship.bill_to_loc     = vl2.loc
             and ship.supplier        = vn.vendor
             and vn.supplier_parent   = s.supplier
         ) use_this
   --
   on (target.shipment = use_this.shipment)
   --
   when not matched then
   insert (shipment,
           order_no,
           asn,
           status_code,
           invc_match_status,
           location,
           location_type,
           location_name,
           bill_to_loc,
           bill_to_loc_name,
           bill_to_loc_type,
           loc_vat_region,
           receive_date,
           supplier,
           supplier_name,
           supplier_site_id,
           supplier_site_name,
           supplier_site_vat_region,
           currency_code,
           total_cost,
           total_qty,
           choice_flag)
   values (use_this.shipment,
           use_this.order_no,
           use_this.asn,
           use_this.status_code,
           use_this.invc_match_status,
           use_this.location,
           use_this.location_type,
           use_this.location_name,
           use_this.bill_to_loc,
           use_this.bill_to_loc_name,
           use_this.bill_to_loc_type,
           use_this.loc_vat_region,
           use_this.receive_date,
           use_this.supplier,
           use_this.supplier_name,
           use_this.supplier_site_id,
           use_this.supplier_site_name,
           use_this.supplier_site_vat_region,
           use_this.currency_code,
           use_this.total_cost,
           use_this.total_qty,
           use_this.choice_flag);


   LOGGER.LOG_INFORMATION(L_program||' doc match key to rcpt match key im_rcpt_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if GET_RCPT_SET_OF_BOOKS(O_error_message) = FALSE then
      return FALSE;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DOC_MATCHKEY_TO_RCPT_MATCHKEY;
--------------------------------------------------------------------------
FUNCTION CALC_MATCH_KEY_GET_RCPT(O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)   := 'REIM_SEARCH_SQL.CALC_MATCH_KEY_GET_RCPT';
   L_start_time         TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete from im_match_key_gtt;

   insert into im_match_key_gtt(workspace_id,
                                match_key_value,
                                match_key_id)
                         select -1,
                                match_key_value,
                                IM_MATCH_KEY_ID_SEQ.NEXTVAL match_key_id
                           from (select distinct gtt.match_key || '^' ||
                                                    DECODE(gtt.match_key,
                                                           REIM_CONSTANTS.MATCH_KEY_PO_LOC,         gtt.order_no || '^' || gtt.location,
                                                           REIM_CONSTANTS.MATCH_KEY_PO,             gtt.order_no,
                                                           REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC,  gtt.po_supplier_site|| '^' || gtt.location,
                                                           REIM_CONSTANTS.MATCH_KEY_SUPP_SITE,      gtt.po_supplier_site,
                                                           REIM_CONSTANTS.MATCH_KEY_SUPP_LOC,       gtt.po_supplier || '^' || gtt.location,
                                                           REIM_CONSTANTS.MATCH_KEY_SUPPLIER,       gtt.po_supplier,
                                                           REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, gtt.supplier_group_id || '^' || gtt.location,
                                                           REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP,     gtt.supplier_group_id) match_key_value
                                   from im_doc_search_gtt gtt);

   LOGGER.LOG_INFORMATION(L_program||' Insert im_match_key_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_search_gtt tgt
   using (select gtt.workspace_id,
                 gtt.match_key_value,
                 gtt.match_key_id
            from im_match_key_gtt gtt) src
   on (src.match_key_value = tgt.match_key || '^' ||
                             DECODE(tgt.match_key,
                                    REIM_CONSTANTS.MATCH_KEY_PO_LOC,         tgt.order_no || '^' || tgt.location,
                                    REIM_CONSTANTS.MATCH_KEY_PO,             tgt.order_no,
                                    REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC,  tgt.po_supplier_site|| '^' || tgt.location,
                                    REIM_CONSTANTS.MATCH_KEY_SUPP_SITE,      tgt.po_supplier_site,
                                    REIM_CONSTANTS.MATCH_KEY_SUPP_LOC,       tgt.po_supplier || '^' || tgt.location,
                                    REIM_CONSTANTS.MATCH_KEY_SUPPLIER,       tgt.po_supplier,
                                    REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, tgt.supplier_group_id || '^' || tgt.location,
                                    REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP,     tgt.supplier_group_id))
   when MATCHED then
      update
         set tgt.match_key_value = src.match_key_value,
             tgt.match_key_id    = src.match_key_id;

   LOGGER.LOG_INFORMATION(L_program||' Merge im_doc_search_gtt match_key_value, match_key_id - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   update im_doc_search_gtt gtt
      set gtt.match_key_value = null,
          gtt.match_key_id    = null
    where gtt.doc_id in(select gtt.doc_id
                          from im_manual_group_invoices mgi
                         where mgi.doc_id = gtt.doc_id);

   LOGGER.LOG_INFORMATION(L_program||' Update im_doc_search_gtt null match_key_value, match_key_id for manual groups - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_invc_key_gtt;

   insert into im_match_invc_key_gtt(workspace_id,
                                     match_key,
                                     match_key_id,
                                     supplier_group_id,
                                     po_supplier,
                                     po_supplier_site_id,
                                     order_no,
                                     location)
                              select distinct -1,
                                              gtt.match_key,
                                              gtt.match_key_id,
                                              DECODE(gtt.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP,     gtt.supplier_group_id,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, gtt.supplier_group_id,
                                                     NULL) supplier_group_id,
                                              DECODE(gtt.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPPLIER, gtt.po_supplier,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_LOC, gtt.po_supplier,
                                                     NULL) po_supplier,
                                              DECODE(gtt.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_SITE,     gtt.po_supplier_site,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC, gtt.po_supplier_site,
                                                     NULL) po_supplier_site_id,
                                              DECODE(gtt.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_PO_LOC, gtt.order_no,
                                                     REIM_CONSTANTS.MATCH_KEY_PO,     gtt.order_no,
                                                     NULL) order_no,
                                              DECODE(gtt.match_key,
                                                     REIM_CONSTANTS.MATCH_KEY_PO_LOC,         gtt.location,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC,  gtt.location,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_LOC,       gtt.location,
                                                     REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, gtt.location,
                                                     NULL) location
                                from im_doc_search_gtt gtt
                               where gtt.doc_id not in(select gtt.doc_id
                                                         from im_manual_group_invoices mgi
                                                        where mgi.doc_id = gtt.doc_id);

   LOGGER.LOG_INFORMATION(L_program||' Insert im_match_invc_key_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_rcpt_search_gtt(shipment,
                                  order_no,
                                  asn,
                                  status_code,
                                  invc_match_status,
                                  location,
                                  location_type,
                                  location_name,
                                  bill_to_loc,
                                  bill_to_loc_name,
                                  bill_to_loc_type,
                                  receive_date,
                                  supplier,
                                  supplier_name,
                                  supplier_site_id,
                                  supplier_site_name,
                                  currency_code,
                                  total_cost,
                                  total_qty,
                                  choice_flag,
                                  loc_vat_region,
                                  supplier_site_vat_region,
                                  match_key_id)
              with order_ws as (select imiw.workspace_id,
                                       imiw.match_key_id,
                                       oh.order_no,
                                       oh.supplier,
                                       DECODE(imiw.match_key,
                                              REIM_CONSTANTS.MATCH_KEY_PO_LOC, imiw.location,
                                              NULL) location,
                                       oh.currency_code
                                  from im_match_invc_key_gtt imiw,
                                       ordhead oh
                                 where imiw.order_no  = oh.order_no
                                   and imiw.match_key IN (REIM_CONSTANTS.MATCH_KEY_PO,
                                                          REIM_CONSTANTS.MATCH_KEY_PO_LOC)
                                union all
                                select imiw.workspace_id,
                                       imiw.match_key_id,
                                       oh.order_no,
                                       oh.supplier,
                                       DECODE(imiw.match_key,
                                              REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC, imiw.location,
                                              NULL) location,
                                       oh.currency_code
                                  from im_match_invc_key_gtt imiw,
                                       ordhead oh
                                 where oh.supplier     = imiw.po_supplier_site_id
                                   and imiw.match_key IN (REIM_CONSTANTS.MATCH_KEY_SUPP_SITE,
                                                          REIM_CONSTANTS.MATCH_KEY_SUPP_SITE_LOC)
                                union all
                                select imiw.workspace_id,
                                       imiw.match_key_id,
                                       oh.order_no,
                                       oh.supplier,
                                       DECODE(imiw.match_key,
                                              REIM_CONSTANTS.MATCH_KEY_SUPP_LOC, imiw.location,
                                              NULL) location,
                                       oh.currency_code
                                  from im_match_invc_key_gtt imiw,
                                       sups s,
                                       ordhead oh
                                 where s.supplier_parent = imiw.po_supplier
                                   and oh.supplier       = s.supplier
                                   and imiw.match_key    IN (REIM_CONSTANTS.MATCH_KEY_SUPPLIER,
                                                             REIM_CONSTANTS.MATCH_KEY_SUPP_LOC)
                                union all
                                select imiw.workspace_id,
                                       imiw.match_key_id,
                                       oh.order_no,
                                       oh.supplier,
                                       DECODE(imiw.match_key,
                                              REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC, imiw.location,
                                              NULL) location,
                                       oh.currency_code
                                  from im_match_invc_key_gtt imiw,
                                       im_supplier_group_members isgm,
                                       sups s,
                                       ordhead oh
                                 where isgm.group_id     = imiw.supplier_group_id
                                   and s.supplier_parent = isgm.supplier
                                   and oh.supplier       = s.supplier
                                   and imiw.match_key    IN (REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP,
                                                             REIM_CONSTANTS.MATCH_KEY_SUPP_GROUP_LOC))
                         select sh.shipment,
                                ows.order_no,
                                sh.asn,
                                sh.status_code,
                                sh.invc_match_status,
                                sh.to_loc,
                                sh.to_loc_type,
                                vl.loc_name location_name,
                                sh.bill_to_loc,
                                vl2.loc_name bill_to_loc_name,
                                sh.bill_to_loc_type,
                                sh.receive_date,
                                vn2.vendor,
                                vn2.vendor_name,
                                ows.supplier,
                                vn.vendor_name,
                                ows.currency_code,
                                0 total_cost,    --to be merged later
                                0 total_qty,     --to be merged later
                                'N' choice_flag,
                                vl2.vat_region loc_vat_region,
                                vn.vat_region supplier_site_vat_region,
                                ows.match_key_id
                           from order_ws ows,
                                shipment sh,
                                (select s.store loc, s.store_name loc_name
                                   from v_store s
                                 union all
                                 select w.wh loc, w.wh_name loc_name
                                   from v_wh w
                                ) vl,
                                (select s.store loc,
                                        s.store_name loc_name,
                                        s.vat_region
                                   from store s
                                 union all
                                 select w.wh loc,
                                        w.wh_name loc_name,
                                        w.vat_region
                                   from wh w
                                ) vl2,
                                (select to_char(s.supplier) vendor, s.sup_name vendor_name,
                                        'SUPP' vendor_type, s.supplier_parent, s.vat_region
                                   from sups s
                                ) vn,
                                (select to_char(s2.supplier) vendor, s2.sup_name vendor_name,
                                        'SUPP' vendor_type, s2.supplier_parent, s2.vat_region
                                   from sups s2
                                ) vn2
                          where sh.order_no          = ows.order_no
                            and sh.bill_to_loc       = NVL(ows.location, sh.bill_to_loc)
                            and sh.status_code       IN (REIM_CONSTANTS.SHIP_STATUS_UNMTCH,
                                                         REIM_CONSTANTS.SHIP_STATUS_RECEIVED,
                                                         REIM_CONSTANTS.SHIP_STATUS_CANCELLED)
                            and sh.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                            and sh.receive_date      is NOT NULL
                            --
                            and sh.to_loc            = vl.loc
                            and sh.bill_to_loc       = vl2.loc
                            and ows.supplier         = vn.vendor
                            and vn.supplier_parent   = vn2.vendor
                            and NOT EXISTS (select 'x'
                                              from im_manual_group_receipts imgr
                                             where imgr.shipment = sh.shipment);

   LOGGER.LOG_INFORMATION(L_program||' Insert im_rcpt_search_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if GET_RCPT_SET_OF_BOOKS(O_error_message) = FALSE then
      return FALSE;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_MATCH_KEY_GET_RCPT;
--------------------------------------------------------------------------
END REIM_SEARCH_SQL;
/
