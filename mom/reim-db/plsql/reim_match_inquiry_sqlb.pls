CREATE OR REPLACE PACKAGE BODY REIM_MATCH_INQUIRY_SQL AS
----------------------------------------------------------------
FUNCTION CREATE_MATCH_INQ_DETAIL_WS(O_error_message    OUT VARCHAR2,
                                    O_workspace_id  IN OUT IM_MATCH_INQ_DETAIL_WS.WORKSPACE_ID%TYPE,
                                    I_doc_id        IN     IM_DOC_HEAD.DOC_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_MATCH_INQUIRY_SQL.CREATE_MATCH_INQ_DETAIL_WS';

BEGIN

   O_workspace_id := im_workspace_id_seq.nextval;

   LOGGER.LOG_INFORMATION(L_program||' workspace: ' || O_workspace_id);

   delete from numeric_id_gtt;
   insert into numeric_id_gtt(numeric_id)
   select distinct match_id 
     from im_detail_match_invc_history ih
    where ih.doc_id   = I_doc_id;

   ------------------------------------------------
   --TOP LEVEL STYLE or SKU
   ------------------------------------------------
   insert into im_match_inq_detail_ws (match_inq_detail_ws_id,
                                       workspace_id,
                                       ancestor_id,
                                       match_status,
                                       item_parent,
                                       item,
                                       item_description,
                                       entity_type)
      with items as (select ih.item
                       from numeric_id_gtt gtt,
                            im_detail_match_invc_history ih
                      where gtt.numeric_id = ih.match_id
                     union
                     select rh.item
                       from numeric_id_gtt gtt,
                            im_detail_match_rcpt_history rh
                      where gtt.numeric_id = rh.match_id
                    )
      select im_match_inq_detail_ws_seq.nextval,
             O_workspace_id,
             inner.ancestor_id,
             inner.match_status,
             inner.item_parent,
             inner.item,
             inner.item_description,
             inner.entity_type
        from (select distinct 
                     null ancestor_id,
                     null match_status,
                     null item_parent,
                     nvl(im.item_parent, im.item) item,
                     nvl(imp.item_desc, im.item_desc) item_description,
                     decode(im.item_parent,
                            null, REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU,
                            REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE) entity_type
                     --
                from items,
                     item_master im,
                     item_master imp
               where items.item     = im.item
                 and im.item_parent = imp.item(+)
              ) inner;

   LOGGER.LOG_INFORMATION(L_program||' TOP LEVEL STYLE or SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --SKU UNDER STYLE
   ------------------------------------------------
   insert into im_match_inq_detail_ws (match_inq_detail_ws_id,
                                       workspace_id,
                                       ancestor_id,
                                       item_parent,
                                       item,
                                       item_description,
                                       entity_type)
      with style as (select imidw.match_inq_detail_ws_id,
                            imidw.item
                       from im_match_inq_detail_ws imidw
                      where imidw.workspace_id = O_workspace_id
                        and imidw.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE),
           items as (select distinct idmih.item
                       from im_detail_match_invc_history idmih
                      where idmih.doc_id       = I_doc_id
                     union
                     select distinct idmrh.item
                       from im_detail_match_invc_history idmih,
                            im_detail_match_rcpt_history idmrh
                      where idmih.doc_id   = I_doc_id
                        and idmrh.match_id = idmih.match_id)
      select im_match_inq_detail_ws_seq.nextval,
             inner.workspace_id,
             inner.ancestor_id,
             inner.item_parent,
             inner.item,
             inner.item_description,
             REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU entity_type
        from (select O_workspace_id workspace_id,
                     style.match_inq_detail_ws_id ancestor_id,
                     im.item_parent,
                     items.item,
                     im.item_desc item_description
                from style,
                     items,
                     item_master im
               where style.item = im.item_parent
                 and im.item    = items.item) inner;

   LOGGER.LOG_INFORMATION(L_program||' SKU UNDER STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --INVC UNDER SKU
   ------------------------------------------------
   insert into im_match_inq_detail_ws (match_inq_detail_ws_id,
                                       workspace_id,
                                       ancestor_id,
                                       match_status,
                                       item_parent,
                                       item,
                                       item_description,
                                       vpn,
                                       entity_type,
                                       invoice_id,
                                       invoice_unit_cost,
                                       invoice_qty,
                                       invoice_ext_cost,
                                       invoice_ext_doc_id)
   select /*+ ORDERED */ im_match_inq_detail_ws_seq.nextval,
          dm.workspace_id,
          dm.match_inq_detail_ws_id ancestor_id,
          decode(id.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'M', 'U') match_status,
          dm.item_parent,
          dm.item,
          dm.item_description,
          its.vpn,
          REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC entity_type,
          --
          id.doc_id invoice_id,
          id.unit_cost invoice_unit_cost,
          id.qty invoice_qty,
          id.qty * id.unit_cost invoice_ext_cost,
          --
          dh.ext_doc_id
     from im_match_inq_detail_ws dm,
          numeric_id_gtt gtt,
          im_detail_match_invc_history ih,
          im_invoice_detail id,
          im_doc_head dh,
          item_supplier its
    where dm.workspace_id   = O_workspace_id
      and dm.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and gtt.numeric_id    = ih.match_id
      and ih.doc_id         = id.doc_id
      and ih.item           = id.item
      and id.item           = dm.item
      and id.doc_id         = dh.doc_id
      and its.item (+)      = id.item
      and its.supplier (+)  = dh.supplier_site_id;

   LOGGER.LOG_INFORMATION(L_program||' INVC UNDER SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --populate im_transform_shipsku_gtt
   ------------------------------------------------
   delete from im_transform_shipsku_gtt;

   insert into im_transform_shipsku_gtt (shipment,
                                         item,
                                         vpn,
                                         seq_no,
                                         ss_qty_received,
                                         ss_qty_matched,
                                         ss_unit_cost,
                                         weight_received,
                                         weight_received_uom,
                                         carton,
                                         catch_weight_type,
                                         order_no,
                                         supplier,
                                         sup_qty_level,
                                         transform_qty_received,
                                         transform_qty_matched,
                                         transform_unit_cost,
                                         invc_match_status)
   select distinct 
          ss.shipment,
          ss.item,
          its.vpn,
          ss.seq_no,
          ss.qty_received,
          ss.qty_matched,
          ss.unit_cost,
          ss.weight_received,
          ss.weight_received_uom,
          ss.carton,
          im.catch_weight_type,
          oh.order_no,
          oh.supplier,
          s.sup_qty_level,
          ss.qty_received,
          ss.qty_matched,
          ss.unit_cost,
          ss.invc_match_status
     from numeric_id_gtt gtt,
          im_detail_match_rcpt_history rh,
          shipsku ss,
          shipment sh,
          ordhead oh,
          sups s,
          item_master im,
          item_supplier its
    where gtt.numeric_id                  = rh.match_id
      and rh.shipment                     = ss.shipment
      and nvl(rh.substitute_item,rh.item) = ss.item
      and ss.shipment                     = sh.shipment
      and sh.order_no                     = oh.order_no
      and oh.supplier                     = s.supplier
      and ss.item                         = im.item
      and its.item                        = ss.item
      and its.supplier                    = oh.supplier;

   LOGGER.LOG_INFORMATION(L_program||' POP im_transform_shipsku_gtt for INVC rows- SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_XFORM_SHIPSKU_SQL.TRANSFORM_SHIPSKU_GTT(O_error_message) = REIM_CONSTANTS.FAIL then
       return REIM_CONSTANTS.FAIL;
   end if;

   LOGGER.TIME_STOP('XFORM_SHIPSKU');

   ------------------------------------------------
   --RCPT UNDER SKU
   ------------------------------------------------
   insert into im_match_inq_detail_ws (match_inq_detail_ws_id,
                                       workspace_id,
                                       ancestor_id,
                                       match_status,
                                       item_parent,
                                       item,
                                       item_description,
                                       vpn,
                                       entity_type,
                                       receipt_id,
                                       receipt_unit_cost,
                                       receipt_avail_qty,
                                       receipt_ext_cost,
                                       receipt_received_qty,
                                       receipt_matched_qty,
                                       orig_receipt_item)
   select im_match_inq_detail_ws_seq.nextval,
          i.workspace_id,
          i.ancestor_id,
          i.match_status,
          i.item_parent,
          i.item,
          i.item_description,
          i.vpn,
          i.entity_type,
          i.receipt_id,
          i.receipt_unit_cost,
          i.receipt_avail_qty,
          i.receipt_ext_cost,
          i.receipt_received_qty,
          i.receipt_matched_qty,
          i.orig_receipt_item
     from (select /*+ ORDERED */
                  dm.workspace_id,
                  dm.match_inq_detail_ws_id ancestor_id,
                  max(x_gtt.invc_match_status) match_status,
                  dm.item_parent,
                  dm.item,
                  dm.item_description,
                  x_gtt.vpn,
                  REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT entity_type,
                  --
                  x_gtt.shipment receipt_id,
                  sum(x_gtt.transform_unit_cost) receipt_unit_cost,
                  sum(x_gtt.transform_qty_received - x_gtt.transform_qty_matched) receipt_avail_qty,
                  sum(x_gtt.transform_unit_cost * (x_gtt.transform_qty_received - x_gtt.transform_qty_matched)) receipt_ext_cost,
                  sum(x_gtt.transform_qty_received) receipt_received_qty,
                  sum(x_gtt.transform_qty_matched) receipt_matched_qty,
                  --
                  decode(dm.item, x_gtt.item,
                         null,
                         x_gtt.item) orig_receipt_item
             from numeric_id_gtt n_gtt,
                  im_transform_shipsku_gtt x_gtt,
                  im_detail_match_rcpt_history idmrh,
                  im_match_inq_detail_ws dm
            where idmrh.match_id  = n_gtt.numeric_id
              and dm.workspace_id = O_workspace_id
              and dm.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
              and dm.item         = idmrh.item
              and x_gtt.item      = NVL(idmrh.substitute_item, idmrh.item)
            group by dm.workspace_id,
                     dm.match_inq_detail_ws_id,
                     dm.item_parent,
                     dm.item,
                     dm.item_description,
                     x_gtt.shipment,
                     x_gtt.item,
                     x_gtt.vpn) i;

   LOGGER.LOG_INFORMATION(L_program||' RCPT UNDER SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --rollup info from INVC to SKU
   ------------------------------------------------
   merge into im_match_inq_detail_ws target
   using (select i.workspace_id,
                 i.item,
                 decode(i.invc_count, 1, i.invoice_id, null) invoice_id,
                 decode(i.invc_count, 1, i.invoice_ext_doc_id, null) invoice_ext_doc_id,
                 decode(i.cost_count, 1, i.invoice_unit_cost, null) invoice_unit_cost,
                 i.invoice_qty,
                 i.invoice_ext_cost
            from (select d.workspace_id,
                         d.item,
                         min(d.invoice_id)
                            over (partition by d.workspace_id, d.item) invoice_id,
                         min(d.invoice_ext_doc_id)
                            over (partition by d.workspace_id, d.item) invoice_ext_doc_id,
                         min(d.invoice_unit_cost)
                            over (partition by d.workspace_id, d.item) invoice_unit_cost,
                         sum(d.invoice_qty)
                            over (partition by d.workspace_id, d.item) invoice_qty,
                         sum(d.invoice_ext_cost)
                            over (partition by d.workspace_id, d.item) invoice_ext_cost,
                         row_number()
                            over (partition by d.workspace_id, d.item order by d.match_inq_detail_ws_id) rownbr,
                         count(distinct d.invoice_id)
                            over (partition by d.workspace_id, d.item) invc_count,
                         count(distinct d.invoice_unit_cost)
                            over (partition by d.workspace_id, d.item) cost_count
                    from im_match_inq_detail_ws d
                   where d.workspace_id = O_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC) i
                 --
           where i.rownbr = 1) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.item         = use_this.item
       and target.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.invoice_id              = use_this.invoice_id,
             target.invoice_ext_doc_id      = use_this.invoice_ext_doc_id,
             target.invoice_unit_cost       = use_this.invoice_unit_cost,
             target.invoice_qty             = use_this.invoice_qty,
             target.invoice_ext_cost        = use_this.invoice_ext_cost;

   LOGGER.LOG_INFORMATION(L_program||' rollup status and what not from INVC to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --rollup info from RCPT to SKU
   ------------------------------------------------
   merge into im_match_inq_detail_ws target
   using (select i.workspace_id,
                 i.item,
                 decode(i.rcpt_count, 1, i.receipt_id, null) receipt_id,
                 decode(i.cost_count, 1, i.receipt_unit_cost, null) receipt_unit_cost,
                 i.receipt_avail_qty,
                 i.receipt_ext_cost,
                 i.receipt_received_qty,
                 i.receipt_matched_qty
            from (select d.workspace_id,
                         d.item,
                         min(d.receipt_id)
                            over (partition by d.workspace_id, d.item) receipt_id,
                         min(d.receipt_unit_cost)
                            over (partition by d.workspace_id, d.item) receipt_unit_cost,
                         sum(d.receipt_avail_qty)
                            over (partition by d.workspace_id, d.item) receipt_avail_qty,
                         sum(d.receipt_ext_cost)
                            over (partition by d.workspace_id, d.item) receipt_ext_cost,
                         sum(d.receipt_received_qty)
                            over (partition by d.workspace_id, d.item) receipt_received_qty,
                         sum(d.receipt_matched_qty)
                            over (partition by d.workspace_id, d.item) receipt_matched_qty,
                         row_number()
                            over (partition by d.workspace_id, d.item order by d.match_inq_detail_ws_id) rownbr,
                         count(distinct d.receipt_id)
                            over (partition by d.workspace_id, d.item) rcpt_count,
                         count(distinct d.receipt_unit_cost)
                            over (partition by d.workspace_id, d.item) cost_count
                    from im_match_inq_detail_ws d
                   where d.workspace_id = O_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT) i
                 --
           where i.rownbr = 1) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.item         = use_this.item
       and target.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.receipt_id             = use_this.receipt_id,
             target.receipt_unit_cost      = use_this.receipt_unit_cost,
             target.receipt_avail_qty      = use_this.receipt_avail_qty,
             target.receipt_ext_cost       = use_this.receipt_ext_cost,
             target.receipt_received_qty   = use_this.receipt_received_qty,
             target.receipt_matched_qty    = use_this.receipt_matched_qty;

   LOGGER.LOG_INFORMATION(L_program||' rollup status and what not from RCPT to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --rollup Variance and other info from both INVC and RCPT to SKU
   ------------------------------------------------

   merge into im_match_inq_detail_ws target
   using (select invc.ancestor_id,
                 rcpt.rcpt_group_cost - invc.invc_group_cost cost_variance,
                 rcpt.rcpt_group_qty - invc.invc_group_qty qty_variance,
                 DECODE(invc_vpn, rcpt_vpn, invc_vpn, NULL) vpn
            from (select id.ancestor_id,
                         min(id.invoice_unit_cost) invc_group_cost,
                         sum(id.invoice_qty) invc_group_qty,
                         DECODE(count(distinct NVL(id.vpn,'VpN')),
                                1, min(id.vpn),
                                NULL) invc_vpn
                   from im_match_inq_detail_ws id
                  where id.workspace_id = O_workspace_id
                    and id.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                  group by id.ancestor_id) invc,
                 (select rd.ancestor_id,
                         min(rd.receipt_unit_cost) rcpt_group_cost,
                         sum(rd.receipt_matched_qty) rcpt_group_qty,
                         DECODE(count(distinct NVL(rd.vpn,'VpN')),
                                1, min(rd.vpn),
                                NULL) rcpt_vpn
                   from im_match_inq_detail_ws rd
                  where rd.workspace_id = O_workspace_id
                    and rd.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                  group by rd.ancestor_id) rcpt
           where invc.ancestor_id = rcpt.ancestor_id) use_this
   on (    target.workspace_id           = O_workspace_id
       and target.match_inq_detail_ws_id = use_this.ancestor_id
       and target.entity_type            = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.unit_cost_variance = use_this.cost_variance,
             target.qty_variance       = use_this.qty_variance,
             target.vpn                = use_this.vpn;

   LOGGER.LOG_INFORMATION(L_program||' rollup variances to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --Update VPN on STYLE
   ------------------------------------------------

   merge into im_match_inq_detail_ws target
   using (select distinct sku_ws.ancestor_id,
                 its.vpn
            from (select inner.ancestor_id,
                         DECODE(count(distinct inner.supplier),
                                1, min(inner.supplier),
                                NULL) supplier
                    from (select id.ancestor_id,
                                 DECODE(count(distinct idh.supplier_site_id),
                                        1, To_Number(min(idh.supplier_site_id)),
                                        NULL) supplier
                            from im_match_inq_detail_ws id,
                                 im_doc_head idh
                           where id.workspace_id = O_workspace_id
                             and id.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                             and idh.doc_id      = id.invoice_id
                           group by id.ancestor_id
                           union all
                           select rd.ancestor_id,
                                  DECODE(count(distinct oh.supplier),
                                         1, min(oh.supplier),
                                         NULL) supplier
                            from im_match_inq_detail_ws rd,
                                 shipment sh,
                                 ordhead oh
                           where rd.workspace_id = O_workspace_id
                             and rd.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                             and sh.shipment     = rd.receipt_id
                             and oh.order_no     = sh.order_no
                           group by rd.ancestor_id)inner
                   group by inner.ancestor_id) invc_rcpt_ws,
                  im_match_inq_detail_ws sku_ws,
                  item_supplier its
           where sku_ws.workspace_id           = O_workspace_id
             and sku_ws.match_inq_detail_ws_id = invc_rcpt_ws.ancestor_id
             and sku_ws.entity_type            = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and its.item                      = sku_ws.item_parent
             and its.supplier                  = invc_rcpt_ws.supplier) use_this
   on (    target.workspace_id           = O_workspace_id
       and target.match_inq_detail_ws_id = use_this.ancestor_id
       and target.entity_type            = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set target.vpn = use_this.vpn;

   LOGGER.LOG_INFORMATION(L_program||' rollup VPN to STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --rollup reason code info to SKU
   ------------------------------------------------
   merge into im_match_inq_detail_ws target
   using (select inner.ancestor_id match_inq_detail_ws_id,
                 --
                 case when inner.match_count > 1 then
                         null
                      else
                         inner.reason_code_id
                 end reason_code_id,
                 --
                 case when inner.match_count > 1 then
                         null
                      else
                         inner.reason_code_desc
                 end reason_code_desc,
                 --
                 case when inner.match_count > 1 then
                         'Y'
                      else
                         'N'
                 end multi_reason_code_id_ind,
                 --
                 case when inner.action_count > 1 then
                         null
                      else
                         inner.action
                 end action
            from (select i.ancestor_id,
                         a.reason_code_id,
                         rc.reason_code_desc,
                         rc.action,
                         count(distinct a.reason_code_id) over (partition by i.ancestor_id) match_count,
                         count(distinct rc.action) over (partition by i.ancestor_id) action_count,
                         row_number() over (partition by i.ancestor_id order by a.reason_code_id) row_nbr
                    from im_match_inq_detail_ws i,
                         im_resolution_action a,
                         im_reason_codes rc
                   where i.workspace_id   = O_workspace_id
                     and i.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                     and i.invoice_id     = a.doc_id
                     and i.item           = a.item
                     and a.reason_code_id = rc.reason_code_id) inner 
           where inner.row_nbr = 1) use_this
   on (    target.workspace_id           = O_workspace_id
       and target.match_inq_detail_ws_id = use_this.match_inq_detail_ws_id
       and target.entity_type            = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.reason_code_id           = use_this.reason_code_id,
             target.reason_code_desc         = use_this.reason_code_desc,
             target.multi_reason_code_id_ind = use_this.multi_reason_code_id_ind,
             target.action                   = use_this.action;

   LOGGER.LOG_INFORMATION(L_program||' Reason code info to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CREATE_MATCH_INQ_DETAIL_WS;
----------------------------------------------------------------
END REIM_MATCH_INQUIRY_SQL;
/
