CREATE OR REPLACE PACKAGE BODY REIM_POSTING_ACCOUNT_SQL as

----------------------------------------------------------------------------------------------
/**
 *  --------- PUBLIC PROCS AND FUNCTIONS BODIES --------------
 */
----------------------------------------------------------------------------------------------
/**    
 * The public function used for loading Posting accounts. Dynamic segments are evaluated if required. 
 *
 * Input param: I_posting_id (The ID of the Posting Process)
 *              I_force_dynamic (Indicator which instructs the function to load dynamic values regardless of IM_GL_OPTIONS' Dynamic Indicator)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION LOAD_POSTING_ACCOUNTS(O_error_message    OUT VARCHAR2,
                               I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                               I_force_dynamic IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'REIM_POSTING_ACCOUNT_SQL.LOAD_POSTING_ACCOUNTS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ': Start - I_posting_id: '  || I_posting_id
                          || ' I_force_dynamic: ' || I_force_dynamic);

   if REIM_POSTING_SQL.UPDATE_DEFAULT_DEPT_CLASS(O_error_message,
                                                 I_posting_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REIM_POSTING_SQL.UPDATE_IMPORTER_LOCATIONS(O_error_message,
                                                 I_posting_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   /*Update the possible account_type and acct code in im_posting_doc_amounts for the
     amount type .
     Default Mapping between amount type and account
     --------------------------------------------------------------------------
     | AmountType                |Account Type|    Account Code
     --------------------------------------------------------------------------
     |doc.header.cost             BT            TAP
     |invoice.non.merch.cost      NMC           (non-merchandise code)
     |variance.within.tolerance   BT            VWT
     |matched.receipt.cost        BT            UNR
     |resolution.action           RCA           (reason code )
     |resolution.doc.nonmerch     NMC           (non-merchandise code)
     |fixed.deal                  BT            DIRAF
     |complex.deal                BT            DIRAR
     |crdnt.dwo                   BT            TAP
     |crdnt.dwo.offset            BT            TAP
     |crdnt.vwt                   BT            VWT
     |crdnt.vwt.offset            BT            VWT
     --------------------------------------------------------------------------
   */

   /*load the accounts in to temporary table in the format segment no, seg val (1,segment1
                                                                                2,segment2
                                                                                3,segment3
                                                                                ....)
     which would later be converted to the format segment1,segment2,segment3....*/
   delete from im_posting_acct_gtt;
   insert into im_posting_acct_gtt(posting_amt_id,
                                   account_type ,
                                   account_code ,
                                   set_of_books_id,
                                   segment_no ,
                                   segment_value,
                                   tax_code)
                            select posting_amt_id,
                                   account_type,
                                   account_code,
                                   set_of_books_id,
                                   segment_no,
                                   segment_value,
                                   tax_code
                              from (--apply the dynamic segment value to all the accounts to be posted
                                    select acc.posting_amt_id,
                                           igcr.account_type,
                                           igcr.account_code,
                                           igcr.set_of_books_id,
                                           igcr.segment_no,
                                           case
                                              when (igcr.segment_value is NULL or I_force_dynamic = REIM_CONSTANTS.YN_YES) then
                                                 case
                                                    when (igo.dynamic_ind = REIM_CONSTANTS.YN_YES or I_force_dynamic = REIM_CONSTANTS.YN_YES) then
                                                       case igo.business_attribute
                                                          when REIM_CONSTANTS.BUSINESS_ATTRIB_COMP then
                                                             (select idsl.company_segment
                                                                from im_dynamic_segment_loc idsl
                                                               where  idsl.location = acc.location)
                                                          when REIM_CONSTANTS.BUSINESS_ATTRIB_LOC then
                                                             (select idsl.loc_segment
                                                                from im_dynamic_segment_loc idsl
                                                               where idsl.location = acc.location)
                                                          when REIM_CONSTANTS.BUSINESS_ATTRIB_DEPT then
                                                             (select idsd.dept_segment
                                                                from im_dynamic_segment_dept idsd
                                                               where idsd.dept            = acc.dept
                                                                 and idsd.set_of_books_id = acc.set_of_books_id)
                                                          when REIM_CONSTANTS.BUSINESS_ATTRIB_CLASS then
                                                             (select idsc.class_segment
                                                                from im_dynamic_segment_class idsc
                                                               where idsc.dept            = acc.dept
                                                                 and idsc.class           = acc.class
                                                                 and idsc.set_of_books_id = acc.set_of_books_id)
                                                          else null
                                                       end
                                                    else null
                                                 end
                                              else igcr.segment_value
                                           end segment_value,
                                           igcr.tax_code
							          from (--retrieves all possible combination of account type and account code from im_posting_doc_amounts.
							                select ipda.posting_amt_id,
							                       ipda.posting_id,
							                       ipda.doc_id,
							                       ipda.amount_type,
							                       ipda.tax_code,
							                       ipda.location,
							                       ipda.set_of_books_id,
							                       ipda.account_type,
							                       ipda.account_code,
							                       NVL(ipda.dept, ipd.default_dept) dept,
							                       NVL(ipda.class, ipd.default_class) class
							                  from im_posting_doc ipd,
							                       im_posting_doc_amounts ipda
							                 where ipd.posting_id = I_posting_id
							                   and ipd.posting_id = ipda.posting_id
							                   and ipda.doc_id    = ipd.doc_id
							                   
                                            union all
							                --amount type 'document.header.cost' can be associated to
							                --account type and account code 'BT' 'PPA'.
							                select ipda.posting_amt_id,
							                       ipda.posting_id,
							                       ipda.doc_id,
							                       ipda.amount_type,
							                       ipda.tax_code,
							                       ipd.location,
							                       ipda.set_of_books_id,
							                       ipda.account_type,
							                       REIM_CONSTANTS.PRE_PAID_ASSET,
							                       NVL(ipda.dept, ipd.default_dept) dept,
							                       NVL(ipda.class, ipd.default_class) class
							                  from im_posting_doc ipd,
							                       im_posting_doc_amounts ipda
							                 where ipd.posting_id   = I_posting_id
							                   and ipd.posting_id   = ipda.posting_id
							                   and ipda.doc_id      = ipd.doc_id
							                   AND IPD.DOC_TYPE <> REIM_CONSTANTS.DOC_TYPE_NMRCHI
							                   and ipda.amount_type = REIM_POSTING_SQL.AMT_DOCUMENT_HEADER_COST
							                union all
							                --amount type 'document.header.cost' can be associated to 
							                --account type and account code 'BT' 'TAPNDI'.
							                select ipda.posting_amt_id,
							                       ipda.posting_id,
							                       ipda.doc_id,
							                       ipda.amount_type,
							                       ipda.tax_code,
							                       ipd.location,
							                       ipda.set_of_books_id,
							                       ipda.account_type,
							                       REIM_CONSTANTS.TAP_NON_DYNAMIC,
							                       NVL(ipda.dept, ipd.default_dept) dept,
							                       NVL(ipda.class, ipd.default_class) class
							                  from im_posting_doc ipd,
							                       im_posting_doc_amounts ipda
							                 where ipd.posting_id   = I_posting_id
							                   and ipd.posting_id   = ipda.posting_id
							                   and ipda.doc_id      = ipd.doc_id
							                   
							                   and ipda.amount_type = REIM_POSTING_SQL.AMT_DOCUMENT_HEADER_COST
							                union all
							                --amount type 'doc.merch.cost' can be associated to the
							                --account type and account code are 'BT' 'CRNNDI'.
							                select ipda.posting_amt_id,
							                       ipda.posting_id,
							                       ipda.doc_id,
							                       ipda.amount_type,
							                       ipda.tax_code,
							                       ipda.location,
							                       ipda.set_of_books_id,
							                       ipda.account_type,
							                       REIM_CONSTANTS.CREDIT_NOTE_NON_DYNAMIC,
							                       NVL(ipda.dept, ipd.default_dept) dept,
							                       NVL(ipda.class, ipd.default_class) class
							                  from im_posting_doc ipd,
							                       im_posting_doc_amounts ipda
							                 where ipd.posting_id   = I_posting_id
							                   and ipd.posting_id   = ipda.posting_id
							                   and ipda.doc_id      = ipd.doc_id
							                   
							                   and ipda.amount_type = REIM_POSTING_SQL.AMT_MERCHANDISE_COST
							                union all
							                --amount type 'doc.merch.cost' can be associated to
							                --account type and account code are 'BT' 'CRN'.
							                select ipda.posting_amt_id,
							                       ipda.posting_id,
							                       ipda.doc_id,
							                       ipda.amount_type,
							                       ipda.tax_code,
							                       ipda.location,
							                       ipda.set_of_books_id,
							                       ipda.account_type,
							                       REIM_CONSTANTS.CREDIT_NOTE,
							                       NVL(ipda.dept, ipd.default_dept) dept,
							                       NVL(ipda.class, ipd.default_class) class
							                  from im_posting_doc ipd,
							                       im_posting_doc_amounts ipda
							                 where ipd.posting_id   = I_posting_id
							                   and ipd.posting_id   = ipda.posting_id
							                   and ipda.doc_id      = ipd.doc_id
							                   
							                   and ipda.amount_type = REIM_POSTING_SQL.AMT_MERCHANDISE_COST
							                union all
							                --amount type 'doc.merch.cost' can be associated to the
							                --account type and account code are 'BT' 'RWO'.
							                select ipda.posting_amt_id,
							                       ipda.posting_id,
							                       ipda.doc_id,
							                       ipda.amount_type,
							                       ipda.tax_code,
							                       ipda.location,
							                       ipda.set_of_books_id,
							                       ipda.account_type,
							                       REIM_CONSTANTS.RECEIPT_WRITE_OFF,
							                       NVL(ipda.dept, ipd.default_dept) dept,
							                       NVL(ipda.class, ipd.default_class) class
							                  from im_posting_doc ipd,
							                       im_posting_doc_amounts ipda
							                 where ipd.posting_id   = I_posting_id
							                   and ipd.posting_id   = ipda.posting_id
							                   and ipda.doc_id      = ipd.doc_id
							                   
							                   and ipd.doc_type     = REIM_POSTING_SQL.DOC_TYPE_RECEIPT_WRITEOFF
							                   and ipda.amount_type = REIM_POSTING_SQL.AMT_MERCHANDISE_COST) acc,
                                           im_gl_options igo,
                                           v_im_gl_cross_ref igcr
                                     where acc.account_type                                               = igcr.account_type
                                       and acc.account_code                                               = igcr.account_code
                                       -- amount_types other than 'tax.amount' , 'acquisition.vat' , 'reverse.charge.vat' , 'acquisition.vat.offset' , 'reverse.charge.vat.offset' 
                                       -- should be joined to igcr with NULL TAX_CODE
                                       and DECODE(acc.amount_type,
                                                  REIM_POSTING_SQL.AMT_TAX,                 acc.tax_code,
                                                  REIM_POSTING_SQL.AMT_ACQ_VAT,             acc.tax_code,
                                                  REIM_POSTING_SQL.AMT_ACQ_VAT_OFFSET,      acc.tax_code,
                                                  REIM_POSTING_SQL.AMT_REV_CHRG_VAT,        acc.tax_code,
                                                  REIM_POSTING_SQL.AMT_REV_CHRG_VAT_OFFSET, acc.tax_code,
                                                  'NoCode')                                               = NVL(igcr.tax_code,'NoCode')
                                       and acc.set_of_books_id                                            = igo.set_of_books_id
                                       and igo.set_of_books_id                                            = igcr.set_of_books_id
                                       and igo.segment_no                                                 = igcr.segment_no
                                       and acc.posting_id                                                 = I_posting_id);

   LOGGER.LOG_INFORMATION(L_program||' insert im_posting_acct_gtt Step 1 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   
   -- Additional insert for the AMOUNT_TYPES 'tax.amount' , 'acquisition.vat' , 'reverse.charge.vat' , 'acquisition.vat.offset' , 'reverse.charge.vat.offset' 
   -- for those TAX_CODES which do not have their segments specified in IGCR
   insert into im_posting_acct_gtt(posting_amt_id,
	                               account_type ,
	                               account_code ,
	                               set_of_books_id,
	                               segment_no ,
	                               segment_value,
	                               tax_code)
					        select posting_amt_id,
					               account_type,
					               account_code,
					               set_of_books_id,
					               segment_no,
					               segment_value,
					               tax_code
					          from (--apply the dynamic segment value to all the accounts to be posted
					                select acc.posting_amt_id,
                                           igcr.account_type,
					                       igcr.account_code,
					                       igcr.set_of_books_id,
					                       igcr.segment_no,
					                       case
                                              when (igcr.segment_value is NULL or I_force_dynamic = REIM_CONSTANTS.YN_YES) then
                                                 case
                                                    when (igo.dynamic_ind = REIM_CONSTANTS.YN_YES or I_force_dynamic = REIM_CONSTANTS.YN_YES) then
                                                       case igo.business_attribute
                                                          when REIM_CONSTANTS.BUSINESS_ATTRIB_COMP then
                                                             (select idsl.company_segment
                                                                from im_dynamic_segment_loc idsl
                                                               where  idsl.location = acc.location)
                                                          when REIM_CONSTANTS.BUSINESS_ATTRIB_LOC then
                                                             (select idsl.loc_segment
                                                                from im_dynamic_segment_loc idsl
                                                               where idsl.location = acc.location)
                                                          when REIM_CONSTANTS.BUSINESS_ATTRIB_DEPT then
                                                             (select idsd.dept_segment
                                                                from im_dynamic_segment_dept idsd
                                                               where idsd.dept            = acc.dept
                                                                 and idsd.set_of_books_id = acc.set_of_books_id)
                                                          when REIM_CONSTANTS.BUSINESS_ATTRIB_CLASS then
                                                             (select idsc.class_segment
                                                                from im_dynamic_segment_class idsc
                                                               where idsc.dept            = acc.dept
                                                                 and idsc.class           = acc.class
                                                                 and idsc.set_of_books_id = acc.set_of_books_id)
                                                          else null
                                                       end
                                                    else null
                                                 end
                                              else igcr.segment_value
                                           end segment_value,
					                       igcr.tax_code
					                  from (select ipda.posting_amt_id,
					                               ipda.posting_id,
					                               ipda.doc_id,
					                               ipda.amount_type,
					                               NULL tax_code,
					                               ipda.location,
					                               ipda.set_of_books_id,
					                               ipda.account_type,
					                               ipda.account_code,
					                               NVL(ipda.dept, ipd.default_dept) dept,
							                       NVL(ipda.class, ipd.default_class) class
					                          from im_posting_doc ipd,
					                               im_posting_doc_amounts ipda
					                         where ipd.posting_id   = I_posting_id
					                           and ipd.posting_id   = ipda.posting_id
					                           and ipda.doc_id      = ipd.doc_id
					                           
					                           and ipda.amount_type IN (REIM_POSTING_SQL.AMT_TAX,
					                                                    REIM_POSTING_SQL.AMT_ACQ_VAT,
					                                                    REIM_POSTING_SQL.AMT_ACQ_VAT_OFFSET,
					                                                    REIM_POSTING_SQL.AMT_REV_CHRG_VAT,
					                                                    REIM_POSTING_SQL.AMT_REV_CHRG_VAT_OFFSET)
					                           and NOT EXISTS (select 'x' 
					                                             from im_posting_acct_gtt ipa_gtt
					                                            where ipa_gtt.posting_amt_id = ipda.posting_amt_id
					                                              and ipa_gtt.account_code = ipda.account_code)) acc,
					                       im_gl_options igo,
					                       v_im_gl_cross_ref igcr
					                 where acc.account_type            = igcr.account_type
					                   and acc.account_code            = igcr.account_code
					                   and NVL(acc.tax_code, 'NoCoDe') = NVL(igcr.tax_code, 'NoCoDe')
					                   and acc.set_of_books_id         = igo.set_of_books_id
					                   and igo.set_of_books_id         = igcr.set_of_books_id
					                   and igo.segment_no              = igcr.segment_no
					                   and acc.posting_id              = I_posting_id);

   LOGGER.LOG_INFORMATION(L_program||' insert im_posting_acct_gtt Step 2 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_posting_doc_accounts(segment1,
                                       segment2,
                                       segment3,
                                       segment4,
                                       segment5,
                                       segment6,
                                       segment7,
                                       segment8,
                                       segment9,
                                       segment10,
                                       segment11,
                                       segment12,
                                       segment13,
                                       segment14,
                                       segment15,
                                       segment16,
                                       segment17,
                                       segment18,
                                       segment19,
                                       segment20,
                                       account_type,
                                       account_code,
                                       status,
                                       posting_amt_id,
                                       set_of_books_id,
                                       tax_code)
                                select NVL(segment1,'NO_VALUE'),
                                       NVL(segment2,'NO_VALUE'),
                                       NVL(segment3,'NO_VALUE'),
                                       NVL(segment4,'NO_VALUE'),
                                       NVL(segment5,'NO_VALUE'),
                                       NVL(segment6,'NO_VALUE'),
                                       NVL(segment7,'NO_VALUE'),
                                       NVL(segment8,'NO_VALUE'),
                                       NVL(segment9,'NO_VALUE'),
                                       NVL(segment10,'NO_VALUE'),
                                       NVL(segment11,'NO_VALUE'),
                                       NVL(segment12,'NO_VALUE'),
                                       NVL(segment13,'NO_VALUE'),
                                       NVL(segment14,'NO_VALUE'),
                                       NVL(segment15,'NO_VALUE'),
                                       NVL(segment16,'NO_VALUE'),
                                       NVL(segment17,'NO_VALUE'),
                                       NVL(segment18,'NO_VALUE'),
                                       NVL(segment19,'NO_VALUE'),
                                       NVL(segment20,'NO_VALUE'),
                                       account_type,
                                       account_code,
                                       REIM_CONSTANTS.POST_ACCT_STATUS_VALIDATE status,
                                       posting_amt_id,
                                       set_of_books_id,
                                       tax_code
                                  from (select account_type,
                                               account_code,
                                               posting_amt_id,
                                               set_of_books_id,
                                               tax_code,
                                               segment_no,
                                               MAX(segment_value) segment_value
                                          from im_posting_acct_gtt
                                         group by account_type,
                                               account_code,
                                               posting_amt_id,
                                               set_of_books_id,
                                               tax_code,
                                               segment_no)
                                 PIVOT (MAX(segment_value) FOR segment_no IN (1 segment1, 2 segment2, 3 segment3, 4 segment4, 5 segment5,
                                                                              6 segment6, 7 segment7, 8 segment8, 9 segment9, 10 segment10,
                                                                              11 segment11, 12 segment12, 13 segment13, 14 segment14, 15 segment15,
                                                                              16 segment16, 17 segment17, 18 segment18, 19 segment19, 20 segment20));

   LOGGER.LOG_INFORMATION(L_program||' Pivot im_posting_acct_gtt into im_posting_doc_accounts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END LOAD_POSTING_ACCOUNTS;
--------------------------------------------------------------------------------
/**
 * The public function used to retrieve accounts for validation. 
 * It then Validates the accounts against IM_VALID_ACCOUNTS.
 * If not VALID, collects them and sends them back for validation  
 *
 * Output param: O_accounts (Collection of accounts that require validation)
 *
 * Input param: I_posting_id (The ID of the Posting Process)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION RETRIEVE_ACCTS_FOR_VALIDATION(O_error_message    OUT VARCHAR2,
                                       O_accounts         OUT OBJ_REIM_POSTING_ACCT_TBL,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'REIM_POSTING_ACCOUNT_SQL.RETRIEVE_ACCTS_FOR_VALIDATION';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   cursor C_FETCH_ACCT is
      select obj_reim_posting_acct_rec(ipda.segment1,
		                               ipda.segment2,
		                               ipda.segment3,
		                               ipda.segment4,
		                               ipda.segment5,
		                               ipda.segment6,
		                               ipda.segment7,
		                               ipda.segment8,
		                               ipda.segment9,
		                               ipda.segment10,
		                               ipda.segment11,
		                               ipda.segment12,
		                               ipda.segment13,
		                               ipda.segment14,
		                               ipda.segment15,
		                               ipda.segment16,
		                               ipda.segment17,
		                               ipda.segment18,
		                               ipda.segment19,
		                               ipda.segment20,
		                               ipda.set_of_books_id,
		                               ipda.account_type,
		                               ipda.account_code,
		                               ipda.tax_code,
		                               status,
		                               ipda.posting_amt_id)
				                  from im_posting_doc_accounts ipda
				                 where ipda.status = REIM_CONSTANTS.POST_ACCT_STATUS_VALIDATE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ': Start - I_posting_id: '  || I_posting_id);

   merge into im_posting_doc_accounts tgt
   using (select distinct iva.set_of_books_id,
                 iva.account_type,
                 iva.account_code,
                 iva.tax_code,
                 iva.segment1,
                 iva.segment2,
                 iva.segment3,
                 iva.segment4,
                 iva.segment5,
                 iva.segment6,
                 iva.segment7,
                 iva.segment8,
                 iva.segment9,
                 iva.segment10,
                 iva.segment11,
                 iva.segment12,
                 iva.segment13,
                 iva.segment14,
                 iva.segment15,
                 iva.segment16,
                 iva.segment17,
                 iva.segment18,
                 iva.segment19,
                 iva.segment20
            from im_valid_accounts iva) src
   on (    tgt.set_of_books_id         = src.set_of_books_id
       and tgt.account_type            = src.account_type
       and tgt.account_code            = src.account_code
       and NVL(tgt.tax_code, 'NoCode') = NVL(src.tax_code, 'NoCode')
       and tgt.segment1                = src.segment1
       and tgt.segment2                = src.segment2
       and tgt.segment3                = src.segment3
       and tgt.segment4                = src.segment4
       and tgt.segment5                = src.segment5
       and tgt.segment6                = src.segment6
       and tgt.segment7                = src.segment7
       and tgt.segment8                = src.segment8
       and tgt.segment9                = src.segment9
       and tgt.segment10               = src.segment10
       and tgt.segment11               = src.segment11
       and tgt.segment12               = src.segment12
       and tgt.segment13               = src.segment13
       and tgt.segment14               = src.segment14
       and tgt.segment15               = src.segment15
       and tgt.segment16               = src.segment16
       and tgt.segment17               = src.segment17
       and tgt.segment18               = src.segment18
       and tgt.segment19               = src.segment19
       and tgt.segment20               = src.segment20)
   when MATCHED then
      update
         set tgt.status = REIM_CONSTANTS.POST_ACCT_STATUS_VALID
       where tgt.status = REIM_CONSTANTS.POST_ACCT_STATUS_VALIDATE;

   LOGGER.LOG_INFORMATION(L_program||' Merge status into im_posting_doc_accounts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Collect all accounts that require validation
   open C_FETCH_ACCT;
   fetch C_FETCH_ACCT BULK COLLECT INTO O_accounts;
   close C_FETCH_ACCT;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END RETRIEVE_ACCTS_FOR_VALIDATION;
--------------------------------------------------------------------------------
/**    
 * The public function used to update status of accounts in IM_POSTING_DOC_ACCOUNTS based on input collection of accounts.
 * It raises a posting error if the status is not VALID
 *
 * Input param: I_posting_id (The ID of the Posting Process)
 *              I_accounts (Collection of accounts used to update IM_POSTING_DOC_ACCOUNTS)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION UPDATE_POSTING_ACCOUNTS_STATUS(O_error_message    OUT VARCHAR2,
                                        I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                        I_accounts      IN     OBJ_REIM_POSTING_ACCT_TBL)

RETURN NUMBER IS

   L_program  VARCHAR2(60) := 'REIM_POSTING_ACCOUNT_SQL.UPDATE_POSTING_ACCOUNTS_STATUS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_user IM_POSTING_STATUS.CREATED_BY%TYPE := get_user;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ': Start - I_posting_id: '  || I_posting_id);

   merge into im_posting_doc_accounts tgt
   using (select acct_tbl.set_of_books_id,
                 acct_tbl.posting_amt_id,
                 acct_tbl.account_type,
                 acct_tbl.account_code,
                 acct_tbl.tax_code,
                 acct_tbl.segment1,
                 acct_tbl.segment2,
                 acct_tbl.segment3,
                 acct_tbl.segment4,
                 acct_tbl.segment5,
                 acct_tbl.segment6,
                 acct_tbl.segment7,
                 acct_tbl.segment8,
                 acct_tbl.segment9,
                 acct_tbl.segment10,
                 acct_tbl.segment11,
                 acct_tbl.segment12,
                 acct_tbl.segment13,
                 acct_tbl.segment14,
                 acct_tbl.segment15,
                 acct_tbl.segment16,
                 acct_tbl.segment17,
                 acct_tbl.segment18,
                 acct_tbl.segment19,
                 acct_tbl.segment20,
                 acct_tbl.status
            from table(cast(I_accounts as OBJ_REIM_POSTING_ACCT_TBL)) acct_tbl) src
   on (    tgt.posting_amt_id          = src.posting_amt_id
       and tgt.set_of_books_id         = src.set_of_books_id
       and tgt.account_type            = src.account_type
       and tgt.account_code            = src.account_code
       and NVL(tgt.tax_code, 'NoCode') = NVL(src.tax_code, 'NoCode')
       and tgt.segment1                = src.segment1
       and tgt.segment2                = src.segment2
       and tgt.segment3                = src.segment3
       and tgt.segment4                = src.segment4
       and tgt.segment5                = src.segment5
       and tgt.segment6                = src.segment6
       and tgt.segment7                = src.segment7
       and tgt.segment8                = src.segment8
       and tgt.segment9                = src.segment9
       and tgt.segment10               = src.segment10
       and tgt.segment11               = src.segment11
       and tgt.segment12               = src.segment12
       and tgt.segment13               = src.segment13
       and tgt.segment14               = src.segment14
       and tgt.segment15               = src.segment15
       and tgt.segment16               = src.segment16
       and tgt.segment17               = src.segment17
       and tgt.segment18               = src.segment18
       and tgt.segment19               = src.segment19
       and tgt.segment20               = src.segment20)
   when MATCHED then
      update
         set tgt.status = src.status;

   LOGGER.LOG_INFORMATION(L_program||' Merge status into im_posting_doc_accounts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Populate gtt with documents with invalid accounts
   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date(number_1,
                                             number_2)
                              select ipd.posting_id,
                                     ipd.doc_id
                                from im_posting_doc ipd,
                                     im_posting_doc_amounts ipdamt,
                                     im_posting_doc_accounts ipdacct
                               where ipd.posting_id         = I_posting_id
                                 and ipdamt.doc_id          = ipd.doc_id
                                 and ipdamt.posting_id      = ipd.posting_id
                                 and ipdacct.posting_amt_id = ipdamt.posting_amt_id
                                 
                                 and ipdacct.status         <> REIM_CONSTANTS.POST_ACCT_STATUS_VALID
                               GROUP BY ipd.posting_id,
                                        ipd.doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Populate gtt for documents with invalid accounts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   
   --Add Posting errors if account is NOT VALID
   insert into im_posting_doc_errors(posting_id,
                                     doc_id,
                                     severity,
                                     error_msg,
                                     category,
                                     creation_date,
                                     created_by,
                                     last_updated_by,
                                     last_update_date,
                                     object_version_id)
                              select gtt.number_1,
                                     gtt.number_2,
                                     REIM_POSTING_SQL.SEVERITY_ERROR severity,
                                     'One or more accounts subjected to posting is invalid' error_msg,
                                     REIM_CONSTANTS.POST_ERR_CAT_ACCOUNT,
                                     sysdate,
                                     L_user,
                                     L_user,
                                     sysdate,
                                     REIM_CONSTANTS.ONE
                                from gtt_num_num_str_str_date_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' Populate posting doc errors for documents with invalid accounts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_posting_doc tgt
   using (select gtt.number_1 posting_id,
                 gtt.number_2 doc_id
            from gtt_num_num_str_str_date_date gtt) src
   on (    tgt.posting_id = src.posting_id
       and tgt.doc_id     = src.doc_id)
   when MATCHED then
      update
         set tgt.has_errors        = REIM_CONSTANTS.YN_YES;

   LOGGER.LOG_INFORMATION(L_program||' Merge Posting doc has_errors for documents with invalid accounts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_posting_status tgt
   using (select distinct gtt.number_1 posting_id
            from gtt_num_num_str_str_date_date gtt) src
   on (tgt.posting_id = src.posting_id)
   when MATCHED then
      update
         set tgt.has_errors        = REIM_CONSTANTS.YN_YES,
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Posting status has_errors for posting Ids with invalid accounts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END UPDATE_POSTING_ACCOUNTS_STATUS;
--------------------------------------------------------------------------------
END REIM_POSTING_ACCOUNT_SQL;
/
