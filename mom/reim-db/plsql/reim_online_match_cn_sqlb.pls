CREATE OR REPLACE PACKAGE BODY REIM_ONLINE_MATCH_CN_SQL AS
----------------------------------------------------------------
----------------------------------------------------------------
FUNCTION CREATE_CNR_GROUPS(O_error_message IN OUT VARCHAR2,
                           I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE,
                           I_match_key_id  IN     IM_MATCH_CNR_WS.MATCH_KEY_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION CREATE_MTCH_GROUP_HEAD(O_error_message     IN OUT VARCHAR2,
                                I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION MATCH_GROUP_HEAD(O_error_message     IN OUT VARCHAR2,
                          I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
FUNCTION UNMATCH_TAX_DISCREPANT(O_error_message     IN OUT VARCHAR2,
                                I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
/**
 *  --------- PUBLIC PROCS AND FUNCTIONS BODIES --------------
 */
----------------------------------------------------------------
FUNCTION CREATE_MATCH_CN_DETAIL_WS(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN OUT IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_CN_SQL.CREATE_MATCH_CN_DETAIL_WS';
   L_start_time  TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program || ' I_workspace_id:' || I_workspace_id);

   ------------------------------------------------
   --put filtered merch hiers to the side, will reapply filter on the new records at the end.
   ------------------------------------------------
   --NUMBER_1 - dept
   --NUMBER_2 - class
   --NUMBER_3 - subclass
   insert into gtt_6_num_6_str_6_date(NUMBER_1,
                                      NUMBER_2,
                                      NUMBER_3)
   select distinct d.dept,
                   d.class,
                   d.subclass
     from im_detail_match_cn_ws d
    where workspace_id    = I_workspace_id
      and d.ui_filter_ind = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' save ui filter merch hier for workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --push choice flags from workspace headers to detail
   ------------------------------------------------

   merge into im_match_cnr_detl_ws tgt
   using (select d.workspace_id,
                 d.doc_id,
                 d.choice_flag
            from im_match_cnr_ws d
           where d.workspace_id = I_workspace_id
   ) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED THEN
      update
         set tgt.choice_flag = src.choice_flag;

   LOGGER.LOG_INFORMATION(L_program||' push choice flag down to im_match_cnr_detl_ws: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_detl_ws tgt
   using (select d.workspace_id,
                 d.doc_id,
                 d.choice_flag
            from im_match_cn_ws d
           where d.workspace_id = I_workspace_id
   ) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED THEN
      update
         set tgt.choice_flag = src.choice_flag;

   LOGGER.LOG_INFORMATION(L_program||' push choice flag down to im_match_cn_detl_ws: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
 
   ------------------------------------------------
   --clear out workspace
   ------------------------------------------------
   delete from im_detail_match_cn_ws where workspace_id = I_workspace_id;
   LOGGER.LOG_INFORMATION(L_program||' clear workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --TOP LEVEL STYLE or SKU
   ------------------------------------------------
   insert into im_detail_match_cn_ws (detail_match_cn_ws_id,
                                      workspace_id,
                                      ancestor_id,
                                      match_status,
                                      item_parent,
                                      item,
                                      item_description,
                                      entity_type,
                                      dept,
                                      class,
                                      subclass,
                                      selected_cnr_count,
                                      selected_cn_count,
                                      choice_flag)
      with items as (select distinct i.workspace_id,
                                     i.item
                       from im_match_cnr_detl_ws i
                      where i.workspace_id = I_workspace_id
                        and i.choice_flag  = 'Y'
                     union 
                     select distinct r.workspace_id,
                                     r.item
                       from im_match_cn_detl_ws r  
                      where r.workspace_id = I_workspace_id
                        and r.choice_flag  = 'Y'
                    )
      select im_detail_match_cn_ws_seq.nextval,
             inner.workspace_id,
             inner.ancestor_id,
             inner.match_status,
             inner.item_parent,
             inner.item,
             inner.item_description,
             inner.entity_type,
             inner.dept,
             inner.class,
             inner.subclass,
             0 selected_cnr_count,
             0 selected_cn_count,
             inner.choice_flag
        from (select distinct I_workspace_id workspace_id,
                     null ancestor_id,
                     null match_status,
                     null item_parent,
                     nvl(im.item_parent, im.item) item,
                     nvl(imp.item_desc, im.item_desc) item_description,
                     decode(im.item_parent,
                            null, REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU,
                            REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_STYLE) entity_type,
                     --
                     im.dept,
                     im.class,
                     im.subclass,
                     --
                     'N' choice_flag
                     --
                from items,
                     item_master im,
                     item_master imp
               where items.item     = im.item
                 and im.item_parent = imp.item(+)
              ) inner;

   LOGGER.LOG_INFORMATION(L_program||' TOP LEVEL STYLE or SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --SKU UNDER STYLE
   ------------------------------------------------
   insert into im_detail_match_cn_ws (detail_match_cn_ws_id,
                                      workspace_id,
                                      ancestor_id,
                                      match_status,
                                      item_parent,
                                      item,
                                      item_description,
                                      entity_type,
                                      dept,
                                      class,
                                      subclass,
                                      selected_cnr_count,
                                      selected_cn_count,
                                      choice_flag)
      with style as (select distinct /*+ ORDERED */ 
                                     d.workspace_id,
                                     im.item_parent,
                                     cnr.item,
                                     d.detail_match_cn_ws_id ancestor_id,
                                     im.dept,
                                     im.class,
                                     im.subclass
                       from im_detail_match_cn_ws d,
                            item_master im,
                            im_match_cnr_detl_ws cnr
                      where d.workspace_id   = I_workspace_id
                        and d.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_STYLE
                        and d.item           = im.item_parent
                        and im.item          = cnr.item
                        and cnr.choice_flag  = 'Y'
                        and cnr.workspace_id = I_workspace_id
                     union
                     select distinct /*+ ORDERED */ 
                                     d.workspace_id,
                                     im.item_parent,
                                     cn.item,
                                     d.detail_match_cn_ws_id ancestor_id,
                                     im.dept,
                                     im.class,
                                     im.subclass
                       from im_detail_match_cn_ws d,
                            item_master im,
                            im_match_cn_detl_ws cn
                      where d.workspace_id = I_workspace_id
                        and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_STYLE
                        and d.item         = im.item_parent
                        and im.item        = cn.item
                        and cn.choice_flag  = 'Y'
                        and cn.workspace_id = I_workspace_id)
      select im_detail_match_cn_ws_seq.nextval,
             inner.workspace_id,
             inner.ancestor_id,
             inner.match_status,
             inner.item_parent,
             inner.item,
             inner.item_description,
             inner.entity_type,
             inner.dept,
             inner.class,
             inner.subclass,
             0 selected_cnr_count,
             0 selected_cn_count,
             inner.choice_flag
        from (select distinct I_workspace_id workspace_id,
                     style.ancestor_id,
                     null match_status,
                     style.item_parent,
                     style.item,
                     im.item_desc item_description,
                     REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU entity_type,
                     --
                     style.dept,
                     style.class,
                     style.subclass,
                     --
                     'N' choice_flag
                     --
                from style,
                     item_master im
               where style.item = im.item) inner;

   LOGGER.LOG_INFORMATION(L_program||' SKU UNDER STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --CN UNDER SKU
   ------------------------------------------------
   insert into im_detail_match_cn_ws (detail_match_cn_ws_id,
                                      workspace_id,
                                      ancestor_id,
                                      match_status,
                                      item_parent,
                                      item,
                                      item_description,
                                      vpn,
                                      entity_type,
                                      cn_id,
                                      cn_ext_doc_id,
                                      cn_order_no,
                                      cn_rtv_order_no,
                                      cn_unit_cost,
                                      cn_qty,
                                      cn_ext_cost,
                                      dept,
                                      class,
                                      subclass,
                                      tolerance_id,
                                      tolerance_exchange_rate,
                                      cost_matched,
                                      qty_matched,
                                      match_hist_id,
                                      manual_group_id,
                                      matched_filter,
                                      choice_flag,
                                      cn_supplier_group_id,
                                      cn_supplier,
                                      cn_supplier_name,
                                      cn_supplier_phone,
                                      cn_supplier_site_id,
                                      cn_supplier_site_name,
                                      cn_tax_code,
                                      cn_tax_rate,
                                      cn_tax_amount)
   select /*+ ORDERED */ im_detail_match_cn_ws_seq.nextval,
          dm.workspace_id,
          dm.detail_match_cn_ws_id ancestor_id,
          decode(cnd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'M', 'U') match_status,
          dm.item_parent,
          dm.item,
          dm.item_description,
          cnd.vpn,
          REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN entity_type,
          --
          cnd.doc_id cn_id,
          cn.ext_doc_id cn_ext_doc_id,
          cn.order_no cn_order_no,
          cn.rtv_order_no cn_rtv_order_no,
          cnd.unit_cost cn_unit_cost,
          cnd.doc_qty cn_qty,
          cnd.doc_qty * cnd.unit_cost cn_ext_cost,
          --
          dm.dept,
          dm.class,
          dm.subclass,
          --
          cn.tolerance_id,
          cn.tolerance_exchange_rate,
          cnd.cost_matched,
          cnd.qty_matched,
          --
          cnd.match_hist_id,
          mg.group_id manual_group_id,
          decode(cnd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'N', 'Y') matched_filter,
          --
          case when count(distinct nvl(cnd.unit_cost,-909098)) over (partition by dm.workspace_id, dm.item) = 1 and
                    count(distinct nvl(cnd.tax_code,'nUlL')) over (partition by dm.workspace_id, dm.item) = 1 and
                    decode(cnd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'M', 'U') = 'U' then
                'Y'
             else
                'N' 
          end choice_flag,
          --
          cn.supplier_group_id,
          cn.supplier,
          cn.supplier_name,
          cn.supplier_phone,
          cn.supplier_site_id,
          cn.supplier_site_name,
          cnd.tax_code,
          cnd.tax_rate,
          cnd.tax_amount
     from im_detail_match_cn_ws dm,
          im_match_cn_ws cn,
          im_match_cn_detl_ws cnd,
          im_cn_manual_groups mg
    where dm.workspace_id   = I_workspace_id
      and dm.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
      and dm.workspace_id   = cnd.workspace_id
      and dm.item           = cnd.item
      and cnd.choice_flag   = 'Y'
      --
      and cnd.workspace_id  = cn.workspace_id
      and cnd.doc_id        = cn.doc_id
      --
      and cnd.doc_id        = mg.doc_id(+);

   LOGGER.LOG_INFORMATION(L_program||' CN UNDER SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --CNR UNDER SKU
   ------------------------------------------------
   insert into im_detail_match_cn_ws (detail_match_cn_ws_id,
                                      workspace_id,
                                      ancestor_id,
                                      match_status,
                                      item_parent,
                                      item,
                                      item_description,
                                      vpn,
                                      entity_type,
                                      cnr_id,
                                      cnr_ext_doc_id,
                                      cnr_order_no,
                                      cnr_rtv_order_no,
                                      cnr_unit_cost,
                                      cnr_qty,
                                      cnr_ext_cost,
                                      dept,
                                      class,
                                      subclass,
                                      match_hist_id,
                                      manual_group_id,
                                      matched_filter,
                                      choice_flag,
                                      cnr_supplier_group_id,
                                      cnr_supplier,
                                      cnr_supplier_name,
                                      cnr_supplier_phone,
                                      cnr_supplier_site_id,
                                      cnr_supplier_site_name,
                                      cnr_tax_code,
                                      cnr_tax_rate,
                                      cnr_tax_amount)
   select /*+ ORDERED */ im_detail_match_cn_ws_seq.nextval,
          dm.workspace_id,
          dm.detail_match_cn_ws_id ancestor_id,
          decode(cnrd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'M', 'U') match_status,
          dm.item_parent,
          cnrd.item,
          dm.item_description,
          cnrd.vpn,
          REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR entity_type,
          --
          cnrd.doc_id cnr_id,
          cnr.ext_doc_id cnr_ext_doc_id,
          cnr.order_no cnr_order_no,
          cnr.rtv_order_no cnr_rtv_order_no,
          cnrd.unit_cost cnr_unit_cost,
          cnrd.doc_qty cnr_qty,
          (cnrd.unit_cost * cnrd.doc_qty) cnr_ext_cost,
          --
          dm.dept,
          dm.class,
          dm.subclass,
          cnrd.match_hist_id,
          mg.group_id manual_group_id,
          decode(cnrd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'N', 'Y') matched_filter,
          --
          case when count(distinct nvl(cnrd.unit_cost, -90908)) over (partition by dm.workspace_id, dm.item) = 1 and
                    count(distinct nvl(cnrd.tax_code, 'nUlL')) over (partition by dm.workspace_id, dm.item) = 1 and
                    decode(cnrd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'M', 'U') = 'U' then
                'Y'
             else
                'N'
          end choice_flag,
          --
          cnr.supplier_group_id,
          cnr.supplier,
          cnr.supplier_name,
          cnr.supplier_phone,
          cnr.supplier_site_id,
          cnr.supplier_site_name,
          cnrd.tax_code,
          cnrd.tax_rate,
          cnrd.tax_amount
     from im_detail_match_cn_ws dm,
          im_match_cnr_detl_ws cnrd,
          im_match_cnr_ws cnr,
          im_cn_manual_groups mg
    where dm.workspace_id   = I_workspace_id
      and dm.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
      and dm.workspace_id   = cnrd.workspace_id
      and dm.item           = cnrd.item
      and cnrd.choice_flag  = 'Y'
      --
      and cnrd.workspace_id = cnr.workspace_id
      and cnrd.doc_id       = cnr.doc_id
      --
      and cnrd.doc_id       = mg.doc_id(+);

   LOGGER.LOG_INFORMATION(L_program||' CNR UNDER SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);


   ------------------------------------------------
   --rollup status and what not from CNR to SKU
   ------------------------------------------------
   merge into im_detail_match_cn_ws target
   using (select i2.workspace_id,
                 i2.item,
                 i2.match_status,
                 i2.matched_filter,
                 decode(i.cnr_count, 1, i.cnr_id, null) cnr_id,
                 decode(i.cnr_count, 1, i.cnr_ext_doc_id, null) cnr_ext_doc_id,
                 decode(i.cnr_order_count, 1, i.cnr_order_no, null) cnr_order_no,
                 decode(i.cnr_rtv_order_count, 1, i.cnr_rtv_order_no, null) cnr_rtv_order_no,
                 decode(i.cost_count, 1, i.cnr_unit_cost, null) cnr_unit_cost,
                 i.cnr_qty,
                 i.cnr_ext_cost,
                 NVL(i.cnr_count, 0) selected_cnr_count,
                 --
                 decode(i.cnr_supplier_group_id_cnt,   1, i.cnr_supplier_group_id) cnr_supplier_group_id,
                 decode(i.cnr_supplier_cnt,            1, i.cnr_supplier) cnr_supplier,
                 decode(i.cnr_supplier_name_cnt,       1, i.cnr_supplier_name) cnr_supplier_name,
                 decode(i.cnr_supplier_phone_cnt,      1, i.cnr_supplier_phone) cnr_supplier_phone,
                 decode(i.cnr_supplier_site_id_cnt,    1, i.cnr_supplier_site_id) cnr_supplier_site_id,
                 decode(i.cnr_supplier_site_name_cnt,  1, i.cnr_supplier_site_name) cnr_supplier_site_name,
                 --
                 decode(i.cnr_tax_code_cnt,  1, i.cnr_tax_code) cnr_tax_code,
                 decode(i.cnr_tax_code_cnt,  1, i.cnr_tax_rate) cnr_tax_rate,
                 decode(i.cnr_tax_code_cnt,  1, i.cnr_tax_amount) cnr_tax_amount
            from (select d.workspace_id,
                         d.item,
                         min(d.cnr_id)
                            over (partition by d.workspace_id, d.item) cnr_id,
                         min(d.cnr_ext_doc_id)
                            over (partition by d.workspace_id, d.item) cnr_ext_doc_id,
                         min(d.cnr_unit_cost)
                            over (partition by d.workspace_id, d.item) cnr_unit_cost,
                         sum(d.cnr_qty)
                            over (partition by d.workspace_id, d.item) cnr_qty,
                         sum(d.cnr_ext_cost)
                            over (partition by d.workspace_id, d.item) cnr_ext_cost,
                         row_number()
                            over (partition by d.workspace_id, d.item order by d.detail_match_cn_ws_id) rownbr,
                         count(distinct d.cnr_id)
                            over (partition by d.workspace_id, d.item) cnr_count,
                         count(distinct d.cnr_unit_cost)
                            over (partition by d.workspace_id, d.item) cost_count,
                         --
                         min(distinct d.cnr_supplier_group_id)
                            over (partition by d.workspace_id, d.item) cnr_supplier_group_id,
                         min(distinct d.cnr_supplier)
                            over (partition by d.workspace_id, d.item) cnr_supplier,
                         min(distinct d.cnr_supplier_name)
                            over (partition by d.workspace_id, d.item) cnr_supplier_name,
                         min(distinct d.cnr_supplier_phone)
                            over (partition by d.workspace_id, d.item) cnr_supplier_phone,
                         min(distinct d.cnr_supplier_site_id)
                            over (partition by d.workspace_id, d.item) cnr_supplier_site_id,
                         min(distinct d.cnr_supplier_site_name)
                            over (partition by d.workspace_id, d.item) cnr_supplier_site_name,
                         --
                         count(distinct d.cnr_supplier_group_id)
                            over (partition by d.workspace_id, d.item) cnr_supplier_group_id_cnt,
                         count(distinct d.cnr_supplier)
                            over (partition by d.workspace_id, d.item) cnr_supplier_cnt,
                         count(distinct d.cnr_supplier_name)
                            over (partition by d.workspace_id, d.item) cnr_supplier_name_cnt,
                         count(distinct d.cnr_supplier_phone)
                            over (partition by d.workspace_id, d.item) cnr_supplier_phone_cnt,
                         count(distinct d.cnr_supplier_site_id)
                            over (partition by d.workspace_id, d.item) cnr_supplier_site_id_cnt,
                         count(distinct d.cnr_supplier_site_name)
                            over (partition by d.workspace_id, d.item) cnr_supplier_site_name_cnt,
                         --
                         min(distinct d.cnr_tax_code)
                            over (partition by d.workspace_id, d.item) cnr_tax_code,
                         min(distinct d.cnr_tax_rate)
                            over (partition by d.workspace_id, d.item) cnr_tax_rate,
                         sum(distinct d.cnr_tax_amount)
                            over (partition by d.workspace_id, d.item) cnr_tax_amount,
                         count(distinct d.cnr_tax_code)
                            over (partition by d.workspace_id, d.item) cnr_tax_code_cnt,
                         --
                         min(d.cnr_order_no)
                             over (partition by d.workspace_id, d.item) cnr_order_no,
                         min(d.cnr_rtv_order_no)
                             over (partition by d.workspace_id, d.item) cnr_rtv_order_no,
                         count(distinct d.cnr_order_no)
                               over (partition by d.workspace_id, d.item) cnr_order_count,
                         count(distinct d.cnr_rtv_order_no)
                               over (partition by d.workspace_id, d.item) cnr_rtv_order_count
                         --
                    from im_detail_match_cn_ws d
                   where d.workspace_id = I_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
                     and d.choice_flag  = 'Y') i,
                 --
                 (select d.workspace_id,
                         d.item,
                         max(d.match_status) match_status,
                         min(d.matched_filter) matched_filter
                    from im_detail_match_cn_ws d
                   where d.workspace_id = I_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
                   group by d.workspace_id, 
                            d.item) i2
           where 1               = i.rownbr(+)
             and i2.workspace_id = i.workspace_id(+)
             and i2.item         = i.item(+)) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.item         = use_this.item
       and target.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU)
   when MATCHED then
      update
         set target.match_status        = use_this.match_status,
             target.matched_filter      = use_this.matched_filter,
             target.cnr_id              = use_this.cnr_id,
             target.cnr_ext_doc_id      = use_this.cnr_ext_doc_id,
             target.cnr_order_no        = use_this.cnr_order_no,
             target.cnr_rtv_order_no    = use_this.cnr_rtv_order_no,
             target.cnr_unit_cost       = use_this.cnr_unit_cost,
             target.cnr_qty             = use_this.cnr_qty,
             target.cnr_ext_cost        = use_this.cnr_ext_cost,
             target.selected_cnr_count  = use_this.selected_cnr_count,
             --
             target.cnr_supplier_group_id   = use_this.cnr_supplier_group_id,
             target.cnr_supplier            = use_this.cnr_supplier,
             target.cnr_supplier_name       = use_this.cnr_supplier_name,
             target.cnr_supplier_phone      = use_this.cnr_supplier_phone,
             target.cnr_supplier_site_id    = use_this.cnr_supplier_site_id,
             target.cnr_supplier_site_name  = use_this.cnr_supplier_site_name,
             --
             target.cnr_tax_code            = use_this.cnr_tax_code,
             target.cnr_tax_rate            = use_this.cnr_tax_rate,
             target.cnr_tax_amount          = use_this.cnr_tax_amount;

   LOGGER.LOG_INFORMATION(L_program||' rollup status and what not from CNR to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --rollup status and what not from CN to SKU
   ------------------------------------------------
   merge into im_detail_match_cn_ws target
   using (select i2.workspace_id,
                 i2.item,
                 i2.match_status,
                 i2.matched_filter,
                 decode(i.cn_count, 1, i.cn_id, null) cn_id,
                 decode(i.cn_count, 1, i.cn_ext_doc_id, null) cn_ext_doc_id,
                 decode(i.cn_order_count, 1, i.cn_order_no, null) cn_order_no,
                 decode(i.cn_rtv_order_count, 1, i.cn_rtv_order_no, null) cn_rtv_order_no,
                 decode(i.cost_count, 1, i.cn_unit_cost, null) cn_unit_cost,
                 decode(i.cost_count, 1, 'Y', 'N') choice_flag,
                 i.cn_qty,
                 i.cn_ext_cost,
                 NVL(i.cn_count, 0) selected_cn_count,
                 --
                 decode(i.cn_supplier_group_id_cnt,   1, i.cn_supplier_group_id) cn_supplier_group_id,
                 decode(i.cn_supplier_cnt,            1, i.cn_supplier) cn_supplier,
                 decode(i.cn_supplier_name_cnt,       1, i.cn_supplier_name) cn_supplier_name,
                 decode(i.cn_supplier_phone_cnt,      1, i.cn_supplier_phone) cn_supplier_phone,
                 decode(i.cn_supplier_site_id_cnt,    1, i.cn_supplier_site_id) cn_supplier_site_id,
                 decode(i.cn_supplier_site_name_cnt,  1, i.cn_supplier_site_name) cn_supplier_site_name,
                 --
                 decode(i.cn_tax_code_cnt,  1, i.cn_tax_code) cn_tax_code,
                 decode(i.cn_tax_code_cnt,  1, i.cn_tax_rate) cn_tax_rate,
                 decode(i.cn_tax_code_cnt,  1, i.cn_tax_amount) cn_tax_amount
            from (select d.workspace_id,
                         d.item,
                         min(d.cn_id)
                            over (partition by d.workspace_id, d.item) cn_id,
                         min(d.cn_ext_doc_id)
                            over (partition by d.workspace_id, d.item) cn_ext_doc_id,
                         min(d.cn_unit_cost)
                            over (partition by d.workspace_id, d.item) cn_unit_cost,
                         sum(d.cn_qty)
                            over (partition by d.workspace_id, d.item) cn_qty,
                         sum(d.cn_ext_cost)
                            over (partition by d.workspace_id, d.item) cn_ext_cost,
                         row_number()
                            over (partition by d.workspace_id, d.item order by d.detail_match_cn_ws_id) rownbr,
                         count(distinct d.cn_id)
                            over (partition by d.workspace_id, d.item) cn_count,
                         count(distinct d.cn_unit_cost)
                            over (partition by d.workspace_id, d.item) cost_count,
                         --
                         min(distinct d.cn_supplier_group_id)
                            over (partition by d.workspace_id, d.item) cn_supplier_group_id,
                         min(distinct d.cn_supplier)
                            over (partition by d.workspace_id, d.item) cn_supplier,
                         min(distinct d.cn_supplier_name)
                            over (partition by d.workspace_id, d.item) cn_supplier_name,
                         min(distinct d.cn_supplier_phone)
                            over (partition by d.workspace_id, d.item) cn_supplier_phone,
                         min(distinct d.cn_supplier_site_id)
                            over (partition by d.workspace_id, d.item) cn_supplier_site_id,
                         min(distinct d.cn_supplier_site_name)
                            over (partition by d.workspace_id, d.item) cn_supplier_site_name,
                 --
                         count(distinct d.cn_supplier_group_id)
                            over (partition by d.workspace_id, d.item) cn_supplier_group_id_cnt,
                         count(distinct d.cn_supplier)
                            over (partition by d.workspace_id, d.item) cn_supplier_cnt,
                         count(distinct d.cn_supplier_name)
                            over (partition by d.workspace_id, d.item) cn_supplier_name_cnt,
                         count(distinct d.cn_supplier_phone)
                            over (partition by d.workspace_id, d.item) cn_supplier_phone_cnt,
                         count(distinct d.cn_supplier_site_id)
                            over (partition by d.workspace_id, d.item) cn_supplier_site_id_cnt,
                         count(distinct d.cn_supplier_site_name)
                            over (partition by d.workspace_id, d.item) cn_supplier_site_name_cnt,
                         --
                         min(distinct d.cn_tax_code)
                            over (partition by d.workspace_id, d.item) cn_tax_code,
                         min(distinct d.cn_tax_rate)
                            over (partition by d.workspace_id, d.item) cn_tax_rate,
                         sum(distinct d.cn_tax_amount)
                            over (partition by d.workspace_id, d.item) cn_tax_amount,
                         count(distinct d.cn_tax_code)
                            over (partition by d.workspace_id, d.item) cn_tax_code_cnt,
                         --
                         min(d.cn_order_no)
                             over (partition by d.workspace_id, d.item) cn_order_no,
                         min(d.cn_rtv_order_no)
                             over (partition by d.workspace_id, d.item) cn_rtv_order_no,
                         count(distinct d.cn_order_no)
                               over (partition by d.workspace_id, d.item) cn_order_count,
                         count(distinct d.cn_rtv_order_no)
                               over (partition by d.workspace_id, d.item) cn_rtv_order_count
                         --
                    from im_detail_match_cn_ws d
                   where d.workspace_id = I_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
                     and d.choice_flag  = 'Y') i,
                 --
                 (select d.workspace_id,
                         d.item,
                         max(d.match_status) match_status,
                         min(d.matched_filter) matched_filter
                    from im_detail_match_cn_ws d
                   where d.workspace_id = I_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
                   group by d.workspace_id, 
                            d.item) i2
           where 1               = i.rownbr(+)
             and i2.workspace_id = i.workspace_id(+)
             and i2.item         = i.item(+)) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.item         = use_this.item
       and target.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU)
   when MATCHED then
      update
         set target.match_status =
                GREATEST(nvl(target.match_status,use_this.match_status),use_this.match_status), --Pick U if any are U, only get M if all are M
             target.matched_filter =
                LEAST(nvl(target.matched_filter,use_this.matched_filter),use_this.matched_filter), --Pick N if any are N, only get Y if all are Y
             target.cn_id             = use_this.cn_id,
             target.cn_ext_doc_id     = use_this.cn_ext_doc_id,
             target.cn_order_no       = use_this.cn_order_no,
             target.cn_rtv_order_no   = use_this.cn_rtv_order_no,
             target.cn_unit_cost      = use_this.cn_unit_cost,
             target.cn_qty            = use_this.cn_qty,
             target.cn_ext_cost       = use_this.cn_ext_cost,
             target.selected_cn_count = use_this.selected_cn_count,
             --
             target.cn_supplier_group_id   = use_this.cn_supplier_group_id,
             target.cn_supplier            = use_this.cn_supplier,
             target.cn_supplier_name       = use_this.cn_supplier_name,
             target.cn_supplier_phone      = use_this.cn_supplier_phone,
             target.cn_supplier_site_id    = use_this.cn_supplier_site_id,
             target.cn_supplier_site_name  = use_this.cn_supplier_site_name,
             --
             target.cn_tax_code            = use_this.cn_tax_code,
             target.cn_tax_rate            = use_this.cn_tax_rate,
             target.cn_tax_amount          = use_this.cn_tax_amount;

   LOGGER.LOG_INFORMATION(L_program||' rollup status and what not from CN to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --merge match info to SKU
   ------------------------------------------------
   merge into im_detail_match_cn_ws target
   using (select cn.ancestor_id,
                 cn.item,
                 DECODE(cn_vpn, cnr_vpn, cn_vpn, NULL) vpn,
                 cn.cn_count,
                 cnr.cnr_count,
                 cn.cn_cost_cnt,
                 cnr.cnr_cost_cnt,
                 --
                 cn.cn_group_cost,
                 cn.cn_group_qty,
                 cnr.cnr_group_cost,
                 cnr.cnr_group_qty,
                 --
                 cn.tolerance_id,
                 cn.tolerance_exchange_rate,
                 tdc.tolerance_detail_id cost_tolerance_dtl_id,
                 tdq.tolerance_detail_id qty_tolerance_dtl_id,
                 --
                 NVL(cnr.cnr_group_cost, 0) - cn.cn_group_cost unit_cost_variance,
                 case when NVL(cnr.cnr_group_cost, 0) = 0 then
                      NULL
                      else
                      (cnr.cnr_group_cost - cn.cn_group_cost) / cnr.cnr_group_cost
                 end unit_cost_variance_pct,
                 NVL(cnr.cnr_group_qty, 0) - cn.cn_group_qty qty_variance,
                 case when NVL(cnr.cnr_group_qty, 0) = 0 then
                      NULL
                      else
                       (cnr.cnr_group_qty - cn.cn_group_qty) / cnr.cnr_group_qty
                 end qty_variance_pct,
                 --
                 case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(cnr.cnr_group_cost, 0) = 0 then
                         'N'
                              when tdc.tolerance_value < ABS(100 * (cn.cn_group_cost - cnr.cnr_group_cost) / cnr.cnr_group_cost) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdc.tolerance_value < ABS((cn.cn_group_cost - NVL(cnr.cnr_group_cost, 0)) * cn.tolerance_exchange_rate) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when cn.cn_group_cost = NVL(cnr.cnr_group_cost, 0) then
                                 'Y'
                              else
                                 'N'
                         end
                 end cost_in_tolerance,
                 --
                 case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(cnr.cnr_group_qty, 0) = 0 then
                         'N'
                              when tdq.tolerance_value < ABS(100 * (cn.cn_group_qty - cnr.cnr_group_qty) / cnr.cnr_group_qty) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdq.tolerance_value < ABS(cn.cn_group_qty - NVL(cnr.cnr_group_qty, 0)) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when cn.cn_group_qty = NVL(cnr.cnr_group_qty, 0) then
                                 'Y'
                              else
                                 'N'
                         end
                 end qty_in_tolerance,
                 --
                 case when cn.cn_tax_cnt = 1 and cnr.cnr_tax_cnt = 1 and cn.cn_tax_code = cnr.cnr_tax_code then
                         'N'
                      when cn.cn_tax_cnt = 0 and cnr.cnr_tax_cnt = 0 then
                         'N'
                      else
                         'Y'
                 end tax_discrepant
                 --
            from (select id.ancestor_id,
                         id.item,
                         id.tolerance_id,
                         id.tolerance_exchange_rate,
                         min(id.cn_unit_cost) cn_group_cost,
                         sum(id.cn_qty) cn_group_qty,
                         count(id.cn_id) cn_count,
                         count(distinct id.cn_unit_cost) cn_cost_cnt,
                         count(distinct id.cn_tax_code) cn_tax_cnt,
                         min(id.cn_tax_code) cn_tax_code,
                         DECODE(count(distinct NVL(id.vpn,'VpN')),
                                1, min(id.vpn),
                                NULL) cn_vpn
                   from im_detail_match_cn_ws id
                  where id.workspace_id   = I_workspace_id
                    and id.match_status   = 'U' --constant
                    and id.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
                    and id.choice_flag    = 'Y'
                  group by id.ancestor_id,
                           id.item,
                           id.tolerance_id,
                           id.tolerance_exchange_rate) cn,
                 (select rd.ancestor_id,
                         rd.item,
                         min(rd.cnr_unit_cost) cnr_group_cost,
                         sum(rd.cnr_qty) cnr_group_qty,
                         count(rd.cnr_id) cnr_count,
                         count(distinct rd.cnr_unit_cost) cnr_cost_cnt,
                         count(distinct rd.cnr_tax_code) cnr_tax_cnt,
                         min(rd.cnr_tax_code) cnr_tax_code,
                         DECODE(count(distinct NVL(rd.vpn,'VpN')),
                                1, min(rd.vpn),
                                NULL) cnr_vpn
                   from im_detail_match_cn_ws rd
                  where rd.workspace_id  = I_workspace_id
                    and rd.match_status  = 'U' --constant
                    and rd.entity_type   =  REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
                    and rd.choice_flag   = 'Y'
                  group by rd.ancestor_id,
                           rd.item,
                           rd.tolerance_id,
                           rd.tolerance_exchange_rate) cnr,
                 --
                 im_tolerance_detail tdc,
                 im_tolerance_detail tdq
           where cn.ancestor_id      = cnr.ancestor_id (+)
             and cn.item             = cnr.item (+)
             --
             and cn.tolerance_id     = tdc.tolerance_id(+)
             and tdc.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
             and tdc.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
             and tdc.lower_limit(+)   <= abs(cn.cn_group_cost)
             and tdc.upper_limit(+)    > abs(cn.cn_group_cost)
             and tdc.favor_of(+)       = case when cn.cn_group_cost > NVL(cnr.cnr_group_cost, 0)
                                              then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                              else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                         end  
             --
             and cn.tolerance_id     = tdq.tolerance_id(+)
             and tdq.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
             and tdq.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
             and tdq.lower_limit(+)   <= cn.cn_group_qty
             and tdq.upper_limit(+)    > cn.cn_group_qty
             and tdq.favor_of(+)       = case when cn.cn_group_qty < NVL(cnr.cnr_group_qty, 0)
                                              then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                              else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                         end
   ) use_this
   on (    target.workspace_id          = I_workspace_id
       and target.detail_match_cn_ws_id = use_this.ancestor_id
       and target.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.in_tolerance = case when use_this.cost_in_tolerance = 'Y' and use_this.qty_in_tolerance = 'Y' then 'Y' else 'N' end,
             target.choice_flag  = case when use_this.cost_in_tolerance = 'Y' and use_this.qty_in_tolerance = 'Y' then 'Y' else 'N' end,
             target.unit_cost_variance     = use_this.unit_cost_variance,
             target.unit_cost_variance_pct = use_this.unit_cost_variance_pct,
             target.cost_tolerance_dtl_id  = use_this.cost_tolerance_dtl_id,
             target.cost_in_tolerance      = use_this.cost_in_tolerance,
             target.qty_variance           = use_this.qty_variance,
             target.qty_variance_pct       = use_this.qty_variance_pct,
             target.qty_tolerance_dtl_id   = use_this.qty_tolerance_dtl_id,
             target.qty_in_tolerance       = use_this.qty_in_tolerance,
             target.tax_discrepant         = use_this.tax_discrepant,
             target.vpn                    = use_this.vpn;

   LOGGER.LOG_INFORMATION(L_program||' merge match info to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --merge sku to style
   ------------------------------------------------
   merge into im_detail_match_cn_ws target
   using (select distinct 
                 d.workspace_id,
                 d.ancestor_id,
                 max(d.choice_flag)
                     over (partition by d.workspace_id, d.ancestor_id) choice_flag,
                 max(d.match_status)
                     over (partition by d.workspace_id, d.ancestor_id) match_status,
                 min(d.matched_filter)
                     over (partition by d.workspace_id, d.ancestor_id) matched_filter,
                 sum(decode(d.choice_flag,'Y',d.selected_cn_count,0))
                     over (partition by d.workspace_id, d.ancestor_id) selected_cn_count,
                 sum(decode(d.choice_flag,'Y',d.selected_cnr_count,0))
                     over (partition by d.workspace_id, d.ancestor_id) selected_cnr_count
             from im_detail_match_cn_ws d
            where d.workspace_id = I_workspace_id
              and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
              and d.ancestor_id  is not null
   ) use_this
   on (    target.workspace_id          = use_this.workspace_id
       and target.detail_match_cn_ws_id = use_this.ancestor_id
       and target.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set target.choice_flag        = use_this.choice_flag,
             target.match_status       = use_this.match_status,
             target.matched_filter     = use_this.matched_filter,
             target.selected_cnr_count = use_this.selected_cnr_count,
             target.selected_cn_count  = use_this.selected_cn_count;

   LOGGER.LOG_INFORMATION(L_program||' rollup SKU to STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --Update VPN on STYLE
   ------------------------------------------------

   merge into im_detail_match_cn_ws target
   using (select distinct sku_ws.ancestor_id,
                 its.vpn
            from (select inner.ancestor_id,
                         DECODE(count(distinct inner.supplier),
                                1, min(inner.supplier),
                                NULL) supplier
                    from (select id.ancestor_id,
                                 DECODE(count(distinct id.cn_supplier_site_id),
                                        1, min(id.cn_supplier_site_id),
                                        NULL) supplier
                            from im_detail_match_cn_ws id
                           where id.workspace_id = I_workspace_id
                             and id.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
                           group by id.ancestor_id
                           union all
                           select rd.ancestor_id,
                                  DECODE(count(distinct rd.cnr_supplier_site_id),
                                         1, min(rd.cnr_supplier_site_id),
                                         NULL) supplier
                            from im_detail_match_cn_ws rd
                           where rd.workspace_id = I_workspace_id
                             and rd.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
                           group by rd.ancestor_id)inner
                   group by inner.ancestor_id) cn_cnr_ws,
                  im_detail_match_cn_ws sku_ws,
                  item_supplier its
           where sku_ws.workspace_id          = I_workspace_id
             and sku_ws.detail_match_cn_ws_id = cn_cnr_ws.ancestor_id
             and sku_ws.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and its.item                     = sku_ws.item_parent
             and its.supplier                 = cn_cnr_ws.supplier) use_this
   on (    target.workspace_id          = I_workspace_id
       and target.detail_match_cn_ws_id = use_this.ancestor_id
       and target.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set target.vpn = use_this.vpn;

   LOGGER.LOG_INFORMATION(L_program||' rollup VPN to STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_detail_match_cn_ws tgt
   using (select gtt.number_1 dept,
                 gtt.number_2 class,
                 gtt.number_3 subclass
            from gtt_6_num_6_str_6_date gtt) src
   on (    tgt.dept     = src.dept
       and tgt.class    = src.class
       and tgt.subclass = src.subclass)
   when MATCHED THEN
      update
         set tgt.ui_filter_ind = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' apply UI filter - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CREATE_MATCH_CN_DETAIL_WS;
----------------------------------------------------------------

-- The public function used for Initializing Credit Note Match Data for Online matching.
-- Output param: O_match_workspace_id (The ID of the Match workspace, new one if I_match_workspace_id is not passed in)
--
-- Input param: I_search_workspace_id (The ID of the Search workspace)
--              I_match_workspace_id (The ID of the Match workspace, if search is performed on the match screen)
--
FUNCTION INIT_ONLINE_MATCH_DATA(O_error_message       OUT VARCHAR2,
                                O_match_wspace_id     OUT IM_MATCH_CN_WS.WORKSPACE_ID%TYPE,
                                I_search_wspace_id IN     IM_CN_SEARCH_WS.WORKSPACE_ID%TYPE,
                                I_match_wspace_id  IN     IM_MATCH_CN_WS.WORKSPACE_ID%TYPE DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(61) := 'REIM_CREDIT_NOTE_MATCH_SQL.INIT_ONLINE_MATCH_DATA';
   L_start_time   TIMESTAMP := SYSTIMESTAMP;

   L_vdate          PERIOD.VDATE%TYPE                   := GET_VDATE;
   L_choice_flag    VARCHAR2(1)                         := 'Y';
   L_selected_curr  IM_MATCH_INVC_WS.CURRENCY_CODE%TYPE := NULL;
   L_supp_count NUMBER                              := REIM_CONSTANTS.ZERO;

   multi_supp EXCEPTION;
   
   cursor C_FETCH_SELECTED_CURR is
      select MAX(imcnw.currency_code)
        from im_match_cn_ws imcnw
       where imcnw.workspace_id = O_match_wspace_id
         and imcnw.choice_flag  = 'Y';
   
   cursor C_CHK_SUPP is
      select count(distinct inner.supplier)
        from (select imcnw.supplier
                from im_match_cn_ws imcnw
               where imcnw.workspace_id = O_match_wspace_id
                 and imcnw.choice_flag  = 'Y'
                 and imcnw.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH
              union all
              select imcnrw.supplier
                from im_match_cnr_ws imcnrw
               where imcnrw.workspace_id = O_match_wspace_id
                 and imcnrw.choice_flag  = 'Y'
                 and imcnrw.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH) inner;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);


   if I_match_wspace_id is NULL then
      O_match_wspace_id := IM_WORKSPACE_ID_SEQ.NEXTVAL;
   else
      O_match_wspace_id := I_match_wspace_id;

      open C_FETCH_SELECTED_CURR;
      fetch C_FETCH_SELECTED_CURR into L_selected_curr;
      close C_FETCH_SELECTED_CURR;
   end if;

   merge into im_match_cn_ws tgt
   using (select O_match_wspace_id workspace_id,
                 icnsw.doc_id,
                 icnsw.ext_doc_id,
                 icnsw.doc_head_version_id,
                 icnsw.status,
                 icnsw.doc_date,
                 idh.due_date, -- revisit
                 so.group_id supplier_group_id, -- revisit
                 icnsw.vendor supplier,
                 icnsw.vendor_name supplier_name,
                 s.contact_phone supplier_phone, -- revisit
                 icnsw.supplier_site_id,
                 icnsw.supplier_site_name,
                 icnsw.supplier_site_vat_region,
                 icnsw.currency_code,
                 icnsw.order_no,
                 icnsw.location,
                 icnsw.loc_type,
                 icnsw.loc_name location_name,
                 icnsw.loc_vat_region,
                 icnsw.loc_set_of_books_id,
                 icnsw.total_cost total_avail_cost, --merge later
                 icnsw.total_qty total_avail_qty, --merge later
                 icnsw.total_cost merch_amount, -- merge later
                 icnsw.total_qty,
                 idh.header_only, --merge later  -- revisit
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig, --revisit
                 NVL(so.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                 NVL(so.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                 NVL(so.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                 icnmg.group_id manual_group_id,
                 NULL cn_match_ref_cnr_ext_doc_id, -- revisit
                 NULL cn_match_ref_invc_ext_doc_id, -- revisit
                 idh.deal_id, -- revisit
                 NULL deal_detail_id, -- revisit
                 idh.deal_type, -- revisit
                 icnsw.rtv_order_no,
                 idh.rtv_ind,
                 idh.hold_status, -- revisit
                 DECODE(I_match_wspace_id,
                        NULL, icnsw.choice_flag,
                        DECODE(L_selected_curr,
                               icnsw.currency_code, icnsw.choice_flag,
                               'N')) choice_flag
            from im_cn_search_ws icnsw,
                 im_doc_head idh,
                 v_im_supp_site_attrib_expl so,
                 im_cn_manual_groups icnmg,
                 sups s -- revisit 
           where icnsw.workspace_id  = I_search_wspace_id
             and icnsw.choice_flag   = 'Y'
             and idh.doc_id          = icnsw.doc_id
             and so.supplier         = icnsw.supplier_site_id
             and icnmg.doc_id (+)    = icnsw.doc_id
             and s.supplier          = icnsw.vendor) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when NOT MATCHED THEN
      insert (workspace_id,
              doc_id,
              ext_doc_id,
              doc_head_version_id,
              status,
              doc_date,
              due_date,
              supplier_group_id,
              supplier,
              supplier_name,
              supplier_phone,
              supplier_site_id,
              supplier_site_name,
              supplier_site_vat_region,
              currency_code,
              order_no,
              location,
              loc_type,
              location_name,
              loc_vat_region,
              loc_set_of_books_id,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              header_only,
              summary_mtch_elig,
              detail_mtch_elig,
              qty_required,
              qty_match_required,
              sku_comp_percent,
              manual_group_id,
              cn_match_ref_cnr_ext_doc_id,
              cn_match_ref_invc_ext_doc_id,
              deal_id,
              deal_detail_id,
              deal_type,
              rtv_order_no,
              rtv_ind,
              hold_status,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.ext_doc_id,
              src.doc_head_version_id,
              src.status,
              src.doc_date,
              src.due_date,
              src.supplier_group_id,
              src.supplier,
              src.supplier_name,
              src.supplier_phone,
              src.supplier_site_id,
              src.supplier_site_name,
              src.supplier_site_vat_region,
              src.currency_code,
              src.order_no,
              src.location,
              src.loc_type,
              src.location_name,
              src.loc_vat_region,
              src.loc_set_of_books_id,
              src.total_avail_cost, --merge later
              src.total_avail_qty, --merge later
              src.merch_amount, -- merge later
              src.total_qty, -- merge later
              src.header_only, -- header_only, merge later
              src.summary_mtch_elig, -- summary_mtch_elig, merge later
              src.detail_mtch_elig, -- detail_mtch_elig, merge later
              src.qty_required, -- to be confirmed
              src.qty_match_required, -- to be confirmed
              src.sku_comp_percent,
              src.manual_group_id,
              src.cn_match_ref_cnr_ext_doc_id,
              src.cn_match_ref_invc_ext_doc_id,
              src.deal_id,
              src.deal_detail_id,
              src.deal_type,
              src.rtv_order_no,
              src.rtv_ind,
              src.hold_status,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert selected Credit Notes from SEARCH_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select O_match_wspace_id workspace_id,
                 icnrsw.doc_id,
                 icnrsw.ext_doc_id,
                 icnrsw.type,
                 icnrsw.doc_head_version_id,
                 icnrsw.status,
                 icnrsw.doc_date,
                 idh.due_date, -- revisit
                 so.group_id supplier_group_id, -- revisit
                 icnrsw.vendor supplier,
                 icnrsw.vendor_name supplier_name,
                 s.contact_phone supplier_phone, -- revisit
                 icnrsw.supplier_site_id,
                 icnrsw.supplier_site_name,
                 icnrsw.supplier_site_vat_region,
                 icnrsw.currency_code,
                 icnrsw.order_no, -- revisit
                 icnrsw.location,
                 icnrsw.loc_type,
                 icnrsw.loc_name location_name,
                 icnrsw.loc_vat_region,
                 icnrsw.loc_set_of_books_id,
                 icnrsw.total_cost total_avail_cost, --merge later
                 icnrsw.total_qty total_avail_qty, --merge later
                 icnrsw.total_cost merch_amount, -- merge later
                 icnrsw.total_qty,
                 idh.header_only, --merge later  -- revisit
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig, --revisit
                 NVL(so.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                 NVL(so.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                 --NVL(so.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                 icnmg.group_id manual_group_id,
                 NULL cn_match_ref_cnr_ext_doc_id, -- revisit
                 NULL cn_match_ref_invc_ext_doc_id, -- revisit
                 idh.deal_id, -- revisit
                 NULL deal_detail_id, -- revisit
                 idh.deal_type, -- revisit
                 icnrsw.rtv_order_no,
                 idh.rtv_ind, -- revisit
                 idh.hold_status, -- revisit
                 DECODE(I_match_wspace_id,
                        NULL, icnrsw.choice_flag,
                        DECODE(L_selected_curr,
                               icnrsw.currency_code, icnrsw.choice_flag,
                               'N')) choice_flag
            from im_cnr_search_ws icnrsw,
                 im_doc_head idh,
                 v_im_supp_site_attrib_expl so,
                 im_cn_manual_groups icnmg,
                 sups s -- revisit
           where icnrsw.workspace_id  = I_search_wspace_id
             and icnrsw.choice_flag   = 'Y'
             and idh.doc_id           = icnrsw.doc_id
             and so.supplier          = icnrsw.supplier_site_id
             and icnmg.doc_id (+)     = icnrsw.doc_id
             and s.supplier           = icnrsw.vendor) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when NOT MATCHED THEN
      insert (workspace_id,
              doc_id,
              ext_doc_id,
              type,
              doc_head_version_id,
              status,
              doc_date,
              due_date,
              supplier_group_id,
              supplier,
              supplier_name,
              supplier_phone,
              supplier_site_id,
              supplier_site_name,
              supplier_site_vat_region,
              currency_code,
              order_no,
              location,
              loc_type,
              location_name,
              loc_vat_region,
              loc_set_of_books_id,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              header_only,
              summary_mtch_elig,
              detail_mtch_elig,
              qty_required,
              qty_match_required,
              --sku_comp_percent,
              manual_group_id,
              cn_match_ref_cnr_ext_doc_id,
              cn_match_ref_invc_ext_doc_id,
              deal_id,
              deal_detail_id,
              deal_type,
              rtv_order_no,
              rtv_ind,
              hold_status,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.ext_doc_id,
              src.type,
              src.doc_head_version_id,
              src.status,
              src.doc_date,
              src.due_date,
              src.supplier_group_id,
              src.supplier,
              src.supplier_name,
              src.supplier_phone,
              src.supplier_site_id,
              src.supplier_site_name,
              src.supplier_site_vat_region,
              src.currency_code,
              src.order_no,
              src.location,
              src.loc_type,
              src.location_name,
              src.loc_vat_region,
              src.loc_set_of_books_id,
              src.total_avail_cost, --merge later
              src.total_avail_qty, --merge later
              src.merch_amount, -- merge later
              src.total_qty, -- merge later
              src.header_only, -- header_only, merge later
              src.summary_mtch_elig, -- summary_mtch_elig, merge later
              src.detail_mtch_elig, -- detail_mtch_elig, merge later
              src.qty_required, -- to be confirmed
              src.qty_match_required, -- to be confirmed
              --src.sku_comp_percent,
              src.manual_group_id,
              src.cn_match_ref_cnr_ext_doc_id,
              src.cn_match_ref_invc_ext_doc_id,
              src.deal_id,
              src.deal_detail_id,
              src.deal_type,
              src.rtv_order_no,
              src.rtv_ind,
              src.hold_status,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert selected Credit Note Requests from SEARCH_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(number_1,
                                             number_2)
      select distinct imcnw.manual_group_id,
             imcnw.loc_set_of_books_id
        from im_match_cn_ws imcnw
       where imcnw.workspace_id    = O_match_wspace_id
         and imcnw.manual_group_id is NOT NULL
      union
      select distinct imcnrw.manual_group_id,
             imcnrw.loc_set_of_books_id
        from im_match_cnr_ws imcnrw
       where imcnrw.workspace_id    = O_match_wspace_id
         and imcnrw.manual_group_id is NOT NULL;

   merge into im_match_cn_ws tgt
   using (select O_match_wspace_id workspace_id,
                 idh.doc_id,
                 idh.ext_doc_id,
                 idh.object_version_id doc_head_version_id,
                 idh.status,
                 idh.doc_date,
                 idh.due_date,
                 so.group_id supplier_group_id, -- revisit
                 idh.vendor supplier,
                 sups_parent.sup_name supplier_name,
                 sups_parent.contact_phone supplier_phone, -- revisit
                 idh.supplier_site_id,
                 sups.sup_name supplier_site_name,
                 sups.vat_region supplier_site_vat_region,
                 idh.currency_code,
                 idh.order_no order_no, -- revisit
                 idh.location,
                 idh.loc_type,
                 loc.loc_name location_name,
                 loc.vat_region loc_vat_region,
                 gtt.number_2 loc_set_of_books_id,
                 idh.total_cost total_avail_cost, --merge later
                 idh.total_qty total_avail_qty, --merge later
                 idh.total_cost merch_amount, -- merge later
                 idh.total_qty,
                 idh.header_only,
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig,
                 NVL(so.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                 NVL(so.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                 NVL(so.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                 icnmg.group_id manual_group_id,
                 NULL cn_match_ref_cnr_ext_doc_id, -- revisit
                 NULL cn_match_ref_invc_ext_doc_id, -- revisit
                 idh.deal_id, -- revisit
                 NULL deal_detail_id, -- revisit
                 idh.deal_type, -- revisit
                 idh.rtv_order_no,
                 idh.rtv_ind, -- revisit
                 idh.hold_status, -- revisit
                 DECODE(I_match_wspace_id,
                        NULL, 'Y',
                        DECODE(L_selected_curr,
                               idh.currency_code, 'Y',
                               'N')) choice_flag
            from gtt_num_num_str_str_date_date gtt,
                 im_cn_manual_groups icnmg,
                 im_doc_head idh,
                 v_im_supp_site_attrib_expl so,
                 sups sups_parent,
                 sups sups,
                 (select store loc,
                         REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                         store_name loc_name,
                         vat_region
                    from store
                  union all
                  select wh loc,
                         REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                         wh_name loc_name,
                         vat_region
                    from wh
                   where wh = physical_wh) loc
           where icnmg.group_id       = gtt.number_1
             and idh.doc_id           = icnmg.doc_id
             and idh.type             = REIM_CONSTANTS.DOC_TYPE_CRDNT
             and so.supplier          = idh.supplier_site_id
             and sups_parent.supplier = idh.vendor
             and sups.supplier        = idh.supplier_site_id
             and loc.loc              = idh.location
             and loc.loc_type         = idh.loc_type) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when NOT MATCHED THEN
      insert (workspace_id,
              doc_id,
              ext_doc_id,
              doc_head_version_id,
              status,
              doc_date,
              due_date,
              supplier_group_id,
              supplier,
              supplier_name,
              supplier_phone,
              supplier_site_id,
              supplier_site_name,
              supplier_site_vat_region,
              currency_code,
              order_no,
              location,
              loc_type,
              location_name,
              loc_vat_region,
              loc_set_of_books_id,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              header_only,
              summary_mtch_elig,
              detail_mtch_elig,
              qty_required,
              qty_match_required,
              sku_comp_percent,
              manual_group_id,
              cn_match_ref_cnr_ext_doc_id,
              cn_match_ref_invc_ext_doc_id,
              deal_id,
              deal_detail_id,
              deal_type,
              rtv_order_no,
              rtv_ind,
              hold_status,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.ext_doc_id,
              src.doc_head_version_id,
              src.status,
              src.doc_date,
              src.due_date,
              src.supplier_group_id,
              src.supplier,
              src.supplier_name,
              src.supplier_phone,
              src.supplier_site_id,
              src.supplier_site_name,
              src.supplier_site_vat_region,
              src.currency_code,
              src.order_no,
              src.location,
              src.loc_type,
              src.location_name,
              src.loc_vat_region,
              src.loc_set_of_books_id,
              src.total_avail_cost, --merge later
              src.total_avail_qty, --merge later
              src.merch_amount, -- merge later
              src.total_qty, -- merge later
              src.header_only, -- header_only, merge later
              src.summary_mtch_elig, -- summary_mtch_elig, merge later
              src.detail_mtch_elig, -- detail_mtch_elig, merge later
              src.qty_required, -- to be confirmed
              src.qty_match_required, -- to be confirmed
              src.sku_comp_percent,
              src.manual_group_id,
              src.cn_match_ref_cnr_ext_doc_id,
              src.cn_match_ref_invc_ext_doc_id,
              src.deal_id,
              src.deal_detail_id,
              src.deal_type,
              src.rtv_order_no,
              src.rtv_ind,
              src.hold_status,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert Credit Notes from MANUAL GROUPS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select O_match_wspace_id workspace_id,
                 idh.doc_id,
                 idh.ext_doc_id,
                 idh.type,
                 idh.object_version_id doc_head_version_id,
                 idh.status,
                 idh.doc_date,
                 idh.due_date,
                 so.group_id supplier_group_id, -- revisit
                 idh.vendor supplier,
                 sups_parent.sup_name supplier_name,
                 sups_parent.contact_phone supplier_phone, -- revisit
                 idh.supplier_site_id,
                 sups.sup_name supplier_site_name,
                 sups.vat_region supplier_site_vat_region,
                 idh.currency_code,
                 idh.order_no, -- revisit
                 idh.location,
                 idh.loc_type,
                 loc.loc_name location_name,
                 loc.vat_region loc_vat_region,
                 gtt.number_2 loc_set_of_books_id,
                 idh.total_cost total_avail_cost, --merge later
                 idh.total_qty total_avail_qty, --merge later
                 idh.total_cost merch_amount, -- merge later
                 idh.total_qty,
                 idh.header_only,
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig,
                 NVL(so.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                 NVL(so.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                 --NVL(so.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                 icnmg.group_id manual_group_id,
                 NULL cn_match_ref_cnr_ext_doc_id, -- revisit
                 NULL cn_match_ref_invc_ext_doc_id, -- revisit
                 idh.deal_id, -- revisit
                 NULL deal_detail_id, -- revisit
                 idh.deal_type, -- revisit
                 idh.rtv_order_no,
                 idh.rtv_ind, -- revisit
                 idh.hold_status, -- revisit
                 DECODE(I_match_wspace_id,
                        NULL, 'Y',
                        DECODE(L_selected_curr,
                               idh.currency_code, 'Y',
                               'N')) choice_flag
            from gtt_num_num_str_str_date_date gtt,
                 im_cn_manual_groups icnmg,
                 im_doc_head idh,
                 v_im_supp_site_attrib_expl so,
                 sups sups_parent,
                 sups sups,
                 (select store loc,
                         REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                         store_name loc_name,
                         vat_region
                    from store
                  union all
                  select wh loc,
                         REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                         wh_name loc_name,
                         vat_region
                    from wh
                   where wh = physical_wh) loc
           where icnmg.group_id       = gtt.number_1
             and idh.doc_id           = icnmg.doc_id
             and idh.type             IN (REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                                          REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                                          REIM_CONSTANTS.DOC_TYPE_CRDNRT)
             and so.supplier          = idh.supplier_site_id
             and sups_parent.supplier = idh.vendor
             and sups.supplier        = idh.supplier_site_id
             and loc.loc              = idh.location
             and loc.loc_type         = idh.loc_type) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when NOT MATCHED THEN
      insert (workspace_id,
              doc_id,
              ext_doc_id,
              type,
              doc_head_version_id,
              status,
              doc_date,
              due_date,
              supplier_group_id,
              supplier,
              supplier_name,
              supplier_phone,
              supplier_site_id,
              supplier_site_name,
              supplier_site_vat_region,
              currency_code,
              order_no,
              location,
              loc_type,
              location_name,
              loc_vat_region,
              loc_set_of_books_id,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              header_only,
              summary_mtch_elig,
              detail_mtch_elig,
              qty_required,
              qty_match_required,
              --sku_comp_percent,
              manual_group_id,
              cn_match_ref_cnr_ext_doc_id,
              cn_match_ref_invc_ext_doc_id,
              deal_id,
              deal_detail_id,
              deal_type,
              rtv_order_no,
              rtv_ind,
              hold_status,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.ext_doc_id,
              src.type,
              src.doc_head_version_id,
              src.status,
              src.doc_date,
              src.due_date,
              src.supplier_group_id,
              src.supplier,
              src.supplier_name,
              src.supplier_phone,
              src.supplier_site_id,
              src.supplier_site_name,
              src.supplier_site_vat_region,
              src.currency_code,
              src.order_no,
              src.location,
              src.loc_type,
              src.location_name,
              src.loc_vat_region,
              src.loc_set_of_books_id,
              src.total_avail_cost, --merge later
              src.total_avail_qty, --merge later
              src.merch_amount, -- merge later
              src.total_qty, -- merge later
              src.header_only, -- header_only, merge later
              src.summary_mtch_elig, -- summary_mtch_elig, merge later
              src.detail_mtch_elig, -- detail_mtch_elig, merge later
              src.qty_required, -- to be confirmed
              src.qty_match_required, -- to be confirmed
              --src.sku_comp_percent,
              src.manual_group_id,
              src.cn_match_ref_cnr_ext_doc_id,
              src.cn_match_ref_invc_ext_doc_id,
              src.deal_id,
              src.deal_detail_id,
              src.deal_type,
              src.rtv_order_no,
              src.rtv_ind,
              src.hold_status,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert Credit Note Requests from MANUAL GROUPS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_tax_ws tgt
   using (select imcnw.workspace_id,
                 imcnw.doc_id,
                 idt.tax_code,
                 idt.tax_rate,
                 idt.tax_basis,
                 idt.tax_amount
      from im_match_cn_ws imcnw,
           im_doc_tax idt
     where imcnw.workspace_id = O_match_wspace_id
       and idt.doc_id         = imcnw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.tax_code     = src.tax_code
       and tgt.tax_rate     = src.tax_rate)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              tax_code,
              tax_rate,
              tax_basis,
              tax_amount)
      values (src.workspace_id,
              src.doc_id,
              src.tax_code,
              src.tax_rate,
              src.tax_basis,
              src.tax_amount);

   LOGGER.LOG_INFORMATION('Insert CN_TAX_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_tax_ws tgt
   using (select imcnrw.workspace_id,
                 imcnrw.doc_id,
                 idt.tax_code,
                 idt.tax_rate,
                 idt.tax_basis,
                 idt.tax_amount
      from im_match_cnr_ws imcnrw,
           im_doc_tax idt
     where imcnrw.workspace_id = O_match_wspace_id
       and idt.doc_id          = imcnrw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.tax_code     = src.tax_code
       and tgt.tax_rate     = src.tax_rate)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              tax_code,
              tax_rate,
              tax_basis,
              tax_amount)
      values (src.workspace_id,
              src.doc_id,
              src.tax_code,
              src.tax_rate,
              src.tax_basis,
              src.tax_amount);

   LOGGER.LOG_INFORMATION('Insert CNR_TAX_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_ws tgt
   using (select imcntw.workspace_id,
                 imcntw.doc_id,
                 count(distinct imcntw.tax_code) over (partition by imcntw.workspace_id, imcntw.doc_id) tax_code_cnt,
                 min(imcntw.tax_code)   over (partition by imcntw.workspace_id, imcntw.doc_id) tax_code,
                 min(imcntw.tax_rate)   over (partition by imcntw.workspace_id, imcntw.doc_id) tax_rate,
                 sum(imcntw.tax_amount) over (partition by imcntw.workspace_id, imcntw.doc_id) tax_amount
            from im_match_cn_tax_ws imcntw
           where imcntw.workspace_id = O_match_wspace_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and src.tax_code_cnt = 1)
   when MATCHED then
      update
         set tgt.tax_code   = src.tax_code,
             tgt.tax_rate   = src.tax_rate,
             tgt.tax_amount = src.tax_amount;

   LOGGER.LOG_INFORMATION('Merge CN_WS with tax (documents with single tax) - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select imcnrtw.workspace_id,
                 imcnrtw.doc_id,
                 count(distinct imcnrtw.tax_code) over (partition by imcnrtw.workspace_id, imcnrtw.doc_id) tax_code_cnt,
                 min(imcnrtw.tax_code)   over (partition by imcnrtw.workspace_id, imcnrtw.doc_id) tax_code,
                 min(imcnrtw.tax_rate)   over (partition by imcnrtw.workspace_id, imcnrtw.doc_id) tax_rate,
                 sum(imcnrtw.tax_amount) over (partition by imcnrtw.workspace_id, imcnrtw.doc_id) tax_amount
            from im_match_cnr_tax_ws imcnrtw
           where imcnrtw.workspace_id = O_match_wspace_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and src.tax_code_cnt = 1)
   when MATCHED then
      update
         set tgt.tax_code   = src.tax_code,
             tgt.tax_rate   = src.tax_rate,
             tgt.tax_amount = src.tax_amount;

   LOGGER.LOG_INFORMATION('Merge CNR_WS with tax (documents with single tax) - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_CHK_SUPP;
   fetch C_CHK_SUPP into L_supp_count;
   close C_CHK_SUPP;
   
   if L_supp_count > REIM_CONSTANTS.ONE then
      RAISE multi_supp;
   end if;

   if I_match_wspace_id is NULL then

	  select case
	            when count(1) > 1 then
	               'N'
	            else
	               'Y'
	         end
	    into L_choice_flag
	  from (select distinct currency_code
	          from im_match_cn_ws imcnw
	         where imcnw.workspace_id = O_match_wspace_id
	        union
	        select distinct currency_code
	          from im_match_cnr_ws imcnrw
	         where imcnrw.workspace_id = O_match_wspace_id);
	
	  update im_match_cn_ws
	     set choice_flag = L_choice_flag
	   where workspace_id = O_match_wspace_id;
	
	  LOGGER.LOG_INFORMATION('Update Choice Flag on CN_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
	
	  update im_match_cnr_ws
	     set choice_flag = L_choice_flag
	   where workspace_id = O_match_wspace_id;
	
	  LOGGER.LOG_INFORMATION('Update Choice Flag on CNR_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   merge into im_match_cn_ws tgt
   using (select imcnw.workspace_id,
                 imcnw.doc_id,
                 SUM(idnm.non_merch_amt) non_merch_amt
            from im_match_cn_ws imcnw,
                 im_doc_non_merch idnm
           where imcnw.workspace_id = O_match_wspace_id
             and idnm.doc_id        = imcnw.doc_id
           GROUP BY imcnw.workspace_id,
                    imcnw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.merch_amount     = tgt.merch_amount - src.non_merch_amt,
             tgt.total_avail_cost = tgt.total_avail_cost - src.non_merch_amt;

   LOGGER.LOG_INFORMATION('Merge CN_WS NON_MERCH_AMT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select imcnrw.workspace_id,
                 imcnrw.doc_id,
                 SUM(idnm.non_merch_amt) non_merch_amt
            from im_match_cnr_ws imcnrw,
                 im_doc_non_merch idnm
           where imcnrw.workspace_id = O_match_wspace_id
             and idnm.doc_id         = imcnrw.doc_id
           GROUP BY imcnrw.workspace_id,
                    imcnrw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.merch_amount     = tgt.merch_amount - src.non_merch_amt,
             tgt.total_avail_cost = tgt.total_avail_cost - src.non_merch_amt;

   LOGGER.LOG_INFORMATION('Merge CNR_WS NON_MERCH_AMT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_detl_ws tgt
   using (select imcnw.workspace_id,
                 imcnw.doc_id,
                 iddrc.item,
                 its.vpn,
                 case when iddrc.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                                                  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD) 
                       and iddrc.qty_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                                                  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD) then
                         REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
                      else
                         REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                 end status,
                 iddrc.adjusted_unit_cost unit_cost,
                 iddrc.adjusted_qty doc_qty,
                 iddrc.cost_matched,
                 iddrc.qty_matched,
                 NULL match_hist_id, --revisit,
                 rct.tax_code,
                 rct.tax_rate,
                 rct.tax_amount,
                 imcnw.choice_flag
      from im_match_cn_ws imcnw,
           im_doc_detail_reason_codes iddrc,
           im_doc_detail_rc_tax rct,
           item_supplier its
     where imcnw.workspace_id                  = O_match_wspace_id
       and iddrc.doc_id                        = imcnw.doc_id
       and iddrc.im_doc_detail_reason_codes_id = rct.im_doc_detail_reason_codes_id (+)
       and its.item (+)                        = iddrc.item
       and its.supplier (+)                    = imcnw.supplier_site_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              item,
              vpn,
              status,
              unit_cost,
              doc_qty,
              cost_matched,
              qty_matched,
              match_hist_id,
              tax_code,
              tax_rate,
              tax_amount,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.item,
              src.vpn,
              src.status,
              src.unit_cost,
              src.doc_qty,
              src.cost_matched,
              src.qty_matched,
              src.match_hist_id,
              src.tax_code,
              src.tax_rate,
              src.tax_amount,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert CN_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_detl_ws tgt
   using (select imcnrw.workspace_id,
                 imcnrw.doc_id,
                 iddrc.item,
                 its.vpn,
                 iddrc.reason_code_id,
                 case when iddrc.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                                                  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD) 
                       and iddrc.qty_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                                                  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD) then
                         REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
                      else
                         REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                 end status,
                 iddrc.adjusted_unit_cost unit_cost,
                 iddrc.adjusted_qty doc_qty,
                 iddrc.cost_matched,
                 iddrc.qty_matched,
                 NULL match_hist_id, --revisit
                 rct.tax_code,
                 rct.tax_rate,
                 rct.tax_amount,
                 imcnrw.choice_flag
      from im_match_cnr_ws imcnrw,
           im_doc_detail_reason_codes iddrc,
           im_doc_detail_rc_tax rct,
           item_supplier its
     where imcnrw.workspace_id                 = O_match_wspace_id
       and iddrc.doc_id                        = imcnrw.doc_id
       and iddrc.im_doc_detail_reason_codes_id = rct.im_doc_detail_reason_codes_id (+)
       and its.item (+)                        = iddrc.item
       and its.supplier (+)                    = imcnrw.supplier_site_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              item,
              vpn,
              reason_code_id,
              status,
              unit_cost,
              doc_qty,
              cost_matched,
              qty_matched,
              match_hist_id,
              tax_code,
              tax_rate,
              tax_amount,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.item,
              src.vpn,
              src.reason_code_id,
              src.status,
              src.unit_cost,
              src.doc_qty,
              src.cost_matched,
              src.qty_matched,
              src.match_hist_id,
              src.tax_code,
              src.tax_rate,
              src.tax_amount,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert CNR_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_ws tgt
   using (select imcndw.workspace_id,
                 imcndw.doc_id,
                 SUM(DECODE(imcndw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imcndw.doc_qty,
                            REIM_CONSTANTS.ZERO) * imcndw.unit_cost) unmatch_item_amt,
                 SUM(DECODE(imcndw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imcndw.doc_qty,
                            REIM_CONSTANTS.ZERO)) unmatch_item_qty
            from im_match_cn_detl_ws imcndw
           where imcndw.workspace_id  = O_match_wspace_id
           GROUP BY imcndw.workspace_id,
                    imcndw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.unmatch_item_amt,
             tgt.total_avail_qty  = src.unmatch_item_qty,
             tgt.header_only      = 'N';

   LOGGER.LOG_INFORMATION('Merge CN_WS MTCH_ITEM_COST_QTY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select imcnrdw.workspace_id,
                 imcnrdw.doc_id,
                 SUM(DECODE(imcnrdw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imcnrdw.doc_qty,
                            REIM_CONSTANTS.ZERO) * imcnrdw.unit_cost) unmatch_item_amt,
                 SUM(DECODE(imcnrdw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imcnrdw.doc_qty,
                            REIM_CONSTANTS.ZERO)) unmatch_item_qty
            from im_match_cnr_detl_ws imcnrdw
           where imcnrdw.workspace_id  = O_match_wspace_id
           GROUP BY imcnrdw.workspace_id,
                    imcnrdw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.unmatch_item_amt,
             tgt.total_avail_qty  = src.unmatch_item_qty,
             tgt.header_only      = 'N';

   LOGGER.LOG_INFORMATION('Merge CNR_WS MTCH_ITEM_COST_QTY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_detl_ws tgt
   using (select imcndw.workspace_id,
                 imcndw.doc_id,
                 imcndw.item,
                 idmcdh.match_id match_hist_id
            from im_match_cn_detl_ws imcndw,
                 im_detail_match_cn_dtl_hist idmcdh
           where imcndw.workspace_id = O_match_wspace_id
             and idmcdh.doc_id       = imcndw.doc_id
             and idmcdh.item         = imcndw.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.match_hist_id = src.match_hist_id;

   LOGGER.LOG_INFORMATION('Merge CN_WS Match History ID - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_detl_ws tgt
   using (select imcnrdw.workspace_id,
                 imcnrdw.doc_id,
                 imcnrdw.item,
                 idmcdh.match_id match_hist_id
            from im_match_cnr_detl_ws imcnrdw,
                 im_detail_match_cn_dtl_hist idmcdh
           where imcnrdw.workspace_id = O_match_wspace_id
             and idmcdh.doc_id        = imcnrdw.doc_id
             and idmcdh.item          = imcnrdw.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.match_hist_id = src.match_hist_id;

   LOGGER.LOG_INFORMATION('Merge CNR_WS Match History ID - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_ws tgt
   using (with im_mtch_cn_ws as (select imcnw.workspace_id,
                                        case
                                           when iddrc_inner.workspace_id is NOT NULL and iddrc_inner.dept_cnt = REIM_CONSTANTS.ONE then
                                              iddrc_inner.dept
                                           else
                                              NULL
                                        end dept,
                                        case
                                           when count(distinct imcnw.supplier_site_id) = REIM_CONSTANTS.ONE then
                                              max(imcnw.supplier_site_id)
                                           else
                                              NULL
                                        end supplier_site_id,
                                        case
                                           when count(distinct imcnw.supplier) = REIM_CONSTANTS.ONE then
                                              max(imcnw.supplier)
                                           else
                                              NULL
                                        end supplier,
                                        case
                                           when count(distinct imcnw.supplier_group_id) = REIM_CONSTANTS.ONE then
                                              max(imcnw.supplier_group_id)
                                           else
                                              NULL
                                        end supplier_group_id
                                   from im_match_cn_ws imcnw,
                                        (select imcnw_inner.workspace_id,
                                                max(im.dept) dept,
                                                count(distinct im.dept) dept_cnt
                                           from im_match_cn_ws imcnw_inner,
                                                im_doc_detail_reason_codes iddrc,
                                                item_master im
                                          where imcnw_inner.workspace_id = O_match_wspace_id
                                            and iddrc.doc_id             = imcnw_inner.doc_id
                                            and im.item                  = iddrc.item
                                          GROUP BY imcnw_inner.workspace_id) iddrc_inner
                                  where imcnw.workspace_id           = O_match_wspace_id
                                    and iddrc_inner.workspace_id (+) = imcnw.workspace_id
                                  GROUP BY imcnw.workspace_id,
                                           iddrc_inner.workspace_id,
                                           iddrc_inner.dept_cnt,
                                           iddrc_inner.dept),
               im_tolerance_ws as (select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SUPP_SITE tolerance_rank
                                     from im_tolerance_level_map
                                    where tolerance_level_type = REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_SITE
                                   union all
                                   select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SUPP tolerance_rank
                                     from im_tolerance_level_map
                                    where tolerance_level_type = REIM_CONSTANTS.TLR_LVL_TYPE_SUPP
                                   union all
                                   select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SUPP_GRP tolerance_rank
                                     from im_tolerance_level_map
                                    where tolerance_level_type = REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_GRP
                                   union all
                                   select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_DEPT tolerance_rank
                                     from im_tolerance_level_map
                                    where tolerance_level_type                   = REIM_CONSTANTS.TLR_LVL_TYPE_DEPT
                                   union all
                                   select NULL tolerance_level_type,
                                          NULL tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SYSTEM tolerance_rank
                                     from im_tolerance_head
                                    where system_default = 'Y'),
               im_curr_conv_ws as (select from_currency,
                                          to_currency,
                                          effective_date,
                                          exchange_rate,
                                          exchange_type,
                                          RANK() OVER (PARTITION BY from_currency,
                                                                    to_currency,
                                                                    exchange_type
                                                           ORDER BY effective_date desc) date_rank
                                     from mv_currency_conversion_rates
                                    where exchange_type  = REIM_CONSTANTS.EXCH_TYPE_CONSOLIDATED
                                      and effective_date <= L_vdate)
          select imcw.workspace_id,
                 imcnw_ws.doc_id,
                 itw.tolerance_id,
                 RANK() OVER (PARTITION BY imcw.workspace_id
                                  ORDER BY itw.tolerance_rank desc) tolerance_rank,
                 ith.currency_code tolerance_currency_code,
                 iccw.exchange_rate tolerance_exchange_rate
            from im_mtch_cn_ws imcw,
                 im_tolerance_ws itw,
                 im_tolerance_head ith,
                 im_match_cn_ws imcnw_ws,
                 im_curr_conv_ws iccw
           where ((   imcw.dept             = Decode(itw.tolerance_level_type,
                                                     REIM_CONSTANTS.TLR_LVL_TYPE_DEPT, itw.tolerance_level_id,
                                                     NULL)
                  or imcw.supplier_site_id  = Decode(itw.tolerance_level_type,
                                                     REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_SITE, itw.tolerance_level_id,
                                                     NULL)
                  or imcw.supplier          = Decode(itw.tolerance_level_type,
                                                     REIM_CONSTANTS.TLR_LVL_TYPE_SUPP, itw.tolerance_level_id,
                                                     NULL)
                  or imcw.supplier_group_id = Decode(itw.tolerance_level_type,
                                                     REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_GRP, itw.tolerance_level_id,
                                                     NULL))
                  or itw.tolerance_rank     = REIM_CONSTANTS.ONE)
              and ith.tolerance_id          = itw.tolerance_id
              and imcnw_ws.workspace_id     = imcw.workspace_id
              and iccw.from_currency        = imcnw_ws.currency_code
              and iccw.to_currency          = ith.currency_code
              and iccw.date_rank            = REIM_CONSTANTS.ONE) src
   on (    tgt.workspace_id   = src.workspace_id
       and tgt.doc_id         = src.doc_id
       and src.tolerance_rank = REIM_CONSTANTS.ONE)
   when MATCHED then
      update
         set tgt.tolerance_id            = src.tolerance_id,
             tgt.tolerance_currency_code = src.tolerance_currency_code,
             tgt.tolerance_exchange_rate = src.tolerance_exchange_rate;

   LOGGER.LOG_INFORMATION('Merge Tolerance im_match_cn_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   
   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when multi_supp then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_SUPP', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END INIT_ONLINE_MATCH_DATA;
----------------------------------------------------------------
FUNCTION PERSIST_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                 I_workspace_id  IN OUT IM_MATCH_CN_DETL_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_CN_SQL.PERSIST_DETAIL_MATCH_WS';
   L_start_time   TIMESTAMP := SYSTIMESTAMP;
   L_user    user_attrib.user_id%TYPE;
   L_vdate   DATE := get_vdate;

   L_match_key_id   IM_MATCH_INVC_WS.MATCH_KEY_ID%TYPE         := im_match_key_id_seq.NEXTVAL;
   L_match_group_id IM_MATCH_GROUP_HEAD_WS.MATCH_GROUP_ID%TYPE := im_match_group_seq.NEXTVAL;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program || ' I_workspace_id:' || I_workspace_id);

   L_user := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
   if L_user is null then
      L_user := get_user;
   end if;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --write match history
   --------------------------------------

   delete from gtt_num_num_str_str_date_date;

   --number_1   im_detail_match_cn_hist_seq
   --varchar2_1 item
   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1)
   select im_detail_match_cn_hist_seq.nextval,
          d.item
     from im_detail_match_cn_ws d
    where d.workspace_id  = I_workspace_id
      and d.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
      and d.choice_flag   = 'Y'
      and d.ui_filter_ind = 'N';

   insert into im_cn_detail_match_his(match_id,
                                      doc_id,
                                      item,
                                      reason_code_id,
                                      created_by,
                                      creation_date,
                                      last_updated_by,
                                      last_update_date,
                                      object_version_id)
      select gtt.number_1,
             d.cn_id,
             d.item,
             null,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt,
             im_detail_match_cn_ws d
       where gtt.varchar2_1 = d.item
         and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
         and d.choice_flag  = 'Y'
         and d.workspace_id = I_workspace_id
      union all
      select gtt.number_1,
             d.cnr_id,
             d.item,
             drc.reason_code_id,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt,
             im_detail_match_cn_ws d,
             im_doc_detail_reason_codes drc
       where gtt.varchar2_1 = d.item
         and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
         and d.choice_flag  = 'Y'
         and d.workspace_id = I_workspace_id
         and d.cnr_id       = drc.doc_id
         and d.item         = drc.item;

   LOGGER.LOG_INFORMATION(L_program||' insert im_cn_detail_match_his - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to doc entities
   --------------------------------------

   merge into im_doc_detail_reason_codes target
   using (select dw.cnr_id doc_id,
                 dw.item
            from gtt_num_num_str_str_date_date gtt,
                 im_detail_match_cn_ws dw
           where dw.workspace_id = I_workspace_id
             and dw.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
             and dw.choice_flag  = 'Y'
             and dw.item         = gtt.varchar2_1
          union all
          select dw.cn_id doc_id,
                 dw.item
            from gtt_num_num_str_str_date_date gtt,
                 im_detail_match_cn_ws dw
           where dw.workspace_id = I_workspace_id
             and dw.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
             and dw.choice_flag  = 'Y'
             and dw.item         = gtt.varchar2_1
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.cost_matched                   = 'Y',
        target.qty_matched                    = 'Y',
        target.last_updated_by                = L_user,
        target.last_update_date               = sysdate,
        target.object_version_id              = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_detail_reason_codes - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head target
   using (with match_doc as (select doc_id,
                                    SUM(DECODE(variance_rnk,
                                               REIM_CONSTANTS.ONE, total_cost_var_item,
                                               REIM_CONSTANTS.ZERO)) total_cost_variance
                               from (select dcn.cn_id doc_id,
                                            (ds.unit_cost_variance * dcn.cn_qty) + (ds.qty_variance * dcn.cn_unit_cost) total_cost_var_item,
                                            rank() over (partition by dcn.cn_id, dcn.item order by dcn.cn_ext_cost desc, dcn.cn_id) variance_rnk
                                       from im_detail_match_cn_ws ds,
                                            im_detail_match_cn_ws dcn
                                      where ds.workspace_id          = I_workspace_id
                                        and ds.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
                                        and ds.choice_flag           = 'Y'
                                        and ds.ui_filter_ind         = 'N'
                                        --
                                        and dcn.workspace_id         = I_workspace_id
                                        and dcn.entity_type          = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
                                        and dcn.choice_flag          = 'Y'
                                        and dcn.ui_filter_ind        = 'N'
                                        --
                                        and ds.item                  = dcn.item)
                              group by doc_id)
          select md.doc_id,
                 min(md.total_cost_variance) total_cost_variance,
                 SUM(DECODE(iddrc.cost_matched,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE))
                 +
                 SUM(DECODE(iddrc.qty_matched,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) unmatch_count
            from match_doc md,
                 im_doc_detail_reason_codes iddrc
           where iddrc.doc_id = md.doc_id
           group by md.doc_id
          union all
          select dcnr.cnr_id doc_id,
                 NULL total_cost_variance,
                 SUM(DECODE(iddrc.cost_matched,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE))
                 +
                 SUM(DECODE(iddrc.qty_matched,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) unmatch_count
            from im_detail_match_cn_ws ds,
                 im_detail_match_cn_ws dcnr,
                 im_doc_detail_reason_codes iddrc
           where ds.workspace_id          = I_workspace_id
             and ds.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
             and ds.choice_flag           = 'Y'
             and ds.ui_filter_ind         = 'N'
             --
             and dcnr.workspace_id        = I_workspace_id
             and dcnr.entity_type         = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
             and dcnr.choice_flag         = 'Y'
             and dcnr.ui_filter_ind       = 'N'
             --
             and ds.item                  = dcnr.item
             --
             and dcnr.cnr_id              = iddrc.doc_id
           group by dcnr.cnr_id) use_this
   on (target.doc_id  = use_this.doc_id)
   when matched then update
    set target.status                    = decode(use_this.unmatch_count,
                                                  REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                  target.status),
        target.variance_within_tolerance = NVL(target.variance_within_tolerance, REIM_CONSTANTS.ZERO) + use_this.total_cost_variance,
        target.match_id                  = decode(use_this.unmatch_count,
                                                  REIM_CONSTANTS.ZERO, L_user,
                                                  NULL),
        target.match_date                = decode(use_this.unmatch_count,
                                                  REIM_CONSTANTS.ZERO, L_vdate,
                                                  NULL),
        target.match_type                = decode(use_this.unmatch_count,
                                                  REIM_CONSTANTS.ZERO, REIM_CONSTANTS.MATCH_TYPE_MANUAL,
                                                  NULL),
        target.last_updated_by           = L_user,
        target.last_update_date          = sysdate,
        target.object_version_id         = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --push match to workspace
   --------------------------------------

   if REIM_ONLINE_MATCH_CN_SQL.REFRESH_MTCH_WSPACE(O_error_message,
                                                   I_workspace_id,
                                                   'N') = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --refresh the detail workspace
   --------------------------------------

   if REIM_ONLINE_MATCH_CN_SQL.CREATE_MATCH_CN_DETAIL_WS(O_error_message,
                                                         I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERSIST_DETAIL_MATCH_WS;
--------------------------------------------------------------------
-- The public function used for Refreshing Credit Note Match Data for Online matching.
-- Input param: I_workspace_id (The ID of the Match workspace)
--              I_skip_matched (Identifier to unselect Matched documents)
--
FUNCTION REFRESH_MTCH_WSPACE(O_error_message    OUT VARCHAR2,
                             I_workspace_id  IN     IM_MATCH_CN_DETL_WS.WORKSPACE_ID%TYPE,
                             I_skip_matched  IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_CN_SQL.REFRESH_MTCH_WSPACE';
   L_start_time  TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program);

   delete from im_match_cn_detl_ws imcndw
    where EXISTS (select 'x'
                    from im_match_cn_ws imcnw
                   where imcnw.workspace_id  = I_workspace_id
                     and imcnw.choice_flag   = 'Y'
                     and imcndw.workspace_id = imcnw.workspace_id
                     and imcndw.doc_id       = imcnw.doc_id);

   LOGGER.LOG_INFORMATION('Delete CN_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_cnr_detl_ws imcnrdw
    where EXISTS (select 'x'
                    from im_match_cnr_ws imcnrw
                   where imcnrw.workspace_id  = I_workspace_id
                     and imcnrw.choice_flag   = 'Y'
                     and imcnrdw.workspace_id = imcnrw.workspace_id
                     and imcnrdw.doc_id       = imcnrw.doc_id);

   LOGGER.LOG_INFORMATION('Delete CNR_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_cn_tax_ws imcntw
    where EXISTS (select 'x'
                    from im_match_cn_ws imcnw
                   where imcnw.workspace_id  = I_workspace_id
                     and imcnw.choice_flag   = 'Y'
                     and imcntw.workspace_id = imcnw.workspace_id
                     and imcntw.doc_id       = imcnw.doc_id);

   LOGGER.LOG_INFORMATION('Delete CN_TAX_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_cnr_tax_ws imcnrtw
    where EXISTS (select 'x'
                    from im_match_cn_ws imcnw
                   where imcnw.workspace_id  = I_workspace_id
                     and imcnw.choice_flag   = 'Y'
                     and imcnrtw.workspace_id = imcnw.workspace_id
                     and imcnrtw.doc_id       = imcnw.doc_id);

   LOGGER.LOG_INFORMATION('Delete CNR_TAX_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_ws tgt
   using (select imcnw.workspace_id workspace_id,
                 imcnw.doc_id,
                 idh.object_version_id doc_head_version_id,
                 idh.status,
                 idh.total_cost total_avail_cost, --merge later
                 idh.total_qty total_avail_qty, --merge later
                 idh.total_cost merch_amount, -- merge later
                 idh.total_qty,
                 idh.header_only,
                 icnmg.group_id manual_group_id,
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig, --revisit
                 idh.hold_status,
                 case 
                    when I_skip_matched = 'Y' then
                       DECODE(idh.status,
                              REIM_CONSTANTS.DOC_STATUS_MTCH, 'N',
                              'Y')
                    else
                       'Y'
                 end choice_flag
            from im_match_cn_ws imcnw,
                 im_doc_head idh,
                 im_cn_manual_groups icnmg
           where imcnw.workspace_id = I_workspace_id
             and imcnw.choice_flag  = 'Y'
             and idh.doc_id         = imcnw.doc_id
             and icnmg.doc_id(+)    = imcnw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.doc_head_version_id = src.doc_head_version_id,
             tgt.status              = src.status,
             tgt.total_avail_cost    = src.total_avail_cost,
             tgt.total_avail_qty     = src.total_avail_qty,
             tgt.merch_amount        = src.merch_amount,
             tgt.total_qty           = src.total_qty,
             tgt.header_only         = src.header_only,
             tgt.manual_group_id     = src.manual_group_id,
             tgt.summary_mtch_elig   = src.summary_mtch_elig,
             tgt.detail_mtch_elig    = src.detail_mtch_elig,
             tgt.hold_status         = src.hold_status,
             tgt.choice_flag         = src.choice_flag;

   LOGGER.LOG_INFORMATION('Merge CN_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select imcnrw.workspace_id workspace_id,
                 imcnrw.doc_id,
                 idh.object_version_id doc_head_version_id,
                 idh.status,
                 idh.total_cost total_avail_cost, --merge later
                 idh.total_qty total_avail_qty, --merge later
                 idh.total_cost merch_amount, -- merge later
                 idh.total_qty,
                 idh.header_only,
                 icnmg.group_id manual_group_id,
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig, --revisit
                 idh.hold_status,
                 case 
                    when I_skip_matched = 'Y' then
                       DECODE(idh.status,
                              REIM_CONSTANTS.DOC_STATUS_MTCH, 'N',
                              'Y')
                    else
                       'Y'
                 end choice_flag
            from im_match_cnr_ws imcnrw,
                 im_doc_head idh,
                 im_cn_manual_groups icnmg
           where imcnrw.workspace_id = I_workspace_id
             and imcnrw.choice_flag  = 'Y'
             and idh.doc_id          = imcnrw.doc_id
             and icnmg.doc_id(+)     = imcnrw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.doc_head_version_id = src.doc_head_version_id,
             tgt.status              = src.status,
             tgt.total_avail_cost    = src.total_avail_cost,
             tgt.total_avail_qty     = src.total_avail_qty,
             tgt.merch_amount        = src.merch_amount,
             tgt.total_qty           = src.total_qty,
             tgt.header_only         = src.header_only,
             tgt.manual_group_id     = src.manual_group_id,
             tgt.summary_mtch_elig   = src.summary_mtch_elig,
             tgt.detail_mtch_elig    = src.detail_mtch_elig,
             tgt.hold_status         = src.hold_status,
             tgt.choice_flag         = src.choice_flag;

   LOGGER.LOG_INFORMATION('Merge CNR_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(number_1,
                                             number_2)
      select distinct imcnw.manual_group_id,
             imcnw.loc_set_of_books_id
        from im_match_cn_ws imcnw
       where imcnw.workspace_id    = I_workspace_id
         and imcnw.choice_flag  = 'Y'
         and imcnw.manual_group_id is NOT NULL
      union
      select distinct imcnrw.manual_group_id,
             imcnrw.loc_set_of_books_id
        from im_match_cnr_ws imcnrw
       where imcnrw.workspace_id    = I_workspace_id
         and imcnrw.choice_flag     = 'Y'
         and imcnrw.manual_group_id is NOT NULL;

   merge into im_match_cn_ws tgt
   using (select I_workspace_id workspace_id,
                 idh.doc_id,
                 idh.object_version_id doc_head_version_id,
                 idh.status,
                 idh.doc_date,
                 idh.due_date,
                 so.group_id supplier_group_id, -- revisit
                 idh.vendor supplier,
                 sups_parent.sup_name supplier_name,
                 sups_parent.contact_phone supplier_phone, -- revisit
                 idh.supplier_site_id,
                 sups.sup_name supplier_site_name,
                 sups.vat_region supplier_site_vat_region,
                 idh.currency_code,
                 idh.order_no, -- revisit
                 idh.location,
                 idh.loc_type,
                 loc.loc_name location_name,
                 loc.vat_region loc_vat_region,
                 gtt.number_2 loc_set_of_books_id,
                 idh.total_cost total_avail_cost, --merge later
                 idh.total_qty total_avail_qty, --merge later
                 idh.total_cost merch_amount, -- merge later
                 idh.total_qty,
                 idh.header_only,
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig,
                 NVL(so.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                 NVL(so.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                 NVL(so.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                 icnmg.group_id manual_group_id,
                 NULL cn_match_ref_cnr_ext_doc_id, -- revisit
                 NULL cn_match_ref_invc_ext_doc_id, -- revisit
                 idh.deal_id, -- revisit
                 NULL deal_detail_id, -- revisit
                 idh.deal_type, -- revisit
                 idh.rtv_order_no,
                 idh.rtv_ind, -- revisit
                 idh.hold_status, -- revisit
                 'Y' choice_flag
            from gtt_num_num_str_str_date_date gtt,
                 im_cn_manual_groups icnmg,
                 im_doc_head idh,
                 v_im_supp_site_attrib_expl so,
                 sups sups_parent,
                 sups sups,
                 (select store loc,
                         REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                         store_name loc_name,
                         vat_region
                    from store
                  union all
                  select wh loc,
                         REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                         wh_name loc_name,
                         vat_region
                    from wh
                   where wh = physical_wh) loc
           where icnmg.group_id       = gtt.number_1
             and idh.doc_id           = icnmg.doc_id
             and idh.type             = REIM_CONSTANTS.DOC_TYPE_CRDNT
             and so.supplier          = idh.supplier_site_id
             and sups_parent.supplier = idh.vendor
             and sups.supplier        = idh.supplier_site_id
             and loc.loc              = idh.location
             and loc.loc_type         = idh.loc_type) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when NOT MATCHED THEN
      insert (workspace_id,
              doc_id,
              doc_head_version_id,
              status,
              doc_date,
              due_date,
              supplier_group_id,
              supplier,
              supplier_name,
              supplier_phone,
              supplier_site_id,
              supplier_site_name,
              supplier_site_vat_region,
              currency_code,
              order_no,
              location,
              loc_type,
              location_name,
              loc_vat_region,
              loc_set_of_books_id,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              header_only,
              summary_mtch_elig,
              detail_mtch_elig,
              qty_required,
              qty_match_required,
              sku_comp_percent,
              manual_group_id,
              cn_match_ref_cnr_ext_doc_id,
              cn_match_ref_invc_ext_doc_id,
              deal_id,
              deal_detail_id,
              deal_type,
              rtv_order_no,
              rtv_ind,
              hold_status,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.doc_head_version_id,
              src.status,
              src.doc_date,
              src.due_date,
              src.supplier_group_id,
              src.supplier,
              src.supplier_name,
              src.supplier_phone,
              src.supplier_site_id,
              src.supplier_site_name,
              src.supplier_site_vat_region,
              src.currency_code,
              src.order_no,
              src.location,
              src.loc_type,
              src.location_name,
              src.loc_vat_region,
              src.loc_set_of_books_id,
              src.total_avail_cost, --merge later
              src.total_avail_qty, --merge later
              src.merch_amount, -- merge later
              src.total_qty, -- merge later
              src.header_only, -- header_only, merge later
              src.summary_mtch_elig, -- summary_mtch_elig, merge later
              src.detail_mtch_elig, -- detail_mtch_elig, merge later
              src.qty_required, -- to be confirmed
              src.qty_match_required, -- to be confirmed
              src.sku_comp_percent,
              src.manual_group_id,
              src.cn_match_ref_cnr_ext_doc_id,
              src.cn_match_ref_invc_ext_doc_id,
              src.deal_id,
              src.deal_detail_id,
              src.deal_type,
              src.rtv_order_no,
              src.rtv_ind,
              src.hold_status,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert Credit Notes from MANUAL GROUPS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select I_workspace_id workspace_id,
                 idh.doc_id,
                 idh.type,
                 idh.object_version_id doc_head_version_id,
                 idh.status,
                 idh.doc_date,
                 idh.due_date,
                 so.group_id supplier_group_id, -- revisit
                 idh.vendor supplier,
                 sups_parent.sup_name supplier_name,
                 sups_parent.contact_phone supplier_phone, -- revisit
                 idh.supplier_site_id,
                 sups.sup_name supplier_site_name,
                 sups.vat_region supplier_site_vat_region,
                 idh.currency_code,
                 idh.order_no, -- revisit
                 idh.location,
                 idh.loc_type,
                 loc.loc_name location_name,
                 loc.vat_region loc_vat_region,
                 gtt.number_2 loc_set_of_books_id,
                 idh.total_cost total_avail_cost, --merge later
                 idh.total_qty total_avail_qty, --merge later
                 idh.total_cost merch_amount, -- merge later
                 idh.total_qty,
                 idh.header_only,
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig,
                 NVL(so.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                 NVL(so.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                 --NVL(so.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                 icnmg.group_id manual_group_id,
                 NULL cn_match_ref_cnr_ext_doc_id, -- revisit
                 NULL cn_match_ref_invc_ext_doc_id, -- revisit
                 idh.deal_id, -- revisit
                 NULL deal_detail_id, -- revisit
                 idh.deal_type, -- revisit
                 idh.rtv_order_no,
                 idh.rtv_ind, -- revisit
                 idh.hold_status, -- revisit
                 'Y' choice_flag
            from gtt_num_num_str_str_date_date gtt,
                 im_cn_manual_groups icnmg,
                 im_doc_head idh,
                 v_im_supp_site_attrib_expl so,
                 sups sups_parent,
                 sups sups,
                 (select store loc,
                         REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                         store_name loc_name,
                         vat_region
                    from store
                  union all
                  select wh loc,
                         REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                         wh_name loc_name,
                         vat_region
                    from wh
                   where wh = physical_wh) loc
           where icnmg.group_id       = gtt.number_1
             and idh.doc_id           = icnmg.doc_id
             and idh.type             IN (REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                                          REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                                          REIM_CONSTANTS.DOC_TYPE_CRDNRT)
             and so.supplier          = idh.supplier_site_id
             and sups_parent.supplier = idh.vendor
             and sups.supplier        = idh.supplier_site_id
             and loc.loc              = idh.location
             and loc.loc_type         = idh.loc_type) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when NOT MATCHED THEN
      insert (workspace_id,
              doc_id,
              type,
              doc_head_version_id,
              status,
              doc_date,
              due_date,
              supplier_group_id,
              supplier,
              supplier_name,
              supplier_phone,
              supplier_site_id,
              supplier_site_name,
              supplier_site_vat_region,
              currency_code,
              order_no,
              location,
              loc_type,
              location_name,
              loc_vat_region,
              loc_set_of_books_id,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              header_only,
              summary_mtch_elig,
              detail_mtch_elig,
              qty_required,
              qty_match_required,
              --sku_comp_percent,
              manual_group_id,
              cn_match_ref_cnr_ext_doc_id,
              cn_match_ref_invc_ext_doc_id,
              deal_id,
              deal_detail_id,
              deal_type,
              rtv_order_no,
              rtv_ind,
              hold_status,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.type,
              src.doc_head_version_id,
              src.status,
              src.doc_date,
              src.due_date,
              src.supplier_group_id,
              src.supplier,
              src.supplier_name,
              src.supplier_phone,
              src.supplier_site_id,
              src.supplier_site_name,
              src.supplier_site_vat_region,
              src.currency_code,
              src.order_no,
              src.location,
              src.loc_type,
              src.location_name,
              src.loc_vat_region,
              src.loc_set_of_books_id,
              src.total_avail_cost, --merge later
              src.total_avail_qty, --merge later
              src.merch_amount, -- merge later
              src.total_qty, -- merge later
              src.header_only, -- header_only, merge later
              src.summary_mtch_elig, -- summary_mtch_elig, merge later
              src.detail_mtch_elig, -- detail_mtch_elig, merge later
              src.qty_required, -- to be confirmed
              src.qty_match_required, -- to be confirmed
              --src.sku_comp_percent,
              src.manual_group_id,
              src.cn_match_ref_cnr_ext_doc_id,
              src.cn_match_ref_invc_ext_doc_id,
              src.deal_id,
              src.deal_detail_id,
              src.deal_type,
              src.rtv_order_no,
              src.rtv_ind,
              src.hold_status,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert Credit Note Requests from MANUAL GROUPS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_tax_ws tgt
   using (select imcnw.workspace_id,
                 imcnw.doc_id,
                 idt.tax_code,
                 idt.tax_rate,
                 idt.tax_basis,
                 idt.tax_amount
      from im_match_cn_ws imcnw,
           im_doc_tax idt
     where imcnw.workspace_id = I_workspace_id
	   and imcnw.choice_flag  = 'Y'
       and idt.doc_id         = imcnw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.tax_code     = src.tax_code
       and tgt.tax_rate     = src.tax_rate)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              tax_code,
              tax_rate,
              tax_basis,
              tax_amount)
      values (src.workspace_id,
              src.doc_id,
              src.tax_code,
              src.tax_rate,
              src.tax_basis,
              src.tax_amount);

   LOGGER.LOG_INFORMATION('Insert CN_TAX_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_tax_ws tgt
   using (select imcnrw.workspace_id,
                 imcnrw.doc_id,
                 idt.tax_code,
                 idt.tax_rate,
                 idt.tax_basis,
                 idt.tax_amount
      from im_match_cnr_ws imcnrw,
           im_doc_tax idt
     where imcnrw.workspace_id = I_workspace_id
	   and imcnrw.choice_flag  = 'Y'
       and idt.doc_id          = imcnrw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.tax_code     = src.tax_code
       and tgt.tax_rate     = src.tax_rate)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              tax_code,
              tax_rate,
              tax_basis,
              tax_amount)
      values (src.workspace_id,
              src.doc_id,
              src.tax_code,
              src.tax_rate,
              src.tax_basis,
              src.tax_amount);

   LOGGER.LOG_INFORMATION('Insert CNR_TAX_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_ws tgt
   using (select imcntw.workspace_id,
                 imcntw.doc_id,
                 count(distinct imcntw.tax_code) over (partition by imcntw.workspace_id, imcntw.doc_id) tax_code_cnt,
                 min(imcntw.tax_code)   over (partition by imcntw.workspace_id, imcntw.doc_id) tax_code,
                 min(imcntw.tax_rate)   over (partition by imcntw.workspace_id, imcntw.doc_id) tax_rate,
                 sum(imcntw.tax_amount) over (partition by imcntw.workspace_id, imcntw.doc_id) tax_amount
            from im_match_cn_tax_ws imcntw,
			     im_match_cn_ws imcnw
           where imcntw.workspace_id = I_workspace_id
		     and imcnw.workspace_id  = imcntw.workspace_id
			 and imcnw.doc_id        = imcntw.doc_id
			 and imcnw.choice_flag   = 'Y') src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and src.tax_code_cnt = 1)
   when MATCHED then
      update
         set tgt.tax_code   = src.tax_code,
             tgt.tax_rate   = src.tax_rate,
             tgt.tax_amount = src.tax_amount;

   LOGGER.LOG_INFORMATION('Merge CN_WS with tax (documents with single tax) - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select imcnrtw.workspace_id,
                 imcnrtw.doc_id,
                 count(distinct imcnrtw.tax_code) over (partition by imcnrtw.workspace_id, imcnrtw.doc_id) tax_code_cnt,
                 min(imcnrtw.tax_code)   over (partition by imcnrtw.workspace_id, imcnrtw.doc_id) tax_code,
                 min(imcnrtw.tax_rate)   over (partition by imcnrtw.workspace_id, imcnrtw.doc_id) tax_rate,
                 sum(imcnrtw.tax_amount) over (partition by imcnrtw.workspace_id, imcnrtw.doc_id) tax_amount
            from im_match_cnr_tax_ws imcnrtw,
			     im_match_cnr_ws imcnrw
           where imcnrtw.workspace_id = I_workspace_id
		     and imcnrw.workspace_id  = imcnrtw.workspace_id
			 and imcnrw.doc_id        = imcnrtw.doc_id
			 and imcnrw.choice_flag   = 'Y') src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and src.tax_code_cnt = 1)
   when MATCHED then
      update
         set tgt.tax_code   = src.tax_code,
             tgt.tax_rate   = src.tax_rate,
             tgt.tax_amount = src.tax_amount;

   LOGGER.LOG_INFORMATION('Merge CNR_WS with tax (documents with single tax) - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_ws tgt
   using (select imcnw.workspace_id,
                 imcnw.doc_id,
                 SUM(idnm.non_merch_amt) non_merch_amt
            from im_match_cn_ws imcnw,
                 im_doc_non_merch idnm
           where imcnw.workspace_id = I_workspace_id
             and imcnw.choice_flag  = 'Y'
             and idnm.doc_id        = imcnw.doc_id
           GROUP BY imcnw.workspace_id,
                    imcnw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.merch_amount     = tgt.merch_amount - src.non_merch_amt,
             tgt.total_avail_cost = tgt.total_avail_cost - src.non_merch_amt;

   LOGGER.LOG_INFORMATION('Merge CN_WS NON_MERCH_AMT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select imcnrw.workspace_id,
                 imcnrw.doc_id,
                 SUM(idnm.non_merch_amt) non_merch_amt
            from im_match_cnr_ws imcnrw,
                 im_doc_non_merch idnm
           where imcnrw.workspace_id = I_workspace_id
             and imcnrw.choice_flag  = 'Y'
             and idnm.doc_id         = imcnrw.doc_id
           GROUP BY imcnrw.workspace_id,
                    imcnrw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.merch_amount     = tgt.merch_amount - src.non_merch_amt,
             tgt.total_avail_cost = tgt.total_avail_cost - src.non_merch_amt;

   LOGGER.LOG_INFORMATION('Merge CNR_WS NON_MERCH_AMT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_detl_ws tgt
   using (select imcnw.workspace_id,
                 imcnw.doc_id,
                 iddrc.item,
                 its.vpn,
                 case when iddrc.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                                                  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD) 
                       and iddrc.qty_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                                                  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD) then
                         REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
                      else
                         REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                 end status,
                 iddrc.adjusted_unit_cost unit_cost,
                 iddrc.adjusted_qty doc_qty,
                 iddrc.cost_matched,
                 iddrc.qty_matched,
                 NULL match_hist_id, --revisit
                 rct.tax_code,
                 rct.tax_rate,
                 rct.tax_amount,
                 imcnw.choice_flag
      from im_match_cn_ws imcnw,
           im_doc_detail_reason_codes iddrc,
           im_doc_detail_rc_tax rct,
           item_supplier its
     where imcnw.workspace_id                  = I_workspace_id
       and imcnw.choice_flag                   = 'Y'
       and iddrc.doc_id                        = imcnw.doc_id
       and iddrc.im_doc_detail_reason_codes_id = rct.im_doc_detail_reason_codes_id (+)
       and its.item (+)                        = iddrc.item
       and its.supplier (+)                    = imcnw.supplier_site_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              item,
              vpn,
              status,
              unit_cost,
              doc_qty,
              cost_matched,
              qty_matched,
              match_hist_id,
              tax_code,
              tax_rate,
              tax_amount,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.item,
              src.vpn,
              src.status,
              src.unit_cost,
              src.doc_qty,
              src.cost_matched,
              src.qty_matched,
              src.match_hist_id,
              src.tax_code,
              src.tax_rate,
              src.tax_amount,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert CN_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_detl_ws tgt
   using (select imcnrw.workspace_id,
                 imcnrw.doc_id,
                 iddrc.item,
                 its.vpn,
                 iddrc.reason_code_id,
                 case when iddrc.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                                                  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD) 
                       and iddrc.qty_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                                                  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD) then
                         REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
                      else
                         REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                 end status,
                 iddrc.adjusted_unit_cost unit_cost,
                 iddrc.adjusted_qty doc_qty,
                 iddrc.cost_matched,
                 iddrc.qty_matched,
                 NULL match_hist_id, --revisit
                 rct.tax_code,
                 rct.tax_rate,
                 rct.tax_amount,
                 imcnrw.choice_flag
      from im_match_cnr_ws imcnrw,
           im_doc_detail_reason_codes iddrc,
           im_doc_detail_rc_tax rct,
           item_supplier its
     where imcnrw.workspace_id                 = I_workspace_id
       and imcnrw.choice_flag                  = 'Y'
       and iddrc.doc_id                        = imcnrw.doc_id
       and iddrc.im_doc_detail_reason_codes_id = rct.im_doc_detail_reason_codes_id (+)
       and its.item (+)                        = iddrc.item
       and its.supplier (+)                    = imcnrw.supplier_site_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              item,
              vpn,
              reason_code_id,
              status,
              unit_cost,
              doc_qty,
              cost_matched,
              qty_matched,
              match_hist_id,
              tax_code,
              tax_rate,
              tax_amount,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.item,
              src.vpn,
              src.reason_code_id,
              src.status,
              src.unit_cost,
              src.doc_qty,
              src.cost_matched,
              src.qty_matched,
              src.match_hist_id,
              src.tax_code,
              src.tax_rate,
              src.tax_amount,
              src.choice_flag);

   LOGGER.LOG_INFORMATION('Insert CNR_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_ws tgt
   using (select imcndw.workspace_id,
                 imcndw.doc_id,
                 SUM(DECODE(imcndw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imcndw.doc_qty,
                            REIM_CONSTANTS.ZERO) * imcndw.unit_cost) unmatch_item_amt,
                 SUM(DECODE(imcndw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imcndw.doc_qty,
                            REIM_CONSTANTS.ZERO)) unmatch_item_qty
            from im_match_cn_detl_ws imcndw
           where imcndw.workspace_id = I_workspace_id
             and imcndw.choice_flag  = 'Y'
           GROUP BY imcndw.workspace_id,
                    imcndw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.unmatch_item_amt,
             tgt.total_avail_qty  = src.unmatch_item_qty,
             tgt.header_only      = 'N';

   LOGGER.LOG_INFORMATION('Merge CN_WS MTCH_ITEM_COST_QTY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select imcnrdw.workspace_id,
                 imcnrdw.doc_id,
                 SUM(DECODE(imcnrdw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imcnrdw.doc_qty,
                            REIM_CONSTANTS.ZERO) * imcnrdw.unit_cost) unmatch_item_amt,
                 SUM(DECODE(imcnrdw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imcnrdw.doc_qty,
                            REIM_CONSTANTS.ZERO)) unmatch_item_qty
            from im_match_cnr_detl_ws imcnrdw
           where imcnrdw.workspace_id  = I_workspace_id
             and imcnrdw.choice_flag  = 'Y'
           GROUP BY imcnrdw.workspace_id,
                    imcnrdw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.unmatch_item_amt,
             tgt.total_avail_qty  = src.unmatch_item_qty,
             tgt.header_only      = 'N';

   LOGGER.LOG_INFORMATION('Merge CNR_WS MTCH_ITEM_COST_QTY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_detl_ws tgt
   using (select imcndw.workspace_id,
                 imcndw.doc_id,
                 imcndw.item,
                 idmcdh.match_id match_hist_id
            from im_match_cn_detl_ws imcndw,
                 im_detail_match_cn_dtl_hist idmcdh
           where imcndw.workspace_id = I_workspace_id
             and imcndw.choice_flag  = 'Y'
             and idmcdh.doc_id       = imcndw.doc_id
             and idmcdh.item         = imcndw.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.match_hist_id = src.match_hist_id;

   LOGGER.LOG_INFORMATION('Merge CN_WS Match History ID - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_detl_ws tgt
   using (select imcnrdw.workspace_id,
                 imcnrdw.doc_id,
                 imcnrdw.item,
                 idmcdh.match_id match_hist_id
            from im_match_cnr_detl_ws imcnrdw,
                 im_detail_match_cn_dtl_hist idmcdh
           where imcnrdw.workspace_id = I_workspace_id
             and imcnrdw.choice_flag  = 'Y'
             and idmcdh.doc_id        = imcnrdw.doc_id
             and idmcdh.item          = imcnrdw.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.match_hist_id = src.match_hist_id;

   LOGGER.LOG_INFORMATION('Merge CNR_WS Match History ID - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REFRESH_MTCH_WSPACE;
----------------------------------------------------------------
FUNCTION DIRTY_LOCK_CHECKS(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_CN_SQL.DIRTY_LOCK_CHECKS';
   L_start_time  TIMESTAMP := SYSTIMESTAMP;

   L_table_name         VARCHAR2(30) := NULL;
   L_dirty_object       VARCHAR2(30) := NULL;
   L_dirty_rec_count    NUMBER       := REIM_CONSTANTS.ZERO;

   dirty_records  EXCEPTION;
   records_locked EXCEPTION;
   PRAGMA         EXCEPTION_INIT(records_locked, -54);

   cursor C_CHK_CN is
      select count(1)
        from im_match_cn_ws cn,
             im_doc_head idh
       where cn.workspace_id        = I_workspace_id
         and cn.choice_flag         = 'Y'
         and cn.status              <> REIM_CONSTANTS.DOC_STATUS_MTCH
         and cn.doc_id              = idh.doc_id
         and cn.doc_head_version_id <> idh.object_version_id;

   cursor C_CHK_CNR is
      select count(1)
        from im_match_cnr_ws cnr,
             im_doc_head idh
       where cnr.workspace_id        = I_workspace_id
         and cnr.choice_flag         = 'Y'
         and cnr.status              <> REIM_CONSTANTS.DOC_STATUS_MTCH
         and cnr.doc_id              = idh.doc_id
         and cnr.doc_head_version_id <> idh.object_version_id;

   cursor C_LOCK_DOC_HEAD is
      select 'x'
        from im_doc_head idh
       where EXISTS (select 'x'
                       from im_match_cn_ws cn
                      where cn.workspace_id = I_workspace_id
                        and cn.choice_flag  = 'Y'
                        and cn.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH
                        and cn.doc_id       = idh.doc_id
                     union all
                     select 'x'
                       from im_match_cnr_ws cnr
                      where cnr.workspace_id = I_workspace_id
                        and cnr.choice_flag  = 'Y'
                        and cnr.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH
                        and cnr.doc_id       = idh.doc_id)
         for UPDATE NOWAIT;

   cursor C_LOCK_REASON_CODE is
      select 'x'
        from im_doc_detail_reason_codes rc
       where EXISTS (select 'x'
                       from im_match_invc_ws cn
                      where cn.workspace_id = I_workspace_id
                        and cn.choice_flag  = 'Y'
                        and cn.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH
                        and cn.doc_id       = rc.doc_id
                     union all
                     select 'x'
                       from im_match_invc_ws cnr
                      where cnr.workspace_id = I_workspace_id
                        and cnr.choice_flag  = 'Y'
                        and cnr.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH
                        and cnr.doc_id       = rc.doc_id)
         for UPDATE NOWAIT;

BEGIN

   LOGGER.LOG_INFORMATION('Start '||L_program||' I_workspace_id:'|| I_workspace_id);

   --DIRTY CHECKS
   L_dirty_object := 'CN DOCUMENT'; --constant
   open C_CHK_CN;
   fetch C_CHK_CN into L_dirty_rec_count;
   close C_CHK_CN;

   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;
   
   L_dirty_object := 'CNR DOCUMENT'; --constant
   open C_CHK_CNR;
   fetch C_CHK_CNR into L_dirty_rec_count;
   close C_CHK_CNR;

   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;

   --CHECK LOCKS
   L_table_name := 'IM_DOC_HEAD'; --constant
   open C_LOCK_DOC_HEAD;
   close C_LOCK_DOC_HEAD;

   L_table_name := 'IM_DOC_DETAIL_REASON_CODES'; --constant
   open C_LOCK_REASON_CODE;
   close C_LOCK_REASON_CODE;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when records_locked then
      O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED', --constant
                                            L_table_name,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when dirty_records then
      O_error_message := SQL_LIB.CREATE_MSG('DIRTY_RECORDS', --constant
                                            L_dirty_object,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DIRTY_LOCK_CHECKS;
----------------------------------------------------------------
FUNCTION PERFORM_SMRY_MATCH_ONLINE(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS
   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_CN_SQL.PERFORM_SMRY_MATCH_ONLINE';
   L_start_time  TIMESTAMP := SYSTIMESTAMP;

   L_tax_cnt  NUMBER(10)  := null;
   L_vdate   DATE         := get_vdate;

   L_match_key_id   IM_MATCH_INVC_WS.MATCH_KEY_ID%TYPE         := im_match_key_id_seq.NEXTVAL;
   L_match_group_id IM_MATCH_GROUP_HEAD_WS.MATCH_GROUP_ID%TYPE := im_match_group_seq.NEXTVAL;

   L_user     user_attrib.user_id%TYPE;
   L_match_id IM_CN_SUMMARY_MATCH_HIS.MATCH_ID%TYPE := IM_CN_SUMMARY_MATCH_HIS_SEQ.NEXTVAL;

BEGIN

   LOGGER.LOG_INFORMATION('Start '||L_program||' I_workspace_id:'||I_workspace_id);

   L_user := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
   if L_user is null then
      L_user := get_user;
   end if;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --create im_match_group_cn_gtt for workspace
   delete from im_match_group_cn_gtt;

   insert into im_match_group_cn_gtt (workspace_id,
                                      cn_group_cost,
                                      cn_group_qty,
                                      cnr_group_cost,
                                      cnr_group_qty,
                                      cost_variance,
                                      qty_variance,
                                      cost_discrepant,
                                      qty_discrepant,
                                      match_available,
                                      match_status,
                                      error_code,
                                      error_context)
                               select I_workspace_id,
                                      inner_cn.cn_group_cost,
                                      inner_cn.cn_group_qty,
                                      inner_cnr.cnr_group_cost,
                                      inner_cnr.cnr_group_qty,
                                      (inner_cn.cn_group_cost - inner_cnr.cnr_group_cost) cost_variance,
                                      (inner_cn.cn_group_qty - inner_cnr.cnr_group_qty) qty_variance,
                                      'N' cost_discrepant,
                                      'N' qty_discrepant,
                                      'Y' match_available,
                                      REIM_CONSTANTS.DOC_STATUS_MTCH match_status,
                                      NULL error_code,
                                      NULL error_context
                                from (select inner.workspace_id,
                                             sum(inner.cn_group_cost) cn_group_cost,
                                             sum(inner.cn_group_qty) cn_group_qty
                                        from (select cnd.workspace_id,
                                                     sum(cnd.unit_cost * cnd.doc_qty) cn_group_cost,
                                                     sum(cnd.doc_qty) cn_group_qty
                                                from im_match_cn_ws cn,
                                                     im_match_cn_detl_ws cnd
                                               where cn.workspace_id  = I_workspace_id
                                                 and cn.choice_flag   = 'Y'
                                                 and cn.header_only   = 'N'
                                                 and cn.workspace_id  = cnd.workspace_id
                                                 and cn.doc_id        = cnd.doc_id
                                                 and cnd.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                                                          REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
                                                 and cnd.qty_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                                                          REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
                                               GROUP BY cnd.workspace_id
                                              union all
                                              select cn.workspace_id,
                                                     sum(cn.merch_amount) cn_group_cost,
                                                     sum(cn.total_qty) cn_group_qty
                                                from im_match_cn_ws cn
                                               where cn.workspace_id = I_workspace_id
                                                 and cn.choice_flag  = 'Y'
                                                 and cn.header_only  = 'Y'
                                               GROUP BY cn.workspace_id) inner
                                       GROUP BY inner.workspace_id) inner_cn,
                                     (select inner2.workspace_id,
                                             sum(inner2.cnr_group_cost) cnr_group_cost,
                                             sum(inner2.cnr_group_qty) cnr_group_qty
                                        from (select cnrd.workspace_id,
                                                     sum(cnrd.unit_cost * cnrd.doc_qty) cnr_group_cost,
                                                     sum(cnrd.doc_qty) cnr_group_qty
                                                from im_match_cnr_ws cnr,
                                                     im_match_cnr_detl_ws cnrd
                                               where cnr.workspace_id  = I_workspace_id
                                                 and cnr.choice_flag   = 'Y'
                                                 and cnr.header_only   = 'N'
                                                 and cnr.workspace_id  = cnrd.workspace_id
                                                 and cnr.doc_id        = cnrd.doc_id
                                                 and cnrd.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                                                           REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
                                                 and cnrd.qty_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                                                           REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
                                               GROUP BY cnrd.workspace_id
                                              union all
                                              select cnr.workspace_id,
                                                     sum(cnr.merch_amount) cnr_group_cost,
                                                     sum(cnr.total_qty) cnr_group_qty
                                                from im_match_cnr_ws cnr
                                               where cnr.workspace_id = I_workspace_id
                                                 and cnr.choice_flag  = 'Y'
                                                 and cnr.header_only  = 'Y'
                                               GROUP BY cnr.workspace_id) inner2
                                       GROUP BY inner2.workspace_id) inner_cnr
                               where inner_cn.workspace_id = I_workspace_id
                                 and inner_cn.workspace_id = inner_cnr.workspace_id;

   LOGGER.LOG_INFORMATION('Insert IM_MATCH_GROUP_CN_HELPER - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_cn_summary_match_his(match_id,
                                       doc_id,
                                       created_by,
                                       creation_date,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
      select L_match_id,
             cn.doc_id,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from im_match_cn_ws cn
       where cn.workspace_id = I_workspace_id
         and cn.choice_flag  = 'Y'
         and cn.status      = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
      union all
      select L_match_id,
             cnr.doc_id,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from im_match_cnr_ws cnr,
             im_doc_head dh
       where cnr.workspace_id = I_workspace_id
         and cnr.choice_flag  = 'Y'
         and cnr.doc_id       = dh.doc_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_cn_summary_match_his - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head target
   using (select cn.doc_id,
                 gh.cost_variance,
                 RANK() OVER (PARTITION BY gh.workspace_id ORDER BY cn.merch_amount) variance_rnk
            from im_match_group_cn_gtt gh,
                 im_match_cn_ws cn
           where gh.workspace_id = I_workspace_id
             and gh.workspace_id = cn.workspace_id
             and cn.choice_flag  = 'Y'
          union all
          select cnr.doc_id,
                 null cost_variance,
                 2 variance_rnk
            from im_match_cnr_ws cnr
           where cnr.workspace_id = I_workspace_id
             and cnr.choice_flag  = 'Y'
   ) use_this
   on (target.doc_id  = use_this.doc_id)
   when matched then update
    set target.status                    = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.variance_within_tolerance = DECODE(use_this.variance_rnk,
                                                  REIM_CONSTANTS.ONE, use_this.cost_variance,
                                                  NULL),
        target.match_type                = REIM_CONSTANTS.MATCH_TYPE_MANUAL,
        target.match_id                  = L_user,
        target.match_date                = L_vdate,
        target.last_updated_by           = L_user,
        target.last_update_date          = sysdate,
        target.object_version_id         = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge to im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_detail_reason_codes target
   using (select rc.doc_id,
                 rc.item,
                 'FaKeReAsOn' reason_code_id
            from im_match_cn_ws cn,
                 im_match_cn_detl_ws cnd,
                 im_doc_detail_reason_codes rc
           where cn.workspace_id = I_workspace_id
             and cn.choice_flag  = 'Y'
             and cn.workspace_id = cnd.workspace_id
             and cn.doc_id       = cnd.doc_id
             and cnd.doc_id      = rc.doc_id
             and cnd.item        = rc.item
             and rc.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                     REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
             and rc.qty_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                     REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
          union all
          select rc.doc_id,
                 rc.item,
                 nvl(rc.reason_code_id, 'FaKeReAsOn') reason_code_id
            from im_match_cnr_ws cnr,
                 im_match_cnr_detl_ws cnrd,
                 im_doc_detail_reason_codes rc
           where cnr.workspace_id = I_workspace_id
             and cnr.choice_flag  = 'Y'
             and cnr.workspace_id = cnrd.workspace_id
             and cnr.doc_id       = cnrd.doc_id
             and cnrd.doc_id      = rc.doc_id
             and cnrd.item            = rc.item
             and nvl(cnrd.reason_code_id,'FaKeReAsOn') = nvl(rc.reason_code_id,'FaKeReAsOn')
             and rc.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                     REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
             and rc.qty_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                     REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
   ) use_this
   on (    target.doc_id                            = use_this.doc_id
       and target.item                              = use_this.item 
       and nvl(target.reason_code_id, 'FaKeReAsOn') = use_this.reason_code_id)
   when matched then update
    set target.cost_matched      = 'Y',
        target.qty_matched       = 'Y',
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE;
        
   LOGGER.LOG_INFORMATION(L_program||' merge to im_doc_detail_reason_codes - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_detl_ws target
   using (select cnd.doc_id,
                 cnd.item,
                 nvl(cnd.reason_code_id, 'FaKeReAsOn') reason_code_id,
                 cn.workspace_id,
                 cnd.status
            from im_match_cn_ws cn,
                 im_match_cn_detl_ws cnd
           where cn.workspace_id = I_workspace_id
             and cn.choice_flag  = 'Y'
             and cn.workspace_id = cnd.workspace_id
             and cn.doc_id       = cnd.doc_id
             and cnd.status      != REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
   ) use_this
   on (    target.doc_id                            = use_this.doc_id
       and target.item                              = use_this.item
       and nvl(target.reason_code_id, 'FaKeReAsOn') = use_this.reason_code_id
       and target.workspace_id                      = use_this.workspace_id)
   when matched then update
    set target.status       = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched = 'Y',
        target.qty_matched  = 'Y',
        target.choice_flag  = 'N';

   LOGGER.LOG_INFORMATION(L_program||' merge to im_match_cn_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_detl_ws target
   using (select cnrd.doc_id,
                 cnrd.item,
                 nvl(cnrd.reason_code_id, 'FaKeReAsOn') reason_code_id,
                 cnr.workspace_id,
                 cnrd.status
            from im_match_cnr_ws cnr,
                 im_match_cnr_detl_ws cnrd
           where cnr.workspace_id = I_workspace_id
             and cnr.choice_flag  = 'Y'
             and cnr.workspace_id = cnrd.workspace_id
             and cnr.doc_id       = cnrd.doc_id
             and cnrd.status      != REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
   ) use_this
   on (    target.doc_id                            = use_this.doc_id
       and target.item                              = use_this.item
       and nvl(target.reason_code_id, 'FaKeReAsOn') = use_this.reason_code_id
       and target.workspace_id                      = use_this.workspace_id)
   when matched then update
    set target.status       = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched = 'Y',
        target.qty_matched  = 'Y',
        target.choice_flag  = 'N';

   LOGGER.LOG_INFORMATION(L_program||' merge to im_match_cnr_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cn_ws target
   using (select cn.doc_id,
                 cn.workspace_id
            from im_match_cn_ws cn
           where cn.workspace_id = I_workspace_id
             and cn.choice_flag  = 'Y'
   ) use_this
   on (    target.doc_id       = use_this.doc_id
       and target.workspace_id = use_this.workspace_id)
   when matched then update
    set target.status      = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.choice_flag = 'N';

   LOGGER.LOG_INFORMATION(L_program||' merge status to im_match_cn_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws target
   using (select cnr.doc_id,
                 cnr.workspace_id
            from im_match_cnr_ws cnr
           where cnr.workspace_id = I_workspace_id
             and cnr.choice_flag  = 'Y'
   ) use_this
   on (    target.doc_id       = use_this.doc_id
       and target.workspace_id = use_this.workspace_id)
   when matched then update
    set target.status      = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.choice_flag = 'N';

   LOGGER.LOG_INFORMATION(L_program||' merge status to im_match_cnr_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERFORM_SMRY_MATCH_ONLINE;
----------------------------------------------------------------
FUNCTION SUGGEST_MATCH(O_error_message    OUT VARCHAR2,
                       O_match_count      OUT NUMBER,
                       I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_CN_SQL.SUGGEST_MATCH';
   L_start_time TIMESTAMP := SYSTIMESTAMP;
   L_vdate   DATE         := get_vdate;

   L_match_key_id   IM_MATCH_INVC_WS.MATCH_KEY_ID%TYPE         := im_match_key_id_seq.NEXTVAL;
   L_cn_group_id    IM_MATCH_GROUP_CN_CN_GTT.CN_GROUP_ID%TYPE  := im_match_group_cn_cn_seq.NEXTVAL;

   L_cn_count        NUMBER       := REIM_CONSTANTS.ZERO;
   L_cnr_count       NUMBER       := REIM_CONSTANTS.ZERO;

   L_table_name      VARCHAR2(30) := NULL;
   L_dirty_object    VARCHAR2(30) := NULL;
   L_dirty_rec_count NUMBER       := REIM_CONSTANTS.ZERO;

   invalid_selection EXCEPTION;
   multi_match       EXCEPTION;
   no_match          EXCEPTION;

   L_mtch_groups     OBJ_NUMERIC_ID_TABLE := NULL;
   
   L_tax_validation_type IM_SYSTEM_OPTIONS.TAX_VALIDATION_TYPE%TYPE := NULL;

   cursor C_FETCH_BEST_MATCH is
      select gh.match_group_id
        from im_match_group_cn_gtt gh
       where gh.workspace_id = I_workspace_id
         and gh.match_status = REIM_CONSTANTS.DOC_STATUS_MTCH;

BEGIN

   LOGGER.LOG_INFORMATION('Start '||L_program||' I_workspace_id:'||I_workspace_id);

   select count(1)
     into L_cn_count
     from im_match_cn_ws cn
    where cn.workspace_id = I_workspace_id
      and cn.choice_flag  = 'Y'
      and cn.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH;

   if L_cn_count = REIM_CONSTANTS.ZERO then
      RAISE invalid_selection;
   end if;

   select count(1)
     into L_cnr_count
     from im_match_cnr_ws cnr
    where cnr.workspace_id = I_workspace_id
      and cnr.choice_flag  = 'Y'
      and cnr.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH;

   if L_cnr_count > REIM_CONSTANTS.ZERO then
      RAISE invalid_selection;
   end if;

   --Select Tax Validation Type
   select tax_validation_type
     into L_tax_validation_type
     from im_system_options;

   --Create Match Key and proceed with match sussgestion.
   update im_match_cn_ws cn
      set cn.match_key_id = L_match_key_id
    where cn.workspace_id = I_workspace_id
      and cn.choice_flag  = 'Y'
      and cn.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH;

   LOGGER.LOG_INFORMATION('Update match key im_match_cn_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

/*is:partially matched - summary match elig flag?*/
   merge into im_match_cn_detl_ws tgt
   using (select cn.workspace_id,
                 cn.doc_id,
                 cn.match_key_id
            from im_match_cn_ws cn
           where cn.workspace_id = I_workspace_id
             and cn.match_key_id = L_match_key_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.status       <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH)
   when MATCHED THEN
      update
         set tgt.match_key_id = src.match_key_id;

   LOGGER.LOG_INFORMATION('Merge match key im_match_cn_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_cnr_ws tgt
   using (select cnr2.workspace_id,
                 cnr2.doc_id
            from (select cnr.workspace_id,
                         cnr.doc_id,
                         cnr.currency_code,
                         nvl(cnr.loc_set_of_books_id,-1) set_of_books_id,
                         nvl(cnr.loc_vat_region, -1) vat_region
                    from im_match_cnr_ws cnr
                   where cnr.workspace_id = I_workspace_id
                     and cnr.choice_flag  = 'N'
                     and cnr.status       = REIM_CONSTANTS.DOC_STATUS_APPRVE
                 ) cnr2,
                 (select distinct 
                         cn.workspace_id,
                         cn.currency_code,
                         nvl(cn.loc_set_of_books_id,-1) set_of_books_id,
                         nvl(cn.loc_vat_region, -1) vat_region
                    from im_match_cn_ws cn
                   where cn.workspace_id = I_workspace_id
                     and cn.match_key_id = L_match_key_id) cn2
           where cnr2.workspace_id    = cn2.workspace_id
             and cnr2.currency_code   = cn2.currency_code
             and cnr2.set_of_books_id = cn2.set_of_books_id
             and cnr2.vat_region      = cn2.vat_region) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.match_key_id = L_match_key_id;

   if SQL%ROWCOUNT = REIM_CONSTANTS.ZERO then
      RAISE no_match;
   end if;

   LOGGER.LOG_INFORMATION('Merge match key im_match_cnr_ws');

/*is:partially matched - summary match elig flag?*/
   merge into im_match_cnr_detl_ws tgt
   using (select cnr.workspace_id,
                 cnr.doc_id,
                 cnr.match_key_id
            from im_match_cnr_ws cnr
           where cnr.workspace_id = I_workspace_id
             and cnr.match_key_id = L_match_key_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.status       <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH)
   when MATCHED THEN
      update
         set tgt.match_key_id = src.match_key_id;

   LOGGER.LOG_INFORMATION('Merge match key im_match_cnr_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_group_cn_cn_gtt;
   insert into im_match_group_cn_cn_gtt(workspace_id,
                                        cn_group_id,
                                        doc_id,
                                        due_date)
                                 select cn.workspace_id,
                                        L_cn_group_id,
                                        cn.doc_id,
                                        cn.due_date
                                   from im_match_cn_ws cn
                                  where cn.workspace_id = I_workspace_id
                                    and cn.match_key_id = L_match_key_id;

   LOGGER.LOG_INFORMATION('Create Invoice Group im_match_group_cn_cn_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if CREATE_CNR_GROUPS(O_error_message,
                        I_workspace_id,
                        L_match_key_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if CREATE_MTCH_GROUP_HEAD(O_error_message,
                             I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if MATCH_GROUP_HEAD(O_error_message,
                       I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if L_tax_validation_type NOT IN (REIM_CONSTANTS.TAX_VALID_TYPE_VENDR,
                                    REIM_CONSTANTS.TAX_VALID_TYPE_NOTAX) then

      if UNMATCH_TAX_DISCREPANT(O_error_message,
                                I_workspace_id) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

   end if;

   --Best Match
   merge into im_match_group_cn_gtt tgt
   using (select gh.match_group_id,
                 RANK() OVER (PARTITION BY gh.workspace_id
                                  ORDER BY DECODE(gh.match_status,
                                                  REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                                  REIM_CONSTANTS.ONE),
                                           ABS(gh.cost_variance),
                                           DECODE(gh.qty_match_required,
                                                  'Y', ABS(gh.qty_variance),
                                                  REIM_CONSTANTS.ONE)) best_match_rank
            from im_match_group_cn_gtt gh
           where gh.workspace_id      = I_workspace_id) src
   on (tgt.match_group_id = src.match_group_id)
   when MATCHED then
      update                           
         set tgt.match_status = DECODE(src.best_match_rank,
                                       REIM_CONSTANTS.ONE, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                       REIM_CONSTANTS.DOC_STATUS_URMTCH);


   LOGGER.LOG_INFORMATION('Merge to select Best Match - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_FETCH_BEST_MATCH;
   fetch C_FETCH_BEST_MATCH BULK COLLECT into L_mtch_groups;
   close C_FETCH_BEST_MATCH;
   if L_mtch_groups is NULL or L_mtch_groups.COUNT = REIM_CONSTANTS.ZERO then
      RAISE no_match;
   elsif L_mtch_groups.COUNT > REIM_CONSTANTS.ONE then
      O_match_count := L_mtch_groups.COUNT;
      RAISE multi_match;
   elsif L_mtch_groups.COUNT = REIM_CONSTANTS.ONE then

      merge into im_match_cnr_ws tgt
      using (select cnr.workspace_id,
                    cnr.doc_id
               from TABLE(CAST(L_mtch_groups as OBJ_NUMERIC_ID_TABLE)) ids,
                    im_match_group_cn_gtt cn,
                    im_match_group_cn_cnr_gtt cnr
              where cn.match_group_id     = value(ids)
                and cn.workspace_id       = I_workspace_id
                and cn.workspace_id       = cnr.workspace_id
                and cn.match_cnr_group_id = cnr.cnr_group_id) src
      on (    tgt.workspace_id = src.workspace_id
          and tgt.doc_id       = src.doc_id)
      when matched then
         update
            set tgt.choice_flag = 'Y';

      LOGGER.LOG_INFORMATION('Merge Best Match CNRs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when no_match then
      O_error_message := SQL_LIB.CREATE_MSG('NO_MATCH', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when multi_match then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_MATCH', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when invalid_selection then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_SELECTION', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END SUGGEST_MATCH;
----------------------------------------------------------------
FUNCTION CREATE_CNR_GROUPS(O_error_message IN OUT VARCHAR2,
                           I_workspace_id  IN     IM_MATCH_CNR_Ws.WORKSPACE_ID%TYPE,
                           I_match_key_id  IN     IM_MATCH_CNR_Ws.MATCH_KEY_ID%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'REIM_ONLINE_MATCH_CN_SQL.CREATE_CNR_GROUPS';
   L_start_time      TIMESTAMP := SYSTIMESTAMP;
   L_best_mtch_limit NUMBER       := 5;

BEGIN

   LOGGER.LOG_INFORMATION('Start '           || L_program ||
                          ' I_workspace_id:' || I_workspace_id ||
                          ' I_match_key_id:' || I_match_key_id);

   delete from im_match_group_cn_cnr_gtt;

   --Insert for match keys exceeding best match limit
   insert into im_match_group_cn_cnr_gtt(workspace_id,
                                         cnr_group_id,
                                         doc_id,
                                         due_date)
              with im_mtch_cnr_ws as (select workspace_id,
                                             match_key_id,
                                             doc_id,
                                             due_date,
                                             match_key_rank,
                                             max_match_key_rank
                                        from (select cnr.workspace_id,
                                                     cnr.match_key_id,
                                                     cnr.doc_id,
                                                     cnr.due_date,
                                                     ROW_NUMBER() OVER (PARTITION BY cnr.workspace_id,
                                                                                     cnr.match_key_id
                                                                            ORDER BY cnr.doc_id) match_key_rank,
                                                     count(1) OVER (PARTITION BY cnr.workspace_id,
                                                                                 cnr.match_key_id) max_match_key_rank
                                                from im_match_cnr_ws cnr     
                                               where cnr.workspace_id = I_workspace_id
                                                 and cnr.match_key_id = I_match_key_id
                                                 and cnr.choice_flag  = 'N'
                                                 and cnr.status = REIM_CONSTANTS.DOC_STATUS_APPRVE)
                                       where max_match_key_rank > L_best_mtch_limit)
                              select cnr2.workspace_id,
                                      im_match_group_cn_cnr_seq.NEXTVAL,
                                      cnr2.doc_id,
                                      cnr2.due_date
                                 from im_mtch_cnr_ws cnr2;

   delete from gtt_10_num_10_str_10_date;
   insert into gtt_10_num_10_str_10_date(number_1,    -- workspace_id
                                         number_2,    -- match_key_id
                                         number_3,    -- doc_id
                                         number_4,    -- match_key_rank
                                         varchar2_1)  -- due_date
   select workspace_id,
          match_key_id,
          doc_id,
          match_key_rank,
          due_date
     from (select cnr.workspace_id,
                  cnr.match_key_id,
                  cnr.doc_id,
                  cnr.due_date,
                  ROW_NUMBER() OVER (PARTITION BY cnr.workspace_id,
                                                  cnr.match_key_id
                                         ORDER BY cnr.doc_id) match_key_rank,
                  count(1) OVER (PARTITION BY cnr.workspace_id,
                                              cnr.match_key_id) max_match_key_rank
             from im_match_cnr_ws cnr
            where cnr.workspace_id = I_workspace_id
              and cnr.match_key_id = NVL(I_match_key_id, cnr.match_key_id)
              and cnr.choice_flag  = 'N'
              and cnr.status       = REIM_CONSTANTS.DOC_STATUS_APPRVE
          )
    where max_match_key_rank <= L_best_mtch_limit;

   for L_match_key_rank in 1 .. L_best_mtch_limit loop

      if L_match_key_rank > REIM_CONSTANTS.ONE then

         delete from gtt_6_num_6_str_6_date;
         insert into gtt_6_num_6_str_6_date(number_1,  -- old_group_id
                                            number_2,  -- new_group_id
                                            number_3,  -- workspace_id
                                            number_4)  -- match_key_id
                                     select cnr_group_id,
                                            im_match_group_cn_cnr_seq.NEXTVAL,
                                            workspace_id,
                                            match_key_id
                                       from (select distinct cnr_grp.cnr_group_id,
                                                    gtt.number_1 workspace_id,
                                                    gtt.number_2 match_key_id
                                               from gtt_10_num_10_str_10_date gtt,
                                                    im_match_group_cn_cnr_gtt cnr_grp
                                              where gtt.number_1            = I_workspace_id
                                                and gtt.number_2            = NVL(I_match_key_id, gtt.number_3)
                                                and gtt.number_4            = L_match_key_rank
                                                and cnr_grp.workspace_id    = gtt.number_1);

         insert into im_match_group_cn_cnr_gtt(workspace_id,
                                               cnr_group_id,
                                               doc_id,
                                               due_date)
         select cnr_grp.workspace_id,
                gtt.number_2 cnr_group_id, --new_group_id
                cnr_grp.doc_id,
                cnr_grp.due_date
           from gtt_6_num_6_str_6_date gtt,
                im_match_group_cn_cnr_gtt cnr_grp
          where cnr_grp.cnr_group_id = gtt.number_1   --old_group_id
            and cnr_grp.workspace_id = gtt.number_3;  --workspace_id

         insert into im_match_group_cn_cnr_gtt(workspace_id,
                                               cnr_group_id,
                                               doc_id,
                                               due_date)
         select gtt10.number_1 workspace_id,
                gtt6.number_2  cnr_group_id, --new_group_id
                gtt10.number_3 doc_id,
                gtt10.varchar2_1 due_date
           from gtt_10_num_10_str_10_date gtt10,
                gtt_6_num_6_str_6_date gtt6
          where gtt10.number_1 = I_workspace_id
            and gtt10.number_2 = NVL(I_match_key_id, gtt10.number_2)
            and gtt10.number_4 = L_match_key_rank
            and gtt6.number_3  = gtt10.number_1
            and gtt6.number_4  = gtt10.number_2;

      end if;


      insert into im_match_group_cn_cnr_gtt(workspace_id,
                                            cnr_group_id,
                                            doc_id,
                                            due_date)
      select gtt10.number_1 workspace_id,
             im_match_group_cn_cnr_seq.NEXTVAL,
             gtt10.number_3 doc_id,
             gtt10.varchar2_1 due_date
        from gtt_10_num_10_str_10_date gtt10
       where gtt10.number_1 = I_workspace_id
         and gtt10.number_2 = NVL(I_match_key_id, gtt10.number_3)
         and gtt10.number_4 = L_match_key_rank;

   end loop;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CREATE_CNR_GROUPS;
----------------------------------------------------------------
FUNCTION CREATE_MTCH_GROUP_HEAD(O_error_message     IN OUT VARCHAR2,
                                I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'REIM_ONLINE_MATCH_CN_SQL.CREATE_MTCH_GROUP_HEAD';
   L_start_time      TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start '           || L_program ||
                          ' I_workspace_id:' || I_workspace_id);

   --create im_match_group_head_ws rows for unmatched data
   insert into im_match_group_cn_gtt (workspace_id,
                                      cn_group_cost,
                                      cn_group_qty,
                                      cnr_group_cost,
                                      cnr_group_qty,
                                      cost_variance,
                                      qty_variance,
                                      cost_discrepant,
                                      qty_discrepant,
                                      match_available,
                                      match_status,
                                      error_code,
                                      error_context,
                                      match_group_id,
                                      match_cn_group_id,
                                      match_cnr_group_id)
        with cnr_group as (select distinct workspace_id,
                                           cnr_group_id
                             from im_match_group_cn_cnr_gtt cnr
                            where workspace_id = I_workspace_id),
             cn_group as (select distinct workspace_id,
                                          cn_group_id
                            from im_match_group_cn_cn_gtt cnr
                           where workspace_id = I_workspace_id)
        select I_workspace_id,
               null cn_group_cost,
               null cn_group_qty,
               null cnr_group_cost,
               null cnr_group_qty,
               null cost_variance,
               null qty_variance,
               'N' cost_discrepant,
               'N' qty_discrepant,
               'N' match_available,
               REIM_CONSTANTS.DOC_STATUS_URMTCH match_status,
               NULL error_code,
               NULL error_context,
               im_match_group_cn_seq.nextval,
               cn.cn_group_id match_cn_group_id,
               cnr.cnr_group_id match_cnr_group_id
          from cn_group cn,
               cnr_group cnr
         where cn.workspace_id  = I_workspace_id
           and cnr.workspace_id = cn.workspace_id;

   --begin ROLLUP_COST_AND_QTY logic

   merge into im_match_group_cn_gtt target
   using (select inner.workspace_id,
                 inner.cn_group_id,
                 SUM(inner.cn_group_cost) invc_group_cost,
                 SUM(inner.cn_group_qty) invc_group_qty,
                 MAX(inner.tolerance_id) tolerance_id,
                 MAX(inner.tolerance_exchange_rate) tolerance_exchange_rate,
                 MAX(inner.tolerance_currency_code) tolerance_currency_code,
                 MIN(inner.qty_match_required) qty_match_required
            from (select gtt.workspace_id,
                         gtt.cn_group_id,
                         SUM(cnd.unit_cost * DECODE(cnd.qty_matched,
                                                   'N', cnd.doc_qty,
                                                   'D', cnd.doc_qty,
                                                   REIM_CONSTANTS.ZERO)) cn_group_cost,
                         SUM(DECODE(cnd.qty_matched,
                                    'N', cnd.doc_qty,
                                    'D', cnd.doc_qty,
                                    REIM_CONSTANTS.ZERO)) cn_group_qty,
                         MAX(cn.tolerance_id) tolerance_id,
                         MAX(cn.tolerance_exchange_rate) tolerance_exchange_rate,
                         MAX(cn.tolerance_currency_code) tolerance_currency_code,
                         MIN(cn.qty_match_required) qty_match_required,
                         'N' header_only
                    from im_match_group_cn_cn_gtt gtt,
                         im_match_cn_ws cn,
                         im_match_cn_detl_ws cnd
                   where gtt.workspace_id = I_workspace_id
                     and gtt.workspace_id = cn.workspace_id
                     and gtt.doc_id       = cn.doc_id
                     and cn.header_only   = 'N'
                     and cn.workspace_id  = cnd.workspace_id
                     and cn.doc_id        = cnd.doc_id
                   GROUP BY gtt.workspace_id,
                            gtt.cn_group_id
                  union all
                  select gtt.workspace_id,
                         gtt.cn_group_id,
                         SUM(DECODE(cn.status,
                                    REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                    cn.total_avail_cost)) cn_group_cost,
                         SUM(DECODE(cn.status,
                                    REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                    cn.total_avail_qty)) cn_group_qty,
                         MAX(cn.tolerance_id) tolerance_id,
                         MAX(cn.tolerance_exchange_rate) tolerance_exchange_rate,
                         MAX(cn.tolerance_currency_code) tolerance_currency_code,
                         MIN(cn.qty_match_required) qty_match_required,
                         'Y' header_only
                    from im_match_group_cn_cn_gtt gtt,
                         im_match_cn_ws cn
                   where gtt.workspace_id = I_workspace_id
                     and cn.workspace_id  = gtt.workspace_id
                     and cn.doc_id        = gtt.doc_id
                     and cn.header_only   = 'Y'
                   GROUP BY gtt.workspace_id,
                            gtt.cn_group_id) inner
           GROUP BY inner.workspace_id,
                    inner.cn_group_id
   ) use_this
   on (    target.workspace_id       = use_this.workspace_id
       and target.match_cn_group_id  = use_this.cn_group_id)
   when MATCHED then
      update set target.cn_group_cost            = use_this.invc_group_cost,
                 target.cn_group_qty             = use_this.invc_group_qty,
                 target.tolerance_id             = use_this.tolerance_id,
                 target.tolerance_exchange_rate  = use_this.tolerance_exchange_rate,
                 target.tolerance_currency_code  = use_this.tolerance_currency_code,
                 target.qty_match_required       = use_this.qty_match_required;

   merge into im_match_group_cn_gtt target
   using (select inner.workspace_id,
                 inner.cnr_group_id,
                 SUM(inner.cnr_group_cost) cnr_group_cost,
                 SUM(inner.cnr_group_qty) cnr_group_qty
            from (select gtt.workspace_id,
                         gtt.cnr_group_id,
                         SUM(cnrd.unit_cost * DECODE(cnrd.qty_matched,
                                                     'N', cnrd.doc_qty,
                                                     'D', cnrd.doc_qty,
                                                     REIM_CONSTANTS.ZERO)) cnr_group_cost,
                         SUM(DECODE(cnrd.qty_matched,
                                    'N', cnrd.doc_qty,
                                    'D', cnrd.doc_qty,
                                    REIM_CONSTANTS.ZERO)) cnr_group_qty,
                         'N' header_only
                    from im_match_group_cn_cnr_gtt gtt,
                         im_match_cnr_ws cnr,
                         im_match_cnr_detl_ws cnrd
                   where gtt.workspace_id = I_workspace_id
                     and gtt.workspace_id = cnr.workspace_id
                     and gtt.doc_id       = cnr.doc_id
                     and cnr.header_only   = 'N' 
                     and cnr.workspace_id  = cnrd.workspace_id
                     and cnr.doc_id        = cnrd.doc_id
                   GROUP BY gtt.workspace_id,
                            gtt.cnr_group_id
                  union all
                  select gtt.workspace_id,
                         gtt.cnr_group_id,
                         SUM(DECODE(cnr.status,
                                    REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                    cnr.total_avail_cost)) cnr_group_cost,
                         SUM(DECODE(cnr.status,
                                    REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                    cnr.total_avail_qty)) cnr_group_qty,
                         'Y' header_only
                    from im_match_group_cn_cnr_gtt gtt,
                         im_match_cnr_ws cnr
                   where gtt.workspace_id = I_workspace_id
                     and cnr.workspace_id  = gtt.workspace_id
                     and cnr.doc_id        = gtt.doc_id
                     and cnr.header_only   = 'Y'
                   GROUP BY gtt.workspace_id,
                            gtt.cnr_group_id) inner
           GROUP BY inner.workspace_id,
                    inner.cnr_group_id
   ) use_this
   on (    target.workspace_id        = use_this.workspace_id
       and target.match_cnr_group_id  = use_this.cnr_group_id)
   when MATCHED then
      update set target.cnr_group_cost            = use_this.cnr_group_cost,
                 target.cnr_group_qty             = use_this.cnr_group_qty;
--is:qty_matched_req? tolerance logic?

   --begin ROLLUP_COST_AND_QTY logic

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CREATE_MTCH_GROUP_HEAD;
----------------------------------------------------------------
FUNCTION MATCH_GROUP_HEAD(O_error_message     IN OUT VARCHAR2,
                          I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS


   L_program         VARCHAR2(61) := 'REIM_ONLINE_MATCH_CN_SQL.MATCH_GROUP_HEAD';
   L_start_time      TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start '           || L_program ||
                          ' I_workspace_id:' || I_workspace_id);

   merge into im_match_group_cn_gtt tgt
   using (with grp_head as (select gh.workspace_id,
                                   gh.match_group_id,
                                   gh.cn_group_cost,
                                   gh.cnr_group_cost,
                                   gh.cn_group_qty,
                                   gh.cnr_group_qty,
                                   gh.tolerance_id,
                                   gh.qty_match_required,
                                   gh.tolerance_exchange_rate,
                                   case when gh.cn_group_cost > gh.cnr_group_cost
                                       then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                       else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                   end cost_favor_of,
                                   case when gh.cn_group_qty < gh.cnr_group_qty
                                       then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                       else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                   end qty_favor_of
                              from im_match_group_cn_gtt gh
                             where gh.workspace_id      = I_workspace_id),
               im_tol_detl_cost as (select itd.*
                                      from im_tolerance_detail itd
                                     where itd.match_level      = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
                                       and itd.match_type       = REIM_CONSTANTS.TLR_MTCH_TYPE_COST),
               im_tol_detl_qty as (select itd.*
                                      from im_tolerance_detail itd
                                     where itd.match_level      = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
                                       and itd.match_type       = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY)
          select group_head.workspace_id,
                 group_head.match_group_id,
                 (group_head.cnr_group_cost - group_head.cn_group_cost) cost_variance,
                 (group_head.cnr_group_qty - group_head.cn_group_qty) qty_variance,
                 case when itd_cost.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when itd_cost.tolerance_value < ABS(100 * (group_head.cn_group_cost - group_head.cnr_group_cost) / group_head.cnr_group_cost) then
                            'Y'
                         else
                            'N'
                    end
                 when itd_cost.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when itd_cost.tolerance_value < ABS((group_head.cn_group_cost - group_head.cnr_group_cost) * group_head.tolerance_exchange_rate) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when group_head.cn_group_cost = group_head.cnr_group_cost then
                            'N'
                         else
                            'Y'
                    end
                 end cost_discrepant,
                 --
                 case when group_head.qty_match_required = 'N' then
                    'N'
                 when itd_qty.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                    case when itd_qty.tolerance_value < ABS(100 * (group_head.cn_group_qty - group_head.cnr_group_qty) / group_head.cnr_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 when itd_qty.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                    case when itd_qty.tolerance_value < ABS(group_head.cn_group_qty - group_head.cnr_group_qty) then
                            'Y'
                         else
                            'N'
                    end
                 else  --no tolerance found, exact match
                    case when group_head.cn_group_qty = group_head.cnr_group_qty then
                            'N'
                         else
                            'Y'
                    end
                 end qty_discrepant,
                 --
                 case when
                    case when itd_cost.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                            case when itd_cost.tolerance_value < ABS(100 * (group_head.cn_group_cost - group_head.cnr_group_cost) / group_head.cnr_group_cost) then
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                            end
                         when itd_cost.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                            case when itd_cost.tolerance_value < ABS((group_head.cn_group_cost - group_head.cnr_group_cost) * group_head.tolerance_exchange_rate) then
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                            end
                         else  --no tolerance found, exact match
                            case when group_head.cn_group_cost = group_head.cnr_group_cost then
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                            end
                    end = REIM_CONSTANTS.DOC_STATUS_MTCH
                    --
                    and
                    --
                    case when group_head.qty_match_required = 'N' then
                            REIM_CONSTANTS.DOC_STATUS_MTCH
                         when itd_qty.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                            case when itd_qty.tolerance_value < ABS(100 * (group_head.cn_group_qty - group_head.cnr_group_qty) / group_head.cnr_group_qty) then
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                            end
                         when itd_qty.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                            case when itd_qty.tolerance_value < ABS(group_head.cn_group_qty - group_head.cnr_group_qty) then
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                            end
                         else  --no tolerance found, exact match
                            case when group_head.cn_group_qty = group_head.cnr_group_qty then
                                    REIM_CONSTANTS.DOC_STATUS_MTCH
                                 else
                                    REIM_CONSTANTS.DOC_STATUS_URMTCH
                            end
                    end = REIM_CONSTANTS.DOC_STATUS_MTCH then
                       REIM_CONSTANTS.DOC_STATUS_MTCH
                    else
                       REIM_CONSTANTS.DOC_STATUS_URMTCH
                 end match_status
            from grp_head group_head,
                 im_tol_detl_cost itd_cost,
                 im_tol_detl_qty itd_qty
           where group_head.cnr_group_qty  <> REIM_CONSTANTS.ZERO
             and group_head.cnr_group_cost <> REIM_CONSTANTS.ZERO
             --
             and itd_cost.tolerance_id(+)  = group_head.tolerance_id
             and itd_cost.lower_limit(+)  <= abs(group_head.cn_group_cost)
             and itd_cost.upper_limit(+)   > abs(group_head.cn_group_cost)
             and itd_cost.favor_of(+)      = group_head.cost_favor_of
             --
             and itd_qty.tolerance_id(+)   = group_head.tolerance_id
             and itd_qty.lower_limit(+)   <= group_head.cn_group_qty
             and itd_qty.upper_limit(+)    > group_head.cn_group_qty
             and itd_qty.favor_of(+)       = group_head.qty_favor_of
             --
             ) src
   on (tgt.match_group_id = src.match_group_id)
   when MATCHED then
      update
         set tgt.cost_variance         = src.cost_variance,
             tgt.qty_variance          = src.qty_variance,
             tgt.cost_discrepant       = src.cost_discrepant,
             tgt.qty_discrepant        = src.qty_discrepant,
             tgt.match_available       = DECODE(src.match_status,
                                                REIM_CONSTANTS.DOC_STATUS_MTCH, 'Y',
                                                'N'),
             tgt.match_status          = src.match_status;

   LOGGER.LOG_INFORMATION(L_program||' Merge match im_match_group_cn_gtt workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END MATCH_GROUP_HEAD;
----------------------------------------------------------------
FUNCTION UNMATCH_TAX_DISCREPANT(O_error_message     IN OUT VARCHAR2,
                                I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program    VARCHAR2(61) := 'REIM_ONLINE_MATCH_CN_SQL.UNMATCH_TAX_DISCREPANT';
   L_start_time TIMESTAMP := SYSTIMESTAMP;
   
   L_cn_has_detl VARCHAR2(1) := 'N';

BEGIN

   LOGGER.LOG_INFORMATION('Start '           || L_program ||
                          ' I_workspace_id:' || I_workspace_id);

   merge into im_match_group_cn_gtt tgt
   using (select distinct inner.match_group_id
            from (with cn_hdr_tax as (select distinct match_gtt.workspace_id,
                                                      match_gtt.match_group_id,
                                                      cn_tax.tax_code,
                                                      cn_tax.tax_rate
                                        from im_match_group_cn_gtt match_gtt,
                                             im_match_group_cn_cn_gtt cn_gtt,
                                             im_match_cn_tax_ws cn_tax
                                       where match_gtt.workspace_id    = I_workspace_id
                                         and match_gtt.match_status    = REIM_CONSTANTS.DOC_STATUS_MTCH
                                         and match_gtt.match_available = 'Y'
                                         and match_gtt.workspace_id    = I_workspace_id
                                         and cn_gtt.cn_group_id        = match_gtt.match_cn_group_id
                                         and cn_tax.workspace_id       = I_workspace_id
                                         and cn_tax.doc_id             = cn_gtt.doc_id),
                      cnr_hdr_tax as (select distinct match_gtt.workspace_id,
                                                      match_gtt.match_group_id,
                                                      cnr_tax.tax_code,
                                                      cnr_tax.tax_rate
                                        from im_match_group_cn_gtt match_gtt,
                                             im_match_group_cn_cnr_gtt cnr_gtt,
                                             im_match_cnr_tax_ws cnr_tax
                                       where match_gtt.workspace_id    = I_workspace_id
                                         and match_gtt.match_status    = REIM_CONSTANTS.DOC_STATUS_MTCH
                                         and match_gtt.match_available = 'Y'
                                         and match_gtt.workspace_id    = I_workspace_id
                                         and cnr_gtt.cnr_group_id      = match_gtt.match_cnr_group_id
                                         and cnr_tax.workspace_id      = I_workspace_id
                                         and cnr_tax.doc_id            = cnr_gtt.doc_id)
                  (select cn.workspace_id,
                          cn.match_group_id,
                          cn.tax_code,
                          cn.tax_rate
                     from cn_hdr_tax cn
                   minus
                   select cnr.workspace_id,
                          cnr.match_group_id,
                          cnr.tax_code,
                          cnr.tax_rate
                     from cnr_hdr_tax cnr)
                  union all
                  (select cnr.workspace_id,
                          cnr.match_group_id,
                          cnr.tax_code,
                          cnr.tax_rate
                     from cnr_hdr_tax cnr
                   minus
                   select cn.workspace_id,
                          cn.match_group_id,
                          cn.tax_code,
                          cn.tax_rate
                     from cn_hdr_tax cn))inner) src
   on (    tgt.match_group_id = src.match_group_id)
   when MATCHED then
      update set tgt.match_available = 'N',
                 tgt.match_status    = REIM_CONSTANTS.DOC_STATUS_TAXDIS;

   LOGGER.LOG_INFORMATION(L_program||' Merge Tax discrepant (Header lvl) as URMTCH on im_match_group_cn_gtt workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select DECODE(max(cn_ws.header_only),
                 'Y', 'N',
                 'Y')
     into L_cn_has_detl
     from im_match_group_cn_gtt match_gtt,
          im_match_group_cn_cn_gtt cn_gtt,
          im_match_cn_ws cn_ws
    where match_gtt.workspace_id    = I_workspace_id
      and match_gtt.match_status    = REIM_CONSTANTS.DOC_STATUS_MTCH
      and match_gtt.match_available = 'Y'
      and match_gtt.workspace_id    = I_workspace_id
      and cn_gtt.cn_group_id        = match_gtt.match_cn_group_id
      and cn_ws.workspace_id        = I_workspace_id
      and cn_ws.doc_id              = cn_gtt.doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Do all selected CNs have details?: L_cn_has_detl: ' || L_cn_has_detl);

   if L_cn_has_detl = 'Y' then

      merge into im_match_group_cn_gtt tgt
      using (select distinct inner.match_group_id
               from (with cn_detl_tax as (select distinct match_gtt.workspace_id,
                                                          match_gtt.match_group_id,
                                                          cn_detl_tax.tax_code,
                                                          cn_detl_tax.tax_rate
                                            from im_match_group_cn_gtt match_gtt,
                                                 im_match_group_cn_cn_gtt cn_gtt,
                                                 im_match_cn_detl_ws cn_detl_tax
                                           where match_gtt.workspace_id    = I_workspace_id
                                             and match_gtt.match_status    = REIM_CONSTANTS.DOC_STATUS_MTCH
                                             and match_gtt.match_available = 'Y'
                                             and match_gtt.workspace_id    = I_workspace_id
                                             and cn_gtt.cn_group_id        = match_gtt.match_cn_group_id
                                             and cn_detl_tax.workspace_id  = I_workspace_id
                                             and cn_detl_tax.doc_id        = cn_gtt.doc_id),
                         cnr_detl_tax as (select distinct match_gtt.workspace_id,
                                                          match_gtt.match_group_id,
                                                          cnr_detl_tax.tax_code,
                                                          cnr_detl_tax.tax_rate
                                            from im_match_group_cn_gtt match_gtt,
                                                 im_match_group_cn_cnr_gtt cnr_gtt,
                                                 im_match_cnr_detl_ws cnr_detl_tax
                                           where match_gtt.workspace_id    = I_workspace_id
                                             and match_gtt.match_status    = REIM_CONSTANTS.DOC_STATUS_MTCH
                                             and match_gtt.match_available = 'Y'
                                             and match_gtt.workspace_id    = I_workspace_id
                                             and cnr_gtt.cnr_group_id      = match_gtt.match_cnr_group_id
                                             and cnr_detl_tax.workspace_id = I_workspace_id
                                             and cnr_detl_tax.doc_id       = cnr_gtt.doc_id)
                     (select cn.workspace_id,
                             cn.match_group_id,
                             cn.tax_code,
                             cn.tax_rate
                        from cn_detl_tax cn
                      minus
                      select cnr.workspace_id,
                             cnr.match_group_id,
                             cnr.tax_code,
                             cnr.tax_rate
                        from cnr_detl_tax cnr)
                     union all
                     (select cnr.workspace_id,
                             cnr.match_group_id,
                             cnr.tax_code,
                             cnr.tax_rate
                        from cnr_detl_tax cnr
                      minus
                      select cn.workspace_id,
                             cn.match_group_id,
                             cn.tax_code,
                             cn.tax_rate
                        from cn_detl_tax cn))inner) src
      on (    tgt.match_group_id = src.match_group_id)
      when MATCHED then
         update set tgt.match_available = 'N',
                    tgt.match_status    = REIM_CONSTANTS.DOC_STATUS_TAXDIS;

   end if;

   LOGGER.LOG_INFORMATION(L_program||' Merge Tax discrepant (Detail lvl) as URMTCH on im_match_group_cn_gtt workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UNMATCH_TAX_DISCREPANT;
----------------------------------------------------------------
-- The public function used for Validating Tax compliance
-- Input param: I_workspace_id (The ID of the Match workspace)
----------------------------------------------------------------
FUNCTION VALIDATE_TAX(O_error_message    OUT VARCHAR2,
                      O_tax_compliant    OUT VARCHAR2,
                      O_tax_disc_lvl     OUT VARCHAR2,
                      I_workspace_id  IN     IM_MATCH_CNR_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(61) := 'REIM_ONLINE_MATCH_CN_SQL.VALIDATE_TAX';
   
   L_start_time   TIMESTAMP  := SYSTIMESTAMP;
   L_disc_tax_cnt NUMBER(10) := REIM_CONSTANTS.ZERO;
   
   L_cn_count  NUMBER(10) := REIM_CONSTANTS.ZERO;
   L_cnr_count NUMBER(10) := REIM_CONSTANTS.ZERO;
   
BEGIN

   LOGGER.LOG_INFORMATION('Start '           || L_program ||
                          ' I_workspace_id:' || I_workspace_id);

   select count(1)
     into L_cn_count
     from im_match_cn_ws cn
    where cn.workspace_id = I_workspace_id
      and cn.choice_flag  = 'Y';

   select count(1)
     into L_cnr_count
     from im_match_cnr_ws cnr
    where cnr.workspace_id = I_workspace_id
      and cnr.choice_flag  = 'Y';

   --check if atleast one cn and one cnr are selected
   if L_cn_count  = REIM_CONSTANTS.ZERO or
      L_cnr_count = REIM_CONSTANTS.ZERO then

      O_tax_compliant := 'Y';
      O_tax_disc_lvl  := NULL;
      REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
      return REIM_CONSTANTS.SUCCESS;

   end if;

   --header tax compliance
   select count(*)
     into L_disc_tax_cnt
     from (select distinct cn.workspace_id,
                  cn_tax.tax_code,
                  cn_tax.tax_rate
             from im_match_cn_ws cn,
                  im_match_cn_tax_ws cn_tax
            where cn.workspace_id     = I_workspace_id
              and cn.choice_flag      = 'Y'
              and cn_tax.workspace_id = cn.workspace_id
              and cn_tax.doc_id       = cn.doc_id
            minus
           select distinct cnr.workspace_id,
                  cnr_tax.tax_code,
                  cnr_tax.tax_rate
             from im_match_cnr_ws cnr,
                  im_match_cnr_tax_ws cnr_tax
            where cnr.workspace_id     = I_workspace_id
              and cnr.choice_flag      = 'Y'
              and cnr_tax.workspace_id = cnr.workspace_id
              and cnr_tax.doc_id       = cnr.doc_id);

   if L_disc_tax_cnt > REIM_CONSTANTS.ZERO  then
      O_tax_compliant := 'N';
      O_tax_disc_lvl  := REIM_CONSTANTS.CN_MTCH_TAX_DISC_LVL_SUMM;
      REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
      return REIM_CONSTANTS.SUCCESS;
   end if;

   select count(*)
     into L_disc_tax_cnt
     from (select distinct cnr.workspace_id,
                  cnr_tax.tax_code,
                  cnr_tax.tax_rate
             from im_match_cnr_ws cnr,
                  im_match_cnr_tax_ws cnr_tax
            where cnr.workspace_id     = I_workspace_id
              and cnr.choice_flag      = 'Y'
              and cnr_tax.workspace_id = cnr.workspace_id
              and cnr_tax.doc_id       = cnr.doc_id
            minus
           select distinct cn.workspace_id,
                  cn_tax.tax_code,
                  cn_tax.tax_rate
             from im_match_cn_ws cn,
                  im_match_cn_tax_ws cn_tax
            where cn.workspace_id     = I_workspace_id
              and cn.choice_flag      = 'Y'
              and cn_tax.workspace_id = cn.workspace_id
              and cn_tax.doc_id       = cn.doc_id);

   if L_disc_tax_cnt > REIM_CONSTANTS.ZERO  then
      O_tax_compliant := 'N';
      O_tax_disc_lvl  := REIM_CONSTANTS.CN_MTCH_TAX_DISC_LVL_SUMM;
      REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
      return REIM_CONSTANTS.SUCCESS;
   end if;

   --detail tax compliance
   select sum(inner.tax_cnt)
     into L_disc_tax_cnt
     from (select decode(inner_cn.tax_code, inner_cnr.tax_code, 0, 1) +
                  decode(inner_cn.tax_rate, inner_cnr.tax_rate, 0, 1) tax_cnt
             from (select cnd.item,     
                          cnd.tax_code,              
                          cnd.tax_rate               
                     from im_match_cn_ws cn,    
                          im_match_cn_detl_ws cnd    
                    where cn.workspace_id = I_workspace_id
                      and cn.choice_flag  = 'Y'  
                      and cn.workspace_id = cnd.workspace_id
                      and cn.doc_id       = cnd.doc_id
                      and cnd.status      <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
                  ) inner_cn,                
                  (select cnrd.item,         
                          cnrd.tax_code,             
                          cnrd.tax_rate              
                     from im_match_cnr_ws cnr,  
                          im_match_cnr_detl_ws cnrd  
                    where cnr.workspace_id = I_workspace_id
                      and cnr.choice_flag  = 'Y'
                      and cnr.workspace_id = cnrd.workspace_id
                      and cnr.doc_id       = cnrd.doc_id
                      and cnrd.status      <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
                  ) inner_cnr
                  where inner_cn.item = inner_cnr.item) inner;

   if L_disc_tax_cnt > 0  then
      O_tax_compliant := 'N';
      O_tax_disc_lvl  := REIM_CONSTANTS.CN_MTCH_TAX_DISC_LVL_DETL;
      REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
      return REIM_CONSTANTS.SUCCESS;
   end if;

   O_tax_compliant := 'Y';
   O_tax_disc_lvl  := NULL;
   
   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;

END VALIDATE_TAX;
----------------------------------------------------------------
/**
 * The public function used to perform Online Resolution
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *              I_cost_decision (Decision indicator, 'R' for CNR, 'N' for CN and 'M' for Match Within Tolerance, NULL if no resolution performed)
 *              I_qty_decision  (Decision indicator, 'R' for CNR, 'N' for CN and 'M' for Match Within Tolerance, NULL if no resolution performed)
 *              I_resln_action_rc_tbl  (List of Reason codes (both cost and qty together) used for resolving the discrepancy)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_ONLINE_RESLN(O_error_message          OUT VARCHAR2,
                              I_workspace_id        IN OUT IM_DETAIL_MATCH_CN_WS.WORKSPACE_ID%TYPE,
                              I_cost_decision       IN     IM_DOC_DETAIL_REASON_CODES.COST_MATCHED%TYPE,
                              I_qty_decision        IN     IM_DOC_DETAIL_REASON_CODES.QTY_MATCHED%TYPE,
                              I_resln_action_rc_tbl IN     IM_RESLN_ACTION_RC_TBL)
RETURN NUMBER
IS

   L_program    VARCHAR2(50) := 'REIM_ONLINE_MATCH_CN_SQL.PERFORM_ONLINE_RESLN';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;

   L_user    IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
   L_vdate   PERIOD.VDATE%TYPE         := GET_VDATE;

   L_adj_cost          IM_DOC_DETAIL_REASON_CODES.ADJUSTED_UNIT_COST%TYPE := NULL;
   L_dwo_unit_cost_adj IM_DOC_DETAIL_REASON_CODES.ADJUSTED_UNIT_COST%TYPE := NULL;
   L_cost_rc_count     NUMBER(10)                                         := NULL;
   L_cost_rc_action    IM_REASON_CODES.ACTION%TYPE                        := NULL;
   L_cost_decision     VARCHAR2(1)                                        := NULL;
   --
   L_adj_qty       IM_DOC_DETAIL_REASON_CODES.ADJUSTED_QTY%TYPE := NULL;
   L_dwo_qty_adj   IM_DOC_DETAIL_REASON_CODES.ADJUSTED_QTY%TYPE := NULL;
   L_qty_rc_count  NUMBER(10)                                   := NULL;
   L_qty_rc_action IM_REASON_CODES.ACTION%TYPE                  := NULL;
   L_qty_decision  VARCHAR2(1)                                  := NULL;

   cursor C_FETCH_ADJ_COST is
      select SUM(rc.adj_value) adj_val,
             SUM(DECODE(irc.action,
                        REIM_CONSTANTS.RC_ACTION_DWO, NVL(rc.adj_value, REIM_CONSTANTS.ZERO),
                        REIM_CONSTANTS.ZERO)) dwo_unit_cost_adj,
             count(1) adj_count,
             max(irc.action) rc_action
        from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc,
             im_reason_codes irc
       where rc.adj_type        = REIM_CONSTANTS.REASON_CODE_TYPE_COST
         and irc.reason_code_id = rc.reason_code_id;

   cursor C_FETCH_ADJ_QTY is
      select SUM(rc.adj_value) adj_val,
             SUM(DECODE(irc.action,
                        REIM_CONSTANTS.RC_ACTION_DWO, NVL(rc.adj_value, REIM_CONSTANTS.ZERO),
                        REIM_CONSTANTS.ZERO)) dwo_qty_adj,
             count(1) adj_count,
             max(irc.action) rc_action
        from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc,
             im_reason_codes irc
       where rc.adj_type        = REIM_CONSTANTS.REASON_CODE_TYPE_QTY
         and irc.reason_code_id = rc.reason_code_id;

   cursor C_FETCH_RC_TBL_4_DEBUG is
      select rc.reason_code_id || '~' || rc.rc_comment || '~' || rc.adj_type || '~' || rc.adj_value || '~' as text
        from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: '  || I_workspace_id
                          || ' I_cost_decision: ' || I_cost_decision
                          || ' I_qty_decision: '  || I_qty_decision);

   LOGGER.LOG_INFORMATION('Begin i/p tbl logging');
   for rec in C_FETCH_RC_TBL_4_DEBUG loop
      LOGGER.LOG_INFORMATION('rec.text: ' || rec.text);
   end loop;
   LOGGER.LOG_INFORMATION('End i/p tbl logging');

   if L_user is null then
      L_user := get_user;
   end if;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   open C_FETCH_ADJ_COST;
   fetch C_FETCH_ADJ_COST into L_adj_cost,
                               L_dwo_unit_cost_adj,
                               L_cost_rc_count,
                               L_cost_rc_action;
   close C_FETCH_ADJ_COST;

   open C_FETCH_ADJ_QTY;
   fetch C_FETCH_ADJ_QTY into L_adj_qty,
                              L_dwo_qty_adj,
                              L_qty_rc_count,
                              L_qty_rc_action;
   close C_FETCH_ADJ_QTY;

   --Reverse the decisions if DWO was used for Mass Resolution and the decision was Credit Note ('N')
   if I_cost_decision = REIM_CONSTANTS.RESLN_DECISION_CN and L_cost_rc_count = REIM_CONSTANTS.ONE and L_cost_rc_action = REIM_CONSTANTS.RC_ACTION_DWO and L_adj_cost is NULL then
      L_cost_decision := REIM_CONSTANTS.RESLN_DECISION_CNR;
   elsif I_cost_decision is NULL or I_cost_decision IN (REIM_CONSTANTS.RESLN_DECISION_CN, REIM_CONSTANTS.RESLN_DECISION_CNR, REIM_CONSTANTS.RESLN_DECISION_MTCH_WT) then
      L_cost_decision := I_cost_decision;
   else
      O_error_message := 'Invalid Cost Decision: ' || I_cost_decision;
      return REIM_CONSTANTS.FAIL;
   end if;

   if I_qty_decision = REIM_CONSTANTS.RESLN_DECISION_CN and L_qty_rc_count = REIM_CONSTANTS.ONE and L_qty_rc_action = REIM_CONSTANTS.RC_ACTION_DWO and L_adj_qty is NULL then
      L_qty_decision := REIM_CONSTANTS.RESLN_DECISION_CNR;
   elsif I_qty_decision is NULL or I_qty_decision IN (REIM_CONSTANTS.RESLN_DECISION_CN, REIM_CONSTANTS.RESLN_DECISION_CNR, REIM_CONSTANTS.RESLN_DECISION_MTCH_WT) then
      L_qty_decision := I_qty_decision;
   else
      O_error_message := 'Invalid Qty Decision: ' || I_qty_decision;
      return REIM_CONSTANTS.FAIL;
   end if;

   --segregate items to be resolved and items which are perfect match
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1,   -- workspace_id
                                      number_2,   -- cn_id
                                      number_3,   -- qty_variance
                                      number_4,   -- unit_cost_variance
                                      number_5,   -- cn_qty
                                      number_6,   -- cnr_qty
                                      varchar2_1, -- item
                                      varchar2_2, -- perfect_match_ind
                                      varchar2_3, -- cost_matched
                                      varchar2_4) -- qty_matched

   select idmw.workspace_id,
          idmwcn.cn_id,
          NVL(L_adj_qty, idmw.qty_variance),
          NVL(L_adj_cost, idmw.unit_cost_variance),
          idmwcn.cn_qty,
          idmw.cnr_qty,
          idmw.item,
          case when idmw.qty_variance = 0 and idmw.unit_cost_variance = 0 then 'Y' else 'N' end,
          DECODE(idmwcn.cost_matched,
                 'R', 'Y',
                 'D', 'N',
                 idmwcn.cost_matched),
          DECODE(idmwcn.qty_matched,
                 'R', 'Y',
                 'D', 'N',
                 idmwcn.qty_matched)
     from im_detail_match_cn_ws idmw,
          im_detail_match_cn_ws idmwcn
    where idmw.workspace_id  = I_workspace_id
      and idmw.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
      and idmw.choice_flag   = 'Y'
      and idmw.ui_filter_ind = 'N'
      --
      and idmwcn.workspace_id  = I_workspace_id
      and idmwcn.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
      and idmwcn.choice_flag   = 'Y'
      and idmwcn.ui_filter_ind = 'N'
      --
      and idmwcn.workspace_id  = idmw.workspace_id
      and idmwcn.item          = idmw.item;

   LOGGER.LOG_INFORMATION(L_program||' Segregate items to be resolved - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Write resolution and comments records
   --------------------------------------

   insert into im_resolution_action(doc_id,
                                    item,
                                    reason_code_id,
                                    action,
                                    qty,
                                    unit_cost,
                                    extended_cost,
                                    status,
                                    shipment,
                                    created_by,
                                    creation_date,
                                    last_updated_by,
                                    last_update_date,
                                    object_version_id)
   select gtt.number_2, --cn_id
          gtt.varchar2_1, --item,
          rc_tbl.reason_code_id,
          irc.action,
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_QTY, NVL(rc_tbl.adj_value, gtt.number_3),
                 NULL) qty,
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_COST, NVL(rc_tbl.adj_value, gtt.number_4),
                 NULL) unit_cost,
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_COST, NVL(rc_tbl.adj_value, gtt.number_4) * gtt.number_5,
                 NULL) extended_cost,
          REIM_CONSTANTS.RCA_STATUS_UNROLLED, -- status
          NULL, --Shipment
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc_tbl,
          im_reason_codes irc,
          gtt_6_num_6_str_6_date gtt
    where irc.reason_code_id = rc_tbl.reason_code_id
      and gtt.varchar2_2     = 'N'; -- Perfect match Ind

   LOGGER.LOG_INFORMATION(L_program||' Insert resolution_actions - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_doc_detail_comments (comment_id,
                                       comment_type,
                                       text,
                                       created_by,
                                       creation_date,
                                       doc_id,
                                       item,
                                       discrepancy_type,
                                       reason_code_id,
                                       debit_reason_code,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
   select im_doc_detail_comments_seq.NEXTVAL,
          REIM_CONSTANTS.CMNT_TYPE_EXTERNAL,
          rc_tbl.rc_comment,
          L_user,
          L_vdate,
          gtt.number_2, --cn_id
          gtt.varchar2_1, --item
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_COST, REIM_CONSTANTS.CMNT_DISC_TYPE_COST,
                 REIM_CONSTANTS.CMNT_DISC_TYPE_QTY) discrepancy_type,
          rc_tbl.reason_code_id,
          NULL debit_reason_code,
          L_user,
          L_vdate,
          REIM_CONSTANTS.ONE
     from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc_tbl,
          im_reason_codes irc,
          gtt_6_num_6_str_6_date gtt
    where irc.reason_code_id  = rc_tbl.reason_code_id
      and gtt.varchar2_2      = 'N' -- Perfect match Ind
      and rc_tbl.rc_comment   is NOT NULL;

   LOGGER.LOG_INFORMATION(L_program||' Insert doc_detail_comments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Update Credit Note records
   --------------------------------------

   merge into im_doc_detail_reason_codes tgt
   using (select gtt.number_1 workspace_id,
                 gtt.number_2 doc_id,
                 gtt.varchar2_1 item,
                 DECODE(L_cost_decision,
                        REIM_CONSTANTS.RESLN_DECISION_CNR, gtt.number_4,
                        REIM_CONSTANTS.RESLN_DECISION_CN,  NVL(L_dwo_unit_cost_adj, REIM_CONSTANTS.ZERO),
                        REIM_CONSTANTS.ZERO) adj_val,
                 DECODE(L_cost_decision,
                        REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                        DECODE(gtt.number_4, -- unit_cost_variance
                               REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD)) cost_matched
            from gtt_6_num_6_str_6_date gtt,
                 im_doc_detail_reason_codes iddrc
           where iddrc.doc_id        = gtt.number_2
             and iddrc.item          = gtt.varchar2_1
             and iddrc.cost_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                         REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
             --
             and gtt.varchar2_3      = 'N' -- cost_matched
         ) src
   on (    tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.adjusted_unit_cost = tgt.adjusted_unit_cost + src.adj_val,
             tgt.cost_matched       = src.cost_matched,
             tgt.last_updated_by    = L_user,
             tgt.last_update_date   = sysdate,
             tgt.object_version_id  = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Cost resolution on im_doc_detail_reason_codes for CN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iddrc.doc_id,
                 SUM(DECODE(L_cost_decision,
                            REIM_CONSTANTS.RESLN_DECISION_CNR,     gtt.number_4 * iddrc.adjusted_qty,
                            REIM_CONSTANTS.RESLN_DECISION_CN,      NVL(L_dwo_unit_cost_adj, REIM_CONSTANTS.ZERO) * iddrc.adjusted_qty,
                            REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, gtt.number_4 * iddrc.adjusted_qty,
                            REIM_CONSTANTS.ZERO)) adj_val
            from gtt_6_num_6_str_6_date gtt,
                 im_doc_detail_reason_codes iddrc
           where iddrc.doc_id        = gtt.number_2
             and iddrc.item          = gtt.varchar2_1
             --
             and gtt.varchar2_3     = 'N' --cost_matched
           GROUP BY iddrc.doc_id) src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update
         set tgt.resolution_adjusted_total_cost = tgt.resolution_adjusted_total_cost + DECODE(L_cost_decision,
                                                                                              REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.ZERO,
                                                                                              src.adj_val),
             tgt.variance_within_tolerance      = tgt.variance_within_tolerance + DECODE(L_cost_decision,
                                                                                         REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, src.adj_val,
                                                                                         REIM_CONSTANTS.ZERO),
             tgt.last_updated_by                = L_user,
             tgt.last_update_date               = sysdate,
             tgt.object_version_id              = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Cost resolution on im_doc_head for CN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_detail_reason_codes tgt
   using (select gtt.number_1 workspace_id,
                 gtt.number_2 doc_id,
                 gtt.varchar2_1 item,
                 DECODE(L_qty_decision,
                        REIM_CONSTANTS.RESLN_DECISION_CNR, gtt.number_3,
                        REIM_CONSTANTS.RESLN_DECISION_CN, NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO),
                        REIM_CONSTANTS.ZERO) adj_val,
                 DECODE(L_qty_decision,
                        REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                        DECODE(gtt.number_3, -- qty_variance
                               REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD)) qty_matched
            from gtt_6_num_6_str_6_date gtt,
                 im_doc_detail_reason_codes iddrc
           where iddrc.doc_id        = gtt.number_2
             and iddrc.item          = gtt.varchar2_1
             and iddrc.qty_matched   IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                         REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
             --
             and gtt.varchar2_4      = 'N' --qty_matched
         ) src
   on (    tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.adjusted_qty      = tgt.adjusted_qty + src.adj_val,
             tgt.qty_matched       = src.qty_matched,
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Qty resolution on im_doc_detail_reason_codes for CN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iddrc.doc_id,
                 SUM(DECODE(L_qty_decision,
                            REIM_CONSTANTS.RESLN_DECISION_CNR, gtt.number_3,
                            REIM_CONSTANTS.RESLN_DECISION_CN,  NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO),
                            REIM_CONSTANTS.ZERO)) adj_qty,
                 SUM(DECODE(L_qty_decision,
                            REIM_CONSTANTS.RESLN_DECISION_CNR,     gtt.number_3 * iddrc.adjusted_unit_cost,
                            REIM_CONSTANTS.RESLN_DECISION_CN,      NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO) * iddrc.adjusted_unit_cost,
                            REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, gtt.number_3 * iddrc.adjusted_unit_cost,
                            REIM_CONSTANTS.ZERO)) adj_total_cost
            from gtt_6_num_6_str_6_date gtt,
                 im_doc_detail_reason_codes iddrc
           where iddrc.doc_id   = gtt.number_2
             and iddrc.item     = gtt.varchar2_1
             --
             and gtt.varchar2_4 = 'N'
           GROUP BY iddrc.doc_id) src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update
         set tgt.resolution_adjusted_total_qty = tgt.resolution_adjusted_total_qty + DECODE(L_qty_decision,
                                                                                            REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.ZERO,
                                                                                            src.adj_qty),
             tgt.resolution_adjusted_total_cost = tgt.resolution_adjusted_total_cost + DECODE(L_qty_decision,
                                                                                              REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.ZERO,
                                                                                              src.adj_total_cost),
             tgt.variance_within_tolerance      = tgt.variance_within_tolerance + DECODE(L_qty_decision,
                                                                                         REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, src.adj_total_cost,
                                                                                         REIM_CONSTANTS.ZERO),
             tgt.last_updated_by               = L_user,
             tgt.last_update_date              = sysdate,
             tgt.object_version_id             = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Qty resolution on im_doc_head for CN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iddrc.doc_id,
                 SUM(DECODE(iddrc.cost_matched,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE))
                 +
                 SUM(DECODE(iddrc.qty_matched,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) unmatch_count
            from gtt_6_num_6_str_6_date gtt,
                 im_doc_detail_reason_codes iddrc
           where iddrc.doc_id = gtt.number_2
             and iddrc.status = REIM_CONSTANTS.DOC_ITEM_STATUS_APPRVE
           GROUP BY iddrc.doc_id) src
   on (    tgt.doc_id        = src.doc_id
       and src.unmatch_count = REIM_CONSTANTS.ZERO)
   when MATCHED then
      update
         set tgt.status            = REIM_CONSTANTS.DOC_STATUS_MTCH,
             tgt.match_id          = L_user,
             tgt.match_date        = L_vdate,
             tgt.match_type        = REIM_CONSTANTS.MATCH_TYPE_MANUAL,
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Status on im_doc_head for CN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Update Credit Note Request records
   --------------------------------------

   delete from gtt_num_num_str_str_date_date;

   --number_1   cnr_id (doc_id)
   --varchar2_1 item
   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1)
   select idmwcnr.cnr_id doc_id,
          idmwcnr.item
     from gtt_6_num_6_str_6_date gtt,
          im_detail_match_cn_ws idmwcnr
    where idmwcnr.workspace_id  = gtt.number_1
      and idmwcnr.item          = gtt.varchar2_1
      and idmwcnr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
      and idmwcnr.choice_flag   = 'Y'
      and idmwcnr.ui_filter_ind = 'N'
      and idmwcnr.cnr_qty       <> REIM_CONSTANTS.ZERO;
      
   merge into im_doc_detail_reason_codes tgt
   using (select gtt.number_1 doc_id,
                 gtt.varchar2_1 item
            from gtt_num_num_str_str_date_date gtt) src
   on (    tgt.doc_id = src.doc_id
       and tgt.item   = src.item)
   when MATCHED then
      update
         set tgt.qty_matched       = REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
             tgt.cost_matched      = REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Status on im_doc_detail_reason_codes for CNRs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iddrc.doc_id,
                 SUM(DECODE(iddrc.cost_matched,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE))
                 +
                 SUM(DECODE(iddrc.qty_matched,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) unmatch_count
            from gtt_num_num_str_str_date_date gtt,
                 im_doc_detail_reason_codes iddrc
           where iddrc.doc_id = gtt.number_1
             and iddrc.status = REIM_CONSTANTS.DOC_ITEM_STATUS_APPRVE
           GROUP BY iddrc.doc_id) src
   on (    tgt.doc_id        = src.doc_id
       and src.unmatch_count = REIM_CONSTANTS.ZERO)
   when MATCHED then
      update
         set tgt.status            = DECODE(src.unmatch_count,
                                            REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                            tgt.status),
             tgt.match_id          = DECODE(src.unmatch_count,
                                            REIM_CONSTANTS.ZERO, L_user,
                                            NULL),
             tgt.match_date        = DECODE(src.unmatch_count,
                                            REIM_CONSTANTS.ZERO, L_vdate,
                                            NULL),
             tgt.match_type        = DECODE(src.unmatch_count,
                                            REIM_CONSTANTS.ZERO, REIM_CONSTANTS.MATCH_TYPE_MANUAL,
                                            tgt.match_type),
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Status on im_doc_head for CNRs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write match history
   --------------------------------------

   delete from gtt_num_num_str_str_date_date;

   --number_1   im_detail_match_cn_hist_seq
   --varchar2_1 item
   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1)
   select im_detail_match_cn_hist_seq.nextval,
          d.item
     from im_detail_match_cn_ws d
    where d.workspace_id  = I_workspace_id
      and d.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
      and d.choice_flag   = 'Y'
      and d.ui_filter_ind = 'N';

   insert into im_cn_detail_match_his(match_id,
                                      doc_id,
                                      item,
                                      reason_code_id,
                                      created_by,
                                      creation_date,
                                      last_updated_by,
                                      last_update_date,
                                      object_version_id)
      select gtt.number_1,
             d.cn_id,
             d.item,
             null,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt,
             im_detail_match_cn_ws d
       where gtt.varchar2_1 = d.item
         and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
         and d.choice_flag  = 'Y'
         and d.workspace_id = I_workspace_id
      union all
      select gtt.number_1,
             d.cnr_id,
             d.item,
             drc.reason_code_id,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt,
             im_detail_match_cn_ws d,
             im_doc_detail_reason_codes drc
       where gtt.varchar2_1 = d.item
         and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
         and d.choice_flag  = 'Y'
         and d.workspace_id = I_workspace_id
         and d.cnr_id       = drc.doc_id
         and d.item         = drc.item;

   LOGGER.LOG_INFORMATION(L_program||' insert im_cn_detail_match_his - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --refresh the workspace
   --------------------------------------

   if REFRESH_MTCH_WSPACE(O_error_message,
                          I_workspace_id,
                          'N') = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --Recreate the Detail Matching Workspace
   --------------------------------------

   if CREATE_MATCH_CN_DETAIL_WS(O_error_message,
                                I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END PERFORM_ONLINE_RESLN;
----------------------------------------------------------------------
/**
 * The public function used to perform Online Tax Resolution
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *              I_tax_decision (Decision indicator, 'R' for CNR, 'N' for CN)
 *              I_tax_resln_rc (Reason code used for the Tax Resolution)
 *              I_tax_resln_cmnt (Resolution Comment if any)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_ONLINE_TAX_RESLN(O_error_message     OUT VARCHAR2,
                                  I_workspace_id   IN OUT IM_DETAIL_MATCH_CN_WS.WORKSPACE_ID%TYPE,
                                  I_tax_decision   IN     VARCHAR2,
                                  I_tax_resln_rc   IN     IM_REASON_CODES.REASON_CODE_ID%TYPE,
                                  I_tax_resln_cmnt IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER
IS

   L_program    VARCHAR2(50) := 'REIM_ONLINE_MATCH_CN_SQL.PERFORM_ONLINE_TAX_RESLN';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;

   L_user    IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
   L_vdate   PERIOD.VDATE%TYPE         := GET_VDATE;
   
   L_tax_document_creation_lvl IM_SYSTEM_OPTIONS.TAX_DOCUMENT_CREATION_LVL%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: ' || I_workspace_id
                          || ' I_tax_decision: ' || I_tax_decision
                          || ' I_tax_resln_rc: ' || I_tax_resln_rc);

   if L_user is null then
      L_user := get_user;
   end if;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if I_tax_decision = 'N' then

      -- Update Tax discrepancy Resolved Indicator to 'Y' and Tax discrepancy Indiator to 'N'.
      update im_detail_match_cn_ws
         set tax_discrepant = 'N',
             tax_disc_rslvd = 'Y'
       where workspace_id   = I_workspace_id
         and entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
         and choice_flag    = 'Y'
         and ui_filter_ind  = 'N'
         and tax_discrepant = 'Y';

      LOGGER.LOG_INFORMATION(L_program||' Update Tax disc Resolved - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   else

      select tax_document_creation_lvl 
        into L_tax_document_creation_lvl 
        from im_system_options;

      LOGGER.LOG_INFORMATION(L_program||' Fetch Tax Document Creation Level - L_tax_document_creation_lvl: ' || L_tax_document_creation_lvl);
   
      if L_tax_document_creation_lvl = REIM_CONSTANTS.TAX_DOC_CRE_LVL_ITEM then

         --Collect Selected CN items
         delete from gtt_6_num_6_str_6_date;
         insert into gtt_6_num_6_str_6_date(number_1,   -- workspace_id
                                            number_2,   -- cn_id
                                            number_3,   -- cn_qty
                                            number_4,   -- cn_unit_cost
                                            varchar2_1) -- item
         select idmw.workspace_id,
                idmwcn.cn_id,
                idmwcn.cn_qty,
                idmwcn.cn_unit_cost,
                idmw.item
           from im_detail_match_cn_ws idmw,
                im_detail_match_cn_ws idmwcn
          where idmw.workspace_id   = I_workspace_id
            and idmw.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
            and idmw.choice_flag    = 'Y'
            and idmw.ui_filter_ind  = 'N'
            and idmw.tax_discrepant = 'Y'
            --
            and idmwcn.workspace_id  = I_workspace_id
            and idmwcn.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
            and idmwcn.choice_flag   = 'Y'
            and idmwcn.ui_filter_ind = 'N'
            --
            and idmwcn.workspace_id  = idmw.workspace_id
            and idmwcn.item          = idmw.item;

         LOGGER.LOG_INFORMATION(L_program||' Populate gtt for selected CN Items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
         
      else

         --Collect All Unmatched CN items
         delete from gtt_6_num_6_str_6_date;
         insert into gtt_6_num_6_str_6_date(number_1,   -- workspace_id
                                            number_2,   -- cn_id
                                            number_3,   -- cn_qty
                                            number_4,   -- cn_unit_cost
                                            varchar2_1) -- item
         with select_cn as (select distinct idmwcn.workspace_id,
                                            idmwcn.cn_id
                              from im_detail_match_cn_ws idmw,
                                   im_detail_match_cn_ws idmwcn
                             where idmw.workspace_id   = I_workspace_id
                               and idmw.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
                               and idmw.choice_flag    = 'Y'
                               and idmw.ui_filter_ind  = 'N'
                               and idmw.tax_discrepant = 'Y'
                               --
                               and idmwcn.workspace_id  = I_workspace_id
                               and idmwcn.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
                               and idmwcn.choice_flag   = 'Y'
                               and idmwcn.ui_filter_ind = 'N'
                               --
                               and idmwcn.workspace_id  = idmw.workspace_id
                               and idmwcn.item          = idmw.item)
         select sc.workspace_id,
                sc.cn_id,
                iddrc.adjusted_qty,
                iddrc.adjusted_unit_cost,
                iddrc.item
           from select_cn sc,
                im_doc_detail_reason_codes iddrc
          where iddrc.doc_id  = sc.cn_id
            and iddrc.cost_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
            and iddrc.qty_matched IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                      REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC);

         LOGGER.LOG_INFORMATION(L_program||' Populate gtt for All Unmatched Items on Selected CN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
         
      end if; -- L_tax_document_creation_lvl

         --Collect Selected CNR items
         delete from gtt_10_num_10_str_10_date;
         insert into gtt_10_num_10_str_10_date(number_1,   -- workspace_id
                                               number_2,   -- cnr_id
                                               number_3,   -- cnr_qty
                                               number_4,   -- cnr_unit_cost
                                               varchar2_1, -- item
                                               varchar2_2) -- reason_code_id
         select idmw.workspace_id,
                idmwcnr.cnr_id,
                idmwcnr.cnr_qty,
                idmwcnr.cnr_unit_cost,
                idmw.item,
                imcdw.reason_code_id
           from im_detail_match_cn_ws idmw,
                im_detail_match_cn_ws idmwcnr,
                im_match_cnr_detl_ws imcdw
          where idmw.workspace_id   = I_workspace_id
            and idmw.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
            and idmw.choice_flag    = 'Y'
            and idmw.ui_filter_ind  = 'N'
            and idmw.tax_discrepant = 'Y'
            --
            and idmwcnr.workspace_id  = idmw.workspace_id
            and idmwcnr.item         = idmw.item
            and idmwcnr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
            and idmwcnr.choice_flag   = 'Y'
            and idmwcnr.ui_filter_ind = 'N'
            --
            and imcdw.workspace_id  = I_workspace_id
            and imcdw.doc_id        = idmwcnr.cnr_id
            and imcdw.item          = idmwcnr.item
            and imcdw.status        <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH;

         LOGGER.LOG_INFORMATION(L_program||' Populate gtt for selected CNR Items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --------------------------------------
      --Write resolution and comments records
      --------------------------------------

      --Resln for Credit Memo
      insert into im_resolution_action(doc_id,
                                       item,
                                       reason_code_id,
                                       action,
                                       qty,
                                       unit_cost,
                                       extended_cost,
                                       status,
                                       shipment,
                                       created_by,
                                       creation_date,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
      select gtt.number_2, --cn_id
             gtt.varchar2_1, --item,
             I_tax_resln_rc,
             irc.action,
             gtt.number_3, --cn_qty
             gtt.number_4, --cn_unit_cost
             gtt.number_3 * gtt.number_4, --ext_cost
             REIM_CONSTANTS.RCA_STATUS_UNROLLED, -- status
             NULL, --Shipment
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt,
             im_reason_codes irc
       where irc.reason_code_id = I_tax_resln_rc;

      LOGGER.LOG_INFORMATION(L_program||' Insert resolution_action rows for Credit Memo - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      insert into im_doc_detail_comments (comment_id,
                                          comment_type,
                                          text,
                                          created_by,
                                          creation_date,
                                          doc_id,
                                          item,
                                          discrepancy_type,
                                          reason_code_id,
                                          debit_reason_code,
                                          last_updated_by,
                                          last_update_date,
                                          object_version_id)
      select im_doc_detail_comments_seq.NEXTVAL,
             REIM_CONSTANTS.CMNT_TYPE_EXTERNAL,
             I_tax_resln_cmnt,
             L_user,
             L_vdate,
             gtt.number_2, --cn_id
             gtt.varchar2_1, --item
             REIM_CONSTANTS.CMNT_DISC_TYPE_TAX discrepancy_type,
             I_tax_resln_rc,
             NULL debit_reason_code,
             L_user,
             L_vdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt
       where I_tax_resln_cmnt is NOT NULL;

      LOGGER.LOG_INFORMATION(L_program||' Insert doc_detail_comments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --Resln for New Credit Note Request
      insert into im_resolution_action(doc_id,
                                       item,
                                       reason_code_id,
                                       action,
                                       qty,
                                       unit_cost,
                                       extended_cost,
                                       status,
                                       shipment,
                                       created_by,
                                       creation_date,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
      select gtt.number_2, --cnr_id
             gtt.varchar2_1, --item,
             gtt.varchar2_2, --reason_code_id
             irc.action,
             gtt.number_3, --cnr_qty
             gtt.number_4, --cnr_unit_cost
             gtt.number_3 * gtt.number_4, --ext_cost
             REIM_CONSTANTS.RCA_STATUS_UNROLLED, -- status
             NULL, --Shipment
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_10_num_10_str_10_date gtt,
             im_reason_codes irc
       where irc.reason_code_id = gtt.varchar2_2;

      LOGGER.LOG_INFORMATION(L_program||' Insert resolution_action rows for New CNR - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --------------------------------------
      --write match history
      --------------------------------------

      delete from gtt_num_num_str_str_date_date;

      --number_1   im_detail_match_cn_hist_seq
      --varchar2_1 item
      --varchar2_2 exact_match
      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1)
      select im_detail_match_cn_hist_seq.nextval,
             d.item
        from im_detail_match_cn_ws d
       where d.workspace_id   = I_workspace_id
         and d.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_SKU
         and d.choice_flag    = 'Y'
         and d.ui_filter_ind  = 'N'
         and d.tax_discrepant = 'Y';

      insert into im_cn_detail_match_his(match_id,
                                         doc_id,
                                         item,
                                         reason_code_id,
                                         created_by,
                                         creation_date,
                                         last_updated_by,
                                         last_update_date,
                                         object_version_id)
         select gtt.number_1,
                d.cn_id,
                d.item,
                null,
                L_user,
                sysdate,
                L_user,
                sysdate,
                0
           from gtt_num_num_str_str_date_date gtt,
                im_detail_match_cn_ws d
          where gtt.varchar2_1 = d.item
            and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CN
            and d.choice_flag  = 'Y'
            and d.workspace_id = I_workspace_id
         union all
         select gtt.number_1,
                d.cnr_id,
                d.item,
                drc.reason_code_id,
                L_user,
                sysdate,
                L_user,
                sysdate,
                0
           from gtt_num_num_str_str_date_date gtt,
                im_detail_match_cn_ws d,
                im_doc_detail_reason_codes drc
          where gtt.varchar2_1 = d.item
            and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_CN_WS_ENT_CNR
            and d.choice_flag  = 'Y'
            and d.workspace_id = I_workspace_id
            and d.cnr_id       = drc.doc_id
            and d.item         = drc.item;

      LOGGER.LOG_INFORMATION(L_program||' insert im_cn_detail_match_his - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --------------------------------------
      --Update CN and CNR records
      --------------------------------------

      merge into im_doc_detail_reason_codes tgt
      using (select gtt.number_1 workspace_id,
                    gtt.number_2 doc_id,
                    gtt.varchar2_1 item
               from gtt_6_num_6_str_6_date gtt
             union all
             select gtt.number_1 workspace_id,
                    gtt.number_2 doc_id,
                    gtt.varchar2_1 item
               from gtt_10_num_10_str_10_date gtt
            ) src
      on (    tgt.doc_id       = src.doc_id
          and tgt.item         = src.item)
      when MATCHED then
         update
            set tgt.cost_matched      = 'Y',
                tgt.qty_matched       = 'Y',
                tgt.tax_matched       = 'Y',
                tgt.last_updated_by   = L_user,
                tgt.last_update_date  = sysdate,
                tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

      LOGGER.LOG_INFORMATION(L_program||' Merge on im_doc_detail_reason_codes for CN and CNR - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into im_doc_head tgt
      using (select iddrc.doc_id,
                    SUM(DECODE(iddrc.cost_matched,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                               REIM_CONSTANTS.ONE))
                    +
                    SUM(DECODE(iddrc.qty_matched,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                               REIM_CONSTANTS.ONE)) unmatch_count
               from gtt_6_num_6_str_6_date gtt6,
                    im_doc_detail_reason_codes iddrc
              where iddrc.doc_id = gtt6.number_2
                and iddrc.status = REIM_CONSTANTS.DOC_ITEM_STATUS_APPRVE
              GROUP BY iddrc.doc_id
             union all
             select iddrc.doc_id,
                    SUM(DECODE(iddrc.cost_matched,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                               REIM_CONSTANTS.ONE))
                    +
                    SUM(DECODE(iddrc.qty_matched,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.ZERO,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.ZERO,
                               REIM_CONSTANTS.ONE)) unmatch_count
               from gtt_10_num_10_str_10_date gtt10,
                    im_doc_detail_reason_codes iddrc
              where iddrc.doc_id = gtt10.number_2
                and iddrc.status = REIM_CONSTANTS.DOC_ITEM_STATUS_APPRVE
              GROUP BY iddrc.doc_id) src
      on (    tgt.doc_id        = src.doc_id
          and src.unmatch_count = REIM_CONSTANTS.ZERO)
      when MATCHED then
         update
            set tgt.status            = REIM_CONSTANTS.DOC_STATUS_MTCH,
                tgt.match_id          = L_user,
                tgt.match_date        = L_vdate,
                tgt.match_type        = REIM_CONSTANTS.MATCH_TYPE_MANUAL,
                tgt.last_updated_by   = L_user,
                tgt.last_update_date  = sysdate,
                tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

      LOGGER.LOG_INFORMATION(L_program||' Merge Status on im_doc_head for CN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --------------------------------------
      --refresh the workspace
      --------------------------------------
      if REFRESH_MTCH_WSPACE(O_error_message,
                             I_workspace_id,
                             'N') = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

      --------------------------------------
      --Recreate the Detail Matching Workspace
      --------------------------------------
      if CREATE_MATCH_CN_DETAIL_WS(O_error_message,
                                   I_workspace_id) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

   end if; -- I_tax_decision

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END PERFORM_ONLINE_TAX_RESLN;
------------------------------------------------------------------------------------------
END REIM_ONLINE_MATCH_CN_SQL;
/
