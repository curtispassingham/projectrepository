CREATE OR REPLACE PACKAGE BODY REIM_TAX_DATA_SQL AS
    --------------------------------------------
    PROCEDURE GET_VENDOR_TAX_CODES (O_STATUS          OUT NUMBER, -- Success status of the procedure.
                                    O_ERROR_MESSAGE   OUT VARCHAR2, -- Error message if the procedure failed.
                                    I_VENDOR_DATA	  IN  OBJ_VENDOR_DATA_TBL, -- A table containing the vendors we desire to return taxes for.
                                    I_DATE            IN  DATE, -- The date to use to determine taxes.
                                    I_TAX_TYPE        IN  VARCHAR2, -- The type of taxes to retreive.
                                    O_VENDOR_TAXES    OUT OBJ_VENDOR_TAX_DATA_TBL -- A table containing the valid vendor taxes for the date supplied.
                                    )
    IS
        L_PROGRAM VARCHAR2(50) := 'REIM_TAX_DATA_SQL.GET_VENDOR_TAX_CODES';
    BEGIN
        REIM_TAX_WRAPPER_SQL.GET_VENDOR_TAX_CODES_SINGLETAX(O_STATUS,
                                                  O_ERROR_MESSAGE,
                                                  I_VENDOR_DATA,
                                                  I_DATE,
                                                  I_TAX_TYPE,
                                                  O_VENDOR_TAXES);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                            ||SQL_LIB.CREATE_MSG(PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;
    END GET_VENDOR_TAX_CODES;
    --------------------------------------------

    --------------------------------------------
    PROCEDURE GET_TAX_BREAKUP (O_STATUS         OUT  NUMBER, -- Success status of the procedure.
                                    O_ERROR_MESSAGE  OUT  VARCHAR2, -- Error message if the procedure failed.
                                    I_TRAN_TYPE      IN	  VARCHAR2, -- The transaction type to retrieve the breakup for.
                                    I_TRANSACTIONS   IN   OBJ_NUMERIC_ID_TABLE, -- 	A collection of transaction numbers to retrieve breakups for.
                                    I_ITEMS          IN   OBJ_VARCHAR_ID_TABLE, --	A collection of items to get the breakup for.  This field is optional.
                                    I_LOCATIONS      IN   OBJ_NUMERIC_ID_TABLE, --	A collection of items to get the breakup for.
                                    I_DATE           IN  DATE, -- The date to use to determine taxes.
                                    O_TAX_BREAKUPS   OUT  OBJ_ITEM_TAX_BREAKUP_TBL -- A table of breakup records.
                                    )
    IS
        L_PROGRAM VARCHAR2(50) := 'REIM_TAX_DATA_SQL.GET_TAX_BREAKUP';
    BEGIN

        REIM_TAX_WRAPPER_SQL.GET_TAX_BREAKUP_SINGLETAX(O_STATUS,
                                             O_ERROR_MESSAGE,
                                             I_TRAN_TYPE,
                                             I_TRANSACTIONS,
                                             I_ITEMS,
                                             I_LOCATIONS,
                                             I_DATE,
                                             O_TAX_BREAKUPS);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                            ||SQL_LIB.CREATE_MSG(PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;
    END GET_TAX_BREAKUP;

----------------------------------------------------------------------------------------
	FUNCTION CHECK_CN_REV_VAT_ELIGIBLITY(O_error_message     OUT VARCHAR2,
	                                     O_is_eligible       OUT VARCHAR2,
	                                     I_vendor_id      IN     IM_INJECT_DOC_HEAD.VENDOR%TYPE,
	                                     I_doc_date       IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE,
	                                     I_reference_cnr  IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF3%TYPE,
	                                     I_reference_invc IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF4%TYPE)
	RETURN NUMBER IS
		L_program VARCHAR2(50) := 'REIM_TAX_DATA_SQL.CHECK_CN_REV_VAT_ELIGIBLITY';
	BEGIN
	
	   return REIM_TAX_WRAPPER_SQL.CHECK_CN_REV_VAT_ELIGIBLITY(O_error_message,
	                                                           O_is_eligible,
	                                                           I_vendor_id,
	                                                           I_doc_date,
	                                                           I_reference_cnr,
	                                                           I_reference_invc);
	
	EXCEPTION

	  when OTHERS then
	     O_error_message := O_error_message ||
	     					SQL_LIB.CREATE_MSG(PACKAGE_ERROR, SQLERRM, L_program, TO_CHAR(SQLCODE));
	     return REIM_CONSTANTS.FAIL;

	END CHECK_CN_REV_VAT_ELIGIBLITY;
----------------------------------------------------------------------------------------	

END REIM_TAX_DATA_SQL;
/
