CREATE OR REPLACE PACKAGE REIM_POSTING_SQL AS

   YN_YES CONSTANT VARCHAR2(1) := 'Y';
   YN_NO  CONSTANT VARCHAR2(1) := 'N';

   DEBUG_ENABLED CONSTANT VARCHAR2(1)  := YN_NO;
   NO_VALUE      CONSTANT VARCHAR2(10) := 'NO_VALUE';

   TDELIM CONSTANT VARCHAR2(10) := ':';

   -- CONSTANTS--
   POSTING_STATUS_NEW        CONSTANT IM_POSTING_STATUS.STATUS%TYPE := 'N';
   POSTING_STATUS_IN_PROCESS CONSTANT IM_POSTING_STATUS.STATUS%TYPE := 'P';
   POSTING_STATUS_COMPLETE   CONSTANT IM_POSTING_STATUS.STATUS%TYPE := 'C';

   -- AMOUNT TYPES FOR POSTING DOCUMENT AMOUNTS --
   AMT_DOCUMENT_HEADER_COST      CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'doc.header.cost';
   AMT_MERCHANDISE_COST          CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'doc.merch.cost';
   AMT_NON_MERCHANDISE_COST      CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'invoice.non.merch.cost';
   AMT_VARIANCE_WITHIN_TOLERANCE CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'variance.within.tolerance';
   AMT_MATCHED_RECEIPT_COST      CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'matched.receipt.cost';
   AMT_RESOLUTION_ACTION         CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'resolution.action';
   AMT_RESOLUTION_ACTION_OFFSET  CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'resolution.action.offset';
   AMT_RESOLUTION_DOC_NON_MERCH  CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'resolution.doc.nonmerch';
   AMT_FIXED_DEAL                CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'fixed.deal';
   AMT_COMPLEX_DEAL              CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'complex.deal';
   AMT_CREDIT_NOTE_DWO           CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'crdnt.dwo';
   AMT_CREDIT_NOTE_DWO_OFFSET    CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'crdnt.dwo.offset';
   AMT_CREDIT_NOTE_VWT           CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'crdnt.vwt';
   AMT_CREDIT_NOTE_VWT_OFFSET    CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'crdnt.vwt.offset';
   AMT_VARIANCE_CALC_TOLERANCE   CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'variance.calc.tolerance';
   AMT_VAR_CALC_TAX_TOLERANCE    CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'variance.calc.tax.tolerance';
   AMT_PPA_COST                  CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'doc.ppa.cost';
   AMT_RWO_COST                  CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'doc.rwo.cost';
   AMT_TAX                       CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'tax.amount';
   AMT_ACQ_VAT                   CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'acquisition.vat';
   AMT_ACQ_VAT_OFFSET            CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'acquisition.vat.offset';
   AMT_REV_CHRG_VAT              CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'reverse.charge.vat';
   AMT_REV_CHRG_VAT_OFFSET       CONSTANT IM_POSTING_DOC_AMOUNTS.AMOUNT_TYPE%TYPE := 'reverse.charge.vat.offset';

   --ACCOUNT TYPE
   BASIC_TRANSACTIONS  CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_TYPE%TYPE := 'BT';
   NON_MERCH_CODES     CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_TYPE%TYPE := 'NMC';
   REASON_CODE_ACTIONS CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_TYPE%TYPE := 'RCA';

   --ACCOUNT CODES
   TRADE_ACCOUNTS_PAYABLE         CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'TAP';
   TAP_NON_DYNAMIC                CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'TAPNDI';
   DEAL_INCOME_RECEIVABLE_COMPLEX CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'DIRAR';
   DEAL_INCOME_RECEIVABLE_FIXED   CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'DIRAF';
   UNMATCHED_RECEIPT              CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'UNR';
   VARIANCE_WITHIN_TOLERANCE      CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'VWT';
   PRE_PAID_ASSET                 CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'PPA';
   CREDIT_NOTE                    CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'CRN';
   CREDIT_NOTE_NON_DYNAMIC        CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'CRNNDI';
   RECEIPT_WRITE_OFF              CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'RWO';
   TAX                            CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'TAX';
   TAX_NON_DYNAMIC                CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'TAXNDI';
   TAX_ACQUISITION                CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'TAXACQ';
   TAX_ACQUISITION_OFFSET         CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'TAXACO';
   TAX_REVERSE_CHARGE             CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'TAXRVC';
   TAX_REVERSE_CHARGE_OFFSET      CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'TAXRVO';	  
   VARIANCE_CALC_TOLERANCE        CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'VCT';
   VARIANCE_CALC_TAX_TOLERANCE    CONSTANT IM_POSTING_DOC_AMOUNTS.ACCOUNT_CODE%TYPE := 'VCTT';

   BASIS_DETAILS          CONSTANT IM_POSTING_DOC.CALC_BASIS%TYPE := 'doc.details';
   BASIS_HEADER           CONSTANT IM_POSTING_DOC.CALC_BASIS%TYPE := 'doc.header';
   BASIS_TRACKED_RECEIPTS CONSTANT IM_POSTING_DOC.CALC_BASIS%TYPE := 'invoice.tracked.receipts';

   SYS_TAX_IND_INTRA_VAT_REG CONSTANT IM_POSTING_DOC.SYS_TAX_IND%TYPE := 'I';
   SYS_TAX_IND_ACQ_VAT       CONSTANT IM_POSTING_DOC.SYS_TAX_IND%TYPE := 'A';
   SYS_TAX_IND_REV_CHRG_VAT  CONSTANT IM_POSTING_DOC.SYS_TAX_IND%TYPE := 'R';

   TAX_REGION_EU_BASE    CONSTANT VAT_REGION.VAT_REGION_TYPE%TYPE := 'E';
   TAX_REGION_EU_MEMBER  CONSTANT VAT_REGION.VAT_REGION_TYPE%TYPE := 'M';
   TAX_REGION_NON_MEMBER CONSTANT VAT_REGION.VAT_REGION_TYPE%TYPE := 'N';

   MATCH_TYPE_SUMMARY CONSTANT IM_POSTING_DOC.MATCH_TYPE%TYPE := 'summary.match';
   MATCH_TYPE_DETAIL  CONSTANT IM_POSTING_DOC.MATCH_TYPE%TYPE := 'detail.match';
   MATCH_TYPE_UNKNOWN CONSTANT IM_POSTING_DOC.MATCH_TYPE%TYPE := 'unknown.match';

   DOC_TYPE_MERCH_INVOICE    CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'MRCHI';
   DOC_TYPE_DEBIT_MEMO_QTY   CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'DEBMEQ';
   DOC_TYPE_DEBIT_MEMO_COST  CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'DEBMEC';
   DOC_TYPE_DEBIT_MEMO_TAX   CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'DEBMET';
   DOC_TYPE_CREDIT_MEMO_QTY  CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'CRDMEQ';
   DOC_TYPE_CREDIT_MEMO_COST CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'CRDMEC';
   DOC_TYPE_NONMERCH_INVOICE CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'NMRCHI';
   DOC_TYPE_CREDIT_NOTE      CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'CRDNT';
   DOC_TYPE_RECEIPT_WRITEOFF CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'RWO';

   DOC_HELD                CONSTANT IM_DOC_HEAD.HOLD_STATUS%TYPE := 'H';
   DOC_NOT_HELD            CONSTANT IM_DOC_HEAD.HOLD_STATUS%TYPE := 'N';
   DOC_RELEASED_FROM_HOLD  CONSTANT IM_DOC_HEAD.HOLD_STATUS%TYPE := 'R';
   DOC_RELEASED_AND_POSTED CONSTANT IM_DOC_HEAD.HOLD_STATUS%TYPE := 'P';

   DOC_STATUS_MATCHED         CONSTANT IM_DOC_HEAD.STATUS%TYPE := 'MTCH';
   DOC_STATUS_POSTED          CONSTANT IM_DOC_HEAD.STATUS%TYPE := 'POSTED';
   DOC_STATUS_APPROVED        CONSTANT IM_DOC_HEAD.STATUS%TYPE := 'APPRVE';
   DOC_STATUS_TAX_DISCREPANCY CONSTANT IM_DOC_HEAD.STATUS%TYPE := 'TAXDIS';
   DOC_STATUS_READY_FOR_MATCH CONSTANT IM_DOC_HEAD.STATUS%TYPE := 'RMTCH';

   SEVERITY_WARNING CONSTANT IM_POSTING_DOC_ERRORS.SEVERITY%TYPE := 'WARN';
   SEVERITY_ERROR   CONSTANT IM_POSTING_DOC_ERRORS.SEVERITY%TYPE := 'ERROR';
   SEVERITY_DEBUG   CONSTANT IM_POSTING_DOC_ERRORS.SEVERITY%TYPE := 'DEBUG';

   ERROR_CAT_VALIDATION CONSTANT IM_POSTING_DOC_ERRORS.CATEGORY%TYPE := 'validation';
   ERROR_CAT_OTHER      CONSTANT IM_POSTING_DOC_ERRORS.CATEGORY%TYPE := 'others';
   ERROR_CAT_BALANCING  CONSTANT IM_POSTING_DOC_ERRORS.CATEGORY%TYPE := 'balancing';

   DEAL_TYPE_FIXED     CONSTANT IM_DOC_HEAD.DEAL_TYPE%TYPE := 'F';
   DEAL_TYPE_COMPLEX   CONSTANT IM_DOC_HEAD.DEAL_TYPE%TYPE := 'C';
   DEAL_TYPE_NON_MERCH CONSTANT IM_DOC_HEAD.DEAL_TYPE%TYPE := 'N';

   REASON_DISCREPANCY_WRITEOFF CONSTANT IM_REASON_CODES.ACTION%TYPE := 'DWO';

   REASON_STATUS_UNROLLED IM_RESOLUTION_ACTION.STATUS%TYPE := 'U';

   EXCHANGE_TYPE_CONSOLIDATED VARCHAR(1) := 'C';
   
   EXCHANGE_TYPE_OPERATIONAL VARCHAR(1) := 'O';

   PACKAGE_ERROR CONSTANT VARCHAR2(20) := 'PACKAGE_ERROR';

   SHIP_TRAN_TYPE CONSTANT VARCHAR2(1) := 'S';

   SUCCESS CONSTANT NUMBER(10) := 1;
   FAIL    CONSTANT NUMBER(10) := 0;

   DEFAULT_DIGITS CONSTANT NUMBER(6) := 4;

   --location
   STORE     CONSTANT VARCHAR(1) := 'S';
   WAREHOUSE CONSTANT VARCHAR(1) := 'W';

------------------------------------------------------------------------------------------------------
FUNCTION BEGIN_POSTING_PROCESS (O_error_message    OUT VARCHAR2,
                                I_posting_id    IN     IM_POSTING_STATUS.POSTING_ID%TYPE,
                                I_description   IN     IM_POSTING_STATUS.DESCRIPTION%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_MATCHED_INVOICES(O_error_message    OUT VARCHAR2,
                                  I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                  I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_PREPAID_INVOICES(O_error_message    OUT VARCHAR2,
                                  I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                  I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_INVOICES_TO_POST(O_error_message                 OUT VARCHAR2,
                                   I_posting_id                 IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                   I_header_to_detail_tolerance IN     NUMBER)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_NON_MERCH_COSTS(O_error_message    OUT VARCHAR2,
                              I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_INVOICE_RESOLUTION_COSTS(O_error_message    OUT VARCHAR2,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_DOC_MERCH_COST(O_error_message    OUT VARCHAR2,
                             I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_INVOICE_VWT(O_error_message    OUT VARCHAR2,
                          I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_INVOICE_MATCHED_RECEIPTS(O_error_message    OUT VARCHAR2,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CHK_DB_CR_BALANCE_GL_STAGE(O_error_message    OUT VARCHAR2,
                                    I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                    I_doc_id        IN     IM_POSTING_DOC.DOC_ID%TYPE DEFAULT NULL)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_DOCUMENT_PPA_VALUE(O_error_message    OUT VARCHAR2,
                                 I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CHK_DB_CR_BALANCE_AP_STAGE(O_error_message    OUT VARCHAR2,
                                    I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                    I_doc_id        IN     IM_POSTING_DOC.DOC_ID%TYPE DEFAULT NULL)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION ADD_POSTING_DOC_ERROR(O_error_message    OUT VARCHAR2,
                               I_posting_id    IN     IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
                               I_doc_id        IN     IM_POSTING_DOC_ERRORS.DOC_ID%TYPE,
                               I_severity      IN     IM_POSTING_DOC_ERRORS.SEVERITY%TYPE,
                               I_category      IN     IM_POSTING_DOC_ERRORS.CATEGORY%TYPE,
                               I_error_msg     IN     IM_POSTING_DOC_ERRORS.ERROR_MSG%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION DELETE_POSTING_DOC_ERROR(O_error_message    OUT VARCHAR2,
                                  I_posting_id    IN     IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
                                  I_doc_id        IN     IM_POSTING_DOC_ERRORS.DOC_ID%TYPE DEFAULT NULL,
                                  I_severity      IN     IM_POSTING_DOC_ERRORS.SEVERITY%TYPE DEFAULT NULL)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_APPROVED_DEBIT_MEMOS(O_error_message    OUT VARCHAR2,
                                      I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                      I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                      I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_APPROVED_CREDIT_MEMOS(O_error_message    OUT VARCHAR2,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                       I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                       I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RESOLUTION_DOCS(O_error_message    OUT VARCHAR2,
                                  I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_FIXED_DEALS(O_error_message    OUT VARCHAR2,
                          I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_COMPLEX_DEALS(O_error_message    OUT VARCHAR2,
                            I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_RESOLUTION_DOC_DETAIL_AMT(O_error_message    OUT VARCHAR2,
                                        I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_DOCUMENT_VALUE(O_error_message    OUT VARCHAR2,
                             I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_DOCUMENT_TAX(O_error_message    OUT VARCHAR2,
                           I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                           I_isprepay      IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION LOAD_DOC_MERCH_COMPONENTS(O_error_message    OUT VARCHAR2,
                                   I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION ADD_ERROR_AUTO_TRAN(O_error_message    OUT VARCHAR2,
                             I_posting_id    IN     IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
                             I_doc_id        IN     IM_POSTING_DOC_ERRORS.DOC_ID%TYPE,
                             I_severity      IN     IM_POSTING_DOC_ERRORS.SEVERITY%TYPE,
                             I_category      IN     IM_POSTING_DOC_ERRORS.CATEGORY%TYPE,
                             I_error_msg     IN     IM_POSTING_DOC_ERRORS.ERROR_MSG%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_APPROVED_NONMERCH(O_error_message    OUT VARCHAR2,
                                   I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                   I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                   I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_APPROVED_CREDIT_NOTES(O_error_message    OUT VARCHAR2,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                       I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                       I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_RELEASED_CREDIT_NOTES(O_error_message    OUT VARCHAR2,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                       I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                       I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_NON_HELD_CREDIT_NOTES(O_error_message    OUT VARCHAR2,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                       I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                       I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_RECEIPT_WRITEOFFS(O_error_message     OUT VARCHAR2,
                                   I_posting_id      IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                   I_rcpt_write_offs IN     OBJ_REIM_RCPT_WRITEOFF_TBL,
                                   I_sys_curr_code   IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION LOAD_RECEIPT_MERCH(O_error_message      OUT VARCHAR2,
                            I_posting_id      IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                            I_doc_id          IN     IM_DOC_HEAD.DOC_ID%TYPE,
                            I_rcpt_write_offs IN     OBJ_REIM_RCPT_WRITEOFF_TBL,
                            I_sys_curr_code   IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_DWO_CREDIT_NOTES(O_error_message    OUT VARCHAR2,
                                  I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                                  I_doc_id_list   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_sys_curr_code IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_CREDIT_NOTE_DWO(O_error_message    OUT VARCHAR2,
                              I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
--------------------------------------------
FUNCTION CALC_CREDIT_NOTE_VWT(O_error_message    OUT VARCHAR2,
                              I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_RWO_COST(O_error_message    OUT VARCHAR2,
                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION LOAD_RWO_TAX_AMOUNT (O_error_message    OUT VARCHAR2,
                              I_tax_breakups         OBJ_ITEM_TAX_BREAKUP_TBL, --BOBY - no in/out?
                              I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_CURRENCY_EXCHANGE_RATE(O_error_message    OUT VARCHAR2,
                                       I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_CURR_EXCH_RWO(O_error_message    OUT VARCHAR2,
                              I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION IS_ACCOUNTABLE_RESOLUTION(I_resolution_action IN IM_RESOLUTION_ACTION.ACTION%TYPE) --BOBY - no out error_message?
RETURN VARCHAR2;
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_DEFAULT_DEPT_CLASS(O_error_message    OUT VARCHAR2,
                                   I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_RESOLUTION_AMOUNT (IM_RESOLUTION_ACTION_ROWID ROWID,
                                 IM_INVOICE_DETAIL_ROWID ROWID) --BOBY - in/out declarations???
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_IMPORTER_LOCATIONS(O_error_message    OUT VARCHAR2,
                                   I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_CREDIT_NOTE_HOLD_STATUS(O_error_message    OUT VARCHAR2,
                                        I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SITE_ID(O_error_message    OUT VARCHAR2,
                          I_posting_id    IN     IM_POSTING_DOC_ERRORS.POSTING_ID%TYPE,
                          I_doc_id        IN     IM_POSTING_DOC_ERRORS.DOC_ID%TYPE,
                          I_import_id     IN     ORDHEAD.IMPORT_ID%TYPE,
                          I_idh_supp      IN     IM_DOC_HEAD.VENDOR%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION ADJ_VARIANCE_GL_STAGE(O_error_message        OUT VARCHAR2,
                               I_posting_id        IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                               I_doc_id            IN     IM_POSTING_DOC.DOC_ID%TYPE DEFAULT NULL,
                               I_balance_tolerance IN     IM_SYSTEM_OPTIONS.BALANCING_TOLERANCE%TYPE,
                               I_signed_amt        IN     IM_FINANCIALS_STAGE.AMOUNT%TYPE,
                               I_trans_amount		   IN     IM_FINANCIALS_STAGE.TRANS_AMOUNT%TYPE,
                               I_prim_trans_amount IN     IM_FINANCIALS_STAGE.PRIM_TRANS_AMOUNT%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_DOCUMENT_VCT(O_error_message    OUT VARCHAR2,
                           I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                           I_is_prepay     IN     VARCHAR2 DEFAULT REIM_CONSTANTS.YN_NO)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_POSTING_DOC_TAX(O_error_message    OUT VARCHAR2,
                                I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION LOAD_ACTUAL_TAX(O_error_message    OUT VARCHAR2,
                         I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION POST_SYSTEM_GEN_TAX(O_error_message    OUT VARCHAR2,
                             I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION PREPAY_MRCHI(O_error_message   IN OUT VARCHAR2,
                      O_prepay_comments    OUT OBJ_VARCHAR_ID_TABLE,
					  O_posting_id		   OUT  NUMBER,
                      I_workspace_id    IN     IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_VOID_CNR(O_error_message         OUT VARCHAR2,
                          I_posting_id         IN     IM_POSTING_DOC.POSTING_ID%TYPE,
                          I_doc_id_list        IN     OBJ_NUMERIC_ID_TABLE,
                          I_prim_currency_code IN     VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION CALC_VOID_CNR_AMOUNT(O_error_message    OUT VARCHAR2,
                              I_posting_id    IN     IM_POSTING_DOC.POSTING_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION TRUNCATE_FINANCIALS_STAGE(O_error_message    OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION TRUNCATE_AP_STAGE(O_error_message    OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION ADJUST_AP_STAGE_DETL(O_error_message       OUT VARCHAR2,
                              I_ap_stage_amt_tbl IN     OBJ_REIM_AP_STAGE_AMT_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_BEST_TERMS_PREPAY_INV(O_error_message         OUT VARCHAR2,
                                      I_doc_id_list           IN  OBJ_NUMERIC_ID_TABLE)
                         
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
END REIM_POSTING_SQL;
/