WHENEVER SQLERROR EXIT FAILURE ROLLBACK

create or replace PACKAGE BODY REIM_WRAPPER_SQL IS
----------------------------------------------------------------------
FUNCTION CONVERT (O_error_message        IN OUT  VARCHAR2,
                  I_currency_value       IN      NUMBER,
                  I_currency             IN      CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  I_currency_out         IN      CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  O_currency_value       IN OUT  NUMBER,
                  I_cost_retail_ind      IN      VARCHAR2,
                  I_effective_date       IN      CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                  I_exchange_type        IN      CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                  I_in_exchange_rate     IN      CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_out_exchange_rate    IN      CURRENCY_RATES.EXCHANGE_RATE%TYPE)
	RETURN NUMBER IS

BEGIN
  if not CURRENCY_SQL.CONVERT(O_error_message,
				                  I_currency_value,
                  				I_currency,
                  				I_currency_out,
                  				O_currency_value,
                  				I_cost_retail_ind,
                  				I_effective_date,
                  				I_exchange_type,
                  				I_in_exchange_rate,
                  				I_out_exchange_rate) then
     return 0;
  else
     return 1;
  end if;

EXCEPTION
  when OTHERS then
     O_error_message := SQLERRM || ' from REIM_WRAPPER_SQL.CONVERT.';
     return 0;
END CONVERT;
-----------------------------------------------------------------------
FUNCTION UPDATE_INVOICE_MATCH_STATUS(O_error_message OUT VARCHAR2,
                                     I_shipments     IN  SHIPMENT_IDS,
                                     I_new_status    IN  VARCHAR2)
   RETURN NUMBER IS
BEGIN
	   if DATES_SQL.RESET_GLOBALS(O_error_message) then
   if SHIPMENT_API_SQL.UPDATE_INVOICE_MATCH_STATUS(O_error_message,
                                                   I_shipments,
                                                   I_new_status) then
      return 1;
    else
      return 0;
   end if;
      else
    return 0;
   end if;
   
EXCEPTION
  when OTHERS then
     O_error_message := SQLERRM || ' from REIM_WRAPPER_SQL.UPDATE_INVOICE_MATCH_STATUS.';
     return 0;
END UPDATE_INVOICE_MATCH_STATUS;
-----------------------------------------------------------------------
FUNCTION UPDATE_INVOICE_QTY_MATCHED(O_error_message OUT VARCHAR2,
                                     I_shipment_item_qtys     IN  SHIPMENT_MATCHED_QTY_ARRAY)
   RETURN NUMBER IS
BEGIN
   if SHIPMENT_API_SQL.UPDATE_INVOICE_QTY_MATCHED(O_error_message,I_shipment_item_qtys) then
      return 1;
    else
      return 0;
   end if;
EXCEPTION
  when OTHERS then
     O_error_message := SQLERRM || ' from REIM_WRAPPER_SQL.UPDATE_INVOICE_QTY_MATCHED.';
     return 0;
END UPDATE_INVOICE_QTY_MATCHED;
-----------------------------------------------------------------------
FUNCTION CLEAR_STAGED_SHIPMENTS(O_error_message OUT VARCHAR2,
                                I_shipments     IN  SHIPMENT_IDS)
   RETURN NUMBER IS
BEGIN
   if SHIPMENT_API_SQL.CLEAR_STAGED_SHIPMENTS(O_error_message,
                                              I_shipments) then
      return 1;
    else
      return 0;
   end if;
EXCEPTION
  when OTHERS then
     O_error_message := SQLERRM || ' from REIM_WRAPPER_SQL.CLEAR_STAGED_SHIPMENTS.';
     return 0;
END CLEAR_STAGED_SHIPMENTS;
-----------------------------------------------------------------------
FUNCTION GET_ORACLE_VENDOR_SITE_ID (O_error_message         OUT VARCHAR2,
                                    O_oracle_vendor_site_id OUT NUMBER,
                                    I_location              IN  NUMBER,
                                    I_vendor_id             IN  VARCHAR2,
                                    I_order_no              IN  NUMBER,
                                    I_rtv_ind 				IN varchar2)
   RETURN NUMBER IS
  type r_cursor is REF CURSOR;
   c_site_id r_cursor;
   BEGIN
    
   IF I_rtv_ind = 'Y' THEN
   OPEN c_site_id FOR
      SELECT ( CASE
               WHEN addr_ord.cnt > 0 THEN
                    (SELECT /*+ INDEX(ad ADDR_I3) */
                            TO_NUMBER(key_value_1)
                       FROM ADDR ad
                           ,RTV_HEAD ord
                      WHERE ord.rtv_order_no  = I_order_no
                        AND ad.KEY_VALUE_1 = TO_CHAR(ord.SUPPLIER)
                        AND ad.addr_type   = REIM_WRAPPER_SQL.ADDR_TYPE_6
                        AND ad.module      = REIM_WRAPPER_SQL.MODULE_SUPP   
						AND ROWNUM < 2)
               ELSE
                    (SELECT (CASE
                             WHEN basic.primary_pay_site = 'Y' THEN
                                  basic.partner
                             WHEN basic.primary_pay_site = 'N' THEN
                                  (SELECT /*+ FIRST_ROWS */
                                           TO_NUMBER(key_value_1) key_value_1
                                     FROM (SELECT key_value_1
                                             FROM ADDR ad
                                            WHERE ad.addr_type  = REIM_WRAPPER_SQL.ADDR_TYPE_6
                                              AND ad.module     = REIM_WRAPPER_SQL.MODULE_SUPP
                                              AND rownum > 0
                                           )
                                    WHERE basic.partner = TO_NUMBER(key_value_1))
                             END)
                       FROM ( 
                       SELECT * FROM ( -- added
                       SELECT sup_part.*
                                FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                          UNION ALL
                                       SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL 
                                            THEN WH.ORG_UNIT_ID 
                                            ELSE (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH ) 
                                        END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH) locs
                                    ,(SELECT pou.PARTNER
                                            ,pou.ORG_UNIT_ID
                                            ,NVL(pou.PRIMARY_PAY_SITE,'N') PRIMARY_PAY_SITE
                                        FROM partner_org_unit pou
                                            ,sups sup
                                       WHERE pou.PARTNER  = sup.SUPPLIER
                                         AND sup.SUPPLIER_PARENT = TO_NUMBER(I_vendor_id)
                                      ) sup_part
                               WHERE sup_part.ORG_UNIT_ID = locs.ORG_UNIT_ID
                                 AND locs.LOCATION           = I_location
                                 ORDER BY primary_pay_site desc -- added
				) where rownum<2 -- added
 			) basic
                    )
               END ) ORACLE_SITE_ID
        FROM ( 
        	SELECT SUM(cnt) as cnt FROM  (
        		SELECT /*+ INDEX(ad ADDR_I4) */
                      COUNT(*) cnt
                 FROM ADDR ad
                     ,RTV_HEAD ord
                     ,( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                          UNION ALL
                                       SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL 
                                            THEN WH.ORG_UNIT_ID 
                                            ELSE (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH ) 
                                        END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH) locs
                     ,(SELECT pou.PARTNER,pou.ORG_UNIT_ID
                              FROM partner_org_unit pou
                                   ,sups sup
                              WHERE pou.PARTNER  = sup.SUPPLIER
                                AND sup.SUPPLIER_PARENT = TO_NUMBER(I_vendor_id)
                       ) sup_part
                WHERE ord.rtv_order_no  = I_order_no
                  AND ad.KEY_VALUE_1 = TO_CHAR(ord.SUPPLIER)
                  AND ad.addr_type   = REIM_WRAPPER_SQL.ADDR_TYPE_6
                  AND ad.module      = REIM_WRAPPER_SQL.MODULE_SUPP
                  AND locs.location  = I_location
                  AND sup_part.ORG_UNIT_ID = locs.ORG_UNIT_ID
                  AND sup_part.partner = ord.SUPPLIER
                  UNION 
               --Query to handle the supplier site off scenario
               SELECT /*+ INDEX(ad ADDR_I4) */
                      COUNT(*) cnt
                 FROM ADDR ad
                     ,RTV_HEAD ord
                WHERE ord.rtv_order_no  = I_order_no
                  AND ad.KEY_VALUE_1 = TO_CHAR(ord.SUPPLIER)
                  AND ad.addr_type   = REIM_WRAPPER_SQL.ADDR_TYPE_6
                  AND ad.module      = REIM_WRAPPER_SQL.MODULE_SUPP
                  AND ord.SUPPLIER = I_vendor_id
              )
              ) addr_ord;
        LOOP
           FETCH c_site_id INTO O_oracle_vendor_site_id;
           exit when c_site_id%notfound;
        END LOOP;
       CLOSE c_site_id;
  
      ELSE 
      OPEN c_site_id FOR
      SELECT ( CASE
               WHEN addr_ord.cnt > 0 THEN
                    (SELECT /*+ INDEX(ad ADDR_I3) */
                            TO_NUMBER(key_value_1)
                       FROM ADDR ad
                           ,ORDHEAD ord
                      WHERE ord.order_no   = I_order_no
                        AND ad.KEY_VALUE_1 = TO_CHAR(ord.SUPPLIER)
                        AND ad.addr_type   = REIM_WRAPPER_SQL.ADDR_TYPE_6
                        AND ad.module      = REIM_WRAPPER_SQL.MODULE_SUPP
						AND ROWNUM < 2)
               ELSE
                    (SELECT (CASE
                             WHEN basic.primary_pay_site = 'Y' THEN
                                  basic.partner
                             WHEN basic.primary_pay_site = 'N' THEN
                                  (SELECT /*+ FIRST_ROWS */
                                           TO_NUMBER(key_value_1) key_value_1
                                     FROM (SELECT key_value_1
                                             FROM ADDR ad
                                            WHERE ad.addr_type  = REIM_WRAPPER_SQL.ADDR_TYPE_6
                                              AND ad.module     = REIM_WRAPPER_SQL.MODULE_SUPP
                                              AND rownum > 0
                                           )
                                    WHERE basic.partner = TO_NUMBER(key_value_1))
                             END)
                       FROM ( 
                       SELECT * FROM ( -- added
                       SELECT sup_part.*
                                FROM ( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                          UNION ALL
                                       SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL 
                                            THEN WH.ORG_UNIT_ID 
                                            ELSE (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH ) 
                                        END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH) locs
                                    ,(SELECT pou.PARTNER
                                            ,pou.ORG_UNIT_ID
                                            ,NVL(pou.PRIMARY_PAY_SITE,'N') PRIMARY_PAY_SITE
                                        FROM partner_org_unit pou
                                            ,sups sup
                                       WHERE pou.PARTNER  = sup.SUPPLIER
                                         AND To_Char(sup.SUPPLIER_PARENT) = To_Char(I_vendor_id)
                                      ) sup_part
                               WHERE sup_part.ORG_UNIT_ID = locs.ORG_UNIT_ID
                                 AND locs.LOCATION           = I_location
                                 ORDER BY primary_pay_site desc -- added
				) where rownum<2 -- added
 			) basic
                    )
               END ) ORACLE_SITE_ID
        FROM ( 
        	SELECT SUM(cnt) as cnt FROM  (
        		SELECT /*+ INDEX(ad ADDR_I4) */
                      COUNT(*) cnt
                 FROM ADDR ad
                     ,ORDHEAD ord
                     ,( SELECT ORG_UNIT_ID,STORE AS LOCATION FROM STORE
                                          UNION ALL
                                       SELECT (CASE WHEN WH.PRIMARY_VWH IS NULL 
                                            THEN WH.ORG_UNIT_ID 
                                            ELSE (SELECT ORG_UNIT_ID FROM WH WAREHOUSE WHERE WAREHOUSE.WH = WH.PRIMARY_VWH ) 
                                        END ) AS ORG_UNIT_ID, WH.WH AS LOCATION FROM WH) locs
                     ,(SELECT pou.PARTNER,pou.ORG_UNIT_ID
                              FROM partner_org_unit pou
                                   ,sups sup
                              WHERE pou.PARTNER  = sup.SUPPLIER
                                AND sup.SUPPLIER_PARENT = TO_NUMBER(I_vendor_id)
                       ) sup_part
                WHERE ord.order_no   = I_order_no
                  AND ad.KEY_VALUE_1 = TO_CHAR(ord.SUPPLIER)
                  AND ad.addr_type   = REIM_WRAPPER_SQL.ADDR_TYPE_6
                  AND ad.module      = REIM_WRAPPER_SQL.MODULE_SUPP
                  AND locs.location  = I_location
                  AND sup_part.ORG_UNIT_ID = locs.ORG_UNIT_ID
                  AND sup_part.partner = ord.SUPPLIER
                  
                  UNION 
               --Query to handle the supplier site off scenario
               SELECT /*+ INDEX(ad ADDR_I4) */
                      COUNT(*) cnt
                 FROM ADDR ad
                     ,ORDHEAD ord
                WHERE ord.order_no   = I_order_no
                  AND ad.KEY_VALUE_1 = TO_CHAR(ord.SUPPLIER)
                  AND ad.addr_type   = REIM_WRAPPER_SQL.ADDR_TYPE_6
                  AND ad.module      = REIM_WRAPPER_SQL.MODULE_SUPP
                  AND ord.SUPPLIER   = TO_NUMBER(I_vendor_id)
              )
              ) addr_ord;
        LOOP
           FETCH c_site_id INTO O_oracle_vendor_site_id;
           exit when c_site_id%notfound;
        END LOOP;
       CLOSE c_site_id;

      END IF;

  
/*
   IF ORG_UNIT_SQL.GET_LOC_VENDOR_SITE_ASSOC(O_error_message,
                                             O_oracle_vendor_site_id,
                                             I_vendor_id,
                                             I_location) THEN
      RETURN 1;
   ELSE
      RETURN 0;
   END IF;
*/

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
     O_error_message := SQLERRM || ' from REIM_WRAPPER_SQL.GET_ORACLE_VENDOR_SITE_ID.' 
                                || ' Location - > ' || I_location 
                                || ' Supplier -> '|| I_vendor_id 
                                || ' Order # -> ' ||I_order_no 
                                || ' Rtv indicator - > ' || I_rtv_ind;
     RETURN 0;
END GET_ORACLE_VENDOR_SITE_ID;
-----------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER_SITE_ID(O_error_message    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'REIM_WRAPPER_SQL.UPDATE_SUPPLIER_SITE_ID';

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   merge into im_doc_head tgt
   using (select idh.doc_id,
                 pou.partner,
					       RANK() OVER (PARTITION BY idh.doc_id
					                        ORDER BY NVL(pou.primary_pay_site, 'N') desc,
										             DECODE(a.key_value_1,
  												            NULL, 1,
														    0),
										             pou.partner) site_rnk
            from im_doc_head idh,
				 sups s,
				 (select store location,
                         REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
						 org_unit_id
                    from store
                  union all
                  select phy.wh location,
                         REIM_CONSTANTS.LOC_TYPE_WH loc_type,
					     pri_vwh.org_unit_id
                    from wh phy,
                         wh pri_vwh
                   where phy.wh     = phy.physical_wh
                     and pri_vwh.wh = phy.primary_vwh) locs,
			     partner_org_unit pou,
				 addr a
		   where idh.supplier_site_id is NULL
			 and idh.vendor_type      = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
			 and s.supplier_parent    = idh.vendor
			 and locs.location        = idh.location
			 and locs.loc_type        = idh.loc_type
			 and pou.partner          = s.supplier
			 and pou.org_unit_id      = locs.org_unit_id
			 and a.key_value_1 (+)    = TO_CHAR(pou.partner)
			 and a.addr_type   (+)    = REIM_CONSTANTS.ADDR_TYPE_REMITT
			 and a.module      (+)    = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER) src
   on (    tgt.doc_id   = src.doc_id
       and src.site_rnk = 1)
   when MATCHED then
      update set tgt.supplier_site_id = src.partner;

   LOGGER.LOG_INFORMATION(L_program||'Updated Supplier Site Ids on IM_DOC_HEAD - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

  when OTHERS then
     O_error_message := SQLERRM || ' from REIM_WRAPPER_SQL.UPDATE_INVOICE_MATCH_STATUS.';
     return REIM_CONSTANTS.FAIL;

END UPDATE_SUPPLIER_SITE_ID;
-----------------------------------------------------------------------
END REIM_WRAPPER_SQL;
/