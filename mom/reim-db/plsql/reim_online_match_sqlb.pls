CREATE OR REPLACE PACKAGE BODY REIM_ONLINE_MATCH_SQL AS
----------------------------------------------------------------------------------------------

LP_expand_group_level NUMBER(4) := 3;

----------------------------------------------------------------------------------------------
/**
 *  -------- PRIVATE PROCS AND FUNCTIONS SPECS --------------
 */
----------------------------------------------------------------------------------------------
FUNCTION EXPAND_INIT_GROUP(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                           I_selected_curr IN     IM_MATCH_INVC_WS.CURRENCY_CODE%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION EXPAND_DETAIL_SELECT(O_error_message    OUT VARCHAR2,
                              I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION EXPAND_REFRESH_MTCH_WSPACE(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to Create/update/delete manual groups.
 * Used for Online Resolutions.
 */
FUNCTION MANAGE_MANUAL_GROUP(O_error_message     IN OUT VARCHAR2,
                             I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The private function used to
 *              a. Check for dirty records in the match workspace
 *              b. Check for DB locks on OPS tables.
 */
FUNCTION DIRTY_LOCK_CHECKS(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION ROLLUP_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION REFRESH_DISCREPANCY_LIST_WS(O_error_message    OUT VARCHAR2,
                                     I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION POP_ORDLOC_UNIT_COST(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION PRORATE_STYLE_LVL_QTY_VAR(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION ROLLUP_STYLE_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                      I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_DEALS_EXIST(O_error_message    OUT VARCHAR2,
                            I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 *  --------- PUBLIC PROCS AND FUNCTIONS BODIES --------------
 */
----------------------------------------------------------------------------------------------
/**
 * The public function used for Initializing Match Data for Online matching.
 * Output param: O_match_wspace_id (The ID of the Match workspace, new one if I_match_wspace_id is not passed in)
 *
 * Input param: I_search_wspace_id (The ID of the Search workspace)
 *              I_match_wspace_id (The ID of the Match workspace, if search is performed on the match screen)
 *
 * Returns 1 on Success
 *         0 on Failure
 */

FUNCTION INIT_ONLINE_MATCH_DATA(O_error_message         OUT VARCHAR2,
                                O_match_wspace_id       OUT IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                                I_search_wspace_id   IN     IM_MATCH_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                                I_match_wspace_id    IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE DEFAULT NULL,
                                I_client_id          IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'REIM_ONLINE_MATCH_SQL.INIT_ONLINE_MATCH_DATA';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_vdate                 PERIOD.VDATE%TYPE                   := GET_VDATE;
   L_choice_flag           VARCHAR2(1)                         := 'Y';
   L_selected_curr         IM_MATCH_INVC_WS.CURRENCY_CODE%TYPE := NULL;
   L_supp_grp_count        NUMBER                              := REIM_CONSTANTS.ONE;
   L_loc_sob_count         NUMBER                              := REIM_CONSTANTS.ONE;
   L_loc_vat_region_count  NUMBER                              := REIM_CONSTANTS.ONE;

   multi_supp        EXCEPTION;
   multi_loc_sob     EXCEPTION;
   multi_loc_vat_reg EXCEPTION;

   cursor C_FETCH_SELECTED_CURR is
      select MAX(currency_code)
        from im_match_invc_ws imiw
       where imiw.workspace_id = I_match_wspace_id
         and imiw.choice_flag  = 'Y';

   cursor C_CHK_VALID_MTCH_GROUP is
      select count(distinct inner.supp_group_id),
             count(distinct inner.loc_set_of_books_id) loc_sob_count,
             count(distinct inner.loc_vat_region) loc_vat_region_count
        from (select NVL(imiw.supplier_group_id, imiw.supplier) supp_group_id,
                     NVL(imiw.loc_set_of_books_id, -1) loc_set_of_books_id,
                     NVL(imiw.loc_vat_region, -1) loc_vat_region
                from im_match_invc_ws imiw
               where imiw.workspace_id = O_match_wspace_id
                 and imiw.choice_flag  = 'Y'
                 and imiw.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH
              union all
              select NVL(isgm.group_id, imrw.supplier) supp_group_id,
                     NVL(imrw.loc_set_of_books_id, -1) loc_set_of_books_id,
                     NVL(imrw.loc_vat_region, -1) loc_vat_region
                from im_match_rcpt_ws imrw,
                     im_supplier_group_members isgm
               where imrw.workspace_id      = O_match_wspace_id
                 and imrw.choice_flag       = 'Y'
                 and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                 and isgm.supplier (+)      = imrw.supplier) inner;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if I_match_wspace_id is NULL then
      O_match_wspace_id := IM_WORKSPACE_ID_SEQ.NEXTVAL;
   else
      O_match_wspace_id := I_match_wspace_id;

      open C_FETCH_SELECTED_CURR;
      fetch C_FETCH_SELECTED_CURR into L_selected_curr;
      close C_FETCH_SELECTED_CURR;
   end if;

   merge into im_match_invc_ws tgt
   using (select O_match_wspace_id workspace_id,
                 imisw.doc_id,
                 imisw.ext_doc_id,
                 imisw.doc_head_version_id,
                 imisw.status,
                 imisw.doc_date,
                 imisw.due_date,
                 imisw.supplier_group_id,
                 imisw.supplier,
                 imisw.supplier_name,
                 imisw.supplier_phone,
                 imisw.supplier_site_id,
                 imisw.supplier_site_name,
                 imisw.currency_code,
                 imisw.order_no,
                 oh.supplier po_supplier_site_id,
                 imisw.location,
                 imisw.loc_type,
                 imisw.location_name,
                 imisw.total_cost total_avail_cost, --merge later
                 imisw.total_qty total_avail_qty, --merge later
                 imisw.total_cost merch_amount, -- merge later
                 imisw.total_qty,
                 imisw.cost_pre_match,
                 'Y' header_only, --merge later
                 'Y' summary_mtch_elig, --merge later
                 'Y' detail_mtch_elig, --merge later
--                 DECODE(idh.header_only,
--                        'Y', 'N',
--                        'Y') detail_mtch_elig,
                 NVL(so.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                 NVL(so.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                 NVL(so.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                 imgi.group_id manual_group_id,
                 DECODE(I_match_wspace_id,
                        NULL, imisw.choice_flag,
                        DECODE(L_selected_curr,
                               imisw.currency_code, imisw.choice_flag,
                               'N')) choice_flag,
                 imisw.loc_set_of_books_id,
                 imisw.loc_vat_region,
                 imisw.supplier_site_vat_region,
                 imisw.match_key_id,
                 imisw.pre_paid_ind,
                 imisw.custom_doc_ref_1,
                 imisw.custom_doc_ref_2,
                 imisw.custom_doc_ref_3,
                 imisw.custom_doc_ref_4
            from im_match_invc_search_ws imisw,
                 v_im_supp_site_attrib_expl so,
                 im_manual_group_invoices imgi,
                  ordhead oh
           where imisw.workspace_id  = I_search_wspace_id
             and imisw.choice_flag   = 'Y'
             and so.supplier         = imisw.supplier_site_id
             and imgi.doc_id(+)      = imisw.doc_id
             and imisw.order_no      = oh.order_no) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when NOT MATCHED THEN
      insert (workspace_id,
              doc_id,
              ext_doc_id,
              doc_head_version_id,
              status,
              doc_date,
              due_date,
              supplier_group_id,
              supplier,
              supplier_name,
              supplier_phone,
              supplier_site_id,
              supplier_site_name,
              currency_code,
              order_no,
              po_supplier_site_id,
              location,
              loc_type,
              location_name,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              cost_pre_match,
              header_only,
              summary_mtch_elig,
              detail_mtch_elig,
              qty_required,
              qty_match_required,
              sku_comp_percent,
              manual_group_id,
              choice_flag,
              loc_set_of_books_id,
              loc_vat_region,
              supplier_site_vat_region,
              match_key_id,
              chunk_num,
              match_luw_id,
              pre_paid_ind,
              custom_doc_ref_1,
              custom_doc_ref_2,
              custom_doc_ref_3,
              custom_doc_ref_4)
      values (src.workspace_id,
              src.doc_id,
              src.ext_doc_id,
              src.doc_head_version_id,
              src.status,
              src.doc_date,
              src.due_date,
              src.supplier_group_id,
              src.supplier,
              src.supplier_name,
              src.supplier_phone,
              src.supplier_site_id,
              src.supplier_site_name,
              src.currency_code,
              src.order_no,
              src.po_supplier_site_id,
              src.location,
              src.loc_type,
              src.location_name,
              src.total_avail_cost, --merge later
              src.total_avail_qty, --merge later
              src.merch_amount, -- merge later
              src.total_qty, -- merge later
              src.cost_pre_match,
              src.header_only, -- header_only, merge later
              src.summary_mtch_elig, -- summary_mtch_elig, merge later
              src.detail_mtch_elig, -- detail_mtch_elig, merge later
              src.qty_required, -- to be confirmed
              src.qty_match_required, -- to be confirmed
              src.sku_comp_percent,
              src.manual_group_id,
              src.choice_flag,
              src.loc_set_of_books_id,
              src.loc_vat_region,
              src.supplier_site_vat_region,
              src.match_key_id,
              REIM_CONSTANTS.ONE,
              REIM_CONSTANTS.ONE,
              src.pre_paid_ind,
              src.custom_doc_ref_1,
              src.custom_doc_ref_2,
              src.custom_doc_ref_3,
              src.custom_doc_ref_4);

   LOGGER.LOG_INFORMATION(L_program||' Insert selected invoices from SEARCH_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_ws tgt
   using (select O_match_wspace_id workspace_id,
                 imrsw.shipment,
                 imrsw.order_no,
                 imrsw.asn,
                 imrsw.status_code,
                 imrsw.invc_match_status,
                 imrsw.location,
                 imrsw.loc_type,
                 imrsw.location_name loc_name,
                 imrsw.bill_to_loc,
                 imrsw.bill_to_loc_type,
                 imrsw.bill_to_loc_name,
                 imrsw.receive_date,
                 imrsw.supplier,
                 imrsw.supplier_name,
                 imrsw.supplier_site_id,
                 imrsw.supplier_site_name,
                 imrsw.currency_code,
                 NVL(imrsw.total_avail_cost, 0) total_avail_cost,
                 NVL(imrsw.total_avail_qty, 0) total_avail_qty,
                 NVL(imrsw.merch_amount, 0) merch_amount,
                 NVL(imrsw.total_qty, 0) total_qty,
                 DECODE(I_match_wspace_id,
                        NULL, imrsw.choice_flag,
                        DECODE(L_selected_curr,
                               imrsw.currency_code, imrsw.choice_flag,
                               'N')) choice_flag,
                 imrsw.loc_set_of_books_id,
                 imrsw.loc_vat_region,
                 imrsw.supplier_site_vat_region,
                 imrsw.match_key_id,
                 imrsw.ship_date
            from im_match_rcpt_search_ws imrsw
           where imrsw.workspace_id = I_search_wspace_id
             and imrsw.choice_flag  = 'Y'
             and imrsw.receive_date is NOT NULL) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment)
   when NOT MATCHED THEN
      insert (workspace_id,
              shipment,
              supplier,
              supplier_name,
              supplier_site_id,
              supplier_site_name,
              order_no,
              asn,
              bill_to_loc,
              bill_to_loc_type,
              bill_to_loc_name,
              ship_to_loc,
              ship_to_loc_type,
              ship_to_loc_name,
              status_code,
              invc_match_status,
              receive_date,
              currency_code,
              total_avail_cost,
              total_avail_qty,
              merch_amount,
              total_qty,
              choice_flag,
              loc_set_of_books_id,
              loc_vat_region,
              supplier_site_vat_region,
              match_key_id,
              chunk_num,
              match_luw_id,
              ship_date)
      values (src.workspace_id,
              src.shipment,
              src.supplier,
              src.supplier_name,
              src.supplier_site_id,
              src.supplier_site_name,
              src.order_no,
              src.asn,
              src.bill_to_loc,
              src.bill_to_loc_type,
              src.bill_to_loc_name,
              src.location,
              src.loc_type,
              src.loc_name,
              src.status_code,
              src.invc_match_status,
              src.receive_date,
              src.currency_code,
              src.total_avail_cost, --merge later
              src.total_avail_qty, --merge later
              src.merch_amount, -- merge later
              src.total_qty, -- merge later
              src.choice_flag,
              src.loc_set_of_books_id,
              src.loc_vat_region,
              src.supplier_site_vat_region,
              src.match_key_id,
              REIM_CONSTANTS.ONE,
              REIM_CONSTANTS.ONE,
              src.ship_date);

   LOGGER.LOG_INFORMATION(L_program||'Insert selected reecipts from SEARCH_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if NVL(I_client_id, 'NULL') != 'REIM_SEARCH_SQL.DISCREPANCY_SEARCH' then

      if EXPAND_INIT_GROUP(O_error_message,
                           O_match_wspace_id,
                           L_selected_curr) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

      open C_CHK_VALID_MTCH_GROUP;
      fetch C_CHK_VALID_MTCH_GROUP into L_supp_grp_count,
                                        L_loc_sob_count,
                                        L_loc_vat_region_count;
      close C_CHK_VALID_MTCH_GROUP;

      if L_supp_grp_count > REIM_CONSTANTS.ONE then
         RAISE multi_supp;
      elsif L_loc_sob_count > REIM_CONSTANTS.ONE then
         RAISE multi_loc_sob;
      elsif L_loc_vat_region_count > REIM_CONSTANTS.ONE then
         RAISE multi_loc_vat_reg;
      end if;

   end if; --I_client_id

   merge into im_match_invc_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 SUM(idnm.non_merch_amt) non_merch_amt
            from im_match_invc_ws imiw,
                 im_doc_non_merch idnm
           where imiw.workspace_id = O_match_wspace_id
             and idnm.doc_id       = imiw.doc_id
           GROUP BY imiw.workspace_id,
                    imiw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.merch_amount     = tgt.merch_amount - src.non_merch_amt,
             tgt.total_avail_cost = tgt.total_avail_cost - src.non_merch_amt;

   LOGGER.LOG_INFORMATION(L_program||'Merge INVC_HEAD_WS NON_MERCH_AMT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_match_wspace_id is NULL then

      select case when NVL(I_client_id, 'NULL') = 'REIM_SEARCH_SQL.DISCREPANCY_SEARCH' then
                     'Y'
                  when count(1) > 1 then
                     'N'
                  else
                     'Y'
             end
        into L_choice_flag
      from (select distinct currency_code
              from im_match_invc_ws imiw
             where imiw.workspace_id = O_match_wspace_id
            union
            select distinct currency_code
              from im_match_rcpt_ws imrw
             where imrw.workspace_id = O_match_wspace_id);

      update im_match_invc_ws
         set choice_flag = L_choice_flag
       where workspace_id = O_match_wspace_id;

      LOGGER.LOG_INFORMATION(L_program||'Update Choice Flag on invc_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      update im_match_rcpt_ws
         set choice_flag = L_choice_flag
       where workspace_id = O_match_wspace_id;

      LOGGER.LOG_INFORMATION(L_program||'Update Choice Flag on rcpt_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   merge into im_match_invc_detl_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 iid.item,
                 its.vpn,
                 iid.object_version_id invc_detl_version_id,
                 imiw.match_key_id,
                 imiw.chunk_num,
                 imiw.match_luw_id,
                 iid.status,
                 iid.resolution_adjusted_unit_cost unit_cost,
                 iid.resolution_adjusted_qty invoice_qty,
                 iid.cost_matched,
                 iid.qty_matched,
                 imiw.choice_flag
      from im_match_invc_ws imiw,
           im_invoice_detail iid,
           item_supplier its
     where imiw.workspace_id = O_match_wspace_id
       and iid.doc_id        = imiw.doc_id
       and its.item (+)      = iid.item
       and its.supplier (+)  = imiw.supplier_site_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              item,
              vpn,
              invc_detl_version_id,
              match_key_id,
              chunk_num,
              match_luw_id,
              status,
              unit_cost,
              invoice_qty,
              cost_matched,
              qty_matched,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.item,
              src.vpn,
              src.invc_detl_version_id,
              src.match_key_id,
              src.chunk_num,
              src.match_luw_id,
              src.status,
              src.unit_cost,
              src.invoice_qty,
              src.cost_matched,
              src.qty_matched,
              src.choice_flag);

   LOGGER.LOG_INFORMATION(L_program||'Insert INVC_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_ws tgt
   using (select imidw.workspace_id,
                 imidw.doc_id,
                 SUM(DECODE(imidw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imidw.invoice_qty,
                            REIM_CONSTANTS.ZERO) * imidw.unit_cost) unmatch_item_amt,
                 SUM(DECODE(imidw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imidw.invoice_qty,
                            REIM_CONSTANTS.ZERO)) unmatch_item_qty
            from im_match_invc_detl_ws imidw
           where imidw.workspace_id  = O_match_wspace_id
           GROUP BY imidw.workspace_id,
                    imidw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.unmatch_item_amt,
             tgt.total_avail_qty  = src.unmatch_item_qty,
             tgt.header_only      = 'N';

   LOGGER.LOG_INFORMATION(L_program||'Merge INVC_HEAD_WS AVAILABLE COST AND QTY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 'N' summary_mtch_elig
            from im_match_invc_ws imiw
           where imiw.workspace_id = O_match_wspace_id
             and EXISTS (select 'x'
                           from im_invoice_detail iid
                          where iid.doc_id            = imiw.doc_id
                            and iid.status            = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                            and DECODE(iid.cost_matched,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES) <> DECODE(iid.qty_matched,
                                                                                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD,
                                                                                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES))) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.summary_mtch_elig = src.summary_mtch_elig;

   LOGGER.LOG_INFORMATION(L_program||'Merge INVC_HEAD_WS SUMMARY_MTCH_ELIG - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 case
                    when imiw.header_only = 'Y' then
                       'N'
                    else
                       'Y'
                 end detail_mtch_elig
            from im_match_invc_ws imiw,
                 im_system_options sys_opt,
                 im_supplier_options supp_opt
           where imiw.workspace_id = O_match_wspace_id
             and supp_opt.supplier = imiw.supplier) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.detail_mtch_elig = src.detail_mtch_elig;

   LOGGER.LOG_INFORMATION(L_program||'Merge INVC_HEAD_WS DETAIL_MTCH_ELIG - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_transform_shipsku_gtt;

   insert into im_transform_shipsku_gtt (shipment,
                                         item,
                                         vpn,
                                         seq_no,
                                         ss_qty_received,
                                         ss_qty_matched,
                                         ss_unit_cost,
                                         weight_received,
                                         weight_received_uom,
                                         carton,
                                         catch_weight_type,
                                         order_no,
                                         supplier,
                                         sup_qty_level,
                                         transform_qty_received,
                                         transform_qty_matched,
                                         transform_unit_cost)
                                  select imrw.shipment,
                                         ss.item,
                                         its.vpn,
                                         ss.seq_no,
                                         ss.qty_received,
                                         ss.qty_matched,
                                         ss.unit_cost,
                                         ss.weight_received,
                                         ss.weight_received_uom,
                                         ss.carton,
                                         im.catch_weight_type,
                                         imrw.order_no,
                                         imrw.supplier_site_id,
                                         s.sup_qty_level,
                                         ss.qty_received,
                                         ss.qty_matched,
                                         ss.unit_cost
                                    from im_match_rcpt_ws imrw,
                                         shipsku ss,
                                         sups s,
                                         item_master im,
                                         item_supplier its
                                   where imrw.workspace_id = O_match_wspace_id
                                     and ss.shipment       = imrw.shipment
                                     and s.supplier        = imrw.supplier_site_id
                                     and im.item           = ss.item
                                     and its.item          = ss.item
                                     and its.supplier      = imrw.supplier_site_id
                                     and NOT EXISTS (select 'x'
                                                       from im_match_rcpt_detl_ws imrdw
                                                      where imrdw.workspace_id = O_match_wspace_id
                                                        and imrdw.shipment     = imrw.shipment);

   LOGGER.LOG_INFORMATION(L_program||'Insert Shipsku xform gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_XFORM_SHIPSKU_SQL.TRANSFORM_SHIPSKU_GTT(O_error_message) = REIM_CONSTANTS.FAIL then
       return REIM_CONSTANTS.FAIL;
   end if;

   insert into im_match_rcpt_detl_ws(workspace_id,
                                     shipment,
                                     item,
                                     vpn,
                                     invc_match_status,
                                     unit_cost,
                                     qty_received,
                                     qty_available,
                                     unit_cost_nc,
                                     qty_available_nc,
                                     catch_weight_type,
                                     choice_flag,
                                     match_luw_id,
                                     chunk_num,
                                     match_key_id)
        with im_xform_shipsku_gtt as (select gtt.shipment,
                                             gtt.item,
                                             gtt.vpn,
                                             case
                                                when SUM(NVL(gtt.ss_qty_received, 0) - NVL(gtt.ss_qty_matched, 0)) <> REIM_CONSTANTS.ZERO then
                                                   REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
                                                else
                                                   REIM_CONSTANTS.SSKU_IM_STATUS_MTCH
                                             end invc_match_status,
                                             gtt.transform_unit_cost,
                                             SUM(NVL(gtt.transform_qty_received, 0)) qty_received,
                                             SUM(NVL(gtt.transform_qty_received, 0) - NVL(ipmr.qty_matched, 0)) qty_available,
                                             gtt.ss_unit_cost unit_cost_nc,
                                             SUM(NVL(gtt.ss_qty_received, 0) - NVL(gtt.ss_qty_matched, 0)) qty_available_nc,
                                             gtt.catch_weight_type
                                        from im_transform_shipsku_gtt gtt,
                                             im_partially_matched_receipts ipmr
                                       where ipmr.shipment (+) = gtt.shipment
                                         and ipmr.item (+)     = gtt.item
                                       GROUP BY gtt.shipment,
                                                gtt.item,
                                                gtt.vpn,
                                                gtt.transform_unit_cost,
                                                gtt.ss_unit_cost,
                                                gtt.catch_weight_type)
                              select imrw.workspace_id,
                                     imrw.shipment,
                                     gtt.item,
                                     gtt.vpn,
                                     gtt.invc_match_status,
                                     gtt.transform_unit_cost,
                                     gtt.qty_received,
                                     gtt.qty_available,
                                     gtt.unit_cost_nc,
                                     gtt.qty_available_nc,
                                     gtt.catch_weight_type,
                                     imrw.choice_flag,
                                     imrw.match_luw_id,
                                     imrw.chunk_num,
                                     imrw.match_key_id
                                from im_match_rcpt_ws imrw,
                                     im_xform_shipsku_gtt gtt
                               where imrw.workspace_id = O_match_wspace_id
                                 and gtt.shipment      = imrw.shipment;

   LOGGER.LOG_INFORMATION(L_program||'Insert im_match_rcpt_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_ws tgt
   using (select imrdw.workspace_id,
                 imrdw.shipment,
                 SUM(imrdw.unit_cost * imrdw.qty_available) total_avail_cost,
                 SUM(imrdw.qty_available) total_avail_qty,
                 SUM(imrdw.unit_cost * imrdw.qty_received) merch_amount,
                 SUM(imrdw.qty_received) total_qty
            from im_match_rcpt_detl_ws imrdw
           where imrdw.workspace_id = O_match_wspace_id
           GROUP BY imrdw.workspace_id,
                    imrdw.shipment) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.total_avail_cost,
             tgt.total_avail_qty  = src.total_avail_qty,
             tgt.merch_amount     = src.merch_amount,
             tgt.total_qty        = src.total_qty;

   LOGGER.LOG_INFORMATION(L_program||'Merge cost-qty im_match_rcpt_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_ws tgt
   using (with im_mtch_invc_ws as (select imiw.workspace_id,
                                          case
                                             when count(distinct imiw.order_no) = REIM_CONSTANTS.ONE then
                                                max(oh.dept)
                                             else
                                                NULL
                                          end dept,
                                          case
                                             when count(distinct imiw.supplier_site_id) = REIM_CONSTANTS.ONE then
                                                max(imiw.supplier_site_id)
                                             else
                                                NULL
                                          end supplier_site_id,
                                          case
                                             when count(distinct imiw.supplier) = REIM_CONSTANTS.ONE then
                                                max(imiw.supplier)
                                             else
                                                NULL
                                          end supplier,
                                          case
                                             when count(distinct imiw.supplier_group_id) = REIM_CONSTANTS.ONE then
                                                max(imiw.supplier_group_id)
                                             else
                                                NULL
                                          end supplier_group_id
                                     from im_match_invc_ws imiw,
                                          ordhead oh
                                    where imiw.workspace_id = O_match_wspace_id
                                      and oh.order_no       = imiw.order_no
                                    GROUP BY imiw.workspace_id),
               im_tolerance_ws as (select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SUPP_SITE tolerance_rank
                                     from im_tolerance_level_map
                                    where tolerance_level_type = REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_SITE
                                   union all
                                   select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SUPP tolerance_rank
                                     from im_tolerance_level_map
                                    where tolerance_level_type = REIM_CONSTANTS.TLR_LVL_TYPE_SUPP
                                   union all
                                   select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SUPP_GRP tolerance_rank
                                     from im_tolerance_level_map
                                    where tolerance_level_type = REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_GRP
                                   union all
                                   select tolerance_level_type,
                                          tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_DEPT tolerance_rank
                                     from im_tolerance_level_map
                                    where tolerance_level_type                   = REIM_CONSTANTS.TLR_LVL_TYPE_DEPT
                                   union all
                                   select NULL tolerance_level_type,
                                          NULL tolerance_level_id,
                                          tolerance_id,
                                          REIM_CONSTANTS.TLR_RANK_SYSTEM tolerance_rank
                                     from im_tolerance_head
                                    where system_default = 'Y'),
               im_curr_conv_ws as (select from_currency,
                                          to_currency,
                                          effective_date,
                                          exchange_rate,
                                          exchange_type,
                                          RANK() OVER (PARTITION BY from_currency,
                                                                    to_currency,
                                                                    exchange_type
                                                           ORDER BY effective_date desc) date_rank
                                     from mv_currency_conversion_rates
                                    where exchange_type  = REIM_CONSTANTS.EXCH_TYPE_CONSOLIDATED
                                      and effective_date <= L_vdate)
          select imiw.workspace_id,
                 imiw_ws.doc_id,
                 itw.tolerance_id,
                 NVL(itw.tolerance_level_type, REIM_CONSTANTS.TLR_LVL_TYPE_SYSTEM) tolerance_level_type,
                 itw.tolerance_level_id,
                 RANK() OVER (PARTITION BY imiw.workspace_id
                                  ORDER BY itw.tolerance_rank desc) tolerance_rank,
                 ith.currency_code tolerance_currency_code,
                 iccw.exchange_rate tolerance_exchange_rate
            from im_mtch_invc_ws imiw,
                 im_tolerance_ws itw,
                 im_tolerance_head ith,
                 im_match_invc_ws imiw_ws,
                 im_curr_conv_ws iccw
           where ((   imiw.dept              = Decode(itw.tolerance_level_type,
                                                     REIM_CONSTANTS.TLR_LVL_TYPE_DEPT, itw.tolerance_level_id,
                                                     NULL)
                  or imiw.supplier_site_id  = Decode(itw.tolerance_level_type,
                                                     REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_SITE, itw.tolerance_level_id,
                                                     NULL)
                  or imiw.supplier          = Decode(itw.tolerance_level_type,
                                                     REIM_CONSTANTS.TLR_LVL_TYPE_SUPP, itw.tolerance_level_id,
                                                     NULL)
                  or imiw.supplier_group_id = Decode(itw.tolerance_level_type,
                                                     REIM_CONSTANTS.TLR_LVL_TYPE_SUPP_GRP, itw.tolerance_level_id,
                                                     NULL))
                  or itw.tolerance_rank     = REIM_CONSTANTS.ONE)
              and ith.tolerance_id          = itw.tolerance_id
              and imiw_ws.workspace_id      = imiw.workspace_id
              and iccw.from_currency        = imiw_ws.currency_code
              and iccw.to_currency          = ith.currency_code
              and iccw.date_rank            = REIM_CONSTANTS.ONE) src
   on (    tgt.workspace_id   = src.workspace_id
       and tgt.doc_id         = src.doc_id
       and src.tolerance_rank = REIM_CONSTANTS.ONE)
   when MATCHED then
      update
         set tgt.tolerance_id            = src.tolerance_id,
             tgt.tolerance_level_type    = src.tolerance_level_type,
             tgt.tolerance_level_id      = src.tolerance_level_id,
             tgt.tolerance_currency_code = src.tolerance_currency_code,
             tgt.tolerance_exchange_rate = src.tolerance_exchange_rate;

   LOGGER.LOG_INFORMATION(L_program||'Merge Tolerance im_match_invc_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_detl_ws tgt
   using (select imidw.workspace_id,
                 imidw.doc_id,
                 imidw.item,
                 idmih.match_id match_hist_id
            from im_match_invc_detl_ws imidw,
                 im_detail_match_invc_history idmih
           where imidw.workspace_id = O_match_wspace_id
             and idmih.doc_id       = imidw.doc_id
             and idmih.item         = imidw.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.match_hist_id     = src.match_hist_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge Match Hist ID on INVC_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_detl_ws tgt
   using (select imrdw.workspace_id,
                 imrdw.shipment,
                 imrdw.item,
                 MAX(idmrh.match_id) match_hist_id,
                 COUNT(1) match_count
            from im_match_rcpt_detl_ws imrdw,
                 im_detail_match_rcpt_history idmrh
           where imrdw.workspace_id = O_match_wspace_id
             and idmrh.shipment     = imrdw.shipment
             and idmrh.item         = NVL(imrdw.substitute_item, imrdw.item)
           GROUP BY imrdw.workspace_id,
                    imrdw.shipment,
                    imrdw.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment
       and tgt.item         = src.item
       and src.match_count  = REIM_CONSTANTS.ONE)
   when MATCHED then
      update
         set tgt.match_hist_id     = src.match_hist_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge Match Hist ID on RCPT_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if POP_ORDLOC_UNIT_COST(O_error_message,
                           O_match_wspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if UPDATE_DEALS_EXIST(O_error_message,
                         O_match_wspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when multi_supp then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_SUPP_GROUP', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when multi_loc_sob then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_LOC_SOB', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when multi_loc_vat_reg then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_LOC_VAT_REGION', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END INIT_ONLINE_MATCH_DATA;
----------------------------------------------------------------------------------------------
FUNCTION EXPAND_INIT_GROUP(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                           I_selected_curr IN     IM_MATCH_INVC_WS.CURRENCY_CODE%TYPE)
RETURN NUMBER IS

   L_program    VARCHAR2(60) := 'REIM_ONLINE_MATCH_SQL.EXPAND_INIT_GROUP';
   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   for x in 1..LP_expand_group_level loop

      delete from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date(number_1,
                                                number_2)
         select distinct imiw.manual_group_id,
                imiw.loc_set_of_books_id
           from im_match_invc_ws imiw
          where imiw.workspace_id    = I_workspace_id
            and imiw.manual_group_id is NOT NULL
         union
         select distinct gr.group_id manual_group_id,
                imrw.loc_set_of_books_id
           from im_match_rcpt_ws imrw,
                im_manual_group_receipts gr
          where imrw.workspace_id = I_workspace_id
            and imrw.shipment     = gr.shipment;

      merge into im_match_invc_ws tgt
      using (select I_workspace_id workspace_id,
                    idh.doc_id,
                    idh.ext_doc_id,
                    idh.object_version_id doc_head_version_id,
                    idh.status,
                    idh.doc_date,
                    idh.due_date,
                    iso.group_id supplier_group_id,
                    idh.vendor supplier,
                    sups_parent.sup_name supplier_name,
                    sups_parent.contact_phone supplier_phone,
                    idh.supplier_site_id,
                    sups.sup_name supplier_site_name,
                    idh.currency_code,
                    idh.order_no,
                    oh.supplier po_supplier_site_id,
                    idh.location,
                    idh.loc_type,
                    loc.loc_name,
                    idh.resolution_adjusted_total_cost total_avail_cost, --merge later
                    idh.resolution_adjusted_total_qty total_avail_qty, --merge later
                    idh.resolution_adjusted_total_cost merch_amount, -- merge later
                    idh.resolution_adjusted_total_qty total_qty,
                    idh.cost_pre_match,
                    'Y' header_only, -- header_only, merge later
                    'Y' summary_mtch_elig, -- summary_mtch_elig, merge later
                    'Y' detail_mtch_elig, -- detail_mtch_elig, merge later
                    NVL(iso.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                    NVL(iso.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                    NVL(iso.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                    imgi.group_id manual_group_id,
                    DECODE(I_workspace_id,
                           NULL, 'Y',
                           DECODE(I_selected_curr,
                                  idh.currency_code, 'Y',
                                  'N')) choice_flag,
                    NVL(gtt.number_2, -1) loc_set_of_books_id,
                    loc.vat_region loc_vat_region,
                    sups.vat_region supplier_site_vat_region,
                    idh.pre_paid_ind,
                    idh.custom_doc_ref_1,
                    idh.custom_doc_ref_2,
                    idh.custom_doc_ref_3,
                    idh.custom_doc_ref_4
               from gtt_num_num_str_str_date_date gtt,
                    im_manual_group_invoices imgi,
                    im_doc_head idh,
                    ordhead oh,
                    v_im_supp_site_attrib_expl iso,
                    --
                    (select to_char(s.supplier) supplier, v_tl.sup_name,
                            s.contact_phone, s.supplier_parent, s.vat_region
                       from sups s,
                            v_sups_tl v_tl
                      where v_tl.supplier = s.supplier
                    ) sups_parent,
                    --
                    (select to_char(s.supplier) supplier, v_tl.sup_name,
                            s.contact_phone, s.supplier_parent, s.vat_region
                       from sups s,
                            v_sups_tl v_tl
                      where v_tl.supplier = s.supplier
                    ) sups,
                    --
                    (select s.store loc,
                            REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                            v_tl.store_name loc_name,
                            s.vat_region
                       from store s,
                            v_store_tl v_tl
                      where v_tl.store = s.store
                     union all
                     select w.wh loc,
                            REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                            v_tl.wh_name loc_name,
                            w.vat_region
                       from wh w,
                            v_wh_tl v_tl
                      where w.wh = w.physical_wh
                        and w.wh = v_tl.wh) loc
              where imgi.group_id        = gtt.number_1
                and idh.doc_id           = imgi.doc_id
                and oh.order_no          = idh.order_no
                and iso.supplier         = idh.supplier_site_id
                and sups_parent.supplier = idh.vendor
                and sups.supplier        = idh.supplier_site_id
                and loc.loc              = idh.location
                and loc.loc_type         = idh.loc_type) src
      on (    tgt.workspace_id = src.workspace_id
          and tgt.doc_id       = src.doc_id)
      when NOT MATCHED THEN
         insert (workspace_id,
                 doc_id,
                 ext_doc_id,
                 doc_head_version_id,
                 status,
                 doc_date,
                 due_date,
                 supplier_group_id,
                 supplier,
                 supplier_name,
                 supplier_phone,
                 supplier_site_id,
                 supplier_site_name,
                 currency_code,
                 order_no,
                 po_supplier_site_id,
                 location,
                 loc_type,
                 location_name,
                 total_avail_cost,
                 total_avail_qty,
                 merch_amount,
                 total_qty,
                 cost_pre_match,
                 header_only,
                 summary_mtch_elig,
                 detail_mtch_elig,
                 qty_required,
                 qty_match_required,
                 sku_comp_percent,
                 manual_group_id,
                 choice_flag,
                 loc_set_of_books_id,
                 loc_vat_region,
                 supplier_site_vat_region,
                 chunk_num,
                 match_luw_id,
                 pre_paid_ind,
                 custom_doc_ref_1,
                 custom_doc_ref_2,
                 custom_doc_ref_3,
                 custom_doc_ref_4)
         values (src.workspace_id,
                 src.doc_id,
                 src.ext_doc_id,
                 src.doc_head_version_id,
                 src.status,
                 src.doc_date,
                 src.due_date,
                 src.supplier_group_id,
                 src.supplier,
                 src.supplier_name,
                 src.supplier_phone,
                 src.supplier_site_id,
                 src.supplier_site_name,
                 src.currency_code,
                 src.order_no,
                 src.po_supplier_site_id,
                 src.location,
                 src.loc_type,
                 src.loc_name,
                 src.total_avail_cost, --merge later
                 src.total_avail_qty, --merge later
                 src.merch_amount, -- merge later
                 src.total_qty,
                 src.cost_pre_match,
                 src.header_only, -- header_only, merge later
                 src.summary_mtch_elig, -- summary_mtch_elig, merge later
                 src.detail_mtch_elig, -- detail_mtch_elig, merge later
                 src.qty_required,
                 src.qty_match_required,
                 src.sku_comp_percent,
                 src.manual_group_id,
                 src.choice_flag,
                 src.loc_set_of_books_id,
                 src.loc_vat_region,
                 src.supplier_site_vat_region,
                 REIM_CONSTANTS.ONE,
                 REIM_CONSTANTS.ONE,
                 src.pre_paid_ind,
                 src.custom_doc_ref_1,
                 src.custom_doc_ref_2,
                 src.custom_doc_ref_3,
                 src.custom_doc_ref_4);

      LOGGER.LOG_INFORMATION(L_program||'Insert invoices from MANUAL GROUPS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into im_match_rcpt_ws tgt
      using (with loc as (select s.store loc,
                                 REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                 v_tl.store_name loc_name,
                                 s.vat_region
                            from store s,
                                 v_store_tl v_tl
                           where v_tl.store = s.store
                          union all
                          select w.wh loc,
                                 REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                 v_tl.wh_name loc_name,
                                 w.vat_region
                            from wh w,
                                 v_wh_tl v_tl
                           where w.wh = w.physical_wh
                             and w.wh = v_tl.wh)
             select distinct I_workspace_id workspace_id,
                    sh.shipment,
                    oh.order_no,
                    sh.asn,
                    sh.status_code,
                    sh.invc_match_status,
                    sh.to_loc location,
                    sh.to_loc_type loc_type,
                    ship_to_loc.loc_name loc_name,
                    sh.bill_to_loc,
                    sh.bill_to_loc_type,
                    bill_to_loc.loc_name bill_to_loc_name,
                    sh.ship_date,
                    sh.receive_date,
                    sups_parent.supplier,
                    sups_parent.sup_name supplier_name,
                    oh.supplier supplier_site_id,
                    sups.sup_name supplier_site_name,
                    oh.currency_code,
                    0 total_avail_cost, --merge later
                    0 total_avail_qty, --merge later
                    0 merch_amount, --merge later
                    0 total_qty, --merge later
                    DECODE(I_workspace_id,
                           NULL, 'Y',
                           DECODE(I_selected_curr,
                                  oh.currency_code, 'Y',
                                  'N')) choice_flag,
                 gtt.number_2 loc_set_of_books_id,
                 bill_to_loc.vat_region loc_vat_region,
                 sups.vat_region supplier_site_vat_region
               from gtt_num_num_str_str_date_date gtt,
                    im_manual_group_receipts imgr,
                    shipment sh,
                    ordhead oh,
                    --
                    (select to_char(s.supplier) supplier, v_tl.sup_name,
                            s.contact_phone, s.supplier_parent, s.vat_region
                       from sups s,
                            v_sups_tl v_tl
                      where v_tl.supplier = s.supplier
                    ) sups_parent,
                    --
                    (select to_char(s.supplier) supplier, v_tl.sup_name,
                            s.contact_phone, s.supplier_parent, s.vat_region
                       from sups s,
                            v_sups_tl v_tl
                      where v_tl.supplier = s.supplier
                    ) sups,
                    --end
                    loc ship_to_loc,
                    loc bill_to_loc
              where imgr.group_id        = gtt.number_1
                and sh.shipment          = imgr.shipment
                and sh.receive_date      is NOT NULL
                and oh.order_no          = sh.order_no
                and sups.supplier        = oh.supplier
                and sups_parent.supplier = sups.supplier_parent
                and ship_to_loc.loc      = sh.to_loc
                and ship_to_loc.loc_type = sh.to_loc_type
                and bill_to_loc.loc      = sh.bill_to_loc
                and bill_to_loc.loc_type = sh.bill_to_loc_type) src
      on (    tgt.workspace_id = src.workspace_id
          and tgt.shipment     = src.shipment)
      when NOT MATCHED THEN
         insert (workspace_id,
                 shipment,
                 supplier,
                 supplier_name,
                 supplier_site_id,
                 supplier_site_name,
                 order_no,
                 asn,
                 bill_to_loc,
                 bill_to_loc_type,
                 bill_to_loc_name,
                 ship_to_loc,
                 ship_to_loc_type,
                 ship_to_loc_name,
                 status_code,
                 invc_match_status,
                 ship_date,
                 receive_date,
                 currency_code,
                 total_avail_cost,
                 total_avail_qty,
                 merch_amount,
                 total_qty,
                 choice_flag,
                 loc_set_of_books_id,
                 loc_vat_region,
                 supplier_site_vat_region,
                 chunk_num,
                 match_luw_id)
         values (src.workspace_id,
                 src.shipment,
                 src.supplier,
                 src.supplier_name,
                 src.supplier_site_id,
                 src.supplier_site_name,
                 src.order_no,
                 src.asn,
                 src.bill_to_loc,
                 src.bill_to_loc_type,
                 src.bill_to_loc_name,
                 src.location,
                 src.loc_type,
                 src.loc_name,
                 src.status_code,
                 src.invc_match_status,
                 src.ship_date,
                 src.receive_date,
                 src.currency_code,
                 src.total_avail_cost, --merge later
                 src.total_avail_qty, --merge later
                 src.merch_amount, -- merge later
                 src.total_qty,
                 src.choice_flag,
                 src.loc_set_of_books_id,
                 src.loc_vat_region,
                 src.supplier_site_vat_region,
                 REIM_CONSTANTS.ONE,
                 REIM_CONSTANTS.ONE);

      LOGGER.LOG_INFORMATION(L_program||'Insert receipts from Manual Groups - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end loop;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END EXPAND_INIT_GROUP;
----------------------------------------------------------------------------------------------
/**
 * The public function used to perform Summary Match from the UI
 * Input param: I_workspace_id (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_SMRY_MATCH_ONLINE(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                                   I_skip_sku_comp IN     VARCHAR2)
RETURN NUMBER
IS

   L_program VARCHAR2(60) := 'REIM_ONLINE_MATCH_SQL.PERFORM_SMRY_MATCH_ONLINE';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_vdate   DATE         := get_vdate;

   L_match_key_id   IM_MATCH_INVC_WS.MATCH_KEY_ID%TYPE         := im_match_key_id_seq.NEXTVAL;
   L_match_group_id IM_MATCH_GROUP_HEAD_WS.MATCH_GROUP_ID%TYPE := im_match_group_seq.NEXTVAL;

   L_hdr_only_doc_count NUMBER       := REIM_CONSTANTS.ZERO;
   L_sku_compliant      NUMBER       := REIM_CONSTANTS.ONE;
   L_tax_compliant      NUMBER       := REIM_CONSTANTS.ONE;

   L_curr_code_count       NUMBER       := REIM_CONSTANTS.ONE;
   L_loc_sob_count         NUMBER       := REIM_CONSTANTS.ONE;
   L_loc_vat_region_count  NUMBER       := REIM_CONSTANTS.ONE;

   multi_curr_code   EXCEPTION;
   multi_loc_sob     EXCEPTION;
   multi_loc_vat_reg EXCEPTION;

   sku_compliance EXCEPTION;
   tax_compliance EXCEPTION;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:'      || I_workspace_id);

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   update im_match_invc_ws imiw
      set imiw.match_key_id = L_match_key_id
    where imiw.workspace_id = I_workspace_id
      and imiw.choice_flag  = 'Y'
      and imiw.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH;

   LOGGER.LOG_INFORMATION(L_program||'Update IM_MATCH_INVC_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   update im_match_rcpt_ws imrw
      set imrw.match_key_id      = L_match_key_id
    where imrw.workspace_id      = I_workspace_id
      and imrw.choice_flag       = 'Y'
      and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH;

   LOGGER.LOG_INFORMATION(L_program||'Update IM_MATCH_RCPT_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(distinct inner.currency_code) curr_count,
          COUNT(distinct inner.loc_set_of_books_id) loc_sob_count,
          COUNT(distinct inner.loc_vat_region) loc_vat_region_count
     into L_curr_code_count,
          L_loc_sob_count,
          L_loc_vat_region_count
     from (select imiw.match_key_id,
                  imiw.currency_code,
                  NVL(imiw.loc_set_of_books_id, -1) loc_set_of_books_id,
                  NVL(imiw.loc_vat_region, -1) loc_vat_region
             from im_match_invc_ws imiw
            where imiw.workspace_id = I_workspace_id
              and imiw.match_key_id = L_match_key_id
          union all
          select imrw.match_key_id,
                 imrw.currency_code,
                 NVL(imrw.loc_set_of_books_id, -1) loc_set_of_books_id,
                 NVL(imrw.loc_vat_region, -1) loc_vat_region
            from im_match_rcpt_ws imrw
           where imrw.workspace_id = I_workspace_id
             and imrw.match_key_id = L_match_key_id) inner
    GROUP BY inner.match_key_id;

   LOGGER.LOG_INFORMATION(L_program||'Check for Matchable documents ');

   if L_curr_code_count > REIM_CONSTANTS.ONE then
      RAISE multi_curr_code;
   elsif L_loc_sob_count > REIM_CONSTANTS.ONE then
      RAISE multi_loc_sob;
   elsif L_loc_vat_region_count > REIM_CONSTANTS.ONE then
      RAISE multi_loc_vat_reg;
   end if;

   merge into im_match_invc_detl_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 imiw.match_key_id
            from im_match_invc_ws imiw
           where imiw.workspace_id = I_workspace_id
             and imiw.match_key_id = L_match_key_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.status       = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH)
   when MATCHED THEN
      update
         set tgt.match_key_id = src.match_key_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_INVC_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_detl_ws tgt
   using (select imrw.workspace_id,
                 imrw.shipment,
                 imrw.match_key_id
            from im_match_rcpt_ws imrw
           where imrw.workspace_id = I_workspace_id
             and imrw.match_key_id = L_match_key_id) src
   on (    tgt.workspace_id      = src.workspace_id
       and tgt.shipment          = src.shipment
       and tgt.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH)
   when MATCHED THEN
      update
         set tgt.match_key_id = src.match_key_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_RCPT_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --create im_match_group_head_ws rows for unmatched data
   insert into im_match_group_head_ws (workspace_id,
                                       match_group_id,
                                       match_stgy_id,
                                       match_stgy_dtl_id,
                                       chunk_num,
                                       match_luw_id,
                                       invc_group_id,
                                       rcpt_group_id,
                                       match_type,
                                       match_key_id,
                                       item,
                                       cost_tolerance_dtl_id,
                                       qty_tolerance_dtl_id,
                                       invc_group_cost,
                                       invc_group_qty,
                                       rcpt_group_cost,
                                       rcpt_group_qty,
                                       cost_variance,
                                       qty_variance,
                                       cost_discrepant,
                                       qty_discrepant,
                                       cost_disc_resolved,
                                       qty_disc_resolved,
                                       cost_adjust_rc,
                                       qty_adjust_rc,
                                       sku_compliant,
                                       tax_compliant,
                                       match_available,
                                       match_status,
                                       persist_status,
                                       error_code,
                                       error_context)
                                select I_workspace_id,
                                       L_match_group_id,
                                       REIM_CONSTANTS.ONLINE_SMRY_MTCH_STGY_ID match_stgy_id,
                                       REIM_CONSTANTS.ONLINE_SMRY_MTCH_STGY_DTL_ID match_stgy_dtl_id,
                                       REIM_CONSTANTS.ONE chunk_num,
                                       REIM_CONSTANTS.ONE match_luw_id,
                                       NULL invc_group_id,
                                       NULL rcpt_group_id,
                                       REIM_CONSTANTS.MATCH_LEVEL_SUMM_ALL_2_ALL match_type,
                                       L_match_key_id,
                                       NULL item,
                                       NULL cost_tolerance_dtl_id,
                                       NULL qty_tolerance_dtl_id,
                                       invc.invc_group_cost,
                                       invc.invc_group_qty,
                                       rcpt.rcpt_group_cost,
                                       rcpt.rcpt_group_qty,
                                       (rcpt.rcpt_group_cost - invc.invc_group_cost) cost_variance,
                                       (rcpt.rcpt_group_qty - invc.invc_group_qty) qty_variance,
                                       'N' cost_discrepant,
                                       'N' qty_discrepant,
                                       NULL cost_disc_resolved,
                                       NULL qty_disc_resolved,
                                       NULL cost_adjust_rc,
                                       NULL qty_adjust_rc,
                                       'Y' sku_compliant,
                                       'Y' tax_compliant,
                                       'Y' match_available,
                                       REIM_CONSTANTS.DOC_STATUS_MTCH match_status,
                                       'N' persist_status,
                                       NULL error_code,
                                       NULL error_context
                                  from (select inner.workspace_id,
                                               inner.match_key_id,
                                               sum(inner.invc_group_cost) invc_group_cost,
                                               sum(inner.invc_group_qty) invc_group_qty
                                          from (select id.workspace_id,
                                                       id.match_key_id,
                                                       sum(id.unit_cost * id.invoice_qty) invc_group_cost,
                                                       sum(id.invoice_qty) invc_group_qty
                                                  from im_match_invc_ws iw,
                                                       im_match_invc_detl_ws id
                                                 where iw.workspace_id = I_workspace_id
                                                   and iw.match_key_id = L_match_key_id
                                                   and iw.choice_flag  = 'Y'
                                                   and iw.header_only  = 'N'
                                                   and iw.workspace_id = id.workspace_id
                                                   and iw.doc_id       = id.doc_id
                                                   and id.status       = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                                                 GROUP BY id.workspace_id,
                                                          id.match_key_id
                                                union all
                                                select iw.workspace_id,
                                                       iw.match_key_id,
                                                       sum(iw.merch_amount) invc_group_cost,
                                                       sum(iw.total_qty) invc_group_qty
                                                  from im_match_invc_ws iw
                                                 where iw.workspace_id = I_workspace_id
                                                   and iw.match_key_id = L_match_key_id
                                                   and iw.choice_flag  = 'Y'
                                                   and iw.header_only  = 'Y'
                                                 GROUP BY iw.workspace_id,
                                                          iw.match_key_id) inner
                                         GROUP BY inner.workspace_id,
                                                  inner.match_key_id) invc,
                                       (select rd.workspace_id,
                                               rd.match_key_id,
                                               sum(rd.unit_cost * NVL(rd.qty_available, 0)) rcpt_group_cost,
                                               sum(rd.qty_available) rcpt_group_qty
                                          from im_match_rcpt_detl_ws rd
                                         where rd.workspace_id      = I_workspace_id
                                           and rd.match_key_id      = L_match_key_id
                                           and NVL(rd.invc_match_status, REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH) = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                                         GROUP BY rd.workspace_id,
                                                  rd.match_key_id) rcpt
                                 where invc.workspace_id   = I_workspace_id
                                   and invc.match_key_id   = L_match_key_id
                                   and rcpt.workspace_id   = invc.workspace_id
                                   and rcpt.match_key_id   = invc.match_key_id;

   LOGGER.LOG_INFORMATION(L_program||'Insert IM_MATCH_GROUP_HEAD_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(1)
     into L_hdr_only_doc_count
     from im_match_invc_ws imiw
    where imiw.workspace_id = I_workspace_id
      and imiw.match_key_id = L_match_key_id
      and imiw.header_only  = 'Y';

   if L_hdr_only_doc_count = REIM_CONSTANTS.ZERO and NVL(I_skip_sku_comp, 'N') = 'N' then

      --sku tolerance rcpt to invc
      merge into im_match_group_head_ws target
      using (with iw as (select distinct
                                i.workspace_id,
                                i.match_key_id,
                                i.sku_comp_percent
                           from im_match_invc_ws i
                          where i.workspace_id = I_workspace_id
                            and i.match_key_id = L_match_key_id),
                  rcpt as (select distinct
                                  rd.workspace_id,
                                  rd.match_key_id,
                                  rd.item
                            from im_match_rcpt_detl_ws rd
                           where rd.workspace_id      = I_workspace_id
                             and rd.match_key_id      = L_match_key_id
                             and rd.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH),
                  invc as (select distinct
                                  id.workspace_id,
                                  id.match_key_id,
                                  id.item
                            from im_match_invc_detl_ws id
                           where id.workspace_id = I_workspace_id
                             and id.match_key_id = L_match_key_id
                             and id.status       = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH)
             select inner.workspace_id,
                    inner.match_key_id,
                    min(inner.sku_compliant) sku_compliant
               from (select rcpt.workspace_id,
                            rcpt.match_key_id,
                            case when (SUM(DECODE(rcpt.item,
                                                  invc.item, REIM_CONSTANTS.ONE,
                                                  REIM_CONSTANTS.ZERO)) / count(rcpt.item)) * 100 <
                                         iw.sku_comp_percent then
                                    'N'
                                 else
                                    'Y'
                            end sku_compliant
                       from iw,
                            rcpt,
                            invc
                      where iw.workspace_id   = rcpt.workspace_id
                        and iw.match_key_id   = rcpt.match_key_id
                        and rcpt.workspace_id = invc.workspace_id (+)
                        and rcpt.match_key_id = invc.match_key_id (+)
                        and rcpt.item         = invc.item (+)
                      group by rcpt.workspace_id,
                               rcpt.match_key_id,
                               iw.sku_comp_percent
                      union all
                     select invc.workspace_id,
                            invc.match_key_id,
                            case when (SUM(DECODE(invc.item,
                                                  rcpt.item, REIM_CONSTANTS.ONE,
                                                  REIM_CONSTANTS.ZERO)) / count(invc.item)) * 100 <
                                         iw.sku_comp_percent then
                                    'N'
                                 else
                                    'Y'
                            end sku_compliant
                       from iw,
                            rcpt,
                            invc
                      where iw.workspace_id   = invc.workspace_id
                        and iw.match_key_id   = invc.match_key_id
                        and invc.workspace_id = rcpt.workspace_id (+)
                        and invc.match_key_id = rcpt.match_key_id (+)
                        and invc.item         = rcpt.item (+)
                      group by invc.workspace_id,
                               invc.match_key_id,
                               iw.sku_comp_percent) inner
              group by inner.workspace_id,
                       inner.match_key_id) use_this
      on (    target.workspace_id = use_this.workspace_id
          and target.match_key_id = use_this.match_key_id)
      when matched then update
       set target.sku_compliant    = use_this.sku_compliant,
           --target.match_available  = 'N',
           target.match_status     = DECODE(use_this.sku_compliant,
                                            'N', REIM_CONSTANTS.DOC_STATUS_URMTCH,
                                            target.match_status),
           target.error_code       = DECODE(use_this.sku_compliant,
                                            'N', 'sku_comp',               --constant
                                            NULL),
           target.error_context    = DECODE(use_this.sku_compliant,
                                            'N', 'sku_comp_context',               --constant
                                            NULL);

      LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_GROUP_HEAD_WS SKU_COMPLIANCE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      select DECODE(imghw.sku_compliant,
                    'Y', REIM_CONSTANTS.ONE,
                    REIM_CONSTANTS.ZERO)
        into L_sku_compliant
        from im_match_group_head_ws imghw
       where imghw.workspace_id = I_workspace_id
         and imghw.match_key_id = L_match_key_id;

      if L_sku_compliant = REIM_CONSTANTS.ZERO then
         RAISE sku_compliance;
      end if;

      if REIM_AUTO_MATCH_SQL.CHECK_TAX_COMPLIANCE(O_error_message,
                                                  I_workspace_id,
                                                  REIM_CONSTANTS.ONE, --I_match_luw_id
                                                  L_match_key_id,
                                                  REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

      select DECODE(imghw.tax_compliant,
                    'Y', REIM_CONSTANTS.ZERO,
                    REIM_CONSTANTS.ONE)
        into L_tax_compliant
        from im_match_group_head_ws imghw
       where imghw.workspace_id = I_workspace_id
         and imghw.match_key_id = L_match_key_id;

      if L_tax_compliant = REIM_CONSTANTS.ONE then
         RAISE tax_compliance;
      end if;

   end if;

   --push matched info to workspace.
   merge into im_match_invc_ws target
   using (select gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 imiw.doc_id
            from im_match_group_head_ws gh,
                 im_match_invc_ws imiw
           where gh.workspace_id     = I_workspace_id
             and gh.match_key_id     = L_match_key_id
             and gh.match_status     = REIM_CONSTANTS.DOC_STATUS_MTCH
             and imiw.match_key_id   = gh.match_key_id
             and imiw.status         <> REIM_CONSTANTS.DOC_STATUS_MTCH
             and imiw.match_group_id is NULL
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id
       and target.doc_id       = use_this.doc_id)
   when matched then update
    set target.status         = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.match_group_id = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_INVC_WS PUSH_BACK_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ FULL(TARGET) */
   into im_match_invc_detl_ws target
   using (select /*+ ORDERED FULL(GH) FULL(ID) */
         gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 id.doc_id,
                 id.item
            from im_match_group_head_ws gh,
                 im_match_invc_detl_ws id
           where gh.workspace_id   = I_workspace_id
             and gh.match_key_id   = L_match_key_id
             and gh.match_status   = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.match_key_id   = id.match_key_id
             and id.status         <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
             and id.match_group_id is null
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id
       and target.doc_id       = use_this.doc_id
       and target.item         = use_this.item)
   when matched then update
    set target.status         = REIM_CONSTANTS.DOC_STATUS_MTCH,
        target.cost_matched   = 'Y',
        target.qty_matched    = 'Y',
        target.match_group_id = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_INVC_DETL_WS PUSH_BACK_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_ws target
   using (select gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 ri.shipment
            from im_match_group_head_ws gh,
                 im_match_rcpt_ws ri
           where gh.workspace_id      = I_workspace_id
             and gh.match_key_id      = L_match_key_id
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.match_key_id      = ri.match_key_id
             and ri.invc_match_status = REIM_CONSTANTS.SHIP_STATUS_UNMTCH
             and ri.match_group_id    is null
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id
       and target.shipment     = use_this.shipment)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_MTCH,
        target.match_group_id    = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_RCPT_WS PUSH_BACK_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge /*+ FULL(TARGET) */
   into im_match_rcpt_detl_ws target
   using (select /*+ ORDERED FULL(GH) FULL(RD) */
         gh.workspace_id,
                 gh.match_key_id,
                 gh.match_group_id,
                 rd.shipment,
                 rd.item
            from im_match_group_head_ws gh,
                 im_match_rcpt_detl_ws rd
           where gh.workspace_id      = I_workspace_id
             and gh.match_key_id      = L_match_key_id
             and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH
             and gh.match_key_id      = rd.match_key_id
             and rd.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
             and rd.match_group_id    is null
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.match_key_id = use_this.match_key_id
       and target.shipment     = use_this.shipment
       and target.item         = use_this.item)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
        target.match_group_id    = use_this.match_group_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_RCPT_DETL_WS PUSH_BACK_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_AUTO_MATCH_SQL.PERSIST_SUMMARY_MATCH_DATA(O_error_message,
                                                     I_workspace_id,
                                                     REIM_CONSTANTS.ONE, --match_luw_id
                                                     L_match_key_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REFRESH_MTCH_WSPACE(O_error_message,
                          I_workspace_id,
                          'Y') = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --Recreate the Detail Matching Workspace
   --------------------------------------

   if CREATE_DETAIL_MATCH_WS(O_error_message,
                             I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REFRESH_DISCREPANCY_LIST_WS(O_error_message,
                                  I_workspace_id) = 0 then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when sku_compliance then
      O_error_message := SQL_LIB.CREATE_MSG('SKU_COMPLIANCE_FAILED', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when tax_compliance then
      O_error_message := SQL_LIB.CREATE_MSG('TAX_COMPLIANCE_FAILED', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when multi_curr_code then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_CURR_CODE', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when multi_loc_sob then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_LOC_SOB', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when multi_loc_vat_reg then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_LOC_VAT_REGION', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERFORM_SMRY_MATCH_ONLINE;
------------------------------------------------------------------------------------------
/**
 * The public function used to refresh match data
 * Input param: I_workspace_id (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION REFRESH_MTCH_WSPACE(O_error_message    OUT VARCHAR2,
                             I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                             I_skip_matched  IN     VARCHAR2)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.REFRESH_MTCH_WSPACE';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start ');

   delete from im_match_invc_detl_ws imidw
    where EXISTS (select 'x'
                    from im_match_invc_ws imiw
                   where imiw.workspace_id = I_workspace_id
                     and imiw.choice_flag  = 'Y'
                     and imidw.workspace_id = imiw.workspace_id
                     and imidw.doc_id       = imiw.doc_id);

   LOGGER.LOG_INFORMATION(L_program||'Delete INVC_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_match_rcpt_detl_ws imrdw
    where EXISTS (select 'x'
                    from im_match_rcpt_ws imrw
                   where imrw.workspace_id  = I_workspace_id
                     and imrw.choice_flag   = 'Y'
                     and imrdw.workspace_id = imrw.workspace_id
                     and imrdw.shipment     = imrw.shipment);

   LOGGER.LOG_INFORMATION(L_program||'Delete RCPT_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_ws tgt
   using (select imiw.workspace_id workspace_id,
                 imiw.doc_id,
                 idh.object_version_id doc_head_version_id,
                 idh.status,
                 idh.resolution_adjusted_total_cost total_avail_cost, --merge later
                 idh.resolution_adjusted_total_qty total_avail_qty, --merge later
                 idh.resolution_adjusted_total_cost merch_amount, -- merge later
                 idh.resolution_adjusted_total_qty total_qty,
                 idh.cost_pre_match,
                 idh.header_only,
                 imgi.group_id manual_group_id,
                 'Y' summary_mtch_elig, --merge later
                 DECODE(idh.header_only,
                        'Y', 'N',
                        'Y') detail_mtch_elig,
                 case
                    when I_skip_matched = 'Y' then
                       DECODE(idh.status,
                              REIM_CONSTANTS.DOC_STATUS_RMTCH, 'Y',
                              REIM_CONSTANTS.DOC_STATUS_URMTCH, 'Y',
                              'N')
                    else
                       'Y'
                 end choice_flag,
                 idh.pre_paid_ind,
                 idh.custom_doc_ref_1,
                 idh.custom_doc_ref_2,
                 idh.custom_doc_ref_3,
                 idh.custom_doc_ref_4
            from im_match_invc_ws imiw,
                 im_doc_head idh,
                 im_manual_group_invoices imgi
           where imiw.workspace_id = I_workspace_id
             and imiw.choice_flag  = 'Y'
             and idh.doc_id        = imiw.doc_id
             and imgi.doc_id(+)    = imiw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.doc_head_version_id = src.doc_head_version_id,
             tgt.status              = src.status,
             tgt.total_avail_cost    = src.total_avail_cost,
             tgt.total_avail_qty     = src.total_avail_qty,
             tgt.merch_amount        = src.merch_amount,
             tgt.total_qty           = src.total_qty,
             tgt.cost_pre_match      = src.cost_pre_match,
             tgt.header_only         = src.header_only,
             tgt.manual_group_id     = src.manual_group_id,
             tgt.summary_mtch_elig   = src.summary_mtch_elig,
             tgt.detail_mtch_elig    = src.detail_mtch_elig,
             tgt.choice_flag         = src.choice_flag,
             tgt.pre_paid_ind        = src.pre_paid_ind,
             tgt.custom_doc_ref_1    = src.custom_doc_ref_1,
             tgt.custom_doc_ref_2    = src.custom_doc_ref_2,
             tgt.custom_doc_ref_3    = src.custom_doc_ref_3,
             tgt.custom_doc_ref_4    = src.custom_doc_ref_4;

   LOGGER.LOG_INFORMATION(L_program||'Merge INVC_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_ws tgt
   using (select imrw.workspace_id workspace_id,
                 sh.shipment,
                 sh.status_code,
                 sh.invc_match_status,
                 0 total_avail_cost, --merge later
                 0 total_avail_qty, --merge later
                 0 merch_amount, --merge later
                 0 total_qty, --merge later
                 case
                    when I_skip_matched = 'Y' then
                       DECODE(sh.invc_match_status,
                              REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH, 'Y',
                              'N')
                    else
                       'Y'
                 end choice_flag
            from im_match_rcpt_ws imrw,
                 shipment sh
           where imrw.workspace_id = I_workspace_id
             and imrw.choice_flag  = 'Y'
             and sh.shipment       = imrw.shipment) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment)
   when MATCHED then
      update
         set tgt.status_code       = src.status_code,
             tgt.invc_match_status = src.invc_match_status,
             tgt.total_avail_cost  = src.total_avail_cost,
             tgt.total_avail_qty   = src.total_avail_qty,
             tgt.merch_amount      = src.merch_amount,
             tgt.total_qty         = src.total_qty,
             tgt.choice_flag       = src.choice_flag;

   LOGGER.LOG_INFORMATION(L_program||'Merge RCPT_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if EXPAND_REFRESH_MTCH_WSPACE(O_error_message,
                                 I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   merge into im_match_invc_detl_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 iid.item,
                 its.vpn,
                 iid.object_version_id invc_detl_version_id,
                 imiw.match_key_id,
                 imiw.chunk_num,
                 imiw.match_luw_id,
                 iid.status,
                 iid.resolution_adjusted_unit_cost unit_cost,
                 iid.resolution_adjusted_qty invoice_qty,
                 iid.cost_matched,
                 iid.qty_matched,
                 imiw.choice_flag
      from im_match_invc_ws imiw,
           im_invoice_detail iid,
           item_supplier its
     where imiw.workspace_id = I_workspace_id
       and imiw.choice_flag  = 'Y'
       and iid.doc_id        = imiw.doc_id
       and its.item (+)      = iid.item
       and its.supplier (+)  = imiw.supplier_site_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when NOT MATCHED then
      insert (workspace_id,
              doc_id,
              item,
              vpn,
              invc_detl_version_id,
              match_key_id,
              chunk_num,
              match_luw_id,
              status,
              unit_cost,
              invoice_qty,
              cost_matched,
              qty_matched,
              choice_flag)
      values (src.workspace_id,
              src.doc_id,
              src.item,
              src.vpn,
              src.invc_detl_version_id,
              src.match_key_id,
              src.chunk_num,
              src.match_luw_id,
              src.status,
              src.unit_cost,
              src.invoice_qty,
              src.cost_matched,
              src.qty_matched,
              src.choice_flag);

   LOGGER.LOG_INFORMATION(L_program||'Insert INVC_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_transform_shipsku_gtt;

   insert into im_transform_shipsku_gtt (shipment,
                                         item,
                                         vpn,
                                         seq_no,
                                         ss_qty_received,
                                         ss_qty_matched,
                                         ss_unit_cost,
                                         weight_received,
                                         weight_received_uom,
                                         carton,
                                         catch_weight_type,
                                         order_no,
                                         supplier,
                                         sup_qty_level,
                                         transform_qty_received,
                                         transform_qty_matched,
                                         transform_unit_cost)
                                  select imrw.shipment,
                                         ss.item,
                                         its.vpn,
                                         ss.seq_no,
                                         ss.qty_received,
                                         ss.qty_matched,
                                         ss.unit_cost,
                                         ss.weight_received,
                                         ss.weight_received_uom,
                                         ss.carton,
                                         im.catch_weight_type,
                                         imrw.order_no,
                                         imrw.supplier_site_id,
                                         s.sup_qty_level,
                                         ss.qty_received,
                                         ss.qty_matched,
                                         ss.unit_cost
                                    from im_match_rcpt_ws imrw,
                                         shipsku ss,
                                         sups s,
                                         item_master im,
                                         item_supplier its
                                   where imrw.workspace_id = I_workspace_id
                                     and imrw.choice_flag  = 'Y'
                                     and ss.shipment       = imrw.shipment
                                     and s.supplier        = imrw.supplier_site_id
                                     and im.item           = ss.item
                                     and its.item          = ss.item
                                     and its.supplier      = imrw.supplier_site_id
                                     and NOT EXISTS (select 'x'
                                                       from im_match_rcpt_detl_ws imrdw
                                                      where imrdw.workspace_id = I_workspace_id
                                                        and imrdw.shipment     = imrw.shipment);

   LOGGER.LOG_INFORMATION(L_program||'Insert Shipsku xform gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_XFORM_SHIPSKU_SQL.TRANSFORM_SHIPSKU_GTT(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   insert into im_match_rcpt_detl_ws(workspace_id,
                                     shipment,
                                     item,
                                     vpn,
                                     invc_match_status,
                                     unit_cost,
                                     qty_received,
                                     qty_available,
                                     unit_cost_nc,
                                     qty_available_nc,
                                     catch_weight_type,
                                     choice_flag,
                                     match_luw_id,
                                     chunk_num,
                                     match_key_id)
        with im_xform_shipsku_gtt as (select gtt.shipment,
                                             gtt.item,
                                             gtt.vpn,
                                             case
                                                when SUM(NVL(gtt.ss_qty_received, 0) - NVL(gtt.ss_qty_matched, 0)) <> REIM_CONSTANTS.ZERO then
                                                   REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
                                                else
                                                   REIM_CONSTANTS.SSKU_IM_STATUS_MTCH
                                             end invc_match_status,
                                             gtt.transform_unit_cost,
                                             SUM(NVL(gtt.transform_qty_received, 0)) qty_received,
                                             SUM(NVL(gtt.transform_qty_received, 0) - NVL(ipmr.qty_matched, 0)) qty_available,
                                             gtt.ss_unit_cost unit_cost_nc,
                                             SUM(NVL(gtt.ss_qty_received, 0) - NVL(gtt.ss_qty_matched, 0)) qty_available_nc,
                                             gtt.catch_weight_type
                                        from im_transform_shipsku_gtt gtt,
                                             im_partially_matched_receipts ipmr
                                       where ipmr.shipment (+) = gtt.shipment
                                         and ipmr.item (+)     = gtt.item
                                       GROUP BY gtt.shipment,
                                                gtt.item,
                                                gtt.vpn,
                                                gtt.transform_unit_cost,
                                                gtt.ss_unit_cost,
                                                gtt.catch_weight_type)
                              select imrw.workspace_id,
                                     imrw.shipment,
                                     gtt.item,
                                     gtt.vpn,
                                     gtt.invc_match_status,
                                     gtt.transform_unit_cost,
                                     gtt.qty_received,
                                     gtt.qty_available,
                                     gtt.unit_cost_nc,
                                     gtt.qty_available_nc,
                                     gtt.catch_weight_type,
                                     imrw.choice_flag,
                                     imrw.match_luw_id,
                                     imrw.chunk_num,
                                     imrw.match_key_id
                                from im_match_rcpt_ws imrw,
                                     im_xform_shipsku_gtt gtt
                               where imrw.workspace_id = I_workspace_id
                                 and imrw.choice_flag  = 'Y'
                                 and gtt.shipment      = imrw.shipment;

   LOGGER.LOG_INFORMATION(L_program||'Insert im_match_rcpt_detl_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 SUM(idnm.non_merch_amt) non_merch_amt
            from im_match_invc_ws imiw,
                 im_doc_non_merch idnm
           where imiw.workspace_id = I_workspace_id
             and imiw.choice_flag  = 'Y'
             and idnm.doc_id       = imiw.doc_id
           GROUP BY imiw.workspace_id,
                    imiw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.merch_amount     = tgt.merch_amount - src.non_merch_amt,
             tgt.total_avail_cost = tgt.total_avail_cost - src.non_merch_amt;

   LOGGER.LOG_INFORMATION(L_program||'Merge INVC_HEAD_WS NON_MERCH_AMT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_ws tgt
   using (select imidw.workspace_id,
                 imidw.doc_id,
                 SUM(DECODE(imidw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imidw.invoice_qty,
                            REIM_CONSTANTS.ZERO) * imidw.unit_cost) unmatch_item_amt,
                 SUM(DECODE(imidw.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH, imidw.invoice_qty,
                            REIM_CONSTANTS.ZERO)) unmatch_item_qty
            from im_match_invc_ws imiw,
                 im_match_invc_detl_ws imidw
           where imiw.workspace_id  = I_workspace_id
             and imiw.choice_flag   = 'Y'
             and imidw.workspace_id = imiw.workspace_id
             and imidw.doc_id       = imiw.doc_id
           GROUP BY imidw.workspace_id,
                    imidw.doc_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.total_avail_cost = src.unmatch_item_amt,
             tgt.total_avail_qty  = src.unmatch_item_qty,
             tgt.header_only      = 'N';

   LOGGER.LOG_INFORMATION(L_program||'Merge INVC_HEAD_WS AVAILABLE COST AND QTY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 'N' summary_mtch_elig
            from im_match_invc_ws imiw
           where imiw.workspace_id = I_workspace_id
             and imiw.choice_flag  = 'Y'
             and EXISTS (select 'x'
                           from im_invoice_detail iid
                          where iid.doc_id = imiw.doc_id
                            and iid.status   = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH
                            and DECODE(iid.cost_matched,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES) <> DECODE(iid.qty_matched,
                                                                                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD,
                                                                                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES))) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED then
      update
         set tgt.summary_mtch_elig = src.summary_mtch_elig;

   LOGGER.LOG_INFORMATION(L_program||'Merge INVC_HEAD_WS SUMMARY_MTCH_ELIG - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_ws tgt
   using (select imrdw.workspace_id,
                 imrdw.shipment,
                 SUM(imrdw.unit_cost * imrdw.qty_available) total_avail_cost,
                 SUM(imrdw.qty_available) total_avail_qty,
                 SUM(imrdw.unit_cost * imrdw.qty_received) merch_amount,
                 SUM(imrdw.qty_received) total_qty
            from im_match_rcpt_detl_ws imrdw
           where imrdw.workspace_id = I_workspace_id
           GROUP BY imrdw.workspace_id,
                    imrdw.shipment) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment
       and tgt.choice_flag  = 'Y')
   when MATCHED then
      update
         set tgt.total_avail_cost = src.total_avail_cost,
             tgt.total_avail_qty  = src.total_avail_qty,
             tgt.merch_amount     = src.merch_amount,
             tgt.total_qty        = src.total_qty;

   LOGGER.LOG_INFORMATION(L_program||'Merge cost-qty im_match_rcpt_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_detl_ws tgt
   using (select imidw.workspace_id,
                 imidw.doc_id,
                 imidw.item,
                 idmih.match_id match_hist_id
            from im_match_invc_detl_ws imidw,
                 im_detail_match_invc_history idmih
           where imidw.workspace_id = I_workspace_id
             and idmih.doc_id       = imidw.doc_id
             and idmih.item         = imidw.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.match_hist_id     = src.match_hist_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge Match Hist ID on INVC_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if POP_ORDLOC_UNIT_COST(O_error_message,
                           I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if UPDATE_DEALS_EXIST(O_error_message,
                         I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   ------------------------------------------------
   --push sub items to rcpt detail workspace
   ------------------------------------------------

   merge into im_match_rcpt_detl_ws tgt
   using (select d.workspace_id,
                 d.receipt_id,
                 d.item,
                 d.substitute_item
            from im_detail_match_ws d
           where d.workspace_id    = I_workspace_id
             and d.entity_type     = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
   ) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.receipt_id
       and tgt.item         = src.item)
   when MATCHED THEN
      update
         set tgt.substitute_item = src.substitute_item;

   LOGGER.LOG_INFORMATION(L_program||' push substitute_item from detail match ws to im_match_rcpt_detl_ws: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_detl_ws tgt
   using (select imrdw.workspace_id,
                 imrdw.shipment,
                 imrdw.item,
                 MAX(idmrh.match_id) match_hist_id,
                 COUNT(1) match_count
            from im_match_rcpt_detl_ws imrdw,
                 im_detail_match_rcpt_history idmrh
           where imrdw.workspace_id = I_workspace_id
             and idmrh.shipment     = imrdw.shipment
             and idmrh.item         = NVL(imrdw.substitute_item, imrdw.item)
           GROUP BY imrdw.workspace_id,
                    imrdw.shipment,
                    imrdw.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment
       and tgt.item         = src.item
       and src.match_count  = REIM_CONSTANTS.ONE)
   when MATCHED then
      update
         set tgt.match_hist_id     = src.match_hist_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge Match Hist ID on RCPT_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REFRESH_MTCH_WSPACE;
------------------------------------------------------------------------------------------
FUNCTION EXPAND_REFRESH_MTCH_WSPACE(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.EXPAND_REFRESH_MTCH_WSPACE';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   for x in 1..LP_expand_group_level loop

      delete from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date(number_1)
         select distinct imiw.manual_group_id
           from im_match_invc_ws imiw
          where imiw.workspace_id = I_workspace_id
            and imiw.choice_flag  = 'Y'
            and imiw.manual_group_id is NOT NULL
         union
         select distinct gr.group_id manual_group_id
           from im_match_rcpt_ws imrw,
                im_manual_group_receipts gr
          where imrw.workspace_id = I_workspace_id
            and imrw.choice_flag  = 'Y'
            and imrw.shipment     = gr.shipment;

      merge into im_match_invc_ws tgt
      using (select I_workspace_id workspace_id,
                    idh.doc_id,
                    idh.ext_doc_id,
                    idh.object_version_id doc_head_version_id,
                    idh.status,
                    idh.doc_date,
                    idh.due_date,
                    iso.group_id supplier_group_id,
                    idh.vendor supplier,
                    sups_parent.sup_name supplier_name,
                    sups_parent.contact_phone supplier_phone,
                    idh.supplier_site_id,
                    sups.sup_name supplier_site_name,
                    idh.currency_code,
                    idh.order_no,
                    idh.location,
                    idh.loc_type,
                    loc.loc_name,
                    idh.resolution_adjusted_total_cost total_avail_cost, --merge later
                    idh.resolution_adjusted_total_qty total_avail_qty, --merge later
                    idh.resolution_adjusted_total_cost merch_amount, -- merge later
                    idh.resolution_adjusted_total_qty total_qty,
                    idh.cost_pre_match,
                    'Y' header_only, -- header_only, merge later
                    'Y' summary_mtch_elig, -- summary_mtch_elig, merge later
                    'Y' detail_mtch_elig, -- detail_mtch_elig, merge later
                    NVL(iso.total_qty_required_ind, 'N') qty_required, -- to be confirmed
                    NVL(iso.match_total_qty_ind, 'N') qty_match_required, -- to be confirmed
                    NVL(iso.sku_comp_percent, REIM_CONSTANTS.ZERO) sku_comp_percent,
                    imgi.group_id manual_group_id,
                    'Y' choice_flag,
                    idh.pre_paid_ind,
                    idh.custom_doc_ref_1,
                    idh.custom_doc_ref_2,
                    idh.custom_doc_ref_3,
                    idh.custom_doc_ref_4
               from gtt_num_num_str_str_date_date gtt,
                    im_manual_group_invoices imgi,
                    im_doc_head idh,
                    v_im_supp_site_attrib_expl iso,
                    sups sups_parent,
                    sups sups,
                    (select store loc,
                            REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                            store_name loc_name
                       from store
                     union all
                     select wh loc,
                            REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                            wh_name loc_name
                       from wh
                      where wh = physical_wh) loc
              where imgi.group_id        = gtt.number_1
                and idh.doc_id           = imgi.doc_id
                and iso.supplier         = idh.supplier_site_id
                and sups_parent.supplier = idh.vendor
                and sups.supplier        = idh.supplier_site_id
                and loc.loc              = idh.location
                and loc.loc_type         = idh.loc_type) src
      on (    tgt.workspace_id = src.workspace_id
          and tgt.doc_id       = src.doc_id)
      when NOT MATCHED THEN
         insert (workspace_id,
                 doc_id,
                 ext_doc_id,
                 doc_head_version_id,
                 status,
                 doc_date,
                 due_date,
                 supplier_group_id,
                 supplier,
                 supplier_name,
                 supplier_phone,
                 supplier_site_id,
                 supplier_site_name,
                 currency_code,
                 order_no,
                 location,
                 loc_type,
                 location_name,
                 total_avail_cost,
                 total_avail_qty,
                 merch_amount,
                 total_qty,
                 cost_pre_match,
                 header_only,
                 summary_mtch_elig,
                 detail_mtch_elig,
                 qty_required,
                 qty_match_required,
                 sku_comp_percent,
                 manual_group_id,
                 choice_flag,
                 chunk_num,
                 match_luw_id,
                 pre_paid_ind,
                 custom_doc_ref_1,
                 custom_doc_ref_2,
                 custom_doc_ref_3,
                 custom_doc_ref_4)
         values (src.workspace_id,
                 src.doc_id,
                 src.ext_doc_id,
                 src.doc_head_version_id,
                 src.status,
                 src.doc_date,
                 src.due_date,
                 src.supplier_group_id,
                 src.supplier,
                 src.supplier_name,
                 src.supplier_phone,
                 src.supplier_site_id,
                 src.supplier_site_name,
                 src.currency_code,
                 src.order_no,
                 src.location,
                 src.loc_type,
                 src.loc_name,
                 src.total_avail_cost, --merge later
                 src.total_avail_qty, --merge later
                 src.merch_amount, -- merge later
                 src.total_qty,
                 src.cost_pre_match,
                 src.header_only, -- header_only, merge later
                 src.summary_mtch_elig, -- summary_mtch_elig, merge later
                 src.detail_mtch_elig, -- detail_mtch_elig, merge later
                 src.qty_required,
                 src.qty_match_required,
                 src.sku_comp_percent,
                 src.manual_group_id,
                 src.choice_flag,
                 REIM_CONSTANTS.ONE,
                 REIM_CONSTANTS.ONE,
                 src.pre_paid_ind,
                 src.custom_doc_ref_1,
                 src.custom_doc_ref_2,
                 src.custom_doc_ref_3,
                 src.custom_doc_ref_4);

      LOGGER.LOG_INFORMATION(L_program||'Insert invoices from MANUAL GROUPS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into im_match_rcpt_ws tgt
      using (with loc as (select store loc,
                                 REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                 store_name loc_name
                            from store
                          union all
                          select wh loc,
                                 REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                 wh_name loc_name
                            from wh
                           where wh = physical_wh)
             select distinct I_workspace_id workspace_id,
                    sh.shipment,
                    oh.order_no,
                    sh.asn,
                    sh.status_code,
                    sh.invc_match_status,
                    sh.to_loc location,
                    sh.to_loc_type loc_type,
                    ship_to_loc.loc_name loc_name,
                    sh.bill_to_loc,
                    sh.bill_to_loc_type,
                    bill_to_loc.loc_name bill_to_loc_name,
                    sh.ship_date,
                    sh.receive_date,
                    sups_parent.supplier,
                    sups_parent.sup_name supplier_name,
                    oh.supplier supplier_site_id,
                    sups.sup_name supplier_site_name,
                    oh.currency_code,
                    0 total_avail_cost, --merge later
                    0 total_avail_qty, --merge later
                    0 merch_amount, --merge later
                    0 total_qty, --merge later
                    'Y' choice_flag
               from gtt_num_num_str_str_date_date gtt,
                    im_manual_group_receipts imgr,
                    shipment sh,
                    ordhead oh,
                    sups sups_parent,
                    sups sups,
                    loc ship_to_loc,
                    loc bill_to_loc
              where imgr.group_id        = gtt.number_1
                and sh.shipment          = imgr.shipment
                and sh.receive_date      is NOT NULL
                and oh.order_no          = sh.order_no
                and sups.supplier        = oh.supplier
                and sups_parent.supplier = sups.supplier_parent
                and ship_to_loc.loc      = sh.to_loc
                and ship_to_loc.loc_type = sh.to_loc_type
                and bill_to_loc.loc      = sh.bill_to_loc
                and bill_to_loc.loc_type = sh.bill_to_loc_type) src
      on (    tgt.workspace_id = src.workspace_id
          and tgt.shipment     = src.shipment)
      when NOT MATCHED THEN
         insert (workspace_id,
                 shipment,
                 supplier,
                 supplier_name,
                 supplier_site_id,
                 supplier_site_name,
                 order_no,
                 asn,
                 bill_to_loc,
                 bill_to_loc_type,
                 bill_to_loc_name,
                 ship_to_loc,
                 ship_to_loc_type,
                 ship_to_loc_name,
                 status_code,
                 invc_match_status,
                 ship_date,
                 receive_date,
                 currency_code,
                 total_avail_cost,
                 total_avail_qty,
                 merch_amount,
                 total_qty,
                 choice_flag,
                 chunk_num,
                 match_luw_id)
         values (src.workspace_id,
                 src.shipment,
                 src.supplier,
                 src.supplier_name,
                 src.supplier_site_id,
                 src.supplier_site_name,
                 src.order_no,
                 src.asn,
                 src.bill_to_loc,
                 src.bill_to_loc_type,
                 src.bill_to_loc_name,
                 src.location,
                 src.loc_type,
                 src.loc_name,
                 src.status_code,
                 src.invc_match_status,
                 src.ship_date,
                 src.receive_date,
                 src.currency_code,
                 src.total_avail_cost, --merge later
                 src.total_avail_qty, --merge later
                 src.merch_amount, -- merge later
                 src.total_qty,
                 src.choice_flag,
                 REIM_CONSTANTS.ONE,
                 REIM_CONSTANTS.ONE);

      LOGGER.LOG_INFORMATION(L_program||'Insert receipts from Manual Groups - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end loop;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END EXPAND_REFRESH_MTCH_WSPACE;
------------------------------------------------------------------------------------------
/**
 * The public function used to Suggest Match from the UI
 * Input param: I_workspace_id (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION SUGGEST_MATCH(O_error_message    OUT VARCHAR2,
                       O_match_count      OUT NUMBER,
                       I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.SUGGEST_MATCH';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_vdate   DATE         := get_vdate;

   L_match_key_id   IM_MATCH_INVC_WS.MATCH_KEY_ID%TYPE         := im_match_key_id_seq.NEXTVAL;
   L_invc_group_id  IM_MATCH_GROUP_INVC_WS.INVC_GROUP_ID%TYPE  := im_match_invc_group_seq.NEXTVAL;

   L_invc_count      NUMBER       := REIM_CONSTANTS.ZERO;
   L_rcpt_count      NUMBER       := REIM_CONSTANTS.ZERO;
   L_sku_compliant   NUMBER       := REIM_CONSTANTS.ONE;
   L_table_name      VARCHAR2(30) := NULL;
   L_dirty_object    VARCHAR2(30) := NULL;
   L_dirty_rec_count NUMBER       := REIM_CONSTANTS.ZERO;

   invalid_selection EXCEPTION;
   multi_match       EXCEPTION;
   no_match          EXCEPTION;

   L_mtch_groups     OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_FETCH_BEST_MATCH is
      select gh.match_group_id
        from im_match_group_head_ws gh
       where gh.workspace_id      = I_workspace_id
         and gh.match_key_id      = L_match_key_id
         and gh.match_stgy_id     = REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_ID
         and gh.match_stgy_dtl_id = REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID
         and gh.match_type        = REIM_CONSTANTS.MATCH_LEVEL_SUMM_1_2_MANY
         and gh.match_status      = REIM_CONSTANTS.DOC_STATUS_MTCH;

BEGIN

   --Create group head records from invc_group and rcpt_group using match keys.
   --Call rollup cost and qty
   --check tolerance
   --do sku compliance and tax validation if match successful.
   --order or rank and find out the best match
   --update choice_flag on receipt and receipt detail ws
   -- Output number of matched or raise a desired exception.(multi_match)

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:'      || I_workspace_id);

   select count(1)
     into L_invc_count
     from im_match_invc_ws imiw
    where imiw.workspace_id = I_workspace_id
      and imiw.choice_flag  = 'Y'
      and imiw.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH;

   if L_invc_count = REIM_CONSTANTS.ZERO then
      RAISE invalid_selection;
   end if;

   select count(1)
     into L_rcpt_count
     from im_match_rcpt_ws imrw
    where imrw.workspace_id      = I_workspace_id
      and imrw.choice_flag       = 'Y'
      and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH;

   if L_rcpt_count > REIM_CONSTANTS.ZERO then
      RAISE invalid_selection;
   end if;

   --Create Match Key and proceed with match suggestion.
   update im_match_invc_ws imiw
      set imiw.match_key_id = L_match_key_id
    where imiw.workspace_id = I_workspace_id
      and imiw.choice_flag  = 'Y'
      and imiw.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH;

   LOGGER.LOG_INFORMATION(L_program||'Update IM_MATCH_INVC_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_invc_detl_ws tgt
   using (select imiw.workspace_id,
                 imiw.doc_id,
                 imiw.match_key_id
            from im_match_invc_ws imiw
           where imiw.workspace_id = I_workspace_id
             and imiw.match_key_id = L_match_key_id) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.status       = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH)
   when MATCHED THEN
      update
         set tgt.match_key_id = src.match_key_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_INVC_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_ws tgt
   using (select rcpt.workspace_id,
                 rcpt.shipment
            from (select imrw.workspace_id,
                         imrw.shipment,
                         imrw.currency_code,
                         nvl(imrw.loc_set_of_books_id,-1) set_of_books_id,
                         nvl(imrw.loc_vat_region, -1) vat_region
                    from im_match_rcpt_ws imrw
                   where imrw.workspace_id      = I_workspace_id
                     and imrw.choice_flag       = 'N'
                     and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH) rcpt,
                 (select distinct workspace_id,
                         currency_code,
                         nvl(imiw.loc_set_of_books_id,-1) set_of_books_id,
                         nvl(imiw.loc_vat_region, -1) vat_region
                    from im_match_invc_ws imiw
                   where imiw.workspace_id = I_workspace_id
                     and imiw.match_key_id = L_match_key_id) invc
           where rcpt.workspace_id    = invc.workspace_id
             and rcpt.currency_code   = invc.currency_code
             and rcpt.set_of_books_id = invc.set_of_books_id
             and rcpt.vat_region      = invc.vat_region) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment)
   when MATCHED then
      update
         set tgt.match_key_id = L_match_key_id;

   if SQL%ROWCOUNT = REIM_CONSTANTS.ZERO then
      RAISE no_match;
   end if;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_RCPT_WS ');

   merge into im_match_rcpt_detl_ws tgt
   using (select imrw.workspace_id,
                 imrw.shipment,
                 imrw.match_key_id
            from im_match_rcpt_ws imrw
           where imrw.workspace_id = I_workspace_id
             and imrw.match_key_id = L_match_key_id) src
   on (    tgt.workspace_id      = src.workspace_id
       and tgt.shipment          = src.shipment
       and tgt.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH)
   when MATCHED THEN
      update
         set tgt.match_key_id = src.match_key_id;

   LOGGER.LOG_INFORMATION(L_program||'Merge IM_MATCH_RCPT_DETL_WS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_match_group_invc_ws(invc_group_id,
                                      doc_id,
                                      workspace_id,
                                      match_stgy_id,
                                      match_stgy_dtl_id,
                                      chunk_num,
                                      match_luw_id,
                                      match_key_id,
                                      due_date)
                               select L_invc_group_id,
                                      imiw.doc_id,
                                      I_workspace_id,
                                      REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_ID,
                                      REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID,
                                      REIM_CONSTANTS.ONE, -- chunk_num
                                      REIM_CONSTANTS.ONE, -- match_luw_id
                                      imiw.match_key_id,
                                      imiw.due_date
                                 from im_match_invc_ws imiw
                                where imiw.workspace_id = I_workspace_id
                                  and imiw.match_key_id = L_match_key_id;

   LOGGER.LOG_INFORMATION(L_program||'Create Invoice Group - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_AUTO_MATCH_SQL.CREATE_RCPT_GROUPS(O_error_message,
                                             I_workspace_id,
                                             REIM_CONSTANTS.ONE, --I_match_luw_id
                                             L_match_key_id,
                                             REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID,
                                             REIM_CONSTANTS.MATCH_TYPE_BEST) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REIM_AUTO_MATCH_SQL.CREATE_MTCH_GROUP_HEAD(O_error_message,
                                                 I_workspace_id,
                                                 REIM_CONSTANTS.ONE, --I_match_luw_id
                                                 L_match_key_id,
                                                 REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID,
                                                 REIM_CONSTANTS.MATCH_LEVEL_SUMM_1_2_MANY) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REIM_AUTO_MATCH_SQL.MATCH_GROUP_HEAD(O_error_message,
                                           I_workspace_id,
                                           REIM_CONSTANTS.ONE, --I_match_luw_id
                                           L_match_key_id,
                                           REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REIM_AUTO_MATCH_SQL.CHECK_SKU_COMPLIANCE(O_error_message,
                                               I_workspace_id,
                                               REIM_CONSTANTS.ONE, --I_match_luw_id
                                               L_match_key_id,
                                               REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REIM_AUTO_MATCH_SQL.CHECK_TAX_COMPLIANCE(O_error_message,
                                               I_workspace_id,
                                               REIM_CONSTANTS.ONE, --I_match_luw_id
                                               L_match_key_id,
                                               REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --Best Match
   merge into im_match_group_head_ws tgt
   using (select gh.match_group_id,
                 RANK() OVER (PARTITION BY gh.workspace_id,
                                           gh.match_luw_id,
                                           gh.match_key_id,
                                           gh.match_stgy_id,
                                           gh.match_stgy_dtl_id
                                  ORDER BY DECODE(gh.match_status,
                                                  REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                                                  REIM_CONSTANTS.ONE),
                                           ABS(gh.cost_variance),
                                           gh.cost_variance desc,
                                           DECODE(gh.qty_match_required,
                                                  'Y', ABS(gh.qty_variance),
                                                  REIM_CONSTANTS.ONE)) best_match_rank
            from im_match_group_head_ws gh
           where gh.workspace_id      = I_workspace_id
             and gh.match_key_id      = L_match_key_id
             and gh.match_stgy_dtl_id = REIM_CONSTANTS.ONLINE_SGST_MTCH_STGY_DTL_ID
             and gh.match_type        = REIM_CONSTANTS.MATCH_LEVEL_SUMM_1_2_MANY) src
   on (tgt.match_group_id = src.match_group_id)
   when MATCHED then
      update
         set tgt.match_status = DECODE(src.best_match_rank,
                                       REIM_CONSTANTS.ONE, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                       REIM_CONSTANTS.DOC_STATUS_URMTCH);

   LOGGER.LOG_INFORMATION(L_program||'Merge to select Best Match - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_FETCH_BEST_MATCH;
   fetch C_FETCH_BEST_MATCH BULK COLLECT into L_mtch_groups;
   close C_FETCH_BEST_MATCH;

   if L_mtch_groups is NULL or L_mtch_groups.COUNT = REIM_CONSTANTS.ZERO then
      RAISE no_match;
   elsif L_mtch_groups.COUNT > REIM_CONSTANTS.ONE then
      O_match_count := L_mtch_groups.COUNT;
      RAISE multi_match;
   elsif L_mtch_groups.COUNT = REIM_CONSTANTS.ONE then

      merge into im_match_rcpt_ws tgt
      using (select gh.workspace_id,
                    gh.match_key_id,
                    gr.shipment
               from TABLE(CAST(L_mtch_groups as OBJ_NUMERIC_ID_TABLE)) ids,
                    im_match_group_head_ws gh,
                    im_match_group_rcpt_ws gr
              where gh.match_group_id    = value(ids)
                --
                and gr.workspace_id      = gh.workspace_id
                and gr.match_key_id      = gh.match_key_id
                and gr.match_stgy_dtl_id = gh.match_stgy_dtl_id
                and gr.rcpt_group_id     = gh.rcpt_group_id) src
      on (    tgt.workspace_id = src.workspace_id
          and tgt.match_key_id = src.match_key_id
          and tgt.shipment     = src.shipment)
      when matched then
         update
            set tgt.choice_flag = 'Y';

      LOGGER.LOG_INFORMATION(L_program||'Merge Best Match Shipments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when no_match then
      O_error_message := SQL_LIB.CREATE_MSG('NO_MATCH', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when multi_match then
      O_error_message := SQL_LIB.CREATE_MSG('MULTI_MATCH', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when invalid_selection then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_SELECTION', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END SUGGEST_MATCH;
------------------------------------------------------------------------------------------
/**
 * The public function used to perform Split Receipt
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION SPLIT_RECEIPT(O_error_message    OUT VARCHAR2,
                       I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.SPLIT_RECEIPT';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_user    IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
   L_vdate   PERIOD.VDATE%TYPE         := GET_VDATE;

   L_ship_item_qtys_tbl IM_SHIP_ITEM_QTYS_TBL := NULL;

   cursor C_FETCH_SHIP_ITEM_QTY is
      select IM_SHIP_ITEM_QTYS_REC(gtt.number_7,
                                   gtt.varchar2_5,
                                   gtt.number_5)
        from gtt_10_num_10_str_10_date gtt,
             im_invoice_detail iid
       where iid.doc_id = gtt.number_2
         and iid.item   = gtt.varchar2_1
         and iid.status = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: '  || I_workspace_id);

   if L_user is null then
      L_user := get_user;
   end if;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   delete from gtt_10_num_10_str_10_date;

   insert into gtt_10_num_10_str_10_date(number_1,   -- workspace_id
                                         number_2,   -- invoice_id
                                         number_3,   -- qty_variance
                                         number_4,   -- unit_cost_variance
                                         number_5,   -- invoice_qty
                                         number_6,   -- receipt_avail_qty
                                         number_7,   -- receipt_id
                                         --number_8, --IRIP seq_no
                                         varchar2_1, -- display_item
                                         varchar2_2, -- perfect_match_ind
                                         varchar2_3, -- cost_matched
                                         varchar2_4, -- qty_matched
                                         varchar2_5) -- orig_item from shipment
   select idmw.workspace_id,
          idmwi.invoice_id,
          idmw.qty_variance,
          idmw.unit_cost_variance,
          idmwi.invoice_qty,
          idmw.receipt_avail_qty,
          idmwr.receipt_id,
          idmw.item,
          case when idmw.qty_variance = 0 and idmw.unit_cost_variance = 0 then 'Y' else 'N' end,
          DECODE(idmwi.cost_matched,
                 'R', 'Y',
                 'D', 'N',
                 idmwi.cost_matched),
          DECODE(idmwi.qty_matched,
                 'R', 'Y',
                 'D', 'N',
                 idmwi.qty_matched),
          idmwr.item
     from im_detail_match_ws idmw,
          im_detail_match_ws idmwi,
          im_detail_match_ws idmwr
    where idmw.workspace_id  = I_workspace_id
      and idmw.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and idmw.choice_flag   = 'Y'
      and idmw.ui_filter_ind = 'N'
      --
      and idmwi.workspace_id  = I_workspace_id
      and idmwi.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
      and idmwi.choice_flag   = 'Y'
      and idmwi.ui_filter_ind = 'N'
      --
      and idmwi.workspace_id  = idmw.workspace_id
      and idmwi.item          = idmw.item
      --
      and idmwr.workspace_id  = I_workspace_id
      and idmwr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
      and idmwr.choice_flag   = 'Y'
      and idmwr.ui_filter_ind = 'N'
      --
      and idmwr.workspace_id  = idmw.workspace_id
      and idmwr.display_item  = idmw.item;

   LOGGER.LOG_INFORMATION(L_program||' Populate GTT with required information - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Update invoice records
   --------------------------------------

   merge into im_invoice_detail tgt
   using (select gtt.number_1 workspace_id,
                 gtt.number_2 doc_id,
                 gtt.varchar2_1 item,
                 DECODE(gtt.number_4, -- unit_cost_variance
                        REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC) cost_matched
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id        = gtt.number_2
             and iid.item          = gtt.varchar2_1
             and iid.cost_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
             --
             and gtt.varchar2_3      = 'N' -- cost_matched
         ) src
   on (    tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.cost_matched                  = src.cost_matched,
             tgt.last_updated_by               = L_user,
             tgt.last_update_date              = sysdate,
             tgt.object_version_id             = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Cost matched im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_invoice_detail tgt
   using (select gtt.number_1 workspace_id,
                 gtt.number_2 doc_id,
                 gtt.varchar2_1 item,
                 REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES qty_matched,
                 DECODE(iid.cost_matched,
                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
                        REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH) status
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id        = gtt.number_2
             and iid.item          = gtt.varchar2_1
             and iid.qty_matched   IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
             --
             and gtt.varchar2_4      = 'N'  -- qty_matched
         ) src
   on (    tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.qty_matched             = src.qty_matched,
             tgt.status                  = src.status,
             tgt.last_updated_by         = L_user,
             tgt.last_update_date        = sysdate,
             tgt.object_version_id       = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Qty matched im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iid.doc_id,
                 DECODE(SUM(DECODE(iid.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)),
                        REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_STATUS_MTCH,
                        REIM_CONSTANTS.DOC_STATUS_URMTCH) status
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id        = gtt.number_2
           GROUP BY iid.doc_id) src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update
         set tgt.status            = src.status,
             tgt.match_id          = DECODE(src.status,
                                            REIM_CONSTANTS.DOC_STATUS_MTCH, L_user,
                                            NULL),
             tgt.match_date        = DECODE(src.status,
                                            REIM_CONSTANTS.DOC_STATUS_MTCH, L_vdate,
                                            NULL),
             tgt.match_type        = DECODE(src.status,
                                            REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.MATCH_TYPE_MANUAL,
                                            NULL),
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Status on im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Partially Matched Receipts
   --------------------------------------

   merge into im_partially_matched_receipts tgt
   using (select gtt.number_7 shipment,
                 gtt.varchar2_5 item,
                 gtt.number_5 qty_matched --invoice_qty
            from gtt_10_num_10_str_10_date gtt
           where gtt.varchar2_4      = 'N') src
   on (    tgt.shipment     = src.shipment
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.qty_matched       = tgt.qty_matched + src.qty_matched,
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE
   when NOT MATCHED then
      insert (shipment,
              item,
              qty_matched,
              created_by,
              creation_date,
              last_updated_by,
              last_update_date,
              object_version_id)
      values (src.shipment,
              src.item,
              src.qty_matched,
              L_user,
              sysdate,
              L_user,
              sysdate,
              REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_PARTIALLY_MATCHED_RECEIPTS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Shipment
   --------------------------------------

   open C_FETCH_SHIP_ITEM_QTY;
   fetch C_FETCH_SHIP_ITEM_QTY BULK COLLECT into L_ship_item_qtys_tbl;
   close C_FETCH_SHIP_ITEM_QTY;

   if SHIPMENT_API_SQL.UPDATE_INVOICE_QTY_MATCHED(O_error_message,
                                                  L_ship_item_qtys_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --write match history
   --------------------------------------

   delete from gtt_num_num_str_str_date_date;

   --number_1   detail_match_history_seq
   --number_2   doc_id
   --varchar2_1 item
   --varchar2_2 receipt_id
   insert into gtt_num_num_str_str_date_date(number_1,
                                             number_2,
                                             varchar2_1,
                                             varchar2_2)
   select im_detail_match_history_seq.nextval,
          gtt.number_2,
          gtt.varchar2_1,
          gtt.number_7
     from gtt_10_num_10_str_10_date gtt,
          im_invoice_detail iid
    where iid.doc_id = gtt.number_2
      and iid.item   = gtt.varchar2_1
      and iid.status = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH;

   insert into im_detail_match_history (match_id,
                                        auto_matched,
                                        exact_match,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             'N',
             'N',
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_num_num_str_str_date_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_invc_history (match_id,
                                             doc_id,
                                             item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             gtt.number_2,
             gtt.varchar2_1,
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_num_num_str_str_date_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_rcpt_history (match_id,
                                             shipment,
                                             item,
                                             substitute_item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             gtt.varchar2_2,
             gtt.varchar2_1,
             DECODE(dr.item,
                    dr.display_item, NULL,
                    dr.item),
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_num_num_str_str_date_date gtt,
             im_detail_match_ws dr
       where gtt.varchar2_1   = dr.display_item
         and dr.workspace_id  = I_workspace_id
         and dr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
         and dr.choice_flag   = 'Y'
         and dr.ui_filter_ind = 'N';

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to Receipt item posting
   --------------------------------------

   merge into gtt_10_num_10_str_10_date tgt
   using (select iid.doc_id,
                 iid.item
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id = gtt.number_2
             and iid.item   = gtt.varchar2_1
             and iid.status = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH) src
   on (    tgt.number_2 = src.doc_id
       and tgt.varchar2_1 = src.item)
   when MATCHED then
      update
         set number_8 = im_receipt_item_posting_seq.nextval;

   insert into im_receipt_item_posting (seq_no,
                                        shipment,
                                        item,
                                        qty_matched,
                                        qty_posted,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_8,
             gtt.number_7,
             gtt.varchar2_5,
             gtt.number_5,
             NULL,
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_10_num_10_str_10_date gtt
       where gtt.number_8 is NOT NULL;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_rcpt_item_posting_invoice (seq_no,
                                             doc_id,
                                             status,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
   select gtt.number_8,
          gtt.number_2,
          'M',
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_10_num_10_str_10_date gtt
    where gtt.number_8 is NOT NULL;

   LOGGER.LOG_INFORMATION(L_program||' insert im_rcpt_item_posting_invoice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Manage Manual Groups
   --------------------------------------

   if MANAGE_MANUAL_GROUP(O_error_message,
                          I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --refresh the workspace
   --------------------------------------

   if REFRESH_MTCH_WSPACE(O_error_message,
                          I_workspace_id,
                          'N') = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --Recreate Detail Match Workspace
   --------------------------------------

   if CREATE_DETAIL_MATCH_WS(O_error_message,
                             I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REFRESH_DISCREPANCY_LIST_WS(O_error_message,
                                  I_workspace_id) = 0 then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END SPLIT_RECEIPT;
------------------------------------------------------------------------------------------
/**
 * The public function used to perform Online Resolution
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *              I_cost_decision (Decision indicator, 'R' for receipt, 'I' for Invoice, 'M' for Match Within Tolerance and 'D' for deferred, NULL if no resolution performed)
 *              I_qty_decision  (Decision indicator, 'R' for receipt, 'I' for Invoice, 'M' for Match Within Tolerance and 'D' for deferred, NULL if no resolution performed)
 *              I_resln_action_rc_tbl  (List of Reason codes (both cost and qty together) used for resolving the discrepancy)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_ONLINE_RESLN(O_error_message          OUT VARCHAR2,
                              I_workspace_id        IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                              I_cost_decision       IN     IM_INVOICE_DETAIL.COST_MATCHED%TYPE,
                              I_qty_decision        IN     IM_INVOICE_DETAIL.QTY_MATCHED%TYPE,
                              I_resln_action_rc_tbl IN     IM_RESLN_ACTION_RC_TBL)
RETURN NUMBER
IS

   L_program           VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.PERFORM_ONLINE_RESLN';
   L_start_time        TIMESTAMP := SYSTIMESTAMP;

   L_user              IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
   L_vdate             PERIOD.VDATE%TYPE         := GET_VDATE;

   L_adj_cost          IM_INVOICE_DETAIL.UNIT_COST%TYPE := NULL;
   L_dwo_unit_cost_adj IM_INVOICE_DETAIL.UNIT_COST%TYPE := NULL;
   L_cost_rc_count     NUMBER(10)                       := NULL;
   L_cost_rc_action    IM_REASON_CODES.ACTION%TYPE      := NULL;
   L_cost_decision     VARCHAR2(1)                      := NULL;
   --
   L_adj_qty           IM_INVOICE_DETAIL.QTY%TYPE       := NULL;
   L_dwo_qty_adj       IM_INVOICE_DETAIL.QTY%TYPE       := NULL;
   L_qty_rc_count      NUMBER(10)                       := NULL;
   L_qty_rc_action     IM_REASON_CODES.ACTION%TYPE      := NULL;
   L_qty_decision      VARCHAR2(1)                      := NULL;

   L_ship_item_qtys_tbl IM_SHIP_ITEM_QTYS_TBL := NULL;

   cursor C_FETCH_COST_ADJ is
      select SUM(rc.adj_value) adj_val,
             SUM(DECODE(irc.action,
                        REIM_CONSTANTS.RC_ACTION_DWO, NVL(rc.adj_value, REIM_CONSTANTS.ZERO),
                        REIM_CONSTANTS.ZERO)) dwo_unit_cost_adj,
             count(1) adj_count,
             max(irc.action) rc_action
        from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc,
             im_reason_codes irc
       where rc.adj_type        = REIM_CONSTANTS.REASON_CODE_TYPE_COST
         and irc.reason_code_id = rc.reason_code_id;

   cursor C_FETCH_QTY_ADJ is
      select SUM(rc.adj_value) adj_val,
             SUM(DECODE(irc.action,
                        REIM_CONSTANTS.RC_ACTION_DWO, NVL(rc.adj_value, REIM_CONSTANTS.ZERO),
                        REIM_CONSTANTS.ZERO)) dwo_qty_adj,
             count(1) adj_count,
             max(irc.action) rc_action
        from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc,
             im_reason_codes irc
       where rc.adj_type        = REIM_CONSTANTS.REASON_CODE_TYPE_QTY
         and irc.reason_code_id = rc.reason_code_id;

   cursor C_FETCH_RC_TBL_4_DEBUG is
      select rc.reason_code_id || '~' || rc.rc_comment || '~' || rc.adj_type || '~' || rc.adj_value || '~' as text
        from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc;

   cursor C_FETCH_SHIP_ITEM_QTY is
      select IM_SHIP_ITEM_QTYS_REC(ssku.shipment,
                                   ssku.item,
                                   ssku.qty_available)
        from (select inner.shipment,
                     inner.item,
                     inner.qty_available + DECODE(inner.ship_rank,
                                                  REIM_CONSTANTS.ONE, inner.qty_adjusted,
                                                  REIM_CONSTANTS.ZERO) qty_available
                from (select idmwr.receipt_id shipment,
                             idmwr.item,
                             DECODE(gtt.varchar2_4,
                                    REIM_CONSTANTS.YN_YES, idmwr.receipt_received_qty - idmwr.receipt_avail_qty,
                                    idmwr.receipt_avail_qty) qty_available,
                             DECODE(L_qty_decision,
                                    REIM_CONSTANTS.RESLN_DECISION_INVC, -1 * (gtt.number_3 - NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO)),
                                    REIM_CONSTANTS.ZERO) qty_adjusted,
                             rank() over (partition by idmwr.workspace_id, idmwr.display_item, idmwr.match_key_id
                                              order by DECODE(idmwr.item,idmwr.display_item,1,2), idmwr.receipt_id) ship_rank
                        from gtt_10_num_10_str_10_date gtt,
                             im_detail_match_ws idmwr,
                             im_invoice_detail iid
                       where idmwr.workspace_id          = gtt.number_1
                         and idmwr.display_item          = gtt.varchar2_1
                         and idmwr.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                         and NVL(idmwr.match_key_id, -1) = gtt.number_7
                         and idmwr.choice_flag           = 'Y'
                         and idmwr.ui_filter_ind         = 'N'
                         --
                         and iid.doc_id                  = gtt.number_2
                         and iid.item                    = gtt.varchar2_1
                         and iid.status                  = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH) inner) ssku;

BEGIN

   LOGGER.LOG_INFORMATION(L_program
                          || ' I_workspace_id: '  || I_workspace_id
                          || ' I_cost_decision: ' || I_cost_decision
                          || ' I_qty_decision: '  || I_qty_decision);

   LOGGER.LOG_INFORMATION('Begin i/p tbl logging');
   for rec in C_FETCH_RC_TBL_4_DEBUG loop
      LOGGER.LOG_INFORMATION('rec.text: ' || rec.text);
   end loop;
   LOGGER.LOG_INFORMATION('End i/p tbl logging');

   if L_user is null then
      L_user := get_user;
   end if;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   open C_FETCH_COST_ADJ;
   fetch C_FETCH_COST_ADJ into L_adj_cost,
                               L_dwo_unit_cost_adj,
                               L_cost_rc_count,
                               L_cost_rc_action;
   close C_FETCH_COST_ADJ;

   open C_FETCH_QTY_ADJ;
   fetch C_FETCH_QTY_ADJ into L_adj_qty,
                              L_dwo_qty_adj,
                              L_qty_rc_count,
                              L_qty_rc_action;
   close C_FETCH_QTY_ADJ;

   --Reverse the decisions if DWO was used for Mass Resolution and the decision was 'I'
   if I_cost_decision = REIM_CONSTANTS.RESLN_DECISION_INVC and L_cost_rc_count = REIM_CONSTANTS.ONE and L_cost_rc_action = REIM_CONSTANTS.RC_ACTION_DWO and L_adj_cost is NULL then
      L_cost_decision := REIM_CONSTANTS.RESLN_DECISION_RCPT;
   elsif I_cost_decision is NULL or I_cost_decision IN (REIM_CONSTANTS.RESLN_DECISION_RCPT, REIM_CONSTANTS.RESLN_DECISION_INVC, REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.RESLN_DECISION_DEFR) then
      L_cost_decision := I_cost_decision;
   else
      O_error_message := 'Invalid Cost Decision: ' || I_cost_decision;
      return REIM_CONSTANTS.FAIL;
   end if;

   if I_qty_decision = REIM_CONSTANTS.RESLN_DECISION_INVC and L_qty_rc_count = REIM_CONSTANTS.ONE and L_qty_rc_action = REIM_CONSTANTS.RC_ACTION_DWO and L_adj_qty is NULL then
      L_qty_decision := REIM_CONSTANTS.RESLN_DECISION_RCPT;
   elsif I_qty_decision is NULL or I_qty_decision IN (REIM_CONSTANTS.RESLN_DECISION_RCPT, REIM_CONSTANTS.RESLN_DECISION_INVC, REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.RESLN_DECISION_DEFR) then
      L_qty_decision := I_qty_decision;
   else
      O_error_message := 'Invalid Qty Decision: ' || I_qty_decision;
      return REIM_CONSTANTS.FAIL;
   end if;

   --segregate items to be resolved and items which are perfect match
   delete from gtt_10_num_10_str_10_date;

   insert into gtt_10_num_10_str_10_date(number_1,   -- workspace_id
                                      number_2,   -- invoice_id
                                      number_3,   -- qty_variance
                                      number_4,   -- unit_cost_variance
                                      number_5,   -- invoice_qty
                                      number_6,   -- receipt_avail_qty
                                         number_7,   -- match_key_id
                                      varchar2_1, -- item
                                      varchar2_2, -- perfect_match_ind
                                      varchar2_3, -- cost_matched
                                      varchar2_4) -- qty_matched
   with im_detl_match_ws as (select workspace_id,
                                    item
                               from im_detail_match_ws
                              where workspace_id  = I_workspace_id
                                and entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                                and choice_flag   = 'Y'
                                and ui_filter_ind = 'N'
                              GROUP BY workspace_id,
                                       item),
        im_detl_match_invc_ws as (select workspace_id,
                                         invoice_id,
                                         sum(invoice_qty) invc_qty,
                                         min(invoice_unit_cost) invc_unit_cost,
                                         item invc_item,
                                         DECODE(cost_matched,
                 REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                 REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC,  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                                cost_matched) cost_matched,
                                         DECODE(qty_matched,
                 REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                 REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC,  REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                                qty_matched) qty_matched,
                                         NVL(match_key_id, -1) match_key_id
                                    from im_detail_match_ws
                                   where workspace_id  = I_workspace_id
                                     and entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                                     and choice_flag   = 'Y'
                                     and ui_filter_ind = 'N'
                                   GROUP BY workspace_id,
                                            invoice_id,
                                            item,
                                            cost_matched,
                                            qty_matched,
                                            match_key_id),
        im_detl_match_rcpt_ws as (select workspace_id,
                                         sum(receipt_avail_qty) rcpt_qty,
                                         min(receipt_unit_cost) rcpt_unit_cost,
                                         display_item rcpt_item,
                                         NVL(match_key_id, -1) match_key_id
                                    from im_detail_match_ws
                                   where workspace_id  = I_workspace_id
                                     and entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                                     and choice_flag   = 'Y'
                                     and ui_filter_ind = 'N'
                                   GROUP BY workspace_id,
                                            display_item,
                                            match_key_id)
   select idmwi.workspace_id,
          idmwi.invoice_id,
          NVL(L_adj_qty, (NVL(idmwr.rcpt_qty, 0) - idmwi.invc_qty)),
          NVL(L_adj_cost, (NVL(idmwr.rcpt_unit_cost, 0) - idmwi.invc_unit_cost)),
          idmwi.invc_qty,
          NVL(idmwr.rcpt_qty, 0),
          idmwi.match_key_id,
          idmwi.invc_item,
          case when (NVL(idmwr.rcpt_qty, 0) - idmwi.invc_qty) = 0
                and (NVL(idmwr.rcpt_unit_cost, 0) - idmwi.invc_unit_cost) = 0
              then 'Y'
              else 'N'
           end,
          idmwi.cost_matched,
          idmwi.qty_matched
     from im_detl_match_ws idmw,
          im_detl_match_invc_ws idmwi,
          im_detl_match_rcpt_ws idmwr
    where idmwi.workspace_id     = idmw.workspace_id
      and idmwi.invc_item        = idmw.item
      and idmwr.workspace_id (+) = idmwi.workspace_id
      and idmwr.match_key_id (+) = idmwi.match_key_id
      and idmwr.rcpt_item (+)    = idmwi.invc_item;

   LOGGER.LOG_INFORMATION(L_program||' Segregate items to be resolved - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Write resolution and comments records
   --------------------------------------

   insert into im_resolution_action(doc_id,
                                    item,
                                    reason_code_id,
                                    action,
                                    qty,
                                    unit_cost,
                                    extended_cost,
                                    status,
                                    shipment,
                                    created_by,
                                    creation_date,
                                    last_updated_by,
                                    last_update_date,
                                    object_version_id)
   --Resolutions other than RUA
   select gtt.number_2,
          gtt.varchar2_1, --item,
          rc_tbl.reason_code_id,
          irc.action,
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_QTY, NVL(rc_tbl.adj_value, gtt.number_3),
                 NULL) qty,
          case when irc.action IN (REIM_CONSTANTS.RC_ACTION_RCA,
                                   REIM_CONSTANTS.RC_ACTION_RCAS,
                                   REIM_CONSTANTS.RC_ACTION_RCAMR,
                                   REIM_CONSTANTS.RC_ACTION_RCAMRS) then -1 else 1 end *
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_COST, NVL(rc_tbl.adj_value, gtt.number_4),
                 NULL) unit_cost,
          case when irc.action IN (REIM_CONSTANTS.RC_ACTION_RCA,
                                   REIM_CONSTANTS.RC_ACTION_RCAS,
                                   REIM_CONSTANTS.RC_ACTION_RCAMR,
                                   REIM_CONSTANTS.RC_ACTION_RCAMRS) then -1 else 1 end *
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_COST, NVL(rc_tbl.adj_value, gtt.number_4) * gtt.number_5,
                 NULL) extended_cost,
          case when irc.action IN (REIM_CONSTANTS.RC_ACTION_RCA,
                                   REIM_CONSTANTS.RC_ACTION_RCAS,
                                   REIM_CONSTANTS.RC_ACTION_RCAMR,
                                   REIM_CONSTANTS.RC_ACTION_RCAMRS) then REIM_CONSTANTS.RCA_STATUS_ROLLEDUP else REIM_CONSTANTS.RCA_STATUS_UNROLLED end,
          NULL, --Shipment
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc_tbl,
          im_reason_codes irc,
          gtt_10_num_10_str_10_date gtt
    where irc.reason_code_id = rc_tbl.reason_code_id
      and irc.action         <> REIM_CONSTANTS.RC_ACTION_RUA
      and gtt.varchar2_2     = 'N' -- Perfect match Ind
      and (   (    irc.reason_code_type = REIM_CONSTANTS.REASON_CODE_TYPE_QTY
               and gtt.number_3         <> REIM_CONSTANTS.ZERO)
           or (    irc.reason_code_type = REIM_CONSTANTS.REASON_CODE_TYPE_COST
               and gtt.number_4         <> REIM_CONSTANTS.ZERO))
   union all
   --RUA Resolutions
   select gtt.number_2,
          gtt.varchar2_1, --item,
          rc_tbl.reason_code_id,
          irc.action,
          -1 *
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_QTY, NVL(rc_tbl.adj_value, gtt.number_3),
                 NULL) qty,
          -1 *
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_COST, NVL(rc_tbl.adj_value, gtt.number_4),
                 NULL) unit_cost,
          -1 *
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_COST, NVL(rc_tbl.adj_value, gtt.number_4) * gtt.number_5,
                 NULL) extended_cost,
          REIM_CONSTANTS.RCA_STATUS_ROLLEDUP end,
          idmwr.receipt_id,
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc_tbl,
          im_reason_codes irc,
          gtt_10_num_10_str_10_date gtt,
          (select workspace_id,
                  min(receipt_id) receipt_id,
                  display_item,
                  match_key_id
             from im_detail_match_ws
            where workspace_id  = I_workspace_id
              and entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
              and choice_flag   = 'Y'
              and ui_filter_ind = 'N'
            group by workspace_id,
                     display_item,
                     match_key_id) idmwr
    where irc.reason_code_id      = rc_tbl.reason_code_id
      and gtt.varchar2_2          = 'N' -- Perfect match Ind
      and (   (    irc.reason_code_type = REIM_CONSTANTS.REASON_CODE_TYPE_QTY
               and gtt.number_3         <> REIM_CONSTANTS.ZERO)
           or (    irc.reason_code_type = REIM_CONSTANTS.REASON_CODE_TYPE_COST
               and gtt.number_4         <> REIM_CONSTANTS.ZERO))
      and irc.action              = REIM_CONSTANTS.RC_ACTION_RUA
      and idmwr.workspace_id           = I_workspace_id
      and idmwr.display_item           = gtt.varchar2_1
      and NVL(idmwr.match_key_id, -1)  = gtt.number_7;

   LOGGER.LOG_INFORMATION(L_program||' Insert resolution_actions - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_doc_detail_comments (comment_id,
                                       comment_type,
                                       text,
                                       created_by,
                                       creation_date,
                                       doc_id,
                                       item,
                                       discrepancy_type,
                                       reason_code_id,
                                       debit_reason_code,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
   select im_doc_detail_comments_seq.NEXTVAL,
          REIM_CONSTANTS.CMNT_TYPE_EXTERNAL,
          rc_tbl.rc_comment,
          L_user,
          L_vdate,
          gtt.number_2,
          gtt.varchar2_1,
          DECODE(irc.reason_code_type,
                 REIM_CONSTANTS.REASON_CODE_TYPE_COST, REIM_CONSTANTS.CMNT_DISC_TYPE_COST,
                 REIM_CONSTANTS.CMNT_DISC_TYPE_QTY) discrepancy_type,
          rc_tbl.reason_code_id,
          NULL debit_reason_code,
          L_user,
          L_vdate,
          REIM_CONSTANTS.ONE
     from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc_tbl,
          im_reason_codes irc,
          gtt_10_num_10_str_10_date gtt
    where irc.reason_code_id  = rc_tbl.reason_code_id
      and gtt.varchar2_2      = 'N' -- Perfect match Ind
      and rc_tbl.rc_comment   is NOT NULL;

   LOGGER.LOG_INFORMATION(L_program||' Insert doc_detail_comments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Write RCA/RUA records
   --------------------------------------

  merge INTO im_receiver_cost_adjust tgt USING
	(SELECT DISTINCT oh.order_no,
 	 idmwr.item,
  MIN(ol.location) over (partition BY sh.shipment, oh.order_no, idmwr.item) location,
  oh.supplier,
  idmwr.receipt_unit_cost - (gtt.number_4 - NVL(L_dwo_unit_cost_adj, REIM_CONSTANTS.ZERO)) adjusted_unit_cost,
  oh.currency_code,
  rc_tbl.rc_comment, --comment
  DECODE(irc.action, REIM_CONSTANTS.RC_ACTION_RCA, REIM_CONSTANTS.RCA_TYPE_POR, REIM_CONSTANTS.RC_ACTION_RCAS, REIM_CONSTANTS.RCA_TYPE_PORS, REIM_CONSTANTS.RC_ACTION_RCAMR, REIM_CONSTANTS.RCA_TYPE_POMR, REIM_CONSTANTS.RC_ACTION_RCAMRS, REIM_CONSTANTS.RCA_TYPE_POMRS) reason_type,
  rc_tbl.reason_code_id,
  MIN(idmwr.receipt_id) over (partition BY oh.order_no, idmwr.item) shipment
FROM TABLE(CAST(I_resln_action_rc_tbl AS IM_RESLN_ACTION_RC_TBL)) rc_tbl,
  im_reason_codes irc,
  gtt_10_num_10_str_10_date gtt,
  im_detail_match_ws idmwr,
  shipment sh,
  ordhead oh,
  ---
  (
  SELECT wh loc,
    physical_wh phy_loc,
    REIM_CONSTANTS.LOC_TYPE_WH loc_type
  FROM wh
  WHERE physical_wh != wh
  UNION ALL
  SELECT store loc,
    store phy_loc,
    REIM_CONSTANTS.LOC_TYPE_STORE loc_type
  FROM store
  ) loc,
  ---
  ordloc ol
WHERE irc.reason_code_id        = rc_tbl.reason_code_id
AND irc.action                 IN (REIM_CONSTANTS.RC_ACTION_RCA, REIM_CONSTANTS.RC_ACTION_RCAS, REIM_CONSTANTS.RC_ACTION_RCAMR, REIM_CONSTANTS.RC_ACTION_RCAMRS)
AND gtt.varchar2_3              = 'N' -- cost_matched
AND idmwr.workspace_id          = I_workspace_id
AND idmwr.display_item          = gtt.varchar2_1
AND idmwr.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
AND NVL(idmwr.match_key_id, -1) = gtt.number_7
AND idmwr.choice_flag           = 'Y'
AND idmwr.ui_filter_ind         = 'N'
AND sh.shipment                 = idmwr.receipt_id
AND oh.order_no                 = sh.order_no
AND sh.to_loc                   = loc.phy_loc
AND loc.loc                     = ol.location
AND ol.order_no                 = oh.order_no
AND ol.item                     = idmwr.item
UNION ALL
SELECT oh.order_no,
  gtt.varchar2_1,
  MIN(ol.location) over (partition BY oh.order_no, gtt.varchar2_1) location,
  oh.supplier,
  ol.unit_cost - (gtt.number_4 - NVL(L_dwo_unit_cost_adj, REIM_CONSTANTS.ZERO)) adjusted_unit_cost,
  oh.currency_code,
  rc_tbl.rc_comment, --comment
  DECODE(irc.action, REIM_CONSTANTS.RC_ACTION_RCA, REIM_CONSTANTS.RCA_TYPE_POR, REIM_CONSTANTS.RC_ACTION_RCAS, REIM_CONSTANTS.RCA_TYPE_PORS, REIM_CONSTANTS.RC_ACTION_RCAMR, REIM_CONSTANTS.RCA_TYPE_POMR, REIM_CONSTANTS.RC_ACTION_RCAMRS, REIM_CONSTANTS.RCA_TYPE_POMRS) reason_type,
  rc_tbl.reason_code_id,
  -1 shipment
FROM TABLE(CAST(I_resln_action_rc_tbl AS IM_RESLN_ACTION_RC_TBL)) rc_tbl,
  im_reason_codes irc,
  gtt_10_num_10_str_10_date gtt,
  im_doc_head idh,
  ordhead oh,
  ---
  (
  SELECT wh loc,
    physical_wh phy_loc,
    REIM_CONSTANTS.LOC_TYPE_WH loc_type
  FROM wh
  WHERE physical_wh != wh
  UNION ALL
  SELECT store loc,
    store phy_loc,
    REIM_CONSTANTS.LOC_TYPE_STORE loc_type
  FROM store
  ) loc,
  ---
  ordloc ol
WHERE irc.reason_code_id = rc_tbl.reason_code_id
AND irc.action          IN (REIM_CONSTANTS.RC_ACTION_RCA, REIM_CONSTANTS.RC_ACTION_RCAS, REIM_CONSTANTS.RC_ACTION_RCAMR, REIM_CONSTANTS.RC_ACTION_RCAMRS)
AND gtt.varchar2_3       = 'N' -- cost_matched
AND idh.doc_id           = gtt.number_2
AND oh.order_no          = idh.order_no
AND idh.location         = loc.phy_loc
AND loc.loc              = ol.location
AND ol.order_no          = oh.order_no
AND ol.item              = gtt.varchar2_1
AND NOT EXISTS
  (SELECT 'x'
  FROM im_detail_match_ws idmwr
  WHERE idmwr.workspace_id        = I_workspace_id
  AND idmwr.display_item          = gtt.varchar2_1
  AND idmwr.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
  AND NVL(idmwr.match_key_id, -1) = gtt.number_7
  AND idmwr.choice_flag           = 'Y'
  AND idmwr.ui_filter_ind         = 'N'
  )
) src ON ( tgt.order_no = src.order_no AND tgt.item = src.item AND tgt.location = src.location)
WHEN MATCHED THEN
  UPDATE
  SET tgt.adjusted_unit_cost = src.adjusted_unit_cost,
    tgt.type                 = src.reason_type,
    tgt.comments             = src.rc_comment,
    tgt.reason_code_id       = src.reason_code_id,
    tgt.last_updated_by      = L_user,
    tgt.last_update_date     = sysdate,
    tgt.object_version_id    = tgt.object_version_id + REIM_CONSTANTS.ONE WHEN NOT MATCHED THEN
  INSERT
    (
      order_no,
      item,
      location,
      supplier,
      adjusted_unit_cost,
      currency_code,
      comments,
      TYPE,
      reason_code_id,
      created_by,
      shipment,
      creation_date,
      last_updated_by,
      last_update_date,
      object_version_id
    )
    VALUES
    (
      src.order_no,
      src.item,
      src.location,
      src.supplier,
      src.adjusted_unit_cost,
      src.currency_code,
      src.rc_comment,
      src.reason_type,
      src.reason_code_id,
      L_user,
      src.shipment,
      L_vdate,
      L_user,
      L_vdate,
      REIM_CONSTANTS.ONE
    );

   LOGGER.LOG_INFORMATION(L_program||' Insert receiver cost adjust rows - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_receiver_unit_adjust (shipment,
                                        item,
                                        seq_no,
                                        adjusted_item_qty,
                                        comments,
                                        reason_code_id,
                                        created_by,
                                        location,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
   select idmwr.receipt_id,
          idmwr.item,
          REIM_CONSTANTS.ONE,
          -1 * (gtt.number_3 - NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO)),
          rc_tbl.rc_comment, --comments
          rc_tbl.reason_code_id,
          L_user,
          sh.to_loc,
          L_vdate,
          L_user,
          L_vdate,
          REIM_CONSTANTS.ONE
     from table(cast(I_resln_action_rc_tbl as IM_RESLN_ACTION_RC_TBL)) rc_tbl,
          im_reason_codes irc,
          gtt_10_num_10_str_10_date gtt,
          (select workspace_id,
                  receipt_id,
                  item,
                  display_item,
                  NVL(match_key_id, -1) match_key_id,
                  rank() over (partition by workspace_id, display_item, match_key_id
                                   order by DECODE(item,display_item,1,2), receipt_id) ship_rank
             from im_detail_match_ws
            where workspace_id  = I_workspace_id
              and entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
              and choice_flag   = 'Y'
              and ui_filter_ind = 'N') idmwr, -- we have 2 rcpts with diff items but same display_item, adjust the one with the same item if possible
          shipment sh
    where irc.reason_code_id  = rc_tbl.reason_code_id
      and irc.action          = REIM_CONSTANTS.RC_ACTION_RUA
      and gtt.varchar2_4      = 'N' -- qty_matched
      and idmwr.workspace_id  = I_workspace_id
      and idmwr.display_item  = gtt.varchar2_1
      and idmwr.match_key_id  = gtt.number_7
      and idmwr.ship_rank     = REIM_CONSTANTS.ONE
      and sh.shipment         = idmwr.receipt_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert receiver unit adjust rows - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Update invoice records
   --------------------------------------

   merge into im_invoice_detail tgt
   using (select gtt.number_1 workspace_id,
                 gtt.number_2 doc_id,
                 gtt.varchar2_1 item,
                 DECODE(L_cost_decision,
                        REIM_CONSTANTS.RESLN_DECISION_RCPT, gtt.number_4,
                        REIM_CONSTANTS.RESLN_DECISION_INVC, NVL(L_dwo_unit_cost_adj, REIM_CONSTANTS.ZERO),
                        REIM_CONSTANTS.ZERO) adj_val,
                 DECODE(L_cost_decision,
                        REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, gtt.number_4,
                        REIM_CONSTANTS.ZERO) cost_vwt,
                 DECODE(L_cost_decision,
                        REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                        DECODE(gtt.number_4, -- unit_cost_variance
                               REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD)) cost_matched,
                 DECODE(gtt.varchar2_2,
                        'Y', iid.qty_matched,
                        DECODE(iid.qty_matched,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC,
                               iid.qty_matched)) qty_matched,
                 DECODE(iid.qty_matched,
                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
                        REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH) status,
                 'N' adjustment_pending
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id        = gtt.number_2
             and iid.item          = gtt.varchar2_1
             and iid.cost_matched  IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
             --
             and gtt.varchar2_3      = 'N'
             and (   gtt.varchar2_2  = 'Y' --perfect_match_ind
                  or NVL(L_cost_decision, 'N') <> REIM_CONSTANTS.RESLN_DECISION_DEFR
                  or gtt.number_4               = REIM_CONSTANTS.ZERO) -- Zero cost variance
         ) src
   on (    tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.resolution_adjusted_unit_cost  = tgt.resolution_adjusted_unit_cost + src.adj_val,
             tgt.cost_variance_within_tolerance = src.cost_vwt,
             tgt.cost_matched                   = src.cost_matched,
             tgt.qty_matched                    = src.qty_matched,
             tgt.status                         = src.status,
             tgt.adjustment_pending             = src.adjustment_pending,
             tgt.last_updated_by                = L_user,
             tgt.last_update_date               = sysdate,
             tgt.object_version_id              = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Cost resolution/vwt on im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iid.doc_id,
                 SUM(DECODE(L_cost_decision,
                            REIM_CONSTANTS.RESLN_DECISION_RCPT, gtt.number_4 * iid.resolution_adjusted_qty,
                            REIM_CONSTANTS.RESLN_DECISION_INVC, NVL(L_dwo_unit_cost_adj, REIM_CONSTANTS.ZERO) * iid.resolution_adjusted_qty,
                            REIM_CONSTANTS.ZERO)) adj_val
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id        = gtt.number_2
             and iid.item          = gtt.varchar2_1
             --
             and gtt.varchar2_3      = 'N' --cost_matched
             and (   gtt.varchar2_2  = 'Y' --perfect_match_ind
                  or NVL(L_cost_decision, 'N') <> REIM_CONSTANTS.RESLN_DECISION_DEFR
                  or gtt.number_4               = REIM_CONSTANTS.ZERO)
           GROUP BY iid.doc_id) src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update
         set tgt.resolution_adjusted_total_cost = tgt.resolution_adjusted_total_cost + src.adj_val,
             tgt.last_updated_by                = L_user,
             tgt.last_update_date               = sysdate,
             tgt.object_version_id              = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Cost resolution im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_invoice_detail tgt
   using (select gtt.number_1 workspace_id,
                 gtt.number_2 doc_id,
                 gtt.varchar2_1 item,
                 DECODE(L_qty_decision,
                        REIM_CONSTANTS.RESLN_DECISION_RCPT, gtt.number_3,
                        REIM_CONSTANTS.RESLN_DECISION_INVC, NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO),
                        REIM_CONSTANTS.ZERO) adj_val,
                 DECODE(L_qty_decision,
                        REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, gtt.number_3,
                        REIM_CONSTANTS.ZERO) qty_vwt,
                 DECODE(L_qty_decision,
                        REIM_CONSTANTS.RESLN_DECISION_MTCH_WT, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                        DECODE(gtt.number_3, -- qty_variance
                               REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES,
                               REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD)) qty_matched,
                 DECODE(iid.cost_matched,
                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO, REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC,
                        iid.cost_matched) cost_matched,
                 DECODE(iid.cost_matched,
                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_RSLVD, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
                        REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_YES, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
                        REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH) status,
                 'N' adjustment_pending
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id        = gtt.number_2
             and iid.item          = gtt.varchar2_1
             and iid.qty_matched   IN (REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_NO,
                                       REIM_CONSTANTS.DOC_ITEM_MTCH_STAT_DISC)
             --
             and gtt.varchar2_4      = 'N' --qty_matched
             and (   gtt.varchar2_2             = 'Y' --perfect_match_ind
                  or NVL(L_qty_decision, 'N')  <> REIM_CONSTANTS.RESLN_DECISION_DEFR
                  or gtt.number_3               = REIM_CONSTANTS.ZERO) -- Zero Qty Variance
         ) src
   on (    tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.resolution_adjusted_qty       = tgt.resolution_adjusted_qty + src.adj_val,
             tgt.qty_variance_within_tolerance = src.qty_vwt,
             tgt.qty_matched                   = src.qty_matched,
             tgt.cost_matched                  = src.cost_matched,
             tgt.status                        = src.status,
             tgt.adjustment_pending            = src.adjustment_pending,
             tgt.last_updated_by               = L_user,
             tgt.last_update_date              = sysdate,
             tgt.object_version_id             = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Qty resolution/vwt on im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iid.doc_id,
                 SUM(DECODE(L_qty_decision,
                            REIM_CONSTANTS.RESLN_DECISION_RCPT, gtt.number_3,
                            REIM_CONSTANTS.RESLN_DECISION_INVC, NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO),
                            REIM_CONSTANTS.ZERO)) adj_qty,
                 SUM(DECODE(L_qty_decision,
                            REIM_CONSTANTS.RESLN_DECISION_RCPT, gtt.number_3 * iid.resolution_adjusted_unit_cost,
                            REIM_CONSTANTS.RESLN_DECISION_INVC, NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO) * iid.resolution_adjusted_unit_cost,
                            REIM_CONSTANTS.ZERO)) adj_total_cost
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id        = gtt.number_2
             and iid.item          = gtt.varchar2_1
             --
             and gtt.varchar2_4      = 'N'
             and (   gtt.varchar2_2  = 'Y' --perfect_match_ind
                  or NVL(L_qty_decision, 'N')  <> REIM_CONSTANTS.RESLN_DECISION_DEFR
                  or gtt.number_3               = REIM_CONSTANTS.ZERO)
           GROUP BY iid.doc_id) src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update
         set tgt.resolution_adjusted_total_qty = tgt.resolution_adjusted_total_qty + src.adj_qty,
             tgt.resolution_adjusted_total_cost = tgt.resolution_adjusted_total_cost + src.adj_total_cost,
             tgt.last_updated_by               = L_user,
             tgt.last_update_date              = sysdate,
             tgt.object_version_id             = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Qty resolution im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iid.doc_id,
                 DECODE(SUM(DECODE(iid.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)),
                        REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_STATUS_MTCH,
                        REIM_CONSTANTS.DOC_STATUS_URMTCH) status
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id        = gtt.number_2
           GROUP BY iid.doc_id) src
   on (tgt.doc_id = src.doc_id)
   when MATCHED then
      update
         set tgt.status            = src.status,
             tgt.match_id          = DECODE(src.status,
                                            REIM_CONSTANTS.DOC_STATUS_MTCH, L_user,
                                            NULL),
             tgt.match_date        = DECODE(src.status,
                                            REIM_CONSTANTS.DOC_STATUS_MTCH, L_vdate,
                                            NULL),
             tgt.match_type        = DECODE(src.status,
                                            REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.MATCH_TYPE_MANUAL,
                                            NULL),
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' Merge Status on im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Partially Matched Receipts
   --------------------------------------

   merge into im_partially_matched_receipts tgt
   using (select idmwr.receipt_id shipment,
                 idmwr.item,
                 idmwr.receipt_avail_qty qty_available,
                 DECODE(L_qty_decision,
                        REIM_CONSTANTS.RESLN_DECISION_INVC, -1 * (gtt.number_3 - NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO)),
                        REIM_CONSTANTS.ZERO) qty_adjusted,
                 rank() over (partition by idmwr.workspace_id, idmwr.display_item, idmwr.match_key_id
                                   order by DECODE(idmwr.item,idmwr.display_item,1,2), idmwr.receipt_id) ship_rank
            from gtt_10_num_10_str_10_date gtt,
                 im_detail_match_ws idmwr
           where idmwr.workspace_id      = gtt.number_1
             and idmwr.display_item      = gtt.varchar2_1
             and idmwr.entity_type       = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and NVL(idmwr.match_key_id, -1)  = gtt.number_7
             and idmwr.choice_flag       = 'Y'
             and idmwr.ui_filter_ind     = 'N'
             and idmwr.receipt_avail_qty <> REIM_CONSTANTS.ZERO
             --
             and gtt.varchar2_4      = 'N'
             and (   gtt.varchar2_2  = 'Y' --perfect_match_ind
                  or NVL(L_qty_decision, 'N') <> REIM_CONSTANTS.RESLN_DECISION_DEFR)) src
   on (    tgt.shipment     = src.shipment
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.qty_matched       = tgt.qty_matched + src.qty_available + DECODE(src.ship_rank,
                                                                                  REIM_CONSTANTS.ONE, qty_adjusted,
                                                                                  REIM_CONSTANTS.ZERO),
             tgt.last_updated_by   = L_user,
             tgt.last_update_date  = sysdate,
             tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE
   when NOT MATCHED then
      insert (shipment,
              item,
              qty_matched,
              created_by,
              creation_date,
              last_updated_by,
              last_update_date,
              object_version_id)
      values (src.shipment,
              src.item,
              src.qty_available + DECODE(src.ship_rank,
                                         REIM_CONSTANTS.ONE, qty_adjusted,
                                         REIM_CONSTANTS.ZERO),
              L_user,
              sysdate,
              L_user,
              sysdate,
              REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' Merge IM_PARTIALLY_MATCHED_RECEIPTS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Shipment
   --------------------------------------

   open C_FETCH_SHIP_ITEM_QTY;
   fetch C_FETCH_SHIP_ITEM_QTY BULK COLLECT into L_ship_item_qtys_tbl;
   close C_FETCH_SHIP_ITEM_QTY;

   if SHIPMENT_API_SQL.UPDATE_INVOICE_QTY_MATCHED(O_error_message,
                                                  L_ship_item_qtys_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   LOGGER.LOG_INFORMATION(L_program||' Merge QTY_MATCHED SHIPSKU ');

   merge into shipsku tgt
   using (select idmwr.receipt_id shipment,
                 idmwr.item,
                 ss.seq_no
            from gtt_10_num_10_str_10_date gtt,
                 im_detail_match_ws idmwr,
                 shipsku ss
           where idmwr.workspace_id  = gtt.number_1
             and idmwr.display_item  = gtt.varchar2_1
             and idmwr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and NVL(idmwr.match_key_id, -1) = gtt.number_7
             and idmwr.choice_flag   = 'Y'
             and idmwr.ui_filter_ind = 'N'
             --
             and ss.shipment                                                     = idmwr.receipt_id
             and ss.item                                                         = idmwr.item
             and NVL(ss.invc_match_status, REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH) = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
             and ss.qty_received                                                 = NVL(ss.qty_matched, REIM_CONSTANTS.ZERO)) src
   on (    tgt.shipment     = src.shipment
       and tgt.item         = src.item
       and tgt.seq_no       = src.seq_no)
   when MATCHED then
      update
         set tgt.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH;

   LOGGER.LOG_INFORMATION(L_program||' Merge INVC_MATCH_STATUS SHIPSKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into shipment tgt
   using (select ss.shipment,
                 SUM(DECODE(ss.invc_match_status,
                            REIM_CONSTANTS.SSKU_IM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) unmatch_count
            from gtt_10_num_10_str_10_date gtt,
                 im_detail_match_ws idmwr,
                 shipsku ss
           where idmwr.workspace_id  = gtt.number_1
             and idmwr.display_item  = gtt.varchar2_1
             and idmwr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and NVL(idmwr.match_key_id, -1) = gtt.number_7
             and idmwr.choice_flag   = 'Y'
             and idmwr.ui_filter_ind = 'N'
             --
             and ss.shipment          = idmwr.receipt_id
           GROUP BY ss.shipment) src
   on (tgt.shipment = src.shipment)
   when MATCHED then
      update
         set tgt.invc_match_status = DECODE(src.unmatch_count,
                                            REIM_CONSTANTS.ZERO, REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
                                            tgt.invc_match_status),
             tgt.invc_match_date   = DECODE(src.unmatch_count,
                                            REIM_CONSTANTS.ZERO, L_vdate,
                                            tgt.invc_match_date);

   LOGGER.LOG_INFORMATION(L_program||' Merge INVC_MATCH_STATUS SHIPMENT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write match history
   --------------------------------------

   delete from gtt_6_num_6_str_6_date;

   --number_1   detail_match_history_seq
   --number_2   doc_id
   --number_3   match_key_id
   --varchar2_1 item
   --varchar2_2 exact_match
   insert into gtt_6_num_6_str_6_date(number_1,
                                             number_2,
                                      number_3,
                                             varchar2_1,
                                             varchar2_2)
   select im_detail_match_history_seq.nextval,
          gtt.number_2,
          gtt.number_7,
          gtt.varchar2_1,
          gtt.varchar2_2
     from gtt_10_num_10_str_10_date gtt,
          im_invoice_detail iid
    where iid.doc_id       = gtt.number_2
      and iid.item         = gtt.varchar2_1
      and iid.status       = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH;

   insert into im_detail_match_history (match_id,
                                        auto_matched,
                                        exact_match,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             'N',
             gtt.varchar2_2,
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_invc_history (match_id,
                                             doc_id,
                                             item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             gtt.number_2,
             gtt.varchar2_1,
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_rcpt_history (match_id,
                                             shipment,
                                             item,
                                             substitute_item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             dr.receipt_id,
             dr.display_item,
             DECODE(dr.item,
                    dr.display_item, NULL,
                    dr.item),
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt,
             im_detail_match_ws dr
       where gtt.varchar2_1   = dr.display_item
         and dr.workspace_id  = I_workspace_id
         and dr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
         and NVL(dr.match_key_id, -1) = gtt.number_3
         and dr.choice_flag   = 'Y'
         and dr.ui_filter_ind = 'N';

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to Receipt item posting
   --------------------------------------

   delete from gtt_6_num_6_str_6_date;

   --number_1   receipt_item_posting_seq
   --number_2   shipment
   --number_3   match_key_id
   --varchar2_1 item
   --varchar2_2 qty_matched
   insert into gtt_6_num_6_str_6_date(number_1,
                                             number_2,
                                      number_3,
                                             varchar2_1,
                                             varchar2_2)
   select im_receipt_item_posting_seq.nextval,
          idmwr.receipt_id shipment,
          gtt.number_7,
          idmwr.item,
          DECODE(gtt.varchar2_4,
                 REIM_CONSTANTS.YN_YES, idmwr.receipt_received_qty - idmwr.receipt_avail_qty,
                 idmwr.receipt_avail_qty) + DECODE(L_qty_decision,
                                                   REIM_CONSTANTS.RESLN_DECISION_INVC, DECODE(rank() over (partition by idmwr.workspace_id, idmwr.display_item
                                                                                                               order by DECODE(idmwr.item,idmwr.display_item,1,2), idmwr.receipt_id),
                                                                                              REIM_CONSTANTS.ONE, -1 * (gtt.number_3 - NVL(L_dwo_qty_adj, REIM_CONSTANTS.ZERO)),
                                                                                              REIM_CONSTANTS.ZERO),
                                                   REIM_CONSTANTS.ZERO) qty_matched
     from gtt_10_num_10_str_10_date gtt,
          im_detail_match_ws idmwr,
          im_invoice_detail iid
    where idmwr.workspace_id      = gtt.number_1
      and idmwr.display_item      = gtt.varchar2_1
      and idmwr.entity_type       = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
      and NVL(idmwr.match_key_id, -1) = gtt.number_7
      and idmwr.choice_flag       = 'Y'
      and idmwr.ui_filter_ind     = 'N'
      --
      and iid.doc_id              = gtt.number_2
      and iid.item                = gtt.varchar2_1
      and iid.status              = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH;

   insert into im_receipt_item_posting (seq_no,
                                        shipment,
                                        item,
                                        qty_matched,
                                        qty_posted,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             gtt.number_2,
             gtt.varchar2_1,
             gtt.varchar2_2,
             NULL,
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_rcpt_item_posting_invoice (seq_no,
                                             doc_id,
                                             status,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
   select gtt6.number_1,
          gtt10.number_2,
          'M',
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_6_num_6_str_6_date gtt6,
          gtt_10_num_10_str_10_date gtt10
    where gtt6.varchar2_1 = gtt10.varchar2_1
      and gtt6.number_3   = gtt10.number_7;

   LOGGER.LOG_INFORMATION(L_program||' insert im_rcpt_item_posting_invoice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --Manage Manual Groups
   --------------------------------------

   if MANAGE_MANUAL_GROUP(O_error_message,
                          I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --refresh the workspace
   --------------------------------------

   if REFRESH_MTCH_WSPACE(O_error_message,
                          I_workspace_id,
                          'N') = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --Recreate the Detail Matching Workspace
   --------------------------------------

   if CREATE_DETAIL_MATCH_WS(O_error_message,
                             I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REFRESH_DISCREPANCY_LIST_WS(O_error_message,
                                  I_workspace_id) = 0 then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END PERFORM_ONLINE_RESLN;
------------------------------------------------------------------------------------------
/**
 * The public function used to populate workspace table used for Online detail matching.
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.CREATE_DETAIL_MATCH_WS';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_count            NUMBER(15)   := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:' || I_workspace_id);

   ------------------------------------------------
   --put filtered merch hiers to the side, will reapply filter on the new records at the end.
   ------------------------------------------------
   --NUMBER_1 - dept
   --NUMBER_2 - class
   --NUMBER_3 - subclass
   insert into gtt_6_num_6_str_6_date(NUMBER_1,
                                      NUMBER_2,
                                      NUMBER_3)
   select distinct d.dept,
                   d.class,
                   d.subclass
     from im_detail_match_ws d
    where workspace_id    = I_workspace_id
      and d.ui_filter_ind = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' save ui filter merch hier for workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --push choice flags from workspace headers to detail
   ------------------------------------------------


   --if discrepancy, set choice flag for all
   select count(*)
    into L_count
    from im_discrepancy_list_ws dl
   where dl.workspace_id = I_workspace_id;

   if L_count > 0 then

      update im_match_invc_ws set choice_flag = 'Y' where workspace_id = I_workspace_id;
      update im_match_rcpt_ws set choice_flag = 'Y' where workspace_id = I_workspace_id;

   else --for non-discrepancy, expand to manual group

      if EXPAND_DETAIL_SELECT(O_error_message,
                              I_workspace_id) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

   end if;

   merge into im_match_invc_detl_ws tgt
   using (select d.workspace_id,
                 d.doc_id,
                 d.choice_flag
            from im_match_invc_ws d
           where d.workspace_id = I_workspace_id
   ) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id)
   when MATCHED THEN
      update
         set tgt.choice_flag = src.choice_flag;

   LOGGER.LOG_INFORMATION(L_program||' push choice flag down to im_match_invc_detl_ws: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_match_rcpt_detl_ws tgt
   using (select d.workspace_id,
                 d.shipment,
                 d.choice_flag
            from im_match_rcpt_ws d
           where d.workspace_id = I_workspace_id
   ) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment)
   when MATCHED THEN
      update
         set tgt.choice_flag = src.choice_flag;

   LOGGER.LOG_INFORMATION(L_program||' push choice flag down to im_match_rcpt_detl_ws: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --clear out workspace
   ------------------------------------------------
   delete from im_detail_match_ws where workspace_id = I_workspace_id;
   LOGGER.LOG_INFORMATION(L_program||' clear workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --TOP LEVEL STYLE or SKU
   ------------------------------------------------
   insert into im_detail_match_ws (detail_match_ws_id,
                                   workspace_id,
                                   ancestor_id,
                                   match_status,
                                   item_parent,
                                   display_item,
                                   item,
                                   item_description,
                                   entity_type,
                                   dept,
                                   class,
                                   subclass,
                                   selected_invoice_count,
                                   selected_receipt_count,
                                   choice_flag,
                                   discrepancy_filter_ind)
      with items as (select distinct i.workspace_id,
                                     i.item
                       from im_match_invc_detl_ws i
                      where i.workspace_id = I_workspace_id
                        and i.choice_flag  = 'Y'
                     union
                     select distinct r.workspace_id,
                                     nvl(r.substitute_item, r.item) item
                       from im_match_rcpt_detl_ws r
                      where r.workspace_id = I_workspace_id
                        and r.choice_flag  = 'Y'
                    )
      select im_detail_match_ws_seq.nextval,
             inner.workspace_id,
             inner.ancestor_id,
             inner.match_status,
             inner.item_parent,
             inner.display_item,
             inner.display_item,
             inner.item_description,
             inner.entity_type,
             inner.dept,
             inner.class,
             inner.subclass,
             0 selected_invoice_count,
             0 selected_receipt_count,
             inner.choice_flag,
             inner.discrepancy_filter_ind
        from (select distinct I_workspace_id workspace_id,
                     null ancestor_id,
                     null match_status,
                     null item_parent,
                     nvl(im.item_parent, im.item) display_item,
                     nvl(imp.item_desc, im.item_desc) item_description,
                     decode(im.item_parent,
                            null, REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU,
                            REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE) entity_type,
                     --
                     im.dept,
                     im.class,
                     im.subclass,
                     --
                     'N' choice_flag,
                     'N' discrepancy_filter_ind
                     --
                from items,
                     item_master im,
                     item_master imp
               where items.item     = im.item
                 and im.item_parent = imp.item(+)
              ) inner;

   LOGGER.LOG_INFORMATION(L_program||' TOP LEVEL STYLE or SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --SKU UNDER STYLE
   ------------------------------------------------
   insert into im_detail_match_ws (detail_match_ws_id,
                                   workspace_id,
                                   ancestor_id,
                                   match_status,
                                   item_parent,
                                   display_item,
                                   item,
                                   item_description,
                                   entity_type,
                                   dept,
                                   class,
                                   subclass,
                                   selected_invoice_count,
                                   selected_receipt_count,
                                   choice_flag,
                                   discrepancy_filter_ind)
      with style as (select distinct /*+ ORDERED */
                                     d.workspace_id,
                                     im.item_parent,
                                     i.item,
                                     d.detail_match_ws_id ancestor_id,
                                     im.dept,
                                     im.class,
                                     im.subclass
                       from im_detail_match_ws d,
                            item_master im,
                            im_match_invc_detl_ws i
                      where d.workspace_id = I_workspace_id
                        and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE
                        and d.item         = im.item_parent
                        and im.item        = i.item
                        and i.choice_flag  = 'Y'
                        and i.workspace_id = I_workspace_id
                     union
                     select distinct /*+ ORDERED */
                                     d.workspace_id,
                                     im.item_parent,
                                     nvl(r.substitute_item, r.item) item,
                                     d.detail_match_ws_id ancestor_id,
                                     im.dept,
                                     im.class,
                                     im.subclass
                       from im_detail_match_ws d,
                            item_master im,
                            im_match_rcpt_detl_ws r
                      where d.workspace_id = I_workspace_id
                        and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE
                        and d.item         = im.item_parent
                        and im.item        = nvl(r.substitute_item, r.item)
                        and r.choice_flag  = 'Y'
                        and r.workspace_id = I_workspace_id)
      select im_detail_match_ws_seq.nextval,
             inner.workspace_id,
             inner.ancestor_id,
             inner.match_status,
             inner.item_parent,
             inner.item,
             inner.item,
             inner.item_description,
             inner.entity_type,
             inner.dept,
             inner.class,
             inner.subclass,
             0 selected_invoice_count,
             0 selected_receipt_count,
             inner.choice_flag,
             inner.discrepancy_filter_ind
        from (select distinct I_workspace_id workspace_id,
                     style.ancestor_id,
                     null match_status,
                     style.item_parent,
                     style.item,
                     im.item_desc item_description,
                     REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU entity_type,
                     --
                     style.dept,
                     style.class,
                     style.subclass,
                     --
                     'N' choice_flag,
                     'N' discrepancy_filter_ind
                     --
                from style,
                     item_master im
               where style.item = im.item) inner;

   LOGGER.LOG_INFORMATION(L_program||' SKU UNDER STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --INVC UNDER SKU
   ------------------------------------------------
   insert into im_detail_match_ws (detail_match_ws_id,
                                   workspace_id,
                                   ancestor_id,
                                   match_status,
                                   item_parent,
                                   display_item,
                                   item,
                                   item_description,
                                   vpn,
                                   deals_exist,
                                   entity_type,
                                   invoice_id,
                                   invoice_ext_doc_id,
                                   invoice_unit_cost,
                                   invoice_qty,
                                   invoice_ext_cost,
                                   dept,
                                   class,
                                   subclass,
                                   tolerance_id,
                                   tolerance_exchange_rate,
                                   cost_matched,
                                   qty_matched,
                                   match_hist_id,
                                   manual_group_id,
                                   matched_filter,
                                   invc_supplier_group_id,
                                   invc_supplier,
                                   invc_supplier_name,
                                   invc_supplier_phone,
                                   invc_po_supplier,
                                   invc_po_supplier_site_id,
                                   invc_supplier_site_id,
                                   invc_supplier_site_name,
                                   invc_ordloc_unit_cost,
                                   choice_flag,
                                   discrepancy_filter_ind)
   select /*+ ORDERED */ im_detail_match_ws_seq.nextval,
          dm.workspace_id,
          dm.detail_match_ws_id ancestor_id,
          decode(iwd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'M', 'U') match_status,
          dm.item_parent,
          dm.display_item,
          dm.item,
          dm.item_description,
          iwd.vpn,
          iwd.deals_exist,
          REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC entity_type,
          --
          iwd.doc_id invoice_id,
          iw.ext_doc_id invoice_ext_doc_id,
          iwd.unit_cost invoice_unit_cost,
          iwd.invoice_qty invoice_qty,
          iwd.invoice_qty * iwd.unit_cost invoice_ext_cost,
          --
          dm.dept,
          dm.class,
          dm.subclass,
          --
          iw.tolerance_id,
          iw.tolerance_exchange_rate,
          iwd.cost_matched,
          iwd.qty_matched,
          --
          iwd.match_hist_id,
          mg.group_id manual_group_id,
          decode(iwd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'N', 'Y') matched_filter,
          --
          iw.supplier_group_id,
          iw.supplier,
          iw.supplier_name,
          iw.supplier_phone,
          iw.po_supplier,
          iw.po_supplier_site_id,
          iw.supplier_site_id,
          iw.supplier_site_name,
          iwd.ordloc_unit_cost,
          --
          case when count(distinct iwd.unit_cost) over (partition by dm.workspace_id, dm.item) = 1 and
                    count(distinct iwd.ordloc_unit_cost) over (partition by dm.workspace_id, dm.item) = 1 and
                    decode(iwd.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 'M', 'U') = 'U' then
                'Y'
             else
                'N'
          end choice_flag,
          'N' discrepancy_filter_ind
          --
     from im_detail_match_ws dm,
          im_match_invc_ws iw,
          im_match_invc_detl_ws iwd,
          im_manual_group_invoices mg
    where dm.workspace_id   = I_workspace_id
      and dm.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and dm.workspace_id   = iwd.workspace_id
      and dm.item           = iwd.item
      and iwd.choice_flag   = 'Y'
      --
      and iwd.workspace_id  = iw.workspace_id
      and iwd.doc_id        = iw.doc_id
      --
      and iwd.doc_id        = mg.doc_id(+);

   LOGGER.LOG_INFORMATION(L_program||' INVC UNDER SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --RCPT UNDER SKU
   ------------------------------------------------
   insert into im_detail_match_ws (detail_match_ws_id,
                                   workspace_id,
                                   ancestor_id,
                                   match_status,
                                   item_parent,
                                   display_item,
                                   item,
                                   substitute_item,
                                   item_description,
                                   vpn,
                                   deals_exist,
                                   entity_type,
                                   receipt_id,
                                   receipt_unit_cost,
                                   receipt_avail_qty,
                                   receipt_ext_cost,
                                   receipt_received_qty,
                                   dept,
                                   class,
                                   subclass,
                                   match_hist_id,
                                   matched_filter,
                                   rcpt_supplier,
                                   rcpt_supplier_name,
                                   rcpt_supplier_site_id,
                                   rcpt_supplier_site_name,
                                   choice_flag,
                                   discrepancy_filter_ind)
   select /*+ ORDERED */ im_detail_match_ws_seq.nextval,
          dm.workspace_id,
          dm.detail_match_ws_id ancestor_id,
          rwd.invc_match_status match_status,
          dm.item_parent,
          nvl(rwd.substitute_item,rwd.item),
          rwd.item,
          rwd.substitute_item,
          dm.item_description,
          rwd.vpn,
          rwd.deals_exist,
          REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT entity_type,
          --
          rwd.shipment receipt_id,
          rwd.unit_cost receipt_unit_cost,
          rwd.qty_available receipt_avail_qty,
          (rwd.unit_cost * rwd.qty_available) receipt_ext_cost,
          rwd.qty_received receipt_received_qty,
          --
          dm.dept,
          dm.class,
          dm.subclass,
          rwd.match_hist_id,
          decode(rwd.invc_match_status, 'M', 'N', 'Y') matched_filter,
          --
          rw.supplier,
          rw.supplier_name,
          rw.supplier_site_id,
          rw.supplier_site_name,
          --
          case when iwd.ol_unit_cost_cnt is null then
                'Y'
               when count(distinct rwd.unit_cost) over (partition by dm.workspace_id, dm.item) = 1 and
                    iwd.ol_unit_cost_cnt = 1 and
                    min(rwd.unit_cost) over (partition by dm.workspace_id, dm.item) = iwd.ol_unit_cost and
                    rwd.invc_match_status = 'U' then
                'Y'
             else
                'N'
          end choice_flag,
          --
          'N' discrepancy_filter_ind
     from im_detail_match_ws dm,
          im_match_rcpt_detl_ws rwd,
          im_match_rcpt_ws rw,
          (select distinct
                  i.workspace_id,
                  i.item,
                  count(distinct i.ordloc_unit_cost) over (partition by i.workspace_id, i.item) ol_unit_cost_cnt,
                  min(distinct i.ordloc_unit_cost) over (partition by i.workspace_id, i.item) ol_unit_cost
             from im_match_invc_detl_ws i
            where i.workspace_id = I_workspace_id
              and i.choice_flag  = 'Y') iwd
    where dm.workspace_id   = I_workspace_id
      and dm.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and dm.workspace_id   = rwd.workspace_id
      and dm.item           = nvl(rwd.substitute_item,rwd.item)
      and rwd.choice_flag   = 'Y'
      --
      and rwd.workspace_id  = rw.workspace_id
      and rwd.shipment      = rw.shipment
      --
      and iwd.workspace_id(+)  = rwd.workspace_id
      and iwd.item(+)          = rwd.item;

   LOGGER.LOG_INFORMATION(L_program||' RCPT UNDER SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_detail_match_ws target
   using (select r.detail_match_ws_id,
                 i.manual_group_id
            from im_detail_match_ws i,
                 im_manual_group_receipts gr,
                 im_detail_match_ws r
           where i.workspace_id    = I_workspace_id
             and i.entity_type     = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
             and i.workspace_id    = I_workspace_id
             and r.entity_type     = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and i.ancestor_id     = r.ancestor_id
             and i.manual_group_id = gr.group_id
             and gr.shipment       = r.receipt_id) use_this
   on (target.detail_match_ws_id = use_this.detail_match_ws_id)
   when MATCHED then
      update
         set target.manual_group_id = use_this.manual_group_id;

   ------------------------------------------------
   --discrepancy filter
   ------------------------------------------------
   if PUSH_DISCREPANCY_TO_DETAIL(O_error_message,
                                 I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END CREATE_DETAIL_MATCH_WS;
----------------------------------------------------------------------------------------------
FUNCTION EXPAND_DETAIL_SELECT(O_error_message    OUT VARCHAR2,
                              I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.EXPAND_DETAIL_SELECT';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:' || I_workspace_id);

   for x in 1..LP_expand_group_level loop

      delete from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date(number_1)
         select distinct imiw.manual_group_id
           from im_match_invc_ws imiw
          where imiw.workspace_id    = I_workspace_id
            and imiw.manual_group_id is NOT NULL
            and imiw.choice_flag     = 'Y'
         union
         select distinct gr.group_id manual_group_id
           from im_match_rcpt_ws imrw,
                im_manual_group_receipts gr
          where imrw.workspace_id = I_workspace_id
            and imrw.shipment     = gr.shipment
            and imrw.choice_flag  = 'Y';

      update im_match_invc_ws i
         set i.choice_flag  = 'Y'
       where i.workspace_id = I_workspace_id
         and i.doc_id       in (select imiw.doc_id
                                  from gtt_num_num_str_str_date_date gtt,
                                       im_match_invc_ws imiw
                                 where gtt.number_1      = imiw.manual_group_id
                                   and imiw.workspace_id = I_workspace_id
                                   and imiw.choice_flag  = 'N');

      update im_match_rcpt_ws r
         set r.choice_flag  = 'Y'
       where r.workspace_id = I_workspace_id
         and r.shipment     in (select imrw.shipment
                                  from gtt_num_num_str_str_date_date gtt,
                                       im_manual_group_receipts gr,
                                       im_match_rcpt_ws imrw
                                 where gtt.number_1      = gr.group_id
                                   and gr.shipment       = imrw.shipment
                                   and imrw.workspace_id = I_workspace_id
                                   and imrw.choice_flag  = 'N');

   end loop;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END EXPAND_DETAIL_SELECT;
----------------------------------------------------------------------------------------------
FUNCTION ROLLUP_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.ROLLUP_DETAIL_MATCH_WS';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:' || I_workspace_id);

   ------------------------------------------------
   --rollup status and what not from INVC to SKU
   ------------------------------------------------
   merge into im_detail_match_ws target
   using (select i2.workspace_id,
                 i2.display_item,
                 i2.match_status,
                 i2.matched_filter,
                 i2.tolerance_id,
                 i2.tolerance_exchange_rate,
                 decode(i.invc_count,                  1, i.invoice_id, null) invoice_id,
                 decode(i.invc_count,                  1, i.invoice_ext_doc_id, null) invoice_ext_doc_id,
                 decode(i.cost_count,                  1, i.invoice_unit_cost, null) invoice_unit_cost,
                 decode(i.invc_ordloc_unit_cost_count, 1, i.invc_ordloc_unit_cost, null) invc_ordloc_unit_cost,
                 i.invoice_qty,
                 i.invoice_ext_cost,
                 NVL(i.invc_count, 0) selected_invoice_count,
                 --
                 decode(i.invc_supplier_group_id_cnt,   1, i.invc_supplier_group_id) invc_supplier_group_id,
                 decode(i.invc_supplier_cnt,            1, i.invc_supplier) invc_supplier,
                 decode(i.invc_supplier_name_cnt,       1, i.invc_supplier_name) invc_supplier_name,
                 decode(i.invc_supplier_phone_cnt,      1, i.invc_supplier_phone) invc_supplier_phone,
                 decode(i.invc_po_supplier_cnt,         1, i.invc_po_supplier) invc_po_supplier,
                 decode(i.invc_po_supplier_site_id_cnt, 1, i.invc_po_supplier_site_id) invc_po_supplier_site_id,
                 decode(i.invc_supplier_site_id_cnt,    1, i.invc_supplier_site_id) invc_supplier_site_id,
                 decode(i.invc_supplier_site_name_cnt,  1, i.invc_supplier_site_name) invc_supplier_site_name,
                 decode(i.cost_matched_cnt,             null, null, 1, i.cost_matched,'N') cost_matched,
                 decode(i.qty_matched_cnt,              null, null, 1, i.qty_matched, 'N') qty_matched
                 --
            from (select d.workspace_id,
                         d.display_item,
                         min(d.invoice_id)
                            over (partition by d.workspace_id, d.display_item) invoice_id,
                         min(d.invoice_ext_doc_id)
                            over (partition by d.workspace_id, d.display_item) invoice_ext_doc_id,
                         min(d.invoice_unit_cost)
                            over (partition by d.workspace_id, d.display_item) invoice_unit_cost,
                         min(d.invc_ordloc_unit_cost)
                            over (partition by d.workspace_id, d.display_item) invc_ordloc_unit_cost,
                         sum(d.invoice_qty)
                            over (partition by d.workspace_id, d.display_item) invoice_qty,
                         sum(d.invoice_ext_cost)
                            over (partition by d.workspace_id, d.display_item) invoice_ext_cost,
                         row_number()
                            over (partition by d.workspace_id, d.display_item order by d.detail_match_ws_id) rownbr,
                         count(distinct d.invoice_id)
                            over (partition by d.workspace_id, d.display_item) invc_count,
                         count(distinct d.invoice_unit_cost)
                            over (partition by d.workspace_id, d.display_item) cost_count,
                         count(distinct d.invc_ordloc_unit_cost)
                            over (partition by d.workspace_id, d.display_item) invc_ordloc_unit_cost_count,
                         --
                         min(distinct d.invc_supplier_group_id)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_group_id,
                         min(distinct d.invc_supplier)
                            over (partition by d.workspace_id, d.display_item) invc_supplier,
                         min(distinct d.invc_supplier_name)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_name,
                         min(distinct d.invc_supplier_phone)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_phone,
                         min(distinct d.invc_po_supplier)
                            over (partition by d.workspace_id, d.display_item) invc_po_supplier,
                         min(distinct d.invc_po_supplier_site_id)
                            over (partition by d.workspace_id, d.display_item) invc_po_supplier_site_id,
                         min(distinct d.invc_supplier_site_id)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_site_id,
                         min(distinct d.invc_supplier_site_name)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_site_name,
                         min(distinct d.cost_matched)
                            over (partition by d.workspace_id, d.display_item) cost_matched,
                         min(distinct d.qty_matched)
                            over (partition by d.workspace_id, d.display_item) qty_matched,
                         --
                         count(distinct d.invc_supplier_group_id)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_group_id_cnt,
                         count(distinct d.invc_supplier)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_cnt,
                         count(distinct d.invc_supplier_name)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_name_cnt,
                         count(distinct d.invc_supplier_phone)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_phone_cnt,
                         count(distinct d.invc_po_supplier)
                            over (partition by d.workspace_id, d.display_item) invc_po_supplier_cnt,
                         count(distinct d.invc_po_supplier_site_id)
                            over (partition by d.workspace_id, d.display_item) invc_po_supplier_site_id_cnt,
                         count(distinct d.invc_supplier_site_id)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_site_id_cnt,
                         count(distinct d.invc_supplier_site_name)
                            over (partition by d.workspace_id, d.display_item) invc_supplier_site_name_cnt,
                         count(distinct d.cost_matched)
                            over (partition by d.workspace_id, d.display_item) cost_matched_cnt,
                         count(distinct d.qty_matched)
                            over (partition by d.workspace_id, d.display_item) qty_matched_cnt
                         --
                    from im_detail_match_ws d
                   where d.workspace_id = I_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                     and d.choice_flag  = 'Y') i,
                 --
                 (select d.workspace_id,
                         d.display_item,
                         max(d.match_status) match_status,
                         min(d.matched_filter) matched_filter,
                         min(d.tolerance_id) tolerance_id,
                         min(d.tolerance_exchange_rate) tolerance_exchange_rate
                    from im_detail_match_ws d
                   where d.workspace_id = I_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                   group by d.workspace_id,
                            d.display_item) i2
           where 1               = i.rownbr(+)
             and i2.workspace_id = i.workspace_id(+)
             and i2.display_item = i.display_item(+)) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.display_item = use_this.display_item
       and target.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.match_status             = use_this.match_status,
             target.matched_filter           = use_this.matched_filter,
             target.invoice_id               = use_this.invoice_id,
             target.invoice_ext_doc_id       = use_this.invoice_ext_doc_id,
             target.invoice_unit_cost        = use_this.invoice_unit_cost,
             target.invc_ordloc_unit_cost    = use_this.invc_ordloc_unit_cost,
             target.invoice_qty              = use_this.invoice_qty,
             target.invoice_ext_cost         = use_this.invoice_ext_cost,
             target.selected_invoice_count   = use_this.selected_invoice_count,
             --
             target.invc_supplier_group_id   = use_this.invc_supplier_group_id,
             target.invc_supplier            = use_this.invc_supplier,
             target.invc_supplier_name       = use_this.invc_supplier_name,
             target.invc_supplier_phone      = use_this.invc_supplier_phone,
             target.invc_po_supplier         = use_this.invc_po_supplier,
             target.invc_po_supplier_site_id = use_this.invc_po_supplier_site_id,
             target.invc_supplier_site_id    = use_this.invc_supplier_site_id,
             target.invc_supplier_site_name  = use_this.invc_supplier_site_name,
             target.cost_matched             = use_this.cost_matched,
             target.qty_matched              = use_this.qty_matched,
             target.tolerance_id             = use_this.tolerance_id,
             target.tolerance_exchange_rate  = use_this.tolerance_exchange_rate;

   LOGGER.LOG_INFORMATION(L_program||' rollup status and what not from INVC to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --rollup status and what not from RCPT to SKU
   ------------------------------------------------
   merge into im_detail_match_ws target
   using (select i2.workspace_id,
                 i2.display_item,
                 i2.match_status,
                 i2.matched_filter,
                 decode(i.rcpt_count, 1, i.receipt_id, null) receipt_id,
                 decode(i.cost_count, 1, i.receipt_unit_cost, null) receipt_unit_cost,
                 decode(i.cost_count, 1, 'Y', 'N') choice_flag,
                 i.receipt_avail_qty,
                 i.receipt_ext_cost,
                 i.receipt_received_qty,
                 NVL(i.rcpt_count, 0) selected_receipt_count,
                 --
                 decode(i.rcpt_supplier_cnt,            1, i.rcpt_supplier) rcpt_supplier,
                 decode(i.rcpt_supplier_name_cnt,       1, i.rcpt_supplier_name) rcpt_supplier_name,
                 decode(i.rcpt_supplier_site_id_cnt,    1, i.rcpt_supplier_site_id) rcpt_supplier_site_id,
                 decode(i.rcpt_supplier_site_name_cnt,  1, i.rcpt_supplier_site_name) rcpt_supplier_site_name
            from (select d.workspace_id,
                         d.display_item,
                         min(d.receipt_id)
                            over (partition by d.workspace_id, d.display_item) receipt_id,
                         min(d.receipt_unit_cost)
                            over (partition by d.workspace_id, d.display_item) receipt_unit_cost,
                         sum(d.receipt_avail_qty)
                            over (partition by d.workspace_id, d.display_item) receipt_avail_qty,
                         sum(d.receipt_ext_cost)
                            over (partition by d.workspace_id, d.display_item) receipt_ext_cost,
                         sum(d.receipt_received_qty)
                            over (partition by d.workspace_id, d.display_item) receipt_received_qty,
                         row_number()
                            over (partition by d.workspace_id, d.display_item order by d.detail_match_ws_id) rownbr,
                         count(distinct d.receipt_id)
                            over (partition by d.workspace_id, d.display_item) rcpt_count,
                         count(distinct d.receipt_unit_cost)
                            over (partition by d.workspace_id, d.display_item) cost_count,
                         --
                         min(distinct d.rcpt_supplier)
                            over (partition by d.workspace_id, d.display_item) rcpt_supplier,
                         min(distinct d.rcpt_supplier_name)
                            over (partition by d.workspace_id, d.display_item) rcpt_supplier_name,
                         min(distinct d.rcpt_supplier_site_id)
                            over (partition by d.workspace_id, d.display_item) rcpt_supplier_site_id,
                         min(distinct d.rcpt_supplier_site_name)
                            over (partition by d.workspace_id, d.display_item) rcpt_supplier_site_name,
                         --
                         count(distinct d.rcpt_supplier)
                            over (partition by d.workspace_id, d.display_item) rcpt_supplier_cnt,
                         count(distinct d.rcpt_supplier_name)
                            over (partition by d.workspace_id, d.display_item) rcpt_supplier_name_cnt,
                         count(distinct d.rcpt_supplier_site_id)
                            over (partition by d.workspace_id, d.display_item) rcpt_supplier_site_id_cnt,
                         count(distinct d.rcpt_supplier_site_name)
                            over (partition by d.workspace_id, d.display_item) rcpt_supplier_site_name_cnt
                    from im_detail_match_ws d
                   where d.workspace_id = I_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                     and d.choice_flag  = 'Y') i,
                 --
                 (select d.workspace_id,
                         d.display_item,
                         max(d.match_status) match_status,
                         min(d.matched_filter) matched_filter
                    from im_detail_match_ws d
                   where d.workspace_id = I_workspace_id
                     and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                   group by d.workspace_id,
                            d.display_item) i2
           where 1               = i.rownbr(+)
             and i2.workspace_id = i.workspace_id(+)
             and i2.display_item         = i.display_item(+)) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.display_item = use_this.display_item
       and target.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.match_status =
                GREATEST(nvl(target.match_status,use_this.match_status),use_this.match_status), --Pick U if any are U, only get M if all are M
             target.matched_filter =
                LEAST(nvl(target.matched_filter,use_this.matched_filter),use_this.matched_filter), --Pick N if any are N, only get Y if all are Y
             target.receipt_id             = use_this.receipt_id,
             target.receipt_unit_cost      = use_this.receipt_unit_cost,
             target.receipt_avail_qty      = use_this.receipt_avail_qty,
             target.receipt_ext_cost       = use_this.receipt_ext_cost,
             target.receipt_received_qty   = use_this.receipt_received_qty,
             target.selected_receipt_count = use_this.selected_receipt_count,
             --
             target.rcpt_supplier            = use_this.rcpt_supplier,
             target.rcpt_supplier_name       = use_this.rcpt_supplier_name,
             target.rcpt_supplier_site_id    = use_this.rcpt_supplier_site_id,
             target.rcpt_supplier_site_name  = use_this.rcpt_supplier_site_name;

   LOGGER.LOG_INFORMATION(L_program||' rollup status and what not from RCPT to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --merge match info to SKU
   ------------------------------------------------
   merge into im_detail_match_ws target
   using (select invc.ancestor_id,
                 invc.display_item,
                 invc.invc_count,
                 rcpt.rcpt_count,
                 invc.invc_cost_cnt,
                 rcpt.rcpt_cost_cnt,
                 --
                 invc.invc_group_cost,
                 invc.invc_group_qty,
                 rcpt.rcpt_group_cost,
                 rcpt.rcpt_group_qty,
                 --
                 invc.tolerance_id,
                 invc.tolerance_exchange_rate,
                 tdc.tolerance_detail_id cost_tolerance_dtl_id,
                 tdq.tolerance_detail_id qty_tolerance_dtl_id,
                 --
                 NVL(NVL(rcpt.rcpt_group_cost, invc.invc_ordloc_group_cost), 0) - invc.invc_group_cost unit_cost_variance,
                 case when NVL(NVL(rcpt.rcpt_group_cost,invc.invc_ordloc_group_cost), 0) = 0 then
                      NULL
                      else
                      100 *((NVL(rcpt.rcpt_group_cost,invc.invc_ordloc_group_cost) - invc.invc_group_cost) / NVL(rcpt.rcpt_group_cost,invc.invc_ordloc_group_cost))
                 end unit_cost_variance_pct,
                 NVL(rcpt.rcpt_group_qty, 0) - invc.invc_group_qty qty_variance,
                 case when NVL(rcpt.rcpt_group_qty, 0) = 0 then
                      NULL
                      else
                       100 *((rcpt.rcpt_group_qty - invc.invc_group_qty) / rcpt.rcpt_group_qty)
                 end qty_variance_pct,
                 --
                 case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(NVL(rcpt.rcpt_group_cost,invc.invc_ordloc_group_cost), 0) = 0 then
                         'N'
                              when tdc.tolerance_value < ABS(100 * (invc.invc_group_cost - NVL(rcpt.rcpt_group_cost,invc.invc_ordloc_group_cost)) / NVL(rcpt.rcpt_group_cost,invc.invc_ordloc_group_cost)) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdc.tolerance_value < ABS((invc.invc_group_cost - NVL(NVL(rcpt.rcpt_group_cost,invc.invc_ordloc_group_cost), 0)) * invc.tolerance_exchange_rate) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when invc.invc_group_cost = NVL(NVL(rcpt.rcpt_group_cost,invc.invc_ordloc_group_cost), 0) then
                                 'Y'
                              else
                                 'N'
                         end
                 end cost_in_tolerance,
                 --
                 case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(rcpt.rcpt_group_qty, 0) = 0 then
                         'N'
                              when tdq.tolerance_value < ABS(100 * (invc.invc_group_qty - rcpt.rcpt_group_qty) / rcpt.rcpt_group_qty) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdq.tolerance_value < ABS(invc.invc_group_qty - NVL(rcpt.rcpt_group_qty, 0)) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when invc.invc_group_qty = NVL(rcpt.rcpt_group_qty, 0) then
                                 'Y'
                              else
                                 'N'
                         end
                 end qty_in_tolerance
                 --
            from (select id.ancestor_id,
                         id.display_item,
                         id.tolerance_id,
                         id.tolerance_exchange_rate,
                         min(id.invoice_unit_cost) invc_group_cost,
                         min(id.invc_ordloc_unit_cost) invc_ordloc_group_cost,
                         sum(id.invoice_qty) invc_group_qty,
                         count(id.invoice_id) invc_count,
                         count(distinct id.invoice_unit_cost) invc_cost_cnt
                   from im_detail_match_ws id
                  where id.workspace_id   = I_workspace_id
                    and id.match_status   = 'U' --constant
                    and id.entity_type    = 'INVC'   --constant
                    and id.choice_flag    = 'Y'
                  group by id.ancestor_id,
                           id.display_item,
                           id.tolerance_id,
                           id.tolerance_exchange_rate) invc,
                 (select rd.ancestor_id,
                         rd.display_item,
                         rd.tolerance_id,
                         rd.tolerance_exchange_rate,
                         min(rd.receipt_unit_cost) rcpt_group_cost,
                         sum(rd.receipt_avail_qty) rcpt_group_qty,
                         count(rd.receipt_id) rcpt_count,
                         count(distinct rd.receipt_unit_cost) rcpt_cost_cnt
                   from im_detail_match_ws rd
                  where rd.workspace_id      = I_workspace_id
                    and rd.match_status      = 'U' --constant
                    and rd.entity_type       = 'RCPT'   --constant
                    and rd.choice_flag    = 'Y'
                  group by rd.ancestor_id,
                           rd.display_item,
                           rd.tolerance_id,
                           rd.tolerance_exchange_rate) rcpt,
                 --
                 im_tolerance_detail tdc,
                 im_tolerance_detail tdq
           where invc.ancestor_id      = rcpt.ancestor_id (+)
             and invc.display_item     = rcpt.display_item (+)
             --
             and invc.tolerance_id     = tdc.tolerance_id(+)
             and tdc.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
             and tdc.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
             and tdc.lower_limit(+)   <= invc.invc_group_cost
             and tdc.upper_limit(+)    > invc.invc_group_cost
             and tdc.favor_of(+)       = case when invc.invc_group_cost > NVL(rcpt.rcpt_group_cost, 0)
                                              then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                              else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                         end
             --
             and invc.tolerance_id     = tdq.tolerance_id(+)
             and tdq.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
             and tdq.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
             and tdq.lower_limit(+)   <= invc.invc_group_qty
             and tdq.upper_limit(+)    > invc.invc_group_qty
             and tdq.favor_of(+)       = case when invc.invc_group_qty > NVL(rcpt.rcpt_group_qty, 0)
                                              then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                              else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                         end
   ) use_this
   on (    target.workspace_id        = I_workspace_id
       and target.detail_match_ws_id  = use_this.ancestor_id
       and target.entity_type         = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.in_tolerance = case when use_this.cost_in_tolerance = 'Y' and use_this.qty_in_tolerance = 'Y' then 'Y' else 'N' end,
             target.choice_flag  = case when use_this.cost_in_tolerance = 'Y' and use_this.qty_in_tolerance = 'Y' then 'Y' else 'N' end,
             target.unit_cost_variance     = use_this.unit_cost_variance,
             target.unit_cost_variance_pct = use_this.unit_cost_variance_pct,
             target.cost_tolerance_dtl_id  = use_this.cost_tolerance_dtl_id,
             target.cost_in_tolerance      = use_this.cost_in_tolerance,
             target.qty_variance           = use_this.qty_variance,
             target.qty_variance_pct       = use_this.qty_variance_pct,
             target.qty_tolerance_dtl_id   = use_this.qty_tolerance_dtl_id,
             target.qty_in_tolerance       = use_this.qty_in_tolerance;

   LOGGER.LOG_INFORMATION(L_program||' merge match into to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --merge VPN to SKU
   ------------------------------------------------
   merge into im_detail_match_ws target
   using (select invc.ancestor_id,
                 DECODE(invc_vpn,
                        DECODE(rcpt.ancestor_id,
                               NULL, invc_vpn,
                               rcpt_vpn), invc_vpn,
                        NULL) vpn
            from (select id.ancestor_id,
                         DECODE(count(distinct NVL(id.vpn,'VpN')),
                                1, min(id.vpn),
                                NULL) invc_vpn
                   from im_detail_match_ws id
                  where id.workspace_id   = I_workspace_id
                    and id.match_status   = 'U' --constant
                    and id.entity_type    = 'INVC'   --constant
                  group by id.ancestor_id) invc,
                 (select rd.ancestor_id,
                         DECODE(count(distinct NVL(rd.vpn,'VpN')),
                                1, min(rd.vpn),
                                NULL) rcpt_vpn
                   from im_detail_match_ws rd
                  where rd.workspace_id      = I_workspace_id
                    and rd.match_status      = 'U' --constant
                    and rd.entity_type       = 'RCPT'   --constant
                  group by rd.ancestor_id) rcpt
           where invc.ancestor_id = rcpt.ancestor_id (+)
   ) use_this
   on (    target.workspace_id        = I_workspace_id
       and target.detail_match_ws_id  = use_this.ancestor_id
       and target.entity_type         = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
   when MATCHED then
      update
         set target.vpn = use_this.vpn;

   LOGGER.LOG_INFORMATION(L_program||' merge VPN to SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if ROLLUP_STYLE_DETAIL_MATCH_WS(O_error_message,
                                   I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   ------------------------------------------------
   --reapply filter to detail workspace
   ------------------------------------------------

   merge into im_detail_match_ws tgt
   using (select gtt.number_1 dept,
                 gtt.number_2 class,
                 gtt.number_3 subclass
            from gtt_6_num_6_str_6_date gtt) src
   on (    tgt.dept     = src.dept
       and tgt.class    = src.class
       and tgt.subclass = src.subclass)
   when MATCHED THEN
      update
         set tgt.ui_filter_ind = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' apply UI filter - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END ROLLUP_DETAIL_MATCH_WS;
----------------------------------------------------------------------------------------------
FUNCTION REFRESH_DISCREPANCY_LIST_WS(O_error_message    OUT VARCHAR2,
                                     I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.REFRESH_DISCREPANCY_LIST_WS';
   L_start_time          TIMESTAMP := SYSTIMESTAMP;
   L_count      NUMBER(15)   := 0;

BEGIN

   select count(*)
    into L_count
    from im_discrepancy_list_ws dl
   where dl.workspace_id = I_workspace_id;

   if L_count > 0 then
      if CREATE_DISCREPANCY_LIST_WS(O_error_message,
                                    I_workspace_id) = 0 then
         return 0;
      end if;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END REFRESH_DISCREPANCY_LIST_WS;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_DISCREPANCY_LIST_WS(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.CREATE_DISCREPANCY_LIST_WS';
   L_start_time          TIMESTAMP := SYSTIMESTAMP;
   L_resolution_due_days NUMBER(3);

BEGIN

   select so.resolution_due_days
     into L_resolution_due_days
     from im_system_options so;

   --------------------------------------
   --set choice_flat aside to reapply at the end
   --------------------------------------
   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date(varchar2_1,
                                             number_1)
   select distinct dl.item,
          dl.invoice_id
     from im_discrepancy_list_ws dl
    where dl.choice_flag  = 'Y'
      and dl.workspace_id = I_workspace_id;

   LOGGER.LOG_INFORMATION(L_program||' save choice flag for workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
  
   --clear out the workspace/working tables
   delete from im_discrepancy_list_rcpt_ws where workspace_id = I_workspace_id;
   delete from im_discrepancy_list_ws where workspace_id = I_workspace_id;
   delete from im_transform_ordloc_gtt;

   insert into im_transform_ordloc_gtt (order_no,
                                        item,
                                        item_desc,
                                        loc,
                                        loc_type,
                                        qty_ordered,
                                        unit_cost,
                                        unit_cost_init,
                                        cost_source,
                                        sup_qty_level,
                                        catch_weight_type,
                                        import_loc,
                                        import_loc_type,
                                        transform_qty_ordered,
                                        transform_unit_cost,
                                        transform_unit_cost_init)
   select distinct mi.order_no,
          invc_ws.item,
          im.item_desc,
          ol.location,
          ol.loc_type,
          ol.qty_ordered,
          ol.unit_cost,
          ol.unit_cost_init,
          ol.cost_source,
          s.sup_qty_level,
          im.catch_weight_type,
          ol.location,
          ol.loc_type,
          ol.qty_ordered,
          ol.unit_cost,
          ol.unit_cost_init
     from im_detail_match_ws invc_ws,
          im_match_invc_ws mi,
          ordloc ol,
          sups s,
          item_master im
    where invc_ws.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
      and (invc_ws.cost_matched = 'D' or
           invc_ws.qty_matched  = 'D')
      and invc_ws.workspace_id   = I_workspace_id
      and invc_ws.workspace_id   = mi.workspace_id
      and invc_ws.invoice_id     = mi.doc_id
      and mi.order_no            = ol.order_no
      and mi.po_supplier_site_id = s.supplier
      and invc_ws.item           = im.item
      and invc_ws.item           = ol.item (+);

   LOGGER.LOG_INFORMATION(L_program||'insert into im_transform_ordloc_gtt - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   if REIM_XFORM_ORDLOC_SQL.TRANSFORM_ORDLOC_GTT(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;
   
   insert into im_discrepancy_list_ws (discrepancy_list_ws_id,
                                       workspace_id,
                                       item_parent,
                                       item,
                                       item_description,
                                       vpn,
                                       dept,
                                       class,
                                       subclass,
                                       location,
                                       loc_type,
                                       location_name,
                                       supplier_group_id,
                                       supplier,
                                       supplier_name,
                                       supplier_phone,
                                       po_supplier,
                                       po_supplier_site_id,
                                       supplier_site_id,
                                       supplier_site_name,
                                       currency_code,
                                       invoice_id,
                                       invoice_unit_cost,
                                       invoice_qty,
                                       invoice_ext_cost,
                                       ext_doc_id,
                                       order_no,
                                       resolve_by_date,
                                       routing_date,
                                       order_unit_cost,
                                       order_ext_cost,
                                       orig_order_unit_cost,
                                       invoice_total_cost,
                                       order_qty,
                                       cash_discount,
                                       receipt_id,
                                       receipt_unit_cost,
                                       receipt_avail_qty,
                                       receipt_ext_cost,
                                       receipt_received_qty,
                                       cost_matched,
                                       qty_matched,
                                       match_key_id,
                                       sku_detail_match_ws_id,
                                       invc_detail_match_ws_id,
                                       choice_flag,
                                       ap_reviewer)
   select im_discrepancy_list_ws_seq.nextval,
          I_workspace_id,
          sku_ws.item_parent,
          sku_ws.item,
          gtt.item_desc item_description,
          invc_ws.vpn,
          sku_ws.dept,
          sku_ws.class,
          sku_ws.subclass,
          mi.location,
          mi.loc_type,
          mi.location_name,
          mi.supplier_group_id,
          mi.supplier,
          mi.supplier_name,
          mi.supplier_phone,
          mi.po_supplier,
          mi.po_supplier_site_id,
          mi.supplier_site_id,
          mi.supplier_site_name,
          mi.currency_code,
          invc_ws.invoice_id,
          invc_ws.invoice_unit_cost,
          invc_ws.invoice_qty,
          invc_ws.invoice_ext_cost,
          dh.ext_doc_id,
          mi.order_no,
          greatest(to_date(dh.due_date - L_resolution_due_days), dh.doc_date) resolve_by_date,
          null routing_date,
          gtt.order_unit_cost,
          gtt.order_ext_cost,
          gtt.orig_order_unit_cost,
          mi.total_avail_cost invoice_total_cost,
          gtt.order_qty,
          null cash_discount,
          null receipt_id,            --merge later
          null receipt_unit_cost,     --merge later
          null receipt_avail_qty,     --merge later
          null receipt_ext_cost,      --merge later
          null receipt_received_qty,  --merge later
          invc_ws.cost_matched,
          invc_ws.qty_matched,
          mi.match_key_id,
          sku_ws.detail_match_ws_id sku_detail_match_ws_id,
          invc_ws.detail_match_ws_id invc_detail_match_ws_id,
          'N' choice_flag,
          ssa.ap_reviewer
          --
     from (select order_no,
                  item,
                  import_loc,
                  item_desc,
                  transform_unit_cost order_unit_cost,
                  sum(transform_unit_cost * transform_qty_ordered) order_ext_cost,
                  transform_unit_cost_init orig_order_unit_cost,
                  sum(transform_qty_ordered) order_qty
             from im_transform_ordloc_gtt
            GROUP BY order_no,
                     item,
                     import_loc,
                     item_desc,
                     transform_unit_cost,
                     transform_unit_cost_init) gtt,
          im_detail_match_ws sku_ws,
          im_match_invc_ws mi,
          im_detail_match_ws invc_ws,
          im_doc_head dh,
          v_im_supp_site_attrib_expl ssa,
          (select store loc, store physical_loc from store
           union all
           select wh loc, physical_wh physical_loc from wh) locs
    where sku_ws.entity_type         = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and invc_ws.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
      --
      and sku_ws.workspace_id        = I_workspace_id
      and sku_ws.workspace_id        = invc_ws.workspace_id
      and sku_ws.detail_match_ws_id  = invc_ws.ancestor_id
      --
      and invc_ws.workspace_id       = mi.workspace_id
      and invc_ws.invoice_id         = mi.doc_id
      --
      and (invc_ws.cost_matched = 'D' or
           invc_ws.qty_matched  = 'D')
      --
      and mi.order_no                = gtt.order_no
      and sku_ws.item                = gtt.item
      and locs.loc                   = gtt.import_loc
      and mi.location                = locs.physical_loc
      --
      and mi.doc_id                  = dh.doc_id
      --
      and ssa.supplier               = mi.supplier_site_id;

   LOGGER.LOG_INFORMATION(L_program||'insert into im_discrepancy_list_ws - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   insert into im_discrepancy_list_rcpt_ws (discrepancy_list_rcpt_ws_id,
                                            discrepancy_list_ws_id,
                                            workspace_id,
                                            rcpt_detail_match_ws_id,
                                            choice_flag)
   select im_discrepancy_list_rcpt_seq.nextval,
          i.discrepancy_list_ws_id,
          I_workspace_id,
          i.detail_match_ws_id,
          'N'
     from (select dl.discrepancy_list_ws_id,
                  rcpt_ws.detail_match_ws_id
             from im_discrepancy_list_ws dl,
                  im_detail_match_ws rcpt_ws,
                  im_match_invc_ws iw,
                  im_manual_group_receipts gr
            where dl.workspace_id           = I_workspace_id
              and dl.workspace_id           = rcpt_ws.workspace_id
              and dl.sku_detail_match_ws_id = rcpt_ws.ancestor_id
              and rcpt_ws.entity_type       = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
              --
              and dl.workspace_id           = iw.workspace_id
              and dl.invoice_id             = iw.doc_id
              --
              and iw.manual_group_id        = gr.group_id
              and gr.shipment               = rcpt_ws.receipt_id
           union
           select dl.discrepancy_list_ws_id,
                  rcpt_ws.detail_match_ws_id
             from im_discrepancy_list_ws dl,
                  im_detail_match_ws rcpt_ws,
                  im_match_invc_ws iw,
                  im_match_rcpt_ws rw
            where dl.workspace_id           = I_workspace_id
              and dl.workspace_id           = rcpt_ws.workspace_id
              and dl.sku_detail_match_ws_id = rcpt_ws.ancestor_id
              and rcpt_ws.entity_type       = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
              --
              and dl.workspace_id           = iw.workspace_id
              and dl.invoice_id             = iw.doc_id
              --
              and rcpt_ws.workspace_id      = rw.workspace_id
              and rcpt_ws.receipt_id        = rw.shipment
              and NVL(iw.match_key_id, -1)  = NVL(rw.match_key_id, -1)) i;

   LOGGER.LOG_INFORMATION(L_program||'insert into im_discrepancy_list_rcpt_ws - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   merge into im_discrepancy_list_ws target
   using (select i.workspace_id,
                 i.discrepancy_list_ws_id,
                 decode(i.rcpt_count, 1, i.receipt_id, null) receipt_id,
                 decode(i.cost_count, 1, i.receipt_unit_cost, null) receipt_unit_cost,
                 i.receipt_avail_qty,
                 i.receipt_ext_cost,
                 i.receipt_received_qty,
                 i.rcpt_count
            from (select d.workspace_id,
                         dlr.discrepancy_list_ws_id,
                         min(d.receipt_id)
                            over (partition by d.workspace_id, dlr.discrepancy_list_ws_id) receipt_id,
                         min(d.receipt_unit_cost)
                            over (partition by d.workspace_id, dlr.discrepancy_list_ws_id) receipt_unit_cost,
                         sum(d.receipt_avail_qty)
                            over (partition by d.workspace_id, dlr.discrepancy_list_ws_id) receipt_avail_qty,
                         sum(d.receipt_ext_cost)
                            over (partition by d.workspace_id, dlr.discrepancy_list_ws_id) receipt_ext_cost,
                         sum(d.receipt_received_qty)
                            over (partition by d.workspace_id, dlr.discrepancy_list_ws_id) receipt_received_qty,
                         row_number()
                            over (partition by d.workspace_id, dlr.discrepancy_list_ws_id order by d.detail_match_ws_id) rownbr,
                         count(distinct d.receipt_id)
                            over (partition by d.workspace_id, dlr.discrepancy_list_ws_id) rcpt_count,
                         count(distinct d.receipt_unit_cost)
                            over (partition by d.workspace_id, dlr.discrepancy_list_ws_id) cost_count
                         --
                    from im_discrepancy_list_rcpt_ws dlr,
                         im_detail_match_ws d
                   where dlr.workspace_id             = I_workspace_id
                     and dlr.workspace_id             = d.workspace_id
                     and dlr.rcpt_detail_match_ws_id  = d.detail_match_ws_id) i
           where i.rownbr = 1) use_this
   on (    target.workspace_id           = use_this.workspace_id
       and target.discrepancy_list_ws_id = use_this.discrepancy_list_ws_id)
   when matched then update
    set target.receipt_id              = use_this.receipt_id,
        target.receipt_unit_cost       = use_this.receipt_unit_cost,
        target.receipt_avail_qty       = use_this.receipt_avail_qty,
        target.receipt_ext_cost        = use_this.receipt_ext_cost,
        target.receipt_received_qty    = use_this.receipt_received_qty,
        target.receipt_count           = use_this.rcpt_count;

   LOGGER.LOG_INFORMATION(L_program||'merge im_discrepancy_list_ws rcpt info - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   merge into im_discrepancy_list_ws target
   using (select idlw.workspace_id,
                 idlw.discrepancy_list_ws_id,
                 case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(idlw.receipt_unit_cost, NVL(idlw.order_unit_cost,0)) = 0 then
                         'N'
                              when tdc.tolerance_value < ABS(100 * (idlw.invoice_unit_cost - NVL(idlw.receipt_unit_cost, idlw.order_unit_cost)) / NVL(idlw.receipt_unit_cost, idlw.order_unit_cost)) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdc.tolerance_value < ABS((idlw.invoice_unit_cost - NVL(idlw.receipt_unit_cost, NVL(idlw.order_unit_cost,0))) * idmw.tolerance_exchange_rate) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when idlw.invoice_unit_cost = NVL(idlw.receipt_unit_cost, NVL(idlw.order_unit_cost,0)) then
                                 'Y'
                              else
                                 'N'
                         end
                 end cost_in_tolerance,
                 --
                 case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(idlw.receipt_avail_qty, 0) = 0 then
                         'N'
                              when tdq.tolerance_value < ABS(100 * (idlw.invoice_qty - idlw.receipt_avail_qty) / idlw.receipt_avail_qty) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdq.tolerance_value < ABS(idlw.invoice_qty - NVL(idlw.receipt_avail_qty, 0)) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when idlw.invoice_qty = NVL(idlw.receipt_avail_qty, 0) then
                                 'Y'
                              else
                                 'N'
                         end
                 end qty_in_tolerance
                 --
            from im_discrepancy_list_ws idlw,
                 im_detail_match_ws idmw,
                 im_tolerance_detail tdc,
                 im_tolerance_detail tdq
           where idlw.workspace_id       = I_workspace_id
             and idmw.workspace_id       = idlw.workspace_id
             and idmw.detail_match_ws_id = idlw.invc_detail_match_ws_id
             --
             and idmw.tolerance_id     = tdc.tolerance_id(+)
             and tdc.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
             and tdc.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
             and tdc.lower_limit(+)   <= idlw.invoice_unit_cost
             and tdc.upper_limit(+)    > idlw.invoice_unit_cost
             and tdc.favor_of(+)       = case when idlw.invoice_unit_cost > NVL(idlw.receipt_unit_cost, NVL(idlw.order_unit_cost,0))
                                              then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                              else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                         end
             --
             and idmw.tolerance_id     = tdq.tolerance_id(+)
             and tdq.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
             and tdq.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
             and tdq.lower_limit(+)   <= idlw.invoice_qty
             and tdq.upper_limit(+)    > idlw.invoice_qty
             and tdq.favor_of(+)       = case when idlw.invoice_qty > NVL(idlw.receipt_avail_qty, 0)
                                              then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                              else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                         end) use_this
   on (    target.workspace_id           = use_this.workspace_id
       and target.discrepancy_list_ws_id = use_this.discrepancy_list_ws_id)
   when matched then update
    set target.cost_in_tolerance      = use_this.cost_in_tolerance,
        target.qty_in_tolerance       = use_this.qty_in_tolerance;

   LOGGER.LOG_INFORMATION(L_program||' merge im_discrepancy_list_ws in_tolerance indicators - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   merge into im_discrepancy_list_ws target
   using (select gtt.varchar2_1 item,
                 gtt.number_1 invoice_id
            from gtt_num_num_str_str_date_date gtt) use_this
   on (    target.workspace_id = I_workspace_id
       and target.item = use_this.item
       and target.invoice_id = use_this.invoice_id)
   when matched then update
    set target.choice_flag   = 'Y';

   LOGGER.LOG_INFORMATION(L_program||'reapply choice flag into im_discrepancy_list_ws - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END CREATE_DISCREPANCY_LIST_WS;
----------------------------------------------------------------------------------------------
FUNCTION SET_DISCREPANCY_POPUP_FLAGS(O_error_message    OUT VARCHAR2,
                                     I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program    VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.SET_DISCREPANCY_POPUP_FLAGS';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   merge into im_detail_match_ws target
   using (--REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
          select distinct dl.sku_detail_match_ws_id detail_match_ws_id
            from im_discrepancy_list_ws dl
           where dl.workspace_id = I_workspace_id
             and dl.choice_flag  = 'Y'
          union all
          --REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE
          select distinct dm.ancestor_id detail_match_ws_id
            from im_discrepancy_list_ws dl,
                 im_detail_match_ws dm
           where dl.workspace_id           = I_workspace_id
             and dl.choice_flag            = 'Y'
             and dl.workspace_id           = dm.workspace_id
             and dl.sku_detail_match_ws_id = dm.detail_match_ws_id
             and dm.ancestor_id            is not null
   ) use_this
   on (target.detail_match_ws_id = use_this.detail_match_ws_id)
   when matched then update
    set target.choice_flag = 'Y';

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END SET_DISCREPANCY_POPUP_FLAGS;
----------------------------------------------------------------------------------------------
FUNCTION PUSH_DISCREPANCY_TO_DETAIL(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program    VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.PUSH_DISCREPANCY_TO_DETAIL';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;
   L_count      NUMBER(15)   := 0;

BEGIN

   select count(*)
    into L_count
    from im_discrepancy_list_ws dl
   where dl.workspace_id = I_workspace_id;

   if L_count > 0 then

      --filter and set choice flag in im_detail_match_ws for choices in discrepancy workspace (STYLE/SKU/INVC/RCPT)
      --for rows that do not have the choice flag set in im_discrepancy_list_ws
      update im_detail_match_ws dm
         set dm.discrepancy_filter_ind = 'Y',
             dm.choice_flag            = 'N'
       where dm.workspace_id           = I_workspace_id
         and dm.detail_match_ws_id     not in (--DETAIL_MATCH_WS_ENTITY_SKU
                                               select distinct dl.sku_detail_match_ws_id detail_match_ws_id
                                                 from im_discrepancy_list_ws dl
                                                where dl.workspace_id = I_workspace_id
                                                  and dl.choice_flag  = 'Y'
                                               union
                                               --DETAIL_MATCH_WS_ENTITY_INVC
                                               select distinct dl.invc_detail_match_ws_id detail_match_ws_id
                                                 from im_discrepancy_list_ws dl
                                                where dl.workspace_id = I_workspace_id
                                                  and dl.choice_flag  = 'Y'
                                               union
                                               --DETAIL_MATCH_WS_ENTITY_RCPT
                                               select distinct dlr.rcpt_detail_match_ws_id detail_match_ws_id
                                                 from im_discrepancy_list_ws dl,
                                                      im_discrepancy_list_rcpt_ws dlr
                                                where dl.workspace_id           = I_workspace_id
                                                  and dl.choice_flag            = 'Y'
                                                  and dl.workspace_id           = dlr.workspace_id
                                                  and dl.discrepancy_list_ws_id = dlr.discrepancy_list_ws_id
                                               union
                                               --DETAIL_MATCH_WS_ENTITY_STYLE
                                               select distinct dm.ancestor_id detail_match_ws_id
                                                 from im_discrepancy_list_ws dl,
                                                      im_detail_match_ws dm
                                                where dl.workspace_id           = I_workspace_id
                                                  and dl.choice_flag            = 'Y'
                                                  and dl.workspace_id           = dm.workspace_id
                                                  and dl.sku_detail_match_ws_id = dm.detail_match_ws_id
                                                  and dm.ancestor_id            is not null
                                              );

      LOGGER.LOG_INFORMATION(L_program||' update im_detail_match_ws discrepancy_filter_ind = Y - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into im_detail_match_ws target
      using (--REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             select distinct dl.sku_detail_match_ws_id detail_match_ws_id,
                    'Y' choice_flag,
                    NULL match_key_id
               from im_discrepancy_list_ws dl
              where dl.workspace_id = I_workspace_id
                and dl.choice_flag  = 'Y'
             union all
             --REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
             select distinct dl.invc_detail_match_ws_id detail_match_ws_id,
                    'Y' choice_flag,
                    dl.match_key_id
               from im_discrepancy_list_ws dl
              where dl.workspace_id = I_workspace_id
                and dl.choice_flag  = 'Y'
             union all
             --REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             select distinct dlr.rcpt_detail_match_ws_id detail_match_ws_id,
                    decode(count(distinct dm.receipt_unit_cost) over (partition by dm.ancestor_id),
                           1, 'Y',
                           'N') choice_flag,
                    dl.match_key_id
               from im_discrepancy_list_ws dl,
                    im_discrepancy_list_rcpt_ws dlr,
                    im_detail_match_ws dm
              where dl.workspace_id             = I_workspace_id
                and dl.choice_flag              = 'Y'
                and dl.workspace_id             = dlr.workspace_id
                and dl.discrepancy_list_ws_id   = dlr.discrepancy_list_ws_id
                and dlr.rcpt_detail_match_ws_id = dm.detail_match_ws_id
             union all
             --REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE
             select distinct dm.ancestor_id detail_match_ws_id,
                    'Y' choice_flag,
                    NULL match_key_id
               from im_discrepancy_list_ws dl,
                    im_detail_match_ws dm
              where dl.workspace_id           = I_workspace_id
                and dl.choice_flag            = 'Y'
                and dl.workspace_id           = dm.workspace_id
                and dl.sku_detail_match_ws_id = dm.detail_match_ws_id
                and dm.ancestor_id            is not null
      ) use_this
      on (target.detail_match_ws_id = use_this.detail_match_ws_id)
      when matched then update
       set target.discrepancy_filter_ind = 'N',
           target.ui_filter_ind          = 'N',
           target.match_key_id           = use_this.match_key_id,
           target.choice_flag            = use_this.choice_flag; --note will get overwritten by ROLLUP_DETAIL_MATCH_WS for SKU/STYLE

      LOGGER.LOG_INFORMATION(L_program||' merge im_detail_match_ws discrepancy_filter_ind = N - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --turn off choice flags on all summary match tables.
      update im_match_invc_ws set choice_flag = 'N' where workspace_id = I_workspace_id;
      LOGGER.LOG_INFORMATION(L_program||' im_match_invc_ws reset choice flag - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
      update im_match_invc_detl_ws set choice_flag = 'N' where workspace_id = I_workspace_id;
      LOGGER.LOG_INFORMATION(L_program||' im_match_invc_detl_ws reset choice flag - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
      update im_match_rcpt_ws set choice_flag = 'N' where workspace_id = I_workspace_id;
      LOGGER.LOG_INFORMATION(L_program||' im_match_rcpt_ws reset choice flag - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
      update im_match_rcpt_detl_ws set choice_flag = 'N' where workspace_id = I_workspace_id;
      LOGGER.LOG_INFORMATION(L_program||' im_match_rcpt_detl_ws reset choice flag - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      --set choice flags on all summary match based on selection in discrepancy list
      merge into im_match_invc_detl_ws target
      using (select d.workspace_id,
                    d.invoice_id doc_id,
                    d.item
               from im_detail_match_ws d
              where d.workspace_id = I_workspace_id
                and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                and d.choice_flag  = 'Y'
      ) use_this
      on (    target.workspace_id = use_this.workspace_id
          and target.doc_id       = use_this.doc_id
          and target.item         = use_this.item)
      when matched then update
       set target.choice_flag = 'Y';

      LOGGER.LOG_INFORMATION(L_program||' merge im_match_invc_detl_ws set choice flag from discrepancy list choice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into im_match_rcpt_detl_ws target
      using (select d.workspace_id,
                    d.receipt_id shipment,
                    d.item
               from im_detail_match_ws d
              where d.workspace_id = I_workspace_id
                and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                and d.choice_flag  = 'Y'
      ) use_this
      on (    target.workspace_id = use_this.workspace_id
          and target.shipment     = use_this.shipment
          and target.item         = use_this.item)
      when matched then update
       set target.choice_flag = 'Y';

      LOGGER.LOG_INFORMATION(L_program||' merge im_match_rcpt_detl_ws set choice flag from discrepancy list choice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into im_match_invc_ws target
      using (select distinct d.workspace_id,
                    d.invoice_id doc_id
               from im_detail_match_ws d
              where d.workspace_id = I_workspace_id
                and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                and d.choice_flag  = 'Y'
      ) use_this
      on (    target.workspace_id = use_this.workspace_id
          and target.doc_id       = use_this.doc_id)
      when matched then update
       set target.choice_flag = 'Y';

      LOGGER.LOG_INFORMATION(L_program||' merge im_match_invc_ws set choice flag from discrepancy list choice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into im_match_rcpt_ws target
      using (select distinct d.workspace_id,
                    d.receipt_id shipment
               from im_detail_match_ws d
              where d.workspace_id = I_workspace_id
                and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                and d.choice_flag  = 'Y'
      ) use_this
      on (    target.workspace_id = use_this.workspace_id
          and target.shipment     = use_this.shipment)
      when matched then update
       set target.choice_flag = 'Y';

      LOGGER.LOG_INFORMATION(L_program||' merge im_match_rcpt_ws set choice flag from discrepancy list choice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   else -- not discrepancy list client

      ------------------------------------------------
      --manual group = choice flag N
      ------------------------------------------------

      merge into im_detail_match_ws tgt
      using (select i.item
               from (select d.item,
                            max(d.manual_group_id) man_grp
                       from im_detail_match_ws d
                      where d.workspace_id = I_workspace_id
                        and d.entity_type  in(REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU,
                                              REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC)
                      group by d.item) i
              where i.man_grp is not null
             union
             select i.item
               from (select d.item,
                            max(gr.group_id) man_grp
                       from im_detail_match_ws d,
                            im_manual_group_receipts gr
                      where d.workspace_id = I_workspace_id
                        and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                        and d.receipt_id   = gr.shipment
                      group by d.item) i
              where i.man_grp is not null
      ) src
      on (    tgt.workspace_id = I_workspace_id
          and tgt.item         = src.item)
      when MATCHED THEN
         update
            set tgt.choice_flag = 'N';

      LOGGER.LOG_INFORMATION(L_program||' manual group choice flag N SKU, INVC, RCPT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if ROLLUP_DETAIL_MATCH_WS(O_error_message,
                             I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END PUSH_DISCREPANCY_TO_DETAIL;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DISCREP_SELECT_ALL(O_error_message    OUT VARCHAR2,
                                     I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program    VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.VALIDATE_DISCREP_SELECT_ALL';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;
   L_error_ind  VARCHAR2(1)  := null;

   cursor c_invc_cnt is
      select 'x'
        from (select count(invoice_id) invc_count
                from im_discrepancy_list_ws  ws
               where ws.workspace_id = I_workspace_id
               group by item) i
       where i.invc_count > 1;

   cursor c_in_favor is
      select 'x'
        from (select distinct
                     dl.item, dl.invoice_id,
                     min(sign(dmi.invoice_unit_cost-dmr.receipt_unit_cost)) over (partition by dl.item, dl.invoice_id) min_sc,
                     max(sign(dmi.invoice_unit_cost-dmr.receipt_unit_cost)) over (partition by dl.item, dl.invoice_id) max_sc,
                     min(sign(sign(dmi.invoice_qty-dmr.receipt_avail_qty))) over (partition by dl.item, dl.invoice_id) min_sq,
                     max(sign(sign(dmi.invoice_qty-dmr.receipt_avail_qty))) over (partition by dl.item, dl.invoice_id) max_sq
                from im_discrepancy_list_ws dl,
                     im_detail_match_ws dmi,
                     im_discrepancy_list_rcpt_ws dlr,
                     im_detail_match_ws dmr
               where dl.workspace_id              = I_workspace_id
                 and dl.workspace_id              = dmi.workspace_id
                 and dl.invc_detail_match_ws_id   = dmi.detail_match_ws_id
                 --
                 and dl.workspace_id              = dlr.workspace_id
                 and dl.discrepancy_list_ws_id    = dlr.discrepancy_list_ws_id
                 and dlr.workspace_id             = dmr.workspace_id
                 and dlr.rcpt_detail_match_ws_id  = dmr.detail_match_ws_id
              ) i
       where abs(i.min_sc - i.max_sc) > 1
          or abs(i.min_sq - i.max_sq) > 1;

BEGIN

   open c_invc_cnt;
   fetch c_invc_cnt into L_error_ind;
   close c_invc_cnt;

   if L_error_ind = 'x' then
      O_error_message := REIM_ONLINE_MATCH_SQL.ERROR_MULTI_ITEM_INVC;
      return REIM_CONSTANTS.FAIL;
   end if;

   --

   open c_in_favor;
   fetch c_in_favor into L_error_ind;
   close c_in_favor;

   if L_error_ind = 'x' then
      O_error_message := REIM_ONLINE_MATCH_SQL.ERROR_MULTI_IN_FAVOR;
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END VALIDATE_DISCREP_SELECT_ALL;
----------------------------------------------------------------------------------------------
/**
 * The public function used to persist match data to Operational tables for Online detail matching.
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERSIST_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                 I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.PERSIST_DETAIL_MATCH_WS';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_user    user_attrib.user_id%TYPE;
   L_vdate   DATE := get_vdate;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:' || I_workspace_id);

   L_user := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
   if L_user is null then
      L_user := get_user;
   end if;

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --write match history
   --------------------------------------

   delete from gtt_num_num_str_str_date_date;

   --number_1   detail_match_history_seq
   --varchar2_1 item
   --varchar2_2 exact_match
   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1,
                                             varchar2_2)
   select im_detail_match_history_seq.nextval,
          d.item,
          case when d.qty_variance = 0 and d.unit_cost_variance = 0 then 'Y' else 'N' end
     from im_detail_match_ws d
    where d.workspace_id  = I_workspace_id
      and d.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and d.choice_flag   = 'Y'
      and d.ui_filter_ind = 'N';

   insert into im_detail_match_history (match_id,
                                        auto_matched,
                                        exact_match,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             'N',
             gtt.varchar2_2,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_invc_history (match_id,
                                             doc_id,
                                             item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             d.invoice_id,
             d.item,
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt,
             im_detail_match_ws d
       where gtt.varchar2_1 = d.item
         and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
         and d.choice_flag  = 'Y'
         and d.workspace_id = I_workspace_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_rcpt_history (match_id,
                                             shipment,
                                             item,
                                             substitute_item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             dr.receipt_id,
             dr.display_item,
             DECODE(dr.item,
                    dr.display_item, NULL,
                    dr.item),
             L_user,
             sysdate,
             L_user,
             sysdate,
             0
        from gtt_num_num_str_str_date_date gtt,
             im_detail_match_ws dr
       where gtt.varchar2_1  = dr.display_item
         and dr.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
         and dr.choice_flag  = 'Y'
         and dr.workspace_id = I_workspace_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to partially matched receipts
   --------------------------------------

   merge into im_partially_matched_receipts target
   using (select d.receipt_id shipment,
                 d.item,
                 d.receipt_avail_qty qty_matched
            from im_detail_match_ws d,
                 gtt_num_num_str_str_date_date gtt
           where d.workspace_id  = I_workspace_id
             and d.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and d.choice_flag   = 'Y'
             and d.display_item  = gtt.varchar2_1
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item)
   when matched then update
    set target.qty_matched       = target.qty_matched + use_this.qty_matched,
        target.last_updated_by   = L_user,
        target.last_update_date  = sysdate,
        target.object_version_id = target.object_version_id + REIM_CONSTANTS.ONE
   when not matched then insert (shipment,
                                 item,
                                 qty_matched,
                                 created_by,
                                 creation_date,
                                 last_updated_by,
                                 last_update_date,
                                 object_version_id)
                         values (use_this.shipment,
                                 use_this.item,
                                 use_this.qty_matched,
                                 L_user,
                                 sysdate,
                                 L_user,
                                 sysdate,
                                 REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' Merge im_partially_matched_receipts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to posting
   --------------------------------------

   delete from gtt_6_num_6_str_6_date;

   --number_1   receipt_item_posting_seq
   --number_2   shipment
   --number_3   qty_matched
   --varchar2_1 item
   --varchar2_2 substitute_item
   insert into gtt_6_num_6_str_6_date(number_1,
                                      number_2,
                                      number_3,
                                      varchar2_1,
                                      varchar2_2)
   select im_receipt_item_posting_seq.nextval,
          idmwr.receipt_id shipment,
          idmwr.receipt_avail_qty,
          idmwr.item,
          idmwr.substitute_item
     from gtt_num_num_str_str_date_date gtt,
          im_detail_match_ws idmwr
    where idmwr.workspace_id  = I_workspace_id
      and idmwr.entity_type   = 'RCPT'
      and idmwr.choice_flag   = 'Y'
      and idmwr.ui_filter_ind = 'N'
      and idmwr.display_item  = gtt.varchar2_1;

   insert into im_receipt_item_posting (seq_no,
                                        shipment,
                                        item,
                                        qty_matched,
                                        qty_posted,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             gtt.number_2,
             gtt.varchar2_1,
             gtt.number_3,
             NULL,
          L_user,
          sysdate,
          L_user,
          sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_rcpt_item_posting_invoice (seq_no,
                                             doc_id,
                                             status,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
   select gtt.number_1,
          di.invoice_id,
          'M',
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_6_num_6_str_6_date gtt,
          im_detail_match_ws di
    where di.workspace_id = I_workspace_id
      and di.entity_type  = 'INVC'
      and di.choice_flag  = 'Y'
      and di.item         = Nvl(gtt.varchar2_2, gtt.varchar2_1);

   LOGGER.LOG_INFORMATION(L_program||' insert im_rcpt_item_posting_invoice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to doc entities
   --------------------------------------

   merge into im_invoice_detail target
   using (select di.invoice_id doc_id,
                 di.item,
                 ds.unit_cost_variance cost_variance,
                 ds.qty_variance qty_variance
            from gtt_num_num_str_str_date_date gtt,
                 im_detail_match_ws di,
                 im_detail_match_ws ds
           where di.workspace_id = I_workspace_id
             and di.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
             and di.choice_flag  = 'Y'
             and di.item         = gtt.varchar2_1
             --
             and ds.workspace_id = I_workspace_id
             and ds.item         = gtt.varchar2_1
             and ds.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and ds.choice_flag  = 'Y'
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status                         = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched                   = 'Y',
        target.qty_matched                    = 'Y',
        target.cost_variance_within_tolerance = use_this.cost_variance,
        target.qty_variance_within_tolerance  = use_this.qty_variance,
        target.last_updated_by                = L_user,
        target.last_update_date               = sysdate,
        target.object_version_id              = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' merge im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head target
   using (select di.invoice_id doc_id,
                 SUM(DECODE(id.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) status_cnt
            from im_detail_match_ws ds,
                 im_detail_match_ws di,
                 im_invoice_detail id
           where ds.workspace_id          = I_workspace_id
             and ds.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and ds.choice_flag           = 'Y'
             and ds.ui_filter_ind         = 'N'
             --
             and di.workspace_id          = I_workspace_id
             and di.entity_type           = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
             and di.choice_flag           = 'Y'
             and di.ui_filter_ind         = 'N'
             --
             and ds.item                  = di.item
             --
             and di.invoice_id            = id.doc_id
           GROUP BY di.invoice_id
   ) use_this
   on (target.doc_id       = use_this.doc_id)
   when matched then update
    set target.status                    = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                  REIM_CONSTANTS.DOC_STATUS_URMTCH),
        target.match_id                  = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, L_user,
                                                  NULL),
        target.match_date                = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, L_vdate,
                                                  NULL),
        target.match_type                = decode(use_this.status_cnt,
                                                  REIM_CONSTANTS.ZERO, REIM_CONSTANTS.MATCH_TYPE_MANUAL,
                                                  NULL),
        target.last_updated_by           = L_user,
        target.last_update_date          = sysdate,
        target.object_version_id         = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to shipment entities
   --------------------------------------

   merge into shipsku target
   using (select ss.shipment,
                 ss.item,
                 ss.seq_no,
                 nvl(ss.qty_received,0) - nvl(ss.qty_matched,0) qty_matched
            from gtt_num_num_str_str_date_date gtt,
                 im_detail_match_ws dr,
                 shipsku ss
           where dr.workspace_id                   = I_workspace_id
             and dr.display_item                   = gtt.varchar2_1
             and dr.entity_type                    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and dr.choice_flag                    = 'Y'
             --
             and dr.receipt_id                     = ss.shipment
             and dr.item                           = ss.item
             and nvl(ss.invc_match_status,'-999') != REIM_CONSTANTS.SSKU_IM_STATUS_MTCH
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item
       and target.seq_no   = use_this.seq_no)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
        target.qty_matched       = NVL(target.qty_matched, REIM_CONSTANTS.ZERO) + use_this.qty_matched;

   LOGGER.LOG_INFORMATION(L_program||' merge shipsku - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into shipment target
   using (select /*+ ORDERED */
                 distinct ss.shipment,
                 count(distinct decode(ss.invc_match_status,null,'U',ss.invc_match_status)) over (partition by ss.shipment) status_cnt
            from im_detail_match_ws ds,
                 im_detail_match_ws dr,
                 shipsku ss
           where ds.workspace_id                   = I_workspace_id
             and ds.entity_type                    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and ds.choice_flag                    = 'Y'
             and ds.ui_filter_ind                  = 'N'
             --
             and dr.workspace_id                   = I_workspace_id
             and dr.entity_type                    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and dr.choice_flag                    = 'Y'
             and dr.ui_filter_ind                  = 'N'
             --
             and ds.item                           = dr.display_item
             --
             and dr.receipt_id                     = ss.shipment
   ) use_this
   on (    target.shipment     = use_this.shipment
       and use_this.status_cnt = 1)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_MTCH,
        target.invc_match_date   = L_vdate;

   LOGGER.LOG_INFORMATION(L_program||' merge shipment - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --push match to workspace
   --------------------------------------

   if REFRESH_MTCH_WSPACE(O_error_message,
                          I_workspace_id,
                          'N') = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --refresh the detail workspace
   --------------------------------------

   if CREATE_DETAIL_MATCH_WS(O_error_message,
                             I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if REFRESH_DISCREPANCY_LIST_WS(O_error_message,
                                  I_workspace_id) = 0 then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERSIST_DETAIL_MATCH_WS;
----------------------------------------------------------------------------------------------
/**
 * The public function used to populate the Invoice Item workspace table used for Online detail matching.
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_INVC_ITEM_VIEW_WS(O_error_message    OUT VARCHAR2,
                                  I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.CREATE_INVC_ITEM_VIEW_WS';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_count            NUMBER(15)   := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:' || I_workspace_id);

   --------------------------------------
   --set choice_flag aside to reapply at the end
   --------------------------------------
   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date(varchar2_1,
                                             number_1)
   select iiivw.item,
          iiivw.invoice_id
     from im_invc_item_view_ws iiivw
    where iiivw.choice_flag  = 'Y'
      and iiivw.workspace_id = I_workspace_id;

   LOGGER.LOG_INFORMATION(L_program||' save choice flag for workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --clear out Invoice Item view workspace
   ------------------------------------------------
   delete from im_invc_item_view_rcpt_ws where workspace_id = I_workspace_id;
   LOGGER.LOG_INFORMATION(L_program||' clear invc_item_rcpt workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   delete from im_invc_item_view_ws where workspace_id = I_workspace_id;
   LOGGER.LOG_INFORMATION(L_program||' clear invc_item workspace: '||I_workspace_id||' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_transform_ordloc_gtt;

   insert into im_transform_ordloc_gtt (order_no,
                                        item,
                                        item_desc,
                                        loc,
                                        loc_type,
                                        qty_ordered,
                                        unit_cost,
                                        unit_cost_init,
                                        cost_source,
                                        sup_qty_level,
                                        catch_weight_type,
                                        import_loc,
                                        import_loc_type,
                                        transform_qty_ordered,
                                        transform_unit_cost,
                                        transform_unit_cost_init)
   select mi.order_no,
          invc_ws.item,
          im.item_desc,
          ol.location,
          ol.loc_type,
          sum(ol.qty_ordered),
          min(ol.unit_cost),
          min(ol.unit_cost_init),
          ol.cost_source,
          s.sup_qty_level,
          im.catch_weight_type,
          ol.location,
          ol.loc_type,
          sum(ol.qty_ordered),
          min(ol.unit_cost),
          min(ol.unit_cost_init)
     from im_detail_match_ws invc_ws,
          im_match_invc_ws mi,
          ordloc ol,
          sups s,
          item_master im
    where invc_ws.workspace_id   = I_workspace_id
      and invc_ws.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
      and invc_ws.match_status   = 'U'
      and invc_ws.workspace_id   = mi.workspace_id
      and invc_ws.invoice_id     = mi.doc_id
      and mi.order_no            = ol.order_no
      and mi.po_supplier_site_id = s.supplier
      and invc_ws.item           = im.item
      and invc_ws.item           = ol.item (+)
    GROUP BY mi.order_no,
             invc_ws.item,
             im.item_desc,
             ol.location,
             ol.loc_type,
             ol.cost_source,
             s.sup_qty_level,
             im.catch_weight_type;

   LOGGER.LOG_INFORMATION(L_program||'insert into im_transform_ordloc_gtt - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   if REIM_XFORM_ORDLOC_SQL.TRANSFORM_ORDLOC_GTT(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   ------------------------------------------------
   --populate Invoice Item view workspace
   ------------------------------------------------

   insert into im_invc_item_view_ws (invc_item_view_ws_id,
                                     workspace_id,
                                     invoice_id,
                                     ext_doc_id,
                                     item,
                                     item_description,
                                     vpn,
                                     item_parent,
                                     dept,
                                     class,
                                     subclass,
                                     currency_code,
                                     supplier_site_id,
                                     supplier_site_name,
                                     supplier,
                                     supplier_name,
                                     ap_reviewer,
                                     location,
                                     loc_type,
                                     location_name,
                                     invoice_total_cost,
                                     cash_discount,
                                     manual_group_id,
                                     order_no,
                                     po_supplier,
                                     po_supplier_site_id,
                                     order_unit_cost,
                                     orig_order_unit_cost,
                                     order_qty,
                                     invoice_unit_cost,
                                     invoice_qty,
                                     invoice_ext_cost,
                                     cost_matched,
                                     qty_matched,
                                     receipt_id,
                                     receipt_unit_cost,
                                     receipt_received_qty,
                                     receipt_avail_qty,
                                     receipt_ext_cost,
                                     unit_cost_variance,
                                     unit_cost_variance_pct,
                                     cost_in_tolerance,
                                     qty_variance,
                                     qty_variance_pct,
                                     qty_in_tolerance,
                                     in_tolerance,
                                     receipt_count,
                                     choice_flag,
                                     sku_detail_match_ws_id,
                                     invc_detail_match_ws_id)
                              select im_invc_item_view_ws_id_seq.nextval,
                                     I_workspace_id,
                                     imiw.doc_id invoice_id,
                                     imiw.ext_doc_id,
                                     idmw.item,
                                     idmw.item_description,
                                     idmwi.vpn,
                                     idmw.item_parent,
                                     idmw.dept,
                                     idmw.class,
                                     idmw.subclass,
                                     imiw.currency_code,
                                     imiw.supplier_site_id,
                                     imiw.supplier_site_name,
                                     imiw.supplier,
                                     imiw.supplier_name,
                                     so.ap_reviewer,
                                     imiw.location,
                                     imiw.loc_type,
                                     imiw.location_name,
                                     imiw.merch_amount invoice_total_cost,
                                     NULL cash_discount, --todo
                                     imiw.manual_group_id,
                                     imiw.order_no,
                                     imiw.po_supplier,
                                     imiw.po_supplier_site_id,
                                     gtt.transform_unit_cost order_unit_cost,
                                     gtt.transform_unit_cost_init orig_order_unit_cost,
                                     gtt.order_qty,
                                     idmwi.invoice_unit_cost,
                                     idmwi.invoice_qty,
                                     idmwi.invoice_ext_cost,
                                     idmwi.cost_matched,
                                     idmwi.qty_matched,
                                     NULL receipt_id, --merge later
                                     NULL receipt_unit_cost, --merge later
                                     REIM_CONSTANTS.ZERO receipt_received_qty, --merge later
                                     REIM_CONSTANTS.ZERO receipt_avail_qty, --merge later
                                     REIM_CONSTANTS.ZERO receipt_ext_cost, --merge later
                                     DECODE(idmwi.cost_matched,
                                            'Y', NULL,
                                            'R', NULL,
                                            gtt.transform_unit_cost - idmwi.invoice_unit_cost) unit_cost_variance,
                                     DECODE(idmwi.cost_matched,
                                            'Y', NULL,
                                            'R', NULL,
                                            ((gtt.transform_unit_cost - idmwi.invoice_unit_cost)/gtt.transform_unit_cost) * 100) unit_cost_variance_pct,
                                     'N' cost_in_tolerance, --merge later
                                     -1 * idmwi.invoice_qty qty_variance, --merge later
                                     NULL qty_variance_pct, --merge later
                                     'N' qty_in_tolerance, --merge later
                                     'N' in_tolerance, --merge later
                                     REIM_CONSTANTS.ZERO receipt_count, --merge later
                                     'N' choice_flag,
                                     idmw.detail_match_ws_id sku_detail_match_ws_id,
                                     idmwi.detail_match_ws_id invc_detail_match_ws_id
                                from (select order_no,
                                             item,
                                             import_loc,
                                             transform_unit_cost,
                                             transform_unit_cost_init,
                                             sum(transform_qty_ordered) order_qty
                                        from im_transform_ordloc_gtt
                                       GROUP BY order_no,
                                                item,
                                                import_loc,
                                                transform_unit_cost,
                                                transform_unit_cost_init) gtt,
                                     im_detail_match_ws idmw,
                                     im_match_invc_ws imiw,
                                     im_detail_match_ws idmwi,
                                     v_im_supp_site_attrib_expl so,
                                     (select store loc, store physical_loc from store
                                      union all
                                      select wh loc, physical_wh physical_loc from wh) locs
                               where idmw.workspace_id       = I_workspace_id
                                 and idmw.workspace_id       = idmwi.workspace_id
                                 and idmw.detail_match_ws_id = idmwi.ancestor_id
                                 --
                                 and idmw.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                                 and idmwi.entity_type       = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                                 and idmwi.match_status      = 'U'
                                 --
                                 and idmwi.workspace_id      = imiw.workspace_id
                                 and idmwi.invoice_id        = imiw.doc_id
                                 --
                                 and imiw.order_no           = gtt.order_no
                                 and idmw.item               = gtt.item
                                 and locs.loc                = gtt.import_loc
                                 and imiw.location           = locs.physical_loc
                                 --
                                 and so.supplier             = imiw.supplier_site_id;

   LOGGER.LOG_INFORMATION(L_program||'insert into im_invc_item_view_ws - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   insert into im_invc_item_view_rcpt_ws (invc_item_view_rcpt_ws_id,
                                          invc_item_view_ws_id,
                                          workspace_id,
                                          rcpt_detail_match_ws_id)
                                   select im_invc_item_view_rcpt_seq.nextval,
                                          inner.invc_item_view_ws_id,
                                          I_workspace_id,
                                          inner.detail_match_ws_id
                                     from (select iiivw.invc_item_view_ws_id,
                                                  idmwr.detail_match_ws_id
                                             from im_invc_item_view_ws iiivw,
                                                  im_detail_match_ws idmwr
                                            where iiivw.workspace_id           = I_workspace_id
                                              and iiivw.manual_group_id        is NULL
                                              and iiivw.workspace_id           = idmwr.workspace_id
                                              and iiivw.sku_detail_match_ws_id = idmwr.ancestor_id
                                              and idmwr.entity_type            = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                                              and idmwr.match_status           = 'U'
                                              and NOT EXISTS (select 'x'
                                                                from im_manual_group_receipts imgr
                                                               where imgr.shipment = idmwr.receipt_id)
                                           union all
                                           select iiivw.invc_item_view_ws_id,
                                                  idmwr.detail_match_ws_id
                                             from im_invc_item_view_ws iiivw,
                                                  im_detail_match_ws idmwr,
                                                  im_manual_group_receipts imgr
                                            where iiivw.workspace_id           = I_workspace_id
                                              and iiivw.workspace_id           = idmwr.workspace_id
                                              and iiivw.sku_detail_match_ws_id = idmwr.ancestor_id
                                              and idmwr.entity_type            = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                                              and idmwr.match_status           = 'U'
                                              and imgr.group_id                = iiivw.manual_group_id
                                              and imgr.shipment                = idmwr.receipt_id) inner;

   LOGGER.LOG_INFORMATION(L_program||'insert into im_invc_item_view_rcpt_ws - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   merge into im_invc_item_view_ws tgt
   using (select iiivrw.workspace_id,
                 iiivrw.invc_item_view_ws_id,
                 MIN(idmwr.receipt_id) receipt_id,
                 MIN(idmwr.receipt_unit_cost) receipt_unit_cost,
                 SUM(idmwr.receipt_avail_qty) receipt_avail_qty,
                 SUM(idmwr.receipt_ext_cost) receipt_ext_cost,
                 SUM(idmwr.receipt_received_qty) receipt_received_qty,
                 COUNT(distinct idmwr.receipt_id) rcpt_count,
                 COUNT(distinct idmwr.receipt_unit_cost) unit_cost_count
            from im_invc_item_view_rcpt_ws iiivrw,
                 im_detail_match_ws idmwr
           where iiivrw.workspace_id             = I_workspace_id
             and iiivrw.workspace_id             = idmwr.workspace_id
             and iiivrw.rcpt_detail_match_ws_id  = idmwr.detail_match_ws_id
           GROUP BY iiivrw.workspace_id,
                    iiivrw.invc_item_view_ws_id) src
   on (    tgt.workspace_id         = src.workspace_id
       and tgt.invc_item_view_ws_id = src.invc_item_view_ws_id)
   when MATCHED then
      update
         set tgt.receipt_id           = DECODE(src.rcpt_count,
                                               REIM_CONSTANTS.ONE, src.receipt_id,
                                               NULL),
             tgt.receipt_unit_cost    = DECODE(src.unit_cost_count,
                                               REIM_CONSTANTS.ONE, src.receipt_unit_cost,
                                               NULL),
             tgt.receipt_received_qty = src.receipt_received_qty,
             tgt.receipt_avail_qty    = src.receipt_avail_qty,
             tgt.receipt_ext_cost     = src.receipt_ext_cost,
             tgt.receipt_count        = src.rcpt_count,
             tgt.qty_variance         = DECODE(tgt.qty_matched,
                                               'Y', NULL,
                                               'R', NULL,
                                               src.receipt_avail_qty - tgt.invoice_qty),
             tgt.qty_variance_pct     = DECODE(tgt.qty_matched,
                                               'Y', NULL,
                                               'R', NULL,
                                               DECODE(src.receipt_avail_qty,
                                                      REIM_CONSTANTS.ZERO, NULL,
                                                      ((src.receipt_avail_qty - tgt.invoice_qty)/src.receipt_avail_qty) * 100));

   LOGGER.LOG_INFORMATION(L_program||'merge im_invc_item_view_ws rcpt info - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   merge into im_invc_item_view_ws tgt
   using (select iiivw.workspace_id,
                 iiivw.invc_item_view_ws_id,
                 case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(iiivw.receipt_unit_cost, NVL(iiivw.order_unit_cost,0)) = 0 then
                         'N'
                              when tdc.tolerance_value < ABS(100 * (iiivw.invoice_unit_cost - NVL(iiivw.receipt_unit_cost, iiivw.order_unit_cost)) / NVL(iiivw.receipt_unit_cost, iiivw.order_unit_cost)) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdc.tolerance_value < ABS((iiivw.invoice_unit_cost - NVL(iiivw.receipt_unit_cost, NVL(iiivw.order_unit_cost,0))) * idmwi.tolerance_exchange_rate) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when iiivw.invoice_unit_cost = NVL(iiivw.receipt_unit_cost, NVL(iiivw.order_unit_cost,0)) then
                                 'Y'
                              else
                                 'N'
                         end
                 end cost_in_tolerance,
                 --
                 case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(iiivw.receipt_avail_qty, 0) = 0 then
                         'N'
                              when tdq.tolerance_value < ABS(100 * (iiivw.invoice_qty - iiivw.receipt_avail_qty) / iiivw.receipt_avail_qty) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdq.tolerance_value < ABS(iiivw.invoice_qty - NVL(iiivw.receipt_avail_qty, 0)) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when iiivw.invoice_qty = NVL(iiivw.receipt_avail_qty, 0) then
                                 'Y'
                              else
                                 'N'
                         end
                 end qty_in_tolerance
                 --
            from im_invc_item_view_ws iiivw,
                 im_detail_match_ws idmwi,
                 im_tolerance_detail tdc,
                 im_tolerance_detail tdq
           where iiivw.workspace_id       = I_workspace_id
             and idmwi.workspace_id       = iiivw.workspace_id
             and idmwi.detail_match_ws_id = iiivw.invc_detail_match_ws_id
             --
             and idmwi.tolerance_id       = tdc.tolerance_id(+)
             and tdc.match_level(+)       = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
             and tdc.match_type(+)        = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
             and tdc.lower_limit(+)       <= iiivw.invoice_unit_cost
             and tdc.upper_limit(+)       > iiivw.invoice_unit_cost
             and tdc.favor_of(+)          = case when iiivw.invoice_unit_cost > NVL(iiivw.receipt_unit_cost, NVL(iiivw.order_unit_cost,0))
                                                 then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                                 else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                            end
             --
             and idmwi.tolerance_id       = tdq.tolerance_id(+)
             and tdq.match_level(+)       = REIM_CONSTANTS.TLR_MTCH_LVL_LINE
             and tdq.match_type(+)        = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
             and tdq.lower_limit(+)       <= iiivw.invoice_qty
             and tdq.upper_limit(+)       > iiivw.invoice_qty
             and tdq.favor_of(+)          = case when iiivw.invoice_qty > NVL(iiivw.receipt_avail_qty, 0)
                                                 then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                                 else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                            end) src
   on (    tgt.workspace_id         = src.workspace_id
       and tgt.invc_item_view_ws_id = src.invc_item_view_ws_id)
   when MATCHED then
      update
         set tgt.cost_in_tolerance      = DECODE(tgt.cost_matched,
                                                 'Y', 'Y',
                                                 'R', 'Y',
                                                 src.cost_in_tolerance),
             tgt.qty_in_tolerance       = DECODE(tgt.qty_matched,
                                                 'Y', 'Y',
                                                 'R', 'Y',
                                                 src.qty_in_tolerance),
             tgt.in_tolerance           = case when tgt.cost_matched IN ('Y','R') then
                                                  src.qty_in_tolerance
                                               when tgt.qty_matched IN ('Y','R') then
                                                  src.cost_in_tolerance
                                               else
                                                  DECODE(src.cost_in_tolerance,
                                                         'N', 'N',
                                                         DECODE(src.qty_in_tolerance,
                                                                'N','N',
                                                                'Y'))
                                          end;

   LOGGER.LOG_INFORMATION(L_program||' merge im_invc_item_view_ws in_tolerance indicators - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   merge into im_invc_item_view_ws tgt
   using (select gtt.varchar2_1 item,
                 gtt.number_1 invoice_id
            from gtt_num_num_str_str_date_date gtt) src
   on (    tgt.workspace_id = I_workspace_id
       and tgt.item         = src.item
       and tgt.invoice_id   = src.invoice_id)
   when MATCHED then
      update
         set tgt.choice_flag   = 'Y';

   LOGGER.LOG_INFORMATION(L_program||'reapply choice flag into im_invc_item_view_ws - SQL%ROWCOUNT: '||SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END CREATE_INVC_ITEM_VIEW_WS;
----------------------------------------------------------------------------------------------
/**
 * The public function used to persist match data to Operational tables for Online detail matching.(Invoice Item View)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERSIST_DETL_MTCH_IIVIEW(O_error_message    OUT VARCHAR2,
                                  I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.PERSIST_DETL_MTCH_IIVIEW';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:' || I_workspace_id);

   --------------------------------------
   --Push selection from Invoice item view to detail Match workspace
   --------------------------------------
   if PUSH_INVC_ITEM_VIEW_TO_DETAIL(O_error_message,
                                    I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --Persist Detail Match to Operational tables
   --------------------------------------
   if PERSIST_DETAIL_MATCH_WS(O_error_message,
                              I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERSIST_DETL_MTCH_IIVIEW;
----------------------------------------------------------------------------------------------
/**
 * The public function used to perform Split Receipt.(Invoice Item View)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION SPLIT_RECEIPT_IIVIEW(O_error_message    OUT VARCHAR2,
                              I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.SPLIT_RECEIPT_IIVIEW';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:' || I_workspace_id);

   --------------------------------------
   --Push selection from Invoice item view to detail Match workspace
   --------------------------------------
   if PUSH_INVC_ITEM_VIEW_TO_DETAIL(O_error_message,
                                    I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --Split receipt
   --------------------------------------
   if SPLIT_RECEIPT(O_error_message,
                    I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END SPLIT_RECEIPT_IIVIEW;
----------------------------------------------------------------------------------------------
/**
 * The public function used to push user selection from the Invoice Item view to the default detail match view.
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PUSH_INVC_ITEM_VIEW_TO_DETAIL(O_error_message    OUT VARCHAR2,
                                       I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program    VARCHAR2(60) := 'REIM_ONLINE_MATCH_SQL.PUSH_INVC_ITEM_VIEW_TO_DETAIL';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;
   L_count      NUMBER(15)   := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id: ' || I_workspace_id);

   --Set choice flag to N in im_detail_match_ws for rows that do not have choice flag set in Invoice Item view workspace (STYLE/SKU/INVC/RCPT)
   update im_detail_match_ws idmw
      set idmw.choice_flag        = 'N'
    where idmw.workspace_id       = I_workspace_id
      and idmw.detail_match_ws_id NOT IN (--DETAIL_MATCH_WS_ENTITY_SKU
                                          select iiivw.sku_detail_match_ws_id detail_match_ws_id
                                            from im_invc_item_view_ws iiivw
                                           where iiivw.workspace_id = I_workspace_id
                                             and iiivw.choice_flag  = 'Y'
                                          union all
                                          --DETAIL_MATCH_WS_ENTITY_INVC
                                          select iiivw.invc_detail_match_ws_id detail_match_ws_id
                                            from im_invc_item_view_ws iiivw
                                           where iiivw.workspace_id = I_workspace_id
                                             and iiivw.choice_flag  = 'Y'
                                          union all
                                          --DETAIL_MATCH_WS_ENTITY_RCPT
                                          select iiivrw.rcpt_detail_match_ws_id detail_match_ws_id
                                            from im_invc_item_view_ws iiivw,
                                                 im_invc_item_view_rcpt_ws iiivrw
                                           where iiivw.workspace_id          = I_workspace_id
                                             and iiivw.choice_flag           = 'Y'
                                             and iiivrw.workspace_id         = iiivw.workspace_id
                                             and iiivrw.invc_item_view_ws_id = iiivw.invc_item_view_ws_id
                                          union all
                                          --DETAIL_MATCH_WS_ENTITY_STYLE
                                          select idmw.ancestor_id detail_match_ws_id
                                            from im_invc_item_view_ws iiivw,
                                                 im_detail_match_ws idmw
                                           where iiivw.workspace_id          = I_workspace_id
                                             and iiivw.choice_flag           = 'Y'
                                             and idmw.workspace_id           = iiivw.workspace_id
                                             and idmw.detail_match_ws_id     = iiivw.sku_detail_match_ws_id
                                             and idmw.ancestor_id            is NOT NULL);

   LOGGER.LOG_INFORMATION(L_program||' update im_detail_match_ws choice_flag = N - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Set choice flag to Y in im_detail_match_ws for rows that do have choice flag set in Invoice Item view workspace (STYLE/SKU/INVC/RCPT)
   merge into im_detail_match_ws tgt
   using (--DETAIL_MATCH_WS_ENTITY_SKU
          select distinct iiivw.sku_detail_match_ws_id detail_match_ws_id,
                 NULL match_key_id
            from im_invc_item_view_ws iiivw
           where iiivw.workspace_id = I_workspace_id
             and iiivw.choice_flag  = 'Y'
          union all
          --DETAIL_MATCH_WS_ENTITY_INVC
          select iiivw.invc_detail_match_ws_id detail_match_ws_id,
                 iiivw.manual_group_id match_key_id
            from im_invc_item_view_ws iiivw
           where iiivw.workspace_id = I_workspace_id
             and iiivw.choice_flag  = 'Y'
          union all
          --DETAIL_MATCH_WS_ENTITY_RCPT
          select iiivrw.rcpt_detail_match_ws_id detail_match_ws_id,
                 iiivw.manual_group_id match_key_id
            from im_invc_item_view_ws iiivw,
                 im_invc_item_view_rcpt_ws iiivrw
           where iiivw.workspace_id          = I_workspace_id
             and iiivw.choice_flag           = 'Y'
             and iiivrw.workspace_id         = iiivw.workspace_id
             and iiivrw.invc_item_view_ws_id = iiivw.invc_item_view_ws_id
          union all
          --DETAIL_MATCH_WS_ENTITY_STYLE
          select distinct idmw.ancestor_id detail_match_ws_id,
                 NULL match_key_id
            from im_invc_item_view_ws iiivw,
                 im_detail_match_ws idmw
           where iiivw.workspace_id          = I_workspace_id
             and iiivw.choice_flag           = 'Y'
             and idmw.workspace_id           = iiivw.workspace_id
             and idmw.detail_match_ws_id     = iiivw.sku_detail_match_ws_id
             and idmw.ancestor_id            is NOT NULL) src
   on (tgt.detail_match_ws_id = src.detail_match_ws_id)
   when MATCHED then
      update
         set tgt.match_key_id = src.match_key_id,
             tgt.choice_flag  = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' merge im_detail_match_ws choice_flag = Y - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Rollup totals and other info to sku or style rows
   if ROLLUP_DETAIL_MATCH_WS(O_error_message,
                             I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   -- Update choice flags which get overridden by the rollup function.
   merge into im_detail_match_ws tgt
   using (--DETAIL_MATCH_WS_ENTITY_SKU
          select distinct iiivw.sku_detail_match_ws_id detail_match_ws_id,
                 NULL match_key_id
            from im_invc_item_view_ws iiivw
           where iiivw.workspace_id = I_workspace_id
             and iiivw.choice_flag  = 'Y'
          union all
          --DETAIL_MATCH_WS_ENTITY_STYLE
          select distinct idmw.ancestor_id detail_match_ws_id,
                 NULL match_key_id
            from im_invc_item_view_ws iiivw,
                 im_detail_match_ws idmw
           where iiivw.workspace_id          = I_workspace_id
             and iiivw.choice_flag           = 'Y'
             and idmw.workspace_id           = iiivw.workspace_id
             and idmw.detail_match_ws_id     = iiivw.sku_detail_match_ws_id
             and idmw.ancestor_id            is NOT NULL) src
   on (tgt.detail_match_ws_id = src.detail_match_ws_id)
   when MATCHED then
      update
         set tgt.choice_flag = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' Merge im_detail_match_ws choice_flag = Y for SKU and STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END PUSH_INVC_ITEM_VIEW_TO_DETAIL;
----------------------------------------------------------------------------------------------
/**
 * The public function used to persist match data to Operational tables for Online detail matching (Style Level)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
------------------------------------------------------------------------------------------------
FUNCTION PERSIST_STYLE_LVL_DETL_MTCH_WS(O_error_message    OUT VARCHAR2,
                                        I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'REIM_ONLINE_MATCH_SQL.PERSIST_STYLE_LVL_DETL_MTCH_WS';

   L_start_time TIMESTAMP                := SYSTIMESTAMP;
   L_user       user_attrib.user_id%TYPE := get_user;
   L_vdate      DATE                     := get_vdate;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start - I_workspace_id:' || I_workspace_id);

   if DIRTY_LOCK_CHECKS(O_error_message,
                        I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   delete from gtt_6_num_6_str_6_date;
   --number_1   detail_match_history_seq
   --varchar2_1 parent_item
   --varchar2_2 exact_match
   --number_2   par_unit_cost_variance
   --number_3   par_qty_variance
   insert into gtt_6_num_6_str_6_date(number_1,
                                      varchar2_1,
                                      varchar2_2,
                                      number_2,
                                      number_3)
   select im_detail_match_history_seq.nextval,
          idmws.item,
          case when idmws.qty_variance = 0 and idmws.unit_cost_variance = 0 then 'Y' else 'N' end,
          idmws.unit_cost_variance,
          idmws.qty_variance
     from im_detail_match_ws idmws
    where idmws.workspace_id  = I_workspace_id
      and idmws.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE
      and idmws.choice_flag   = 'Y'
      and idmws.ui_filter_ind = 'N';

   LOGGER.LOG_INFORMATION(L_program||' insert gtt with im_detail_match_history_seq - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to detail match history tables
   --------------------------------------

   insert into im_detail_match_history (match_id,
                                        auto_matched,
                                        exact_match,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             'N',
             gtt.varchar2_2,
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_invc_history (match_id,
                                             doc_id,
                                             item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             idmwi.invoice_id,
             idmwi.item,
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt,
             im_detail_match_ws idmws,
             im_detail_match_ws idmwi
       where gtt.varchar2_1     = idmws.item_parent
         and idmws.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
         and idmws.choice_flag  = 'Y'
         and idmws.workspace_id = I_workspace_id
         and idmwi.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
         and idmwi.choice_flag  = 'Y'
         and idmwi.workspace_id = I_workspace_id
         and idmwi.ancestor_id  = idmws.detail_match_ws_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_invc_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_detail_match_rcpt_history (match_id,
                                             shipment,
                                             item,
                                             substitute_item,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
      select gtt.number_1,
             idmwr.receipt_id,
             idmwr.display_item,
             DECODE(idmwr.item,
                    idmwr.display_item, NULL,
                    idmwr.item),
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_6_num_6_str_6_date gtt,
             im_detail_match_ws idmws,
             im_detail_match_ws idmwr
       where gtt.varchar2_1     = idmws.item_parent
         and idmws.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
         and idmws.choice_flag  = 'Y'
         and idmws.workspace_id = I_workspace_id
         and idmwr.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
         and idmwr.choice_flag  = 'Y'
         and idmwr.workspace_id = I_workspace_id
         and idmwr.ancestor_id  = idmws.detail_match_ws_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_detail_match_rcpt_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to partially matched receipts
   --------------------------------------

   merge into im_partially_matched_receipts tgt
   using (select idmwr.receipt_id shipment,
                 idmwr.item,
                 idmwr.receipt_avail_qty qty_matched
            from gtt_6_num_6_str_6_date gtt,
                 im_detail_match_ws idmws,
                 im_detail_match_ws idmwr
           where gtt.varchar2_1      = idmws.item_parent
             and idmws.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and idmws.choice_flag   = 'Y'
             and idmws.workspace_id  = I_workspace_id
             and idmwr.workspace_id  = I_workspace_id
             and idmwr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and idmwr.choice_flag   = 'Y'
             and idmwr.ancestor_id   = idmws.detail_match_ws_id) src
   on (    tgt.shipment = src.shipment
       and tgt.item     = src.item)
   when matched then update
    set tgt.qty_matched       = tgt.qty_matched + src.qty_matched,
        tgt.last_updated_by   = L_user,
        tgt.last_update_date  = sysdate,
        tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE
   when not matched then insert (shipment,
                                 item,
                                 qty_matched,
                                 created_by,
                                 creation_date,
                                 last_updated_by,
                                 last_update_date,
                                 object_version_id)
                         values (src.shipment,
                                 src.item,
                                 src.qty_matched,
                                 L_user,
                                 sysdate,
                                 L_user,
                                 sysdate,
                                 REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' Merge im_partially_matched_receipts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   --------------------------------------
   --write to receipt item posting
   --------------------------------------
   delete from gtt_10_num_10_str_10_date;

   --number_1   receipt_item_posting_seq
   --number_2   shipment
   --number_3   qty_matched
   --varchar2_1 item
   --varchar2_2 substitute_item
   --varchar2_3 item_parent
   insert into gtt_10_num_10_str_10_date(number_1,
                                         number_2,
                                         number_3,
                                         varchar2_1,
                                         varchar2_2,
                                         varchar2_3)
   select im_receipt_item_posting_seq.nextval,
          idmwr.receipt_id shipment,
          idmwr.receipt_avail_qty,
          idmwr.item,
          idmwr.substitute_item,
          idmwr.item_parent
     from gtt_6_num_6_str_6_date gtt,
          im_detail_match_ws idmws,
          im_detail_match_ws idmwr
    where gtt.varchar2_1      = idmws.item_parent
      and idmws.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and idmws.choice_flag   = 'Y'
      and idmws.workspace_id  = I_workspace_id
      and idmwr.workspace_id  = I_workspace_id
      and idmwr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
      and idmwr.choice_flag   = 'Y'
      and idmwr.ui_filter_ind = 'N'
      and idmwr.ancestor_id   = idmws.detail_match_ws_id;

   insert into im_receipt_item_posting (seq_no,
                                        shipment,
                                        item,
                                        qty_matched,
                                        qty_posted,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date,
                                        object_version_id)
      select gtt.number_1,
             gtt.number_2,
             gtt.varchar2_1,
             gtt.number_3,
             NULL,
             L_user,
             sysdate,
             L_user,
             sysdate,
             REIM_CONSTANTS.ONE
        from gtt_10_num_10_str_10_date gtt;

   LOGGER.LOG_INFORMATION(L_program||' insert im_receipt_item_posting - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_rcpt_item_posting_invoice (seq_no,
                                             doc_id,
                                             status,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             object_version_id)
   select distinct gtt.number_1,
          idmwi.invoice_id,
          'M',
          L_user,
          sysdate,
          L_user,
          sysdate,
          REIM_CONSTANTS.ONE
     from gtt_10_num_10_str_10_date gtt,
          im_detail_match_ws idmws,
          im_detail_match_ws idmwi
    where gtt.varchar2_3     = idmws.item_parent
      and idmws.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and idmws.choice_flag  = 'Y'
      and idmws.workspace_id = I_workspace_id
      and idmwi.workspace_id = I_workspace_id
      and idmwi.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
      and idmwi.choice_flag  = 'Y'
      and idmwi.ancestor_id  = idmws.detail_match_ws_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_rcpt_item_posting_invoice - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if PRORATE_STYLE_LVL_QTY_VAR(O_error_message,
                                I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --write to doc entities
   --------------------------------------

   merge into im_invoice_detail target
   using (select gtt.varchar2_1 doc_id,
                 gtt.varchar2_2 item,
                 gtt.number_1   cost_variance,
                 gtt.number_2   qty_variance
            from gtt_10_num_10_str_10_date gtt
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status                         = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched                   = 'Y',
        target.qty_matched                    = 'Y',
        target.cost_variance_within_tolerance = use_this.cost_variance,
        target.qty_variance_within_tolerance  = use_this.qty_variance,
        target.last_updated_by                = L_user,
        target.last_update_date               = sysdate,
        target.object_version_id              = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' merge im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head tgt
   using (select iid.doc_id,
                 SUM(DECODE(iid.status,
                            REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) status_cnt
            from gtt_10_num_10_str_10_date gtt,
                 im_invoice_detail iid
           where iid.doc_id = gtt.varchar2_1
           GROUP BY iid.doc_id) src
   on (tgt.doc_id = src.doc_id)
   when matched then update
    set tgt.status            = DECODE(src.status_cnt,
                                       REIM_CONSTANTS.ZERO, REIM_CONSTANTS.DOC_STATUS_MTCH,
                                       REIM_CONSTANTS.DOC_STATUS_URMTCH),
        tgt.match_id          = DECODE(src.status_cnt,
                                       REIM_CONSTANTS.ZERO, L_user,
                                       NULL),
        tgt.match_date        = DECODE(src.status_cnt,
                                       REIM_CONSTANTS.ZERO, L_vdate,
                                       NULL),
        tgt.match_type        = DECODE(src.status_cnt,
                                       REIM_CONSTANTS.ZERO, REIM_CONSTANTS.MATCH_TYPE_MANUAL,
                                       NULL),
        tgt.detail_matched=REIM_CONSTANTS.YN_YES,
        tgt.last_updated_by   = L_user,
        tgt.last_update_date  = sysdate,
        tgt.object_version_id = tgt.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --write to shipment entities
   --------------------------------------

   merge into shipsku target
   using (select ss.shipment,
                 ss.item,
                 ss.seq_no,
                 nvl(ss.qty_received,0) - nvl(ss.qty_matched,0) qty_matched
            from gtt_6_num_6_str_6_date gtt,
                 im_detail_match_ws idmws,
                 im_detail_match_ws idmwr,
                 shipsku ss
           where gtt.varchar2_1     = idmws.item_parent
             and idmws.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and idmws.choice_flag  = 'Y'
             and idmws.workspace_id = I_workspace_id
             --
             and idmwr.workspace_id = I_workspace_id
             and idmwr.item_parent  = gtt.varchar2_1
             and idmwr.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and idmwr.choice_flag  = 'Y'
             and idmwr.ancestor_id  = idmws.detail_match_ws_id
             --
             and idmwr.receipt_id              = ss.shipment
             and idmwr.item                    = ss.item
             and nvl(ss.invc_match_status,'U') != REIM_CONSTANTS.SSKU_IM_STATUS_MTCH
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item
       and target.seq_no   = use_this.seq_no)
   when matched then update
    set target.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_MTCH,
        target.qty_matched       = NVL(target.qty_matched, REIM_CONSTANTS.ZERO) + use_this.qty_matched;

   LOGGER.LOG_INFORMATION(L_program||' merge shipsku - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into shipment tgt
   using (select /*+ ORDERED */
                 ss.shipment,
                 SUM(DECODE(NVL(ss.invc_match_status,'U'),
                            REIM_CONSTANTS.SSKU_IM_STATUS_MTCH, REIM_CONSTANTS.ZERO,
                            REIM_CONSTANTS.ONE)) status_cnt
            from gtt_6_num_6_str_6_date gtt,
                 im_detail_match_ws idmws,
                 im_detail_match_ws idmwr,
                 shipsku ss
           where gtt.varchar2_1      = idmws.item_parent
             and idmws.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and idmws.choice_flag   = 'Y'
             and idmws.workspace_id  = I_workspace_id
             --
             and idmwr.workspace_id  = I_workspace_id
             and idmwr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and idmwr.choice_flag   = 'Y'
             and idmwr.ui_filter_ind = 'N'
             --
             and idmwr.ancestor_id   = idmws.detail_match_ws_id
             --
             and idmwr.receipt_id    = ss.shipment
           GROUP BY ss.shipment) src
   on (    tgt.shipment   = src.shipment
       and src.status_cnt = REIM_CONSTANTS.ZERO)
   when matched then update
    set tgt.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_MTCH,
        tgt.invc_match_date   = L_vdate;

   LOGGER.LOG_INFORMATION(L_program||' merge shipment - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --------------------------------------
   --push match to workspace
   --------------------------------------

   if REFRESH_MTCH_WSPACE(O_error_message,
                          I_workspace_id,
                          'N') = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   --------------------------------------
   --refresh the detail workspace
   --------------------------------------

   if CREATE_DETAIL_MATCH_WS(O_error_message,
                             I_workspace_id) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PERSIST_STYLE_LVL_DETL_MTCH_WS;
----------------------------------------------------------------------------------------------
/**
 * The public function used to select or deselect all Styles and Skus on the Detail Matching workspace
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *              I_choice_flag   (Y or N for select/deselect)
 *              I_match_status  (U or M for unmatched or matched SKUs)
 * 
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION UPDATE_ALL_DETL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                  I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                                  I_choice_flag   IN     VARCHAR2,
                                  I_match_status  IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'REIM_ONLINE_MATCH_SQL.UPDATE_ALL_DETL_MATCH_WS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' Start - I_workspace_id:' || I_workspace_id
                                    ||' I_choice_flag:'          || I_choice_flag
                                    ||' I_match_status:'         || I_match_status);

   update im_detail_match_ws
      set choice_flag = I_choice_flag
    where workspace_id   = I_workspace_id
      and ui_filter_ind  = 'N'
      and entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
      and match_status   = DECODE(I_match_status,
                                  'U', 'U',
                                  match_status)
      and matched_filter = DECODE(I_match_status,
                                  'M', 'N',
                                  matched_filter);

   LOGGER.LOG_INFORMATION(L_program||' Select All SKUs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_choice_flag = 'Y' or I_match_status = 'M' then
      --------------------------------------
      --Rollup SKU Match info to Style
      --------------------------------------
      if ROLLUP_STYLE_DETAIL_MATCH_WS(O_error_message,
                                      I_workspace_id) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

      update im_detail_match_ws
         set choice_flag = I_choice_flag
       where workspace_id  = I_workspace_id
         and ui_filter_ind = 'N'
         and entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE;

      LOGGER.LOG_INFORMATION(L_program||' Select All Styles - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   else

      update im_detail_match_ws
         set choice_flag = 'N',
             match_status             = 'U',
             matched_filter           = 'Y',
             invoice_id               = NULL,
             invoice_ext_doc_id       = NULL,
             invoice_unit_cost        = NULL,
             invc_ordloc_unit_cost    = NULL,
             invoice_qty              = NULL,
             invoice_ext_cost         = NULL,
             invc_supplier_group_id   = NULL,
             invc_supplier            = NULL,
             invc_supplier_name       = NULL,
             invc_supplier_phone      = NULL,
             invc_po_supplier         = NULL,
             invc_po_supplier_site_id = NULL,
             invc_supplier_site_id    = NULL,
             invc_supplier_site_name  = NULL,
             cost_matched             = NULL,
             qty_matched              = NULL,
             receipt_id               = NULL,
             receipt_unit_cost        = NULL,
             receipt_avail_qty        = NULL,
             receipt_ext_cost         = NULL,
             receipt_received_qty     = NULL,
             rcpt_supplier            = NULL,
             rcpt_supplier_name       = NULL,
             rcpt_supplier_site_id    = NULL,
             rcpt_supplier_site_name  = NULL,
             selected_invoice_count   = REIM_CONSTANTS.ZERO,
             selected_receipt_count   = REIM_CONSTANTS.ZERO,
             in_tolerance             = NULL,
             unit_cost_variance       = NULL,
             unit_cost_variance_pct   = NULL,
             cost_tolerance_dtl_id    = NULL,
             cost_in_tolerance        = NULL,
             qty_variance             = NULL,
             qty_variance_pct         = NULL,
             qty_tolerance_dtl_id     = NULL,
             qty_in_tolerance         = NULL
       where workspace_id  = I_workspace_id
         and ui_filter_ind = 'N'
         and entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE;

      LOGGER.LOG_INFORMATION(L_program||' Deselect All Styles - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UPDATE_ALL_DETL_MATCH_WS;
----------------------------------------------------------------------------------------------
/**
 *  --------- PRIVATE PROCS AND FUNCTIONS BODIES --------------
 */
----------------------------------------------------------------------------------------------
/**
 * The private procedure used to
 *              a. Check for dirty records in the match workspace
 *              b. Check for DB locks on OPS tables.
 */
FUNCTION DIRTY_LOCK_CHECKS(O_error_message    OUT VARCHAR2,
                           I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.DIRTY_LOCK_CHECKS';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_table_name         VARCHAR2(30) := NULL;
   L_dirty_object       VARCHAR2(30) := NULL;
   L_dirty_rec_count    NUMBER       := REIM_CONSTANTS.ZERO;

   dirty_records  EXCEPTION;
   records_locked EXCEPTION;
   PRAGMA         EXCEPTION_INIT(records_locked, -54);

   cursor C_CHK_INVOICE is
      select count(1)
        from im_match_invc_ws imiw,
             im_doc_head idh
       where imiw.workspace_id        = I_workspace_id
         and imiw.choice_flag         = 'Y'
         and imiw.status              <> REIM_CONSTANTS.DOC_STATUS_MTCH
         and imiw.doc_id              = idh.doc_id
         and imiw.doc_head_version_id <> idh.object_version_id;

   cursor C_CHK_SHIPMENT is
      select count(1)
        from im_match_rcpt_ws imrw,
             shipment sh
       where imrw.workspace_id        = I_workspace_id
         and imrw.choice_flag         = 'Y'
         and imrw.invc_match_status   = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
         and sh.shipment              = imrw.shipment
         and sh.invc_match_status     <> imrw.invc_match_status;

   cursor C_CHK_SHIPSKU is
      with shipsku_ws as (select ss.shipment,
                                 ss.item,
                                 SUM(NVL(ss.qty_received, 0) - NVL(ss.qty_matched, 0)) ss_qty_available,
                                 ss.unit_cost ss_unit_cost,
                                 NVL(ss.invc_match_status, REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH) invc_match_status
                            from im_match_rcpt_ws imrw,
                                 shipsku ss
                           where imrw.workspace_id                                               = I_workspace_id
                             and imrw.choice_flag                                                = 'Y'
                             and imrw.invc_match_status                                          = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                             and ss.shipment                                                     = imrw.shipment
                             and NVL(ss.invc_match_status, REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH) = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH
                           GROUP BY ss.shipment,
                                    ss.item,
                                    ss.unit_cost,
                                    ss.invc_match_status)
      select count(1)
        from (select imrw.workspace_id,
                     imrdw.shipment,
                     imrdw.item ws_item,
                     imrdw.invc_match_status,
                     ss_ws.invc_match_status ss_invc_match_status,
                     imrdw.qty_available_nc,
                     ss_ws.ss_qty_available,
                     imrdw.unit_cost_nc,
                     ss_ws.ss_unit_cost
                from im_match_rcpt_ws imrw,
                     shipsku_ws ss_ws,
                     im_match_rcpt_detl_ws imrdw
               where imrw.workspace_id                                                  = I_workspace_id
                 and imrw.choice_flag                                                   = 'Y'
                 and imrw.invc_match_status                                             = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                 and ss_ws.shipment                                                     = imrw.shipment
                 and imrdw.shipment (+)                                                 = ss_ws.shipment
                 and imrdw.item (+)                                                     = ss_ws.item
                 and imrdw.workspace_id (+)                                             = imrw.workspace_id
                 and NVL(imrdw.invc_match_status, REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH) = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH)
       where ws_item is NULL
          or invc_match_status <> ss_invc_match_status
          or qty_available_nc  <> ss_qty_available
          or unit_cost_nc      <> ss_unit_cost;

   cursor C_LOCK_DOC_HEAD is
      select 'x'
        from im_doc_head idh
       where EXISTS (select 'x'
                       from im_match_invc_ws imiw
                      where imiw.workspace_id = I_workspace_id
                        and imiw.choice_flag  = 'Y'
                        and imiw.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH
                        and imiw.doc_id       = idh.doc_id)
         for UPDATE NOWAIT;

   cursor C_LOCK_INVC_DETAIL is
      select 'x'
        from im_invoice_detail iid
       where EXISTS (select 'x'
                       from im_match_invc_ws imiw
                      where imiw.workspace_id = I_workspace_id
                        and imiw.choice_flag  = 'Y'
                        and imiw.status       <> REIM_CONSTANTS.DOC_STATUS_MTCH
                        and imiw.doc_id       = iid.doc_id)
         for UPDATE NOWAIT;

   cursor C_LOCK_SHIPMENT is
      select 'x'
        from shipment sh
       where EXISTS (select 'x'
                       from im_match_rcpt_ws imrw
                      where imrw.workspace_id      = I_workspace_id
                        and imrw.choice_flag       = 'Y'
                        and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                        and imrw.shipment          = sh.shipment)
         for UPDATE NOWAIT;

   cursor C_LOCK_SHIPSKU is
      select 'x'
        from shipsku ss
       where EXISTS (select 'x'
                       from im_match_rcpt_ws imrw
                      where imrw.workspace_id      = I_workspace_id
                        and imrw.choice_flag       = 'Y'
                        and imrw.invc_match_status = REIM_CONSTANTS.SHIP_IM_STATUS_UNMTCH
                        and imrw.shipment          = ss.shipment)
         for UPDATE NOWAIT;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:'|| I_workspace_id);

   --DIRTY CHECKS
   L_dirty_object := 'INVOICE'; --constant
   open C_CHK_INVOICE;
   fetch C_CHK_INVOICE into L_dirty_rec_count;
   close C_CHK_INVOICE;

   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;

   L_dirty_object := 'SHIPMENT'; --constant
   open C_CHK_SHIPMENT;
   fetch C_CHK_SHIPMENT into L_dirty_rec_count;
   close C_CHK_SHIPMENT;

   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;

   open C_CHK_SHIPSKU;
   fetch C_CHK_SHIPSKU into L_dirty_rec_count;
   close C_CHK_SHIPSKU;

   if L_dirty_rec_count > REIM_CONSTANTS.ZERO then
      RAISE dirty_records;
   end if;

   --CHECK LOCKS
   L_table_name := 'IM_DOC_HEAD'; --constant
   open C_LOCK_DOC_HEAD;
   close C_LOCK_DOC_HEAD;

   L_table_name := 'IM_INVOICE_DETAIL'; --constant
   open C_LOCK_INVC_DETAIL;
   close C_LOCK_INVC_DETAIL;

   L_table_name := 'SHIPMENT'; --constant
   open C_LOCK_SHIPMENT;
   close C_LOCK_SHIPMENT;
   LOGGER.LOG_INFORMATION(L_program||' lock shipment');

   L_table_name := 'SHIPSKU'; --constant
   open C_LOCK_SHIPSKU;
   close C_LOCK_SHIPSKU;
   LOGGER.LOG_INFORMATION(L_program||' lock shipsku');

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when records_locked then
      O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED', --constant
                                            L_table_name,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when dirty_records then
      O_error_message := SQL_LIB.CREATE_MSG('DIRTY_RECORDS', --constant
                                            L_dirty_object,
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DIRTY_LOCK_CHECKS;
----------------------------------------------------------------------------------------------
/**
 * The private procedure used for Create/update/delete manual groups.
 * Used for Online Resolutions.
 */
FUNCTION MANAGE_MANUAL_GROUP(O_error_message     IN OUT VARCHAR2,
                             I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(61) := 'REIM_ONLINE_MATCH_SQL.MANAGE_MANUAL_GROUP';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_user    IM_DOC_HEAD.MATCH_ID%TYPE := SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID');
   L_vdate   PERIOD.VDATE%TYPE         := GET_VDATE;

   L_del_grp_list     OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_FETCH_DEL_GRP_LIST is
      select distinct imgi.group_id
        from im_detail_match_ws idmw,
             im_detail_match_ws idmwi,
             im_manual_group_invoices imgi
       where idmw.workspace_id  = I_workspace_id
         and idmw.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
         and idmw.choice_flag   = 'Y'
         and idmw.ui_filter_ind = 'N'
         --
         and idmwi.workspace_id  = I_workspace_id
         and idmwi.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
         and idmwi.choice_flag   = 'Y'
         and idmwi.ui_filter_ind = 'N'
         --
         and idmwi.workspace_id = idmw.workspace_id
         and idmwi.item         = idmw.item
         --
         and imgi.doc_id        = idmwi.invoice_id
         and NOT EXISTS (select 'x'
                           from im_invoice_detail iid
                          where iid.doc_id = imgi.doc_id
                            and DECODE(iid.cost_matched,
                                       'R','Y',
                                       'D','N',
                                       iid.cost_matched) <> DECODE(iid.qty_matched,
                                                                   'R','Y',
                                                                   'D','N',
                                                                   iid.qty_matched));

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id: ' || I_workspace_id);

   if L_user is null then
      L_user := get_user;
   end if;

   open C_FETCH_DEL_GRP_LIST;
   fetch C_FETCH_DEL_GRP_LIST BULK COLLECT into L_del_grp_list;
   close C_FETCH_DEL_GRP_LIST;

   delete
     from im_manual_group_receipts imgr
    where imgr.group_id in (select value(ids)
                              from table(cast(L_del_grp_list as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete Manual Group Receipts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_manual_group_invoices imgi
    where imgi.group_id in (select value(ids)
                              from table(cast(L_del_grp_list as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete Manual Group Invoices - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from im_manual_groups img
    where img.group_id in (select value(ids)
                             from table(cast(L_del_grp_list as OBJ_NUMERIC_ID_TABLE)) ids);

   LOGGER.LOG_INFORMATION(L_program||' Delete Manual Groups - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from gtt_num_num_str_str_date_date;
   delete from gtt_6_num_6_str_6_date;

   --number_1   group_id
   --number_2   doc_id
   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
   select im_manual_groups_seq.nextval,
          invoice_id
     from (select distinct idmwi.invoice_id
             from im_detail_match_ws idmw,
                  im_detail_match_ws idmwi,
                  im_invoice_detail iid
            where idmw.workspace_id  = I_workspace_id
              and idmw.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
              and idmw.choice_flag   = 'Y'
              and idmw.ui_filter_ind = 'N'
              --
              and idmwi.workspace_id  = I_workspace_id
              and idmwi.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
              and idmwi.choice_flag   = 'Y'
              and idmwi.ui_filter_ind = 'N'
              --
              and idmwi.workspace_id = idmw.workspace_id
              and idmwi.item         = idmw.item
              --
              and iid.doc_id         = idmwi.invoice_id
              and iid.item           = idmwi.item
              and iid.status         <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
              and NOT EXISTS (select 'x'
                                from im_manual_group_invoices imgi
                               where imgi.doc_id = iid.doc_id));

   --number_1   group_id
   --number_2   doc_id
   --number_3   match_key_id
   --varchar2_1 item
   insert into gtt_6_num_6_str_6_date (number_1,
                                       number_2,
                                       number_3,
                                       varchar2_1)
   with im_detl_mtch_ws as (select idmw.workspace_id,
                                   idmw.item
                              from im_detail_match_ws idmw
                             where idmw.workspace_id  = I_workspace_id
                               and idmw.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                               and idmw.choice_flag   = 'Y'
                               and idmw.ui_filter_ind = 'N')
   select gtt.number_1,
          idmwi.invoice_id,
          NVL(idmwi.match_key_id, -1),
          idmwi.item
     from gtt_num_num_str_str_date_date gtt,
          im_detl_mtch_ws idmw,
          im_detail_match_ws idmwi
    where idmwi.workspace_id  = I_workspace_id
      and idmwi.workspace_id  = idmw.workspace_id
      and idmwi.item          = idmw.item
      and idmwi.invoice_id    = gtt.number_2
      and idmwi.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
      and idmwi.choice_flag   = 'Y'
      and idmwi.ui_filter_ind = 'N'
   union all
   select imgi.group_id,
          iid.doc_id,
          NVL(idmwi.match_key_id, -1),
          iid.item
     from im_detl_mtch_ws idmw,
          im_detail_match_ws idmwi,
          im_invoice_detail iid,
          im_manual_group_invoices imgi
    where idmwi.workspace_id  = I_workspace_id
      and idmwi.workspace_id  = idmw.workspace_id
      and idmwi.item          = idmw.item
      and idmwi.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
      and idmwi.choice_flag   = 'Y'
      and idmwi.ui_filter_ind = 'N'
      and iid.doc_id          = idmwi.invoice_id
      and iid.item            = idmwi.item
      and iid.status          <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
      and iid.doc_id          = imgi.doc_id;

   LOGGER.LOG_INFORMATION(L_program||' Fetch New and Old groups in gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_manual_groups tgt
   using (select distinct gtt.number_1 group_id
            from gtt_6_num_6_str_6_date gtt) src
   on (tgt.group_id = src.group_id)
   when NOT MATCHED then
      insert (group_id,
              created_by,
              creation_date,
              last_updated_by,
              last_update_date,
              object_version_id)
      values (src.group_id,
              L_user,
              L_vdate,
              L_user,
              L_vdate,
              REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' Create New Manual Groups - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_manual_group_invoices tgt
   using (select distinct gtt.number_1 group_id,
                 gtt.number_2 doc_id
            from gtt_6_num_6_str_6_date gtt) src
   on (    tgt.group_id = src.group_id
       and tgt.doc_id   = src.doc_id)
   when NOT MATCHED then
      insert (group_id,
              doc_id,
              created_by,
              creation_date,
              last_updated_by,
              last_update_date,
              object_version_id)
      values (src.group_id,
              src.doc_id,
              L_user,
              L_vdate,
              L_user,
              L_vdate,
              REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' Create New Manual Group Invoices - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_manual_group_receipts tgt
   using (select distinct gtt.number_1 group_id,
                 idmwr.receipt_id shipment
            from gtt_6_num_6_str_6_date gtt,
                 im_detail_match_ws idmwr
           where idmwr.workspace_id  = I_workspace_id
             and idmwr.item          = gtt.varchar2_1
             and idmwr.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
             and NVL(idmwr.match_key_id, -1) = gtt.number_3
             and idmwr.choice_flag   = 'Y'
             and idmwr.ui_filter_ind = 'N') src
   on (    tgt.group_id = src.group_id
       and tgt.shipment = src.shipment)
   when NOT MATCHED then
      insert (group_id,
              shipment,
              created_by,
              creation_date,
              last_updated_by,
              last_update_date,
              object_version_id)
      values (src.group_id,
              src.shipment,
              L_user,
              L_vdate,
              L_user,
              L_vdate,
              REIM_CONSTANTS.ONE);

   LOGGER.LOG_INFORMATION(L_program||' Create New Manual Group Receipts - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END MANAGE_MANUAL_GROUP;
------------------------------------------------------------------------------------------
FUNCTION POP_ORDLOC_UNIT_COST(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program           VARCHAR2(61) := 'REIM_ONLINE_MATCH_SQL.POP_ORDLOC_UNIT_COST';
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;
   L_order_sup_items   OBJ_NUM_NUM_STR_TBL;

BEGIN

   select OBJ_NUM_NUM_STR_REC(inner.order_no,
                              inner.po_supplier_site_id,
                              inner.item)
     bulk collect into L_order_sup_items
     from (select distinct i.order_no,
                           i.po_supplier_site_id,
                           d.item
             from im_match_invc_ws i,
                  im_match_invc_detl_ws d
            where i.workspace_id        = I_workspace_id
              and i.workspace_id        = d.workspace_id
              and i.doc_id              = d.doc_id) inner;

   LOGGER.LOG_INFORMATION(L_program||' Get order supplier items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_XFORM_ORDLOC_SQL.TRANSFORM_ORDLOC_GTT(O_error_message,
                                                 L_order_sup_items) = REIM_CONSTANTS.FAIL then
       return REIM_CONSTANTS.FAIL;
   end if;

   merge into im_match_invc_detl_ws tgt
   using (with ws as (select i.workspace_id,
                             i.doc_id,
                             i.location,
                             d.item,
                             s.match_key,
                             oh.order_no,
                             oh.import_id
                        from im_match_invc_ws i,
                             im_match_invc_detl_ws d,
                             v_im_supp_site_attrib_expl s,
                             ordhead oh
                       where i.workspace_id        = I_workspace_id
                         and i.workspace_id        = d.workspace_id
                         and i.doc_id              = d.doc_id
                         and i.po_supplier_site_id = s.supplier
                         and i.order_no            = oh.order_no)
          --SHIPMENT EXISTS
          select distinct ws.workspace_id,
                          ws.doc_id,
                          ws.item,
                          min(ol.transform_unit_cost)  over (partition by ol.order_no, ol.item, ol.loc) ordloc_unit_cost,
                          1 cost_count
            from ws,
                 shipment sh,
                 (select store loc, store phy_loc from store
                  union all
                  select wh loc, physical_wh phy_loc from wh) loc,
                 im_transform_ordloc_gtt ol
           where ws.match_key != REIM_CONSTANTS.MATCH_KEY_PO
             and ws.import_id  is null
             and ws.order_no   = sh.order_no
             and ws.location   = sh.bill_to_loc
             and ws.location   = loc.phy_loc
             and loc.loc       = ol.loc
             and ws.order_no   = ol.order_no
             and ws.item       = ol.item
          union all
          --NO SHIPMENT
          select distinct ws.workspace_id,
                          ws.doc_id,
                          ws.item,
                          min(ol.transform_unit_cost)  over (partition by ol.order_no, ol.item, ol.loc) ordloc_unit_cost,
                          1 cost_count
            from ws,
                 im_transform_ordloc_gtt ol
           where ws.match_key != REIM_CONSTANTS.MATCH_KEY_PO
             and ws.import_id  is null
             and ws.order_no   = ol.order_no
             and ws.item       = ol.item
             and not exists (select 'x' from shipment where order_no = ws.order_no)
          union all
          --IMPORT
          select distinct ws.workspace_id,
                          ws.doc_id,
                          ws.item,
                          min(ol.transform_unit_cost)  over (partition by ol.order_no, ol.item, ol.loc) ordloc_unit_cost,
                          1 cost_count
            from ws,
                 im_transform_ordloc_gtt ol
           where ws.match_key != REIM_CONSTANTS.MATCH_KEY_PO
             and ws.import_id  is not null
             and ws.order_no   = ol.order_no
             and ws.item       = ol.item
          union all
          --PO Match Key
          select distinct ws.workspace_id,
                          ws.doc_id,
                          ws.item,
                          min(ol.transform_unit_cost)  over (partition by ws.doc_id, ol.order_no, ol.item) ordloc_unit_cost,
                          count(distinct ol.transform_unit_cost) over (partition by ws.doc_id, ol.order_no, ol.item) cost_count
            from ws,
                 im_transform_ordloc_gtt ol
           where ws.match_key  = REIM_CONSTANTS.MATCH_KEY_PO
             and ws.order_no   = ol.order_no
             and ws.item       = ol.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item
       and src.cost_count   = 1)
   when MATCHED then update
         set tgt.ordloc_unit_cost = src.ordloc_unit_cost;

   LOGGER.LOG_INFORMATION(L_program||' Merge im_match_invc_detl_ws PO ordloc_unit_cost - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END POP_ORDLOC_UNIT_COST;
----------------------------------------------------------------------------------------------
FUNCTION ROLLUP_STYLE_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                      I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.ROLLUP_STYLE_DETAIL_MATCH_WS';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||'Start - I_workspace_id:' || I_workspace_id);

   ------------------------------------------------
   --merge sku to style - Matching columns
   ------------------------------------------------
   merge into im_detail_match_ws target
   using (select d.workspace_id,
                 d.ancestor_id,
                 max(d.match_status) match_status,
                 min(d.matched_filter) matched_filter
            from im_detail_match_ws d
           where d.workspace_id = I_workspace_id
             and d.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and d.ancestor_id  is not null
           GROUP BY d.workspace_id,
                    d.ancestor_id
   ) use_this
   on (    target.workspace_id       = use_this.workspace_id
       and target.detail_match_ws_id = use_this.ancestor_id
       and target.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set target.choice_flag    = 'N',
             target.match_status   = use_this.match_status,
             target.matched_filter = use_this.matched_filter;

   LOGGER.LOG_INFORMATION(L_program||' rollup Matching Columns from SKU to STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --merge sku to style - Invoice values
   ------------------------------------------------
   merge into im_detail_match_ws target
   using (select d.workspace_id,
                 d.ancestor_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invoice_id, -1))),
                        1, max(decode(d.choice_flag,'Y',d.invoice_id))
                        ,NULL) invoice_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invoice_ext_doc_id, '-NulL'))),
                        1, max(decode(d.choice_flag,'Y',d.invoice_ext_doc_id))
                        ,NULL) invoice_ext_doc_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invoice_unit_cost, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.invoice_unit_cost))
                        ,NULL) invoice_unit_cost,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_ordloc_unit_cost, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_ordloc_unit_cost))
                        ,NULL) invc_ordloc_unit_cost,
                 sum(decode(d.choice_flag,'Y',d.invoice_qty)) invoice_qty,
                 sum(decode(d.choice_flag,'Y',d.invoice_ext_cost)) invoice_ext_cost,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_supplier_group_id, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_supplier_group_id))
                        ,NULL) invc_supplier_group_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_supplier, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_supplier))
                        ,NULL) invc_supplier,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_supplier_name, '-NulL'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_supplier_name))
                        ,NULL) invc_supplier_name,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_supplier_phone, '-NulL'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_supplier_phone))
                        ,NULL) invc_supplier_phone,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_po_supplier, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_po_supplier))
                        ,NULL) invc_po_supplier,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_po_supplier_site_id, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_po_supplier_site_id))
                        ,NULL) invc_po_supplier_site_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_supplier_site_id, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_supplier_site_id))
                        ,NULL) invc_supplier_site_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.invc_supplier_site_name, '-NulL'))),
                        1, max(decode(d.choice_flag,'Y',d.invc_supplier_site_name))
                        ,NULL) invc_supplier_site_name,
                 max(decode(d.choice_flag,'Y',d.cost_matched)) cost_matched,
                 max(decode(d.choice_flag,'Y',d.qty_matched)) qty_matched,
                 min(tolerance_id) tolerance_id,
                 min(tolerance_exchange_rate) tolerance_exchange_rate
            from im_detail_match_ws d
           where d.workspace_id           = I_workspace_id
             and d.entity_type            = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and d.ancestor_id            is not null
             and d.selected_invoice_count > 0
           GROUP BY d.workspace_id,
                    d.ancestor_id
   ) use_this
   on (    target.workspace_id       = use_this.workspace_id
       and target.detail_match_ws_id = use_this.ancestor_id
       and target.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set target.invoice_id               = use_this.invoice_id,
             target.invoice_ext_doc_id       = use_this.invoice_ext_doc_id,
             target.invoice_unit_cost        = use_this.invoice_unit_cost,
             target.invc_ordloc_unit_cost    = use_this.invc_ordloc_unit_cost,
             target.invoice_qty              = use_this.invoice_qty,
             target.invoice_ext_cost         = use_this.invoice_ext_cost,
             target.invc_supplier_group_id   = use_this.invc_supplier_group_id,
             target.invc_supplier            = use_this.invc_supplier,
             target.invc_supplier_name       = use_this.invc_supplier_name,
             target.invc_supplier_phone      = use_this.invc_supplier_phone,
             target.invc_po_supplier         = use_this.invc_po_supplier,
             target.invc_po_supplier_site_id = use_this.invc_po_supplier_site_id,
             target.invc_supplier_site_id    = use_this.invc_supplier_site_id,
             target.invc_supplier_site_name  = use_this.invc_supplier_site_name,
             target.cost_matched             = use_this.cost_matched,
             target.qty_matched              = use_this.qty_matched,
             target.tolerance_id             = use_this.tolerance_id,
             target.tolerance_exchange_rate  = use_this.tolerance_exchange_rate;

   LOGGER.LOG_INFORMATION(L_program||' rollup SKU to STYLE (Invoice) - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --merge sku to style - Receipt values
   ------------------------------------------------
   merge into im_detail_match_ws target
   using (select d.workspace_id,
                 d.ancestor_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.receipt_id, -1))),
                        1, max(decode(d.choice_flag,'Y',d.receipt_id))
                        ,NULL) receipt_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.receipt_unit_cost, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.receipt_unit_cost))
                        ,NULL) receipt_unit_cost,
                 sum(decode(d.choice_flag,'Y',d.receipt_avail_qty)) receipt_avail_qty,
                 sum(decode(d.choice_flag,'Y',d.receipt_ext_cost)) receipt_ext_cost,
                 sum(decode(d.choice_flag,'Y',d.receipt_received_qty)) receipt_received_qty,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.rcpt_supplier, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.rcpt_supplier))
                        ,NULL) rcpt_supplier,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.rcpt_supplier_name, '-NulL'))),
                        1, max(decode(d.choice_flag,'Y',d.rcpt_supplier_name))
                        ,NULL) rcpt_supplier_name,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.rcpt_supplier_site_id, '-1'))),
                        1, max(decode(d.choice_flag,'Y',d.rcpt_supplier_site_id))
                        ,NULL) rcpt_supplier_site_id,
                 decode(count(distinct decode(d.choice_flag,'Y',NVL(d.rcpt_supplier_site_name, '-NulL'))),
                        1, max(decode(d.choice_flag,'Y',d.rcpt_supplier_site_name))
                        ,NULL) rcpt_supplier_site_name
            from im_detail_match_ws d
           where d.workspace_id           = I_workspace_id
             and d.entity_type            = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and d.ancestor_id            is not null
             and d.selected_receipt_count > 0
           GROUP BY d.workspace_id,
                    d.ancestor_id
   ) use_this
   on (    target.workspace_id       = use_this.workspace_id
       and target.detail_match_ws_id = use_this.ancestor_id
       and target.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set target.receipt_id              = use_this.receipt_id,
             target.receipt_unit_cost       = use_this.receipt_unit_cost,
             target.receipt_avail_qty       = use_this.receipt_avail_qty,
             target.receipt_ext_cost        = use_this.receipt_ext_cost,
             target.receipt_received_qty    = use_this.receipt_received_qty,
             target.rcpt_supplier           = use_this.rcpt_supplier,
             target.rcpt_supplier_name      = use_this.rcpt_supplier_name,
             target.rcpt_supplier_site_id   = use_this.rcpt_supplier_site_id,
             target.rcpt_supplier_site_name = use_this.rcpt_supplier_site_name;

   LOGGER.LOG_INFORMATION(L_program||' rollup SKU to STYLE (Receipt) - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --apply invoice and receipt counts
   ------------------------------------------------

   merge into im_detail_match_ws tgt
   using (with sku as (select s.detail_match_ws_id
                         from im_detail_match_ws s
                        where s.workspace_id   = I_workspace_id
                          and s.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                          and s.choice_flag    = 'Y'
                          and s.item_parent    is not null
                        GROUP BY s.detail_match_ws_id)
          select I_workspace_id workspace_id,
                 invc.item_parent,
                 invc.selected_invoice_count
            from (select i.item_parent,
                         count(distinct invoice_id) selected_invoice_count
                    from sku s,
                         im_detail_match_ws i
                   where i.workspace_id = I_workspace_id
                     and i.match_status = 'U' --constant
                     and i.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                     and i.choice_flag  = 'Y'
                     and i.ancestor_id    = s.detail_match_ws_id
                   group by i.item_parent) invc) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.item         = src.item_parent
       and tgt.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set tgt.selected_invoice_count = src.selected_invoice_count;

   LOGGER.LOG_INFORMATION(L_program||' Merge Selected Invoice counts to Style - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);


   merge into im_detail_match_ws tgt
   using (with sku as (select s.detail_match_ws_id
                         from im_detail_match_ws s
                        where s.workspace_id   = I_workspace_id
                          and s.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                          and s.choice_flag    = 'Y'
                          and s.item_parent    is not null
                        GROUP BY s.detail_match_ws_id)
          select I_workspace_id workspace_id,
                 rcpt.item_parent,
                 --
                 rcpt.selected_receipt_count
            from (select r.item_parent,
                         count(distinct receipt_id) selected_receipt_count
                    from sku s,
                         im_detail_match_ws r
                   where r.workspace_id = I_workspace_id
                     and r.match_status = 'U'      --constant
                     and r.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                     and r.choice_flag  = 'Y'
                     and r.ancestor_id  = s.detail_match_ws_id
                   group by r.item_parent) rcpt) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.item         = src.item_parent
       and tgt.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set tgt.selected_receipt_count = src.selected_receipt_count;

   LOGGER.LOG_INFORMATION(L_program||' Merge Selected Receipt counts to Style - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --apply style match info
   ------------------------------------------------

   merge into im_detail_match_ws target
   using (with sku as (select s.detail_match_ws_id
                         from im_detail_match_ws s
                        where s.workspace_id   = I_workspace_id
                          and s.entity_type    = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
                          and s.choice_flag    = 'Y'
                          and s.item_parent    is not null
                        GROUP BY s.detail_match_ws_id)
                 select I_workspace_id workspace_id,
                 invc.item_parent,
                 invc.invoice_unit_cost,
                 invc.invc_group_qty,
                 invc.invc_cost_cnt,
                 --
                 rcpt.receipt_unit_cost,
                 rcpt.rcpt_group_qty,
                 rcpt.rcpt_cost_cnt,
                 --
                 invc.tolerance_id,
                 invc.tolerance_exchange_rate,
                 tdc.tolerance_detail_id cost_tolerance_dtl_id,
                 tdq.tolerance_detail_id qty_tolerance_dtl_id,
                 --
                 NVL(rcpt.receipt_unit_cost, 0) - invc.invoice_unit_cost par_unit_cost_variance,
                 case when NVL(rcpt.receipt_unit_cost, 0) = 0 then
                      NULL
                      else
                      100 * ((rcpt.receipt_unit_cost - invc.invoice_unit_cost) / rcpt.receipt_unit_cost)
                 end par_unit_cost_variance_pct,
                 NVL(rcpt.rcpt_group_qty, 0) - invc.invc_group_qty par_qty_variance,
                 case when NVL(rcpt.rcpt_group_qty, 0) = 0 then
                      NULL
                      else
                      100 * ((rcpt.rcpt_group_qty - invc.invc_group_qty) / rcpt.rcpt_group_qty)
                 end par_qty_variance_pct,
                 --
                 case when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(rcpt.receipt_unit_cost, 0) = 0 then
                         'N'
                              when tdc.tolerance_value < ABS(100 * (invc.invoice_unit_cost - rcpt.receipt_unit_cost) / (rcpt.receipt_unit_cost)) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdc.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdc.tolerance_value < ABS((invc.invoice_unit_cost - NVL(rcpt.receipt_unit_cost,0)) * invc.tolerance_exchange_rate) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when invc.invoice_unit_cost = NVL(rcpt.receipt_unit_cost, 0) then
                                 'Y'
                              else
                                 'N'
                         end
                 end par_cost_in_tolerance,
                 --
                 case when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_PERCENT then
                         case when NVL(rcpt.rcpt_group_qty, 0) = 0 then
                         'N'
                              when tdq.tolerance_value < ABS(100 * (invc.invc_group_qty - rcpt.rcpt_group_qty) / rcpt.rcpt_group_qty) then
                                 'N'
                              else
                                 'Y'
                         end
                      when tdq.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                         case when tdq.tolerance_value < ABS(invc.invc_group_qty - NVL(rcpt.rcpt_group_qty, 0)) then
                                 'N'
                              else
                                 'Y'
                         end
                      else  --no tolerance found, exact match
                         case when invc.invc_group_qty = NVL(rcpt.rcpt_group_qty, 0) then
                                 'Y'
                              else
                                 'N'
                         end
                 end par_qty_in_tolerance
                 --
            from (select i.item_parent,
                         i.tolerance_id,
                         i.tolerance_exchange_rate,
                         max(i.invoice_unit_cost) invoice_unit_cost,
                         sum(i.invoice_qty) invc_group_qty,
                         count(distinct i.invoice_unit_cost) invc_cost_cnt
                    from sku s,
                         im_detail_match_ws i
                   where i.workspace_id = I_workspace_id
                     and i.match_status = 'U' --constant
                     and i.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                     and i.choice_flag  = 'Y'
                     and i.ancestor_id    = s.detail_match_ws_id
                   group by i.item_parent,
                            i.tolerance_id,
                            i.tolerance_exchange_rate) invc,
                 (select r.item_parent,
                         max(r.receipt_unit_cost) receipt_unit_cost,
                         sum(r.receipt_avail_qty) rcpt_group_qty,
                         count(distinct r.receipt_unit_cost) rcpt_cost_cnt
                    from sku s,
                         im_detail_match_ws r
                   where r.workspace_id = I_workspace_id
                     and r.match_status = 'U'      --constant
                     and r.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                     and r.choice_flag  = 'Y'
                     and r.ancestor_id  = s.detail_match_ws_id
                   group by r.item_parent) rcpt,
                 --
                 im_tolerance_detail tdc,
                 im_tolerance_detail tdq
           where invc.item_parent   = rcpt.item_parent
             and invc.invc_cost_cnt = 1
             and rcpt.rcpt_cost_cnt = 1
             --
             and invc.tolerance_id  = tdc.tolerance_id(+)
             and tdc.match_level(+) = REIM_CONSTANTS.TLR_MTCH_LVL_PARENT
             and tdc.match_type(+)  = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
             and tdc.lower_limit(+) <= invc.invoice_unit_cost
             and tdc.upper_limit(+) > invc.invoice_unit_cost
             and tdc.favor_of(+)    = case when invc.invoice_unit_cost > NVL(rcpt.receipt_unit_cost, 0)
                                           then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                           else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                      end
             --
             and invc.tolerance_id  = tdq.tolerance_id(+)
             and tdq.match_level(+) = REIM_CONSTANTS.TLR_MTCH_LVL_PARENT
             and tdq.match_type(+)  = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
             and tdq.lower_limit(+) <= invc.invc_group_qty
             and tdq.upper_limit(+) > invc.invc_group_qty
             and tdq.favor_of(+)    = case when invc.invc_group_qty > NVL(rcpt.rcpt_group_qty, 0)
                                           then REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
                                           else REIM_CONSTANTS.TLR_FAVOR_OF_RETL
                                      end
   ) use_this
   on (    target.workspace_id = use_this.workspace_id
       and target.item         = use_this.item_parent
       and target.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set target.in_tolerance           = case
                                                when use_this.par_cost_in_tolerance = 'Y' and use_this.par_qty_in_tolerance  = 'Y' then
                                                   'Y'
                                                else
                                                   'N'
                                             end,
             target.unit_cost_variance     = use_this.par_unit_cost_variance,
             target.unit_cost_variance_pct = use_this.par_unit_cost_variance_pct,
             target.cost_tolerance_dtl_id  = use_this.cost_tolerance_dtl_id,
             target.cost_in_tolerance      = use_this.par_cost_in_tolerance,
             target.qty_variance           = use_this.par_qty_variance,
             target.qty_variance_pct       = use_this.par_qty_variance_pct,
             target.qty_tolerance_dtl_id   = use_this.qty_tolerance_dtl_id,
             target.qty_in_tolerance       = use_this.par_qty_in_tolerance;

   LOGGER.LOG_INFORMATION(L_program||' rollup match info to Style - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   ------------------------------------------------
   --Update VPN on STYLE
   ------------------------------------------------

   merge into im_detail_match_ws target
   using (select distinct sku_ws.ancestor_id,
                 its.vpn
            from (select inner.ancestor_id,
                         DECODE(count(distinct inner.supplier),
                                1, min(inner.supplier),
                                NULL) supplier
                    from (select id.ancestor_id,
                                 DECODE(count(distinct id.invc_supplier_site_id),
                                        1, min(id.invc_supplier_site_id),
                                        NULL) supplier
                            from im_detail_match_ws id
                           where id.workspace_id = I_workspace_id
                             and id.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                           group by id.ancestor_id
                           union all
                           select rd.ancestor_id,
                                  DECODE(count(distinct rd.rcpt_supplier_site_id),
                                         1, min(rd.rcpt_supplier_site_id),
                                         NULL) supplier
                            from im_detail_match_ws rd
                           where rd.workspace_id = I_workspace_id
                             and rd.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_RCPT
                           group by rd.ancestor_id)inner
                   group by inner.ancestor_id) invc_rcpt_ws,
                  im_detail_match_ws sku_ws,
                  item_supplier its
           where sku_ws.workspace_id       = I_workspace_id
             and sku_ws.detail_match_ws_id = invc_rcpt_ws.ancestor_id
             and sku_ws.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
             and its.item                  = sku_ws.item_parent
             and its.supplier              = invc_rcpt_ws.supplier) use_this
   on (    target.workspace_id       = I_workspace_id
       and target.detail_match_ws_id = use_this.ancestor_id
       and target.entity_type        = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
   when MATCHED then
      update
         set target.vpn = use_this.vpn;

   LOGGER.LOG_INFORMATION(L_program||' rollup VPN to STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;
END ROLLUP_STYLE_DETAIL_MATCH_WS;
----------------------------------------------------------------------------------------------
FUNCTION PRORATE_STYLE_LVL_QTY_VAR(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.PRORATE_STYLE_LVL_QTY_VAR';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   delete from gtt_10_num_10_str_10_date;
   insert into gtt_10_num_10_str_10_date (varchar2_1, --doc_id
                                          varchar2_2, --item
                                          number_1,   --cost_variance
                                          number_2)   --qty_variance
   select doc_id,
          item,
          cost_variance,
          qty_variance
     from (select di.invoice_id doc_id,
                  di.item,
                  di.item_parent,
                  gtt.number_2 cost_variance,
                  gtt.number_3 total_qty_variance,
                  SIGN(gtt.number_3) * CEIL(ABS(gtt.number_3) * RATIO_TO_REPORT(di.invoice_qty) OVER (PARTITION BY di.item_parent)) doc_qty_variance,
                  di.invoice_qty
             from gtt_6_num_6_str_6_date gtt,
                  im_detail_match_ws di,
                  im_detail_match_ws ds
            where di.workspace_id = I_workspace_id
              and di.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
              and di.choice_flag  = 'Y'
              and di.item_parent  = gtt.varchar2_1
              --
              and ds.workspace_id = I_workspace_id
              and ds.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU
              and ds.choice_flag  = 'Y'
              and ds.item_parent  = gtt.varchar2_1
              --
              and di.item         = ds.item)inner
    model
       partition by (item_parent)
       dimension by (ROW_NUMBER() over (PARTITION BY item_parent ORDER BY item_parent, invoice_qty desc, doc_id) rn)
       measures (doc_id,
                 item,
                 item_parent,
                 cost_variance,
                 total_qty_variance,
                 doc_qty_variance,
                 SUM(NVL(doc_qty_variance,0)) OVER (PARTITION BY item_parent
                                                        ORDER BY item_parent, invoice_qty desc, doc_id) run_total,
                 0 qty_variance)
       rules sequential order(qty_variance[rn] = case
                                                    when total_qty_variance[CV(rn)] = REIM_CONSTANTS.ZERO THEN
                                                       REIM_CONSTANTS.ZERO
                                                    when Abs(run_total[CV(rn)]) <= Abs(total_qty_variance[CV(rn)]) then
                                                       doc_qty_variance[CV(rn)]
                                                    when Abs(run_total[CV(rn) - 1]) < Abs(total_qty_variance[CV(rn)]) then
                                                       total_qty_variance[CV(rn)] - run_total[CV(rn) - 1]
                                                    else
                                                       REIM_CONSTANTS.ZERO
                                                 end);

   LOGGER.LOG_INFORMATION(L_program||' Insert Prorated Variance into gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END PRORATE_STYLE_LVL_QTY_VAR;
------------------------------------------------------------------------------------------
FUNCTION UPDATE_DEALS_EXIST(O_error_message    OUT VARCHAR2,
                            I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_ONLINE_MATCH_SQL.UPDATE_DEALS_EXIST';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   ---------------------------------------------------------------------
   -- Update Deals Exists on Invoice Detail Workspace
   ---------------------------------------------------------------------
   merge into im_match_invc_detl_ws tgt
   using (select imidw.workspace_id,
                 imidw.doc_id,
                 imidw.item,
                 DECODE(iida.item,
                        NULL, 'N',
                        'Y') deals_exist
            from im_match_invc_detl_ws imidw,
                 im_invoice_detail_allowance iida
           where imidw.workspace_id = I_workspace_id
             and iida.doc_id (+)    = imidw.doc_id
             and iida.item (+)      = imidw.item
            GROUP BY imidw.workspace_id,
                     imidw.doc_id,
                     imidw.item,
                     iida.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.doc_id       = src.doc_id
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.deals_exist = src.deals_exist;

   LOGGER.LOG_INFORMATION(L_program||' Merge Deals Indicator on Invoice Detail Rows - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   ---------------------------------------------------------------------
   -- Update Deals Exists on Receipt Detail Workspace
   ---------------------------------------------------------------------
   merge into im_match_rcpt_detl_ws tgt
   using (select imrdw.workspace_id,
                 imrdw.shipment,
                 imrdw.item,
                 DECODE(od.item,
                        NULL, 'N',
                        'Y') deals_exist
            from im_match_rcpt_ws imrw,
                 im_match_rcpt_detl_ws imrdw,
                 ordloc_discount od
           where imrw.workspace_id  = I_workspace_id
             and imrdw.workspace_id = I_workspace_id
             and imrdw.shipment     = imrw.shipment
             and od.order_no (+)    = imrw.order_no
             and od.item     (+)    = imrdw.item
             and od.location (+)    = imrw.bill_to_loc
           GROUP BY imrdw.workspace_id,
                    imrdw.shipment,
                    imrdw.item,
                    od.item) src
   on (    tgt.workspace_id = src.workspace_id
       and tgt.shipment     = src.shipment
       and tgt.item         = src.item)
   when MATCHED then
      update
         set tgt.deals_exist = src.deals_exist;

   LOGGER.LOG_INFORMATION(L_program||' Merge Deals Indicator on Receipt Detail Rows - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END UPDATE_DEALS_EXIST;
------------------------------------------------------------------------------------------
END REIM_ONLINE_MATCH_SQL;
/