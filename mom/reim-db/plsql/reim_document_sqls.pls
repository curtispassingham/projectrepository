CREATE OR REPLACE PACKAGE REIM_DOCUMENT_SQL AS
-------------------------------------------------------------------------------------------------------------

   -- VALIDATION RULES

   DUP_VEND_DOC_NUM            CONSTANT VARCHAR2(40)   := 'DUP_VEND_DOC_NUM';
   VALID_DUE_DATE              CONSTANT VARCHAR2(40)   := 'VALID_DUE_DATE';
   VALID_CURR_CODE             CONSTANT VARCHAR2(40)   := 'VALID_CURR_CODE';
   VALID_CROSS_REF_DOC         CONSTANT VARCHAR2(40)   := 'VALID_CROSS_REF_DOC';
   VALID_ORD_CURR              CONSTANT VARCHAR2(40)   := 'VALID_ORD_CURR';
   VALID_FR8_TYPE              CONSTANT VARCHAR2(40)   := 'VALID_FR8_TYPE';
   SUPP_OPTION_UNDEFINED       CONSTANT VARCHAR2(40)   := 'SUPP_OPTION_UNDEFINED';
   VALID_NMRCH                 CONSTANT VARCHAR2(40)   := 'VALID_NMRCH';
   VALID_ORDER                 CONSTANT VARCHAR2(40)   := 'VALID_ORDER';
   VALID_ORD_VEND              CONSTANT VARCHAR2(40)   := 'VALID_ORD_VEND';
   VALID_RTV_VEND              CONSTANT VARCHAR2(40)   := 'VALID_RTV_VEND';
   VALID_TERMS                 CONSTANT VARCHAR2(40)   := 'VALID_TERMS';
   VALID_DOC_DATE              CONSTANT VARCHAR2(40)   := 'VALID_DOC_DATE';
   VALID_STORE                 CONSTANT VARCHAR2(40)   := 'VALID_STORE';
   VALID_WH                    CONSTANT VARCHAR2(40)   := 'VALID_WH';
   VALID_SUPPLIER              CONSTANT VARCHAR2(40)   := 'VALID_SUPPLIER';
   VALID_PARTNER               CONSTANT VARCHAR2(40)   := 'VALID_PARTNER';
   DOCTYPE_COST_INC_TAX_SIGN   CONSTANT VARCHAR2(40)   := 'DOCTYPE_COST_INC_TAX_SIGN';
   TOTAL_COST_INCL_TAX         CONSTANT VARCHAR2(40)   := 'TOTAL_COST_INCL_TAX';
   VALID_SUPPLIER_SUPP_SITE    CONSTANT VARCHAR2(40)   := 'VALID_SUPPLIER_SUPP_SITE';

   VALID_ORD_LOC               CONSTANT VARCHAR2(40)   := 'VALID_ORD_LOC';

   TOTAL_HDR_QTY_REQ           CONSTANT VARCHAR2(40)   := 'TOTAL_HDR_QTY_REQ';
   HDR_DTL_QTY_MTCH            CONSTANT VARCHAR2(40)   := 'HDR_DTL_QTY_MTCH';

   TAX_NOT_ALLOWED             CONSTANT VARCHAR2(40)   := 'TAX_NOT_ALLOWED';
   TAX_COST_SAME_SIGN          CONSTANT VARCHAR2(40)   := 'TAX_COST_SAME_SIGN';
   TAX_REQUIRED                CONSTANT VARCHAR2(40)   := 'TAX_REQUIRED';
   ZERO_TAX_REQUIRED           CONSTANT VARCHAR2(40)   := 'ZERO_TAX_REQUIRED';
   ZERO_MRCHI_DTL_REQ          CONSTANT VARCHAR2(40)   := 'ZERO_MRCHI_DTL_REQ';
   DOC_DTL_NMRCH_REQ           CONSTANT VARCHAR2(40)   := 'DOC_DTL_NMRCH_REQ';
   TAX_AMT_NON_ZERO            CONSTANT VARCHAR2(40)   := 'TAX_AMT_NON_ZERO';

   CRDNT_NO_DETAIL             CONSTANT VARCHAR2(40)   := 'CRDNT_NO_DETAIL';

   ALLW_TOTAL                  CONSTANT VARCHAR2(40)   := 'ALLW_TOTAL';

   RTV_REASON_CODE             CONSTANT VARCHAR2(40)   := 'RTV_REASON_CODE';
   CMC_REASON_CODE             CONSTANT VARCHAR2(40)   := 'CMC_REASON_CODE';

   DUP_ITEM                    CONSTANT VARCHAR2(40)   := 'DUP_ITEM';
   DUP_VPN                     CONSTANT VARCHAR2(40)   := 'DUP_VPN';
   DUP_UPC                     CONSTANT VARCHAR2(40)   := 'DUP_UPC';

   VALID_ITEM                  CONSTANT VARCHAR2(40)   := 'VALID_ITEM';
   VALID_ITEM_SUPPLIER         CONSTANT VARCHAR2(40)   := 'VALID_ITEM_SUPPLIER';
   VALID_ORDER_ITEM            CONSTANT VARCHAR2(40)   := 'VALID_ORDER_ITEM';
   VALID_RTV_ORDER_ITEM        CONSTANT VARCHAR2(40)   := 'VALID_RTV_ORDER_ITEM';
   VALID_REASON_CODE_ID        CONSTANT VARCHAR2(40)   := 'VALID_REASON_CODE_ID';

   VALID_VPN_SUPPLIER          CONSTANT VARCHAR2(40)   := 'VALID_VPN_SUPPLIER';
   DUP_VPN_SUPPLIER            CONSTANT VARCHAR2(40)   := 'DUP_VPN_SUPPLIER';
   VALID_UPC                   CONSTANT VARCHAR2(40)   := 'VALID_UPC';
   VALID_UPC_SUPPLIER          CONSTANT VARCHAR2(40)   := 'VALID_UPC_SUPPLIER';
   UNABLE_TO_RETRIEVE_ITEM     CONSTANT VARCHAR2(40)   := 'UNABLE_TO_RETRIEVE_ITEM';

   VALID_NMRCH_CODE            CONSTANT VARCHAR2(40)   := 'VALID_NMRCH_CODE';
   VALID_SRVC_PERF_STORE       CONSTANT VARCHAR2(40)   := 'VALID_SRVC_PERF_STORE';

   VALID_RTV                   CONSTANT VARCHAR2(15)   := 'VALID_RTV';
   VALID_RTV_LOC               CONSTANT VARCHAR2(15)   := 'VALID_RTV_LOC';

   DTL_TAX_NOT_IN_HDR          CONSTANT VARCHAR2(40)   := 'DTL_TAX_NOT_IN_HDR';
   DTL_NM_TAX_NOT_IN_HDR       CONSTANT VARCHAR2(40)   := 'DTL_NM_TAX_NOT_IN_HDR';

   VALID_HDR_TAX               CONSTANT VARCHAR2(40)   := 'VALID_HDR_TAX';
   VALID_DTL_TAX               CONSTANT VARCHAR2(40)   := 'VALID_DTL_TAX';
   VALID_DTL_NM_TAX            CONSTANT VARCHAR2(40)   := 'VALID_DTL_NM_TAX';

   VALID_ITEM_TAX              CONSTANT VARCHAR2(40)   := 'VALID_ITEM_TAX';

   REV_VAT_ITEM_ZERO_TAX       CONSTANT VARCHAR2(40)   := 'REV_VAT_ITEM_ZERO_TAX';

   --LOCATION ID THAT RMS WILL SEND FOR NO LOCATION ON NON-MERCH INVOICES
   NO_LOCATION                 CONSTANT  NUMBER(10)    := -999999999;

   NON_MRCH_COST               CONSTANT VARCHAR2(40)   := 'NON_MRCH_COST';
   DETAIL_COST                 CONSTANT VARCHAR2(40)   := 'DETAIL_COST';
   DETAIL_NM_COST              CONSTANT VARCHAR2(40)   := 'DETAIL_NM_COST';

   TOTAL_BASIS                 CONSTANT VARCHAR2(40)   := 'TOTAL_BASIS';
   NON_MRCH_BASIS              CONSTANT VARCHAR2(40)   := 'NON_MRCH_BASIS';
   DETAIL_BASIS                CONSTANT VARCHAR2(40)   := 'DETAIL_BASIS';
   DETAIL_NM_BASIS             CONSTANT VARCHAR2(40)   := 'DETAIL_BASIS';

   TOTAL_TAX                   CONSTANT VARCHAR2(40)   := 'TOTAL_TAX';
   NON_MRCH_TAX                CONSTANT VARCHAR2(40)   := 'NON_MRCH_TAX';
   DETAIL_TAX                  CONSTANT VARCHAR2(40)   := 'DETAIL_TAX';
   DETAIL_NM_TAX               CONSTANT VARCHAR2(40)   := 'DETAIL_TAX';
   ALLW_TAX                    CONSTANT VARCHAR2(40)   := 'ALLW_TAX';
   ACQ_TAX_DETAIL_REQ          CONSTANT VARCHAR2(40)   := 'ACQ_TAX_DETAIL_REQ';
   EXEMPT_DOCS_WITH_TAX        CONSTANT VARCHAR2(40)   := 'EXEMPT_DOCS_WITH_TAX';

   VALID_COMPLEX_DEAL_CURR_CODE CONSTANT VARCHAR2(40)  := 'VALID_COMPLEX_DEAL_CURR_CODE';
   VALID_LOCATION_FOR_DEAL      CONSTANT VARCHAR2(40)  := 'VALID_LOCATION_FOR_DEAL';
   VALID_MERCH_HIER             CONSTANT VARCHAR2(40)  := 'VALID_MERCH_HIER';
   VALID_FIXED_DEAL             CONSTANT VARCHAR2(40)  := 'VALID_FIXED_DEAL';
   VALID_COMPLEX_DEAL           CONSTANT VARCHAR2(40)  := 'VALID_COMPLEX_DEAL';
   VALID_FIXED_DEAL_LOC         CONSTANT VARCHAR2(40)  := 'VALID_FIXED_DEAL_LOC';
   VALID_COMPLEX_DEAL_DETAIL    CONSTANT VARCHAR2(40)  := 'VALID_COMPLEX_DEAL_DETAIL';

   --Induction Prevalidation
   PV_TEMPLATE_DATA             CONSTANT VARCHAR2(40)  := 'PV_TEMPLATE_DATA';

   --Transaction Types
   PO_TRAN_TYPE                CONSTANT VARCHAR2(1)    := 'P';
   SHIPMENT_TRAN_TYPE          CONSTANT VARCHAR2(1)    := 'S';

   MATCH_KEY_PO_LOC            CONSTANT VARCHAR2(3)    := 'POL';
   MATCH_KEY_PO                CONSTANT VARCHAR2(3)    := 'PO';
   MATCH_KEY_SUPP_SITE_LOC     CONSTANT VARCHAR2(3)    := 'SSL';
   MATCH_KEY_SUPP_SITE         CONSTANT VARCHAR2(3)    := 'SS';
   MATCH_KEY_SUPP_LOC          CONSTANT VARCHAR2(3)    := 'SL';
   MATCH_KEY_SUPPLIER          CONSTANT VARCHAR2(3)    := 'S';
   MATCH_KEY_SUPP_GROUP_LOC    CONSTANT VARCHAR2(3)    := 'SGL';
   MATCH_KEY_SUPP_GROUP        CONSTANT VARCHAR2(3)    := 'SG';

   ---
   DEFAULT_DECIMAL_DIGITS      CONSTANT NUMBER(10)     := 4;

-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_DOCUMENT
-- Purpose: This function performs validations on All Tabs and across Tabs of a document.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOCUMENT(O_error_message   IN OUT VARCHAR2,
                           I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                           I_trial_id        IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_WKSHT_DOC
-- Purpose: This function performs validations on the initial entry screen for document creation.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_WKSHT_DOC(O_error_message   IN OUT VARCHAR2,
                            I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                            I_trial_id        IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_DOC_HEADER
-- Purpose: This function performs validations on fields on Document Header Tab.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOC_HEADER(O_error_message   IN OUT VARCHAR2,
                             I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                             I_trial_id        IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_DOC_HEADER_TAX
-- Purpose: This function performs validations on fields on Document Header Tax Tab.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOC_HEADER_TAX(O_error_message   IN OUT VARCHAR2,
                                 I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                 I_trial_id        IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_DOC_DETAIL
-- Purpose: This function performs validations on fields on Document Details/Items Tax Tab.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOC_DETAIL(O_error_message   IN OUT VARCHAR2,
                             I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                             I_trial_id        IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_DOC_NON_MERCH
-- Purpose: This function performs validations on fields on Document Non-Merchandise Cost Tab.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOC_NON_MERCH(O_error_message    IN OUT VARCHAR2,
                                I_inject_doc_id    IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                I_trial_id         IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_DOC_COMMENTS
-- Purpose: This function performs validations on fields on Document Comments Tab.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOC_COMMENTS(O_error_message   IN OUT VARCHAR2,
                               I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                               I_trial_id        IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_DOC_FIXED_DEAL_DETAIL
-- Purpose: This function performs validations on fields on Document Deals Tab.(Deal type = Fixed)
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOC_FIXED_DEAL_DETAIL(O_error_message   IN OUT VARCHAR2,
                                        I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                        I_trial_id        IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_DOC_CMPLX_DEAL_DETAIL
-- Purpose: This function performs validations on fields on Document Deals Tab.(Deal type = Complex)
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOC_CMPLX_DEAL_DETAIL(O_error_message   IN OUT VARCHAR2,
                                        I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                        I_trial_id        IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    DEFAULT_DETL_FROM_PO
-- Purpose: This function defaults items from PO of the document.
--          It inserts rows into IM_INJECT_DOC_DETAIL and IM_INJECT_DOC_DETAIL_TAX.
-------------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_DETL_FROM_PO(O_error_message   IN OUT VARCHAR2,
                              I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                              I_location        IN     IM_INJECT_DOC_HEAD.LOCATION%TYPE,
                              I_doc_date        IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE,
                              I_inject_id       IN     IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                              I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    FETCH_RCPT_FOR_INVOICE
-- Purpose: This function inserts records into IM_INVC_DETL_RCPT_WS based on the match key.
-------------------------------------------------------------------------------------------------------------
FUNCTION FETCH_RCPT_FOR_INVOICE(O_error_message   IN OUT VARCHAR2,
                                I_inject_id       IN     IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                                I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    DEFAULT_DETL_FROM_RCPT
-- Purpose: This function defaults items from available Receipts of the PO of the document.
--          It inserts rows into IM_INJECT_DOC_DETAIL and IM_INJECT_DOC_DETAIL_TAX.
-------------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_DETL_FROM_RCPT(O_error_message   IN OUT VARCHAR2,
                                I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    DEFAULT_NON_MERCH_FROM_PO
-- Purpose: This function defaults Non-Merch details from PO of the document.
--          It inserts rows into IM_INJECT_DOC_NON_MERCH and IM_INJECT_DOC_NON_MERCH_TAX.
-------------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_NON_MERCH_FROM_PO(O_error_message   IN OUT VARCHAR2,
                                   I_inject_doc_id   IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    SAVE_DOCUMENT
-- Purpose: This function saves Document to Operational tables.
-------------------------------------------------------------------------------------------------------------
FUNCTION SAVE_DOCUMENT(O_error_message     IN OUT VARCHAR2,
                       I_inject_doc_id     IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                       I_add_non_merch_tax IN     VARCHAR2 DEFAULT 'N')
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    LOAD_DOCUMENT
-- Purpose: This function loads documents from Operational Tables to Injector tables for the user to edit Online.
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_DOCUMENT(O_error_message IN OUT VARCHAR2,
                       O_inject_id        OUT IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                       O_inject_doc_id    OUT IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                       I_doc_id        IN     IM_DOC_HEAD.DOC_ID%TYPE,
                       I_group_id      IN     IM_DOC_HEAD.GROUP_ID%TYPE DEFAULT NULL)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    FETCH_DOC_ITEM_TAX
-- Purpose: This function fetches taxes that can be assigned to an item during document entry.
-------------------------------------------------------------------------------------------------------------
FUNCTION FETCH_DOC_ITEM_TAX(O_error_message IN OUT VARCHAR2,
                            O_item_tax_tbl     OUT OBJ_ITEM_TAX_BREAK_TBL,
                            O_fix_tax_col      OUT VARCHAR2,
                            I_inject_id     IN     IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                            I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                            I_item          IN     IM_INJECT_DOC_DETAIL.ITEM%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    ROLLUP_DOC_DETAIL_TAX
-- Purpose: This function updates im_doc_tax with the rolled up values from document details.
-------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_DOC_DETAIL_TAX(O_error_message IN OUT VARCHAR2,
                               I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    DELETE_DOCS
-- Purpose: This function updates status of eligible documents to DELETE.
-------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DOCS(O_error_message     IN OUT VARCHAR2,
                     O_delete_comments      OUT OBJ_VARCHAR_ID_TABLE,
                     I_workspace_id      IN     IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    CREATE_CN_FROM_CNR
-- Purpose: This function creates credit notes from credit note requests.
-------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_CN_FROM_CNR(O_error_message IN OUT VARCHAR2,
                            O_cnr_comments     OUT OBJ_VARCHAR_ID_TABLE,
                            O_inject_id        OUT IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                            O_inject_doc_id    OUT IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                            I_workspace_id  IN     IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VOID_CNR
-- Purpose: This function updates status of eligible documents to VOID.
-------------------------------------------------------------------------------------------------------------
FUNCTION VOID_CNR(O_error_message  IN OUT VARCHAR2,
                  O_void_comments     OUT OBJ_VARCHAR_ID_TABLE,
                  I_workspace_id   IN     IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                  I_reason_code_id IN     IM_DOC_DETAIL_COMMENTS.REASON_CODE_ID%TYPE,
                  I_comment        IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    RELEASE_HOLD
-- Purpose: This function updates status of eligible documents to RELEASED.
-------------------------------------------------------------------------------------------------------------
FUNCTION RELEASE_HOLD(O_error_message     IN OUT VARCHAR2,
                      O_release_comments     OUT OBJ_VARCHAR_ID_TABLE,
                      I_workspace_id      IN     IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION REVERSE_DEBIT_MEMO(O_error_message IN OUT VARCHAR2,
                            O_inject_id        OUT IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                            O_inject_doc_id    OUT IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                            I_doc_id        IN     IM_DOC_HEAD.DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_CREDIT_MEMO(O_error_message IN OUT VARCHAR2,
                            O_comments         OUT OBJ_VARCHAR_ID_TABLE,
                            I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                            I_ext_doc_id    IN     IM_DOC_HEAD.EXT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    CHK_AND_UPD_REV_CHRG_VAT
-- Purpose: This function Checks if reverse vat threshold was met and updates taxes of items that are eligible for reverse charge vat.
-- Output:
   --O_flip_status:
        --NULL - No rows were updated
        --'R'  - Atleast one item was updated to Zero vat.
        --'S'  - Atleast one item was brought back to system vat.
FUNCTION CHK_AND_UPD_REV_CHRG_VAT(O_error_message IN OUT VARCHAR2,
                                  O_flip_status      OUT VARCHAR2,
                                  I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_GROUP_ENTRY_DOCS
-- Purpose: This function performs validations on Group Entry documents.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_ENTRY_DOCS(O_error_message IN OUT VARCHAR2,
                                   I_inject_id     IN     IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                                   I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                   I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    APPROVE_GROUP_ENTRY_DOCS
-- Purpose: This function performs validations on Group Entry documents and moves them to their entry status in IM_DOC_HEAD.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION APPROVE_GROUP_ENTRY_DOCS(O_error_message IN OUT VARCHAR2,
                                  O_error_exists     OUT VARCHAR2,
                                  I_inject_id     IN     IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                                  I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    SAVE_GROUP_ENTRY_DOC
-- Purpose: This function performs validations on Group Entry document and saves it to IM_DOC_HEAD in WKSHT status.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION SAVE_GROUP_ENTRY_DOC(O_error_message IN OUT VARCHAR2,
                              I_inject_id     IN     IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                              I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    LOAD_GROUP_ENTRY_DOCS
-- Purpose: This function Loads all documents that belong to the Group into Inject workspace for user action.
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_GROUP_ENTRY_DOCS(O_error_message IN OUT VARCHAR2,
                               O_inject_id        OUT IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                               I_group_id      IN     IM_DOC_HEAD.GROUP_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    DEFLT_GRP_ENTRY_DOC_FROM_PO
-- Purpose: This function defaults data from PO (Extent of it will be based on Performance findings).
-------------------------------------------------------------------------------------------------------------
FUNCTION DEFLT_GRP_ENTRY_DOC_FROM_PO(O_error_message IN OUT VARCHAR2,
                                     I_inject_id     IN     IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                                     I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    GET_TAX_STATUS
-- Purpose: This function checks if Tax is applicable.
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_STATUS(O_error_message    OUT VARCHAR2,
                        O_tax_ind          OUT VARCHAR2,
                        I_vendor        IN     IM_INJECT_DOC_HEAD.VENDOR%TYPE,
                        I_vendor_type   IN     IM_INJECT_DOC_HEAD.VENDOR_TYPE%TYPE,
                        I_location      IN     IM_INJECT_DOC_HEAD.LOCATION%TYPE,
                        I_loc_type      IN     IM_INJECT_DOC_HEAD.LOC_TYPE%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    FETCH_DOC_DUE_DATE
-- Purpose: This function fetches due date based on the input document date and terms.
-------------------------------------------------------------------------------------------------------------
FUNCTION FETCH_DOC_DUE_DATE(O_error_message    OUT VARCHAR2,
                            O_due_date         OUT IM_INJECT_DOC_HEAD.DUE_DATE%TYPE,
                            I_doc_date      IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE,
                            I_terms         IN     TERMS_HEAD.TERMS%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_INDUCTION_DOCS
-- Purpose: This function performs validations on documents uploaded through Induction.
--          It inserts rows into IM_INJECT_DOC_ERROR in case of any failures.
-------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_INDUCTION_DOCS(O_error_message IN OUT VARCHAR2,
                                 I_inject_id     IN     IM_INJECT_DOC_HEAD.INJECT_ID%TYPE,
                                 I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                 I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
END REIM_DOCUMENT_SQL;
/
