CREATE OR REPLACE PACKAGE BODY REIM_OI_SQL AS

----------------------------------------------------------------------------------------------
/**
 *  -------- PRIVATE PROCS AND FUNCTIONS SPECS --------------
 */
----------------------------------------------------------------------------------------------
/**
 * The private procedure used to
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */

----------------------------------------------------------------------------------------------

/**
 *  --------- PUBLIC PROCS AND FUNCTIONS BODIES --------------
 */
----------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SESSION_ID(O_error_message    OUT VARCHAR2,
                             O_session_id       OUT OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'REIM_OI_SQL.GET_NEXT_SESSION_ID';

BEGIN

   if OI_UTILITY.GET_NEXT_SESSION_ID(O_error_message,
                                     O_session_id) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END GET_NEXT_SESSION_ID;
----------------------------------------------------------------------------------------------
/**
 * The public function used to fetch Supplier Site Information.
 *
 * Input param: I_session_id       (OI Session Id of the User)
 *              I_supplier_site_id (The ID of the Supplier Site)
 *
 * Output param: O_result (An Object Type Containing Supplier Site Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_SUPPLIER_INFO(O_error_message       OUT VARCHAR2,
                             O_result           IN OUT REIM_OI_SUPP_SITE_INFO_REC,
                             I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                             I_supplier_site_id IN     SUPS.SUPPLIER%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.FETCH_SUPPLIER_INFO';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   cursor C_FETCH_SUPP_INFO is
      select REIM_OI_SUPP_SITE_INFO_REC(v_tl.sup_name,
                                        v_tl.contact_name,
                                        s.contact_phone,
                                        s.contact_email,
                                        a.add_1 || a.add_2 || a.add_3 || a.city || a.state || a.country_id || a.post)
        from sups s,
             v_sups_tl v_tl,
             addr a
       where s.supplier         = I_supplier_site_id
         and v_tl.supplier      = s.supplier
         and a.key_value_1      = s.supplier
         and a.addr_type        = REIM_CONSTANTS.ADDR_TYPE_REMITT
         and a.MODULE           = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
         and a.primary_addr_ind = 'Y';

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id
                                    || ' I_supplier_site_id: ' || I_supplier_site_id);

   open C_FETCH_SUPP_INFO;
   fetch C_FETCH_SUPP_INFO into O_result;
   close C_FETCH_SUPP_INFO;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END FETCH_SUPPLIER_INFO;
----------------------------------------------------------------------------------------------
/**
 * The public function used to fetch Supplier Site Information.
 *
 * Input param: I_session_id       (OI Session Id of the User)
 *              I_supplier_site_id (The ID of the Supplier Site)
 *
 * Output param: O_result (An Object Type Containing Supplier Site Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_QTY_COMP_INFO(O_error_message       OUT VARCHAR2,
                             O_result           IN OUT REIM_OI_QUANTITY_INFO_REC,
                             I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                             I_workspace_id     IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                             I_item_view_ws_id  IN     IM_DETAIL_MATCH_WS.DETAIL_MATCH_WS_ID%TYPE,
                             I_iitem_view_ws_id IN     IM_INVC_ITEM_VIEW_WS.INVC_ITEM_VIEW_WS_ID%TYPE,
                             I_disc_view_ws_id  IN     IM_DISCREPANCY_LIST_WS.DISCREPANCY_LIST_WS_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.FETCH_QTY_COMP_INFO';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_order_sup_items OBJ_NUM_NUM_STR_TBL := NULL;

   cursor C_FETCH_ORDER_SUP_ITEMS is
      select OBJ_NUM_NUM_STR_REC(inner.order_no,
                                 inner.po_supplier_site_id,
                                 inner.item)
        from (with skus as (select idmw_sku.detail_match_ws_id
                              from im_detail_match_ws idmw_sku
                             where idmw_sku.workspace_id = I_workspace_id
                               and (   idmw_sku.ancestor_id        = I_item_view_ws_id
                                    or idmw_sku.detail_match_ws_id = I_item_view_ws_id)
                               and idmw_sku.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU)
              select distinct imiw.order_no,
                              imiw.po_supplier_site_id,
                              idmwi.item
                from skus,
                     im_detail_match_ws idmwi,
                     im_match_invc_ws imiw
               where idmwi.workspace_id  = I_workspace_id
                 and idmwi.entity_type   = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                 and idmwi.ancestor_id   = skus.detail_match_ws_id
                 and idmwi.choice_flag   = 'Y'
                 and idmwi.ui_filter_ind = 'N'
                 and imiw.doc_id         = idmwi.invoice_id) inner;

   cursor C_FETCH_QTY_COMP_INFO_IVIEW is
      with ord_qty as (select sum(transform_qty_ordered) qty_ordered
                         from im_transform_ordloc_gtt)
      select REIM_OI_QUANTITY_INFO_REC(NVL(oq.qty_ordered,0),
                                       NVL(idmws.receipt_received_qty,0),
                                       idmws.invoice_qty,
                                       NVL(idmws.receipt_avail_qty,0))
        from im_detail_match_ws idmws,
             ord_qty oq
       where idmws.workspace_id       = I_workspace_id
         and idmws.detail_match_ws_id = I_item_view_ws_id;

   cursor C_FETCH_QTY_COMP_INFO_IIVIEW is
      select REIM_OI_QUANTITY_INFO_REC(NVL(iiview_ws.order_qty,0),
                                       NVL(iiview_ws.receipt_received_qty,0),
                                       iiview_ws.invoice_qty,
                                       NVL(iiview_ws.receipt_avail_qty,0))
        from im_invc_item_view_ws iiview_ws
       where iiview_ws.workspace_id         = I_workspace_id
         and iiview_ws.invc_item_view_ws_id = I_iitem_view_ws_id;

   cursor C_FETCH_QTY_COMP_INFO_DVIEW is
      select REIM_OI_QUANTITY_INFO_REC(NVL(disc_ws.order_qty,0),
                                       NVL(disc_ws.receipt_received_qty,0),
                                       disc_ws.invoice_qty,
                                       NVL(disc_ws.receipt_avail_qty,0))
        from im_discrepancy_list_ws disc_ws
       where disc_ws.workspace_id           = I_workspace_id
         and disc_ws.discrepancy_list_ws_id = I_disc_view_ws_id;



BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id
                                    || ' I_workspace: '  || I_workspace_id
                                    || ' I_item_view_ws_id: '  || I_item_view_ws_id
                                    || ' I_iitem_view_ws_id: ' || I_iitem_view_ws_id
                                    || ' I_disc_view_ws_id: '  || I_disc_view_ws_id);


   if I_item_view_ws_id is NOT NULL then

      open C_FETCH_ORDER_SUP_ITEMS;
      fetch C_FETCH_ORDER_SUP_ITEMS BULK COLLECT into L_order_sup_items;
      close C_FETCH_ORDER_SUP_ITEMS;

      if REIM_XFORM_ORDLOC_SQL.TRANSFORM_ORDLOC_GTT(O_error_message,
                                                    L_order_sup_items) = REIM_CONSTANTS.FAIL then
         return REIM_CONSTANTS.FAIL;
      end if;

      open C_FETCH_QTY_COMP_INFO_IVIEW;
      fetch C_FETCH_QTY_COMP_INFO_IVIEW into O_result;
      close C_FETCH_QTY_COMP_INFO_IVIEW;

   elsif I_iitem_view_ws_id is NOT NULL then

      open C_FETCH_QTY_COMP_INFO_IIVIEW;
      fetch C_FETCH_QTY_COMP_INFO_IIVIEW into O_result;
      close C_FETCH_QTY_COMP_INFO_IIVIEW;

   elsif I_disc_view_ws_id is NOT NULL then

      open C_FETCH_QTY_COMP_INFO_DVIEW;
      fetch C_FETCH_QTY_COMP_INFO_DVIEW into O_result;
      close C_FETCH_QTY_COMP_INFO_DVIEW;

   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END FETCH_QTY_COMP_INFO;
----------------------------------------------------------------------------------------------
/**
 * The public function used to fetch Tolerance Range Information for Summary Match Screen.
 *
 * Input param: I_session_id   (OI Session Id of the User)
 *              I_workspace_id (The ID of the User's Matching Workspace)
 *
 * Output param: O_result (An Object Type Containing Tolerance Range Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_SMRY_TOLERANCE_INFO(O_error_message    OUT VARCHAR2,
                                   O_result        IN OUT REIM_OI_TOLERANCE_INFO_REC,
                                   I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                   I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.FETCH_SMRY_TOLERANCE_INFO';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   cursor C_FETCH_TOLERANCE_INFO is
      with tol_info as (select I_workspace_id workspace_id,
                               MAX(tolerance_id) tolerance_id,
                               MAX(tolerance_level_type) tolerance_entity,
                               MAX(tolerance_exchange_rate) tolerance_exchange_rate,
                               SUM(total_avail_cost) invc_cost,
                               SUM(total_avail_qty) invc_qty
                          from im_match_invc_ws
                         where workspace_id = I_workspace_id
                           and choice_flag  = 'Y'),
           rcpt as (select I_workspace_id workspace_id,
                           NVL(SUM(total_avail_cost),0) rcpt_cost,
                           NVL(SUM(total_avail_qty),0) rcpt_qty
                      from im_match_rcpt_ws
                     where workspace_id = I_workspace_id
                       and choice_flag  = 'Y')
      select REIM_OI_TOLERANCE_INFO_REC(ti.tolerance_id,
                                        ti.tolerance_entity,
                                        case
                                           when r.rcpt_cost <= ti.invc_cost then
                                              tdc_lower.tolerance_value_type
                                           else
                                              tdc_upper.tolerance_value_type
                                        end,
                                        tdc_lower.tolerance_value,
                                        tdc_upper.tolerance_value,
                                        case
                                           when r.rcpt_cost <= ti.invc_cost then
                                              case
                                                 when tdc_lower.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    r.rcpt_cost - ti.invc_cost
                                                 else
                                                    DECODE(r.rcpt_cost, 0, 100, 100 * (r.rcpt_cost - ti.invc_cost)/r.rcpt_cost)
                                              end
                                           else
                                              case
                                                 when tdc_upper.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    r.rcpt_cost - ti.invc_cost
                                                 else
                                                    DECODE(r.rcpt_cost, 0, 100, 100 * (r.rcpt_cost - ti.invc_cost)/r.rcpt_cost)
                                              end
                                        end,
                                        case
                                           when r.rcpt_cost <= ti.invc_cost then
                                              case
                                                 when tdc_lower.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    case
                                                       when ti.invc_cost - r.rcpt_cost <= tdc_lower.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                                 else
                                                    case
                                                       when DECODE(r.rcpt_cost, 0, 100, 100 * (ti.invc_cost - r.rcpt_cost)/r.rcpt_cost) <= tdc_lower.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                              end
                                           else
                                              case
                                                 when tdc_upper.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    case
                                                       when r.rcpt_cost - ti.invc_cost <= tdc_upper.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                                 else
                                                    case
                                                       when DECODE(r.rcpt_cost, 0, 100, 100 * (r.rcpt_cost - ti.invc_cost)/r.rcpt_cost) <= tdc_upper.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                              end
                                        end,
                                        DECODE(tdc_lower.tolerance_value_type,
                                               tdc_upper.tolerance_value_type, 'N',
                                               'Y'),
                                        case
                                           when r.rcpt_qty <= ti.invc_qty then
                                              tdq_lower.tolerance_value_type
                                           else
                                              tdq_upper.tolerance_value_type
                                        end,
                                        tdq_lower.tolerance_value,
                                        tdq_upper.tolerance_value,
                                        case
                                           when r.rcpt_qty <= ti.invc_qty then
                                              case
                                                 when tdq_lower.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    r.rcpt_qty - ti.invc_qty
                                                 else
                                                    DECODE(r.rcpt_qty, 0, 100, 100 * (r.rcpt_qty - ti.invc_qty)/r.rcpt_qty)
                                              end
                                           else
                                              case
                                                 when tdq_upper.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    r.rcpt_qty - ti.invc_qty
                                                 else
                                                    DECODE(r.rcpt_qty, 0, 100, 100 * (r.rcpt_qty - ti.invc_qty)/r.rcpt_qty)
                                              end
                                        end,
                                        case
                                           when r.rcpt_qty <= ti.invc_qty then
                                              case
                                                 when tdq_lower.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    case
                                                       when ti.invc_qty - r.rcpt_qty <= tdq_lower.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                                 else
                                                    case
                                                       when DECODE(r.rcpt_qty, 0, 100, 100 * (ti.invc_qty - r.rcpt_qty)/r.rcpt_qty) <= tdq_lower.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                              end
                                           else
                                              case
                                                 when tdq_upper.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    case
                                                       when r.rcpt_qty - ti.invc_qty <= tdq_upper.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                                 else
                                                    case
                                                       when DECODE(r.rcpt_qty, 0, 100, 100 * (r.rcpt_qty - ti.invc_qty)/r.rcpt_qty) <= tdq_upper.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                              end
                                        end,
                                        DECODE(tdq_lower.tolerance_value_type,
                                               tdq_upper.tolerance_value_type, 'N',
                                               'Y'),
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL)
        from tol_info ti,
             rcpt r,
             im_tolerance_detail tdq_lower,
             im_tolerance_detail tdq_upper,
             im_tolerance_detail tdc_lower,
             im_tolerance_detail tdc_upper
       where r.workspace_id(+)           = ti.workspace_id
         and ti.tolerance_id             = tdq_lower.tolerance_id(+)
         and tdq_lower.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
         and tdq_lower.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
         and tdq_lower.lower_limit(+)   <= ti.invc_qty
         and tdq_lower.upper_limit(+)    > ti.invc_qty
         and tdq_lower.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
         --
         and ti.tolerance_id             = tdq_upper.tolerance_id(+)
         and tdq_upper.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
         and tdq_upper.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
         and tdq_upper.lower_limit(+)   <= ti.invc_qty
         and tdq_upper.upper_limit(+)    > ti.invc_qty
         and tdq_upper.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_RETL
         --
         and ti.tolerance_id             = tdc_lower.tolerance_id(+)
         and tdc_lower.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
         and tdc_lower.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
         and tdc_lower.lower_limit(+)   <= (ti.invc_cost * ti.tolerance_exchange_rate)
         and tdc_lower.upper_limit(+)    > (ti.invc_cost * ti.tolerance_exchange_rate)
         and tdc_lower.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
         --
         and ti.tolerance_id             = tdc_upper.tolerance_id(+)
         and tdc_upper.match_level(+)    = REIM_CONSTANTS.TLR_MTCH_LVL_SMRY
         and tdc_upper.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
         and tdc_upper.lower_limit(+)   <= (ti.invc_cost * ti.tolerance_exchange_rate)
         and tdc_upper.upper_limit(+)    > (ti.invc_cost * ti.tolerance_exchange_rate)
         and tdc_upper.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_RETL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id
                                    || ' I_workspace_id: ' || I_workspace_id);

   open C_FETCH_TOLERANCE_INFO;
   fetch C_FETCH_TOLERANCE_INFO into O_result;
   close C_FETCH_TOLERANCE_INFO;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END FETCH_SMRY_TOLERANCE_INFO;
----------------------------------------------------------------------------------------------
/**
 * The public function used to fetch Tolerance Range Information for Detail Match Screen.
 *
 * Input param: I_session_id       (OI Session Id of the User)
 *              I_workspace_id     (The ID of the User's Matching Workspace)
 *              I_item_view_ws_id  (The Detail match Workspace ID of the highlighted row on Detail match screen)
 *              I_iitem_view_ws_id (The Invoice Item View Workspace ID of the highlighted row on Invoice Item View screen)
 *
 * Output param: O_result (An Object Type Containing Tolerance Range Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_DETL_TOLERANCE_INFO(O_error_message       OUT VARCHAR2,
                                   O_result           IN OUT REIM_OI_TOLERANCE_INFO_REC,
                                   I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                   I_workspace_id     IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                                   I_item_view_ws_id  IN     IM_DETAIL_MATCH_WS.DETAIL_MATCH_WS_ID%TYPE,
                                   I_iitem_view_ws_id IN     IM_INVC_ITEM_VIEW_WS.INVC_ITEM_VIEW_WS_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.FETCH_DETL_TOLERANCE_INFO';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   cursor C_FETCH_TOLERANCE_INFO is
      with detl_ws as (select I_workspace_id workspace_id,
                              tolerance_id,
                              tolerance_exchange_rate,
                              invoice_unit_cost invc_cost,
                              invoice_qty invc_qty,
                              invc_ordloc_unit_cost po_cost,
                              NVL(receipt_avail_qty,0) rcpt_qty,
                              entity_type
                         from im_detail_match_ws
                        where workspace_id       = I_workspace_id
                          and detail_match_ws_id = I_item_view_ws_id
                          and entity_type        IN (REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU,
                                                     REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_STYLE)
                       union all
                       select I_workspace_id workspace_id,
                              detl_view.tolerance_id,
                              detl_view.tolerance_exchange_rate,
                              ii_view.invoice_unit_cost invc_cost,
                              ii_view.invoice_qty invc_qty,
                              ii_view.order_unit_cost po_cost,
                              NVL(ii_view.receipt_avail_qty,0) rcpt_qty,
                              REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU entity_type
                         from im_invc_item_view_ws ii_view,
                              im_detail_match_ws detl_view
                        where ii_view.workspace_id         = I_workspace_id
                          and ii_view.invc_item_view_ws_id = I_iitem_view_ws_id
                          and detl_view.detail_match_ws_id = ii_view.sku_detail_match_ws_id),
           tol_entity as (select I_workspace_id workspace_id,
                                 max(tolerance_level_type) tolerance_entity
                            from im_match_invc_ws
                           where workspace_id = I_workspace_id)
      select REIM_OI_TOLERANCE_INFO_REC(dw.tolerance_id,
                                        te.tolerance_entity,
                                        case
                                           when dw.po_cost <= dw.invc_cost then
                                              tdc_lower.tolerance_value_type
                                           else
                                              tdc_upper.tolerance_value_type
                                        end,
                                        tdc_lower.tolerance_value,
                                        tdc_upper.tolerance_value,
                                        case
                                           when dw.po_cost <= dw.invc_cost then
                                              case
                                                 when tdc_lower.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    dw.po_cost - dw.invc_cost
                                                 else
                                                    DECODE(dw.po_cost, 0, 100, 100 * (dw.po_cost - dw.invc_cost)/dw.po_cost)
                                              end
                                           else
                                              case
                                                 when tdc_upper.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    dw.po_cost - dw.invc_cost
                                                 else
                                                    DECODE(dw.po_cost, 0, 100, 100 * (dw.po_cost - dw.invc_cost)/dw.po_cost)
                                              end
                                        end,
                                        case
                                           when dw.po_cost <= dw.invc_cost then
                                              case
                                                 when tdc_lower.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    case
                                                       when dw.invc_cost - dw.po_cost <= tdc_lower.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                                 else
                                                    case
                                                       when DECODE(dw.po_cost, 0, 100, 100 * (dw.invc_cost - dw.po_cost)/dw.po_cost) <= tdc_lower.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                              end
                                           else
                                              case
                                                 when tdc_upper.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    case
                                                       when dw.po_cost - dw.invc_cost <= tdc_upper.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                                 else
                                                    case
                                                       when DECODE(dw.po_cost, 0, 100, 100 * (dw.po_cost - dw.invc_cost)/dw.po_cost) <= tdc_upper.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                              end
                                        end,
                                        DECODE(tdc_lower.tolerance_value_type,
                                               tdc_upper.tolerance_value_type, 'N',
                                               'Y'),
                                        case
                                           when dw.rcpt_qty <= dw.invc_qty then
                                              tdq_lower.tolerance_value_type
                                           else
                                              tdq_upper.tolerance_value_type
                                        end,
                                        tdq_lower.tolerance_value,
                                        tdq_upper.tolerance_value,
                                        case
                                           when dw.rcpt_qty <= dw.invc_qty then
                                              case
                                                 when tdq_lower.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    dw.rcpt_qty - dw.invc_qty
                                                 else
                                                    DECODE(dw.rcpt_qty, 0, 100, 100 * (dw.rcpt_qty - dw.invc_qty)/dw.rcpt_qty)
                                              end
                                           else
                                              case
                                                 when tdq_upper.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    dw.rcpt_qty - dw.invc_qty
                                                 else
                                                    DECODE(dw.rcpt_qty, 0, 100, 100 * (dw.rcpt_qty - dw.invc_qty)/dw.rcpt_qty)
                                              end
                                        end,
                                        case
                                           when dw.rcpt_qty <= dw.invc_qty then
                                              case
                                                 when tdq_lower.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    case
                                                       when dw.invc_qty - dw.rcpt_qty <= tdq_lower.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                                 else
                                                    case
                                                       when DECODE(dw.rcpt_qty, 0, 100, 100 * (dw.invc_qty - dw.rcpt_qty)/dw.rcpt_qty) <= tdq_lower.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                              end
                                           else
                                              case
                                                 when tdq_upper.tolerance_value_type = REIM_CONSTANTS.TLR_VAL_TYPE_ACTUAL then
                                                    case
                                                       when dw.rcpt_qty - dw.invc_qty <= tdq_upper.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                                 else
                                                    case
                                                       when DECODE(dw.rcpt_qty, 0, 100, 100 * (dw.rcpt_qty - dw.invc_qty)/dw.rcpt_qty) <= tdq_upper.tolerance_value then
                                                          'Y'
                                                       else
                                                          'N'
                                                    end
                                              end
                                        end,
                                        DECODE(tdq_lower.tolerance_value_type,
                                               tdq_upper.tolerance_value_type, 'N',
                                               'Y'),
                                        tdc_lower.auto_res_value,
                                        tdc_upper.auto_res_value,
                                        tdq_lower.auto_res_value,
                                        tdq_upper.auto_res_value)
        from detl_ws dw,
             tol_entity te,
             im_tolerance_detail tdq_lower,
             im_tolerance_detail tdq_upper,
             im_tolerance_detail tdc_lower,
             im_tolerance_detail tdc_upper
       where te.workspace_id             = dw.workspace_id
         and dw.tolerance_id             = tdq_lower.tolerance_id(+)
         and tdq_lower.match_level(+)    = DECODE(dw.entity_type,
                                                  REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU, REIM_CONSTANTS.TLR_MTCH_LVL_LINE,
                                                  REIM_CONSTANTS.TLR_MTCH_LVL_PARENT)
         and tdq_lower.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
         and tdq_lower.lower_limit(+)   <= dw.invc_qty
         and tdq_lower.upper_limit(+)    > dw.invc_qty
         and tdq_lower.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
         --
         and dw.tolerance_id             = tdq_upper.tolerance_id(+)
         and tdq_upper.match_level(+)    = DECODE(dw.entity_type,
                                                  REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU, REIM_CONSTANTS.TLR_MTCH_LVL_LINE,
                                                  REIM_CONSTANTS.TLR_MTCH_LVL_PARENT)
         and tdq_upper.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_QTY
         and tdq_upper.lower_limit(+)   <= dw.invc_qty
         and tdq_upper.upper_limit(+)    > dw.invc_qty
         and tdq_upper.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_RETL
         --
         and dw.tolerance_id             = tdc_lower.tolerance_id(+)
         and tdc_lower.match_level(+)    = DECODE(dw.entity_type,
                                                  REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU, REIM_CONSTANTS.TLR_MTCH_LVL_LINE,
                                                  REIM_CONSTANTS.TLR_MTCH_LVL_PARENT)
         and tdc_lower.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
         and tdc_lower.lower_limit(+)   <= (dw.invc_cost * dw.tolerance_exchange_rate)
         and tdc_lower.upper_limit(+)    > (dw.invc_cost * dw.tolerance_exchange_rate)
         and tdc_lower.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_SUPP
         --
         and dw.tolerance_id             = tdc_upper.tolerance_id(+)
         and tdc_upper.match_level(+)    = DECODE(dw.entity_type,
                                                  REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_SKU, REIM_CONSTANTS.TLR_MTCH_LVL_LINE,
                                                  REIM_CONSTANTS.TLR_MTCH_LVL_PARENT)
         and tdc_upper.match_type(+)     = REIM_CONSTANTS.TLR_MTCH_TYPE_COST
         and tdc_upper.lower_limit(+)   <= (dw.invc_cost * dw.tolerance_exchange_rate)
         and tdc_upper.upper_limit(+)    > (dw.invc_cost * dw.tolerance_exchange_rate)
         and tdc_upper.favor_of(+)       = REIM_CONSTANTS.TLR_FAVOR_OF_RETL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id
                                    || ' I_workspace: '  || I_workspace_id
                                    || ' I_item_view_ws_id: '  || I_item_view_ws_id
                                    || ' I_iitem_view_ws_id: ' || I_iitem_view_ws_id);


   open C_FETCH_TOLERANCE_INFO;
   fetch C_FETCH_TOLERANCE_INFO into O_result;
   close C_FETCH_TOLERANCE_INFO;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END FETCH_DETL_TOLERANCE_INFO;
----------------------------------------------------------------------------------------------
/**
 * The public function used to fetch Cost events Information.
 *
 * Input param: I_session_id       (OI Session Id of the User)
 *              I_workspace_id     (The ID of the User's Matching Workspace)
 *              I_item_view_ws_id  (The Detail match Workspace ID of the highlighted row on Detail match screen)
 *              I_iitem_view_ws_id (The Invoice Item View Workspace ID of the highlighted row on Invoice Item View screen)
 *              I_item_view_ws_id  (The Discrepancy List Workspace ID of the highlighted row on Discrepancy List screen)
 *
 * Output param: O_result (An Object Type Containing Cost Event Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_COST_EVENT_INFO(O_error_message       OUT VARCHAR2,
                               O_result           IN OUT REIM_OI_COST_EVENT_INFO_REC,
                               I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                               I_workspace_id     IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                               I_item_view_ws_id  IN     IM_DETAIL_MATCH_WS.DETAIL_MATCH_WS_ID%TYPE,
                               I_iitem_view_ws_id IN     IM_INVC_ITEM_VIEW_WS.INVC_ITEM_VIEW_WS_ID%TYPE,
                               I_disc_view_ws_id  IN     IM_DISCREPANCY_LIST_WS.DISCREPANCY_LIST_WS_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.FETCH_COST_EVENT_INFO';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

      cursor C_FETCH_COST_EVENT_INFO is
      select REIM_OI_COST_EVENT_INFO_REC(gtt.number_2,
                                         gtt.date_2,
                                         gtt.number_5,
                                         gtt.varchar2_3,
                                         gtt.date_1,
                                         gtt.number_6,
                                         gtt.date_11,
                                         gtt.number_11,
                                         gtt.date_12,
                                         gtt.number_12,
                                         gtt.date_13,
                                         gtt.number_13)
        from gtt_15_num_15_str_15_date gtt;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id
                                    ||' I_workspace: '  || I_workspace_id
                                    ||' I_item_view_ws_id: '  || I_item_view_ws_id
                                    ||' I_iitem_view_ws_id: ' || I_iitem_view_ws_id
                                    ||' I_disc_view_ws_id: '  || I_disc_view_ws_id);

   delete from gtt_15_num_15_str_15_date;
   insert into gtt_15_num_15_str_15_date(number_1,   --order_no
                                         number_2,   --doc_id
                                         number_3,   --location
                                         number_4,   --supplier_site
                                         number_5,   --invc_unit_cost
                                         number_6,   --PO_unit_cost
                                         number_7,   --import_id
                                         number_8,   --invc_location
                                         number_9,   --conv_factor
                                         number_11,  --Event1_unit_cost
                                         number_12,  --Event2_unit_cost
                                         number_13,  --Event3_unit_cost
                                         varchar2_1, --item
                                         varchar2_2, --origin_country_id
                                         varchar2_3, --currency_code
                                         date_1,     --orig_approval_date
                                         date_2,     --doc_date
                                         date_11,    --Event1_date
                                         date_12,    --Event2_date
                                         date_13)    --Event3_date
      with detl_ws as (select idmwi.detail_match_ws_id
                         from im_detail_match_ws idmwi
                        where idmwi.workspace_id        = I_workspace_id
                          and idmwi.detail_match_ws_id  = I_item_view_ws_id
                          and idmwi.entity_type         = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                          and idmwi.match_status        = 'U'
                       union all
                       select max(idmwi.detail_match_ws_id) detail_match_ws_id
                         from im_detail_match_ws idmwi
                        where idmwi.workspace_id = I_workspace_id
                          and idmwi.ancestor_id  = I_item_view_ws_id
                          and idmwi.entity_type  = REIM_CONSTANTS.DETAIL_MATCH_WS_ENTITY_INVC
                          and idmwi.match_status = 'U'
                       union all
                       select iiivw.invc_detail_match_ws_id detail_match_ws_id
                         from im_invc_item_view_ws iiivw
                        where iiivw.workspace_id         = I_workspace_id
                          and iiivw.invc_item_view_ws_id = I_iitem_view_ws_id
                       union all
                       select idlw.invc_detail_match_ws_id detail_match_ws_id
                         from im_discrepancy_list_ws idlw
                        where idlw.workspace_id           = I_workspace_id
                          and idlw.discrepancy_list_ws_id = I_disc_view_ws_id)
      select imiw.order_no,
             imiw.doc_id,
             NULL, --merge later
             idmwi.invc_po_supplier_site_id,
             idmwi.invoice_unit_cost,
             idmwi.invc_ordloc_unit_cost,
             oh.import_id,
             imiw.location,
             NULL, --merge later
             NULL, --merge later
             NULL, --merge later
             NULL, --merge later
             idmwi.item,
             os.origin_country_id,
             imiw.currency_code,
             oh.orig_approval_date,
             imiw.doc_date,
             NULL, --merge later
             NULL, --merge later
             NULL  --merge later
        from detl_ws dw,
             im_detail_match_ws idmwi,
             im_match_invc_ws imiw,
             ordhead oh,
             ordsku os
       where idmwi.detail_match_ws_id = dw.detail_match_ws_id
         and imiw.doc_id              = idmwi.invoice_id
         and imiw.workspace_id        = idmwi.workspace_id
         and oh.order_no              = imiw.order_no
         and os.order_no              = imiw.order_no
         and os.item                  = idmwi.item;

   LOGGER.LOG_INFORMATION(L_program||' Insert Primary Information on GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_transform_ordloc_gtt;
   insert into im_transform_ordloc_gtt (order_no,
                                        item,
                                        item_desc,
                                        loc,
                                        loc_type,
                                        qty_ordered,
                                        unit_cost,
                                        unit_cost_init,
                                        cost_source,
                                        sup_qty_level,
                                        catch_weight_type,
                                        import_loc,
                                        import_loc_type,
                                        transform_qty_ordered,
                                        transform_unit_cost,
                                        transform_unit_cost_init)
   select distinct gtt.number_1,
          gtt.varchar2_1,
          im.item_desc,
          ol.location,
          ol.loc_type,
          ol.qty_ordered,
          ol.unit_cost,
          ol.unit_cost_init,
          ol.cost_source,
          s.sup_qty_level,
          im.catch_weight_type,
          ol.location,
          ol.loc_type,
          ol.qty_ordered,
          ol.unit_cost,
          ol.unit_cost_init
     from gtt_15_num_15_str_15_date gtt,
          ordloc ol,
          sups s,
          item_master im
    where ol.order_no = gtt.number_1
      and ol.item     = gtt.varchar2_1
      and s.supplier  = gtt.number_4
      and im.item     = gtt.varchar2_1;

   LOGGER.LOG_INFORMATION(L_program||' Insert transform ordloc gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_XFORM_ORDLOC_SQL.TRANSFORM_ORDLOC_GTT(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   merge into gtt_15_num_15_str_15_date tgt
   using (select gtt.number_1,
                 gtt.varchar2_1,
                 xform_gtt.loc,
                 RANK() OVER (PARTITION BY xform_gtt.order_no
                                  ORDER BY NVL(gtt.number_7, xform_gtt.loc),
                                           xform_gtt.loc) loc_rnk,
                 xform_gtt.transform_unit_cost/xform_gtt.unit_cost conv_factor
            from gtt_15_num_15_str_15_date gtt,
                 im_transform_ordloc_gtt xform_gtt,
                 (select store location, store physical_loc from store
                  union all
                  select wh location, physical_wh physical_loc from wh) locs
           where xform_gtt.order_no = gtt.number_1
             and xform_gtt.item     = gtt.varchar2_1
             and locs.location      = NVL(gtt.number_7, xform_gtt.loc)) src
   on (    tgt.number_1   = src.number_1
       and tgt.varchar2_1 = src.varchar2_1
       and src.loc_rnk    = REIM_CONSTANTS.ONE)
   when MATCHED then
      update
         set tgt.number_3 = src.loc,
             tgt.number_9 = src.conv_factor;

   LOGGER.LOG_INFORMATION(L_program||' Merge Location and conv_factor on gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into gtt_15_num_15_str_15_date tgt
   using (select gtt.number_1,
                 gtt.varchar2_1,
                 fc.pricing_cost*gtt.number_9 future_cost,
                 fc.active_date future_cost_date,
                 RANK() OVER (PARTITION BY fc.item,
                                           fc.location,
                                           fc.supplier
                                  ORDER BY fc.active_date) date_rnk
            from gtt_15_num_15_str_15_date gtt,
                 future_cost fc
           where fc.item     = gtt.varchar2_1
             and fc.location = gtt.number_3
             and fc.supplier = gtt.number_4
             and fc.active_date > gtt.date_1
             and fc.cost_change is NOT NULL) src
   on (    tgt.number_1   = src.number_1
       and tgt.varchar2_1 = src.varchar2_1
       and src.date_rnk   = REIM_CONSTANTS.ONE)
   when MATCHED then
      update
         set tgt.number_13 = src.future_cost,
             tgt.date_13   = src.future_cost_date;

   LOGGER.LOG_INFORMATION(L_program||' Merge Future Cost Events on gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into gtt_15_num_15_str_15_date tgt
   using (select inner.number_1,
                 inner.varchar2_1,
                 Max(Decode(inner.date_rnk,1,inner.past_cost_date,NULL)) event_2_date,
                 Max(Decode(inner.date_rnk,1,inner.past_cost,NULL)) event_2_unit_cost,
                 Max(Decode(inner.date_rnk,2,inner.past_cost_date,NULL)) event_1_date,
                 Max(Decode(inner.date_rnk,2,inner.past_cost,NULL)) event_1_unit_cost
            from (select gtt.number_1,
                         gtt.varchar2_1,
                         ph.unit_cost*gtt.number_9 past_cost,
                         ph.action_date past_cost_date,
                         RANK() OVER (PARTITION BY ph.item,
                                                   ph.loc
                                          ORDER BY ph.action_date desc) date_rnk
                    from gtt_15_num_15_str_15_date gtt,
                         price_hist ph
                   where ph.item        = gtt.varchar2_1
                     and ph.loc         = gtt.number_3
                     and ph.action_date <= gtt.date_1
                     and ph.tran_type   = 2) inner
           where inner.date_rnk in (1,2)
           group by inner.number_1,
                    inner.varchar2_1) src
   on (    tgt.number_1   = src.number_1
       and tgt.varchar2_1 = src.varchar2_1)
   when MATCHED then
      update
         set tgt.number_11 = src.event_1_unit_cost,
             tgt.date_11   = src.event_1_date,
             tgt.number_12 = src.event_2_unit_cost,
             tgt.date_12   = src.event_2_date;

   LOGGER.LOG_INFORMATION(L_program||' Merge Most recent Price hist on gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_FETCH_COST_EVENT_INFO;
   fetch C_FETCH_COST_EVENT_INFO into O_result;
   close C_FETCH_COST_EVENT_INFO;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END FETCH_COST_EVENT_INFO;
----------------------------------------------------------------------------------------------
/**
 * The public function used to fetch Auto Match rate Information.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Output param: O_result (An Object Type Containing Auto Match rate Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_AUTO_MATCH_RATE_INFO(O_error_message    OUT VARCHAR2,
                                    O_result        IN OUT REIM_OI_AUTOMATCH_RATE_REC,
                                    I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.FETCH_AUTO_MATCH_RATE_INFO';

   L_start_time TIMESTAMP := SYSTIMESTAMP;
   L_vdate DATE := get_vdate;

   cursor C_FETCH_AUTO_MATCH_RATE_INFO is
      select REIM_OI_AUTOMATCH_RATE_REC(NVL(SUM(total_matched_yday), 0),
                                        NVL(SUM(auto_matched_yday), 0),
                                        NVL(SUM(manual_matched_yday), 0),
                                        DECODE(NVL(SUM(total_matched_yday), 0),
                                               0, 0,
                                               (NVL(SUM(auto_matched_yday), 0)/NVL(SUM(total_matched_yday), 0))* 100),
                                        NVL(SUM(total_matched_3mth), 0),
                                        NVL(SUM(auto_matched_3mth), 0),
                                        NVL(SUM(manual_matched_3mth), 0),
                                        DECODE(NVL(SUM(total_matched_3mth), 0),
                                               0, 0,
                                               (NVL(SUM(auto_matched_3mth), 0)/NVL(SUM(total_matched_3mth), 0))* 100))
        from (select DECODE(match_date,
                            L_vdate - 1, 1,
                            0) total_matched_yday,
                     DECODE(match_date,
                            L_vdate - 1, DECODE(match_type,
                                                REIM_CONSTANTS.MATCH_TYPE_MANUAL, 0,
                                                1),
                            0) auto_matched_yday,
                     DECODE(match_date,
                            L_vdate - 1, DECODE(match_type,
                                                REIM_CONSTANTS.MATCH_TYPE_MANUAL, 1,
                                                0),
                            0) manual_matched_yday,
                     1 total_matched_3mth,
                     DECODE(match_type,
                            REIM_CONSTANTS.MATCH_TYPE_MANUAL, 0,
                            1) auto_matched_3mth,
                     DECODE(match_type,
                            REIM_CONSTANTS.MATCH_TYPE_MANUAL, 1,
                            0) manual_matched_3mth
                from im_doc_head idh
               where idh.type       = REIM_CONSTANTS.DOC_TYPE_MRCHI
                 and idh.status     IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                        REIM_CONSTANTS.DOC_STATUS_POSTED)
                 and idh.match_date BETWEEN L_vdate - THREE_MONTHS AND L_vdate - 1) inner;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id);

   open C_FETCH_AUTO_MATCH_RATE_INFO;
   fetch C_FETCH_AUTO_MATCH_RATE_INFO into O_result;
   close C_FETCH_AUTO_MATCH_RATE_INFO;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END FETCH_AUTO_MATCH_RATE_INFO;
----------------------------------------------------------------------------------------------
/**
 * The public function used to fetch Upcoming Invoices Information.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Output param: O_result (An Object Type Containing Upcoming Invoices Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_UPCMNG_INVC_INFO(O_error_message    OUT VARCHAR2,
                                O_result        IN OUT REIM_OI_UPCMNG_INVC_INFO_REC,
                                I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.FETCH_UPCMNG_INVC_INFO';

   L_start_time TIMESTAMP                 := SYSTIMESTAMP;
   L_vdate      DATE                      := get_vdate;
   L_user       IM_DOC_HEAD.MATCH_ID%TYPE := get_user;

   cursor C_FETCH_UPCMNG_INVC_INFO is
      select REIM_OI_UPCMNG_INVC_INFO_REC(L_vdate,
                                          NVL(SUM(complete_past_due), 0),
                                          NVL(SUM(remain_past_due), 0),
                                          NVL(SUM(complete_today), 0),
                                          NVL(SUM(remain_today), 0),
                                          NVL(SUM(complete_tmrw), 0),
                                          NVL(SUM(remain_tmrw), 0),
                                          NVL(SUM(complete_2days_out), 0),
                                          NVL(SUM(remain_2days_out), 0),
                                          NVL(SUM(complete_3days_out), 0),
                                          NVL(SUM(remain_3days_out), 0))
        from (with suppliers as (select distinct supplier
                                   from (select s.supplier
                                           from im_supplier_groups isg,
                                                im_supplier_group_members isgm,
                                                sups s
                                          where isg.ap_reviewer   = L_user
                                            and isgm.group_id     = isg.group_id
                                            and s.supplier_parent = isgm.supplier
                                         union all
                                         select s.supplier
                                           from im_supplier_options iso,
                                                sups s
                                          where iso.ap_reviewer = L_user
                                            and iso.supplier    IN (s.supplier,
                                                                    s.supplier_parent)))
              select case
                        when due_date < L_vdate and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_past_due,
                     case
                        when due_date < L_vdate and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                               REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_past_due,
                     case
                        when due_date = L_vdate and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_today,
                     case
                        when due_date = L_vdate and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                               REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_today,
                     case
                        when due_date = L_vdate + 1 and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_tmrw,
                     case
                        when due_date = L_vdate + 1 and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                   REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_tmrw,
                     case
                        when due_date = L_vdate + 2 and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_2days_out,
                     case
                        when due_date = L_vdate + 2 and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                   REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_2days_out,
                     case
                        when due_date = L_vdate + 3 and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_3days_out,
                     case
                        when due_date = L_vdate + 3 and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                   REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_3days_out
                from suppliers s,
                     im_doc_head idh
               where idh.supplier_site_id = s.supplier
                 and idh.type             = REIM_CONSTANTS.DOC_TYPE_MRCHI
                 and idh.status           NOT IN (REIM_CONSTANTS.DOC_STATUS_WORKSHEET,
                                                  REIM_CONSTANTS.DOC_STATUS_DELETE)) inner;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id);

   open C_FETCH_UPCMNG_INVC_INFO;
   fetch C_FETCH_UPCMNG_INVC_INFO into O_result;
   close C_FETCH_UPCMNG_INVC_INFO;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END FETCH_UPCMNG_INVC_INFO;
----------------------------------------------------------------------------------------------
/**
 * The public function used to Create Employee Invoice Report.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Populates im_oi_employee_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_EMPLOYEE_REPORT(O_error_message    OUT VARCHAR2,
                                I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.CREATE_EMPLOYEE_REPORT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;
   L_vdate DATE := get_vdate;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id);

   delete
     from im_oi_employee_report
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete existing rows from employee report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_oi_employee_report (session_id,
                                      user_id,
                                      complete_past_due,
                                      remain_past_due,
                                      complete_today,
                                      remain_today,
                                      complete_tmrw,
                                      remain_tmrw,
                                      complete_2days_out,
                                      remain_2days_out,
                                      complete_3days_out,
                                      remain_3days_out)
      select I_session_id,
             user_id,
             NVL(SUM(complete_past_due), 0),
             NVL(SUM(remain_past_due), 0),
             NVL(SUM(complete_today), 0),
             NVL(SUM(remain_today), 0),
             NVL(SUM(complete_tmrw), 0),
             NVL(SUM(remain_tmrw), 0),
             NVL(SUM(complete_2days_out), 0),
             NVL(SUM(remain_2days_out), 0),
             NVL(SUM(complete_3days_out), 0),
             NVL(SUM(remain_3days_out), 0)
        from (select so.ap_reviewer user_id,
                     case
                        when due_date < L_vdate and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_past_due,
                     case
                        when due_date < L_vdate and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                               REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_past_due,
                     case
                        when due_date = L_vdate and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_today,
                     case
                        when due_date = L_vdate and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                               REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_today,
                     case
                        when due_date = L_vdate + 1 and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_tmrw,
                     case
                        when due_date = L_vdate + 1 and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                   REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_tmrw,
                     case
                        when due_date = L_vdate + 2 and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_2days_out,
                     case
                        when due_date = L_vdate + 2 and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                   REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_2days_out,
                     case
                        when due_date = L_vdate + 3 and match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL then
                           1
                        else
                           0
                     end complete_3days_out,
                     case
                        when due_date = L_vdate + 3 and status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                   REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                           1
                        else
                           0
                     end remain_3days_out
                from im_doc_head idh,
                     v_im_supp_site_attrib_expl so
               where idh.type                 = REIM_CONSTANTS.DOC_TYPE_MRCHI
                 and idh.status               NOT IN (REIM_CONSTANTS.DOC_STATUS_WORKSHEET,
                                                      REIM_CONSTANTS.DOC_STATUS_DELETE)
                 and idh.supplier_site_id (+) = so.supplier
                 and idh.due_date             BETWEEN (L_vdate - THREE_MONTHS) AND (L_vdate + 3)) inner
             GROUP BY user_id;

   LOGGER.LOG_INFORMATION(L_program||' Create Employee Report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END CREATE_EMPLOYEE_REPORT;
----------------------------------------------------------------------------------------------
/**
 * The public function used to Create Supplier Sites Report.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Populates im_oi_supp_site_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_SUPP_SITE_REPORT(O_error_message    OUT VARCHAR2,
                                 I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.CREATE_SUPP_SITE_REPORT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_vdate           DATE                              := get_vdate;
   L_user            IM_DOC_HEAD.MATCH_ID%TYPE         := get_user;
   L_exchange_type   CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   L_weight_non_discrep NUMBER(5, 2) := NULL;
   L_weight_exact_match NUMBER(5, 2) := NULL;
   L_weight_timely_ship NUMBER(5, 2) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id);

   select DECODE(consolidation_ind, 'Y', 'C', 'O')
     into L_exchange_type
     from system_config_options;

   LOGGER.LOG_INFORMATION(L_program||' Fetch Exchange Type - L_exchange_type: ' || L_exchange_type);

   select weight_exact_match,
          weight_non_discrepancy,
          weight_timely_ship
     into L_weight_exact_match,
          L_weight_non_discrep,
          L_weight_timely_ship
     from im_oi_sys_options;

   LOGGER.LOG_INFORMATION(L_program||' Fetch Weightage - L_weight_exact_match: ' || L_weight_exact_match
                                                     || ' L_weight_non_discrep: '|| L_weight_non_discrep
                                                     || ' L_weight_timely_ship: '|| L_weight_timely_ship);

   delete
     from im_oi_supp_site_report
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete existing rows from Supplier Site report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_oi_supp_site_report (session_id,
                                       supplier,
                                       sup_name,
                                       compliance_scard,
                                       automatch_count,
                                       automatch_rate,
                                       cost_disc_count,
                                       qty_disc_count,
                                       early_ship_invc_count,
                                       late_ship_invc_count,
                                       invoice_count,
                                       total_amount,
                                       supp_currency_code,
                                       filter_ind)
      select I_session_id session_id,
             inner.supplier supplier,
             inner.sup_name sup_name,
             case
                        when inner.compliance > 95 then
                           5
                        when inner.compliance >= 75 and
                             inner.compliance < 95 then
                           4
                        when inner.compliance >= 55 and
                             inner.compliance < 75 then
                           3
                        when inner.compliance >= 35 and
                             inner.compliance < 55 then
                           2
                        when inner.compliance < 35 then
                           1
             end compliance_scard,
             inner.automatch_count,
             inner.automatch_rate,
             inner.cost_disc_count,
             inner.qty_disc_count,
             inner.early_ship_invc_count,
             inner.late_ship_invc_count,
             inner.invoice_count,
             inner.total_amount,
             inner.currency_code,
             'N' filter_ind
        from (select invc.supplier supplier,
                     v_tl.sup_name sup_name,
                     (100 - ((L_weight_non_discrep * (SUM(invc.cost_discrepant) + SUM(invc.qty_discrepant)) / DECODE(COUNT(invc.doc_id),
                                                                                                                     0, 1,
                                                                                                                     COUNT(invc.doc_id))) +
                             (L_weight_exact_match * (SUM(DECODE(invc.auto_match_ind,
                                                                 1, invc.hdr_vwt,
                                                                 0)) / DECODE(SUM(invc.auto_match_ind),
                                                                                  0, 1,
                                                                                  SUM(invc.auto_match_ind)))) +
                             (L_weight_timely_ship * ((SUM(DECODE(invc.early_shipment,
                                                                  1, 1,
                                                                  invc.late_shipment))) / DECODE(COUNT(invc.doc_id),
                                                                                                 0, 1,
                                                                                                 COUNT(invc.doc_id)))))) compliance,
                     SUM(invc.auto_match_ind) automatch_count,
                     (SUM(invc.auto_match_ind)/COUNT(invc.doc_id)) * 100 automatch_rate,
                     SUM(invc.cost_discrepant) cost_disc_count,
                     SUM(invc.qty_discrepant) qty_disc_count,
                     SUM(invc.early_shipment) early_ship_invc_count,
                     SUM(invc.late_shipment) late_ship_invc_count,
                     COUNT(invc.doc_id) invoice_count,
                     SUM(invc.total_cost) total_amount,
                     invc.currency_code
                from (with suppliers as (select distinct supplier,
                                                         currency_code
                                           from (select s.supplier,
                                                        s.currency_code
                                                   from im_supplier_groups isg,
                                                        im_supplier_group_members isgm,
                                                        sups s
                                                  where isg.ap_reviewer   = L_user
                                                    and isgm.group_id     = isg.group_id
                                                    and s.supplier_parent = isgm.supplier
                                                 union all
                                                 select s.supplier,
                                                        currency_code
                                                   from im_supplier_options iso,
                                                        sups s
                                                  where iso.ap_reviewer = L_user
                                                    and iso.supplier    IN (s.supplier,
                                                                            s.supplier_parent))),
                            invoices as (select idh.doc_id,
                                                idh.order_no,
                                                idh.currency_code,
                                                idh.total_cost_inc_tax,
                                                idh.supplier_site_id,
                                                DECODE(NVL(idh.variance_within_tolerance, 0),
                                                       0, 0,
                                                       1) hdr_vwt,
                                                DECODE(idh.match_type,
                                                       REIM_CONSTANTS.MATCH_TYPE_AUTO, 1,
                                                       REIM_CONSTANTS.MATCH_TYPE_EXTERNAL, 1,
                                                       0) auto_match_ind
                                           from suppliers s,
                                                im_doc_head idh,
                                                (select store location
                                                   from v_store
                                                 union
                                                 select wh location
                                                   from v_wh) sec
                                          where idh.supplier_site_id = s.supplier
                                            and idh.type                 = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                            and idh.status               NOT IN (REIM_CONSTANTS.DOC_STATUS_WORKSHEET,
                                                                                 REIM_CONSTANTS.DOC_STATUS_DELETE)
                                            and idh.due_date             BETWEEN (L_vdate - THREE_MONTHS) AND (L_vdate + 3)
                                            and idh.location             = sec.location),
                            receipts as (select ir.supplier_site_id,
                                                ir.order_no,
                                                max(early_shipment) early_shipment,
                                                max(late_shipment) late_shipment
                                           from (select i.supplier_site_id,
                                                        sh.order_no,
                                                        case
                                                           when sh.receive_date < oh.not_before_date then
                                                              1
                                                           else
                                                              0
                                                        end early_shipment,
                                                        case
                                                           when sh.receive_date > oh.not_after_date then
                                                              1
                                                           else
                                                              0
                                                        end late_shipment
                                                   from invoices i,
                                                        ordhead oh,
                                                        shipment sh
                                                  where oh.order_no     = i.order_no
                                                    and sh.order_no     = i.order_no
                                                    and sh.receive_date is NOT NULL) ir
                                         GROUP BY ir.supplier_site_id,
                                                  ir.order_no),
                            invc_detl as (select i.doc_id,
                                                 MAX(DECODE(iid.cost_matched,
                                                                    'D', 1,
                                                                    'R', 1,
                                                                    0)) cost_discrepant,
                                                 MAX(DECODE(iid.qty_matched,
                                                                    'D', 1,
                                                                    'R', 1,
                                                                    0)) qty_discrepant
                                           from invoices i,
                                                im_invoice_detail iid
                                          where iid.doc_id = i.doc_id
                                          GROUP BY i.doc_id),
                            curr_conv as (select from_currency,
                                                 to_currency,
                                                 effective_date,
                                                 exchange_rate,
                                                 RANK() OVER (PARTITION BY from_currency,
                                                                           to_currency
                                                                  ORDER BY effective_date DESC) rnk
                                            from mv_currency_conversion_rates
                                           where exchange_type   = L_exchange_type
                                             and effective_date <= L_vdate)
                      select s.supplier,
                             i.doc_id,
                             DECODE(i.auto_match_ind,
                                    1, DECODE(NVL(id.cost_discrepant, 0),
                                              1, 0,
                                              DECODE(NVL(id.qty_discrepant, 0),
                                                     1, 0,
                                                     1)),
                                    i.auto_match_ind) auto_match_ind,
                             i.hdr_vwt,
                             NVL(id.cost_discrepant, 0) cost_discrepant,
                             NVL(id.qty_discrepant, 0) qty_discrepant,
                             r.early_shipment,
                             r.late_shipment,
                             i.total_cost_inc_tax * cc.exchange_rate total_cost,
                             s.currency_code
                        from suppliers s,
                             invoices i,
                             receipts r,
                             invc_detl id,
                             curr_conv cc
                       where i.supplier_site_id = s.supplier
                         and i.supplier_site_id = r.supplier_site_id
                         and i.order_no         = r.order_no
                         and id.doc_id (+)      = i.doc_id
                         and cc.from_currency   = i.currency_code
                         and cc.to_currency     = s.currency_code
                         and cc.rnk             = 1) invc,
                     v_sups_tl v_tl
               where invc.supplier = v_tl.supplier
               GROUP BY invc.supplier,
                        invc.currency_code,
                        v_tl.sup_name) inner;

   LOGGER.LOG_INFORMATION(L_program||' Create Supplier Site Report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END CREATE_SUPP_SITE_REPORT;
----------------------------------------------------------------------------------------------
/**
 * The public function used to Filter Records on Supplier Site Report.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *              I_filter_type (Filter Type used - Can be 'D'(Dept),'SS'(Supplier Site))
 *              I_filter_dept (Department Identifier)
 *              I_filter_supp_site (Supplier Site Identifier)
 *
 * Updates im_oi_supp_site_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FILTER_SUPP_SITE_REPORT(O_error_message       OUT VARCHAR2,
                                 I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                 I_filter_type      IN     VARCHAR2,
                                 I_filter_dept      IN     DEPS.DEPT%TYPE,
                                 I_filter_supp_site IN     SUPS.SUPPLIER%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.FILTER_SUPP_SITE_REPORT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id
                                    || ' I_filter_type: ' || I_filter_type
                                    || ' I_filter_dept: ' || I_filter_dept
                                    || ' I_filter_supp_site: ' || I_filter_supp_site);

   update im_oi_supp_site_report
      set filter_ind = DECODE(I_filter_type,
                              NULL, 'N',
                              'Y')
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Hide/Show all rows based on filter type - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_oi_supp_site_report tgt
   using (select distinct supplier
            from (select its.supplier
                    from item_master im,
                         item_supplier its
                   where I_filter_type = 'D'
                     and im.dept       = I_filter_dept
                     and its.item      = im.item
                  union all
                  select I_filter_supp_site supplier
                    from dual
                   where I_filter_type = 'SS')) src
   on (    tgt.session_id = I_session_id
       and tgt.supplier = src.supplier)
   when MATCHED then
      update
         set tgt.filter_ind = 'N';

   LOGGER.LOG_INFORMATION(L_program||' Show Required rows on Supplier Site report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END FILTER_SUPP_SITE_REPORT;
----------------------------------------------------------------------------------------------
/**
 * The public function used to Create Invoice Report.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Populates im_oi_invoice_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_INVOICE_REPORT(O_error_message    OUT VARCHAR2,
                               I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.CREATE_INVOICE_REPORT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_vdate           DATE                                     := get_vdate;
   L_user            IM_DOC_HEAD.MATCH_ID%TYPE                := get_user;
   L_exchange_type   CURRENCY_RATES.EXCHANGE_TYPE%TYPE        := NULL;
   L_system_currency SYSTEM_CONFIG_OPTIONS.CURRENCY_CODE%TYPE := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id);

   select DECODE(consolidation_ind, 'Y', 'C', 'O'),
          currency_code
     into L_exchange_type,
          L_system_currency
     from system_config_options;

   LOGGER.LOG_INFORMATION(L_program||' Fetch Exchange Type - L_exchange_type: ' || L_exchange_type);

   delete
     from im_oi_invoice_report
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete existing rows from Invoice report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_oi_invoice_report (session_id,
                                     user_id,
                                     priority,
                                     doc_id,
                                     ext_doc_id,
                                     order_no,
                                     supplier_site_id,
                                     supplier_site_name,
                                     location,
                                     loc_type,
                                     doc_date,
                                     due_date,
                                     items,
                                     total_cost,
                                     total_cost_sy_curr,
                                     invoice_curr,
                                     system_curr,
                                     cash_discount_ind,
                                     cost_disc_ind,
                                     qty_disc_ind,
                                     tax_disc_ind,
                                     supplier)
      select I_session_id session_id,
             L_user user_id,
             RANK() OVER (ORDER BY inner.due_date,
                                   inner.cash_discount_ind desc,
                                   DECODE(inner.cash_discount_ind,
                                          'Y', inner.total_cost_sy_curr,
                                          1) desc,
                                   (inner.cost_discrepant + inner.qty_discrepant) desc,
                                   inner.cost_discrepant desc,
                                   inner.qty_discrepant desc,
                                   inner.total_cost_sy_curr desc) priority,
             inner.doc_id,
             inner.ext_doc_id,
             inner.order_no,
             inner.supplier_site_id,
             inner.supplier_site_name,
             inner.location,
             inner.loc_type,
             inner.doc_date,
             inner.due_date,
             inner.items,
             inner.total_cost,
             inner.total_cost_sy_curr,
             inner.invoice_curr,
             L_system_currency system_curr,
             inner.cash_discount_ind,
             DECODE(inner.cost_discrepant,
                    0, 'N',
                    'Y') cost_disc_ind,
             DECODE(inner.qty_discrepant,
                    0, 'N',
                    'Y') qty_disc_ind,
             inner.tax_disc_ind,
             inner.supplier
        from (with suppliers as (select distinct supplier
                                   from (select s.supplier
                                           from im_supplier_groups isg,
                                                im_supplier_group_members isgm,
                                                sups s
                                          where isg.ap_reviewer   = L_user
                                            and isgm.group_id     = isg.group_id
                                            and s.supplier_parent = isgm.supplier
                                         union all
                                         select s.supplier
                                           from im_supplier_options iso,
                                                sups s
                                          where iso.ap_reviewer = L_user
                                            and iso.supplier    IN (s.supplier,
                                                                    s.supplier_parent))),
                   invoices as (select idh.doc_id,
                                       idh.ext_doc_id,
                                       idh.order_no,
                                       idh.terms,
                                       idh.doc_date,
                                       idh.due_date,
                                       idh.location,
                                       idh.loc_type,
                                       idh.total_cost_inc_tax,
                                       idh.total_cost_inc_tax/idh.exchange_rate total_cost_sy_curr,
                                       idh.currency_code,
                                       idh.supplier_site_id,
                                       DECODE(td.doc_id,
                                              NULL, 'N',
                                              'Y') tax_disc_ind,
                                       idh.vendor
                                  from suppliers s,
                                       im_doc_head idh,
                                       (select store location
                                          from v_store
                                        union
                                        select wh location
                                          from v_wh) sec,
                                       (select distinct doc_id
                                          from im_tax_discrepancy) td
                                 where idh.supplier_site_id = s.supplier
                                   and idh.type                 = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                   and idh.status               IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,                                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH,
REIM_CONSTANTS.DOC_STATUS_TAXDIS)
                                   and idh.due_date             <= (L_vdate + 3)
                                   and idh.location             = sec.location
                                   and td.doc_id (+)            = idh.doc_id),
                   invc_detl as (select i.doc_id,
                                        count(iid.item) item_cnt,
                                        MAX(DECODE(iid.cost_matched,
                                                   'D', 1,
                                                   'R', 1,
                                                   0)) cost_discrepant,
                                        MAX(DECODE(iid.qty_matched,
                                                   'D', 1,
                                                   'R', 1,
                                                   0)) qty_discrepant
                                   from invoices i,
                                        im_invoice_detail iid
                                  where iid.doc_id = i.doc_id
                                  GROUP BY i.doc_id),
                   terms_detl as (select distinct terms,
                                         percent,
                                         NVL(start_date_active, TO_DATE('00010101', 'YYYYMMDD')) start_date_active,
                                         NVL(end_date_active, TO_DATE('99990101', 'YYYYMMDD')) end_date_active
                                    from terms_detail)
              select i.supplier_site_id,
                     i.doc_id,
                     i.ext_doc_id,
                     i.order_no,
                     v_tl.sup_name supplier_site_name,
                     i.location,
                     i.loc_type,
                     i.doc_date,
                     i.due_date,
                     NVL(id.item_cnt, 0) items,
                     i.total_cost_inc_tax total_cost,
                     i.total_cost_sy_curr,
                     i.currency_code invoice_curr,
                     DECODE(td.percent,
                            0, 'N',
                            'Y') cash_discount_ind,
                     NVL(id.cost_discrepant, 0) cost_discrepant,
                     NVL(id.qty_discrepant, 0) qty_discrepant,
                     i.tax_disc_ind,
                     RANK() OVER (PARTITION BY i.doc_id
                                      ORDER BY td.start_date_active desc,
                                               td.end_date_active) terms_rnk,
                     i.vendor supplier
                from suppliers s,
                     invoices i,
                     invc_detl id,
                     terms_detl td,
                     v_sups_tl v_tl
               where i.supplier_site_id = s.supplier
                 and id.doc_id (+)      = i.doc_id
                 and td.terms           = i.terms
                 and i.doc_date         BETWEEN td.start_date_active and td.end_date_active
                 and v_tl.supplier      = s.supplier) inner
       where inner.terms_rnk = 1;

   LOGGER.LOG_INFORMATION(L_program||' Create Invoice Report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END CREATE_INVOICE_REPORT;
----------------------------------------------------------------------------------------------
/**
 * The public function used to Delete records from Employee Invoice Report belonging to the user session.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * deletes from im_oi_employee_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION DELETE_EMPLOYEE_REPORT(O_error_message    OUT VARCHAR2,
                                I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.DELETE_EMPLOYEE_REPORT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id);

   delete
     from im_oi_employee_report
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete existing rows from employee report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END DELETE_EMPLOYEE_REPORT;
----------------------------------------------------------------------------------------------
/**
 * The public function used to Delete records from Supplier Sites Report belonging to the user session.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * deletes from im_oi_supp_site_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION DELETE_SUPP_SITE_REPORT(O_error_message    OUT VARCHAR2,
                                 I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.DELETE_SUPP_SITE_REPORT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id);

   delete
     from im_oi_supp_site_report
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete existing rows from Supplier Site report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END DELETE_SUPP_SITE_REPORT;
----------------------------------------------------------------------------------------------
/**
 * The public function used to Delete records Invoice Report belonging to the user session.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * deletes from im_oi_invoice_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION DELETE_INVOICE_REPORT(O_error_message    OUT VARCHAR2,
                               I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_OI_SQL.DELETE_INVOICE_REPORT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||'Start - I_session_id: ' || I_session_id);

   delete
     from im_oi_invoice_report
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete existing rows from Invoice report - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return REIM_CONSTANTS.FAIL;

END DELETE_INVOICE_REPORT;
----------------------------------------------------------------------------------------------
END REIM_OI_SQL;
/
