CREATE OR REPLACE PACKAGE BODY REIM_TAX_CALC_SQL AS
    --------------------------------------------
    PROCEDURE CALCULATE_ITEM_TAXES (O_STATUS              OUT   NUMBER,  -- Success status of the procedure.
	                                O_ERROR_MESSAGE       OUT   VARCHAR2, -- Error message if the procedure failed.
	                                I_ITEM_TAX_CRITERIA   IN    OBJ_ITEM_TAX_CRITERIA_TBL, -- The tax criteria to calculate taxes for.
	                                I_ITEM_TAX_CALC_OVRD  IN    OBJ_ITEM_TAX_CALC_OVRD_TBL, -- Overriding values for calculating taxes.
                                    O_ITEM_TAX_RESULTS    OUT   OBJ_ITEM_TAX_BREAK_TBL -- A table type for the results of the tax calculations, one row per tax
	                                )
    IS
        L_PROGRAM VARCHAR2(50) := 'REIM_TAX_CALC_SQL.CALCULATE_ITEM_TAXES';
    BEGIN
        REIM_TAX_WRAPPER_SQL.CALCULATE_ITEM_TAXES_SINGLETAX(O_STATUS,
                                                  O_ERROR_MESSAGE,
                                                  I_ITEM_TAX_CRITERIA,
                                                  I_ITEM_TAX_CALC_OVRD,
                                                  O_ITEM_TAX_RESULTS);
    EXCEPTION
        WHEN OTHERS THEN
          O_ERROR_MESSAGE := O_ERROR_MESSAGE
                            ||SQL_LIB.CREATE_MSG(PACKAGE_ERROR,SQLERRM,L_PROGRAM, TO_CHAR(SQLCODE));
          O_STATUS := FAIL;
    END CALCULATE_ITEM_TAXES;

    --------------------------------------------

END REIM_TAX_CALC_SQL;
/
