CREATE OR REPLACE PACKAGE BODY REIM_HOLDING_SQL AS


  PROCEDURE UPDATE_INVOICE_DOCUMENTS
       (O_STATUS          OUT NUMBER,
        O_ERROR_MESSAGE   OUT VARCHAR2,
        I_RESOLUTION_DOC  IN OBJ_REIM_INV_DOCUMENT_TBL)
  AS
  BEGIN
  
  
    -- -------------------------------------------------------------------------
    -- First, insert documents based off merchandise invoices.  This should 
    -- include all resolution documents, i.e. credit note requests, 
    -- debit memos, etc.
    -- -------------------------------------------------------------------------
    INSERT INTO IM_INV_DOCUMENT
               (INV_ID,
                REF_DOC_ID)
    (SELECT INPUT.DOCUMENT_ID,
            INPUT.REF_DOC_ID
     FROM   (SELECT DOCUMENT_ID,
                    REF_DOC_ID
             FROM   TABLE(CAST(I_RESOLUTION_DOC AS OBJ_REIM_INV_DOCUMENT_TBL))) INPUT,
            IM_DOC_HEAD IDH
     WHERE  IDH.DOC_ID = INPUT.DOCUMENT_ID
            AND IDH.TYPE = 'MRCHI'
            AND NOT EXISTS (SELECT 'X'
                            FROM   IM_INV_DOCUMENT IID
                            WHERE  IID.INV_ID = INPUT.DOCUMENT_ID
                                   AND IID.REF_DOC_ID = INPUT.REF_DOC_ID));
    
    -- -------------------------------------------------------------------------
    -- Second, we need to insert credit notes that could have potentially been
    -- matched with documents already in the cross reference (including things
    -- we just inserted above -- but not limited to just these).
    -- -------------------------------------------------------------------------
    INSERT INTO IM_INV_DOCUMENT
               (INV_ID,
                REF_DOC_ID)
    (SELECT DISTINCT IID.INV_ID,
                     INPUT.DOCUMENT_ID
     FROM   (SELECT DOCUMENT_ID,
                    REF_DOC_ID
             FROM   TABLE(CAST(I_RESOLUTION_DOC AS OBJ_REIM_INV_DOCUMENT_TBL))) INPUT,
            IM_DOC_HEAD DH,
            IM_CN_DETAIL_MATCH_HIS CN,
            IM_CN_DETAIL_MATCH_HIS CNR,
            IM_INV_DOCUMENT IID
     WHERE  INPUT.DOCUMENT_ID = DH.DOC_ID
            AND DH.TYPE = 'CRDNT'
            AND CN.DOC_ID = INPUT.DOCUMENT_ID
            AND CN.MATCH_ID = CNR.MATCH_ID
            AND CN.ITEM = CNR.ITEM
            AND CN.DOC_ID != CNR.DOC_ID
            AND CNR.DOC_ID = IID.REF_DOC_ID
            AND NOT EXISTS (SELECT 'X'
                            FROM   IM_INV_DOCUMENT IID2
                            WHERE  IID.INV_ID = IID2.INV_ID
                                   AND IID2.REF_DOC_ID = INPUT.DOCUMENT_ID));
    
    -- -------------------------------------------------------------------------
    -- Third, we need to account for resolution documents that may have been
    -- based off of credit notes (as opposed to invoices) that are existing in 
    -- the cross reference.  Remember that credit notes still belong in the
    -- REF_DOC_ID column of IM_INV_DOCUMENT.
    -- -------------------------------------------------------------------------
    INSERT INTO IM_INV_DOCUMENT
               (INV_ID,
                REF_DOC_ID)
    (SELECT DISTINCT IID.INV_ID,
                     INPUT.REF_DOC_ID
     FROM   (SELECT DOCUMENT_ID,
                    REF_DOC_ID
             FROM   TABLE(CAST(I_RESOLUTION_DOC AS OBJ_REIM_INV_DOCUMENT_TBL))) INPUT,
            IM_DOC_HEAD IDH,
            IM_INV_DOCUMENT IID
     WHERE  IDH.DOC_ID = INPUT.DOCUMENT_ID
            AND IDH.TYPE = 'CRDNT'
            AND IID.REF_DOC_ID = IDH.DOC_ID
            AND NOT EXISTS (SELECT 'X'
                            FROM   IM_INV_DOCUMENT IID2
                            WHERE  IID2.INV_ID = IID.INV_ID
                                   AND IID2.REF_DOC_ID = INPUT.REF_DOC_ID));
    
    O_STATUS := LP_SUCCESS;
  EXCEPTION
    WHEN OTHERS THEN
      O_ERROR_MESSAGE := O_ERROR_MESSAGE
                         ||SQL_LIB.CREATE_MSG(REIM_HOLDING_SQL.LP_PACKAGE_ERROR,SQLERRM,
                                              LP_PROGRAM,TO_CHAR(SQLCODE));
      
      O_STATUS := LP_FAIL;
      
  
  END UPDATE_INVOICE_DOCUMENTS;
  
  
  PROCEDURE UPDATE_DOCUMENT_HOLDING
       (O_STATUS         OUT NUMBER,
        O_ERROR_MESSAGE  OUT VARCHAR2)
  AS
  BEGIN
  
  
    -- -------------------------------------------------------------------------------
    -- Release held invoices that have no outstanding CNRs that are not matched voided
    -- -------------------------------------------------------------------------------
    UPDATE IM_DOC_HEAD IDH
    SET    IDH.HOLD_STATUS = 'R'
    WHERE  IDH.HOLD_STATUS = 'H'
           AND IDH.TYPE = 'MRCHI'
           AND NOT EXISTS (SELECT 'X'
                           FROM   IM_INV_DOCUMENT IID,
                                  IM_DOC_HEAD RES
                           WHERE  IID.REF_DOC_ID = RES.DOC_ID
                                  AND IDH.DOC_ID = IID.INV_ID
                                  AND RES.STATUS = 'APPRVE' 
                                  AND (RES.TYPE='CRDNRC' OR RES.TYPE='CRDNRQ' OR RES.TYPE='CRDNRT'));
    
    -- -------------------------------------------------------------------------
    -- Release held credit notes that are tied to an invoice that is no longer
    -- being held.
    -- -------------------------------------------------------------------------
    UPDATE IM_DOC_HEAD IDH
    SET    IDH.HOLD_STATUS = 'R'
	WHERE  IDH.HOLD_STATUS = 'H'
       AND IDH.TYPE = 'CRDNT'
       AND IDH.STATUS = 'MTCH'
       AND NOT EXISTS (SELECT 'X'
                       FROM   IM_INV_DOCUMENT IID,
                              IM_DOC_HEAD INVOICE
                       WHERE  IID.REF_DOC_ID = IDH.DOC_ID
                              AND IID.INV_ID = INVOICE.DOC_ID
                              AND INVOICE.TYPE = 'MRCHI'
                              AND INVOICE.HOLD_STATUS = 'H')
       AND NOT EXISTS (SELECT 'X'
                       FROM   IM_INV_DOCUMENT IID,
                              IM_DOC_HEAD INVOICE,
                              IM_DOC_HEAD CNR,
                              IM_CN_SUMMARY_MATCH_HIS SMH1,
                              IM_CN_SUMMARY_MATCH_HIS SMH2
                       WHERE  IDH.DOC_ID = SMH1.DOC_ID
                              AND SMH2.DOC_ID != SMH1.DOC_ID
                              AND SMH2.MATCH_ID = SMH1.MATCH_ID
							  AND SMH2.DOC_ID = CNR.DOC_ID
                              AND (CNR.TYPE = 'CRDNRQ' OR CNR.TYPE = 'CRDNRC')
                              AND CNR.DOC_ID = IID.REF_DOC_ID
                              AND IID.INV_ID = INVOICE.DOC_ID
                              AND INVOICE.TYPE = 'MRCHI'
                              AND INVOICE.HOLD_STATUS = 'H')
       AND NOT EXISTS (SELECT 'X'
                       FROM   IM_INV_DOCUMENT IID,
                              IM_DOC_HEAD INVOICE,
                              IM_DOC_HEAD CNR,
                              IM_CN_DETAIL_MATCH_HIS DMH1,
                              IM_CN_DETAIL_MATCH_HIS DMH2
                       WHERE  IDH.DOC_ID = DMH1.DOC_ID
                              AND DMH2.DOC_ID != DMH1.DOC_ID
                              AND DMH2.MATCH_ID = DMH1.MATCH_ID
                              AND DMH2.ITEM = DMH1.ITEM
							  AND DMH2.DOC_ID = CNR.DOC_ID
                              AND (CNR.TYPE = 'CRDNRQ' OR CNR.TYPE = 'CRDNRC')
                              AND CNR.DOC_ID = IID.REF_DOC_ID
                              AND IID.INV_ID = INVOICE.DOC_ID
                              AND INVOICE.TYPE = 'MRCHI'
                              AND INVOICE.HOLD_STATUS = 'H');
    
    -- -------------------------------------------------------------------------
    -- Update invoices for holding that do have outstanding resolution 
    -- documents or credit notes that are in approved status.  We don't hold
    -- credit notes here because they're automatically held when they're 
    -- uploaded from EDI.
    -- -------------------------------------------------------------------------
    UPDATE IM_DOC_HEAD DH
    SET    DH.HOLD_STATUS = 'H'
    WHERE  DH.HOLD_STATUS = 'N'
           AND DH.TYPE = 'MRCHI'
           AND DH.STATUS != 'POSTED'
           AND DH.DOC_ID IN (SELECT DISTINCT IID.INV_ID
                             FROM   IM_INV_DOCUMENT IID,
                                    IM_DOC_HEAD DH2,
                                    IM_SUPPLIER_OPTIONS ISO
                             WHERE  DH2.DOC_ID = IID.REF_DOC_ID
                                    AND DH2.STATUS = 'APPRVE'
                                    AND TO_CHAR(DH2.VENDOR) = TO_CHAR(ISO.SUPPLIER)
                                    AND ISO.HOLD_INVOICES = 'Y');
    
    O_STATUS := LP_SUCCESS;
  EXCEPTION
    WHEN OTHERS THEN
      O_ERROR_MESSAGE := O_ERROR_MESSAGE
                         ||SQL_LIB.CREATE_MSG(REIM_HOLDING_SQL.LP_PACKAGE_ERROR,SQLERRM,
                                              LP_PROGRAM,TO_CHAR(SQLCODE));
      
      O_STATUS := LP_FAIL;
      
  
  END UPDATE_DOCUMENT_HOLDING;
END REIM_HOLDING_SQL;
/