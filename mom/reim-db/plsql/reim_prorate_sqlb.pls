CREATE OR REPLACE PACKAGE BODY REIM_PRORATE_SQL AS

    /*****

    ******/
    FUNCTION GET_NEXT_PRORATE_WORKSPACE_ID
      RETURN IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE
      IS
        L_NEXT_SEQ  IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE;
      BEGIN
        SELECT IM_PRORATE_WORKSPACE_SEQ.NEXTVAL INTO L_NEXT_SEQ FROM DUAL;
        RETURN L_NEXT_SEQ;
      END GET_NEXT_PRORATE_WORKSPACE_ID;


    /*****

    ******/
    PROCEDURE DO_PRORATION(I_PRORATE_WORKSPACE_ID IN IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE)
      IS
      BEGIN

          UPDATE im_prorate_workspace
          SET amount_to_prorate_1 = Decode(amount_to_prorate_1, nULL, 0, amount_to_prorate_1),
              share_in_group_1 = Decode(share_in_group_1, NULL, 1,  0, 1,  share_in_group_1),

              amount_to_prorate_2 = Decode(amount_to_prorate_2, NULL, 0, amount_to_prorate_2),
              share_in_group_2 = Decode(share_in_group_2, NULL, 1,  0, 1,  share_in_group_2),

              amount_to_prorate_3 = Decode(amount_to_prorate_3, NULL, 0, amount_to_prorate_3),
              share_in_group_3 = Decode(share_in_group_3, NULL, 1,  0, 1,  share_in_group_3)
          WHERE prorate_workspace_id = i_prorate_workspace_id;


          MERGE INTO IM_PRORATE_WORKSPACE TGT
          USING
          (
            SELECT PRORATED.PRORATE_WORKSPACE_ID,
                  PRORATED.PRORATE_GROUP_KEY,
                  PRORATED.ROW_ID,
                  CASE
                      WHEN POS_IN_GROUP_1 = TOTAL_POS_IN_GROUP_1
                      THEN PRORATED.PRORATED_AMOUNT_1 +
                          (PRORATED.AMOUNT_TO_PRORATE_1 -
                            sum(PRORATED.PRORATED_AMOUNT_1) over (partition by PRORATED.PRORATE_GROUP_KEY order by PRORATED.SHARE_IN_GROUP_1))
                      ELSE PRORATED_AMOUNT_1
                  END AS PRORATED_AMOUNT_1,

                  CASE
                      WHEN POS_IN_GROUP_2 = TOTAL_POS_IN_GROUP_2
                      THEN PRORATED.PRORATED_AMOUNT_2 +
                          (PRORATED.AMOUNT_TO_PRORATE_2 -
                            sum(PRORATED.PRORATED_AMOUNT_2) over (partition by PRORATED.PRORATE_GROUP_KEY order by PRORATED.SHARE_IN_GROUP_2))
                      ELSE PRORATED_AMOUNT_2
                  END AS PRORATED_AMOUNT_2,

                  CASE
                      WHEN POS_IN_GROUP_3 = TOTAL_POS_IN_GROUP_3
                      THEN PRORATED.PRORATED_AMOUNT_3 +
                          (PRORATED.AMOUNT_TO_PRORATE_3 -
                            sum(PRORATED.PRORATED_AMOUNT_3) over (partition by PRORATED.PRORATE_GROUP_KEY order by PRORATED.SHARE_IN_GROUP_3))
                      ELSE PRORATED_AMOUNT_3
                  END AS PRORATED_AMOUNT_3
            FROM
            (
                SELECT IPW.PRORATE_WORKSPACE_ID,
                      IPW.PRORATE_GROUP_KEY,
                      ROWID AS ROW_ID,

                      Round(IPW.AMOUNT_TO_PRORATE_1, IPW.DECIMAL_SCALE) AS AMOUNT_TO_PRORATE_1,
                      Round(IPW.SHARE_IN_GROUP_1, IPW.DECIMAL_SCALE) AS SHARE_IN_GROUP_1,
                      Count(IPW.SHARE_IN_GROUP_1) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY ORDER BY IPW.SHARE_IN_GROUP_1) POS_IN_GROUP_1,
                      Count(IPW.SHARE_IN_GROUP_1) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY) TOTAL_POS_IN_GROUP_1,
                      Round(IPW.AMOUNT_TO_PRORATE_1
                          * IPW.SHARE_IN_GROUP_1
                          / (Sum(IPW.SHARE_IN_GROUP_1) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY)), IPW.DECIMAL_SCALE) PRORATED_AMOUNT_1,

                      Round(IPW.AMOUNT_TO_PRORATE_2, IPW.DECIMAL_SCALE) AS AMOUNT_TO_PRORATE_2,
                      Round(IPW.SHARE_IN_GROUP_2, IPW.DECIMAL_SCALE) AS SHARE_IN_GROUP_2,
                      Count(IPW.SHARE_IN_GROUP_2) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY ORDER BY IPW.SHARE_IN_GROUP_2) POS_IN_GROUP_2,
                      Count(IPW.SHARE_IN_GROUP_2) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY) TOTAL_POS_IN_GROUP_2,
                      Round(IPW.AMOUNT_TO_PRORATE_2
                          * IPW.SHARE_IN_GROUP_2
                          / (Sum(IPW.SHARE_IN_GROUP_2) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY)), IPW.DECIMAL_SCALE) PRORATED_AMOUNT_2,

                      Round(IPW.AMOUNT_TO_PRORATE_3, IPW.DECIMAL_SCALE) AS AMOUNT_TO_PRORATE_3,
                      Round(IPW.SHARE_IN_GROUP_3, IPW.DECIMAL_SCALE) AS SHARE_IN_GROUP_3,
                      Count(IPW.SHARE_IN_GROUP_3) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY ORDER BY IPW.SHARE_IN_GROUP_3) POS_IN_GROUP_3,
                      Count(IPW.SHARE_IN_GROUP_3) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY) TOTAL_POS_IN_GROUP_3,
                      Round(IPW.AMOUNT_TO_PRORATE_3
                          * IPW.SHARE_IN_GROUP_3
                          / (Sum(IPW.SHARE_IN_GROUP_3) OVER (PARTITION BY IPW.PRORATE_GROUP_KEY)), IPW.DECIMAL_SCALE) PRORATED_AMOUNT_3
                FROM IM_PRORATE_WORKSPACE IPW
                WHERE IPW.PRORATE_WORKSPACE_ID = I_PRORATE_WORKSPACE_ID
                ORDER BY IPW.PRORATE_WORKSPACE_ID, IPW.PRORATE_GROUP_KEY
            ) PRORATED
        ) SOURCE
        ON   (TGT.PRORATE_WORKSPACE_ID = SOURCE.PRORATE_WORKSPACE_ID
              AND TGT.PRORATE_GROUP_KEY = SOURCE.PRORATE_GROUP_KEY
              AND TGT.ROWID = SOURCE.ROW_ID)
        WHEN MATCHED THEN
              UPDATE SET  TGT.RESULT_PRORATED_AMOUNT_1 = SOURCE.PRORATED_AMOUNT_1,
                          TGT.RESULT_PRORATED_AMOUNT_2 = SOURCE.PRORATED_AMOUNT_2,
                          TGT.RESULT_PRORATED_AMOUNT_3 = SOURCE.PRORATED_AMOUNT_3
        WHEN NOT MATCHED THEN
              INSERT (TGT.PRORATE_WORKSPACE_ID, TGT.PRORATE_GROUP_KEY)
              VALUES (SOURCE.PRORATE_WORKSPACE_ID, SOURCE.PRORATE_GROUP_KEY||' ERROR ');
    END DO_PRORATION;


    /*****

    ******/
    PROCEDURE END_PRORATION(I_PRORATE_WORKSPACE_ID IN IM_PRORATE_WORKSPACE.PRORATE_WORKSPACE_ID%TYPE)
    IS
    BEGIN
       DELETE FROM IM_PRORATE_WORKSPACE IPW WHERE IPW.PRORATE_WORKSPACE_ID = I_PRORATE_WORKSPACE_ID;
    END END_PRORATION;


END REIM_PRORATE_SQL;

/

