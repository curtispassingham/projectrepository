CREATE OR REPLACE PACKAGE BODY REIM_VARIANCE_SQL AS
----------------------------------------------------
PROCEDURE GET_VARIANCE_COST_QTY(O_STATUS              OUT VARCHAR2,
                                O_ERROR_MESSAGE       OUT VARCHAR2,
                                VARIANCE_QTY          OUT NUMBER,
                                VARIANCE_UNIT_COST    OUT NUMBER,
                                I_DOC_ID           IN     NUMBER,
                                I_ITEM             IN     VARCHAR2,
                                I_REASON_CODE      IN     VARCHAR2)
IS
   L_DOC_TYPE      IM_DOC_HEAD.TYPE%TYPE;
   L_VARIANCE_TYPE IM_REASON_CODES.REASON_CODE_TYPE%TYPE;
   L_ACTION        IM_REASON_CODES.ACTION%TYPE;

   CURSOR C_GET_DOC_TYPE
   IS
   SELECT TYPE
     FROM IM_DOC_HEAD
    WHERE DOC_ID = I_DOC_ID;

   CURSOR C_GET_RC
   IS
   SELECT REASON_CODE_TYPE,
          ACTION
     FROM IM_REASON_CODES
    WHERE REASON_CODE_ID = I_REASON_CODE;

   CURSOR C_GET_Q_VAR_MRCHI
   IS
   SELECT SUM(IRA.QTY),
          -- get cost for an item
          NVL((SELECT SS.UNIT_COST
                 FROM IM_RCPT_ITEM_POSTING_INVOICE IRIPI,
                      IM_RECEIPT_ITEM_POSTING IRIP,
                      V_IM_SHIPSKU SS
               WHERE  IRIPI.DOC_ID = I_DOC_ID
                 AND IRIP.SEQ_NO = IRIPI.SEQ_NO
                 AND IRIP.ITEM = I_ITEM
                 AND SS.SHIPMENT = IRIP.SHIPMENT
                 AND SS.ITEM = IRIP.ITEM
                 AND ROWNUM < 2),
              -- if item not found on receipt get unit cost from
              -- im_invoice_detail res. adjusted unit cost
              (SELECT IID.RESOLUTION_ADJUSTED_UNIT_COST
                 FROM IM_INVOICE_DETAIL IID
                WHERE IID.DOC_ID = I_DOC_ID
                  AND IID.ITEM = I_ITEM)) UNIT_COST
     INTO VARIANCE_QTY,VARIANCE_UNIT_COST
     FROM IM_RESOLUTION_ACTION IRA
    WHERE IRA.DOC_ID      = I_DOC_ID
      AND IRA.ITEM        = I_ITEM
      AND IRA.REASON_CODE_ID = I_REASON_CODE
    GROUP BY UNIT_COST;

   CURSOR C_GET_C_VAR_MRCHI
   IS
   SELECT IID.QTY,
          IRA.UNIT_COST
     FROM IM_RESOLUTION_ACTION IRA,
          IM_INVOICE_DETAIL IID
    WHERE IRA.DOC_ID      = I_DOC_ID
      AND IRA.ITEM        = I_ITEM
      AND IID.DOC_ID      = I_DOC_ID
      AND IID.ITEM        = I_ITEM
      AND IRA.REASON_CODE_ID = I_REASON_CODE;

   CURSOR C_GET_Q_VAR_OTHERS
   IS
   SELECT SUM(IRA.QTY),
          -- get cost for an item
          (SELECT ADJUSTED_UNIT_COST
             FROM IM_DOC_DETAIL_REASON_CODES
            WHERE DOC_ID IN (SELECT ICM.DOC_ID
                               FROM IM_CN_DETAIL_MATCH_HIS ICM,
                                    IM_DOC_HEAD IDH
                              WHERE ICM.MATCH_ID IN (SELECT MATCH_ID
                                                       FROM IM_CN_DETAIL_MATCH_HIS
                                                      WHERE DOC_ID = I_DOC_ID
                                                        AND ITEM = I_ITEM)
                                AND IDH.TYPE IN ('CRDNRQ','CRDNT')
                                AND ICM.DOC_ID = IDH.DOC_ID)
              AND ROWNUM < 2) UNIT_COST
     FROM IM_RESOLUTION_ACTION IRA
    WHERE IRA.DOC_ID      = I_DOC_ID
      AND IRA.ITEM        = I_ITEM
      AND IRA.REASON_CODE_ID = I_REASON_CODE
    GROUP BY UNIT_COST;

   CURSOR C_GET_C_VAR_OTHERS
   IS
   SELECT IDRC.ORIG_QTY,
          IRA.UNIT_COST
     FROM IM_RESOLUTION_ACTION IRA,
          IM_DOC_DETAIL_REASON_CODES IDRC
    WHERE IRA.DOC_ID      = IDRC.DOC_ID
      AND IRA.ITEM        = IDRC.ITEM
      AND IDRC.DOC_ID     = I_DOC_ID
      AND IDRC.ITEM       = I_ITEM
      AND IRA.REASON_CODE_ID = I_REASON_CODE;

BEGIN

   OPEN C_GET_DOC_TYPE;
   FETCH C_GET_DOC_TYPE INTO L_DOC_TYPE;
   CLOSE C_GET_DOC_TYPE;

   OPEN C_GET_RC;
   FETCH C_GET_RC INTO L_VARIANCE_TYPE,
                       L_ACTION;
   CLOSE C_GET_RC;

    IF L_DOC_TYPE IN ('MRCHI') THEN

      IF L_VARIANCE_TYPE = 'Q' THEN

         OPEN C_GET_Q_VAR_MRCHI;
         FETCH C_GET_Q_VAR_MRCHI INTO VARIANCE_QTY,
                                      VARIANCE_UNIT_COST;
         CLOSE C_GET_Q_VAR_MRCHI;

      ELSE

         OPEN C_GET_C_VAR_MRCHI;
         FETCH C_GET_C_VAR_MRCHI INTO VARIANCE_QTY,
                                      VARIANCE_UNIT_COST;
         CLOSE C_GET_C_VAR_MRCHI;

      END IF;

    ELSE

      IF L_VARIANCE_TYPE = 'Q' THEN

         OPEN C_GET_Q_VAR_OTHERS;
         FETCH C_GET_Q_VAR_OTHERS INTO VARIANCE_QTY,
                                       VARIANCE_UNIT_COST;
         CLOSE C_GET_Q_VAR_OTHERS;

      ELSE

         OPEN C_GET_C_VAR_OTHERS;
         FETCH C_GET_C_VAR_OTHERS INTO VARIANCE_QTY,
                                       VARIANCE_UNIT_COST;
         CLOSE C_GET_C_VAR_OTHERS;

      END IF;

    END IF;


    -- Take care of cost variance sign..
    IF L_ACTION IN ('CBQ', 'CBC', 'DMTI', 'DMTF') THEN
        -- cns and dbmemos always have negative cost
        IF VARIANCE_UNIT_COST > 0 THEN
            VARIANCE_UNIT_COST := VARIANCE_UNIT_COST * -1;
        END IF;
    ELSE -- invoices will always have positive cost
        IF VARIANCE_UNIT_COST < 0 THEN
            VARIANCE_UNIT_COST := VARIANCE_UNIT_COST * -1;
        END IF;
    END IF;

    -- Take care of qty variance sign.. should always be POSITIVE
    IF variance_qty < 0 THEN
        variance_qty := variance_qty * -1;
    END IF;



  END GET_VARIANCE_COST_QTY;
END REIM_VARIANCE_SQL;
/
