CREATE OR REPLACE PACKAGE BODY REIM_PKG_UTIL_SQL AS
---------------------------------------------------------------------------------
/***********************************************\
* Retrieves tokens from a standard delimited string.
* Example: GET_TOKEN('A::B:C', ':', 1) returns 'A'
* GET_TOKEN('A::B:C', ':', 2) returns ''
* GET_TOKEN('A::B:C', ':', 3) returns 'B'
***********************************************/
FUNCTION GET_TOKEN(I_STRING    VARCHAR2,
                   I_DELIM     VARCHAR2,
                   I_TOKEN_NUM NUMBER)
RETURN VARCHAR2
IS
   START_POS NUMBER;
   END_POS NUMBER;
BEGIN

   IF I_TOKEN_NUM = 1 THEN
      START_POS := 1;
   ELSE
      START_POS := INSTR(I_STRING, I_DELIM, 1, I_TOKEN_NUM - 1) ;
      IF START_POS = 0 THEN
         RETURN NULL;
      ELSE
         START_POS := START_POS + LENGTH(I_DELIM) ;
      END IF;
   END IF;
   
   END_POS := INSTR(I_STRING, I_DELIM, START_POS, 1) ;
   IF END_POS = 0 THEN
      RETURN SUBSTR(I_STRING, START_POS) ;
   ELSE
      RETURN SUBSTR(I_STRING, START_POS, END_POS - START_POS) ;
   END IF;

EXCEPTION
   WHEN OTHERS THEN
      RETURN NULL;

END GET_TOKEN;
---------------------------------------------------------------------------------
PROCEDURE LOG_TIME(I_log_message    IN VARCHAR2,
                   I_start_time     IN TIMESTAMP)
IS
   L_end_time              TIMESTAMP;
   L_hours                 varchar2(50);
   L_mins                  varchar2(50);
   L_seconds               varchar2(50);
   L_total_milliseconds    varchar2(50);

BEGIN
   L_end_time := SYSTIMESTAMP;
   select extract( hour from diff ) hours,
          extract( minute from diff ) minutes,
          extract( second from diff ) seconds,
          --
          extract( day from diff )*24*60*60*1000 +
             extract( hour from diff )*60*60*1000 +
             extract( minute from diff )*60*1000 +
             round(extract( second from diff )*1000) total_milliseconds
     into L_hours,
          L_mins,
          L_seconds,
          L_total_milliseconds
     from (select L_end_time-I_start_time diff from dual);
   --LOGGER.LOG_INFORMATION(I_log_message||' - hours:'||L_hours||' minuets:'||L_mins||' seconds:'||L_seconds||' total mili:'||L_total_milliseconds);
   LOGGER.LOG_INFORMATION(I_log_message||'~TIMING~'||L_hours||'~'||L_mins||'~'||L_seconds||'~'||L_total_milliseconds);
END;
---------------------------------------------------------------------------------
END REIM_PKG_UTIL_SQL; 
/
