CREATE OR REPLACE PACKAGE REIM_TAX_RESOLUTION AS
--------------------------------------------------------------------
FUNCTION RESOLVE(O_error_message    IN OUT VARCHAR2,
                 I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE,
                 I_resolution_side  IN     VARCHAR2,
                 I_reason_code_id   IN     IM_RESOLUTION_ACTION.REASON_CODE_ID%TYPE,
                 I_comment          IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
END REIM_TAX_RESOLUTION;
/
