CREATE OR REPLACE PACKAGE SHIPMENT_API_SQL AS

---------------------------------------------------------------------------------
-- Function Name: UPDATE_INVOICE_MATCH_STATUS
-- Purpose:       This function serves as an API to allow
--                external systems to update the invoice matching
--                status for a shipment.
----------------------------------------------------------------------------------
FUNCTION UPDATE_INVOICE_MATCH_STATUS(O_error_message OUT VARCHAR2,
                                     I_shipments     IN  SHIPMENT_IDS,
                                     I_new_status    IN  VARCHAR2)
	RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: UPDATE_INVOICE_QTY_MATCHED 
-- Purpose:       This function serves as an API to allow
--                external systems to update the invoice qty
--                matched for a shipment.
----------------------------------------------------------------------------------
FUNCTION UPDATE_INVOICE_QTY_MATCHED(O_error_message OUT VARCHAR2,
                                     I_shipment_item_qtys     IN  SHIPMENT_MATCHED_QTY_ARRAY)
	RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION UPDATE_INVOICE_MATCH_STATUS(O_error_message OUT VARCHAR2,
                                     I_shipment      IN  NUMBER,
                                     I_new_status    IN  VARCHAR2,
                                     I_vdate         IN  DATE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CLEAR_STAGED_SHIPMENTS
-- Purpose      : This function removes all data from the stage_purged_shipments
--                and the stage_purged_shipksus tables other than the shipment ids
--                specified.
----------------------------------------------------------------------------------
FUNCTION CLEAR_STAGED_SHIPMENTS(O_error_message OUT VARCHAR2,
                                I_shipments     IN  SHIPMENT_IDS)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION UPDATE_INVOICE_QTY_MATCHED(O_error_message         IN OUT VARCHAR2,
                                    I_im_ship_item_qtys_tbl IN     IM_SHIP_ITEM_QTYS_TBL)
RETURN NUMBER;
----------------------------------------------------------------------------------
/**
 * The public function used to deduct matched qty in Shipment during unmatching
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION DEDUCT_INVOICE_QTY_MATCHED(O_error_message            OUT VARCHAR2,
                                    I_im_ship_item_qtys_tbl IN     IM_SHIP_ITEM_QTYS_TBL)
RETURN NUMBER;
----------------------------------------------------------------------------------
END SHIPMENT_API_SQL;
/
