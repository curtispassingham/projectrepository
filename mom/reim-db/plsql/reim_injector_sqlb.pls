CREATE OR REPLACE PACKAGE BODY REIM_INJECTOR_SQL AS

    LP_vdate DATE := GET_VDATE;
    LP_username VARCHAR2(30) := NULL;

    LP_inject_doc_ids OBJ_NUMERIC_ID_TABLE := NULL;

    --System Options
    LP_consolidation_ind  SYSTEM_CONFIG_OPTIONS.CONSOLIDATION_IND%TYPE  := NULL;

    --IM System options
    LP_post_dated_doc_days IM_SYSTEM_OPTIONS.POST_DATED_DOC_DAYS%TYPE := NULL;
    LP_num_tax_allow IM_SYSTEM_OPTIONS.NUM_TAX_ALLOW%TYPE := NULL;
    LP_tax_validation_type IM_SYSTEM_OPTIONS.TAX_VALIDATION_TYPE%TYPE := NULL;
    LP_dflt_cost_underbill_rc IM_SYSTEM_OPTIONS.DFLT_COST_UNDERBILL_RC%TYPE := NULL;
    LP_calc_tolerance_ind IM_SYSTEM_OPTIONS.CALC_TOLERANCE_IND%TYPE := NULL;
    LP_calc_tolerance IM_SYSTEM_OPTIONS.CALC_TOLERANCE%TYPE := NULL;

    --Injector Options
    LP_default_loc          NUMBER(10)  := NULL;
    LP_default_dept         NUMBER(4)   := NULL;
    LP_default_class        NUMBER(4)   := NULL;
    LP_inc_date_for_dup_chk VARCHAR2(1) := NULL;
    LP_inc_year_for_dup_chk VARCHAR2(1) := NULL;
    LP_allow_bfr_post_dated_days VARCHAR2(1) := NULL;

    --Reason Codes
    LP_rtv_reason_code IM_REASON_CODES.REASON_CODE_ID%TYPE := NULL;
    LP_cmc_reason_code IM_REASON_CODES.REASON_CODE_ID%TYPE := NULL;

    /**
     *  -------- PRIVATE PROCS AND FUNCTIONS SPECS --------------
     */

    FUNCTION INIT_INJ_DOCS(O_error_message    OUT VARCHAR2,
                           I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE,
                           I_thread_id     IN     IM_INJECT_DOC_HEAD.THREAD_ID%TYPE,
                           I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                           I_process       IN     NUMBER)
    RETURN NUMBER;

    FUNCTION SET_GLOBALS(O_error_message    OUT VARCHAR2)
    RETURN NUMBER;

    FUNCTION DERIVE_ITEM(O_error_message    OUT VARCHAR2,
                         I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
    RETURN NUMBER;

    FUNCTION SET_REQUIRED_FIELDS(O_error_message    OUT VARCHAR2)
    RETURN NUMBER;

    FUNCTION COMPUTE_EXCH_RATE(O_error_message    OUT VARCHAR2)
    RETURN NUMBER;

    FUNCTION HANDLE_DUPS_ON_CONSIGNMENTS(O_error_message    OUT VARCHAR2)
    RETURN NUMBER;

    FUNCTION UPDATE_FIXABLE_IND(O_error_message    OUT VARCHAR2)
    RETURN NUMBER;

    FUNCTION VALIDATE_TRAN_TAX(O_error_message    OUT VARCHAR2,
                               I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
    RETURN NUMBER;

    FUNCTION UPDATE_REVERSE_VAT(O_error_message    OUT VARCHAR2,
                                I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
    RETURN NUMBER;

    FUNCTION VALIDATE_DUP_VEND_DOC_NUM(O_error_message    OUT VARCHAR2,
                                       I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE,
                                       I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
    RETURN NUMBER;

    /**
     *  --------- PUBLIC PROCS AND FUNCTIONS BODIES --------------
     */

    /**
     * The public function used for assigning threads to transactions
     * Input param: I_inject_id (Unique identifier for the injector process)
     *
     * Output param: O_num_chunks (Number of chunks the data gets divided into)
     *
     * Returns 1 on Success
     *         0 on Failure
     */
FUNCTION INIT_THREADS(O_error_message    OUT VARCHAR2,
                      O_num_chunks       OUT NUMBER,
                      I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.INIT_THREADS';

   L_doc_count NUMBER                                  := NULL;
   L_luw       IM_SYSTEM_OPTIONS.LUW_EDI_INJECTOR%TYPE := NULL;

   cursor C_FETCH_DOC_COUNT is
   select count(1)
     from im_inject_doc_head iidh
    where iidh.inject_id = I_inject_id;

   cursor C_FETCH_LUW is
   select luw_edi_injector
     from im_system_options;

BEGIN

   if (I_inject_id is NULL) then
      -- TODO - Replace SQL_LIB calls with logger calls
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            'Inject Id must be provided',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;
   end if;

   open C_FETCH_LUW;
   fetch C_FETCH_LUW into L_luw;
   close C_FETCH_LUW;

   if (L_luw = REIM_CONSTANTS.ZERO) then

      update im_inject_doc_head iidh
         set thread_id = REIM_CONSTANTS.ONE
       where iidh.inject_id = I_inject_id;

      O_num_chunks := REIM_CONSTANTS.ONE;

   else

      open C_FETCH_DOC_COUNT;
      fetch C_FETCH_DOC_COUNT into L_doc_count;
      close C_FETCH_DOC_COUNT;

      O_num_chunks := CEIL(L_doc_count/L_luw);

      merge into im_inject_doc_head tgt
      using (select iidh.inject_doc_id,
                    CEIL(rownum / L_luw) thread_id
               from im_inject_doc_head iidh
              where iidh.inject_id = I_inject_id) src
      on (tgt.inject_doc_id = src.inject_doc_id)
      when MATCHED then
         update
            set tgt.thread_id = src.thread_id;

   end if;

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      -- TODO - Replace SQL_LIB calls with logger calls
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;

END INIT_THREADS;

    /**
     * The public function used for validation of transactions
     * called from the process which tries to inject transactions
     * Input param: I_inject_id (Unique identifier for the injector process)
     *              I_thread_id (Thread Id, used when multi-threaded)
     *              I_inject_doc_id (Id of the Transaction that needs to be validated)
     *              I_username  (Username from ReimUserContext)
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION VALIDATE_TRANSACTIONS(O_error_message    OUT VARCHAR2,
                                   I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE,
                                   I_thread_id     IN     IM_INJECT_DOC_HEAD.THREAD_ID%TYPE,
                                   I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                   I_username      IN     VARCHAR2)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.VALIDATE_TRANSACTIONS';
        O_status  NUMBER(1) := REIM_CONSTANTS.SUCCESS;
        L_trial_id im_inject_doc_error.trial_id%TYPE;

    BEGIN

        L_trial_id := im_trial_id_seq.nextval;

        -- Validate the Input parameters and initialize the global collection with the set of inject_doc_ids based on input.

        O_status := INIT_INJ_DOCS(O_error_message,
                                  I_inject_id,
                                  I_thread_id,
                                  I_inject_doc_id,
                                  PROCESS_VALIDATION);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        -- Set all required global variables.
        O_status := SET_GLOBALS(O_error_message);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        LP_username := I_username;

        -- Set required fields which are NULL.
        O_status := SET_REQUIRED_FIELDS(O_error_message);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        -- Duplicate Header Validation (performed across threads documents and repeated in each individual thread)
        if (I_inject_id is not NULL) then

            O_status := VALIDATE_DUP_VEND_DOC_NUM(O_error_message, I_inject_id, L_trial_id);

            if (O_status <> REIM_CONSTANTS.SUCCESS) then
                return O_status;
            end if;

        end if;

        -- Inject Header Validations
        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidh_inner.inject_id,
                   iidh_inner.inject_doc_id,
                   iidh_inner.rule,
                   L_trial_id,
                   DECODE(iidh_inner.rule,
                          DUP_VEND_DOC_NUM, 'Vendor Document Number = ' || iidh_inner.ext_doc_id || ' for vendor = ' || NVL(iidh_inner.ref_vendor, iidh_inner.vendor),
                          VALID_DUE_DATE, 'Due Date = ' || iidh_inner.due_date,
                          VALID_CURR_CODE, 'Currency Code = ' || iidh_inner.currency_code,
                          VALID_CROSS_REF_DOC, 'Cross Reference Document ID = ' || iidh_inner.cross_ref_doc,
                          VALID_ORDER, 'Order Number = ' || iidh_inner.order_no,
                          VALID_ORD_CURR, 'Currency Code = ' || iidh_inner.currency_code,
                          VALID_SUPPLIER,'Invalid Supplier ='||iidh_inner.vendor,
                          VALID_SUPPLIER_SITE,'Supplier Site ='||iidh_inner.vendor,
                          VALID_FR8_TYPE, 'Freight Type = ' || iidh_inner.freight_type,
                          VALID_COMP_DEAL_DETAIL, 'Deal detail id = ' || iidh_inner.deal_detail_id,
                          SUPP_OPTION_UNDEFINED, 'Supplier = ' || iidh_inner.vendor,
                          VALID_TERMS, 'Terms = ' || iidh_inner.terms,
                          VALID_DOC_DATE, 'Document date (In format YYYYMMDD) = ' || to_char(iidh_inner.doc_date,'YYYYMMDD'),
                          VALID_STORE, 'Store = ' || iidh_inner.location,
                          VALID_WH, 'Wh = ' || iidh_inner.location,
                          CRDNT_NO_DETAIL, NULL, -- to be removed once finalised
                          TAX_NOT_ALLOWED, NULL, -- to be removed once finalised
                          VALID_ORD_LOC, 'Location = ' || iidh_inner.location,
                          VALID_RTV, 'RTV Order Number = ' || iidh_inner.rtv_order_no,
                          VALID_RTV_LOC, 'RTV Order Number = ' || iidh_inner.rtv_order_no || ' and Location = ' || iidh_inner.location,
                          VALID_ORD_VEND, 'Order ID = ' || iidh_inner.order_no || ' and Supplier = ' || iidh_inner.vendor,
                          VALID_RTV_VEND, 'RTV = ' || iidh_inner.rtv_order_no || ' and Supplier = ' || iidh_inner.vendor,
                          DOCTYPE_COST_INC_TAX_SIGN, 'Document Type = ' || iidh_inner.doc_type || ' Total Cost Including Tax Sign = ' || DECODE(sign(iidh_inner.total_cost_inc_tax),
                                                                                                                                                REIM_CONSTANTS.SIGN_POSITIVE, 'Positive',
                                                                                                                                                REIM_CONSTANTS.SIGN_NEGATIVE, 'Negative',
                                                                                                                                                REIM_CONSTANTS.SIGN_ZERO, 'Zero'),
                          NULL) error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (select iidh.*, -- to be replaced by individual columns
                           case
                                when LP_allow_bfr_post_dated_days = REIM_CONSTANTS.YN_NO and iidh.doc_date < (LP_vdate - LP_post_dated_doc_days) then
                                     VALID_DOC_DATE
                                when exists (select 'x' from im_doc_head idh
                                              where idh.vendor     = NVL(iidh.ref_vendor, iidh.vendor)
                                                and idh.type       = iidh.doc_type
                                                and idh.status     <> REIM_CONSTANTS.DOC_STATUS_DELETE
                                                and idh.ext_doc_id = GET_EXT_DOC_ID_WITH_PRFX(iidh.ext_doc_id, iidh.doc_type)
                                                and idh.doc_date   = decode(LP_inc_date_for_dup_chk,
                                                                            REIM_CONSTANTS.YN_YES, iidh.doc_date,
                                                                            idh.doc_date)
                                                and to_char(idh.doc_date,'YYYY') = decode(LP_inc_year_for_dup_chk,
                                                                                          REIM_CONSTANTS.YN_YES, to_char(iidh.doc_date,'YYYY'),
                                                                                          to_char(idh.doc_date,'YYYY'))) then
                                     DUP_VEND_DOC_NUM
                                when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                 and iidh.total_cost = REIM_CONSTANTS.ZERO and not exists (select 'x'
                                                                                             from im_inject_doc_detail iidd
                                                                                            where iidd.inject_doc_id = iidh.inject_doc_id) then
                                     ZERO_MRCHI_DTL_REQ
                                when iidh.due_date is NOT NULL and iidh.due_date < iidh.doc_date then
                                     VALID_DUE_DATE
                                when not exists (select 'x'
                                                   from currencies
                                                  where currency_code = iidh.currency_code) then
                                     VALID_CURR_CODE
                                when iidh.cross_ref_doc is NOT NULL and not exists (select 'x'
                                                                                      from im_doc_head
                                                                                     where doc_id = iidh.cross_ref_doc
                                                                                       and status <> REIM_CONSTANTS.DOC_STATUS_DELETE) then
                                     VALID_CROSS_REF_DOC
                                when iidh.vendor_type = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER and not exists (select 'x'
                                                                                                              from sups s
                                                                                                             where s.supplier = iidh.vendor) then
                                     VALID_SUPPLIER
                                when iidh.vendor_type = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER and iidh.ref_vendor is NULL then
                                     VALID_SUPPLIER_SITE
                                when iidh.vendor_type <> REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER and not exists (select 'x'
                                                                                                               from partner p
                                                                                                              where p.partner_id = iidh.vendor) then
                                     VALID_PARTNER
                                when iidh.order_no is NOT NULL
                                 and NOT EXISTS (select 'x'
                                                   from ordhead oh
                                                  where oh.order_no = iidh.order_no) then
                                     VALID_ORDER
                                when iidh.order_no is NOT NULL
                                 and NOT EXISTS (select 'x'
                                                   from ordhead
                                                  where order_no      = iidh.order_no
                                                    and currency_code = iidh.currency_code) then
                                     VALID_ORD_CURR
                                when iidh.freight_type is NOT NULL and not exists (select 'x'
                                                                                     from code_detail cd
                                                                                    where cd.code_type = REIM_CONSTANTS.CODE_TYPE_FREIGHT
                                                                                      and cd.code      = iidh.freight_type) then
                                     VALID_FR8_TYPE
                                when iidh.deal_detail_id is NOT NULL and not exists (select 'x'
                                                                                     from deal_detail dd
                                                                                    where dd.deal_id = iidh.deal_id
                                                                                      and dd.deal_detail_id = iidh.deal_detail_id) then
                                     VALID_COMP_DEAL_DETAIL
                                when iidh.vendor_type = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER and not exists (select 'x'
                                                                                                              from v_im_supp_site_attrib_expl iso
                                                                                                             where iso.supplier = iidh.vendor) then
                                     SUPP_OPTION_UNDEFINED
                                when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                 and NVL(iidh.total_qty, 0) = 0


                                 and NVL((select total_qty_required_ind
                                            from im_supplier_groups isg,
                                                 im_supplier_group_members isgm
                                           where isgm.supplier = NVL(iidh.ref_vendor, iidh.vendor)
                                             and isg.group_id  = isgm.group_id
                                             and rownum        < 2), REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_YES then
                                     TOTAL_HDR_QTY_REQ
                                when iidh.terms is NOT NULL and not exists (select 'x'
                                                                              from terms t
                                                                             where t.terms = iidh.terms
                                                                               and t.enabled_flag = REIM_CONSTANTS.YN_YES
                                                                               and iidh.doc_date  between nvl(t.start_date_active, iidh.doc_date) and nvl(t.end_date_active, iidh.doc_date)) then
                                     VALID_TERMS
                                when iidh.loc_type = REIM_CONSTANTS.LOC_TYPE_STORE and not exists (select 'x'
                                                                                                     from store s
                                                                                                    where s.store = iidh.location) then
                                     VALID_STORE
                                when iidh.loc_type = REIM_CONSTANTS.LOC_TYPE_WH and not exists (select 'x'
                                                                                                  from wh wh
                                                                                                 where wh.wh = iidh.location) then
                                     VALID_WH
                                when LP_num_tax_allow = REIM_CONSTANTS.NUM_TAX_ALLOW_NONE and exists (select 'x'
                                                                                                       from im_inject_doc_tax iidt
                                                                                                      where iidt.inject_doc_id = iidh.inject_doc_id) then
                                     TAX_NOT_ALLOWED
                                when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_CRDNT
                                 and iidh.vendor_type NOT IN (REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER,
                                                              REIM_CONSTANTS.VENDOR_TYPE_MANUFACTURER,
                                                              REIM_CONSTANTS.VENDOR_TYPE_DISTRIBUTOR,
                                                              REIM_CONSTANTS.VENDOR_TYPE_WHOLESALER) and exists (select 'x'
                                                                                                                   from im_inject_doc_detail iidd
                                                                                                                  where iidd.inject_doc_id = iidh.inject_doc_id) then
                                     CRDNT_NO_DETAIL
                                when iidh.doc_type IN (REIM_CONSTANTS.DOC_TYPE_DEBMEQ,
                                                       REIM_CONSTANTS.DOC_TYPE_CRDNRQ) and LP_rtv_reason_code is NULL then
                                     RTV_REASON_CODE
                                when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_CRDMEC
                                 and LP_dflt_cost_underbill_rc is NULL and LP_cmc_reason_code is NULL then
                                     CMC_REASON_CODE
                                when iidh.vendor_type = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                                 and iidh.order_no is NOT NULL
                                 and NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) != REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT
                                 and NVL(iidh.rtv_ind, REIM_CONSTANTS.YN_NO ) = REIM_CONSTANTS.YN_NO and not exists (select 'x'
                                                   from sups s
                                                  where s.supplier = iidh.vendor
                                                    and s.final_dest_ind = REIM_CONSTANTS.YN_YES)
                                 and not exists (select 'x'
                                                   from ordhead oh
                                                  where oh.order_no    = iidh.order_no
                                                    and oh.import_type = REIM_CONSTANTS.IMPORT_TYPE_STORE
                                                    and oh.import_id   = iidh.location)
                                 and not exists (select 'x'
                                                   from v_im_ordloc ol
                                                  where iidh.loc_type = REIM_CONSTANTS.LOC_TYPE_STORE
                                                    and ol.order_no   = iidh.order_no
                                                    and ol.loc_type   = iidh.loc_type
                                                    and ol.location   = iidh.location)
                                 and not exists (select 'x'
                                                   from v_im_ordloc ol,
                                                        wh wh
                                                  where iidh.loc_type  = REIM_CONSTANTS.LOC_TYPE_WH
                                                    and ol.order_no    = iidh.order_no
                                                    and ol.loc_type    = iidh.loc_type
                                                    and wh.wh          = ol.location
                                                    and wh.physical_wh = iidh.location) then
                                     VALID_ORD_LOC
                                when NVL(iidh.rtv_ind, REIM_CONSTANTS.YN_NO ) = REIM_CONSTANTS.YN_YES and not exists (select 'x'
                                                                                                                        from rtv_head rh
                                                                                                                       where rh.rtv_order_no = iidh.rtv_order_no) then
                                     VALID_RTV
                                when NVL(iidh.rtv_ind, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_YES and not exists (select 'x'
                                                                                                                       from rtv_head rh,
                                                                                                                            store s
                                                                                                                      where iidh.loc_type   = REIM_CONSTANTS.LOC_TYPE_STORE
                                                                                                                        and rh.rtv_order_no = iidh.rtv_order_no
                                                                                                                        and s.store         = iidh.location
                                                                                                                        and rh.store        = s.store
                                                                                                                     union all
                                                                                                                     select 'x'
                                                                                                                       from rtv_head rh,
                                                                                                                            wh wh
                                                                                                                      where iidh.loc_type   = REIM_CONSTANTS.LOC_TYPE_WH
                                                                                                                        and rh.rtv_order_no = iidh.rtv_order_no
                                                                                                                        and wh.wh           = iidh.location
                                                                                                                        and rh.wh           = wh.wh) then
                                     VALID_RTV_LOC
                                when iidh.order_no is NOT NULL
                                 and NVL(iidh.rtv_ind, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_NO and not exists (select 'x'
                                                                                 from ordhead oh
                                                                                where oh.order_no = iidh.order_no
                                                                                  and oh.supplier IN (iidh.vendor,
                                                                                                      NVL(iidh.ref_vendor, iidh.vendor)))
                                                               and not exists (select 'x'
                                                                                 from ordhead oh,
                                                                                      sups s,
                                                                                      im_supplier_group_members isgm_iidh,
                                                                                      im_supplier_group_members isgm_ord
                                                                                where oh.order_no = iidh.order_no
                                                                                  and s.supplier  = oh.supplier
                                                                                  and isgm_iidh.supplier = NVL(iidh.ref_vendor, iidh.vendor)
                                                                                  and isgm_ord.supplier = NVL(s.supplier_parent, s.supplier)
                                                                                  and isgm_ord.group_id = isgm_iidh.group_id) then
                                     VALID_ORD_VEND
                                when NVL(iidh.rtv_ind, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_YES and not exists (select 'x'
                                                                                                                       from rtv_head rh
                                                                                                                      where rh.rtv_order_no = iidh.rtv_order_no
                                                                                                                        and rh.supplier     IN (iidh.vendor,
                                                                                                                                                NVL(iidh.ref_vendor, iidh.vendor)))
                                                                                                     and not exists (select 'x'
                                                                                                                       from rtv_head rh,
                                                                                                                            sups s,
                                                                                                                            im_supplier_group_members isgm_iidh,
                                                                                                                            im_supplier_group_members isgm_ord
                                                                                                                      where rh.rtv_order_no = iidh.rtv_order_no
                                                                                                                        and s.supplier      = rh.supplier
                                                                                                                        and isgm_iidh.supplier = NVL(iidh.ref_vendor, iidh.vendor)
                                                                                                                        and isgm_ord.supplier = NVL(s.supplier_parent, s.supplier)
                                                                                                                        and isgm_ord.group_id = isgm_iidh.group_id) then
                                     VALID_RTV_VEND
                                when (iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                      and sign(iidh.total_cost_incl_tax) not in (REIM_CONSTANTS.SIGN_POSITIVE,
                                                                                REIM_CONSTANTS.SIGN_ZERO))
                                  or (iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_NMRCHI
                                      and sign(iidh.total_cost_incl_tax) not in (REIM_CONSTANTS.SIGN_POSITIVE,
                                                                                REIM_CONSTANTS.SIGN_NEGATIVE))
                                  or (iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_CRDNT
                                      and sign(iidh.total_cost_incl_tax) != REIM_CONSTANTS.SIGN_NEGATIVE)
                                  or (iidh.doc_type in (REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                                                        REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                                                        REIM_CONSTANTS.DOC_TYPE_CRDNRT)
                                      and sign(iidh.total_cost_incl_tax) != REIM_CONSTANTS.SIGN_NEGATIVE)
                                  or (iidh.doc_type in (REIM_CONSTANTS.DOC_TYPE_DEBMEC,
                                                        REIM_CONSTANTS.DOC_TYPE_DEBMEQ,
                                                        REIM_CONSTANTS.DOC_TYPE_DEBMET)
                                      and sign(iidh.total_cost_incl_tax) != REIM_CONSTANTS.SIGN_NEGATIVE)
                                  or (iidh.doc_type in (REIM_CONSTANTS.DOC_TYPE_CRDMEC,
                                                        REIM_CONSTANTS.DOC_TYPE_CRDMEQ)
                                      and sign(iidh.total_cost_incl_tax) != REIM_CONSTANTS.SIGN_POSITIVE) then
                                     DOCTYPE_COST_INC_TAX_SIGN
                           end as rule
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           (select iidh_ext.*, -- to be replaced by individual columns
                                   round((iidh_ext.total_cost + NVL(iidh_ext.total_tax_amount, REIM_CONSTANTS.ZERO)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) total_cost_incl_tax
                              from im_inject_doc_head iidh_ext) iidh
                     where iidh.inject_doc_id = value(ids)
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidh.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) iidh_inner,
                   im_inject_doc_rule iidr
             where iidh_inner.rule = iidr.rule;

        -- Compute Exchange Rate when its NULL. Do this after validating Order.
        O_status := COMPUTE_EXCH_RATE(O_error_message);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        -- Cleanup duplicate items on consignment invoices
        O_status := HANDLE_DUPS_ON_CONSIGNMENTS(O_error_message);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        -- Duplicate Details validation
        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidd_inner.inject_id,
                   iidd_inner.inject_doc_id,
                   iidd_inner.rule,
                   L_trial_id,
                   DECODE(iidd_inner.rule,
                          DUP_ITEM, 'Item Id = ' || iidd_inner.item,
                          DUP_VPN, 'VPN = ' || iidd_inner.item,
                          DUP_UPC, 'UPC = ' || iidd_inner.item,
                          NULL) error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (select iidd.inject_id,
                           iidd.inject_doc_id,
                           iidd.item item,
                           DUP_ITEM rule
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_detail iidd
                     where iidd.inject_doc_id = value(ids)
                       and iidd.item_source   = REIM_CONSTANTS.ITEM_SRC_ITEM_ID
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidd.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                      group by iidd.inject_id,
                               iidd.inject_doc_id,
                               iidd.item
                     having count(1) > 1
                    union all
                    select iidd.inject_id,
                           iidd.inject_doc_id,
                           iidd.vpn item,
                           DUP_VPN rule
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_detail iidd
                     where iidd.inject_doc_id = value(ids)
                       and iidd.item_source   = REIM_CONSTANTS.ITEM_SRC_VPN
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidd.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                      group by iidd.inject_id,
                               iidd.inject_doc_id,
                               iidd.vpn
                     having count(1) > 1
                    union all
                    select iidd.inject_id,
                           iidd.inject_doc_id,
                           iidd.upc item,
                           DUP_UPC rule
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_detail iidd
                     where iidd.inject_doc_id = value(ids)
                       and iidd.item_source   = REIM_CONSTANTS.ITEM_SRC_UPC
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidd.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                      group by iidd.inject_id,
                               iidd.inject_doc_id,
                               iidd.upc
                     having count(1) > 1) iidd_inner,
                   im_inject_doc_rule iidr
             where iidd_inner.rule = iidr.rule;

        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        detail_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidd_inner.inject_id,
                   iidd_inner.inject_doc_id,
                   iidd_inner.detail_id,
                   iidd_inner.rule,
                   L_trial_id,
                   DECODE(iidd_inner.rule,
                          VALID_ITEM, 'Item Id = ' || iidd_inner.item,
                          VALID_ITEM_SUPPLIER, 'Item Id = ' || iidd_inner.item || ' , Supplier = ' || NVL(iidd_inner.ref_vendor, iidd_inner.vendor),
                          VALID_ORDER_ITEM, 'Item Id = ' || iidd_inner.item || ' , Order = ' || iidd_inner.order_no,
                          VALID_RTV_ORDER_ITEM, 'Item Id = ' || iidd_inner.item || ' , RTV Order = ' || iidd_inner.rtv_order_no,
                          VALID_VPN_SUPPLIER, 'VPN = ' || iidd_inner.vpn,
                          DUP_VPN_SUPPLIER, 'VPN = ' || iidd_inner.vpn,
                          VALID_UPC,  'UPC = ' || iidd_inner.upc,
                          VALID_VPN_SUPPLIER, 'UPC = ' || iidd_inner.upc,
                          NULL) error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (select iidd.inject_id,
                           iidd.inject_doc_id,
                           iidd.detail_id,
                           iidd.item item,
                           iidd.vpn,
                           iidd.upc,
                           iidh.ref_vendor,
                           iidh.vendor,
                           iidh.order_no,
                           iidh.rtv_order_no,
                           case
                                when iidd.item_source = REIM_CONSTANTS.ITEM_SRC_ITEM_ID and not exists (select 'x'
                                                                                                          from item_master im
                                                                                                         where im.item = iidd.item
                                                                                                           and im.tran_level = im.item_level) then
                                VALID_ITEM
                                when iidd.item_source = REIM_CONSTANTS.ITEM_SRC_ITEM_ID and not exists (select 'x'
                                                                                                          from item_supplier its
                                                                                                         where its.item = iidd.item
                                                                                                           and its.supplier IN (select s.supplier supplier
                                                                                                                                  from sups s
                                                                                                                                 where (s.supplier_parent = NVL(iidh.ref_vendor, iidh.vendor)
                                                                                                                                        or s.supplier     = NVL(iidh.ref_vendor, iidh.vendor))
                                                                                                                                union all
                                                                                                                                select s.supplier supplier
                                                                                                                                  from sups s,
                                                                                                                                       im_supplier_group_members isgm_iidh,
                                                                                                                                       im_supplier_group_members isgm_its
                                                                                                                                 where isgm_iidh.supplier = NVL(iidh.ref_vendor, iidh.vendor)
                                                                                                                                   and isgm_iidh.group_id    = isgm_its.group_id
                                                                                                                                   and its.supplier          = s.supplier
                                                                                                                                   and (s.supplier_parent    = isgm_its.supplier
                                                                                                                                    or s.supplier            = isgm_its.supplier))) then
                                VALID_ITEM_SUPPLIER
                                when iidd.item_source                           = REIM_CONSTANTS.ITEM_SRC_ITEM_ID
                                 and NVL(iidh.rtv_ind, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_NO
                                 and NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) != REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT
                                 and iidh.order_no is NOT NULL and not exists (select 'x'
                                                                                 from ordsku os
                                                                                where os.order_no = iidh.order_no
                                                                                  and os.item     = iidd.item) then
                                VALID_ORDER_ITEM
                                when iidd.item_source = REIM_CONSTANTS.ITEM_SRC_ITEM_ID
                                 and NVL(iidh.rtv_ind, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_YES
                                 and NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) != REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT
                                 and not exists (select 'x'
                                                   from rtv_detail rd
                                                  where rd.rtv_order_no = iidh.rtv_order_no
                                                    and rd.item         = iidd.item) then
                                VALID_RTV_ORDER_ITEM
                                when iidh.order_no    is NOT NULL
                                 and iidd.item_source = REIM_CONSTANTS.ITEM_SRC_VPN and not exists (select 'x'
                                                                                                      from item_supplier its
                                                                                                     where its.vpn = iidd.vpn
                                                                                                       and its.supplier IN (select oh.supplier supplier
                                                                                                                              from ordhead oh
                                                                                                                             where oh.order_no = iidh.order_no
                                                                                                                            union all
                                                                                                                            select s_its.supplier supplier
                                                                                                                              from ordhead oh,
                                                                                                                                   sups s_oh,
                                                                                                                                   im_supplier_group_members isgm_oh,
                                                                                                                                   im_supplier_group_members isgm_its,
                                                                                                                                   sups s_its
                                                                                                                             where oh.order_no = iidh.order_no
                                                                                                                               and oh.supplier  = s_oh.supplier
                                                                                                                               and isgm_oh.supplier = s_oh.supplier_parent
                                                                                                                               and isgm_oh.group_id    = isgm_its.group_id
                                                                                                                               and (s_its.supplier_parent = isgm_its.supplier
                                                                                                                                    or s_its.supplier     = isgm_its.supplier))) then
                                VALID_VPN_SUPPLIER
                                when iidh.order_no    is NOT NULL
                                 and iidd.item_source = REIM_CONSTANTS.ITEM_SRC_VPN and exists (select 'x'
                                                                                                  from item_supplier its
                                                                                                 where its.vpn = iidd.vpn
                                                                                                   and its.supplier IN (select oh.supplier supplier
                                                                                                                          from ordhead oh
                                                                                                                         where oh.order_no = iidh.order_no
                                                                                                                        union all
                                                                                                                        select s_its.supplier supplier
                                                                                                                          from ordhead oh,
                                                                                                                               sups s_oh,
                                                                                                                               im_supplier_group_members isgm_oh,
                                                                                                                               im_supplier_group_members isgm_its,
                                                                                                                               sups s_its
                                                                                                                         where oh.order_no = iidh.order_no
                                                                                                                           and oh.supplier  = s_oh.supplier
                                                                                                                           and isgm_oh.supplier = s_oh.supplier_parent
                                                                                                                           and isgm_oh.group_id    = isgm_its.group_id
                                                                                                                           and (s_its.supplier_parent = isgm_its.supplier
                                                                                                                                or s_its.supplier     = isgm_its.supplier))
                                                                                                   group by its.supplier,
                                                                                                            its.vpn
                                                                                                   having count(*) > 1) then
                                DUP_VPN_SUPPLIER
                                when iidd.item_source = REIM_CONSTANTS.ITEM_SRC_UPC and not exists (select 'x'
                                                                                                      from item_master im
                                                                                                     where im.item = iidd.upc
                                                                                                       and im.tran_level < im.item_level) then
                                VALID_UPC
                                when iidh.order_no    is NOT NULL
                                 and iidd.item_source = REIM_CONSTANTS.ITEM_SRC_UPC and not exists (select 'x'
                                                                                                      from item_supplier its,
                                                                                                           item_master im
                                                                                                     where im.item = iidd.upc
                                                                                                       and im.tran_level < im.item_level
                                                                                                       and its.item = DECODE(im.item_level,
                                                                                                                             im.tran_level, im.item,
                                                                                                                             im.item_parent)
                                                                                                       and its.supplier IN (select oh.supplier supplier
                                                                                                                              from ordhead oh
                                                                                                                             where oh.order_no = iidh.order_no
                                                                                                                            union all
                                                                                                                            select s_its.supplier supplier
                                                                                                                              from ordhead oh,
                                                                                                                                   sups s_oh,
                                                                                                                                   im_supplier_group_members isgm_oh,
                                                                                                                                   im_supplier_group_members isgm_its,
                                                                                                                                   sups s_its
                                                                                                                             where oh.order_no = iidh.order_no
                                                                                                                               and oh.supplier  = s_oh.supplier
                                                                                                                               and isgm_oh.supplier = s_oh.supplier_parent
                                                                                                                               and isgm_oh.group_id    = isgm_its.group_id
                                                                                                                               and (s_its.supplier_parent = isgm_its.supplier
                                                                                                                                    or s_its.supplier     = isgm_its.supplier))) then
                                VALID_UPC_SUPPLIER
                           end as rule
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_head iidh,
                           im_inject_doc_detail iidd
                     where iidh.inject_doc_id = value(ids)
                       and iidh.vendor_type   = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                       and iidd.inject_doc_id = iidh.inject_doc_id
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidh.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) iidd_inner,
                   im_inject_doc_rule iidr
             where iidd_inner.rule = iidr.rule;

        -- Non Merch validation
        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidnm_inner.inject_id,
                   iidnm_inner.inject_doc_id,
                   iidnm_inner.rule,
                   L_trial_id,
                   DECODE(iidnm_inner.rule,
                          VALID_NMRCH_CODE, 'Non-Merchandise Code = ' || iidnm_inner.non_merch_code,
                          VALID_SRVC_PERF_STORE, 'Non-Merchandise Code = ' || iidnm_inner.non_merch_code || ' and Store = ' || iidnm_inner.store,
                          NULL) error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (select iidnm.*, -- to be replaced with individual columns
                           case
                                when not exists (select 'x'
                                                   from non_merch_code_head
                                                  where non_merch_code = iidnm.non_merch_code) then
                                     VALID_NMRCH_CODE
                                when NVL(iidnm.service_performed, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_YES and not exists (select 'x'
                                                                                                                                  from store s
                                                                                                                                 where s.store = iidnm.store) then
                                     VALID_SRVC_PERF_STORE
                           end as rule
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_non_merch iidnm
                     where iidnm.inject_doc_id = value(ids)
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidnm.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) iidnm_inner,
                   im_inject_doc_rule iidr
             where iidnm_inner.rule = iidr.rule;

        --Cost and Qty Totals Validation
        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidt_inner.inject_id,
                   iidt_inner.inject_doc_id,
                   iidt_inner.rule,
                   L_trial_id,
                   NULL error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (with doc_hdr as
                    (select iidh.inject_id,
                            iidh.inject_doc_id,
                            iidh.doc_type,
                            iidh.total_cost,
                            iidh.total_qty,
                            NVL(iidh.ref_vendor, iidh.vendor) vendor_id
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_head iidh
                      where iidh.inject_doc_id = value(ids)
                        and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidh.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))),
                    doc_dtl as
                    (select iidd.inject_doc_id,
                            sum(iidd.qty * iidd.unit_cost) total_cost,
                            sum(iidd.qty) total_qty
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_detail iidd
                      where iidd.inject_doc_id = value(ids)
                        and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidd.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                     group by iidd.inject_doc_id),
                    doc_nm as
                    (select iidnm.inject_doc_id,
                            sum(non_merch_amount) total_non_merch
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_non_merch iidnm
                      where iidnm.inject_doc_id = value(ids)
                        and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidnm.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                     group by iidnm.inject_doc_id)
                    select dh.inject_id,
                           dh.inject_doc_id,
                           case
                                when dh.doc_type <> REIM_CONSTANTS.DOC_TYPE_NMRCHI and dh.total_qty is NOT NULL and dd.total_qty is NOT NULL
                                 and NVL((select total_qty_required_ind
                                            from im_supplier_groups isg,
                                                 im_supplier_group_members isgm
                                           where isgm.supplier = dh.vendor_id
                                             and isg.group_id  = isgm.group_id
                                             and rownum        < 2), REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_YES
                                 and abs(dh.total_qty - dd.total_qty) > REIM_CONSTANTS.ZERO then
                                HDR_DTL_QTY_MTCH
                                when dh.doc_type = REIM_CONSTANTS.DOC_TYPE_NMRCHI and dnm.total_non_merch is NOT NULL
                                 and abs(abs(dh.total_cost) - abs(dnm.total_non_merch)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dnm.total_non_merch) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                NON_MRCH_COST
                                when dd.total_cost is NOT NULL and dnm.total_non_merch is NOT NULL
                                 and abs(abs(dh.total_cost) - abs(dd.total_cost + dnm.total_non_merch)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dd.total_cost + dnm.total_non_merch) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                DETAIL_NM_COST
                                when dh.doc_type <> REIM_CONSTANTS.DOC_TYPE_NMRCHI and dnm.total_non_merch is NOT NULL and dd.total_cost is NOT NULL
                                 and abs(abs(dh.total_cost - dd.total_cost) - abs(dnm.total_non_merch)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dnm.total_non_merch) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                NON_MRCH_COST
                                when dh.total_cost is NOT NULL and dnm.total_non_merch is NULL
                                 and abs(abs(dh.total_cost) - abs(dd.total_cost)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dd.total_cost) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                DETAIL_COST
                           end as rule
                      from doc_hdr dh,
                           doc_dtl dd,
                           doc_nm dnm
                     where dh.inject_doc_id = dd.inject_doc_id (+)
                       and dh.inject_doc_id = dnm.inject_doc_id (+)) iidt_inner,
                   im_inject_doc_rule iidr
             where iidt_inner.rule = iidr.rule;


        --Allowance Totals Validation
        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        detail_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidd.inject_id,
                   iidd.inject_doc_id,
                   iidd.detail_id,
                   iidr.rule,
                   L_trial_id,
                   'Item/UPC/VPN = ' || DECODE(iidd.item_source,
                                               REIM_CONSTANTS.ITEM_SRC_ITEM_ID, iidd.item,
                                               REIM_CONSTANTS.ITEM_SRC_UPC, iidd.upc,
                                               REIM_CONSTANTS.ITEM_SRC_VPN, iidd.vpn) error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   im_inject_doc_detail iidd,
                   (select iidda.detail_id,
                           sum(iidda.allowance_amount) total_allowance_amt
                      from im_inject_doc_detail_allowance iidda
                     group by iidda.detail_id) iidda_inner,
                   im_inject_doc_rule iidr
             where iidd.inject_doc_id = value(ids)
               and iidd.detail_id     = iidda_inner.detail_id
               and iidd.total_allowance <> iidda_inner.total_allowance_amt
               and iidr.rule          = ALLW_TOTAL
               and not exists (select 'x'
                                 from im_inject_doc_error iide
                                where iide.inject_doc_id  = iidd.inject_doc_id
                                  and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                              REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED));


        -- Function to derive item if item source is VPN/UPC
        O_status := DERIVE_ITEM(O_error_message, L_trial_id);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        if LP_num_tax_allow <> REIM_CONSTANTS.NUM_TAX_ALLOW_NONE then

            -- Function to perform all tax related validations
            O_status := VALIDATE_TRAN_TAX(O_error_message, L_trial_id);

            if (O_status <> REIM_CONSTANTS.SUCCESS) then
                return O_status;
            end if;

        end if;

        -- Update Fixable indicators to 'N' if the documents are of special types.
        -- Should be called after all validations.
        O_status := UPDATE_FIXABLE_IND(O_error_message);
        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END VALIDATE_TRANSACTIONS;

    /**
     * The public function used for Injection of transactions
     *
     * Input param: I_inject_id (Unique identifier for the injector process)
     *              I_thread_id (Thread Id, used when multi-threaded)
     *              I_inject_doc_id (Id of the Transaction that needs to be injected)
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION INJECT_VALID_TRANSACTIONS(O_error_message    OUT VARCHAR2,
                                       I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE,
                                       I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                       I_username      IN     VARCHAR2)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.INJECT_VALID_TRANSACTIONS';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

    BEGIN

        -- Validate the Input parameters and initialize the global collection with the set of inject_doc_ids based on input.
        O_status := INIT_INJ_DOCS(O_error_message,
                                  I_inject_id,
                                  NULL, -- I_thread_id
                                  I_inject_doc_id,
                                  PROCESS_INJECTION);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        -- Set all required global variables.
        O_status := SET_GLOBALS(O_error_message);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        LP_username := NVL(I_username, get_user);

        delete from im_inject_doc_xform_gtt;

        insert into im_inject_doc_xform_gtt(inject_doc_id,
                                            doc_id)
            select iidh.inject_doc_id,
                   im_doc_head_seq.NEXTVAL
              from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   im_inject_doc_head iidh
             where iidh.inject_doc_id = value(ids)
               and iidh.multi_loc_ind = REIM_CONSTANTS.YN_NO
               and not exists (select 'x'
                                 from im_inject_doc_error iide
                                where iide.inject_doc_id  = iidh.inject_doc_id
                                  and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                              REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED));
        --Supplier Document header
        insert into im_doc_head(doc_id,
                                type,
                                status,
                                order_no,
                                rtv_order_no,
                                location,
                                loc_type,
                                total_discount,
                                group_id,
                                parent_id,
                                doc_date,
                                creation_date,
                                created_by,
                                vendor_type,
                                vendor,
                                ext_doc_id,
                                edi_download_ind,
                                terms,
                                terms_dscnt_pct,
                                due_date,
                                payment_method,
                                match_id,
                                match_date,
                                approval_id,
                                approval_date,
                                pre_paid_ind,
                                pre_paid_id,
                                post_date,
                                currency_code,
                                exchange_rate,
                                total_cost,
                                total_qty,
                                manually_paid_ind,
                                custom_doc_ref_1,
                                custom_doc_ref_2,
                                custom_doc_ref_3,
                                custom_doc_ref_4,
                                last_updated_by,
                                last_update_date,
                                freight_type,
                                ref_doc,
                                ref_auth_no,
                                cost_pre_match,
                                detail_matched,
                                best_terms,
                                best_terms_source,
                                best_terms_date,
                                best_terms_date_source,
                                variance_within_tolerance,
                                resolution_adjusted_total_cost,
                                resolution_adjusted_total_qty,
                                consignment_ind,
                                deal_id,
                                rtv_ind,
                                discount_date,
                                deal_type,
                                hold_status,
                                total_cost_inc_tax,
                                tax_disc_create_date,
                                dsd_ind,
                                ers_ind,
                                supplier_site_id,
                                reverse_vat_ind,
                                header_only,
                                doc_source,
                                match_type,
                                deal_detail_id,
                                ref_cnr_ext_doc_id,
                                ref_inv_ext_doc_id)
             select gtt.doc_id, -- doc_id
                    case
                        when iidh.vendor_type = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                         and iidh.doc_type IN (REIM_CONSTANTS.DOC_TYPE_DEBMEQ,
                                               REIM_CONSTANTS.DOC_TYPE_DEBMEC,
                                               REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                                               REIM_CONSTANTS.DOC_TYPE_CRDNRQ)
                         and NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT then
                            DECODE(iso.send_debit_memo,
                                   REIM_CONSTANTS.SEND_DM_ALWAYS, REIM_CONSTANTS.DOC_TYPE_DEBMEQ,
                                   REIM_CONSTANTS.DOC_TYPE_CRDNRQ)
                        else
                            iidh.doc_type
                    end as doc_type, --type
                    case
                        when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_MRCHI
                         and (NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) or
                              s.settlement_code = REIM_CONSTANTS.EVAL_SUPP) then
                            REIM_CONSTANTS.DOC_STATUS_MTCH
                        when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_MRCHI then
                            REIM_CONSTANTS.DOC_STATUS_RMTCH
                        else
                            REIM_CONSTANTS.DOC_STATUS_APPRVE
                    end as status, --status,
                    iidh.order_no, --order_no,
                    iidh.rtv_order_no, --rtv_order_no,
                    case
                        when loc_type = REIM_CONSTANTS.LOC_TYPE_WH then
                            (select physical_wh
                               from wh
                              where wh.wh = iidh.location)
                        else
                            iidh.location
                    end as location, --location,
                    iidh.loc_type, --loc_type,
                    round(iidh.total_discount, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --total_discount,
                    iidh.group_id, --group_id,
                    NULL, --parent_id,
                    iidh.doc_date, --doc_date,
                    SYSDATE, --create_date,
                    LP_username, --created_by,
                    iidh.vendor_type, --vendor_type
                    NVL(iidh.ref_vendor, iidh.vendor), --vendor
                    GET_EXT_DOC_ID_WITH_PRFX(iidh.ext_doc_id, iidh.doc_type), --ext_doc_id,
                    REIM_CONSTANTS.YN_NO,  --edi_download_ind,
                    iidh.terms,
--                    DECODE(NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO),
--                           REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT, isyso.default_pay_now_terms,
--                           iidh.terms) as terms, --terms,
                    case
                        when NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT then
                            REIM_CONSTANTS.ZERO
                        else
                            (select percent
                               from terms t
                              where t.terms = iidh.terms
                                and t.enabled_flag = REIM_CONSTANTS.YN_YES
                                and iidh.doc_date  between nvl(t.start_date_active, iidh.doc_date) and nvl(t.end_date_active, iidh.doc_date)
                                and rownum < 2)
                        end as terms_dscnt_pct, --terms_dscnt_pct,
                    iidh.due_date, --due_date,
                    iidh.payment_method, --payment_method,
                    case
                        when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_MRCHI
                         and (NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) or
                              s.settlement_code = REIM_CONSTANTS.EVAL_SUPP) then
                            LP_username
                        else
                            NULL
                    end as match_id, --match_id,
                    case
                        when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_MRCHI
                         and (NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) or
                              s.settlement_code = REIM_CONSTANTS.EVAL_SUPP) then
                            iidh.doc_date
                        else
                            NULL
                    end as match_date, --match_date,
                    case
                        when iidh.doc_type <> REIM_CONSTANTS.DOC_TYPE_MRCHI then
                            LP_username
                        else
                            NULL
                    end as approval_id, --approval_id,
                    case
                        when iidh.doc_type <> REIM_CONSTANTS.DOC_TYPE_MRCHI then
                            LP_vdate
                        else
                            NULL
                    end as approval_date, --approval_date,
                    REIM_CONSTANTS.YN_NO, --pre_paid_ind,
                    NULL, --pre_paid_id,
                    NULL, --post_date,
                    iidh.currency_code, --currency_code,
                    iidh.exchange_rate, --exchange_rate,
                    round(iidh.total_cost, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --total_cost,
                    NVL(iidh.total_qty, 0), --total_qty,
                    iidh.manually_paid_ind, --manually_paid_ind,
                    iidh.custom_doc_ref1, --custom_doc_ref_1,
                    iidh.custom_doc_ref2, --custom_doc_ref_2,
                    iidh.custom_doc_ref3, --custom_doc_ref_3,
                    iidh.custom_doc_ref4, --custom_doc_ref_4,
                    LP_username, --last_updated_by,
                    SYSDATE, --last_update_date,
                    iidh.freight_type, --freight_type,
                    iidh.cross_ref_doc, --ref_doc,
                    NULL, --ref_auth_no,
                    REIM_CONSTANTS.YN_NO, --cost_pre_match,
                    REIM_CONSTANTS.YN_NO, --detail_matched,
                    DECODE(iidh.doc_type,
                           REIM_CONSTANTS.DOC_TYPE_MRCHI, NULL,
                           iidh.terms), --best_terms
                    DECODE(iidh.doc_type,
                           REIM_CONSTANTS.DOC_TYPE_MRCHI, NULL,
                           REIM_CONSTANTS.TERMS_SRC_DOC), --best_terms_source
                    DECODE(iidh.doc_type,
                           REIM_CONSTANTS.DOC_TYPE_MRCHI, NULL,
                           iidh.doc_date), --best_terms_date,
                    DECODE(iidh.doc_type,
                           REIM_CONSTANTS.DOC_TYPE_MRCHI, NULL,
                           REIM_CONSTANTS.TERMS_DATE_SRC_DOC), --best_terms_date_source,
                    NULL, --variance_within_tolerance,
                    round(iidh.total_cost, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --resolution_adjusted_total_cost,
                    NVL(iidh.total_qty, 0), --resolution_adjusted_total_qty,
                    DECODE(NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO),
                           REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT, REIM_CONSTANTS.YN_YES,
                           REIM_CONSTANTS.YN_NO), --consignment_ind,
                    iidh.deal_id, --deal_id,
                    NVL(iidh.rtv_ind, REIM_CONSTANTS.YN_NO), --rtv_ind
                    NULL, --discount_date,
                    NULL, --deal_type,
                    case
                        when iidh.vendor_type = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER and iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_CRDNT
                         and NVL(iso.hold_invoices, REIM_CONSTANTS.YN_NO) = REIM_CONSTANTS.YN_YES then
                            REIM_CONSTANTS.HOLD_STATUS_HELD
                        else
                            REIM_CONSTANTS.HOLD_STATUS_NEVERHELD
                    end as hold_status, --hold_status
                    round((iidh.total_cost + NVL(iidh.total_tax_amount, REIM_CONSTANTS.ZERO)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --total_cost_inc_tax,
                    NULL, --tax_disc_create_date,
                    DECODE(NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO),
                           REIM_CONSTANTS.DOC_MERCH_TYPE_DSD, REIM_CONSTANTS.YN_YES,
                           REIM_CONSTANTS.YN_NO), --dsd_ind,
                    DECODE(NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO),
                           REIM_CONSTANTS.DOC_MERCH_TYPE_ERS, REIM_CONSTANTS.YN_YES,
                           REIM_CONSTANTS.YN_NO), --ers_ind,
                    DECODE(iidh.ref_vendor,
                           NULL, NULL,
                           iidh.vendor), --supplier_site_id
                    iidh.reverse_vat_ind, --reverse_vat_ind,
                    case
                       when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_NMRCHI then
                          REIM_CONSTANTS.YN_NO
                       when exists (select 'x'
                                      from im_inject_doc_detail iidd
                                     where iidd.inject_id     = iidh.inject_id
                                       and iidd.inject_doc_id = iidh.inject_doc_id) then
                          REIM_CONSTANTS.YN_NO
                       else
                          REIM_CONSTANTS.YN_YES
                    end header_only,
                    NVL(iidh.doc_source,REIM_CONSTANTS.DOC_SOURCE_EDI),

                     case
                        when iidh.doc_type = REIM_CONSTANTS.DOC_TYPE_MRCHI
                         and (NVL(iidh.merch_type, REIM_CONSTANTS.YN_NO) IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) or
                              s.settlement_code = REIM_CONSTANTS.EVAL_SUPP) then
                            REIM_CONSTANTS.MATCH_TYPE_EXTERNAL
                        else
                            NULL
                    end as match_type, --match_type
                    IIDH.DEAL_DETAIL_ID, --deal_detail_id
                    iidh.ref_cnr_ext_doc_id, -- ref_cnr_ext_doc_id
                    iidh.ref_inv_ext_doc_id  -- ref_inv_ext_doc_id
              from im_inject_doc_xform_gtt gtt,
                   im_inject_doc_head iidh,
                   v_im_supp_site_attrib_expl iso,
                   sups s,
                   im_system_options isyso
             where iidh.inject_doc_id = gtt.inject_doc_id
               and iidh.vendor_type   = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
               and iidh.multi_loc_ind = REIM_CONSTANTS.YN_NO
               and iso.supplier       = iidh.vendor
               and s.supplier         = iidh.vendor;

        --Partner Document header
        insert into im_doc_head(doc_id,
                                type,
                                status,
                                order_no,
                                rtv_order_no,
                                location,
                                loc_type,
                                total_discount,
                                group_id,
                                parent_id,
                                doc_date,
                                creation_date,
                                created_by,
                                vendor_type,
                                vendor,
                                ext_doc_id,
                                edi_download_ind,
                                terms,
                                terms_dscnt_pct,
                                due_date,
                                payment_method,
                                match_id,
                                match_date,
                                approval_id,
                                approval_date,
                                pre_paid_ind,
                                pre_paid_id,
                                post_date,
                                currency_code,
                                exchange_rate,
                                total_cost,
                                total_qty,
                                manually_paid_ind,
                                custom_doc_ref_1,
                                custom_doc_ref_2,
                                custom_doc_ref_3,
                                custom_doc_ref_4,
                                last_updated_by,
                                last_update_date,
                                freight_type,
                                ref_doc,
                                ref_auth_no,
                                cost_pre_match,
                                detail_matched,
                                best_terms,
                                best_terms_source,
                                best_terms_date,
                                best_terms_date_source,
                                variance_within_tolerance,
                                resolution_adjusted_total_cost,
                                resolution_adjusted_total_qty,
                                consignment_ind,
                                deal_id,
                                rtv_ind,
                                discount_date,
                                deal_type,
                                hold_status,
                                total_cost_inc_tax,
                                tax_disc_create_date,
                                dsd_ind,
                                ers_ind,
                                supplier_site_id,
                                reverse_vat_ind,
                                header_only,
                                doc_source,
                                deal_detail_id,
                                ref_cnr_ext_doc_id,
                                ref_inv_ext_doc_id)
             select gtt.doc_id, -- doc_id
                    iidh.doc_type, --doc_type, --type
                    REIM_CONSTANTS.DOC_STATUS_APPRVE, --status,
                    iidh.order_no, --order_no,
                    iidh.rtv_order_no, --rtv_order_no
                    case
                        when loc_type = REIM_CONSTANTS.LOC_TYPE_WH then
                            (select physical_wh
                               from wh
                              where wh.wh = iidh.location)
                        else
                            iidh.location
                    end as location, --location,
                    iidh.loc_type, --loc_type,
                    iidh.total_discount, --total_discount,
                    iidh.group_id, --group_id,
                    NULL, --parent_id,
                    iidh.doc_date, --doc_date,
                    SYSDATE, --create_date,
                    LP_username, --create_id,
                    iidh.vendor_type, --vendor_type
                    NVL(iidh.ref_vendor, iidh.vendor), --vendor
                    GET_EXT_DOC_ID_WITH_PRFX(iidh.ext_doc_id, iidh.doc_type), --ext_doc_id,
                    REIM_CONSTANTS.YN_NO,  --edi_download_ind,
                    iidh.terms, --terms,
                    (select percent
                       from terms t
                      where t.terms = iidh.terms
                        and t.enabled_flag = REIM_CONSTANTS.YN_YES
                        and iidh.doc_date  between nvl(t.start_date_active, iidh.doc_date) and nvl(t.end_date_active, iidh.doc_date)
                        and rownum < 2), --terms_dscnt_pct,
                    iidh.due_date, --due_date,
                    iidh.payment_method, --payment_method,
                    NULL, --match_id,
                    NULL, --match_date,
                    LP_username, --approval_id,
                    LP_vdate, --approval_date,
                    REIM_CONSTANTS.YN_NO, --pre_paid_ind,
                    NULL, --pre_paid_id,
                    NULL, --post_date,
                    iidh.currency_code, --currency_code,
                    iidh.exchange_rate, --exchange_rate,
                    round(iidh.total_cost, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --total_cost,
                    NVL(iidh.total_qty, 0), --total_qty,
                    iidh.manually_paid_ind, --manually_paid_ind,
                    iidh.custom_doc_ref1, --custom_doc_ref_1,
                    iidh.custom_doc_ref2, --custom_doc_ref_2,
                    iidh.custom_doc_ref3, --custom_doc_ref_3,
                    iidh.custom_doc_ref4, --custom_doc_ref_4,
                    LP_username, --last_updated_by,
                    SYSDATE, --last_update_date,
                    iidh.freight_type, --freight_type,
                    iidh.cross_ref_doc, --ref_doc,
                    NULL, --ref_auth_no,
                    REIM_CONSTANTS.YN_NO, --cost_pre_match,
                    REIM_CONSTANTS.YN_NO, --detail_matched,
                    iidh.terms, --best_terms
                    REIM_CONSTANTS.TERMS_SRC_DOC, --best_terms_source
                    iidh.doc_date, --best_terms_date,
                    REIM_CONSTANTS.TERMS_DATE_SRC_DOC, --best_terms_date_source,
                    NULL, --variance_within_tolerance,
                    round(iidh.total_cost, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --resolution_adjusted_total_cost,
                    NVL(iidh.total_qty, 0), --resolution_adjusted_total_qty,
                    REIM_CONSTANTS.YN_NO, --consignment_ind,
                    iidh.deal_id, --deal_id,
                    REIM_CONSTANTS.YN_NO, --rtv_ind
                    NULL, --discount_date,
                    NULL, --deal_type,
                    REIM_CONSTANTS.HOLD_STATUS_NEVERHELD, --hold_status
                    round((iidh.total_cost + NVL(iidh.total_tax_amount, REIM_CONSTANTS.ZERO)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --total_cost_inc_tax,
                    NULL, --tax_disc_create_date,
                    REIM_CONSTANTS.YN_NO, --dsd_ind,
                    REIM_CONSTANTS.YN_NO, --ers_ind,
                    NULL, --supplier_site_id
                    iidh.reverse_vat_ind, --reverse_vat_ind
                    REIM_CONSTANTS.YN_NO, --header_only (Only NMRCHI, will aways have non_merch details)
                    NVL(iidh.doc_source,REIM_CONSTANTS.DOC_SOURCE_EDI),
                    iidh.deal_detail_id, --deal_detail_id
                    iidh.ref_cnr_ext_doc_id, -- ref_cnr_ext_doc_id
                    iidh.ref_inv_ext_doc_id  -- ref_inv_ext_doc_id
              from im_inject_doc_xform_gtt gtt,
                   im_inject_doc_head iidh
             where iidh.inject_doc_id = gtt.inject_doc_id
               and iidh.vendor_type   <> REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER;

        --Insert Invoice details
        insert into im_invoice_detail(doc_id,
                                      item,
                                      unit_cost,
                                      qty,
                                      resolution_adjusted_unit_cost,
                                      resolution_adjusted_qty,
                                      status,
                                      cost_matched,
                                      qty_matched,
                                      cost_variance_within_tolerance,
                                      qty_variance_within_tolerance,
                                      adjustment_pending,
                                      last_updated_by,
                                      last_update_date,
                                      created_by,
                                      creation_date,
                                      tax_discrepancy_ind,
                                      unit_freight,
                                      unit_mrp,
                                      unit_retail)
            select gtt.doc_id,
                   iidd.item,
                   round(iidd.unit_cost, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                   iidd.qty,
                   round(iidd.unit_cost, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                   iidd.qty,
                   DECODE(idh.status,
                          REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
                          REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH), -- status
                   DECODE(idh.status,
                          REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.YN_YES,
                          REIM_CONSTANTS.YN_NO), -- cost_matched
                   DECODE(idh.status,
                          REIM_CONSTANTS.DOC_STATUS_MTCH, REIM_CONSTANTS.YN_YES,
                          REIM_CONSTANTS.YN_NO), -- qty_matched
                   REIM_CONSTANTS.ZERO,
                   REIM_CONSTANTS.ZERO,
                   REIM_CONSTANTS.YN_NO, --adjustment_pending
                   LP_username,
                   sysdate,
                   LP_username,
                   sysdate,
                   REIM_CONSTANTS.YN_NO, -- tax_discrepancy_ind
                   NULL, --unit_freight
                   NULL, --unit_mrp
                   NULL --unit_retail
              from im_inject_doc_xform_gtt gtt,
                   im_doc_head idh,
                   im_inject_doc_detail iidd
             where idh.doc_id         = gtt.doc_id
               and idh.type           = REIM_CONSTANTS.DOC_TYPE_MRCHI
               and iidd.inject_doc_id = gtt.inject_doc_id;

        --Insert document details
        insert into im_doc_detail_reason_codes(im_doc_detail_reason_codes_id,
                                               doc_id,
                                               item,
                                               reason_code_id,
                                               status,
                                               cost_matched,
                                               qty_matched,
                                               adjusted_unit_cost,
                                               adjusted_qty,
                                               last_updated_by,
                                               last_update_date,
                                               created_by,
                                               creation_date,
                                               orig_unit_cost,
                                               orig_qty,
                                               tax_matched,
                                               unit_freight,
                                               unit_mrp,
                                               unit_retail)
            select im_doc_detail_reason_codes_seq.NEXTVAL,
                   idh.doc_id,
                   iidd.item,
                   NVL(iidd.reason_code_id, DECODE(idh.type,
                                                   REIM_CONSTANTS.DOC_TYPE_DEBMEQ, LP_rtv_reason_code,
                                                   REIM_CONSTANTS.DOC_TYPE_CRDNRQ, LP_rtv_reason_code,
                                                   REIM_CONSTANTS.DOC_TYPE_CRDMEC, LP_cmc_reason_code,
                                                   NULL)), --reason_code_id,
                   REIM_CONSTANTS.DOC_ITEM_STATUS_APPRVE,
                   REIM_CONSTANTS.YN_NO, --cost_matched
                   REIM_CONSTANTS.YN_NO, --qty_matched
                   round(iidd.unit_cost, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                   iidd.qty,
                   LP_username,
                   sysdate,
                   LP_username,
                   sysdate,
                   round(iidd.unit_cost, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                   iidd.qty,
                   NULL, --tax_matched
                   NULL, --unit_freight
                   NULL, --unit_mrp
                   NULL --unit_retail
              from im_inject_doc_xform_gtt gtt,
                   im_doc_head idh,
                   im_inject_doc_detail iidd
             where idh.doc_id         = gtt.doc_id
               and idh.type          <> REIM_CONSTANTS.DOC_TYPE_MRCHI
               and iidd.inject_doc_id = gtt.inject_doc_id;

        --Insert document non merch
        insert into im_doc_non_merch(doc_id,
                                     non_merch_code,
                                     non_merch_amt,
                                     created_by,
                                     creation_date,
                                     last_updated_by,
                                     last_update_date)
            select gtt.doc_id,
                   iidnm.non_merch_code,
                   round(iidnm.non_merch_amount, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                   LP_username,
                   sysdate,
                   LP_username,
                   sysdate
              from im_inject_doc_xform_gtt gtt,
                   im_inject_doc_non_merch iidnm
             where iidnm.inject_doc_id         = gtt.inject_doc_id;

        --Insert document allowance
        insert into im_invoice_detail_allowance(doc_id,
                                                item,
                                                allowance_code,
                                                allowance_amount,
                                                created_by,
                                                creation_date,
                                                last_updated_by,
                                                last_update_date)
            select gtt.doc_id,
                   iidd.item,
                   iidda.allowance_code,
                   round(iidda.allowance_amount, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                   LP_username,
                   sysdate,
                   LP_username,
                   sysdate
              from im_inject_doc_xform_gtt gtt,
                   im_inject_doc_detail iidd,
                   im_inject_doc_detail_allowance iidda
             where iidd.inject_doc_id = gtt.inject_doc_id
               and iidda.detail_id    = iidd.detail_id;

        if LP_num_tax_allow   <> REIM_CONSTANTS.NUM_TAX_ALLOW_NONE then

            --Insert Document Taxes
            insert into im_doc_tax(doc_id,
                                   tax_code,
                                   tax_rate,
                                   tax_basis,
                                   tax_amount,
                                   created_by,
                                   creation_date,
                                   last_updated_by,
                                   last_update_date)
                select gtt.doc_id,
                       iidt.tax_code,
                       iidt.tax_rate,
                       round(iidt.tax_basis, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                       round((iidt.tax_basis * (iidt.tax_rate/100)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --tax_amount,
                       LP_username,
                       sysdate,
                       LP_username,
                       sysdate
                  from im_inject_doc_xform_gtt gtt,
                       im_inject_doc_tax iidt
                 where iidt.inject_doc_id = gtt.inject_doc_id;

            --Insert Invoice detail taxes
            insert into im_invoice_detail_tax(doc_id,
                                              item,
                                              tax_code,
                                              tax_rate,
                                              tax_basis,
                                              tax_amount,
                                              tax_formula,
                                              tax_order,
                                              tax_formula_type,
                                              reverse_vat_ind,
                                              created_by,
                                              creation_date,
                                              last_updated_by,
                                              last_update_date)
                select idh.doc_id,
                       iidd.item,
                       iiddt.tax_code,
                       iiddt.tax_rate,
                       round(iiddt.tax_basis, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                       round((iiddt.tax_basis * (iiddt.tax_rate/100)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --tax_amount
                       REIM_CONSTANTS.TAX_FORMULA_COST,
                       REIM_CONSTANTS.ONE,
                       REIM_CONSTANTS.TAX_FORM_TYPE_COST_ONLY,
                       iiddt.reverse_vat_ind,
                       LP_username,
                       sysdate,
                       LP_username,
                       sysdate
                  from im_inject_doc_xform_gtt gtt,
                       im_doc_head idh,
                       im_inject_doc_detail iidd,
                       im_inject_doc_detail_tax iiddt
                 where idh.doc_id         = gtt.doc_id
                   and idh.type           = REIM_CONSTANTS.DOC_TYPE_MRCHI
                   and iidd.inject_doc_id = gtt.inject_doc_id
                   and iiddt.detail_id    = iidd.detail_id;

            --Insert document detail rc taxes
            insert into im_doc_detail_rc_tax(im_doc_detail_reason_codes_id,
                                             tax_code,
                                             tax_rate,
                                             tax_basis,
                                             tax_amount,
                                             tax_formula,
                                             tax_order,
                                             tax_formula_type,
                                             reverse_vat_ind,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date)
                select iddrc.im_doc_detail_reason_codes_id,
                       iiddt.tax_code,
                       iiddt.tax_rate,
                       round(iiddt.tax_basis, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                       round((iiddt.tax_basis * (iiddt.tax_rate/100)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                       REIM_CONSTANTS.TAX_FORMULA_COST,
                       REIM_CONSTANTS.ONE,
                       REIM_CONSTANTS.TAX_FORM_TYPE_COST_ONLY,
                       iiddt.reverse_vat_ind,
                       LP_username,
                       sysdate,
                       LP_username,
                       sysdate
                  from im_inject_doc_xform_gtt gtt,
                       im_inject_doc_detail iidd,
                       im_inject_doc_detail_tax iiddt,
                       im_doc_detail_reason_codes iddrc
                 where iidd.inject_doc_id = gtt.inject_doc_id
                   and iidd.detail_id     = iiddt.detail_id
                   and iddrc.doc_id       = gtt.doc_id
                   and iddrc.item         = iidd.item;

            --Insert document allowance taxes
            insert into im_invoice_detail_allw_tax(doc_id,
                                                   item,
                                                   tax_code,
                                                   tax_rate,
                                                   tax_basis,
                                                   tax_amount,
                                                   tax_formula,
                                                   tax_order,
                                                   tax_formula_type,
                                                   allowance_code,
                                                   created_by,
                                                   creation_date,
                                                   last_updated_by,
                                                   last_update_date)
                select gtt.doc_id,
                       iidd.item,
                       iiddat.tax_code,
                       iiddat.tax_rate,
                       round(iiddat.tax_basis, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                       round((iiddat.tax_basis * (iiddat.tax_rate/100)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                       REIM_CONSTANTS.TAX_FORMULA_COST,
                       REIM_CONSTANTS.ONE,
                       REIM_CONSTANTS.TAX_FORM_TYPE_COST_ONLY,
                       iidda.allowance_code,
                       LP_username,
                       sysdate,
                       LP_username,
                       sysdate
                  from im_inject_doc_xform_gtt gtt,
                       im_inject_doc_detail iidd,
                       im_inject_doc_detail_allowance iidda,
                       im_inject_doc_detail_allow_tax iiddat
                 where iidd.inject_doc_id    = gtt.inject_doc_id
                   and iidda.detail_id       = iidd.detail_id
                   and iiddat.detail_id      = iidda.detail_id
                   and iiddat.allowance_code = iiddat.allowance_code;

            --Insert document non merch taxes
            insert into im_doc_non_merch_tax(doc_id,
                                             non_merch_code,
                                             tax_code,
                                             tax_rate,
                                             tax_basis,
                                             tax_amount,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date)
                select gtt.doc_id,
                       iidnmt.non_merch_code,
                       iidnmt.tax_code,
                       iidnmt.tax_rate,
                       round(iidnmt.tax_basis, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                       round((iidnmt.tax_basis * (iidnmt.tax_rate/100)), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS), --tax_amount
                       LP_username,
                       sysdate,
                       LP_username,
                       sysdate
                  from im_inject_doc_xform_gtt gtt,
                       im_inject_doc_non_merch_tax iidnmt
                 where iidnmt.inject_doc_id         = gtt.inject_doc_id;

            if LP_tax_validation_type = REIM_CONSTANTS.TAX_VALID_TYPE_RECON then
                --Insert tax discrepancies
                insert into im_tax_discrepancy(doc_id,
                                               order_no,
                                               item,
                                               tax_code,
                                               doc_tax_rate,
                                               doc_tax_amount,
                                               verify_tax_rate,
                                               verify_tax_code,
                                               verify_tax_amount,
                                               verify_tax_src,
                                               verify_tax_formula,
                                               verify_tax_order,
                                               created_by,
                                               creation_date,
                                               last_updated_by,
                                               last_update_date,
                                               object_version_id)
                    select gtt.doc_id,
                           iitd.order_no,
                           iitd.item,
                           iitd.tax_code,
                           iitd.doc_tax_rate,
                           round(iitd.doc_tax_amount, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                           iitd.verify_tax_rate,
                           iitd.verify_tax_code,
                           round(iitd.verify_tax_amount, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                           iitd.verify_tax_src,
                           iitd.verify_tax_formula,
                           iitd.verify_tax_order,
                           LP_username,
                           sysdate,
                           LP_username,
                           sysdate,
                           1
                      from im_inject_doc_xform_gtt gtt,
                           im_inject_tax_discrepancy iitd
                     where iitd.inject_doc_id = gtt.inject_doc_id;

                --Update Tax discrepancy
                merge into im_doc_head tgt
                using (select gtt.doc_id
                         from im_inject_doc_xform_gtt gtt
                        where exists (select 'x'
                                        from im_tax_discrepancy itd
                                       where itd.doc_id = gtt.doc_id)) src
                on (tgt.doc_id = src.doc_id)
                when matched then
                update set tgt.status               = REIM_CONSTANTS.DOC_STATUS_TAXDIS,
                           tgt.tax_disc_create_date = sysdate,
                           tgt.last_updated_by      = LP_username,
                           tgt.last_update_date     = sysdate;

                merge into im_invoice_detail tgt
                using (select gtt.doc_id,
                              iid.item
                         from im_inject_doc_xform_gtt gtt,
                              im_invoice_detail iid
                        where iid.doc_id = gtt.doc_id
                          and exists (select 'x'
                                        from im_tax_discrepancy itd
                                       where itd.doc_id = gtt.doc_id
                                         and itd.item   = iid.item)) src
                on (tgt.doc_id = src.doc_id and
                    tgt.item   = src.item)
                when matched then
                update set tgt.tax_discrepancy_ind  = REIM_CONSTANTS.YN_YES,
                           tgt.last_updated_by      = LP_username,
                           tgt.last_update_date     = sysdate;

            elsif LP_tax_validation_type = REIM_CONSTANTS.TAX_VALID_TYPE_VENDR then
                --Insert im_item_tax_audit
                insert into im_item_tax_audit(doc_id,
                                              item,
                                              order_no,
                                              tax_code,
                                              doc_tax_rate,
                                              doc_tax_amount,
                                              doc_tax_formula,
                                              doc_tax_order,
                                              created_by,
                                              creation_date)
                    select gtt.doc_id,
                           iitd.item,
                           iitd.order_no,
                           iitd.tax_code,
                           iitd.doc_tax_rate,
                           round(iitd.doc_tax_amount, REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS),
                           iitd.verify_tax_formula,
                           iitd.verify_tax_order,
                           LP_username,
                           sysdate
                      from im_inject_doc_xform_gtt gtt,
                           im_inject_tax_discrepancy iitd
                     where iitd.inject_doc_id = gtt.inject_doc_id;
            end if; --LP_tax_validation_type

        end if; -- LP_num_tax_allow

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END INJECT_VALID_TRANSACTIONS;


    /**
     * The function which appends default prefix from system options.
     */
    FUNCTION GET_EXT_DOC_ID_WITH_PRFX(I_ext_doc_id IN     IM_INJECT_DOC_HEAD.EXT_DOC_ID%TYPE,
                                      I_doc_type  IN     IM_INJECT_DOC_HEAD.DOC_TYPE%TYPE)
    RETURN VARCHAR2
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.GET_EXT_DOC_ID_WITH_PRFX';

        O_ext_doc_id IM_INJECT_DOC_HEAD.EXT_DOC_ID%TYPE := NULL;

    BEGIN

        if I_doc_type NOT IN (REIM_CONSTANTS.DOC_TYPE_DEBMEC,
                              REIM_CONSTANTS.DOC_TYPE_DEBMEQ,
                              REIM_CONSTANTS.DOC_TYPE_DEBMET,
                              REIM_CONSTANTS.DOC_TYPE_CRDMEC,
                              REIM_CONSTANTS.DOC_TYPE_CRDMEQ,
                              REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                              REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                              REIM_CONSTANTS.DOC_TYPE_CRDNRT) then
            return I_ext_doc_id;
        end if;

        select SUBSTR((DECODE(I_doc_type,
                              REIM_CONSTANTS.DOC_TYPE_DEBMEC, iso.debit_memo_prefix_cost,
                              REIM_CONSTANTS.DOC_TYPE_DEBMEQ, iso.debit_memo_prefix_qty,
                              REIM_CONSTANTS.DOC_TYPE_DEBMET, iso.debit_memo_prefix_tax,
                              REIM_CONSTANTS.DOC_TYPE_CRDMEC, iso.credit_memo_prefix_cost,
                              REIM_CONSTANTS.DOC_TYPE_CRDMEQ, iso.credit_memo_prefix_qty,
                              REIM_CONSTANTS.DOC_TYPE_CRDNRC, iso.credit_note_req_prefix_cost,
                              REIM_CONSTANTS.DOC_TYPE_CRDNRQ, iso.credit_note_req_prefix_qty,
                              REIM_CONSTANTS.DOC_TYPE_CRDNRT, iso.credit_note_req_prefix_tax) || I_ext_doc_id), 1, 150)
          into O_ext_doc_id
          from im_system_options iso;

        return O_ext_doc_id;

    EXCEPTION
        when OTHERS then
            return I_ext_doc_id;

    END GET_EXT_DOC_ID_WITH_PRFX;


    /**
     * The function which checks the credit note's eligiblity for reverse vat.
     */
    FUNCTION IS_CN_REVERSE_VAT_ELIGIBLE(I_vendor_id      IN     IM_INJECT_DOC_HEAD.VENDOR%TYPE,
                                        I_doc_date       IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE,
                                        I_reference_cnr  IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF3%TYPE,
                                        I_reference_invc IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF4%TYPE)
    RETURN VARCHAR2
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.IS_CN_REVERSE_VAT_ELIGIBLE';

        O_error_message VARCHAR2(2000) := NULL;
        O_is_eligible   VARCHAR2(1)    := REIM_CONSTANTS.YN_NO;
        L_retval        NUMBER         := NULL;

    BEGIN

       L_retval := REIM_TAX_DATA_SQL.CHECK_CN_REV_VAT_ELIGIBLITY(O_error_message,
                                                                 O_is_eligible,
                                                                 I_vendor_id,
                                                                 I_doc_date,
                                                                 I_reference_cnr,
                                                                 I_reference_invc);

       if L_retval = REIM_CONSTANTS.SUCCESS then
          return O_is_eligible;
       else
          return REIM_CONSTANTS.YN_NO;
       end if;

    EXCEPTION
        when OTHERS then
            return REIM_CONSTANTS.YN_NO;

    END IS_CN_REVERSE_VAT_ELIGIBLE;

     /**
     *  --------- PRIVATE PROCS AND FUNCTIONS BODIES --------------
     */

    /**
     * The function which validates Input Parameters and assign the global collection with the set of affected inject_doc_ids
     *
     * Input param: I_inject_id (Unique identifier for the injector process)
     *              I_thread_id (Thread Id, used when multi-threaded)
     *              I_inject_doc_id (Id of the Transaction that needs to be updated)
     *              I_process (Process that calls, either Validation or Insertion)
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION INIT_INJ_DOCS(O_error_message    OUT VARCHAR2,
                           I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE,
                           I_thread_id     IN     IM_INJECT_DOC_HEAD.THREAD_ID%TYPE,
                           I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                           I_process       IN     NUMBER)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.INIT_INJ_DOCS';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

        CURSOR C_FETCH_FOR_VALIDATION
        IS
        select iidh.inject_doc_id
          from im_inject_doc_head iidh
         where iidh.inject_id            = NVL(I_inject_id, iidh.inject_id)
           and iidh.thread_id            = NVL(I_thread_id, iidh.thread_id)
           and iidh.inject_doc_id        = NVL(I_inject_doc_id, iidh.inject_doc_id)
           and not exists (select 'x'
                             from im_inject_doc_error iide
                            where iide.inject_doc_id  = iidh.inject_doc_id
                              and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                          REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED));

        CURSOR C_FETCH_FOR_INJECTION
        IS
        select iidh.inject_doc_id
          from im_inject_doc_head iidh
         where iidh.inject_id            = NVL(I_inject_id, iidh.inject_id)
           and iidh.inject_doc_id        = NVL(I_inject_doc_id, iidh.inject_doc_id)
           and not exists (select 'x'
                             from im_inject_doc_error iide
                            where iide.inject_doc_id  = iidh.inject_doc_id
                              and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                          REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED));

    BEGIN

        if (I_process = PROCESS_VALIDATION) then

            if((I_inject_id is NULL or
                I_thread_id is NULL) and
                I_inject_doc_id is NULL) then
                -- TODO - Replace SQL_LIB calls with logger calls
                O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                      'Either Inject Id-Thread Id or Inject doc Id must be provided',
                                                      L_program,
                                                      TO_CHAR(SQLCODE));
                return REIM_CONSTANTS.FAIL;
            end if;

            OPEN C_FETCH_FOR_VALIDATION;
            FETCH C_FETCH_FOR_VALIDATION BULK COLLECT INTO LP_inject_doc_ids;
            CLOSE C_FETCH_FOR_VALIDATION;

        elsif (I_process = PROCESS_INJECTION) then

            if(I_inject_id is NULL and
               I_inject_doc_id is NULL) then
                -- TODO - Replace SQL_LIB calls with logger calls
                O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                      'Either Inject Id or Inject doc Id must be provided',
                                                      L_program,
                                                      TO_CHAR(SQLCODE));
                return REIM_CONSTANTS.FAIL;
            end if;

            OPEN C_FETCH_FOR_INJECTION;
            FETCH C_FETCH_FOR_INJECTION BULK COLLECT INTO LP_inject_doc_ids;
            CLOSE C_FETCH_FOR_INJECTION;

        end if;


        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END INIT_INJ_DOCS;

    /**
     * The function which  updates global varibles with their respective values.
     *  System options from both RMS and REIM
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION SET_GLOBALS(O_error_message    OUT VARCHAR2)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.SET_GLOBALS';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

        CURSOR C_FETCH_RMS_SYSTEM_OPTIONS
        IS
        select consolidation_ind
          from system_config_options;

        CURSOR C_FETCH_REIM_SYSTEM_OPTIONS
        IS
        select post_dated_doc_days,
               num_tax_allow,
               tax_validation_type,
               dflt_cost_underbill_rc,
               calc_tolerance_ind,
               NVL(calc_tolerance, REIM_CONSTANTS.ZERO), -- tolerance must be zero when not declared
               dflt_non_merch_location,
               dflt_dept,
               dflt_class,
               incl_doc_date_for_dupl_check,
               incl_doc_year_for_dupl_check,
               REIM_CONSTANTS.YN_NO --LP_allow_bfr_post_dated_days (Not allowed through EDI)
          from im_system_options;

        CURSOR C_FETCH_RTV_REASON_CODE
        IS
        select irc.reason_code_id
          from im_reason_codes irc
         where irc.reason_code_type = REIM_CONSTANTS.REASON_CODE_TYPE_RTV
           and irc.delete_ind       = REIM_CONSTANTS.YN_NO
           and rownum < 2;

        CURSOR C_FETCH_CMC_REASON_CODE
        IS
        select irc.reason_code_id
          from im_reason_codes irc
         where irc.action     = REIM_CONSTANTS.REASON_CODE_ACTION_CMC
           and irc.delete_ind = REIM_CONSTANTS.YN_NO
           and rownum < 2;
    BEGIN

        OPEN C_FETCH_RMS_SYSTEM_OPTIONS;
        FETCH C_FETCH_RMS_SYSTEM_OPTIONS into LP_consolidation_ind;
        CLOSE C_FETCH_RMS_SYSTEM_OPTIONS;

        OPEN C_FETCH_REIM_SYSTEM_OPTIONS;
        FETCH C_FETCH_REIM_SYSTEM_OPTIONS into LP_post_dated_doc_days,
                                               LP_num_tax_allow,
                                               LP_tax_validation_type,
                                               LP_dflt_cost_underbill_rc,
                                               LP_calc_tolerance_ind,
                                               LP_calc_tolerance,
                                               LP_default_loc,
                                               LP_default_dept,
                                               LP_default_class,
                                               LP_inc_date_for_dup_chk,
                                               LP_inc_year_for_dup_chk,
                                               LP_allow_bfr_post_dated_days;
        CLOSE C_FETCH_REIM_SYSTEM_OPTIONS;

        OPEN C_FETCH_RTV_REASON_CODE;
        FETCH C_FETCH_RTV_REASON_CODE into LP_rtv_reason_code;
        CLOSE C_FETCH_RTV_REASON_CODE;

        OPEN C_FETCH_CMC_REASON_CODE;
        FETCH C_FETCH_CMC_REASON_CODE into LP_cmc_reason_code;
        CLOSE C_FETCH_CMC_REASON_CODE;

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END SET_GLOBALS;

    /**
     * The function which sets ref vendor id (used internally), default location and type, terms and computes due dates for documents which do not have one.
     *
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION SET_REQUIRED_FIELDS(O_error_message    OUT VARCHAR2)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.SET_REQUIRED_FIELDS';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

    BEGIN

        -- update ref vendor id
        merge into im_inject_doc_head tgt
            using (select s.supplier_parent ref_vendor,
                          iidh.inject_doc_id
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_head iidh,
                          sups s
                    where iidh.inject_doc_id = value(ids)
                      and iidh.vendor_type   = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                      and s.supplier         = iidh.vendor
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id  = iidh.inject_doc_id
                                         and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id)
        when matched then
        update set tgt.ref_vendor = src.ref_vendor;

        --update default location and loc type
        merge into im_inject_doc_head tgt
            using (select iidh.inject_doc_id,
                          LP_default_loc loc,
                          locs.loc_type
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_head iidh,
                          (select store loc,
                                  REIM_CONSTANTS.LOC_TYPE_STORE loc_type
                             from store
                           union all
                           select wh loc,
                                  REIM_CONSTANTS.LOC_TYPE_WH loc_type
                             from wh) locs
                    where iidh.inject_doc_id = value(ids)
                      and iidh.doc_type      = REIM_CONSTANTS.DOC_TYPE_NMRCHI
                      and (   iidh.location   is NULL
                           or iidh.location = REIM_INJECTOR_SQL.NO_LOCATION)
                      and locs.loc           = LP_default_loc
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id  = iidh.inject_doc_id
                                         and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id)
        when matched then
        update set tgt.location = src.loc,
                   tgt.loc_type = src.loc_type;


        --update terms if null
        merge into im_inject_doc_head tgt
            using (select s.terms,
                          iidh.inject_doc_id
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_head iidh,
                          sups s
                    where iidh.inject_doc_id = value(ids)
                      and iidh.vendor_type   = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                      and iidh.terms         is NULL
                      and s.supplier         = iidh.vendor
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id  = iidh.inject_doc_id
                                         and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                   union all
                   select p.terms,
                          iidh.inject_doc_id
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_head iidh,
                          partner p
                    where iidh.inject_doc_id = value(ids)
                      and iidh.vendor_type   <> REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                      and iidh.terms         is NULL
                      and p.partner_id       = iidh.vendor
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id  = iidh.inject_doc_id
                                         and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id)
        when matched then
        update set tgt.terms = src.terms;

        --update due_date if null
        merge into im_inject_doc_head tgt
        using(select
                     case
                         when d.fixed_date is NOT NULL then
                              d.fixed_date
                         when Nvl(d.discdays, 0) > 0 then
                              case
                                  when iidh.doc_date + d.discdays >= get_vdate then
                                       iidh.doc_date + d.discdays
                                  else
                                       iidh.doc_date + d.duedays
                              end
                         when Nvl(d.duedays, 0) > 0  then
                              iidh.doc_date + d.duedays
                         when Nvl(d.disc_dom, 0) > 0 then
                              case
                                  when LP_vdate <=
                                                  (case
                                                      when d.cutoff_day > 0 and d.cutoff_day <= to_char(Add_Months(iidh.doc_date, d.disc_mm_fwd),'DD') then
                                                          case
                                                              when d.disc_dom = 31 then
                                                                  Last_Day(Trunc(Add_Months(Add_Months(iidh.doc_date, d.disc_mm_fwd),1), 'MON'))
                                                              else
                                                                  To_Date(d.disc_dom || '-' || To_Char(Trunc(Add_Months(Add_Months(iidh.doc_date, d.disc_mm_fwd),1), 'MON'),'MM-YYYY'),'DD-MM-YYYY')
                                                          end
                                                      else
                                                          case
                                                              when d.disc_dom = 31 then
                                                                  Last_Day(Trunc(Add_Months(iidh.doc_date, d.disc_mm_fwd), 'MON'))
                                                              else
                                                                  To_Date(d.disc_dom || '-' || To_Char(Trunc(Add_Months(iidh.doc_date, d.disc_mm_fwd), 'MON'),'MM-YYYY'),'DD-MM-YYYY')
                                                          end
                                                   end) then
                                      case
                                          when d.cutoff_day > 0 and d.cutoff_day <= to_char(Add_Months(iidh.doc_date, d.disc_mm_fwd),'DD') then
                                              case
                                                  when d.disc_dom = 31 then
                                                      Last_Day(Trunc(Add_Months(Add_Months(iidh.doc_date, d.disc_mm_fwd),1), 'MON'))
                                                  else
                                                      To_Date(d.disc_dom || '-' || To_Char(Trunc(Add_Months(Add_Months(iidh.doc_date, d.disc_mm_fwd),1), 'MON'),'MM-YYYY'),'DD-MM-YYYY')
                                              end
                                          else
                                              case
                                                  when d.disc_dom = 31 then
                                                      Last_Day(Trunc(Add_Months(iidh.doc_date, d.disc_mm_fwd), 'MON'))
                                                  else
                                                      To_Date(d.disc_dom || '-' || To_Char(Trunc(Add_Months(iidh.doc_date, d.disc_mm_fwd), 'MON'),'MM-YYYY'),'DD-MM-YYYY')
                                              end
                                      end
                                  when Nvl(d.due_dom, 0) > 0  then
                                      case
                                          when d.cutoff_day > 0 and d.cutoff_day <= to_char(Add_Months(iidh.doc_date, d.due_mm_fwd),'DD') then
                                              case
                                                  when d.due_dom = 31 then
                                                      Last_Day(Trunc(Add_Months(Add_Months(iidh.doc_date, d.due_mm_fwd),1), 'MON'))
                                                  else
                                                      To_Date(d.due_dom || '-' || To_Char(Trunc(Add_Months(Add_Months(iidh.doc_date, d.due_mm_fwd),1), 'MON'),'MM-YYYY'),'DD-MM-YYYY')
                                              end
                                          else
                                              case
                                                  when d.due_dom = 31 then
                                                      Last_Day(Trunc(Add_Months(iidh.doc_date, d.disc_mm_fwd), 'MON'))
                                                  else
                                                      To_Date(d.due_dom || '-' || To_Char(Trunc(Add_Months(iidh.doc_date, d.due_mm_fwd), 'MON'),'MM-YYYY'),'DD-MM-YYYY')
                                              end
                                      end
                                  else
                                      iidh.doc_date
                              end
                         when Nvl(d.due_dom, 0) > 0 then
                              case
                                  when d.cutoff_day > 0 and d.cutoff_day <= to_char(Add_Months(iidh.doc_date, d.due_mm_fwd),'DD') then
                                      case
                                          when d.due_dom = 31 then
                                              Last_Day(Trunc(Add_Months(Add_Months(iidh.doc_date, d.due_mm_fwd),1), 'MON'))
                                          else
                                              To_Date(d.due_dom || '-' || To_Char(Trunc(Add_Months(Add_Months(iidh.doc_date, d.due_mm_fwd),1), 'MON'),'MM-YYYY'),'DD-MM-YYYY')
                                      end
                                  else
                                      case
                                          when d.due_dom = 31 then
                                              Last_Day(Trunc(Add_Months(iidh.doc_date, d.disc_mm_fwd), 'MON'))
                                          else
                                              To_Date(d.due_dom || '-' || To_Char(Trunc(Add_Months(iidh.doc_date, d.due_mm_fwd), 'MON'),'MM-YYYY'),'DD-MM-YYYY')
                                      end
                              end
                         else
                             iidh.doc_date
                     end AS due_date,
                     iidh.inject_doc_id
                from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     im_inject_doc_head iidh,
                     terms_head h,
                     terms_detail d
               where iidh.inject_doc_id = value(ids)
                 and iidh.due_date      is NULL
                 and iidh.terms         is NOT NULL
                 and h.terms            = iidh.terms
                 and d.terms            = h.terms
                 and d.enabled_flag     = REIM_CONSTANTS.YN_YES
                 and iidh.doc_date      between nvl(d.start_date_active, iidh.doc_date) and nvl(d.end_date_active, iidh.doc_date)
                 and not exists (select 'x'
                                   from im_inject_doc_error iide
                                  where iide.inject_doc_id  = iidh.inject_doc_id
                                    and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id)
        when matched then
        update set tgt.due_date = src.due_date;

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END SET_REQUIRED_FIELDS;

    /**
     * The function which computes due dates for documents which do not have one.
     *
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION COMPUTE_EXCH_RATE(O_error_message    OUT VARCHAR2)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.COMPUTE_EXCH_RATE';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

    BEGIN

        merge into im_inject_doc_head tgt
        using (select iidh.inject_doc_id,
                      oh.exchange_rate
                 from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      im_inject_doc_head iidh,
                      ordhead oh
                where iidh.inject_doc_id = value(ids)
                  and iidh.exchange_rate is NULL
                  and iidh.RTV_IND       = REIM_CONSTANTS.YN_NO
                  and iidh.order_no      is NOT NULL
                  and oh.order_no        = iidh.order_no
                  and not exists (select 'x'
                                    from im_inject_doc_error iide
                                   where iide.inject_doc_id  = iidh.inject_doc_id
                                     and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                 REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id)
        when matched then
        update set tgt.exchange_rate = src.exchange_rate;

        merge into im_inject_doc_head tgt
        using (select iidh.inject_doc_id,
                      cr.exchange_rate
                 from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      im_inject_doc_head iidh,
                      currency_rates cr
                where iidh.inject_doc_id = value(ids)
                  and iidh.exchange_rate is NULL
                  and cr.currency_code   = iidh.currency_code
                  and cr.exchange_type   = DECODE(LP_consolidation_ind,
                                                  REIM_CONSTANTS.YN_YES, REIM_CONSTANTS.EXCH_TYPE_CONSOLIDATED,
                                                  REIM_CONSTANTS.EXCH_TYPE_OPERATIONAL)
                  and cr.effective_date  = (select max(cri.effective_date)
                                              from currency_rates cri
                                             where cri.currency_code  = iidh.currency_code
                                               and cri.exchange_type  = DECODE(LP_consolidation_ind,
                                                                               REIM_CONSTANTS.YN_YES, REIM_CONSTANTS.EXCH_TYPE_CONSOLIDATED,
                                                                               REIM_CONSTANTS.EXCH_TYPE_OPERATIONAL)
                                               and cri.effective_date <= iidh.doc_date)
                  and not exists (select 'x'
                                    from im_inject_doc_error iide
                                   where iide.inject_doc_id  = iidh.inject_doc_id
                                     and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                 REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id)
        when matched then
        update set tgt.exchange_rate = src.exchange_rate;

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END COMPUTE_EXCH_RATE;


    /**
     * The function which calculates average cost for duplicate items in a consignment invoice.
     * Duplicates are then removed.
     *
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION HANDLE_DUPS_ON_CONSIGNMENTS(O_error_message    OUT VARCHAR2)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.HANDLE_DUPS_ON_CONSIGNMENTS';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

    BEGIN

        delete from gtt_10_num_10_str_10_date;
        insert into gtt_10_num_10_str_10_date(number_1, -- inject_id
                                              number_2, -- inject_doc_id
                                              number_3, -- detail_id
                                              number_4, -- detail_rn
                                              number_5, -- total_qty
                                              number_6, -- total_tax_basis
                                              number_7, -- avg_unit_cost
                                              varchar2_1) -- item
           select iidd_inner.inject_id,
                  iidd_inner.inject_doc_id,
                  iidd_inner.detail_id,
                  iidd_inner.detail_rn,
                  iidd_inner.total_qty,
                  iidd_inner.total_tax_basis,
                  (iidd_inner.total_tax_basis / iidd_inner.total_qty),
                  iidd_inner.item
             from (select iidh.inject_id,
                          iidh.inject_doc_id,
                          iidd.detail_id,
                          rank() OVER (PARTITION BY iidd.inject_doc_id, iidd.item ORDER BY iidd.detail_id) detail_rn,
                          SUM(iidd.qty) OVER (PARTITION BY iidd.inject_doc_id, iidd.item) total_qty,
                          ROUND(SUM(iidd.unit_cost * iidd.qty) OVER (PARTITION BY iidd.inject_doc_id, iidd.item), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) total_tax_basis,
                          iidd.item,
                          COUNT(iidd.detail_id) OVER (PARTITION BY iidd.inject_doc_id, iidd.item) item_count
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_head iidh,
                          im_inject_doc_detail iidd
                    where iidh.inject_doc_id = value(ids)
                      and iidh.merch_type    = REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT
                      and iidh.inject_doc_id = iidd.inject_doc_id
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id  = iidh.inject_doc_id
                                         and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) iidd_inner
            where iidd_inner.item_count > 1;

        merge into im_inject_doc_detail tgt
        using (select gtt.number_1 inject_id,
                      gtt.number_2 inject_doc_id,
                      gtt.number_3 detail_id,
                      gtt.number_4 detail_rn,
                      gtt.number_5 total_qty,
                      gtt.number_6 total_tax_basis,
                      gtt.number_7 avg_unit_cost,
                      gtt.varchar2_1 item
                 from gtt_10_num_10_str_10_date gtt
                where number_4 = REIM_CONSTANTS.ONE) src
        on (    src.inject_id     = tgt.inject_id
            and src.inject_doc_id = tgt.inject_doc_id
            and src.detail_id     = tgt.detail_id)
        when matched then
        update set tgt.unit_cost = src.avg_unit_cost,
                   tgt.qty       = src.total_qty;

        merge into im_inject_doc_detail_tax tgt
        using (select gtt.number_1 inject_id,
                      gtt.number_2 inject_doc_id,
                      gtt.number_3 detail_id,
                      gtt.number_4 detail_rn,
                      gtt.number_5 total_qty,
                      gtt.number_6 total_tax_basis,
                      gtt.number_7 avg_unit_cost,
                      gtt.varchar2_1 item
                 from gtt_10_num_10_str_10_date gtt
                where number_4 = REIM_CONSTANTS.ONE) src
        on (src.detail_id     = tgt.detail_id)
        when matched then
        update set tgt.tax_basis  = src.total_tax_basis,
                   tgt.tax_amount = src.total_tax_basis * (tgt.tax_rate/100);

        delete
          from im_inject_doc_detail_tax iiddt
         where exists (select 'x'
                         from gtt_10_num_10_str_10_date gtt
                        where gtt.number_4 > 1
                          and gtt.number_3 = iiddt.detail_id);

        delete
          from im_inject_doc_detail iidd
         where exists (select 'x'
                         from gtt_10_num_10_str_10_date gtt
                        where gtt.number_4 > 1
                          and gtt.number_2 = iidd.inject_doc_id
                          and gtt.number_3 = iidd.detail_id);

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END HANDLE_DUPS_ON_CONSIGNMENTS;


    /**
     * The function which updates fixable ind in IM_INJECTDOC_ERROR to 'N'
     *     If the document belongs to the following categories
     *         a. RTV
     *         b. CONSIGNMENT
     *         c. VENDOR TYPE is NOT supplier
     *
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION UPDATE_FIXABLE_IND(O_error_message    OUT VARCHAR2)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.UPDATE_FIXABLE_IND';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

    BEGIN
        -- TODO write a query to update fixable for the pre validation checks (just for those which are null)
--        merge into im_inject_doc_error tgt
--        using (select rule,
--                      fixable_default
--                 from im_inject_doc_rule) src
--        on (    src.rule    = tgt.rule
--            and tgt.fixable is NULL
--            and tgt.status  = REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW
--            and exists (select 'x'
--                          from im_inject_doc_head iidh
--                         where iidh.inject_doc_id = tgt.inject_doc_id
--                           and iidh.inject_id     = NVL(I_inject_id, iidh.inject_id)
--                           and iidh.inject_doc_id = NVL(I_inject_doc_id, iidh.inject_doc_id)))
--        when matched then
--        update set tgt.fixable = src.fixable_default;
        update im_inject_doc_error iide
           set iide.fixable = REIM_CONSTANTS.YN_NO
         where iide.fixable = REIM_CONSTANTS.YN_YES
           and EXISTS (select 'x'
                         from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                              im_inject_doc_head iidh
                        where value(ids) = iide.inject_doc_id
                          and iidh.inject_doc_id = value(ids)
                          and (iidh.rtv_ind      = REIM_CONSTANTS.YN_YES OR
                               iidh.merch_type  IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                   REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                   REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) OR
                               iidh.vendor_type != REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER));

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END UPDATE_FIXABLE_IND;

    /**
     * The function which derives item from VPN/UPC.
     *
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION DERIVE_ITEM(O_error_message    OUT VARCHAR2,
                         I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.DERIVE_ITEM';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

    BEGIN

        --Derive item from UPC
        merge into im_inject_doc_detail tgt
            using (select iidd.inject_doc_id,
                          iidd.detail_id,
                          DECODE(im.item_level,
                                 im.tran_level, im.item,
                                 im.item_parent) item
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_detail iidd,
                          item_master im
                    where iidd.inject_doc_id = value(ids)
                      and iidd.item_source   = REIM_CONSTANTS.ITEM_SRC_UPC
                      and iidd.item       is NULL
                      and im.item            = iidd.upc
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id  = iidd.inject_doc_id
                                         and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id and
            src.detail_id     = tgt.detail_id)
        when matched then
        update set tgt.item = src.item;

        --Derive item from VPN (direct)
        merge into im_inject_doc_detail tgt
            using (select iidd.inject_doc_id,
                          iidd.detail_id,
                          its.item item
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_head iidh,
                          im_inject_doc_detail iidd,
                          ordhead oh,
                          item_supplier its
                    where iidh.inject_doc_id = value(ids)
                      and iidd.inject_doc_id = iidh.inject_doc_id
                      and iidd.item_source   = REIM_CONSTANTS.ITEM_SRC_VPN
                      and iidd.item       is NULL
                      and oh.order_no        = iidh.order_no
                      and its.supplier       = oh.supplier
                      and its.vpn            = iidd.vpn
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id  = iidd.inject_doc_id
                                         and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id and
            src.detail_id     = tgt.detail_id)
        when matched then
        update set tgt.item = src.item;

        --Derive item from VPN (supplier group)
        merge into im_inject_doc_detail tgt
            using (select distinct iidd.inject_doc_id,
                          iidd.detail_id,
                          its.item item
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_head iidh,
                          im_inject_doc_detail iidd,
                          ordhead oh,
                          sups s_oh,
                          im_supplier_group_members isgm_oh,
                          im_supplier_group_members isgm_its,
                          sups s_its,
                          item_supplier its
                    where iidh.inject_doc_id = value(ids)
                      and iidd.inject_doc_id = iidh.inject_doc_id
                      and iidd.item_source   = REIM_CONSTANTS.ITEM_SRC_VPN
                      and iidd.item       is NULL
                      and oh.order_no        = iidh.order_no
                      and s_oh.supplier      = oh.supplier
                      and isgm_oh.supplier   = s_oh.supplier_parent
                      and isgm_its.group_id  = isgm_oh.group_id
                      and (s_its.supplier_parent = isgm_its.supplier
                           or s_its.supplier     = isgm_its.supplier)
                      and its.supplier       = s_its.supplier
                      and its.vpn            = iidd.vpn
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id  = iidd.inject_doc_id
                                         and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (src.inject_doc_id = tgt.inject_doc_id and
            src.detail_id     = tgt.detail_id)
        when matched then
        update set tgt.item = src.item;

        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        detail_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidd.inject_id,
                   iidd.inject_doc_id,
                   iidd.detail_id,
                   iidr.rule,
                   I_trial_id,
                   'Inject doc id = ' || iidd.inject_doc_id || ' and VPN or UPC = ' || DECODE(iidd.item_source,
                                                                                              REIM_CONSTANTS.ITEM_SRC_UPC, iidd.upc,
                                                                                              REIM_CONSTANTS.ITEM_SRC_VPN, iidd.vpn) error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   im_inject_doc_detail iidd,
                   im_inject_doc_rule iidr
             where iidd.inject_doc_id = value(ids)
               and iidd.item_source   IN (REIM_CONSTANTS.ITEM_SRC_UPC,
                                          REIM_CONSTANTS.ITEM_SRC_VPN)
               and iidd.item          is NULL
               and iidr.rule          = UNABLE_TO_RETRIEVE_ITEM
               and not exists (select 'x'
                                 from im_inject_doc_error iide
                                where iide.inject_doc_id  = iidd.inject_doc_id
                                  and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                              REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED));

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END DERIVE_ITEM;


    /**
     * The function which performs all tax related validations.
     * A separate function so that all configurable/multi tax changes can be done at one place.
     *
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION VALIDATE_TRAN_TAX(O_error_message     OUT VARCHAR2,
                               I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.VALIDATE_TRAN_TAX';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

        L_obj_vendor_tax_tbl OBJ_VENDOR_TAX_DATA_TBL := NULL;

        L_obj_all_vendors_tax_tbl OBJ_VENDOR_TAX_DATA_TBL := OBJ_VENDOR_TAX_DATA_TBL();

        L_obj_item_tax_crit_tbl OBJ_ITEM_TAX_CRITERIA_TBL := NULL;
        L_obj_item_tax_calc_ovrd_tbl OBJ_ITEM_TAX_CALC_OVRD_TBL := OBJ_ITEM_TAX_CALC_OVRD_TBL();
        L_obj_item_tax_break_tbl OBJ_ITEM_TAX_BREAK_TBL := NULL;

        cursor C_FETCH_VEND_DOC_DATE is
        with locs as (select store loc,
                             REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                             vat_region
                        from store
                       union all
                      select wh loc,
                             REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                             vat_region
                        from wh)
        select distinct iidh.vendor,
               iidh.vendor_type,
               iidh.doc_date
          from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
               im_inject_doc_head iidh,
               sups s,
               locs,
               vat_region vr_loc,
               vat_region vr_sups
         where iidh.inject_doc_id    = value(ids)
           and iidh.vendor_type      = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
           and iidh.vendor           = s.supplier
           and iidh.location         = locs.loc
           and iidh.loc_type         = locs.loc_type
           and locs.vat_region       = vr_loc.vat_region
           and s.vat_region          = vr_sups.vat_region
           and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT NOT IN (vr_loc.vat_calc_type,
                                                           vr_sups.vat_calc_type)
           and not exists (select 'x'
                             from im_inject_doc_error iide
                            where iide.inject_doc_id  = iidh.inject_doc_id
                              and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                          REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
        union all
        select distinct iidh.vendor,
               iidh.vendor_type,
               iidh.doc_date
          from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
               im_inject_doc_head iidh,
               partner p,
               locs,
               vat_region vr_loc,
               vat_region vr_part
         where iidh.inject_doc_id    = value(ids)
           and iidh.vendor_type      <> REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
           and iidh.vendor           = p.partner_id
           and iidh.location         = locs.loc
           and iidh.loc_type         = locs.loc_type
           and locs.vat_region       = vr_loc.vat_region
           and p.vat_region          = vr_part.vat_region
           and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT NOT IN (vr_loc.vat_calc_type,
                                                           vr_part.vat_calc_type)
           and not exists (select 'x'
                             from im_inject_doc_error iide
                            where iide.inject_doc_id  = iidh.inject_doc_id
                              and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                          REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED));

        cursor C_FETCH_ITEM_TAX_CRIT is
        select OBJ_ITEM_TAX_CRITERIA_REC(item,
                                         vendor,
                                         location,
                                         doc_date,
                                         unit_cost,
                                         NULL, -- mrp
                                         NULL, -- retail
                                         NULL, -- freight
                                         NULL) -- tax_type
        from (select distinct iidd.item, -- Using a distinct to avoid massive dataset during comparison
                     iidh.vendor,
                     iidh.location,
                     iidh.doc_date,
                     iidd.unit_cost
                from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     im_inject_doc_head iidh,
                     sups s,
                     (select store loc,
                             REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                             vat_region
                        from store
                      union all
                      select wh loc,
                             REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                             vat_region
                        from wh) locs,
                     vat_region vr,
                     im_inject_doc_detail iidd
               where iidh.inject_doc_id = value(ids)
                 and iidh.doc_type      = REIM_CONSTANTS.DOC_TYPE_MRCHI
                 and iidh.vendor_type   = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                 and iidh.multi_loc_ind = REIM_CONSTANTS.YN_NO
                 and s.supplier         = iidh.vendor
                 and locs.loc           = iidh.location
                 and locs.loc_type      = iidh.loc_type
                 and locs.vat_region    = s.vat_region
                 and vr.vat_region      = locs.vat_region
                 and vr.vat_calc_type   <> REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT
                 and iidd.inject_doc_id = iidh.inject_doc_id
                 and not exists (select 'x'
                                   from im_inject_doc_error iide
                                  where iide.inject_doc_id  = iidh.inject_doc_id
                                    and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED)));

    BEGIN

        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select docs.inject_id,
                   docs.inject_doc_id,
                   EXEMPT_DOCS_WITH_TAX,
                   I_trial_id,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (with locs as (select store loc,
                                         REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                         vat_region
                                    from store
                                   union all
                                  select wh loc,
                                         REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                         vat_region
                                    from wh)
                    select iidh.inject_id,
                           iidh.inject_doc_id
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_head iidh,
                           sups s,
                           locs,
                           vat_region vr_loc,
                           vat_region vr_sups
                     where iidh.inject_doc_id    = value(ids)
                       and iidh.vendor_type      = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                       and iidh.vendor           = s.supplier
                       and iidh.location         = locs.loc
                       and iidh.loc_type         = locs.loc_type
                       and locs.vat_region       = vr_loc.vat_region
                       and s.vat_region          = vr_sups.vat_region
                       and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT IN (vr_loc.vat_calc_type,
                                                                   vr_sups.vat_calc_type)
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidh.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                    union all
                    select iidh.inject_id,
                           iidh.inject_doc_id
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_head iidh,
                           partner p,
                           locs,
                           vat_region vr_loc,
                           vat_region vr_part
                     where iidh.inject_doc_id   = value(ids)
                       and iidh.vendor_type     <> REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                       and iidh.vendor          = p.partner_id
                       and iidh.location        = locs.loc
                       and iidh.loc_type        = locs.loc_type
                       and locs.vat_region      = vr_loc.vat_region
                       and p.vat_region         = vr_part.vat_region
                       and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT IN (vr_loc.vat_calc_type,
                                                                   vr_part.vat_calc_type)
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidh.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) docs,
                   im_inject_doc_rule iidr
             where iidr.rule = EXEMPT_DOCS_WITH_TAX
               and (   EXISTS (select 'x'
                                from im_inject_doc_tax iidt
                               where iidt.inject_doc_id = docs.inject_doc_id)
                    or EXISTS (select 'x'
                                 from im_inject_doc_detail iidd,
                                      im_inject_doc_detail_tax iiddt
                                where iidd.inject_doc_id = docs.inject_doc_id
                                  and iiddt.detail_id    = iidd.detail_id)
                    or EXISTS (select 'x'
                                 from im_inject_doc_non_merch_tax iidnmt
                                where iidnmt.inject_doc_id = docs.inject_doc_id));

        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        detail_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidt_inner.inject_id,
                   iidt_inner.inject_doc_id,
                   iidt_inner.detail_id,
                   iidt_inner.rule,
                   I_trial_id,
                   'Tax code = ' || iidt_inner.tax_code error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (select iidd.inject_id,
                           iidd.inject_doc_id,
                           iidd.detail_id,
                           iiddt.tax_code,
                           DTL_TAX_NOT_IN_HDR rule
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_detail iidd,
                           im_inject_doc_detail_tax iiddt
                     where iidd.inject_doc_id = value(ids)
                       and iiddt.detail_id    = iidd.detail_id
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id      = iidd.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                       and not exists (select 'x'
                                         from im_inject_doc_tax iidt
                                        where iidt.inject_doc_id = iidd.inject_doc_id
                                          and iidt.tax_code      = iiddt.tax_code)
                    union all
                    select iidnmt.inject_id,
                           iidnmt.inject_doc_id,
                           NULL detail_id,
                           iidnmt.tax_code,
                           DTL_NM_TAX_NOT_IN_HDR rule
                      from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           im_inject_doc_non_merch_tax iidnmt
                     where iidnmt.inject_doc_id = value(ids)
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id      = iidnmt.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                       and not exists (select 'x'
                                         from im_inject_doc_tax iidt
                                        where iidt.inject_doc_id = iidnmt.inject_doc_id
                                          and iidt.tax_code      = iidnmt.tax_code)) iidt_inner,
                   im_inject_doc_rule iidr
             where iidt_inner.rule = iidr.rule;

        --Allowance Tax Validation
        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        detail_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidd.inject_id,
                   iidd.inject_doc_id,
                   iidd.detail_id,
                   iidr.rule,
                   I_trial_id,
                   'Allowance code = ' || iidda.allowance_code error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   USER -- to be replaced by input batch user
              from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   im_inject_doc_detail iidd,
                   im_inject_doc_detail_allowance iidda,
                   im_inject_doc_rule iidr
             where iidd.inject_doc_id = value(ids)
               and iidd.detail_id     = iidda.detail_id
               and iidr.rule          = ALLW_TAX
               and not exists (select 'x'
                                 from im_inject_doc_error iide
                                where iide.inject_doc_id  = iidd.inject_doc_id
                                  and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                              REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
               and not exists (select 'x'
                                 from im_inject_doc_detail_allow_tax iiddat
                                where iiddat.detail_id      = iidda.detail_id
                                  and iiddat.allowance_code = iidda.allowance_code);

        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidh_inner.inject_id,
                   iidh_inner.inject_doc_id,
                   iidh_inner.rule,
                   I_trial_id,
                   NULL error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (with
                        locs as
                        (select store loc,
                                REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                vat_region
                           from store
                          union all
                         select wh loc,
                                REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                vat_region
                           from wh),
                        docs as
                        (select iidh.inject_id,
                                iidh.inject_doc_id,
                                iidh.doc_type,
                                iidh.location loc,
                                iidh.loc_type,
                                iidh.vendor,
                                iidh.vendor_type,
                                iidh.total_cost,
                                iidh.total_tax_amount
                           from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                im_inject_doc_head iidh
                          where iidh.inject_doc_id = value(ids)
                            and not exists (select 'x'
                                                 from im_inject_doc_error iide
                                                where iide.inject_doc_id  = iidh.inject_doc_id
                                                  and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                              REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED)))
                    select d.inject_id,
                           d.inject_doc_id,
                           case
                                when l.vat_region = s.vat_region
                                 and vr_loc.vat_calc_type <> REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT
                                 and vr_sup.vat_calc_type <> REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT and not exists (select 'x'
                                                                                                                   from im_inject_doc_tax iidt
                                                                                                                  where iidt.inject_doc_id = d.inject_doc_id) then
                                    TAX_REQUIRED
                                when l.vat_region <> s.vat_region and exists (select 'x'
                                                                                from im_inject_doc_tax iidt
                                                                               where iidt.inject_doc_id = d.inject_doc_id
                                                                                 and iidt.tax_rate      <> REIM_CONSTANTS.ZERO) then
                                    ZERO_TAX_REQUIRED
                                when l.vat_region <> s.vat_region and d.doc_type <> REIM_CONSTANTS.DOC_TYPE_NMRCHI
                                 and vr_loc.acquisition_vat_ind = REIM_CONSTANTS.YN_YES
                                 and vr_sup.vat_region_type in (REIM_CONSTANTS.TAX_REGION_EU_BASE,
                                                                REIM_CONSTANTS.TAX_REGION_EU_MEMBER) and not exists (select 'x'
                                                                                                                       from im_inject_doc_detail iidd
                                                                                                                      where iidd.inject_doc_id  = d.inject_doc_id) then
                                    ACQ_TAX_DETAIL_REQ
                                when l.vat_region <> s.vat_region and NVL(d.total_tax_amount, REIM_CONSTANTS.ZERO) <>  REIM_CONSTANTS.ZERO then
                                    TAX_AMT_NON_ZERO
                           end as rule
                      from docs d,
                           locs l,
                           sups s,
                           vat_region vr_loc,
                           vat_region vr_sup
                     where d.vendor_type     = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                       and d.loc             = l.loc
                       and d.loc_type        = l.loc_type
                       and d.vendor          = s.supplier
                       and vr_loc.vat_region = l.vat_region
                       and vr_sup.vat_region = s.vat_region
                    union all
                    select d.inject_id,
                           d.inject_doc_id,
                           case
                                when l.vat_region = p.vat_region
                                 and vr_loc.vat_calc_type <> REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT
                                 and vr_part.vat_calc_type <> REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT  and not exists (select 'x'
                                                                                                                    from im_inject_doc_tax iidt
                                                                                                                   where iidt.inject_doc_id = d.inject_doc_id) then
                                    TAX_REQUIRED
                                when l.vat_region <> p.vat_region and exists (select 'x'
                                                                                from im_inject_doc_tax iidt
                                                                               where iidt.inject_doc_id = d.inject_doc_id
                                                                                 and iidt.tax_rate <> REIM_CONSTANTS.ZERO) then
                                    ZERO_TAX_REQUIRED
                                when l.vat_region <> p.vat_region and NVL(d.total_tax_amount, REIM_CONSTANTS.ZERO) <>  REIM_CONSTANTS.ZERO then
                                    TAX_AMT_NON_ZERO
                           end as rule
                      from docs d,
                           locs l,
                           partner p,
                           vat_region vr_loc,
                           vat_region vr_part
                     where d.vendor_type      <> REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                       and d.loc              = l.loc
                       and d.loc_type         = l.loc_type
                       and d.vendor           = p.partner_id
                       and vr_loc.vat_region  = l.vat_region
                       and vr_part.vat_region = p.vat_region) iidh_inner,
                   im_inject_doc_rule iidr
             where iidh_inner.rule = iidr.rule;

        for rec in C_FETCH_VEND_DOC_DATE loop

            REIM_TAX_DATA_SQL.GET_VENDOR_TAX_CODES(O_status,
                                                   O_error_message,
                                                   obj_vendor_data_tbl(obj_vendor_data_rec(rec.vendor, rec.vendor_type)),
                                                   rec.doc_date,
                                                   NULL, --I_TAX_TYPE
                                                   L_obj_vendor_tax_tbl);

            if (O_status <> REIM_CONSTANTS.SUCCESS) then
                return O_status;
            end if;

            if (L_obj_vendor_tax_tbl IS NULL OR L_obj_vendor_tax_tbl.Count = 0) then
                O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                    'Unable to retrieve Vendor Taxes for vendor: ' || rec.vendor,
                                    L_program,
                                    TO_CHAR(SQLCODE));
                return REIM_CONSTANTS.FAIL;
            end if;

            --add all vendor taxes to a single object to reduce looping later during validation.
            for i in 1 .. L_obj_vendor_tax_tbl.count loop
                L_obj_all_vendors_tax_tbl.EXTEND();
                L_obj_all_vendors_tax_tbl(L_obj_all_vendors_tax_tbl.COUNT) := L_obj_vendor_tax_tbl(i);
            end loop;

            L_obj_vendor_tax_tbl := NULL;

        end loop;

        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidt_inner.inject_id,
                   iidt_inner.inject_doc_id,
                   iidt_inner.rule,
                   I_trial_id,
                   iidt_inner.error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (with im_inj_doc_header as
                    (select iidh.inject_id,
                            iidh.inject_doc_id,
                            iidh.vendor,
                            iidh.vendor_type,
                            iidh.doc_date
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_head iidh
                      where iidh.inject_doc_id = value(ids)
                        and not exists (select 'x'
                                          from im_inject_doc_error iide
                                         where iide.inject_doc_id  = iidh.inject_doc_id
                                           and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                       REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED)))
                    select iidh.inject_id,
                           iidt.inject_doc_id,
                           VALID_HDR_TAX rule,
                           'Header Tax code = ' || iidt.tax_code error_context
                      from im_inj_doc_header iidh,
                           im_inject_doc_tax iidt
                     where iidt.inject_doc_id = iidh.inject_doc_id
                       and not exists (select 'x'
                                         from table(cast(L_obj_all_vendors_tax_tbl as OBJ_VENDOR_TAX_DATA_TBL)) tbl
                                        where tbl.vendor_id   = iidh.vendor
                                          and tbl.vendor_type = iidh.vendor_type
                                          and tbl.eff_date    = iidh.doc_date
                                          and tbl.tax_code    = iidt.tax_code)
                    union all
                    select iidh.inject_id,
                           iidd.inject_doc_id,
                           VALID_DTL_TAX rule,
                           'Tax code = ' || iiddt.tax_code || ' for Item/vpn/upc = ' || decode(iidd.item_source,
                                                                                             REIM_CONSTANTS.ITEM_SRC_ITEM_ID, iidd.item,
                                                                                             REIM_CONSTANTS.ITEM_SRC_VPN, iidd.vpn,
                                                                                             REIM_CONSTANTS.ITEM_SRC_UPC, iidd.upc) error_context
                      from im_inj_doc_header iidh,
                           im_inject_doc_detail iidd,
                           im_inject_doc_detail_tax iiddt
                     where iidd.inject_doc_id = iidh.inject_doc_id
                       and iiddt.detail_id    = iidd.detail_id
                       and not exists (select 'x'
                                         from table(cast(L_obj_all_vendors_tax_tbl as OBJ_VENDOR_TAX_DATA_TBL)) tbl
                                        where tbl.vendor_id   = iidh.vendor
                                          and tbl.vendor_type = iidh.vendor_type
                                          and tbl.eff_date    = iidh.doc_date
                                          and tbl.tax_code    = iiddt.tax_code)
                    union all
                    select iidh.inject_id,
                           iidnmt.inject_doc_id,
                           VALID_DTL_NM_TAX rule,
                           'Tax code = ' || iidnmt.tax_code || ' for Non Merchandise Code = ' || iidnmt.non_merch_code error_context
                      from im_inj_doc_header iidh,
                           im_inject_doc_non_merch iidnm,
                           im_inject_doc_non_merch_tax iidnmt
                     where iidnm.inject_doc_id = iidh.inject_doc_id
                       and iidnm.inject_doc_id = iidnmt.inject_doc_id
                       and iidnm.non_merch_code = iidnmt.non_merch_code
                       and not exists (select 'x'
                                         from table(cast(L_obj_all_vendors_tax_tbl as OBJ_VENDOR_TAX_DATA_TBL)) tbl
                                        where tbl.vendor_id   = iidh.vendor
                                          and tbl.vendor_type = iidh.vendor_type
                                          and tbl.eff_date    = iidh.doc_date
                                          and tbl.tax_code    = iidnmt.tax_code)) iidt_inner,
                   im_inject_doc_rule iidr
             where iidt_inner.rule = iidr.rule;

        L_obj_all_vendors_tax_tbl := NULL;

        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidt_inner.inject_id,
                   iidt_inner.inject_doc_id,
                   iidt_inner.rule,
                   I_trial_id,
                   NULL error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (with locs as (select store loc,
                                         REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                         vat_region
                                    from store
                                   union all
                                  select wh loc,
                                         REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                         vat_region
                                    from wh),
                    doc_hdr as
                    (select iidh.inject_id,
                            iidh.inject_doc_id,
                            iidh.doc_type,
                            iidh.total_cost,
                            iidh.total_tax_amount
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_head iidh,
                            sups s,
                            locs,
                            vat_region vr_loc,
                            vat_region vr_sups
                      where iidh.inject_doc_id    = value(ids)
                        and iidh.vendor_type      = REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                        and iidh.vendor           = s.supplier
                        and iidh.location         = locs.loc
                        and iidh.loc_type         = locs.loc_type
                        and locs.vat_region       = vr_loc.vat_region
                        and s.vat_region          = vr_sups.vat_region
                        and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT NOT IN (vr_loc.vat_calc_type,
                                                                        vr_sups.vat_calc_type)
                        and not exists (select 'x'
                                          from im_inject_doc_error iide
                                         where iide.inject_doc_id  = iidh.inject_doc_id
                                           and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                       REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                     union all
                     select iidh.inject_id,
                            iidh.inject_doc_id,
                            iidh.doc_type,
                            iidh.total_cost,
                            iidh.total_tax_amount
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_head iidh,
                            partner p,
                            locs,
                            vat_region vr_loc,
                            vat_region vr_part
                      where iidh.inject_doc_id    = value(ids)
                        and iidh.vendor_type      <> REIM_CONSTANTS.VENDOR_TYPE_SUPPLIER
                        and iidh.vendor           = p.partner_id
                        and iidh.location         = locs.loc
                        and iidh.loc_type         = locs.loc_type
                        and locs.vat_region       = vr_loc.vat_region
                        and p.vat_region          = vr_part.vat_region
                        and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT NOT IN (vr_loc.vat_calc_type,
                                                                        vr_part.vat_calc_type)
                        and not exists (select 'x'
                                          from im_inject_doc_error iide
                                         where iide.inject_doc_id  = iidh.inject_doc_id
                                           and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                       REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))),
                    doc_tax as
                    (select iidt.inject_doc_id,
                            sum(iidt.tax_basis) total_tax_basis,
                            round(sum(iidt.tax_basis * (tax_rate/100)),REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) total_tax_amount
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_tax iidt
                      where iidt.inject_doc_id = value(ids)
                        and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidt.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                     group by iidt.inject_doc_id),
                    doc_dtl as
                    (select iidd.inject_doc_id,
                            sum(iiddt.tax_basis) total_tax_basis,
                            round(sum(iiddt.tax_basis * (tax_rate/100)),REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) total_tax_amount
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_detail iidd,
                            im_inject_doc_detail_tax iiddt
                      where iidd.inject_doc_id = value(ids)
                        and iiddt.detail_id    = iidd.detail_id
                        and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidd.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                     group by iidd.inject_doc_id),
                    doc_nm as
                    (select iidnmt.inject_doc_id,
                            sum(iidnmt.tax_basis) total_tax_basis,
                            sum(iidnmt.tax_basis * (tax_rate/100)) total_tax_amount
                       from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            im_inject_doc_non_merch_tax iidnmt
                      where iidnmt.inject_doc_id  = value(ids)
                        and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidnmt.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                     group by iidnmt.inject_doc_id)
                    select dh.inject_id,
                           dh.inject_doc_id,
                           case
                                --Basis
                                when abs(abs(dt.total_tax_basis) - abs(dh.total_cost)) > DECODE(LP_calc_tolerance_ind,
                                                                                                REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dh.total_cost) * (LP_calc_tolerance/100)),
                                                                                                REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                TOTAL_BASIS
                                when dd.total_tax_basis is NOT NULL and dnm.total_tax_basis is NOT NULL
                                 and abs(abs(dh.total_cost) - abs(dd.total_tax_basis + dnm.total_tax_basis)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dd.total_tax_basis + dnm.total_tax_basis) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                DETAIL_NM_BASIS
                                when dh.doc_type = REIM_CONSTANTS.DOC_TYPE_NMRCHI and dnm.total_tax_basis is NOT NULL
                                 and abs(abs(dh.total_cost) - abs(dnm.total_tax_basis)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dnm.total_tax_basis) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                NON_MRCH_BASIS
                                when dh.doc_type <> REIM_CONSTANTS.DOC_TYPE_NMRCHI and dnm.total_tax_basis is NOT NULL and dd.total_tax_basis is NOT NULL
                                 and abs(abs(dh.total_cost - dd.total_tax_basis) - abs(dnm.total_tax_basis)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dnm.total_tax_basis) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                NON_MRCH_BASIS
                                when dd.total_tax_basis is NOT NULL and dnm.total_tax_basis is NULL
                                 and abs(abs(dh.total_cost) - abs(dd.total_tax_basis)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dd.total_tax_basis) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                DETAIL_BASIS
                                --Tax
                                when abs(abs(dt.total_tax_amount) - abs(dh.total_tax_amount)) > DECODE(LP_calc_tolerance_ind,
                                                                                                REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dh.total_tax_amount) * (LP_calc_tolerance/100)),
                                                                                                REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                TOTAL_TAX
                                when dh.doc_type = REIM_CONSTANTS.DOC_TYPE_NMRCHI and dnm.total_tax_basis is NOT NULL
                                 and abs(abs(dh.total_tax_amount) - abs(dnm.total_tax_amount)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dnm.total_tax_amount) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                NON_MRCH_TAX
                                when dd.total_tax_basis is NOT NULL and dnm.total_tax_basis is NOT NULL
                                 and abs(abs(dh.total_tax_amount) - abs(dd.total_tax_amount + dnm.total_tax_amount)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dd.total_tax_amount + dnm.total_tax_amount) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                DETAIL_NM_TAX
                                when dh.doc_type <> REIM_CONSTANTS.DOC_TYPE_NMRCHI and dnm.total_tax_basis is NOT NULL and dd.total_tax_basis is NOT NULL
                                 and abs(abs(dh.total_tax_amount - dd.total_tax_amount) - abs(dnm.total_tax_amount)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dnm.total_tax_amount) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                NON_MRCH_TAX
                                when dd.total_tax_basis is NOT NULL and dnm.total_tax_basis is NULL
                                 and abs(abs(dh.total_tax_amount) - abs(dd.total_tax_amount)) > DECODE(LP_calc_tolerance_ind,
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_PERCENT, (abs(dd.total_tax_amount) * (LP_calc_tolerance/100)),
                                                                                                      REIM_CONSTANTS.CALC_TOL_IND_AMOUNT, LP_calc_tolerance) then
                                DETAIL_TAX
                           end as rule
                      from doc_hdr dh,
                           doc_tax dt,
                           doc_dtl dd,
                           doc_nm dnm
                     where dh.inject_doc_id = dt.inject_doc_id
                       and dh.inject_doc_id = dd.inject_doc_id (+)
                       and dh.inject_doc_id = dnm.inject_doc_id (+)) iidt_inner,
                   im_inject_doc_rule iidr
             where iidt_inner.rule = iidr.rule;

        -- Function to update or reject Merch Invoices which are eligible for Reverse vat
        O_status := UPDATE_REVERSE_VAT(O_error_message, I_trial_id);

        if (O_status <> REIM_CONSTANTS.SUCCESS) then
            return O_status;
        end if;

        --Item Tax Validation
        open C_FETCH_ITEM_TAX_CRIT;
        fetch C_FETCH_ITEM_TAX_CRIT BULK COLLECT into L_obj_item_tax_crit_tbl;
        close C_FETCH_ITEM_TAX_CRIT;

        if (L_obj_item_tax_crit_tbl IS NOT NULL and L_obj_item_tax_crit_tbl.count > 0) then

            REIM_TAX_CALC_SQL.CALCULATE_ITEM_TAXES(O_status,
                                                   O_error_message,
                                                   L_obj_item_tax_crit_tbl,
                                                   L_obj_item_tax_calc_ovrd_tbl,
                                                   L_obj_item_tax_break_tbl);

            if (O_status <> REIM_CONSTANTS.SUCCESS) then
                return O_status;
            end if;

            if (L_obj_item_tax_break_tbl IS NULL OR L_obj_item_tax_break_tbl.Count = 0) then
                O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                    'Unable to retrieve Item Taxes',
                                    L_program,
                                    TO_CHAR(SQLCODE));
                return REIM_CONSTANTS.FAIL;
            end if;

            if (LP_tax_validation_type = REIM_CONSTANTS.TAX_VALID_TYPE_RETLR) then
                --insert into error table
                insert into im_inject_doc_error(inject_id,
                                                inject_doc_id,
                                                detail_id,
                                                rule,
                                                trial_id,
                                                error_context,
                                                fixable,
                                                status,
                                                creation_date,
                                                created_by)
                    select iidt_inner.inject_id,
                           iidt_inner.inject_doc_id,
                           iidt_inner.detail_id,
                           iidt_inner.rule,
                           I_trial_id,
                           iidt_inner.error_context,
                           iidr.fixable_default,
                           REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                           SYSDATE,
                           LP_username
                      from (with im_inj_doc_header as
                            (select iidh.inject_id,
                                    iidh.inject_doc_id,
                                    iidh.vendor,
                                    iidh.vendor_type,
                                    iidh.location,
                                    iidh.doc_date,
                                    iidh.reverse_vat_ind
                               from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    im_inject_doc_head iidh
                              where iidh.inject_doc_id = value(ids)
                                and not exists (select 'x'
                                                  from im_inject_doc_error iide
                                                 where iide.inject_doc_id  = iidh.inject_doc_id
                                                   and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                               REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED)))
                            select iidh.inject_id,
                                   iidd.inject_doc_id,
                                   iidd.detail_id,
                                   VALID_ITEM_TAX rule,
                                   'Tax code = ' || iiddt.tax_code || ' for Item/vpn/upc = ' || decode(iidd.item_source,
                                                                                                     REIM_CONSTANTS.ITEM_SRC_ITEM_ID, iidd.item,
                                                                                                     REIM_CONSTANTS.ITEM_SRC_VPN, iidd.vpn,
                                                                                                     REIM_CONSTANTS.ITEM_SRC_UPC, iidd.upc) error_context
                              from table(cast(L_obj_item_tax_break_tbl as OBJ_ITEM_TAX_BREAK_TBL)) tbl,
                                   im_inj_doc_header iidh,
                                   im_inject_doc_detail iidd,
                                   im_inject_doc_detail_tax iiddt
                             where iidd.inject_doc_id        = iidh.inject_doc_id
                               and iiddt.detail_id           = iidd.detail_id
                               and iiddt.reverse_vat_ind     = REIM_CONSTANTS.YN_NO
                               and tbl.supplier    = iidh.vendor
                               and tbl.location    = iidh.location
                               and tbl.item        = iidd.item
                               and tbl.eff_date    = iidh.doc_date
                               and (tbl.tax_code    <> iiddt.tax_code
                                    or tbl.tax_rate <> iiddt.tax_rate)) iidt_inner,
                           im_inject_doc_rule iidr
                     where iidt_inner.rule = iidr.rule;

            elsif (LP_tax_validation_type IN (REIM_CONSTANTS.TAX_VALID_TYPE_RECON,
                                              REIM_CONSTANTS.TAX_VALID_TYPE_VENDR)) then

                delete from im_inject_tax_discrepancy iitd
                 where exists (select 'x'
                                 from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      im_inject_doc_head iidh
                                where iidh.inject_doc_id = value(ids)
                                  and iidh.inject_doc_id = iitd.inject_doc_id
                                  and not exists (select 'x'
                                                    from im_inject_doc_error iide
                                                   where iide.inject_doc_id  = iidh.inject_doc_id
                                                     and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                                 REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED)));

                --insert into inject tax discrepancy table
                insert into im_inject_tax_discrepancy(inject_doc_id,
                                                      order_no,
                                                      item,
                                                      tax_code,
                                                      doc_tax_rate,
                                                      doc_tax_amount,
                                                      verify_tax_rate,
                                                      verify_tax_code,
                                                      verify_tax_amount,
                                                      verify_tax_src,
                                                      verify_tax_formula,
                                                      verify_tax_order)
                    with im_inj_doc_header as
                            (select iidh.inject_doc_id,
                                    iidh.order_no,
                                    iidh.vendor,
                                    iidh.vendor_type,
                                    iidh.location,
                                    iidh.doc_date,
                                    iidh.reverse_vat_ind
                               from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    im_inject_doc_head iidh
                              where iidh.inject_doc_id = value(ids)
                                and not exists (select 'x'
                                                  from im_inject_doc_error iide
                                                 where iide.inject_doc_id  = iidh.inject_doc_id
                                                   and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                               REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED)))
                    select iidh.inject_doc_id,
                           iidh.order_no,
                           iidd.item,
                           iiddt.tax_code,
                           iiddt.tax_rate,
                           iiddt.tax_basis * (iiddt.tax_rate/100) doc_tax_amount,
                           tbl.tax_rate,
                           tbl.tax_code,
                           iiddt.tax_basis * (tbl.tax_rate/100) verify_tax_amount,
                           REIM_CONSTANTS.TAX_SRC_SYSTEM,
                           tbl.tax_basis_formula,
                           tbl.application_order
                      from table(cast(L_obj_item_tax_break_tbl as OBJ_ITEM_TAX_BREAK_TBL)) tbl,
                           im_inj_doc_header iidh,
                           im_inject_doc_detail iidd,
                           im_inject_doc_detail_tax iiddt
                     where iidd.inject_doc_id = iidh.inject_doc_id
                       and iiddt.detail_id    = iidd.detail_id
                       and iiddt.reverse_vat_ind     = REIM_CONSTANTS.YN_NO
                       and iidd.item       = tbl.item
                       and iidh.vendor     = tbl.supplier
                       and iidh.location   = tbl.location
                       and iidh.doc_date      = tbl.eff_date
                       and (iiddt.tax_rate <> tbl.tax_rate or
                            iiddt.tax_code <> tbl.tax_code);

            end if; -- tax_validation_type

        end if;

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END VALIDATE_TRAN_TAX;

    /**
     * The function that performs reverse vat validations.
     * Only Merchandise Invoices are considered.
     *
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION UPDATE_REVERSE_VAT(O_error_message    OUT VARCHAR2,
                                I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.UPDATE_REVERSE_VAT';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

        L_zero_tax_code VAT_CODE_RATES.VAT_CODE%TYPE := NULL;

        CURSOR C_GET_ZERO_TAX
        IS
        SELECT vcr.vat_code tax_code
          FROM vat_code_rates vcr
         WHERE vcr.vat_rate = 0
           AND vcr.active_date <= (select min(doc_date)
                                     from im_rev_vat_doc_item_gtt rvgtt)
           AND ROWNUM < 2;

    BEGIN

        insert into im_rev_vat_doc_item_gtt (inject_id,
                                             inject_doc_id,
                                             transaction_id,
                                             doc_date,
                                             merch_type,
                                             detail_id,
                                             item,
                                             tax_code,
                                             tax_rate,
                                             tax_basis,
                                             vat_region,
                                             reverse_vat_threshold)
        --Merch invoices
        select iidh.inject_id,
               iidh.inject_doc_id,
               iidh.transaction_id,
               iidh.doc_date,
               nvl(iidh.merch_type, REIM_CONSTANTS.YN_NO) merch_type,
               iidd.detail_id,
               iidd.item,
               iiddt.tax_code,
               iiddt.tax_rate,
               iiddt.tax_basis,
               vr.vat_region,
               vr.reverse_vat_threshold
          from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
               im_inject_doc_head iidh,
               im_inject_doc_detail iidd,
               im_inject_doc_detail_tax iiddt,
               sups s,
               (select store loc,
                       REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                       vat_region
                  from store
                union all
                select wh loc,
                       REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                       vat_region
                  from wh) locs,
               vat_region vr,
               vat_item vi
         where iidh.inject_doc_id = value(ids)
           and iidh.doc_type      = REIM_CONSTANTS.DOC_TYPE_MRCHI
           and iidd.inject_doc_id = iidh.inject_doc_id
           and iiddt.detail_id    = iidd.detail_id
           and s.supplier         = iidh.vendor
           and locs.loc           = iidh.location
           and locs.loc_type      = iidh.loc_type
           and vr.vat_region      = locs.vat_region
           and s.vat_region       = locs.vat_region
           and vr.vat_calc_type   = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
           and vr.reverse_vat_threshold is NOT NULL
           and vi.item            = iidd.item
           and vi.vat_region      = vr.vat_region
           and vi.reverse_vat_ind = REIM_CONSTANTS.YN_YES
           and vi.vat_type        IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                      REIM_CONSTANTS.TAX_TYPE_BOTH)
           and vi.active_date     = (select MAX(active_date)
                                       from vat_item v
                                      where v.item = vi.item
                                        and v.vat_region = vi.vat_region
                                        and v.vat_type   IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                             REIM_CONSTANTS.TAX_TYPE_BOTH)
                                        and v.active_date <= iidh.doc_date)
           and not exists (select 'x'
                             from im_inject_doc_error iide
                            where iide.inject_doc_id  = iidh.inject_doc_id
                              and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                          REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
        union all
        -- Credit Notes whose reference docs have reverse vat
        select iidh.inject_id,
               iidh.inject_doc_id,
               iidh.transaction_id,
               iidh.doc_date,
               iidh.merch_type,
               iidd.detail_id,
               iidd.item,
               iiddt.tax_code,
               iiddt.tax_rate,
               iiddt.tax_basis,
               -9999 vat_region,
               -9999 reverse_vat_threshold
          from (select iidh.inject_id,
                       iidh.inject_doc_id,
                       iidh.transaction_id,
                       iidh.doc_date,
                       REIM_CONSTANTS.YN_NO merch_type
                  from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                       im_inject_doc_head iidh
                 where iidh.inject_doc_id                                                  = value(ids)
                   and iidh.doc_type                                                       = REIM_CONSTANTS.DOC_TYPE_CRDNT
                   and IS_CN_REVERSE_VAT_ELIGIBLE(NVL(iidh.ref_vendor, iidh.vendor),
                                                  iidh.doc_date,
                                                  iidh.custom_doc_ref3,
                                                  iidh.custom_doc_ref4)                    = REIM_CONSTANTS.YN_YES
                   and not exists (select 'x'
                                     from im_inject_doc_error iide
                                    where iide.inject_doc_id  = iidh.inject_doc_id
                                      and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                  REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) iidh,
               im_inject_doc_detail iidd,
               im_inject_doc_detail_tax iiddt
         where iidd.inject_doc_id = iidh.inject_doc_id
           and iiddt.detail_id    = iidd.detail_id;

        --Documents from vendors which do not comply with reverse vat config would get rejected.
        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select inner_rvgtt.inject_id,
                   inner_rvgtt.inject_doc_id,
                   inner_rvgtt.rule,
                   I_trial_id,
                   'Item Id = ' || inner_rvgtt.item,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (-- Non-RMS merch invoices eligible for reverse vat
                    select rvgtt.inject_id,
                           rvgtt.inject_doc_id,
                           rvgtt.item,
                           rvgtt.tax_rate,
                           sum(tax_basis) over (partition by rvgtt.inject_doc_id) tax_basis_sum,
                           rvgtt.reverse_vat_threshold,
                           REV_VAT_ITEM_ZERO_TAX rule
                      from im_rev_vat_doc_item_gtt rvgtt
                     where rvgtt.merch_type NOT IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                    REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                    REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                       and rvgtt.reverse_vat_threshold <> -9999
                    union all
                    -- Credit notes eligible for reverse vat
                    select rvgtt.inject_id,
                           rvgtt.inject_doc_id,
                           rvgtt.item,
                           rvgtt.tax_rate,
                           1 tax_basis_sum,         -- use constant value to pass threshold check in outer query
                           1 reverse_vat_threshold, -- use constant value to pass threshold check in outer query
                           REV_VAT_ITEM_ZERO_TAX rule
                      from im_rev_vat_doc_item_gtt rvgtt
                     where rvgtt.merch_type NOT IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                    REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                    REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                       and rvgtt.reverse_vat_threshold = -9999) inner_rvgtt,
                   im_inject_doc_rule iidr
             where inner_rvgtt.reverse_vat_threshold <= inner_rvgtt.tax_basis_sum
               and inner_rvgtt.tax_rate <> REIM_CONSTANTS.ZERO
               and iidr.rule      = inner_rvgtt.rule;


        --Update reverse_vat_ind on detail tax
        merge into im_inject_doc_detail_tax tgt
            using (select rvgtt.detail_id
                     from im_rev_vat_doc_item_gtt rvgtt
                    where rvgtt.merch_type NOT IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                   REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                   REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                      and not exists (select 'x'
                                        from im_inject_doc_error iide
                                       where iide.inject_doc_id = rvgtt.inject_doc_id
                                         and iide.status IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                             REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) src
        on (tgt.detail_id = src.detail_id)
        when matched then
        update
            set reverse_vat_ind = REIM_CONSTANTS.YN_YES;

        --Update reverse_vat_ind on header
        merge into im_inject_doc_head tgt
            using (select inner_rvgtt.inject_doc_id
                     from (-- Non-RMS Merch Invoices
                           select rvgtt.inject_doc_id,
                                  sum(tax_basis) tax_basis_sum,
                                  rvgtt.reverse_vat_threshold
                             from im_rev_vat_doc_item_gtt rvgtt
                            where rvgtt.merch_type NOT IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                           REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                           REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                              and rvgtt.reverse_vat_threshold <> -9999
                              and not exists (select 'x'
                                                from im_inject_doc_error iide
                                               where iide.inject_doc_id = rvgtt.inject_doc_id
                                                 and iide.status IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                            group by rvgtt.inject_doc_id,
                                     rvgtt.reverse_vat_threshold
                           union all
                           -- Credit notes eligible for reverse vat
                           select distinct rvgtt.inject_doc_id,
                                  1 tax_basis_sum,
                                  1 reverse_vat_threshold
                             from im_rev_vat_doc_item_gtt rvgtt
                            where rvgtt.merch_type NOT IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                           REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                           REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                              and rvgtt.reverse_vat_threshold = -9999
                              and not exists (select 'x'
                                                from im_inject_doc_error iide
                                               where iide.inject_doc_id = rvgtt.inject_doc_id
                                                 and iide.status IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) inner_rvgtt
                    where inner_rvgtt.reverse_vat_threshold <= inner_rvgtt.tax_basis_sum) src
        on (tgt.inject_doc_id = src.inject_doc_id)
        when matched then
        update
            set reverse_vat_ind = REIM_CONSTANTS.YN_YES;

        --Documents from RMS which do not comply with reverse vat config would get updated. (Temporary until RMS provides reverse vat for eligible item)
        open C_GET_ZERO_TAX;
        fetch C_GET_ZERO_TAX into L_zero_tax_code;
        close C_GET_ZERO_TAX;

        if L_zero_tax_code is NULL then
            O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                  'NO Tax Code with Zero rate Found',
                                                  L_program,
                                                  TO_CHAR(SQLCODE));
        end if;

        merge into im_inject_doc_detail_tax tgt
            using (select inner_rvgtt.detail_id,
                          inner_rvgtt.tax_basis_sum,
                          inner_rvgtt.reverse_vat_threshold
                     from (select rvgtt.inject_doc_id,
                                  rvgtt.detail_id,
                                  rvgtt.tax_rate,
                                  sum(tax_basis) over (partition by rvgtt.inject_doc_id) tax_basis_sum,
                                  rvgtt.reverse_vat_threshold
                             from im_rev_vat_doc_item_gtt rvgtt
                            where rvgtt.merch_type IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                       REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                       REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)) inner_rvgtt) src
        on (tgt.detail_id = src.detail_id)
        when matched then
        update
            set tgt.reverse_vat_ind = REIM_CONSTANTS.YN_YES,
                tgt.tax_code = case when src.reverse_vat_threshold <= src.tax_basis_sum then
                                    L_zero_tax_code
                               else
                                    tgt.tax_code
                               end,
                tgt.tax_rate = case when src.reverse_vat_threshold <= src.tax_basis_sum then
                                    REIM_CONSTANTS.ZERO
                               else
                                    tgt.tax_rate
                               end;

        --Update reverse_vat_ind on header
        merge into im_inject_doc_head tgt
            using (select inner_rvgtt.inject_doc_id
                     from (select rvgtt.inject_doc_id,
                                  sum(tax_basis) tax_basis_sum,
                                  rvgtt.reverse_vat_threshold
                             from im_rev_vat_doc_item_gtt rvgtt
                            where rvgtt.merch_type IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                       REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                       REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                              and not exists (select 'x'
                                                from im_inject_doc_error iide
                                               where iide.inject_doc_id = rvgtt.inject_doc_id
                                                 and iide.status IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                     REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))
                            group by rvgtt.inject_doc_id,
                                     rvgtt.reverse_vat_threshold) inner_rvgtt
                    where inner_rvgtt.reverse_vat_threshold <= inner_rvgtt.tax_basis_sum) src
        on (tgt.inject_doc_id = src.inject_doc_id)
        when matched then
        update
            set reverse_vat_ind = REIM_CONSTANTS.YN_YES;


        delete from im_inject_doc_tax
         where inject_doc_id in (select distinct rvgtt.inject_doc_id
                                   from im_rev_vat_doc_item_gtt rvgtt,
                                        im_inject_doc_head iidh
                                  where rvgtt.inject_doc_id = iidh.inject_doc_id
                                    and rvgtt.merch_type IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                             REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                                    and iidh.reverse_vat_ind = REIM_CONSTANTS.YN_YES);

        insert into im_inject_doc_tax(inject_doc_id,
                                      inject_id,
                                      transaction_id,
                                      tax_code,
                                      tax_rate,
                                      tax_basis,
                                      last_updated_by,
                                      last_update_date)
            select inject_doc_id,
                   inject_id,
                   transaction_id,
                   tax_code,
                   tax_rate,
                   sum(tax_basis),
                   LP_username,
                   sysdate
              from (with im_inj_doc_header as
                        (select iidh.inject_doc_id,
                               iidh.inject_id,
                               iidh.transaction_id
                          from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                               im_inject_doc_head iidh
                         where iidh.inject_doc_id = value(ids)
                           and iidh.merch_type    IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                      REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                      REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                           and iidh.reverse_vat_ind = REIM_CONSTANTS.YN_YES
                           and not exists (select 'x'
                                             from im_inject_doc_error iide
                                            where iide.inject_doc_id = iidh.inject_doc_id
                                              and iide.status IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                  REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED)))
                    select iidh.inject_doc_id,
                           iidh.inject_id,
                           iidh.transaction_id,
                           iiddt.tax_code,
                           iiddt.tax_rate,
                           sum(iiddt.tax_basis) tax_basis
                      from im_inj_doc_header iidh,
                           im_inject_doc_detail iidd,
                           im_inject_doc_detail_tax iiddt
                     where iidd.inject_doc_id   = iidh.inject_doc_id
                       and iiddt.detail_id      = iidd.detail_id
                     group by iidh.inject_doc_id,
                              iidh.inject_id,
                              iidh.transaction_id,
                              iiddt.tax_code,
                              iiddt.tax_rate
                    union all
                    select iidh.inject_doc_id,
                           iidh.inject_id,
                           iidh.transaction_id,
                           iidnmt.tax_code,
                           iidnmt.tax_rate,
                           sum(iidnmt.tax_basis) tax_basis
                      from im_inj_doc_header iidh,
                           im_inject_doc_non_merch iidnm,
                           im_inject_doc_non_merch_tax iidnmt
                     where iidnm.inject_doc_id   = iidh.inject_doc_id
                       and iidnmt.inject_doc_id  = iidnm.inject_doc_id
                       and iidnmt.non_merch_code = iidnm.non_merch_code
                     group by iidh.inject_doc_id,
                              iidh.inject_id,
                              iidh.transaction_id,
                              iidnmt.tax_code,
                              iidnmt.tax_rate)
            group by inject_doc_id,
                     inject_id,
                     transaction_id,
                     tax_code,
                     tax_rate;

        merge into im_inject_doc_head tgt
            using (select iidt.inject_doc_id,
                          sum(iidt.tax_basis * (iidt.tax_rate/100)) total_tax_amount
                     from table(cast(LP_inject_doc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          im_inject_doc_head iidh,
                          im_inject_doc_tax iidt
                    where iidh.inject_doc_id = value(ids)
                      and iidh.merch_type    IN (REIM_CONSTANTS.DOC_MERCH_TYPE_CONSIGNMENT,
                                                 REIM_CONSTANTS.DOC_MERCH_TYPE_DSD,
                                                 REIM_CONSTANTS.DOC_MERCH_TYPE_ERS)
                      and iidh.reverse_vat_ind = REIM_CONSTANTS.YN_YES
                      and iidt.inject_doc_id   = iidh.inject_doc_id
                    group by iidt.inject_doc_id) src
        on (tgt.inject_doc_id = src.inject_doc_id)
        when matched then
        update set tgt.total_tax_amount = src.total_tax_amount,
                   reverse_vat_ind = REIM_CONSTANTS.YN_YES;

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END UPDATE_REVERSE_VAT;

    /**
     * This private function is to validate duplicate vendor doc numbers between
     * the currently uploading transactions.
     *
     * Input param: I_inject_id (Unique identifier for the injector process)
     *
     * Returns 1 on Success
     *         0 on Failure
     */
    FUNCTION VALIDATE_DUP_VEND_DOC_NUM(O_error_message    OUT VARCHAR2,
                                       I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE,
                                       I_trial_id      IN     IM_INJECT_DOC_ERROR.TRIAL_ID%TYPE)
    RETURN NUMBER
    IS

        L_program VARCHAR(50) := 'REIM_INJECTOR_SQL.VALIDATE_DUP_VEND_DOC_NUM';

        O_status NUMBER(1) := REIM_CONSTANTS.SUCCESS;

    BEGIN

        insert into im_inject_doc_error(inject_id,
                                        inject_doc_id,
                                        rule,
                                        trial_id,
                                        error_context,
                                        fixable,
                                        status,
                                        creation_date,
                                        created_by)
            select iidh_inner.inject_id,
                   iidh_inner.inject_doc_id,
                   iidh_inner.rule,
                   I_trial_id,
                   DECODE(iidh_inner.rule,
                          DUP_VEND_DOC_NUM, 'Vendor Document Number = ' || iidh_inner.ext_doc_id || ' for vendor = ' || NVL(iidh_inner.ref_vendor, iidh_inner.vendor),
                          NULL) error_context,
                   iidr.fixable_default,
                   REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                   SYSDATE,
                   LP_username
              from (select iidh.*, -- to be replaced by individual columns
                           case
                               when exists (select 'x' from (select iidh_dup.inject_doc_id
                                                               from im_inject_doc_head iidh_dup
                                                              where iidh_dup.inject_id     = iidh.inject_id
                                                                and NVL(iidh_dup.ref_vendor, iidh_dup.vendor) = NVL(iidh.ref_vendor, iidh.vendor)
                                                                and iidh_dup.doc_type      = iidh.doc_type
                                                                and iidh_dup.ext_doc_id    = iidh.ext_doc_id
                                                                and iidh_dup.doc_date      = decode(LP_inc_date_for_dup_chk,
                                                                                                 REIM_CONSTANTS.YN_YES, iidh.doc_date,
                                                                                                 iidh_dup.doc_date)
                                                                and to_char(iidh_dup.doc_date,'YYYY') = decode(LP_inc_year_for_dup_chk,
                                                                                                            REIM_CONSTANTS.YN_YES, to_char(iidh.doc_date,'YYYY'),
                                                                                                            to_char(iidh_dup.doc_date,'YYYY'))) iidh_dup_inner
                                             where iidh.inject_doc_id <> iidh_dup_inner.inject_doc_id) then
                                   DUP_VEND_DOC_NUM
                           end as rule
                      from im_inject_doc_head iidh
                     where iidh.inject_id = I_inject_id
                       and not exists (select 'x'
                                         from im_inject_doc_error iide
                                        where iide.inject_doc_id  = iidh.inject_doc_id
                                          and iide.status         IN (REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_NEW,
                                                                      REIM_CONSTANTS.INJ_DOC_ERROR_STATUS_ROUTED))) iidh_inner,
                   im_inject_doc_rule iidr
             where iidh_inner.rule = iidr.rule;

        return O_status;

    EXCEPTION

        when OTHERS then
          -- TODO - Replace SQL_LIB calls with logger calls
          O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));

          return REIM_CONSTANTS.FAIL;

    END VALIDATE_DUP_VEND_DOC_NUM;


END REIM_INJECTOR_SQL;
/
