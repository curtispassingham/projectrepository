CREATE OR REPLACE PACKAGE REIM_MATCH_INQUIRY_SQL AS
----------------------------------------------------------------
FUNCTION CREATE_MATCH_INQ_DETAIL_WS(O_error_message    OUT VARCHAR2,
                                    O_workspace_id  IN OUT IM_MATCH_INQ_DETAIL_WS.WORKSPACE_ID%TYPE,
                                    I_doc_id        IN     IM_DOC_HEAD.DOC_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
END REIM_MATCH_INQUIRY_SQL;
/
