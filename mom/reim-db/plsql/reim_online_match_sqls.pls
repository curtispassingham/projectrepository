CREATE OR REPLACE PACKAGE REIM_ONLINE_MATCH_SQL AS

ERROR_MULTI_ITEM_INVC    CONSTANT VARCHAR2(20) := 'MULTI_ITEM_INVC';
ERROR_MULTI_IN_FAVOR     CONSTANT VARCHAR2(20) := 'MULTI_IN_FAVOR';

--------------------------------------------------------------------
/**
 * The public function used for Initializing Match Data for Online matching.
 * Output param: O_match_wspace_id (The ID of the Match workspace, new one if I_match_wspace_id is not passed in)
 *
 * Input param: I_search_wspace_id (The ID of the Search workspace)
 *              I_match_wspace_id (The ID of the Match workspace, if search is performed on the match screen)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION INIT_ONLINE_MATCH_DATA(O_error_message         OUT VARCHAR2,
                                O_match_wspace_id       OUT IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                                I_search_wspace_id   IN     IM_MATCH_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                                I_match_wspace_id    IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE DEFAULT NULL,
                                I_client_id          IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to perform Summary Match from the UI
 * Input param: I_workspace_id (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_SMRY_MATCH_ONLINE(O_error_message    OUT VARCHAR2,
                                   I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                                   I_skip_sku_comp IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to refresh match data (for online matching)
 * Input param: I_workspace_id (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION REFRESH_MTCH_WSPACE(O_error_message    OUT VARCHAR2,
                             I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE,
                             I_skip_matched  IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to Suggest Match from the UI
 * Input param: I_workspace_id (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION SUGGEST_MATCH(O_error_message    OUT VARCHAR2,
                       O_match_count      OUT NUMBER,
                       I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to perform Online Resolution.(Item View)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *              I_cost_decision (Decision indicator, 'R' for receipt, 'I' for Invoice, 'M' for Match Within Tolerance and 'D' for deferred, NULL if no resolution performed)
 *              I_qty_decision  (Decision indicator, 'R' for receipt, 'I' for Invoice, 'M' for Match Within Tolerance and 'D' for deferred, NULL if no resolution performed)
 *              I_resln_action_rc_tbl  (List of Reason codes (both cost and qty together) used for resolving the discrepancy)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_ONLINE_RESLN(O_error_message          OUT VARCHAR2,
                              I_workspace_id        IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                              I_cost_decision       IN     IM_INVOICE_DETAIL.COST_MATCHED%TYPE,
                              I_qty_decision        IN     IM_INVOICE_DETAIL.QTY_MATCHED%TYPE,
                              I_resln_action_rc_tbl IN     IM_RESLN_ACTION_RC_TBL)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to perform Split Receipt.(Item View)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION SPLIT_RECEIPT(O_error_message    OUT VARCHAR2,
                       I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to populate workspace table used for Online detail matching.
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to populate workspace table used for Online disrepancy list.
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_DISCREPANCY_LIST_WS(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
FUNCTION SET_DISCREPANCY_POPUP_FLAGS(O_error_message    OUT VARCHAR2,
                                     I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
FUNCTION PUSH_DISCREPANCY_TO_DETAIL(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
FUNCTION VALIDATE_DISCREP_SELECT_ALL(O_error_message    OUT VARCHAR2,
                                     I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to persist match data to Operational tables for Online detail matching.(Item View)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERSIST_DETAIL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                 I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to populate the Invoice Item workspace table used for Online detail matching.
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_INVC_ITEM_VIEW_WS(O_error_message    OUT VARCHAR2,
                                  I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to persist match data to Operational tables for Online detail matching.(Invoice Item View)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERSIST_DETL_MTCH_IIVIEW(O_error_message    OUT VARCHAR2,
                                  I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to perform Split Receipt.(Invoice Item View)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION SPLIT_RECEIPT_IIVIEW(O_error_message    OUT VARCHAR2,
                              I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to push user selection from the Invoice Item view to the default detail match view.
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PUSH_INVC_ITEM_VIEW_TO_DETAIL(O_error_message    OUT VARCHAR2,
                                       I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
/**
 * The public function used to persist match data to Operational tables for Online detail matching (Style Level)
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERSIST_STYLE_LVL_DETL_MTCH_WS(O_error_message    OUT VARCHAR2,
                                        I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to select or deselect all Styles and Skus on the Detail Matching workspace
 * Input param: I_workspace_id  (ID of the Match process workspace)
 *              I_choice_flag   (Y or N for select/deselect)
 *              I_match_status  (U or M for unmatched or matched SKUs)
 * 
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION UPDATE_ALL_DETL_MATCH_WS(O_error_message    OUT VARCHAR2,
                                  I_workspace_id  IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                                  I_choice_flag   IN     VARCHAR2,
                                  I_match_status  IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------
END REIM_ONLINE_MATCH_SQL;
/
