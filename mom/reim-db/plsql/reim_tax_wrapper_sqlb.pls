CREATE OR REPLACE PACKAGE BODY REIM_TAX_WRAPPER_SQL AS
--------------------------------------------
PROCEDURE CALCULATE_ITEM_TAXES_SINGLETAX (O_status                OUT NUMBER,                     -- Success status of the procedure.
                                          O_error_message         OUT VARCHAR2,                   -- Error message if the procedure failed.
                                          I_item_tax_criteria  IN     OBJ_ITEM_TAX_CRITERIA_TBL,  -- The tax criteria to calculate taxes for.
                                          I_item_tax_calc_ovrd IN     OBJ_ITEM_TAX_CALC_OVRD_TBL, -- Overriding values for calculating taxes.
                                          O_item_tax_results      OUT OBJ_ITEM_TAX_BREAK_TBL)     -- A table type for the results of the tax calculations, one row per tax
IS
   L_program VARCHAR2(60) := 'REIM_TAX_WRAPPER_SQL.CALCULATE_ITEM_TAXES_SINGLETAX';

   L_custom_tax_calc_tbl OBJ_TAX_CALC_TBL := NULL;

   custom_tax_error EXCEPTION;

    CURSOR C_FETCH_ITEM_TAX is
        select OBJ_ITEM_TAX_BREAK_REC(calc.calc_item,
                                      calc.calc_supplier,
                                      calc.calc_location,
                                      calc.calc_eff_date,
                                      calc.tax_code,
                                      calc.tax_rate,
                                      DECODE(calc.tax_rate,
                                             NULL, 0,
                                             round(calc.calc_unit_cost * calc.tax_rate/100, calc.decimal_digits)),
                                      calc.calc_unit_cost,
                                      calc.reverse_vat_ind,
                                      NULL,
                                      NULL,
                                      NULL,
                                      REIM_CONSTANTS.TAX_FORMULA_COST,
                                      REIM_CONSTANTS.TAX_DEFLT_APP_ORDER,
                                      REIM_CONSTANTS.TAX_FORM_TYPE_COST_ONLY)
          from (select calc_item,
                       calc_supplier,
                       calc_location,
                       calc_eff_date,
                       tax_code,
                       tax_rate,
                       calc_unit_cost,
                       reverse_vat_ind,
                       NVL((select max(icl.currency_cost_dec)
                              from im_currencies icl
                             where icl.currency_code = currency_code), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) decimal_digits
                  from (-- Supplier and location have the same vat_region
                        select crit.item calc_item,
                               crit.supplier calc_supplier,
                               crit.location calc_location,
                               crit.eff_date calc_eff_date,
                               NVL(ovrd.tax_code, vi.vat_code) tax_code,
                               NVL(ovrd.tax_rate, vi.vat_rate) tax_rate,
                               crit.unit_cost calc_unit_cost,
                               vi.reverse_vat_ind reverse_vat_ind,
                               s.currency_code currency_code
                          from TABLE(CAST(I_item_tax_criteria as OBJ_ITEM_TAX_CRITERIA_TBL)) crit,
                               TABLE(CAST(I_item_tax_calc_ovrd as OBJ_ITEM_TAX_CALC_OVRD_TBL)) ovrd,
                               vat_item vi,
                               sups s,
                               (select wh location,
                                       REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                       physical_wh physical_loc,
                                       vat_region
                                  from wh
                                union all
                                select store location,
                                       REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                       store physical_loc,
                                       vat_region
                                  from store) locs,
                               vat_region vr
                         where crit.supplier    = s.supplier
                           and crit.location    = locs.location
                           and crit.item        = vi.item
                           and crit.item        = ovrd.item(+)
                           and crit.supplier    = ovrd.supplier(+)
                           and crit.location    = ovrd.location(+)
                           and s.vat_region     = locs.vat_region
                           and vr.vat_region    = locs.vat_region
                           and vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                           and locs.vat_region  = vi.vat_region
                           and vi.vat_type      IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                    REIM_CONSTANTS.TAX_TYPE_BOTH)
                           and vi.active_date   = (select max(active_date)
                                                     from vat_item v
                                                    where v.item = vi.item
                                                      and v.vat_region = vi.vat_region
                                                      and v.vat_type in(REIM_CONSTANTS.TAX_TYPE_COST,
                                                                        REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                      and v.active_date <= crit.eff_date)
                        union all
                        -- Supplier and location have different vat_region and exists a vat code with zero vat rate
                        select inner.calc_item,
                               inner.calc_supplier,
                               inner.calc_location,
                               inner.calc_eff_date,
                               inner.tax_code,
                               inner.tax_rate,
                               inner.calc_unit_cost,
                               inner.reverse_vat_ind,
                               inner.currency_code
                          from (select crit.item calc_item,
                                       crit.supplier calc_supplier,
                                       crit.location calc_location,
                                       crit.eff_date calc_eff_date,
                                       NVL(ovrd.tax_code, vcr.vat_code) tax_code,
                                       NVL(ovrd.tax_rate, REIM_CONSTANTS.ZERO) tax_rate,
                                       crit.unit_cost calc_unit_cost,
                                       REIM_CONSTANTS.YN_NO reverse_vat_ind,
                                       s.currency_code currency_code,
                                       RANK() OVER (PARTITION BY crit.item, crit.supplier, crit.location
                                                      ORDER BY vcr.vat_code desc, vcr.active_date desc) vat_code_rank
                                  from TABLE(CAST(I_item_tax_criteria as OBJ_ITEM_TAX_CRITERIA_TBL)) crit,
                                       TABLE(CAST(I_item_tax_calc_ovrd as OBJ_ITEM_TAX_CALC_OVRD_TBL)) ovrd,
                                       sups s,
                                       (select wh location,
                                               REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                               physical_wh physical_loc,
                                               vat_region
                                          from wh
                                        union all
                                        select store location,
                                               REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                               store physical_loc,
                                               vat_region
                                          from store) locs,
                                       vat_region locs_vr,
                                       vat_region sups_vr,
                                       vat_code_rates vcr
                                 where crit.supplier         = s.supplier
                                   and crit.location         = locs.location
                                   and crit.item             = ovrd.item(+)
                                   and crit.supplier         = ovrd.supplier(+)
                                   and crit.location         = ovrd.location(+)
                                   and locs_vr.vat_region    = locs.vat_region
                                   and sups_vr.vat_region    = s.vat_region
                                   and locs_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                   and sups_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                   and NVL(s.vat_region, -1) != NVL(locs.vat_region, -2)
                                   and vcr.vat_rate          = REIM_CONSTANTS.ZERO
                                   and vcr.active_date       < crit.eff_date) inner
                         where inner.vat_code_rank = REIM_CONSTANTS.ONE
                        union all
                        -- Supplier and location have different vat_region and there is no vat code with zero vat rate
                        select crit.item calc_item,
                               crit.supplier calc_supplier,
                               crit.location calc_location,
                               crit.eff_date calc_eff_date,
                               ovrd.tax_code tax_code,
                               ovrd.tax_rate tax_rate,
                               crit.unit_cost calc_unit_cost,
                               REIM_CONSTANTS.YN_NO reverse_vat_ind,
                               s.currency_code currency_code
                          from TABLE(CAST(I_item_tax_criteria as OBJ_ITEM_TAX_CRITERIA_TBL)) crit,
                               TABLE(CAST(I_item_tax_calc_ovrd as OBJ_ITEM_TAX_CALC_OVRD_TBL)) ovrd,
                               sups s,
                               (select wh location,
                                       REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                       physical_wh physical_loc,
                                       vat_region
                                  from wh
                                union all
                                select store location,
                                       REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                       store physical_loc,
                                       vat_region
                                  from store) locs,
                               vat_region locs_vr,
                               vat_region sups_vr
                         where crit.supplier         = s.supplier
                           and crit.location         = locs.location
                           and crit.item             = ovrd.item(+)
                           and crit.supplier         = ovrd.supplier(+)
                           and crit.location         = ovrd.location(+)
                           and locs_vr.vat_region    = locs.vat_region
                           and sups_vr.vat_region    = s.vat_region
                           and locs_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                           and sups_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                           and NVL(s.vat_region, -1) != NVL(locs.vat_region, -2)
                           and NOT EXISTS (select 'x'
                                             from vat_code_rates vcr
                                            where vcr.vat_rate          = REIM_CONSTANTS.ZERO
                                              and vcr.active_date <= crit.eff_date)
                        union all
                        -- Custom vat from RMS
                        select tax_tbl.I_item calc_item,
                               to_number(tax_tbl.I_from_entity) calc_supplier,
                               to_number(tax_tbl.I_to_entity) calc_location,
                               tax_tbl.I_effective_from_date calc_eff_date,
                               tax_detl_tbl.tax_code,
                               tax_detl_tbl.tax_rate,
                               tax_tbl.I_amount calc_unit_cost,
                               REIM_CONSTANTS.YN_NO reverse_vat_ind,
                               tax_tbl.I_amount_curr currency_code
                          from TABLE(CAST(L_custom_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_tbl,
                               TABLE(tax_tbl.O_tax_detail_tbl) tax_detl_tbl)) calc;

    CURSOR C_XFORM_TAX_OBJ is
        select OBJ_TAX_CALC_REC(crit.item,       -- I_item
                                NULL,            -- I_pack_ind
                                crit.supplier,   -- I_from_entity
                                REIM_CONSTANTS.TAX_ENTITY_TYPE_SUPP, -- I_from_entity_type
                                crit.location,   -- I_to_entity
                                DECODE(locs.loc_type,
                                       REIM_CONSTANTS.LOC_TYPE_STORE, REIM_CONSTANTS.TAX_ENTITY_TYPE_STORE,
                                       REIM_CONSTANTS.TAX_ENTITY_TYPE_WH), -- I_to_entity_type
                                crit.eff_date,   -- I_effective_from_date
                                crit.unit_cost,  -- I_amount
                                s.currency_code, -- I_amount_curr
                                'Y',             -- I_amount_tax_incl_ind
                                NULL,            -- I_origin_country_id
                                NULL,            -- O_cum_tax_pct
                                NULL,            -- O_cum_tax_value
                                NULL,            -- O_total_tax_amount
                                NULL,            -- O_total_tax_amount_curr
                                NULL,            -- O_total_recover_amount
                                NULL,            -- O_total_recover_amount_curr
                                NULL,            -- O_tax_detail_tbl
                                NULL,            -- I_tran_type
                                crit.eff_date,   -- I_tran_date
                                NULL,            -- I_tran_id
                                'C',             -- I_cost_retail_ind
                                NULL,            -- I_source_entity
                                NULL,            -- I_source_entity_type
                                'N',             -- O_tax_exempt_ind
                                ovrd.tax_code,   -- I_vat_code
                                ovrd.tax_rate)   -- I_vat_rate
          from TABLE(CAST(I_item_tax_criteria as OBJ_ITEM_TAX_CRITERIA_TBL)) crit,
               TABLE(CAST(I_item_tax_calc_ovrd as OBJ_ITEM_TAX_CALC_OVRD_TBL)) ovrd,
               sups s,
               (select wh location,
                       REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                       physical_wh physical_loc,
                       vat_region
                  from wh
                union all
                select store location,
                       REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                       store physical_loc,
                       vat_region
                  from store) locs,
               vat_region locs_vr,
               vat_region sups_vr
         where crit.supplier         = s.supplier
           and crit.location         = locs.location
           and crit.item             = ovrd.item(+)
           and crit.supplier         = ovrd.supplier(+)
           and crit.location         = ovrd.location(+)
           and locs_vr.vat_region    = locs.vat_region
           and sups_vr.vat_region    = s.vat_region
           and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT NOT IN (locs_vr.vat_calc_type,
                                                           sups_vr.vat_calc_type)
           and REIM_CONSTANTS.TAX_CALC_TYPE_CUSTOM IN (locs_vr.vat_calc_type,
                                                       sups_vr.vat_calc_type);

BEGIN

   LOGGER.LOG_INFORMATION('Begin - ' || L_program);

   open C_XFORM_TAX_OBJ;
   fetch C_XFORM_TAX_OBJ BULK COLLECT into L_custom_tax_calc_tbl;
   close C_XFORM_TAX_OBJ;

   LOGGER.LOG_INFORMATION('End of Cursor C_XFORM_TAX_OBJ ');

   if L_custom_tax_calc_tbl is NOT NULL and L_custom_tax_calc_tbl.COUNT > 0 then

      if CUSTOM_TAX_SQL.CALC_CTAX_COST_TAX(O_error_message,
                                           L_custom_tax_calc_tbl) = FALSE then
         RAISE custom_tax_error;
      end if;

   end if;

   open C_FETCH_ITEM_TAX;
   fetch C_FETCH_ITEM_TAX BULK COLLECT into O_item_tax_results;
   close C_FETCH_ITEM_TAX;

   LOGGER.LOG_INFORMATION('End of Cursor C_FETCH_ITEM_TAX ');

   LOGGER.LOG_INFORMATION('End - ' || L_program);

   O_status := REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when custom_tax_error then
      O_status := REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := REIM_CONSTANTS.FAIL;

END CALCULATE_ITEM_TAXES_SINGLETAX;
--------------------------------------------
PROCEDURE GET_VENDOR_TAX_CODES_SINGLETAX (O_status           OUT NUMBER,                  -- Success status of the procedure.
                                          O_error_message    OUT VARCHAR2,                -- Error message if the procedure failed.
                                          I_vendor_data   IN     OBJ_VENDOR_DATA_TBL,     -- A table containing the vendors we desire to return taxes for.
                                          I_date          IN     DATE,                    -- The date to use to determine taxes.
                                          I_tax_type      IN     VARCHAR2,                -- The type of taxes to retreive.
                                          O_vendor_taxes     OUT OBJ_VENDOR_TAX_DATA_TBL) -- A table containing the valid vendor taxes for the date supplied.
IS
    L_program VARCHAR2(60) := 'REIM_TAX_WRAPPER_SQL.GET_VENDOR_TAX_CODES_SINGLETAX';

    -- to be replaced with RMS API
    CURSOR C_FETCH_VAT_CODE_RATES is
      select obj_vendor_tax_data_rec(vends.vendor_id,
                                     vends.vendor_type,
                                     I_date,
                                     rates.vat_code,
                                     rates.vat_rate)
      from (select vcr.vat_code,
                   vcr.vat_rate
              from vat_code_rates vcr,
                   (select vat_code,
                           max(active_date) as active_date
                      from vat_code_rates vcr
                     where active_date <= I_date
                     GROUP BY vat_code) latest
             where vcr.vat_code    = latest.vat_code
               and vcr.active_date = latest.active_date) rates,
           table(cast(I_vendor_data as obj_vendor_data_tbl)) vends;

BEGIN

   LOGGER.LOG_INFORMATION('Begin - ' || L_program);

   open C_FETCH_VAT_CODE_RATES;
   fetch C_FETCH_VAT_CODE_RATES BULK COLLECT into O_vendor_taxes;
   close C_FETCH_VAT_CODE_RATES;

   LOGGER.LOG_INFORMATION('End of Cursor C_FETCH_VAT_CODE_RATES ');

   LOGGER.LOG_INFORMATION('End - ' || L_program);

    O_status := REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := O_error_message
                         ||SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,SQLERRM,L_program, TO_CHAR(SQLCODE));
      O_status := REIM_CONSTANTS.FAIL;

END GET_VENDOR_TAX_CODES_SINGLETAX;
--------------------------------------------
PROCEDURE GET_TAX_BREAKUP_SINGLETAX (O_status           OUT NUMBER,                   -- Success status of the procedure.
                                     O_error_message    OUT VARCHAR2,                 -- Error message if the procedure failed.
                                     I_tran_type     IN     VARCHAR2,                 -- The transaction type to retrieve the breakup for.
                                     I_transactions  IN     OBJ_NUMERIC_ID_TABLE,     -- A collection of transaction numbers to retrieve breakups for.
                                     I_items         IN     OBJ_VARCHAR_ID_TABLE,     -- A collection of items to get the breakup for.  This field is optional.
                                     I_locations     IN     OBJ_NUMERIC_ID_TABLE,     -- A collection of items to get the breakup for.
                                     I_date          IN     DATE,                     -- The date to use to determine taxes.
                                     O_tax_breakups     OUT OBJ_ITEM_TAX_BREAKUP_TBL) -- A table of breakup records.
IS
   L_program VARCHAR2(60) := 'REIM_TAX_WRAPPER_SQL.GET_TAX_BREAKUP_SINGLETAX';

   L_custom_tax_calc_tbl OBJ_TAX_CALC_TBL := NULL;

   custom_tax_error EXCEPTION;

   cursor C_FETCH_ITEM_TAX is
      select obj_item_tax_breakup_rec(trans_type, -- tran_type     varchar2(30),
                                      trans_number, -- varchar2(30)
                                      null,  -- from_ref      number(10,0),
                                      null,  -- to_ref        number(10,0),
                                      trans_item, -- varchar2(25)
                                      trans_location, -- locationid  number(10),
                                      null,     -- assignment_id number(38),
                                      tax_rate,  -- tax_rate      number(20,10),
                                      tax_code, -- tax_code      varchar2(6),
                                      tax_amount, -- tax_amount    number(20,4),
                                      tax_basis, -- tax_basis    number(20,4),
                                      reverse_vat_ind, --reverse_vat_ind varchar2 (1),
                                      reim_constants.tax_deflt_app_order, -- application_ord number(6),
                                      reim_constants.tax_formula_cost, -- applied_on    varchar(500),
                                      reim_constants.tax_form_type_cost_only, -- basis_type    varchar(500),
                                      eff_date) -- eff_date      date
        from (select trans_type,
                     trans_number,
                     trans_item,
                     trans_location,
                     tax_code,
                     tax_rate,
                     decode(tax_rate,
                            null, 0,
                            round(unit_cost * tax_rate/100, decimal_digits)) tax_amount,
                     unit_cost tax_basis,
                     reverse_vat_ind,
                     eff_date
                from (/******
                       get tax breakdown when tran_type is purchase order
                       ******/
                      select REIM_CONSTANTS.TAX_TRAN_TYPE_PO trans_type,
                             trans_number,
                             trans_item,
                             trans_location,
                             tax_code,
                             tax_rate,
                             unit_cost,
                             reverse_vat_ind,
                             eff_date,
                             nvl((select max(icl.currency_cost_dec)
                                    from im_currencies icl
                                   where icl.currency_code = currency_code), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) decimal_digits
                        from (-- Supplier and Location have the same vat region
                              with ord_item_loc as (select value(ord_tbl) order_no,
                                                           value(item_tbl) item,
                                                           value(loc_tbl) location
                                                      from table(cast(I_transactions as obj_numeric_id_table)) ord_tbl,
                                                           table(cast(I_items as obj_varchar_id_table)) item_tbl,
                                                           table(cast(I_locations as obj_numeric_id_table)) loc_tbl),
                                  zero_vat_code as (select vr.vat_code,
                                                           vr.vat_rate,
                                                           vr.active_date
                                                      from vat_code_rates vr
                                                     where vr.vat_rate    = REIM_CONSTANTS.ZERO
                                                       and vr.active_date = (select Max(active_date)
                                                                               from vat_code_rates v
                                                                              where v.vat_rate    = vr.vat_rate
                                                                                and v.vat_code    = vr.vat_code
                                                                                and v.active_date <= I_date)
                                                        and ROWNUM < 2)
                              select ol.order_no trans_number,
                                     ol.item trans_item,
                                     locs.physical_loc trans_location,
                                     vi.vat_code tax_code,
                                     vi.vat_rate tax_rate,
                                     ol.unit_cost unit_cost,
                                     vi.reverse_vat_ind reverse_vat_ind,
                                     vi.active_date eff_date,
                                     oh.currency_code currency_code
                                from ord_item_loc oil,
                                     v_im_ordloc ol,
                                     ordhead oh,
                                     sups s,
                                     (select wh location,
                                             REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                             physical_wh physical_loc,
                                             vat_region from wh
                                      union all
                                      select store location,
                                             REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                             store physical_loc,
                                             vat_region
                                        from store) locs,
                                     vat_item vi,
                                     vat_region vr
                               where I_tran_type      = REIM_CONSTANTS.TAX_TRAN_TYPE_PO
                                 and ol.order_no      = oil.order_no
                                 and ol.item          = oil.item
                                 and oh.order_no      = ol.order_no
                                 and oh.supplier      = s.supplier
                                 and ol.location      = locs.location
                                 and ol.loc_type      = locs.loc_type
                                 and oil.location     = locs.physical_loc
                                 and ol.item          = vi.item
                                 and s.vat_region     = locs.vat_region
                                 and vr.vat_region    = locs.vat_region
                                 and vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and locs.vat_region  = vi.vat_region
                                 and vi.vat_type      IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                          REIM_CONSTANTS.TAX_TYPE_BOTH)
                                 and vi.active_date   = (select max(active_date)
                                                           from vat_item v
                                                          where v.item = vi.item
                                                            and v.vat_region = vi.vat_region
                                                            and v.vat_type in(REIM_CONSTANTS.TAX_TYPE_COST,
                                                                              REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                            and v.active_date <= I_date)
                              union all
                              -- Supplier and Location have different vat region and exists a vat code with Zero vat rate
                              select ol.order_no trans_number,
                                     ol.item trans_item,
                                     locs.physical_loc trans_location,
                                     zvc.vat_code tax_code,
                                     zvc.vat_rate tax_rate,
                                     ol.unit_cost unit_cost,
                                     REIM_CONSTANTS.YN_NO reverse_vat_ind,
                                     zvc.active_date eff_date,
                                     oh.currency_code currency_code
                                from ord_item_loc oil,
                                     v_im_ordloc ol,
                                     ordhead oh,
                                     sups s,
                                     (select wh location,
                                             REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                             physical_wh physical_loc,
                                             vat_region from wh
                                      union all
                                      select store location,
                                             REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                             store physical_loc,
                                             vat_region
                                        from store) locs,
                                     zero_vat_code zvc,
                                     vat_region locs_vr,
                                     vat_region sups_vr
                               where I_tran_type           = REIM_CONSTANTS.TAX_TRAN_TYPE_PO
                                 and ol.order_no           = oil.order_no
                                 and ol.item               = oil.item
                                 and oh.order_no           = ol.order_no
                                 and oh.supplier           = s.supplier
                                 and ol.location           = locs.location
                                 and ol.loc_type           = locs.loc_type
                                 and oil.location          = locs.physical_loc
                                 and sups_vr.vat_region    = s.vat_region
                                 and locs_vr.vat_region    = locs.vat_region
                                 and locs_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and sups_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and (   nvl(s.vat_region, -1) != nvl(locs.vat_region, -2)
                                      or NOT EXISTS (select 'x'
                                                       from vat_item v
                                                      where v.item        = ol.item
                                                        and v.vat_region  = s.vat_region
                                                        and v.vat_type    IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                                             REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                        and v.active_date <= I_date))
                              union all
                              -- Supplier and Location have different vat region and there is no vat code with Zero vat rate
                              select ol.order_no trans_number,
                                     ol.item trans_item,
                                     locs.physical_loc trans_location,
                                     NULL tax_code,
                                     NULL tax_rate,
                                     ol.unit_cost unit_cost,
                                     REIM_CONSTANTS.YN_NO reverse_vat_ind,
                                     I_date eff_date,
                                     oh.currency_code currency_code
                                from ord_item_loc oil,
                                     v_im_ordloc ol,
                                     ordhead oh,
                                     sups s,
                                     (select wh location,
                                             REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                             physical_wh physical_loc,
                                             vat_region from wh
                                      union all
                                      select store location,
                                             REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                             store physical_loc,
                                             vat_region
                                        from store) locs,
                                     vat_region locs_vr,
                                     vat_region sups_vr
                               where I_tran_type           = REIM_CONSTANTS.TAX_TRAN_TYPE_PO
                                 and ol.order_no           = oil.order_no
                                 and ol.item               = oil.item
                                 and oh.order_no           = ol.order_no
                                 and oh.supplier           = s.supplier
                                 and ol.location           = locs.location
                                 and ol.loc_type           = locs.loc_type
                                 and oil.location          = locs.physical_loc
                                 and sups_vr.vat_region    = s.vat_region
                                 and locs_vr.vat_region    = locs.vat_region
                                 and locs_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and sups_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and NOT EXISTS (select 'x'
                                                   from vat_code_rates vr
                                                  where vr.vat_rate = 0
                                                    and vr.active_date <= I_date)
                                 and (   nvl(s.vat_region, -1) != nvl(locs.vat_region, -2)
                                      or NOT EXISTS (select 'x'
                                                       from vat_item v
                                                      where v.item        = ol.item
                                                        and v.vat_region  = s.vat_region
                                                        and v.vat_type    IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                                              REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                        and v.active_date <= I_date)))
                      union all
                      /******
                       get tax breakdown when tran_type is shipment
                      ******/
                      select REIM_CONSTANTS.TAX_TRAN_TYPE_SHPMNT trans_type,
                             trans_number,
                             trans_item,
                             trans_location,
                             tax_code,
                             tax_rate,
                             unit_cost,
                             reverse_vat_ind,
                             eff_date,
                             nvl((select max(icl.currency_cost_dec)
                                    from im_currencies icl
                                   where icl.currency_code = currency_code), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) decimal_digits
                        from (-- Supplier and Location have the same vat region
                              with ship_item_loc as (select value(ship_tbl) shipment,
                                                            value(item_tbl) item,
                                                            value(loc_tbl) location
                                                       from table(cast(I_transactions as obj_numeric_id_table)) ship_tbl,
                                                            table(cast(I_items as obj_varchar_id_table)) item_tbl,
                                                            table(cast(I_locations as obj_numeric_id_table)) loc_tbl),
                                   zero_vat_code as (select vr.vat_code,
                                                            vr.vat_rate,
                                                            vr.active_date
                                                       from vat_code_rates vr
                                                      where vr.vat_rate    = REIM_CONSTANTS.ZERO
                                                        and vr.active_date = (select Max(active_date)
                                                                                from vat_code_rates v
                                                                               where v.vat_rate    = vr.vat_rate
                                                                                 and v.vat_code    = vr.vat_code
                                                                                 and v.active_date <= I_date)
                                                        and ROWNUM < 2)
                              select ss.shipment trans_number,
                                     ss.item trans_item,
                                     locs.location trans_location,
                                     vi.vat_code tax_code,
                                     vi.vat_rate tax_rate,
                                     ss.unit_cost unit_cost,
                                     vi.reverse_vat_ind reverse_vat_ind,
                                     vi.active_date eff_date,
                                     oh.currency_code currency_code
                                from ship_item_loc sil,
                                     v_im_shipsku ss,
                                     shipment sh,
                                     ordhead oh,
                                     sups s,
                                     (select wh location,
                                             REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                             vat_region
                                        from wh
                                       where wh = physical_wh
                                      union all
                                      select store location,
                                             REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                             vat_region
                                        from store) locs,
                                     vat_region vr,
                                     vat_item vi
                               where I_tran_type         = REIM_CONSTANTS.TAX_TRAN_TYPE_SHPMNT
                                 and sh.shipment         = ss.shipment
                                 and oh.order_no         = sh.order_no
                                 and oh.supplier         = s.supplier
                                 and sh.bill_to_loc      = locs.location
                                 and sh.bill_to_loc_type = locs.loc_type
                                 and ss.item             = vi.item
                                 and sh.shipment         = sil.shipment
                                 and ss.item             = sil.item
                                 and sil.location        = locs.location
                                 and s.vat_region        = locs.vat_region
                                 and vr.vat_region       = locs.vat_region
                                 and vr.vat_calc_type    = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and locs.vat_region     = vi.vat_region
                                 and vi.vat_type         IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                             REIM_CONSTANTS.TAX_TYPE_BOTH)
                                 and vi.active_date      = (select max(active_date)
                                                              from vat_item v
                                                             where v.item        = vi.item
                                                               and v.vat_region  = vi.vat_region
                                                               and v.vat_type    IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                                                     REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                               and v.active_date <= I_date)
                              union all
                              -- Supplier and Location have different vat region and exists a vat code with Zero vat rate
                              select ss.shipment trans_number,
                                     ss.item trans_item,
                                     locs.location trans_location,
                                     zvc.vat_code tax_code,
                                     zvc.vat_rate tax_rate,
                                     ss.unit_cost unit_cost,
                                     REIM_CONSTANTS.YN_NO reverse_vat_ind,
                                     zvc.active_date eff_date,
                                     oh.currency_code currency_code
                                from ship_item_loc sil,
                                     v_im_shipsku ss,
                                     shipment sh,
                                     ordhead oh,
                                     sups s,
                                     (select wh location,
                                             REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                             vat_region
                                        from wh
                                       where wh = physical_wh
                                      union all
                                      select store location,
                                             REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                             vat_region
                                        from store) locs,
                                     vat_region locs_vr,
                                     vat_region sups_vr,
                                     zero_vat_code zvc
                               where I_tran_type           = REIM_CONSTANTS.TAX_TRAN_TYPE_SHPMNT
                                 and sh.shipment           = ss.shipment
                                 and oh.order_no           = sh.order_no
                                 and oh.supplier           = s.supplier
                                 and sh.bill_to_loc        = locs.location
                                 and sh.bill_to_loc_type   = locs.loc_type
                                 and locs_vr.vat_region    = locs.vat_region
                                 and sups_vr.vat_region    = s.vat_region
                                 and locs_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and sups_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and sh.shipment           = sil.shipment
                                 and ss.item               = sil.item
                                 and sil.location          = locs.location
                                 and (   nvl(s.vat_region, -1) != nvl(locs.vat_region, -2)
                                      or NOT EXISTS (select 'x'
                                                       from vat_item v
                                                      where v.item        = ss.item
                                                        and v.vat_region  = s.vat_region
                                                        and v.vat_type    IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                                              REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                        and v.active_date <= I_date))
                              union all
                              -- Supplier and Location have different vat region and there is no vat code with Zero vat rate
                              select ss.shipment trans_number,
                                     ss.item trans_item,
                                     locs.location trans_location,
                                     NULL tax_code,
                                     NULL tax_rate,
                                     ss.unit_cost unit_cost,
                                     REIM_CONSTANTS.YN_NO reverse_vat_ind,
                                     I_date eff_date,
                                     oh.currency_code currency_code
                                from ship_item_loc sil,
                                     v_im_shipsku ss,
                                     shipment sh,
                                     ordhead oh,
                                     sups s,
                                     (select wh location,
                                             REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                                             vat_region
                                        from wh
                                       where wh = physical_wh
                                      union all
                                      select store location,
                                             REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                                             vat_region
                                        from store) locs,
                                     vat_region locs_vr,
                                     vat_region sups_vr
                               where I_tran_type           = REIM_CONSTANTS.TAX_TRAN_TYPE_SHPMNT
                                 and sh.shipment           = ss.shipment
                                 and oh.order_no           = sh.order_no
                                 and oh.supplier           = s.supplier
                                 and sh.bill_to_loc        = locs.location
                                 and sh.bill_to_loc_type   = locs.loc_type
                                 and locs_vr.vat_region    = locs.vat_region
                                 and sups_vr.vat_region    = s.vat_region
                                 and locs_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and sups_vr.vat_calc_type = REIM_CONSTANTS.TAX_CALC_TYPE_SIMPLE
                                 and sh.shipment           = sil.shipment
                                 and ss.item               = sil.item
                                 and sil.location          = locs.location
                                 and NOT EXISTS (select 'x'
                                                   from vat_code_rates vr
                                                  where vr.vat_rate = 0
                                                    and vr.active_date <= I_date)
                                 and (   nvl(s.vat_region, -1) != nvl(locs.vat_region, -2)
                                      or NOT EXISTS (select 'x'
                                                       from vat_item v
                                                      where v.item        = ss.item
                                                        and v.vat_region  = s.vat_region
                                                        and v.vat_type    IN (REIM_CONSTANTS.TAX_TYPE_COST,
                                                                              REIM_CONSTANTS.TAX_TYPE_BOTH)
                                                        and v.active_date <= I_date)))
                      union all
                      --Custom Vat through RMS API
                      select I_tran_type,
                             to_number(tax_tbl.I_tran_id) trans_number,
                             tax_tbl.I_item trans_item,
                             to_number(tax_tbl.I_to_entity) trans_location,
                             tax_detl_tbl.tax_code tax_code,
                             tax_detl_tbl.tax_rate tax_rate,
                             tax_tbl.I_amount unit_cost,
                             'N' reverse_vat_ind,
                             tax_tbl.I_effective_from_date,
                             nvl((select max(icl.currency_cost_dec)
                                    from im_currencies icl
                                   where icl.currency_code = tax_tbl.I_amount_curr), REIM_CONSTANTS.DEFAULT_ROUNDUP_DIGITS) decimal_digits
                        from TABLE(CAST(L_custom_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_tbl,
                             TABLE(tax_tbl.O_tax_detail_tbl) tax_detl_tbl
                     ) trans);

    CURSOR C_XFORM_TAX_OBJ is
        select OBJ_TAX_CALC_REC(inner.item, -- I_item
                                NULL, -- I_pack_ind
                                inner.supplier, -- I_from_entity
                                REIM_CONSTANTS.TAX_ENTITY_TYPE_SUPP, -- I_from_entity_type
                                inner.location, -- I_to_entity
                                DECODE(inner.loc_type,
                                       REIM_CONSTANTS.LOC_TYPE_STORE, REIM_CONSTANTS.TAX_ENTITY_TYPE_STORE,
                                       REIM_CONSTANTS.TAX_ENTITY_TYPE_WH), -- I_to_entity_type
                                inner.eff_date,   -- I_effective_from_date
                                inner.unit_cost,  -- I_amount
                                inner.currency_code, -- I_amount_curr
                                'Y',             -- I_amount_tax_incl_ind
                                NULL,            -- I_origin_country_id
                                NULL,            -- O_cum_tax_pct
                                NULL,            -- O_cum_tax_value
                                NULL,            -- O_total_tax_amount
                                NULL,            -- O_total_tax_amount_curr
                                NULL,            -- O_total_recover_amount
                                NULL,            -- O_total_recover_amount_curr
                                NULL,            -- O_tax_detail_tbl
                                NULL,    -- I_tran_type
                                inner.eff_date,   -- I_tran_date
                                inner.tran_id,            -- I_tran_id
                                'C')             -- I_cost_retail_ind
          from (with trans as (select value(tran_tbl) tran_id,
                                      value(item_tbl) item,
                                      value(loc_tbl) location
                                 from table(cast(I_transactions as obj_numeric_id_table)) tran_tbl,
                                      table(cast(I_items as obj_varchar_id_table)) item_tbl,
                                      table(cast(I_locations as obj_numeric_id_table)) loc_tbl)
                -- Tran Type: Shipment
                select t.item,
                       oh.supplier,
                       locs.physical_loc location,
                       locs.loc_type,
                       I_date eff_date,
                       ss.unit_cost unit_cost,
                       oh.currency_code,
                       sh.shipment tran_id
                  from trans t,
                       shipment sh,
                       ordhead oh,
                       v_im_shipsku ss,
                       sups s,
                       (select wh location,
                               REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                               physical_wh physical_loc,
                               vat_region from wh
                        union all
                        select store location,
                               REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                               store physical_loc,
                               vat_region
                          from store) locs,
                       vat_region locs_vr,
                       vat_region sups_vr
                 where I_tran_type           = REIM_CONSTANTS.TAX_TRAN_TYPE_SHPMNT
                   and sh.shipment           = t.tran_id
                   and oh.order_no           = sh.order_no
                   and oh.supplier           = s.supplier
                   and sh.bill_to_loc        = locs.location
                   and sh.bill_to_loc_type   = locs.loc_type
                   and t.location            = locs.physical_loc
                   and ss.shipment           = t.tran_id
                   and ss.item               = t.item
                   and locs_vr.vat_region    = locs.vat_region
                   and sups_vr.vat_region    = s.vat_region
                   and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT NOT IN (locs_vr.vat_calc_type,
                                                                   sups_vr.vat_calc_type)
                   and REIM_CONSTANTS.TAX_CALC_TYPE_CUSTOM IN (locs_vr.vat_calc_type,
                                                               sups_vr.vat_calc_type)
                union all
                -- Tran Type: PO
                select t.item,
                       oh.supplier,
                       locs.physical_loc location,
                       locs.loc_type,
                       I_date eff_date,
                       ol.unit_cost unit_cost,
                       oh.currency_code,
                       ol.order_no tran_id
                  from trans t,
                       ordhead oh,
                       sups s,
                       v_im_ordloc ol,
                       (select wh location,
                               REIM_CONSTANTS.LOC_TYPE_WH loc_type,
                               physical_wh physical_loc,
                               vat_region from wh
                        union all
                        select store location,
                               REIM_CONSTANTS.LOC_TYPE_STORE loc_type,
                               store physical_loc,
                               vat_region
                          from store) locs,
                       vat_region locs_vr,
                       vat_region sups_vr
                 where I_tran_type           = REIM_CONSTANTS.TAX_TRAN_TYPE_PO
                   and oh.order_no           = t.tran_id
                   and oh.supplier           = s.supplier
                   and ol.order_no           = t.tran_id
                   and ol.item               = t.item
                   and ol.location           = locs.location
                   and ol.loc_type           = locs.loc_type
                   and t.location            = locs.physical_loc
                   and sups_vr.vat_region    = s.vat_region
                   and locs_vr.vat_region    = locs.vat_region
                   and REIM_CONSTANTS.TAX_CALC_TYPE_EXEMPT NOT IN (locs_vr.vat_calc_type,
                                                                   sups_vr.vat_calc_type)
                   and REIM_CONSTANTS.TAX_CALC_TYPE_CUSTOM IN (locs_vr.vat_calc_type,
                                                               sups_vr.vat_calc_type)) inner;

BEGIN
   LOGGER.LOG_INFORMATION('Begin - ' || L_program);

   open C_XFORM_TAX_OBJ;
   fetch C_XFORM_TAX_OBJ BULK COLLECT into L_custom_tax_calc_tbl;
   close C_XFORM_TAX_OBJ;

   LOGGER.LOG_INFORMATION('End of Cursor C_XFORM_TAX_OBJ ');

   if L_custom_tax_calc_tbl is NOT NULL and L_custom_tax_calc_tbl.COUNT > 0 then

      if CUSTOM_TAX_SQL.CALC_CTAX_COST_TAX(O_error_message,
                                           L_custom_tax_calc_tbl) = FALSE then
         RAISE custom_tax_error;
      end if;

   end if;

   OPEN C_FETCH_ITEM_TAX;
   FETCH C_FETCH_ITEM_TAX BULK COLLECT INTO O_tax_breakups;
   CLOSE C_FETCH_ITEM_TAX;

   LOGGER.LOG_INFORMATION('End of Cursor C_FETCH_ITEM_TAX ');

   LOGGER.LOG_INFORMATION('End - ' || L_program);

   O_status := REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when custom_tax_error then
      O_status := REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status := REIM_CONSTANTS.FAIL;

END GET_TAX_BREAKUP_SINGLETAX;

--------------------------------------------
FUNCTION CHECK_CN_REV_VAT_ELIGIBLITY(O_error_message     OUT VARCHAR2,
                                     O_is_eligible       OUT VARCHAR2,
                                     I_vendor_id      IN     IM_INJECT_DOC_HEAD.VENDOR%TYPE,
                                     I_doc_date       IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE,
                                     I_reference_cnr  IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF3%TYPE,
                                     I_reference_invc IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF4%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'REIM_TAX_DATA_SQL.CHECK_CN_REV_VAT_ELIGIBLITY';

   cursor C_GET_REV_VAT_IND(I_doc_reference IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF3%TYPE,
                            I_doc_type_list OBJ_VARCHAR_ID_TABLE) is
     select reverse_vat_ind
       from (select reverse_vat_ind
               from (select idh.reverse_vat_ind,
                            1 supp_rnk,
                            idh.doc_date
                       from im_doc_head idh
                      where idh.ext_doc_id = I_doc_reference
                        and idh.type       IN (select * from table(cast(I_doc_type_list as OBJ_VARCHAR_ID_TABLE)))
                        and idh.vendor     = I_vendor_id
                        and idh.doc_date   <= I_doc_date
                        and idh.status     <> REIM_CONSTANTS.DOC_STATUS_DELETE
                     union all
                     select idh.reverse_vat_ind,
                            2 supp_rnk,
                            idh.doc_date
                       from im_doc_head idh,
                            im_supplier_group_members isgm_cn,
                            im_supplier_group_members isgm_ref
                      where idh.ext_doc_id      = I_doc_reference
                        and idh.type            IN (select * from table(cast(I_doc_type_list as OBJ_VARCHAR_ID_TABLE)))
                        and idh.doc_date        <= I_doc_date
                        and idh.status          <> REIM_CONSTANTS.DOC_STATUS_DELETE
                        and isgm_cn.supplier = I_vendor_id
                        and isgm_ref.group_id   = isgm_cn.group_id
                        and idh.vendor          = isgm_ref.supplier)
              order by supp_rnk, doc_date desc)
      where rownum < 2;

      L_doc_type_list_MRCHI OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE(REIM_CONSTANTS.DOC_TYPE_MRCHI);

      L_doc_type_list_CRDNR OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE(REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                                                                         REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                                                                         REIM_CONSTANTS.DOC_TYPE_CRDNRT);
BEGIN

   LOGGER.LOG_INFORMATION('Begin - ' || L_program);

   O_is_eligible := REIM_CONSTANTS.YN_NO;

   if I_reference_cnr is NOT NULL then
      open C_GET_REV_VAT_IND(I_reference_cnr, L_doc_type_list_CRDNR);
      fetch C_GET_REV_VAT_IND into O_is_eligible;
      close C_GET_REV_VAT_IND;
   elsif I_reference_invc is NOT NULL then
      open C_GET_REV_VAT_IND(I_reference_invc, L_doc_type_list_MRCHI);
      fetch C_GET_REV_VAT_IND into O_is_eligible;
      close C_GET_REV_VAT_IND;
   end if;

   LOGGER.LOG_INFORMATION('End - ' || L_program);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;

END CHECK_CN_REV_VAT_ELIGIBLITY;
--------------------------------------------
FUNCTION FETCH_ZERO_RATE_TAX_CODE(O_error_message    OUT VARCHAR2,
                                  O_zero_tax_code    OUT VAT_CODE_RATES.VAT_CODE%TYPE,
                                  I_action_date   IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_TAX_DATA_SQL.FETCH_ZERO_RATE_TAX_CODE';

   cursor C_FETCH_ZERO_TAX_CODE is
      select vat_code
        from (select vcr.vat_code,
                     RANK() OVER (ORDER BY vcr.vat_code desc, active_date desc) vat_code_rank
                from vat_code_rates vcr
               where vcr.active_date < I_action_date
                 and vcr.vat_rate    = REIM_CONSTANTS.ZERO)
       where vat_code_rank = REIM_CONSTANTS.ONE;

   no_zero_rate_tax_code EXCEPTION;

BEGIN

   LOGGER.LOG_INFORMATION('Begin - ' || L_program);

   Open C_FETCH_ZERO_TAX_CODE;
   fetch C_FETCH_ZERO_TAX_CODE into O_zero_tax_code;
   close C_FETCH_ZERO_TAX_CODE;

   if O_zero_tax_code is NULL then
      RAISE no_zero_rate_tax_code;
   end if;

   LOGGER.LOG_INFORMATION('End - ' || L_program);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION

   when no_zero_rate_tax_code then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ZERO_RATE_TAX_CODE', --constant
                                            L_program);
      return REIM_CONSTANTS.FAIL;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;

END FETCH_ZERO_RATE_TAX_CODE;
-----------------------------------------------------------------------
END REIM_TAX_WRAPPER_SQL;
/