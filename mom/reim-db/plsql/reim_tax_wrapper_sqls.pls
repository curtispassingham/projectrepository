CREATE OR REPLACE PACKAGE REIM_TAX_WRAPPER_SQL AS
/************************************

REIM_TAX_WRAPPER_SQL

This package is meant to abstract ReIM from a concrete
implementation of processing modules related to taxes.

The procedures and functions in this package are meant
as pass-through to another system's APIs (ex. ORMS).

!!! Avoid putting in too much business logic around taxes
on the operations.

*************************************/

/****
CALCULATE_ITEM_TAXES FOR SINGLE TAX (VAT_ITEM)

Given the supplied arguments, create a table containing the tax breakdown for a given item.
Tax region resolution should take place inside of the procedure.
The following rules apply with regard to the criteria:

- When unit cost is not supplied, use the system cost of the item for the given date.
- When MRP is not supplied, use the system MRP of the item for the given date.
- When retail is not supplied, use the system retail of the item for the given date.
- When freight is not supplied, use the system freight amount for the given date.

Note that the term �system� is used loosely.  Some of the values required for calculation
may have to be resolved in means beyond simple table joins.  This information should all be
resolvable based on the arguments supplied as a part of this procedure.

The I_ITEM_TAX_CALC_OVRD input parameter is optional.
If supplied,  the procedure will calculate taxes using the supplied values in
this structure such as the tax basis formula,   tax rate, and application order
instead of the equivalent values in the system.
This feature is used for instance, in the Invoice-Matching application - in order
do hypothetical tax calculations to verify correctness of taxes on the invoice.
***/
--------------------------------------------
PROCEDURE CALCULATE_ITEM_TAXES_SINGLETAX (O_status                OUT NUMBER,                     -- Success status of the procedure.
                                          O_error_message         OUT VARCHAR2,                   -- Error message if the procedure failed.
                                          I_item_tax_criteria  IN     OBJ_ITEM_TAX_CRITERIA_TBL,  -- The tax criteria to calculate taxes for.
                                          I_item_tax_calc_ovrd IN     OBJ_ITEM_TAX_CALC_OVRD_TBL, -- Overriding values for calculating taxes.
                                          O_item_tax_results      OUT OBJ_ITEM_TAX_BREAK_TBL);    -- A table type for the results of the tax calculations, one row per tax
--------------------------------------------

/****
GET_VENDOR_TAX_CODES FOR SINGLE TAX (VAT_ITEM)

This procedure is responsible for collecting the applicable taxes for the specified
vendor on a specific date.  Vendor tax registration tables will have to be consulted
to determine if a tax is applicable for a given supplier.  Supplier sites should
also be considered when implementing this.  In the event supplier sites
functionality is enabled, this procedure should return all applicable supplier
site taxes as well for vendors of type supplier.
****/
--------------------------------------------
PROCEDURE GET_VENDOR_TAX_CODES_SINGLETAX (O_status           OUT NUMBER,                   -- Success status of the procedure.
                                          O_error_message    OUT VARCHAR2,                 -- Error message if the procedure failed.
                                          I_vendor_data   IN     OBJ_VENDOR_DATA_TBL,      -- A table containing the vendors we desire to return taxes for.
                                          I_date          IN     DATE,                     -- The date to use to determine taxes.
                                          I_tax_type      IN     VARCHAR2,                 -- The type of taxes to retreive.
                                          O_vendor_taxes     OUT OBJ_VENDOR_TAX_DATA_TBL); -- A table containing the valid vendor taxes for the date supplied.
--------------------------------------------

/****
GET_TAX_BREAKUP FOR SINGLE TAX (VAT_ITEM)

This procedure is responsible for collating a collection of applicable taxes
for the specified vendor on a specific date.  Vendor tax registration tables
will have to be consulted to determine if a tax is applicable for a given supplier.
Supplier sites should also be considered when implementing this.  In the event supplier
sites functionality is enabled, this procedure should return all applicable
supplier site taxes as well for vendors of type supplier.
****/
--------------------------------------------
PROCEDURE GET_TAX_BREAKUP_SINGLETAX (O_status           OUT NUMBER,                    -- Success status of the procedure.
                                     O_error_message    OUT VARCHAR2,                  -- Error message if the procedure failed.
                                     I_tran_type     IN     VARCHAR2,                  -- The transaction type to retrieve the breakup for.
                                     I_transactions  IN     OBJ_NUMERIC_ID_TABLE,      -- A collection of transaction numbers to retrieve breakups for.
                                     I_items         IN     OBJ_VARCHAR_ID_TABLE,      -- A collection of items to get the breakup for.  This field is optional.
                                     I_locations     IN     OBJ_NUMERIC_ID_TABLE,      -- A collection of items to get the breakup for.
                                     I_date          IN     DATE,                      -- The date to use to determine taxes.
                                     O_tax_breakups     OUT OBJ_ITEM_TAX_BREAKUP_TBL); -- A table of breakup records.
--------------------------------------------

-------------------------------------------------------------------------------------------
-- Function Name: CHECK_CN_REV_VAT_ELIGIBLITY
-- Purpose:       This function checks if reverse vat is applicable to the credit note.
--                Uses the reference CNR or INVC ext doc id values to check for eligibility.
-------------------------------------------------------------------------------------------
FUNCTION CHECK_CN_REV_VAT_ELIGIBLITY(O_error_message     OUT VARCHAR2,
                                     O_is_eligible       OUT VARCHAR2,
                                     I_vendor_id      IN     IM_INJECT_DOC_HEAD.VENDOR%TYPE,
                                     I_doc_date       IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE,
                                     I_reference_cnr  IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF3%TYPE,
                                     I_reference_invc IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF4%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
-- Function Name: FETCH_ZERO_RATE_TAX_CODE
-- Purpose:       This function fetches the vat code which is active on a given date and has Zero tax rate.
-------------------------------------------------------------------------------------------
FUNCTION FETCH_ZERO_RATE_TAX_CODE(O_error_message    OUT VARCHAR2,
                                  O_zero_tax_code    OUT VAT_CODE_RATES.VAT_CODE%TYPE,
                                  I_action_date   IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------
END REIM_TAX_WRAPPER_SQL;
/
