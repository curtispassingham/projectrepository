create or replace PACKAGE reim_variance_sql AS


    PACKAGE_ERROR  CONSTANT VARCHAR2(20) := 'PACKAGE_ERROR';



    --------------------------------------------
    PROCEDURE GET_VARIANCE_COST_QTY (
                            O_STATUS OUT VARCHAR2,
                            O_ERROR_MESSAGE  OUT VARCHAR2,
                            variance_qty  out number,
                            variance_unit_cost out number,
                            i_doc_id   in number,
                            i_item     in varchar2,
                            I_REASON_CODE   IN VARCHAR2);
    --------------------------------------------

END REIM_VARIANCE_SQL ;
/
