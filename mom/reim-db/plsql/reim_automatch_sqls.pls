CREATE OR REPLACE PACKAGE REIM_AUTO_MATCH_SQL AS

   INCLUDE_IND CONSTANT VARCHAR2(1) := 'I';
   EXCLUDE_IND CONSTANT VARCHAR2(1) := 'E';

   STATUS_INIT    CONSTANT VARCHAR2(8) := 'INIT';
   STATUS_PROCESS CONSTANT VARCHAR2(8) := 'PROCESS';
   STATUS_SUCCESS CONSTANT VARCHAR2(8) := 'SUCCESS';
   STATUS_FAILED  CONSTANT VARCHAR2(8) := 'FAILED';

   TASK_STATUS_STARTED CONSTANT VARCHAR2(8) := 'STARTED';
   TASK_STATUS_SUCCESS CONSTANT VARCHAR2(8) := 'SUCCESS';
   TASK_STATUS_FAILED  CONSTANT VARCHAR2(8) := 'FAILED';

   TASK_LVL_BATCH CONSTANT VARCHAR2(1) := 'B';
   TASK_LVL_CHUNK CONSTANT VARCHAR2(1) := 'C';
   TASK_LVL_LUW   CONSTANT VARCHAR2(1) := 'L';

   TASK_TYPE_INIT    CONSTANT VARCHAR2(1) := 'I';
   TASK_TYPE_MATCH   CONSTANT VARCHAR2(1) := 'M';
   TASK_TYPE_PERSIST CONSTANT VARCHAR2(1) := 'P';


/**
 * The public function used for Initializing the Match Process
 * Input param: I_supplier_id (The Supplier which needs to be included/excluded)
 *              I_incl_excl (Indicates whether to 'I'nclude or 'E'xclude)
 *              I_cost_pre_match_ind (Indicates whether to perform cost pre match logic or not. Y/N)
 *
 * Output param: O_workspace_id (Workspace id for the match process)
 *               O_auto_match_chunk_tbl (Table of LUWs and Chunks to be processed by the Batch)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION INIT_MATCH(O_error_message           OUT VARCHAR2,
                    O_workspace_id            OUT IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                    O_auto_match_chunk_tbl    OUT IM_AUTO_MATCH_LUW_TBL,
                    I_supplier_id          IN     SUPS.SUPPLIER%TYPE,
                    I_incl_excl            IN     VARCHAR2,
                    I_cost_pre_match_ind   IN     VARCHAR2 DEFAULT 'Y')
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The public function used to perform Summary All to All Match
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_stgy_dtl_id (Match Strategy Detail ID)
 *              I_match_luw_id (ID of the Match LUW)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_SMRY_ALL_2_ALL(O_error_message        OUT VARCHAR2,
                                I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The public function used to perform Summary One to Many Match.
 * Invoices would get grouped into groups of one invoice each.
 * Best Match: All combinations of receipts (for each group key) are formed as groups.
 * If the number of receipts exceed five then the best match switches to regular match which is an One to One Match.
 * Group header table holds all combinations of invoice and receipt groups
 *
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_stgy_dtl_id (Match Strategy Detail ID)
 *              I_match_luw_id (ID of the Match LUW)
 *              I_match_type (Match Type - Can be 'R'-Regular  or 'B'-Best Match)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_SMRY_ONE_2_MANY(O_error_message        OUT VARCHAR2,
                                 I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                 I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                 I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE, -- Match_type can be 'R'-Regular  or 'B'-Best Match
                                 I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)

RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The public function that performs Detail Matching on the Line items of the invoices in the chunk.
 * Match_type: Regular would match each invoice line item with the sum of all available receipt line item(same item)
 *             If more than one Invoice item matches to the sum of receipt items (same item) all invoice items would be marked as discrepant.
 * Match_type: Best Match would match each invoice line item with the sum of all available receipt line item(same item)
 *             If more than one Invoice item matches to the sum of receipt items (same item), best match is decided based on criteria.
 *             If undecided even after applying criteria, all invoice items would be marked as discrepant.
 * Group header table holds combinations of invoice and receipt groups(for the same item).
 *
 * Input param: I_workspace_id (ID of the Match process workspace)
 *              I_match_stgy_dtl_id (Match Strategy Detail ID)
 *              I_chunk_num (ID of the Chunk of Match Data)
 *              I_match_type (Match Type - Can be 'R'-Regular  or 'B'-Best Match)
 *
 * Output param: None
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION PERFORM_DETAIL_MATCHING(O_error_message        OUT VARCHAR2,
                                 I_workspace_id      IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                 I_match_stgy_dtl_id IN     IM_MATCH_STGY_DETAIL.MATCH_STGY_DTL_ID%TYPE,
                                 I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE, -- Match_type can be 'R'-Regular  or 'B'-Best Match
                                 I_match_luw_id      IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                 I_chunk_num         IN     IM_AUTO_MATCH_TASK_STATUS.CHUNK_NUM%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * Calls PERSIST_DETAIL_MATCH_DATA and PERSIST_SUMMARY_MATCH_DATA to persist matched data to OPS tables.
 */
FUNCTION PERSIST_MATCH_DATA(O_error_message    OUT VARCHAR2,
                            I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                            I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * Update Invoice Header and Shipment based on their details' statuses (for detail matches).
 * Update Invoice Header and Shipment for successful summary matches.
 * Create Summary Match History Data.
 * Create Receipt Item Posting Data for summary Matches
 *
 */
FUNCTION PERSIST_SUMMARY_MATCH_DATA(O_error_message    OUT VARCHAR2,
                                    I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE,
                                    I_match_luw_id  IN     IM_AUTO_MATCH_TASK_STATUS.MATCH_LUW_ID%TYPE,
                                    I_match_key_id  IN     IM_MATCH_INVC_WS.MATCH_KEY_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The function used for Creating Combinations(rcpt groups) of receipts.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION CREATE_RCPT_GROUPS(O_error_message     IN OUT VARCHAR2,
                            I_workspace_id      IN     IM_MATCH_GROUP_RCPT_WS.WORKSPACE_ID%TYPE,
                            I_match_luw_id      IN     IM_MATCH_GROUP_RCPT_WS.MATCH_LUW_ID%TYPE,
                            I_match_key_id      IN     IM_MATCH_GROUP_RCPT_WS.MATCH_KEY_ID%TYPE,
                            I_match_stgy_dtl_id IN     IM_MATCH_GROUP_RCPT_WS.MATCH_STGY_DTL_ID%TYPE,
                            I_match_type        IN     IM_MATCH_STGY_DETAIL.MATCH_TYPE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The function used for Creating Group head for One to Many Match.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION CREATE_MTCH_GROUP_HEAD(O_error_message     IN OUT VARCHAR2,
                                I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                                I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                                I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                                I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE,
                                I_match_type        IN     IM_MATCH_GROUP_HEAD_WS.MATCH_TYPE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The function used for Matching Group head based on tolerance for One to Many Match.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION MATCH_GROUP_HEAD(O_error_message     IN OUT VARCHAR2,
                          I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                          I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                          I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                          I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The function used for Checking SKU compliance.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION CHECK_SKU_COMPLIANCE(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                              I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                              I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                              I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The function used for Determining the best match for the invoices.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION DETERMINE_BEST_MATCH(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                              I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                              I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                              I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * Update Workspace tables with Summary match results.
 * Used for Summary One to Many (Batch) and Suggest Match (Online)
 */
FUNCTION UPD_WS_SMRY_MTCH_DATA(O_error_message     IN OUT VARCHAR2,
                               I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                               I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                               I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                               I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
/**
 * The public function used to perform the necessary actions to end the automatch batch.
 *      1. Updates Invoices which dont have receipts to URMTCH.
 *      2. Updates discrepant status of Invoice items which do not have corresponding receipt items provided the invoice is not matched at a summary level.
 *      3. Updates discrepant status of Invoice items which have receipts but not matched.
 * Input param: I_workspace_id (ID of the Match process workspace)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION END_MATCH(O_error_message    OUT VARCHAR2,
                   I_workspace_id  IN     IM_AUTO_MATCH_STATUS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
FUNCTION CHECK_TAX_COMPLIANCE(O_error_message     IN OUT VARCHAR2,
                              I_workspace_id      IN     IM_MATCH_GROUP_HEAD_WS.WORKSPACE_ID%TYPE,
                              I_match_luw_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_LUW_ID%TYPE,
                              I_match_key_id      IN     IM_MATCH_GROUP_HEAD_WS.MATCH_KEY_ID%TYPE,
                              I_match_stgy_dtl_id IN     IM_MATCH_GROUP_HEAD_WS.MATCH_STGY_DTL_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------
END REIM_AUTO_MATCH_SQL;
/
