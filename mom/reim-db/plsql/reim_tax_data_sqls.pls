CREATE OR REPLACE PACKAGE REIM_TAX_DATA_SQL AS
    /************************************
    
    REIM_TAX_DATA_SQL
    
    All operation ReIM requires to obtain tax information
    from the system are housed in this package.
    
    No calculations should be done in this package.
    
    This package depends on REIM_TAX_WRAPPER_SQL
    
    *************************************/

    PACKAGE_ERROR  CONSTANT VARCHAR2(20) := 'PACKAGE_ERROR';

    SUCCESS  CONSTANT NUMBER(10) := 1;
    FAIL  CONSTANT NUMBER(10) := 0;

    YN_YES  CONSTANT VARCHAR2(1) := 'Y';
    YN_NO  CONSTANT VARCHAR2(1) := 'N';


    /****
    GET_VENDOR_TAX_CODES

    This procedure is responsible for collecting the applicable taxes for the specified
    vendor on a specific date.  Vendor tax registration tables will have to be consulted
    to determine if a tax is applicable for a given supplier.  Supplier sites should
    also be considered when implementing this.  In the event supplier sites
    functionality is enabled, this procedure should return all applicable supplier
    site taxes as well for vendors of type supplier.
    ****/
    --------------------------------------------
    PROCEDURE GET_VENDOR_TAX_CODES (O_STATUS          OUT NUMBER, -- Success status of the procedure.
                                    O_ERROR_MESSAGE   OUT VARCHAR2, -- Error message if the procedure failed.
                                    I_VENDOR_DATA	  IN  OBJ_VENDOR_DATA_TBL, -- A table containing the vendors we desire to return taxes for.
                                    I_DATE            IN  DATE, -- The date to use to determine taxes.
                                    I_TAX_TYPE        IN  VARCHAR2, -- The type of taxes to retreive.
                                    O_VENDOR_TAXES    OUT OBJ_VENDOR_TAX_DATA_TBL -- A table containing the valid vendor taxes for the date supplied.
                                    );
    --------------------------------------------


    /****
    GET_TAX_BREAKUP

    This procedure is responsible for collating a collection of applicable taxes
    for the specified vendor on a specific date.  Vendor tax registration tables
    will have to be consulted to determine if a tax is applicable for a given supplier.
    Supplier sites should also be considered when implementing this.  In the event supplier
    sites functionality is enabled, this procedure should return all applicable
    supplier site taxes as well for vendors of type supplier.

    O_STATUS         OUT	NUMBER	Success status of the procedure.
    O_ERROR_MESSAGE  OUT	VARCHAR2	Error message if the procedure failed.
    I_TRAN_TYPE      IN	TAX_BREAKUP.TRAN_TYPE	The transaction type to retrieve the breakup for.
    I_TRANSACTIONS   IN	TAX_BREAKUP.TRAN_NUMBER (Collection)	A collection of transaction numbers to retrieve breakups for.
    I_ITEMS          IN	ITEM_MASTER.ITEM (Collection)	A collection of items to get the breakup for.  This field is optional.
    O_TAX_BREAKUPS   OUT	OBJ_ITEM_TAX_BREAKUP_TBL	A table of breakup records.
    ****/
    --------------------------------------------
    PROCEDURE GET_TAX_BREAKUP (O_STATUS         OUT  NUMBER, -- Success status of the procedure.
                                    O_ERROR_MESSAGE  OUT  VARCHAR2, -- Error message if the procedure failed.
                                    I_TRAN_TYPE      IN	  VARCHAR2, -- The transaction type to retrieve the breakup for.
                                    I_TRANSACTIONS   IN   OBJ_NUMERIC_ID_TABLE, -- 	A collection of transaction numbers to retrieve breakups for.
                                    I_ITEMS          IN   OBJ_VARCHAR_ID_TABLE, --	A collection of items to get the breakup for.  This field is optional.
                                    I_LOCATIONS      IN   OBJ_NUMERIC_ID_TABLE, --	A collection of items to get the breakup for. 
                                    I_DATE            IN  DATE, -- The date to use to determine taxes.
                                    O_TAX_BREAKUPS   OUT  OBJ_ITEM_TAX_BREAKUP_TBL -- A table of breakup records.
                                    );
	-------------------------------------------------------------------------------------------
	-- Function Name: CHECK_CN_REV_VAT_ELIGIBLITY
	-- Purpose:       This function checks if reverse vat is applicable to the credit note.
	--                Uses the reference CNR or INVC ext doc id values to check for eligibility.
	-------------------------------------------------------------------------------------------
	FUNCTION CHECK_CN_REV_VAT_ELIGIBLITY(O_error_message     OUT VARCHAR2,
	                                     O_is_eligible       OUT VARCHAR2,
	                                     I_vendor_id      IN     IM_INJECT_DOC_HEAD.VENDOR%TYPE,
	                                     I_doc_date       IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE,
	                                     I_reference_cnr  IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF3%TYPE,
	                                     I_reference_invc IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF4%TYPE)
	RETURN NUMBER;
	-------------------------------------------------------------------------------------------
END REIM_TAX_DATA_SQL;
/
