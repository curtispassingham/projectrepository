CREATE OR REPLACE PACKAGE REIM_MATCH_SQL AS
--------------------------------------------
SUCCESS  CONSTANT NUMBER(10) := 1;
FAIL     CONSTANT NUMBER(10) := 0;

YN_YES   CONSTANT VARCHAR2(1) := 'Y';
YN_NO    CONSTANT VARCHAR2(1) := 'N';

MATCH_TYPE_CRDNT_CRDNRQ  CONSTANT VARCHAR2(30) := 'CRDNT-CRDNRQ';
MATCH_TYPE_MRCHI_RCPT    CONSTANT VARCHAR2(30) := 'MRCHI-RCPT';

DOC_TYPE_INVOICE              CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'MRCHI';
DOC_TYPE_RECEIPT              CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'RCPT';
DOC_TYPE_CREDIT_NOTE          CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'CRDNT';
DOC_TYPE_CREDIT_NOTE_RQ_COST  CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'CRDNRC';
DOC_TYPE_CREDIT_NOTE_RQ_QTY   CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'CRDNRQ';
DOC_TYPE_CREDIT_NOTE_RQ_TAX   CONSTANT IM_DOC_HEAD.TYPE%TYPE := 'CRDNRT';

TOLERANCE_OWNER_SUPPLIER      CONSTANT VARCHAR2(30) := 'SUPPLIER';
TOLERANCE_OWNER_DEPARTMENT    CONSTANT VARCHAR2(30) := 'DEPARTMENT';
TOLERANCE_OWNER_SYSTEM        CONSTANT VARCHAR2(30) := 'SYSTEM';

MATCH_SIDE_LEFT               CONSTANT VARCHAR2(30) := 'LHS';
MATCH_SIDE_RIGHT              CONSTANT VARCHAR2(30) := 'RHS';

MATCH_LEVEL_SUMMARY           CONSTANT VARCHAR2(30) := 'summary.match';
MATCH_LEVEL_ONE_TO_ONE        CONSTANT VARCHAR2(30) := 'one.to.one';
MATCH_LEVEL_DETAIL            CONSTANT VARCHAR2(30) := 'detail.match';

MATCH_ERROR_PREFIX            CONSTANT VARCHAR2(50) := '|ERROR:';

FAVOR_OF_SUPPLIER   CONSTANT VARCHAR(20):= 'SUPP';
FAVOR_OF_RETAILER   CONSTANT VARCHAR(20):= 'RET';

---- SUMMARY MATCH
--------------------------------------------
PROCEDURE INIT_SUMMARY_MATCH(O_STATUS             OUT NUMBER,
                             O_ERROR_MESSAGE      OUT VARCHAR2,
                             I_PROCESS_ID      IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                             I_CONFIG_ID       IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                             I_SUPPLIER_ID     IN     SUPS.SUPPLIER%TYPE);

--------------------------------------------
PROCEDURE VALIDATE_SUMMARY_MATCH(O_STATUS         OUT NUMBER,
                                 O_ERROR_MESSAGE  OUT VARCHAR2,
                                 I_PROCESS_ID  IN IM_MATCH_DOC.PROCESS_ID%TYPE,
                                 I_CONFIG_ID   IN IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                 I_SUPPLIER_ID IN SUPS.SUPPLIER%TYPE);

---- ONE-TO-ONE MATCH
--------------------------------------------
PROCEDURE INIT_ONE_TO_ONE_MATCH(O_STATUS           OUT NUMBER,
                                O_ERROR_MESSAGE    OUT VARCHAR2,
                                I_PROCESS_ID    IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID     IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID   IN     SUPS.SUPPLIER%TYPE);
--------------------------------------------
PROCEDURE VALIDATE_ONE_TO_ONE_MATCH(O_STATUS           OUT NUMBER,
                                    O_ERROR_MESSAGE    OUT VARCHAR2,
                                    I_PROCESS_ID    IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                    I_CONFIG_ID     IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                    I_SUPPLIER_ID   IN     SUPS.SUPPLIER%TYPE);

---- DETAIL MATCH
--------------------------------------------
PROCEDURE INIT_DETAIL_MATCH (O_STATUS                OUT NUMBER,
                             O_ERROR_MESSAGE         OUT VARCHAR2,
                             I_PROCESS_ID         IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                             I_CONFIG_ID          IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                             I_SUPPLIER_ID        IN     SUPS.SUPPLIER%TYPE);

--------------------------------------------
PROCEDURE CREATE_DETAIL_DISCREPANCIES(O_STATUS           OUT NUMBER,
                                      O_ERROR_MESSAGE    OUT VARCHAR2,
                                      I_PROCESS_ID    IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                      I_CONFIG_ID     IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                      I_SUPPLIER_ID   IN     SUPS.SUPPLIER%TYPE);
--------------------------------------------
PROCEDURE VALIDATE_DETAIL_MATCH(O_STATUS           OUT NUMBER,
                                O_ERROR_MESSAGE    OUT VARCHAR2,
                                I_PROCESS_ID    IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID     IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID   IN     SUPS.SUPPLIER%TYPE);

---- OTHER PROCEDURES AND FUNCTIONS

--------------------------------------------
PROCEDURE DETERMINE_MATCH_TOLERANCES(O_STATUS            OUT NUMBER,
                                     O_ERROR_MESSAGE     OUT VARCHAR2,
                                     I_PROCESS_ID     IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                     I_CONFIG_ID      IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                     I_SUPPLIER_ID    IN     SUPS.SUPPLIER%TYPE);

--------------------------------------------
PROCEDURE APPLY_MATCH_TOLERANCES(O_STATUS             OUT NUMBER,
                                 O_ERROR_MESSAGE      OUT VARCHAR2,
                                 I_PROCESS_ID      IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                 I_CONFIG_ID       IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                 I_SUPPLIER_ID   IN     SUPS.SUPPLIER%TYPE,
                                 I_MATCH_LEVEL     IN     IM_MATCH_POOL_RESULTS.MATCH_LEVEL%TYPE);

--------------------------------------------
PROCEDURE PROCESS_SUCCESSFUL_MATCHES(O_STATUS           OUT NUMBER,
                                     O_ERROR_MESSAGE    OUT VARCHAR2,
                                     I_PROCESS_ID    IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                     I_CONFIG_ID     IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                     I_SUPPLIER_ID   IN     SUPS.SUPPLIER%TYPE,
                                     I_MATCH_LEVEL   IN     IM_MATCH_POOL_RESULTS.MATCH_LEVEL%TYPE);

--------------------------------------------
PROCEDURE GET_VALID_DEFAULT_REASON_CODES(O_STATUS          OUT NUMBER,
                                         O_ERROR_MESSAGE   OUT VARCHAR2,
                                         O_CBC_REASON      OUT IM_SYSTEM_OPTIONS.DFLT_COST_OVERBILL_RC%TYPE,
                                         O_CBQ_REASON      OUT IM_SYSTEM_OPTIONS.DFLT_QTY_OVERBILL_RC%TYPE,
                                         O_CMC_REASON      OUT IM_SYSTEM_OPTIONS.DFLT_COST_UNDERBILL_RC%TYPE,
                                         O_CMQ_REASON      OUT IM_SYSTEM_OPTIONS.DFLT_QTY_UNDERBILL_RC%TYPE);

--------------------------------------------
PROCEDURE VALIDATE_DISCREPANCIES(O_STATUS            OUT NUMBER,
                                O_ERROR_MESSAGE      OUT VARCHAR2,
                                I_PROCESS_ID      IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                I_CONFIG_ID       IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                I_SUPPLIER_ID     IN     SUPS.SUPPLIER%TYPE,
                                I_MATCH_LEVEL     IN     IM_MATCH_POOL_RESULTS.MATCH_LEVEL%TYPE);


--------------------------------------------
FUNCTION GET_RESOLUTION_DATE(I_DUE_DATE         DATE,
                             I_CURRENT_DATE     DATE,
                             I_DISCREPANCY_TYPE VARCHAR2)
    RETURN DATE;


--------------------------------------------
FUNCTION GET_FAVOR_OF(I_RETAILER_AMT       NUMBER,
                      I_SUPPLIER_AMT       NUMBER,
                      I_MATCH_TYPE         VARCHAR2)
    RETURN VARCHAR2;


--------------------------------------------
FUNCTION GET_MATCH_SIDE (I_DOC_TYPE VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------
FUNCTION AS_ERROR(I_MSG  IM_MATCH_POOL_RESULTS.RESULT_TXT%TYPE)
RETURN VARCHAR2;


--------------------------------------------
FUNCTION IS_ERROR(I_TXT IM_MATCH_POOL_RESULTS.RESULT_TXT%TYPE)
RETURN VARCHAR2;


--------------------------------------------
FUNCTION IS_CREDIT_NOTE_REQUEST(DOC_TYPE    IM_DOC_HEAD.TYPE%TYPE)
RETURN VARCHAR2;

--------------------------------------------
PROCEDURE VALIDATE_MATCH_RESULTS (O_STATUS           OUT NUMBER,
                                  O_ERROR_MESSAGE    OUT VARCHAR2,
                                  I_PROCESS_ID    IN     IM_MATCH_DOC.PROCESS_ID%TYPE,
                                  I_CONFIG_ID     IN     IM_MATCH_POOL_CONFIG.CONFIG_ID%TYPE,
                                  I_SUPPLIER_ID   IN     SUPS.SUPPLIER%TYPE);
--------------------------------------------
END REIM_MATCH_SQL;
/
