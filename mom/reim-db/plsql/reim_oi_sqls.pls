CREATE OR REPLACE PACKAGE REIM_OI_SQL AS

   THREE_MONTHS   CONSTANT NUMBER(2)    := 84;

--------------------------------------------------------------------
/**
 * The public function used to generate the session ID.
 *
 *
 * Output param: O_session_id (The Newly generated Session ID)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION GET_NEXT_SESSION_ID(O_error_message    OUT VARCHAR2,
                             O_session_id       OUT OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
/**
 * The public function used to fetch Supplier Site Information.
 *
 * Input param: I_session_id       (OI Session Id of the User)
 *              I_supplier_site_id (The ID of the Supplier Site)
 *
 * Output param: O_result (An Object Type Containing Supplier Site Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_SUPPLIER_INFO(O_error_message       OUT VARCHAR2,
                             O_result           IN OUT REIM_OI_SUPP_SITE_INFO_REC,
                             I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                             I_supplier_site_id IN     SUPS.SUPPLIER%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to fetch Quantity Comparison Information.
 *
 * Input param: I_session_id       (OI Session Id of the User)
 *              I_workspace_id     (The ID of the User's Matching Workspace)
 *              I_item_view_ws_id  (The Detail match Workspace ID of the highlighted row on Detail match screen)
 *              I_iitem_view_ws_id (The Invoice Item View Workspace ID of the highlighted row on Invoice Item View screen)
 *              I_item_view_ws_id  (The Discrepancy List Workspace ID of the highlighted row on Discrepancy List screen)
 *
 * Output param: O_result (An Object Type Containing Quantity Comparison Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_QTY_COMP_INFO(O_error_message       OUT VARCHAR2,
                             O_result           IN OUT REIM_OI_QUANTITY_INFO_REC,
                             I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                             I_workspace_id     IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                             I_item_view_ws_id  IN     IM_DETAIL_MATCH_WS.DETAIL_MATCH_WS_ID%TYPE,
                             I_iitem_view_ws_id IN     IM_INVC_ITEM_VIEW_WS.INVC_ITEM_VIEW_WS_ID%TYPE,
                             I_disc_view_ws_id  IN     IM_DISCREPANCY_LIST_WS.DISCREPANCY_LIST_WS_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to fetch Tolerance Range Information for Summary Match Screen.
 *
 * Input param: I_session_id   (OI Session Id of the User)
 *              I_workspace_id (The ID of the User's Matching Workspace)
 *
 * Output param: O_result (An Object Type Containing Tolerance Range Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_SMRY_TOLERANCE_INFO(O_error_message    OUT VARCHAR2,
                                   O_result        IN OUT REIM_OI_TOLERANCE_INFO_REC,
                                   I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                   I_workspace_id  IN     IM_MATCH_INVC_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to fetch Tolerance Range Information for Detail Match Screen.
 *
 * Input param: I_session_id       (OI Session Id of the User)
 *              I_workspace_id     (The ID of the User's Matching Workspace)
 *              I_item_view_ws_id  (The Detail match Workspace ID of the highlighted row on Detail match screen)
 *              I_iitem_view_ws_id (The Invoice Item View Workspace ID of the highlighted row on Invoice Item View screen)
 *
 * Output param: O_result (An Object Type Containing Tolerance Range Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_DETL_TOLERANCE_INFO(O_error_message       OUT VARCHAR2,
                                   O_result           IN OUT REIM_OI_TOLERANCE_INFO_REC,
                                   I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                   I_workspace_id     IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                                   I_item_view_ws_id  IN     IM_DETAIL_MATCH_WS.DETAIL_MATCH_WS_ID%TYPE,
                                   I_iitem_view_ws_id IN     IM_INVC_ITEM_VIEW_WS.INVC_ITEM_VIEW_WS_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to fetch Cost events Information.
 *
 * Input param: I_session_id       (OI Session Id of the User)
 *              I_workspace_id     (The ID of the User's Matching Workspace)
 *              I_item_view_ws_id  (The Detail match Workspace ID of the highlighted row on Detail match screen)
 *              I_iitem_view_ws_id (The Invoice Item View Workspace ID of the highlighted row on Invoice Item View screen)
 *              I_item_view_ws_id  (The Discrepancy List Workspace ID of the highlighted row on Discrepancy List screen)
 *
 * Output param: O_result (An Object Type Containing Cost Event Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_COST_EVENT_INFO(O_error_message       OUT VARCHAR2,
                               O_result           IN OUT REIM_OI_COST_EVENT_INFO_REC,
                               I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                               I_workspace_id     IN     IM_DETAIL_MATCH_WS.WORKSPACE_ID%TYPE,
                               I_item_view_ws_id  IN     IM_DETAIL_MATCH_WS.DETAIL_MATCH_WS_ID%TYPE,
                               I_iitem_view_ws_id IN     IM_INVC_ITEM_VIEW_WS.INVC_ITEM_VIEW_WS_ID%TYPE,
                               I_disc_view_ws_id  IN     IM_DISCREPANCY_LIST_WS.DISCREPANCY_LIST_WS_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to fetch Auto Match rate Information.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Output param: O_result (An Object Type Containing Auto Match rate Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_AUTO_MATCH_RATE_INFO(O_error_message    OUT VARCHAR2,
                                    O_result        IN OUT REIM_OI_AUTOMATCH_RATE_REC,
                                    I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to fetch Upcoming Invoices Information.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Output param: O_result (An Object Type Containing Upcoming Invoices Information)
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FETCH_UPCMNG_INVC_INFO(O_error_message    OUT VARCHAR2,
                                O_result        IN OUT REIM_OI_UPCMNG_INVC_INFO_REC,
                                I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to Create Employee Invoice Report.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Populates im_oi_employee_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_EMPLOYEE_REPORT(O_error_message    OUT VARCHAR2,
                                I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to Create Supplier Sites Report.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Populates im_oi_supp_site_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_SUPP_SITE_REPORT(O_error_message    OUT VARCHAR2,
                                 I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to Filter Records on Supplier Site Report.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *              I_filter_type (Filter Type used - Can be 'D'(Dept),'SS'(Supplier Site))
 *              I_filter_dept (Department Identifier)
 *              I_filter_supp_site (Supplier Site Identifier)
 *
 * Updates im_oi_supp_site_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION FILTER_SUPP_SITE_REPORT(O_error_message       OUT VARCHAR2,
                                 I_session_id       IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                 I_filter_type      IN     VARCHAR2,
                                 I_filter_dept      IN     DEPS.DEPT%TYPE,
                                 I_filter_supp_site IN     SUPS.SUPPLIER%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to Create Invoice Report.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * Populates im_oi_invoice_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION CREATE_INVOICE_REPORT(O_error_message    OUT VARCHAR2,
                               I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to Delete records from Employee Invoice Report belonging to the user session.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * deletes from im_oi_employee_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION DELETE_EMPLOYEE_REPORT(O_error_message    OUT VARCHAR2,
                                I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to Delete records from Supplier Sites Report belonging to the user session.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * deletes from im_oi_supp_site_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION DELETE_SUPP_SITE_REPORT(O_error_message    OUT VARCHAR2,
                                 I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
/**
 * The public function used to Delete records Invoice Report belonging to the user session.
 *
 * Input param: I_session_id (OI Session Id of the User)
 *
 * deletes from im_oi_invoice_report
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION DELETE_INVOICE_REPORT(O_error_message    OUT VARCHAR2,
                               I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------
END REIM_OI_SQL;
/
