CREATE OR REPLACE PACKAGE REIM_TAX_CALC_SQL AS
    /************************************
    
    REIM_TAX_CALC_SQL
    
    This package houses all operations around
    calculation of taxes.
    
    May use REIM_TAX_WRAPPER and other REIM_TAX* packages.    
    *************************************/

    PACKAGE_ERROR  CONSTANT VARCHAR2(20) := 'PACKAGE_ERROR';

    SUCCESS  CONSTANT NUMBER(10) := 1;
    FAIL  CONSTANT NUMBER(10) := 0;

    YN_YES  CONSTANT VARCHAR2(1) := 'Y';
    YN_NO  CONSTANT VARCHAR2(1) := 'N';


    /****
    CALCULATE_ITEM_TAXES

    Given the supplied arguments, create a table containing the tax breakdown for a given item.
    Tax region resolution should take place inside of the procedure.
    The following rules apply with regard to the criteria:

    - When unit cost is not supplied, use the system cost of the item for the given date.
    - When MRP is not supplied, use the system MRP of the item for the given date.
    - When retail is not supplied, use the system retail of the item for the given date.
    - When freight is not supplied, use the system freight amount for the given date.

    Note that the term �system� is used loosely.  Some of the values required for calculation
    may have to be resolved in means beyond simple table joins.  This information should all be
    resolvable based on the arguments supplied as a part of this procedure.

    The I_ITEM_TAX_CALC_OVRD input parameter is optional.
    If supplied,  the procedure will calculate taxes using the supplied values in
    this structure such as the tax basis formula,   tax rate, and application order
    instead of the equivalent values in the system.
    This feature is used for instance, in the Invoice-Matching application - in order
    do hypothetical tax calculations to verify correctness of taxes on the invoice.
    ***/
    --------------------------------------------
    PROCEDURE CALCULATE_ITEM_TAXES (O_STATUS              OUT   NUMBER,  -- Success status of the procedure.
	                                O_ERROR_MESSAGE       OUT   VARCHAR2, -- Error message if the procedure failed.
	                                I_ITEM_TAX_CRITERIA   IN    OBJ_ITEM_TAX_CRITERIA_TBL, -- The tax criteria to calculate taxes for.
	                                I_ITEM_TAX_CALC_OVRD  IN    OBJ_ITEM_TAX_CALC_OVRD_TBL, -- Overriding values for calculating taxes.
                                    O_ITEM_TAX_RESULTS    OUT   OBJ_ITEM_TAX_BREAK_TBL -- A table type for the results of the tax calculations, one row per tax
	                                );
    --------------------------------------------

END REIM_TAX_CALC_SQL;
/
