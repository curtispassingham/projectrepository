CREATE OR REPLACE PACKAGE BODY REIM_PURGE_SQL AS
------------------------------------------------------------------

LP_status_days NUMBER(4) := 365;

------------------------------------------------------------------
FUNCTION DELETE_DOCUMENT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER;
--
FUNCTION DELETE_MISC(O_error_message IN OUT VARCHAR2)
RETURN NUMBER;
--
FUNCTION REBUILD_WS_INDEX(O_error_message  IN OUT VARCHAR2,
                          I_ws_table_name  IN     VARCHAR2,
                          I_ws_table_owner IN     VARCHAR2)
RETURN NUMBER;
--
FUNCTION DELETE_INJECT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------
FUNCTION CLEAR_DOCUMENT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program       VARCHAR2(61) := 'REIM_PURGE_SQL.CLEAR_DOCUMENT';
   L_start_time    TIMESTAMP := SYSTIMESTAMP;
   L_vdate         DATE := get_vdate;
   L_doc_hist_days im_system_options.doc_hist_days%TYPE;

BEGIN

   select doc_hist_days
     into L_doc_hist_days
     from im_system_options;

   -- delete deleted and posted status that reference other documents
   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date (number_1, number_2)
   select dh.doc_id, dh.group_id
     from im_doc_head dh
    where dh.status in(REIM_CONSTANTS.DOC_STATUS_DELETE)
      and (dh.ref_doc            is not null or
           dh.ref_cnr_ext_doc_id is not null or
           dh.ref_inv_ext_doc_id is not null)
      and not exists (select f.doc_id from im_financials_stage f where f.doc_id = dh.doc_id)
   union all
   select dh.doc_id, dh.group_id
     from im_doc_head dh
    where dh.status in(REIM_CONSTANTS.DOC_STATUS_POSTED)
      and dh.post_date + L_doc_hist_days < L_vdate
      and (dh.ref_doc            is not null or
           dh.ref_cnr_ext_doc_id is not null or
           dh.ref_inv_ext_doc_id is not null)
      and not exists (select f.doc_id from im_financials_stage f where f.doc_id = dh.doc_id)
   union all
   select dh.doc_id, dh.group_id
     from im_doc_head dh
    where dh.status in(REIM_CONSTANTS.DOC_STATUS_MTCH)
      and dh.type in(REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                     REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                     REIM_CONSTANTS.DOC_TYPE_CRDNRT)
      and dh.match_date + L_doc_hist_days < L_vdate
      and (dh.ref_doc            is not null or
           dh.ref_cnr_ext_doc_id is not null or
           dh.ref_inv_ext_doc_id is not null)
      and not exists (select f.doc_id from im_financials_stage f where f.doc_id = dh.doc_id);

   LOGGER.LOG_INFORMATION('delete docs with refs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --remove docs that still have docs in their group that are not on the gtt.
   delete from gtt_num_num_str_str_date_date d
    where d.number_1 in(select gtt3.number_1
                          from gtt_num_num_str_str_date_date gtt3,
                               (select dh.doc_id, dh.group_id
                                  from im_doc_head dh,
                                       gtt_num_num_str_str_date_date gtt1
                                 where dh.group_id = gtt1.number_2
                                minus
                                select gtt2.number_1, gtt2.number_2
                                  from gtt_num_num_str_str_date_date gtt2) inner
                         where inner.group_id = gtt3.number_2);

   LOGGER.LOG_INFORMATION('remove docs that still have docs in their group remaining - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);


   if DELETE_DOCUMENT(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   -- delete deleted and posted status that are not referenced by other documents
   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date (number_1, number_2)
   select dh.doc_id, dh.group_id
     from im_doc_head dh
    where dh.status in(REIM_CONSTANTS.DOC_STATUS_DELETE)
      and not exists (select 'x' from im_doc_head refdoc
                        where (refdoc.ref_doc            = dh.doc_id or
                               refdoc.ref_cnr_ext_doc_id = dh.ext_doc_id or
                               refdoc.ref_inv_ext_doc_id = dh.ext_doc_id))
      and not exists (select f.doc_id from im_financials_stage f where f.doc_id = dh.doc_id)
   union all
   select dh.doc_id, dh.group_id
     from im_doc_head dh
    where dh.status in(REIM_CONSTANTS.DOC_STATUS_POSTED)
      and to_date(dh.post_date + L_doc_hist_days) < L_vdate
      and not exists (select 'x' from im_doc_head refdoc
                        where (refdoc.ref_doc            = dh.doc_id or
                               refdoc.ref_cnr_ext_doc_id = dh.ext_doc_id or
                               refdoc.ref_inv_ext_doc_id = dh.ext_doc_id))
      and not exists (select f.doc_id from im_financials_stage f where f.doc_id = dh.doc_id)
   union all
   select dh.doc_id, dh.group_id
     from im_doc_head dh
    where dh.status in(REIM_CONSTANTS.DOC_STATUS_MTCH)
      and dh.type in(REIM_CONSTANTS.DOC_TYPE_CRDNRC,
                     REIM_CONSTANTS.DOC_TYPE_CRDNRQ,
                     REIM_CONSTANTS.DOC_TYPE_CRDNRT)
      and to_date(dh.match_date + L_doc_hist_days) < L_vdate
      and not exists (select 'x' from im_doc_head refdoc
                        where (refdoc.ref_doc            = dh.doc_id or
                               refdoc.ref_cnr_ext_doc_id = dh.ext_doc_id or
                               refdoc.ref_inv_ext_doc_id = dh.ext_doc_id))
      and not exists (select f.doc_id from im_financials_stage f where f.doc_id = dh.doc_id);

   LOGGER.LOG_INFORMATION('delete docs with out refs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --remove docs that still have docs in their group that are not on the gtt.
   delete from gtt_num_num_str_str_date_date d
    where d.number_1 in(select gtt3.number_1
                          from gtt_num_num_str_str_date_date gtt3,
                               (select dh.doc_id, dh.group_id
                                  from im_doc_head dh,
                                       gtt_num_num_str_str_date_date gtt1
                                 where dh.group_id = gtt1.number_2
                                minus
                                select gtt2.number_1, gtt2.number_2
                                  from gtt_num_num_str_str_date_date gtt2) inner
                         where inner.group_id = gtt3.number_2);

   LOGGER.LOG_INFORMATION('2 remove docs that still have docs in their group remaining - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if DELETE_DOCUMENT(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   if DELETE_MISC(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CLEAR_DOCUMENT;
------------------------------------------------------------------
FUNCTION DELETE_DOCUMENT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program       VARCHAR2(61) := 'REIM_PURGE_SQL.DELETE_DOCUMENT';
   L_start_time    TIMESTAMP := SYSTIMESTAMP;

BEGIN

--match hist -- 1 doc in set, delete set

   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select match_id from im_summary_match_invc_history 
    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   delete from im_summary_match_invc_history where match_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_summary_match_rcpt_history where match_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_summary_match_history      where match_id in(select number_1 from gtt_6_num_6_str_6_date);

   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select match_id from im_detail_match_invc_history 
    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   delete from im_detail_match_invc_history where match_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_detail_match_rcpt_history where match_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_detail_match_history      where match_id in(select number_1 from gtt_6_num_6_str_6_date);

   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select match_id from im_sum_match_cn_dtl_hist 
    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   delete from im_sum_match_cn_dtl_hist     where match_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_sum_match_cn_hist         where match_id in(select number_1 from gtt_6_num_6_str_6_date);

   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select match_id from im_detail_match_cn_dtl_hist 
    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   delete from im_detail_match_cn_dtl_hist where match_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_detail_match_cn_hist     where match_id in(select number_1 from gtt_6_num_6_str_6_date);

--manual grp -- 1 doc in set, delete set
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select group_id from im_manual_group_invoices 
    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   delete from im_manual_group_invoices where group_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_manual_group_receipts where group_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_manual_groups         where group_id in(select number_1 from gtt_6_num_6_str_6_date);

   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select group_id from im_cn_manual_groups 
    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   delete from im_cn_manual_groups      where group_id in(select number_1 from gtt_6_num_6_str_6_date); 

--deal
   delete from im_complex_deal_detail_tax  where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_complex_deal_detail      where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

--level from im_doc_detail_reason_codes
   delete from im_doc_detail_rc_tax
    where im_doc_detail_reason_codes_id in(select im_doc_detail_reason_codes_id
                                             from im_doc_detail_reason_codes
                                            where doc_id in(select number_1 from gtt_num_num_str_str_date_date)); 
   delete from im_cn_detail_match_his         where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

--level from im_fixed_deal_detail
   delete from im_fixed_deal_detail_tax       where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_fixed_deal_detail           where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

--level from rcpt posting
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select seq_no from im_rcpt_item_posting_invoice 
    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   delete from im_rcpt_item_posting_invoice   where seq_no in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_receipt_item_posting        where seq_no in(select number_1 from gtt_6_num_6_str_6_date);

--level from rcpt postint stage
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select seq_no from im_rcpt_item_posting_inv_stage
    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   delete from im_rcpt_item_posting_inv_stage where seq_no in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_receipt_item_posting_stage  where seq_no in(select number_1 from gtt_6_num_6_str_6_date);


--level from im_invoice_detail
   delete from im_invoice_detail_allowance    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

--1 level up from im_doc_head
   delete from im_order_item_tax_audit        where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_item_tax_audit              where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_doc_sys_tax_posting         where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_doc_tax                     where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_cn_summary_match_his        where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_doc_detail_comments         where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_doc_head_comments           where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_doc_non_merch_tax           where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_doc_non_merch               where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_invoice_detail_allw_tax     where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_invoice_detail_tax          where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_invoice_detail              where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_resolution_action           where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_tax_discrepancy             where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_tax_discrepancy_history     where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_doc_detail_reason_codes     where doc_id in(select number_1 from gtt_num_num_str_str_date_date);
   delete from im_inv_document                where inv_id in(select number_1 from gtt_num_num_str_str_date_date)
                                                 or ref_doc_id in(select number_1 from gtt_num_num_str_str_date_date);

--AP stage Audit table
   delete from im_ap_stage_detail_audit where doc_id in (select number_1 from gtt_num_num_str_str_date_date);

--unmatch tables
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_1)
   select ius.unmtch_id 
     from im_unmtch_status ius
    where doc_id in (select number_1 from gtt_num_num_str_str_date_date);

   delete from im_unmtch_detl_match_rcpt_hist where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_detl_match_invc_hist where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_detl_match_hist      where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_smry_match_rcpt_hist where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_smry_match_invc_hist where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_smry_match_hist      where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_resolution_action    where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_doc_detl_comments    where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_part_mtch_rcpt       where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_rcpt_item_post_invc  where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_rcpt_item_posting    where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_unmtch_status               where unmtch_id in(select number_1 from gtt_6_num_6_str_6_date);

--im_doc_head
   delete from im_doc_head                    where doc_id in(select number_1 from gtt_num_num_str_str_date_date);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DELETE_DOCUMENT;
------------------------------------------------------------------
FUNCTION DELETE_MISC(O_error_message IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program       VARCHAR2(61) := 'REIM_PURGE_SQL.DELETE_MISC';
   L_start_time    TIMESTAMP := SYSTIMESTAMP;
   L_vdate         DATE      := get_vdate;

BEGIN

   --delete after LP_status_days
   delete from im_auto_match_task_status where (start_datetime + LP_status_days) < L_vdate;
   delete from im_auto_match_status      where (start_datetime + LP_status_days) < L_vdate;
   delete from im_posting_status         where (creation_date  + LP_status_days) < L_vdate;

   --delete when no im_inject_doc_head rows exist (For EDI Injector)
   delete from im_inject_status i 
    where i.inject_type                   = REIM_CONSTANTS.INJECT_TYPE_EDI
      and (i.start_date + LP_status_days) < L_vdate
      and NOT EXISTS (select 'x'
                        from im_inject_doc_head idh
                       where idh.inject_id = i.inject_id);

   --rms order
   delete from im_receiver_cost_adjust rca where not exists (select 'x' from ordhead oh where oh.order_no = rca.order_no);

   --rms shipment
   delete from im_partially_matched_receipts d where not exists (select 'x' from shipment s where s.shipment = d.shipment);
   delete from im_receiver_unit_adjust d       where not exists (select 'x' from shipment s where s.shipment = d.shipment);
   delete from im_rwo_shipment_hist d          where not exists (select 'x' from shipment s where s.shipment = d.shipment);
   delete from im_rwo_shipsku_hist d           where not exists (select 'x' from shipment s where s.shipment = d.shipment);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DELETE_MISC;
------------------------------------------------------------------
FUNCTION CLEAR_WORKSPACES(O_error_message IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program       VARCHAR2(61) := 'REIM_PURGE_SQL.CLEAR_WORKSPACES';
   L_owner         VARCHAR2(30);
   L_start_time    TIMESTAMP := SYSTIMESTAMP;

BEGIN

   select table_owner
     into L_owner
    from system_config_options;

   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_CNR_SEARCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_CN_SEARCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_DETAIL_MATCH_CN_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_DETAIL_MATCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_DISCREPANCY_LIST_RCPT_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_DISCREPANCY_LIST_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_DOC_INVC_SEARCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_EDI_DOC_SEARCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_INVC_DETL_RCPT_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_CNR_DETL_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_CNR_TAX_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_CNR_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_CN_DETL_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_CN_TAX_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_CN_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_GROUP_INVC_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_GROUP_RCPT_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_GROUP_HEAD_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_INQ_DETAIL_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_INQ_SEARCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_INVC_SEARCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_RCPT_SEARCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_INVC_TAX_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_RCPT_TAX_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_INVC_DETL_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_INVC_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_RCPT_DETL_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_RCPT_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_RESOLUTION_ACTION_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_REVERSE_DEBIT_MEMO_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_TAX_REVIEW_LIST_SEARCH_WS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_INTRA_TAX_TMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_COST_VAR';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_COST_VAR_HIST';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_DOC_HIST';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_POOL_ITEM';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_POOL_ITEM_HIST';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_POOL_RESULTS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_POOL_RESULTS_HIST';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_POOL_TOLERANCES';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_QTY_VAR';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_QTY_VAR_HIST';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_MATCH_DOC';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_POSTING_DOC_ACCOUNTS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_POSTING_DOC_AMOUNTS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_POSTING_DOC_DEPTCLASS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_POSTING_DOC_ERRORS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_POSTING_DOC';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_PRORATE_WORKSPACE';
    
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.IM_OBIEE_PAYLOAD';

   if REBUILD_WS_INDEX(O_error_message,'IM_CNR_SEARCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_CN_SEARCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_DETAIL_MATCH_CN_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_DETAIL_MATCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_DISCREPANCY_LIST_RCPT_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_DISCREPANCY_LIST_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_DOC_INVC_SEARCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_EDI_DOC_SEARCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_INVC_DETL_RCPT_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_CNR_DETL_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_CNR_TAX_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_CNR_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_CN_DETL_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_CN_TAX_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_CN_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_GROUP_INVC_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_GROUP_RCPT_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_GROUP_HEAD_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_INQ_DETAIL_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_INQ_SEARCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_INVC_SEARCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_RCPT_SEARCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_INVC_TAX_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_RCPT_TAX_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_INVC_DETL_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_INVC_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_RCPT_DETL_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_RCPT_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_RESOLUTION_ACTION_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_REVERSE_DEBIT_MEMO_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_TAX_REVIEW_LIST_SEARCH_WS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_INTRA_TAX_TMP',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_COST_VAR',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_COST_VAR_HIST',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_DOC_HIST',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_POOL_ITEM',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_POOL_ITEM_HIST',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_POOL_RESULTS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_POOL_RESULTS_HIST',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_POOL_TOLERANCES',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_QTY_VAR',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_QTY_VAR_HIST',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_MATCH_DOC',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_POSTING_DOC_ACCOUNTS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_POSTING_DOC_AMOUNTS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_POSTING_DOC_DEPTCLASS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_POSTING_DOC_ERRORS',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_POSTING_DOC',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;
   if REBUILD_WS_INDEX(O_error_message,'IM_PRORATE_WORKSPACE',L_owner) = REIM_CONSTANTS.FAIL then return REIM_CONSTANTS.FAIL; end if;

   if DELETE_INJECT(O_error_message) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END CLEAR_WORKSPACES;
------------------------------------------------------------------
FUNCTION REBUILD_WS_INDEX(O_error_message  IN OUT VARCHAR2,
                          I_ws_table_name  IN     VARCHAR2,
                          I_ws_table_owner IN     VARCHAR2)
RETURN NUMBER IS

   L_program       VARCHAR2(61) := 'REIM_PURGE_SQL.REBUILD_WS_INDEX';
   L_start_time    TIMESTAMP := SYSTIMESTAMP;

   cursor c_index is
      select di.index_name
        from dba_indexes di
       where di.table_name  = I_ws_table_name
         and di.table_owner = I_ws_table_owner;

BEGIN

   for rec in c_index loop
      EXECUTE IMMEDIATE 'ALTER INDEX '||I_ws_table_owner||'.'||rec.index_name||' REBUILD';
   end loop;
  
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END REBUILD_WS_INDEX;
------------------------------------------------------------------
FUNCTION DELETE_INJECT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program    VARCHAR2(61) := 'REIM_PURGE_SQL.DELETE_INJECT';
   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_s9t_purge_days IM_SYSTEM_OPTIONS.S9T_PURGE_DAYS%TYPE;

BEGIN

   select s9t_purge_days
     into L_s9t_purge_days
     from im_system_options;

   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(number_2,
                                      number_1,
                                      varchar2_1)
   select iidh.inject_id,
          iidh.inject_doc_id,
          'Y'
     from im_inject_doc_head iidh
    where iidh.workspace_type IN (REIM_CONSTANTS.INJECT_WORKSPACE_DOC_MAINTAIN,
                                  REIM_CONSTANTS.INJECT_WORKSPACE_INJECT_COPY)
   union all
   select distinct iidr.inject_id,
          iidr.inject_doc_id,
          case
             when (iis.last_update_date + L_s9t_purge_days) < sysdate then
                'Y'
             else
                'N'
          end
     from im_inject_status iis,
          im_inject_doc_row iidr
    where iis.inject_type    = REIM_CONSTANTS.INJECT_TYPE_S9T
      and iidr.inject_id (+) = iis.inject_id;

   delete from im_inject_doc_detail_allowance   where detail_id in(select detail_id
                                                                     from im_inject_doc_detail
                                                                    where inject_doc_id in(select number_1
                                                                                             from gtt_6_num_6_str_6_date));
   delete from im_inject_doc_detail_allow_tax   where detail_id in(select detail_id
                                                                     from im_inject_doc_detail
                                                                    where inject_doc_id in(select number_1
                                                                                             from gtt_6_num_6_str_6_date));
   delete from im_inject_doc_detail_tax         where detail_id in(select detail_id
                                                                     from im_inject_doc_detail
                                                                    where inject_doc_id in(select number_1
                                                                                             from gtt_6_num_6_str_6_date));

   delete from im_inject_doc_error              where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date where varchar2_1 = 'Y');
   delete from im_inject_complex_deal_detail    where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_complex_deal_tax       where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_doc_comments           where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_doc_non_merch_tax      where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_doc_record             where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_doc_tax                where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_fixed_deal_tax         where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_fixed_deal_detail      where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_tax_discrepancy        where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_doc_non_merch          where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_doc_detail             where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date);
   delete from im_inject_doc_head               where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date where varchar2_1 = 'Y');
   delete from im_inject_doc_row                where inject_doc_id in(select number_1 from gtt_6_num_6_str_6_date where varchar2_1 = 'Y');
   --delete inject_request based on inject_id
   delete from im_inject_request                where inject_id in(select number_2 from gtt_6_num_6_str_6_date where varchar2_1 = 'Y');
   --delete inject_status for Induction
   delete from im_inject_status                 where inject_type = REIM_CONSTANTS.INJECT_TYPE_S9T and inject_id in(select number_2 from gtt_6_num_6_str_6_date where varchar2_1 = 'Y');

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program,L_start_time);
   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END DELETE_INJECT;
------------------------------------------------------------------
END REIM_PURGE_SQL;
/
