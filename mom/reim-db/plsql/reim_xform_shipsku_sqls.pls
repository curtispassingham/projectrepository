CREATE OR REPLACE PACKAGE REIM_XFORM_SHIPSKU_SQL AS
---------------------------------------------------------------
/**
 * The public function used for Transforming Shipsku records to be used in ReIM.
 * Uses im_transform_shipsku_gtt.
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION TRANSFORM_SHIPSKU_GTT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------------
/**
 * This function populates im_transform_shipsku_gtt based on the passed in 
 * shipments and then uses TRANSFORM_SHIPSKU_GTT to do the transformation.
 */
FUNCTION POP_TRANSFORM_SHIPSKU_GTT(O_error_message     IN OUT VARCHAR2,
                                   I_shipments         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
---------------------------------------------------------------
/**
 * This function populates im_transform_shipsku_gtt based on the passed in
 * shipments/items and then uses TRANSFORM_SHIPSKU_GTT to do the transformation.
 */
FUNCTION POP_TRANSFORM_SHIPSKU_GTT(O_error_message         IN OUT VARCHAR2,
                                   I_im_ship_item_qtys_tbl IN     IM_SHIP_ITEM_QTYS_TBL)
RETURN NUMBER;
---------------------------------------------------------------
END REIM_XFORM_SHIPSKU_SQL;
/
