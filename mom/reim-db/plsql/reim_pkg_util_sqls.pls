CREATE OR REPLACE PACKAGE REIM_PKG_UTIL_SQL AS 
-----------------------------------------------------
FUNCTION GET_TOKEN(i_string  VARCHAR2,
                   i_delim  VARCHAR2,
                   i_token_num  NUMBER) 
RETURN VARCHAR2; 
-----------------------------------------------------
PROCEDURE LOG_TIME(I_log_message    IN VARCHAR2,
                   I_start_time     IN TIMESTAMP);
-----------------------------------------------------
END REIM_PKG_UTIL_SQL;
/

