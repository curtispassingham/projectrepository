CREATE OR REPLACE PACKAGE BODY SHIPMENT_API_SQL AS
--------------------------------------------------------
FUNCTION UPDATE_INVOICE_MATCH_STATUS(O_error_message OUT VARCHAR2,
                                     I_shipments      IN  SHIPMENT_IDS,
                                     I_new_status    IN  VARCHAR2)
   RETURN BOOLEAN IS

   L_vdate DATE := null;

BEGIN

   L_vdate := GET_VDATE;

   FOR i in 1..I_shipments.COUNT LOOP
      if not UPDATE_INVOICE_MATCH_STATUS(O_error_message,
                                         I_shipments(i),
                                         I_new_status,
                       L_vdate) then
         return false;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SHIPMENT_API_SQL',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_INVOICE_MATCH_STATUS;
--------------------------------------------------------
FUNCTION UPDATE_INVOICE_QTY_MATCHED(O_error_message         IN OUT VARCHAR2,
                                    I_im_ship_item_qtys_tbl IN     IM_SHIP_ITEM_QTYS_TBL)
RETURN NUMBER IS

   L_program    VARCHAR2(61) := 'SHIPMENT_API_SQL.UPDATE_INVOICE_QTY_MATCHED';
   L_shipments  OBJ_NUMERIC_ID_TABLE;


   L_remaining_qty_to_match   NUMBER       := 0;
   L_shipment                 NUMBER       := -9909909;
   L_item                     VARCHAR2(25) := 'FaKeItEMid';
   L_qty_to_be_matched        BOOLEAN      := true;
   L_seq_no                   NUMBER       := 0;
   L_qty_received             NUMBER       := 0;
   L_qty_matched              NUMBER       := 0;
   L_possible_match_qty       NUMBER       := 0;

   cursor c_xform is
      select gtt.shipment,
             gtt.seq_no,
             gtt.item,
             gtt.ss_qty_received,
             gtt.transform_qty_received qty_received,
             gtt.transform_qty_matched qty_matched
        from im_transform_shipsku_gtt gtt
       where gtt.shipment = L_shipment
         and gtt.item     = L_item;

BEGIN

   if REIM_XFORM_SHIPSKU_SQL.POP_TRANSFORM_SHIPSKU_GTT(O_error_message,
                                                       I_im_ship_item_qtys_tbl) = REIM_CONSTANTS.FAIL then
      return 0;
   end if;

   for i in 1..I_im_ship_item_qtys_tbl.count loop

      if L_shipment != I_im_ship_item_qtys_tbl(i).shipment or L_item != I_im_ship_item_qtys_tbl(i).item then

         L_shipment               := I_im_ship_item_qtys_tbl(i).shipment;
         L_item                   := I_im_ship_item_qtys_tbl(i).item;
         L_remaining_qty_to_match := I_im_ship_item_qtys_tbl(i).qty_matched;

      end if;

      if (L_remaining_qty_to_match <> 0) then
    
         for c_rec in c_xform loop

            L_seq_no             := c_rec.seq_no;
            L_qty_received       := c_rec.qty_received;
            L_qty_matched        := c_rec.qty_matched;
            L_possible_match_qty := L_qty_received - nvl(L_qty_matched, 0);

            if (L_possible_match_qty <> 0) then 

               if (L_remaining_qty_to_match <= L_possible_match_qty) then

                  --use the ratio between qty_received and ss_qty_received to update the match qty
                  update shipsku ss 
                     set ss.qty_matched = nvl(qty_matched, 0) + L_remaining_qty_to_match * (c_rec.ss_qty_received/c_rec.qty_received)
                   where ss.shipment = L_shipment
                     and ss.item     = L_item
                     and ss.seq_no   = L_seq_no;

               else 

                  --consume all the remaining qty
                  update shipsku ss
                     set ss.qty_matched = nvl(ss.qty_received,0) - nvl(ss.qty_matched,0)
                   where ss.shipment = L_shipment
                     and ss.item     = L_item
                     and ss.seq_no   = L_seq_no;

               end if;

               L_remaining_qty_to_match := L_remaining_qty_to_match - L_possible_match_qty;

            end if;
         
            if (L_remaining_qty_to_match <= 0) then
               exit;
            end if;

         end loop; --shipsku

      end if; 

   end loop; --input

   return 1;

EXCEPTION
   when OTHERS then
       O_error_message :=  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;
END UPDATE_INVOICE_QTY_MATCHED;
--------------------------------------------------------
FUNCTION UPDATE_INVOICE_QTY_MATCHED(O_error_message OUT VARCHAR2,
                                     I_shipment_item_qtys     IN  SHIPMENT_MATCHED_QTY_ARRAY)
   RETURN BOOLEAN IS

   L_remaining_qty_to_match NUMBER := 0;
   L_shipment_id NUMBER := 0;
   L_item_id VARCHAR2(25) := null;
   L_qty_to_be_matched BOOLEAN := true;
   L_seq_no NUMBER := 0;
   L_qty_received NUMBER := 0;
   L_qty_matched NUMBER := 0;
   L_possible_match_qty NUMBER := 0;
   L_supp_pack_size NUMBER := 1;
        
    CURSOR C_SHIPSKUS IS
      SELECT SS.SEQ_NO,
            SS.QTY_RECEIVED,
            SS.QTY_MATCHED,
            1 AS SUPP_PACK_SIZE
       FROM V_IM_SHIPSKU SS
      WHERE SS.SHIPMENT = L_SHIPMENT_ID
        AND SS.ITEM = L_ITEM_ID
        AND SS.ITEM IN (SELECT ITEM FROM ITEM_MASTER WHERE CATCH_WEIGHT_TYPE='2' OR CATCH_WEIGHT_TYPE='4')
      UNION ALL
     SELECT SS.SEQ_NO,
            SS.QTY_RECEIVED,
            SS.QTY_MATCHED,
            1 AS SUPP_PACK_SIZE
       FROM V_IM_SHIPSKU SS,
            SHIPMENT S,
            ORDHEAD OH,
            SUPS SP
      WHERE SS.SHIPMENT = L_SHIPMENT_ID
        AND SS.ITEM = L_ITEM_ID
        AND SS.ITEM NOT IN (SELECT ITEM FROM ITEM_MASTER WHERE CATCH_WEIGHT_TYPE='2' OR CATCH_WEIGHT_TYPE='4')
        AND SS.SHIPMENT = S.SHIPMENT
        AND OH.ORDER_NO = S.ORDER_NO
        AND SP.SUPPLIER = OH.SUPPLIER
        AND SP.SUP_QTY_LEVEL = 'EA'
      UNION ALL
     SELECT SS.SEQ_NO,
            SS.QTY_RECEIVED,
            SS.QTY_MATCHED,
            OS.SUPP_PACK_SIZE AS SUPP_PACK_SIZE
       FROM V_IM_SHIPSKU SS,
            SHIPMENT S,
            ORDHEAD OH,
            ORDSKU OS,
            SUPS SP
      WHERE SS.SHIPMENT = L_SHIPMENT_ID
        AND SS.ITEM = L_ITEM_ID
        AND SS.ITEM NOT IN (SELECT ITEM FROM ITEM_MASTER WHERE CATCH_WEIGHT_TYPE='2' OR CATCH_WEIGHT_TYPE='4')
        AND SS.SHIPMENT = S.SHIPMENT
        AND OH.ORDER_NO = S.ORDER_NO
        AND SP.SUPPLIER = OH.SUPPLIER
        AND SP.SUP_QTY_LEVEL = 'CA'
        AND OS.ORDER_NO=OH.ORDER_NO
        AND OS.ITEM=SS.ITEM
      ORDER BY SEQ_NO;
        
   
BEGIN
  
 FOR i in 1..I_shipment_item_qtys.COUNT LOOP
    
    L_shipment_id := I_shipment_item_qtys(i).SHIPMENT_ID;
    L_item_id := I_shipment_item_qtys(i).ITEM;
    L_remaining_qty_to_match := I_shipment_item_qtys(i).QTY_MATCHED;
    
    
    if (L_remaining_qty_to_match <> 0) then
    
       for c_rec in C_shipskus loop
         L_seq_no := c_rec.seq_no;
         L_qty_received   := c_rec.qty_received;
         L_qty_matched    := c_rec.qty_matched;
         L_supp_pack_size := c_rec.supp_pack_size;
      
          L_possible_match_qty := L_qty_received - nvl(L_qty_matched, 0);
         
         if (L_possible_match_qty <> 0) then 
            if ( L_remaining_qty_to_match <= L_possible_match_qty) then
                 update shipsku 
                        set qty_matched = nvl(qty_matched, 0) + L_remaining_qty_to_match * L_supp_pack_size 
                  where shipment        = L_shipment_id 
                    and item            = L_item_id 
                    and seq_no          = L_seq_no;
            else 
               update shipsku 
                  set qty_matched = nvl(qty_matched, 0) + L_possible_match_qty * L_supp_pack_size 
                where shipment    = L_shipment_id 
                  and item        = L_item_id 
                  and seq_no      = L_seq_no;
            end if;
            L_remaining_qty_to_match := L_remaining_qty_to_match - L_possible_match_qty;
         end if;
         
         if (L_remaining_qty_to_match <= 0) then
            exit;
         end if;
       end loop;
    end if;
        
 END LOOP;
  
 return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message :=  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM || '->Shipment Id[ '||L_shipment_id || ' ] Item id [ '||L_item_id ||']',
                                             'SHIPMENT_API_SQL',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_INVOICE_QTY_MATCHED;
--------------------------------------------------------
FUNCTION UPDATE_INVOICE_MATCH_STATUS(O_error_message OUT VARCHAR2,
                                     I_shipment      IN  NUMBER,
                                     I_new_status    IN  VARCHAR2,
                                     I_vdate         IN  DATE)
   RETURN BOOLEAN IS

   cursor C_LOCK_SHIPMENT is
      select 'x'
        from shipment
       where shipment = I_shipment
         for update nowait;

BEGIN

  open C_LOCK_SHIPMENT;
  close C_LOCK_SHIPMENT;

  UPDATE shipment
      set invc_match_status = I_new_status,
          invc_match_date = I_vdate
    where shipment = I_shipment;

  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SHIPMENT_API_SQL',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_INVOICE_MATCH_STATUS;
--------------------------------------------------------
FUNCTION CLEAR_STAGED_SHIPMENTS(O_error_message OUT VARCHAR2,
                    I_shipments      IN  SHIPMENT_IDS)
   RETURN BOOLEAN IS

BEGIN

   -- construct a where clause for delete
   if I_shipments is not null and
      I_shipments.COUNT > 0 then

   DELETE FROM STAGE_PURGED_SHIPSKUS
   WHERE SHIPMENT NOT IN ( SELECT *
            FROM TABLE ( CAST ( I_SHIPMENTS
                      AS SHIPMENT_IDS ) ) );
                      
   DELETE FROM STAGE_PURGED_SHIPMENTS
   WHERE SHIPMENT NOT IN ( SELECT *
            FROM TABLE ( CAST ( I_SHIPMENTS
                      AS SHIPMENT_IDS ) ) );
                                         
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SHIPMENT_API_SQL',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CLEAR_STAGED_SHIPMENTS;
--------------------------------------------------------
FUNCTION DEDUCT_INVOICE_QTY_MATCHED(O_error_message            OUT VARCHAR2,
                                    I_im_ship_item_qtys_tbl IN     IM_SHIP_ITEM_QTYS_TBL)
RETURN NUMBER IS

   L_program    VARCHAR2(61) := 'SHIPMENT_API_SQL.DEDUCT_INVOICE_QTY_MATCHED';

BEGIN

	LOGGER.LOG_INFORMATION(L_program ||' Start ');

   if REIM_XFORM_SHIPSKU_SQL.POP_TRANSFORM_SHIPSKU_GTT(O_error_message,
                                                       I_im_ship_item_qtys_tbl) = REIM_CONSTANTS.FAIL then
      return REIM_CONSTANTS.FAIL;
   end if;

   merge into shipsku tgt
   using (select shipment,
                 seq_no,
                 item,
                 deduct_qty
            from (select gtt.shipment,
                         gtt.seq_no,
                         gtt.item,
                         gtt.invc_match_status,
                         gtt.ss_qty_matched gtt_qty_matched,
                         tbl.qty_matched * (gtt.ss_qty_received/gtt.transform_qty_received) tbl_qty_matched
                    from im_transform_shipsku_gtt gtt,
                         table(cast(I_im_ship_item_qtys_tbl AS IM_SHIP_ITEM_QTYS_TBL)) tbl
                   where gtt.shipment    = tbl.shipment
                     and gtt.item        = tbl.item
                     and tbl.qty_matched <> REIM_CONSTANTS.ZERO) inner
                   model
                      partition by (shipment,
                                    item)
                      dimension by (ROW_NUMBER() OVER (PARTITION BY shipment,
                                                                    item
                                                           ORDER BY NVL(invc_match_status, REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH) desc,
                                                                    seq_no) rn)
                      measures (seq_no,
                                SUM(NVL(gtt_qty_matched,0)) OVER (PARTITION BY shipment,
                                                                               item
                                                                      ORDER BY Nvl(invc_match_status, REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH) DESC,
                                                                               seq_no) run_total,
                                0 deduct_qty,
                                gtt_qty_matched,
                                tbl_qty_matched)
                      rules sequential order (deduct_qty[rn] = case
                                                                  when run_total[CV(rn)] <= tbl_qty_matched[CV(rn)] then
                                                                     gtt_qty_matched[CV(rn)]
                                                                  else
                                                                     case
                                                                        when CV(rn) = 1 then
                                                                           tbl_qty_matched[CV(rn)]
                                                                        else
                                                                           case
                                                                              when tbl_qty_matched[CV(rn)] - run_total[CV(rn) - 1] > REIM_CONSTANTS.ZERO then
                                                                                 tbl_qty_matched[CV(rn)] - run_total[CV(rn) - 1]
                                                                              else
                                                                                 REIM_CONSTANTS.ZERO
                                                                           end
                                                                     end
                                                               end)) src
   on (    tgt.shipment   = src.shipment
       and tgt.item       = src.item
       and tgt.seq_no     = src.seq_no
       and src.deduct_qty > REIM_CONSTANTS.ZERO)
   when MATCHED then
      update
         set tgt.qty_matched       = tgt.qty_matched - src.deduct_qty,
             tgt.invc_match_status = REIM_CONSTANTS.SSKU_IM_STATUS_UNMTCH;

   LOGGER.LOG_INFORMATION(L_program||' Deduct matched quantity from Shipsku - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
       O_error_message :=  SQL_LIB.CREATE_MSG(REIM_CONSTANTS.PACKAGE_ERROR,
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      return REIM_CONSTANTS.FAIL;

END DEDUCT_INVOICE_QTY_MATCHED;
--------------------------------------------------------
END SHIPMENT_API_SQL;
/
