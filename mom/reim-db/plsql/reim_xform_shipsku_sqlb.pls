CREATE OR REPLACE PACKAGE BODY REIM_XFORM_SHIPSKU_SQL AS

/**
 * The public function used for Transforming Shipsku records to be used in ReIM.
 * Uses im_transform_shipsku_gtt.
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION TRANSFORM_SHIPSKU_GTT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER
IS

   L_program VARCHAR2(50) := 'REIM_XFORM_SHIPSKU_SQL.TRANSFORM_SHIPSKU_GTT';

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program);

   --catch_weight_type 3,1,NULL and sup_qty_level = 'EA', no transform needed

   --catch_weight_type 3,1,NULL and sup_qty_level = 'CA'
   merge into im_transform_shipsku_gtt target
   using (select gtt.shipment,
                 gtt.item,
                 gtt.seq_no,
                 gtt.transform_qty_received / os.supp_pack_size transform_qty_received,
                 gtt.transform_qty_matched  / os.supp_pack_size transform_qty_matched,
                 gtt.transform_unit_cost    * os.supp_pack_size transform_unit_cost
            from im_transform_shipsku_gtt gtt,
                 ordsku os
           where gtt.sup_qty_level     = 'CA'
             and (   gtt.catch_weight_type IN ('3',
                                               '1')
                  or gtt.catch_weight_type is NULL)
             and gtt.order_no          = os.order_no
             and gtt.item              = os.item
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item
       and target.seq_no   = use_this.seq_no)
   when matched then update
    set target.transform_qty_received = use_this.transform_qty_received,
        target.transform_qty_matched  = use_this.transform_qty_matched,
        target.transform_unit_cost    = use_this.transform_unit_cost;

   LOGGER.LOG_INFORMATION('Merge RowCount - catch_weight_type 3,1,NULL and sup_qty_level = CA: ' || SQL%ROWCOUNT);

   --catch_weight_type 4
   merge into im_transform_shipsku_gtt target
   using (select gtt.shipment,
                 gtt.item,
                 gtt.seq_no,
                 case when gtt.weight_received_uom is null or gtt.weight_received_uom = isc.cost_uom then
                         gtt.weight_received
                      when uc.operator = 'M' then
                         nvl (gtt.weight_received, 0) * uc.factor
                      else
                         nvl (gtt.weight_received, 0) / uc.factor
                 end as transform_qty_received,
                 gtt.transform_unit_cost / nvl(iscd.net_weight, 1) * isc.supp_pack_size transform_unit_cost
            from im_transform_shipsku_gtt gtt,
                 ordsku os,
                 item_supp_country isc,
                 item_supp_country_dim iscd,
                 uom_conversion uc
           where gtt.catch_weight_type   = '4'
             and gtt.order_no            = os.order_no
             and gtt.item                = os.item
             and gtt.supplier            = isc.supplier
             and gtt.item                = isc.item
             and os.origin_country_id    = isc.origin_country_id
             and isc.supplier            = iscd.supplier
             and isc.item                = iscd.item
             and isc.origin_country_id   = iscd.origin_country
             and iscd.dim_object         = 'CA'
             and gtt.weight_received_uom = uc.from_uom(+)
             and isc.cost_uom            = uc.to_uom(+)
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item
       and target.seq_no   = use_this.seq_no)
   when matched then update
    set target.transform_qty_received = use_this.transform_qty_received,
        target.transform_unit_cost    = use_this.transform_unit_cost;

   LOGGER.LOG_INFORMATION('Merge RowCount - catch_weight_type 4: ' || SQL%ROWCOUNT);

   --catch_weight_type 2
   merge into im_transform_shipsku_gtt target
   using (select gtt.shipment,
                 gtt.item,
                 gtt.seq_no,
                 case when gtt.weight_received_uom is null or gtt.weight_received_uom = isc.cost_uom then
                         gtt.weight_received
                      when uc.operator = 'M' then
                         nvl (gtt.weight_received, 0) * uc.factor
                      else
                         nvl (gtt.weight_received, 0) / uc.factor
                 end transform_qty_received,
                 gtt.transform_unit_cost / nvl(pi.pack_qty,1) transform_unit_cost
            from im_transform_shipsku_gtt gtt,
                 ordsku os,
                 item_supp_country isc,
                 packitem pi,
                 uom_conversion uc
           where gtt.catch_weight_type   = '2'
             and gtt.order_no            = os.order_no
             and gtt.item                = os.item
             and gtt.supplier            = isc.supplier
             and gtt.item                = isc.item
             and os.origin_country_id    = isc.origin_country_id
             and gtt.item                = pi.pack_no
             and gtt.weight_received_uom = uc.from_uom(+)
             and isc.cost_uom            = uc.to_uom(+)
   ) use_this
   on (    target.shipment = use_this.shipment
       and target.item     = use_this.item
       and target.seq_no   = use_this.seq_no)
   when matched then update
    set target.transform_qty_received = use_this.transform_qty_received,
        target.transform_unit_cost    = use_this.transform_unit_cost;

   LOGGER.LOG_INFORMATION('Merge RowCount - catch_weight_type 2: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('End ' || L_program);

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END TRANSFORM_SHIPSKU_GTT;
------------------------------------------------------------------------------------------
FUNCTION POP_TRANSFORM_SHIPSKU_GTT(O_error_message     IN OUT VARCHAR2,
                                   I_shipments         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program            VARCHAR2(61)   := 'REIM_XFORM_SHIPSKU_SQL.POP_TRANSFORM_SHIPSKU_GTT';

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program);

   delete from im_transform_shipsku_gtt;

   insert into im_transform_shipsku_gtt (shipment,
                                         item,
                                         seq_no,
                                         ss_qty_received,
                                         ss_qty_matched,
                                         ss_unit_cost,
                                         weight_received,
                                         weight_received_uom,
                                         carton,
                                         catch_weight_type,
                                         order_no,
                                         supplier,
                                         sup_qty_level,
                                         transform_qty_received,
                                         transform_qty_matched,
                                         transform_unit_cost,
                                         vpn)
   select sh.shipment,
          ss.item,
          ss.seq_no,
          ss.qty_received,
          ss.qty_matched,
          ss.unit_cost,
          ss.weight_received,
          ss.weight_received_uom,
          ss.carton,
          im.catch_weight_type,
          oh.order_no,
          oh.supplier,
          s.sup_qty_level,
          ss.qty_received,
          ss.qty_matched,
          ss.unit_cost,
          its.vpn
     from table(cast(I_shipments as OBJ_NUMERIC_ID_TABLE)) in_shipments,
          shipment sh,
          shipsku ss,
          ordhead oh,
          sups s,
          item_master im,
          item_supplier its
    where value(in_shipments) = sh.shipment
      and sh.shipment         = ss.shipment
      and sh.order_no         = oh.order_no
      and oh.supplier         = s.supplier
      and ss.item             = im.item
      and its.item            = ss.item
      and its.supplier        = oh.supplier;

   LOGGER.LOG_INFORMATION(L_program||' - Populate im_transform_shipsku_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_XFORM_SHIPSKU_SQL.TRANSFORM_SHIPSKU_GTT(O_error_message) = 0 then
      return REIM_CONSTANTS.FAIL;
   end if;

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END POP_TRANSFORM_SHIPSKU_GTT;
------------------------------------------------------------------------------------------
FUNCTION POP_TRANSFORM_SHIPSKU_GTT(O_error_message         IN OUT VARCHAR2,
                                   I_im_ship_item_qtys_tbl IN     IM_SHIP_ITEM_QTYS_TBL)
RETURN NUMBER IS

   L_program             VARCHAR2(61)   := 'REIM_XFORM_SHIPSKU_SQL.POP_TRANSFORM_SHIPSKU_GTT';

BEGIN

   LOGGER.LOG_INFORMATION('Start ' || L_program);

   delete from im_transform_shipsku_gtt;

   insert into im_transform_shipsku_gtt (shipment,
                                         item,
                                         seq_no,
                                         ss_qty_received,
                                         ss_qty_matched,
                                         ss_unit_cost,
                                         weight_received,
                                         weight_received_uom,
                                         carton,
                                         catch_weight_type,
                                         order_no,
                                         supplier,
                                         sup_qty_level,
                                         transform_qty_received,
                                         transform_qty_matched,
                                         transform_unit_cost,
                                         vpn)
   select sh.shipment,
          ss.item,
          ss.seq_no,
          ss.qty_received,
          ss.qty_matched,
          ss.unit_cost,
          ss.weight_received,
          ss.weight_received_uom,
          ss.carton,
          im.catch_weight_type,
          oh.order_no,
          oh.supplier,
          s.sup_qty_level,
          ss.qty_received,
          ss.qty_matched,
          ss.unit_cost,
          its.vpn
     from table(cast(I_im_ship_item_qtys_tbl as IM_SHIP_ITEM_QTYS_TBL)) in_ship_items,
          shipment sh,
          shipsku ss,
          ordhead oh,
          sups s,
          item_master im,
          item_supplier its
    where in_ship_items.shipment = sh.shipment
      and in_ship_items.item     = ss.item
      and sh.shipment            = ss.shipment
      and sh.order_no            = oh.order_no
      and oh.supplier            = s.supplier
      and ss.item                = im.item
      and its.item               = ss.item
      and its.supplier           = oh.supplier;

   LOGGER.LOG_INFORMATION(L_program||' - Populate im_transform_shipsku_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if REIM_XFORM_SHIPSKU_SQL.TRANSFORM_SHIPSKU_GTT(O_error_message) = 0 then
      return REIM_CONSTANTS.FAIL;
   end if;

   return REIM_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return REIM_CONSTANTS.FAIL;
END POP_TRANSFORM_SHIPSKU_GTT;
------------------------------------------------------------------------------------------
END REIM_XFORM_SHIPSKU_SQL;
/
