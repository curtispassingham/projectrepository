CREATE OR REPLACE PACKAGE REIM_INJECTOR_SQL AS

    --INJECTOR PROCESSES
    PROCESS_VALIDATION CONSTANT NUMBER(1) := 0;
    PROCESS_INJECTION CONSTANT NUMBER(1) := 1;


    --INJECTOR VALIDATION RULES

    DUP_VEND_DOC_NUM CONSTANT VARCHAR2(40) := 'DUP_VEND_DOC_NUM';
    VALID_DUE_DATE CONSTANT VARCHAR2(40) := 'VALID_DUE_DATE';
    VALID_CURR_CODE CONSTANT VARCHAR2(40) := 'VALID_CURR_CODE';
    VALID_CROSS_REF_DOC CONSTANT VARCHAR2(40) := 'VALID_CROSS_REF_DOC';
    VALID_ORD_CURR CONSTANT VARCHAR2(40) := 'VALID_ORD_CURR';
    VALID_FR8_TYPE CONSTANT VARCHAR2(40) := 'VALID_FR8_TYPE';
    SUPP_OPTION_UNDEFINED CONSTANT VARCHAR2(40) := 'SUPP_OPTION_UNDEFINED';
    VALID_NMRCH CONSTANT VARCHAR2(40) := 'VALID_NMRCH';
    VALID_ORDER CONSTANT VARCHAR2(40) := 'VALID_ORDER';
    VALID_ORD_VEND CONSTANT VARCHAR2(40) := 'VALID_ORD_VEND';
    VALID_RTV_VEND CONSTANT VARCHAR2(40) := 'VALID_RTV_VEND';
    VALID_TERMS CONSTANT VARCHAR2(40) := 'VALID_TERMS';
    VALID_DOC_DATE CONSTANT VARCHAR2(40) := 'VALID_DOC_DATE';
    VALID_STORE CONSTANT VARCHAR2(40) := 'VALID_STORE';
    VALID_WH CONSTANT VARCHAR2(40) := 'VALID_WH';
    VALID_SUPPLIER CONSTANT VARCHAR2(40) := 'VALID_SUPPLIER';
    VALID_SUPPLIER_SITE CONSTANT VARCHAR2(40) := 'VALID_SUPPLIER_SITE';
    VALID_PARTNER CONSTANT VARCHAR2(40) := 'VALID_PARTNER';
    DOCTYPE_COST_INC_TAX_SIGN CONSTANT VARCHAR2(40) := 'DOCTYPE_COST_INC_TAX_SIGN';

    VALID_ORD_LOC CONSTANT VARCHAR2(40) := 'VALID_ORD_LOC';
	VALID_COMP_DEAL_DETAIL VARCHAR2(40) := 'VALID_COMP_DEAL_DETAIL';

    TOTAL_HDR_QTY_REQ CONSTANT VARCHAR2(40) := 'TOTAL_HDR_QTY_REQ';
    HDR_DTL_QTY_MTCH CONSTANT VARCHAR2(40) := 'HDR_DTL_QTY_MTCH';

    TAX_NOT_ALLOWED CONSTANT VARCHAR2(40) := 'TAX_NOT_ALLOWED';
    TAX_COST_SAME_SIGN CONSTANT VARCHAR2(40) := 'TAX_COST_SAME_SIGN';
    TAX_REQUIRED CONSTANT VARCHAR2(40) := 'TAX_REQUIRED';
    ZERO_TAX_REQUIRED CONSTANT VARCHAR2(40) := 'ZERO_TAX_REQUIRED';
    ZERO_MRCHI_DTL_REQ CONSTANT VARCHAR2(40) := 'ZERO_MRCHI_DTL_REQ';
    TAX_AMT_NON_ZERO CONSTANT VARCHAR2(40) := 'TAX_AMT_NON_ZERO';

    CRDNT_NO_DETAIL CONSTANT VARCHAR2(40) := 'CRDNT_NO_DETAIL';

    ALLW_TOTAL CONSTANT VARCHAR2(40) := 'ALLW_TOTAL';

    RTV_REASON_CODE CONSTANT VARCHAR2(40) := 'RTV_REASON_CODE';
    CMC_REASON_CODE CONSTANT VARCHAR2(40) := 'CMC_REASON_CODE';

    DUP_ITEM CONSTANT VARCHAR2(40) := 'DUP_ITEM';
    DUP_VPN CONSTANT VARCHAR2(40) := 'DUP_VPN';
    DUP_UPC CONSTANT VARCHAR2(40) := 'DUP_UPC';

    VALID_ITEM CONSTANT VARCHAR2(40) := 'VALID_ITEM';
    VALID_ITEM_SUPPLIER CONSTANT VARCHAR2(40) := 'VALID_ITEM_SUPPLIER';
    VALID_ORDER_ITEM CONSTANT VARCHAR2(40) := 'VALID_ORDER_ITEM';
    VALID_RTV_ORDER_ITEM CONSTANT VARCHAR2(40) := 'VALID_RTV_ORDER_ITEM';
    
    VALID_VPN_SUPPLIER CONSTANT VARCHAR2(40) := 'VALID_VPN_SUPPLIER';
    DUP_VPN_SUPPLIER CONSTANT VARCHAR2(40) := 'DUP_VPN_SUPPLIER';
    VALID_UPC CONSTANT VARCHAR2(40) := 'VALID_UPC';
    VALID_UPC_SUPPLIER CONSTANT VARCHAR2(40) := 'VALID_UPC_SUPPLIER';
    UNABLE_TO_RETRIEVE_ITEM CONSTANT VARCHAR2(40) := 'UNABLE_TO_RETRIEVE_ITEM';

    VALID_NMRCH_CODE CONSTANT VARCHAR2(40) := 'VALID_NMRCH_CODE';
    VALID_SRVC_PERF_STORE CONSTANT VARCHAR2(40) := 'VALID_SRVC_PERF_STORE';

    VALID_RTV CONSTANT VARCHAR2(40) := 'VALID_RTV';
    VALID_RTV_LOC CONSTANT VARCHAR2(40) := 'VALID_RTV_LOC';

    DTL_TAX_NOT_IN_HDR CONSTANT VARCHAR2(40) := 'DTL_TAX_NOT_IN_HDR';
    DTL_NM_TAX_NOT_IN_HDR CONSTANT VARCHAR2(40) := 'DTL_NM_TAX_NOT_IN_HDR';

    VALID_HDR_TAX CONSTANT VARCHAR2(40) := 'VALID_HDR_TAX';
    VALID_DTL_TAX CONSTANT VARCHAR2(40) := 'VALID_DTL_TAX';
    VALID_DTL_NM_TAX CONSTANT VARCHAR2(40) := 'VALID_DTL_NM_TAX';

    VALID_ITEM_TAX CONSTANT VARCHAR2(40) := 'VALID_ITEM_TAX';

    REV_VAT_ITEM_ZERO_TAX CONSTANT VARCHAR2(40) := 'REV_VAT_ITEM_ZERO_TAX';

    --LOCATION ID THAT RMS WILL SEND FOR NO LOCATION ON NON-MERCH INVOICES
    NO_LOCATION CONSTANT  NUMBER(10) := -999999999;


    NON_MRCH_COST CONSTANT VARCHAR2(40) := 'NON_MRCH_COST';
    DETAIL_COST CONSTANT VARCHAR2(40) := 'DETAIL_COST';
    DETAIL_NM_COST CONSTANT VARCHAR2(40) := 'DETAIL_NM_COST';

    TOTAL_BASIS CONSTANT VARCHAR2(40) := 'TOTAL_BASIS';
    NON_MRCH_BASIS CONSTANT VARCHAR2(40) := 'NON_MRCH_BASIS';
    DETAIL_BASIS CONSTANT VARCHAR2(40) := 'DETAIL_BASIS';
    DETAIL_NM_BASIS CONSTANT VARCHAR2(40) := 'DETAIL_BASIS';

    TOTAL_TAX CONSTANT VARCHAR2(40) := 'TOTAL_TAX';
    NON_MRCH_TAX CONSTANT VARCHAR2(40) := 'NON_MRCH_TAX';
    DETAIL_TAX CONSTANT VARCHAR2(40) := 'DETAIL_TAX';
    DETAIL_NM_TAX CONSTANT VARCHAR2(40) := 'DETAIL_TAX';
    ALLW_TAX CONSTANT VARCHAR2(40) := 'ALLW_TAX';
    ACQ_TAX_DETAIL_REQ CONSTANT VARCHAR2(40) := 'ACQ_TAX_DETAIL_REQ';
    EXEMPT_DOCS_WITH_TAX CONSTANT VARCHAR2(40) := 'EXEMPT_DOCS_WITH_TAX';

    FUNCTION INIT_THREADS(O_error_message    OUT VARCHAR2,
                          O_num_chunks       OUT NUMBER,
                          I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE)
    RETURN NUMBER;

    FUNCTION VALIDATE_TRANSACTIONS(O_error_message    OUT VARCHAR2,
                                   I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE,
                                   I_thread_id     IN     IM_INJECT_DOC_HEAD.THREAD_ID%TYPE,
                                   I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                   I_username      IN     VARCHAR2)
    RETURN NUMBER;

    FUNCTION INJECT_VALID_TRANSACTIONS(O_error_message    OUT VARCHAR2,
                                       I_inject_id     IN     IM_INJECT_STATUS.INJECT_ID%TYPE,
                                       I_inject_doc_id IN     IM_INJECT_DOC_HEAD.INJECT_DOC_ID%TYPE,
                                       I_username      IN     VARCHAR2)
    RETURN NUMBER;

    FUNCTION GET_EXT_DOC_ID_WITH_PRFX(I_ext_doc_id IN     IM_INJECT_DOC_HEAD.EXT_DOC_ID%TYPE,
                                      I_doc_type   IN     IM_INJECT_DOC_HEAD.DOC_TYPE%TYPE)
    RETURN VARCHAR2;
    
    FUNCTION IS_CN_REVERSE_VAT_ELIGIBLE(I_vendor_id      IN     IM_INJECT_DOC_HEAD.VENDOR%TYPE,
                                        I_doc_date       IN     IM_INJECT_DOC_HEAD.DOC_DATE%TYPE,
                                        I_reference_cnr  IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF3%TYPE,
                                        I_reference_invc IN     IM_INJECT_DOC_HEAD.CUSTOM_DOC_REF4%TYPE)
    RETURN VARCHAR2;

END REIM_INJECTOR_SQL;
/
