CREATE OR REPLACE PACKAGE REIM_SEARCH_SQL AS
--------------------------------------------------------------------
DISCREPANT_COST_IND           CONSTANT VARCHAR2(2) := 'C';
DISCREPANT_QTY_IND            CONSTANT VARCHAR2(2) := 'Q';
DISCREPANT_BOTH_IND           CONSTANT VARCHAR2(2) := 'B';
DISCREPANT_SEARCH_CRITERIA    CONSTANT VARCHAR2(2) := 'SC';
--------------------------------------------------------------------
--populates: IM_DOC_INVC_SEARCH_WS
FUNCTION DOC_SEARCH(O_error_message     IN OUT VARCHAR2,
                    O_workspace_id         OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                    O_max_rows_exceeded    OUT NUMBER,
                    I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER;
--------------------------------------------------------------------------
--populates: IM_MATCH_INVC_SEARCH_WS 
--           IM_MATCH_RCPT_SEARCH_WS
FUNCTION MANUAL_SEARCH(O_error_message     IN OUT VARCHAR2,
                       O_workspace_id         OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                       O_max_rows_exceeded    OUT NUMBER,
                       I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER;
--------------------------------------------------------------------------
--populates: IM_MATCH_INVC_SEARCH_WS
--           IM_MATCH_RCPT_SEARCH_WS
FUNCTION CONTEXT_SEARCH(O_error_message     IN OUT VARCHAR2,
                        O_workspace_id         OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                        O_max_rows_exceeded    OUT NUMBER,
                        I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER;
--------------------------------------------------------------------------
--populates: IM_CNR_SEARCH_WS
--           IM_CN_SEARCH_WS
FUNCTION CREDIT_NOTE_SEARCH(O_error_message        IN OUT VARCHAR2,
                            O_workspace_id            OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                            O_max_rows_exceeded       OUT NUMBER,
                            I_cnr_search_criteria  IN OUT IM_DOC_SEARCH_CRITERIA_REC,
                            I_cn_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER;
--------------------------------------------------------------------------
--populates: IM_MATCH_INVC_SEARCH_WS
FUNCTION MATCH_INQ_SEARCH(O_error_message     IN OUT VARCHAR2,
                          O_workspace_id         OUT IM_MATCH_INQ_SEARCH_WS.WORKSPACE_ID%TYPE,
                          O_max_rows_exceeded    OUT NUMBER,
                          I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER;
--------------------------------------------------------------------------
--populates: 
--           IM_DOC_INVC_SEARCH_WS   (only used internally)
--           IM_MATCH_RCPT_SEARCH_WS (only used internally)
--
--           IM_MATCH_INVC_WS
--           IM_MATCH_INVC_DETL_WS
--           IM_MATCH_RCPT_WS
--           IM_MATCH_RCPT_DETL_WS
--
--           IM_DETAIL_MATCH_WS
--
--           IM_DISCREPANCY_LIST_WS
--
--I_search_criteria can be:
--   DISCREPANT_COST_IND    CONSTANT VARCHAR2(1) := 'C';
--   DISCREPANT_QTY_IND     CONSTANT VARCHAR2(1) := 'Q';
--   DISCREPANT_BOTH_IND    CONSTANT VARCHAR2(1) := 'B';
--
FUNCTION DISCREPANCY_SEARCH(O_error_message     IN OUT VARCHAR2,
                            O_workspace_id         OUT IM_DOC_INVC_SEARCH_WS.WORKSPACE_ID%TYPE,
                            O_max_rows_exceeded    OUT NUMBER,
                            I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC,
                            I_discrepancy_privs IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
--populates: IM_TAX_REVIEW_LIST_WS
FUNCTION TAX_REVIEW_LIST_SEARCH(O_error_message     IN OUT VARCHAR2,
                                O_workspace_id         OUT IM_MATCH_INQ_SEARCH_WS.WORKSPACE_ID%TYPE,
                                O_max_rows_exceeded    OUT NUMBER,
                                I_search_criteria   IN OUT IM_DOC_SEARCH_CRITERIA_REC)
RETURN NUMBER;
--------------------------------------------------------------------------
END REIM_SEARCH_SQL;
/
