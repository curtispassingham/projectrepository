CREATE OR REPLACE PACKAGE BODY REIM_TAX_RESOLUTION AS
--------------------------------------------------------------------
FUNCTION RESOLVE_INVC(O_error_message    IN OUT VARCHAR2,
                      I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE,
                      I_resolution_side  IN     VARCHAR2,
                      I_reason_code_id   IN     IM_RESOLUTION_ACTION.REASON_CODE_ID%TYPE,
                      I_comment          IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER;
---
FUNCTION RESOLVE_SYS_FULL(O_error_message    IN OUT VARCHAR2,
                          I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE,
                          I_resolution_side  IN     VARCHAR2,
                          I_reason_code_id   IN     IM_RESOLUTION_ACTION.REASON_CODE_ID%TYPE,
                          I_comment          IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER;
---
FUNCTION RESOLVE_SYS_ITEM(O_error_message    IN OUT VARCHAR2,
                          I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE,
                          I_resolution_side  IN     VARCHAR2,
                          I_reason_code_id   IN     IM_RESOLUTION_ACTION.REASON_CODE_ID%TYPE,
                          I_comment          IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER;
---
FUNCTION DOC_HEAD_UPDATE(O_error_message    IN OUT VARCHAR2,
                         I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------
FUNCTION RESOLVE(O_error_message    IN OUT VARCHAR2,
                 I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE,
                 I_resolution_side  IN     VARCHAR2,
                 I_reason_code_id   IN     IM_RESOLUTION_ACTION.REASON_CODE_ID%TYPE,
                 I_comment          IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER IS

   L_program                    VARCHAR2(61) := 'REIM_TAX_RESOLUTION.RESOLVE';
   L_start_time                 TIMESTAMP    := SYSTIMESTAMP;
   L_tax_document_creation_lvl  im_system_options.tax_document_creation_lvl%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||
      ' Start workspace:'||I_workspace_id||' resolution side:'||I_resolution_side||' reason_code_id:'||I_reason_code_id||' comment:'||I_comment);

   select tax_document_creation_lvl
     into L_tax_document_creation_lvl
     from im_system_options;

   if I_resolution_side = REIM_CONSTANTS.TAX_RES_INVOICE then

      if RESOLVE_INVC(O_error_message,
                      I_workspace_id,
                      I_resolution_side,
                      I_reason_code_id,
                      I_comment) = 0 then
         return 0;
      end if;

   end if;

   if I_resolution_side = REIM_CONSTANTS.TAX_RES_SYSTEM and
      L_tax_document_creation_lvl = REIM_CONSTANTS.TAX_DOC_CRE_LVL_FULL_INVC then

      if RESOLVE_SYS_FULL(O_error_message,
                          I_workspace_id,
                          I_resolution_side,
                          I_reason_code_id,
                          I_comment) = 0 then
         return 0;
      end if;

   end if;

   if I_resolution_side = REIM_CONSTANTS.TAX_RES_SYSTEM and
      L_tax_document_creation_lvl = REIM_CONSTANTS.TAX_DOC_CRE_LVL_ITEM then

      if RESOLVE_SYS_ITEM(O_error_message,
                          I_workspace_id,
                          I_resolution_side,
                          I_reason_code_id,
                          I_comment) = 0 then
         return 0;
      end if;

   end if;

   delete from im_tax_review_list_search_ws w
    where w.workspace_id = I_workspace_id
      and w.choice_flag  = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' delete im_tax_review_list_search_ws - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END RESOLVE;
--------------------------------------------------------------------
FUNCTION RESOLVE_INVC(O_error_message    IN OUT VARCHAR2,
                      I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE,
                      I_resolution_side  IN     VARCHAR2,
                      I_reason_code_id   IN     IM_RESOLUTION_ACTION.REASON_CODE_ID%TYPE,
                      I_comment          IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'REIM_TAX_RESOLUTION.RESOLVE_INVC';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_user             user_attrib.user_id%TYPE := get_user;

BEGIN

   insert into im_doc_detail_comments (comment_id,
                                       comment_type,
                                       text,
                                       created_by,
                                       creation_date,
                                       doc_id,
                                       item,
                                       discrepancy_type,
                                       reason_code_id,
                                       debit_reason_code,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
   select im_doc_detail_comments_seq.nextval,
          'I', --internal
          I_comment,
          L_user,
          sysdate,
          w.doc_id,
          w.item,
          'TAX' discrepancy_type,
          I_reason_code_id,
          NULL debit_reason_code,
          L_user,
          sysdate,
          0
     from im_tax_review_list_search_ws w
    where w.workspace_id = I_workspace_id
      and w.choice_flag  = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' insert im_doc_detail_comments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --move to history
   insert into im_tax_discrepancy_history (doc_id,
                                           order_no,
                                           item,
                                           tax_code,
                                           doc_tax_rate,
                                           doc_tax_amount,
                                           verify_tax_rate,
                                           verify_tax_code,
                                           verify_tax_amount,
                                           created_by,
                                           creation_date,
                                           last_updated_by,
                                           last_update_date,
                                           object_version_id)
   select w.doc_id,
          w.order_no,
          w.item,
          w.doc_tax_code,
          w.doc_tax_rate,
          w.doc_tax_amount,
          w.verify_tax_rate,
          w.verify_tax_code,
          w.verify_tax_amount,
          L_user,
          sysdate,
          L_user,
          sysdate,
          0
     from im_tax_review_list_search_ws w
    where w.workspace_id = I_workspace_id
      and w.choice_flag  = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' insert im_tax_discrepancy_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_tax_discrepancy td
    where exists (select 'x'
                    from im_tax_review_list_search_ws w
                   where w.workspace_id = I_workspace_id
                     and w.choice_flag  = 'Y'
                     and w.doc_id       = td.doc_id
                     and w.item         = td.item
                     and w.doc_tax_code = td.tax_code);

   LOGGER.LOG_INFORMATION(L_program||' delete im_tax_discrepancy - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_item_tax_audit(doc_id,
                                 item,
                                 order_no,
                                 tax_code,
                                 sys_tax_rate,
                                 sys_tax_amount,
                                 sys_tax_formula,
                                 sys_tax_order,
                                 doc_tax_rate,
                                 doc_tax_amount,
                                 doc_tax_formula,
                                 doc_tax_order,
                                 created_by,
                                 creation_date,
                                 last_updated_by,
                                 last_update_date,
                                 object_version_id)
   select w.doc_id,
          w.item,
          w.order_no,
          w.doc_tax_code,
          w.verify_tax_rate,
          w.verify_tax_amount,
          null verify_tax_formula,
          null verify_tax_order,
          w.doc_tax_rate,
          w.doc_tax_amount,
          null doc_tax_formula,
          null doc_tax_order,
          L_user,
          sysdate,
          L_user,
          sysdate,
          0
     from im_tax_review_list_search_ws w
    where w.workspace_id = I_workspace_id
      and w.choice_flag  = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' insert im_item_tax_audit - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_resolution_action (doc_id,
                                     item,
                                     reason_code_id,
                                     action,
                                     qty,
                                     unit_cost,
                                     extended_cost,
                                     status,
                                     shipment,
                                     created_by,
                                     creation_date,
                                     last_updated_by,
                                     last_update_date,
                                     object_version_id)
   select w.doc_id,
          w.item,
          rc.reason_code_id,
          rc.action,
          id.qty,
          id.unit_cost,
          id.qty * id.unit_cost,
          REIM_CONSTANTS.RCA_STATUS_ROLLEDUP,
          null,
          L_user,
          sysdate,
          L_user,
          sysdate,
          0
     from im_tax_review_list_search_ws w,
          im_reason_codes rc,
          im_invoice_detail id
    where w.workspace_id   = I_workspace_id
      and w.choice_flag    = 'Y'
      and I_reason_code_id = rc.reason_code_id
      and w.doc_id         = id.doc_id
      and w.item           = id.item;

   LOGGER.LOG_INFORMATION(L_program||' insert im_resolution_action - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --update doc detail status
   merge into im_invoice_detail target
   using (select w.doc_id,
                 w.item
            from im_tax_review_list_search_ws w
           where w.workspace_id = I_workspace_id
             and w.choice_flag  = 'Y'
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status              = REIM_CONSTANTS.DOC_ITEM_STATUS_UNMTCH,
        target.tax_discrepancy_ind = 'N',
        target.last_updated_by     = L_user,
        target.last_update_date    = sysdate,
        target.object_version_id   = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' merge im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into im_doc_head target
   using (select id.doc_id,
                 SUM(DECODE(id.tax_discrepancy_ind, 'N', 0, 1)) tax_disc_cnt
            from im_tax_review_list_search_ws w,
                 im_invoice_detail id
           where w.workspace_id = I_workspace_id
             and w.choice_flag  = 'Y'
             --
             and w.doc_id       = id.doc_id
           GROUP BY id.doc_id
   ) use_this
   on (    target.doc_id         = use_this.doc_id
       and use_this.tax_disc_cnt = 0)
   when matched then update
    set target.status                    = REIM_CONSTANTS.DOC_STATUS_RMTCH,
        target.last_updated_by           = L_user,
        target.last_update_date          = sysdate,
        target.object_version_id         = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END RESOLVE_INVC;
--------------------------------------------------------------------
FUNCTION RESOLVE_SYS_FULL(O_error_message    IN OUT VARCHAR2,
                          I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE,
                          I_resolution_side  IN     VARCHAR2,
                          I_reason_code_id   IN     IM_RESOLUTION_ACTION.REASON_CODE_ID%TYPE,
                          I_comment          IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'REIM_TAX_RESOLUTION.RESOLVE_SYS_FULL';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_user             user_attrib.user_id%TYPE := get_user;

BEGIN

   insert into im_doc_detail_comments (comment_id,
                                       comment_type,
                                       text,
                                       created_by,
                                       creation_date,
                                       doc_id,
                                       item,
                                       discrepancy_type,
                                       reason_code_id,
                                       debit_reason_code,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
   select im_doc_detail_comments_seq.nextval,
          'I', --internal
          I_comment,
          L_user,
          sysdate,
          id.doc_id,
          id.item,
          'TAX' discrepancy_type,
          I_reason_code_id,
          null debit_reason_code,
          L_user,
          sysdate,
          0
     from (select distinct doc_id
             from im_tax_review_list_search_ws w
            where w.workspace_id = I_workspace_id
              and w.choice_flag  = 'Y') w,
          im_invoice_detail id,
          im_tax_discrepancy td
    where I_comment is NOT NULL
      and w.doc_id  =  id.doc_id
      and id.doc_id =  td.doc_id
      and id.status <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH;

   LOGGER.LOG_INFORMATION(L_program||' insert im_doc_detail_comments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --move to hitsory
   insert into im_tax_discrepancy_history (doc_id,
                                           order_no,
                                           item,
                                           tax_code,
                                           doc_tax_rate,
                                           doc_tax_amount,
                                           verify_tax_rate,
                                           verify_tax_code,
                                           verify_tax_amount,
                                           created_by,
                                           creation_date,
                                           last_updated_by,
                                           last_update_date,
                                           object_version_id)
   select td.doc_id,
          dh.order_no,
          td.item,
          td.tax_code,
          td.doc_tax_rate,
          td.doc_tax_amount,
          td.verify_tax_rate,
          td.verify_tax_code,
          td.verify_tax_amount,
          L_user,
          sysdate,
          L_user,
          sysdate,
          0
     from (select distinct doc_id
             from im_tax_review_list_search_ws w
            where w.workspace_id = I_workspace_id
              and w.choice_flag  = 'Y') w,
          im_invoice_detail id,
          im_tax_discrepancy td,
          im_doc_head dh
    where w.doc_id               = id.doc_id
      and id.tax_discrepancy_ind = 'Y'
      and id.doc_id              = td.doc_id
      and id.item                = td.item
      and td.doc_id              = dh.doc_id;

   LOGGER.LOG_INFORMATION(L_program||' insert im_tax_discrepancy_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_tax_discrepancy td
    where td.doc_id in (select doc_id
                         from im_tax_review_list_search_ws w
                        where w.workspace_id = I_workspace_id
                          and w.choice_flag  = 'Y');

   LOGGER.LOG_INFORMATION(L_program||' delete im_tax_discrepancy - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_resolution_action (doc_id,
                                     item,
                                     reason_code_id,
                                     action,
                                     qty,
                                     unit_cost,
                                     extended_cost,
                                     status,
                                     shipment,
                                     created_by,
                                     creation_date,
                                     last_updated_by,
                                     last_update_date,
                                     object_version_id)
   select id.doc_id,
          id.item,
          rc.reason_code_id,
          rc.action,
          id.qty,
          id.unit_cost,
          id.qty * id.unit_cost,
          REIM_CONSTANTS.RCA_STATUS_UNROLLED,
          null,
          L_user,
          sysdate,
          L_user,
          sysdate,
          0
     from im_tax_review_list_search_ws w,
          im_reason_codes rc,
          im_invoice_detail id
    where w.workspace_id         =  I_workspace_id
      and w.choice_flag          =  'Y'
      and I_reason_code_id       =  rc.reason_code_id
      and w.doc_id               =  id.doc_id
      and id.status              <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH;

   LOGGER.LOG_INFORMATION(L_program||' insert im_resolution_action - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --update doc detail status
   merge into im_invoice_detail target
   using (select distinct id.doc_id,
                          id.item
            from im_tax_review_list_search_ws w,
                 im_invoice_detail id
           where w.workspace_id     =  I_workspace_id
             and w.choice_flag      =  'Y'
             and w.doc_id           =  id.doc_id
             and id.status          <> REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status                        = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched                  = 'Y',
        target.qty_matched                   = 'Y',
        target.tax_discrepancy_ind           = 'N',
        target.resolution_adjusted_unit_cost = target.unit_cost,
        target.resolution_adjusted_qty       = target.qty,
        target.last_updated_by               = L_user,
        target.last_update_date              = sysdate,
        target.object_version_id             = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' insert im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if DOC_HEAD_UPDATE(O_error_message,
                      I_workspace_id) = 0 then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END RESOLVE_SYS_FULL;
--------------------------------------------------------------------
FUNCTION RESOLVE_SYS_ITEM(O_error_message    IN OUT VARCHAR2,
                          I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE,
                          I_resolution_side  IN     VARCHAR2,
                          I_reason_code_id   IN     IM_RESOLUTION_ACTION.REASON_CODE_ID%TYPE,
                          I_comment          IN     IM_DOC_DETAIL_COMMENTS.TEXT%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'REIM_TAX_RESOLUTION.RESOLVE_SYS_ITEM';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_user             user_attrib.user_id%TYPE := get_user;

BEGIN

   insert into im_doc_detail_comments (comment_id,
                                       comment_type,
                                       text,
                                       created_by,
                                       creation_date,
                                       doc_id,
                                       item,
                                       discrepancy_type,
                                       reason_code_id,
                                       debit_reason_code,
                                       last_updated_by,
                                       last_update_date,
                                       object_version_id)
   select im_doc_detail_comments_seq.nextval,
          'I', --internal
          I_comment,
          L_user,
          sysdate,
          w.doc_id,
          w.item,
          'TAX' discrepancy_type,
          I_reason_code_id,
          null debit_reason_code,
          L_user,
          sysdate,
          0
     from im_tax_review_list_search_ws w
    where w.workspace_id = I_workspace_id
      and w.choice_flag  = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' insert im_doc_detail_comments - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --move to hitsory
   insert into im_tax_discrepancy_history (doc_id,
                                           order_no,
                                           item,
                                           tax_code,
                                           doc_tax_rate,
                                           doc_tax_amount,
                                           verify_tax_rate,
                                           verify_tax_code,
                                           verify_tax_amount,
                                           created_by,
                                           creation_date,
                                           last_updated_by,
                                           last_update_date,
                                           object_version_id)
   select w.doc_id,
          w.order_no,
          w.item,
          w.doc_tax_code,
          w.doc_tax_rate,
          w.doc_tax_amount,
          w.verify_tax_rate,
          w.verify_tax_code,
          w.verify_tax_amount,
          L_user,
          sysdate,
          L_user,
          sysdate,
          0
     from im_tax_review_list_search_ws w
    where w.workspace_id = I_workspace_id
      and w.choice_flag  = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' insert im_tax_discrepancy_history - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from im_tax_discrepancy td
    where exists (select 'x'
                    from im_tax_review_list_search_ws w
                   where w.workspace_id = I_workspace_id
                     and w.choice_flag  = 'Y'
                     and w.doc_id       = td.doc_id
                     and w.item         = td.item
                     and w.doc_tax_code = td.tax_code);

   LOGGER.LOG_INFORMATION(L_program||' delete im_tax_discrepancy - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --update doc detail status
   merge into im_invoice_detail target
   using (select w.doc_id,
                 w.item
            from im_tax_review_list_search_ws w
           where w.workspace_id = I_workspace_id
             and w.choice_flag  = 'Y'
   ) use_this
   on (    target.doc_id = use_this.doc_id
       and target.item   = use_this.item)
   when matched then update
    set target.status                        = REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH,
        target.cost_matched                  = 'Y',
        target.qty_matched                   = 'Y',
        target.tax_discrepancy_ind           = 'N',
        target.resolution_adjusted_unit_cost = target.unit_cost,
        target.resolution_adjusted_qty       = target.qty,
        target.last_updated_by               = L_user,
        target.last_update_date              = sysdate,
        target.object_version_id             = target.object_version_id + 1;

   LOGGER.LOG_INFORMATION(L_program||' merge im_invoice_detail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into im_resolution_action (doc_id,
                                     item,
                                     reason_code_id,
                                     action,
                                     qty,
                                     unit_cost,
                                     extended_cost,
                                     status,
                                     shipment,
                                     created_by,
                                     creation_date,
                                     last_updated_by,
                                     last_update_date,
                                     object_version_id)
   select w.doc_id,
          w.item,
          rc.reason_code_id,
          rc.action,
          id.qty,
          id.unit_cost,
          id.qty * id.unit_cost,
          REIM_CONSTANTS.RCA_STATUS_UNROLLED,
          null,
          L_user,
          sysdate,
          L_user,
          sysdate,
          0
     from im_tax_review_list_search_ws w,
          im_reason_codes rc,
          im_invoice_detail id
    where w.workspace_id   = I_workspace_id
      and w.choice_flag    = 'Y'
      and I_reason_code_id = rc.reason_code_id
      and w.doc_id         = id.doc_id
      and w.item           = id.item;

   LOGGER.LOG_INFORMATION(L_program||' insert im_resolution_action - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if DOC_HEAD_UPDATE(O_error_message,
                      I_workspace_id) = 0 then
      return 0;
   end if;

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END RESOLVE_SYS_ITEM;
--------------------------------------------------------------------
FUNCTION DOC_HEAD_UPDATE(O_error_message    IN OUT VARCHAR2,
                         I_workspace_id     IN     IM_TAX_REVIEW_LIST_SEARCH_WS.WORKSPACE_ID%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'REIM_TAX_RESOLUTION.DOC_HEAD_UPDATE';
   L_start_time       TIMESTAMP := SYSTIMESTAMP;
   L_user             user_attrib.user_id%TYPE := get_user;

BEGIN

   merge into im_doc_head target
   using (select id.doc_id,
                 case when count(*) = SUM(DECODE(id.status, REIM_CONSTANTS.DOC_ITEM_STATUS_MTCH, 1, 0)) then
                         REIM_CONSTANTS.DOC_STATUS_MTCH
                      when SUM(DECODE(id.tax_discrepancy_ind, 'Y', 1, 0)) = 0 then
                         REIM_CONSTANTS.DOC_STATUS_RMTCH
                      else
                         'NOUPDATE'
                 end status,
                 SUM(DECODE(id.tax_discrepancy_ind, 'Y', 0, id.resolution_adjusted_unit_cost * id.resolution_adjusted_qty)) res_cost,
                 SUM(DECODE(id.tax_discrepancy_ind, 'Y', 0, id.resolution_adjusted_qty)) res_qty
            from (select w.doc_id
                    from im_tax_review_list_search_ws w
                   where w.workspace_id = I_workspace_id
                     and w.choice_flag  = 'Y') ws,
                 im_invoice_detail id
           where ws.doc_id       = id.doc_id
           group by id.doc_id
   ) use_this
   on (target.doc_id    = use_this.doc_id)
   when matched then update
    set target.status                         = decode(use_this.status, 'NOUPDATE', target.status, use_this.status),
        target.resolution_adjusted_total_cost = use_this.res_cost,
        target.resolution_adjusted_total_qty  = use_this.res_qty,
        target.last_updated_by                = L_user,
        target.last_update_date               = sysdate,
        target.object_version_id              = target.object_version_id + REIM_CONSTANTS.ONE;

   LOGGER.LOG_INFORMATION(L_program||' merge im_doc_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   REIM_PKG_UTIL_SQL.LOG_TIME(L_program ||'-'||I_workspace_id,L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END DOC_HEAD_UPDATE;
--------------------------------------------------------------------
END REIM_TAX_RESOLUTION;
/
