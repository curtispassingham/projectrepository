CREATE OR REPLACE PACKAGE REIM_HOLDING_SQL AS


  LP_PROGRAM  CONSTANT VARCHAR2(32) := 'REIM_HOLDING_SQL';
  
  LP_PACKAGE_ERROR  CONSTANT VARCHAR2(20) := 'PACKAGE_ERROR';
  
  LP_SUCCESS  CONSTANT NUMBER(10) := 1;
  
  LP_FAIL  CONSTANT NUMBER(10) := 0;
  
  
  -- -----------------------------------------------------------------------------
  --  PROCEDURE UPDATE_INVOICE_DOCUMENTS
  --  
  --  Given a table of document references, insert documents into IM_INV_DOCUMENT
  --  based on the document that the new resolution document is referencing.
  -- -----------------------------------------------------------------------------
  PROCEDURE UPDATE_INVOICE_DOCUMENTS(O_STATUS  OUT NUMBER,O_ERROR_MESSAGE  OUT VARCHAR2,I_RESOLUTION_DOC  IN OBJ_REIM_INV_DOCUMENT_TBL);
  
  
  -- -----------------------------------------------------------------------------
  --  PROCEDURE UPDATE_DOCUMENT_HOLDING
  --
  --  Hold and release invoices and credit notes based on the contents of the
  --  IM_INV_DOCUMENT table.
  -- -----------------------------------------------------------------------------
  PROCEDURE UPDATE_DOCUMENT_HOLDING(O_STATUS  OUT NUMBER,O_ERROR_MESSAGE  OUT VARCHAR2);
  
END REIM_HOLDING_SQL;
/