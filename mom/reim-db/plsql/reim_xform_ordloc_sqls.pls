CREATE OR REPLACE PACKAGE REIM_XFORM_ORDLOC_SQL AS
-------------------------------------
/**
 * The public function used for Transforming ordloc records to be used in ReIM.
 * Uses im_transform_ordloc_gtt.
 *
 * Returns 1 on Success
 *         0 on Failure
 */
FUNCTION TRANSFORM_ORDLOC_GTT(O_error_message IN OUT VARCHAR2)
RETURN NUMBER;
-------------------------------------
FUNCTION TRANSFORM_ORDLOC_GTT(O_error_message     IN OUT VARCHAR2,
                              I_order_sup_items   IN     OBJ_NUM_NUM_STR_TBL)
RETURN NUMBER;
-------------------------------------
END REIM_XFORM_ORDLOC_SQL;
/
