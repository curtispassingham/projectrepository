--------------------------------------------------------
--  File created - Thursday-March-21-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger REIM_TABLE_IRUA_AIR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "REIM_TABLE_IRUA_AIR" 
 AFTER INSERT
 ON IM_RECEIVER_UNIT_ADJUST
 FOR EACH ROW
DECLARE

   L_program             VARCHAR2(30)               :=  'REIM_TABLE_IRUA_AIR';
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE   :=  NULL;

   L_receiver_unit_adj   IM_RECEIVER_UNIT_ADJUST%ROWTYPE;
   L_qty_received        SHIPSKU.QTY_RECEIVED%TYPE;
   L_qty_matched         SHIPSKU.QTY_MATCHED%TYPE;
   L_seq_no              SHIPSKU.SEQ_NO%TYPE;
   L_carton              SHIPSKU.CARTON%TYPE;
   L_remaining_qty       SHIPSKU.QTY_MATCHED%TYPE := 0;
   L_unmatched_qty       SHIPSKU.QTY_MATCHED%TYPE;

   L_catch_weight_type   ITEM_MASTER.catch_weight_type%TYPE;
   L_adjusted_weight     SHIPSKU.WEIGHT_RECEIVED%TYPE;
   L_cuom                UOM_CLASS.UOM%TYPE :=  NULL;
   L_item                ITEM_MASTER.ITEM%TYPE;
   L_supplier_pack_size         ITEM_SUPP_COUNTRY.supp_pack_size%TYPE;
   L_supplier_qty_level         SUPS.SUP_QTY_LEVEL%TYPE;
   L_inners_recalc_factor       ITEM_SUPP_COUNTRY.supp_pack_size%TYPE;

   -- This is the adjusted qty we are going to send to RMS.
   -- As we received adjusted weight as adjusted qty from ReIM for catch weight simple packs type 2 and type 4
   -- for such items we would need to use the received adjusted qty as adjusted weight and to send NULL for adjusted qty to RMS
   L_adjusted_qty_rms    SHIPSKU.QTY_RECEIVED%TYPE;

   L_adjusted_qty_reim   SHIPSKU.QTY_RECEIVED%TYPE;

   PROGRAM_ERROR         EXCEPTION;


   CURSOR C_shipsku IS
          select  qty_received, qty_matched, seq_no, carton
           from  v_im_shipsku
          where  shipment = L_receiver_unit_adj.shipment
          and  item     = L_receiver_unit_adj.item
		  and invc_match_status in ('U','P');

   cursor C_CATCH_WEIGHT_TYPE is
     select catch_weight_type
       from item_master
      where item = L_item;

   cursor C_INNERS_RECALC is
	  SELECT ORDSKU.SUPP_PACK_SIZE, SUPS.SUP_QTY_LEVEL
	  FROM ORDSKU, ORDHEAD, SUPS, SHIPMENT
	  WHERE SHIPMENT.ORDER_NO=ORDHEAD.ORDER_NO
	  AND ORDSKU.ORDER_NO=ORDHEAD.ORDER_NO
	  AND ORDHEAD.SUPPLIER=SUPS.SUPPLIER
	  AND SHIPMENT.SHIPMENT=L_RECEIVER_UNIT_ADJ.SHIPMENT
	  AND ORDSKU.ITEM=L_RECEIVER_UNIT_ADJ.ITEM;

BEGIN

--  NOTE:  The qty on the inserted im_receiver_unit_adjust row is an adjustment qty (positive or negative) not a replacement qty
--
-- This cursor loops through all shipskus for the shipment/item being inserted
-- If the adjustment qty is >0, it adjusts the first row and exists
-- If the adjustment qty is <0, it adjusts every row (if necessary) down to the matched qty (could be zero) until it runs out of qty

   L_receiver_unit_adj.shipment           := :NEW.shipment;
   L_receiver_unit_adj.item               := :NEW.item;
   L_receiver_unit_adj.seq_no             := :NEW.seq_no;
   L_receiver_unit_adj.adjusted_item_qty  := :NEW.adjusted_item_qty;

   L_adjusted_qty_reim := L_receiver_unit_adj.adjusted_item_qty;
   L_item := L_receiver_unit_adj.item;

   OPEN  C_CATCH_WEIGHT_TYPE;
   FETCH C_CATCH_WEIGHT_TYPE INTO L_catch_weight_type;
   CLOSE C_CATCH_WEIGHT_TYPE;

     	  -- Inners require recalculation similar to catch weight items
  	  -- catch weight items can be part of inners recalc as well
  	  OPEN C_INNERS_RECALC;
  	  FETCH C_INNERS_RECALC INTO L_supplier_pack_size, L_supplier_qty_level;
  	  CLOSE C_INNERS_RECALC;

	    IF L_supplier_qty_level = 'CA' then
         L_inners_recalc_factor := L_supplier_pack_size;
  	  ELSE
         L_inners_recalc_factor := 1;
  	  END IF;


   FOR C_rec in C_shipsku LOOP

      L_qty_received := nvl(C_rec.QTY_RECEIVED,0);
      L_qty_matched  := nvl(C_rec.QTY_MATCHED,0);
      L_seq_no       := C_rec.SEQ_NO;
      L_carton       := C_rec.CARTON;
      L_unmatched_qty := L_qty_received - L_qty_matched;

      if (L_receiver_unit_adj.adjusted_item_qty > 0 or
          abs(L_adjusted_qty_reim) <= (L_unmatched_qty)) then
	  -- If > 0 or there is enough in this row to adjust down, do it and stop

    	  IF L_catch_weight_type is null then
    	     L_adjusted_weight := NULL;
    	     L_adjusted_qty_rms := L_adjusted_qty_reim * L_inners_recalc_factor;
    	  ELSIF((L_catch_weight_type = '2') OR (L_catch_weight_type = '4')) then
    	     IF NOT ITEM_SUPP_COUNTRY_SQL.GET_COST_UOM(L_error_message,
                                               L_cuom,
                                               L_receiver_unit_adj.item) THEN
            raise PROGRAM_ERROR;
           END IF;
           L_adjusted_weight := L_adjusted_qty_reim ;
           L_adjusted_qty_rms := NULL;
    	  ELSE
    	  	 L_adjusted_weight := NULL;
    	     L_adjusted_qty_rms := L_adjusted_qty_reim * L_inners_recalc_factor;
    	  END IF;

         if REC_UNIT_ADJ_SQL.CHECK_RECORDS(L_error_message,
                                           L_receiver_unit_adj.shipment,
                                           L_receiver_unit_adj.item,
                                           L_seq_no,
                                           L_adjusted_qty_rms,
                                           L_carton,
                                           'REIM',
                                           L_adjusted_weight,
                                           L_cuom)= FALSE then
            raise PROGRAM_ERROR;
         end if;
         exit;
      else
         if L_qty_matched < L_qty_received then
              -- Adjustment is negative and there isn't enough qty in the row to do the whole adjustment
            if abs(L_adjusted_qty_reim) > L_unmatched_qty then
                L_remaining_qty := L_unmatched_qty + L_adjusted_qty_reim;
                L_adjusted_qty_reim := L_adjusted_qty_reim - L_remaining_qty;
            end if;

    	  IF L_catch_weight_type is null then
    	     L_adjusted_weight := NULL;
    	     L_adjusted_qty_rms := L_adjusted_qty_reim * L_inners_recalc_factor;
    	  ELSIF((L_catch_weight_type = '2') OR (L_catch_weight_type = '4')) then
    	     IF NOT ITEM_SUPP_COUNTRY_SQL.GET_COST_UOM(L_error_message,
                                               L_cuom,
                                               L_receiver_unit_adj.item) THEN
            raise PROGRAM_ERROR;
           END IF;
           L_adjusted_weight := L_adjusted_qty_reim;
           L_adjusted_qty_rms := NULL;
    	  ELSE
    	  	 L_adjusted_weight := NULL;
    	     L_adjusted_qty_rms := L_adjusted_qty_reim * L_inners_recalc_factor;
    	  END IF;

            if REC_UNIT_ADJ_SQL.CHECK_RECORDS(L_error_message,
                                              L_receiver_unit_adj.shipment,
                                              L_receiver_unit_adj.item,
                                              L_seq_no,
                                              L_adjusted_qty_rms,
                                              L_carton,
                                              'REIM',
                                              L_adjusted_weight,
                                              L_cuom)= FALSE then
               raise PROGRAM_ERROR;
            end if;
            if L_remaining_qty = 0 then
               exit;
            else
               L_adjusted_qty_reim := L_remaining_qty;
            end if;
         end if;
      end if;

   END LOOP;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/
ALTER TRIGGER "REIM_TABLE_IRUA_AIR" ENABLE;
