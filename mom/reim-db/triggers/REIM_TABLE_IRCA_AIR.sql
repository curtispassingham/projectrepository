--------------------------------------------------------
--  File created - Thursday-March-21-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger REIM_TABLE_IRCA_AIR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "REIM_TABLE_IRCA_AIR" 
 AFTER INSERT OR UPDATE
 ON IM_RECEIVER_COST_ADJUST
 FOR EACH ROW
DECLARE

   L_program                    VARCHAR2(30)             :=   'REIM_TABLE_IRC_AIR';
   L_table                      VARCHAR2(50)             :=   'COST_CHANGE_LOC_TEMP';
   
   ----- Variable defined to check if the resolution action performed is for matched receipts or not.
   I_adjust_matched_ind         VARCHAR2(1)              :=   'N';
   
   L_error_message              RTK_ERRORS.RTK_TEXT%TYPE :=   NULL;

   L_ordsku_rec                 ORDSKU%ROWTYPE;
   L_ordloc_rec                 ORDLOC%ROWTYPE;

   L_vdate           CONSTANT   PERIOD.VDATE%TYPE         :=   GET_VDATE;
   L_exists                     BOOLEAN;

   L_order_no                   ORDHEAD.ORDER_NO%TYPE;
   L_item                       ORDSKU.ITEM%TYPE;
   L_location                   ITEM_LOC.LOC%TYPE;
   L_adjusted_unit_cost         IM_RECEIVER_COST_ADJUST.ADJUSTED_UNIT_COST%TYPE;
   L_currency_code              CURRENCIES.CURRENCY_CODE%TYPE;
   L_supplier                   SUPS.SUPPLIER%TYPE;
   L_type                       IM_RECEIVER_COST_ADJUST.TYPE%TYPE;

   L_total_elc                  NUMBER;
   L_total_exp                  NUMBER;
   L_exp_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_exp          CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty                  NUMBER;
   L_dty_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_cost_change                COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;

   L_pc_new_loc                 NUMBER;
   L_pc_old_loc                 NUMBER;
   L_lc_new_loc                 NUMBER;
   L_lc_old_loc                 NUMBER;

   L_new_wac                    ITEM_LOC_SOH.AV_COST%TYPE;
   L_neg_soh_wac_adj_amt        ITEM_LOC_SOH.AV_COST%TYPE;

   L_av_cost                    ITEM_LOC_SOH.av_cost%TYPE;
   L_stock_on_hand              ITEM_LOC_SOH.stock_on_hand%TYPE;
   L_pack_comp_soh              ITEM_LOC_SOH.pack_comp_soh%TYPE;

   L_return_code                VARCHAR2(5);
   L_different_cost_exist       VARCHAR2(1) ;
   L_dummy                      VARCHAR2(1) := NULL;

   L_catch_weight_type          ITEM_MASTER.catch_weight_type%TYPE;
   L_pack_qty                   PACKITEM.PACK_QTY%TYPE;
   L_net_weight                 ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_supplier_pack_size         ITEM_SUPP_COUNTRY.supp_pack_size%TYPE;
   L_supplier_qty_level         SUPS.SUP_QTY_LEVEL%TYPE;
   L_inners_recalc_factor       ITEM_SUPP_COUNTRY.supp_pack_size%TYPE;
   

   cursor C_LOCK_COST_CHANGE_LOC is
      select 'x'
        from cost_change_loc_temp
       where cost_change       = L_cost_change
         and supplier          = L_supplier
         and location          = L_location
         and item              = L_item
         and origin_country_id = L_ordsku_rec.origin_country_id
         and loc_type          = L_ordloc_rec.loc_type;

   cursor C_LOCS_HAVE_DIFFERENT_COST is
       select 'x'
         from cost_change_loc_temp t1, cost_change_loc_temp t2
       where t1.cost_change = L_cost_change
         and t2.cost_change = L_cost_change
         and t1.unit_cost_old != t2.unit_cost_old;

   cursor C_CATCH_WEIGHT_TYPE is
     select catch_weight_type
       from item_master
      where item = L_item;

   cursor C_PACK_QTY is
     select pack_qty
     from   packitem
     where  pack_no = L_item;

   cursor C_INNERS_RECALC is
     SELECT ORDSKU.SUPP_PACK_SIZE, SUPS.SUP_QTY_LEVEL
     FROM ORDSKU, ORDHEAD, SUPS
     WHERE ORDSKU.ORDER_NO = ORDHEAD.ORDER_NO
     AND ORDHEAD.SUPPLIER = SUPS.SUPPLIER
     AND ORDSKU.ORDER_NO = L_order_no
     AND ORDSKU.ITEM = L_item;

   cursor C_NET_WEIGHT is
    SELECT ISCD.NET_WEIGHT, ISC.supp_pack_size
    FROM   ITEM_SUPP_COUNTRY_DIM ISCD, ORDSKU, item_supp_country ISC
    WHERE  ISCD.ITEM = L_item
    AND    ISCD.SUPPLIER = L_supplier
    AND    ORDSKU.ORDER_NO = L_order_no
    AND    ORDSKU.ITEM = L_item
    AND    ISCD.ORIGIN_COUNTRY = ORDSKU.ORIGIN_COUNTRY_ID
    AND    ISCD.DIM_OBJECT = 'CA'
    AND    ISC.item = L_item
    AND    ISC.SUPPLIER = L_supplier
    AND    ISC.ORIGIN_COUNTRY_ID = ORDSKU.ORIGIN_COUNTRY_ID;

   PROGRAM_ERROR         EXCEPTION;
BEGIN

   L_order_no             := :NEW.order_no;
   L_item                 := :NEW.item;
   L_location             := :NEW.location;
   L_adjusted_unit_cost   := :NEW.adjusted_unit_cost;
   L_currency_code        := :NEW.currency_code;
   L_supplier             := :NEW.supplier;
   L_type                 := :NEW.type;


   if ORDER_ATTRIB_SQL.GET_ORDSKU_ROW(L_error_message,
                                      L_exists,
                                      L_ordsku_rec,
                                      L_order_no,
                                      L_item) = FALSE then
      raise PROGRAM_ERROR;

end if;
   ---
   if ORDER_ATTRIB_SQL.GET_ORDLOC_ROW(L_error_message,
                                      L_exists,
                                      L_ordloc_rec,
                                      L_order_no,
                                      L_item,
                                      L_location) = FALSE then
      raise PROGRAM_ERROR;
   end if;


   -- Before sending cost to RMS as adjustment we need to convert to cost at pack level for catch weight simple packs type 2 and 4
   -- as for such items the adjustment willl be done at CUOM level and RMS expects pack level

      OPEN  C_CATCH_WEIGHT_TYPE;
  	  FETCH C_CATCH_WEIGHT_TYPE INTO L_catch_weight_type;
  	  CLOSE C_CATCH_WEIGHT_TYPE;

  	  -- Inners require recalculation similar to catch weight items
  	  -- catch weight items can be part of inners recalc as well
  	  OPEN C_INNERS_RECALC;
  	  FETCH C_INNERS_RECALC INTO L_supplier_pack_size, L_supplier_qty_level;
  	  CLOSE C_INNERS_RECALC;

  	  IF L_supplier_qty_level = 'CA' then
         L_inners_recalc_factor := L_supplier_pack_size;
  	  ELSE
         L_inners_recalc_factor := 1;
  	  END IF;

  	  IF L_catch_weight_type is null then
         -- Apply inners recalculation to send data to RMS at each level. ecalc isn't applicable for catch weight simple packs type 2 and 4
         L_adjusted_unit_cost   := L_adjusted_unit_cost / L_inners_recalc_factor;

  	  ELSIF(L_catch_weight_type = '2') then

           OPEN C_PACK_QTY;
           FETCH C_PACK_QTY INTO L_pack_qty;
  	       CLOSE C_PACK_QTY;


  	       if L_pack_qty is null then
  	          L_pack_qty := 1;
           end if;

         L_adjusted_unit_cost   := L_adjusted_unit_cost / L_pack_qty;

 	    ELSIF(L_catch_weight_type = '4') then

         OPEN C_NET_WEIGHT;
         FETCH C_NET_WEIGHT INTO L_net_weight, L_supplier_pack_size;
         CLOSE C_NET_WEIGHT;

 	       if L_net_weight is null and L_supplier_pack_size is null then
  	          L_net_weight := 1;
  	          L_supplier_pack_size := 1;
         end if;

 	       L_adjusted_unit_cost   := (L_adjusted_unit_cost / L_net_weight) * L_supplier_pack_size;
      END IF;

---- Modifications made in the call to RMS API for cost adjustment for line item based on the action passed from resolution screen.
   IF L_type IN('POR','PORS')
   THEN
   I_adjust_matched_ind := 'N';
   if REC_COST_ADJ_SQL.ADJUST_LINEITEM_COST(L_error_message,
                                             L_item,
                                             L_order_no,
                                             L_adjusted_unit_cost,
                                             L_currency_code,
                                             L_location,I_adjust_matched_ind) = FALSE then
      raise PROGRAM_ERROR;

end if;

ELSE
I_adjust_matched_ind := 'Y';
if REC_COST_ADJ_SQL.ADJUST_LINEITEM_COST(L_error_message,
                                             L_item,
                                             L_order_no,
                                             L_adjusted_unit_cost,
                                             L_currency_code,
                                             L_location,I_adjust_matched_ind) = FALSE then
      raise PROGRAM_ERROR;

end if;

END IF;
   ---
   if L_type IN('PORS','POMRS') then
      ---
      if COST_CHANGE_SQL.COST_CHANGE_EXISTS(L_error_message,
                                            L_exists,
                                            L_supplier,
                                            L_item,
                                            L_location,
                                            L_vdate) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---
      if L_exists = TRUE then
         insert into RCA_REJECT(order_no,
                                item_no,
                                location,
                                reject_reason)
                         values(L_order_no,
                                L_item,
                                L_location,
                                'SUPP_COST_CHANGE_EXIST');
      else
         NEXT_COSTCHG_NUMBER(L_cost_change,
                             L_return_code,
                             L_error_message);
         ---
         if (L_return_code = 'FALSE') then
            raise PROGRAM_ERROR;
         end if;
         ---
         if COST_CHANGE_SQL.CREATE_RCA_COST_CHG(L_error_message,
                                                L_cost_change,
                                                8,
                                                L_vdate + 1,
                                                'A',
                                                'SKU',
                                                SYSDATE,
                                                USER,
                                                NULL,
                                                NULL) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
         if COST_CHANGE_SQL.POP_TEMP_DETAIL_LOC(L_error_message,
                                                L_exists,
                                                'NEW',
                                                L_cost_change,
                                                L_supplier,
                                                L_ordsku_rec.origin_country_id,
                                                L_item,
                                                8) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_COST_CHANGE_LOC',
                          L_table,
                          'unit_cost_new');

         open  C_LOCK_COST_CHANGE_LOC;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_COST_CHANGE_LOC',
                          L_table,
                          'unit_cost_new');
         close C_LOCK_COST_CHANGE_LOC;

         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          L_table,
                          'Item:'||L_item||'Supplier:'||L_supplier||'Location:'||L_location||'Loc_type:'||L_ordloc_rec.loc_type);

          SQL_LIB.SET_MARK('OPEN',
                          'C_LOCS_HAVE_DIFFERENT_COST',
                          L_table,
                          'unit_cost_new');
          open C_LOCS_HAVE_DIFFERENT_COST;
          SQL_LIB.SET_MARK('FETCH',
                          'C_LOCS_HAVE_DIFFERENT_COST',
                          L_table,
                          'unit_cost_new');
          fetch C_LOCS_HAVE_DIFFERENT_COST into L_dummy;
          if C_LOCS_HAVE_DIFFERENT_COST%NOTFOUND then
             L_different_cost_exist :=  'N';
          else
             L_different_cost_exist := 'Y';
          end if;
           SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCS_HAVE_DIFFERENT_COST',
                          L_table,
                          'unit_cost_new');
           close C_LOCS_HAVE_DIFFERENT_COST;


          update cost_change_loc_temp
            set unit_cost_new       = L_adjusted_unit_cost
          where cost_change         = L_cost_change
            and supplier            = L_supplier
            and item                = L_item
            and origin_country_id   = L_ordsku_rec.origin_country_id
            and ( (loc_type            = L_ordloc_rec.loc_type
                     and location            = L_location)
                     or L_different_cost_exist = 'N');
         ---
         if COST_CHANGE_SQL.INSERT_UPDATE_COST_CHANGE(L_error_message,
                                                      L_cost_change) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
         if COST_CHANGE_SQL.DELETE_COST_CHANGE_LOC_TEMP(L_error_message,
                                                        L_cost_change) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
      end if;
      ---
   end if;
   ---
EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/
ALTER TRIGGER "REIM_TABLE_IRCA_AIR" ENABLE;
