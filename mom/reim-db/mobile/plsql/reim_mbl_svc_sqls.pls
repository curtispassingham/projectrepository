CREATE OR REPLACE PACKAGE REIM_MBL_SVC_SQL AS

   --Ordering
   ORDER_ASC  CONSTANT VARCHAR2(10) := 'ASC';
   ORDER_DESC CONSTANT VARCHAR2(10) := 'DESC';

   --Used in Supplier List
   SORT_BY_NUM_OF_INVOICES CONSTANT VARCHAR2(15) := 'INVOICES_DUE';
   SORT_BY_SUPPLIER_NAME   CONSTANT VARCHAR2(20) := 'SUPPLIER_SITE_NAME';
   --Used in Invoice List
   SORT_BY_INVOICE_COST    CONSTANT VARCHAR2(15) := 'INVOICE_COST';
   SORT_BY_INVOICE_NUMBER  CONSTANT VARCHAR2(15) := 'INVOICE_NUMBER';
   --Used in Employee_List
   SORT_BY_REMAIN_INVOICE  CONSTANT VARCHAR2(20) := 'TOTAL_INVOICES_DUE';
   SORT_BY_EMPLOYEE_ID     CONSTANT VARCHAR2(15) := 'EMPLOYEE_ID';

   --Calendar
   PAST_DUE       CONSTANT VARCHAR2(10) := 'PAST_DUE';
   TODAY          CONSTANT VARCHAR2(5)  := 'TODAY';
   TOMORROW       CONSTANT VARCHAR2(10) := 'TOMORROW';
   TWO_DAYS_OUT   CONSTANT VARCHAR2(15) := 'TWO_DAYS_OUT';
   THREE_DAYS_OUT CONSTANT VARCHAR2(15) := 'THREE_DAYS_OUT';
   THREE_MONTHS   CONSTANT NUMBER(2)    := 84;

--------------------------------------------------------------------------------
FUNCTION GET_DASHBOARD(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dashboard        OUT REIM_MBL_DASHBOARD_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_LIST(O_error_message             OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_reim_mbl_supp_list_tbl    OUT REIM_MBL_SUPP_LIST_TBL,
                           IO_pagination            IN OUT MBL_PAGINATION_REC,
                           I_supp_list_crit         IN     REIM_MBL_SUPP_LIST_CRIT_REC,
                           I_search_string          IN     VARCHAR2,
                           I_sort_attrib            IN     VARCHAR2,
                           I_sort_direction         IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION GET_SUPPLIER_DETAILS(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_supplier_details    OUT REIM_MBL_SUPPLIER_DTL_REC,
                              I_supplier_id      IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_INVOICES(O_error_message     OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_invoices          OUT REIM_MBL_INVOICE_DTL_TBL,
                      IO_pagination    IN OUT MBL_PAGINATION_REC,
                      I_invoice_crit   IN     REIM_MBL_INVOICE_CRIT_REC,
                      I_days_out       IN     VARCHAR2,
                      I_search_string  IN     VARCHAR2,
                      I_sort_attrib    IN     VARCHAR2,
                      I_sort_direction IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_EMPLOYEES(O_error_message     OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_employees         OUT REIM_MBL_EMPLOYEES_TBL,
                       IO_pagination    IN OUT MBL_PAGINATION_REC,
                       I_sort_attrib    IN     VARCHAR2,
                       I_sort_direction IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_EMPLOYEE_DETAILS(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_employee_id      OUT VARCHAR2,   
                              O_emp_inv_count    OUT REIM_MBL_EMPLOYEE_DTL_DATE_REC,
                              I_employee_id   IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION INVOICE_CRIT_SUPPLIERS(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_suppliers        OUT REIM_MBL_SUPP_CRIT_TBL,
                                IO_pagination   IN OUT MBL_PAGINATION_REC,
                                I_input_string  IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION INVOICE_CRIT_EMPLOYEES(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_employees        OUT OBJ_VARCHAR_ID_TABLE,
                                IO_pagination   IN OUT MBL_PAGINATION_REC,
                                I_input_string  IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
END REIM_MBL_SVC_SQL;
/