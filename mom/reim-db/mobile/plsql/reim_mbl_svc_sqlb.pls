CREATE OR REPLACE PACKAGE BODY REIM_MBL_SVC_SQL AS

--------------------------------------------------------------------------------
--                         PRIVATE FUNCTION SPECS                             --
--------------------------------------------------------------------------------
FUNCTION POP_SEARCH_SUPS_EMPLOYEES(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POP_DEFAULT_SUPS_EMPLOYEES(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POP_CRIT_SUPS_EMPLOYEES(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_supplier_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                 I_employee_ids  IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--                            PUBLIC FUNCTIONS                                --
--------------------------------------------------------------------------------
FUNCTION GET_DASHBOARD(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dashboard        OUT REIM_MBL_DASHBOARD_REC)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'REIM_MBL_SVC_SQL.GET_DASHBOARD';

   L_vdate DATE := GET_VDATE;

   cursor C_DASHBOARD is
      select REIM_MBL_DASHBOARD_REC(L_vdate,
                                    unmatch_past_due,
                                    unmatch_today,
                                    unmatch_tomorrow,
                                    unmatch_two_days,
                                    unmatch_three_days,
                                    match_past_due,
                                    match_today,
                                    match_tomorrow,
                                    match_two_days,
                                    match_three_days,
                                    supplier_count,
                                    employee_count,
                                    invoice_count,
                                    yesterday_automatch_count,
                                    yesterday_manual_match_count,
                                    three_month_automatch_count,
                                    three_month_manual_match_count)
        from (--Indicates if the remaining invoice is past due
              select SUM(unmatch_past_due) OVER() unmatch_past_due,
                     SUM(unmatch_today) OVER() unmatch_today,
                     SUM(unmatch_tomorrow) OVER() unmatch_tomorrow,
                     SUM(unmatch_two_days) OVER() unmatch_two_days,
                     SUM(unmatch_three_days) OVER() unmatch_three_days,
                     SUM(match_past_due) OVER() match_past_due,
                     SUM(match_today) OVER() match_today,
                     SUM(match_tomorrow) OVER() match_tomorrow,
                     SUM(match_two_days) OVER() match_two_days,
                     SUM(match_three_days) OVER() match_three_days,
                     COUNT(distinct supplier_count) OVER() -1 supplier_count,
                     MAX(employee_count) OVER() employee_count,
                     SUM(invoice_count) OVER() invoice_count,
                     SUM(yesterday_automatch_count) OVER() yesterday_automatch_count,
                     SUM(yesterday_manual_match_count) OVER() yesterday_manual_match_count,
                     SUM(three_month_automatch_count) OVER() three_month_automatch_count,
                     SUM(three_month_manual_match_count) OVER() three_month_manual_match_count,
                     ROW_NUMBER() OVER(ORDER BY rownum) row_num
                from (select --Indicates if the remaining invoice is due yesterday
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH) and
                                     idh.due_date < L_vdate then
                                     ---
                                   1
                                else
                                   0
                             end unmatch_past_due,
                             --Indicates if the remaining invoice is due today
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH) and
                                     idh.due_date = L_vdate then
                                     ---
                                   1
                                else
                                   0
                             end unmatch_today,
                             --Indicates if the remaining invoice is due tomorrow
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH) and
                                     idh.due_date = L_vdate + 1 then
                                     ---
                                   1
                                else
                                   0
                             end unmatch_tomorrow,
                             --Indicates if the remaining invoice is due in 2 days
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH) and
                                     idh.due_date = L_vdate + 2 then
                                     ---
                                   1
                                else
                                   0
                             end unmatch_two_days,
                             --Indicates if the remaining invoice is due in 3 days
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH) and
                                     idh.due_date = L_vdate + 3 then
                                     ---
                                   1
                                else
                                   0
                             end unmatch_three_days,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL and
                                     idh.due_date < L_vdate then
                                     ---
                                   1
                                else
                                   0
                             end match_past_due,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL and
                                     idh.due_date = L_vdate then
                                     ---
                                   1
                                else
                                   0
                             end match_today,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL and
                                     idh.due_date = L_vdate + 1 then
                                     ---
                                   1
                                else
                                   0
                             end match_tomorrow,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL and
                                     idh.due_date = L_vdate + 2 then
                                     ---
                                   1
                                else
                                   0
                             end match_two_days,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL and
                                     idh.due_date = L_vdate + 3 then
                                     ---
                                   1
                                else
                                   0
                             end match_three_days,
                             decode(idh.status, REIM_CONSTANTS.DOC_STATUS_RMTCH, idh.supplier_site_id,
                                                REIM_CONSTANTS.DOC_STATUS_URMTCH, idh.supplier_site_id,
                                                -999) supplier_count,
                             DENSE_RANK() OVER (ORDER BY gtt.varchar2_1) employee_count,
                             decode(idh.status, REIM_CONSTANTS.DOC_STATUS_RMTCH, 1,
                                                REIM_CONSTANTS.DOC_STATUS_URMTCH, 1,
                                                0) invoice_count,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type IN (REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                        REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) and
                                     idh.due_date = L_vdate - 1 then
                                   ---
                                   1
                                else
                                   0
                             end yesterday_automatch_count,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL and
                                     idh.due_date = L_vdate - 1 then
                                   ---
                                   1
                                else
                                   0
                             end yesterday_manual_match_count,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type IN (REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                        REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) and
                                     idh.due_date BETWEEN L_vdate - THREE_MONTHS and L_vdate - 1 then
                                   ---
                                   1
                                else
                                   0
                             end three_month_automatch_count,
                             case
                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                     idh.match_type = REIM_CONSTANTS.MATCH_TYPE_MANUAL and
                                     idh.due_date BETWEEN L_vdate - THREE_MONTHS and L_vdate - 1 then
                                   ---
                                   1
                                else
                                   0
                             end three_month_manual_match_count
                        from im_doc_head idh,
                             gtt_num_num_str_str_date_date gtt,
                             (select store
                                from v_store
                              union
                              select wh store
                                from v_wh) sec
                       where idh.supplier_site_id is NOT NULL
                         and idh.supplier_site_id = gtt.number_1
                         and idh.type             = REIM_CONSTANTS.DOC_TYPE_MRCHI
                         and idh.due_date         BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate + 3)
                         and idh.location         = sec.store))
       where row_num = 1;

BEGIN

   if POP_DEFAULT_SUPS_EMPLOYEES(O_error_message) = 0 then
      return MBL_CONSTANTS.FAILURE;
   end if;

   open C_DASHBOARD;
   fetch C_DASHBOARD into O_dashboard;
   close C_DASHBOARD;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END GET_DASHBOARD;
------------------------------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_LIST(O_error_message             OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_reim_mbl_supp_list_tbl    OUT REIM_MBL_SUPP_LIST_TBL,
                           IO_pagination            IN OUT MBL_PAGINATION_REC,
                           I_supp_list_crit         IN     REIM_MBL_SUPP_LIST_CRIT_REC,
                           I_search_string          IN     VARCHAR2,
                           I_sort_attrib            IN     VARCHAR2,
                           I_sort_direction         IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'REIM_MBL_SVC_SQL.GET_SUPPLIER_LIST';

   L_vdate DATE := GET_VDATE;

   L_row_start     NUMBER(10)           := 0;
   L_row_end       NUMBER(10)           := 0;
   L_num_rec_found NUMBER(10)           := 0;
   L_counter_table OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

   L_weight_non_discrep NUMBER(5, 2) := NULL;
   L_weight_exact_match NUMBER(5, 2) := NULL;
   L_weight_timely_ship NUMBER(5, 2) := NULL;

   L_exchange_type   CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   cursor C_SUPPLIER_LIST is
      select REIM_MBL_SUPP_LIST_REC(sup_name,
                                    supplier_site_id,
                                    supp_scorecard_rating,
                                    invoice_count_remain,
                                    sum_of_outstanding_invoices,
                                    currency_code),
             total_rows
        from (select supplier_site_id,
                     sup_name,
                     invoice_count_remain,
                     currency_code,
                     --Compliance
                     case
                        when compliance > 95 then
                           5
                        when compliance >= 75 and
                             compliance < 95 then
                           4
                        when compliance >= 55 and
                             compliance < 75 then
                           3
                        when compliance >= 35 and
                             compliance < 55 then
                           2
                        when compliance < 35 then
                           1
                     end supp_scorecard_rating,
                     sum_of_outstanding_invoices,
                     --Sorting
                     case
                        when I_sort_attrib = SORT_BY_NUM_OF_INVOICES and
                             I_sort_direction = ORDER_ASC then
                           ROW_NUMBER() OVER (ORDER BY invoice_count_remain asc,
                                                       sup_name)
                        when I_sort_attrib = SORT_BY_NUM_OF_INVOICES and
                             I_sort_direction = ORDER_DESC then
                           ROW_NUMBER() OVER (ORDER BY invoice_count_remain desc,
                                                       sup_name)
                        when I_sort_attrib = SORT_BY_SUPPLIER_NAME and
                             I_sort_direction = ORDER_ASC then
                           ROW_NUMBER() OVER (ORDER BY sup_name asc)
                        when I_sort_attrib = SORT_BY_SUPPLIER_NAME and
                             I_sort_direction = ORDER_DESC then
                           ROW_NUMBER() OVER (ORDER BY sup_name desc)
                        else
                           ROW_NUMBER() OVER (ORDER BY invoice_count_remain desc,
                                                       sup_name)
                     end counter,
                     --Paging
                     COUNT(*) OVER () total_rows
                from (select supplier_site_id,
                             sup_name,
                             invoice_count_remain,
                             currency_code,
                             sum_of_outstanding_invoices,
                             --Decode statements handle if those count values are zero - thus handling divide by zero errors
                             (100 - ((L_weight_non_discrep * (cost_disc_count + qty_disc_count) / DECODE(invoice_count,
                                                                                                         0, 1,
                                                                                                         invoice_count)) +
                                     (L_weight_exact_match * (automatch_w_tol_cnt / DECODE(automatch_cnt,
                                                                                           0, 1,
                                                                                           automatch_cnt))) +
                                     (L_weight_timely_ship * ((early_ship_cnt + late_ship_cnt) / DECODE(ship_cnt,
                                                                                                        0, 1,
                                                                                                        ship_cnt))))) compliance
                        from (select supplier_site_id,
                                     sup_name,
                                     --Calculate total cost of invoices and indicate what records have invoice amounts
                                     SUM(total_cost_remain) OVER (PARTITION BY supplier_site_id) sum_of_outstanding_invoices,
                                     currency_code,
                                     --Sum the number of outstanding invoices based on the indicator invoice_count
                                     SUM(invoice_count_remain) OVER (PARTITION BY supplier_site_id) invoice_count_remain,
                                     --Rank all invoices per supplier so we can have a distinct list of suppliers
                                     RANK() OVER (PARTITION BY supplier_site_id
                                                      ORDER BY rownum) distinct_supp_rank,
                                     --SUPP_SCORECARD_RATING Variables
                                     --Total number of invoices per supplier
                                     COUNT(*) OVER (PARTITION BY supplier_site_id) invoice_count,
                                     --Sum the number of cost discrepancies based on the indicator
                                     SUM(cost_disc) OVER (PARTITION BY supplier_site_id) cost_disc_count,
                                     --Sum the number of unit discrepancies based on the indicator
                                     SUM(qty_disc) OVER (PARTITION BY supplier_site_id) qty_disc_count,
                                     --Sum the number of auto matches within tolerance based on the indicator
                                     SUM(automatch_w_tolerance) OVER (PARTITION BY supplier_site_id) automatch_w_tol_cnt,
                                     --Sum the number of auto matches based on the indicator
                                     SUM(automatch) OVER (PARTITION BY supplier_site_id) automatch_cnt,
                                     MIN(early_ship) OVER (PARTITION BY supplier_site_id) early_ship_cnt,
                                     MIN(late_ship) OVER (PARTITION BY supplier_site_id) late_ship_cnt,
                                     MIN(ship_count) OVER (PARTITION BY supplier_site_id) ship_cnt
                                from (select supplier_site_id,
                                             sup_name,
                                             total_cost_remain,
                                             currency_code,
                                             invoice_count_remain,
                                             MAX(cost_disc) OVER (PARTITION BY doc_id) cost_disc,
                                             MAX(qty_disc) OVER (PARTITION BY doc_id) qty_disc,
                                             automatch_w_tolerance,
                                             automatch,
                                             SUM(decode(distinct_shipment,1,early_ship,0)) OVER (PARTITION BY supplier_site_id) early_ship,
                                             SUM(decode(distinct_shipment,1,late_ship,0)) OVER (PARTITION BY supplier_site_id) late_ship,
                                             count(distinct shipment) OVER (PARTITION BY supplier_site_id) ship_count,
                                             --Get rid of duplicate doc id records created from joining im_invoice_detail
                                             RANK() OVER (PARTITION BY doc_id
                                                              ORDER BY rownum) distinct_doc_id
                                        from (select idh.doc_id,
                                                     idh.supplier_site_id,
                                                     ship.shipment,
                                                     RANK() OVER (PARTITION BY shipment
                                                                      ORDER BY rownum) distinct_shipment,
                                                     sups.sup_name,
                                                     case
                                                        when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                            REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                                                           idh.total_cost_inc_tax * mc.exchange_rate
                                                        else
                                                           0
                                                     end total_cost_remain,
                                                     sups.currency_code,
                                                     --Indicates if the record is a remaining invoice
                                                     case
                                                        when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                            REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                                                           1
                                                        else
                                                           0
                                                     end invoice_count_remain,
                                                     case
                                                        when iid.cost_matched in('D','R') then
                                                           1
                                                        else
                                                           0
                                                     end cost_disc,
                                                     case
                                                        when iid.qty_matched in('D','R') then
                                                           1
                                                        else
                                                           0
                                                     end qty_disc,
                                                     --Indicates if the invoice is automatch with tolerance
                                                     case
                                                        when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                                            REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                                             idh.match_type IN (REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                                                REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) and
                                                             idh.variance_within_tolerance is NOT NULL and
                                                             idh.variance_within_tolerance != 0 then
                                                           ---
                                                           1
                                                        else
                                                           0
                                                     end automatch_w_tolerance,
                                                     --Indicates if the invoice is automatch
                                                     case
                                                        when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                                            REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                                             idh.match_type IN (REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                                                REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) then
                                                           ---
                                                           1
                                                        else
                                                           0
                                                     end automatch,
                                                     --Early Shipdate
                                                     case
                                                        when ord.not_before_date > ship.ship_date then
                                                           ---
                                                           1
                                                        else
                                                           0
                                                     end early_ship,
                                                     --Late Shipdate
                                                     case
                                                        when ord.not_after_date < ship.ship_date then
                                                           ---
                                                           1
                                                        else
                                                           0
                                                     end late_ship
                                                from gtt_num_num_str_str_date_date gtt,
                                                     im_doc_head idh,
                                                     sups,
                                                     ordhead ord,
                                                     shipment ship,
                                                     (select store
                                                        from v_store
                                                      union
                                                      select wh store
                                                        from v_wh) sec,
                                                     --
                                                     (select from_currency,
                                                              to_currency,
                                                              effective_date,
                                                              exchange_rate,
                                                              rank() over
                                                                  (PARTITION BY from_currency, to_currency, exchange_type
                                                                       ORDER BY effective_date DESC) date_rank
                                                         from mv_currency_conversion_rates
                                                        where exchange_type   = L_exchange_type
                                                          and effective_date <= L_vdate) mc,
                                                     --
                                                     im_invoice_detail iid
                                               where gtt.number_1     = idh.supplier_site_id
                                                 and sups.supplier    = idh.supplier_site_id
                                                 and idh.type         = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                                 and idh.due_date     BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1)
                                                 and idh.status       not in(REIM_CONSTANTS.DOC_STATUS_WORKSHEET,
                                                                             REIM_CONSTANTS.DOC_STATUS_DELETE)
                                                 --Handles search string logic
                                                 and (   LOWER(sups.supplier) LIKE('%'||LOWER(NVL(I_search_string, sups.supplier))||'%')
                                                      or LOWER(sups.sup_name) LIKE('%'||LOWER(NVL(I_search_string, sups.sup_name))||'%'))
                                                 and idh.order_no     = ord.order_no
                                                 and idh.order_no     = ship.order_no(+)
                                                 and idh.location     = sec.store
                                                 and mc.from_currency = idh.currency_code
                                                 and mc.to_currency   = sups.currency_code
                                                 and mc.date_rank     = 1
                                                 and idh.doc_id       = iid.doc_id (+)))
                               where distinct_doc_id = 1)
                       --Filter out number of invoices if it is populated
                       where invoice_count_remain >= NVL(I_supp_list_crit.num_of_invoices, 0)
                         and invoice_count_remain > 0
                         and distinct_supp_rank = 1))
       where counter >= L_row_start
         and counter <  L_row_end
       order by counter;

BEGIN

   select decode(consolidation_ind, 'Y', 'C', 'O')
     into L_exchange_type
     from system_config_options;

   --Paging Logic
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   select weight_exact_match,
          weight_non_discrepancy,
          weight_timely_ship
     into L_weight_exact_match,
          L_weight_non_discrep,
          L_weight_timely_ship
     from im_oi_sys_options;

   if I_search_string is NOT NULL then
      if POP_SEARCH_SUPS_EMPLOYEES(O_error_message) = 0 then
         return MBL_CONSTANTS.FAILURE;
      end if;
   elsif (I_supp_list_crit.supplier_ids is NOT NULL and
          I_supp_list_crit.supplier_ids.COUNT > 0) or
         (I_supp_list_crit.employee_ids is NOT NULL and
          I_supp_list_crit.employee_ids.COUNT > 0) then
       
      if POP_CRIT_SUPS_EMPLOYEES(O_error_message,
                                 I_supp_list_crit.supplier_ids,
                                 I_supp_list_crit.employee_ids) = 0 then
         return MBL_CONSTANTS.FAILURE;
      end if;
   else
      if POP_DEFAULT_SUPS_EMPLOYEES(O_error_message) = 0 then
         return MBL_CONSTANTS.FAILURE;
      end if;
   end if;

   open C_SUPPLIER_LIST;
   fetch C_SUPPLIER_LIST BULK COLLECT into O_reim_mbl_supp_list_tbl,
                                           L_counter_table;
   close C_SUPPLIER_LIST;

   --Paging Logic
   if L_counter_table is NOT NULL and
      L_counter_table.COUNT > 0 then
      ---
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   else
      L_num_rec_found := 0;
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END GET_SUPPLIER_LIST;
------------------------------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_DETAILS(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_supplier_details    OUT REIM_MBL_SUPPLIER_DTL_REC,
                              I_supplier_id      IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'REIM_MBL_SVC_SQL.GET_SUPPLIER_DETAILS';

   L_vdate DATE := GET_VDATE;

   L_weight_non_discrep NUMBER(5, 2) := NULL;
   L_weight_exact_match NUMBER(5, 2) := NULL;
   L_weight_timely_ship NUMBER(5, 2) := NULL;

   L_exchange_type   CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   cursor C_SUPPLIER_DETAIL is
      select REIM_MBL_SUPPLIER_DTL_REC(sup_name,
                                       supplier_site_id,
                                       --Compliance
                                       case
                                          when compliance > 95 then
                                             5
                                          when compliance >= 75 and
                                               compliance < 95 then
                                             4
                                          when compliance >= 55 and
                                               compliance < 75 then
                                             3
                                          when compliance >= 35 and
                                               compliance < 55 then
                                             2
                                          when compliance < 35 then
                                             1
                                       end,
                                       invoice_past_due,
                                       invoice_due_today,
                                       invoice_due_tomorrow,
                                       assigned_to,
                                       invoice_count,
                                       total_cost,
                                       automatch_cnt,
                                       automatch_rate,
                                       cost_disc,
                                       qty_disc,
                                       cost_percentage,
                                       qty_percentage,
                                       currency_code)
        from (select supplier_site_id,
                     sup_name,
                     --Decode statements handle if those count values are zero - thus handling divide by zero errors
                     (100 - ((L_weight_non_discrep * (cost_disc + qty_disc) / DECODE(invoice_count,
                                                                                     0, 1,
                                                                                     invoice_count)) +
                             (L_weight_exact_match * (automatch_w_tol_cnt / DECODE(automatch_cnt,
                                                                                   0, 1,
                                                                                   automatch_cnt))) +
                            (L_weight_timely_ship * ((early_ship_cnt + late_ship_cnt) / DECODE(ship_cnt,
                                                                                                0, 1,
                                                                                                ship_cnt))))) compliance,
                     invoice_past_due,
                     invoice_due_today,
                     invoice_due_tomorrow,
                     assigned_to,
                     invoice_count,
                     total_cost,
                     automatch_cnt,
                     (automatch_cnt/decode(match_count,0,1,match_count)) * 100 automatch_rate,
                     cost_disc,
                     qty_disc,
                     /*is:could be on invoice_past_due not on invoice_count */
                     (100 * (cost_disc / decode(invoice_count,0,1,invoice_count))) cost_percentage,
                     (100 * (qty_disc / decode(invoice_count,0,1,invoice_count))) qty_percentage,
                     currency_code
                from (select supplier_site_id,
                             sup_name,
                             SUM(invoice_past_due) OVER (PARTITION BY supplier_site_id) invoice_past_due,
                             SUM(invoice_due_today) OVER (PARTITION BY supplier_site_id) invoice_due_today,
                             SUM(invoice_due_tomorrow) OVER (PARTITION BY supplier_site_id) invoice_due_tomorrow,
                             assigned_to,
                             SUM(invoice_count) OVER (PARTITION BY supplier_site_id) invoice_count,
                             SUM(total_cost) OVER (PARTITION BY supplier_site_id) total_cost,
                             SUM(match_count) OVER (PARTITION BY supplier_site_id) match_count,
                             SUM(automatch) OVER (PARTITION BY supplier_site_id) automatch_cnt,
                             SUM(cost_disc) OVER (PARTITION BY supplier_site_id) cost_disc,
                             SUM(qty_disc) OVER (PARTITION BY supplier_site_id) qty_disc,
                             currency_code,
                             RANK() OVER (PARTITION BY supplier_site_id
                                              ORDER BY rownum) supp_distinct_ind,
                             --SUPP_SCORECARD_RATING Variables
                             --Sum the number of auto matches within tolerance based on the indicator
                             SUM(automatch_w_tolerance) OVER (PARTITION BY supplier_site_id) automatch_w_tol_cnt,
                             --Sum the number of early shipments based on the indicator
                             MIN(early_ship) OVER (PARTITION BY supplier_site_id) early_ship_cnt,
                             --Sum the number of late shipments based on the indicator
                             MIN(late_ship) OVER (PARTITION BY supplier_site_id) late_ship_cnt,
                             MIN(ship_count) OVER (PARTITION BY supplier_site_id) ship_cnt
                        from (select supplier_site_id,
                                     sup_name,
                                     invoice_past_due,
                                     invoice_due_today,
                                     invoice_due_tomorrow,
                                     assigned_to,
                                     invoice_count,
                                     total_cost,
                                     currency_code,
                                     MAX(cost_disc) OVER (PARTITION BY doc_id) cost_disc,
                                     MAX(qty_disc) OVER (PARTITION BY doc_id) qty_disc,
                                     automatch_w_tolerance,
                                     match_count,
                                     automatch,
                                     SUM(decode(distinct_shipment,1,early_ship,0)) OVER (PARTITION BY supplier_site_id) early_ship,
                                     SUM(decode(distinct_shipment,1,late_ship,0)) OVER (PARTITION BY supplier_site_id) late_ship,
                                     COUNT(distinct shipment) OVER (PARTITION BY supplier_site_id) ship_count,
                                     RANK() OVER (PARTITION BY doc_id
                                                      ORDER BY rownum) distinct_doc_id
                                from (select idh.doc_id,
                                             idh.supplier_site_id,
                                             sups.sup_name,
                                             ship.shipment,
                                             RANK() OVER (PARTITION BY shipment
                                                              ORDER BY rownum) distinct_shipment,
                                             --Indicates if the remaining invoice is past due
                                             case
                                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH) and
                                                     idh.due_date < L_vdate then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end invoice_past_due,
                                             --Indicates if the remaining invoice is due today
                                             case
                                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH) and
                                                     idh.due_date = L_vdate then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end invoice_due_today,
                                             --Indicates if the remaining invoice is due tomorrow
                                             case
                                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                    REIM_CONSTANTS.DOC_STATUS_URMTCH) and
                                                     idh.due_date = L_vdate + 1 then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end invoice_due_tomorrow,
                                             supp_expl.ap_reviewer assigned_to,
                                             --Indicates if the invoice happened in the last 3 months
                                             case
                                                when idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end invoice_count,
                                             --Indicates if the invoice happened in the last 3 months
                                             case
                                                when idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) 
                                                     and idh.status in (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                                        REIM_CONSTANTS.DOC_STATUS_URMTCH,
                                                                        REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                                        REIM_CONSTANTS.DOC_STATUS_POSTED) then
                                                   ---
                                                   idh.total_cost_inc_tax * mc.exchange_rate
                                                else
                                                   0
                                             end total_cost,
                                             --Indicates if the invoice is matched
                                             case
                                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                                     idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end match_count,
                                             --Indicates if the invoice is automatch
                                             case
                                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                                     idh.match_type IN (REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                                        REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) and
                                                     idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end automatch,
                                             case
                                                when iid.cost_matched in ('D','R') and
                                                     idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) then
                                                   1
                                                else
                                                   0
                                             end cost_disc,
                                             case
                                                when iid.qty_matched in ('D','R') and
                                                     idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) then
                                                   1
                                                else
                                                   0
                                             end qty_disc,
                                             --Indicates if the invoice is automatch with tolerance
                                             case
                                                when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                                    REIM_CONSTANTS.DOC_STATUS_POSTED) and
                                                     idh.match_type IN (REIM_CONSTANTS.MATCH_TYPE_AUTO,
                                                                        REIM_CONSTANTS.DOC_MERCH_TYPE_ERS) and
                                                     idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) and
                                                     idh.variance_within_tolerance is NOT NULL and
                                                     idh.variance_within_tolerance != 0 then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end automatch_w_tolerance,
                                             --Early Shipdate
                                             case
                                                when ord.not_before_date > ship.ship_date and
                                                     idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end early_ship,
                                             --Late Shipdate
                                             case
                                                when ord.not_after_date < ship.ship_date and
                                                     idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1) then
                                                   ---
                                                   1
                                                else
                                                   0
                                             end late_ship,
                                             sups.currency_code
                                        from im_doc_head idh,
                                             sups,
                                             ordhead ord,
                                             shipment ship,
                                             v_im_supp_site_attrib_expl supp_expl,
                                             (select store
                                                from v_store
                                              union
                                              select wh store
                                                from v_wh) sec,
                                             --
                                             (select from_currency,
                                                     to_currency,
                                                     effective_date,
                                                     exchange_rate,
                                                     rank() over
                                                         (PARTITION BY from_currency, to_currency, exchange_type
                                                              ORDER BY effective_date DESC) date_rank
                                                from mv_currency_conversion_rates
                                               where exchange_type   = L_exchange_type
                                                 and effective_date <= L_vdate) mc,
                                             --
                                             im_invoice_detail iid
                                       where idh.supplier_site_id = I_supplier_id
                                         and idh.supplier_site_id = supp_expl.supplier
                                         and idh.type             = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                         and idh.due_date         BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate + 1)
                                         and idh.status           not in(REIM_CONSTANTS.DOC_STATUS_WORKSHEET,
                                                                         REIM_CONSTANTS.DOC_STATUS_DELETE)
                                         and sups.supplier        = idh.supplier_site_id
                                         and idh.order_no         = ord.order_no
                                         and idh.order_no         = ship.order_no(+)
                                         and sec.store            = idh.location
                                         and mc.from_currency     = idh.currency_code
                                         and mc.to_currency       = sups.currency_code
                                         and mc.date_rank         = 1
                                         and idh.doc_id           = iid.doc_id (+)))
                       where distinct_doc_id = 1)
               where supp_distinct_ind = 1);

BEGIN

   select decode(consolidation_ind, 'Y', 'C', 'O')
     into L_exchange_type
     from system_config_options;

   --No need to call the GTT logic since a valid supplier site has to be selected
   --prior to calling this function
   select weight_exact_match,
          weight_non_discrepancy,
          weight_timely_ship
     into L_weight_exact_match,
          L_weight_non_discrep,
          L_weight_timely_ship
     from im_oi_sys_options;

   open C_SUPPLIER_DETAIL;
   fetch C_SUPPLIER_DETAIL into O_supplier_details;
   close C_SUPPLIER_DETAIL;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END GET_SUPPLIER_DETAILS;
--------------------------------------------------------------------------------
FUNCTION GET_INVOICES(O_error_message     OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_invoices          OUT REIM_MBL_INVOICE_DTL_TBL,
                      IO_pagination    IN OUT MBL_PAGINATION_REC,
                      I_invoice_crit   IN     REIM_MBL_INVOICE_CRIT_REC,
                      I_days_out       IN     VARCHAR2,
                      I_search_string  IN     VARCHAR2,
                      I_sort_attrib    IN     VARCHAR2,
                      I_sort_direction IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'REIM_MBL_SVC_SQL.GET_INVOICES';

   L_vdate DATE := GET_VDATE;

   L_row_start     NUMBER(10)           := 0;
   L_row_end       NUMBER(10)           := 0;
   L_num_rec_found NUMBER(10)           := 0;
   L_counter_table OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

   L_exchange_type   CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   cursor C_INVOICES is
      select REIM_MBL_INVOICE_DTL_REC(supplier_id,
                                      supplier_name,
                                      invoice_due_date,
                                      invoice_number,
                                      location,
                                      invoice_cost,
                                      currency_code),
             total_rows
        from (select supplier_id,
                     supplier_name,
                     invoice_due_date,
                     invoice_number,
                     location,
                     invoice_cost,
                     currency_code,
                     --Sorting
                     case
                        when I_sort_attrib = SORT_BY_INVOICE_COST and
                             I_sort_direction = ORDER_ASC then
                           ROW_NUMBER() OVER (ORDER BY invoice_cost asc,
                                                       invoice_number)
                        when I_sort_attrib = SORT_BY_INVOICE_COST and
                             I_sort_direction = ORDER_DESC then
                           ROW_NUMBER() OVER (ORDER BY invoice_cost desc,
                                                       invoice_number)
                        when I_sort_attrib = SORT_BY_INVOICE_NUMBER and
                             I_sort_direction = ORDER_ASC then
                           ROW_NUMBER() OVER (ORDER BY invoice_number asc,
                                                       invoice_cost)
                        when I_sort_attrib = SORT_BY_INVOICE_NUMBER and
                             I_sort_direction = ORDER_DESC then
                           ROW_NUMBER() OVER (ORDER BY invoice_number desc,
                                                       invoice_cost)
                        else
                           ROW_NUMBER() OVER (ORDER BY invoice_cost desc,
                                                       invoice_number)
                     end counter,
                     --Paging
                     COUNT(*) OVER () total_rows
                from (select supplier_id,
                             supplier_name,
                             currency_code,
                             invoice_due_date,
                             invoice_number,
                             location,
                             invoice_cost,
                             RANK() OVER (PARTITION BY invoice_number
                                              ORDER BY rownum) distinct_doc_id,
                             MAX(cost_discrep_ind) OVER (PARTITION BY invoice_number) cost_discrep_ind,
                             MAX(unit_discrep_ind) OVER (PARTITION BY invoice_number) unit_discrep_ind,
                             MAX(cash_discount_ind) OVER (PARTITION BY invoice_number) cash_discount_ind
                        from (select sups.supplier supplier_id,
                                     sups.sup_name supplier_name,
                                     sups.currency_code,
                                     idh.due_date invoice_due_date,
                                     idh.doc_id invoice_number,
                                     idh.location,
                                     idh.total_cost_inc_tax * mc.exchange_rate invoice_cost,
                                     case
                                        when I_invoice_crit.cost_discrepancy_ind = 1 then
                                           case
                                              when iid.cost_matched = 'D' then
                                                 1
                                              else
                                                 0
                                           end
                                        else
                                           1
                                     end cost_discrep_ind,
                                     case
                                        when I_invoice_crit.unit_discrepancy_ind = 1 then
                                           case
                                              when iid.qty_matched = 'D' then
                                                 1
                                              else
                                                 0
                                           end
                                        else
                                           1
                                     end unit_discrep_ind,
                                     case
                                        when I_invoice_crit.cash_discount_ind = 1 then
                                           case
                                              when idh.terms_dscnt_pct > 0 then
                                                 1
                                              else
                                                 0
                                           end
                                        else
                                           1
                                     end cash_discount_ind
                                from gtt_num_num_str_str_date_date gtt,
                                     im_doc_head idh,
                                     sups,
                                     (select store
                                        from v_store
                                      union
                                      select wh store
                                        from v_wh) sec,
                                     --
                                     (select from_currency,
                                             to_currency,
                                             effective_date,
                                             exchange_rate,
                                             rank() over
                                                 (PARTITION BY from_currency, to_currency, exchange_type
                                                      ORDER BY effective_date DESC) date_rank
                                        from mv_currency_conversion_rates
                                       where exchange_type   = L_exchange_type
                                         and effective_date <= L_vdate) mc,
                                     --
                                     im_invoice_detail iid
                               where gtt.number_1  = idh.supplier_site_id
                                 and idh.type      = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                 and idh.status    IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                       REIM_CONSTANTS.DOC_STATUS_URMTCH)
                                 --
                                 and mc.from_currency = idh.currency_code
                                 and mc.to_currency   = sups.currency_code
                                 and mc.date_rank     = 1
                                 --
                                 and (   (    I_days_out   = PAST_DUE
                                          and idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate - 1))
                                      or (    I_days_out   = TODAY
                                          and idh.due_date = L_vdate)
                                      or (    I_days_out   = TOMORROW
                                          and idh.due_date = L_vdate + 1)
                                      or (    I_days_out   = TWO_DAYS_OUT
                                          and idh.due_date = L_vdate + 2)
                                      or (    I_days_out   = THREE_DAYS_OUT
                                          and idh.due_date = L_vdate + 3))
                                 and sups.supplier = idh.supplier_site_id
                                 --Handles search string logic
                                 and (   LOWER(sups.supplier) LIKE('%'||LOWER(NVL(I_search_string, sups.supplier))||'%')
                                      or LOWER(sups.sup_name) LIKE('%'||LOWER(NVL(I_search_string, sups.sup_name))||'%')
                                      or LOWER(idh.doc_id) LIKE('%'||LOWER(NVL(I_search_string, idh.doc_id))||'%'))
                                 and idh.location  = sec.store
                                 and idh.doc_id    = iid.doc_id (+)))
               where invoice_cost      BETWEEN NVL(I_invoice_crit.invoice_amount_low, 0) and
                                               NVL(I_invoice_crit.invoice_amount_high, 999999999999999)
                 and cost_discrep_ind  = 1
                 and unit_discrep_ind  = 1
                 and cash_discount_ind = 1
                 and distinct_doc_id   = 1)
       where counter >= L_row_start
         and counter <  L_row_end
       order by counter;

BEGIN

   select decode(consolidation_ind, 'Y', 'C', 'O')
     into L_exchange_type
     from system_config_options;

   --Paging Logic
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   if I_search_string is NOT NULL  then
      if POP_SEARCH_SUPS_EMPLOYEES(O_error_message) = 0 then
         return MBL_CONSTANTS.FAILURE;
      end if;
   elsif (I_invoice_crit.supplier_ids is NOT NULL and
          I_invoice_crit.supplier_ids.COUNT > 0) or
         (I_invoice_crit.employee_ids is NOT NULL and
          I_invoice_crit.employee_ids.COUNT > 0) then
      if POP_CRIT_SUPS_EMPLOYEES(O_error_message,
                                 I_invoice_crit.supplier_ids,
                                 I_invoice_crit.employee_ids) = 0 then
         return MBL_CONSTANTS.FAILURE;
      end if;
   else
      if POP_DEFAULT_SUPS_EMPLOYEES(O_error_message) = 0 then
         return MBL_CONSTANTS.FAILURE;
      end if;
   end if;

   open C_INVOICES;
   fetch C_INVOICES BULK COLLECT into O_invoices,
                                      L_counter_table;
   close C_INVOICES;

   --Paging Logic
   if L_counter_table is NOT NULL and
      L_counter_table.COUNT > 0 then
      ---
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   else
      L_num_rec_found := 0;
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END GET_INVOICES;
------------------------------------------------------------------------------------------------------
FUNCTION GET_EMPLOYEES(O_error_message     OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_employees         OUT REIM_MBL_EMPLOYEES_TBL,
                       IO_pagination    IN OUT MBL_PAGINATION_REC,
                       I_sort_attrib    IN     VARCHAR2,
                       I_sort_direction IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'REIM_MBL_SVC_SQL.GET_EMPLOYEES';

   L_vdate DATE := GET_VDATE;

   L_row_start     NUMBER(10)           := 0;
   L_row_end       NUMBER(10)           := 0;
   L_num_rec_found NUMBER(10)           := 0;
   L_counter_table OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

   cursor C_EMPLOYEE_LIST is
      select REIM_MBL_EMPLOYEES_REC(employee_id,
                                    invoice_count_remain,
                                    total_matched_invoices),
             total_rows
        from (select employee_id,
                     invoice_count_remain,
                     total_matched_invoices,
                     --Sorting
                     case
                        when I_sort_attrib = SORT_BY_REMAIN_INVOICE and
                             I_sort_direction = ORDER_ASC then
                           ROW_NUMBER() OVER (ORDER BY invoice_count_remain asc,
                                                       employee_id)
                        when I_sort_attrib = SORT_BY_REMAIN_INVOICE and
                             I_sort_direction = ORDER_DESC then
                           ROW_NUMBER() OVER (ORDER BY invoice_count_remain desc,
                                                       employee_id)
                        when I_sort_attrib = SORT_BY_EMPLOYEE_ID and
                             I_sort_direction = ORDER_ASC then
                           ROW_NUMBER() OVER (ORDER BY employee_id asc,
                                                       invoice_count_remain)
                        when I_sort_attrib = SORT_BY_EMPLOYEE_ID and
                             I_sort_direction = ORDER_DESC then
                           ROW_NUMBER() OVER (ORDER BY employee_id desc,
                                                       invoice_count_remain)
                        else
                           ROW_NUMBER() OVER (ORDER BY invoice_count_remain desc,
                                                       employee_id)
                     end counter,
                     --Paging
                     COUNT(*) OVER () total_rows
                from (select employee_id,
                             SUM(invoice_count_remain) OVER (PARTITION BY employee_id) invoice_count_remain,
                             SUM(total_matched_invoices) OVER (PARTITION BY employee_id) total_matched_invoices,
                             RANK() OVER (PARTITION BY employee_id
                                              ORDER BY rownum) distinct_ind
                        from (select gtt.varchar2_1 employee_id,
                                     --Indicates if the record is a remaining invoice
                                     case
                                        when idh.status IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                            REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                                           1
                                        else
                                           0
                                     end invoice_count_remain,
                                     case
                                        when idh.status IN (REIM_CONSTANTS.DOC_STATUS_MTCH,
                                                            REIM_CONSTANTS.DOC_STATUS_POSTED) then
                                           1
                                        else
                                           0
                                     end total_matched_invoices
                                from gtt_num_num_str_str_date_date gtt,
                                     im_doc_head idh,
                                     (select store
                                        from v_store
                                      union
                                      select wh store
                                        from v_wh) sec
                               where gtt.number_1 = idh.supplier_site_id
                                 and idh.due_date BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate + 3)
                                 and idh.type     = REIM_CONSTANTS.DOC_TYPE_MRCHI
                                 and idh.location = sec.store)
                       where employee_id is NOT NULL)
               where distinct_ind = 1)
       where counter >= L_row_start
         and counter <  L_row_end
       order by counter;

BEGIN

   --Paging Logic
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   if POP_DEFAULT_SUPS_EMPLOYEES(O_error_message) = 0 then
      return MBL_CONSTANTS.FAILURE;
   end if;

   open C_EMPLOYEE_LIST;
   fetch C_EMPLOYEE_LIST BULK COLLECT into O_employees,
                                           L_counter_table;
   close C_EMPLOYEE_LIST;

   --Paging Logic
   if L_counter_table is NOT NULL and
      L_counter_table.COUNT > 0 then
      ---
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   else
      L_num_rec_found := 0;
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END GET_EMPLOYEES;
--------------------------------------------------------------------------
FUNCTION GET_EMPLOYEE_DETAILS(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_employee_id      OUT VARCHAR2,
                              O_emp_inv_count    OUT REIM_MBL_EMPLOYEE_DTL_DATE_REC,
                              I_employee_id   IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'REIM_MBL_SVC_SQL.GET_EMPLOYEE_DETAILS';

   L_vdate DATE := GET_VDATE;

   cursor C_EMPLOYEE_DETAIL is
      select REIM_MBL_EMPLOYEE_DTL_DATE_REC(past_due,
                                            today,
                                            tomorrow,
                                            two_days_out,
                                            three_days_out),
             employee_id
        from (select SUM(past_due) OVER () past_due,
                     SUM(today) OVER () today,
                     SUM(tomorrow) OVER () tomorrow,
                     SUM(two_days_out) OVER () two_days_out,
                     SUM(three_days_out) OVER () three_days_out,
                     employee_id,
                     RANK() OVER (PARTITION BY employee_id
                                      ORDER BY rownum) emp_distinct_ind
                from (select --Indicates if the record is a remaining invoice
                             case
                                when idh.due_date < L_vdate and
                                     idh.status   IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                      REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                                   1
                                else
                                   0
                             end past_due,
                             case
                                when idh.due_date = L_vdate and
                                     idh.status   IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                      REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                                   1
                                else
                                   0
                             end today,
                             case
                                when idh.due_date = L_vdate + 1 and
                                     idh.status   IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                      REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                                   1
                                else
                                   0
                             end tomorrow,
                             case
                                when idh.due_date = L_vdate + 2 and
                                     idh.status   IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                      REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                                   1
                                else
                                   0
                             end two_days_out,
                             case
                                when idh.due_date = L_vdate + 3 and
                                     idh.status   IN (REIM_CONSTANTS.DOC_STATUS_RMTCH,
                                                      REIM_CONSTANTS.DOC_STATUS_URMTCH) then
                                   1
                                else
                                   0
                             end three_days_out,
                             supp_expl.ap_reviewer employee_id
                        from v_im_supp_site_attrib_expl supp_expl,
                             im_doc_head idh,
                             (select store
                                from v_store
                              union
                              select wh store
                                from v_wh) sec
                       where supp_expl.ap_reviewer = I_employee_id
                         and supp_expl.supplier    = idh.supplier_site_id
                         and idh.due_date          BETWEEN (L_vdate - THREE_MONTHS) and (L_vdate + 3)
                         and idh.type              = REIM_CONSTANTS.DOC_TYPE_MRCHI
                         and idh.location          = sec.store))
       where emp_distinct_ind = 1;

BEGIN

   --No need to call the GTT logic since a valid employee has to be selected
   --prior to calling this function
   open C_EMPLOYEE_DETAIL;
   fetch C_EMPLOYEE_DETAIL into O_emp_inv_count,
                                O_employee_id;
   close C_EMPLOYEE_DETAIL;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END GET_EMPLOYEE_DETAILS;
--------------------------------------------------------------------------
FUNCTION INVOICE_CRIT_SUPPLIERS(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_suppliers        OUT REIM_MBL_SUPP_CRIT_TBL,
                                IO_pagination   IN OUT MBL_PAGINATION_REC,
                                I_input_string  IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'REIM_MBL_SVC_SQL.INVOICE_CRIT_SUPPLIERS';

   L_row_start     NUMBER(10)           := 0;
   L_row_end       NUMBER(10)           := 0;
   L_num_rec_found NUMBER(10)           := 0;
   L_counter_table OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

   cursor C_SUPPLIER_LIST is
      select REIM_MBL_SUPP_CRIT_REC(supplier,
                                    sup_name),
             COUNT(*) OVER () total_rows
        from (select supplier,
                     sup_name,
                     ROW_NUMBER() OVER (ORDER BY sup_name) counter
                from (select supp_sites.supplier,
                             sups.sup_name,
                             RANK() OVER (PARTITION BY sups.supplier
                                              ORDER BY rownum) sup_distinct_ind
                        from v_im_supp_site_attrib_expl supp_sites,
                             im_doc_head idh,
                             sups,
                             (select store
                                from v_store
                              union
                              select wh store
                                from v_wh) sec
                       where idh.supplier_site_id = supp_sites.supplier
                         and sups.supplier        = supp_sites.supplier
                         and idh.type             = REIM_CONSTANTS.DOC_TYPE_MRCHI
                         and sec.store            = idh.location
                         --Handles search string logic
                         and (   LOWER(sups.supplier) LIKE('%'||LOWER(NVL(I_input_string, sups.supplier))||'%')
                              or LOWER(sups.sup_name) LIKE('%'||LOWER(NVL(I_input_string, sups.sup_name))||'%')))
               where sup_distinct_ind = 1)
       where counter >= L_row_start
         and counter <  L_row_end
       order by counter;

BEGIN

   --Paging Logic
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   if POP_DEFAULT_SUPS_EMPLOYEES(O_error_message) = 0 then
      return MBL_CONSTANTS.FAILURE;
   end if;

   open C_SUPPLIER_LIST;
   fetch C_SUPPLIER_LIST BULK COLLECT into O_suppliers,
                                           L_counter_table;
   close C_SUPPLIER_LIST;

   --Paging Logic
   if L_counter_table is NOT NULL and
      L_counter_table.COUNT > 0 then
      ---
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   else
      L_num_rec_found := 0;
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END INVOICE_CRIT_SUPPLIERS;
--------------------------------------------------------------------------
FUNCTION INVOICE_CRIT_EMPLOYEES(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_employees        OUT OBJ_VARCHAR_ID_TABLE,
                                IO_pagination   IN OUT MBL_PAGINATION_REC,
                                I_input_string  IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'REIM_MBL_SVC_SQL.INVOICE_CRIT_EMPLOYEES';

   L_row_start     NUMBER(10)           := 0;
   L_row_end       NUMBER(10)           := 0;
   L_num_rec_found NUMBER(10)           := 0;
   L_counter_table OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

   cursor C_EMPLOYEE_LIST is
      select employee_id,
             COUNT(*) OVER () total_rows
        from (select ap_reviewer employee_id,
                     ROW_NUMBER() OVER (ORDER BY ap_reviewer) counter
                from (select supp_sites.ap_reviewer,
                             RANK() OVER (PARTITION BY supp_sites.ap_reviewer
                                              ORDER BY rownum) emp_distinct_ind
                        from v_im_supp_site_attrib_expl supp_sites,
                             im_doc_head idh,
                             sups,
                             (select store
                                from v_store
                              union
                              select wh store
                                from v_wh) sec
                       where idh.supplier_site_id = supp_sites.supplier
                         and sups.supplier        = supp_sites.supplier
                         and sec.store            = idh.location
                         and idh.type             = REIM_CONSTANTS.DOC_TYPE_MRCHI
                         --Handles search string logic
                         and LOWER(supp_sites.ap_reviewer) LIKE('%'||LOWER(NVL(I_input_string, supp_sites.ap_reviewer))||'%'))
               where emp_distinct_ind = 1)
       where counter >= L_row_start
         and counter <  L_row_end
       order by counter;

BEGIN

   --Paging Logic
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   if POP_DEFAULT_SUPS_EMPLOYEES(O_error_message) = 0 then
      return MBL_CONSTANTS.FAILURE;
   end if;

   open C_EMPLOYEE_LIST;
   fetch C_EMPLOYEE_LIST BULK COLLECT into O_employees,
                                           L_counter_table;
   close C_EMPLOYEE_LIST;

   --Paging Logic
   if L_counter_table is NOT NULL and
      L_counter_table.COUNT > 0 then
      ---
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   else
      L_num_rec_found := 0;
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END INVOICE_CRIT_EMPLOYEES;
--------------------------------------------------------------------------
FUNCTION POP_DEFAULT_SUPS_EMPLOYEES(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'REIM_MBL_SVC_SQL.POP_DEFAULT_SUPS_EMPLOYEES';

BEGIN

   delete from gtt_num_num_str_str_date_date;
   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1)
      select supp_sites.supplier,
             employees.application_user_id
        from --Find all employees tied to who is logged in
             (--Employee ids
              select database_user_id,
                     application_user_id
                from sec_user
               where SYS_CONTEXT('RETAIL_CTX','APP_USER_ID') = application_user_id
              union all
              --Employees that have a manager
              select sec2.database_user_id,
                     sec2.application_user_id
                from sec_user sec1,
                     sec_user sec2
               where SYS_CONTEXT('RETAIL_CTX','APP_USER_ID') = sec1.application_user_id
                 and sec1.user_seq                           = sec2.manager
              union all
              --Employees that are 2 levels under their manager
              select sec3.database_user_id,
                     sec3.application_user_id
                from sec_user sec1,
                     sec_user sec2,
                     sec_user sec3
               where SYS_CONTEXT('RETAIL_CTX','APP_USER_ID') = sec1.application_user_id
                 and sec1.user_seq                           = sec2.manager
                 and sec2.user_seq                           = sec3.manager) employees,
             --Find all possible suppliers
             v_im_supp_site_attrib_expl supp_sites
       where lower(employees.application_user_id) = lower(supp_sites.ap_reviewer);

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END POP_DEFAULT_SUPS_EMPLOYEES;
--------------------------------------------------------------------------
FUNCTION POP_CRIT_SUPS_EMPLOYEES(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_supplier_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                 I_employee_ids  IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'REIM_MBL_SVC_SQL.POP_CRIT_SUPS_EMPLOYEES';

BEGIN

  if POP_DEFAULT_SUPS_EMPLOYEES(O_error_message) = 0 then
     return MBL_CONSTANTS.FAILURE;
  end if;

   --employees are populated and suppliers are not
   if I_employee_ids is NOT NULL and
      I_employee_ids.COUNT > 0 and
      I_supplier_ids.COUNT = 0 then

      delete from gtt_num_num_str_str_date_date gtt where not exists
        ( select supplier
           from (select supp_sites.supplier,
                        RANK() OVER (PARTITION BY supp_sites.supplier
                                         ORDER BY rownum) sup_rank
                   from --Find all employees tied to who is logged in
                        table(cast(I_employee_ids as OBJ_VARCHAR_ID_TABLE)) employees,
                        --Find all possible suppliers
                        v_im_supp_site_attrib_expl supp_sites
                  where VALUE(employees) = supp_sites.ap_reviewer
                  and gtt.number_1=supp_sites.supplier)
          where sup_rank = 1);

   --suppliers are populated and employees are not
   elsif I_employee_ids.COUNT = 0 and
         I_supplier_ids is NOT NULL and
         I_supplier_ids.COUNT > 0 then

      delete from gtt_num_num_str_str_date_date gtt where not exists
         (select supplier
           from (select supp_sites.supplier,
                        RANK() OVER (PARTITION BY supp_sites.supplier
                                         ORDER BY rownum) sup_rank
                   from table(cast(I_supplier_ids as OBJ_NUMERIC_ID_TABLE)) suppliers,
                        --Find all possible suppliers
                        v_im_supp_site_attrib_expl supp_sites
                  where VALUE(suppliers) = supp_sites.supplier
                  and gtt.number_1=supp_sites.supplier)
          where sup_rank = 1);

   --employees and suppliers are populated
   else

      delete from gtt_num_num_str_str_date_date gtt where not exists
         (select supplier
           from (select supp_sites.supplier,
                        RANK() OVER (PARTITION BY supp_sites.supplier
                                         ORDER BY rownum) sup_rank
                   from table(cast(I_supplier_ids as OBJ_NUMERIC_ID_TABLE)) suppliers,
                        table(cast(I_employee_ids as OBJ_VARCHAR_ID_TABLE)) employees,
                        v_im_supp_site_attrib_expl supp_sites
                  where VALUE(suppliers) = supp_sites.supplier
                    and VALUE(employees) = supp_sites.ap_reviewer
                    and gtt.number_1=supp_sites.supplier)
          where sup_rank = 1);

   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;

END POP_CRIT_SUPS_EMPLOYEES;

------------------------------------------------------------------------------------

FUNCTION POP_SEARCH_SUPS_EMPLOYEES(O_error_message  OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'REIM_MBL_SVC_SQL.POP_SEARCH_SUPS_EMPLOYEES';

BEGIN

      delete from gtt_num_num_str_str_date_date;
      insert into gtt_num_num_str_str_date_date(number_1)
         select supplier
           from (select supp_sites.supplier,
                        RANK() OVER (PARTITION BY supp_sites.supplier
                                         ORDER BY rownum) sup_rank
                   from v_im_supp_site_attrib_expl supp_sites)
          where sup_rank = 1;
          
          return MBL_CONSTANTS.SUCCESS;
          
EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message,
                                   L_program);
      return MBL_CONSTANTS.FAILURE;
          
END POP_SEARCH_SUPS_EMPLOYEES;
--------------------------------------------------------------------------
END REIM_MBL_SVC_SQL;
/
