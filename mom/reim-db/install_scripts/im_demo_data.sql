------------------------------------------------------------------------------------------
---
---  IM_DEMO_DATA.SQL
---
--- Script to create demo application data needed for ReIM installation.
--- It is assumed that LDAP ldif scripts have been ran and user have been created in LDAP.
---
--- It is assumed that RMS demo data has been populated, as the script depends on RMS data present. 
---  
---	The script doesn't preserve data.
------------------------------------------------------------------------------------------
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

prompt Deleteing pre-existing data ...

prompt Deleting from SEC_USER_GROUP
DELETE
FROM
  SEC_USER_GROUP
WHERE
  GROUP_ID IN
  (
    SELECT
      GROUP_ID
    FROM
      SEC_GROUP
    WHERE
      GROUP_NAME = 'REIM_GROUP'
  );

prompt Deleting from SEC_GROUP
DELETE
FROM 
  SEC_GROUP 
WHERE GROUP_NAME='REIM_GROUP';

prompt Deleting from IM_SEC_GRP_REASON_CODE
DELETE
FROM
  IM_SEC_GRP_REASON_CODE;

prompt Deleting from IM_DYNAMIC_SEGMENT_LOC
DELETE
FROM
  IM_DYNAMIC_SEGMENT_LOC;

prompt Deleting from IM_DYNAMIC_SEGMENT_DEPT
DELETE
FROM
  IM_DYNAMIC_SEGMENT_DEPT;

prompt Deleting from IM_DYNAMIC_SEGMENT_CLASS
DELETE
FROM
  IM_DYNAMIC_SEGMENT_CLASS;

prompt Deleting from IM_SUPPLIER_GROUP_MEMBERS
DELETE 
FROM 
  IM_SUPPLIER_GROUP_MEMBERS;

prompt Deleting from IM_SUPPLIER_GROUPS
DELETE 
FROM 
  IM_SUPPLIER_GROUPS;

prompt Deleting from IM_SUPPLIER_OPTIONS
DELETE 
FROM 
  IM_SUPPLIER_OPTIONS;
  
prompt Deleting from IM_MATCH_STGY_DETAIL
DELETE
FROM 
  IM_MATCH_STGY_DETAIL;
  
prompt Deleting from IM_MATCH_STGY_HEAD
DELETE
FROM 
  IM_MATCH_STGY_HEAD;

prompt Deleting from IM_RECEIVER_COST_ADJUST, as dependency of IM_RESOLUTION_ACTION
DELETE 
FROM
  IM_RECEIVER_COST_ADJUST;
  
prompt Deleting from IM_RECEIVER_UNIT_ADJUST, as dependency of IM_RESOLUTION_ACTION
DELETE 
FROM
  IM_RECEIVER_UNIT_ADJUST;

prompt Deleting from IM_RESOLUTION_ACTION, as dependency of IM_REASON_CODES 
DELETE
FROM 
  IM_RESOLUTION_ACTION;
  
prompt Deleting from IM_CN_DETAIL_MATCH_HIS, as dependency of IM_DOC_DETAIL_REASON_CODES
DELETE 
FROM 
  IM_CN_DETAIL_MATCH_HIS;
  
prompt Deleting from IM_DOC_DETAIL_RC_TAX, as dependency of IM_REASON_CODES   
DELETE 
FROM 
  IM_DOC_DETAIL_RC_TAX;
  
prompt Deleting from IM_DOC_DETAIL_REASON_CODES, as dependency of IM_REASON_CODES  
DELETE 
FROM 
  IM_DOC_DETAIL_REASON_CODES;

prompt Deleting from IM_DOC_DETAIL_COMMENTS, as dependency of IM_REASON_CODES   
DELETE 
FROM 
  IM_DOC_DETAIL_COMMENTS;

prompt Deleting from IM_REASON_TRAN_CODE_MAP, as dependency of IM_REASON_CODES   
DELETE 
FROM 
  IM_REASON_TRAN_CODE_MAP;

prompt Deleting from IM_TRAN_CODE, as dependency of IM_REASON_CODES   
DELETE 
FROM 
  IM_TRAN_CODE;

prompt Deleting from IM_SYSTEM_OPTIONS
DELETE 
FROM 
  IM_SYSTEM_OPTIONS;
  
prompt Deleting from IM_REASON_CODES
DELETE 
FROM 
  IM_REASON_CODES;

prompt Deleting from IM_GL_CROSS_REF
DELETE 
FROM 
  IM_GL_CROSS_REF;

prompt Deleting from IM_GL_OPTIONS
DELETE 
FROM 
  IM_GL_OPTIONS;
  
prompt Deleting from IM_VALID_ACCOUNTS
DELETE
FROM 
  IM_VALID_ACCOUNTS;
  
prompt Deleting from IM_TOLERANCE_LEVEL_MAP
DELETE
FROM 
  IM_TOLERANCE_LEVEL_MAP;
  
prompt Deleting from IM_TOLERANCE_DETAIL
DELETE
FROM 
  IM_TOLERANCE_DETAIL;
  
prompt Deleting from IM_TOLERANCE_HEAD
DELETE
FROM 
  IM_TOLERANCE_HEAD;
  
prompt Deleting from IM_OI_SYS_OPTIONS
DELETE
FROM 
  IM_OI_SYS_OPTIONS;
  
prompt Deleting from IM_FIXED_DEAL_SOB_LOC_DEFAULT
DELETE
FROM 
  IM_FIXED_DEAL_SOB_LOC_DEFAULT;

COMMIT;
-------------------------------------

prompt Populating new data ...

----------------------sec_group------
prompt Adding data to SEC_GROUP table

INSERT
INTO
  SEC_GROUP
  (
    GROUP_ID,
    GROUP_NAME,
    COMMENTS
  )
SELECT
  SEC_GROUP_SEQUENCE.NEXTVAL AS GROUP_ID,
  'REIM_GROUP'               AS GROUP_NAME,
  'ReIM security group'      AS COMMENTS
FROM
  DUAL;
-------------------------------------------
----------------------sec_user-------------
prompt Adding data to SEC_USER table

INSERT
INTO
  SEC_USER
  (
    USER_SEQ,
    DATABASE_USER_ID,
    APPLICATION_USER_ID,
    MANAGER,
    REIM_USER_IND
  )
SELECT
  SEC_USER_SEQUENCE.NEXTVAL AS USER_SEQ,
  NULL                      AS DATABASE_USER_ID,
  users.reim_user           AS APPLICATION_USER_ID,
  NULL                      AS MANAGER,
  'Y'                       AS REIM_USER_IND
FROM
  (
    SELECT
      'ABIGAIL_FRASER' 	AS reim_user
    FROM
      dual
    UNION
    SELECT
      'FRAN_MORETTI' 	AS reim_user
    FROM
      dual
    UNION
    SELECT
      'BENNY_ANDERSON' 	AS reim_user
    FROM
      dual
    UNION
    SELECT
      'CONNER_LU' 		AS reim_user
    FROM
      dual
    UNION
    SELECT
      'REIM_ADMIN' 		AS reim_user
    FROM
      dual
    UNION
    SELECT
      'REIM_STEWARD' 	AS reim_user
    FROM
      dual
  )
  USERS
WHERE
  NOT EXISTS
  (
    SELECT
      'X'
    FROM
      SEC_USER
    WHERE
      APPLICATION_USER_ID IN ( 'ABIGAIL_FRASER', 'FRAN_MORETTI',
      'BENNY_ANDERSON', 'CONNER_LU', 'REIM_ADMIN', 'REIM_STEWARD')
  );
-------------------------------------------
----------------------sec_user_group-------
prompt Adding data to SEC_USER_GROUP table
	
INSERT
INTO
  SEC_USER_GROUP
  (
    GROUP_ID,
    USER_SEQ,
	CREATE_ID,
	CREATE_DATETIME
  )
SELECT
  SG.GROUP_ID,
  SU.USER_SEQ,
  'REIM_DEMO_USER',
  SYSDATE
FROM
  SEC_USER SU,
  SEC_GROUP SG
WHERE
  SU.APPLICATION_USER_ID IN ( 'ABIGAIL_FRASER','FRAN_MORETTI','BENNY_ANDERSON','CONNER_LU','REIM_ADMIN','REIM_STEWARD' )
AND SG.GROUP_NAME         = 'REIM_GROUP';
---------------------------------------------------
----------------------im_dynamic_segment_loc-------
prompt Adding data to IM_DYNAMIC_SEGMENT_LOC table

INSERT INTO IM_DYNAMIC_SEGMENT_LOC
  (
    DYN_SEG_LOC_ID,
    LOCATION,
    LOC_SEGMENT,
    COMPANY_SEGMENT,
	 CREATED_BY,
	 LAST_UPDATED_BY
  )
SELECT IM_DYN_SEG_LOC_SEQ.NEXTVAL,
       LOCS.LOCATION,
       LOCS.LOC_SEGMENT,
       'DEMO12345678910',
       'REIM_DEMO_USER',
       'REIM_DEMO_USER'
  FROM (SELECT STORE LOCATION,
               ('DEMOS10000000' ||STORE) LOC_SEGMENT
          FROM STORE
        UNION ALL
        SELECT WH LOCATION,
               ('DEMOW10000000' ||WH) LOC_SEGMENT
          FROM WH
         WHERE WH = PHYSICAL_WH) LOCS;
---------------------------------------------------------
----------------------im_dynamic_segment_dept------
prompt Adding data to IM_DYNAMIC_SEGMENT_DEPT table

INSERT
INTO
  IM_DYNAMIC_SEGMENT_DEPT
  (
    DYN_SEG_DEPT_ID,
    DEPT,
    SET_OF_BOOKS_ID,
    DEPT_SEGMENT,    
	 CREATED_BY,
	 LAST_UPDATED_BY
  )
SELECT
  IM_DYN_SEG_DEPT_SEQ.NEXTVAL,
  D.DEPT,
  FGS.SET_OF_BOOKS_ID,
  ('DEMOD33333333' || D.DEPT),  
  'REIM_DEMO_USER',
  'REIM_DEMO_USER'
FROM
  DEPS D, FIF_GL_SETUP FGS;
---------------------------------------------------------
----------------------im_dynamic_segment_class------
prompt Adding data to IM_DYNAMIC_SEGMENT_CLASS table

INSERT
INTO
  IM_DYNAMIC_SEGMENT_CLASS
  (
    DYN_SEG_CLASS_ID,
    DEPT,
    CLASS,
    CLASS_SEGMENT,
    SET_OF_BOOKS_ID,
	 CREATED_BY,
	 LAST_UPDATED_BY
  )
SELECT
  IM_DYN_SEG_CLASS_SEQ.NEXTVAL,
  C.DEPT,
  C.CLASS,
  ('DEMOC44444444' || C.CLASS),
  FGS.SET_OF_BOOKS_ID,
  'REIM_DEMO_USER',
  'REIM_DEMO_USER'
FROM
  CLASS C, FIF_GL_SETUP FGS;
--------------------------------------------
----------------------im_fixed_deal_sob_loc_default------
prompt Adding data to IM_FIXED_DEAL_SOB_LOC_DEFAULT table

INSERT
INTO
  IM_FIXED_DEAL_SOB_LOC_DEFAULT
  (
    SET_OF_BOOKS_ID,
	LOCATION
  )
SELECT
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  MAX(WH.WH)          AS LOCATION
FROM
  FIF_GL_SETUP FGS,
  WH WH
WHERE
  WH.WH = PHYSICAL_WH
GROUP BY
  FGS.SET_OF_BOOKS_ID;
----------------------im_reason_codes-------
prompt Adding data to IM_REASON_CODES table

---POSITIVE COST DISCREPANCY 
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 											AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                         AS REASON_CODE_ID,
  'Pos. Cost Discrep-Price Protection Not Taken' AS REASON_CODE_DESC,
  'RCAS'                                         AS ACTION,
  'N'                                            AS COMMENT_REQUIRED_IND,
  NULL                                           AS HINT_COMMENT,
  'N'                                            AS DELETE_IND,
  'REIM_DEMO_USER'									 AS CREATED_BY,
  'REIM_DEMO_USER'									 AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                            AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                         AS REASON_CODE_ID,
  'Pos. Cost Discrep-Price Drop' AS REASON_CODE_DESC,
  'RCAS'                         AS ACTION,
  'N'                            AS COMMENT_REQUIRED_IND,
  NULL                           AS HINT_COMMENT,
  'N'                            AS DELETE_IND,
  'REIM_DEMO_USER'					 AS CREATED_BY,
  'REIM_DEMO_USER'					 AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                             AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                          AS REASON_CODE_ID,
  'Pos. Cost Discrep-MDF DFI Allowance Not Taken' AS REASON_CODE_DESC,
  'RCA'                                           AS ACTION,
  'N'                                             AS COMMENT_REQUIRED_IND,
  NULL                                            AS HINT_COMMENT,
  'N'                                             AS DELETE_IND,
  'REIM_DEMO_USER'									  AS CREATED_BY,
  'REIM_DEMO_USER'									  AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                             AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                          AS REASON_CODE_ID,
  'Pos. Cost Discrep-Freight Allowance Not Taken' AS REASON_CODE_DESC,
  'RCA'                                           AS ACTION,
  'N'                                             AS COMMENT_REQUIRED_IND,
  NULL                                            AS HINT_COMMENT,
  'N'                                             AS DELETE_IND,
  'REIM_DEMO_USER'									  AS CREATED_BY,
  'REIM_DEMO_USER'									  AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                               AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                            AS REASON_CODE_ID,
  'Pos. Cost Discrep-Defective Allowance Not Taken' AS REASON_CODE_DESC,
  'RCA'                                             AS ACTION,
  'N'                                               AS COMMENT_REQUIRED_IND,
  NULL                                              AS HINT_COMMENT,
  'N'                                               AS DELETE_IND,
  'REIM_DEMO_USER'									 	AS CREATED_BY,
  'REIM_DEMO_USER'									 	AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                          AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                       AS REASON_CODE_ID,
  'Pos. Cost Discrep-DFI1 Allowance Not Taken' AS REASON_CODE_DESC,
  'RCA'                                        AS ACTION,
  'N'                                          AS COMMENT_REQUIRED_IND,
  NULL                                         AS HINT_COMMENT,
  'N'                                          AS DELETE_IND,
  'REIM_DEMO_USER'								   AS CREATED_BY,
  'REIM_DEMO_USER'								   AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                          AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                       AS REASON_CODE_ID,
  'Pos. Cost Discrep-DFI2 Allowance Not Taken' AS REASON_CODE_DESC,
  'RCA'                                        AS ACTION,
  'N'                                          AS COMMENT_REQUIRED_IND,
  NULL                                         AS HINT_COMMENT,
  'N'                                          AS DELETE_IND,
  'REIM_DEMO_USER'								   AS CREATED_BY,
  'REIM_DEMO_USER'								   AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                         AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                      AS REASON_CODE_ID,
  'Pos. Cost Discre-DFI3 Allowance Not Taken' AS REASON_CODE_DESC,
  'RCA'                                       AS ACTION,
  'N'                                         AS COMMENT_REQUIRED_IND,
  NULL                                        AS HINT_COMMENT,
  'N'                                         AS DELETE_IND,
  'REIM_DEMO_USER'								  AS CREATED_BY,
  'REIM_DEMO_USER'								  AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                           AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                        AS REASON_CODE_ID,
  'Order and Matched receipts adjustments only' AS REASON_CODE_DESC,
  'RCAMR'                                       AS ACTION,
  'N'                                           AS COMMENT_REQUIRED_IND,
  NULL                                          AS HINT_COMMENT,
  'N'                                           AS DELETE_IND,
  'REIM_DEMO_USER'									AS CREATED_BY,
  'REIM_DEMO_USER'									AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                                   AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                                AS REASON_CODE_ID,
  'Order and Matched receipts and supplier adjustments' AS REASON_CODE_DESC,
  'RCASMR'                                              AS ACTION,
  'N'                                                   AS COMMENT_REQUIRED_IND,
  NULL 													AS HINT_COMMENT,
  'N'  													AS DELETE_IND,
  'REIM_DEMO_USER'									        AS CREATED_BY,
  'REIM_DEMO_USER'									        AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 											 AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                         AS REASON_CODE_ID,
  'Cost under-bill' 							 AS REASON_CODE_DESC,
  'CMC'                                          AS ACTION,
  'Y'                                            AS COMMENT_REQUIRED_IND,
  NULL                                           AS HINT_COMMENT,
  'N'                                            AS DELETE_IND,
  'REIM_DEMO_USER'									 AS CREATED_BY,
  'REIM_DEMO_USER'									 AS LAST_UPDATED_BY
FROM
  DUAL;

---NEGATIVE COST DISCREPANCY
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                                AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                             AS REASON_CODE_ID,
  'Cost Claim-Price Protection not given on invoice' AS REASON_CODE_DESC,
  'CBC'                                              AS ACTION,
  'N'                                                AS COMMENT_REQUIRED_IND,
  NULL                                               AS HINT_COMMENT,
  'N'                                                AS DELETE_IND,
  'REIM_DEMO_USER'									 	 AS CREATED_BY,
  'REIM_DEMO_USER'									 	 AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                          AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                       AS REASON_CODE_ID,
  'Cost Claim-Price Drop not given on invoice' AS REASON_CODE_DESC,
  'CBC'                                        AS ACTION,
  'N'                                          AS COMMENT_REQUIRED_IND,
  NULL                                         AS HINT_COMMENT,
  'N'                                          AS DELETE_IND,
  'REIM_DEMO_USER'								   AS CREATED_BY,
  'REIM_DEMO_USER'								   AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                             AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                          AS REASON_CODE_ID,
  'Cost Claim-MDF DFI Allow not given on invoice' AS REASON_CODE_DESC,
  'CBC'                                           AS ACTION,
  'N'                                             AS COMMENT_REQUIRED_IND,
  NULL                                            AS HINT_COMMENT,
  'N'                                             AS DELETE_IND,
  'REIM_DEMO_USER'									  AS CREATED_BY,
  'REIM_DEMO_USER'									  AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                             AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                          AS REASON_CODE_ID,
  'Cost Claim-Freight Allow not given on invoice' AS REASON_CODE_DESC,
  'CBC'                                           AS ACTION,
  'N'                                             AS COMMENT_REQUIRED_IND,
  NULL                                            AS HINT_COMMENT,
  'N'                                             AS DELETE_IND,
  'REIM_DEMO_USER'									  AS CREATED_BY,
  'REIM_DEMO_USER'									  AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                               AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                            AS REASON_CODE_ID,
  'Cost Claim-Defective Allow not given on invoice' AS REASON_CODE_DESC,
  'CBC'                                             AS ACTION,
  'N'                                               AS COMMENT_REQUIRED_IND,
  NULL                                              AS HINT_COMMENT,
  'N'                                               AS DELETE_IND,
  'REIM_DEMO_USER'									    AS CREATED_BY,
  'REIM_DEMO_USER'									    AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                          AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                       AS REASON_CODE_ID,
  'Cost Claim-DFI1 Allow not given on invoice' AS REASON_CODE_DESC,
  'CBC'                                        AS ACTION,
  'N'                                          AS COMMENT_REQUIRED_IND,
  NULL                                         AS HINT_COMMENT,
  'N'                                          AS DELETE_IND,
  'REIM_DEMO_USER'								   AS CREATED_BY,
  'REIM_DEMO_USER'								   AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                          AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                       AS REASON_CODE_ID,
  'Cost Claim-DFI2 Allow not given on invoice' AS REASON_CODE_DESC,
  'CBC'                                        AS ACTION,
  'N'                                          AS COMMENT_REQUIRED_IND,
  NULL                                         AS HINT_COMMENT,
  'N'                                          AS DELETE_IND,
  'REIM_DEMO_USER'								   AS CREATED_BY,
  'REIM_DEMO_USER'								   AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C'                                          AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                       AS REASON_CODE_ID,
  'Cost Claim-DFI3 Allow not given on invoice' AS REASON_CODE_DESC,
  'CBC'                                        AS ACTION,
  'N'                                          AS COMMENT_REQUIRED_IND,
  NULL                                         AS HINT_COMMENT,
  'N'                                          AS DELETE_IND,
  'REIM_DEMO_USER'								   AS CREATED_BY,
  'REIM_DEMO_USER'								   AS LAST_UPDATED_BY
FROM
  DUAL;

---QUANTITY OVERAGE DISCREPANCY
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 												AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                            AS REASON_CODE_ID,
  'Qty Over. Discrep-Units removed during Phys Inv' AS REASON_CODE_DESC,
  'DWO'                                             AS ACTION,
  'N'                                               AS COMMENT_REQUIRED_IND,
  NULL                                              AS HINT_COMMENT,
  'N'                                               AS DELETE_IND,
  'REIM_DEMO_USER'								   		AS CREATED_BY,
  'REIM_DEMO_USER'								   		AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 											   AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                           AS REASON_CODE_ID,
  'Qty Over. Discrep-Units removed as a CC to Inv' AS REASON_CODE_DESC,
  'DWO'                                            AS ACTION,
  'N'                                              AS COMMENT_REQUIRED_IND,
  NULL                                             AS HINT_COMMENT,
  'N'                                              AS DELETE_IND,
  'REIM_DEMO_USER'								   	   AS CREATED_BY,
  'REIM_DEMO_USER'								   	   AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 								AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                            AS REASON_CODE_ID,
  'Qty Over. Discrep-Over received' AS REASON_CODE_DESC,
  'RUA'                             AS ACTION,
  'N'                               AS COMMENT_REQUIRED_IND,
  NULL                              AS HINT_COMMENT,
  'N'                               AS DELETE_IND,
  'REIM_DEMO_USER'						AS CREATED_BY,
  'REIM_DEMO_USER'						AS LAST_UPDATED_BY
FROM
  DUAL;

---QUANTITY SHORTAGE DISCREPANCY
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 																AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                                           	AS REASON_CODE_ID,
  'Qty Short Claim-Units added during Phy Inv AS REASON_CODE_DESC' 	AS REASON_CODE_DESC,
  'DWO' 															AS ACTION,
  'N'   															AS COMMENT_REQUIRED_IND,
  NULL  															AS HINT_COMMENT,
  'N'   															AS DELETE_IND,
  'REIM_DEMO_USER'								   	   					AS CREATED_BY,
  'REIM_DEMO_USER'								   	   					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 											AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                       	AS REASON_CODE_ID,
  'Qty Short Claim-Units added as a CC to Inv' 	AS REASON_CODE_DESC,
  'DWO'                                        	AS ACTION,
  'N'                                          	AS COMMENT_REQUIRED_IND,
  NULL                                         	AS HINT_COMMENT,
  'N'                                          	AS DELETE_IND,
  'REIM_DEMO_USER'								   	AS CREATED_BY,
  'REIM_DEMO_USER'								   	AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 													AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                              	AS REASON_CODE_ID,
  'Qty Short Claim-Product not found-Please send POD' 	AS REASON_CODE_DESC,
  'CBQ'                                               	AS ACTION,
  'N'                                                 	AS COMMENT_REQUIRED_IND,
  NULL                                                	AS HINT_COMMENT,
  'N'                                                 	AS DELETE_IND,
  'REIM_DEMO_USER'								   	   		AS CREATED_BY,
  'REIM_DEMO_USER'								   	   		AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 									AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                               	AS REASON_CODE_ID,
  'Qty Short Claim-Concealed Shortage' 	AS REASON_CODE_DESC,
  'CBQ'                                	AS ACTION,
  'N'                                  	AS COMMENT_REQUIRED_IND,
  NULL                                 	AS HINT_COMMENT,
  'N'                                  	AS DELETE_IND,
  'REIM_DEMO_USER'							AS CREATED_BY,
  'REIM_DEMO_USER'							AS LAST_UPDATED_BY
FROM
  DUAL;

---DISPUTED CLAIM REPAYMENT-COST
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 													AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                              	AS REASON_CODE_ID,
  'Repay Cost Claim-Pr Pro. Applied wrong-Adjust WAC' 	AS REASON_CODE_DESC,
  'DWO'                                               	AS ACTION,
  'N'                                                 	AS COMMENT_REQUIRED_IND,
  NULL                                                	AS HINT_COMMENT,
  'N'                                                 	AS DELETE_IND,
  'REIM_DEMO_USER'								   	   		AS CREATED_BY,
  'REIM_DEMO_USER'								   	   		AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 													AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                              	AS REASON_CODE_ID,
  'Repay Cost Claim-Pr Drop Applied wrong-Adjust WAC' 	AS REASON_CODE_DESC,
  'DWO'                                               	AS ACTION,
  'N'                                                 	AS COMMENT_REQUIRED_IND,
  NULL                                                	AS HINT_COMMENT,
  'N'                                                 	AS DELETE_IND,
  'REIM_DEMO_USER'								   	   		AS CREATED_BY,
  'REIM_DEMO_USER'								   	   		AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 													AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                               	AS REASON_CODE_ID,
  'Repay Cost Claim-MDF DFI Allow clmd wrong-R MDF AR' 	AS REASON_CODE_DESC,
  'DWO'                                                	AS ACTION,
  'N'                                                  	AS COMMENT_REQUIRED_IND,
  NULL                                                 	AS HINT_COMMENT,
  'N'                                                  	AS DELETE_IND,
  'REIM_DEMO_USER'								   	   		AS CREATED_BY,
  'REIM_DEMO_USER'								   	   		AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 												AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                            AS REASON_CODE_ID,
  'Repay Cost Claim-Freight Allow clmd wrong-R FAR' AS REASON_CODE_DESC,
  'DWO'                                             AS ACTION,
  'N'                                               AS COMMENT_REQUIRED_IND,
  NULL                                              AS HINT_COMMENT,
  'N'                                               AS DELETE_IND,
  'REIM_DEMO_USER'								   	   	AS CREATED_BY,
  'REIM_DEMO_USER'								   	   	AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 													AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                              	AS REASON_CODE_ID,
  'Repay Cost Claim-Defective Allow clmd wrong-R DAR' 	AS REASON_CODE_DESC,
  'DWO'                                               	AS ACTION,
  'N'                                                 	AS COMMENT_REQUIRED_IND,
  NULL                                                	AS HINT_COMMENT,
  'N'                                                 	AS DELETE_IND,
  'REIM_DEMO_USER'								   	   		AS CREATED_BY,
  'REIM_DEMO_USER'								   	   		AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 													AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                              	AS REASON_CODE_ID,
  'Repay Cost Claim-DFI1 Allow clmd wrong-Adjust WAC' 	AS REASON_CODE_DESC,
  'DWO'                                               	AS ACTION,
  'N'                                                 	AS COMMENT_REQUIRED_IND,
  NULL                                                	AS HINT_COMMENT,
  'N'                                                 	AS DELETE_IND,
  'REIM_DEMO_USER'								   	   		AS CREATED_BY,
  'REIM_DEMO_USER'								   	   		AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 													AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                              	AS REASON_CODE_ID,
  'Repay Cost Claim-DFI2 Allow clmd wrong-Adjust WAC' 	AS REASON_CODE_DESC,
  'DWO'                                               	AS ACTION,
  'N'                                                 	AS COMMENT_REQUIRED_IND,
  NULL                                                	AS HINT_COMMENT,
  'N'                                                 	AS DELETE_IND,
  'REIM_DEMO_USER'								   	   		AS CREATED_BY,
  'REIM_DEMO_USER'								   	   		AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'C' 													AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                              	AS REASON_CODE_ID,
  'Repay Cost Claim-DFI3 Allow clmd wrong-Adjust WAC' 	AS REASON_CODE_DESC,
  'DWO'                                               	AS ACTION,
  'N'                                                 	AS COMMENT_REQUIRED_IND,
  NULL                                                	AS HINT_COMMENT,
  'N'                                                 	AS DELETE_IND,
  'REIM_DEMO_USER'								   	   		AS CREATED_BY,
  'REIM_DEMO_USER'								   	   		AS LAST_UPDATED_BY
FROM
  DUAL;

---DISPUTED DISCREP REPAYMENT-QUANTITY
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 												AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                            AS REASON_CODE_ID,
  'Repay Qty Claim-Valid POD-Units added in PhyInv' AS REASON_CODE_DESC,
  'DWO'                                             AS ACTION,
  'N'                                               AS COMMENT_REQUIRED_IND,
  NULL                                              AS HINT_COMMENT,
  'N'                                               AS DELETE_IND,
  'REIM_DEMO_USER'								   	   	AS CREATED_BY,
  'REIM_DEMO_USER'								   	   	AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 												AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                            AS REASON_CODE_ID,
  'Repay Qty Claim-Valid POD-Units added CC to Inv' AS REASON_CODE_DESC,
  'DWO'                                             AS ACTION,
  'N'                                               AS COMMENT_REQUIRED_IND,
  NULL                                              AS HINT_COMMENT,
  'N'                                               AS DELETE_IND,
  'REIM_DEMO_USER'								   	   	AS CREATED_BY,
  'REIM_DEMO_USER'								   	   	AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE,
    REASON_CODE_ID,
    REASON_CODE_DESC,
    ACTION,
    COMMENT_REQUIRED_IND,
    HINT_COMMENT,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'Q' 											 AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                                         AS REASON_CODE_ID,
  'Qty under-bill' 							 	 AS REASON_CODE_DESC,
  'CMQ'                                          AS ACTION,
  'Y'                                            AS COMMENT_REQUIRED_IND,
  NULL                                           AS HINT_COMMENT,
  'N'                                            AS DELETE_IND,
  'REIM_DEMO_USER'								   	 AS CREATED_BY,
  'REIM_DEMO_USER'								   	 AS LAST_UPDATED_BY
FROM
  DUAL;

--Tax discrepancies resolutions
INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE ,
    REASON_CODE_ID ,
    REASON_CODE_DESC ,
    ACTION ,
    COMMENT_REQUIRED_IND ,
    HINT_COMMENT ,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'T' 							  AS REASON_CODE_TYPE,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                          AS REASON_CODE_ID,
  'Invoice item tax is incorrect' AS REASON_CODE_DESC ,
  'DMTI'                          AS ACTION ,
  'N'                             AS COMMENT_REQUIRED_IND,
  NULL                            AS HINT_COMMENT ,
  'N'                             AS DELETE_IND,
  'REIM_DEMO_USER'					  AS CREATED_BY,
  'REIM_DEMO_USER'					  AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE ,
    REASON_CODE_ID ,
    REASON_CODE_DESC ,
    ACTION ,
    COMMENT_REQUIRED_IND ,
    HINT_COMMENT ,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'T' 							AS REASON_CODE_TYPE ,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                        AS REASON_CODE_ID,
  'Invoice taxes are incorrect' AS REASON_CODE_DESC ,
  'DMTF'                        AS ACTION ,
  'N'                           AS COMMENT_REQUIRED_IND,
  NULL                          AS HINT_COMMENT,
  'N'                           AS DELETE_IND,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  dual;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE ,
    REASON_CODE_ID ,
    REASON_CODE_DESC ,
    ACTION ,
    COMMENT_REQUIRED_IND ,
    HINT_COMMENT ,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'CNT' 					 AS REASON_CODE_TYPE ,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                     AS REASON_CODE_ID,
  'CN item tax is incorrect' AS REASON_CODE_DESC ,
  'CNRTI'                    AS ACTION ,
  'N'                        AS COMMENT_REQUIRED_IND,
  NULL                       AS HINT_COMMENT,
  'N'                        AS DELETE_IND,
  'REIM_DEMO_USER'				 AS CREATED_BY,
  'REIM_DEMO_USER'				 AS LAST_UPDATED_BY
FROM
  dual;

INSERT
INTO
  IM_REASON_CODES
  (
    REASON_CODE_TYPE ,
    REASON_CODE_ID ,
    REASON_CODE_DESC ,
    ACTION ,
    COMMENT_REQUIRED_IND ,
    HINT_COMMENT ,
    DELETE_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  'CNT' 				   AS REASON_CODE_TYPE ,
  NVL(
  (
    SELECT
      MAX(to_number(REASON_CODE_ID))+5
    FROM
      im_reason_codes
  )
  , 100)                   AS REASON_CODE_ID,
  'CN taxes are incorrect' AS REASON_CODE_DESC ,
  'CNRTF'                  AS ACTION ,
  'N'                      AS COMMENT_REQUIRED_IND,
  NULL                     AS HINT_COMMENT,
  'N'                      AS DELETE_IND,
  'REIM_DEMO_USER'			   AS CREATED_BY,
  'REIM_DEMO_USER'			   AS LAST_UPDATED_BY
FROM
  dual; 
-------------------------------------------------------------------------
----------------------im_sec_grp_reason_code-----------------------------
prompt Adding data to IM_SEC_GRP_REASON_CODE table
	
INSERT
INTO
  IM_SEC_GRP_REASON_CODE
SELECT
  SG.GROUP_ID,
  IRC.reason_code_id,
  IM_SEC_GRP_RC_SEQ.NEXTVAL
FROM
  im_reason_codes IRC,
  SEC_GROUP SG
WHERE
  IRC.delete_ind  ='N'
AND SG.GROUP_NAME = 'REIM_GROUP';
-------------------------------------------------------------------------   
---------------------------im_match_stgy_head----------------------------
prompt Adding data to IM_MATCH_STGY_HEAD table

INSERT
INTO
  IM_MATCH_STGY_HEAD
  (
    MATCH_STGY_ID ,
    DESCRIPTION ,
    SYSTEM_DEFAULT,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_MATCH_STGY_HEAD_SEQ.NEXTVAL            AS MATCH_STGY_ID ,
  'Default system strategy - regular match' AS DESCRIPTION ,
  'Y'                                       AS SYSTEM_DEFAULT,
  'REIM_DEMO_USER'			   					AS CREATED_BY,
  'REIM_DEMO_USER'			   					AS LAST_UPDATED_BY
FROM
  dual;
  
INSERT
INTO
  IM_MATCH_STGY_HEAD
  (
    MATCH_STGY_ID ,
    DESCRIPTION ,
    SYSTEM_DEFAULT,
    CREATED_BY,
    LAST_UPDATED_BY
  )
SELECT
  IM_MATCH_STGY_HEAD_SEQ.NEXTVAL             AS MATCH_STGY_ID ,
  'Non-default system strategy - best match' AS DESCRIPTION ,
  'N'                                        AS SYSTEM_DEFAULT,
  'REIM_DEMO_USER'                                AS CREATED_BY,
  'REIM_DEMO_USER'                                AS LAST_UPDATED_BY
FROM
  dual;
---------------------------------------------------------------------------
---------------------------im_match_stgy_detail----------------------------
prompt Adding data to IM_MATCH_STGY_DETAIL table

INSERT
INTO
  IM_MATCH_STGY_DETAIL
  (
    MATCH_STGY_DTL_ID ,
    MATCH_STGY_ID ,
    MATCH_STGY_RANK ,
    MATCH_LEVEL ,
    MATCH_TYPE,
    CREATED_BY,
    LAST_UPDATED_BY
  )
SELECT
  IM_MATCH_STGY_DETAIL_SEQ.NEXTVAL 	AS MATCH_STGY_DTL_ID ,
  (
    SELECT
      MATCH_STGY_ID					AS MATCH_STGY_ID
    FROM
      IM_MATCH_STGY_HEAD
    WHERE
      SYSTEM_DEFAULT = 'Y'
  )           						AS MATCH_STGY_ID ,
  1           						AS MATCH_STGY_RANK ,
  'SA'        						AS MATCH_LEVEL ,
  'R'         						AS MATCH_TYPE,
  'REIM_DEMO_USER' 						AS CREATED_BY,
  'REIM_DEMO_USER' 						AS LAST_UPDATED_BY
FROM
  dual;

INSERT
INTO
  IM_MATCH_STGY_DETAIL
  (
    MATCH_STGY_DTL_ID ,
    MATCH_STGY_ID ,
    MATCH_STGY_RANK ,
    MATCH_LEVEL ,
    MATCH_TYPE,
	CREATED_BY,
    LAST_UPDATED_BY
  )
SELECT
  IM_MATCH_STGY_DETAIL_SEQ.NEXTVAL 	AS MATCH_STGY_DTL_ID ,
  (
    SELECT
      MATCH_STGY_ID					AS MATCH_STGY_ID
    FROM
      IM_MATCH_STGY_HEAD
    WHERE
      SYSTEM_DEFAULT = 'Y'
  )    								AS MATCH_STGY_ID ,
  2    								AS MATCH_STGY_RANK ,
  'SO' 								AS MATCH_LEVEL ,
  'R'  								AS MATCH_TYPE,
  'REIM_DEMO_USER' 						AS CREATED_BY,
  'REIM_DEMO_USER' 						AS LAST_UPDATED_BY
FROM
  dual;

INSERT
INTO
  IM_MATCH_STGY_DETAIL
  (
    MATCH_STGY_DTL_ID ,
    MATCH_STGY_ID ,
    MATCH_STGY_RANK ,
    MATCH_LEVEL ,
    MATCH_TYPE,
	CREATED_BY,
    LAST_UPDATED_BY
  )
SELECT
  IM_MATCH_STGY_DETAIL_SEQ.NEXTVAL 	AS MATCH_STGY_DTL_ID ,
  (
    SELECT
      MATCH_STGY_ID					AS MATCH_STGY_ID
    FROM
      IM_MATCH_STGY_HEAD
    WHERE
      SYSTEM_DEFAULT = 'Y'
  )   								AS MATCH_STGY_ID ,
  3   								AS MATCH_STGY_RANK ,
  'D' 								AS MATCH_LEVEL ,
  'R' 								AS MATCH_TYPE,
  'REIM_DEMO_USER' 						AS CREATED_BY,
  'REIM_DEMO_USER' 						AS LAST_UPDATED_BY
FROM
  dual;

INSERT
INTO
  IM_MATCH_STGY_DETAIL
  (
    MATCH_STGY_DTL_ID ,
    MATCH_STGY_ID ,
    MATCH_STGY_RANK ,
    MATCH_LEVEL ,
    MATCH_TYPE,
	CREATED_BY,
    LAST_UPDATED_BY
  )
SELECT
  IM_MATCH_STGY_DETAIL_SEQ.NEXTVAL AS MATCH_STGY_DTL_ID ,
  (
    SELECT
      MATCH_STGY_ID					AS MATCH_STGY_ID
    FROM
      IM_MATCH_STGY_HEAD
    WHERE
      SYSTEM_DEFAULT = 'N'
  )    								AS MATCH_STGY_ID ,
  1    								AS MATCH_STGY_RANK ,
  'SA' 								AS MATCH_LEVEL ,
  'R'  								AS MATCH_TYPE,
  'REIM_DEMO_USER' 						AS CREATED_BY,
  'REIM_DEMO_USER' 						AS LAST_UPDATED_BY
FROM
  dual;

INSERT
INTO
  IM_MATCH_STGY_DETAIL
  (
    MATCH_STGY_DTL_ID ,
    MATCH_STGY_ID ,
    MATCH_STGY_RANK ,
    MATCH_LEVEL ,
    MATCH_TYPE,
	CREATED_BY,
    LAST_UPDATED_BY
  )
SELECT
  IM_MATCH_STGY_DETAIL_SEQ.NEXTVAL 	AS MATCH_STGY_DTL_ID ,
  (
    SELECT
      MATCH_STGY_ID					AS MATCH_STGY_ID
    FROM
      IM_MATCH_STGY_HEAD
    WHERE
      SYSTEM_DEFAULT = 'N'
  )    								AS MATCH_STGY_ID ,
  2    								AS MATCH_STGY_RANK ,
  'SO' 								AS MATCH_LEVEL ,
  'B'  								AS MATCH_TYPE,
  'REIM_DEMO_USER' 						AS CREATED_BY,
  'REIM_DEMO_USER' 						AS LAST_UPDATED_BY
FROM
  dual;

INSERT
INTO
  IM_MATCH_STGY_DETAIL
  (
    MATCH_STGY_DTL_ID ,
    MATCH_STGY_ID ,
    MATCH_STGY_RANK ,
    MATCH_LEVEL ,
    MATCH_TYPE,
	CREATED_BY,
    LAST_UPDATED_BY
  )
SELECT
  IM_MATCH_STGY_DETAIL_SEQ.NEXTVAL 	AS MATCH_STGY_DTL_ID ,
  (
    SELECT
      MATCH_STGY_ID					AS MATCH_STGY_ID
    FROM
      IM_MATCH_STGY_HEAD
    WHERE
      SYSTEM_DEFAULT = 'N'
  )   								AS MATCH_STGY_ID ,
  3   								AS MATCH_STGY_RANK ,
  'D' 								AS MATCH_LEVEL ,
  'B' 								AS MATCH_TYPE,
  'REIM_DEMO_USER' 						AS CREATED_BY,
  'REIM_DEMO_USER' 						AS LAST_UPDATED_BY
FROM
  dual;
------------------------------------------------------------------------
---------------------------im_system_options----------------------------
prompt Adding data to IM_SYSTEM_OPTIONS table
 
INSERT INTO IM_SYSTEM_OPTIONS ( DEBIT_MEMO_SEND_DAYS
							  , CLOSE_OPEN_RECEIPT_DAYS
							  , RESOLUTION_DUE_DAYS
							  , DOC_HIST_DAYS
							  , DEBIT_MEMO_PREFIX_COST
							  , DEBIT_MEMO_PREFIX_QTY
							  , DEBIT_MEMO_PREFIX_TAX
							  , CREDIT_MEMO_PREFIX_COST
							  , CREDIT_MEMO_PREFIX_QTY
							  , CREDIT_NOTE_REQ_PREFIX_COST
							  , CREDIT_NOTE_REQ_PREFIX_QTY
							  , CREDIT_NOTE_REQ_PREFIX_TAX
							  , POST_DATED_DOC_DAYS
							  , MAX_TOLERANCE_PCT
							  , DAYS_BEFORE_DUE_DATE
							  , DFLT_PAY_NOW_TERMS_DEALS
							  , CALC_TOLERANCE
							  , TAX_VALIDATION_TYPE
							  , TAX_DOCUMENT_CREATION_LVL
							  , DFLT_HEADER_TAX
							  , TAX_RESOLUTION_DUE_DAYS
							  , CALC_TOLERANCE_IND
							  , RECEIPT_WRITE_OFF_DAYS
							  , DFLT_COST_OVERBILL_RC
							  , DFLT_QTY_OVERBILL_RC
							  , DFLT_COST_UNDERBILL_RC
							  , DFLT_QTY_UNDERBILL_RC
							  , NUM_TAX_ALLOW
							  , BALANCING_TOLERANCE
							  , POST_ON_DOC_HEADER
							  , PRORATE_ACROSS_TAX_CODES			      
							  , LUW_AUTO_MATCH
							  , DELAY_LN_MTCH_UNTL_ROUTE_DATE
							  , DFLT_PAY_NOW_TERMS_RTV
							  , TOTAL_QTY_REQUIRED
							  , DFLT_NON_MERCH_LOCATION
							  , DFLT_DEPT
							  , DFLT_CLASS
							  , GL_MAX_SEGMENTS
							  , DFLT_SOB
							  , DEAL_DOCS_PURGE_DAYS
							  , DOC_QTY_DECIMALS
							  , BATCH_DATE_FORMAT
							  , WS_FIN_ACC_VALID_URL
							  , WS_FIN_ACC_VALID_CRED
							  , WS_FIN_DRILL_FWD_URL
							  , WS_FIN_DRILL_FWD_CRED
							  , INCL_DOC_DATE_FOR_DUPL_CHECK
							  , INCL_DOC_YEAR_FOR_DUPL_CHECK
							  , POST_INTRA_REGION_TAX_IND
							  , OBIEE_ENABLED
							  , CREATED_BY
							  ,	LAST_UPDATED_BY)
select 	 1 		as DEBIT_MEMO_SEND_DAYS
       , 20 	as CLOSE_OPEN_RECEIPT_DAYS
	   , 5		as RESOLUTION_DUE_DAYS
       , 20 	as DOC_HIST_DAYS
       , 'DMC' 	as DEBIT_MEMO_PREFIX_COST
       , 'DMQ'	as DEBIT_MEMO_PREFIX_QTY
       , 'DMT'	as DEBIT_MEMO_PREFIX_TAX
       , 'CMC'	as CREDIT_MEMO_PREFIX_COST
       , 'CMQ'	as CREDIT_MEMO_PREFIX_QTY
       , 'CNC'	as CREDIT_NOTE_REQ_PREFIX_COST
       , 'CNQ'	as CREDIT_NOTE_REQ_PREFIX_QTY
       , 'CNT'	as CREDIT_NOTE_REQ_PREFIX_TAX
       , 365	as POST_DATED_DOC_DAYS
       , 10		as MAX_TOLERANCE_PCT
       , 10		as DAYS_BEFORE_DUE_DATE
       , (SELECT TERMS 
			FROM TERMS 
		   WHERE (START_DATE_ACTIVE IS NULL 
					OR 
				  START_DATE_ACTIVE > GET_VDATE ) 
			 AND END_DATE_ACTIVE IS NULL 
			 AND ENABLED_FLAG = 'Y' 
			 AND ROWNUM < 2)		
				as DFLT_PAY_NOW_TERMS_DEALS
       , 0.5 	as CALC_TOLERANCE
       , 'RECON'as TAX_VALIDATION_TYPE
       , 'ITEM' as TAX_DOCUMENT_CREATION_LVL
       , 'Y'	as DFLT_HEADER_TAX
       , 0		as TAX_RESOLUTION_DUE_DAYS
       , 'A'	as CALC_TOLERANCE_IND
       , 0		as RECEIPT_WRITE_OFF_DAYS
       , NULL	as DFLT_COST_OVERBILL_RC
       , NULL	as DFLT_QTY_OVERBILL_RC
       , NULL	as DFLT_COST_UNDERBILL_RC
       , NULL	as DFLT_QTY_UNDERBILL_RC
       , 'S'	as NUM_TAX_ALLOW
       , 1		as BALANCING_TOLERANCE
       , 'Y'	as POST_ON_DOC_HEADER
       , 'N'	as PRORATE_ACROSS_TAX_CODES
       , 10000	as LUW_AUTO_MATCH
       , 'Y'	as DELAY_LN_MTCH_UNTL_ROUTE_DATE
       , (SELECT TERMS 
			FROM TERMS 
		   WHERE (START_DATE_ACTIVE IS NULL 
					OR 
				  START_DATE_ACTIVE > GET_VDATE ) 
			 AND END_DATE_ACTIVE IS NULL 
			 AND ENABLED_FLAG = 'Y' 
			 AND ROWNUM < 2)		
				as DFLT_PAY_NOW_TERMS_RTV
       , 'N'	as TOTAL_QTY_REQUIRED
       , (select store 
		    from store 
		   where rownum < 2)		
				as DFLT_NON_MERCH_LOCATION
       , (select max(dept) dept 
	        from class )		
		       as DFLT_DEPT
       , (select max(c.class) class
		    from class c
			   , (select max(dept) dept
					from class ) d
		   where d.dept = c.dept) 
				as DFLT_CLASS
       , 20		as GL_MAX_SEGMENTS
       , (select NVL((SELECT MIN(FGS.SET_OF_BOOKS_ID) FROM FIF_GL_SETUP FGS), -1) from dual) 
				as DFLT_SOB
       , 90 	as DEAL_DOCS_PURGE_DAYS
       , 1 		as DOC_QTY_DECIMALS
       , 'yyyyMMddHHmmss' 
				as BATCH_DATE_FORMAT
       , NULL 	as WS_FIN_ACC_VALID_URL
       , NULL 	as WS_FIN_ACC_VALID_CRED
       , NULL 	as WS_FIN_DRILL_FWD_URL
       , NULL 	as WS_FIN_DRILL_FWD_CRED
       , 'N' 	as INCL_DOC_DATE_FOR_DUPL_CHECK
       , 'N' 	as INCL_DOC_YEAR_FOR_DUPL_CHECK
       , 'N' 	as POST_INTRA_REGION_TAX_IND
	   , 'N'    as OBIEE_ENABLED
	   , 'REIM_DEMO_USER' AS CREATED_BY
	   , 'REIM_DEMO_USER' AS LAST_UPDATED_BY
from dual;
-----------------------------------------------------------
----------------------im_system_options--------------------
prompt Adding data to IM_OI_SYS_OPTIONS table

INSERT
INTO
  IM_OI_SYS_OPTIONS
  (
    WEIGHT_EXACT_MATCH,
    WEIGHT_NON_DISCREPANCY,
    WEIGHT_TIMELY_SHIP
  )
SELECT
  30 AS WEIGHT_EXACT_MATCH,
  50 AS WEIGHT_NON_DISCREPANCY,
  20 AS WEIGHT_TIMELY_SHIP
FROM
  DUAL;
-------------------------------------------------
---------------------------im_supplier_options---
prompt Adding data to im_supplier_options tab

INSERT
INTO
  IM_SUPPLIER_OPTIONS
  (
    SUPPLIER,
    SEND_DEBIT_MEMO,
    MANUALLY_PAID_IND,
    USE_INVOICE_TERMS_IND,
    ROG_DATE_ALLOWED_IND,
    AP_REVIEWER,
    CLOSE_OPEN_SHIPMENT_DAYS,
    QTY_DISC_DAY_BEFORE_RTE,
    HOLD_INVOICES,
    MATCH_KEY,
    MATCH_STGY_ID,
    SKU_COMP_PERCENT,
    MATCH_TOTAL_QTY_IND,
    TOTAL_QTY_REQUIRED_IND,
	ONLINE_PARENT_MATCH_IND,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  s.supplier 					AS SUPPLIER,
  'A'        					AS SEND_DEBIT_MEMO,
  'N'        					AS MANUALLY_PAID_IND,
  'N'        					AS USE_INVOICE_TERMS_IND,
  'Y'        					AS ROG_DATE_ALLOWED_IND,
  'FRAN_MORETTI'      			AS AP_REVIEWER,
  60         					AS CLOSE_OPEN_SHIPMENT_DAYS,
  0          					AS QTY_DISC_DAY_BEFORE_RTE,
  'N'        					AS HOLD_INVOICES,
  'POL'      					AS MATCH_KEY,
  (
    SELECT
      MATCH_STGY_ID
    FROM
      IM_MATCH_STGY_HEAD
    WHERE
      SYSTEM_DEFAULT = 'Y'
  )   							AS MATCH_STGY_ID,
  2   							AS SKU_COMP_PERCENT,
  'N' 							AS MATCH_TOTAL_QTY_IND,
  'N' 							AS TOTAL_QTY_REQUIRED_IND,
  'N'							AS ONLINE_PARENT_MATCH_IND,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  SUPS S
WHERE
  s.SUPPLIER_PARENT IS NULL;
------------------------------------------------
---------------------------im_tolerance_head----
prompt Adding data to IM_TOLERANCE_HEAD table

INSERT
INTO
  IM_TOLERANCE_HEAD
  (
    TOLERANCE_ID,
    DESCRIPTION,
    CURRENCY_CODE,
    SYSTEM_DEFAULT,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_HEAD_SEQ.NEXTVAL AS TOLERANCE_ID ,
  'Tolerance Default'           AS DESCRIPTION ,
  'USD'                         AS CURRENCY_CODE ,
  'Y'                           AS SYSTEM_DEFAULT,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_TOLERANCE_HEAD
  (
    TOLERANCE_ID,
    DESCRIPTION,
    CURRENCY_CODE,
    SYSTEM_DEFAULT,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_HEAD_SEQ.NEXTVAL AS TOLERANCE_ID ,
  'Tolerance Non-Default'       AS DESCRIPTION ,
  'USD'                         AS CURRENCY_CODE ,
  'N'                           AS SYSTEM_DEFAULT,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

------------------------------------------------
---------------------------im_tolerance_detail----
prompt Adding data to IM_TOLERANCE_DETAIL table  

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Default'
  )          					AS TOLERANCE_ID ,
  'S'        					AS MATCH_LEVEL ,
  'C'        					AS MATCH_TYPE ,
  'R'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  8          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Default'
  )          					AS TOLERANCE_ID ,
  'S'        					AS MATCH_LEVEL ,
  'C'        					AS MATCH_TYPE ,
  'S'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  7          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Default'
  )          					AS TOLERANCE_ID ,
  'S'        					AS MATCH_LEVEL ,
  'Q'        					AS MATCH_TYPE ,
  'R'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  8          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,	
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Default'
  )          					AS TOLERANCE_ID ,
  'S'        					AS MATCH_LEVEL ,
  'Q'        					AS MATCH_TYPE ,
  'S'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  7          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Default'
  )          					AS TOLERANCE_ID ,
  'L'        					AS MATCH_LEVEL ,
  'C'        					AS MATCH_TYPE ,
  'R'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  5          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Default'
  )          					AS TOLERANCE_ID ,
  'L'        					AS MATCH_LEVEL ,
  'C'        					AS MATCH_TYPE ,
  'S'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  5          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Default'
  )          					AS TOLERANCE_ID ,
  'L'        					AS MATCH_LEVEL ,
  'Q'        					AS MATCH_TYPE ,
  'R'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  5          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Default'
  )          					AS TOLERANCE_ID ,
  'L'        					AS MATCH_LEVEL ,
  'Q'        					AS MATCH_TYPE ,
  'S'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  5          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  )          					AS TOLERANCE_ID ,
  'S'        					AS MATCH_LEVEL ,
  'C'        					AS MATCH_TYPE ,
  'R'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  5          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  )          					AS TOLERANCE_ID ,
  'S'        					AS MATCH_LEVEL ,
  'C'        					AS MATCH_TYPE ,
  'S'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  4          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  )          					AS TOLERANCE_ID ,
  'S'        					AS MATCH_LEVEL ,
  'Q'        					AS MATCH_TYPE ,
  'R'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  5          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,	
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  )          					AS TOLERANCE_ID ,
  'S'        					AS MATCH_LEVEL ,
  'Q'        					AS MATCH_TYPE ,
  'S'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  4          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  )          					AS TOLERANCE_ID ,
  'L'        					AS MATCH_LEVEL ,
  'C'        					AS MATCH_TYPE ,
  'R'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  3          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  )          					AS TOLERANCE_ID ,
  'L'        					AS MATCH_LEVEL ,
  'C'        					AS MATCH_TYPE ,
  'S'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  3          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;

INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  )          					AS TOLERANCE_ID ,
  'L'        					AS MATCH_LEVEL ,
  'Q'        					AS MATCH_TYPE ,
  'R'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  3          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;
  
INSERT
INTO
  IM_TOLERANCE_DETAIL
  (
    TOLERANCE_DETAIL_ID,
    TOLERANCE_ID,
    MATCH_LEVEL,
    MATCH_TYPE,
    FAVOR_OF,
    LOWER_LIMIT,
    UPPER_LIMIT,
    TOLERANCE_VALUE_TYPE,
    TOLERANCE_VALUE,
    AUTO_RES_VALUE,
    REASON_CODE_ID,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_DETAIL_SEQ.NEXTVAL AS TOLERANCE_DETAIL_ID ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  )          					AS TOLERANCE_ID ,
  'L'        					AS MATCH_LEVEL ,
  'Q'        					AS MATCH_TYPE ,
  'S'        					AS FAVOR_OF ,
  0          					AS LOWER_LIMIT ,
  1000000000 					AS UPPER_LIMIT ,
  'P'        					AS TOLERANCE_VALUE_TYPE ,
  3          					AS TOLERANCE_VALUE ,
  NULL       					AS AUTO_RES_LIMIT ,
  NULL       					AS REASON_CODE_ID,
  'REIM_DEMO_USER'					AS CREATED_BY,
  'REIM_DEMO_USER'					AS LAST_UPDATED_BY
FROM
  DUAL;
-------------------------------------------------------------------------
--------------------------im_tolerance_level_map----------------------------------
prompt Adding data to IM_TOLERANCE_LEVEL_MAP table
  
INSERT
INTO
  IM_TOLERANCE_LEVEL_MAP
  (
    TOLERANCE_LEVEL_MAP_ID,
    TOLERANCE_LEVEL_TYPE,
    TOLERANCE_LEVEL_ID,
    TOLERANCE_ID,
    CREATED_BY,
    LAST_UPDATED_BY
  )
SELECT
  IM_TOLERANCE_LEVEL_MAP_SEQ.NEXTVAL AS TOLERANCE_LEVEL_MAP_ID ,
  'S'                                AS TOLERANCE_LEVEL_TYPE ,
  S.SUPPLIER                         AS TOLERANCE_LEVEL_ID ,
  T.TOLERANCE_ID                     AS TOLERANCE_ID,
  'REIM_DEMO_USER'                        AS CREATED_BY,
  'REIM_DEMO_USER'                        AS LAST_UPDATED_BY
FROM
  SUPS S ,
  (
    SELECT
      TOLERANCE_ID
    FROM
      IM_TOLERANCE_HEAD
    WHERE
      DESCRIPTION = 'Tolerance Non-Default'
  ) T
WHERE
  SUPPLIER_PARENT IS NULL;
-------------------------------------------------------------------------
--------------------------IM_GL_OPTIONS----------------------------------
prompt Adding data to IM_GL_OPTIONS table

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL
  )
SELECT
  '1'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Company'           AS SEGMENT_LABEL
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL, 
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '2'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Location'          AS SEGMENT_LABEL,
  'Location'		  AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL,
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '3'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Account'           AS SEGMENT_LABEL,
  'Account'		  	  AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL,
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '4'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Department'        AS SEGMENT_LABEL,
  'Department'		  AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL,
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '5'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Class'             AS SEGMENT_LABEL,
  'Class'		  	  AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL,
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '6'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Affliate'          AS SEGMENT_LABEL,
  'Affliate'		  AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL,
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '7'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Future1'           AS SEGMENT_LABEL,
  'Future1'		  	  AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL,
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '8'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Future2'           AS SEGMENT_LABEL,
  'Future2'		  	  AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL,
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '9'                 AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Future3'           AS SEGMENT_LABEL,
  'Future3'		      AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;

INSERT
INTO
  IM_GL_OPTIONS
  (
    SEGMENT_NO,
    DYNAMIC_IND,
    SET_OF_BOOKS_ID,
    SEGMENT_LABEL,
	BUSINESS_ATTRIBUTE,
	CREATED_BY,
	LAST_UPDATED_BY
  )
SELECT
  '10'                AS SEGMENT_NO,
  'N'                 AS DYNAMIC_IND,
  FGS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
  'Future4'           AS SEGMENT_LABEL,
  'Future4'		      AS BUSINESS_ATTRIBUTE,
  'REIM_DEMO_USER'		  AS CREATED_BY,
  'REIM_DEMO_USER'		  AS LAST_UPDATED_BY
FROM
  FIF_GL_SETUP FGS;
-------------------------------------------------------------------------
--------------------------IM_GL_CROSS_REF----------------------------------
prompt Adding data to IM_GL_CROSS_REF table

INSERT
INTO
  IM_GL_CROSS_REF
  (
    GL_CROSS_REF_ID,
    ACCOUNT_TYPE,
    ACCOUNT_CODE,
    SET_OF_BOOKS_ID,
    TAX_CODE,
    CREATED_BY,
    LAST_UPDATED_BY,
    SEGMENT1,
    SEGMENT2,
    SEGMENT3,
    SEGMENT4,
    SEGMENT5,
    SEGMENT6,
    SEGMENT7,
    SEGMENT8,
    SEGMENT9,
    SEGMENT10
  )
SELECT IM_GL_CROSS_REF_SEQ.NEXTVAL,
       BT.TRAN_TYPE,
       BT.TRAN_CODE,
       FGS.SET_OF_BOOKS_ID,
       NULL,
       'REIM_DEMO_USER',
       'REIM_DEMO_USER',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG1',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG2',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG3',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG4',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG5',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG6',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG7',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG8',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG9',
       BT.TRAN_TYPE || BT.TRAN_CODE ||'SEG10'
  FROM FIF_GL_SETUP FGS,
       (SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAP' TRAN_CODE, 'Trade Accounts Payable' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAPNDI' TRAN_CODE, 'Trade Accounts Payable - Non Dynamic' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'DIRAR' TRAN_CODE, 'Deal Income Receivable - Complex' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'DIRAF' TRAN_CODE, 'Deal Income Receivable - Fixed' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'UNR' TRAN_CODE, 'Unmatched Receipt' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'VWT' TRAN_CODE, 'Variance Within Tolerance' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'PPA' TRAN_CODE, 'Prepaid Asset' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'CRN' TRAN_CODE, 'Credit Note' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'CRNNDI' TRAN_CODE, 'Credit Note - Non Dynamic' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'RWO' TRAN_CODE, 'Receipt Write Off' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAX' TRAN_CODE, 'Tax' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXNDI' TRAN_CODE, 'Tax - Non Dynamic' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXACQ' TRAN_CODE, 'Acquisition Tax' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXACO' TRAN_CODE, 'Acquisition Tax Offset' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXRVC' TRAN_CODE, 'Reverse Charge Vat' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXRVO' TRAN_CODE, 'Reverse Charge Vat Offset' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'VCT' TRAN_CODE, 'Variance Calc Tolerance' TRAN_CODE_DESCRIPTION FROM DUAL
	      UNION ALL
	      SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'VCTT' TRAN_CODE, 'Variance Calc Tax Tolerance' TRAN_CODE_DESCRIPTION FROM DUAL) BT;

INSERT
INTO
  IM_GL_CROSS_REF
  (
    GL_CROSS_REF_ID,
    ACCOUNT_TYPE,
    ACCOUNT_CODE,
    SET_OF_BOOKS_ID,
    TAX_CODE,
    CREATED_BY,
    LAST_UPDATED_BY,
    SEGMENT1,
    SEGMENT2,
    SEGMENT3,
    SEGMENT4,
    SEGMENT5,
    SEGMENT6,
    SEGMENT7,
    SEGMENT8,
    SEGMENT9,
    SEGMENT10
  )
SELECT IM_GL_CROSS_REF_SEQ.NEXTVAL,
       'NMC',
       NMCH.NON_MERCH_CODE,
       FGS.SET_OF_BOOKS_ID,
       NULL,
       'REIM_DEMO_USER',
       'REIM_DEMO_USER',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG1',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG2',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG3',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG4',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG5',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG6',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG7',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG8',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG9',
       'NMC' ||NMCH.NON_MERCH_CODE ||'SEG10'
  FROM FIF_GL_SETUP FGS,
       NON_MERCH_CODE_HEAD NMCH;


INSERT
INTO
  IM_GL_CROSS_REF
  (
    GL_CROSS_REF_ID,
    ACCOUNT_TYPE,
    ACCOUNT_CODE,
    SET_OF_BOOKS_ID,
    TAX_CODE,
    CREATED_BY,
    LAST_UPDATED_BY,
    SEGMENT1,
    SEGMENT2,
    SEGMENT3,
    SEGMENT4,
    SEGMENT5,
    SEGMENT6,
    SEGMENT7,
    SEGMENT8,
    SEGMENT9,
    SEGMENT10
  )
SELECT IM_GL_CROSS_REF_SEQ.NEXTVAL,
       'RCA',
       IRC.REASON_CODE_ID,
       FGS.SET_OF_BOOKS_ID,
       NULL,
       'REIM_DEMO_USER',
       'REIM_DEMO_USER',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG1',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG2',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG3',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG4',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG5',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG6',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG7',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG8',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG9',
       'RCA' ||IRC.REASON_CODE_ID ||'SEG10'
  FROM FIF_GL_SETUP FGS,
       IM_REASON_CODES IRC;

-------------------------------------------------------------------------
--------------------------IM_TRAN_CODE-----------------------------------

 prompt Adding Basic Transaction data to IM_TRAN_CODE table
 INSERT INTO IM_TRAN_CODE(TRAN_ID,
                          TRAN_TYPE,
                          TRAN_TYPE_DESCRIPTION,
                          TRAN_CODE,
                          TRAN_CODE_DESCRIPTION,
                          CREATED_BY,
                          LAST_UPDATED_BY)
 SELECT IM_TRAN_ID_SEQ.NEXTVAL TRAN_ID, 
        TRAN_TYPE,
		  TRAN_TYPE_DESCRIPTION,
        TRAN_CODE,
        TRAN_CODE_DESCRIPTION,
        'REIM_DEMO_USER',
        'REIM_DEMO_USER'
 FROM
    (SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAP' TRAN_CODE, 'Trade Accounts Payable' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAPNDI' TRAN_CODE, 'Trade Accounts Payable - Non Dynamic' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'DIRAR' TRAN_CODE, 'Deal Income Receivable - Complex' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'DIRAF' TRAN_CODE, 'Deal Income Receivable - Fixed' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'UNR' TRAN_CODE, 'Unmatched Receipt' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'VWT' TRAN_CODE, 'Variance Within Tolerance' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'PPA' TRAN_CODE, 'Prepaid Asset' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'CRN' TRAN_CODE, 'Credit Note' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'CRNNDI' TRAN_CODE, 'Credit Note - Non Dynamic' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'RWO' TRAN_CODE, 'Receipt Write Off' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAX' TRAN_CODE, 'Tax' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXNDI' TRAN_CODE, 'Tax - Non Dynamic' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXACQ' TRAN_CODE, 'Acquisition Tax' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXACO' TRAN_CODE, 'Acquisition Tax Offset' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXRVC' TRAN_CODE, 'Reverse Charge Vat' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'TAXRVO' TRAN_CODE, 'Reverse Charge Vat Offset' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'VCT' TRAN_CODE, 'Variance Calc Tolerance' TRAN_CODE_DESCRIPTION FROM DUAL
	 UNION ALL
	 SELECT 'BT' TRAN_TYPE, 'Basic Transactions' TRAN_TYPE_DESCRIPTION, 'VCTT' TRAN_CODE, 'Variance Calc Tax Tolerance' TRAN_CODE_DESCRIPTION FROM DUAL);

 prompt Adding Non Merch data to IM_TRAN_CODE table
 INSERT INTO IM_TRAN_CODE(TRAN_ID,
                          TRAN_TYPE,
                          TRAN_TYPE_DESCRIPTION,
                          TRAN_CODE,
                          TRAN_CODE_DESCRIPTION,
                          CREATED_BY,
                          LAST_UPDATED_BY)
 SELECT IM_TRAN_ID_SEQ.NEXTVAL, 
        'NMC',
		  'Non-Merchandise Code',
        NMCH.NON_MERCH_CODE,
        'Generated tran code for NMC ' || NMCH.NON_MERCH_CODE,
        'REIM_DEMO_USER',
        'REIM_DEMO_USER'
   FROM NON_MERCH_CODE_HEAD NMCH;

INSERT INTO GTT_6_NUM_6_STR_6_DATE(VARCHAR2_1,  --NEW_TRAN_CODE
                                   VARCHAR2_2,  --OLD_REASON_CODE
                                   VARCHAR2_3)  --SET_OF_BOOKS_ID
SELECT IM_TRAN_CODE_SEQ.NEXTVAL,
       IGCR.ACCOUNT_CODE,
       IGCR.SET_OF_BOOKS_ID
  FROM IM_GL_CROSS_REF IGCR
 WHERE IGCR.ACCOUNT_TYPE = 'RCA'
  AND EXISTS (SELECT 'x'
                FROM IM_REASON_CODES IRC
               WHERE IRC.REASON_CODE_ID = IGCR.ACCOUNT_CODE);

prompt Adding Reason Code data to IM_TRAN_CODE table
INSERT INTO IM_TRAN_CODE(TRAN_ID,
                         TRAN_TYPE,
                         TRAN_TYPE_DESCRIPTION,
                         TRAN_CODE,
                         TRAN_CODE_DESCRIPTION,
                         CREATED_BY,
                         LAST_UPDATED_BY)
                  SELECT IM_TRAN_ID_SEQ.NEXTVAL,
				             'RCA' TRAN_TYPE,
                         'Reason Code Action' TRAN_TYPE_DESCRIPTION,
                         GTT.VARCHAR2_1 TRAN_CODE,
                         'Generated tran code for reason code ' || GTT.VARCHAR2_2 || ' for Set of Books ID ' || GTT.VARCHAR2_3,
                         'REIM_DEMO_USER',
                         'REIM_DEMO_USER'
                    FROM GTT_6_NUM_6_STR_6_DATE GTT;

-------------------------------------------------------------------------
--------------------------IM_REASON_TRAN_CODE_MAP------------------------

prompt Adding data to IM_REASON_TRAN_CODE_MAP table
INSERT INTO IM_REASON_TRAN_CODE_MAP(REASON_TRAN_CODE_MAP_ID,
                                    REASON_CODE_ID,
									         TRAN_TYPE,
                                    TRAN_CODE,
                                    SET_OF_BOOKS_ID,
                                    CREATED_BY,
                                    LAST_UPDATED_BY)
                             SELECT IM_REASON_TRAN_CODE_MAP_SEQ.NEXTVAL,
                                    IRC.REASON_CODE_ID,
									         'RCA',
                                    GTT.VARCHAR2_1,
                                    GTT.VARCHAR2_3,
                                    'REIM_DEMO_USER',
                                    'REIM_DEMO_USER'
                               FROM IM_REASON_CODES IRC,
                                    GTT_6_NUM_6_STR_6_DATE GTT
                              WHERE IRC.REASON_CODE_ID = GTT.VARCHAR2_2;

-------------------------------------------------------------------------
--------------------------IM_GL_CROSS_REF--------------------------------

prompt Updating reson code data on IM_GL_CROSS_REF table
MERGE INTO IM_GL_CROSS_REF TGT
USING (SELECT GTT.VARCHAR2_1 NEW_ACCOUNT_CODE,
              IGCR.GL_CROSS_REF_ID
		 FROM GTT_6_NUM_6_STR_6_DATE GTT,
		      IM_GL_CROSS_REF IGCR
	    WHERE IGCR.ACCOUNT_CODE   = GTT.VARCHAR2_2
		  AND IGCR.SET_OF_BOOKS_ID = GTT.VARCHAR2_3) SRC
ON (TGT.GL_CROSS_REF_ID = SRC.GL_CROSS_REF_ID)
WHEN MATCHED THEN
   UPDATE
      SET TGT.ACCOUNT_CODE = SRC.NEW_ACCOUNT_CODE;

-------------------------------------------------------------------------
prompt Commiting data inserts
COMMIT;

prompt Load of ReIM demo data is complete!