REM ---------------------------------------------------------------------
REM
REM  IM_BASE_DATA.SQL
REM
REM  Script to create base application data
REM  needed for ReIM installation.
REM
REM 	ATTENTION: This script DOES NOT preserve data.
REM
REM	The customer DBA IS responsible TO resequence this script TO ensure
REM	data IS preserved AS desired.
REM  
REM
REM ---------------------------------------------------------------------
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

SET ECHO OFF;

prompt Deleting from IM_SYSTEM_OPTIONS
DELETE FROM IM_SYSTEM_OPTIONS;



REM ----------------------------------------------------------------------

prompt Adding data to IM_SYSTEM_OPTIONS table
 
INSERT INTO IM_SYSTEM_OPTIONS ( DEBIT_MEMO_SEND_DAYS
							  , CLOSE_OPEN_RECEIPT_DAYS
							  , DOC_HIST_DAYS
							  , DEBIT_MEMO_PREFIX_COST
							  , DEBIT_MEMO_PREFIX_QTY
							  , DEBIT_MEMO_PREFIX_TAX
							  , CREDIT_MEMO_PREFIX_COST
							  , CREDIT_MEMO_PREFIX_QTY
							  , CREDIT_NOTE_REQ_PREFIX_COST
							  , CREDIT_NOTE_REQ_PREFIX_QTY
							  , CREDIT_NOTE_REQ_PREFIX_TAX
							  , POST_DATED_DOC_DAYS
							  , MAX_TOLERANCE_PCT
							  , DAYS_BEFORE_DUE_DATE
							  , DFLT_PAY_NOW_TERMS_DEALS
							  , CALC_TOLERANCE
							  , TAX_VALIDATION_TYPE
							  , TAX_DOCUMENT_CREATION_LVL
							  , DFLT_HEADER_TAX
							  , TAX_RESOLUTION_DUE_DAYS
							  , CALC_TOLERANCE_IND
							  , RECEIPT_WRITE_OFF_DAYS
							  , DFLT_COST_OVERBILL_RC
							  , DFLT_QTY_OVERBILL_RC
							  , DFLT_COST_UNDERBILL_RC
							  , DFLT_QTY_UNDERBILL_RC
							  , NUM_TAX_ALLOW
							  , BALANCING_TOLERANCE
							  , POST_ON_DOC_HEADER
							  , PRORATE_ACROSS_TAX_CODES			      
							  , LUW_AUTO_MATCH
							  , DELAY_LN_MTCH_UNTL_ROUTE_DATE
							  , DFLT_PAY_NOW_TERMS_RTV
							  , TOTAL_QTY_REQUIRED
							  , DFLT_NON_MERCH_LOCATION
							  , DFLT_DEPT
							  , DFLT_CLASS
							  , GL_MAX_SEGMENTS
							  , DFLT_SOB
							  , DEAL_DOCS_PURGE_DAYS
							  , DOC_QTY_DECIMALS
							  , BATCH_DATE_FORMAT
							  , WS_FIN_ACC_VALID_URL
							  , WS_FIN_ACC_VALID_CRED
							  , WS_FIN_DRILL_FWD_URL
							  , WS_FIN_DRILL_FWD_CRED
							  , INCL_DOC_DATE_FOR_DUPL_CHECK
							  , INCL_DOC_YEAR_FOR_DUPL_CHECK
							  , POST_INTRA_REGION_TAX_IND
							  ,	RESOLUTION_DUE_DAYS 
							  , OBIEE_ENABLED
							  , LUW_EDI_INJECTOR
							  , INJECTOR_STAGED_HIST_DAYS
							  , BATCH_STATUS_HIST_DAYS
							  , BATCH_THREAD_TIMEOUT_MS
							  , S9T_PURGE_DAYS
							  , CREATED_BY
							  , LAST_UPDATED_BY)
select 	 1 		as DEBIT_MEMO_SEND_DAYS
       , 0 		as CLOSE_OPEN_RECEIPT_DAYS
       , 20 	as DOC_HIST_DAYS
       , 'DMC' 	as DEBIT_MEMO_PREFIX_COST
       , 'DMQ'	as DEBIT_MEMO_PREFIX_QTY
       , 'DMT'	as DEBIT_MEMO_PREFIX_TAX
       , 'CMC'	as CREDIT_MEMO_PREFIX_COST
       , 'CMQ'	as CREDIT_MEMO_PREFIX_QTY
       , 'CNC'	as CREDIT_NOTE_REQ_PREFIX_COST
       , 'CNQ'	as CREDIT_NOTE_REQ_PREFIX_QTY
       , 'CNT'	as CREDIT_NOTE_REQ_PREFIX_TAX
       , 365	as POST_DATED_DOC_DAYS
       , 30		as MAX_TOLERANCE_PCT
       , 10		as DAYS_BEFORE_DUE_DATE
       , (SELECT TERMS 
			FROM TERMS 
		   WHERE (START_DATE_ACTIVE IS NULL 
					OR 
				  START_DATE_ACTIVE > GET_VDATE ) 
			 AND END_DATE_ACTIVE IS NULL 
			 AND ENABLED_FLAG = 'Y' 
			 AND ROWNUM < 2)		
				as DFLT_PAY_NOW_TERMS_DEALS
       , 0.5 	as CALC_TOLERANCE
       , 'RECON'as TAX_VALIDATION_TYPE
       , 'ITEM' as TAX_DOCUMENT_CREATION_LVL
       , 'Y'	as DFLT_HEADER_TAX
       , 0		as TAX_RESOLUTION_DUE_DAYS
       , 'A'	as CALC_TOLERANCE_IND
       , 0		as RECEIPT_WRITE_OFF_DAYS
       , NULL	as DFLT_COST_OVERBILL_RC
       , NULL	as DFLT_QTY_OVERBILL_RC
       , NULL	as DFLT_COST_UNDERBILL_RC
       , NULL	as DFLT_QTY_UNDERBILL_RC
       , 'S'	as NUM_TAX_ALLOW
       , 1		as BALANCING_TOLERANCE
       , 'Y'	as POST_ON_DOC_HEADER
       , 'N'	as PRORATE_ACROSS_TAX_CODES
       , 1		as LUW_AUTO_MATCH
       , 'Y'	as DELAY_LN_MTCH_UNTL_ROUTE_DATE
       , (SELECT TERMS 
			FROM TERMS 
		   WHERE (START_DATE_ACTIVE IS NULL 
					OR 
				  START_DATE_ACTIVE > GET_VDATE ) 
			 AND END_DATE_ACTIVE IS NULL 
			 AND ENABLED_FLAG = 'Y' 
			 AND ROWNUM < 2)		
				as DFLT_PAY_NOW_TERMS_RTV
       , 'N'	as TOTAL_QTY_REQUIRED
       , (select store 
		    from store 
		   where rownum < 2)		
				as DFLT_NON_MERCH_LOCATION
       , (select max(dept) dept 
	        from class )		
		       as DFLT_DEPT
       , (select max(c.class) class
		    from class c
			   , (select max(dept) dept
					from class ) d
		   where d.dept = c.dept) 
				as DFLT_CLASS
       , 20		as GL_MAX_SEGMENTS
       , (select NVL((SELECT MIN(FGS.SET_OF_BOOKS_ID) FROM FIF_GL_SETUP FGS), null) from dual) 
				as DFLT_SOB
       , 90 	as DEAL_DOCS_PURGE_DAYS
       , 1 		as DOC_QTY_DECIMALS
       , 'yyyyMMddHHmmss' 
				as BATCH_DATE_FORMAT
       , NULL 	as WS_FIN_ACC_VALID_URL
       , 'N' 	as WS_FIN_ACC_VALID_CRED
       , NULL 	as WS_FIN_DRILL_FWD_URL
       , 'N' 	as WS_FIN_DRILL_FWD_CRED
       , 'N' 	as INCL_DOC_DATE_FOR_DUPL_CHECK
       , 'N' 	as INCL_DOC_YEAR_FOR_DUPL_CHECK
       , 'N' 	as POST_INTRA_REGION_TAX_IND
	   , 1      as RESOLUTION_DUE_DAYS
	   , 'N'    as OBIEE_ENABLED
	   , 10000  as LUW_EDI_INJECTOR
	   , 30     as INJECTOR_STAGED_HIST_DAYS
	   , 60     as BATCH_STATUS_HIST_DAYS
	   , 360000 as BATCH_THREAD_TIMEOUT_MS	
	   , 30     as S9T_PURGE_DAYS
	   ,'REIM_DEMO_USER'	AS CREATED_BY
       ,'REIM_DEMO_USER'	AS LAST_UPDATED_BY
from dual;


PROMPT Committing Changes

COMMIT;

SET ECHO OFF

PROMPT Data Load OF IM TABLES Complete

