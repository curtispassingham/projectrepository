------------------------------------------------------------------------------------------
---
---  IM_OI_SYS_OPTIONS.SQL
---
---  Script to create base application data needed for ReIM installation.
---
---  The script doesn't preserve data. It is intended to be ran on a fresh env.
------------------------------------------------------------------------------------------
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

prompt Inserting into IM_OI_SYS_OPTIONS
insert into im_oi_sys_options (weight_exact_match,
                               weight_non_discrepancy,
                               weight_timely_ship)
                       values (30,
                               50,
                               20);

commit;