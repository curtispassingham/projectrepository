DELETE FROM im_currencies;

INSERT INTO im_currencies(currency_code, currency_cost_dec)
SELECT 'GNF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SAR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'IRR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'IDR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BAM' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BTN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'EGP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BMD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BBD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BRL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'NZD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LRD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CYP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'THB' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GYD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'NPR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CUP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'YER' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KGS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'USS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'PAB' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'FRF' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ATS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BDT' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AOA' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ALL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'PEN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'UAH' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'DEM' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SKK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GWP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ANG' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'UGX' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AFA' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'VEB' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BOB' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AUD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'XAF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'UYU' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'NOK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'FJD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'RUB' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BGL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SVC' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GMD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'XCD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CVE' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MZN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MGF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BWP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SDD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ETB' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'HKD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LTL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SGD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AZN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CZK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AED' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'INR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'EEK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LVL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'DKK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MOP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BIF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MWK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BND' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TPE' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BSD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ITL' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MNT' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MRO' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AZM' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MGA' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TRL' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'VEF' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'PYG' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TRY' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ESP' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'FIM' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GTQ' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'USN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SBD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TOP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KHR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CRC' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TTD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'VUV' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MUR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GEL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BHD' as currency_code, 3 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'OMR' as currency_code, 3 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CAD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MTL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GRD' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ZWL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LKR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TMT' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'NAD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SEK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MZM' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KRW' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SOS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SRG' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TND' as currency_code, 3 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SYP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TWD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MXN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BYB' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LYD' as currency_code, 3 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'DZD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'USD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'XOF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GBP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TJS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'RUR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'NLG' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'UZS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MVR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'YUM' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SDG' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KPW' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MAD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'VND' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AWG' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GHC' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BYR' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'JMD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'HTG' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'EUR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SIT' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BEF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GHS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ARS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ZWN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LUF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ZAR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ZMK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CDF' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CUC' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'PLN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MKD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'RON' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BGN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'QAR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AYM' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ERN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'RWF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MXV' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LAK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SZL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'JOD' as currency_code, 3 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'IQD' as currency_code, 3 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SRD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'HNL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MDL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'IEP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SCR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'COP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'JPY' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'PKR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'WST' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'DJF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CLF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ZWR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'RSD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BZD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'PTE' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'PHP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'GIP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'NGN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'HUF' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KWD' as currency_code, 3 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KZT' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TZS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'NIO' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LSL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ISK' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'PGK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CLP' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'XPF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MYR' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'FKP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CHF' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KMF' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AMD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'STD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'TMM' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ROL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'BOV' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CSD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KYD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'LBP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'HRK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ILS' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'KES' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SLL' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ADP' as currency_code, 0 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'ZWD' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'CNY' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'AFN' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'MMK' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'DOP' as currency_code, 2 as currency_cost_dec FROM DUAL
UNION ALL
SELECT 'SHP' as currency_code, 2 as currency_cost_dec FROM DUAL;

COMMIT;