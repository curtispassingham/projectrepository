delete from raf_notification where notification_type in (select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim') ;
delete from raf_notification_type_tl where notification_type in (select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim') and language = 'US';
delete from raf_notification_type_b where application_code='Reim';



Insert into RAF_NOTIFICATION_TYPE_B (NOTIFICATION_TYPE,CREATED_BY,CREATE_DATE,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,APPLICATION_CODE,RETENTION_DAYS,OBJECT_VERSION_NUMBER,NOTIFICATION_TYPE_CODE)
values (RAF_NOTIFICATION_TYPE_SEQ.nextval,'Admin',to_timestamp('23-DEC-13 03.07.43.245000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-DEC-13 03.07.46.942000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'Admin','Admin','Reim',-1,null,'Invoice Induction Complete');
Insert into RAF_NOTIFICATION_TYPE_B (NOTIFICATION_TYPE,CREATED_BY,CREATE_DATE,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,APPLICATION_CODE,RETENTION_DAYS,OBJECT_VERSION_NUMBER,NOTIFICATION_TYPE_CODE) values 
(RAF_NOTIFICATION_TYPE_SEQ.nextval,'Admin',to_timestamp('23-DEC-13 03.07.43.245000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-DEC-13 03.07.46.942000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'Admin','Admin','Reim',-1,null,'Invoice Induction Failure');


Insert into RAF_NOTIFICATION_TYPE_TL (NOTIFICATION_TYPE,LANGUAGE,NAME,DESCRIPTION,SOURCE_LANG,CREATE_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) values 
((select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim' and notification_type_code='Invoice Induction Complete'),'US','Invoice Induction Complete','Invoice Induction Complete','US',to_timestamp('23-DEC-13 03.09.00.559000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'Admin',to_timestamp('23-DEC-13 03.09.24.490000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'Admin','Admin',null);
Insert into RAF_NOTIFICATION_TYPE_TL (NOTIFICATION_TYPE,LANGUAGE,NAME,DESCRIPTION,SOURCE_LANG,CREATE_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER)
values ((select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim' and notification_type_code='Invoice Induction Failure'),'US','Invoice Induction Failure','Invoice Induction Failure','US',to_timestamp('23-DEC-13 03.09.00.559000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'Admin',to_timestamp('23-DEC-13 03.09.24.490000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'Admin','Admin',null);
