-- DELETE SCRIPT for Look up types
DELETE FROM RTC_LOOKUP_TYPES_TL WHERE LANGUAGE = 'S' AND LOOKUP_TYPE_ID IN (SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM');
-- REM INSERTING into RTC_LOOKUP_TYPES_TL
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Document Type'),'S','US','Document Type','Dokumenttyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Document Status'),'S','US','Document Status','Dokumentstatus','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Vendor Type'),'S','US','Vendor Type','Leverantörstyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Location Type'),'S','US','Location Type','Platstyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Freight Payment Type'),'S','US','Freight Payment Type','Fraktbetalningstyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'ReIMBooleanYesNo'),'S','US','ReIMBooleanYesNo','ReIMBooleanYesNo','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Document Source'),'S','US','Document Source','Dokumentkälla','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Manual Search Status'),'S','US','Manual Search Status','Status för manuell sökning','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Document Deal Type'),'S','US','Document Deal Type','Dokumentavtalstyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Document Comment Type'),'S','US','Document Comment Type','Dokumentkommentarstyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Reason Code Type'),'S','US','Reason Code Type','Typ av orsakskod','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Tolerance Match Level'),'S','US','Tolerance Match Level','Toleransmatchningsnivå','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Tolerance Match Type'),'S','US','Tolerance Match Type','Toleransmatchningstyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Tolerance Favor Of'),'S','US','Tolerance Favor Of','Tolerans till fördel för','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Tolerance Value Type'),'S','US','Tolerance Value Type','Toleransvärdetyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Tolerance Level Type'),'S','US','Tolerance Level Type','Toleransnivåtyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Match Stgy Match Level'),'S','US','Match Stgy Match Level','Match.nivå för match.strat.','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Match Stgy Match Type'),'S','US','Match Stgy Match Type','Match.typ för match.strat.','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Match Stgy Match Type Detail'),'S','US','Match Stgy Match Type Detail','Match.typdetaljer för match.strat.','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Detail Match Status'),'S','US','Detail Match Status','Detaljmatchningsstatus','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Detail Match Cost Quantity'),'S','US','Detail Match Cost Quantity','Kostnadskvantitet för detaljmatchning','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Tax Validation Type'),'S','US','Tax Validation Type','Momsvalideringstyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Tax Doc Creation Level'),'S','US','Tax Doc Creation Level','Skapandenivå för momsdok.','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Num Tax Allow'),'S','US','Num Tax Allow','Tillåt moms','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Supplier Level'),'S','US','Supplier Level','Leverantörsnivå','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Match Key'),'S','US','Match Key','Matchningsnyckel','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Send Debit Memo'),'S','US','Send Debit Memo','Skicka debetnota','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Reason Code Action'),'S','US','Reason Code Action','Orsakskodsåtgärd','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'EDI Document Type'),'S','US','EDI Document Type','Dokumenttyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Cross Reference Type'),'S','US','Cross Reference Type','Korsreferenstyp','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Dynamic Segment Type'),'S','US','Dynamic Segment Type','Typ av dynamiskt segment','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Inquiry Status'),'S','US','Inquiry Status','Frågestatus','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Async Process Status'),'S','US','Async Process Status','Asynkron processtatus','Admin',sysdate,'Admin',sysdate,NULL,1);
INSERT INTO RTC_LOOKUP_TYPES_TL (LOOKUP_TYPE_ID,LANGUAGE,SOURCE_LANG,MEANING,DESCRIPTION,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) VALUES
((SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'REIM' AND LOOKUP_TYPE = 'Template Type'),'S','US','Template Type','Malltyp','Admin',sysdate,'Admin',sysdate,NULL,1);
