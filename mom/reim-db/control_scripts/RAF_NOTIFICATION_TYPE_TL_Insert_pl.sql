delete from raf_notification_type_tl where notification_type in (select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim') and language = 'PL';
delete from raf_notification_type_tl where NAME='Invoice Induction Complete' and LANGUAGE='PL';
delete from raf_notification_type_tl where NAME='Invoice Induction Failure' and LANGUAGE='PL';
Insert into RAF_NOTIFICATION_TYPE_TL (NOTIFICATION_TYPE,LANGUAGE,NAME,DESCRIPTION,SOURCE_LANG,CREATE_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) values 
((select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim' and notification_type_code='Invoice Induction Complete'),'PL','Invoice Induction Complete','Wpływ faktury - ukończone','US',SYSDATE,'Admin',SYSDATE,'Admin','Admin',null);
Insert into RAF_NOTIFICATION_TYPE_TL (NOTIFICATION_TYPE,LANGUAGE,NAME,DESCRIPTION,SOURCE_LANG,CREATE_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) values 
((select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim' and notification_type_code='Invoice Induction Failure'),'PL','Invoice Induction Failure','Wpływ faktury - niepowodzenie','US',SYSDATE,'Admin',SYSDATE,'Admin','Admin',null);

