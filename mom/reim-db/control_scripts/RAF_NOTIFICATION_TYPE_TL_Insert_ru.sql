delete from raf_notification_type_tl where notification_type in (select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim') and language = 'RU';
delete from raf_notification_type_tl where NAME='Invoice Induction Complete' and LANGUAGE='RU';
delete from raf_notification_type_tl where NAME='Invoice Induction Failure' and LANGUAGE='RU';
Insert into RAF_NOTIFICATION_TYPE_TL (NOTIFICATION_TYPE,LANGUAGE,NAME,DESCRIPTION,SOURCE_LANG,CREATE_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) values 
((select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim' and notification_type_code='Invoice Induction Complete'),'RU','Invoice Induction Complete','Введение счета-фактуры завершено','US',SYSDATE,'Admin',SYSDATE,'Admin','Admin',null);
Insert into RAF_NOTIFICATION_TYPE_TL (NOTIFICATION_TYPE,LANGUAGE,NAME,DESCRIPTION,SOURCE_LANG,CREATE_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER) values 
((select NOTIFICATION_TYPE  from  RAF_NOTIFICATION_TYPE_B where application_code='Reim' and notification_type_code='Invoice Induction Failure'),'RU','Invoice Induction Failure','Ошибка введения счета-фактуры','US',SYSDATE,'Admin',SYSDATE,'Admin','Admin',null);

