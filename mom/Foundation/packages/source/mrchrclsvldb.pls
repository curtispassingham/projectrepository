



CREATE OR REPLACE PACKAGE BODY MERCH_RECLASS_VALIDATE_SQL AS
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
FUNCTION EXIST(O_error_message         IN OUT   VARCHAR2,
               O_exists                IN OUT   BOOLEAN,
               I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
               I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
               I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
               I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)

   RETURN BOOLEAN IS
   
   L_program     VARCHAR2(50)  := 'MERCH_RECLASS_VALIDATE_SQL.EXIST';
   
   L_exist       VARCHAR2(1);   

   -- used for retrieving the correct record
   L_hier_id	        PEND_MERCH_HIER.MERCH_HIER_ID%TYPE;
   L_hier_parent_id       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE;
   L_hier_grandparent_id  PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE;
   
   cursor C_PEND_EXIST is
      select 'x'
        from PEND_MERCH_HIER
       where hier_type = I_hier_type
         and merch_hier_id = L_hier_id
         and NVL(merch_hier_parent_id, -999) = 
             NVL(L_hier_parent_id, NVL(merch_hier_parent_id, -999))
         and NVL(merch_hier_grandparent_id, -999) = 
             NVL(L_hier_grandparent_id, NVL(merch_hier_grandparent_id, -999));

BEGIN

   --CHECK FOR REQUIRED FIELDS
   if I_hier_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_type not in (MERCH_RECLASS_VALIDATE_SQL.LP_div_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_grp_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_dept_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_cls_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_MERCH_LEVEL',
                                            I_hier_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code or
      I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_hier_parent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_parent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_hier_grandparent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_grandparent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;

   -- To determine which record retrieve, parent id should only be used 
   -- for class and subclass, grandparent id should only be used for subclass. 
   if not MERCH_RECLASS_SQL.DETERMINE_HIERARCHY(O_error_message,
                                                L_hier_id,
                                                L_hier_parent_id,
                                                L_hier_grandparent_id,
                                                I_hier_type,
                                                I_hier_id,
                                                I_hier_parent_id,
                                                I_hier_grandparent_id) then
         return FALSE;
   end if;

   -- perform query
   SQL_LIB.SET_MARK('OPEN',
                    'C_PEND_EXIST',
                    'PEND_MERCH_HIER',
                    ' hier_type: '|| I_hier_type ||
                    ' merch_hier_id: '||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(L_hier_grandparent_id));
   open C_PEND_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PEND_EXIST',
                    'PEND_MERCH_HIER',
                    ' hier_type: '|| I_hier_type ||
                    ' merch_hier_id: '||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(L_hier_grandparent_id));
   fetch C_PEND_EXIST into L_exist;

   if C_PEND_EXIST%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PEND_EXIST',
                    'PEND_MERCH_HIER',
                    ' hier_type: '|| I_hier_type ||
                    ' merch_hier_id: '||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(L_hier_grandparent_id));
   close C_PEND_EXIST;
 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END EXIST;
-------------------------------------------------------------------------------------------------------
FUNCTION CHILD_EXIST(O_error_message    IN OUT   VARCHAR2,
                     O_exists           IN OUT   BOOLEAN,
                     I_hier_type        IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                     I_hier_id          IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                     I_hier_parent_id   IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                     I_effective_date   IN       PEND_MERCH_HIER.EFFECTIVE_DATE%TYPE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(50)  := 'MERCH_RECLASS_VALIDATE_SQL.CHILD_EXIST';
   
   L_exists            VARCHAR2(1)   := NULL;
   L_child_hier_type   PEND_MERCH_HIER.HIER_TYPE%TYPE;
   L_grandparent_id    PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE := NULL;

   -- if I_effective date is null, check if any child hierarchy exists;
   -- if I_effective date is not null, check if any child hierarchy exists
   -- with date prior to I_effective_date. 
   cursor C_CHILD_EXIST is
      select 'x'
        from PEND_MERCH_HIER
       where hier_type = L_child_hier_type
         and merch_hier_parent_id = I_hier_id
         and NVL(merch_hier_grandparent_id, -999) =
             NVL(L_grandparent_id, NVL(merch_hier_grandparent_id, -999))
         and effective_date < NVL(I_effective_date, effective_date + 1);

BEGIN

   --CHECK FOR REQUIRED FIELDS
   if I_hier_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_type not in (MERCH_RECLASS_VALIDATE_SQL.LP_div_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_grp_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_dept_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_cls_code) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_MERCH_LEVEL',
                                            I_hier_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code then
      if I_hier_parent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_parent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;

      -- only class should pass in grandparent id, all others, use null
      L_grandparent_id  := I_hier_parent_id;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_div_code then
      L_child_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_grp_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_grp_code then
      L_child_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_dept_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_dept_code then
      L_child_hier_type:= MERCH_RECLASS_VALIDATE_SQL.LP_cls_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code then
      L_child_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_scls_code;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHILD_EXIST',
                    'PEND_MERCH_HIER',
                    ' hier_type: '|| L_child_hier_type ||
                    ' merch_hier_parent_id: '||TO_CHAR(I_hier_id)||
                    ' merch_hier_grandparent_id : '||TO_CHAR(L_grandparent_id));
   open C_CHILD_EXIST;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHILD_EXIST',
                    'PEND_MERCH_HIER',
                    ' hier_type: '|| L_child_hier_type ||
                    ' merch_hier_parent_id: '||TO_CHAR(I_hier_id)||
                    ' merch_hier_grandparent_id : '||TO_CHAR(L_grandparent_id));
   fetch C_CHILD_EXIST into L_exists;

   if C_CHILD_EXIST%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHILD_EXIST',
                    'PEND_MERCH_HIER',
                    ' hier_type: '|| L_child_hier_type ||
                    ' merch_hier_parent_id: '||TO_CHAR(I_hier_id)||
                    ' merch_hier_grandparent_id : '||TO_CHAR(L_grandparent_id));
   close C_CHILD_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHILD_EXIST;
-------------------------------------------------------------------------------------------------
FUNCTION GET_MERCH_ACTION(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_action_type           OUT      PEND_MERCH_HIER.ACTION_TYPE%TYPE,
                          I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                          I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                          I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                          I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)

   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(60)   := 'MERCH_RECLASS_VALIDATE_SQL.GET_MERCH_ACTION';

   -- used for retrieving the correct record
   L_hier_id	        PEND_MERCH_HIER.MERCH_HIER_ID%TYPE;
   L_hier_parent_id       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE;
   L_hier_grandparent_id  PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE;
   
   cursor C_GET_ACTION is
      select action_type
        from PEND_MERCH_HIER
       where hier_type = I_hier_type
         and merch_hier_id = L_hier_id
         and NVL(merch_hier_parent_id, -999) = 
             NVL(L_hier_parent_id, NVL(merch_hier_parent_id, -999))
         and NVL(merch_hier_grandparent_id, -999) = 
             NVL(L_hier_grandparent_id, NVL(merch_hier_grandparent_id, -999));

BEGIN

   --CHECK FOR REQUIRED FIELDS
   if I_hier_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_type not in (MERCH_RECLASS_VALIDATE_SQL.LP_div_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_grp_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_dept_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_cls_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_MERCH_LEVEL',
                                            I_hier_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code or
      I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_hier_parent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_parent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_hier_grandparent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_grandparent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;

   -- To determine which record retrieve, parent id should only be used 
   -- for class and subclass, grandparent id should only be used for subclass. 
   if not MERCH_RECLASS_SQL.DETERMINE_HIERARCHY(O_error_message,
                                                L_hier_id,
                                                L_hier_parent_id,
                                                L_hier_grandparent_id,
                                                I_hier_type,
                                                I_hier_id,
                                                I_hier_parent_id,
                                                I_hier_grandparent_id) then
         return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ACTION',
                    'PEND_MERCH_HIER',
                    ' hier_type: '||I_hier_type||
                    ' merch_hier_id: '||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(L_hier_grandparent_id));
   open C_GET_ACTION;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ACTION',
                    'PEND_MERCH_HIER',
                    ' hier_type: '||I_hier_type||
                    ' merch_hier_id: '||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(L_hier_grandparent_id));
   fetch C_GET_ACTION into O_action_type;

   if C_GET_ACTION%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_NOT_EXIST',
                                            I_hier_type,
                                            TO_CHAR(L_hier_id) ||
                                            '/' || TO_CHAR(L_hier_parent_id) ||
                                            '/' || TO_CHAR(L_hier_grandparent_id),
                                            null);
      return false;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ACTION',
                    'PEND_MERCH_HIER',
                    ' hier_type: '||I_hier_type||
                    ' merch_hier_id: '||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(L_hier_grandparent_id));
   close C_GET_ACTION;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_MERCH_ACTION;
-------------------------------------------------------------------------------------------------
END MERCH_RECLASS_VALIDATE_SQL;
/
