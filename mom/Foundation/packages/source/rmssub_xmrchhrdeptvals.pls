
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRDEPT_VALIDATE AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dept_rec        OUT    NOCOPY   MERCH_SQL.DEPT_REC,
                       I_message         IN              "RIB_XMrchHrDeptDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dept_rec        OUT    NOCOPY   MERCH_SQL.DEPT_REC,
                       I_message         IN              "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRDEPT_VALIDATE;
/
