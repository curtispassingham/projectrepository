CREATE OR REPLACE PACKAGE BODY CORESVC_WF_COST_TMPL_UPLD_SQL AS
----------------------------------------------------------------------------------------------
-- GLOBAL VARIABLES
----------------------------------------------------------------------------------------------

   LP_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE := NULL;
   LP_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE := NULL;
   L_vdate                  PERIOD.VDATE%TYPE     := GET_VDATE;
   L_upd_rows               NUMBER := 0;
----------------------------------------------------------------------------------------------
-- PL/SQL COLLECTIONS
----------------------------------------------------------------------------------------------

TYPE TYP_fctmpl_fhead_rec is RECORD
(
   process_id        SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
   fhead_count       NUMBER(10),
   ftail_count       NUMBER(10),
   frecs_count       NUMBER(10),
   file_rec_cntr     SVC_WF_COST_TMPL_UPLD_FHEAD.FILE_RECORD_DESCRIPTOR%TYPE,
   file_type         SVC_WF_COST_TMPL_UPLD_FHEAD.FILE_TYPE%TYPE,
   thead_count       NUMBER(10),
   tdetl_count       NUMBER(10),   
   ttail_count       NUMBER(10)
);

TYPE TYP_fctmpl_tran_rec is RECORD
(
   process_id        SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
   seq_no            SVC_WF_COST_TMPL_UPLD_FHEAD.SEQ_NO%TYPE,
   thead_count       NUMBER(10),
   tdetl_count       NUMBER(10),
   ttail_count       NUMBER(10),
   tran_rec_cntr     SVC_WF_COST_TMPL_UPLD_TTAIL.TRAN_RECORD_COUNTER%TYPE,
   message_type      SVC_WF_COST_TMPL_UPLD_THEAD.MESSAGE_TYPE%TYPE,
   template_id       SVC_WF_COST_TMPL_UPLD_THEAD.TEMPLATE_ID%TYPE,
   template_desc     SVC_WF_COST_TMPL_UPLD_THEAD.TEMPLATE_DESC%TYPE,
   first_applied     SVC_WF_COST_TMPL_UPLD_THEAD.FIRST_APPLIED%TYPE,
   percentage        SVC_WF_COST_TMPL_UPLD_THEAD.PERCENTAGE%TYPE,
   cost              SVC_WF_COST_TMPL_UPLD_THEAD.COST%TYPE,
   final_cost        SVC_WF_COST_TMPL_UPLD_THEAD.FINAL_COST%TYPE,
   existing_templ_id WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE
);

TYPE TYP_fctmpl_tran_tbl is TABLE OF TYP_fctmpl_tran_rec INDEX BY BINARY_INTEGER;


TYPE TYP_fctmpl_tdetl_rec is RECORD
(
   row_id              VARCHAR2(20),
   process_id          SVC_WF_COST_TMPL_UPLD_TDETL.PROCESS_ID%TYPE,
   seq_no              SVC_WF_COST_TMPL_UPLD_TDETL.SEQ_NO%TYPE,
   message_type        SVC_WF_COST_TMPL_UPLD_TDETL.MESSAGE_TYPE%TYPE,
   dept                SVC_WF_COST_TMPL_UPLD_TDETL.DEPT%TYPE,
   class_id            SVC_WF_COST_TMPL_UPLD_TDETL.CLASS%TYPE,
   subclass_id         SVC_WF_COST_TMPL_UPLD_TDETL.SUBCLASS%TYPE,
   item                SVC_WF_COST_TMPL_UPLD_TDETL.ITEM%TYPE,
   location            SVC_WF_COST_TMPL_UPLD_TDETL.LOCATION%TYPE,
   start_date          SVC_WF_COST_TMPL_UPLD_TDETL.START_DATE%TYPE,
   end_date            SVC_WF_COST_TMPL_UPLD_TDETL.END_DATE%TYPE,
   new_start_date      SVC_WF_COST_TMPL_UPLD_TDETL.NEW_START_DATE%TYPE,
   new_end_date        SVC_WF_COST_TMPL_UPLD_TDETL.NEW_END_DATE%TYPE,
   cost_comp_id        SVC_WF_COST_TMPL_UPLD_TDETL.COST_COMP_ID%TYPE,
   store_type          STORE.STORE_TYPE%TYPE,
   exist_store         STORE.STORE%TYPE,
   thead_msg_type      SVC_WF_COST_TMPL_UPLD_THEAD.MESSAGE_TYPE%TYPE,
   thead_first_applied SVC_WF_COST_TMPL_UPLD_THEAD.FIRST_APPLIED%TYPE,
   exist_item          ITEM_MASTER.ITEM%TYPE,
   item_status         ITEM_MASTER.STATUS%TYPE,
   item_level          ITEM_MASTER.ITEM_LEVEL%TYPE,
   tran_level          ITEM_MASTER.TRAN_LEVEL%TYPE,
   orderable_ind       ITEM_MASTER.ORDERABLE_IND%TYPE,
   map_merch_hier      SUBCLASS.SUBCLASS%TYPE,
   exist_comp_id       ELC_COMP.COMP_ID%TYPE,
   comp_type           ELC_COMP.COMP_TYPE%TYPE,
   up_chrg_group       ELC_COMP.UP_CHRG_GROUP%TYPE
);

TYPE TYP_fctmpl_tdetl_tbl is TABLE OF TYP_fctmpl_tdetl_rec INDEX BY BINARY_INTEGER;

TYPE TYP_process_chunk_rec is RECORD
(
   process_id        SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
   seq_no            SVC_WF_COST_TMPL_UPLD_FHEAD.SEQ_NO%TYPE
);

TYPE TYP_process_chunk_tbl is TABLE OF TYP_process_chunk_rec INDEX BY BINARY_INTEGER;

TYPE TYP_process_thead_rec is RECORD
(
   message_type       SVC_WF_COST_TMPL_UPLD_THEAD.MESSAGE_TYPE%TYPE,
   template_id        SVC_WF_COST_TMPL_UPLD_THEAD.TEMPLATE_ID%TYPE,
   template_desc      SVC_WF_COST_TMPL_UPLD_THEAD.TEMPLATE_DESC%TYPE,
   first_applied      SVC_WF_COST_TMPL_UPLD_THEAD.FIRST_APPLIED%TYPE,
   percentage         SVC_WF_COST_TMPL_UPLD_THEAD.PERCENTAGE%TYPE,
   cost               SVC_WF_COST_TMPL_UPLD_THEAD.COST%TYPE,
   final_cost         SVC_WF_COST_TMPL_UPLD_THEAD.FINAL_COST%TYPE
);

TYPE TYP_process_tdetl_rec is RECORD
(
   rowid              VARCHAR2(20),
   message_type       SVC_WF_COST_TMPL_UPLD_TDETL.MESSAGE_TYPE%TYPE,
   dept               SVC_WF_COST_TMPL_UPLD_TDETL.DEPT%TYPE,
   class              SVC_WF_COST_TMPL_UPLD_TDETL.CLASS%TYPE,
   subclass           SVC_WF_COST_TMPL_UPLD_TDETL.SUBCLASS%TYPE,
   item               SVC_WF_COST_TMPL_UPLD_TDETL.ITEM%TYPE,
   location           SVC_WF_COST_TMPL_UPLD_TDETL.LOCATION%TYPE,
   start_date         SVC_WF_COST_TMPL_UPLD_TDETL.START_DATE%TYPE,
   end_date           SVC_WF_COST_TMPL_UPLD_TDETL.END_DATE%TYPE,
   new_start_date     SVC_WF_COST_TMPL_UPLD_TDETL.NEW_START_DATE%TYPE,
   new_end_date       SVC_WF_COST_TMPL_UPLD_TDETL.NEW_END_DATE%TYPE,
   cost_comp_id       SVC_WF_COST_TMPL_UPLD_TDETL.COST_COMP_ID%TYPE
);

TYPE TYP_process_tdetl_tbl is TABLE OF TYP_process_tdetl_rec INDEX BY BINARY_INTEGER;
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
-- PRIVATE PROCEDURES AND FUNCTIONS
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
-- Write errors to the the parameter tables
-- Only captured errors of a functional or known technical reason is written
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR_PARAMS(I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                             I_seq_no          IN SVC_WF_COST_TMPL_UPLD_FHEAD.SEQ_NO%TYPE,                             
                             I_table_name      IN VARCHAR2 DEFAULT NULL);
----------------------------------------------------------------------------------------------                              
-- Update the process/chunk status to error
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                      I_chunk_id        IN SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE,
                      I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE);
----------------------------------------------------------------------------------------------
-- Write success status to the parameter tables
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS_PARAMS(I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                               I_seq_no          IN SVC_WF_COST_TMPL_UPLD_FHEAD.SEQ_NO%TYPE);
----------------------------------------------------------------------------------------------
-- Write reject status to the the parameter tables
-- Only captured errors of a functional is written
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_REJECT_PARAMS(I_error_message   IN VARCHAR2,
                              I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                              I_seq_no          IN SVC_WF_COST_TMPL_UPLD_FHEAD.SEQ_NO%TYPE,
                              I_error_rowid     IN VARCHAR2 DEFAULT NULL,
                              I_table_name      IN VARCHAR2 DEFAULT NULL);
-- Update the process/chunk status to success
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS(I_process_id      IN SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                        I_chunk_id        IN SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE);
----------------------------------------------------------------------------------------------                        
-- Lock wf_cost_buildup_tmpl_hd_tl table affected by the update and delete operation
----------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_HEAD_TABLE_TL(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                     I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------                     
-- Lock wf_cost_buildup_tmpl_head table affected by the update and delete operation
----------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_HEAD_TABLE(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                  I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------                        
-- Lock wf_cost_relationship table affected by the update and delete operation
----------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_RELN_TABLE(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                  I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Lock wf_cost_buildup_tmpl_dtl_tl table affected by the update and delete operation
----------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_DETL_TABLE_TL(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                     I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Lock wf_cost_buildup_tmpl_detail table affected by the update and delete operation
----------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_DETL_TABLE(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                  I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Validate if there are any cost components in the WF_COST_BUILDUP_TMPL_DETAIL 
-- table for a given the templ id.
-----------------------------------------------------------------------------------------------
FUNCTION COST_COMP_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_comp_exists     IN OUT   BOOLEAN,
                          I_templ_id        IN       WF_COST_BUILDUP_TMPL_DETAIL.TEMPL_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name : DUP_RELN_EXISTS
-- Purpose       : This function will check if duplicate record already exists in WF_COST_RELATIONSHIP
-- for the passed in department,classs,subclass,location,item, start_date and end_date.
-------------------------------------------------------------------------------------------------------
FUNCTION DUP_RELN_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_dup_reln_exists IN OUT  BOOLEAN,
                          O_dup_reln_rowid  IN OUT  VARCHAR2,
                          I_dept            IN      WF_COST_RELATIONSHIP.DEPT%TYPE,
                          I_class           IN      WF_COST_RELATIONSHIP.CLASS%TYPE,
                          I_subclass        IN      WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                          I_location        IN      WF_COST_RELATIONSHIP.LOCATION%TYPE,
                          I_start_date      IN      WF_COST_RELATIONSHIP.START_DATE%TYPE,
                          I_end_date        IN      WF_COST_RELATIONSHIP.END_DATE%TYPE,
                          I_item            IN      WF_COST_RELATIONSHIP.ITEM%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR_PARAMS(I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                             I_seq_no          IN SVC_WF_COST_TMPL_UPLD_FHEAD.SEQ_NO%TYPE,
                             I_table_name      IN VARCHAR2 DEFAULT NULL) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program             VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.WRITE_ERROR_PARAMS';

   cursor C_LOCK_FHEAD is
      select 'x'
        from svc_wf_cost_tmpl_upld_fhead fh
       where fh.process_id = I_process_id
         and fh.seq_no = I_seq_no
         for update of fh.error_msg, fh.status nowait;
         
   cursor C_LOCK_FTAIL is
      select 'x'
        from svc_wf_cost_tmpl_upld_ftail ft
       where ft.process_id = I_process_id
         and ft.seq_no =  I_seq_no
         for update of ft.error_msg, ft.status nowait;      

BEGIN

   if (I_table_name = CORESVC_WF_COST_TMPL_UPLD_SQL.FHEAD) then
   
      open C_LOCK_FHEAD;
      close C_LOCK_FHEAD;
      
      update svc_wf_cost_tmpl_upld_fhead fh
         set fh.error_msg = I_error_message,
             fh.status = CORESVC_WF_COST_TMPL_UPLD_SQL.ERROR,
             fh.last_update_datetime = sysdate
       where fh.process_id = I_process_id
         and fh.seq_no = I_seq_no;
      
   end if;

   if (I_table_name = CORESVC_WF_COST_TMPL_UPLD_SQL.FTAIL)then
   
      open C_LOCK_FTAIL;
      close C_LOCK_FTAIL;
      
      update svc_wf_cost_tmpl_upld_ftail ft
         set ft.error_msg = I_error_message,
             ft.status = CORESVC_WF_COST_TMPL_UPLD_SQL.ERROR
       where ft.process_id = I_process_id
         and ft.seq_no = I_seq_no ;
   end if;
 
   commit;

EXCEPTION
   when OTHERS then
      if C_LOCK_FHEAD%ISOPEN then
         close C_LOCK_FHEAD;
      end if;
      
      if C_LOCK_FTAIL%ISOPEN then
         close C_LOCK_FTAIL;
      end if;
      
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                      I_chunk_id        IN SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE,
                      I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program     VARCHAR2(64)                              := 'CORESVC_WF_COST_TMPL_UPLD_SQL.WRITE_ERROR';
   L_max_chunk   SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE := NULL;

   cursor C_LOCK_SVC_COSTTMPL_STATUS is
      select 'x'
        from svc_wf_cost_tmpl_upld_status s
       where process_id = I_process_id
         and chunk_id   = I_chunk_id
         for update of s.status, s.error_msg nowait;

BEGIN

   open C_LOCK_SVC_COSTTMPL_STATUS;
   close C_LOCK_SVC_COSTTMPL_STATUS;
   
   update svc_wf_cost_tmpl_upld_status
      set status    = CORESVC_WF_COST_TMPL_UPLD_SQL.ERROR,
          error_msg = I_error_message,
          last_update_datetime = sysdate
    where process_id = I_process_id
      and chunk_id   = I_chunk_id;

   commit;

EXCEPTION
   when OTHERS then
      
      if C_LOCK_SVC_COSTTMPL_STATUS%ISOPEN then
         close C_LOCK_SVC_COSTTMPL_STATUS;
      end if;      
      
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS_PARAMS(I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                               I_seq_no          IN SVC_WF_COST_TMPL_UPLD_FHEAD.SEQ_NO%TYPE) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.WRITE_SUCCESS_PARAMS';

   cursor C_LOCK_FHEAD is
      select 'x'
        from svc_wf_cost_tmpl_upld_fhead fh
       where fh.process_id = I_process_id
         for update of fh.status nowait;

   cursor C_LOCK_FTAIL is
      select 'x'
        from svc_wf_cost_tmpl_upld_ftail ft
       where ft.process_id = I_process_id
         for update of ft.status nowait;
         
   cursor C_LOCK_THEAD is
      select 'x'
        from svc_wf_cost_tmpl_upld_thead th
       where th.process_id = I_process_id
         and th.seq_no = I_seq_no
         for update of th.status nowait;
     
   cursor C_LOCK_TDETL is
      select 'x'
        from svc_wf_cost_tmpl_upld_tdetl td
       where td.process_id = I_process_id
         and td.seq_no = I_seq_no
         for update of td.status nowait;
     
   cursor C_LOCK_TTAIL is
      select 'x'
        from svc_wf_cost_tmpl_upld_ttail tt
       where tt.process_id = I_process_id
         and tt.seq_no = I_seq_no
         for update of tt.status nowait;
BEGIN

   if (I_process_id is NOT NULL and I_seq_no is NULL) then
   
      open C_LOCK_FHEAD;
      close C_LOCK_FHEAD;
      
      update svc_wf_cost_tmpl_upld_fhead fh
         set fh.status = CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESSED,
             fh.last_update_datetime = sysdate
       where fh.process_id = I_process_id;
  
      open C_LOCK_FTAIL;
      close C_LOCK_FTAIL;
      
      update svc_wf_cost_tmpl_upld_ftail ft
         set ft.status = CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESSED
       where ft.process_id = I_process_id;
      
   end if;
   
   if (I_process_id is NOT NULL and I_seq_no is NOT NULL) then
   
      open C_LOCK_THEAD;
      close C_LOCK_THEAD;
      
      update svc_wf_cost_tmpl_upld_thead th
         set th.status = CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESSED
       where th.process_id = I_process_id
         and th.seq_no = I_seq_no;
      
      open C_LOCK_TDETL;
      close C_LOCK_TDETL;
            
      update svc_wf_cost_tmpl_upld_tdetl td
         set td.status = CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESSED
       where td.process_id = I_process_id
         and td.seq_no = I_seq_no;
      
      open C_LOCK_TTAIL;
      close C_LOCK_TTAIL;
      
      update svc_wf_cost_tmpl_upld_ttail tt
         set tt.status = CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESSED
       where tt.process_id = I_process_id
         and tt.seq_no = I_seq_no;
   
   end if;

   commit;

EXCEPTION
   when OTHERS then
      if C_LOCK_FHEAD%ISOPEN then
         close C_LOCK_FHEAD;
      end if;
      
      if C_LOCK_FTAIL%ISOPEN then
         close C_LOCK_FTAIL;
      end if;
      
      if C_LOCK_THEAD%ISOPEN then
         close C_LOCK_THEAD;
      end if;
      
      if C_LOCK_TDETL%ISOPEN then
         close C_LOCK_TDETL;
      end if;
      
      if C_LOCK_TTAIL%ISOPEN then
         close C_LOCK_TTAIL;
      end if;
      
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_REJECT_PARAMS(I_error_message   IN VARCHAR2,
                              I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                              I_seq_no          IN SVC_WF_COST_TMPL_UPLD_FHEAD.SEQ_NO%TYPE,
                              I_error_rowid     IN VARCHAR2 DEFAULT NULL,
                              I_table_name      IN VARCHAR2 DEFAULT NULL) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.WRITE_REJECT_PARAMS';
   
   cursor C_LOCK_THEAD is
      select 'x'
        from svc_wf_cost_tmpl_upld_thead th
       where th.process_id = I_process_id
         and th.seq_no = I_seq_no
         for update of th.error_msg, th.status nowait;

         
   cursor C_LOCK_TTAIL is
      select 'x'
        from svc_wf_cost_tmpl_upld_ttail tt
       where tt.process_id = I_process_id
         and tt.seq_no = I_seq_no
         for update of tt.error_msg, tt.status nowait;
         
         
   cursor C_LOCK_TDETL is
      select 'x'
        from svc_wf_cost_tmpl_upld_tdetl td
       where td.process_id = I_process_id
         and td.seq_no = I_seq_no
         and td.rowid = I_error_rowid
         for update of td.error_msg, td.status nowait; 
      
BEGIN

   if (I_table_name = CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD and
       I_seq_no is NOT NULL and I_seq_no > 0) then
      
      open C_LOCK_THEAD;
      close C_LOCK_THEAD;
      
      update svc_wf_cost_tmpl_upld_thead th
         set th.error_msg = th.error_msg ||';'||I_error_message,
             th.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
       where th.process_id = I_process_id
         and th.seq_no = I_seq_no;
      
   end if;
   
   if (I_table_name = CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL and
       I_seq_no is NOT NULL and I_seq_no > 0) then
   
      open C_LOCK_TDETL;
      close C_LOCK_TDETL;
      
      update svc_wf_cost_tmpl_upld_tdetl td
         set td.error_msg = I_error_message,
             td.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
       where td.process_id = I_process_id
         and td.seq_no = I_seq_no
         and td.rowid = I_error_rowid;
      
      open C_LOCK_THEAD;
      close C_LOCK_THEAD;
      
      update svc_wf_cost_tmpl_upld_thead th
         set th.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
       where th.process_id = I_process_id
         and th.seq_no = I_seq_no;

   end if;

   if (I_table_name = CORESVC_WF_COST_TMPL_UPLD_SQL.TTAIL and
       I_seq_no is NOT NULL and I_seq_no > 0) then
      
      open C_LOCK_TTAIL;
      close C_LOCK_TTAIL;
      
      update svc_wf_cost_tmpl_upld_ttail tt
         set tt.error_msg = I_error_message,
             tt.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
       where tt.process_id = I_process_id
         and tt.seq_no = I_seq_no;
      
      open C_LOCK_THEAD;
      close C_LOCK_THEAD;
      
      update svc_wf_cost_tmpl_upld_thead th
         set th.error_msg = I_error_message,
             th.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
       where th.process_id = I_process_id
         and th.seq_no = I_seq_no;

   end if;
   
   if (I_table_name = CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL and
       I_seq_no is NOT NULL and I_seq_no < 1) then
   
      open C_LOCK_TDETL;
      close C_LOCK_TDETL;
      
      update svc_wf_cost_tmpl_upld_tdetl td
         set td.error_msg = I_error_message,
             td.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
       where td.process_id = I_process_id
         and td.seq_no = I_seq_no;         
   end if;
  
   if (I_table_name = CORESVC_WF_COST_TMPL_UPLD_SQL.TTAIL and
       I_seq_no is NOT NULL and I_seq_no < 1) then
   
      open C_LOCK_TTAIL;
      close C_LOCK_TTAIL;
      
      update svc_wf_cost_tmpl_upld_tdetl td
         set td.error_msg = I_error_message,
             td.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
       where td.process_id = I_process_id
         and td.seq_no = I_seq_no;         
      
      open C_LOCK_TTAIL;
      close C_LOCK_TTAIL;
      
      update svc_wf_cost_tmpl_upld_ttail tt
         set tt.error_msg = I_error_message,
             tt.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
       where tt.process_id = I_process_id
         and tt.seq_no = I_seq_no;
         
   end if;
   
   commit;
  
EXCEPTION
   when OTHERS then
   
      if C_LOCK_THEAD%ISOPEN then
         close C_LOCK_THEAD;
      end if;
      
      if C_LOCK_TDETL%ISOPEN then
         close C_LOCK_TDETL;
      end if;
      
      if C_LOCK_TTAIL%ISOPEN then
         close C_LOCK_TTAIL;
      end if;
      
      rollback;
      
END WRITE_REJECT_PARAMS;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS(I_process_id      IN SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                        I_chunk_id        IN SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program     VARCHAR2(64)                              := 'CORESVC_WF_COST_TMPL_UPLD_SQL.WRITE_SUCCESS';
   L_max_chunk   SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE := NULL;

   cursor C_LOCK_UPLD_STATUS is
      select 'x'
        from svc_wf_cost_tmpl_upld_status s
       where s.process_id = I_process_id
         and s.chunk_id   = I_chunk_id
         for update of s.status nowait;

BEGIN

   open C_LOCK_UPLD_STATUS;
   close C_LOCK_UPLD_STATUS;

   update svc_wf_cost_tmpl_upld_status
      set status    = CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESSED,
          last_update_datetime = sysdate
    where process_id = I_process_id
      and chunk_id   = I_chunk_id;

   commit;

EXCEPTION
   when OTHERS then
      
      if C_LOCK_UPLD_STATUS%ISOPEN then
         close C_LOCK_UPLD_STATUS;
      end if;
      
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_HEAD_TABLE_TL(O_error_message  IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                     I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(74) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.LOCK_COSTTMPL_HEAD_TABLE_TL';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_COST_TMPL_HEAD_TL is
      select wcbth.templ_id
        from wf_cost_buildup_tmpl_hd_tl wcbth
       where wcbth.templ_id in (select th.template_id
                                  from svc_wf_cost_tmpl_upld_thead th
                                 where th.templATE_id = wcbth.templ_id
                                   and th.process_id  = I_process_id
                                   and th.seq_no      = I_seq_no)
         for update nowait;

BEGIN
   
   L_table := 'WF_COST_BUILDUP_TMPL_HD_TL';
   
   -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
   FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
      
      BEGIN
         SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_HEAD_TL',L_table,NULL);
         open C_LOCK_COST_TMPL_HEAD_TL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_HEAD_TL',L_table,NULL);
         close C_LOCK_COST_TMPL_HEAD_TL;
         L_locked_acquired := 'Y';
         exit;
      EXCEPTION
         when RECORD_LOCKED then
            NULL;
      END;
      
      -- Sleep before the next retry
      DBMS_LOCK.SLEEP(LP_retry_wait_time);
      ---
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_HEAD_TL',L_table,NULL);
      open C_LOCK_COST_TMPL_HEAD_TL;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_HEAD_TL',L_table,NULL);
      close C_LOCK_COST_TMPL_HEAD_TL;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_RECORD_LOCKED',
                                                  L_table,
                                                  NULL,
                                                  NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_COST_TMPL_HEAD_TL%ISOPEN then
         close C_LOCK_COST_TMPL_HEAD_TL;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_COSTTMPL_HEAD_TABLE_TL;
---------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_HEAD_TABLE(O_error_message  IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                  I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.LOCK_COSTTMPL_HEAD_TABLE';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_COST_TMPL_HEAD is
      select wcbth.templ_id
        from wf_cost_buildup_tmpl_head wcbth
       where wcbth.templ_id in (select th.template_id
                                  from svc_wf_cost_tmpl_upld_thead th
                                 where th.templATE_id = wcbth.templ_id
                                   and th.process_id = I_process_id
                                   and th.seq_no = I_seq_no)
         for update nowait;

BEGIN
   
   L_table := 'WF_COST_BUILDUP_TMPL_HEAD';
   
   -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
   FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
      
      BEGIN
         SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_HEAD',L_table,NULL);
         open C_LOCK_COST_TMPL_HEAD;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_HEAD',L_table,NULL);
         close C_LOCK_COST_TMPL_HEAD;
         L_locked_acquired := 'Y';
         exit;
      EXCEPTION
         when RECORD_LOCKED then
            NULL;
      END;
      
      -- Sleep before the next retry
      DBMS_LOCK.SLEEP(LP_retry_wait_time);
      ---
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_HEAD',L_table,NULL);
      open C_LOCK_COST_TMPL_HEAD;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_HEAD',L_table,NULL);
      close C_LOCK_COST_TMPL_HEAD;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_RECORD_LOCKED',
                                                  L_table,
                                                  NULL,
                                                  NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_COST_TMPL_HEAD%ISOPEN then
         close C_LOCK_COST_TMPL_HEAD;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_COSTTMPL_HEAD_TABLE;
---------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_RELN_TABLE(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                  I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.LOCK_COSTTMPL_RELN_TABLE';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor c_lock_cost_tmpl_reln is 
      select wcr.templ_id
        from wf_cost_relationship wcr
       where wcr.rowid in (select wcri.rowid 
                             from wf_cost_relationship wcri,
                                  svc_wf_cost_tmpl_upld_thead th,
                                  svc_wf_cost_tmpl_upld_tdetl td
                            where wcri.templ_id = th.template_id
                              and th.process_id  = td.process_id
                              and th.seq_no = td.seq_no
                              and th.template_id = wcri.templ_id
                              and td.location =  wcri.location
                              and td.start_date = wcri.start_date
                              and td.end_date = wcri.end_date
                              and ((th.first_applied ! = 'C' and td.dept = wcri.dept and nvl(td.class, '-1') = wcri.class  and nvl(td.subclass, '-1') = wcri.subclass)
                                   or
                                  (th.first_applied  = 'C' and  td.item = wcri.item))
                              and td.process_id = i_process_id
                              and td.seq_no = i_seq_no)
         for update nowait;

BEGIN
   
   L_table := 'WF_COST_RELATIONSHIP';
   
   -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
   FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
   
      BEGIN
         SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_RELN',L_table,NULL);
         open C_LOCK_COST_TMPL_RELN;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_RELN',L_table,NULL);
         close C_LOCK_COST_TMPL_RELN;
         L_locked_acquired := 'Y';
         exit;
      EXCEPTION
         when RECORD_LOCKED then
            NULL;
      END;
      -- Sleep before the next retry
      DBMS_LOCK.SLEEP(LP_retry_wait_time);
      
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_RELN',L_table,NULL);
      open C_LOCK_COST_TMPL_RELN;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_RELN',L_table,NULL);
      close C_LOCK_COST_TMPL_RELN;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_RECORD_LOCKED',
                                                  L_table,
                                                  NULL,
                                                  NULL);
      return FALSE;
   when OTHERS then
      
      if C_LOCK_COST_TMPL_RELN%ISOPEN then
         close C_LOCK_COST_TMPL_RELN;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END LOCK_COSTTMPL_RELN_TABLE;
---------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_DETL_TABLE_TL(O_error_message  IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id     IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                     I_seq_no         IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(74) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.LOCK_COSTTMPL_DETL_TABLE_TL';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_COST_TMPL_DETL_TL is 
      select wcbtd.templ_id
        from wf_cost_buildup_tmpl_dtl_tl wcbtd
       where wcbtd.rowid in (select wcbtd1.rowid 
                               from wf_cost_buildup_tmpl_dtl_tl wcbtd1,
                                    svc_wf_cost_tmpl_upld_thead th,
                                    svc_wf_cost_tmpl_upld_tdetl td
                              where wcbtd1.templ_id = th.template_id
                                and th.process_id  = td.process_id
                                and th.seq_no = td.seq_no
                                and wcbtd1.cost_comp_id = td.cost_comp_id 
                                and td.process_id = I_process_id
                                and td.seq_no = I_seq_no)
         for update nowait;

BEGIN
   
   L_table := 'WF_COST_BUILDUP_TMPL_DTL_TL';
   -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
   FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
   
      BEGIN
         SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_DETL_TL',L_table,NULL);
         open C_LOCK_COST_TMPL_DETL_TL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_DETL_TL',L_table,NULL);
         close C_LOCK_COST_TMPL_DETL_TL;
         L_locked_acquired := 'Y';
         exit;
      EXCEPTION
         when RECORD_LOCKED then
            NULL;
      END;
      -- Sleep before the next retry
      DBMS_LOCK.SLEEP(LP_retry_wait_time);
      
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_DETL_TL',L_table,NULL);
      open C_LOCK_COST_TMPL_DETL_TL;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_DETL_TL',L_table,NULL);
      close C_LOCK_COST_TMPL_DETL_TL;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_RECORD_LOCKED',
                                                  L_table,
                                                  NULL,
                                                  NULL);
      return FALSE;
   when OTHERS then
      
      if C_LOCK_COST_TMPL_DETL_TL%ISOPEN then
         close C_LOCK_COST_TMPL_DETL_TL;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_COSTTMPL_DETL_TABLE_TL;
---------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_COSTTMPL_DETL_TABLE(O_error_message  IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                  I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.LOCK_COSTTMPL_DETL_TABLE';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_COST_TMPL_DETL is 
      select wcbtd.templ_id
        from wf_cost_buildup_tmpl_detail wcbtd
       where wcbtd.rowid in (select wcbtd1.rowid 
                               from wf_cost_buildup_tmpl_detail wcbtd1,
                                    svc_wf_cost_tmpl_upld_thead th,
                                    svc_wf_cost_tmpl_upld_tdetl td
                              where wcbtd1.templ_id = th.template_id
                                and th.process_id  = td.process_id
                                and th.seq_no = td.seq_no
                                and wcbtd1.cost_comp_id = td.cost_comp_id 
                                and td.process_id = I_process_id
                                and td.seq_no = I_seq_no)
         for update nowait;

BEGIN
   
   L_table := 'WF_COST_BUILDUP_TMPL_DETAIL';
   -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
   FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
   
      BEGIN
         SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_DETL',L_table,NULL);
         open C_LOCK_COST_TMPL_DETL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_DETL',L_table,NULL);
         close C_LOCK_COST_TMPL_DETL;
         L_locked_acquired := 'Y';
         exit;
      EXCEPTION
         when RECORD_LOCKED then
            NULL;
      END;
      -- Sleep before the next retry
      DBMS_LOCK.SLEEP(LP_retry_wait_time);
      
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_TMPL_DETL',L_table,NULL);
      open C_LOCK_COST_TMPL_DETL;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_TMPL_DETL',L_table,NULL);
      close C_LOCK_COST_TMPL_DETL;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_RECORD_LOCKED',
                                                  L_table,
                                                  NULL,
                                                  NULL);
      return FALSE;
   when OTHERS then
      
      if C_LOCK_COST_TMPL_DETL%ISOPEN then
         close C_LOCK_COST_TMPL_DETL;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_COSTTMPL_DETL_TABLE;
--------------------------------------------------------------------------------------
FUNCTION COST_COMP_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_comp_exists     IN OUT   BOOLEAN,
                          I_templ_id        IN       WF_COST_BUILDUP_TMPL_DETAIL.TEMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.COST_COMP_EXISTS';
   L_exists          VARCHAR2(1)  := NULL;
   L_table           VARCHAR2(30) := NULL;

   cursor C_CHECK_EXISTS is
      select 'x'
        from wf_cost_buildup_tmpl_detail
       where templ_id = I_templ_id;

BEGIN
   
   L_table := 'WF_COST_BUILDUP_TMPL_DETAIL';
   if I_templ_id is NULL then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                                  'I_templ_id',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS',L_table,NULL);
   open C_CHECK_EXISTS;   
   SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS',L_table,NULL);
   fetch C_CHECK_EXISTS into L_exists;   
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS',L_table,NULL);
   close C_CHECK_EXISTS;

   if L_exists is NULL then
      O_comp_exists := FALSE;
   else
      O_comp_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
        
        if C_CHECK_EXISTS%ISOPEN then
           close C_CHECK_EXISTS;
        end if;
        
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END COST_COMP_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION DUP_RELN_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_dup_reln_exists IN OUT  BOOLEAN,
                          O_dup_reln_rowid  IN OUT  VARCHAR2,
                          I_dept            IN      WF_COST_RELATIONSHIP.DEPT%TYPE,
                          I_class           IN      WF_COST_RELATIONSHIP.CLASS%TYPE,
                          I_subclass        IN      WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                          I_location        IN      WF_COST_RELATIONSHIP.LOCATION%TYPE,
                          I_start_date      IN      WF_COST_RELATIONSHIP.START_DATE%TYPE,
                          I_end_date        IN      WF_COST_RELATIONSHIP.END_DATE%TYPE,
                          I_item            IN      WF_COST_RELATIONSHIP.ITEM%TYPE)
RETURN BOOLEAN is

   L_program   VARCHAR2(64) := 'COST_BUILDUP_SQL.DUP_RELN_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;
   L_rowid     VARCHAR2(20) := NULL;

   cursor C_DUP_RELN_EXISTS is
      select w.rowid
        from wf_cost_relationship w
       where w.dept    = I_dept
         and w.class   = I_class
         and w.subclass = I_subclass
         and w.location = I_location
         and w.start_date = I_start_date 
         and w.end_date   = I_end_date
         and NVL(w.item,'-1') = NVL(I_item,'-1')
         and rownum = 1;

BEGIN

   -- check required parameters
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_subclass',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_start_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_end_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_end_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('OPEN','C_DUP_RELN_EXISTS','WF_COST_RELATIONSHIP',NULL);
   open C_DUP_RELN_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_DUP_RELN_EXISTS','WF_COST_RELATIONSHIP',NULL);
   fetch C_DUP_RELN_EXISTS into L_rowid;
   SQL_LIB.SET_MARK('CLOSE','C_DUP_RELN_EXISTS','WF_COST_RELATIONSHIP',NULL);
   close C_DUP_RELN_EXISTS;

   if L_rowid is NULL then
      O_dup_reln_exists := FALSE;
   else
      O_dup_reln_exists := TRUE;
   end if;
   
   O_dup_reln_rowid := L_rowid;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DUP_RELN_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_PROCESS_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                                   I_reference_id    IN     SVC_WF_COST_TMPL_UPLD_STATUS.REFERENCE_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.INITIALIZE_PROCESS_STATUS';

BEGIN

   -- Validate required input
   if I_process_id is NULL then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                                  'I_process_id',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;

   -- Insert initial status record with chunk 0
   insert into svc_wf_cost_tmpl_upld_status (process_id,
                                             chunk_id,
                                             reference_id,
                                             status,
                                             last_update_datetime)
                                     values (I_process_id,
                                             0,
                                             I_reference_id,
                                             CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD,
                                             sysdate);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  L_program,
                                                  to_char(SQLCODE));
      return FALSE;
END INITIALIZE_PROCESS_STATUS;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PARAMETER_TABLES(O_error_message   IN OUT NOCOPY VARCHAR2,
                                   I_process_id      IN SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'SVC_WF_COST_TMPL_UPLD_FHEAD.VALIDATE_PARAMETER_TABLES';
   L_typ_frecs            TYP_fctmpl_fhead_rec;
   L_typ_trecs_tbl        TYP_fctmpl_tran_tbl;
   L_typ_tdetl_recs_tbl   TYP_fctmpl_tdetl_tbl;
   L_dummy                VARCHAR2(1);
   L_error_message        VARCHAR2(4000):= NULL;
   L_overlap_exists       BOOLEAN;
   L_exists               BOOLEAN;
   L_table                VARCHAR2(255) := NULL;   

   cursor C_GET_FHEAD_REC is
      select fh.process_id as process_id,
             count(distinct fh.rowid) as fhead_count,
             count(distinct ft.rowid) as ftail_count,
             count(distinct th.rowid) + count(distinct td.rowid) + count(distinct tt.rowid) as tran_recs_cnt,
             max(ft.file_record_counter) as file_rec_cntr,
             max(fh.file_type) as file_type,
             count(distinct th.rowid) as thead_count,
             count(distinct td.rowid) as tdetl_count,
             count(distinct tt.rowid) as ttail_count
        from svc_wf_cost_tmpl_upld_fhead fh,
             svc_wf_cost_tmpl_upld_thead th,
             svc_wf_cost_tmpl_upld_tdetl td,
             svc_wf_cost_tmpl_upld_ttail tt,
             svc_wf_cost_tmpl_upld_ftail ft
       where fh.process_id = ft.process_id(+)
         and fh.process_id = th.process_id(+)
         and fh.process_id = td.process_id(+)
         and fh.process_id = tt.process_id(+)
         and fh.status = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD
         and th.status (+) = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD
         and td.status (+) = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD
         and tt.status (+) = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD
         and ft.status (+) = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD
         and fh.process_id = I_process_id
    group by fh.process_id;
         
         
   cursor C_GET_TRAN_REC is
      select th.process_id as process_id,
             th.seq_no as seq_no,
             count(distinct th.rowid) as thead_count,
             count(td.rowid) as tdetl_count,
             count(distinct tt.rowid) as ttail_count,
             max(tt.tran_record_counter) as tran_rec_cntr,
             max(th.message_type) as message_type,
             max(th.template_id) as template_id,
             max(th.template_desc) as template_desc,
             max(th.first_applied) as first_applied,
             max(th.percentage) as percentage,
             max(th.cost) as cost,
             max(th.final_cost) as final_cost,
             max(wcth.templ_id) as existing_templ_id
        from svc_wf_cost_tmpl_upld_thead th,
             svc_wf_cost_tmpl_upld_tdetl td,
             svc_wf_cost_tmpl_upld_ttail tt,
             wf_cost_buildup_tmpl_head wcth
       where th.seq_no = td.seq_no(+)
         and th.seq_no   = tt.seq_no(+)
         and th.process_id = td.process_id(+)
         and th.process_id = tt.process_id(+)
         and th.template_id = wcth.templ_id(+)
         and th.status (+) = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD
         and td.status (+) = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD
         and tt.status (+) = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD
         and th.process_id = I_process_id
    group by th.process_id,th.seq_no;
   
        
   cursor C_GET_TDETL_REC is
      select td.rowid,
             td.process_id,
             td.seq_no,
             td.message_type,
             td.dept,
             td.class,
             td.subclass,
             td.item,
             td.location,
             td.start_date,
             td.end_date,
             td.new_start_date,
             td.new_end_date,
             td.cost_comp_id,
             s.store_type,
             s.store,
             th.message_type,       
             th.first_applied,      
             im.item,
             im.status,
             im.item_level,
             im.tran_level,
             im.orderable_ind,
             decode(th.first_applied,'C',sc.subclass,decode(td.dept, null,null,decode(td.class,null,d.dept,decode(td.subclass,null,c.class,sc.subclass)))) map_merch_hier,
             ec.comp_id,
             ec.comp_type,
             ec.up_chrg_group
        from svc_wf_cost_tmpl_upld_tdetl td,
             svc_wf_cost_tmpl_upld_thead th,
             store s,
             deps d,
             class c,
             subclass sc,
             item_master im,
             elc_comp ec
       where td.process_id = th.process_id
         and td.seq_no = th.seq_no
         and (td.subclass = sc.subclass (+) and td.dept = sc.dept(+) and td.class = sc.class(+))
         and td.dept = d.dept(+)
         and td.location = s.store(+)
         and td.class = c.class(+)
         and td.dept = c.dept(+)
         and td.subclass = sc.subclass(+)
         and td.item = im.item(+)
         and td.cost_comp_id = ec.comp_id(+)
         and td.process_id = I_process_id
         and td.status = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD;
   
   cursor C_TDETL_ORPHANS is
      select 'x'
        from svc_wf_cost_tmpl_upld_tdetl td 
       where td.process_id = I_process_id
         and td.seq_no < 1;
         
   cursor C_TTAIL_ORPHANS is
      select 'x'
        from svc_wf_cost_tmpl_upld_ttail tt
       where tt.process_id = I_process_id
         and tt.seq_no < 1;
   
BEGIN
   -------------------------------------------------------------------------------------
   -- VALIDATE REQUIRED INPUT
   -------------------------------------------------------------------------------------
   
   if I_process_id is NULL then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                                  'I_process_id',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;

   -------------------------------------------------------------------------------------
   -- SVC_WF_COST_TMPL_UPLD_FHEAD and SVC_WF_COST_TMPL_UPLD_FTAIL VALIDATION
   -------------------------------------------------------------------------------------
   
   L_table := 'SVC_WF_COST_TMPL_UPLD_FHEAD, SVC_WF_COST_TMPL_UPLD_THEAD, SVC_WF_COST_TMPL_UPLD_TDETL, SVC_WF_COST_TMPL_UPLD_TTAIL, SVC_WF_COST_TMPL_UPLD_FTAIL';
   
   SQL_LIB.SET_MARK('OPEN','C_GET_FHEAD_REC',L_table,NULL);
   open C_GET_FHEAD_REC;
   SQL_LIB.SET_MARK('FETCH','C_GET_FHEAD_REC',L_table,NULL);
   fetch C_GET_FHEAD_REC into L_typ_frecs;
   SQL_LIB.SET_MARK('CLOSE','C_GET_FHEAD_REC',L_table,NULL);
   close C_GET_FHEAD_REC;
   
   if (L_typ_frecs.process_id is NULL) then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MISSING_FHEAD',
                                                  I_process_id,
                                                  NULL,
                                                  NULL);
      
      WRITE_ERROR(I_process_id,
                  0,
                  L_error_message);
      return FALSE;
   end if;
   
   if L_typ_frecs.fhead_count > 1 then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INV_FHEAD_COUNT',
                                                  I_process_id,
                                                  NULL,
                                                  NULL);
                                                  
      WRITE_ERROR_PARAMS(L_error_message, I_process_id, 0, CORESVC_WF_COST_TMPL_UPLD_SQL.FHEAD);
      
      WRITE_ERROR(I_process_id,
                  0,
                  L_error_message);
      return FALSE;
   end if;
   
   if (L_typ_frecs.file_type != 'CTMPL')  then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INV_FILE_TYPE',
                                                  I_process_id,
                                                  NULL,
                                                  NULL);
          
      WRITE_ERROR_PARAMS(L_error_message, I_process_id, 0, CORESVC_WF_COST_TMPL_UPLD_SQL.FHEAD);
          
      WRITE_ERROR(I_process_id,
                  0,
                  L_error_message);
      return FALSE;
   end if;
      
   if L_typ_frecs.ftail_count < 1 then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MISSING_FTAIL',
                                                  I_process_id,
                                                  NULL,
                                                  NULL);
          
      WRITE_ERROR(I_process_id,
                 0,
                 L_error_message);
      return FALSE;
   end if;
   
   if L_typ_frecs.ftail_count > 1 then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INV_FTAIL_COUNT',
                                                  I_process_id,
                                                  NULL,
                                                  NULL);
                                                  
      WRITE_ERROR_PARAMS(L_error_message, I_process_id, 0, CORESVC_WF_COST_TMPL_UPLD_SQL.FTAIL);
      
      WRITE_ERROR(I_process_id,
                  0,
                  L_error_message);
      return FALSE;
   end if;
   
   if L_typ_frecs.frecs_count != L_typ_frecs.file_rec_cntr then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_COUNT_NOT_MATCH',
                                                  I_process_id,
                                                  NULL,
                                                  NULL);
                                                  
      WRITE_ERROR_PARAMS(L_error_message, I_process_id, 0, CORESVC_WF_COST_TMPL_UPLD_SQL.FTAIL);
      
      WRITE_ERROR(I_process_id,
                  0,
                  L_error_message);
      return FALSE;
   end if;
   
   if L_typ_frecs.thead_count < 1 and (L_typ_frecs.tdetl_count > 1 or L_typ_frecs.ttail_count > 1) then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_FILE_THEAD_CNT_0',
                                                  I_process_id,
                                                  NULL,
                                                  NULL);
                                                  
      WRITE_ERROR_PARAMS(L_error_message, I_process_id, 0, CORESVC_WF_COST_TMPL_UPLD_SQL.FHEAD);
      
      WRITE_ERROR(I_process_id,
                  0,
                  L_error_message);
      return FALSE;       
      
   end if;
   
   -------------------------------------------------------------------------------------
   -- SVC_WF_COST_TMPL_UPLD_TDETL and SVC_WF_COST_TMPL_UPLD_TTAIL VALIDATION FOR ORPHANS
   -------------------------------------------------------------------------------------
   
   -- Reject detail records which have sequence no as 0
   SQL_LIB.SET_MARK('OPEN','C_TDETL_ORPHANS','SVC_WF_COST_TMPL_UPLD_TDETL',NULL);
   open C_TDETL_ORPHANS;
   SQL_LIB.SET_MARK('FETCH','C_TDETL_ORPHANS','SVC_WF_COST_TMPL_UPLD_TDETL',NULL);
   fetch C_TDETL_ORPHANS into L_dummy;
   if C_TDETL_ORPHANS%FOUND then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MISSING_THEAD',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      WRITE_REJECT_PARAMS(L_error_message, I_process_id, 0,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_TDETL_ORPHANS','SVC_WF_COST_TMPL_UPLD_TDETL',NULL);
   close C_TDETL_ORPHANS;
   
   -- Reject tail records which have sequence no as 0
   SQL_LIB.SET_MARK('OPEN','C_TTAIL_ORPHANS','SVC_WF_COST_TMPL_UPLD_TTAIL',NULL);   
   open C_TTAIL_ORPHANS;
   SQL_LIB.SET_MARK('FETCH','C_TTAIL_ORPHANS','SVC_WF_COST_TMPL_UPLD_TTAIL',NULL);   
   fetch C_TTAIL_ORPHANS into L_dummy;
   if C_TTAIL_ORPHANS%FOUND then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MISSING_THEAD',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      WRITE_REJECT_PARAMS(L_error_message, I_process_id, 0,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.TTAIL);
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_TTAIL_ORPHANS','SVC_WF_COST_TMPL_UPLD_TTAIL',NULL);
   close C_TTAIL_ORPHANS;
   
   L_error_message := NULL;
   
   -------------------------------------------------------------------------------------
   -- SVC_WF_COST_TMPL_UPLD_THEAD VALIDATION
   -------------------------------------------------------------------------------------
   
   L_table := 'SVC_WF_COST_TMPL_UPLD_THEAD, SVC_WF_COST_TMPL_UPLD_TDETL, SVC_WF_COST_TMPL_UPLD_TTAIL, WF_COST_BUILDUP_TMPL_HEAD WCTH';
   
   SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_REC',L_table,NULL);
   open  C_GET_TRAN_REC;       
   SQL_LIB.SET_MARK('FETCH','C_GET_TRAN_REC',L_table,NULL);
   fetch C_GET_TRAN_REC BULK COLLECT into L_typ_trecs_tbl;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRAN_REC',L_table,NULL);
   close C_GET_TRAN_REC;
   
   FOR i in L_typ_trecs_tbl.first .. L_typ_trecs_tbl.last 
   LOOP
      L_error_message := NULL;
      
      
      if L_typ_trecs_tbl(i).thead_count < 1 then
         L_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MISSING_THEAD',
                                                     NULL,
                                                     NULL,
                                                     NULL);
           
      end if;      
      
      if L_typ_trecs_tbl(i).thead_count > 1 then 
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INV_THEAD_COUNT',
                                                                             I_process_id,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if L_typ_trecs_tbl(i).ttail_count < 1 then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MISSING_TTAIL',
                                                                             I_process_id,
                                                                             NULL,
                                                                             NULL);
      end if;
   
      if L_typ_trecs_tbl(i).message_type NOT IN (CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_ADD, CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_MOD, CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_DEL) or 
         L_typ_trecs_tbl(i).message_type is NULL then
             
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INV_MESSAGE_TYPE',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if L_typ_trecs_tbl(i).template_id is NULL or L_typ_trecs_tbl(i).template_id < 0 then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MISSING_TEMPLID',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if L_typ_trecs_tbl(i).template_desc is NULL  then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MISSING_TMPLDESC',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if (L_typ_trecs_tbl(i).first_applied NOT IN ('U','M','R','C') or L_typ_trecs_tbl(i).first_applied is NULL) then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INVALID_FAPPLIED',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if L_typ_trecs_tbl(i).first_applied IN ('M','R') and (L_typ_trecs_tbl(i).percentage is NULL or L_typ_trecs_tbl(i).percentage < 0) then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INVALID_PERCENT',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if L_typ_trecs_tbl(i).first_applied = 'R' and L_typ_trecs_tbl(i).percentage is NOT NULL and L_typ_trecs_tbl(i).percentage > 100 then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('PCT_OFF_RETAIL_100',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if L_typ_trecs_tbl(i).first_applied = 'C' and (L_typ_trecs_tbl(i).cost is NULL or L_typ_trecs_tbl(i).cost < 0)  then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INVALID_COST',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if L_typ_trecs_tbl(i).first_applied = 'C' and (L_typ_trecs_tbl(i).final_cost NOT IN ('Y','N') or L_typ_trecs_tbl(i).final_cost is NULL) then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INVALID_FIN_COST',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      if L_typ_trecs_tbl(i).first_applied IN ('M','R','U') and (L_typ_trecs_tbl(i).cost is NOT NULL or L_typ_trecs_tbl(i).final_cost is NOT NULL) then
        L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_COST_FINAL_NULL',
                                                                            NULL,
                                                                            NULL,
                                                                            NULL);
      end if;

      -- Write reject to the parameter tables
      if L_error_message is NOT NULL then
         WRITE_REJECT_PARAMS(L_error_message, I_process_id, L_typ_trecs_tbl(i).seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
      end if;
     
      ---------------------------------------------------
      -- SVC_WF_COST_TMPL_UPLD_TTAIL VALIDATION
      ---------------------------------------------------
      L_error_message := NULL;
      
      if L_typ_trecs_tbl(i).ttail_count > 1 then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_INV_TTAIL_COUNT',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;                                   
      if L_typ_trecs_tbl(i).tdetl_count != L_typ_trecs_tbl(i).tran_rec_cntr then
         L_error_message := L_error_message ||';'|| SQL_LIB.GET_MESSAGE_TEXT('CTMPL_TRECCNTR_NOMATCH',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL);
      end if;
      
      -- Write reject to the parameter tables
      if L_error_message is NOT NULL then
         WRITE_REJECT_PARAMS(L_error_message, I_process_id, L_typ_trecs_tbl(i).seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.TTAIL);
      end if;
   END LOOP;
   
   ---------------------------------------------------
   -- SVC_WF_COST_TMPL_UPLD_TDETL VALIDATION
   ---------------------------------------------------
   
   L_table := 'SVC_WF_COST_TMPL_UPLD_TDETL, SVC_WF_COST_TMPL_UPLD_THEAD, STORE, DEPS, CLASS, SUBCLASS, ITEM_MASTER, ELC_COMP';
   SQL_LIB.SET_MARK('OPEN','C_GET_TDETL_REC',L_table,NULL);
   open C_GET_TDETL_REC; 
   SQL_LIB.SET_MARK('FETCH','C_GET_TDETL_REC',L_table,NULL);
   fetch C_GET_TDETL_REC BULK COLLECT into L_typ_tdetl_recs_tbl;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TDETL_REC',L_table,NULL);
   close C_GET_TDETL_REC;
   
   if L_typ_tdetl_recs_tbl.count > 0 and L_typ_tdetl_recs_tbl is NOT NULL then
      
      FOR i in L_typ_tdetl_recs_tbl.first .. L_typ_tdetl_recs_tbl.last 
      LOOP
         L_error_message := NULL;
      
         if L_typ_tdetl_recs_tbl(i).message_type NOT IN (CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_ADD, CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_MOD, CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_DEL) or
            L_typ_tdetl_recs_tbl(i).message_type is NULL then
          
            L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_DTL_INV_MSG_TYPE',
                                                                               NULL,
                                                                               NULL,
                                                                               NULL);
         end if;
      
         if L_typ_tdetl_recs_tbl(i).thead_msg_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_ADD and
            L_typ_tdetl_recs_tbl(i).message_type != CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_ADD then
         
            L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MSG_TYP_ADD_ERR',
                                                                               NULL,
                                                                               NULL,
                                                                               NULL);
         end if;
      
         if L_typ_tdetl_recs_tbl(i).thead_msg_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_DEL and
            L_typ_tdetl_recs_tbl(i).message_type != CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_DEL then
         
            L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_MSG_TYP_DEL_ERR',
                                                                               NULL,
                                                                               NULL,
                                                                               NULL);
         end if;
         
         if L_typ_tdetl_recs_tbl(i).thead_first_applied in ('M','C','R') then
            if (L_typ_tdetl_recs_tbl(i).dept is NULL and L_typ_tdetl_recs_tbl(i).item is NULL) then
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_DETL_MANDATORY',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;
            if L_typ_tdetl_recs_tbl(i).cost_comp_id is NOT NULL then 
      
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_COSTCOMP_NOTREQ',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;
         end if;
         
         if L_typ_tdetl_recs_tbl(i).thead_first_applied in ('M','C','R') or 
            (L_typ_tdetl_recs_tbl(i).thead_first_applied = 'U' and L_typ_tdetl_recs_tbl(i).cost_comp_id is NULL) then
            if (L_typ_tdetl_recs_tbl(i).location is NULL) then
         
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_LOCATION_REQ',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;
      
            if L_typ_tdetl_recs_tbl(i).location is NOT NULL then
               
               if (L_typ_tdetl_recs_tbl(i).store_type != 'F') then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('MUST_BE_FR_STORE',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
                              
               if (L_typ_tdetl_recs_tbl(i).exist_store is NULL) then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('INV_STORE',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
            end if;
            
            if L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_ADD or 
               L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_MOD then
               
               if (L_typ_tdetl_recs_tbl(i).new_start_date is NULL or L_typ_tdetl_recs_tbl(i).new_end_date is NULL) then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_DATES_REQ_ADD_MOD',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
            end if;
            
            if L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_MOD or
               L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_DEL then
               
               if (L_typ_tdetl_recs_tbl(i).start_date is NULL or L_typ_tdetl_recs_tbl(i).end_date is NULL) then
                 
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_DATES_REQ_MOD_DEL',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
            end if;
          
            if L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_MOD or
               L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_ADD then
                       
               if L_typ_tdetl_recs_tbl(i).new_start_date is NOT NULL and L_typ_tdetl_recs_tbl(i).new_end_date is NOT NULL then
               
                  if (L_typ_tdetl_recs_tbl(i).new_start_date > L_typ_tdetl_recs_tbl(i).new_end_date) then
                     L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NEW_DATE_S_BEFORE_E',
                                                                                        NULL,
                                                                                        NULL,
                                                                                         NULL);
                  end if;
                    
                  if (L_typ_tdetl_recs_tbl(i).new_start_date < L_vdate or L_typ_tdetl_recs_tbl(i).new_start_date = L_vdate) then
                     L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_DATE_S_NOT_BEFORE',
                                                                                        L_vdate+1,
                                                                                        NULL,
                                                                                        NULL);
                  end if;
                    
                  if (L_typ_tdetl_recs_tbl(i).new_end_date < L_vdate) then
                     L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_DATE_E_NOT_BEFORE',
                                                                                        NULL,
                                                                                        NULL,
                                                                                        NULL);
                  end if;
               end if;
            end if;
         end if;
      
         if L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_MOD or
            L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_DEL then
                    
            if L_typ_tdetl_recs_tbl(i).start_date is NOT NULL and L_typ_tdetl_recs_tbl(i).end_date is NOT NULL then
            
               if (L_typ_tdetl_recs_tbl(i).start_date > L_typ_tdetl_recs_tbl(i).end_date) then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_OLD_DATE_S_BEFORE_E',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
            end if;
         end if;
      
         if L_typ_tdetl_recs_tbl(i).thead_first_applied in ('M','R') or 
            (L_typ_tdetl_recs_tbl(i).thead_first_applied = 'U' and L_typ_tdetl_recs_tbl(i).cost_comp_id is NULL) then
          
            if (L_typ_tdetl_recs_tbl(i).dept is NULL) then
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_HIER_REQ',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;
            if (L_typ_tdetl_recs_tbl(i).dept is NOT NULL) then
               
               if (L_typ_tdetl_recs_tbl(i).map_merch_hier is NULL and L_typ_tdetl_recs_tbl(i).class_id is NOT NULL 
                                                                  and L_typ_tdetl_recs_tbl(i).subclass_id is NOT NULL) then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('INV_SUBCLASS_CLASS_DEPT',
                                                                                     L_typ_tdetl_recs_tbl(i).subclass_id,
                                                                                     L_typ_tdetl_recs_tbl(i).dept,
                                                                                     L_typ_tdetl_recs_tbl(i).class_id);
               elsif (L_typ_tdetl_recs_tbl(i).map_merch_hier is NULL and L_typ_tdetl_recs_tbl(i).class_id is NOT NULL 
                                                                     and L_typ_tdetl_recs_tbl(i).subclass_id is NULL) then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('INV_CLASS_DEPT',
                                                                                     L_typ_tdetl_recs_tbl(i).class_id,
                                                                                     L_typ_tdetl_recs_tbl(i).dept,
                                                                                     NULL);
               elsif (L_typ_tdetl_recs_tbl(i).map_merch_hier is NULL and L_typ_tdetl_recs_tbl(i).class_id is NULL 
                                                                     and L_typ_tdetl_recs_tbl(i).subclass_id is NULL) then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('INV_DEPT',
                                                                                     L_typ_tdetl_recs_tbl(i).dept,
                                                                                     NULL,
                                                                                     NULL);
               end if;
            end if;
         end if;
      
         if L_typ_tdetl_recs_tbl(i).thead_first_applied ='U' then
         
            if L_typ_tdetl_recs_tbl(i).cost_comp_id is NULL and L_typ_tdetl_recs_tbl(i).dept is NULL then 
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_U_COMP_OR_RELN_REQ',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;
         
            if L_typ_tdetl_recs_tbl(i).cost_comp_id is NOT NULL and L_typ_tdetl_recs_tbl(i).dept is NOT NULL then 
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_U_COMP_OR_RELN',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;
         
            if L_typ_tdetl_recs_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_MOD and L_typ_tdetl_recs_tbl(i).cost_comp_id is NOT NULL then
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_UPCHRG_INVALID_ACT',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;
            
            if L_typ_tdetl_recs_tbl(i).message_type != CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_MOD then
               
               if L_typ_tdetl_recs_tbl(i).cost_comp_id is NOT NULL and L_typ_tdetl_recs_tbl(i).exist_comp_id is NULL then 
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_COMP_NOT_EXIST',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
               if L_typ_tdetl_recs_tbl(i).exist_comp_id is NOT NULL and
                  (L_typ_tdetl_recs_tbl(i).comp_type != 'U' or  L_typ_tdetl_recs_tbl(i).up_chrg_group  != 'W') then 
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('INV_UPCHRG_COMP',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
            end if;
            
            if L_typ_tdetl_recs_tbl(i).cost_comp_id is NOT NULL and (L_typ_tdetl_recs_tbl(i).dept is NOT NULL or L_typ_tdetl_recs_tbl(i).class_id is NOT NULL or 
               L_typ_tdetl_recs_tbl(i).subclass_id is NOT NULL or L_typ_tdetl_recs_tbl(i).location is NOT NULL or
               L_typ_tdetl_recs_tbl(i).start_date is NOT NULL or L_typ_tdetl_recs_tbl(i).end_date is NOT NULL or
               L_typ_tdetl_recs_tbl(i).new_start_date is NOT NULL or L_typ_tdetl_recs_tbl(i).new_end_date is NOT NULL or
               L_typ_tdetl_recs_tbl(i).item is NOT NULL) then 
               
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_HIER_NOT_REQ',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;  
         end if;
      
         if L_typ_tdetl_recs_tbl(i).thead_first_applied ='C' then
         
            if L_typ_tdetl_recs_tbl(i).item is NULL then 
               L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_ITEM_REQ',
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL);
            end if;
            
            if L_typ_tdetl_recs_tbl(i).item is NOT NULL then
               if L_typ_tdetl_recs_tbl(i).exist_item is NULL then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('INVALID_ITEM',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
               if L_typ_tdetl_recs_tbl(i).exist_item is NOT NULL and L_typ_tdetl_recs_tbl(i).item_status != 'A' then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('INV_ITEM_NOT_APPROVED',
                                                                                     L_typ_tdetl_recs_tbl(i).item,
                                                                                     NULL,
                                                                                     NULL);
               end if;
               if L_typ_tdetl_recs_tbl(i).exist_item is NOT NULL and L_typ_tdetl_recs_tbl(i).item_level <> L_typ_tdetl_recs_tbl(i).tran_level then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('MUST_BE_TRAN_LEVEL',
                                                                                     L_typ_tdetl_recs_tbl(i).item,
                                                                                     NULL,
                                                                                     NULL);
               end if;
               if L_typ_tdetl_recs_tbl(i).exist_item is NOT NULL and L_typ_tdetl_recs_tbl(i).orderable_ind != 'Y' then
                  L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('ITEM_NOT_ORDERABLE',
                                                                                     NULL,
                                                                                     NULL,
                                                                                     NULL);
               end if;
               
               if L_typ_tdetl_recs_tbl(i).exist_item is NOT NULL then
                  if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(L_error_message,
                                                                   L_exists,
                                                                   L_typ_tdetl_recs_tbl(i).exist_item,
                                                                  'ITEM_MASTER')= FALSE then
                     WRITE_REJECT_PARAMS(L_error_message, I_process_id, L_typ_tdetl_recs_tbl(i).seq_no,L_typ_tdetl_recs_tbl(i).row_id, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
                  if L_exists then
                     L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_ITEM_DLY_PRG',
                                                                                        NULL,
                                                                                        NULL,
                                                                                        NULL);
                  end if;
               end if;
            end if;
         end if;
      
         if (L_typ_tdetl_recs_tbl(i).thead_first_applied in ('M','U','R') and L_typ_tdetl_recs_tbl(i).item is NOT NULL) then 
      
            L_error_message := L_error_message ||';'||SQL_LIB.GET_MESSAGE_TEXT('CTMPL_ITEM_NOT_REQ',
                                                                               NULL,
                                                                               NULL,
                                                                               NULL);
         end if;
      
         -- Write reject to the parameter tables
         if L_error_message is NOT NULL then
            WRITE_REJECT_PARAMS(L_error_message, I_process_id, L_typ_tdetl_recs_tbl(i).seq_no,L_typ_tdetl_recs_tbl(i).row_id, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
         end if;
      END LOOP;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      
      if C_GET_FHEAD_REC%ISOPEN then
         close C_GET_FHEAD_REC;
      end if;
      
      if C_GET_TRAN_REC%ISOPEN then
         close C_GET_TRAN_REC;
      end if;
      
      if C_GET_TDETL_REC%ISOPEN then
         close C_GET_TDETL_REC;
      end if;
      
      if C_TDETL_ORPHANS%ISOPEN then
         close C_TDETL_ORPHANS;
      end if;
      
      if C_TTAIL_ORPHANS%ISOPEN then
         close C_TTAIL_ORPHANS;
      end if;
      
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  L_program,
                                                  to_char(SQLCODE));
      return FALSE;
END VALIDATE_PARAMETER_TABLES;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHUNK_COST_TMPL_UPLD(O_error_message    IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id       IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.CHUNK_COST_TMPL_UPLD';
   L_max_chunk_size     RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE := NULL;
   L_thead_count        NUMBER(10) := NULL;   
   L_number_of_chunks   NUMBER(10) := NULL;

BEGIN
   -------------------------------------------------------------------------------------
   -- VALIDATE REQUIRED INPUT
   -------------------------------------------------------------------------------------
   if I_process_id is NULL then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -------------------------------------------------------------------------------------
   -- Get the maximum size of a cost template upload transaction
   -------------------------------------------------------------------------------------
   select max_chunk_size into L_max_chunk_size
     from rms_plsql_batch_config
    where program_name = 'CORESVC_WF_COST_TMPL_UPLD_SQL';
   
   -- Check if commit_max_ctr was retrieved
   if L_max_chunk_size is NULL then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_MAX_TRAN_SIZE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Get the number of stkupld detail rows
   select count(*) into L_thead_count
     from svc_wf_cost_tmpl_upld_thead
    where process_id = I_process_id
      and status     = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD;
   
   -- Check if no records were found
   if L_thead_count is NULL or L_thead_count = 0 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_THEADS_FOR_CHUNK',
                                            I_process_id,
                                            NULL,
                                            NULL);
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, 0, CORESVC_WF_COST_TMPL_UPLD_SQL.FHEAD);
      return FALSE;
   end if;

   -- Chunk the records on svc_wf_cost_tmpl_upld_thead
   merge into svc_wf_cost_tmpl_upld_thead target
   using (select ith.process_id,
                 ith.seq_no,
                 ith.status,
                 CEIL(ith.seq_no/L_max_chunk_size) chunk_id  from  svc_wf_cost_tmpl_upld_thead ith
         ) source
      on (target.process_id = source.process_id and
          target.seq_no = source.seq_no and
          source.process_id = I_process_id and
          source.status = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD)
     when matched then
        update set target.chunk_id = source.chunk_id;
      
   -- Check for records that did not get chunked
   -- Return fatal if no records are found
   if L_thead_count <> NVL(SQL%ROWCOUNT, 0) then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_CHUNK_ERROR',
                                            I_process_id,
                                            NULL,
                                            NULL);
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, 0, CORESVC_WF_COST_TMPL_UPLD_SQL.FHEAD);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHUNK_COST_TMPL_UPLD;
---------------------------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_CHUNK_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.INITIALIZE_CHUNK_STATUS';
   L_max_chunk          SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE := NULL;

BEGIN

   -- Validate required input
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Retrieve highest chunk value from the detail table
   select max(chunk_id) into L_max_chunk
     from svc_wf_cost_tmpl_upld_thead th
    where th.process_id = I_process_id
      and th.status = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD;

   -- Generate status records for each chunk
   insert into svc_wf_cost_tmpl_upld_status(process_id,
                                            chunk_id,
                                            reference_id,
                                            status,
                                            last_update_datetime)
                                      select s.process_id,
                                             x.chunk_id,
                                             s.reference_id,
                                             s.status,
                                             sysdate
                                        from (select process_id,
                                                     reference_id,
                                                     status
                                                from svc_wf_cost_tmpl_upld_status
                                               where process_id = I_process_id
                                                 and chunk_id   = 0) s,  --seed record
                                              (select level chunk_id
                                                 from dual
                                              connect by level < L_max_chunk + 1) x;

   -- Delete the initial status records
   delete from svc_wf_cost_tmpl_upld_status s
         where s.process_id = I_process_id
           and s.chunk_id = 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INITIALIZE_CHUNK_STATUS;
---------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_FUTURE_COST(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                             I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESS_FUTURE_COST';
   L_reln_tbl   OBJ_TEMPL_RELN_EVENT_TBL;
   L_reln_rec   OBJ_TEMPL_RELN_EVENT_REC;
   L_action  COST_EVENT.ACTION%TYPE;
   L_user_id COST_EVENT.USER_ID%TYPE DEFAULT user;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;

   
   cursor C_ACTION is
      select distinct action 
        from gtt_wf_cost_relationship;

   cursor C_DATA is
      select dept,           
             class,          
             subclass, 
             location,
             old_start_date,     
             old_end_date,
             new_start_date,
             new_end_date,
             templ_id,
             decode(item,'-1',NULL,item) item
        from gtt_wf_cost_relationship     
       where action = L_action;
BEGIN
   
   
  if SQL_LIB.GET_USER_NAME(O_error_message,
                            L_user_id) = FALSE then
      WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
      return FALSE;
   end if;
   
   
   L_reln_tbl := new OBJ_TEMPL_RELN_EVENT_TBL();
   
   FOR i in C_ACTION LOOP
      L_action := i.action;
      L_reln_tbl.DELETE;
      FOR j in C_DATA LOOP
         L_reln_rec := new OBJ_TEMPL_RELN_EVENT_REC(j.dept,           
                                                    j.class,          
                                                    j.subclass,
                                                    j.location,
                                                    j.old_start_date,     
                                                    j.new_start_date,
                                                    j.old_end_date,
                                                    j.new_end_date,
                                                    j.templ_id,
                                                    j.item);

         L_reln_tbl.EXTEND;
         L_reln_tbl(L_reln_tbl.COUNT) := L_reln_rec;
      END LOOP;
      
      if FUTURE_COST_EVENT_SQL.ADD_COST_TMPL_RELATIONSHIP_CHG(O_error_message,
                                                              L_cost_event_process_id,
                                                              L_reln_tbl,
                                                              L_action,
                                                              L_user_id) = FALSE then
                                            
         WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);                                            
         return FALSE;
      end if;
  
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      
      if C_ACTION%ISOPEN then
         close C_ACTION;
      end if;
      
      if C_DATA%ISOPEN then
         close C_DATA;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_FUTURE_COST;
---------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COST_TMPL_UPLD(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                I_chunk_id        IN     SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESS_COST_TMPL_UPLD';
   L_typ_proc_chunk_tbl   TYP_process_chunk_tbl;
   L_table                VARCHAR2(255) := NULL;
  
   cursor C_GET_TRAN_IN_CHUNK is
      select process_id,
             seq_no
        from svc_wf_cost_tmpl_upld_thead
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and status = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD;
      
BEGIN

   -- Validate required inputs
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chunk_id',
                                            L_program,
                                            NULL);
      
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Retrieve the header row from the parameter table
   -- There should only be one row retrieved as this was validated in the VALIDATE_PARAMETER_TABLES function
   select process_id,
          seq_no,
          file_record_descriptor,
          file_line_id,
          file_type,
          file_create_date,
          status,
          error_msg,
          last_update_datetime
     into GP_costtmplupld_fhead_row
     from svc_wf_cost_tmpl_upld_fhead
    where process_id = I_process_id
      and status = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD;
      
   -- Check if header row was found
   if GP_costtmplupld_fhead_row.process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CTMPL_MISSING_FHEAD',
                                            I_process_id,
                                            NULL,
                                            NULL);
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if; 
   
   L_table := 'SVC_WF_COST_TMPL_UPLD_THEAD';
   
   SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_IN_CHUNK',L_table,NULL);
   open C_GET_TRAN_IN_CHUNK;
   SQL_LIB.SET_MARK('FETCH','C_GET_TRAN_IN_CHUNK',L_table,NULL);
   fetch C_GET_TRAN_IN_CHUNK BULK COLLECT into L_typ_proc_chunk_tbl;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRAN_IN_CHUNK',L_table,NULL);
   close C_GET_TRAN_IN_CHUNK;

   if (L_typ_proc_chunk_tbl is NOT NULL and L_typ_proc_chunk_tbl.COUNT > 0) then
   
      FOR i in L_typ_proc_chunk_tbl.first .. L_typ_proc_chunk_tbl.last 
      LOOP
         
         SAVEPOINT BEGIN_TRAN_PROCESS;
         if PROCESS_TRANSACTION(O_error_message,
                                I_process_id,
                                L_typ_proc_chunk_tbl(i).seq_no) and PROCESS_FUTURE_COST(O_error_message,
                                                                                        I_process_id,
                                                                                        L_typ_proc_chunk_tbl(i).seq_no) then
                                        
            WRITE_SUCCESS_PARAMS(I_process_id, L_typ_proc_chunk_tbl(i).seq_no);
            
            WRITE_SUCCESS(I_process_id,
                          I_chunk_id);
         else
            ROLLBACK TO BEGIN_TRAN_PROCESS;
            O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_TRAN_PROCESS_FAILED',
                                                        NULL,
                                                        NULL,
                                                        NULL);
           
            
            WRITE_ERROR(I_process_id,
                       I_chunk_id,
                       O_error_message);
        
         end if;
      END LOOP;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_TRAN_IN_CHUNK%ISOPEN then
         close C_GET_TRAN_IN_CHUNK;
      end if;      
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
END PROCESS_COST_TMPL_UPLD;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TRANSACTION(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                             I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN IS
   
   L_program              VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESS_TRANSACTION';
   L_typ_process_thead_rec   TYP_process_thead_rec;
   L_typ_process_tdetl_tbl   TYP_process_tdetl_tbl;
   child_detected EXCEPTION;
   PRAGMA EXCEPTION_INIT(child_detected, -2292);
   L_templ_exists             BOOLEAN := FALSE;
   L_overlap_exists           BOOLEAN := FALSE;
   L_exists                   BOOLEAN := FALSE;
   L_first_applied            SVC_WF_COST_TMPL_UPLD_THEAD.FIRST_APPLIED%TYPE;
   L_child_exists             VARCHAR2(1)  := NULL;
   L_comp_exists              BOOLEAN := FALSE;
   L_dup_reln_exists          BOOLEAN := FALSE;
   L_item_dept                ITEM_MASTER.DEPT%TYPE;
   L_item_class               ITEM_MASTER.CLASS%TYPE;
   L_item_subclass            ITEM_MASTER.SUBCLASS%TYPE;
   L_templ_id_tbl             OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE(); 
   L_user_id                  COST_EVENT.USER_ID%TYPE DEFAULT user;
   L_cost_event_process_id    COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_table                    VARCHAR2(255) := NULL;    
   L_wf_ord_exists            VARCHAR2(1);
   L_dup_reln_rowid           VARCHAR2(20) := NULL;
   
   cursor C_GET_TRANSACTION_HEAD is  
      select th.message_type,
             th.template_id,
             th.template_desc,
             th.first_applied,
             th.percentage,
             th.cost,
             nvl(th.final_cost,'N')
        from svc_wf_cost_tmpl_upld_thead th
       where th.process_id = I_process_id
         and th.seq_no = I_seq_no
         and th.status = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD;
         
   cursor C_GET_TRANSACTION_DETL is  
      select td.rowid,
             td.message_type,
             td.dept,
             td.class,
             td.subclass,
             td.item,
             td.location,
             td.start_date,
             td.end_date,
             td.new_start_date,
             td.new_end_date,
             td.cost_comp_id
        from svc_wf_cost_tmpl_upld_tdetl td
       where td.process_id = I_process_id
         and td.seq_no = I_seq_no
         and td.status = CORESVC_WF_COST_TMPL_UPLD_SQL.NEW_RECORD; 
       
   cursor C_CHECK_WF_ORD_DETAIL(TEMPLATE_ID WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE) is
      select 'x'
        from wf_order_detail
       where templ_id = template_id
         and rownum = 1;
         
   cursor C_GET_HEADER_INFO(TEMPLATE_ID WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE) is
      select templ_id,
             templ_desc,
             first_applied,
             margin_pct,
             cost,
             final_cost_ind
        from wf_cost_buildup_tmpl_head
       where templ_id = template_id
         and rownum = 1;         

BEGIN
   
   -- Populate retry parameters
   select retry_lock_attempts,
          retry_wait_time
     into LP_retry_lock_attempts,
          LP_retry_wait_time
     from rms_plsql_batch_config
    where program_name = 'CORESVC_WF_COST_TMPL_UPLD_SQL'
      and rownum = 1;
      
   if SQL_LIB.GET_USER_NAME(O_error_message,
                            L_user_id) = FALSE then
      WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
      return FALSE;
   end if;
   

   L_table := 'SVC_WF_COST_TMPL_UPLD_THEAD';
   
   SQL_LIB.SET_MARK('OPEN','C_GET_TRANSACTION_HEAD',L_table,NULL);
   open C_GET_TRANSACTION_HEAD;
   SQL_LIB.SET_MARK('FETCH','C_GET_TRANSACTION_HEAD',L_table,NULL);
   fetch C_GET_TRANSACTION_HEAD into L_typ_process_thead_rec;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRANSACTION_HEAD',L_table,NULL);
   close C_GET_TRANSACTION_HEAD;

   -----------------------------------------------
    --Begin Transaction Header Processing for Add
   -----------------------------------------------
   
   if L_typ_process_thead_rec.message_type is NOT NULL then
      
      if L_typ_process_thead_rec.message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_ADD then
         
         BEGIN
            insert into wf_cost_buildup_tmpl_head (templ_id,
                                                   templ_desc,
                                                   first_applied,
                                                   margin_pct,
                                                   cost,
                                                   final_cost_ind)
                                            values (L_typ_process_thead_rec.template_id,
                                                    L_typ_process_thead_rec.template_desc,
                                                    L_typ_process_thead_rec.first_applied,
                                                    L_typ_process_thead_rec.percentage,
                                                    L_typ_process_thead_rec.cost,
                                                    L_typ_process_thead_rec.final_cost
                                                    );
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               
               O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_DUP_ON_CTMPL_HEAD',
                                                            NULL,
                                                            NULL,
                                                            NULL);
                                                                       
               WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
               return FALSE;
            when OTHERS then
               O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                     SQLERRM,
                                                     L_program,
                                                     to_char(SQLCODE));
               WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
               return FALSE;
         END;
      end if; 
   end if; 
   -----------------------------------------------
   -- Begin Transaction Detail Processing
   -----------------------------------------------         
   L_table := 'SVC_WF_COST_TMPL_UPLD_TDETL';
   SQL_LIB.SET_MARK('OPEN','C_GET_TRANSACTION_DETL',L_table,NULL);
   open C_GET_TRANSACTION_DETL;
   SQL_LIB.SET_MARK('FETCH','C_GET_TRANSACTION_DETL',L_table,NULL);
   fetch C_GET_TRANSACTION_DETL BULK COLLECT into L_typ_process_tdetl_tbl;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRANSACTION_DETL',L_table,NULL);
   close C_GET_TRANSACTION_DETL;
   
   if (L_typ_process_tdetl_tbl is NOT NULL and L_typ_process_tdetl_tbl.count > 0) then                                   
      FOR i in L_typ_process_tdetl_tbl.first .. L_typ_process_tdetl_tbl.last
      LOOP
         --- fetch dept,class,subclass for the item
         if L_typ_process_thead_rec.first_applied = 'C' and L_typ_process_tdetl_tbl(i).item is NOT NULL then
            if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                              L_typ_process_tdetl_tbl(i).item,
                                              L_item_dept,
                                              L_item_class,
                                              L_item_subclass) = FALSE then
               WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
               return FALSE;
            end if;
         end if;
         
         if L_typ_process_tdetl_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_ADD then
            
            if L_typ_process_thead_rec.first_applied in ('M','R') or
               (L_typ_process_thead_rec.first_applied = 'U' and L_typ_process_tdetl_tbl(i).cost_comp_id is NULL) then
               
               if COST_BUILDUP_SQL.COST_RELATION_OVERLAP_CHECK(O_error_message,
                                                               L_overlap_exists,
                                                               L_typ_process_tdetl_tbl(i).dept,
                                                               L_typ_process_tdetl_tbl(i).class,
                                                               L_typ_process_tdetl_tbl(i).subclass,
                                                               'S',
                                                               L_typ_process_tdetl_tbl(i).location,
                                                               L_typ_process_tdetl_tbl(i).new_start_date,
                                                               L_typ_process_tdetl_tbl(i).new_end_date,
                                                               NULL,
                                                               NULL) = FALSE then
                  
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
               if NOT L_overlap_exists then
                  
                  if COST_BUILDUP_SQL.COST_RELATION_EXPLODE(O_error_message,
                                                            'N',
                                                            L_typ_process_tdetl_tbl(i).dept,
                                                            L_typ_process_tdetl_tbl(i).class,
                                                            L_typ_process_tdetl_tbl(i).subclass,
                                                            'S',
                                                            L_typ_process_tdetl_tbl(i).location,
                                                            L_typ_process_tdetl_tbl(i).new_start_date,
                                                            L_typ_process_tdetl_tbl(i).new_end_date,
                                                            L_typ_process_thead_rec.template_id,
                                                            NULL,
                                                            NULL) = FALSE then
      
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
               else
                  O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_RELN_EXISTS',
                                                              NULL,
                                                              NULL,
                                                              NULL);
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
            end if;   
               
            
            if L_typ_process_thead_rec.first_applied = 'C' then
               
               if COST_BUILDUP_SQL.COST_RELATION_OVERLAP_CHECK(O_error_message,
                                                               L_overlap_exists,
                                                               L_item_dept,
                                                               L_item_class,
                                                               L_item_subclass,
                                                               'S',
                                                               L_typ_process_tdetl_tbl(i).location,
                                                               L_typ_process_tdetl_tbl(i).new_start_date,
                                                               L_typ_process_tdetl_tbl(i).new_end_date,
                                                               NULL,
                                                               L_typ_process_tdetl_tbl(i).item) = FALSE then
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
               if NOT L_overlap_exists then
                  
                  if COST_BUILDUP_SQL.COST_RELATION_EXPLODE(O_error_message,
                                                            'N',
                                                            L_item_dept,
                                                            L_item_class,
                                                            L_item_subclass,
                                                            'S',
                                                            L_typ_process_tdetl_tbl(i).location,
                                                            L_typ_process_tdetl_tbl(i).new_start_date,
                                                            L_typ_process_tdetl_tbl(i).new_end_date,
                                                            L_typ_process_thead_rec.template_id,
                                                            NULL,
                                                            L_typ_process_tdetl_tbl(i).item) = FALSE then
      
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
               else
                  O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_ITEM_RELN_EXISTS',
                                                              NULL,
                                                              NULL,
                                                              NULL);
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
               
            end if;
        
            if (L_typ_process_thead_rec.first_applied = 'U' and L_typ_process_tdetl_tbl(i).cost_comp_id is NOT NULL) then
               
               if COST_BUILDUP_SQL.TEMPL_RELATION_EXISTS(O_error_message,
                                                         L_templ_exists,
                                                         L_typ_process_thead_rec.template_id) = FALSE then
                           
                           
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
                  return FALSE;
               end if;
               -- if no relationships exists, allow addition of new upcharge cost component
               if NOT L_templ_exists then               
                  if COST_BUILDUP_SQL.COST_COMP_EXISTS(O_error_message,
                                                       L_exists,
                                                       L_typ_process_tdetl_tbl(i).cost_comp_id,
                                                       L_typ_process_thead_rec.template_id) = FALSE then
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
                  if NOT L_exists then
                     if COST_BUILDUP_SQL.INSERT_COMP_DETAIL(O_error_message,
                                                            L_typ_process_tdetl_tbl(i).cost_comp_id,
                                                            L_typ_process_thead_rec.template_id) = FALSE then
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                     end if;
                  else
                     O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_COST_TEMPL_EXISTS',
                                                                 NULL,
                                                                 NULL,
                                                                 NULL);
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
               else
                  O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_ADD_COMP',
                                                              NULL,
                                                              NULL,
                                                              NULL);
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
            end if;
         end if;

         if L_typ_process_tdetl_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_DEL then
              
            if  L_typ_process_thead_rec.first_applied IN ('M','R') or (L_typ_process_thead_rec.first_applied = 'U' and L_typ_process_tdetl_tbl(i).cost_comp_id is NULL) then -- begin when first applied != 'C','U' 
                          
               if LOCK_COSTTMPL_RELN_TABLE(O_error_message,
                                           I_process_id,
                                           I_seq_no) = FALSE then
                  
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
               BEGIN
                  
                  update wf_cost_relationship
                     set end_date    = L_vdate
                   where dept        = L_typ_process_tdetl_tbl(i).dept
                     and class       = nvl(L_typ_process_tdetl_tbl(i).class, '-1')
                     and subclass    = nvl(L_typ_process_tdetl_tbl(i).subclass, '-1')
                     and location    = L_typ_process_tdetl_tbl(i).location
                     and item        = nvl(L_typ_process_tdetl_tbl(i).item,'-1')
                     and start_date  = L_typ_process_tdetl_tbl(i).start_date
                     and end_date    = L_typ_process_tdetl_tbl(i).end_date
                     and templ_id    = L_typ_process_thead_rec.template_id
                     and start_date <= L_vdate
                     and end_date    > L_vdate;
                     
                  L_upd_rows := sql%rowcount;   
                     
                  delete from wf_cost_relationship
                        where dept       = L_typ_process_tdetl_tbl(i).dept
                          and class      = nvl(L_typ_process_tdetl_tbl(i).class, '-1')
                          and subclass   = nvl(L_typ_process_tdetl_tbl(i).subclass, '-1')
                          and location   = L_typ_process_tdetl_tbl(i).location
                          and item       = nvl(L_typ_process_tdetl_tbl(i).item,'-1')
                          and start_date = L_typ_process_tdetl_tbl(i).start_date
                          and end_date   = L_typ_process_tdetl_tbl(i).end_date
                          and templ_id   = L_typ_process_thead_rec.template_id
                          and start_date > L_vdate;
                          
                  if SQL%NOTFOUND and L_upd_rows = 0 then
                     O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_REC_ON_TBL_DEL',
                                                                  'WF_COST_RELATIONSHIP',
                                                                  NULL,
                                                                  NULL);   
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
               EXCEPTION
                  when OTHERS then
                     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           to_char(SQLCODE));
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
               END;
            end if;
             
            if L_typ_process_thead_rec.first_applied = 'C' then
             
               if LOCK_COSTTMPL_RELN_TABLE(O_error_message,
                                           I_process_id,
                                           I_seq_no) = FALSE then
                    
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
               BEGIN
                  
                  update wf_cost_relationship
                     set end_date    = L_vdate
                   where item        = L_typ_process_tdetl_tbl(i).item
                     and location    = L_typ_process_tdetl_tbl(i).location
                     and start_date  = L_typ_process_tdetl_tbl(i).start_date
                     and end_date    = L_typ_process_tdetl_tbl(i).end_date 
                     and templ_id    = L_typ_process_thead_rec.template_id
                     and start_date <= L_vdate
                     and end_date    > L_vdate;
                     
                  L_upd_rows := sql%rowcount;   
                 
                  delete from wf_cost_relationship
                        where item       = L_typ_process_tdetl_tbl(i).item
                          and location   = L_typ_process_tdetl_tbl(i).location
                          and start_date = L_typ_process_tdetl_tbl(i).start_date
                          and end_date   = L_typ_process_tdetl_tbl(i).end_date 
                          and templ_id   = L_typ_process_thead_rec.template_id
                          and start_date > L_vdate;
                               
                  if SQL%NOTFOUND and L_upd_rows = 0 then
                     O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_REC_ON_TBL_DEL',
                                                                  'WF_COST_RELATIONSHIP',
                                                                   NULL,
                                                                   NULL);   
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
               EXCEPTION
                  when OTHERS then
                     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           to_char(SQLCODE));
                     return FALSE;
              END;
            end if; 
             
            if (L_typ_process_thead_rec.first_applied = 'U' and L_typ_process_tdetl_tbl(i).cost_comp_id is NOT NULL) then
             
               if COST_BUILDUP_SQL.TEMPL_RELATION_EXISTS(O_error_message,
                                                         L_templ_exists,
                                                         L_typ_process_thead_rec.template_id) = FALSE then
                           
                           
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
                  return FALSE;
               end if;
               -- if no relationships exists, allow addition of new upcharge cost component
               if NOT L_templ_exists then
               
                  if LOCK_COSTTMPL_DETL_TABLE_TL(O_error_message,
                                                 I_process_id,
                                                 I_seq_no) = FALSE then
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
               
                  if LOCK_COSTTMPL_DETL_TABLE(O_error_message,
                                              I_process_id,
                                              I_seq_no) = FALSE then
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
                  BEGIN
                     delete from wf_cost_buildup_tmpl_dtl_tl
                           where cost_comp_id = L_typ_process_tdetl_tbl(i).cost_comp_id
                             and templ_id   = L_typ_process_thead_rec.template_id;
                             
                     delete from wf_cost_buildup_tmpl_detail
                           where cost_comp_id = L_typ_process_tdetl_tbl(i).cost_comp_id
                             and templ_id   = L_typ_process_thead_rec.template_id;
                     if SQL%NOTFOUND then
                        O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_REC_ON_TBL_DEL',
                                                                     'WF_COST_BUILDUP_TMPL_DETAIL',
                                                                     NULL,
                                                                     NULL);   
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                     end if;
                     if SQL%FOUND then
                        if COST_COMP_EXISTS(O_error_message,
                                            L_comp_exists,
                                            L_typ_process_thead_rec.template_id) = FALSE then
                           WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                           return FALSE; 
                        end if;
                        if NOT L_comp_exists then
                           O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_COST_COMP_NO_DEL',
                                                                        NULL,
                                                                        NULL,
                                                                        NULL); 
                           WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                           return FALSE;
                        end if;
                     end if;
                  EXCEPTION
                     when OTHERS then
                        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                              SQLERRM,
                                                              L_program,
                                                              to_char(SQLCODE));
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                  END;
               else
                  O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_DEL_COMP',
                                                               NULL,
                                                               NULL,
                                                               NULL); 
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;   
               end if;
            end if;            
         end if;
       
         if L_typ_process_tdetl_tbl(i).message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_RELN_MOD then
                
            if L_typ_process_thead_rec.first_applied IN ('M','R') or (L_typ_process_thead_rec.first_applied = 'U' and L_typ_process_tdetl_tbl(i).cost_comp_id is NULL) then
               if DUP_RELN_EXISTS(O_error_message,
                                  L_dup_reln_exists,
                                  L_dup_reln_rowid,
                                  L_typ_process_tdetl_tbl(i).dept,
                                  NVL(L_typ_process_tdetl_tbl(i).class,'-1'),
                                  NVL(L_typ_process_tdetl_tbl(i).subclass, '-1'),
                                  L_typ_process_tdetl_tbl(i).location,
                                  L_typ_process_tdetl_tbl(i).start_date,
                                  L_typ_process_tdetl_tbl(i).end_date,
                                  NULL) = FALSE then
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
             
               if L_dup_reln_exists then
               
                  if L_typ_process_tdetl_tbl(i).start_date > L_vdate and L_typ_process_tdetl_tbl(i).end_date > L_vdate then
      
                     if COST_BUILDUP_SQL.COST_RELATION_OVERLAP_CHECK(O_error_message,
                                                                     L_overlap_exists,
                                                                     L_typ_process_tdetl_tbl(i).dept,
                                                                     L_typ_process_tdetl_tbl(i).class,
                                                                     L_typ_process_tdetl_tbl(i).subclass,
                                                                     'S',
                                                                     L_typ_process_tdetl_tbl(i).location,
                                                                     L_typ_process_tdetl_tbl(i).new_start_date,
                                                                     L_typ_process_tdetl_tbl(i).new_end_date,
                                                                     L_dup_reln_rowid,
                                                                     NULL) = FALSE then
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                     end if;
                  end if;
                  
                  if ((L_typ_process_tdetl_tbl(i).start_date < L_vdate or L_typ_process_tdetl_tbl(i).start_date = L_vdate) and L_typ_process_tdetl_tbl(i).end_date > L_vdate) then
                      
                     if COST_BUILDUP_SQL.COST_RELATION_OVERLAP_CHECK(O_error_message,
                                                                     L_overlap_exists,
                                                                     L_typ_process_tdetl_tbl(i).dept,
                                                                     L_typ_process_tdetl_tbl(i).class,
                                                                     L_typ_process_tdetl_tbl(i).subclass,
                                                                     'S',
                                                                     L_typ_process_tdetl_tbl(i).location,
                                                                     L_typ_process_tdetl_tbl(i).start_date,
                                                                     L_typ_process_tdetl_tbl(i).new_end_date,
                                                                     L_dup_reln_rowid,
                                                                     NULL) = FALSE then
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                     end if;
                  end if;
                  if NOT L_overlap_exists then
                     if LOCK_COSTTMPL_RELN_TABLE(O_error_message,
                                                 I_process_id,
                                                 I_seq_no) = FALSE then
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                     end if;
                     
                     if L_typ_process_tdetl_tbl(i).start_date > L_vdate and L_typ_process_tdetl_tbl(i).end_date > L_vdate then
                        if COST_BUILDUP_SQL.COST_RELATION_EXPLODE(O_error_message,
                                                                  'N',
                                                                  L_typ_process_tdetl_tbl(i).dept,
                                                                  L_typ_process_tdetl_tbl(i).class,
                                                                  L_typ_process_tdetl_tbl(i).subclass,
                                                                  'S',
                                                                  L_typ_process_tdetl_tbl(i).location,
                                                                  L_typ_process_tdetl_tbl(i).new_start_date,
                                                                  L_typ_process_tdetl_tbl(i).new_end_date,
                                                                  L_typ_process_thead_rec.template_id,
                                                                  L_dup_reln_rowid,
                                                                  NULL) = FALSE then
      
                           WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                           return FALSE;
                        end if;
                     end if;
                     
                     if (L_typ_process_tdetl_tbl(i).start_date < L_vdate or L_typ_process_tdetl_tbl(i).start_date = L_vdate) and  L_typ_process_tdetl_tbl(i).end_date > L_vdate then
                        if COST_BUILDUP_SQL.COST_RELATION_EXPLODE(O_error_message,
                                                                  'N',
                                                                  L_typ_process_tdetl_tbl(i).dept,
                                                                  L_typ_process_tdetl_tbl(i).class,
                                                                  L_typ_process_tdetl_tbl(i).subclass,
                                                                  'S',
                                                                  L_typ_process_tdetl_tbl(i).location,
                                                                  L_typ_process_tdetl_tbl(i).start_date,
                                                                  L_typ_process_tdetl_tbl(i).new_end_date,
                                                                  L_typ_process_thead_rec.template_id,
                                                                  L_dup_reln_rowid,
                                                                  NULL) = FALSE then
      
                           WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                           return FALSE;
                        end if;
                     end if;
                             
                  else
                     O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_RELN_EXISTS',
                                                                 NULL,
                                                                 NULL,
                                                                 NULL);
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;  
                  end if;
               else
                  O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_REC_ON_TBL_UPD',
                                                               'WF_COST_RELATIONSHIP',
                                                               NULL,
                                                               NULL);   
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
            end if;   
            
            if L_typ_process_thead_rec.first_applied = 'C' then
               if DUP_RELN_EXISTS(O_error_message,
                                  L_dup_reln_exists,
                                  L_dup_reln_rowid,
                                  L_item_dept,
                                  L_item_class,
                                  L_item_subclass,
                                  L_typ_process_tdetl_tbl(i).location,
                                  L_typ_process_tdetl_tbl(i).start_date,
                                  L_typ_process_tdetl_tbl(i).end_date,
                                  L_typ_process_tdetl_tbl(i).item) = FALSE then
                  
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
                 
               if L_dup_reln_exists then
                  
                  if L_typ_process_tdetl_tbl(i).start_date > L_vdate and L_typ_process_tdetl_tbl(i).end_date > L_vdate then
                     if COST_BUILDUP_SQL.COST_RELATION_OVERLAP_CHECK(O_error_message,
                                                                     L_overlap_exists,
                                                                     L_item_dept,
                                                                     L_item_class,
                                                                     L_item_subclass,
                                                                     'S',
                                                                     L_typ_process_tdetl_tbl(i).location,
                                                                     L_typ_process_tdetl_tbl(i).new_start_date,
                                                                     L_typ_process_tdetl_tbl(i).new_end_date,
                                                                     L_dup_reln_rowid,
                                                                     L_typ_process_tdetl_tbl(i).item) = FALSE then
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                     end if;
                  end if;
                  
                  if ((L_typ_process_tdetl_tbl(i).start_date < L_vdate or L_typ_process_tdetl_tbl(i).start_date = L_vdate) and L_typ_process_tdetl_tbl(i).end_date > L_vdate) then
                     if COST_BUILDUP_SQL.COST_RELATION_OVERLAP_CHECK(O_error_message,
                                                                     L_overlap_exists,
                                                                     L_item_dept,
                                                                     L_item_class,
                                                                     L_item_subclass,
                                                                     'S',
                                                                     L_typ_process_tdetl_tbl(i).location,
                                                                     L_typ_process_tdetl_tbl(i).start_date,
                                                                     L_typ_process_tdetl_tbl(i).new_end_date,
                                                                     L_dup_reln_rowid,
                                                                     L_typ_process_tdetl_tbl(i).item) = FALSE then
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                     end if;
                  end if;
                  if NOT L_overlap_exists then
                     if LOCK_COSTTMPL_RELN_TABLE(O_error_message,
                                                 I_process_id,
                                                 I_seq_no) = FALSE then
                        WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                        return FALSE;
                     end if;
                     if L_typ_process_tdetl_tbl(i).start_date > L_vdate and L_typ_process_tdetl_tbl(i).end_date > L_vdate then
                        if COST_BUILDUP_SQL.COST_RELATION_EXPLODE(O_error_message,
                                                                  'N',
                                                                  L_item_dept,
                                                                  L_item_class,
                                                                  L_item_subclass,
                                                                  'S',
                                                                  L_typ_process_tdetl_tbl(i).location,
                                                                  L_typ_process_tdetl_tbl(i).new_start_date,
                                                                  L_typ_process_tdetl_tbl(i).new_end_date,
                                                                  L_typ_process_thead_rec.template_id,
                                                                  L_dup_reln_rowid,
                                                                  L_typ_process_tdetl_tbl(i).item) = FALSE then
      
                           WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                           return FALSE;
                        end if;
                     end if;
                     if (L_typ_process_tdetl_tbl(i).start_date < L_vdate or L_typ_process_tdetl_tbl(i).start_date = L_vdate) and  L_typ_process_tdetl_tbl(i).end_date > L_vdate then
                        if COST_BUILDUP_SQL.COST_RELATION_EXPLODE(O_error_message,
                                                                  'N',
                                                                  L_item_dept,
                                                                  L_item_class,
                                                                  L_item_subclass,
                                                                  'S',
                                                                  L_typ_process_tdetl_tbl(i).location,
                                                                  L_typ_process_tdetl_tbl(i).start_date,
                                                                  L_typ_process_tdetl_tbl(i).new_end_date,
                                                                  L_typ_process_thead_rec.template_id,
                                                                  L_dup_reln_rowid,
                                                                  L_typ_process_tdetl_tbl(i).item) = FALSE then
      
                           WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                           return FALSE;
                        end if;
                     end if;
                      
                  else
                     O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_ITEM_RELN_EXISTS',
                                                                  NULL,
                                                                  NULL,
                                                                  NULL);   
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                     return FALSE;
                  end if;
               else
                  O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_REC_ON_TBL_UPD',
                                                               'WF_COST_RELATIONSHIP',
                                                               NULL,
                                                               NULL);   
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,L_typ_process_tdetl_tbl(i).rowid, CORESVC_WF_COST_TMPL_UPLD_SQL.TDETL);
                  return FALSE;
               end if;
            end if;
         end if;
                  
      END LOOP;
      
      if L_typ_process_thead_rec.first_applied = 'U' and L_typ_process_thead_rec.message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_ADD then
         if COST_COMP_EXISTS(O_error_message,
                             L_comp_exists,
                             L_typ_process_thead_rec.template_id) = FALSE then
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
            return FALSE; 
         end if;
         if NOT L_comp_exists then
            O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('UPCHARGE_REQ',
                                                         NULL,
                                                         NULL,
                                                         NULL); 
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
            return FALSE;
         end if;
      end if;
   end if;

   -------------------------------------------------
   -- Begin Transaction Header Processing for Mod
   -------------------------------------------------
      
   if L_typ_process_thead_rec.message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_MOD then
      
      open C_GET_HEADER_INFO(L_typ_process_thead_rec.template_id);
      fetch C_GET_HEADER_INFO into GP_wf_cost_tmpl_head_row;
      close C_GET_HEADER_INFO;
      
      if GP_wf_cost_tmpl_head_row.templ_id is NULL then
         O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_REC_ON_TBL_UPD',
                                                      'WF_COST_BUILDUP_TMPL_HEAD',
                                                      NULL,
                                                      NULL); 
         WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);  
         return FALSE;
      end if;
      
      -- check if there is a change in cost template header information
      if GP_wf_cost_tmpl_head_row.templ_desc <> L_typ_process_thead_rec.template_desc or
         GP_wf_cost_tmpl_head_row.first_applied <> L_typ_process_thead_rec.first_applied or 
         GP_wf_cost_tmpl_head_row.margin_pct <> L_typ_process_thead_rec.percentage or
         GP_wf_cost_tmpl_head_row.cost <> L_typ_process_thead_rec.cost or
         GP_wf_cost_tmpl_head_row.final_cost_ind <> nvl(L_typ_process_thead_rec.final_cost,'N')  then
         
         -- if a mod in header is expected then check if relationships exists
         if COST_BUILDUP_SQL.TEMPL_RELATION_EXISTS(O_error_message,
                                                   L_templ_exists,
                                                   L_typ_process_thead_rec.template_id) = FALSE then
                                 
                                 
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
            return FALSE;
         end if;
         -- if no relationships exists, allow update to header
         
         if NOT L_templ_exists then
         -- if existing is upcharge then check if cost components exists
            if GP_wf_cost_tmpl_head_row.first_applied in ('M','U') then
               if COST_COMP_EXISTS(O_error_message,
                                   L_comp_exists,
                                   L_typ_process_thead_rec.template_id) = FALSE then
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
                  return FALSE;
               end if;
               if L_comp_exists and L_typ_process_thead_rec.first_applied NOT IN ('M','U') then
                  O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_ONLY_UM_MOD_TO_MU',
                                                                NULL,
                                                                NULL,
                                                                NULL); 
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
                  return FALSE;
               end if;
            end if;
            
            if LOCK_COSTTMPL_HEAD_TABLE(O_error_message,
                                        I_process_id,
                                        I_seq_no) = FALSE then
               WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD); 
               return FALSE;
            end if;
            BEGIN
               update wf_cost_buildup_tmpl_head
                  set templ_desc = L_typ_process_thead_rec.template_desc,
                      first_applied = L_typ_process_thead_rec.first_applied,
                      margin_pct = L_typ_process_thead_rec.percentage,
                      cost = L_typ_process_thead_rec.cost,
                      final_cost_ind = 
                                      CASE WHEN L_typ_process_thead_rec.first_applied = 'C' THEN L_typ_process_thead_rec.final_cost
                                      ELSE 'N' END
                where templ_id = L_typ_process_thead_rec.template_id;
               if SQL%NOTFOUND then
                  O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_REC_ON_TBL_UPD',
                                                               'WF_COST_BUILDUP_TMPL_HEAD',
                                                                NULL,
                                                                NULL); 
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);  
                  return FALSE;
               end if;
            EXCEPTION
               when OTHERS then
                  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                        SQLERRM,
                                                        L_program,
                                                        to_char(SQLCODE));
                  return FALSE;
            END;
         else
            O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CTMPL_HEADER_NO_MOD',
                                                        NULL,
                                                        NULL,
                                                        NULL); 
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD); 
            return FALSE;
         end if;
      end if;
      
      if L_typ_process_thead_rec.first_applied = 'U' then
         if COST_COMP_EXISTS(O_error_message,
                             L_comp_exists,
                             L_typ_process_thead_rec.template_id) = FALSE then
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
            return FALSE; 
         end if;
         if NOT L_comp_exists then
            O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_COST_COMP_NO_MOD',
                                                         NULL,
                                                         NULL,
                                                         NULL); 
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
            return FALSE;
         end if;
      end if;
   end if;  
   
   -----------------------------------------------
   -- Begin Transaction Header Processing for Del
   -----------------------------------------------
   if L_typ_process_thead_rec.message_type = CORESVC_WF_COST_TMPL_UPLD_SQL.TMPL_HEAD_DEL then
      if COST_BUILDUP_SQL.TEMPL_RELATION_EXISTS(O_error_message,
                                                L_templ_exists,
                                                L_typ_process_thead_rec.template_id) = FALSE then
                           
                           
         WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('OPEN','C_CHECK_WF_ORD_DETAIL','WF_ORDER_DETAIL',NULL);
      open C_CHECK_WF_ORD_DETAIL(L_typ_process_thead_rec.template_id);
      SQL_LIB.SET_MARK('FETCH','C_CHECK_WF_ORD_DETAIL','WF_ORDER_DETAIL',NULL);
      fetch C_CHECK_WF_ORD_DETAIL into L_wf_ord_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_WF_ORD_DETAIL','WF_ORDER_DETAIL',NULL);
      close C_CHECK_WF_ORD_DETAIL;
      
      if L_wf_ord_exists is NOT NULL then
         O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_ORD_EXISTS',
                                                     L_typ_process_thead_rec.template_id,
                                                     NULL,
                                                     NULL);
         WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
         return FALSE;
      end if;
      if NOT L_templ_exists then
         if LOCK_COSTTMPL_HEAD_TABLE_TL(O_error_message,
                                        I_process_id,
                                        I_seq_no) = FALSE then
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD); 
            return FALSE;
         end if;
         if LOCK_COSTTMPL_HEAD_TABLE(O_error_message,
                                    I_process_id,
                                    I_seq_no) = FALSE then
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD); 
            return FALSE;
         end if;
         BEGIN
            delete from wf_cost_buildup_tmpl_hd_tl
                  where templ_id = L_typ_process_thead_rec.template_id;
                  
            delete from wf_cost_buildup_tmpl_head
                  where templ_id = L_typ_process_thead_rec.template_id;  
            if SQL%NOTFOUND then
               O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('CTMPL_NO_REC_ON_TBL_DEL',
                                                            'WF_COST_BUILDUP_TMPL_HEAD',
                                                            NULL,
                                                            NULL);   
                  WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
                  return FALSE;
            end if;
         EXCEPTION
            when child_detected then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('COST_REL_NO_DELETE',
                                                           NULL,
                                                           NULL,
                                                           NULL);
               WRITE_REJECT_PARAMS(O_error_message, I_process_id,I_seq_no, NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
               return FALSE;
            when OTHERS then
               O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                     SQLERRM,
                                                     L_program,
                                                     to_char(SQLCODE));
               return FALSE;                                                         
         END;
         if L_typ_process_thead_rec.first_applied = 'U' then
            if LOCK_COSTTMPL_DETL_TABLE_TL(O_error_message,
                                           I_process_id,
                                           I_seq_no) = FALSE then
               WRITE_REJECT_PARAMS(O_error_message, I_process_id, NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
               return FALSE;
            end if;
            if LOCK_COSTTMPL_DETL_TABLE(O_error_message,
                                        I_process_id,
                                        I_seq_no) = FALSE then
                     
               WRITE_REJECT_PARAMS(O_error_message, I_process_id, NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
               return FALSE;
                         
            end if;
            BEGIN
               delete from wf_cost_buildup_tmpl_dtl_tl
                     where templ_id   = L_typ_process_thead_rec.template_id;
                     
               delete from wf_cost_buildup_tmpl_detail
                     where templ_id   = L_typ_process_thead_rec.template_id;
            EXCEPTION
               when OTHERS then
                  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                        SQLERRM,
                                                        L_program,
                                                        to_char(SQLCODE));
                     WRITE_REJECT_PARAMS(O_error_message, I_process_id, NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
                     return FALSE;
            END;
         end if;
      else
         O_error_message :=  SQL_LIB.GET_MESSAGE_TEXT('COST_REL_NO_DELETE',
                                                      NULL,
                                                      NULL,
                                                      NULL);
            WRITE_REJECT_PARAMS(O_error_message, I_process_id, I_seq_no,NULL, CORESVC_WF_COST_TMPL_UPLD_SQL.THEAD);
            return FALSE;
      end if;
   end if;
        
   return TRUE;   
EXCEPTION
   when OTHERS then
      
      if C_GET_TRANSACTION_HEAD%ISOPEN then
         close C_GET_TRANSACTION_HEAD;
      end if;
      
      if C_GET_TRANSACTION_DETL%ISOPEN then
         close C_GET_TRANSACTION_DETL;
      end if;
      
      if C_CHECK_WF_ORD_DETAIL%ISOPEN then
         close C_CHECK_WF_ORD_DETAIL;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
             
END PROCESS_TRANSACTION;
---------------------------------------------------------------------------------------------------------------------
FUNCTION WRITE_FILE_PROCESS_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.WRITE_FILE_PROCESS_STATUS';
   L_reject_rec_exists   VARCHAR2(1);
   L_table                VARCHAR2(255) := NULL;

   cursor C_GET_REJECT_REC is
      select 'x'
        from svc_wf_cost_tmpl_upld_thead th
       where th.process_id = I_process_id
         and th.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
      UNION
      select 'x'
        from svc_wf_cost_tmpl_upld_tdetl td
       where td.process_id = I_process_id
        and td.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
      UNION
      select 'x'
        from svc_wf_cost_tmpl_upld_ttail tt
       where tt.process_id = I_process_id
        and tt.status = CORESVC_WF_COST_TMPL_UPLD_SQL.REJECTED
      UNION
      select 'x'
        from svc_wf_cost_tmpl_upld_fhead fh
       where fh.process_id = I_process_id
         and fh.status = CORESVC_WF_COST_TMPL_UPLD_SQL.ERROR
      UNION
      select 'x'
        from svc_wf_cost_tmpl_upld_ftail ft
       where ft.process_id = I_process_id
         and ft.status = CORESVC_WF_COST_TMPL_UPLD_SQL.ERROR;
      
BEGIN
   
   L_table := 'SVC_WF_COST_TMPL_UPLD_FHEAD, SVC_WF_COST_TMPL_UPLD_THEAD, SVC_WF_COST_TMPL_UPLD_TDETL, SVC_WF_COST_TMPL_UPLD_TTAIL , SVC_WF_COST_TMPL_UPLD_FTAIL';
   SQL_LIB.SET_MARK('OPEN','C_GET_REJECT_REC',L_table,NULL);
   open C_GET_REJECT_REC;
   SQL_LIB.SET_MARK('FETCH','C_GET_REJECT_REC',L_table,NULL);
   fetch C_GET_REJECT_REC into L_reject_rec_exists;
   SQL_LIB.SET_MARK('CLOSE','C_GET_REJECT_REC',L_table,NULL);
   close C_GET_REJECT_REC;
   
   if L_reject_rec_exists is NULL then
   
      WRITE_SUCCESS_PARAMS(I_process_id, NULL);
      
   end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      
      if C_GET_REJECT_REC%ISOPEN then
         close C_GET_REJECT_REC;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END WRITE_FILE_PROCESS_STATUS;
---------------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_RECORDS(O_error_message IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

    cursor C_GET_RETENTION_DAYS is
      select fdn_stg_retention_days
        from system_options;
    
    cursor C_PURGE (RETENTION_DAYS NUMBER)is
       select process_id
        from svc_wf_cost_tmpl_upld_fhead
       where last_update_datetime < L_vdate - retention_days;

    TYPE TYP_prg_rec_type is TABLE OF C_purge%RowType;
    L_typ_prg_rec TYP_prg_rec_type;
    L_program             VARCHAR2(64) := 'CORESVC_WF_COST_TMPL_UPLD_SQL.PURGE_RECORDS';
    L_retention_days      NUMBER(3)    := 0;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_RETENTION_DAYS','SYSTEM_OPTIONS',NULL);
   open C_GET_RETENTION_DAYS;
   SQL_LIB.SET_MARK('FETCH','C_GET_RETENTION_DAYS','SYSTEM_OPTIONS',NULL);
   fetch C_GET_RETENTION_DAYS into L_retention_days;
   SQL_LIB.SET_MARK('CLOSE','C_GET_RETENTION_DAYS','SYSTEM_OPTIONS',NULL);
   close C_GET_RETENTION_DAYS;  
  
  
   SQL_LIB.SET_MARK('OPEN','C_PURGE','SVC_WF_COST_TMPL_UPLD_FHEAD',NULL);
   open C_PURGE(L_retention_days);
   SQL_LIB.SET_MARK('FETCH','C_PURGE','SVC_WF_COST_TMPL_UPLD_FHEAD',NULL);
   fetch C_PURGE bulk collect into L_typ_prg_rec limit 1000;
   SQL_LIB.SET_MARK('CLOSE','C_PURGE','SVC_WF_COST_TMPL_UPLD_FHEAD',NULL);
   close C_PURGE;
      
   if   L_typ_prg_rec.count > 0 then
      
      FOR i in L_typ_prg_rec.first .. L_typ_prg_rec.last 
      LOOP 
      
        delete from svc_wf_cost_tmpl_upld_ftail
               where process_id = L_typ_prg_rec(i).process_id;
      
        delete from svc_wf_cost_tmpl_upld_ttail
              where process_id = L_typ_prg_rec(i).process_id;
      
         delete from svc_wf_cost_tmpl_upld_tdetl
               where process_id = L_typ_prg_rec(i).process_id;
     
         delete from svc_wf_cost_tmpl_upld_thead
               where process_id = L_typ_prg_rec(i).process_id;
      
         delete from svc_wf_cost_tmpl_upld_fhead
               where process_id = L_typ_prg_rec(i).process_id;
         delete from svc_wf_cost_tmpl_upld_status
               where process_id = L_typ_prg_rec(i).process_id;
         commit;
      END LOOP;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      
      if C_PURGE%ISOPEN then
         close C_PURGE;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END PURGE_RECORDS;
---------------------------------------------------------------------------------------------------------------------
END CORESVC_WF_COST_TMPL_UPLD_SQL;
/