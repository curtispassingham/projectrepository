CREATE OR REPLACE PACKAGE FOUNDATION_HIERARCHY_SQL AUTHID CURRENT_USER IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : class (Form Module)
  -- Source Object            : PROCEDURE  -> P_CHECK_UDA_CURSOR
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_UDA_CURSOR(O_error_message   IN OUT   VARCHAR2,
                          I_CLASS           IN       NUMBER,
                          I_DEPT            IN       NUMBER)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : dept (Form Module)
  -- Source Object            : FUNCTION   -> F_VAT_EXISTS
  ---------------------------------------------------------------------------------------------
FUNCTION VAT_EXISTS(O_error_message   IN OUT   VARCHAR2,
                    O_ret             IN OUT   BOOLEAN,
                    I_DEPT            IN       NUMBER)
   return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : lclstwh (Form Module)
  -- Source Object            : PROCEDURE  -> P_CHECK_DUP_LOCS
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_LOCS(O_error_message         IN OUT   VARCHAR2,
                        I_LOCATION              IN       NUMBER,
                        I_LOC_TYPE              IN       VARCHAR2,
                        I_TI_LOC_LIST           IN       NUMBER,
                        P_IntrnlVrblsGvDplcte   IN OUT   BOOLEAN)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : partner (Form Module)
  -- Source Object            : FUNCTION   -> P_CHECK_ADDRESS
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_ADDRESS(O_error_message   IN OUT   VARCHAR2,
                       O_mode               OUT   VARCHAR2,
                       I_PARTNER_ID      IN       VARCHAR2,
                       I_PARTNER_TYPE    IN       VARCHAR2,
                       P_PM_MODE         IN       VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : store (Form Module)
  -- Source Object            : PROCEDURE  -> P_GET_TIMEZONE_NAME
  ---------------------------------------------------------------------------------------------
FUNCTION GET_TIMEZONE_NAME(O_error_message      IN OUT   VARCHAR2,
                           G_USER_LANG          IN       VARCHAR2,
                           I_TIMEZONE_NAME      IN OUT   VARCHAR2,
                           I_TI_TIMEZONE_NAME   IN       VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : subclass (Form Module)
  -- Source Object            : PROCEDURE  -> P_INSERT_DAILY_PURGE
  ---------------------------------------------------------------------------------------------
FUNCTION INSERT_DAILY_PURGE(O_error_message   IN OUT   VARCHAR2,
                            I_CLASS           IN       NUMBER,
                            I_DEPT            IN       NUMBER,
                            I_SUBCLASS        IN       NUMBER)
   return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : subclass (Form Module)
  -- Source Object            : PROCEDURE  -> P_CHECK_UDA_CURSOR
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_UDA_CURSOR_SUBCLASS(O_error_message   IN OUT   VARCHAR2,
                                   I_CLASS           IN       NUMBER,
                                   I_DEPT            IN       NUMBER,
                                   I_SUBCLASS        IN       NUMBER)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : supvwedt (Form Module)
  -- Source Object            : PROCEDURE  -> P_CONTRACTING
  ---------------------------------------------------------------------------------------------
FUNCTION CONTRACTING(O_error_message            IN OUT   VARCHAR2,
                     P_IntrnlVrblsGvCntrctInd   IN OUT   VARCHAR2)
   return BOOLEAN ;
END FOUNDATION_HIERARCHY_SQL;
/