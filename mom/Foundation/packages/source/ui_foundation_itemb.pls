CREATE OR REPLACE PACKAGE Body FOUNDATION_ITEM_SQL IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : itemfind (Form Module)
  -- Source Object            : PROCEDURE  -> FORM_VALIDATION.VPN
  -- NEW ARGUMENTS (3)
  -- ITEM          :B_MAIN.TI_SUPPLIER..................... -> I_TI_SUPPLIER
  -- ITEM          :B_MAIN.TI_SUPPLIER_SITE................ -> I_TI_SUPPLIER_SITE
  -- ITEM          :B_MAIN.TI_VPN.......................... -> I_TI_VPN
  --------------------------------------------------------------------
FUNCTION VPN(O_error_message      IN OUT   VARCHAR2,
             I_TI_SUPPLIER        IN       NUMBER,
             I_TI_SUPPLIER_SITE   IN       NUMBER,
             I_TI_VPN             IN       VARCHAR2)
   return BOOLEAN
is
  L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
  L_valid             VARCHAR2(1) := 'N';
  L_vpn_supp_exists   BOOLEAN := FALSE;
  L_program           VARCHAR2(64) := 'FOUNDATION_ITEM_SQL.VPN';
  ---
  cursor C_VALID_VPN is
     select 'Y'
       from item_supplier
      where vpn = I_TI_VPN;
BEGIN
  if I_TI_VPN is NOT NULL then
    if I_TI_SUPPLIER is NULL and I_TI_SUPPLIER_SITE is NULL then
      open C_VALID_VPN;
      fetch C_VALID_VPN into L_valid;
      close C_VALID_VPN;
      ---
      if L_valid = 'N' then
         O_error_message := sql_lib.create_msg('INV_VPN',
                                               NULL,
                                               NULL,
                                               NULL);
      return FALSE;
      end if;
    else
      if SUPP_ITEM_ATTRIB_SQL.CHECK_VPN_SUPP_EXIST(L_error_message,
                                                   L_vpn_supp_exists,
                                                   I_TI_VPN,
                                                   NVL(I_TI_SUPPLIER_SITE,I_TI_SUPPLIER)) = FALSE then
        O_error_message := sql_lib.create_msg(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
      if L_vpn_supp_exists = FALSE then
         O_error_message := sql_lib.create_msg('INV_VPN',
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
    end if;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end VPN;
---------------------------------------------------------------------------------------------
-- Module                   : itemudam (Form Module)
-- Source Object            : PROCEDURE  -> FORM_VALIDATION.VALIDATE_ALL
-- NEW ARGUMENTS (8)
-- ITEM          :B_UDA.DISPLAY_TYPE..................... -> I_DISPLAY_TYPE
-- ITEM          :B_UDA.SINGLE_VALUE_IND................. -> I_SINGLE_VALUE_IND
-- ITEM          :B_UDA_ITEM_DATE.ITEM................... -> I_ITEM0101
-- ITEM          :B_UDA_ITEM_FF.ITEM..................... -> I_ITEM01
-- ITEM          :B_UDA_ITEM_LOV.ITEM.................... -> I_ITEM
-- ITEM          :B_UDA_ITEM_LOV.UDA_VALUE............... -> I_UDA_VALUE
-- PARAMETER     :PARAMETER.PM_UDA_ID.................... -> P_PM_UDA_ID
-- SYSTEM        :SYSTEM.RECORD_STATUS................... -> SYSTEM_RECORD_STATUS
-- ITEM          :B_UDA_ITEM_LOV.ROWID ............... -> I_UDA_ROWID
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ALL(O_error_message        IN OUT   VARCHAR2,
                      I_DISPLAY_TYPE         IN       VARCHAR2,
                      I_SINGLE_VALUE_IND     IN       VARCHAR2,
                      I_ITEM0101             IN       VARCHAR2,
                      I_ITEM01               IN       VARCHAR2,
                      I_ITEM                 IN       VARCHAR2,
                      I_UDA_VALUE            IN       NUMBER,
                      P_PM_UDA_ID            IN       NUMBER,
                      SYSTEM_RECORD_STATUS   IN       VARCHAR2,
                      I_UDA_ROWID                     ROWID)
   return BOOLEAN
is
  L_dummy           VARCHAR2(3);
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_exists          BOOLEAN := FALSE;
  L_item            UDA_ITEM_LOV.ITEM%TYPE := NULL;
  L_temp            NUMBER;
  L_program         VARCHAR2(64) := 'FOUNDATION_ITEM_SQL.VALIDATE_ALL';
  cursor C_CHECK_DUPLICATE_LOV is
     select 'x'
       from uda_item_lov
      where uda_id  = P_PM_UDA_ID
        and uda_value = I_UDA_VALUE
        and item = I_ITEM
        and rowid != NVL(I_UDA_ROWID, '00000000.0000.0000');
  cursor C_CHECK_DUPLICATE_FF is
     select 'x'
       from uda_item_ff
      where uda_id = P_PM_UDA_ID
        and item = I_ITEM01;
  cursor C_CHECK_DUPLICATE_DATE is
     select 'x'
       from uda_item_date
      where uda_id = P_PM_UDA_ID
        and item = I_ITEM0101;
BEGIN
  if I_DISPLAY_TYPE = 'LV' then
    open C_CHECK_DUPLICATE_LOV;
    fetch C_CHECK_DUPLICATE_LOV into L_dummy;
    if C_CHECK_DUPLICATE_LOV%FOUND then
      close C_CHECK_DUPLICATE_LOV;
         O_error_message := sql_lib.create_msg('UDA_ITEM_LOV_DUP',
                                               NULL,
                                               NULL,
                                               NULL);
      return FALSE;
    end if;
    close C_CHECK_DUPLICATE_LOV;
    L_item                 := I_ITEM;
  elsif I_DISPLAY_TYPE      = 'FF' then
    if SYSTEM_RECORD_STATUS = 'INSERT' then
      open C_CHECK_DUPLICATE_FF;
      fetch C_CHECK_DUPLICATE_FF into L_dummy;
      if C_CHECK_DUPLICATE_FF%FOUND then
        close C_CHECK_DUPLICATE_FF;
           O_error_message := sql_lib.create_msg('UDA_ITEM_FF_DUP',
                                                 NULL,
                                                 NULL,
                                                 NULL);
        return FALSE;
      end if;
      close C_CHECK_DUPLICATE_FF;
    end if;
    L_item := I_ITEM01;
  elsif I_DISPLAY_TYPE = 'DT' then
    if SYSTEM_RECORD_STATUS = 'INSERT' then
      open C_CHECK_DUPLICATE_DATE;
      fetch C_CHECK_DUPLICATE_DATE into L_dummy;
      if C_CHECK_DUPLICATE_DATE%FOUND then
        close C_CHECK_DUPLICATE_DATE;
           O_error_message := sql_lib.create_msg('UDA_ITEM_DATE_DUP',
                                                 NULL,
                                                 NULL,
                                                 NULL);
        return FALSE;
      end if;
      close C_CHECK_DUPLICATE_DATE;
    end if;
    L_item := I_ITEM0101;
  end if;
  if I_SINGLE_VALUE_IND  = 'Y' and SYSTEM_RECORD_STATUS in ('NEW', 'INSERT') then
    if UDA_SQL.CHECK_SINGLE_UDA(L_error_message,
                                L_exists,
                                P_PM_UDA_ID,
                                NULL,
                                L_item,
                                NULL,
                                NULL,
                                NULL) = FALSE then
       O_error_message := sql_lib.create_msg(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
    if L_exists then
       O_error_message := sql_lib.create_msg('SINGLE_UDA_ITEM',
                                             to_char(P_PM_UDA_ID),
                                             (L_item),
                                             NULL);
    return FALSE;
    end if;
  end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
end VALIDATE_ALL;
---------------------------------------------------------------------------------------------
-- Module                   : mclocn (Form Module)
-- Source Object            : PROCEDURE  -> P_DELETE
-- NEW ARGUMENTS (2)
-- ITEM          :B_LOCATION.LI_TYPE..................... -> I_LI_TYPE
-- ITEM          :B_LOCATION.TI_VALUE.................... -> I_TI_VALUE
---------------------------------------------------------------------------------------------
FUNCTION DELETE_MCLOCN(O_error_message   IN OUT   VARCHAR2,
                       I_LI_TYPE         IN       VARCHAR2 ,
                       I_TI_VALUE        IN       VARCHAR2 )
  return BOOLEAN
is
  L_type      VARCHAR2(6)  := I_LI_TYPE;
  L_value     VARCHAR2(10) := I_TI_VALUE;
  L_program   VARCHAR2(64) := 'FOUNDATION_ITEM_SQL.DELETE_MCLOCN';
BEGIN
  if L_type = 'AL' then
    delete
      from mc_location_temp;
    -- delete the store from the location list
  elsif L_type = 'S' then
    delete
      from mc_location_temp
     where location = L_value;
    -- delete all the stores within the class
    -- from the location list
  elsif L_type = 'C' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select store.store
          from store
         where store.store_class = L_value);
    -- delete all the stores within the district
    -- from the location list
  elsif L_type = 'D' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select store.store
          from store
         where store.district = L_value);
    -- delete all the stores within the region
    -- from the location list
  elsif L_type = 'R' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select store.store
          from store,
               region,
               district
         where store.district = district.district
           and region.region = district.region
           and region.region = L_value);
  elsif L_type = 'A' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select store.store
          from store,
               region,
               district,
               area
        where store.district = district.district
          and region.region = district.region
          and area.area = region.area
          and area.area = L_value);
    -- delete all the stores within the transfer zone
    -- from the location list
  elsif L_type = 'T' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select store.store
          from store
         where store.transfer_zone = L_value);
    -- delete all the stores with the location trait
    -- from the location list
  elsif L_type = 'L' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select store.store
          from store,
               loc_traits_matrix
         where loc_traits_matrix.store = store.store
           and loc_traits_matrix.loc_trait = L_value);
    -- delete all the stores from the location list
  elsif L_type = 'AS' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select store.store
          from store);
  elsif L_type = 'LLS' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select location
          from loc_list_detail lld
         where lld.loc_list = L_value
           and lld.loc_type = 'S');
    --delete the default warehouse from the location list
  elsif L_type = 'DW' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select store.store
          from store
         where store.default_wh = L_value);
    -- delete the physical warehouse from the location list
  elsif L_type = 'PW' then
    delete
      from mc_location_temp
     where location in
       (select wh
          from wh
         where physical_wh = L_value
           and stockholding_ind = 'Y');
    -- delete the warehouse from the location list
  elsif L_type = 'W' then
    delete
      from mc_location_temp
     where mc_location_temp.location = L_value;
    -- delete all the warehouses from the location list
  elsif L_type = 'AW' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select wh.wh
          from wh);
  elsif L_type = 'LLW' then
    delete
      from mc_location_temp
     where mc_location_temp.location in
       (select wh.wh
          from wh,
               loc_list_detail l
         where l.loc_list        = L_value
           and wh.stockholding_ind = 'Y'
           and (l.location = wh.wh
       or l.location = wh.physical_wh));
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : mcmrhier (Form Module)
-- Source Object            : PROCEDURE  -> P_DELETE_REJECTS
-- NEW ARGUMENTS (1)
-- Pack.Spec     P_INTERNAL_VARIABLE.IV_USER_ID.......... -> P_PIntrnlVrbleIvUsrId
---------------------------------------------------------------------------------------------
FUNCTION DELETE_REJECTS(O_error_message         IN OUT   VARCHAR2,
                        P_PIntrnlVrbleIvUsrId   IN       VARCHAR2)
  return BOOLEAN
is
  L_program VARCHAR2(64) := 'FOUNDATION_ITEM_SQL.DELETE_REJECTS';
BEGIN
  delete
     from mc_rejections
    where change_type = 'M'
      and user_id = P_PIntrnlVrbleIvUsrId;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : slhead (Form Module)
-- Source Object            : PROCEDURE  -> P_DELETE_TPG_LIST_ITEMS
-- NEW ARGUMENTS (1)
-- ITEM          :B_SKULIST.SKULIST...................... -> I_SKULIST
---------------------------------------------------------------------------------------------
FUNCTION DELETE_TPG_LIST_ITEMS(O_error_message   IN OUT   VARCHAR2,
                               O_ret             IN OUT   BOOLEAN,
                               I_SKULIST         IN       NUMBER)
   return BOOLEAN
is
  L_program       VARCHAR2(64) := 'FOUNDATION_ITEM_SQL.DELETE_TPG_LIST_ITEMS';
  L_item          skulist_detail.item%TYPE;
  L_exists        VARCHAR(1);
  L_table         VARCHAR2(50) :=NULL;
  RECORD_LOCKED   EXCEPTION;
  PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
  cursor C_SKULIST_DETAIL is
     select item
        from skulist_detail
       where skulist=I_SKULIST;
  cursor C_CHECK_TPG_SKULIST_DETAIL is
     select 'Y'
       from skulist_head h,
            skulist_detail d
      where h.skulist =d.skulist
        and d.item =L_item
        and h.skulist != I_SKULIST;
  cursor C_LOCK_SKULIST_CRITERIA is
     select 'x'
       from skulist_criteria
      where skulist_criteria.skulist=I_SKULIST
        and skulist_criteria.item =L_item FOR update OF skulist nowait;
  cursor C_LOCK_SKULIST_DETAIL is
     select 'x'
       from skulist_detail
      where skulist_detail.skulist=I_SKULIST
        and skulist_detail.item =L_item FOR update OF skulist nowait;
BEGIN
  FOR C_rec in C_SKULIST_DETAIL
  LOOP
    L_item  :=C_rec.item;
    L_exists:='N';
    open C_CHECK_TPG_SKULIST_DETAIL;
    fetch C_CHECK_TPG_SKULIST_DETAIL into L_exists;
    if L_exists='Y' then
      close C_CHECK_TPG_SKULIST_DETAIL;
      ---
      L_table := 'SKULIST_CRITERIA';
      open C_LOCK_SKULIST_CRITERIA;
      close C_LOCK_SKULIST_CRITERIA;
      ---
       delete
         from skulist_criteria
        where skulist_criteria.skulist = I_SKULIST
          and skulist_criteria.item = L_item;
      ---
      L_table := 'SKULIST_DETAIL';
      open C_LOCK_SKULIST_DETAIL;
      close C_LOCK_SKULIST_DETAIL;
      ---
      delete
        from skulist_detail
       where skulist_detail.skulist = I_SKULIST
         and skulist_detail.item = L_item;
    else
      close C_CHECK_TPG_SKULIST_DETAIL;
    end if;
  end LOOP;
  return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            to_char(I_SKULIST),
                                            NULL);
      O_ret := FALSE;
  return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
end FOUNDATION_ITEM_SQL;
/
