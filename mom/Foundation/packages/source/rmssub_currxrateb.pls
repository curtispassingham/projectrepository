
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_CUR_RATES AS
---------------------------------------------------------------------
TYPE curr_rectype IS RECORD(from_currency          currency_rates.currency_code%TYPE,
                            to_currency            currency_rates.currency_code%TYPE,
                            conversion_date        currency_rates.effective_date%TYPE,
                            conversion_rate        currency_rates.exchange_rate%TYPE,
                            exch_type              fif_currency_xref.fif_exchange_type%TYPE);
---------------------------------------------------------------------

LP_currency_code          SYSTEM_OPTIONS.currency_code%TYPE := NULL;


/* Private functions and procedures */
---------------------------------------------------------------------
FUNCTION PARSE_HEADER(O_error_message     OUT     VARCHAR2,
                      O_curr_record       OUT     curr_rectype,
                      I_curr_root         IN OUT  xmldom.DOMElement)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION PROCESS_HEADER processes xml after it has been
-- received and parsed.
---------------------------------------------------------------------
FUNCTION PROCESS_HEADER(O_error_message     IN OUT VARCHAR2,
                        I_curr_record       IN     CURR_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION PROCESS_RATES will take the input rates record
-- and place the information into a local rates record which will
-- be used in the package to manipulate the data. It will then
-- call a series of support functions to perform all business logic
-- on the record.
---------------------------------------------------------------------
FUNCTION PROCESS_RATES (O_text             IN OUT VARCHAR2,
                        O_table_locked     IN OUT BOOLEAN,
                        I_raterec          IN     CURR_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION INSERT_RATES will insert any valid rates on the rates
-- table.  It is called from PUT_RATES
---------------------------------------------------------------------
FUNCTION INSERT_RATES(O_text       IN OUT VARCHAR2,
                      I_raterec    IN     CURR_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION UPDATE_RATES will update any valid rates on the rates
-- table.  It is called from PUT_RATES
---------------------------------------------------------------------
FUNCTION UPDATE_RATES(O_text           IN OUT VARCHAR2,
                      O_table_locked   IN OUT BOOLEAN,
                      I_raterec        IN     CURR_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION VALIDATE_RATES is a wrapper function which is
-- used to call CHECK_NULLS, CHECK_PRIM_CURR_VDATE.
---------------------------------------------------------------------
FUNCTION VALIDATE_RATES(O_text        IN OUT VARCHAR2,
                        I_raterec     IN     CURR_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION CHECK_SYSTEM will check that conversion_date >= vdate.
-- This function also ensures the input to_currency is equal to the
-- primary currency when RMS is not using Oracle Financials version
-- 11.5.10.  If RMS is using Oracle Financials version 11.5.10, this
-- function will output FALSE via the 'ignore message' paramter,
-- causing the API to not process the current message.
---------------------------------------------------------------------
FUNCTION CHECK_SYSTEM(O_text    IN OUT VARCHAR2,
                      I_date    IN     DATE,
                      I_to_curr IN     VARCHAR2,
                      O_ignore_message IN OUT BOOLEAN)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION CHECK_NULLS will check an input value if it's null.  If
--  so, an error message will be created and the int_errors error
--  handling will be called.
---------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_text            IN OUT VARCHAR2,
                      I_record_variable IN     VARCHAR2,
                      I_record_name     IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION CONVERT_TYPE will convert
-- fif_exchange_type to rms_exch_type
-- through the table fif_currency_xref
---------------------------------------------------------------------
FUNCTION CONVERT_TYPE(O_text              OUT VARCHAR2,
                      O_rms_exch_type     OUT VARCHAR2,
                      I_fif_exch_type  IN     VARCHAR2)
return BOOLEAN;
-----------------------------------------------------------------------


/* Function and Procedure Bodies */
-------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message           OUT  VARCHAR2,
                 I_message                 IN   CLOB)
return BOOLEAN IS

   curr_root        xmldom.DOMElement;
   L_curr_record    curr_rectype;
   L_ignore_message BOOLEAN := FALSE;

BEGIN
   -- parse the document and return the root element.
   -- Send in entire xml document, the xml file header name, and the following defaults (for now).
   curr_root := rib_xml.readRoot(I_message, 'CurrRateDesc');

   -- Process header record by retrieving the value for each associated xml tag.
   if PARSE_HEADER(O_error_message,
                   L_curr_record,
                   curr_root) = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_SYSTEM(O_error_message,
                   L_curr_record.conversion_date,
                   L_curr_record.to_currency,
                   L_ignore_message) = FALSE then
      return FALSE;
   end if;

   ---
   -- Oracle Financials version 11.5.10 is not aware of the primary currency within RMS.  As such,
   -- it is not able to limit what it sends in the to-currency field to the primary currency within
   -- RMS, it must send all currency rates it holds.  If the API raised an error for every message
   -- sent without a to-currency matching the primary RMS currency, the RIB error hospital would be
   -- filled with a large number of messages that cannot be 'fixed' and resubmitted to the API.  The
   -- only way to get these messages out of the RIB error hospital is to delete them.  This is not
   -- desirable behaviour, and so if RMS is being used with Oracle Financials version 11.5.10, this
   -- API will ignore any message where the to-currency is not the primary currency within RMS.
   if L_ignore_message = TRUE then
      return TRUE;
   else
      if PROCESS_HEADER(O_error_message,
                        L_curr_record) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

 EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_CUR_RATES.CONSUME',
                                            to_char(SQLCODE));
         return FALSE;
END CONSUME;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_HEADER(O_error_message     OUT     VARCHAR2,
                      O_curr_record       OUT     curr_rectype,
                      I_curr_root         IN OUT  xmldom.DOMElement)
return BOOLEAN IS

   CURSOR c_system IS
      select currency_code
        from system_options;

BEGIN

   -- The package RIB_XML is called to retrieve values from the XML document.
   -- In order to do this, the specific tag names on the XML document will need to be known
   -- for each value we need to retrieve.

   -- Oracle Financials is not aware of primary currency of RMS, therefore it
   -- sends the currency_rates conversions for both from and to location as
   -- primary_currency. If we use the exchange_rate with primary currency equal
   -- to to_currency then we need to invert the exchange rate before inserting
   -- in RMS currency_rates table. We have avoided this by directly using the
   -- value with primary_currency as from_currency. This will avoid error that
   -- might occur when we invert the exchange_rate.
   open c_system;
   fetch c_system into LP_currency_code;
   close c_system;

   O_curr_record.from_currency                 := rib_xml.getChildText(I_curr_root, 'to_currency');
   O_curr_record.to_currency                   := rib_xml.getChildText(I_curr_root, 'from_currency');
   O_curr_record.conversion_date               := rib_xml.getChildDate(I_curr_root, 'conversion_date');
   O_curr_record.conversion_rate               := TO_NUMBER(rib_xml.getChildText(I_curr_root, 'conversion_rate'));
   O_curr_record.exch_type                     := rib_xml.getChildText(I_curr_root, 'user_conversion_type');
   
   return TRUE;

EXCEPTION
    when OTHERS then

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_CUR_RATES.PARSE_HEADER',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_HEADER;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_HEADER(O_error_message     IN OUT VARCHAR2,
                        I_curr_record       IN     CURR_RECTYPE)
return BOOLEAN IS

   L_raterec          CURR_RECTYPE;
   L_table_locked     BOOLEAN            := FALSE;
   L_cause            VARCHAR2(1)        := NULL;


BEGIN
   L_raterec.from_currency := I_curr_record.from_currency;
   L_raterec.to_currency := I_curr_record.to_currency;
   L_raterec.conversion_rate := to_number(I_curr_record.conversion_rate);
   L_raterec.conversion_date := I_curr_record.conversion_date;
   ---
   if CONVERT_TYPE(O_error_message,
                   L_raterec.exch_type,
                   I_curr_record.exch_type) = FALSE then
      return FALSE;
   end if;
   --- End populating rate record ---
   if PROCESS_RATES(O_error_message,
                    L_table_locked,
                    L_raterec) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_INVADJUST.PROCESS_HEADER',
                                             to_char(SQLCODE));
       return FALSE;

END PROCESS_HEADER;
----------------------------------------------------------------------------------------
FUNCTION PROCESS_RATES (O_text             IN OUT VARCHAR2,
                        O_table_locked     IN OUT BOOLEAN,
                        I_raterec          IN     CURR_RECTYPE)
return BOOLEAN IS

   L_raterec          CURR_RECTYPE   := I_raterec;
   L_exists           VARCHAR2(1)        := 'N';

   CURSOR c_exists IS
      select 'Y'
        from currency_rates
       where currency_code = L_raterec.from_currency
         and exchange_type = L_raterec.exch_type
         and effective_date = L_raterec.conversion_date;

BEGIN

   if VALIDATE_RATES(O_text,
                     L_raterec) = FALSE then
      return FALSE;
   end if;

   open c_exists;
   fetch c_exists into L_exists;
   close c_exists;

   if L_exists = 'N' then
      if INSERT_RATES(O_text,
                      L_raterec) = FALSE then
         return FALSE;
      end if;
   else
      if UPDATE_RATES(O_text,
                      O_table_locked,
                      L_raterec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
       O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                     SQLERRM,
                                     'RMSSUB_CUR_RATES.PROCESS_RATES',
                                     to_char(SQLCODE));
      return FALSE;

END PROCESS_RATES;
---------------------------------------------------------------------------------
FUNCTION INSERT_RATES(O_text        IN OUT VARCHAR2,
                      I_raterec     IN     CURR_RECTYPE)
return BOOLEAN IS


BEGIN
   insert into currency_rates(currency_code,
                              effective_date,
                              exchange_type,
                              exchange_rate)
                      values (I_raterec.from_currency,
                              I_raterec.conversion_date,
                              I_raterec.exch_type,
                              I_raterec.conversion_rate);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_text := sql_lib.create_msg('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_CUR_RATES.INSERT_RATES',
                                   to_char(SQLCODE));
      return FALSE;
END INSERT_RATES;
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_RATES(O_text         IN OUT VARCHAR2,
                      O_table_locked IN OUT BOOLEAN,
                      I_raterec      IN     CURR_RECTYPE)
return BOOLEAN IS

   L_luw             VARCHAR2(100)      := NULL;  -- primary key on currency_rates --
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   CURSOR c_lock_rates IS
      select 'Y'
        from currency_rates
       where currency_code = I_raterec.from_currency
         and exchange_type = I_raterec.exch_type
         and effective_date = I_raterec.conversion_date
         for update nowait;

BEGIN
   -- Lock the rates table before updating
   open c_lock_rates;
   close c_lock_rates;
   --

   update currency_rates
      set exchange_rate = I_raterec.conversion_rate
    where currency_code = I_raterec.from_currency
      and exchange_type = I_raterec.exch_type
      and effective_date = I_raterec.conversion_date;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      -- primary key on currency_rates --
      L_luw := I_raterec.from_currency||I_raterec.exch_type||to_char(I_raterec.conversion_date);
      O_text := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                   'CURRENCY_RATES',
                                   L_luw,
                                   'RMSSUB_CUR_RATES.UPDATE_RATES');
      O_table_locked := TRUE;
      return FALSE;
   when OTHERS then
      O_text := sql_lib.create_msg('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_CUR_RATES.UPDATE_RATES',
                                   to_char(SQLCODE));
      return FALSE;
END UPDATE_RATES;
----------------------------------------------------------------------------
FUNCTION VALIDATE_RATES(O_text        IN OUT VARCHAR2,
                        I_raterec     IN     CURR_RECTYPE)
return BOOLEAN IS

   CURSOR c_check_currency (I_currency CURRENCIES.CURRENCY_CODE%TYPE) IS
      SELECT currency_code
        FROM currencies
       WHERE currency_code = I_currency;

   L_check_currency CURRENCIES.CURRENCY_CODE%TYPE := NULL;

BEGIN
   if CHECK_NULLS(O_text,
                  I_raterec.from_currency,
                  'from_currency') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_text,
                  to_char(I_raterec.conversion_rate),
                  'conversion_rate') = FALSE then
      return FALSE;
   end if;

   if I_raterec.conversion_rate <= 0 then
      O_text := sql_lib.create_msg('INV_CONVERSION_RATE',
                                   NULL,
                                   NULL,
                                   NULL);
      return FALSE;
   end if;

   if CHECK_NULLS(O_text,
                  to_char(I_raterec.conversion_date),
                  'conversion_date') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_text,
                  I_raterec.exch_type,
                  'user_conversion_type') = FALSE then
      return FALSE;
   end if;

   -- Make sure the from-currency exists in RMS.  If it does not exist, output a user-friendly
   -- error message, as opposed to allowing the API to fail with a foreign key constraint
   -- violation (CRT_CUR_FK) upon insert.
   OPEN c_check_currency(I_raterec.from_currency);
   FETCH c_check_currency INTO L_check_currency;
   if c_check_currency%NOTFOUND then
      O_text := SQL_LIB.CREATE_MSG('INV_CURR_CURR',
                                   I_raterec.from_currency,
                                   NULL,
                                   NULL);
      return FALSE;
   end if;
   CLOSE c_check_currency;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_CUR_RATES.VALIDATE_RATES',
                                   to_char(SQLCODE));
      return FALSE;

END VALIDATE_RATES;
-----------------------------------------------------------------------
FUNCTION CHECK_SYSTEM(O_text    IN OUT VARCHAR2,
                      I_date    IN     DATE,
                      I_to_curr IN     VARCHAR2,
                      O_ignore_message IN OUT BOOLEAN)
return BOOLEAN IS

BEGIN

   --- Only import current rates
   if (GET_VDATE > I_date) then
      O_text := sql_lib.create_msg('DEACT_DATE_NOT_BEF_TODAY',
                                   NULL,
                                   NULL,
                                   NULL);
      return FALSE;
   end if;

   -- If RMS is being used with Oracle Financials version 11.5.10 or later, all messages sent with
   -- a to-currency not equal to the primary currency within RMS will be ignored.  If RMS is not
   -- being used with Oracle Financials version 11.5.10 or later, all messages sent with a to-currency
   -- not equal to the primary currency within RMS will be rejected by this API and sent to the RIB
   -- hospital.
  
   if (I_to_curr != LP_currency_code) then
      O_ignore_message := TRUE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_CUR_RATES.CHECK_SYSTEM',
                                   to_char(SQLCODE));
      return FALSE;

END CHECK_SYSTEM;
----------------------------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_text            IN OUT VARCHAR2,
                      I_record_variable IN     VARCHAR2,
                      I_record_name     IN     VARCHAR2)
return BOOLEAN IS

BEGIN

   if I_record_variable is NULL then
      O_text := sql_lib.create_msg('INVALID_PARM',
                                   I_record_name,
                                   'NULL',
                                   'NOT NULL');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_CUR_RATES.CHECK_NULLS',
                                   to_char(SQLCODE));
      return FALSE;

END CHECK_NULLS;
-----------------------------------------------------------------------
FUNCTION CONVERT_TYPE(O_text              OUT VARCHAR2,
                      O_rms_exch_type     OUT VARCHAR2,
                      I_fif_exch_type  IN     VARCHAR2)
return BOOLEAN IS

   L_rms_exch_type   fif_currency_xref.rms_exchange_type%TYPE  := NULL;

   CURSOR c_exch_type IS
      select rms_exchange_type
        from fif_currency_xref
       where fif_exchange_type = I_fif_exch_type;

BEGIN
   open c_exch_type;
   fetch c_exch_type into L_rms_exch_type;
   if c_exch_type%NOTFOUND then
      O_text := sql_lib.create_msg('EXCHANGE_TYPE_NOT_FOUND',
                                   NULL,
                                   NULL,
                                   NULL);
      close c_exch_type;
      return FALSE;
   end if;

   close c_exch_type;
   O_rms_exch_type := L_rms_exch_type;
   return TRUE;


EXCEPTION
    when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_CUR_RATES.CONVERT_TYPE',
                                   to_char(SQLCODE));
      return FALSE;

END CONVERT_TYPE;
-----------------------------------------------------------------------------------------
END RMSSUB_CUR_RATES;
/
