CREATE OR REPLACE PACKAGE BODY COUNTRY_TAX_JURS_SQL AS
---------------------------------------------------------
FUNCTION CHECK_DUP_JURS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists        IN OUT  BOOLEAN,
                        I_country_id    IN      COUNTRY.COUNTRY_ID%TYPE,
                        I_state         IN      STATE.STATE%TYPE,
                        I_jurisdiction  IN      COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)

RETURN BOOLEAN IS
   L_program             VARCHAR2(50) := 'COUNTRY_TAX_JURS_SQL.CHECK_DUP_JURS';
   L_jurisdiction_exists VARCHAR2(1)  := NULL;
   
   cursor C_CHECK_JURS is
      select 'x'
        from country_tax_jurisdiction ct
       where ct.country_id = I_country_id
         and ct.state = I_state
         and ct.jurisdiction_code = I_jurisdiction;

BEGIN
   if I_jurisdiction is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_jurisdiction', L_program);
      return FALSE;
   end if;

   if I_state is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_state', L_program);
      return FALSE;
   end if;

   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_country_id', L_program);
      return FALSE;
   end if;
   
   open C_CHECK_JURS;
   fetch C_CHECK_JURS into L_jurisdiction_exists;
   close C_CHECK_JURS;

   if L_jurisdiction_exists is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DUP_JURS;
---------------------------------------------------------
FUNCTION DELETE_JURS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists          IN OUT   BOOLEAN,
                     I_country_id      IN       COUNTRY.COUNTRY_ID%TYPE,
                     I_state           IN       STATE.STATE%TYPE,
                     I_jurisdiction    IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'COUNTRY_TAX_JURS_SQL.DELETE_JURS';
   L_exists      VARCHAR2(1)  := 'N';

   cursor C_CHECK_IF_EXISTS is
      select 'Y'
        from addr
       where jurisdiction_code = I_jurisdiction
         and state = I_state
         and country_id = I_country_id
       UNION
      select 'Y'
        from competitor
       where jurisdiction_code = I_jurisdiction
         and state = I_state
         and country_id = I_country_id
       UNION
      select 'Y'
        from comphead
       where co_jurisdiction_code = I_jurisdiction
         and co_state = I_state
         and co_country = I_country_id
       UNION
      select 'Y'
        from comp_store
       where jurisdiction_code = I_jurisdiction
         and state = I_state
         and country_id = I_country_id   
       UNION
      select 'Y'
        from ordcust
       where deliver_jurisdiction = I_jurisdiction
         and deliver_state = I_state
         and deliver_country_id = I_country_id
       UNION
      select 'Y'
        from outloc ol
       where ol.outloc_jurisdiction_code = I_jurisdiction
         and ol.outloc_state = I_state
         and ol.outloc_country_id = I_country_id
       UNION
      select 'Y'
        from rtv_head
       where ship_to_jurisdiction_code = I_jurisdiction
         and state = I_state
         and ship_to_country_id = I_country_id;

BEGIN

   if I_jurisdiction is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_jurisdiction', L_program);
      return FALSE;
   end if;

   if I_state is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_state', L_program);
      return FALSE;
   end if;

   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_country_id', L_program);
      return FALSE;
   end if;

   open C_CHECK_IF_EXISTS;
   fetch C_CHECK_IF_EXISTS into L_exists;
   close C_CHECK_IF_EXISTS;

   if L_exists = 'N' then
      O_exists := FALSE;

      delete from country_tax_jurisdiction
            where country_id = I_country_id
              and state = I_state
              and jurisdiction_code = I_jurisdiction;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_JURS;
---------------------------------------------------------
FUNCTION GET_JURS_DESC(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists            IN OUT BOOLEAN,
                       O_jurisdiction_desc IN OUT COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE, 
                       I_country_id        IN     COUNTRY.COUNTRY_ID%TYPE,
                       I_state             IN     STATE.STATE%TYPE,
                       I_jurisdiction      IN     COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
return BOOLEAN IS
   L_program     VARCHAR2(50) := 'COUNTRY_TAX_JURS_SQL.GET_JURS_DESC';

   cursor C_GET_DESC is
      select jurisdiction_desc
        from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction
         and state = nvl(I_state,state)
         and country_id = nvl(I_country_id,country_id);

BEGIN

   if I_jurisdiction is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_jurisdiction', L_program);
      return FALSE;
   end if;


   open C_GET_DESC;
   fetch C_GET_DESC into O_jurisdiction_desc;
   close C_GET_DESC;
   
   if O_jurisdiction_desc is NULL then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_TAX_JURIS',NULL);
      return FALSE;
   else
      O_exists := TRUE;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_JURS_DESC;
---------------------------------------------------------
END COUNTRY_TAX_JURS_SQL;
/
