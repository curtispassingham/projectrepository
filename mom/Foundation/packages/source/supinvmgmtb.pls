CREATE OR REPLACE PACKAGE BODY SUP_INV_MGMT_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION GET_INV_MGMT_DATA(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_sup_inv_rec       IN OUT SUP_INV_RECTYPE,
                           I_supplier          IN     SUP_INV_MGMT.SUPPLIER%TYPE,
                           I_dept              IN     SUP_INV_MGMT.DEPT%TYPE,
                           I_location          IN     SUP_INV_MGMT.LOCATION%TYPE)
RETURN BOOLEAN IS

   cursor C_SUP_INV_MGMT_REC is
      select sup_dept_seq_no,
             loc_type,
             review_cycle,
             repl_order_ctrl,
             scale_cnstr_ind,
             scale_cnstr_lvl,
             scale_cnstr_obj,             
             scale_cnstr_type1,             
             scale_cnstr_uom1,
             scale_cnstr_curr1,             
             scale_cnstr_min_val1,
             scale_cnstr_max_val1,          
             scale_cnstr_min_tol1,          
             scale_cnstr_max_tol1,
             scale_cnstr_type2,
             scale_cnstr_uom2,
             scale_cnstr_curr2,
             scale_cnstr_min_val2,
             scale_cnstr_max_val2,
             scale_cnstr_min_tol2 ,
             scale_cnstr_max_tol2,
             min_cnstr_lvl,
             min_cnstr_conj,
             min_cnstr_type1,
             min_cnstr_uom1,
             min_cnstr_curr1,
             min_cnstr_val1, 
             min_cnstr_type2,
             min_cnstr_uom2,
             min_cnstr_curr2,
             min_cnstr_val2,
             truck_split_ind,
             truck_split_method,
             truck_cnstr_type1,
             truck_cnstr_uom1,
             truck_cnstr_val1,
             truck_cnstr_tol1, 
             truck_cnstr_type2,
             truck_cnstr_uom2, 
             truck_cnstr_val2,
             truck_cnstr_tol2,
             ltl_approval_ind, 
             due_ord_process_ind,
             due_ord_lvl, 
             due_ord_serv_basis,
             non_due_ord_create_ind, 
             mult_vehicle_ind, 
             single_loc_ind, 
             round_lvl, 
             round_to_inner_pct,
             round_to_case_pct, 
             round_to_layer_pct,
             round_to_pallet_pct, 
             ord_purge_ind, 
             pool_supplier,
             pickup_loc,
             purchase_type,
             threshold_next_bracket,
             bracket_type1,
             bracket_uom1,
             bracket_uom2,
             bracket_type2,
             ib_ind,
             ib_order_ctrl   
        from sup_inv_mgmt
       where supplier = I_supplier
         and (dept    = I_dept 
              or dept is NULL)
         and (location = I_location
         or location = (select physical_wh
                          from wh
                              where wh = I_location)
              or location is NULL)
       order by location,dept;
      
BEGIN
 
   if I_supplier IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',      
                                            'NOT NULL');   
      return FALSE;
   end if;  
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUP_INV_MGMT_REC', 
                    'sup_inv_mgmt', 
                     NULL);
   open C_SUP_INV_MGMT_REC;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUP_INV_MGMT_REC',
           'sup_inv_mgmt',
                NULL);
   fetch C_SUP_INV_MGMT_REC into O_sup_inv_rec.sup_dept_seq_no,
                                 O_sup_inv_rec.loc_type,
                                 O_sup_inv_rec.review_cycle,
                                 O_sup_inv_rec.repl_order_ctrl,
                                 O_sup_inv_rec.scale_cnstr_ind,
                                 O_sup_inv_rec.scale_cnstr_lvl,
                                 O_sup_inv_rec.scale_cnstr_obj,             
                                 O_sup_inv_rec.scale_cnstr_type1,             
                                 O_sup_inv_rec.scale_cnstr_uom1,
                                 O_sup_inv_rec.scale_cnstr_curr1,             
                                 O_sup_inv_rec.scale_cnstr_min_val1,
                                 O_sup_inv_rec.scale_cnstr_max_val1,          
                                 O_sup_inv_rec.scale_cnstr_min_tol1,          
                                 O_sup_inv_rec.scale_cnstr_max_tol1,
                                 O_sup_inv_rec.scale_cnstr_type2,
                                 O_sup_inv_rec.scale_cnstr_uom2,
                                 O_sup_inv_rec.scale_cnstr_curr2,
                                 O_sup_inv_rec.scale_cnstr_min_val2,
                                 O_sup_inv_rec.scale_cnstr_max_val2,
                                 O_sup_inv_rec.scale_cnstr_min_tol2 ,
                                 O_sup_inv_rec.scale_cnstr_max_tol2,
                                 O_sup_inv_rec.min_cnstr_lvl,
                                 O_sup_inv_rec.min_cnstr_conj,
                                 O_sup_inv_rec.min_cnstr_type1,
                                 O_sup_inv_rec.min_cnstr_uom1,
                                 O_sup_inv_rec.min_cnstr_curr1,
                                 O_sup_inv_rec.min_cnstr_val1, 
                                 O_sup_inv_rec.min_cnstr_type2,
                                 O_sup_inv_rec.min_cnstr_uom2,
                                 O_sup_inv_rec.min_cnstr_curr2,
                                 O_sup_inv_rec.min_cnstr_val2,
                                 O_sup_inv_rec.truck_split_ind,
                                 O_sup_inv_rec.truck_split_method,
                                 O_sup_inv_rec.truck_cnstr_type1,
                                 O_sup_inv_rec.truck_cnstr_uom1,
                                 O_sup_inv_rec.truck_cnstr_val1,
                                 O_sup_inv_rec.truck_cnstr_tol1, 
                                 O_sup_inv_rec.truck_cnstr_type2,
                                 O_sup_inv_rec.truck_cnstr_uom2, 
                                 O_sup_inv_rec.truck_cnstr_val2,
                                 O_sup_inv_rec.truck_cnstr_tol2,
                                 O_sup_inv_rec.ltl_approval_ind, 
                                 O_sup_inv_rec.due_ord_process_ind,
                                 O_sup_inv_rec.due_ord_lvl, 
                                 O_sup_inv_rec.due_ord_serv_basis,
                                 O_sup_inv_rec.non_due_ord_create_ind, 
                                 O_sup_inv_rec.mult_vehicle_ind, 
                                 O_sup_inv_rec.single_loc_ind, 
                                 O_sup_inv_rec.round_lvl, 
                                 O_sup_inv_rec.round_to_inner_pct,
                                 O_sup_inv_rec.round_to_case_pct, 
                                 O_sup_inv_rec.round_to_layer_pct,
                                 O_sup_inv_rec.round_to_pallet_pct, 
                                 O_sup_inv_rec.ord_purge_ind, 
                                 O_sup_inv_rec.pool_supplier,
                                 O_sup_inv_rec.pickup_loc,
                                 O_sup_inv_rec.purchase_type,
                                 O_sup_inv_rec.threshold_next_bracket,
                                 O_sup_inv_rec.bracket_type1,
                                 O_sup_inv_rec.bracket_uom1,
                                 O_sup_inv_rec.bracket_uom2,
                                 O_sup_inv_rec.bracket_type2,
                                 O_sup_inv_rec.ib_ind,
                                 O_sup_inv_rec.ib_order_ctrl;  
            
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUP_INV_MGMT_REC',
           'sup_inv_mgmt',
           NULL); 
   close C_SUP_INV_MGMT_REC;  
   
   return TRUE;                                    
   

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA',
                                            to_char(SQLCODE));
      return FALSE;

END GET_INV_MGMT_DATA;   
-------------------------------------------------------------------------------------
FUNCTION SUPP_REPL_INFO(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_review_cycle      IN OUT SUP_INV_MGMT.REVIEW_CYCLE%TYPE,
                        O_repl_order_ctrl   IN OUT SUP_INV_MGMT.REPL_ORDER_CTRL%TYPE,
                        O_sup_dept_seq_no   IN OUT SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE,
                        I_supplier          IN     SUP_INV_MGMT.SUPPLIER%TYPE,
                        I_dept              IN     SUP_INV_MGMT.DEPT%TYPE,
                        I_location          IN     SUP_INV_MGMT.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_sup_inv_rec       SUP_INV_RECTYPE;


BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',      
                                            'NOT NULL');   
   end if;
---
   if SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA(O_error_message,
                                         L_sup_inv_rec,
                                         I_supplier,
                                         I_dept,
                                         I_location) = FALSE then
      return FALSE;
   end if;  
   
   O_review_cycle    := L_sup_inv_rec.review_cycle; 
   O_repl_order_ctrl := L_sup_inv_rec.repl_order_ctrl;
   O_sup_dept_seq_no := L_sup_inv_rec.sup_dept_seq_no;
                  
   return TRUE;     

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                      SQLERRM,
                          'SUP_INV_MGMT_SQL.SUPP_REPL_INFO',
                      to_char(SQLCODE));
   RETURN FALSE;

END SUPP_REPL_INFO;
-------------------------------------------------------------------------------------
FUNCTION DEL_INV_MGMT (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE)
RETURN BOOLEAN IS

L_table          VARCHAR2(30);
RECORD_LOCKED    EXCEPTION;
PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
L_program        VARCHAR2(50) := 'SUP_INV_MGMT_SQL.DEL_INV_MGMT';

cursor C_LOCK_SUP_REPL_DAY is
   select 'x'
     from sup_repl_day srd          
    where srd.sup_dept_seq_no in (select sup_dept_seq_no
                                    from sup_inv_mgmt
                                   where supplier = I_supplier)
      for update nowait;

cursor C_LOCK_SUP_INV_MGMT is
   select 'x'
     from sup_inv_mgmt
    where supplier = I_supplier 
      for update nowait;
 
BEGIN

  if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',    
                                           'NOT NULL');   
  end if;

  L_table := 'SUP_REPL_DAY';
  SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SUP_DEPT', 'SUP_REPL_DAY', 
                     'Supplier: ' || to_char(I_supplier));
  open C_LOCK_SUP_REPL_DAY;
  SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SUP_DEPT', 'SUP_REPL_DAY', 
                     'Supplier: ' || to_char(I_supplier));
  close C_LOCK_SUP_REPL_DAY;

  SQL_LIB.SET_MARK('DELETE', NULL, 'SUP_REPL_DAY', 
                   'Supplier: ' || to_char(I_supplier));
  delete from sup_repl_day 
        where sup_dept_seq_no in (select sup_dept_seq_no
                                    from sup_inv_mgmt
                                   where supplier = I_supplier);
   ---
  L_table := 'SUP_INV_MGMT';
  SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SUPPLIER', 'SUP_INV_MGMT', 
                     'Supplier: ' || to_char(I_supplier));
  open C_LOCK_SUP_INV_MGMT;
  SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SUPPLIER', 'SUP_INV_MGMT', 
                     'Supplier: ' || to_char(I_supplier));
  close C_LOCK_SUP_INV_MGMT;

  SQL_LIB.SET_MARK('DELETE', NULL, 'SUP_INV_MGMT', 
                   'Supplier: ' || to_char(I_supplier));
  delete from sup_inv_mgmt
        where supplier = I_supplier;

  return TRUE;

EXCEPTION 
   when RECORD_LOCKED then
      O_error_message :=
            SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                               'Supplier: ' || to_char(I_supplier),
                                NULL);
   return false;
   
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END DEL_INV_MGMT;
-------------------------------------------------------------------------------------
FUNCTION INV_MGMT_EXIST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exist         IN OUT BOOLEAN,
                         I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE,
                         I_dept          IN     SUP_INV_MGMT.DEPT%TYPE,
                         I_loc           IN     SUP_INV_MGMT.LOCATION%TYPE)
                         RETURN BOOLEAN IS

L_exists   VARCHAR2(1);
L_program  VARCHAR2(50) := 'SUP_INV_MGMT_SQL.INV_MGMT_EXIST';

cursor C_SUP_EXIST is 
   select 'x'
     from sup_inv_mgmt 
    where supplier = I_supplier 
      and dept     is NULL
      and location is NULL;

cursor C_SUP_DEPT_EXIST is 
   select 'x'
     from sup_inv_mgmt 
    where supplier  = I_supplier 
      and dept      = I_dept
      and location is NULL;
   
cursor C_SUP_DEPT_LOC_EXIST is
   select 'x'
     from sup_inv_mgmt
   where supplier = I_supplier
     and dept     = I_dept
     and location = I_loc;
   
cursor C_SUP_LOC_EXIST is
   select 'x'
     from sup_inv_mgmt
   where supplier = I_supplier
     and dept is NULL
     and location = I_loc;

BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',      
                                            'NOT NULL');   
   end if;
   ---
   O_exist := TRUE;
   ---
   if I_dept is NULL and I_loc is NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_SUP_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier));
      open C_SUP_EXIST;
      SQL_LIB.SET_MARK('FETCH', 'C_SUP_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier));
      fetch C_SUP_EXIST into L_exists;
      ---
      if C_SUP_EXIST%NOTFOUND then
         O_exist := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_SUP_EXIST', 'SUP_INV_MGMT', 
                     'Supplier: ' || to_char(I_supplier));
      close C_SUP_EXIST;
   
   elsif I_dept is NOT NULL and I_loc is NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_SUP_DEPT_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier)||
                       ',Dept: ' || to_char(I_dept));
      open C_SUP_DEPT_EXIST;
      SQL_LIB.SET_MARK('FETCH', 'C_SUP_DEPT_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier)||
                       ',Dept: ' || to_char(I_dept));
      fetch C_SUP_DEPT_EXIST into L_exists;
      ---
      if C_SUP_DEPT_EXIST%NOTFOUND then
         O_exist := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_SUP_DEPT_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier)||
                       ',Dept: ' || to_char(I_dept));
      close C_SUP_DEPT_EXIST;
   
   elsif I_dept is NOT NULL and I_loc is NOT NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_SUP_DEPT_LOC_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier)||
                       ',Dept: ' || to_char(I_dept)||
                       ',Location: ' || to_char(I_loc));
      open C_SUP_DEPT_LOC_EXIST;
      SQL_LIB.SET_MARK('FETCH', 'C_SUP_DEPT_LOC_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier)||
                       ',Dept: ' || to_char(I_dept)||
                       ',Location: ' || to_char(I_loc));
      fetch C_SUP_DEPT_LOC_EXIST into L_exists;
      ---
      if C_SUP_DEPT_LOC_EXIST%NOTFOUND then
         O_exist := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_SUP_DEPT_LOC_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier)||
                       ',Dept: ' || to_char(I_dept)||
                       ',Location: ' || to_char(I_loc));
      close C_SUP_DEPT_LOC_EXIST;
  
   elsif I_dept is NULL and I_loc is NOT NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_SUP_LOC_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier)||
                       ',Location: ' || to_char(I_loc));
      open C_SUP_LOC_EXIST;
      SQL_LIB.SET_MARK('FETCH', 'C_SUP_LOC_EXIST', 'SUP_INV_MGMT', 
                       'Supplieer: ' || to_char(I_supplier)||
                       ',Location: ' || to_char(I_loc));
      fetch C_SUP_LOC_EXIST into L_exists;
      ---
      if C_SUP_LOC_EXIST%NOTFOUND then
         O_exist := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_SUP_LOC_EXIST', 'SUP_INV_MGMT', 
                       'Supplier: ' || to_char(I_supplier)||
                       ',Location: ' || to_char(I_loc));
      close C_SUP_LOC_EXIST;
   end if;

   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END INV_MGMT_EXIST;
---------------------------------------------------------------------------------------
FUNCTION SUP_INV_MGMT_EXIST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist         IN OUT BOOLEAN,
                             I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE)
                             RETURN BOOLEAN IS

L_exists         VARCHAR2(1);
L_program        VARCHAR2(50) := 'SUP_INV_MGMT_SQL.SUP_INV_MGMT_EXIST';

cursor C_SUP_EXIST is 
   select 'x'
     from sup_inv_mgmt 
    where supplier = I_supplier;

BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',      
                                            'NOT NULL');   
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SUP_EXIST', 'SUP_INV_MGMT', 
                    'Supplier: ' || to_char(I_supplier));
   open C_SUP_EXIST;
   SQL_LIB.SET_MARK('FETCH', 'C_SUP_EXIST', 'SUP_INV_MGMT', 
                    'Supplier: ' || to_char(I_supplier));
   fetch C_SUP_EXIST into L_exists;
   ---
   if C_SUP_EXIST%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_SUP_EXIST', 'SUP_INV_MGMT', 
                    'Supplier: ' || to_char(I_supplier));
   close C_SUP_EXIST;
   return TRUE;
 ---
EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END SUP_INV_MGMT_EXIST;
----------------------------------------------------------------------
FUNCTION GET_INV_MGMT_LEVEL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_inv_mgmt_lvl  IN OUT  SUPS.INV_MGMT_LVL%TYPE,
                            I_supplier      IN      SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(60) := 'SUP_INV_MGMT_SQL.GET_INV_MGMT_LEVEL';
 
   cursor C_GET_INV_MGMT_LEVEL is
      select inv_mgmt_lvl
        from sups
       where supplier = I_supplier;

BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_GET_INV_MGMT_LEVEL','SUPS', 
                    'Supplier: '|| to_char(I_supplier));
   open C_GET_INV_MGMT_LEVEL;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_INV_MGMT_LEVEL','SUPS', 
                    'Supplier: '|| to_char(I_supplier));
   fetch C_GET_INV_MGMT_LEVEL into O_inv_mgmt_lvl;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_INV_MGMT_LEVEL','SUPS', 
                    'Supplier: '|| to_char(I_supplier));
   close C_GET_INV_MGMT_LEVEL;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

   return FALSE;
END GET_INV_MGMT_LEVEL;
----------------------------------------------------------------------------------------
FUNCTION GET_POOL_SUPPLIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_pool_supplier   IN OUT   SUP_INV_MGMT.POOL_SUPPLIER%TYPE,
                           I_supplier        IN       SUP_INV_MGMT.SUPPLIER%TYPE,
                           I_dept            IN       SUP_INV_MGMT.DEPT%TYPE,
                           I_location        IN       SUP_INV_MGMT.LOCATION%TYPE)
   return BOOLEAN is

   L_sup_inv_rec       SUP_INV_RECTYPE;

BEGIN
   if SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA(O_error_message,
                                         L_sup_inv_rec,
                                         I_supplier,
                                         I_dept,
                                         I_location) = FALSE then
      return FALSE;
   end if;  

   O_pool_supplier := L_sup_inv_rec.pool_supplier;

   return TRUE;     

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_INV_MGMT_SQL.GET_POOL_SUPPLIER',
                                            to_char(SQLCODE));
      return FALSE;
END GET_POOL_SUPPLIER;
----------------------------------------------------------------------------------------
FUNCTION GET_SCALING_CNSTR_INFO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_scale_cnstr_ind        IN OUT   SUP_INV_MGMT.SCALE_CNSTR_IND%TYPE,
                                O_scale_cnstr_lvl        IN OUT   SUP_INV_MGMT.SCALE_CNSTR_LVL%TYPE,
                                O_scale_cnstr_obj        IN OUT   SUP_INV_MGMT.SCALE_CNSTR_OBJ%TYPE,
                                O_scale_cnstr_type1      IN OUT   SUP_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                                O_scale_cnstr_uom1       IN OUT   SUP_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                                O_scale_cnstr_curr1      IN OUT   SUP_INV_MGMT.SCALE_CNSTR_CURR1%TYPE,
                                O_scale_cnstr_min_val1   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
                                O_scale_cnstr_max_val1   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE,
                                O_scale_cnstr_min_tol1   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_TOL1%TYPE,
                                O_scale_cnstr_max_tol1   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_TOL1%TYPE,
                                O_scale_cnstr_type2      IN OUT   SUP_INV_MGMT.SCALE_CNSTR_TYPE2%TYPE,
                                O_scale_cnstr_uom2       IN OUT   SUP_INV_MGMT.SCALE_CNSTR_UOM2%TYPE,
                                O_scale_cnstr_curr2      IN OUT   SUP_INV_MGMT.SCALE_CNSTR_CURR2%TYPE,
                                O_scale_cnstr_min_val2   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL2%TYPE,
                                O_scale_cnstr_max_val2   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL2%TYPE,
                                O_scale_cnstr_min_tol2   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_TOL2%TYPE,
                                O_scale_cnstr_max_tol2   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_TOL2%TYPE,
                                I_supplier               IN       SUP_INV_MGMT.SUPPLIER%TYPE,
                                I_dept                   IN       SUP_INV_MGMT.DEPT%TYPE,
                                I_location               IN       SUP_INV_MGMT.LOCATION%TYPE) 
   return BOOLEAN is

   L_sup_inv_rec       SUP_INV_RECTYPE;

BEGIN
   if SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA(O_error_message,
                                         L_sup_inv_rec,
                                         I_supplier,
                                         I_dept,
                                         I_location) = FALSE then
      return FALSE;
   end if;  

   O_scale_cnstr_ind      := L_sup_inv_rec.scale_cnstr_ind;
   O_scale_cnstr_lvl      := L_sup_inv_rec.scale_cnstr_lvl;
   O_scale_cnstr_obj      := L_sup_inv_rec.scale_cnstr_obj;
   O_scale_cnstr_type1    := L_sup_inv_rec.scale_cnstr_type1;
   O_scale_cnstr_uom1     := L_sup_inv_rec.scale_cnstr_uom1;
   O_scale_cnstr_curr1    := L_sup_inv_rec.scale_cnstr_curr1;
   O_scale_cnstr_min_val1 := L_sup_inv_rec.scale_cnstr_min_val1;
   O_scale_cnstr_max_val1 := L_sup_inv_rec.scale_cnstr_max_val1;
   O_scale_cnstr_min_tol1 := L_sup_inv_rec.scale_cnstr_min_tol1;
   O_scale_cnstr_max_tol1 := L_sup_inv_rec.scale_cnstr_max_tol1;
   O_scale_cnstr_type2    := L_sup_inv_rec.scale_cnstr_type2;
   O_scale_cnstr_uom2     := L_sup_inv_rec.scale_cnstr_uom2;
   O_scale_cnstr_curr2    := L_sup_inv_rec.scale_cnstr_curr2;
   O_scale_cnstr_min_val2 := L_sup_inv_rec.scale_cnstr_min_val2;
   O_scale_cnstr_max_val2 := L_sup_inv_rec.scale_cnstr_max_val2;
   O_scale_cnstr_min_tol2 := L_sup_inv_rec.scale_cnstr_min_tol2;
   O_scale_cnstr_max_tol2 := L_sup_inv_rec.scale_cnstr_max_tol2;

   return TRUE;     

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_INV_MGMT_SQL.GET_SCALING_CNSTR_INFO',
                                            to_char(SQLCODE));
      return FALSE;
END GET_SCALING_CNSTR_INFO;
----------------------------------------------------------------------------------------
FUNCTION GET_PURCHASE_PICKUP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_purchase_type IN OUT SUP_INV_MGMT.PURCHASE_TYPE%TYPE,
                             O_pickup_loc    IN OUT SUP_INV_MGMT.PICKUP_LOC%TYPE,
                             I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE,
                             I_dept          IN     SUP_INV_MGMT.DEPT%TYPE,
                             I_location      IN     SUP_INV_MGMT.LOCATION%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60) := 'SUP_INV_MGMT_SQL.GET_PURCHASE_PICKUP';
   L_sup_inv_rec    SUP_INV_RECTYPE;

BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');   
   end if;
   ---
   if SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA(O_error_message,
                                         L_sup_inv_rec,
                                         I_supplier,
                                         I_dept,
                                         I_location) = FALSE then
      return FALSE;
   end if;

   O_purchase_type := L_sup_inv_rec.purchase_type;
   if L_sup_inv_rec.purchase_type not in ('DLV') then
      O_pickup_loc := L_sup_inv_rec.pickup_loc;
   else
      O_pickup_loc := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                             SQLERRM,
                                L_program,
                             to_char(SQLCODE));
      return FALSE;
END GET_PURCHASE_PICKUP;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_BRACKET_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE)
RETURN BOOLEAN IS
   L_table          VARCHAR2(30) := 'SUP_INV_MGMT';
   L_program        VARCHAR2(60) := 'SUP_INV_MGMT_SQL.CHECK_BRACKET_INFO';
   L_exists         VARCHAR2(1)  := NULL;
   ---
   cursor C_SIM_BRACKET is
      select 'x'
        from sup_inv_mgmt
       where (threshold_next_bracket is not NULL
              or bracket_type1 is not NULL )
         and supplier = I_supplier;
BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SIM_BRACKET', L_table, 
                    'Supplier: ' || to_char(I_supplier));
   open C_SIM_BRACKET;
   SQL_LIB.SET_MARK('FETCH', 'C_SIM_BRACKET', L_table, 
                    'Supplier: ' || to_char(I_supplier));
   fetch C_SIM_BRACKET into L_exists;
   O_exists := C_SIM_BRACKET%FOUND;
   SQL_LIB.SET_MARK('CLOSE', 'C_SIM_BRACKET', L_table, 
                    'Supplier: ' || to_char(I_supplier));
   close C_SIM_BRACKET;
   ---
   return TRUE; 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_BRACKET_INFO;
---------------------------------------------------------------------------------------------
FUNCTION CLEAR_BRACKET_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE)
RETURN BOOLEAN IS
   ---
   L_table          VARCHAR2(30) := 'SUP_INV_MGMT';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_program        VARCHAR2(60) := 'SUP_INV_MGMT_SQL.CLEAR_BRACKET_INFO';
   ---
cursor C_LOCK_SUP_INV_MGMT is
   select 'x'
     from sup_inv_mgmt          
    where supplier = I_supplier
      for update nowait;
BEGIN
   If I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   -- Lock table for update
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SUP_INV_MGMT', L_table, 
                    'Supplier: ' || to_char(I_supplier));
   open C_LOCK_SUP_INV_MGMT;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SUP_INV_MGMT', L_table, 
                    'Supplier: ' || to_char(I_supplier));
   close C_LOCK_SUP_INV_MGMT;
   ---
   -- Clear out the SUP_INV_MGMT bracket values
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL, 'SUP_INV_MGMT', 
                    'Supplier: ' || to_char(I_supplier));
   ---
   update sup_inv_mgmt
      set threshold_next_bracket = NULL,
            bracket_type1        = NULL,
            bracket_type2        = NULL,
            bracket_uom1         = NULL,
            bracket_uom2         = NULL
        where supplier = I_supplier;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                                            to_char(I_supplier),
                                            NULL);
      return FALSE;
      ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLEAR_BRACKET_INFO;
---------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL,
                          I_supplier            IN       SUP_INV_MGMT.SUPPLIER%TYPE,
                          I_dept                IN       SUP_INV_MGMT.DEPT%TYPE,
                          I_location            IN       SUP_INV_MGMT.LOCATION%TYPE,
                          I_loc_type            IN       SUP_INV_MGMT.LOC_TYPE%TYPE,
                          I_default_from        IN       VARCHAR2)
IS
   L_program         VARCHAR2(64) := 'SUP_INV_MGMT_SQL.QUERY_PROCEDURE';
   L_sup_inv_rec     SUP_INV_RECTYPE;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_sup_dept_seq_no SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE;

   cursor C_QUERY is
      select sup_dept_seq_no,
             supplier,
             dept,
             location,
             loc_type,
             review_cycle,
             'N', -- Sunday_ind
             'N', -- Monday_ind
             'N', -- Tuesday_ind
             'N', -- Wednesday_ind
             'N', -- Thursday_ind
             'N', -- Friday_ind
             'N', -- Saturday_ind
             repl_order_ctrl,
             scale_cnstr_ind,
             scale_cnstr_lvl,
             scale_cnstr_obj,
             scale_cnstr_type1,
             scale_cnstr_uom1,
             scale_cnstr_curr1,
             scale_cnstr_min_val1,
             scale_cnstr_max_val1,
             scale_cnstr_min_tol1,
             scale_cnstr_max_tol1,
             scale_cnstr_type2,
             scale_cnstr_uom2,
             scale_cnstr_curr2,
             scale_cnstr_min_val2,
             scale_cnstr_max_val2,
             scale_cnstr_min_tol2,
             scale_cnstr_max_tol2,
             min_cnstr_lvl,
             min_cnstr_conj,
             min_cnstr_type1,
             min_cnstr_uom1,
             min_cnstr_curr1,
             min_cnstr_val1,
             min_cnstr_type2,
             min_cnstr_uom2,
             min_cnstr_curr2,
             min_cnstr_val2,
             truck_split_ind,
             truck_split_method,
             truck_cnstr_type1,
             truck_cnstr_uom1,
             truck_cnstr_val1,
             truck_cnstr_tol1,
             truck_cnstr_type2,
             truck_cnstr_uom2,
             truck_cnstr_val2,
             truck_cnstr_tol2,
             ltl_approval_ind,
             due_ord_process_ind,
             due_ord_lvl,
             due_ord_serv_basis,
             non_due_ord_create_ind,
             mult_vehicle_ind,
             single_loc_ind,
             round_lvl,
             round_to_inner_pct,
             round_to_case_pct,
             round_to_layer_pct,
             round_to_pallet_pct,
             ord_purge_ind,
             pool_supplier,
             pickup_loc,
             purchase_type,
             threshold_next_bracket,
             bracket_type1,
             bracket_uom1,
             bracket_type2,
             bracket_uom2,
             ib_ind,
             ib_order_ctrl,
             NULL,   ---error_message
             'TRUE'  ---return code
        from sup_inv_mgmt
       where supplier  = I_supplier
          and ((dept     = I_dept and I_dept is NOT NULL)
                 or (dept is NULL and I_dept is NULL))
          and  ((location = I_location and I_location is NOT NULL)
                  or (location is NULL and I_location is NULL));                                              
   
BEGIN
   if I_default_from is NOT NULL then
      ---
      if I_default_from = 'D' then
         if SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA(L_error_message,
                                               L_sup_inv_rec,
                                               I_supplier,
                                               I_dept,
                                               NULL) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;  
      else
         if SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA(L_error_message,
                                               L_sup_inv_rec,
                                               I_supplier,
                                               NULL,
                                               NULL) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;  
      end if;
      ---
      if L_sup_inv_rec.sup_dept_seq_no is NOT NULL then
         ---
         if SUPP_ATTRIB_SQL.NEXT_INV_MGMT_SEQ_NO(L_error_message,
                                                 L_sup_dept_seq_no) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;
         ---
         insert into SUP_INV_MGMT (sup_dept_seq_no,
                                   supplier,
                                   dept,
                                   location,
                                   loc_type,
                                   review_cycle,
                                   repl_order_ctrl,
                                   scale_cnstr_ind,
                                   scale_cnstr_lvl,
                                   scale_cnstr_obj,
                                   scale_cnstr_type1,
                                   scale_cnstr_uom1,
                                   scale_cnstr_curr1,
                                   scale_cnstr_min_val1,
                                   scale_cnstr_max_val1,
                                   scale_cnstr_min_tol1,
                                   scale_cnstr_max_tol1,
                                   scale_cnstr_type2,
                                   scale_cnstr_uom2,
                                   scale_cnstr_curr2,
                                   scale_cnstr_min_val2,
                                   scale_cnstr_max_val2,
                                   scale_cnstr_min_tol2,
                                   scale_cnstr_max_tol2,
                                   min_cnstr_lvl,
                                   min_cnstr_conj,
                                   min_cnstr_type1,
                                   min_cnstr_uom1,
                                   min_cnstr_curr1,
                                   min_cnstr_val1,
                                   min_cnstr_type2,
                                   min_cnstr_uom2,
                                   min_cnstr_curr2,
                                   min_cnstr_val2,
                                   truck_split_ind,
                                   truck_split_method,
                                   truck_cnstr_type1,
                                   truck_cnstr_uom1,
                                   truck_cnstr_val1,
                                   truck_cnstr_tol1,
                                   truck_cnstr_type2,
                                   truck_cnstr_uom2,
                                   truck_cnstr_val2,
                                   truck_cnstr_tol2,
                                   ltl_approval_ind,
                                   due_ord_process_ind,
                                   due_ord_lvl,
                                   due_ord_serv_basis,
                                   non_due_ord_create_ind,
                                   mult_vehicle_ind,
                                   single_loc_ind,
                                   round_lvl,
                                   round_to_inner_pct,
                                   round_to_case_pct,
                                   round_to_layer_pct,
                                   round_to_pallet_pct,
                                   ord_purge_ind,
                                   pool_supplier,
                                   pickup_loc,
                                   purchase_type,
                                   threshold_next_bracket,
                                   bracket_type1,
                                   bracket_uom1,
                                   bracket_type2,
                                   bracket_uom2,
                                   ib_ind,
                                   ib_order_ctrl)
                           values (L_sup_dept_seq_no,
                                   I_supplier,
                                   I_dept,
                                   I_location,
                                   DECODE(I_loc_type,'ST','S','WH','W',I_loc_type),
                                   L_sup_inv_rec.review_cycle,
                                   L_sup_inv_rec.repl_order_ctrl,
                                   L_sup_inv_rec.scale_cnstr_ind,
                                   L_sup_inv_rec.scale_cnstr_lvl,
                                   L_sup_inv_rec.scale_cnstr_obj,
                                   L_sup_inv_rec.scale_cnstr_type1,
                                   L_sup_inv_rec.scale_cnstr_uom1,
                                   L_sup_inv_rec.scale_cnstr_curr1,
                                   L_sup_inv_rec.scale_cnstr_min_val1,
                                   L_sup_inv_rec.scale_cnstr_max_val1,
                                   L_sup_inv_rec.scale_cnstr_min_tol1,
                                   L_sup_inv_rec.scale_cnstr_max_tol1,
                                   L_sup_inv_rec.scale_cnstr_type2,
                                   L_sup_inv_rec.scale_cnstr_uom2,
                                   L_sup_inv_rec.scale_cnstr_curr2,
                                   L_sup_inv_rec.scale_cnstr_min_val2,
                                   L_sup_inv_rec.scale_cnstr_max_val2,
                                   L_sup_inv_rec.scale_cnstr_min_tol2,
                                   L_sup_inv_rec.scale_cnstr_max_tol2,
                                   L_sup_inv_rec.min_cnstr_lvl,
                                   L_sup_inv_rec.min_cnstr_conj,
                                   L_sup_inv_rec.min_cnstr_type1,
                                   L_sup_inv_rec.min_cnstr_uom1,
                                   L_sup_inv_rec.min_cnstr_curr1,
                                   L_sup_inv_rec.min_cnstr_val1,
                                   L_sup_inv_rec.min_cnstr_type2,
                                   L_sup_inv_rec.min_cnstr_uom2,
                                   L_sup_inv_rec.min_cnstr_curr2,
                                   L_sup_inv_rec.min_cnstr_val2,
                                   L_sup_inv_rec.truck_split_ind,
                                   L_sup_inv_rec.truck_split_method,
                                   L_sup_inv_rec.truck_cnstr_type1,
                                   L_sup_inv_rec.truck_cnstr_uom1,
                                   L_sup_inv_rec.truck_cnstr_val1,
                                   L_sup_inv_rec.truck_cnstr_tol1,
                                   L_sup_inv_rec.truck_cnstr_type2,
                                   L_sup_inv_rec.truck_cnstr_uom2,
                                   L_sup_inv_rec.truck_cnstr_val2,
                                   L_sup_inv_rec.truck_cnstr_tol2,
                                   L_sup_inv_rec.ltl_approval_ind,
                                   L_sup_inv_rec.due_ord_process_ind,
                                   L_sup_inv_rec.due_ord_lvl,
                                   L_sup_inv_rec.due_ord_serv_basis,
                                   L_sup_inv_rec.non_due_ord_create_ind,
                                   L_sup_inv_rec.mult_vehicle_ind,
                                   L_sup_inv_rec.single_loc_ind,
                                   L_sup_inv_rec.round_lvl,
                                   L_sup_inv_rec.round_to_inner_pct,
                                   L_sup_inv_rec.round_to_case_pct,
                                   L_sup_inv_rec.round_to_layer_pct,
                                   L_sup_inv_rec.round_to_pallet_pct,
                                   L_sup_inv_rec.ord_purge_ind,
                                   L_sup_inv_rec.pool_supplier,
                                   L_sup_inv_rec.pickup_loc,
                                   L_sup_inv_rec.purchase_type,
                                   DECODE(I_loc_type,'ST',NULL,L_sup_inv_rec.threshold_next_bracket),
                                   DECODE(I_loc_type,'ST',NULL,L_sup_inv_rec.bracket_type1),
                                   DECODE(I_loc_type,'ST',NULL,L_sup_inv_rec.bracket_uom1),
                                   DECODE(I_loc_type,'ST',NULL,L_sup_inv_rec.bracket_type2),
                                   DECODE(I_loc_type,'ST',NULL,L_sup_inv_rec.bracket_uom2),
                                   DECODE(I_loc_type,'ST','N',L_sup_inv_rec.ib_ind),
                                   DECODE(I_loc_type,'ST',NULL,L_sup_inv_rec.ib_order_ctrl));
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_QUERY',
                    'SUP_INV_MGMT',
                    'Supplier - '||I_supplier||' , Dept - '||I_dept||' , Location - '||I_location||' , Loc Type - '||I_loc_type);
   open C_QUERY;

   SQL_LIB.SET_MARK('FETCH',
                    'C_QUERY',
                    'SUP_INV_MGMT',
                    'Supplier - '||I_supplier||' , Dept - '||I_dept||' , Location - '||I_location||' , Loc Type - '||I_loc_type);
   fetch C_QUERY bulk collect into IO_sup_inv_mgmt_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_QUERY',
                    'SUP_INV_MGMT',
                    'Supplier - '||I_supplier||' , Dept - '||I_dept||' , Location - '||I_location||' , Loc Type - '||I_loc_type);
   close C_QUERY;
   --
   if IO_sup_inv_mgmt_tbl(1).sup_dept_seq_no is NOT NULL then
      if REPLENISHMENT_DAYS_SQL.QUERY_DAY_SUPP(L_error_message,
                                               NVL(L_sup_inv_rec.sup_dept_seq_no,IO_sup_inv_mgmt_tbl(1).sup_dept_seq_no),
                                               IO_sup_inv_mgmt_tbl(1).sunday_ind,
                                               IO_sup_inv_mgmt_tbl(1).monday_ind,
                                               IO_sup_inv_mgmt_tbl(1).tuesday_ind,
                                               IO_sup_inv_mgmt_tbl(1).wednesday_ind,
                                               IO_sup_inv_mgmt_tbl(1).thursday_ind,
                                               IO_sup_inv_mgmt_tbl(1).friday_ind,
                                               IO_sup_inv_mgmt_tbl(1).saturday_ind) = FALSE then
         raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
      end if;
   end if;
   ---
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END QUERY_PROCEDURE;
---------------------------------------------------------------------------------------------
-- Private Function
FUNCTION PROCESS_LOC_LIST (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_sup_inv_mgmt_tbl   IN     SUP_INV_MGMT_TBL,
                           I_index              IN     NUMBER)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'PROCESS_LOC_LIST';
   L_sup_inv_rec       SUP_INV_RECTYPE;
   L_sup_dept_seq_no   SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE;

   cursor C_EXPLODE_LOC_LIST is
      select location loc
        from loc_list_detail lld
       where lld.loc_list = I_sup_inv_mgmt_tbl(I_index).location
         and lld.loc_type = 'S';

BEGIN
   for rec in C_EXPLODE_LOC_LIST
   loop
      L_sup_inv_rec := NULL;
      ---
      if SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA(O_error_message,
                                            L_sup_inv_rec,
                                            I_sup_inv_mgmt_tbl(I_index).supplier,
                                            I_sup_inv_mgmt_tbl(I_index).dept,
                                            rec.loc) = FALSE then
         return FALSE;
      end if;  

      if L_sup_inv_rec.sup_dept_seq_no is NOT NULL then
         update sup_inv_mgmt
            set review_cycle            = I_sup_inv_mgmt_tbl(I_index).review_cycle,
                repl_order_ctrl         = I_sup_inv_mgmt_tbl(I_index).repl_order_ctrl,
                scale_cnstr_ind         = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_ind,
                scale_cnstr_lvl         = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_lvl,
                scale_cnstr_obj         = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_obj,
                scale_cnstr_type1       = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_type1,
                scale_cnstr_uom1        = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_uom1,
                scale_cnstr_curr1       = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_curr1,
                scale_cnstr_min_val1    = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_min_val1,
                scale_cnstr_max_val1    = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_max_val1,
                scale_cnstr_min_tol1    = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_min_tol1,
                scale_cnstr_max_tol1    = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_max_tol1,
                scale_cnstr_type2       = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_type2,
                scale_cnstr_uom2        = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_uom2,
                scale_cnstr_curr2       = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_curr2,
                scale_cnstr_min_val2    = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_min_val2,
                scale_cnstr_max_val2    = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_max_val2,
                scale_cnstr_min_tol2    = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_min_tol2,
                scale_cnstr_max_tol2    = I_sup_inv_mgmt_tbl(I_index).scale_cnstr_max_tol2,
                min_cnstr_lvl           = I_sup_inv_mgmt_tbl(I_index).min_cnstr_lvl,
                min_cnstr_conj          = I_sup_inv_mgmt_tbl(I_index).min_cnstr_conj,
                min_cnstr_type1         = I_sup_inv_mgmt_tbl(I_index).min_cnstr_type1,
                min_cnstr_uom1          = I_sup_inv_mgmt_tbl(I_index).min_cnstr_uom1,
                min_cnstr_curr1         = I_sup_inv_mgmt_tbl(I_index).min_cnstr_curr1,
                min_cnstr_val1          = I_sup_inv_mgmt_tbl(I_index).min_cnstr_val1,
                min_cnstr_type2         = I_sup_inv_mgmt_tbl(I_index).min_cnstr_type2,
                min_cnstr_uom2          = I_sup_inv_mgmt_tbl(I_index).min_cnstr_uom2,
                min_cnstr_curr2         = I_sup_inv_mgmt_tbl(I_index).min_cnstr_curr2,
                min_cnstr_val2          = I_sup_inv_mgmt_tbl(I_index).min_cnstr_val2,
                truck_split_ind         = I_sup_inv_mgmt_tbl(I_index).truck_split_ind,
                truck_split_method      = I_sup_inv_mgmt_tbl(I_index).truck_split_method,
                truck_cnstr_type1       = I_sup_inv_mgmt_tbl(I_index).truck_cnstr_type1,
                truck_cnstr_uom1        = I_sup_inv_mgmt_tbl(I_index).truck_cnstr_uom1,
                truck_cnstr_val1        = I_sup_inv_mgmt_tbl(I_index).truck_cnstr_val1,
                truck_cnstr_tol1        = I_sup_inv_mgmt_tbl(I_index).truck_cnstr_tol1,
                truck_cnstr_type2       = I_sup_inv_mgmt_tbl(I_index).truck_cnstr_type2,
                truck_cnstr_uom2        = I_sup_inv_mgmt_tbl(I_index).truck_cnstr_uom2,
                truck_cnstr_val2        = I_sup_inv_mgmt_tbl(I_index).truck_cnstr_val2,
                truck_cnstr_tol2        = I_sup_inv_mgmt_tbl(I_index).truck_cnstr_tol2,
                ltl_approval_ind        = I_sup_inv_mgmt_tbl(I_index).ltl_approval_ind,
                due_ord_process_ind     = I_sup_inv_mgmt_tbl(I_index).due_ord_process_ind,
                due_ord_lvl             = I_sup_inv_mgmt_tbl(I_index).due_ord_lvl,
                due_ord_serv_basis      = I_sup_inv_mgmt_tbl(I_index).due_ord_serv_basis,
                non_due_ord_create_ind  = I_sup_inv_mgmt_tbl(I_index).non_due_ord_create_ind,
                mult_vehicle_ind        = I_sup_inv_mgmt_tbl(I_index).mult_vehicle_ind,
                single_loc_ind          = I_sup_inv_mgmt_tbl(I_index).single_loc_ind,
                round_lvl               = I_sup_inv_mgmt_tbl(I_index).round_lvl,
                round_to_inner_pct      = I_sup_inv_mgmt_tbl(I_index).round_to_inner_pct,
                round_to_case_pct       = I_sup_inv_mgmt_tbl(I_index).round_to_case_pct,
                round_to_layer_pct      = I_sup_inv_mgmt_tbl(I_index).round_to_layer_pct,
                round_to_pallet_pct     = I_sup_inv_mgmt_tbl(I_index).round_to_pallet_pct,
                ord_purge_ind           = I_sup_inv_mgmt_tbl(I_index).ord_purge_ind,
                pool_supplier           = I_sup_inv_mgmt_tbl(I_index).pool_supplier,
                pickup_loc              = I_sup_inv_mgmt_tbl(I_index).pickup_loc,
                purchase_type           = I_sup_inv_mgmt_tbl(I_index).purchase_type,
                threshold_next_bracket  = I_sup_inv_mgmt_tbl(I_index).threshold_next_bracket,
                bracket_type1           = I_sup_inv_mgmt_tbl(I_index).bracket_type1,
                bracket_uom1            = I_sup_inv_mgmt_tbl(I_index).bracket_uom1,
                bracket_type2           = I_sup_inv_mgmt_tbl(I_index).bracket_type2,
                bracket_uom2            = I_sup_inv_mgmt_tbl(I_index).bracket_uom2,
                ib_ind                  = I_sup_inv_mgmt_tbl(I_index).ib_ind,
                ib_order_ctrl           = I_sup_inv_mgmt_tbl(I_index).ib_order_ctrl
          where sup_dept_seq_no         = L_sup_inv_rec.sup_dept_seq_no;
         ---  
         if REPLENISHMENT_DAYS_SQL.UPDATE_DAY_SUPP(O_error_message,
                                                   L_sup_inv_rec.sup_dept_seq_no,
                                                   I_sup_inv_mgmt_tbl(I_index).sunday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).monday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).tuesday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).wednesday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).thursday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).friday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).saturday_ind) = FALSE then
            return FALSE;
         end if;
      else
         ---
         if SUPP_ATTRIB_SQL.NEXT_INV_MGMT_SEQ_NO(O_error_message,
                                                L_sup_dept_seq_no) = FALSE then
            return FALSE;
         end if;
         ---
         insert into sup_inv_mgmt (sup_dept_seq_no,
                                   supplier,
                                   dept,
                                   location,
                                   loc_type,
                                   review_cycle,
                                   repl_order_ctrl,
                                   scale_cnstr_ind,
                                   scale_cnstr_lvl,
                                   scale_cnstr_obj,
                                   scale_cnstr_type1,
                                   scale_cnstr_uom1,
                                   scale_cnstr_curr1,
                                   scale_cnstr_min_val1,
                                   scale_cnstr_max_val1,
                                   scale_cnstr_min_tol1,
                                   scale_cnstr_max_tol1,
                                   scale_cnstr_type2,
                                   scale_cnstr_uom2,
                                   scale_cnstr_curr2,
                                   scale_cnstr_min_val2,
                                   scale_cnstr_max_val2,
                                   scale_cnstr_min_tol2,
                                   scale_cnstr_max_tol2,
                                   min_cnstr_lvl,
                                   min_cnstr_conj,
                                   min_cnstr_type1,
                                   min_cnstr_uom1,
                                   min_cnstr_curr1,
                                   min_cnstr_val1,
                                   min_cnstr_type2,
                                   min_cnstr_uom2,
                                   min_cnstr_curr2,
                                   min_cnstr_val2,
                                   truck_split_ind,
                                   truck_split_method,
                                   truck_cnstr_type1,
                                   truck_cnstr_uom1,
                                   truck_cnstr_val1,
                                   truck_cnstr_tol1,
                                   truck_cnstr_type2,
                                   truck_cnstr_uom2,
                                   truck_cnstr_val2,
                                   truck_cnstr_tol2,
                                   ltl_approval_ind,
                                   due_ord_process_ind,
                                   due_ord_lvl,
                                   due_ord_serv_basis,
                                   non_due_ord_create_ind,
                                   mult_vehicle_ind,
                                   single_loc_ind,
                                   round_lvl,
                                   round_to_inner_pct,
                                   round_to_case_pct,
                                   round_to_layer_pct,
                                   round_to_pallet_pct,
                                   ord_purge_ind,
                                   pool_supplier,
                                   pickup_loc,
                                   purchase_type,
                                   threshold_next_bracket,
                                   bracket_type1,
                                   bracket_uom1,
                                   bracket_type2,
                                   bracket_uom2,
                                   ib_ind,
                                   ib_order_ctrl)
                           values (L_sup_dept_seq_no,
                                   I_sup_inv_mgmt_tbl(I_index).supplier,
                                   I_sup_inv_mgmt_tbl(I_index).dept,
                                   rec.loc,
                                   'S',
                                   I_sup_inv_mgmt_tbl(I_index).review_cycle,
                                   I_sup_inv_mgmt_tbl(I_index).repl_order_ctrl,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_ind,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_lvl,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_obj,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_type1,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_uom1,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_curr1,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_min_val1,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_max_val1,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_min_tol1,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_max_tol1,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_type2,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_uom2,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_curr2,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_min_val2,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_max_val2,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_min_tol2,
                                   I_sup_inv_mgmt_tbl(I_index).scale_cnstr_max_tol2,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_lvl,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_conj,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_type1,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_uom1,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_curr1,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_val1,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_type2,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_uom2,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_curr2,
                                   I_sup_inv_mgmt_tbl(I_index).min_cnstr_val2,
                                   I_sup_inv_mgmt_tbl(I_index).truck_split_ind,
                                   I_sup_inv_mgmt_tbl(I_index).truck_split_method,
                                   I_sup_inv_mgmt_tbl(I_index).truck_cnstr_type1,
                                   I_sup_inv_mgmt_tbl(I_index).truck_cnstr_uom1,
                                   I_sup_inv_mgmt_tbl(I_index).truck_cnstr_val1,
                                   I_sup_inv_mgmt_tbl(I_index).truck_cnstr_tol1,
                                   I_sup_inv_mgmt_tbl(I_index).truck_cnstr_type2,
                                   I_sup_inv_mgmt_tbl(I_index).truck_cnstr_uom2,
                                   I_sup_inv_mgmt_tbl(I_index).truck_cnstr_val2,
                                   I_sup_inv_mgmt_tbl(I_index).truck_cnstr_tol2,
                                   I_sup_inv_mgmt_tbl(I_index).ltl_approval_ind,
                                   I_sup_inv_mgmt_tbl(I_index).due_ord_process_ind,
                                   I_sup_inv_mgmt_tbl(I_index).due_ord_lvl,
                                   I_sup_inv_mgmt_tbl(I_index).due_ord_serv_basis,
                                   I_sup_inv_mgmt_tbl(I_index).non_due_ord_create_ind,
                                   I_sup_inv_mgmt_tbl(I_index).mult_vehicle_ind,
                                   I_sup_inv_mgmt_tbl(I_index).single_loc_ind,
                                   I_sup_inv_mgmt_tbl(I_index).round_lvl,
                                   I_sup_inv_mgmt_tbl(I_index).round_to_inner_pct,
                                   I_sup_inv_mgmt_tbl(I_index).round_to_case_pct,
                                   I_sup_inv_mgmt_tbl(I_index).round_to_layer_pct,
                                   I_sup_inv_mgmt_tbl(I_index).round_to_pallet_pct,
                                   I_sup_inv_mgmt_tbl(I_index).ord_purge_ind,
                                   I_sup_inv_mgmt_tbl(I_index).pool_supplier,
                                   I_sup_inv_mgmt_tbl(I_index).pickup_loc,
                                   I_sup_inv_mgmt_tbl(I_index).purchase_type,
                                   I_sup_inv_mgmt_tbl(I_index).threshold_next_bracket,
                                   I_sup_inv_mgmt_tbl(I_index).bracket_type1,
                                   I_sup_inv_mgmt_tbl(I_index).bracket_uom1,
                                   I_sup_inv_mgmt_tbl(I_index).bracket_type2,
                                   I_sup_inv_mgmt_tbl(I_index).bracket_uom2,
                                   I_sup_inv_mgmt_tbl(I_index).ib_ind,
                                   I_sup_inv_mgmt_tbl(I_index).ib_order_ctrl);

         ---  
         if REPLENISHMENT_DAYS_SQL.INSERT_DAY_SUPP(O_error_message,
                                                   L_sup_dept_seq_no,
                                                   I_sup_inv_mgmt_tbl(I_index).sunday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).monday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).tuesday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).wednesday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).thursday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).friday_ind,
                                                   I_sup_inv_mgmt_tbl(I_index).saturday_ind) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_LOC_LIST;
---------------------------------------------------------------------------------------------
PROCEDURE INSERT_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL)
IS
   L_program         VARCHAR2(64) := 'SUP_INV_MGMT_SQL.INSERT_PROCEDURE';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   for i in 1..IO_sup_inv_mgmt_tbl.count
   loop
      if IO_sup_inv_mgmt_tbl(i).loc_type = 'SL' then
         if PROCESS_LOC_LIST(L_error_message,
                             IO_sup_inv_mgmt_tbl,
                             i) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;
      else
         insert into sup_inv_mgmt (sup_dept_seq_no,
                                   supplier,
                                   dept,
                                   location,
                                   loc_type,
                                   review_cycle,
                                   repl_order_ctrl,
                                   scale_cnstr_ind,
                                   scale_cnstr_lvl,
                                   scale_cnstr_obj,
                                   scale_cnstr_type1,
                                   scale_cnstr_uom1,
                                   scale_cnstr_curr1,
                                   scale_cnstr_min_val1,
                                   scale_cnstr_max_val1,
                                   scale_cnstr_min_tol1,
                                   scale_cnstr_max_tol1,
                                   scale_cnstr_type2,
                                   scale_cnstr_uom2,
                                   scale_cnstr_curr2,
                                   scale_cnstr_min_val2,
                                   scale_cnstr_max_val2,
                                   scale_cnstr_min_tol2,
                                   scale_cnstr_max_tol2,
                                   min_cnstr_lvl,
                                   min_cnstr_conj,
                                   min_cnstr_type1,
                                   min_cnstr_uom1,
                                   min_cnstr_curr1,
                                   min_cnstr_val1,
                                   min_cnstr_type2,
                                   min_cnstr_uom2,
                                   min_cnstr_curr2,
                                   min_cnstr_val2,
                                   truck_split_ind,
                                   truck_split_method,
                                   truck_cnstr_type1,
                                   truck_cnstr_uom1,
                                   truck_cnstr_val1,
                                   truck_cnstr_tol1,
                                   truck_cnstr_type2,
                                   truck_cnstr_uom2,
                                   truck_cnstr_val2,
                                   truck_cnstr_tol2,
                                   ltl_approval_ind,
                                   due_ord_process_ind,
                                   due_ord_lvl,
                                   due_ord_serv_basis,
                                   non_due_ord_create_ind,
                                   mult_vehicle_ind,
                                   single_loc_ind,
                                   round_lvl,
                                   round_to_inner_pct,
                                   round_to_case_pct,
                                   round_to_layer_pct,
                                   round_to_pallet_pct,
                                   ord_purge_ind,
                                   pool_supplier,
                                   pickup_loc,
                                   purchase_type,
                                   threshold_next_bracket,
                                   bracket_type1,
                                   bracket_uom1,
                                   bracket_type2,
                                   bracket_uom2,
                                   ib_ind,
                                   ib_order_ctrl)
                           values (IO_sup_inv_mgmt_tbl(i).sup_dept_seq_no,
                                   IO_sup_inv_mgmt_tbl(i).supplier,
                                   IO_sup_inv_mgmt_tbl(i).dept,
                                   IO_sup_inv_mgmt_tbl(i).location,
                                   DECODE(IO_sup_inv_mgmt_tbl(i).loc_type,'ST','S','WH','W',IO_sup_inv_mgmt_tbl(i).loc_type),
                                   IO_sup_inv_mgmt_tbl(i).review_cycle,
                                   IO_sup_inv_mgmt_tbl(i).repl_order_ctrl,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_ind,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_lvl,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_obj,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_type1,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_uom1,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_curr1,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_val1,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_val1,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_tol1,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_tol1,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_type2,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_uom2,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_curr2,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_val2,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_val2,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_tol2,
                                   IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_tol2,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_lvl,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_conj,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_type1,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_uom1,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_curr1,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_val1,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_type2,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_uom2,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_curr2,
                                   IO_sup_inv_mgmt_tbl(i).min_cnstr_val2,
                                   IO_sup_inv_mgmt_tbl(i).truck_split_ind,
                                   IO_sup_inv_mgmt_tbl(i).truck_split_method,
                                   IO_sup_inv_mgmt_tbl(i).truck_cnstr_type1,
                                   IO_sup_inv_mgmt_tbl(i).truck_cnstr_uom1,
                                   IO_sup_inv_mgmt_tbl(i).truck_cnstr_val1,
                                   IO_sup_inv_mgmt_tbl(i).truck_cnstr_tol1,
                                   IO_sup_inv_mgmt_tbl(i).truck_cnstr_type2,
                                   IO_sup_inv_mgmt_tbl(i).truck_cnstr_uom2,
                                   IO_sup_inv_mgmt_tbl(i).truck_cnstr_val2,
                                   IO_sup_inv_mgmt_tbl(i).truck_cnstr_tol2,
                                   IO_sup_inv_mgmt_tbl(i).ltl_approval_ind,
                                   IO_sup_inv_mgmt_tbl(i).due_ord_process_ind,
                                   IO_sup_inv_mgmt_tbl(i).due_ord_lvl,
                                   IO_sup_inv_mgmt_tbl(i).due_ord_serv_basis,
                                   IO_sup_inv_mgmt_tbl(i).non_due_ord_create_ind,
                                   IO_sup_inv_mgmt_tbl(i).mult_vehicle_ind,
                                   IO_sup_inv_mgmt_tbl(i).single_loc_ind,
                                   IO_sup_inv_mgmt_tbl(i).round_lvl,
                                   IO_sup_inv_mgmt_tbl(i).round_to_inner_pct,
                                   IO_sup_inv_mgmt_tbl(i).round_to_case_pct,
                                   IO_sup_inv_mgmt_tbl(i).round_to_layer_pct,
                                   IO_sup_inv_mgmt_tbl(i).round_to_pallet_pct,
                                   IO_sup_inv_mgmt_tbl(i).ord_purge_ind,
                                   IO_sup_inv_mgmt_tbl(i).pool_supplier,
                                   IO_sup_inv_mgmt_tbl(i).pickup_loc,
                                   IO_sup_inv_mgmt_tbl(i).purchase_type,
                                   IO_sup_inv_mgmt_tbl(i).threshold_next_bracket,
                                   IO_sup_inv_mgmt_tbl(i).bracket_type1,
                                   IO_sup_inv_mgmt_tbl(i).bracket_uom1,
                                   IO_sup_inv_mgmt_tbl(i).bracket_type2,
                                   IO_sup_inv_mgmt_tbl(i).bracket_uom2,
                                   IO_sup_inv_mgmt_tbl(i).ib_ind,
                                   IO_sup_inv_mgmt_tbl(i).ib_order_ctrl);
         ---
         if REPLENISHMENT_DAYS_SQL.INSERT_DAY_SUPP(L_error_message,
                                                   IO_sup_inv_mgmt_tbl(i).sup_dept_seq_no,
                                                   IO_sup_inv_mgmt_tbl(i).sunday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).monday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).tuesday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).wednesday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).thursday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).friday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).saturday_ind) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;
      end if;
   end loop;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END INSERT_PROCEDURE;
---------------------------------------------------------------------------------------------
PROCEDURE DELETE_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL)
IS
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_program         VARCHAR2(64) := 'SUP_INV_MGMT_SQL.DELETE_PROCEDURE';
BEGIN
   for i in 1..IO_sup_inv_mgmt_tbl.count
   loop
      delete sup_inv_mgmt
       where sup_dept_seq_no = IO_sup_inv_mgmt_tbl(i).sup_dept_seq_no;
   end loop;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END DELETE_PROCEDURE;
---------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL)
IS
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_program            VARCHAR2(64) := 'SUP_INV_MGMT_SQL.LOCK_PROCEDURE';
   L_dummy_lock_fetch   VARCHAR2(1);
BEGIN
   for i in 1..IO_sup_inv_mgmt_tbl.count
   loop
      if IO_sup_inv_mgmt_tbl(i).loc_type != 'SL' then
         select 'x'
           into L_dummy_lock_fetch
           from sup_inv_mgmt
          where sup_dept_seq_no = IO_sup_inv_mgmt_tbl(i).sup_dept_seq_no
            for update nowait;
      end if;
   end loop;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END LOCK_PROCEDURE;
---------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL)
IS
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_program         VARCHAR2(64) := 'SUP_INV_MGMT_SQL.UPDATE_PROCEDURE';
BEGIN
   for i in 1..IO_sup_inv_mgmt_tbl.count
   loop
      if IO_sup_inv_mgmt_tbl(i).loc_type = 'SL' then
         if PROCESS_LOC_LIST(L_error_message,
                             IO_sup_inv_mgmt_tbl,
                             i) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;
      else
         update sup_inv_mgmt 
            set review_cycle            = IO_sup_inv_mgmt_tbl(i).review_cycle,
                repl_order_ctrl         = IO_sup_inv_mgmt_tbl(i).repl_order_ctrl,
                scale_cnstr_ind         = IO_sup_inv_mgmt_tbl(i).scale_cnstr_ind,
                scale_cnstr_lvl         = IO_sup_inv_mgmt_tbl(i).scale_cnstr_lvl,
                scale_cnstr_obj         = IO_sup_inv_mgmt_tbl(i).scale_cnstr_obj,
                scale_cnstr_type1       = IO_sup_inv_mgmt_tbl(i).scale_cnstr_type1,
                scale_cnstr_uom1        = IO_sup_inv_mgmt_tbl(i).scale_cnstr_uom1,
                scale_cnstr_curr1       = IO_sup_inv_mgmt_tbl(i).scale_cnstr_curr1,
                scale_cnstr_min_val1    = IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_val1,
                scale_cnstr_max_val1    = IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_val1,
                scale_cnstr_min_tol1    = IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_tol1,
                scale_cnstr_max_tol1    = IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_tol1,
                scale_cnstr_type2       = IO_sup_inv_mgmt_tbl(i).scale_cnstr_type2,
                scale_cnstr_uom2        = IO_sup_inv_mgmt_tbl(i).scale_cnstr_uom2,
                scale_cnstr_curr2       = IO_sup_inv_mgmt_tbl(i).scale_cnstr_curr2,
                scale_cnstr_min_val2    = IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_val2,
                scale_cnstr_max_val2    = IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_val2,
                scale_cnstr_min_tol2    = IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_tol2,
                scale_cnstr_max_tol2    = IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_tol2,
                min_cnstr_lvl           = IO_sup_inv_mgmt_tbl(i).min_cnstr_lvl,
                min_cnstr_conj          = IO_sup_inv_mgmt_tbl(i).min_cnstr_conj,
                min_cnstr_type1         = IO_sup_inv_mgmt_tbl(i).min_cnstr_type1,
                min_cnstr_uom1          = IO_sup_inv_mgmt_tbl(i).min_cnstr_uom1,
                min_cnstr_curr1         = IO_sup_inv_mgmt_tbl(i).min_cnstr_curr1,
                min_cnstr_val1          = IO_sup_inv_mgmt_tbl(i).min_cnstr_val1,
                min_cnstr_type2         = IO_sup_inv_mgmt_tbl(i).min_cnstr_type2,
                min_cnstr_uom2          = IO_sup_inv_mgmt_tbl(i).min_cnstr_uom2,
                min_cnstr_curr2         = IO_sup_inv_mgmt_tbl(i).min_cnstr_curr2,
                min_cnstr_val2          = IO_sup_inv_mgmt_tbl(i).min_cnstr_val2,
                truck_split_ind         = IO_sup_inv_mgmt_tbl(i).truck_split_ind,
                truck_split_method      = IO_sup_inv_mgmt_tbl(i).truck_split_method,
                truck_cnstr_type1       = IO_sup_inv_mgmt_tbl(i).truck_cnstr_type1,
                truck_cnstr_uom1        = IO_sup_inv_mgmt_tbl(i).truck_cnstr_uom1,
                truck_cnstr_val1        = IO_sup_inv_mgmt_tbl(i).truck_cnstr_val1,
                truck_cnstr_tol1        = IO_sup_inv_mgmt_tbl(i).truck_cnstr_tol1,
                truck_cnstr_type2       = IO_sup_inv_mgmt_tbl(i).truck_cnstr_type2,
                truck_cnstr_uom2        = IO_sup_inv_mgmt_tbl(i).truck_cnstr_uom2,
                truck_cnstr_val2        = IO_sup_inv_mgmt_tbl(i).truck_cnstr_val2,
                truck_cnstr_tol2        = IO_sup_inv_mgmt_tbl(i).truck_cnstr_tol2,
                ltl_approval_ind        = IO_sup_inv_mgmt_tbl(i).ltl_approval_ind,
                due_ord_process_ind     = IO_sup_inv_mgmt_tbl(i).due_ord_process_ind,
                due_ord_lvl             = IO_sup_inv_mgmt_tbl(i).due_ord_lvl,
                due_ord_serv_basis      = IO_sup_inv_mgmt_tbl(i).due_ord_serv_basis,
                non_due_ord_create_ind  = IO_sup_inv_mgmt_tbl(i).non_due_ord_create_ind,
                mult_vehicle_ind        = IO_sup_inv_mgmt_tbl(i).mult_vehicle_ind,
                single_loc_ind          = IO_sup_inv_mgmt_tbl(i).single_loc_ind,
                round_lvl               = IO_sup_inv_mgmt_tbl(i).round_lvl,
                round_to_inner_pct      = IO_sup_inv_mgmt_tbl(i).round_to_inner_pct,
                round_to_case_pct       = IO_sup_inv_mgmt_tbl(i).round_to_case_pct,
                round_to_layer_pct      = IO_sup_inv_mgmt_tbl(i).round_to_layer_pct,
                round_to_pallet_pct     = IO_sup_inv_mgmt_tbl(i).round_to_pallet_pct,
                ord_purge_ind           = IO_sup_inv_mgmt_tbl(i).ord_purge_ind,
                pool_supplier           = IO_sup_inv_mgmt_tbl(i).pool_supplier,
                pickup_loc              = IO_sup_inv_mgmt_tbl(i).pickup_loc,
                purchase_type           = IO_sup_inv_mgmt_tbl(i).purchase_type,
                threshold_next_bracket  = IO_sup_inv_mgmt_tbl(i).threshold_next_bracket,
                bracket_type1           = IO_sup_inv_mgmt_tbl(i).bracket_type1,
                bracket_uom1            = IO_sup_inv_mgmt_tbl(i).bracket_uom1,
                bracket_type2           = IO_sup_inv_mgmt_tbl(i).bracket_type2,
                bracket_uom2            = IO_sup_inv_mgmt_tbl(i).bracket_uom2,
                ib_ind                  = IO_sup_inv_mgmt_tbl(i).ib_ind,
                ib_order_ctrl           = IO_sup_inv_mgmt_tbl(i).ib_order_ctrl
          where sup_dept_seq_no         = IO_sup_inv_mgmt_tbl(i).sup_dept_seq_no;
         ---
         if REPLENISHMENT_DAYS_SQL.UPDATE_DAY_SUPP(L_error_message,
                                                   IO_sup_inv_mgmt_tbl(i).sup_dept_seq_no,
                                                   IO_sup_inv_mgmt_tbl(i).sunday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).monday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).tuesday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).wednesday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).thursday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).friday_ind,
                                                   IO_sup_inv_mgmt_tbl(i).saturday_ind) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;
      end if;
   end loop;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END UPDATE_PROCEDURE;
---------------------------------------------------------------------------------------------
PROCEDURE INSERT_PROCEDURE_WRP(IO_sup_inv_mgmt_tbl   IN OUT   WRP_SUP_INV_MGMT_TBL)
IS
   L_program           VARCHAR2(64) := 'SUP_INV_MGMT_SQL.INSERT_PROCEDURE_WRP';
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   
   L_sup_inv_mgmt_rec  SUP_INV_MGMT_SQL.SUP_INV_MGMT_REC;
   L_sup_inv_mgmt_tbl  SUP_INV_MGMT_SQL.SUP_INV_MGMT_TBL;

BEGIN
   --copy the data in database type IO_sup_inv_mgmt_tbl to PLSQL type L_sup_inv_mgmt_tbl
   for i in IO_sup_inv_mgmt_tbl.FIRST .. IO_sup_inv_mgmt_tbl.last loop
      L_sup_inv_mgmt_rec.sup_dept_seq_no        := IO_sup_inv_mgmt_tbl(i).sup_dept_seq_no;
      L_sup_inv_mgmt_rec.supplier               := IO_sup_inv_mgmt_tbl(i).supplier;
      L_sup_inv_mgmt_rec.dept                   := IO_sup_inv_mgmt_tbl(i).dept;
      L_sup_inv_mgmt_rec.location               := IO_sup_inv_mgmt_tbl(i).location;
      L_sup_inv_mgmt_rec.loc_type               := IO_sup_inv_mgmt_tbl(i).loc_type;
      L_sup_inv_mgmt_rec.review_cycle           := IO_sup_inv_mgmt_tbl(i).review_cycle;
      L_sup_inv_mgmt_rec.sunday_ind             := IO_sup_inv_mgmt_tbl(i).sunday_ind;
      L_sup_inv_mgmt_rec.monday_ind             := IO_sup_inv_mgmt_tbl(i).monday_ind;
      L_sup_inv_mgmt_rec.tuesday_ind            := IO_sup_inv_mgmt_tbl(i).tuesday_ind;
      L_sup_inv_mgmt_rec.wednesday_ind          := IO_sup_inv_mgmt_tbl(i).wednesday_ind;
      L_sup_inv_mgmt_rec.thursday_ind           := IO_sup_inv_mgmt_tbl(i).thursday_ind;
      L_sup_inv_mgmt_rec.friday_ind             := IO_sup_inv_mgmt_tbl(i).friday_ind;
      L_sup_inv_mgmt_rec.saturday_ind           := IO_sup_inv_mgmt_tbl(i).saturday_ind;
      L_sup_inv_mgmt_rec.repl_order_ctrl        := IO_sup_inv_mgmt_tbl(i).repl_order_ctrl;
      L_sup_inv_mgmt_rec.scale_cnstr_ind        := IO_sup_inv_mgmt_tbl(i).scale_cnstr_ind;
      L_sup_inv_mgmt_rec.scale_cnstr_lvl        := IO_sup_inv_mgmt_tbl(i).scale_cnstr_lvl;
      L_sup_inv_mgmt_rec.scale_cnstr_obj        := IO_sup_inv_mgmt_tbl(i).scale_cnstr_obj;
      L_sup_inv_mgmt_rec.scale_cnstr_type1      := IO_sup_inv_mgmt_tbl(i).scale_cnstr_type1;
      L_sup_inv_mgmt_rec.scale_cnstr_uom1       := IO_sup_inv_mgmt_tbl(i).scale_cnstr_uom1;
      L_sup_inv_mgmt_rec.scale_cnstr_curr1      := IO_sup_inv_mgmt_tbl(i).scale_cnstr_curr1;
      L_sup_inv_mgmt_rec.scale_cnstr_min_val1   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_val1;
      L_sup_inv_mgmt_rec.scale_cnstr_max_val1   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_val1;
      L_sup_inv_mgmt_rec.scale_cnstr_min_tol1   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_tol1;
      L_sup_inv_mgmt_rec.scale_cnstr_max_tol1   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_tol1;
      L_sup_inv_mgmt_rec.scale_cnstr_type2      := IO_sup_inv_mgmt_tbl(i).scale_cnstr_type2;
      L_sup_inv_mgmt_rec.scale_cnstr_uom2       := IO_sup_inv_mgmt_tbl(i).scale_cnstr_uom2;
      L_sup_inv_mgmt_rec.scale_cnstr_curr2      := IO_sup_inv_mgmt_tbl(i).scale_cnstr_curr2;
      L_sup_inv_mgmt_rec.scale_cnstr_min_val2   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_val2;
      L_sup_inv_mgmt_rec.scale_cnstr_max_val2   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_val2;
      L_sup_inv_mgmt_rec.scale_cnstr_min_tol2   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_tol2;
      L_sup_inv_mgmt_rec.scale_cnstr_max_tol2   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_tol2;
      L_sup_inv_mgmt_rec.min_cnstr_lvl          := IO_sup_inv_mgmt_tbl(i).min_cnstr_lvl;
      L_sup_inv_mgmt_rec.min_cnstr_conj         := IO_sup_inv_mgmt_tbl(i).min_cnstr_conj;
      L_sup_inv_mgmt_rec.min_cnstr_type1        := IO_sup_inv_mgmt_tbl(i).min_cnstr_type1;
      L_sup_inv_mgmt_rec.min_cnstr_uom1         := IO_sup_inv_mgmt_tbl(i).min_cnstr_uom1;
      L_sup_inv_mgmt_rec.min_cnstr_curr1        := IO_sup_inv_mgmt_tbl(i).min_cnstr_curr1;
      L_sup_inv_mgmt_rec.min_cnstr_val1         := IO_sup_inv_mgmt_tbl(i).min_cnstr_val1;
      L_sup_inv_mgmt_rec.min_cnstr_type2        := IO_sup_inv_mgmt_tbl(i).min_cnstr_type2;
      L_sup_inv_mgmt_rec.min_cnstr_uom2         := IO_sup_inv_mgmt_tbl(i).min_cnstr_uom2;
      L_sup_inv_mgmt_rec.min_cnstr_curr2        := IO_sup_inv_mgmt_tbl(i).min_cnstr_curr2;
      L_sup_inv_mgmt_rec.min_cnstr_val2         := IO_sup_inv_mgmt_tbl(i).min_cnstr_val2;
      L_sup_inv_mgmt_rec.truck_split_ind        := IO_sup_inv_mgmt_tbl(i).truck_split_ind;
      L_sup_inv_mgmt_rec.truck_split_method     := IO_sup_inv_mgmt_tbl(i).truck_split_method;
      L_sup_inv_mgmt_rec.truck_cnstr_type1      := IO_sup_inv_mgmt_tbl(i).truck_cnstr_type1;
      L_sup_inv_mgmt_rec.truck_cnstr_uom1       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_uom1;
      L_sup_inv_mgmt_rec.truck_cnstr_val1       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_val1;
      L_sup_inv_mgmt_rec.truck_cnstr_tol1       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_tol1;
      L_sup_inv_mgmt_rec.truck_cnstr_type2      := IO_sup_inv_mgmt_tbl(i).truck_cnstr_type2;
      L_sup_inv_mgmt_rec.truck_cnstr_uom2       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_uom2;
      L_sup_inv_mgmt_rec.truck_cnstr_val2       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_val2;
      L_sup_inv_mgmt_rec.truck_cnstr_tol2       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_tol2;
      L_sup_inv_mgmt_rec.ltl_approval_ind       := IO_sup_inv_mgmt_tbl(i).ltl_approval_ind;
      L_sup_inv_mgmt_rec.due_ord_process_ind    := IO_sup_inv_mgmt_tbl(i).due_ord_process_ind;
      L_sup_inv_mgmt_rec.due_ord_lvl            := IO_sup_inv_mgmt_tbl(i).due_ord_lvl;
      L_sup_inv_mgmt_rec.due_ord_serv_basis     := IO_sup_inv_mgmt_tbl(i).due_ord_serv_basis;
      L_sup_inv_mgmt_rec.non_due_ord_create_ind := IO_sup_inv_mgmt_tbl(i).non_due_ord_create_ind;
      L_sup_inv_mgmt_rec.mult_vehicle_ind       := IO_sup_inv_mgmt_tbl(i).mult_vehicle_ind;
      L_sup_inv_mgmt_rec.single_loc_ind         := IO_sup_inv_mgmt_tbl(i).single_loc_ind;
      L_sup_inv_mgmt_rec.round_lvl              := IO_sup_inv_mgmt_tbl(i).round_lvl;
      L_sup_inv_mgmt_rec.round_to_inner_pct     := IO_sup_inv_mgmt_tbl(i).round_to_inner_pct;
      L_sup_inv_mgmt_rec.round_to_case_pct      := IO_sup_inv_mgmt_tbl(i).round_to_case_pct;
      L_sup_inv_mgmt_rec.round_to_layer_pct     := IO_sup_inv_mgmt_tbl(i).round_to_layer_pct;
      L_sup_inv_mgmt_rec.round_to_pallet_pct    := IO_sup_inv_mgmt_tbl(i).round_to_pallet_pct;
      L_sup_inv_mgmt_rec.ord_purge_ind          := IO_sup_inv_mgmt_tbl(i).ord_purge_ind;
      L_sup_inv_mgmt_rec.pool_supplier          := IO_sup_inv_mgmt_tbl(i).pool_supplier;
      L_sup_inv_mgmt_rec.pickup_loc             := IO_sup_inv_mgmt_tbl(i).pickup_loc;
      L_sup_inv_mgmt_rec.purchase_type          := IO_sup_inv_mgmt_tbl(i).purchase_type;
      L_sup_inv_mgmt_rec.threshold_next_bracket := IO_sup_inv_mgmt_tbl(i).threshold_next_bracket;
      L_sup_inv_mgmt_rec.bracket_type1          := IO_sup_inv_mgmt_tbl(i).bracket_type1;
      L_sup_inv_mgmt_rec.bracket_uom1           := IO_sup_inv_mgmt_tbl(i).bracket_uom1;
      L_sup_inv_mgmt_rec.bracket_type2          := IO_sup_inv_mgmt_tbl(i).bracket_type2;
      L_sup_inv_mgmt_rec.bracket_uom2           := IO_sup_inv_mgmt_tbl(i).bracket_uom2;
      L_sup_inv_mgmt_rec.ib_ind                 := IO_sup_inv_mgmt_tbl(i).ib_ind;
      L_sup_inv_mgmt_rec.ib_order_ctrl          := IO_sup_inv_mgmt_tbl(i).ib_order_ctrl;
      L_sup_inv_mgmt_rec.error_message          := IO_sup_inv_mgmt_tbl(i).error_message;
      L_sup_inv_mgmt_rec.return_code            := IO_sup_inv_mgmt_tbl(i).return_code;

      L_sup_inv_mgmt_tbl(i) := L_sup_inv_mgmt_rec;
   end loop;
   
   INSERT_PROCEDURE( L_sup_inv_mgmt_tbl);
   
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END INSERT_PROCEDURE_WRP;
---------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE_WRP(IO_sup_inv_mgmt_tbl   IN OUT   WRP_SUP_INV_MGMT_TBL)
IS
   L_program           VARCHAR2(64) := 'SUP_INV_MGMT_SQL.UPDATE_PROCEDURE_WRP';
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   
   L_sup_inv_mgmt_rec  SUP_INV_MGMT_SQL.SUP_INV_MGMT_REC;
   L_sup_inv_mgmt_tbl  SUP_INV_MGMT_SQL.SUP_INV_MGMT_TBL;

BEGIN
   --copy the data in database type IO_sup_inv_mgmt_tbl to PLSQL type L_sup_inv_mgmt_tbl
   for i in IO_sup_inv_mgmt_tbl.FIRST .. IO_sup_inv_mgmt_tbl.last loop
      L_sup_inv_mgmt_rec.sup_dept_seq_no        := IO_sup_inv_mgmt_tbl(i).sup_dept_seq_no;
      L_sup_inv_mgmt_rec.supplier               := IO_sup_inv_mgmt_tbl(i).supplier;
      L_sup_inv_mgmt_rec.dept                   := IO_sup_inv_mgmt_tbl(i).dept;
      L_sup_inv_mgmt_rec.location               := IO_sup_inv_mgmt_tbl(i).location;
      L_sup_inv_mgmt_rec.loc_type               := IO_sup_inv_mgmt_tbl(i).loc_type;
      L_sup_inv_mgmt_rec.review_cycle           := IO_sup_inv_mgmt_tbl(i).review_cycle;
      L_sup_inv_mgmt_rec.sunday_ind             := IO_sup_inv_mgmt_tbl(i).sunday_ind;
      L_sup_inv_mgmt_rec.monday_ind             := IO_sup_inv_mgmt_tbl(i).monday_ind;
      L_sup_inv_mgmt_rec.tuesday_ind            := IO_sup_inv_mgmt_tbl(i).tuesday_ind;
      L_sup_inv_mgmt_rec.wednesday_ind          := IO_sup_inv_mgmt_tbl(i).wednesday_ind;
      L_sup_inv_mgmt_rec.thursday_ind           := IO_sup_inv_mgmt_tbl(i).thursday_ind;
      L_sup_inv_mgmt_rec.friday_ind             := IO_sup_inv_mgmt_tbl(i).friday_ind;
      L_sup_inv_mgmt_rec.saturday_ind           := IO_sup_inv_mgmt_tbl(i).saturday_ind;
      L_sup_inv_mgmt_rec.repl_order_ctrl        := IO_sup_inv_mgmt_tbl(i).repl_order_ctrl;
      L_sup_inv_mgmt_rec.scale_cnstr_ind        := IO_sup_inv_mgmt_tbl(i).scale_cnstr_ind;
      L_sup_inv_mgmt_rec.scale_cnstr_lvl        := IO_sup_inv_mgmt_tbl(i).scale_cnstr_lvl;
      L_sup_inv_mgmt_rec.scale_cnstr_obj        := IO_sup_inv_mgmt_tbl(i).scale_cnstr_obj;
      L_sup_inv_mgmt_rec.scale_cnstr_type1      := IO_sup_inv_mgmt_tbl(i).scale_cnstr_type1;
      L_sup_inv_mgmt_rec.scale_cnstr_uom1       := IO_sup_inv_mgmt_tbl(i).scale_cnstr_uom1;
      L_sup_inv_mgmt_rec.scale_cnstr_curr1      := IO_sup_inv_mgmt_tbl(i).scale_cnstr_curr1;
      L_sup_inv_mgmt_rec.scale_cnstr_min_val1   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_val1;
      L_sup_inv_mgmt_rec.scale_cnstr_max_val1   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_val1;
      L_sup_inv_mgmt_rec.scale_cnstr_min_tol1   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_tol1;
      L_sup_inv_mgmt_rec.scale_cnstr_max_tol1   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_tol1;
      L_sup_inv_mgmt_rec.scale_cnstr_type2      := IO_sup_inv_mgmt_tbl(i).scale_cnstr_type2;
      L_sup_inv_mgmt_rec.scale_cnstr_uom2       := IO_sup_inv_mgmt_tbl(i).scale_cnstr_uom2;
      L_sup_inv_mgmt_rec.scale_cnstr_curr2      := IO_sup_inv_mgmt_tbl(i).scale_cnstr_curr2;
      L_sup_inv_mgmt_rec.scale_cnstr_min_val2   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_val2;
      L_sup_inv_mgmt_rec.scale_cnstr_max_val2   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_val2;
      L_sup_inv_mgmt_rec.scale_cnstr_min_tol2   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_min_tol2;
      L_sup_inv_mgmt_rec.scale_cnstr_max_tol2   := IO_sup_inv_mgmt_tbl(i).scale_cnstr_max_tol2;
      L_sup_inv_mgmt_rec.min_cnstr_lvl          := IO_sup_inv_mgmt_tbl(i).min_cnstr_lvl;
      L_sup_inv_mgmt_rec.min_cnstr_conj         := IO_sup_inv_mgmt_tbl(i).min_cnstr_conj;
      L_sup_inv_mgmt_rec.min_cnstr_type1        := IO_sup_inv_mgmt_tbl(i).min_cnstr_type1;
      L_sup_inv_mgmt_rec.min_cnstr_uom1         := IO_sup_inv_mgmt_tbl(i).min_cnstr_uom1;
      L_sup_inv_mgmt_rec.min_cnstr_curr1        := IO_sup_inv_mgmt_tbl(i).min_cnstr_curr1;
      L_sup_inv_mgmt_rec.min_cnstr_val1         := IO_sup_inv_mgmt_tbl(i).min_cnstr_val1;
      L_sup_inv_mgmt_rec.min_cnstr_type2        := IO_sup_inv_mgmt_tbl(i).min_cnstr_type2;
      L_sup_inv_mgmt_rec.min_cnstr_uom2         := IO_sup_inv_mgmt_tbl(i).min_cnstr_uom2;
      L_sup_inv_mgmt_rec.min_cnstr_curr2        := IO_sup_inv_mgmt_tbl(i).min_cnstr_curr2;
      L_sup_inv_mgmt_rec.min_cnstr_val2         := IO_sup_inv_mgmt_tbl(i).min_cnstr_val2;
      L_sup_inv_mgmt_rec.truck_split_ind        := IO_sup_inv_mgmt_tbl(i).truck_split_ind;
      L_sup_inv_mgmt_rec.truck_split_method     := IO_sup_inv_mgmt_tbl(i).truck_split_method;
      L_sup_inv_mgmt_rec.truck_cnstr_type1      := IO_sup_inv_mgmt_tbl(i).truck_cnstr_type1;
      L_sup_inv_mgmt_rec.truck_cnstr_uom1       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_uom1;
      L_sup_inv_mgmt_rec.truck_cnstr_val1       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_val1;
      L_sup_inv_mgmt_rec.truck_cnstr_tol1       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_tol1;
      L_sup_inv_mgmt_rec.truck_cnstr_type2      := IO_sup_inv_mgmt_tbl(i).truck_cnstr_type2;
      L_sup_inv_mgmt_rec.truck_cnstr_uom2       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_uom2;
      L_sup_inv_mgmt_rec.truck_cnstr_val2       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_val2;
      L_sup_inv_mgmt_rec.truck_cnstr_tol2       := IO_sup_inv_mgmt_tbl(i).truck_cnstr_tol2;
      L_sup_inv_mgmt_rec.ltl_approval_ind       := IO_sup_inv_mgmt_tbl(i).ltl_approval_ind;
      L_sup_inv_mgmt_rec.due_ord_process_ind    := IO_sup_inv_mgmt_tbl(i).due_ord_process_ind;
      L_sup_inv_mgmt_rec.due_ord_lvl            := IO_sup_inv_mgmt_tbl(i).due_ord_lvl;
      L_sup_inv_mgmt_rec.due_ord_serv_basis     := IO_sup_inv_mgmt_tbl(i).due_ord_serv_basis;
      L_sup_inv_mgmt_rec.non_due_ord_create_ind := IO_sup_inv_mgmt_tbl(i).non_due_ord_create_ind;
      L_sup_inv_mgmt_rec.mult_vehicle_ind       := IO_sup_inv_mgmt_tbl(i).mult_vehicle_ind;
      L_sup_inv_mgmt_rec.single_loc_ind         := IO_sup_inv_mgmt_tbl(i).single_loc_ind;
      L_sup_inv_mgmt_rec.round_lvl              := IO_sup_inv_mgmt_tbl(i).round_lvl;
      L_sup_inv_mgmt_rec.round_to_inner_pct     := IO_sup_inv_mgmt_tbl(i).round_to_inner_pct;
      L_sup_inv_mgmt_rec.round_to_case_pct      := IO_sup_inv_mgmt_tbl(i).round_to_case_pct;
      L_sup_inv_mgmt_rec.round_to_layer_pct     := IO_sup_inv_mgmt_tbl(i).round_to_layer_pct;
      L_sup_inv_mgmt_rec.round_to_pallet_pct    := IO_sup_inv_mgmt_tbl(i).round_to_pallet_pct;
      L_sup_inv_mgmt_rec.ord_purge_ind          := IO_sup_inv_mgmt_tbl(i).ord_purge_ind;
      L_sup_inv_mgmt_rec.pool_supplier          := IO_sup_inv_mgmt_tbl(i).pool_supplier;
      L_sup_inv_mgmt_rec.pickup_loc             := IO_sup_inv_mgmt_tbl(i).pickup_loc;
      L_sup_inv_mgmt_rec.purchase_type          := IO_sup_inv_mgmt_tbl(i).purchase_type;
      L_sup_inv_mgmt_rec.threshold_next_bracket := IO_sup_inv_mgmt_tbl(i).threshold_next_bracket;
      L_sup_inv_mgmt_rec.bracket_type1          := IO_sup_inv_mgmt_tbl(i).bracket_type1;
      L_sup_inv_mgmt_rec.bracket_uom1           := IO_sup_inv_mgmt_tbl(i).bracket_uom1;
      L_sup_inv_mgmt_rec.bracket_type2          := IO_sup_inv_mgmt_tbl(i).bracket_type2;
      L_sup_inv_mgmt_rec.bracket_uom2           := IO_sup_inv_mgmt_tbl(i).bracket_uom2;
      L_sup_inv_mgmt_rec.ib_ind                 := IO_sup_inv_mgmt_tbl(i).ib_ind;
      L_sup_inv_mgmt_rec.ib_order_ctrl          := IO_sup_inv_mgmt_tbl(i).ib_order_ctrl;
      L_sup_inv_mgmt_rec.error_message          := IO_sup_inv_mgmt_tbl(i).error_message;
      L_sup_inv_mgmt_rec.return_code            := IO_sup_inv_mgmt_tbl(i).return_code;

      L_sup_inv_mgmt_tbl(i) := L_sup_inv_mgmt_rec;
   end loop;
   
   UPDATE_PROCEDURE( L_sup_inv_mgmt_tbl);

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END UPDATE_PROCEDURE_WRP;
---------------------------------------------------------------------------------------------
END;
/