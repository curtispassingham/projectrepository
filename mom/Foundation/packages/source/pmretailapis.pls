CREATE OR REPLACE PACKAGE PM_RETAIL_API_SQL AUTHID CURRENT_USER IS

--------------------------------------------------------------------------------
-- Data Types
--------------------------------------------------------------------------------
ZONE_ID                    RPM_ZONE.ZONE_ID%TYPE;
ZONE_NAME                  RPM_ZONE.NAME%TYPE;
ZONE_GROUP_ID              RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
ZONE_GROUP_NAME            RPM_ZONE_GROUP.NAME%TYPE;
MARKUP_PERCENT             RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE;

-- Define errors so procedures can raise_application_error
ERRNUM_PACKAGE_CALL        NUMBER(6) := -20020;
ERRNUM_INVALID_PARAM       NUMBER(6) := -20021;
ERRNUM_OTHERS              NUMBER(6) := -20022;
---
ERROR_PACKAGE_CALL         EXCEPTION;
ERROR_INVALID_PARAM        EXCEPTION;
ERROR_OTHERS               EXCEPTION;
---
PRAGMA EXCEPTION_INIT(ERROR_PACKAGE_CALL, -20020);
PRAGMA EXCEPTION_INIT(ERROR_INVALID_PARAM, -20021);
PRAGMA EXCEPTION_INIT(ERROR_OTHERS, -20022);

--------------------

TYPE ITEM_PRICING_REC IS RECORD
( -- From pricing system
 ITEM                        ITEM_MASTER.ITEM%TYPE,
 RPM_ZONE_GROUP_ID           RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
 ZONE_ID                     PM_RETAIL_API_SQL.ZONE_ID%TYPE,
 ZONE_DISPLAY_ID             PM_RETAIL_API_SQL.ZONE_ID%TYPE,
 ZONE_DESCRIPTION            PM_RETAIL_API_SQL.ZONE_NAME%TYPE,
 UNIT_RETAIL                 ITEM_LOC.UNIT_RETAIL%TYPE,
 SELLING_UNIT_RETAIL         ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
 SELLING_UOM                 ITEM_LOC.SELLING_UOM%TYPE,
 MULTI_UNITS                 ITEM_LOC.MULTI_UNITS%TYPE,
 MULTI_UNIT_RETAIL           ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
 MULTI_SELLING_UOM           ITEM_LOC.MULTI_SELLING_UOM%TYPE,
 BASE_RETAIL_IND             VARCHAR2(1),
 CURRENCY_CODE               CURRENCIES.CURRENCY_CODE%TYPE,
 STANDARD_UOM                ITEM_LOC.SELLING_UOM%TYPE,
 --- Calculated after calling pricing system
 SELLING_MARK_UP             NUMBER,
 MULTI_SELLING_MARK_UP       NUMBER,
 --- Set by item retail form
 DEFAULT_TO_CHILDREN         VARCHAR2(1),
 --- Currency conversion values
 UNIT_RETAIL_PRIM            ITEM_LOC.UNIT_RETAIL%TYPE,
 SELLING_UNIT_RETAIL_PRIM    ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
 MULTI_UNIT_RETAIL_PRIM      ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
 UNIT_RETAIL_EURO            ITEM_LOC.UNIT_RETAIL%TYPE,
 SELLING_UNIT_RETAIL_EURO    ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
 MULTI_UNIT_RETAIL_EURO      ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
 --- error handling.  These are necessary because the Form's auto generated query
 ---  triggers do not contain exception handling (meaning they will not correctly
 ---  decode a raised application error.)
 ERROR_MESSAGE               RTK_ERRORS.RTK_TEXT%TYPE,
 RETURN_CODE                 VARCHAR2(5),
 AREA_DIFF_ZONE_IND          NUMBER(1)
);

TYPE ITEM_PRICING_TABLE is TABLE OF ITEM_PRICING_REC INDEX BY BINARY_INTEGER;

--------------------

TYPE CHILD_ITEM_PRICING_REC IS RECORD
(CHILD_ITEM           ITEM_MASTER.ITEM%TYPE,
 UNIT_RETAIL          ITEM_LOC.UNIT_RETAIL%TYPE,
 STANDARD_UOM         ITEM_MASTER.STANDARD_UOM%TYPE,
 CURRENCY_CODE        CURRENCIES.CURRENCY_CODE%TYPE,
 SELLING_UNIT_RETAIL  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
 SELLING_UOM          ITEM_LOC.SELLING_UOM%TYPE
);

TYPE CHILD_ITEM_PRICING_TABLE is TABLE OF CHILD_ITEM_PRICING_REC INDEX BY BINARY_INTEGER;

--------------------

TYPE LOCATION_REC IS RECORD
(LOCATION_ID    STORE.STORE%TYPE,
 LOCATION_NAME  STORE.STORE_NAME%TYPE,
 ERROR_MESSAGE  RTK_ERRORS.RTK_TEXT%TYPE,
 RETURN_CODE    VARCHAR2(5)
);

TYPE LOCATION_TABLE IS TABLE OF LOCATION_REC INDEX BY BINARY_INTEGER;
--------------------------------------------------------------------------------

--TYPE ZONE_ID_REC is RECORD (zone_id     PRICE_ZONE.ZONE_ID%TYPE,
--                            zone_desc   PRICE_ZONE.DESCRIPTION%TYPE);
--TYPE ZONE_ID_TBL is TABLE OF ZONE_ID_REC index by BINARY_INTEGER;

--------------------------------------------------------------------------------
-- GET_UNIT_RETAIL
-- This function will fetch the unit retail using the price_hist table, *NOT*
--  RPM.  this function is used in ediupack.pc and rplext.pc.  if a retail is
-- needed for a certain loc PRICING_ATTRIB_SQL.GET_RETAIL should be called. if
-- a retail is needed and a loc is not specified PRICING_ATTRIB_SQL.GET_BASE_ZONE_RETAIL
-- should be called. (both these functions call the appropriate function into RPM)
--------------------------------------------------------------------------------
FUNCTION GET_UNIT_RETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_regular_retail    IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                         O_promotion_retail  IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                         O_promotion_ind     IN OUT  BOOLEAN,
                         O_current_retail    IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                         O_clearance_ind     IN OUT  BOOLEAN,
                         I_item              IN      ITEM_MASTER.ITEM%TYPE,
                         I_location_type     IN      ITEM_LOC.LOC_TYPE%TYPE,
                         I_location          IN      ITEM_LOC.LOC%TYPE,
                         I_tran_date         IN      PRICE_HIST.ACTION_DATE%TYPE,
                         I_tran_type         IN      VARCHAR,
                         I_pack_process_ind  IN      BOOLEAN,
                         I_class_vat_ind     IN      BOOLEAN,
                         I_vat_rate          IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- GET_ITEM_PRICING_INFO
-- This procedure will get a collection of pricing records.  It must be a
-- procedure because a form block is based on it.
--------------------------------------------------------------------------------
PROCEDURE GET_ITEM_PRICING_INFO(O_item_pricing_table  IN OUT  PM_RETAIL_API_SQL.ITEM_PRICING_TABLE,
                                I_item                IN      ITEM_MASTER.ITEM%TYPE,
                                I_dept                IN      ITEM_MASTER.DEPT%TYPE,
                                I_class               IN      ITEM_MASTER.CLASS%TYPE,
                                I_subclass            IN      ITEM_MASTER.SUBCLASS%TYPE,
                                I_cost_currency_code  IN      CURRENCIES.CURRENCY_CODE%TYPE,
                                I_cost                IN      ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                                I_called_from_form    IN      VARCHAR2 DEFAULT 'N');
--------------------------------------------------------------------------------
-- GET_ZONE_LOCATIONS
-- This procedure will get a collection of location records.
--------------------------------------------------------------------------------
PROCEDURE GET_ZONE_LOCATIONS(O_location_table      IN OUT  NOCOPY OBJ_RPM_LOC_TBL,
                             I_rpm_zone_group_id   IN             PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE,
                             I_zone_id             IN             PM_RETAIL_API_SQL.ZONE_ID%TYPE);
--------------------------------------------------------------------------------
-- GET_ZONE_LOCATIONS
-- This procedure will get a collection of location records.  It must be a
-- procedure because a form block is based on it.
--------------------------------------------------------------------------------
PROCEDURE GET_ZONE_LOCATIONS(O_location_table      IN OUT  PM_RETAIL_API_SQL.LOCATION_TABLE,
                             I_rpm_zone_group_id   IN      PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE,
                             I_zone_id             IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE);
--------------------------------------------------------------------------------
PROCEDURE SET_ITEM_PRICING_INFO(I_item_pricing_table  IN  ITEM_PRICING_TABLE);
--------------------------------------------------------------------------------
FUNCTION SET_CHILD_ITEM_PRICING_INFO(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_parent_item               IN      ITEM_MASTER.ITEM%TYPE,
                                     I_zone_id                   IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE,
                                     I_child_item_pricing_table  IN      CHILD_ITEM_PRICING_TABLE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION LOCK_PRICING_RECORDS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_lock_success   IN OUT  BOOLEAN,
                              I_item           IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- APPLY_VAT
-- This function will accept a pricing table from price manangment containing a
-- group of items and their associated location, loc type
--------------------------------------------------------------------------------
PROCEDURE APPLY_VAT(O_error_message                OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_success                      OUT  VARCHAR2,
                    IO_obj_item_pricing_table   IN OUT  NOCOPY OBJ_ITEM_PRICING_TBL);
--------------------------------------------------------------------------------
-- MERCH_PRICING_DEFS_EXIST
-- This function will check to see if the primary zone information has been set
-- up in RPM.
--------------------------------------------------------------------------------
FUNCTION MERCH_PRICING_DEFS_EXIST(O_error_message       IN OUT  VARCHAR2,
                                  O_success             IN OUT  VARCHAR2,
                                  I_dept                IN      DEPS.DEPT%TYPE,
                                  I_class               IN      CLASS.CLASS%TYPE,
                                  I_subclass            IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_RPM_PRICING_WRAPPER(O_error_message          OUT  VARCHAR2,
                                 O_item_pricing_table     OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
                                 I_item_parent         IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                 I_item                IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                 I_dept                IN      ITEM_MASTER.DEPT%TYPE,
                                 I_class               IN      ITEM_MASTER.CLASS%TYPE,
                                 I_subclass            IN      ITEM_MASTER.SUBCLASS%TYPE,
                                 I_cost_currency_code  IN      STORE.CURRENCY_CODE%TYPE,
                                 I_cost                IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION SET_RPM_PRICING_WRAPPER(O_error_message          OUT  VARCHAR2,
                                 I_item_pricing_table  IN      OBJ_ITEM_PRICING_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION BUILD_PRICING_OBJECT_WRAPPER(O_error_message           OUT  VARCHAR2,
                                      O_item_pricing_table      OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
                                      I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                                      I_dept                 IN      DEPS.DEPT%TYPE,
                                      I_class                IN      CLASS.CLASS%TYPE,
                                      I_subclass             IN      SUBCLASS.SUBCLASS%TYPE,
                                      I_price_currency_code  IN      CURRENCIES.CURRENCY_CODE%TYPE,
                                      I_retail_price         IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CHECK_RETAIL_EXISTS (O_error_message     OUT  VARCHAR2,
                              O_exists            OUT  VARCHAR2,
                              I_item           IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION COMPARE_RETAIL_DEF(O_error_message           OUT  VARCHAR2,
                            O_equal                   OUT  VARCHAR2,
                            I_new_dept             IN      DEPS.DEPT%TYPE,
                            I_new_class            IN      CLASS.CLASS%TYPE,
                            I_new_subclass         IN      SUBCLASS.SUBCLASS%TYPE,
                            I_old_dept             IN      DEPS.DEPT%TYPE,
                            I_old_class            IN      CLASS.CLASS%TYPE,
                            I_old_subclass         IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION DELETE_RPM_ITEM_ZONE_PRICE(O_error_message           OUT  VARCHAR2,
                                    I_item                 IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- GET_RPM_MARKUP_INFO
-- This function will fetch the RPM markup calc type and percent
--------------------------------------------------------------------------------
FUNCTION GET_RPM_MARKUP_INFO(O_error_message        IN OUT  VARCHAR2,
                             O_markup_calc_type     IN OUT  DEPS.MARKUP_CALC_TYPE%TYPE,
                             O_markup_percent       IN OUT  NUMBER,
                             I_dept                 IN      DEPS.DEPT%TYPE,
                             I_class                IN      CLASS.CLASS%TYPE,
                             I_subclass             IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- GET_RPM_ITEM_ZONE_PRICE_WRAPPER
-- This function will call MERCH_RETAIL_API_SQL.GET_RPM_ITEM_ZONE_PRICE.
--------------------------------------------------------------------------------
FUNCTION GET_RPM_ITEM_ZONE_PRC_WRAPPER(O_error_message                IN OUT   VARCHAR2,
                                       I_item                         IN       rpm_item_zone_price.item%TYPE,
                                       I_zone_id                      IN       rpm_item_zone_price.zone_id%TYPE,
                                       O_standard_retail              IN OUT   rpm_item_zone_price.standard_retail%TYPE,
                                       O_standard_retail_currency     IN OUT   rpm_item_zone_price.standard_retail_currency%TYPE,
                                       O_standard_uom                 IN OUT   rpm_item_zone_price.standard_uom%TYPE,
                                       O_selling_retail               IN OUT   rpm_item_zone_price.selling_retail%TYPE,
                                       O_selling_retail_currency      IN OUT   rpm_item_zone_price.selling_retail_currency%TYPE,
                                       O_selling_uom                  IN OUT   rpm_item_zone_price.selling_uom%TYPE,
                                       O_multi_units                  IN OUT   rpm_item_zone_price.multi_units%TYPE,
                                       O_multi_unit_retail            IN OUT   rpm_item_zone_price.multi_unit_retail%TYPE,
                                       O_multi_unit_retail_currency   IN OUT   rpm_item_zone_price.multi_unit_retail_currency%TYPE,
                                       O_multi_selling_uom            IN OUT   rpm_item_zone_price.multi_selling_uom%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- SET_GTT_ZONE_INFO
-- This function will get the pricing object returned by RPM and insert the zone
-- information to the GTT_ZONE_INFO temp table.
--------------------------------------------------------------------------------
FUNCTION SET_GTT_ZONE_INFO(O_error_message        IN OUT  VARCHAR2,
                           O_rpm_zone_group_id    IN OUT  RPM_ZONE.ZONE_GROUP_ID%TYPE,
                           O_rpm_base_zone        IN OUT  RPM_ZONE.ZONE_ID%TYPE,
                           O_rpm_display_zone_id  IN OUT  RPM_ZONE.ZONE_DISPLAY_ID%TYPE,
                           I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                           I_dept                 IN      DEPS.DEPT%TYPE,
                           I_class                IN      CLASS.CLASS%TYPE,
                           I_subclass             IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- GET_BASE_RETAIL
-- This function will call RPM to get a base retail value for instances where
-- a location is not available and a retail value is needed (ex. on item master
-- a retail value is displayed, but it's not location specific)
--------------------------------------------------------------------------------
FUNCTION GET_BASE_RETAIL(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_selling_retail       IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                         O_selling_uom          IN OUT  ITEM_LOC.SELLING_UOM%TYPE,
                         O_multi_units          IN OUT  ITEM_LOC.MULTI_UNITS%TYPE,
                         O_multi_unit_retail    IN OUT  ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                         O_multi_selling_uom    IN OUT  ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                         O_currency_code        IN OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                         I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                         I_dept                 IN      DEPS.DEPT%TYPE,
                         I_class                IN      CLASS.CLASS%TYPE,
                         I_subclass             IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- GET_NEW_ITEM_LOC_RETAIL
-- This function will call RPM to get a retail value for a non ranged location.
-- the values will be returned in the currency of the passed in I_location
---------------------------------------------------------------------------------
FUNCTION GET_NEW_ITEM_LOC_RETAIL(O_error_message        IN OUT  VARCHAR2,
                                 O_selling_retail       IN OUT  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                 O_selling_uom          IN OUT  ITEM_LOC.SELLING_UOM%TYPE,
                                 O_multi_units          IN OUT  ITEM_LOC.MULTI_UNITS%TYPE,
                                 O_multi_unit_retail    IN OUT  ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                 O_multi_selling_uom    IN OUT  ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                 I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                                 I_dept                 IN      DEPS.DEPT%TYPE,
                                 I_class                IN      CLASS.CLASS%TYPE,
                                 I_subclass             IN      SUBCLASS.SUBCLASS%TYPE,
                                 I_location             IN      ITEM_LOC.LOC%TYPE,
                                 I_loc_type             IN      ITEM_LOC.LOC_TYPE%TYPE,
                                 I_unit_cost            IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                 I_unit_cost_curr_code  IN      CURRENCIES.CURRENCY_CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- GET_ZONE_VAT_RATE
-- This function will return the VAT rate for a given RPM Zone ID, Dept and Item.
-- If the stores in the Zone belong to different VAT regions, thus having different
-- VAT rates, the average VAT rate is computed. This is not considered a common
-- practice but this is how the code manages this rarity.
-- If all the stores in the Zone have the same VAT rates (belonging to the same
-- VAT region), computation logic will still be the same but this is essentially
-- similar to getting the VAT rate for one of the stores in the zone.
--
-- Any changes due to fixes or functional enhancements to this function should be applied
-- to the APPLY_VAT function in this package.
-- The APPLY_VAT function is a bulk implementation of this function.
--------------------------------------------------------------------------------
FUNCTION GET_ZONE_VAT_RATE(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_vat_rate             IN OUT  VAT_CODE_RATES.VAT_RATE%TYPE,
                           O_tax_amount           IN OUT  GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE,
                           I_retail_zone_id       IN      RPM_ZONE.ZONE_ID%TYPE,
                           I_dept                 IN      ITEM_MASTER.DEPT%TYPE,
                           I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                           I_unit_retail          IN      ITEM_LOC.UNIT_RETAIL%TYPE DEFAULT NULL,
                           I_effective_date       IN      PERIOD.VDATE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- GET_BASE_ZONE
-- This function will get the base zone id from the rpm_zone table.
---------------------------------------------------------------------------------
FUNCTION GET_BASE_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_zone_id         IN OUT   RPM_ZONE.ZONE_ID%TYPE,
                       I_zone_group_id   IN       RPM_ZONE.ZONE_GROUP_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--- GET_AREA_DIFF_PRICE
--- This function will return the secondary area retail price if the area differential
--- is defined for a department/zone combination.
--------------------------------------------------------------------------------
FUNCTION GET_AREA_DIFF_PRICE(O_error_message          OUT  VARCHAR2,
                             O_selling_retail         OUT  RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                             I_zone_group_id       IN      RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                             I_item                IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                             I_sec_zone_id         IN      RPM_ZONE.ZONE_ID%TYPE,
                             I_sec_currency_code   IN      RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                             I_prm_zone_id         IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                             I_prm_selling_retail  IN      RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                             I_prm_selling_uom     IN      RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                             I_prm_currency_code   IN      RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_PRICING_ZONE_ID(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_zone_id         IN OUT OBJ_ZONE_TBL,
                             O_exists          IN OUT BOOLEAN,
                             I_new_loc         IN     ITEM_LOC.LOC%TYPE,
                             I_new_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_zone_id         IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
---GET_ITEM_PRICING_INFO_WRP
---This function  is a wrapper function for GET_ITEM_PRICING_INFO that returns
---a database object type to be called from Java
--------------------------------------------------------------------------------
PROCEDURE GET_ITEM_PRICING_INFO_WRP(O_item_pricing_table IN OUT WRP_ITEM_PRICING_TBL,
                                    I_item               IN ITEM_MASTER.ITEM%TYPE,
                                    I_dept               IN ITEM_MASTER.DEPT%TYPE,
                                    I_class              IN ITEM_MASTER.CLASS%TYPE,
                                    I_subclass           IN ITEM_MASTER.SUBCLASS%TYPE,
                                    I_cost_currency_code IN CURRENCIES.CURRENCY_CODE%TYPE,
                                    I_cost               IN ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                                    I_called_from_form   IN VARCHAR2 DEFAULT 'N' );
-------------------------------------------------------------------------------
---GET_ZONE_LOCATIONS_WRP
---This function  is a wrapper function for GET_ZONE_LOCATIONS_WRP that returns
---a database object type to be called from Java
--------------------------------------------------------------------------------
PROCEDURE GET_ZONE_LOCATIONS_WRP(O_location_table    IN OUT WRP_LOCATION_TBL,
                                 I_rpm_zone_group_id IN PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE,
                                 I_zone_id           IN PM_RETAIL_API_SQL.ZONE_ID%TYPE );
---------------------------------------------------------------------------------
---SET_ITEM_PRICING_INFO_WRP
---This function  is a wrapper function for GET_ZONE_LOCATIONS_WRP that takes
---a database object type which is sent from Java
--------------------------------------------------------------------------------
PROCEDURE SET_ITEM_PRICING_INFO_WRP(I_item_pricing_table IN WRP_ITEM_PRICING_TBL);
--------------------------------------------------------------------------------
FUNCTION AREA_DIFF_RECALC
(O_error_message          OUT  VARCHAR2,
 I_item_pricing_table  IN      ITEM_PRICING_TABLE
)
RETURN BOOLEAN;                        
--------------------------------------------------------------------------------
END PM_RETAIL_API_SQL;
/
