



CREATE OR REPLACE PACKAGE OUTSIDE_LOCATION_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_DELETE_OUTLOC
-- Purpose      : Verify that outside location is not referenced in other modules.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_OUTLOC (O_error_message IN OUT  VARCHAR2,
                              O_exists        IN OUT  BOOLEAN,
                              I_outloc_id     IN   outloc.outloc_id%TYPE,
                              I_outloc_type   IN   outloc.outloc_type%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_DESC
-- Purpose      : Retrieves outside location description based on outloc_id and outloc_type.
---------------------------------------------------------------------------------------------
FUNCTION GET_DESC (O_error_message IN OUT  VARCHAR2,
                   O_outloc_desc   IN OUT  outloc.outloc_desc%TYPE,
                   I_outloc_id     IN      outloc.outloc_id%TYPE,
                   I_outloc_type   IN      outloc.outloc_type%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_CURRENCY
-- Purpose      : Retrieves the outside location's currency code.
---------------------------------------------------------------------------------------------
FUNCTION GET_CURRENCY(O_error_message   IN OUT VARCHAR2,
                      O_outloc_currency IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                      I_outloc_id       IN     OUTLOC.OUTLOC_ID%TYPE,
                      I_outloc_type     IN     OUTLOC.OUTLOC_TYPE%TYPE) 
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CLEAR_ZONE_IMP_COUNTRY_EXISTS
-- Purpose      : Checks if a clearing zone is setup for the input country.
---------------------------------------------------------------------------------------------
FUNCTION CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists             IN OUT   BOOLEAN,
                                       I_outloc_country_id  IN       OUTLOC.OUTLOC_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CLEAR_ZONE_EXISTS
-- Purpose      : Checks if a clearing zone is setup in the system
---------------------------------------------------------------------------------------------
FUNCTION CLEAR_ZONE_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_PRIMARY_IND
-- Purpose      : Sets the current primary clearing zone as non primary.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_outloc_country_id IN       OUTLOC.OUTLOC_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_DELETE_CLEAR_ZONE
-- Purpose      : Checks if the input outloc id (clearing zone) can be deleted.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_CLEAR_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists          IN OUT   BOOLEAN,
                                 I_outloc_id       IN       OUTLOC.OUTLOC_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_PRIM_CLEAR_ZONE_IMP_CTRY
-- Purpose      : Retrieves the primary clearing zone for the input country.
---------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_CLEAR_ZONE_IMP_CTRY(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists              IN OUT   BOOLEAN,
                                      O_outloc_id           IN OUT   OUTLOC.OUTLOC_ID%TYPE,
                                      O_outloc_desc         IN OUT   OUTLOC.OUTLOC_DESC%TYPE,
                                      I_outloc_country_id   IN       OUTLOC.OUTLOC_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_ROW
-- Purpose      : Retrieves the entire ROW for the input outside location.
---------------------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists           IN OUT   BOOLEAN,
                 O_outloc_rec       IN OUT   OUTLOC%ROWTYPE,
                 I_outloc_id        IN       OUTLOC.OUTLOC_ID%TYPE,
                 I_outloc_type      IN       OUTLOC.OUTLOC_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION DEL_OUTLOC_ATTRIB(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_outloc_id        IN       OUTLOC_L10N_EXT.OUTLOC_ID%TYPE,
                           I_outloc_type      IN       OUTLOC_L10N_EXT.OUTLOC_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/


