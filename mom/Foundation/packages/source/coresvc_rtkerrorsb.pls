CREATE OR REPLACE PACKAGE BODY CORESVC_RTK_ERRORS as
----------------------------------------------------------------------------------------------
   cursor C_SVC_RTK_ERRORS(I_process_id   NUMBER,
                           I_chunk_id     NUMBER) is
      select pk_rtk_errors.rowid                 as pk_rtk_errors_rid,
             cd_rety.rowid                       as cd_rid,
             st.rowid                            as st_rid,
             st.rtk_approved,
             UPPER(pk_rtk_errors.rtk_approved)   as pk_rtk_errors_rtk_approved,
             st.rtk_text,
             pk_rtk_errors.rtk_text              as pk_rtk_errors_rtk_text,
             UPPER(st.rtk_key)                   as rtk_key,
             st.rtk_type,
             UPPER(pk_rtk_errors.rtk_type)       as pk_rtk_errors_rtk_type,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)                    as action,
             st.process$status
        from svc_rtk_errors st,
             rtk_errors pk_rtk_errors,
             code_detail cd_rety,
             dual
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and UPPER(st.rtk_key)     = pk_rtk_errors.rtk_key (+)
         and st.rtk_type           = cd_rety.code (+)
         and cd_rety.code_type (+) = 'RETY';

   Type errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   Lp_errors_tab       errors_tab_typ;
   Type s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   Lp_s9t_errors_tab   s9t_errors_tab_typ;

   Type RTKTL_rec_tab IS TABLE OF RTK_ERRORS_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LasT_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LasT_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES (I_file_id   NUMBER) IS
   L_sheets             s9t_pkg.names_map_typ;
   RTK_ERRORS_cols      s9t_pkg.names_map_typ;
   RTK_ERRORS_TL_cols   s9t_pkg.names_map_typ;

BEGIN
   L_sheets                :=s9t_pkg.get_sheet_names(I_file_id);
   RTK_ERRORS_cols         :=s9t_pkg.get_coL_names(I_file_id,
                                                   RTK_ERRORS_sheet);
   RTK_ERRORS$Action       := RTK_ERRORS_cols('ACTION');
   RTK_ERRORS$RTK_APPROVED := RTK_ERRORS_cols('RTK_APPROVED');
   RTK_ERRORS$RTK_TEXT     := RTK_ERRORS_cols('RTK_TEXT');
   RTK_ERRORS$RTK_KEY      := RTK_ERRORS_cols('RTK_KEY');
   RTK_ERRORS$RTK_TYPE     := RTK_ERRORS_cols('RTK_TYPE');

   RTK_ERRORS_TL_cols         :=s9t_pkg.get_coL_names(I_file_id,
                                                      RTK_ERRORS_TL_sheet);
   RTK_ERRORS_TL$Action       := RTK_ERRORS_TL_cols('ACTION');
   RTK_ERRORS_TL$RTK_TEXT     := RTK_ERRORS_TL_cols('RTK_TEXT');
   RTK_ERRORS_TL$RTK_KEY      := RTK_ERRORS_TL_cols('RTK_KEY');
   RTK_ERRORS_TL$LANG         := RTK_ERRORS_TL_cols('LANG');
END POPULATE_NAMES;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_RTK_ERRORS(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = RTK_ERRORS_sheet)
               select s9t_row(s9t_cells(CORESVC_RTK_ERRORS.action_mod,
                                        RTK_TYPE,
                                        RTK_KEY,
                                        RTK_TEXT,
                                        RTK_APPROVED))
                 from RTK_ERRORS ;
END POPULATE_RTK_ERRORS;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_RTK_ERRORS_TL(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = RTK_ERRORS_TL_sheet)
               select s9t_row(s9t_cells(CORESVC_RTK_ERRORS.action_mod,
                                        LANG,
                                        RTK_KEY,
                                        RTK_TEXT))
                 from RTK_ERRORS_TL;
END POPULATE_RTK_ERRORS_TL;
----------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(RTK_ERRORS_sheet);
   L_file.sheets(L_file.get_sheet_index(RTK_ERRORS_sheet)).column_headers := s9t_cells('ACTION',
                                                                                       'RTK_TYPE',
                                                                                       'RTK_KEY',
                                                                                       'RTK_TEXT',
                                                                                       'RTK_APPROVED');

   L_file.add_sheet(RTK_ERRORS_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(RTK_ERRORS_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                          'LANG',
                                                                                          'RTK_KEY',
                                                                                          'RTK_TEXT');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_RTK_ERRORS.CREATE_S9T';
   L_file      s9t_file;

BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_RTK_ERRORS(O_file_id);
      POPULATE_RTK_ERRORS_TL(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_RTK_ERRORS(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_RTK_ERRORS.process_id%TYPE)IS

   Type svc_RTK_ERRORS_coL_typ IS TABLE OF SVC_RTK_ERRORS%ROWTYPE;
   L_temp_rec           SVC_RTK_ERRORS%ROWTYPE;
   svc_RTK_ERRORS_col   svc_RTK_ERRORS_coL_typ := NEW svc_RTK_ERRORS_coL_typ();
   L_process_id         SVC_RTK_ERRORS.process_id%TYPE;
   L_error              BOOLEAN := FALSE;
   L_default_rec        SVC_RTK_ERRORS%ROWTYPE;

   cursor c_mandatory_ind is
      select RTK_APPROVED_mi,
             RTK_TEXT_mi,
             RTK_KEY_mi,
             RTK_TYPE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_RTK_ERRORS.template_key
                 and wksht_key = 'RTK_ERRORS')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('RTK_APPROVED' as RTK_APPROVED,
                                       'RTK_TEXT' as RTK_TEXT,
                                       'RTK_KEY' as RTK_KEY,
                                       'RTK_TYPE' as RTK_TYPE,
                                       NULL as dummy));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_RTK_ERRORS';
   L_pk_columns    VARCHAR2(255)  := 'Key,Language';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN(select RTK_APPROVED_dv,
                     RTK_TEXT_dv,
                     RTK_KEY_dv,
                     RTK_TYPE_dv,
                     NULL as dummy
                from (select column_key,
                             default_value
                        from s9t_tmpL_cols_def
                       where template_key  = CORESVC_RTK_ERRORS.template_key
                         and wksht_key = 'RTK_ERRORS')
                       PIVOT (MAX(default_value) as dv
                         FOR (column_key) IN ('RTK_APPROVED' as RTK_APPROVED,
                                              'RTK_TEXT'     as RTK_TEXT,
                                              'RTK_KEY'      as RTK_KEY,
                                              'RTK_TYPE'     as RTK_TYPE,
                                              NULL           as dummy)))

   LOOP
      BEGIN
         L_default_rec.RTK_APPROVED := rec.RTK_APPROVED_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'RTK_ERRORS',
                            NULL,
                            'RTK_APPROVED',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.RTK_TEXT := rec.RTK_TEXT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'RTK_ERRORS',
                            NULL,
                            'RTK_TEXT',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.RTK_KEY := rec.RTK_KEY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'RTK_ERRORS',
                            NULL,
                            'RTK_KEY',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.RTK_TYPE := rec.RTK_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'RTK_ERRORS',
                            NULL,
                            'RTK_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(RTK_ERRORS$Action)       as Action,
                     r.get_cell(RTK_ERRORS$RTK_APPROVED) as RTK_APPROVED,
                     r.get_cell(RTK_ERRORS$RTK_TEXT)     as RTK_TEXT,
                     UPPER(r.get_cell(RTK_ERRORS$RTK_KEY))      as RTK_KEY,
                     r.get_cell(RTK_ERRORS$RTK_TYPE)     as RTK_TYPE,
                     r.get_row_seq()                     as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(RTK_ERRORS_sheet))

   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RTK_APPROVED := rec.RTK_APPROVED;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_sheet,
                            rec.row_seq,
                            'RTK_APPROVED',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RTK_TEXT := rec.RTK_TEXT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_sheet,
                            rec.row_seq,
                            'RTK_TEXT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RTK_KEY := rec.RTK_KEY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_sheet,
                            rec.row_seq,
                            'RTK_KEY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RTK_TYPE := rec.RTK_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_sheet,
                            rec.row_seq,
                            'RTK_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_RTK_ERRORS.action_new then
         L_temp_rec.RTK_APPROVED := NVL( L_temp_rec.RTK_APPROVED,L_default_rec.RTK_APPROVED);
         L_temp_rec.RTK_TEXT := NVL( L_temp_rec.RTK_TEXT,L_default_rec.RTK_TEXT);
         L_temp_rec.RTK_KEY := NVL( L_temp_rec.RTK_KEY,L_default_rec.RTK_KEY);
         L_temp_rec.RTK_TYPE := NVL( L_temp_rec.RTK_TYPE,L_default_rec.RTK_TYPE);
      end if;

      if NOT L_error then
         svc_RTK_ERRORS_col.extend();
         svc_RTK_ERRORS_col(svc_RTK_ERRORS_col.count()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_RTK_ERRORS_col.count SAVE EXCEPTIONS
      merge into SVC_RTK_ERRORS st
      using (select (case
                     when L_mi_rec.RTK_APPROVED_mi = 'N'
                      and svc_RTK_ERRORS_col(i).action = coresvc_rtk_errors.action_mod
                      and s1.RTK_APPROVED is NULL then
                          mt.RTK_APPROVED
                     else s1.RTK_APPROVED
                     end) as RTK_APPROVED,
                    (case
                     when L_mi_rec.RTK_TEXT_mi = 'N'
                      and svc_RTK_ERRORS_col(i).action = coresvc_rtk_errors.action_mod
                      and s1.RTK_TEXT is NULL then
                          mt.RTK_TEXT
                     else s1.RTK_TEXT
                     end) as RTK_TEXT,
                    (case
                     when L_mi_rec.RTK_KEY_mi = 'N'
                      and svc_RTK_ERRORS_col(i).action = coresvc_rtk_errors.action_mod
                      and s1.RTK_KEY is NULL then
                          mt.RTK_KEY
                     else s1.RTK_KEY
                     end) as RTK_KEY,
                    (case
                     when L_mi_rec.RTK_TYPE_mi = 'N'
                      and svc_RTK_ERRORS_col(i).action = coresvc_rtk_errors.action_mod
                      and s1.RTK_TYPE is NULL then
                          mt.RTK_TYPE
                     else s1.RTK_TYPE
                     end) as RTK_TYPE,
                     NULL as dummy
               from (select svc_RTK_ERRORS_col(i).RTK_APPROVED as RTK_APPROVED,
                            svc_RTK_ERRORS_col(i).RTK_TEXT     as RTK_TEXT,
                            svc_RTK_ERRORS_col(i).RTK_KEY      as RTK_KEY,
                            svc_RTK_ERRORS_col(i).RTK_TYPE     as RTK_TYPE,
                            NULL as dummy
                       from dual) s1,
                            RTK_ERRORS mt
                      where mt.RTK_KEY (+)  = s1.RTK_KEY) sq
                        ON (st.RTK_KEY  = sq.RTK_KEY and
                            svc_RTK_ERRORS_col(i).action in (CORESVC_RTK_ERRORS.action_mod,CORESVC_RTK_ERRORS.action_del))
      when matched then
      update
         set PROCESS_ID      = svc_RTK_ERRORS_col(i).PROCESS_ID ,
             CHUNK_ID          = svc_RTK_ERRORS_col(i).CHUNK_ID ,
             ROW_SEQ           = svc_RTK_ERRORS_col(i).ROW_SEQ ,
             action            = svc_RTK_ERRORS_col(i).ACTION,
             PROCESS$STATUS    = svc_RTK_ERRORS_col(i).PROCESS$STATUS ,
             RTK_TEXT          = sq.RTK_TEXT ,
             RTK_TYPE          = sq.RTK_TYPE ,
             RTK_APPROVED      = sq.RTK_APPROVED ,
             CREATE_ID         = svc_RTK_ERRORS_col(i).CREATE_ID ,
             CREATE_DATETIME   = svc_RTK_ERRORS_col(i).CREATE_DATETIME ,
             LasT_UPD_ID       = svc_RTK_ERRORS_col(i).LasT_UPD_ID ,
             LasT_UPD_DATETIME = svc_RTK_ERRORS_col(i).LasT_UPD_DATETIME
      when NOT matched then
      insert(PROCESS_ID ,
             CHUNK_ID ,
             ROW_SEQ ,
             ACTION ,
             PROCESS$STATUS ,
             RTK_APPROVED ,
             RTK_TEXT ,
             RTK_KEY ,
             RTK_TYPE ,
             CREATE_ID ,
             CREATE_DATETIME ,
             LasT_UPD_ID ,
             LasT_UPD_DATETIME)
      values(svc_RTK_ERRORS_col(i).PROCESS_ID ,
             svc_RTK_ERRORS_col(i).CHUNK_ID ,
             svc_RTK_ERRORS_col(i).ROW_SEQ ,
             svc_RTK_ERRORS_col(i).ACTION ,
             svc_RTK_ERRORS_col(i).PROCESS$STATUS ,
             sq.RTK_APPROVED ,
             sq.RTK_TEXT ,
             sq.RTK_KEY ,
             sq.RTK_TYPE ,
             svc_RTK_ERRORS_col(i).CREATE_ID ,
             svc_RTK_ERRORS_col(i).CREATE_DATETIME ,
             svc_RTK_ERRORS_col(i).LasT_UPD_ID ,
             svc_RTK_ERRORS_col(i).LasT_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
             L_error_code:=sql%bulk_exceptions(i).error_code;
             if L_error_code=1 then
                L_error_code:=NULL;
                L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
             end if;
         WRITE_S9T_ERROR( I_file_id,
                          RTK_ERRORS_sheet,
                          svc_RTK_ERRORS_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
         END LOOP;

   END;
END PROCESS_S9T_RTK_ERRORS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_RTK_ERRORS_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id   IN   SVC_RTK_ERRORS.process_id%TYPE)IS

   Type SVC_RTK_ERRORS_TL_COL_TYP IS TABLE OF SVC_RTK_ERRORS_TL%ROWTYPE;
   L_temp_rec              SVC_RTK_ERRORS_TL%ROWTYPE;
   svc_RTK_ERRORS_TL_col   SVC_RTK_ERRORS_TL_COL_TYP := NEW SVC_RTK_ERRORS_TL_COL_TYP();
   L_process_id            SVC_RTK_ERRORS_TL.PROCESS_ID%TYPE;
   L_error                 BOOLEAN := FALSE;
   L_default_rec           SVC_RTK_ERRORS_TL%ROWTYPE;

   cursor c_mandatory_ind is
      select RTK_TEXT_mi,
             RTK_KEY_mi,
             LANG_mi
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_RTK_ERRORS.template_key
                 and wksht_key = 'RTK_ERRORS_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('RTK_TEXT' as RTK_TEXT,
                                       'RTK_KEY' as RTK_KEY,
                                       'LANG' as LANG));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_RTK_ERRORS_TL';
   L_pk_columns    VARCHAR2(255)  := 'Key, Language';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN(select RTK_TEXT_dv,
                     RTK_KEY_dv,
                     LANG_dv
                from (select column_key,
                             default_value
                        from s9t_tmpL_cols_def
                       where template_key  = CORESVC_RTK_ERRORS.template_key
                         and wksht_key = RTK_ERRORS_TL_sheet)
                       PIVOT (MAX(default_value) as dv
                         FOR (column_key) IN ('RTK_TEXT'     as RTK_TEXT,
                                              'RTK_KEY'      as RTK_KEY,
                                              'LANG'         as LANG)))

   LOOP
      BEGIN
         L_default_rec.RTK_TEXT := rec.RTK_TEXT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_TL_sheet,
                            NULL,
                            'RTK_TEXT',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.RTK_KEY := rec.RTK_KEY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_TL_sheet,
                            NULL,
                            'RTK_KEY',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_TL_sheet,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(RTK_ERRORS_TL$Action)         as Action,
                     r.get_cell(RTK_ERRORS_TL$RTK_TEXT)       as RTK_TEXT,
                     UPPER(r.get_cell(RTK_ERRORS_TL$RTK_KEY)) as RTK_KEY,
                     r.get_cell(RTK_ERRORS_TL$LANG)           as LANG,
                     r.get_row_seq()                          as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(RTK_ERRORS_TL_sheet))

   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RTK_TEXT := rec.RTK_TEXT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_TL_sheet,
                            rec.row_seq,
                            'RTK_TEXT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RTK_KEY := rec.RTK_KEY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_TL_sheet,
                            rec.row_seq,
                            'RTK_KEY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ERRORS_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_RTK_ERRORS.action_new then
         L_temp_rec.RTK_TEXT := NVL( L_temp_rec.RTK_TEXT,L_default_rec.RTK_TEXT);
         L_temp_rec.RTK_KEY := NVL( L_temp_rec.RTK_KEY,L_default_rec.RTK_KEY);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,L_default_rec.LANG);
      end if;

      if NOT L_error then
         svc_RTK_ERRORS_TL_col.extend();
         svc_RTK_ERRORS_TL_col(svc_RTK_ERRORS_TL_col.count()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_RTK_ERRORS_TL_col.count SAVE EXCEPTIONS
      merge into SVC_RTK_ERRORS_TL st
      using (select (case
                     when L_mi_rec.RTK_TEXT_mi = 'N'
                      and svc_RTK_ERRORS_TL_col(i).action = coresvc_rtk_errors.action_mod
                      and s1.RTK_TEXT is NULL then
                          mt.RTK_TEXT
                     else s1.RTK_TEXT
                     end) as RTK_TEXT,
                    (case
                     when L_mi_rec.RTK_KEY_mi = 'N'
                      and svc_RTK_ERRORS_TL_col(i).action = coresvc_rtk_errors.action_mod
                      and s1.RTK_KEY is NULL then
                          mt.RTK_KEY
                     else s1.RTK_KEY
                     end) as RTK_KEY,
                    (case
                     when L_mi_rec.LANG_mi = 'N'
                      and svc_RTK_ERRORS_TL_col(i).action = coresvc_rtk_errors.action_mod
                      and s1.LANG is NULL then
                          mt.LANG
                     else s1.LANG
                     end) as LANG
               from (select svc_RTK_ERRORS_TL_col(i).RTK_TEXT     as RTK_TEXT,
                            svc_RTK_ERRORS_TL_col(i).RTK_KEY      as RTK_KEY,
                            svc_RTK_ERRORS_TL_col(i).LANG         as LANG
                       from dual) s1,
                            RTK_ERRORS_TL mt
                      where mt.RTK_KEY (+)  = s1.RTK_KEY
                        and mt.LANG (+)     = S1.LANG) sq
                        ON (st.RTK_KEY  = sq.RTK_KEY and
                            st.LANG     = sq.LANG and
                            svc_RTK_ERRORS_TL_col(i).action in (CORESVC_RTK_ERRORS.action_mod,CORESVC_RTK_ERRORS.action_del))
      when matched then
      update
         set PROCESS_ID        = svc_RTK_ERRORS_TL_col(i).PROCESS_ID ,
             CHUNK_ID          = svc_RTK_ERRORS_TL_col(i).CHUNK_ID ,
             ROW_SEQ           = svc_RTK_ERRORS_TL_col(i).ROW_SEQ ,
             action            = svc_RTK_ERRORS_TL_col(i).ACTION,
             PROCESS$STATUS    = svc_RTK_ERRORS_TL_col(i).PROCESS$STATUS ,
             RTK_TEXT          = sq.RTK_TEXT
      when NOT matched then
      insert(PROCESS_ID ,
             CHUNK_ID ,
             ROW_SEQ ,
             ACTION ,
             PROCESS$STATUS ,
             RTK_TEXT ,
             RTK_KEY ,
             LANG)
      values(svc_RTK_ERRORS_TL_col(i).PROCESS_ID ,
             svc_RTK_ERRORS_TL_col(i).CHUNK_ID ,
             svc_RTK_ERRORS_TL_col(i).ROW_SEQ ,
             svc_RTK_ERRORS_TL_col(i).ACTION ,
             svc_RTK_ERRORS_TL_col(i).PROCESS$STATUS ,
             sq.RTK_TEXT ,
             sq.RTK_KEY ,
             sq.LANG);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
             L_error_code:=sql%bulk_exceptions(i).error_code;
             if L_error_code=1 then
                L_error_code:=NULL;
                L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
             end if;
         WRITE_S9T_ERROR( I_file_id,
                          RTK_ERRORS_sheet,
                          svc_RTK_ERRORS_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
         END LOOP;

   END;
END PROCESS_S9T_RTK_ERRORS_TL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_RTK_ERRORS.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_RTK_ERRORS(I_file_id,I_process_id);
      PROCESS_S9T_RTK_ERRORS_TL(I_file_id,I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   --Update process$status in svc_process_tracker
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
           values Lp_s9t_errors_tab(i);

      update svc_process_tracker
      set status = 'PE',
          file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ERRORS_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rtk_errors_temp_rec   IN       RTK_ERRORS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_RTK_ERRORS_INS';

BEGIN
   insert into RTK_ERRORS
        values I_rtk_errors_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_RTK_ERRORS_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ERRORS_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rtk_errors_temp_rec   IN       RTK_ERRORS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_RTK_ERRORS_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'RTK_ERRORS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_RTK_ERRORS_UPD is
      select 'x'
        from RTK_ERRORS
       where RTK_KEY = I_rtk_errors_temp_rec.RTK_KEY
         for update nowait;

BEGIN
   open  C_LOCK_RTK_ERRORS_UPD;
   close C_LOCK_RTK_ERRORS_UPD;

   update RTK_ERRORS
      set row = I_rtk_errors_temp_rec
    where RTK_KEY = I_rtk_errors_temp_rec.RTK_KEY;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_rtk_errors_temp_rec.RTK_KEY);
      return FALSE;
   when OTHERS then
      if C_LOCK_RTK_ERRORS_UPD%ISOPEN then
         close C_LOCK_RTK_ERRORS_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_RTK_ERRORS_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ERRORS_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rtk_errors_temp_rec   IN       RTK_ERRORS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_RTK_ERRORS_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'RTK_ERRORS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_RTK_ERRORS_DEL is
      select 'x'
        from rtk_errors
       where rtk_key = I_rtk_errors_temp_rec.rtk_key
         for update nowait;

   cursor C_LOCK_RTK_ERRORS_TL_DEL is
      select 'x'
        from rtk_errors_tl
       where rtk_key = I_rtk_errors_temp_rec.rtk_key
         for update nowait;

BEGIN
   open  C_LOCK_RTK_ERRORS_TL_DEL;
   close C_LOCK_RTK_ERRORS_TL_DEL;

   open  C_LOCK_RTK_ERRORS_DEL;
   close C_LOCK_RTK_ERRORS_DEL;

   delete from rtk_errors_tl
    where rtk_key = I_rtk_errors_temp_rec.rtk_key;

   delete from rtk_errors
    where rtk_key = I_rtk_errors_temp_rec.rtk_key;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_rtk_errors_temp_rec.rtk_key);
      return FALSE;
   when OTHERS then
      if C_LOCK_RTK_ERRORS_TL_DEL%ISOPEN then
         close C_LOCK_RTK_ERRORS_TL_DEL;
      end if;

      if C_LOCK_RTK_ERRORS_DEL%ISOPEN then
         close C_LOCK_RTK_ERRORS_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_RTK_ERRORS_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ERRORS_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rtk_errors_tl_ins_tab    IN       RTKTL_REC_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_RTK_ERRORS_TL_INS';

BEGIN
   if I_rtk_errors_tl_ins_tab is NOT NULL and I_rtk_errors_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_rtk_errors_tl_ins_tab.COUNT()
         insert into rtk_errors_tl
              values I_rtk_errors_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_RTK_ERRORS_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ERRORS_TL_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rtk_errors_tl_upd_tab   IN       RTKTL_REC_TAB,
                                I_errors_tl_upd_rst       IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_RTK_ERRORS_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_RTK_ERRORS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_RTK_ERRORS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'RTK_ERRORS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1        VARCHAR2(30) := NULL;
   L_key_val2        VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_RTK_ERRORS_TL_UPD(I_rtk_key  RTK_ERRORS_TL.RTK_KEY%TYPE,
                                   I_lang     RTK_ERRORS_TL.LANG%TYPE) is
      select 'x'
        from rtk_errors_tl
       where rtk_key = I_rtk_key
         and lang = I_lang
         for update nowait;

BEGIN
   if I_rtk_errors_tl_upd_tab is NOT NULL and I_rtk_errors_tl_upd_tab.count > 0 then
      for i in I_rtk_errors_tl_upd_tab.FIRST..I_rtk_errors_tl_upd_tab.LAST loop
         BEGIN
         L_key_val1 := 'Lang: '||to_char(I_rtk_errors_tl_upd_tab(i).lang);
         L_key_val2 := 'RTK Key: '||to_char(I_rtk_errors_tl_upd_tab(i).rtk_key);
         open C_LOCK_RTK_ERRORS_TL_UPD(I_rtk_errors_tl_upd_tab(i).rtk_key,
                                       I_rtk_errors_tl_upd_tab(i).lang);
         close C_LOCK_RTK_ERRORS_TL_UPD;
         
         update rtk_errors_tl
            set rtk_text = I_rtk_errors_tl_upd_tab(i).rtk_text,
                last_update_id = I_rtk_errors_tl_upd_tab(i).last_update_id,
                last_update_datetime = I_rtk_errors_tl_upd_tab(i).last_update_datetime
          where lang = I_rtk_errors_tl_upd_tab(i).lang
            and rtk_key = I_rtk_errors_tl_upd_tab(i).rtk_key;
         
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_RTK_ERRORS_TL',
                           I_errors_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_RTK_ERRORS_TL_UPD%ISOPEN then
         close C_LOCK_RTK_ERRORS_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_RTK_ERRORS_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ERRORS_TL_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rtk_errors_tl_del_tab   IN       RTKTL_REC_TAB,
                                I_errors_tl_del_rst       IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_RTK_ERRORS_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_RTK_ERRORS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_RTK_ERRORS_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'RTK_ERRORS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1        VARCHAR2(30) := NULL;
   L_key_val2        VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_RTK_ERRORS_TL_DEL(I_rtk_key  RTK_ERRORS.RTK_KEY%TYPE,
                                   I_lang     RTK_ERRORS_TL.LANG%TYPE) is
      select 'x'
        from rtk_errors_tl
       where rtk_key = I_rtk_key
         and lang = I_lang
         for update nowait;

BEGIN
   if I_rtk_errors_tl_del_tab is NOT NULL and I_rtk_errors_tl_del_tab.count > 0 then
      for i in I_rtk_errors_tl_del_tab.FIRST..I_rtk_errors_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_rtk_errors_tl_del_tab(i).lang);
            L_key_val2 := 'RTK Key: '||to_char(I_rtk_errors_tl_del_tab(i).rtk_key);
            open C_LOCK_RTK_ERRORS_TL_DEL(I_rtk_errors_tl_del_tab(i).rtk_key,
                                          I_rtk_errors_tl_del_tab(i).lang);
            close C_LOCK_RTK_ERRORS_TL_DEL;
            
            delete rtk_errors_tl
             where lang = I_rtk_errors_tl_del_tab(i).lang
               and rtk_key = I_rtk_errors_tl_del_tab(i).rtk_key;
            
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_RTK_ERRORS_TL',
                           I_errors_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_RTK_ERRORS_TL_DEL%ISOPEN then
         close C_LOCK_RTK_ERRORS_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_RTK_ERRORS_TL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_ACCESS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_edit_access	       IN OUT   BOOLEAN,
                      O_edit_priv_access   IN OUT   BOOLEAN,
                      O_view_access	       IN OUT   BOOLEAN,
                      O_view_priv_access   IN OUT   BOOLEAN,
                      O_new_access	       IN OUT   BOOLEAN,
                      O_new_priv_access    IN OUT   BOOLEAN)
RETURN BOOLEAN IS
   L_program    VARCHAR2(64) := 'CORESVC_RTK_ERRORS.CHECK_ACCESS';
   L_username   VARCHAR2(30);
   L_role       VARCHAR2(30);

   cursor C_GET_USERNAME is
      select username
        from user_users;

   cursor C_GET_ROLES is
      select granted_role
        from user_role_privs
       where username = L_username;

BEGIN
   O_edit_access      := FALSE;
   O_edit_priv_access := FALSE;
   O_view_access      := FALSE;
   O_view_priv_access := FALSE;
   O_new_access       := FALSE;
   O_new_priv_access  := FALSE;

--- The purpose of this PROCEDURE is to establish security FOR this FORm. The valid values to pass to
--- this PROCEDURE are 'Y'(the button should be enabled),'N'(the button should be disabled), or 'X'(the
--- button should be left IN whatever state it was IN.The PROCEDURE will then check whether the GET_USER is
--- authorized to perFORm the requested function.Listed below is an example of how this could
--- be done (given 2 roles developer and buyer).This PROCEDURE is called from the P_FORm_startup
--- PROCEDURE as well as from all places where buttons are changed from disabled to enabled.
--- This FORm is passed the buttons that are desired to be enabled, and this PROCEDURE determines,
--- based on the GET_USER's role, whether or NOT to  enable the button(s).  IN the following example,
--- buyer only has the ability to view the hierarchy.

   open  C_GET_USERNAME;
   fetch C_GET_USERNAME into L_username;
   close C_GET_USERNAME;

   open C_GET_ROLES;

-- LOOP to find all of the valid roles for the users and determine what functionality
-- they have access to.

   LOOP
      fetch C_GET_ROLES into L_role;
      exit when C_GET_ROLES%NOTFOUND;
--- here is where you would check a table to see if the person's role has
--- the access needed
      if L_role = 'MSG_ADMIN' then
         O_edit_access      := TRUE;
         O_edit_priv_access := TRUE;
         O_view_access      := TRUE;
         O_view_priv_access := TRUE;
         O_new_access       := TRUE;
         O_new_priv_access  := TRUE;
      end if;
   END LOOP;

   close C_GET_ROLES;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_USERNAME%ISOPEN then
         close C_GET_USERNAME;
      end if;
      if C_GET_ROLES%ISOPEN then
         close C_GET_ROLES;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ACCESS;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_RTK_ERRORS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_RTK_ERRORS.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_RTK_ERRORS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program               VARCHAR2(64) := 'CORESVC_RTK_ERRORS.PROCESS_RTK_ERRORS';
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_RTK_ERRORS';
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN := FALSE;
   L_rtk_errors_temp_rec   RTK_ERRORS%ROWTYPE;
   L_edit_access           BOOLEAN := NULL;
   L_edit_priv_access      BOOLEAN := NULL;
   L_view_access           BOOLEAN := NULL;
   L_view_priv_access      BOOLEAN := NULL;
   L_new_access            BOOLEAN := NULL;
   L_new_priv_access       BOOLEAN := NULL;

BEGIN
   FOR rec IN C_SVC_RTK_ERRORS(I_process_id,
                               I_chunk_id)
   LOOP
      L_error := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         or (rec.action = action_new and rec.RTK_APPROVED = 'Y') then
         if CHECK_ACCESS(O_error_message,
                            L_edit_access,
                            L_edit_priv_access,
                            L_view_access,
                            L_view_priv_access,
                            L_new_access,
                            L_new_priv_access) = FALSE then
               WRITE_ERROR(rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           rec.chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_error := TRUE;
            elsif L_edit_access      != TRUE
               or L_edit_priv_access != TRUE
               or L_view_access      != TRUE
               or L_view_priv_access != TRUE
               or L_new_access       != TRUE
               or L_new_priv_access  != TRUE then
               if rec.action IN (action_mod,action_del) then
                  WRITE_ERROR(rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              rec.chunk_id,
                              L_table,
                              rec.row_seq,
                              'ACTION',
                              'USER_PRIVS');
               end if;
               if rec.action = action_new then
                  WRITE_ERROR(rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              rec.chunk_id,
                              L_table,
                              rec.row_seq,
                              'RTK_APPROVED',
                              'USER_PRIVS_APPROVE');
               end if;
               L_error := TRUE;
            end if;
         end if;

      if rec.action IN (action_mod,action_del)
         and L_edit_access      = TRUE
         and L_edit_priv_access = TRUE
         and L_view_access      = TRUE
         and L_view_priv_access = TRUE
         and L_new_access       = TRUE
         and L_new_priv_access  = TRUE then

         if rec.PK_RTK_ERRORS_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'Key,Language',
                        'NO_RECORD');
            L_error :=TRUE;
         end if;

         if rec.action = action_mod
            and (rec.PK_RTK_ERRORS_rtk_text <> rec.rtk_text
            or rec.PK_RTK_ERRORS_rtk_type <> UPPER(rec.rtk_type)
            or rec.PK_RTK_ERRORS_rtk_approved <> UPPER(rec.rtk_approved)) then
            if NOT(UPPER(rec.RTK_APPROVED) IN ('Y','N')) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'RTK_APPROVED',
                           'INV_Y_N_IND');
               L_error :=TRUE;
            end if;
            if rec.RTK_TYPE is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'RTK_TYPE',
                           'ENTER_CLASS_TYPE');
               L_error :=TRUE;
            end if;
            if rec.RTK_TEXT is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'RTK_TEXT',
                           'FIELD_NOT_NULL');
               L_error :=TRUE;
            end if;
            if rec.RTK_APPROVED is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'RTK_APPROVED',
                           'FIELD_NOT_NULL');
               L_error :=TRUE;
            end if;
            if rec.rtk_type is NOT NULL
               and rec.cd_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'RTK_TYPE',
                           'ENTER_TYPE');
               L_error :=TRUE;
            end if;
         end if; --if rec.action = action_mod
      end if; --if rec.action IN (action_mod,action_del)

      if rec.action = action_new then
         if rec.PK_RTK_ERRORS_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'Key,Language',
                        'ERROR_CODE_EXISTS');
            L_error :=TRUE;
         end if;
         if rec.RTK_TYPE is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'RTK_TYPE',
                        'ENTER_CLASS_TYPE');
            L_error :=TRUE;
         end if;
         if rec.rtk_type is NOT NULL
            and rec.cd_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'RTK_TYPE',
                        'ENTER_TYPE');
            L_error :=TRUE;
         end if;
         if rec.RTK_TEXT is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'RTK_TEXT',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         if NOT(UPPER(rec.RTK_APPROVED) IN ('Y','N'))
            or rec.RTK_APPROVED IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'RTK_APPROVED',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;
      end if;    -- if rec.action = action_new

      if NOT L_error then
         L_rtk_errors_temp_rec.RTK_TYPE := UPPER(rec.RTK_TYPE);
         L_rtk_errors_temp_rec.RTK_KEY  := UPPER(rec.RTK_KEY);
         L_rtk_errors_temp_rec.RTK_TEXT := rec.RTK_TEXT;
         L_rtk_errors_temp_rec.RTK_USER := GET_USER;
         if rec.action IN (action_new, action_mod)
            and L_new_priv_access = TRUE then
            L_rtk_errors_temp_rec.RTK_APPROVED := UPPER(rec.RTK_APPROVED);
         elsif rec.action = action_new then
            L_rtk_errors_temp_rec.RTK_APPROVED := 'N';
         elsif rec.action = action_mod then
            L_rtk_errors_temp_rec.RTK_APPROVED := rec.pk_rtk_errors_rtk_approved;
         end if;

         if rec.action = action_new then
            if EXEC_RTK_ERRORS_INS(O_error_message,
                                   L_rtk_errors_temp_rec) = FALSE then
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.nextval,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                          NULL,
                          O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod
            and(rec.PK_RTK_ERRORS_rtk_text <> rec.rtk_text
            or rec.PK_RTK_ERRORS_rtk_type <> UPPER(rec.rtk_type)
            or rec.PK_RTK_ERRORS_rtk_approved <> UPPER(L_rtk_errors_temp_rec.RTK_APPROVED)) then
            if EXEC_RTK_ERRORS_UPD(O_error_message,
                                   L_rtk_errors_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_RTK_ERRORS_DEL(O_error_message,
                                   L_rtk_errors_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_RTK_ERRORS;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_RTK_ERRORS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_RTK_ERRORS_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_RTK_ERRORS_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                       BOOLEAN;
   L_program                     VARCHAR2(64) := 'CORESVC_RTK_ERRORS.PROCESS_RTK_ERRORS_TL';
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'RTK_ERRORS_TL';
   L_base_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'RTK_ERRORS';
   L_table                       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_RTK_ERRORS_TL';
   L_process_error               BOOLEAN                           := FALSE;
   L_rtk_errors_tl_temp_rec      RTK_ERRORS_TL%ROWTYPE;
   L_errors_tl_upd_rst           ROW_SEQ_TAB;
   L_errors_tl_del_rst           ROW_SEQ_TAB;


   cursor C_RTK_ERRORS_TL(I_process_id NUMBER,
                          I_chunk_id NUMBER) is
      select pk_rtk_errors_tl.rowid as pk_rtk_errors_tl_rid,
             fk_rtk_errors.rowid    as fk_rtk_errors_rid,
             fk_lang.rowid          as fk_lang_rid,
             st.lang,
             st.rtk_key,
             st.rtk_text,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_rtk_errors_tl  st,
             rtk_errors         fk_rtk_errors,
             rtk_errors_tl      pk_rtk_errors_tl,
             lang               fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.rtk_key     =  fk_rtk_errors.rtk_key (+)
         and st.lang        =  pk_rtk_errors_tl.lang (+)
         and st.rtk_key     =  pk_rtk_errors_tl.rtk_key (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_RTKTL is TABLE OF C_RTK_ERRORS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_rtk_errors_tl_tab       SVC_RTKTL;

   L_rtktl_ins_tab         RTKTL_rec_tab         := NEW RTKTL_rec_tab();
   L_rtktl_upd_tab         RTKTL_rec_tab         := NEW RTKTL_rec_tab();
   L_rtktl_del_tab         RTKTL_rec_tab         := NEW RTKTL_rec_tab();

BEGIN
   if C_RTK_ERRORS_TL%ISOPEN then
      close C_RTK_ERRORS_TL;
   end if;
   ---
   open C_RTK_ERRORS_TL(I_process_id,I_chunk_id);
   LOOP
      fetch C_RTK_ERRORS_TL bulk collect into L_svc_rtk_errors_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_rtk_errors_tl_tab.COUNT > 0 then
         FOR i in L_svc_rtk_errors_tl_tab.FIRST..L_svc_rtk_errors_tl_tab.LAST
         LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_rtk_errors_tl_tab(i).action is NULL
               or L_svc_rtk_errors_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_rtk_errors_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_rtk_errors_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_rtk_errors_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

   	        -- check if primary key exists
            if L_svc_rtk_errors_tl_tab(i).action = action_new
               and L_svc_rtk_errors_tl_tab(i).pk_rtk_errors_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_rtk_errors_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_rtk_errors_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_rtk_errors_tl_tab(i).lang is NOT NULL
               and L_svc_rtk_errors_tl_tab(i).rtk_key is NOT NULL
               and L_svc_rtk_errors_tl_tab(i).pk_rtk_errors_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_rtk_errors_tl_tab(i).row_seq,
                            NULL,
                            'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_rtk_errors_tl_tab(i).action = action_new
               and L_svc_rtk_errors_tl_tab(i).rtk_key is NOT NULL
               and L_svc_rtk_errors_tl_tab(i).fk_rtk_errors_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_rtk_errors_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_rtk_errors_tl_tab(i).action = action_new
               and L_svc_rtk_errors_tl_tab(i).lang is NOT NULL
               and L_svc_rtk_errors_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_rtk_errors_tl_tab(i).row_seq,
                            NULL,
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_rtk_errors_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_rtk_errors_tl_tab(i).rtk_text is NULL then
                  WRITE_ERROR( I_process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               I_chunk_id,
                               L_table,
                               L_svc_rtk_errors_tl_tab(i).row_seq,
                              'RTK_TEXT',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_rtk_errors_tl_tab(i).rtk_key is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_rtk_errors_tl_tab(i).row_seq,
                           'RTK_KEY',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_rtk_errors_tl_tab(i).lang is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_rtk_errors_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_rtk_errors_tl_temp_rec.lang := L_svc_rtk_errors_tl_tab(i).lang;
               L_rtk_errors_tl_temp_rec.rtk_key := L_svc_rtk_errors_tl_tab(i).rtk_key;
               L_rtk_errors_tl_temp_rec.rtk_text := L_svc_rtk_errors_tl_tab(i).rtk_text;
               L_rtk_errors_tl_temp_rec.create_datetime := SYSDATE;
               L_rtk_errors_tl_temp_rec.create_id := GET_USER;
               L_rtk_errors_tl_temp_rec.last_update_datetime := SYSDATE;
               L_rtk_errors_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_rtk_errors_tl_tab(i).action = action_new then
                  L_rtktl_ins_tab.extend;
                  L_rtktl_ins_tab(L_rtktl_ins_tab.count()) := L_rtk_errors_tl_temp_rec;
               end if;

               if L_svc_rtk_errors_tl_tab(i).action = action_mod then
                  L_rtktl_upd_tab.extend;
                  L_rtktl_upd_tab(L_rtktl_upd_tab.count()) := L_rtk_errors_tl_temp_rec;
                  L_errors_tl_upd_rst(L_rtktl_upd_tab.count()) := L_svc_rtk_errors_tl_tab(i).row_seq;
               end if;

               if L_svc_rtk_errors_tl_tab(i).action = action_del then
                  L_rtktl_del_tab.extend;
                  L_rtktl_del_tab(L_rtktl_del_tab.count()) := L_rtk_errors_tl_temp_rec;
                  L_errors_tl_del_rst(L_rtktl_del_tab.count()) := L_svc_rtk_errors_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
    EXIT WHEN C_RTK_ERRORS_TL%NOTFOUND;
   END LOOP;
   close C_RTK_ERRORS_TL;

   if EXEC_RTK_ERRORS_TL_INS(O_error_message,
                             L_rtktl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_RTK_ERRORS_TL_UPD(O_error_message,
                             L_rtktl_upd_tab,
                             L_errors_tl_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_RTK_ERRORS_TL_DEL(O_error_message,
                             L_rtktl_del_tab,
                             L_errors_tl_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      if C_RTK_ERRORS_TL%ISOPEN then
         close C_RTK_ERRORS_TL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_RTK_ERRORS_TL;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_rtk_errors_tl
      where process_id = I_process_id;

   delete from svc_rtk_errors
      where process_id = I_process_id;
END;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count   IN OUT   NUMBER,
                 I_process_id    IN       NUMBER,
                 I_chunk_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_RTK_ERRORS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';


BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   Lp_errors_tab := NEW errors_tab_typ();
   if PROCESS_RTK_ERRORS(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_RTK_ERRORS_TL(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
          set status = (CASE
                          when status = 'PE'
                          then 'PE'
                    else L_process_status
                    END),
              action_date = SYSDATE
        where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------
END CORESVC_RTK_ERRORS;
/