
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRGRP_SQL AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_GROUP
   -- Purpose      :  Inserts a record on the group table
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_GROUP
   -- Purpose      :  Updates a record on the group table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_GROUP
   -- Purpose      :  Deletes a record on the groups table
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_group_rec       IN       GROUPS%ROWTYPE,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XMRCHHRGRP.LP_cre_type then
      if CREATE_GROUP(O_error_message,
                      I_group_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRGRP.LP_mod_type then
      if MODIFY_GROUP(O_error_message,
                      I_group_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRGRP.LP_del_type then
      if DELETE_GROUP(O_error_message,
                      I_group_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_SQL.CREATE_GROUP';

BEGIN

   if not MERCH_SQL.INSERT_GROUP(O_error_message,
                                 I_group_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_GROUP;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_SQL.MODIFY_GROUP';

BEGIN

   if not MERCH_SQL.MODIFY_GROUP(O_error_message,
                                 I_group_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_GROUP;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_SQL.DELETE_GROUP';

BEGIN

   if not MERCH_SQL.DELETE_GROUP(O_error_message,
                                 I_group_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_GROUP;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRGRP_SQL;
/
