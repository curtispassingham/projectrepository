CREATE OR REPLACE PACKAGE BODY OUTSIDE_LOCATION_SQL AS
-------------------------------------------------------------------------
FUNCTION CHECK_DELETE_OUTLOC (O_error_message IN OUT  VARCHAR2,
                              O_exists        IN OUT  BOOLEAN,
                              I_outloc_id     IN   outloc.outloc_id%TYPE,
                              I_outloc_type   IN   outloc.outloc_type%TYPE)
   return BOOLEAN is
   
   L_program   VARCHAR2(64)   :='OUTSIDE_LOCATION_SQL.CHECK_DELETE_OUTLOC';
   L_exists    VARCHAR2(1)       := NULL;

   
   cursor C_CHECK_ORDERS is
      select 'Y'
        from ordhead
       where (discharge_port    = I_outloc_id 
              and I_outloc_type = 'DP')
          or (lading_port       = I_outloc_id
              and I_outloc_type = 'LP');

   cursor C_CHECK_SUP is
      select 'Y'
        from sup_import_attr
       where (discharge_port    = I_outloc_id 
              and I_outloc_type = 'DP')
          or (lading_port       = I_outloc_id
              and I_outloc_type = 'LP');

   cursor C_CHECK_ITEM_EXP is
      select 'Y'
        from item_exp_head
       where (discharge_port    = I_outloc_id 
              and I_outloc_type = 'DP')
          or (lading_port       = I_outloc_id
              and I_outloc_type = 'LP');

   cursor C_CHECK_EXP_PROF is
      select 'Y'
        from exp_prof_head
       where (discharge_port    = I_outloc_id 
              and I_outloc_type = 'DP')
          or (lading_port       = I_outloc_id
              and I_outloc_type = 'LP');

   cursor C_CHECK_SUPS_ROUTING_LOC is
      select 'Y'
        from sups_routing_loc
       where (routing_loc_id    = I_outloc_id
              and I_outloc_type = 'RL');
BEGIN
   if I_outloc_type in ('DP','LP', 'BT') then
      --Check for existing orders--
      O_exists := FALSE;
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ORDERS', 'ORDHEAD', 
             'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
      open C_CHECK_ORDERS;
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ORDERS', 'ORDHEAD', 
             'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
      fetch C_CHECK_ORDERS into L_exists;
      if C_CHECK_ORDERS%FOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ORDERS', 'ORDHEAD', 
                'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         close C_CHECK_ORDERS;
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_OUTLOC_ORD', NULL, NULL, NULL);
         O_exists := TRUE;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ORDERS', 'ORDHEAD', 
             'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
      close C_CHECK_ORDERS;
      if I_outloc_type != 'BT' then
         --Check for existing suppliers--
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SUP', 'SUP_IMPORT_ATTR', 
              'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         open C_CHECK_SUP;
         SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SUP', 'SUP_IMPORT_ATTR', 
             'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         fetch C_CHECK_SUP into L_exists;
         if C_CHECK_SUP%FOUND then
            SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SUP', 'SUP_IMPORT_ATTR', 
            'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
            close C_CHECK_SUP;
            O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_OUTLOC_SUP', NULL, NULL, NULL);
            O_exists := TRUE;
            return TRUE;
         end if;
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SUP', 'SUP_IMPORT_ATTR', 
                'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         close C_CHECK_SUP;
   
         --Check for existing item expenses--
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ITEM_EXP', 'ITEM_EXP_HEAD', 
             'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         open C_CHECK_ITEM_EXP;
         SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ITEM_EXP', 'ITEM_EXP_HEAD', 
                'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         fetch C_CHECK_ITEM_EXP into L_exists;
         if C_CHECK_ITEM_EXP%FOUND then
            SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM_EXP', 'ITEM_EXP_HEAD', 
                   'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
            close C_CHECK_ITEM_EXP;
            O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_OUTLOC_ITEM', NULL, NULL, NULL);
            O_exists := TRUE;
            return TRUE;
         end if;
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM_EXP', 'ITEM_EXP_HEAD', 
                'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         close C_CHECK_ITEM_EXP;

         --Check for existing expense profile--
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_EXP_PROF', 'EXP_PROF_HEAD',
                          'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         open C_CHECK_EXP_PROF;
         SQL_LIB.SET_MARK('FETCH', 'C_CHECK_EXP_PROF', 'EXP_PROF_HEAD',
                          'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         fetch C_CHECK_EXP_PROF into L_exists;
         if C_CHECK_EXP_PROF%FOUND then
            SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_EXP_PROF', 'EXP_PROF_HEAD',
                             'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
            close C_CHECK_EXP_PROF;
            O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_OUTLOC_EXP', NULL, NULL, NULL);
            O_exists := TRUE;
            return TRUE;
         end if;
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_EXP_PROF', 'EXP_PROF_HEAD',
                          'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         close C_CHECK_EXP_PROF;
      end if;  -- I_outloc_type != 'BT'
   elsif I_outloc_type = 'RL' then
      --Check for existing suppliers--
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SUPS_ROUTING_LOC', 'SUPS_ROUTING_LOC', 
             'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
      open C_CHECK_SUPS_ROUTING_LOC;
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SUPS_ROUTING_LOC', 'SUPS_ROUTING_LOC', 
          'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
      fetch C_CHECK_SUPS_ROUTING_LOC into L_exists;
      if C_CHECK_SUPS_ROUTING_LOC%FOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SUPS_ROUTING_LOC', 'SUPS_ROUTING_LOC', 
           'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
         close C_CHECK_SUPS_ROUTING_LOC;
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_OUTLOC_SUP', NULL, NULL, NULL);
         O_exists := TRUE;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SUPS_ROUTING_LOC', 'SUPS_ROUTING_LOC', 
             'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
      close C_CHECK_SUPS_ROUTING_LOC;
   end if; -- I_outloc_type in ('DP','LP', 'BT')

   return TRUE;
 
EXCEPTION

   when OTHERS then
      O_error_message :=SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE_OUTLOC;
-------------------------------------------------------------------------
FUNCTION GET_DESC (O_error_message IN OUT  VARCHAR2,
                   O_outloc_desc   IN OUT  outloc.outloc_desc%TYPE,
                   I_outloc_id     IN   outloc.outloc_id%TYPE,
                   I_outloc_type   IN      outloc.outloc_type%TYPE)
   return BOOLEAN is

   cursor C_OUTLOC_DESC is
      SELECT outloc_desc
        from v_outloc_tl
       where outloc_id   = I_outloc_id
         and outloc_type = I_outloc_type;

BEGIN

   SQL_LIB.SET_MARK ('OPEN', 'C_OUTLOC_DESC', 'V_OUTLOC_TL',
                     'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
   open C_OUTLOC_DESC;
   SQL_LIB.SET_MARK ('FETCH', 'C_OUTLOC_DESC', 'V_OUTLOC_TL',
                     'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
   fetch C_OUTLOC_DESC into O_outloc_desc;
   ---
   if C_OUTLOC_DESC%NOTFOUND then
      ---
      if I_outloc_type = 'LP' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LADING_PORT',NULL,NULL,NULL);
      elsif I_outloc_type = 'DP' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DISCHARGE_PORT',NULL,NULL,NULL);
      elsif I_outloc_type = 'CZ' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CLEARING_ZONE',NULL,NULL,NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',NULL,NULL,NULL);
      end if;
      ---
      SQL_LIB.SET_MARK ('CLOSE', 'C_OUTLOC_DESC', 'V_OUTLOC_TL',
                        'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
      close C_OUTLOC_DESC;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK ('CLOSE', 'C_OUTLOC_DESC', 'V_OUTLOC_TL',
                     'outloc id'||I_outloc_id||':'||'outloc type'||I_outloc_type);
   close C_OUTLOC_DESC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                            'OUTSIDE_LOCATION_SQL.GET_DESC', 
                                             to_char(SQLCODE));
   return FALSE;
END GET_DESC;
-------------------------------------------------------------------------
FUNCTION GET_CURRENCY(O_error_message   IN OUT VARCHAR2,
                      O_outloc_currency IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                      I_outloc_id       IN     OUTLOC.OUTLOC_ID%TYPE,
                      I_outloc_type     IN     OUTLOC.OUTLOC_TYPE%TYPE) 
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'OUTSIDE_LOCATION_SQL.GET_CURRENCY';
 
   cursor C_OUTLOC_CURRENCY is
      select outloc_currency
        from outloc
       where outloc_id   = I_outloc_id
         and outloc_type = I_outloc_type;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_OUTLOC_CURRENCY','OUTLOC',
                    'Outloc_id: '||I_outloc_id||' Outloc_type: '||I_outloc_type);
   open C_OUTLOC_CURRENCY;

   SQL_LIB.SET_MARK('FETCH','C_OUTLOC_CURRENCY','OUTLOC',
                    'Outloc_id: '||I_outloc_id||' Outloc_type: '||I_outloc_type);
   fetch C_OUTLOC_CURRENCY into O_outloc_currency;

   if C_OUTLOC_CURRENCY%NOTFOUND then 
      O_error_message := SQL_LIB.CREATE_MSG('INV_OUTLOC',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_OUTLOC_CURRENCY','OUTLOC',
                    'Outloc_id: '||I_outloc_id||' Outloc_type: '||I_outloc_type);
      close C_OUTLOC_CURRENCY;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_OUTLOC_CURRENCY','OUTLOC',
                    'Outloc_id: '||I_outloc_id||' Outloc_type: '||I_outloc_type);
   close C_OUTLOC_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CURRENCY;
---------------------------------------------------------------------------------------------
FUNCTION CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists             IN OUT   BOOLEAN,
                                       I_outloc_country_id  IN       OUTLOC.OUTLOC_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   L_program               VARCHAR2(64)  := 'OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS';
   L_clearing_zone_exists  VARCHAR2(1)   := NULL;

   cursor C_GET_CLEARING_ZONE is
      select 'x'
        from outloc
       where outloc_country_id = I_outloc_country_id
         and outloc_type       = 'CZ'
         and rownum            = 1;

BEGIN
   O_exists := TRUE;
   ---
   if I_outloc_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_outloc_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CLEARING_ZONE',
                    'OUTLOC',
                    'outloc_country_id: '||I_outloc_country_id);
   open C_GET_CLEARING_ZONE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CLEARING_ZONE',
                    'OUTLOC',
                    'outloc_country_id: '||I_outloc_country_id);
   fetch C_GET_CLEARING_ZONE into L_clearing_zone_exists;

   if C_GET_CLEARING_ZONE%NOTFOUND then 
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CLEARING_ZONE',
                    'OUTLOC',
                    'outloc_country_id: '||I_outloc_country_id);
   close C_GET_CLEARING_ZONE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLEAR_ZONE_IMP_COUNTRY_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION CLEAR_ZONE_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64)  := 'OUTSIDE_LOCATION_SQL.CLEAR_ZONE_EXISTS';
   L_clearing_zone_exists  VARCHAR2(1)   := NULL;

   cursor C_GET_CLEARING_ZONE is
      select 'x'
        from outloc
       where outloc_type = 'CZ'
         and rownum      = 1;

BEGIN
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CLEARING_ZONE',
                    'OUTLOC',
                    NULL);
   open C_GET_CLEARING_ZONE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CLEARING_ZONE',
                    'OUTLOC',
                    NULL);
   fetch C_GET_CLEARING_ZONE into L_clearing_zone_exists;

   if C_GET_CLEARING_ZONE%NOTFOUND then 
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CLEARING_ZONE',
                    'OUTLOC',
                    NULL);
   close C_GET_CLEARING_ZONE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLEAR_ZONE_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_outloc_country_id IN       OUTLOC.OUTLOC_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64)  := 'OUTSIDE_LOCATION_SQL.UPDATE_PRIMARY_IND';
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_CLEARING_ZONE is
      select 'x'
        from outloc
       where outloc_type       = 'CZ'
         and outloc_country_id = I_outloc_country_id
         and primary_ind       = 'Y'
         for update nowait;

BEGIN
   if I_outloc_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_outloc_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_CLEARING_ZONE',
                    'OUTLOC',
                    'outloc_country_id: '||I_outloc_country_id);
   open C_LOCK_CLEARING_ZONE;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_CLEARING_ZONE',
                    'OUTLOC',
                    NULL);
   close C_LOCK_CLEARING_ZONE;
   ---
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'OUTLOC',
                    'outloc_country_id: '||I_outloc_country_id);

   update outloc
      set primary_ind = 'N'
    where outloc_type       = 'CZ'
      and outloc_country_id = I_outloc_country_id
      and primary_ind       = 'Y';
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                             'OUTLOC',
                                             I_outloc_country_id, 
                                             'Primary_Ind = Y');
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_PRIMARY_IND;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_CLEAR_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists          IN OUT   BOOLEAN,
                                 I_outloc_id       IN       OUTLOC.OUTLOC_ID%TYPE)
   RETURN BOOLEAN IS
   L_program               VARCHAR2(64)  := 'OUTSIDE_LOCATION_SQL.CHECK_DELETE_CLEAR_ZONE';
   L_clearing_zone_exists  VARCHAR2(1)   := NULL;

   cursor C_CHECK_ITEM_HTS is
      select 'x'
        from item_hts
       where clearing_zone_id = I_outloc_id
         and rownum        = 1;

   cursor C_CHECK_ORDHEAD is
      select 'x'
        from ordhead
       where clearing_zone_id = I_outloc_id
         and rownum        = 1;

   cursor C_CHECK_LAST_CZ is
      select 'x'
        from outloc o
       where outloc_id = I_outloc_id
         and outloc_type = 'CZ'
         and primary_ind = 'Y'
         and exists (select 'x'
                       from outloc o2
                      where o2.outloc_id != o.outloc_id
                        and o2.outloc_country_id = o.outloc_country_id
                        and o2.outloc_type = 'CZ');
         

   cursor C_CHECK_RATES_EXIST is
      select 'x'
        from hts_tax_zone
       where clearing_zone_id = I_outloc_id
         and rownum = 1
       union all
      select 'x'
        from hts_fee_zone
       where clearing_zone_id = I_outloc_id
         and rownum = 1
       union all
      select 'x'
        from hts_tariff_treatment_zone
       where clearing_zone_id = I_outloc_id
         and rownum = 1;

BEGIN
   O_exists := FALSE;
   ---
   if I_outloc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_outloc_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_ITEM_HTS;

   fetch C_CHECK_ITEM_HTS into L_clearing_zone_exists;
   
   close C_CHECK_ITEM_HTS;
   --
   if L_clearing_zone_exists is not null then
      O_exists        := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_CLZ_ITEM_HTS', NULL, NULL, NULL);
      return TRUE;
   end if;
   ---
   open C_CHECK_ORDHEAD;

   fetch C_CHECK_ORDHEAD into L_clearing_zone_exists;

   close C_CHECK_ORDHEAD;
   --
   if L_clearing_zone_exists is not null then
      O_exists        := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_CLZ_ORDHEAD', NULL, NULL, NULL);
      return TRUE;
   end if;
   ---
   open C_CHECK_LAST_CZ;

   fetch C_CHECK_LAST_CZ into L_clearing_zone_exists;

   close C_CHECK_LAST_CZ;
   --
   if L_clearing_zone_exists is not null then
      O_exists        := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_PRIM_LOC', NULL, NULL, NULL);
      return TRUE;
   end if;
   ---
   open C_CHECK_RATES_EXIST;

   fetch C_CHECK_RATES_EXIST into L_clearing_zone_exists;

   close C_CHECK_RATES_EXIST;
   --
   if L_clearing_zone_exists is not null then
      O_exists        := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_RATES_EXISTS', NULL, NULL, NULL);
      return TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE_CLEAR_ZONE;
---------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_CLEAR_ZONE_IMP_CTRY(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists              IN OUT   BOOLEAN,
                                      O_outloc_id           IN OUT   OUTLOC.OUTLOC_ID%TYPE,
                                      O_outloc_desc         IN OUT   OUTLOC.OUTLOC_DESC%TYPE,
                                      I_outloc_country_id   IN       OUTLOC.OUTLOC_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   L_program               VARCHAR2(64)  := 'OUTSIDE_LOCATION_SQL.GET_PRIM_CLEAR_ZONE_IMP_CTRY';

   CURSOR C_GET_DESC IS
      SELECT o.outloc_id,
             otl.outloc_desc
        FROM outloc o, 
        v_outloc_tl otl
       WHERE o.outloc_country_id = I_outloc_country_id
         AND o.outloc_type       = 'CZ'
         AND o.primary_ind       = 'Y'
         AND otl.outloc_id       = o.outloc_id
         and otl.outloc_type     = o.outloc_type;

BEGIN
   O_exists := TRUE;
   ---
   if I_outloc_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_outloc_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DESC',
                    'OUTLOC, V_OUTLOC_TL',
                    'outloc_country_id: '||I_outloc_country_id||' Primary Ind = Y');
   open C_GET_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DESC',
                    'OUTLOC , V_OUTLOC_TL',
                    'outloc_country_id: '||I_outloc_country_id||' Primary Ind = Y');
   fetch C_GET_DESC into O_outloc_id, O_outloc_desc;

   if C_GET_DESC%NOTFOUND then 
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DESC',
                    'OUTLOC , V_OUTLOC_TL',
                    'outloc_country_id: '||I_outloc_country_id||' Primary Ind = Y');
   close C_GET_DESC;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIM_CLEAR_ZONE_IMP_CTRY;
---------------------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists           IN OUT   BOOLEAN,
                 O_outloc_rec       IN OUT   OUTLOC%ROWTYPE,
                 I_outloc_id        IN       OUTLOC.OUTLOC_ID%TYPE,
                 I_outloc_type      IN       OUTLOC.OUTLOC_TYPE%TYPE)
   RETURN BOOLEAN IS
   L_program               VARCHAR2(64)  := 'OUTSIDE_LOCATION_SQL.GET_ROW';

   CURSOR C_GET_REC IS
      SELECT o.outloc_type,
             o.outloc_id,
             otl.outloc_desc,
             o.outloc_currency,
             o.outloc_add1,
             o.outloc_add2,
             o.outloc_city,
             o.outloc_state,
             o.outloc_country_id,
             o.outloc_post,
             o.outloc_vat_region,
             o.contact_name,
             o.contact_phone,
             o.contact_fax,
             o.contact_telex,
             o.contact_email,
             o.primary_ind,
             o.outloc_name_secondary,
             o.outloc_jurisdiction_code, 
             o.create_id,
             o.create_datetime
        FROM outloc o,
             v_outloc_tl otl
       WHERE o.outloc_id       = I_outloc_id
         AND o.outloc_type     = I_outloc_type
         AND otl.outloc_id     = o.outloc_id
         and otl.outloc_type   = o.outloc_type;

BEGIN
   O_exists := TRUE;
   ---
   if I_outloc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_outloc_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_outloc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_outloc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_REC',
                    'OUTLOC , V_OUTLOC_TL',
                    'outloc_id: '||I_outloc_id||' outloc_type: '||I_outloc_type);
   open C_GET_REC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_REC',
                    'OUTLOC , V_OUTLOC_TL',
                    'outloc_id: '||I_outloc_id||' outloc_type: '||I_outloc_type);
   fetch C_GET_REC into O_outloc_rec;

   if C_GET_REC%NOTFOUND then 
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_REC',
                    'OUTLOC , V_OUTLOC_TL',
                    'outloc_id: '||I_outloc_id||' outloc_type: '||I_outloc_type);
   close C_GET_REC;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ROW;
---------------------------------------------------------------------------------------------
FUNCTION DEL_OUTLOC_ATTRIB(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_outloc_id        IN       OUTLOC_L10N_EXT.OUTLOC_ID%TYPE,
                           I_outloc_type      IN       OUTLOC_L10N_EXT.OUTLOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'OUTSIDE_LOCATION_SQL.DEL_OUTLOC_ATTRIB';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_OUTLOC_L10N_EXT is
      select 'x'
        from outloc_l10n_ext
       where outloc_id = I_outloc_id
         and outloc_type = I_outloc_type
         for update nowait;

BEGIN

   L_table := 'OUTLOC_L10N_EXT';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_OUTLOC_L10N_EXT',
                    L_table,
                    'outloc_id: '||I_outloc_id||'outloc_type: ' || I_outloc_type);

   open  C_LOCK_OUTLOC_L10N_EXT;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_OUTLOC_L10N_EXT',
                    L_table,
                    'outloc_id: '||I_outloc_id||'outloc_type: ' || I_outloc_type);

   close C_LOCK_OUTLOC_L10N_EXT;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'outloc_id: '||I_outloc_id||'outloc_type: ' || I_outloc_type);

   delete from outloc_l10n_ext
         where outloc_id = I_outloc_id
           and outloc_type = I_outloc_type;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'outloc_id: '||I_outloc_id,
                                             'outloc_type: '|| I_outloc_type);
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END DEL_OUTLOC_ATTRIB;
----------------------------------------------------------------------------------------
END;
/


