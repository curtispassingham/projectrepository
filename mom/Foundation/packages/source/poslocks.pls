
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE POS_CONFIG_LOCK_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------
--- FUNCTION:  LOCK_POS_COUPON_HEAD
--- Purpose:   Locks entries so users can not edit the same id at the same time.
--- Called By: When Tab Page Changed and When Mouse Doubleclick
---------------------------------------------------------------------------
FUNCTION LOCK_POS_COUPON_HEAD (O_error_message IN OUT VARCHAR2,
                               O_lock_ind      IN OUT BOOLEAN,
                               I_config_id     IN     POS_COUPON_HEAD.COUPON_ID%TYPE) return BOOLEAN;

---------------------------------------------------------------------------
--- FUNCTION:  LOCK_POS_PROD_REST_HEAD
--- Purpose:   Locks entries so users can not edit the same id at the same time.
--- Called By: When Tab Page Changed and When Mouse Doubleclick
---------------------------------------------------------------------------
FUNCTION LOCK_POS_PROD_REST_HEAD (O_error_message IN OUT VARCHAR2,
                                  O_lock_ind      IN OUT BOOLEAN,
                                  I_config_id     IN     POS_PROD_REST_HEAD.POS_PROD_REST_ID%TYPE) return BOOLEAN;

---------------------------------------------------------------------------
--- FUNCTION:  LOCK_POS_TENDER_TYPE_HEAD
--- Purpose:   Locks entries so users can not edit the same id at the same time.
--- Called By: When Tab Page Changed and When Mouse Doubleclick
---------------------------------------------------------------------------
FUNCTION LOCK_POS_TENDER_TYPE_HEAD (O_error_message IN OUT VARCHAR2,
                                    O_lock_ind      IN OUT BOOLEAN,
                                    I_config_id     IN     POS_TENDER_TYPE_HEAD.TENDER_TYPE_ID%TYPE) return BOOLEAN;
END POS_CONFIG_LOCK_SQL;
/


