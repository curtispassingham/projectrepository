CREATE OR REPLACE PACKAGE SOURCE_DLVRY_SCHED_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------
-- Function Name: APPLY_SUPLOC_ITEMLIST
-- Purpose      : This function will take the item list and other delivery
--                information and insert a list of item exceptions 
--                into the SOURCE_DLVRY_SCHED_EXC table.
-------------------------------------------------------------------------------------
FUNCTION APPLY_SUPLOC_ITEMLIST(O_error_message IN OUT VARCHAR2,
                               O_not_exists    IN OUT BOOLEAN,
                               I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                               I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                               I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                               I_day           IN     SOURCE_DLVRY_SCHED_EXC.DAY%TYPE,
                               I_item_list     IN     SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: DELETE_SUPLOC_ITEMLIST
-- Purpose      : This function will take the item list and other delivery schedule
--                information and delete a list of item exceptions from the 
--                SOURCE_DLVRY_SCHED_EXC table.
--------------------------------------------------------------------------------------
FUNCTION DELETE_SUPLOC_ITEMLIST(O_error_message IN OUT VARCHAR2,
                                I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                                I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                                I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                                I_day           IN     SOURCE_DLVRY_SCHED_EXC.DAY%TYPE,
                                I_item_list     IN     SOURCE_DLVRY_SCHED_EXC.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: DELETE_SCHEDULE
-- Purpose      : This function will delete the entire schedule form the 
--                SOURCE_DLVRY_SCHED, SOURCE_DLVRY_SCHED_DAYS  and SOURCE_DLVRY_SCHED_EXC tables.
---------------------------------------------------------------------------------------
FUNCTION DELETE_SCHEDULE(O_error_message IN OUT VARCHAR2,
                          I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                         I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                         I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: DELETE_EXC_RECORDS
-- Purpose      : This function will delete all the records from the SOURCE_DLVRY_SCHED_EXC
--                table for the given supplier, location, loc_type and day.
----------------------------------------------------------------------------------------
FUNCTION DELETE_EXC_RECORDS (O_error_message IN OUT VARCHAR2,
                             I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                             I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                             I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                             I_day           IN     SOURCE_DLVRY_SCHED_EXC.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DELETE_EXC_AND_DAYS_RECORDS
-- Purpose      : This function will delete the records from SOURCE_DLVRY_SCHED_DAYS and 
--                SOURCE_DLVRY_SCHED_EXC where the supplier, location and loc_type are passed
--                in.
----------------------------------------------------------------------------------------
FUNCTION DELETE_EXC_AND_DAYS_RECORDS (O_error_message IN OUT VARCHAR2,
                                       I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
						  I_location      IN      SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
	                                I_loc_type      IN      SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: CHECK_DAY_EXIST
-- Purpose      : This function will verify that a duplicate day cannot be entered for
--                a particular supplier and location for a given delivery schedule.
---------------------------------------------------------------------------------------
FUNCTION CHECK_DAY_EXIST (O_error_message IN OUT VARCHAR2,
                          O_exists        IN OUT BOOLEAN,
                          I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                          I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                          I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                          I_day           IN     SOURCE_DLVRY_SCHED_EXC.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CHECK_EXC_DAY_EXIST
-- Purpose      : This function will verify that a duplicate day cannot be added to the 
--                SOURCE_DLVRY_SCHED_EXC table for a specific delivery schedule.
----------------------------------------------------------------------------------------
FUNCTION CHECK_EXC_DAY_EXIST (O_error_message IN OUT VARCHAR2,
                              O_exists        IN OUT BOOLEAN,
                              I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                              I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                              I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                              I_day           IN     SOURCE_DLVRY_SCHED_EXC.DAY%TYPE,
                              I_item          IN     SOURCE_DLVRY_SCHED_EXC.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------                           
FUNCTION INSERT_DAY_SOURCE(O_error_message IN OUT  VARCHAR2,
                           I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
				 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                         I_location      IN      repl_day.location%TYPE,
                         I_loc_type      IN      repl_day.loc_type%TYPE,
				 I_start_date    IN      SOURCE_DLVRY_SCHED.start_date%TYPE,
				 I_start_time    IN      SOURCE_DLVRY_SCHED_days.start_time%TYPE,
				 I_end_time    IN      SOURCE_DLVRY_SCHED_days.end_time%TYPE,
                         I_delivery_cycle IN   SOURCE_DLVRY_SCHED.delivery_cycle%TYPE,
                         I_SUN           IN      VARCHAR2,
                         I_MON           IN      VARCHAR2,
                         I_TUE           IN      VARCHAR2,
                         I_WED           IN      VARCHAR2,
                         I_THU           IN      VARCHAR2,
                         I_FRI           IN      VARCHAR2,
                         I_SAT           IN      VARCHAR2,
                         I_action        IN      VARCHAR2)
RETURN BOOLEAN;
-------------------------------------
FUNCTION GET_LOCATIONS (O_error_message IN OUT  VARCHAR2,
                        I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
			 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
               I_location      IN      repl_day.location%TYPE,
               I_loc_type      IN      repl_day.loc_type%TYPE,
 		   I_start_date    IN      SOURCE_DLVRY_SCHED.start_date%TYPE,
		   I_start_time    IN      SOURCE_DLVRY_SCHED_days.start_time%TYPE,
		   I_end_time    IN      SOURCE_DLVRY_SCHED_days.end_time%TYPE,
               I_delivery_cycle IN   SOURCE_DLVRY_SCHED.delivery_cycle%TYPE,
               I_SUN           IN      VARCHAR2,
               I_MON           IN      VARCHAR2,
               I_TUE           IN      VARCHAR2,
               I_WED           IN      VARCHAR2,
               I_THU           IN      VARCHAR2,
               I_FRI           IN      VARCHAR2,
               I_SAT           IN      VARCHAR2,
               I_action        IN      VARCHAR2)
RETURN BOOLEAN;

---------------------------------------
FUNCTION MANAGE_SOURCE_DLVRY_SCHED(O_error_message IN OUT  VARCHAR2,
                         I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					 I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                         I_location      IN      repl_day.location%TYPE,
                         I_loc_type      IN      repl_day.loc_type%TYPE,
				 I_start_date    IN      SOURCE_DLVRY_SCHED.start_date%TYPE,
                         I_delivery_cycle IN   SOURCE_DLVRY_SCHED.delivery_cycle%TYPE)
RETURN BOOLEAN;
--------------------------------------
FUNCTION GET_DELIVERY_CYCLE(O_error_message IN OUT  VARCHAR2,
                            O_delivery_cycle IN OUT SOURCE_DLVRY_SCHED.delivery_cycle%TYPE,
                            O_start_date     IN OUT SOURCE_DLVRY_SCHED.start_date%TYPE,
                            I_source      IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
				    I_source_type  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                            I_location      IN      repl_day.location%TYPE) 
RETURN BOOLEAN;
---------------------------------------

FUNCTION QUERY_DAY_SOURCE(O_error_message IN OUT  VARCHAR2,
                        I_source_type   IN  source_dlvry_sched.source_type%TYPE,
                        I_source   IN  source_dlvry_sched.source%TYPE,
                        I_loc_type IN  source_dlvry_sched.loc_type%TYPE,
                        I_location   IN  source_dlvry_sched.location%TYPE,
                        O_SUN           IN OUT  VARCHAR2,
                        O_MON           IN OUT  VARCHAR2,
                        O_TUE           IN OUT  VARCHAR2,
                        O_WED           IN OUT  VARCHAR2,
                        O_THU           IN OUT  VARCHAR2,
                        O_FRI           IN OUT  VARCHAR2,
                        O_SAT           IN OUT  VARCHAR2)
 RETURN BOOLEAN;
----------------------------------
--- Purpose: This function is called from the Delivery Schedule Exceptions form
---          to validate that the item or item list entered is setup at the source
---          and the destination location.
---
FUNCTION VALIDATE_SUPLOC_EXC(O_error_message IN OUT VARCHAR2,
                             O_valid         IN OUT BOOLEAN,
                             I_source        IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                             I_source_type   IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                             I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                             I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                             I_item_list     IN     SKULIST_HEAD.SKULIST%TYPE,
                             I_item          IN     SOURCE_DLVRY_SCHED_EXC.ITEM%TYPE)
   return BOOLEAN;
----------------------------------
--- Purpose: This function is called from transfers form to retrive the 
---          delivery slot description for a delivery slot 
FUNCTION GET_DELIVERY_DESC  (O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_delivery_slot_desc IN OUT  DELIVERY_SLOT.DELIVERY_SLOT_DESC%TYPE,
                             I_delivery_slot_id   IN      DELIVERY_SLOT.DELIVERY_SLOT_ID%TYPE)

   RETURN BOOLEAN; 

----------------------------------
END SOURCE_DLVRY_SCHED_SQL;
/


