
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE CLOSE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
-- Function Name: LOC_CLOSING_EXISTS
-- Purpose	: This function will check to see if the given location/close_date 
--                combination already exists.		  
-----------------------------------------------------------------------------------
FUNCTION LOC_CLOSING_EXISTS (O_error_message IN OUT VARCHAR2,
                             O_exists        IN OUT BOOLEAN,
                             I_location      IN     LOCATION_CLOSED.LOCATION%TYPE,
                             I_close_date    IN     LOCATION_CLOSED.CLOSE_DATE%TYPE)
RETURN BOOLEAN;	
-----------------------------------------------------------------------------------
-- Function Name: CO_CLOSING_EXISTS
-- Purpose	: This function will check to see if the given close_date 
--                does already exist.	  
-----------------------------------------------------------------------------------
FUNCTION CO_CLOSING_EXISTS (O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_close_date     IN     COMPANY_CLOSED.CLOSE_DATE%TYPE)
RETURN BOOLEAN;	
-----------------------------------------------------------------------------------
-- Function Name: EXCEPT_EXISTS
-- Purpose	: This function will check to see if the given location/close_date 
--                combination does already exist.		  
-----------------------------------------------------------------------------------
FUNCTION EXCEPT_EXISTS (O_error_message  IN OUT VARCHAR2,
                        O_exists         IN OUT BOOLEAN,
                        I_location       IN     COMPANY_CLOSED_EXCEP.LOCATION%TYPE,
                        I_close_date     IN     COMPANY_CLOSED_EXCEP.CLOSE_DATE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: LOCK_EXCEP
-- Purpose	: This function will lock company exception records for a given
--                close date.		  
-----------------------------------------------------------------------------------
FUNCTION LOCK_EXCEP(O_error_message  IN OUT  VARCHAR2,
                    I_close_date     IN      COMPANY_CLOSED_EXCEP.CLOSE_DATE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: DELETE_EXCEP
-- Purpose	: This function will delete company exception records for a given
--                close date.		  
-----------------------------------------------------------------------------------
FUNCTION DELETE_EXCEP(O_error_message  IN OUT  VARCHAR2,
                      I_close_date     IN      COMPANY_CLOSED_EXCEP.CLOSE_DATE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: APPLY_LOC_EXCEPS
-- Purpose	: This function will insert company exception records for a given
--                close date.
--                Call DELETE_LOC_EXCEPS to delete, then call INSERT_LOC_EXCEPS
--                to insert.		  
-----------------------------------------------------------------------------------
FUNCTION APPLY_LOC_EXCEPS(O_error_message  IN OUT  VARCHAR2,
                          I_close_date     IN      COMPANY_CLOSED_EXCEP.CLOSE_DATE%TYPE,
                          I_group_type     IN      VARCHAR2,
                          I_group_value    IN      VARCHAR2,
                          I_recv_ind       IN      VARCHAR2,
                          I_sales_ind      IN      VARCHAR2,
                          I_ship_ind       IN      VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: DELETE_LOC_EXCEPS
-- Purpose	: This function will delete company exception records for a given
--                close date.		  
-----------------------------------------------------------------------------------
FUNCTION DELETE_LOC_EXCEPS(O_error_message  IN OUT  VARCHAR2,
                           I_close_date     IN      COMPANY_CLOSED_EXCEP.CLOSE_DATE%TYPE,
                           I_group_type     IN      VARCHAR2,
                           I_group_value    IN      VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: INSERT_LOC_EXCEPS
-- Purpose	: This function will insert company exception records for a given
--                close date.		  
-----------------------------------------------------------------------------------
FUNCTION INSERT_LOC_EXCEPS(O_error_message  IN OUT  VARCHAR2,
                           I_close_date     IN      COMPANY_CLOSED_EXCEP.CLOSE_DATE%TYPE,
                           I_group_type     IN      VARCHAR2,
                           I_group_value    IN      VARCHAR2,
                           I_recv_ind       IN      VARCHAR2,
                           I_sales_ind      IN      VARCHAR2,
                           I_ship_ind       IN      VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: APPLY_CLOSED_LOCATIONS
-- Purpose	: This function will insert location closed records for a given
--                close date.
--                Call DELETE_CLOSED_LOCATIONS to delete, then call INSERT_CLOSED_LOCATIONS
--                to insert.		  
-----------------------------------------------------------------------------------
FUNCTION APPLY_CLOSED_LOCATIONS(O_error_message  IN OUT  VARCHAR2,
                                I_close_date     IN      LOCATION_CLOSED.CLOSE_DATE%TYPE,
                                I_group_type     IN      VARCHAR2,
                                I_group_value    IN      VARCHAR2,
                                I_recv_ind       IN      VARCHAR2,
                                I_sales_ind      IN      VARCHAR2,
                                I_ship_ind       IN      VARCHAR2,
                                I_close_reason   IN      LOCATION_CLOSED.REASON%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: DELETE_CLOSED_LOCATIONS
-- Purpose	: This function will delete location closed records for a given
--                close date.		  
-----------------------------------------------------------------------------------
FUNCTION DELETE_CLOSED_LOCATIONS(O_error_message  IN OUT  VARCHAR2,
                                 I_close_date     IN      LOCATION_CLOSED.CLOSE_DATE%TYPE,
                                 I_group_type     IN      VARCHAR2,
                                 I_group_value    IN      VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: INSERT_CLOSED_LOCATIONS
-- Purpose	: This function will insert location closed records for a given
--                close date.		  
-----------------------------------------------------------------------------------
FUNCTION INSERT_CLOSED_LOCATIONS(O_error_message  IN OUT  VARCHAR2,
                                 I_close_date     IN      LOCATION_CLOSED.CLOSE_DATE%TYPE,
                                 I_group_type     IN      VARCHAR2,
                                 I_group_value    IN      VARCHAR2,
                                 I_recv_ind       IN      VARCHAR2,
                                 I_sales_ind      IN      VARCHAR2,
                                 I_ship_ind       IN      VARCHAR2,
                                 I_close_reason   IN      LOCATION_CLOSED.REASON%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END CLOSE_SQL;

/


