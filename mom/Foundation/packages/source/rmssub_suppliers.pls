
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_SUPPLIER AUTHID CURRENT_USER AS
-------------------------------------------------------------------------
TYPE ADDRESS_DATA IS TABLE OF ADDR%ROWTYPE
   INDEX BY BINARY_INTEGER;
TYPE ADDRESS_TYPE_DATA IS TABLE OF ADD_TYPE.ADDRESS_TYPE%TYPE
   INDEX BY BINARY_INTEGER;
-- Used for org unit addition/modification   
TYPE PARTNER_ORG_UNIT_TBL IS TABLE OF PARTNER_ORG_UNIT%ROWTYPE
   INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------
FUNCTION CONSUME(O_status                  OUT  VARCHAR2,
                 O_error_message           OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 I_document             IN      CLOB)
return BOOLEAN;
-------------------------------------------------------------------------
END RMSSUB_SUPPLIER;
/
