CREATE OR REPLACE PACKAGE BODY CORESVC_ITEM_RESERVE_SQL AS
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FIELDS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_itemnumcrivo_rec   IN       "RIB_ItemNumCriVo_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_itemnumcrivo_rec   IN       "RIB_ItemNumCriVo_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_NUMBER_TYPE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_itemnumcrivo_rec   IN       "RIB_ItemNumCriVo_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ITEM_RESERVE(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_itemnumcrivo_rec         IN       "RIB_ItemNumCriVo_REC",
                               O_rib_itemnumcoldesc_rec      OUT   "RIB_ItemNumColDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PURGE_ITEM_RESERVE_TABLE(O_error_message IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'CORESVC_ITEM_RESERVE_SQL.PURGE_ITEM_RESERVE_TABLE';

BEGIN

   delete from svc_item_reservation
    where expiry_date < get_vdate;

 return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_ITEM_RESERVE_TABLE;
-------------------------------------------------------------------------------------------------------
FUNCTION RESERVE_ITEM_NUMBER(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_businessObject            IN OUT   "RIB_ItemNumColDesc_REC",
                             I_serviceOperationContext   IN       "RIB_ServiceOpContext_REC",
                             I_businessObject            IN       "RIB_ItemNumCriVo_REC")
RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'CORESVC_ITEM_RESERVE_SQL.RESERVE_ITEM_NUMBER';
   L_itemnumreqdesc_rec   "RIB_ItemNumCriVo_REC";

BEGIN

   L_itemnumreqdesc_rec := treat(I_businessObject AS "RIB_ItemNumCriVo_REC");

   if L_itemnumreqdesc_rec is null then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', null, null,
      null);
      raise PROGRAM_ERROR;
   end if;

   -- Validate Message Contents
   if VALIDATE_FIELDS(O_error_message,
                      L_itemnumreqdesc_rec) = FALSE then
      return FALSE;
   end if;

   if POPULATE_ITEM_RESERVE(O_error_message,
                            L_itemnumreqdesc_rec,
                            O_businessObject) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return TRUE;
END RESERVE_ITEM_NUMBER;
-----------------------------------------------------------------------------------------
FUNCTION VALIDATE_FIELDS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_itemnumcrivo_rec   IN       "RIB_ItemNumCriVo_REC")
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50)  := 'CORESVC_ITEM_RESERVE_SQL.VALIDATE_FIELDS';
   L_max_quantity      CORESVC_ITEM_CONFIG.MAX_ITEM_RESV_QTY%TYPE;
   L_max_expiry_days   CORESVC_ITEM_CONFIG.MAX_ITEM_EXPIRY_DAYS%TYPE;

BEGIN

   select max_item_resv_qty,
          max_item_expiry_days
     into L_max_quantity,
          L_max_expiry_days
     from coresvc_item_config;
     
   if CHECK_REQUIRED_FIELDS(O_error_message,
                            I_itemnumcrivo_rec) = FALSE then
      return FALSE;
   end if;

   if CHECK_ITEM_NUMBER_TYPE(O_error_message,
                             I_itemnumcrivo_rec) = FALSE then
      return FALSE;
   end if;

   if I_itemnumcrivo_rec.expiry_days <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('DAYS_UNTIL_EXPIRE',
                                            I_itemnumcrivo_rec.expiry_days,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_itemnumcrivo_rec.quantity <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('QTY_GREATER_ZERO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_itemnumcrivo_rec.quantity > L_max_quantity then
      O_error_message := SQL_LIB.CREATE_MSG('EXCEED_MAX_ITEMRESV_QTY',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   if I_itemnumcrivo_rec.expiry_days > L_max_expiry_days then
      O_error_message := SQL_LIB.CREATE_MSG('EXCEED_MAX_EXPIRY_DAYS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_FIELDS;
-----------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_itemnumcrivo_rec   IN       "RIB_ItemNumCriVo_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'CORESVC_ITEM_RESERVE_SQL.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_itemnumcrivo_rec.item_number_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Item Number Type');
      return FALSE;
   end if;

    if I_itemnumcrivo_rec.quantity is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Quantity');
         return FALSE;
   end if;

   if I_itemnumcrivo_rec.expiry_days is NULL then
        O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Expiry Days');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_NUMBER_TYPE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_itemnumcrivo_rec   IN       "RIB_ItemNumCriVo_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'CORESVC_ITEM_RESERVE_SQL.CHECK_ITEM_NUMBER_TYPE';

BEGIN

   if I_itemnumcrivo_rec.item_number_type is NOT NULL then
      if I_itemnumcrivo_rec.item_number_type NOT in ('ITEM','UPC-A','UPC-AS','EAN13') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_RESERVE_ITEM_TYPE',
                                               I_itemnumcrivo_rec.item_number_type,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ITEM_NUMBER_TYPE;
-----------------------------------------------------------------------------------------
FUNCTION POPULATE_ITEM_RESERVE(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_itemnumcrivo_rec         IN       "RIB_ItemNumCriVo_REC",
                               O_rib_itemnumcoldesc_rec      OUT   "RIB_ItemNumColDesc_REC")
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'CORESVC_ITEM_RESERVE_SQL.POPULATE_ITEM_RESERVE';
   L_RIB_ItemNumDesc_REC   "RIB_ItemNumDesc_REC" := NULL;
   L_RIB_ItemNumDesc_TBL   "RIB_ItemNumDesc_TBL" := NULL;
   L_item                  SVC_ITEM_RESERVATION.ITEM%TYPE;
   L_item_number_type      SVC_ITEM_RESERVATION.ITEM_NUMBER_TYPE%TYPE;
   L_expiry_days           CORESVC_ITEM_CONFIG.MAX_ITEM_EXPIRY_DAYS%TYPE;
   L_vdate                 DATE := GET_VDATE();

BEGIN

   if I_itemnumcrivo_rec.item_number_type is NOT NULL then
      if I_itemnumcrivo_rec.item_number_type NOT in ('ITEM','UPC-A','UPC-AS','EAN13') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_RESERVE_ITEM_TYPE',
                                               I_itemnumcrivo_rec.item_number_type,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if I_itemnumcrivo_rec.quantity > 0 then
      L_RIB_ItemNumDesc_TBL := "RIB_ItemNumDesc_TBL"(NULL);
      L_RIB_ItemNumDesc_TBL.DELETE;
      L_RIB_ItemNumDesc_REC   := "RIB_ItemNumDesc_REC" (NULL,NULL,NULL,NULL);
      L_item_number_type  := I_itemnumcrivo_rec.item_number_type;
      L_expiry_days := I_itemnumcrivo_rec.expiry_days;

      FOR i in 1..I_itemnumcrivo_rec.quantity LOOP
         if ITEM_NUMBER_TYPE_SQL.GET_NEXT (O_error_message,
                                           L_item,
                                           L_item_number_type) = FALSE then
            return FALSE;
         end if;

         L_RIB_ItemNumDesc_REC.item := L_item;
         L_RIB_ItemNumDesc_REC.item_number_type := L_item_number_type;
         L_RIB_ItemNumDesc_REC.expiry_date := L_vdate + L_expiry_days;
         L_RIB_ItemNumDesc_TBL.EXTEND;
         L_RIB_ItemNumDesc_TBL(L_RIB_ItemNumDesc_TBL.COUNT) := L_RIB_ItemNumDesc_REC;
      END LOOP;

   end if;

   if L_RIB_ItemNumDesc_TBL.COUNT > 0 then
      O_rib_itemnumcoldesc_rec         := "RIB_ItemNumColDesc_REC"(null,null);
      O_rib_itemnumcoldesc_rec.ItemNumDesc_TBL := L_RIB_ItemNumDesc_TBL;
      O_rib_itemnumcoldesc_rec.collection_size := L_RIB_ItemNumDesc_TBL.COUNT;

      FOR i in L_RIB_ItemNumDesc_TBL.FIRST..L_RIB_ItemNumDesc_TBL.LAST LOOP
         insert into svc_item_reservation(item,
                                      item_number_type,
                                      expiry_date,
                                      create_id,
                                      create_datetime)
                               values(L_RIB_ItemNumDesc_TBL(i).item,
                                      L_RIB_ItemNumDesc_TBL(i).item_number_type,
                                      L_RIB_ItemNumDesc_TBL(i).expiry_date,
                                      USER,
                                      SYSDATE);
      END LOOP;
   else
      O_rib_itemnumcoldesc_rec := "RIB_ItemNumColDesc_REC"(0,      --rib_oid
                                                           0,      --collection_size
                                                           NULL);  --RIB_ItemNumDesc_TBL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_ITEM_RESERVE;
-----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists             OUT   BOOLEAN,
                           I_item            IN       SVC_ITEM_RESERVATION.ITEM%TYPE,
                           I_item_num_type   IN       SVC_ITEM_RESERVATION.ITEM_NUMBER_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'CORESVC_ITEM_RESERVE_SQL.CHECK_ITEM_EXISTS';
   L_count     NUMBER(1);

BEGIN

   select count(0)
     into L_count
     from svc_item_reservation
    where item = I_item;

   if L_count = 0 then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ITEM_EXISTS;
-----------------------------------------------------------------------------------------
END CORESVC_ITEM_RESERVE_SQL;
/
