CREATE OR REPLACE PACKAGE ITEM_SUPP_COUNTRY_LOC_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
--Function Name: LOC_EXISTS
--Purpose      : This function will check if the passed in location already exists
--               for the item/supplier/origin country combination.
-------------------------------------------------------------------------------
FUNCTION LOC_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists            IN OUT BOOLEAN,
                    I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                    I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                    I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                    I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CREATE_LOCATION
-- Purpose      : Insert a new record into the item_supp_country_loc table.  This function
--                is called from new_item_loc function.
----------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_loc               IN     ITEM_LOC.LOC%TYPE,
                         I_like_store        IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type          IN     ITEM_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Function Name: GET_PRIMARY_LOC
--Purpose      : Check if the passed in item/supplier/origin country combination has a primary
--               location specified and will output the primary location if it doesn't.  This
--               function is used in the validation of the primary location indicator to determine
--               if the edited location can be selected as the primary.
----------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_LOC(O_error_message     IN OUT VARCHAR2,
                         O_exists            IN OUT BOOLEAN,
                         O_loc               IN OUT ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: ALL_PACK_COMPONENTS_EXIST
-- Purpose      : This function will check if all pack components exist on the item_supp_country_loc
--                table.  This function is called when validating a newe item/supplier/origin country/location.
--                For pack items, all components of the pack must already exist before the pack item can have the
--                can have the country added.
----------------------------------------------------------------------------------------
FUNCTION ALL_PACK_COMPONENTS_EXIST(O_error_message     IN OUT VARCHAR2,
                                   O_exists            IN OUT BOOLEAN,
                                   I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                                   I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                                   I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                                   I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CREATE_LOCATION
-- Purpose      : Insert a new record into the item_supp_country_loc table.  This function
--                is called from the itemsupcntry form and the order_rcv_sql.on_order function.
----------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message     IN OUT VARCHAR2,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_loc               IN     ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CREATE_LOCATION
-- Purpose      : Insert a new record into the item_supp_country_loc table.  This function
--                is called from the previous create_location function.
----------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message     IN OUT VARCHAR2,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_loc               IN     ITEM_LOC.LOC%TYPE,
                         I_like_store        IN     ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: UPDATE_LOCATION
-- Purpose      : Update location default information on the item_supp_country_loc table
--                after changes are made on item_supp_country.
----------------------------------------------------------------------------------------
FUNCTION UPDATE_LOCATION(O_error_message     IN OUT VARCHAR2,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                         I_edit_cost         IN     VARCHAR2,
                         I_update_all_locs   IN     VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CHECK_LOCATION
-- Purpose      : Check item_supp_country location default information to see whether
--                users are keeping cost and supplier information at the location level
--                or at the item_supp_country level.  This check will dictate (along with
--                a prompt on itemsupcntry.fmb) whether location default information
--                from item_supp_country will be defaulted to all locations or just the
--                primary location (primary_loc_ind = 'Y') for the item/supplier/origin
--                country combination being passed into the function.
----------------------------------------------------------------------------------------
FUNCTION CHECK_LOCATION(O_error_message     IN OUT VARCHAR2,
                        O_exists            IN OUT BOOLEAN,
                        I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                        I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                        I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DEFAULT_LOCATION_TO_CHILDREN
-- Purpose      : Insert locations for child items from item_supp_country when locations
--                exist on item_loc, but the corresponding item_supp_country_loc records
--                do not yet exist.  Furthermore, this function will update all child items
--                on item_supp_country_loc when changes are made to the location default
--                information of the parent item on item_supp_country and the user
--                wishes to default those changes down.
----------------------------------------------------------------------------------------
FUNCTION DEFAULT_LOCATION_TO_CHILDREN(O_error_message       IN OUT VARCHAR2,
                                      I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                      I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                      I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                      I_edit_cost           IN     VARCHAR2,
                                      I_update_all_locs     IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_LOC_TO_CHILDREN_ISCL
-- Purpose      : Insert locations for child items from item_supp_country_loc when locations
--                exist on item_loc, but the corresponding item_supp_country_loc records
--                do not yet exist.  Furthermore, this function will update all child items
--                on item_supp_country_loc when changes are made to the parent item on item_supp_country_loc and the user
--                wishes to default those changes down.
----------------------------------------------------------------------------------------
FUNCTION DEFAULT_LOC_TO_CHILDREN_ISCL(O_error_message       IN OUT  VARCHAR2,
                                     I_item                IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                                     I_supplier            IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                                     I_origin_country_id   IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                                     I_loc                 IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
         RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Function Name: MASS_UPDATE
--Purpose      : This function will dynamically build a where clause then execute it to update
--               ITEM_SUPP_COUNTRY_LOC for numerous locations defined by the where clause.  It
--               also makes a call to UPDATE_BASE_COST.CHANGE_COST and UPDATE_CHILD_LOCATION to
--               handle parent/child costs on ITEM_LOC, ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_LOC
--               and to handle child supplier hierarchy on ITEM_SUPP_COUNTRY_LOC, respectively.
----------------------------------------------------------------------------------------
FUNCTION MASS_UPDATE(O_error_message       IN OUT VARCHAR2,
                     O_virtual_wh_exists   IN OUT BOOLEAN,
                     I_group_type          IN     VARCHAR2,
                     I_group_value         IN     VARCHAR2,
                     I_unit_cost           IN     ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                     I_change_rnd_lvl_ind  IN     VARCHAR2,
                     I_round_lvl           IN     ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
                     I_change_inner_ind    IN     VARCHAR2,
                     I_round_to_inner_pct  IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
                     I_change_case_ind     IN     VARCHAR2,
                     I_round_to_case_pct   IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
                     I_change_layer_ind    IN     VARCHAR2,
                     I_round_to_layer_pct  IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
                     I_change_pallet_ind   IN     VARCHAR2,
                     I_round_to_pallet_pct IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
                     I_change_lvl_1        IN     VARCHAR2,
                     I_supp_hier_lvl_1     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE,
                     I_change_lvl_2        IN     VARCHAR2,
                     I_supp_hier_lvl_2     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE,
                     I_change_lvl_3        IN     VARCHAR2,
                     I_supp_hier_lvl_3     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE,
                     I_change_pickup_ind   IN     VARCHAR2,
                     I_pickup_lead_time    IN     ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                     I_item                IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                     I_supplier            IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                     I_origin_country_id   IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                     I_process_children    IN     VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
--Function Name: UPDATE_CHILD_LOCATION
--Purpose      : Updates ITEM_SUPP_COUNTRY_LOC supplier hierarchy and pickup lead time
--               for all children of the passed in
--               item/supplier/country/location combination.
----------------------------------------------------------------------------------------
FUNCTION UPDATE_CHILD_LOCATION(O_error_message       IN OUT VARCHAR2,
                               I_change_rnd_lvl_ind  IN     VARCHAR2,
                               I_round_lvl           IN     ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
                               I_change_inner_ind    IN     VARCHAR2,
                               I_round_to_inner_pct  IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
                               I_change_case_ind     IN     VARCHAR2,
                               I_round_to_case_pct   IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
                               I_change_layer_ind    IN     VARCHAR2,
                               I_round_to_layer_pct  IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
                               I_change_pallet_ind   IN     VARCHAR2,
                               I_round_to_pallet_pct IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
                               I_change_lvl_1        IN     VARCHAR2,
                               I_supp_hier_type_1    IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_1%TYPE,
                               I_supp_hier_lvl_1     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE,
                               I_change_lvl_2        IN     VARCHAR2,
                               I_supp_hier_type_2    IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_2%TYPE,
                               I_supp_hier_lvl_2     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE,
                               I_change_lvl_3        IN     VARCHAR2,
                               I_supp_hier_type_3    IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_3%TYPE,
                               I_supp_hier_lvl_3     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE,
                               I_change_pickup_ind   IN     VARCHAR2,
                               I_pickup_lead_time    IN     ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                               I_item                IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                               I_supplier            IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                               I_origin_country_id   IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                               I_location            IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Function Name: GET_ALL_COST
--Purpose      : Gets BC,NIC,EBC,IC from item_supp_country_loc
----------------------------------------------------------------------------------------
FUNCTION GET_ALL_COST(O_error_message        IN OUT VARCHAR2,
                      O_base_cost            IN OUT ITEM_SUPP_COUNTRY_LOC.BASE_COST%TYPE,
                      O_negotiated_item_cost IN OUT ITEM_SUPP_COUNTRY_LOC.NEGOTIATED_ITEM_COST%TYPE,
                      O_extended_base_cost   IN OUT ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE,
                      O_inclusive_cost       IN OUT ITEM_SUPP_COUNTRY_LOC.INCLUSIVE_COST%TYPE,
                      I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                      I_supplier             IN     SUPS.SUPPLIER%TYPE,
                      I_origin_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                      I_loc                  IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------
--Function Name: GET_COST
--Purpose      : Gets unit cost from item_supp_country_loc
----------------------------------------------------------------------------------------
FUNCTION GET_COST(O_error_message     IN OUT VARCHAR2,
                      O_unit_cost         IN OUT ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                      I_item              IN     ITEM_MASTER.ITEM%TYPE,
                      I_supplier          IN     SUPS.SUPPLIER%TYPE,
                      I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                      I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CHECK_LOCATION_FOR_CHILDREN
-- Purpose      : Check item_supp_country location default information to see whether
--                users are keeping cost and supplier information at the location level
--                or at the item_supp_country level.  This check will dictate (along with
--                a prompt on itemsuppctry.fmb) whether location default information
--                from item_supp_country will be defaulted to all locations or just the
--                primary location (primary_loc_ind = 'Y') for the item/supplier/origin
--                country combination being passed into the function.  This particular
--                function applies only to child items when defaulting for a parent or
--                grandparent item from itemsuppctry.fmb.
----------------------------------------------------------------------------------------
FUNCTION CHECK_LOCATION_FOR_CHILDREN(O_error_message     IN OUT VARCHAR2,
                                     O_exists            IN OUT BOOLEAN,
                                     I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                                     I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                                     I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: GET_LEAD_TIMES
-- Purpose      : Retrieves the lead time from ITEM_SUPP_COUNTRY and the pickup lead time from
--                ITEM_SUPP_COUNTRY_LOC as well as pass out the sum of the two values.
----------------------------------------------------------------------------------------
FUNCTION GET_LEAD_TIMES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_total_lead_time     IN OUT   NUMBER,
                        O_lead_time           IN OUT   ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                        O_pickup_lead_time    IN OUT   ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                        I_item                IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                        I_supplier            IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                        I_origin_country_id   IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                        I_location            IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION SINGLE_UPDATE(O_error_message       IN OUT  VARCHAR2,
                       I_item                IN      ITEM_MASTER.ITEM%TYPE,
                       I_supplier            IN      SUPS.SUPPLIER%TYPE,
                       I_country             IN      COUNTRY.COUNTRY_ID%TYPE,
                       I_location            IN      ITEM_LOC.LOC%TYPE,
                       I_loc_type            IN      ITEM_LOC.LOC_TYPE%TYPE,
                       I_item_status         IN      ITEM_MASTER.STATUS%TYPE,
                       I_edit_unit_cost      IN      VARCHAR2,
                       I_unit_cost           IN      ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                       I_edit_lvl_1          IN      VARCHAR2,
                       I_supp_hier_lvl_1     IN      ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE,
                       I_edit_lvl_2          IN      VARCHAR2,
                       I_supp_hier_lvl_2     IN      ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE,
                       I_edit_lvl_3          IN      VARCHAR2,
                       I_supp_hier_lvl_3     IN      ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE,
                       I_edit_pickup_ind     IN      VARCHAR2,
                       I_pickup_lead_time    IN      ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                       I_edit_round_lvl      IN      VARCHAR2,
                       I_round_lvl           IN      ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
                       I_edit_inner_pct      IN      VARCHAR2,
                       I_round_to_inner_pct  IN      ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
                       I_edit_case_pct       IN      VARCHAR2,
                       I_round_to_case_pct   IN      ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
                       I_edit_layer_pct      IN      VARCHAR2,
                       I_round_to_layer_pct  IN      ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
                       I_edit_pallet_pct     IN      VARCHAR2,
                       I_round_to_pallet_pct IN      ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
                       I_default_down_ind    IN      VARCHAR2,
                       I_primary_loc_ind     IN      VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: GET_ISCL
-- Purpose      : Retrives values from Item_supp_country_loc table and passes them back.
--
-------------------------------------------------------------------------------------------
FUNCTION GET_ISCL(O_error_message       IN OUT VARCHAR2,
                  O_currency_code       IN OUT STORE.CURRENCY_CODE%TYPE,
                  O_case_cost           IN OUT NUMBER,
                  O_unit_cost           IN OUT ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                  O_round_lvl           IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
                  O_round_to_inner_pct  IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
                  O_round_to_case_pct   IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
                  O_round_to_layer_pct  IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
                  O_round_to_pallet_pct IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
                  O_supp_hier_type_1    IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_1%TYPE,
                  O_supp_hier_type_2    IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_2%TYPE,
                  O_supp_hier_type_3    IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_3%TYPE,
                  O_supp_hier_lvl_1     IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE,
                  O_supp_hier_lvl_2     IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE,
                  O_supp_hier_lvl_3     IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE,
                  O_pickup_lead_time    IN OUT ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                  I_item                IN     ITEM_MASTER.ITEM%TYPE,
                  I_supplier            IN     SUPS.SUPPLIER%TYPE,
                  I_origin_country_id   IN     COUNTRY.COUNTRY_ID%TYPE,
                  I_loc                 IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: BULK_INSERT
-- Purpose      : Inserts rows for all locations/costs in LOC_COST_TBLTYPE using a bulk insert and
--                default values for all other columns for 1 item/supplier/country.
--                Used for XITEM subscription API.
------------------------------------------------------------------------------------
FUNCTION BULK_INSERT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_loc_cost        IN       RMSSUB_XITEM.LOC_COST_TBLTYPE,
                     I_iscl_def        IN       ITEM_SUPP_COUNTRY_LOC%ROWTYPE)

   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: BULK_DELETE
-- Purpose      : Deletes rows for all locations in a LOC_TBL using a bulk insert for
--                1 item/supplier/country.  Also, does a single locations delete.
--                Used for XITEM subscription API.
------------------------------------------------------------------------------------
FUNCTION BULK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_locs            IN       LOC_TBL,
                     I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                     I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                     I_item            IN       ITEM_MASTER.ITEM%TYPE,
                     I_cascade_ind     IN       CORESVC_ITEM_CONFIG.CASCADE_IUD_ITEM_SUPP_COUNTRY%TYPE DEFAULT 'N')
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: UPDATE_YES_PRIM_IND
--  Purpose: Updates primary_loc_ind to Y for 1 row in the item_supp_country_loc table.
--           NOTE:  only used for XITEM subscription API after UPDATE_PRIMARY_INDICATORS is called
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_YES_PRIM_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_iscl_rec      IN ITEM_SUPP_COUNTRY_LOC%ROWTYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: SINGLE_INSERT
-- Purpose      : Inserts rows for a single location with
--                default values for all other columns for 1 item/supplier/country.
--                Used for XITEM subscription API for store and warehouse.
------------------------------------------------------------------------------------
FUNCTION SINGLE_INSERT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_iscl_def        IN       ITEM_SUPP_COUNTRY_LOC%ROWTYPE)

   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_ROW
-- Purpose      : Retrieves row for a specific item, supplier, country, and location combination.
----------------------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists          IN OUT   BOOLEAN,
                 O_iscl_rec        IN OUT   ITEM_SUPP_COUNTRY_LOC%ROWTYPE,
                 I_item            IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                 I_supplier        IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                 I_country         IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                 I_location        IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--Function Name: GET_LAST_RECEIPT_COST
--Purpose      : This function will return the last receipt cost for an item/supplier/location passed in
--------------------------------------------------------------------------------
FUNCTION GET_LAST_RECEIPT_COST(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_last_receipt_cost   IN OUT  SHIPSKU.UNIT_COST%TYPE,
                               I_item                IN      SHIPSKU.ITEM%TYPE,
                               I_supplier            IN      ORDHEAD.SUPPLIER%TYPE,
                               I_location            IN      SHIPMENT.TO_LOC%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name: ITEMLOC_SUPP_FILTER_LIST
--Purpose      : This function will return "Y" if user don't have access to all the
--               records in the item_loc for the input Item, Group Type and Group ID
--               In addition to the checks present in ITEM_VALIDATE_SQL.ITEMLOC_FILTER_LIST
--               this also includes the check on V_SUPS
--------------------------------------------------------------------------------
FUNCTION ITEMLOC_SUPP_FILTER_LIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_diff_ind        IN OUT VARCHAR2,
                                  I_item            IN     ITEM_LOC.ITEM%TYPE,
                                  I_group_type      IN     CODE_DETAIL.CODE%TYPE,
                                  I_group_id        IN     PARTNER.PARTNER_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CREATE_LOCATION
-- Purpose      : Insert a new record into the item_supp_country_loc table.  This function
--                is called from new_item_loc function
----------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_loc               IN     ITEM_LOC.LOC%TYPE,
                         I_like_store        IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type          IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_wh_unit_cost      IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                         I_wf_ind            IN     VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CREATE_LOCATION
-- Purpose      : Insert a new record into the item_supp_country_loc table.  This function
--                is called from new_item_loc function Must call   to write the correct item cost
----------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_comp_item_cost_tbl IN OUT NOCOPY OBJ_COMP_ITEM_COST_TBL,
                         I_item                IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_loc                 IN     ITEM_LOC.LOC%TYPE,
                         I_like_store          IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_wh_unit_cost        IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                         I_wf_ind              IN     VARCHAR2,
                         I_itemloc_ind         IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--Function Name : INSERT_RECORDS_GTT
--Purpose       : This function is called at the form startup of item supplier country location
--                screen, so that the existing values are stored in the global temporary table
--                which can further be used to compare the changed values in the table
--                item_supp_country_loc
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_RECORDS_GTT (O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                IN      ITEM_MASTER.ITEM%TYPE,
                             I_supplier            IN      SUPS.SUPPLIER%TYPE,
                             I_origin_country_id   IN      COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name : FUTURE_COST_UPD
--Purpose       : This function is called when saving the entries in the Item Supplier Country Location
--                screen.It collects the records(having different values as stated in function INSERT_RECORDS_GTT )
--                in a table and passes it to FUTURE_COST_EVENT_SQL.ADD_SUPP_HIER_CHANGE.
--------------------------------------------------------------------------------------------------------
FUNCTION FUTURE_COST_UPD (O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)

   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Function Name: CREATE_LOCATION
-- Purpose      : Insert a new records into the item_supp_country_loc table.
--                Uses a collection IO_comp_item_cost_tbl to insert multiple records.
--                This function is exclusively called from the itemloc.fmb form when ranging
--                multiple locations using groups.
----------------------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_comp_item_cost_tbl IN OUT NOCOPY OBJ_COMP_ITEM_COST_TBL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Function Name : UPDATE_FRNACHISE_COST
-- Purpose       : If updating the unit cost of a virtual  warehouse in itemcost screen called from itemsuppctryloc,
--                 this function is called to update all the wholesale frnachise store cost whose source wh is the above wh 
--                 and all the virual warehouse under the physical warehouse of above wh.
---------------------------------------------------------------------------------------------------
FUNCTION UPDATE_FRANCHISE_COST(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_virtual_wh_exists    IN OUT BOOLEAN,
                               I_item                 IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                               I_supplier             IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                               I_origin_country_id    IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                               I_group_type           IN     VARCHAR2,
                               I_group_value          IN     VARCHAR2,
                               I_process_children     IN     VARCHAR2,
                               I_unit_cost            IN     ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                               I_base_cost            IN     ITEM_SUPP_COUNTRY_LOC.BASE_COST%TYPE,
                               I_negotiated_item_cost IN     ITEM_SUPP_COUNTRY_LOC.NEGOTIATED_ITEM_COST%TYPE,
                               I_extended_base_cost   IN     ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE,
                               I_inclusive_cost       IN     ITEM_SUPP_COUNTRY_LOC.INCLUSIVE_COST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
END ITEM_SUPP_COUNTRY_LOC_SQL;
/
