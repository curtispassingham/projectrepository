CREATE OR REPLACE PACKAGE BODY RETAIL_API_SQL AS

   -- store the system options to use across all functions
   LP_system_options        SYSTEM_OPTIONS%ROWTYPE;

---------------------------------------------------------------------------------------------------------
-- Function: GET_DEPS_MARKUP_INFO
-- Purpose : Fetches markup_calc_type and markup_percent info from deps table.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_DEPS_MARKUP_INFO(O_error_message     IN OUT  VARCHAR2,
                              O_markup_calc_type  IN OUT  DEPS.MARKUP_CALC_TYPE%TYPE,
                              O_markup_percent    IN OUT  NUMBER,
                              I_dept              IN      DEPS.DEPT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(61) := 'RETAIL_API_SQL.GET_DEPS_MARKUP_INFO';

   L_bud_int             DEPS.BUD_INT%TYPE;
   L_bud_mkup            DEPS.BUD_MKUP%TYPE;
   L_markup_calc_type    DEPS.MARKUP_CALC_TYPE%TYPE;

   cursor C_GET_DEPS_INFO is
      select bud_int, bud_mkup, markup_calc_type
        from deps
       where dept = I_dept;

BEGIN

   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if O_markup_calc_type is NOT NULL and O_markup_calc_type NOT IN ('C','R') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MKUP_CALC_TYPE',
                                            O_markup_calc_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

    open C_GET_DEPS_INFO;
   fetch C_GET_DEPS_INFO into L_bud_int, L_bud_mkup, L_markup_calc_type;
   close C_GET_DEPS_INFO;

   if O_markup_calc_type is NULL then
      O_markup_calc_type := L_markup_calc_type;
   end if;

   if O_markup_calc_type = 'C' then
      O_markup_percent := L_bud_mkup;
   else
      O_markup_percent := L_bud_int;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEPS_MARKUP_INFO;
---------------------------------------------------------------------------------------------------------
-- Function: GET_SUPP_VAT_RATE
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_VAT_RATE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_vat_rate       IN OUT  VAT_CODE_RATES.VAT_RATE%TYPE,
                           O_tax_amount     IN OUT  GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE,
                           I_supplier       IN      SUPS.SUPPLIER%TYPE,
                           I_dept           IN      ITEM_MASTER.DEPT%TYPE,
                           I_item           IN      ITEM_MASTER.ITEM%TYPE,
                           I_unit_retail    IN      ITEM_LOC.UNIT_RETAIL%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program             VARCHAR2(61) := 'RETAIL_API_SQL.GET_SUPP_VAT_RATE';

   L_pack_ind            ITEM_MASTER.PACK_IND%TYPE;
   L_obj_tax_calc_rec    OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_obj_tax_calc_tbl    OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_vdate               DATE   := GET_VDATE();
   L_sum_count           NUMBER                              :=0;
   L_vat_rate            VAT_CODE_RATES.VAT_RATE%TYPE        :=0;
   L_tax_amount          GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE :=0;

   cursor C_CHK_PACK_IND is
      select pack_ind
        from item_master
       where  item = I_item;

BEGIN

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('REQUIRED_INPUT_IS_NULL',
                                             'I_supplier',
                                             L_program,
                                             NULL);
      return FALSE;
   end If;

   open C_CHK_PACK_IND;
   fetch C_CHK_PACK_IND into L_pack_ind;
   close C_CHK_PACK_IND;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   if LP_system_options.default_tax_type = 'SVAT' then

      L_obj_tax_calc_rec.I_item := I_item;
      L_obj_tax_calc_rec.I_pack_ind := L_pack_ind;
      L_obj_tax_calc_rec.I_from_entity := I_supplier;
      L_obj_tax_calc_rec.I_from_entity_type := 'SU';
      L_obj_tax_calc_rec.I_amount := I_unit_retail;
      L_obj_tax_calc_rec.I_amount_tax_incl_ind := 'N';
      ---
      L_obj_tax_calc_rec.I_tran_type := 'RPMRETAIL';
      L_obj_tax_calc_rec.I_tran_date := L_vdate;
      L_obj_tax_calc_rec.I_cost_retail_ind := 'R';
      ---
      L_obj_tax_calc_tbl.EXTEND;
      L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT) := L_obj_tax_calc_rec;

      if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                 L_obj_tax_calc_tbl,
                                 TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
         return FALSE;
      end if;

      O_vat_rate   := NVL(L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).o_cum_tax_pct,0);
      O_tax_amount := NVL(L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).o_cum_tax_value, 0);

   elsif LP_system_options.default_tax_type = 'GTAX' then

      L_obj_tax_calc_rec := OBJ_TAX_CALC_REC();
      L_obj_tax_calc_tbl := OBJ_TAX_CALC_TBL();
      L_obj_tax_calc_rec.I_item := I_item;
      L_obj_tax_calc_rec.I_pack_ind := L_pack_ind;
      L_obj_tax_calc_rec.I_from_entity := I_supplier;
      L_obj_tax_calc_rec.I_from_entity_type := 'SU';
      L_obj_tax_calc_rec.I_amount := I_unit_retail;
      L_obj_tax_calc_rec.I_amount_tax_incl_ind := 'Y';
      ---
      L_obj_tax_calc_rec.I_tran_type := 'RPMRETAIL';
      L_obj_tax_calc_rec.I_tran_date := L_vdate;
      L_obj_tax_calc_rec.I_cost_retail_ind := 'R';
      ---
      L_obj_tax_calc_tbl.EXTEND;
      L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT) := L_obj_tax_calc_rec;

      if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                 L_obj_tax_calc_tbl,
                                 TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
         return FALSE;
      end if;

      if L_obj_tax_calc_tbl is NULL or L_obj_tax_calc_tbl.COUNT <= 0 then
         O_error_message := SQL_LIB.CREATE_MSG ('TAX_NOT_FOUND',
                                                I_item,
                                                I_supplier,
                                                NULL);
         return FALSE;
      end if;

      L_sum_count := L_obj_tax_calc_tbl.COUNT;

      if L_sum_count > 0 then
         L_vat_rate := 0;
         L_tax_amount := 0;
         for i in L_obj_tax_calc_tbl.FIRST..L_obj_tax_calc_tbl.LAST LOOP
            L_vat_rate   := L_vat_rate + NVL(L_obj_tax_calc_tbl(i).o_cum_tax_pct,0);
            L_tax_amount := L_tax_amount + L_obj_tax_calc_tbl(i).o_cum_tax_value;
         end LOOP;

         O_vat_rate   := L_vat_rate/ L_sum_count;
         O_tax_amount := L_tax_amount/ L_sum_count;
      else
         O_vat_rate := 0;
         O_tax_amount := 0;
      end if;
   end if;

   if O_vat_rate is NULL then
      O_vat_rate   := 0;
      O_tax_amount := 0;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_SUPP_VAT_RATE;
---------------------------------------------------------------------------------------------------------
-- Function: CALC_RETAIL
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION CALC_RETAIL(O_error_message         OUT  VARCHAR2,
                     O_selling_retail        OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                     O_selling_uom           OUT  ITEM_LOC.SELLING_UOM%TYPE,
                     O_multi_units           OUT  ITEM_LOC.MULTI_UNITS%TYPE,
                     O_multi_unit_retail     OUT  ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                     O_multi_selling_uom     OUT  ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                     O_currency_code         OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                     I_item               IN      ITEM_MASTER.ITEM%TYPE,
                     I_dept               IN      ITEM_MASTER.DEPT%TYPE,
                     I_class              IN      ITEM_MASTER.CLASS%TYPE,
                     I_subclass           IN      ITEM_MASTER.SUBCLASS%TYPE,
                     I_location           IN      ITEM_LOC.LOC%TYPE,
                     I_loc_type           IN      ITEM_LOC.LOC_TYPE%TYPE,
                     I_unit_cost          IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  DEFAULT NULL,
                     I_currency           IN      SUPS.CURRENCY_CODE%TYPE           DEFAULT NULL)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'RETAIL_API_SQL.CALC_RETAIL';

   L_invalid_param           VARCHAR2(30) := NULL;

   L_markup_calc_type        DEPS.MARKUP_CALC_TYPE%TYPE;
   L_budgeted_intake         DEPS.BUD_INT%TYPE;
   L_budgeted_markup         DEPS.BUD_MKUP%TYPE;
   L_markup_percent          NUMBER;

   L_multi_currency_exists   BOOLEAN;
   L_loc_curr_code           CURRENCIES.CURRENCY_CODE%TYPE;

   L_supplier                ITEM_SUPPLIER.SUPPLIER%TYPE;
   L_unit_cost_sup           ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;

   L_sup_curr_code           SUPS.CURRENCY_CODE%TYPE;

BEGIN

   --- Validate parameters
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_dept is NULL then
      L_invalid_param := 'I_dept';
   elsif I_class is NULL then
      L_invalid_param := 'I_class';
   elsif I_location is NULL then
      L_invalid_param := 'I_location';
   elsif I_loc_type is NULL then
      L_invalid_param := 'I_loc_type';
   elsif I_unit_cost is not NULL and I_currency is NULL then
      L_invalid_param := 'I_currency';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if DEPT_ATTRIB_SQL.GET_MARKUP(O_error_message,
                                 L_markup_calc_type,
                                 L_budgeted_intake,
                                 L_budgeted_markup,
                                 I_dept) = FALSE then
      return FALSE;
   end if;

   if L_markup_calc_type = 'C' then
      L_markup_percent := L_budgeted_markup;
   else
      L_markup_percent := L_budgeted_intake;
   end if;

   if LOCATION_ATTRIB_SQL.GET_LOCATION_CURRENCY(O_error_message,
                                                L_multi_currency_exists,
                                                L_loc_curr_code,
                                                I_loc_type,
                                                I_location) = FALSE then
      return FALSE;
   end if;

   if I_unit_cost is NULL then
      if SUPP_ITEM_SQL.GET_PRI_SUP_COST(O_error_message,
                                        L_supplier,
                                        L_unit_cost_sup,
                                        I_item,
                                        I_location) = FALSE then
         RETURN FALSE;
      elsif L_unit_cost_sup is NULL then
         return TRUE;
      end if;

      if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                           L_sup_curr_code,
                                           L_supplier) = FALSE then
         return FALSE;
      end if;
   else
      L_unit_cost_sup   := I_unit_cost;
      L_sup_curr_code   := I_currency;
   end if;

   if CALCULATE_RETAIL_FROM_MKUP(O_error_message,
                                 O_selling_retail,
                                 O_selling_uom,
                                 O_currency_code,
                                 I_item,
                                 I_dept,
                                 I_class,
                                 L_markup_calc_type,
                                 L_markup_percent,
                                 I_location,
                                 I_loc_type,
                                 L_loc_curr_code,
                                 L_unit_cost_sup,
                                 L_sup_curr_code) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_RETAIL;
---------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_RETAIL_FROM_MKUP
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_RETAIL_FROM_MKUP(O_error_message           OUT  VARCHAR2,
                                    O_selling_retail          OUT  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                    O_selling_uom             OUT  ITEM_LOC.SELLING_UOM%TYPE,
                                    O_selling_retail_curr     OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                                    I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                                    I_dept                 IN      ITEM_MASTER.DEPT%TYPE,
                                    I_class                IN      ITEM_MASTER.CLASS%TYPE,
                                    I_markup_calc_type     IN      VARCHAR2,
                                    I_markup_percent       IN      NUMBER,
                                    I_location             IN      ITEM_LOC.LOC%TYPE,
                                    I_loc_type             IN      ITEM_LOC.LOC_TYPE%TYPE,
                                    I_loc_currency         IN      STORE.CURRENCY_CODE%TYPE,
                                    I_unit_cost            IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  DEFAULT NULL,
                                    I_supp_currency        IN      SUPS.CURRENCY_CODE%TYPE           DEFAULT NULL)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'RETAIL_API_SQL.CALCULATE_RETAIL_FROM_MKUP';

   L_item_record             ITEM_MASTER%ROWTYPE        := NULL;
   L_selling_retail          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_conv_selling_retail     ITEM_LOC.UNIT_RETAIL%TYPE;
   L_vdate                   DATE   := get_vdate();
   L_rounded_selling_retail  ITEM_LOC.UNIT_RETAIL%TYPE;

BEGIN

      -- Get Item's Standard UOM
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
      return FALSE;
   end if;

   if CALC_RETAIL_FROM_MARKUP(O_error_message,
                              L_selling_retail,
                              I_markup_percent,
                              I_markup_calc_type,
                              I_unit_cost) = FALSE then
      return FALSE;
   end if;

   if I_loc_currency <> I_supp_currency then

      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_selling_retail,
                              I_supp_currency,
                              I_loc_currency,
                              L_conv_selling_retail,
                              'R',
                              NULL,
                              NULL,
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
      L_selling_retail := L_conv_selling_retail;
   end if;

   if ADD_TAX(O_error_message,
              I_item,
              I_dept,
              I_class,
              L_item_record.pack_ind,
              I_location,
              I_loc_type ,
              I_loc_currency,
              L_vdate,
              L_selling_retail) = FALSE then
      return FALSE;
   end if;

   if CURRENCY_SQL.ROUND_CURRENCY(O_error_message,
                                  L_selling_retail,
                                  I_loc_currency,
                                  'R') = FALSE then
      return FALSE;
   end if;

   O_selling_retail       := L_selling_retail;
   O_selling_uom          := L_item_record.standard_uom;
   O_selling_retail_curr  := I_loc_currency;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALCULATE_RETAIL_FROM_MKUP;
---------------------------------------------------------------------------------------------------------
-- Function: CALC_RETAIL_FROM_MARKUP
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_FROM_MARKUP(O_error_message        OUT  VARCHAR2,
                                 O_unit_retail          OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                                 I_markup_percent    IN      NUMBER,
                                 I_markup_calc_type  IN      VARCHAR2,
                                 I_unit_cost         IN      ITEM_LOC_SOH.UNIT_COST%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'RETAIL_API_SQL.CALC_RETAIL_FROM_MARKUP';

BEGIN

   if I_markup_calc_type = 'C' then
      O_unit_retail := I_unit_cost * ( 1 + (I_markup_percent/100)) ;
   elsif I_markup_calc_type = 'R' then
      O_unit_retail := I_unit_cost / ( 1 - (I_markup_percent/100));
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_RETAIL_FROM_MARKUP;
---------------------------------------------------------------------------------------------------------
-- Function: ADD_TAX
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION ADD_TAX(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item             IN     ITEM_MASTER.ITEM%TYPE,
                 I_dept             IN     ITEM_MASTER.DEPT%TYPE,
                 I_class            IN     ITEM_MASTER.CLASS%TYPE,
                 I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                 I_from_loc         IN     ITEM_LOC.LOC%TYPE,
                 I_from_entity_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                 I_from_loc_curr    IN     CURRENCIES.CURRENCY_CODE%TYPE,
                 I_date             IN     DATE,
                 IO_retail          IN OUT ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61) := 'RETAIL_API_SQL.ADD_TAX';

   L_class_vat_ind           CLASS.CLASS_VAT_IND%TYPE;
   L_retail_before_tax       ITEM_LOC.UNIT_RETAIL%TYPE;
   L_retail_after_tax        ITEM_LOC.UNIT_RETAIL%TYPE;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   if LP_system_options.default_tax_type = 'SALES' then
      return TRUE;
   end if;

   if CLASS_ATTRIB_SQL.GET_CLASS_VAT_IND(O_error_message,
                                         L_class_vat_ind,
                                         I_dept,
                                         I_class) = FALSE then
      return FALSE;
   end if;

   if LP_system_options.default_tax_type = 'GTAX' or
      (LP_system_options.default_tax_type = 'SVAT' and
         (LP_system_options.class_level_vat_ind = 'N' or
            (LP_system_options.class_level_vat_ind = 'Y' and
             L_class_vat_ind = 'Y'))) then

      L_retail_before_tax := IO_retail;

      if TAX_SQL.ADD_RETAIL_TAX(O_error_message,
                                L_retail_after_tax,
                                I_item,
                                I_pack_ind,
                                I_dept,
                                I_class,
                                I_from_loc,
                                I_from_entity_type,
                                I_date,
                                L_retail_before_tax,
                                I_from_loc_curr,
                                1, -- I_qty
                                TAX_SQL.LP_CALL_TYPE_RPM_NIL) = FALSE then
         return FALSE;
      end if;

      IO_retail := L_retail_after_tax;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_TAX;

END RETAIL_API_SQL;
/