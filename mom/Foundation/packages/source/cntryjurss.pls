CREATE OR REPLACE PACKAGE COUNTRY_TAX_JURS_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------
-- Function Name: CHECK_DUP_JURS
-- Purpose:       This function validates that the passed in state, country,
--                jurisdiction code combination is unique 
---------------------------------------------------------------------------------
FUNCTION CHECK_DUP_JURS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists        IN OUT BOOLEAN,
                        I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_state         IN     STATE.STATE%TYPE,
                        I_jurisdiction  IN     COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: DELETE_JURS
-- Purpose:       This function will validate if the passed in state, country 
--                jurisdiction code combination are not being used in the system.
--                If not being used, the function deletes the jurisdiction code. 
---------------------------------------------------------------------------------
FUNCTION DELETE_JURS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists           IN OUT BOOLEAN,
                     I_country_id       IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_state            IN     STATE.STATE%TYPE,
                     I_jurisdiction     IN     COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_JURS_DESC
-- Purpose:       This function retrieves the jurisdiction code description for a 
--                given jurisdiction code, country and state.
---------------------------------------------------------------------------------
FUNCTION GET_JURS_DESC(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists            IN OUT BOOLEAN,
                       O_jurisdiction_desc IN OUT COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE, 
                       I_country_id        IN     COUNTRY.COUNTRY_ID%TYPE,
                       I_state             IN     STATE.STATE%TYPE,
                       I_jurisdiction      IN     COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE)
return BOOLEAN;
---------------------------------------------------------
END COUNTRY_TAX_JURS_SQL;
/
