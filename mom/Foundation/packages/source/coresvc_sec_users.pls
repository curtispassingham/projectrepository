CREATE OR REPLACE PACKAGE CORESVC_SEC_USER AUTHID CURRENT_USER AS
------------------------------------------------------------------
   template_key                    CONSTANT VARCHAR2(255):='SEC_USER_DATA';
   action_new                      VARCHAR2(25)          := 'NEW';
   action_mod                      VARCHAR2(25)          := 'MOD';
   action_del                      VARCHAR2(25)          := 'DEL';
   
   SEC_USER_ROLE_sheet             VARCHAR2(255)         := 'SEC_USER_ROLE';
   SEC_USER_ROLE$Action            NUMBER                :=1;
   SEC_USER_ROLE$ROLE              NUMBER                :=2;
   SEC_USER_ROLE$USER_SEQ          NUMBER                :=3;    
   
   SEC_USER_sheet                  VARCHAR2(255)         := 'SEC_USER';
   SEC_USER$Action                 NUMBER                :=1;
   SEC_USER$APPLICATION_USER_ID    NUMBER                :=4;
   SEC_USER$DATABASE_USER_ID       NUMBER                :=3;
   SEC_USER$USER_SEQ               NUMBER                :=2;
   
   SEC_USER$ALLOCATION_USER_IND    NUMBER                :=11;
   SEC_USER$REIM_USER_IND          NUMBER                :=10;
   SEC_USER$RESA_USER_IND          NUMBER                :=9;
   SEC_USER$RMS_USER_IND           NUMBER                :=8;
   SEC_USER$MANAGER                NUMBER                :=7;
   
   sheet_name_trans                S9T_PKG.trans_map_typ;
   action_column                   VARCHAR2(255) := 'ACTION';
   template_category               CODE_DETAIL.CODE%TYPE := 'RMSSEC';
   TYPE SEC_USER_rec_tab IS TABLE OF SEC_USER%ROWTYPE;
   TYPE SEC_USER_ROLE_rec_tab IS TABLE OF SEC_USER_ROLE%ROWTYPE;
   -------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   -------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
   -------------------------------------------------------------------
END CORESVC_SEC_USER;
/
