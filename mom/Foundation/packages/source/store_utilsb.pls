CREATE OR REPLACE PACKAGE BODY STORE_UTILS AS
--------------------------------------------------------------------
FUNCTION check_country(p_store      IN NUMBER,
					   p_store_type IN VARCHAR2)
	RETURN VARCHAR2 IS
   
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
   L_exists VARCHAR2(10) := 'Y';
   L_addr_rec ADDR%ROWTYPE;
   L_country_attrib_rec COUNTRY_ATTRIB%ROWTYPE;
   L_currect_country_id COUNTRY_ATTRIB.COUNTRY_ID%TYPE;
   L_module ADDR.MODULE%TYPE;
   
BEGIN
   if p_store_type != 'C' then
      L_module      := 'WFST';
   else
      L_module := 'ST';
   end if;
   L_addr_rec.key_value_1 := p_store;
   if ADDRESS_SQL.GET_PRIM_ADDR(L_error_message, L_addr_rec.add_1, L_addr_rec.add_2, L_addr_rec.add_3, L_addr_rec.city, L_addr_rec.state, L_addr_rec.country_id, L_addr_rec.post, L_module, L_addr_rec.key_value_1, NULL) = FALSE then
      return L_error_message;
   end if;
   ---
   L_currect_country_id := L_addr_rec.country_id;
   if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(L_error_message, L_country_attrib_rec, NULL, L_currect_country_id) = FALSE then
      return L_error_message;
   end if;
  ---
   if L_country_attrib_rec.localized_ind                    != 'Y' then
      if STORE_SQL.DEL_STORE_ATTRIB(L_error_message, p_store) = FALSE then
         return L_error_message;
      end if;
   end if;
   
   return L_error_message;
   
EXCEPTION
   when OTHERS then
      return SQLERRM;
END check_country;

--------------------------------------------------------------------
FUNCTION CHECK_L10N(p_store      IN NUMBER,
					p_store_type IN VARCHAR2,
					p_pm_mode    IN VARCHAR2)
	RETURN VARCHAR2 IS

   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
   L_key_tbl L10N_ENTITY_SQL.TBL_KEY_VAL_TBL;
   L_base_table EXT_ENTITY.BASE_RMS_TABLE%TYPE := 'STORE';
   L_key_name_1 VARCHAR2(250)                  := 'STORE';
   L_add_1 ADDR.ADD_1%TYPE;
   L_add_2 ADDR.ADD_2%TYPE;
   L_add_3 ADDR.ADD_3%TYPE;
   L_city ADDR.CITY%TYPE;
   L_state ADDR.STATE%TYPE;
   L_country_id ADDR.COUNTRY_ID%TYPE;
   L_post ADDR.POST%TYPE;
   L_module ADDR.MODULE%TYPE;
   
BEGIN
   if p_pm_mode    = 'NEW' then
      L_base_table := 'STORE_ADD';
   else
      L_base_table := 'STORE';
   end if;
   if p_store_type != 'C' then
      L_module      := 'WFST';
   else
      L_module := 'ST';
   end if;
   if L10N_ENTITY_SQL.BUILD_L10N_EXT_KEY(L_error_message, L_key_tbl, I_base_table=>L_base_table, I_key_name_1=>L_key_name_1, I_key_val_1 =>p_store) = FALSE then
      return L_error_message;
   end if;
   ---
   if ADDRESS_SQL.GET_PRIM_ADDR(L_error_message, L_add_1, L_add_2, L_add_3, L_city, L_state, L_country_id, L_post, L_module, p_store, NULL) = FALSE then
      return L_error_message;
   end if;
   ---
   if L10N_ENTITY_SQL.CHECK_REQ_EXT(L_error_message, L_base_table, L_country_id, L_key_tbl) = FALSE then
      return L_error_message;
   end if;
   
   return L_error_message;
  ---
EXCEPTION
   when OTHERS then
      return SQLERRM;
END CHECK_L10N;

--------------------------------------------------------------------
FUNCTION check_pricing_zone_id(p_store      IN NUMBER,
							   p_price_zone IN ITEM_LOC.LOC%TYPE) 
	RETURN VARCHAR2 IS
	
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_zone_id                  OBJ_ZONE_TBL;
   L_exists                   BOOLEAN;
   L_result                   BOOLEAN;
   
BEGIN
   L_result :=PM_RETAIL_API_SQL.GET_PRICING_ZONE_ID (L_error_message,
                                             L_zone_id,
                                             L_exists,
                                             p_store,
                                             'S',
                                             p_price_zone);
   return l_error_message;
   
END check_pricing_zone_id;

--------------------------------------------------------------------
FUNCTION get_exists_pricing_zone_id(o_error_message IN OUT varchar2,
									o_exists        IN OUT number,
									p_price_zone    IN ITEM_LOC.LOC%TYPE)
	RETURN NUMBER IS
	
   L_zone_id                  OBJ_ZONE_TBL;
   L_result                   NUMBER;
   
BEGIN
   L_result := PM_RETAIL_API_WRP.GET_PRICING_ZONE_ID (o_error_message,
                                             L_zone_id,
                                             o_exists,
                                             null,
                                             null,
                                             p_price_zone);
   return L_result;
	
END get_exists_pricing_zone_id;

--------------------------------------------------------------------
FUNCTION validate_district(o_error_message IN OUT varchar2,
						   i_district      IN number,
						   i_store         IN number)
  RETURN NUMBER IS
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
   L_exists         BOOLEAN := NULL;
   L_orig_district  DISTRICT.DISTRICT%TYPE;
   L_temp           VARCHAR2(1);
   L_table          VARCHAR2(30);
   L_district_name  DISTRICT.DISTRICT_NAME%TYPE;
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_CHECK_DISTRICT is
      select district
        from store
       where store = i_store;
   ---
   cursor C_LOCK_STORE_HIER is
      select 'x'
        from store_hierarchy
       where store = i_store
         for update nowait;

BEGIN
   if i_district is not NULL then
      Savepoint districtValidate;
      if ORGANIZATION_ATTRIB_SQL.GET_NAME(L_error_message,
                                          40,
                                          i_district,
                                          L_district_name) = FALSE then
         L_district_name := NULL;
         --emessage(L_error_message);
         --raise FORM_TRIGGER_FAILURE;
         o_error_message := L_error_message;
         return 0;
      end if;
      ---
      open C_CHECK_DISTRICT;
      fetch C_CHECK_DISTRICT into L_orig_district;
      close C_CHECK_DISTRICT;
      ---
      if L_orig_district != i_district then 
         open C_LOCK_STORE_HIER;
         fetch C_LOCK_STORE_HIER into L_temp;
         while C_LOCK_STORE_HIER%FOUND loop
            fetch C_LOCK_STORE_HIER into L_temp;
         end loop;
         close C_LOCK_STORE_HIER;
         ---
         update store_hierarchy
            set district = i_district,
                region = (select region from district 
                           where district = i_district),
                area = (select r.area from region r, district d 
                         where d.district = i_district 
                           and d.region = r.region),
                chain = (select a.chain from area a, region r, district d 
                          where d.district = i_district
                            and d.region = r.region and r.area = a.area)
         where store = i_store;
         ---
      end if;
      ---
   else
      L_district_name := NULL;
   end if;
   
   return 1;

EXCEPTION
   when RECORD_LOCKED then
      Rollback to districtValidate;
      -- NOTE: L_table not initialized !!!
      if L_table = 'STORE_HIERARCHY' then
         --emessage('STORE_HIER_LOCKED');
         o_error_message := 'STORE_HIER_LOCKED';
      elsif L_table = 'SYSTEM_OPTIONS' then
         --emessage('SYSTEM_OPTIONS_LOCKED');
         o_error_message := 'SYSTEM_OPTIONS_LOCKED';
      else
         o_error_message := SQLERRM;
      end if;
      ---
      -- raise FORM_TRIGGER_FAILURE;
      return 0;
--   when FORM_TRIGGER_FAILURE then
--      raise;
   when OTHERS then
      --emessage(SQLERRM);
      --raise FORM_TRIGGER_FAILURE;
      o_error_message := SQLERRM;
      return 0;
END validate_district;

--------------------------------------------------------------------
FUNCTION cancel_store(i_store in number) RETURN VARCHAR2 IS
  L_error_message rtk_errors.rtk_text%type;
  L_result boolean;
BEGIN
   if STORE_SQL.CANCEL_STORE(L_error_message,
                             i_store) = FALSE then
      return L_error_message;
--      raise FORM_TRIGGER_FAILURE;
   end if;

   if L10N_SQL.REFRESH_MV_L10N_ENTITY(L_error_message) = FALSE then
      return L_error_message;
--      raise FORM_TRIGGER_FAILURE;
   end if;
      
   if ADDRESS_SQL.REFRESH_MV_LOC_PRIM_ADDR(L_error_message) = FALSE then
      return L_error_message;
--      raise FORM_TRIGGER_FAILURE;
   end if;
   return null;
EXCEPTION WHEN OTHERS THEN
    return SQLERRM;
end cancel_store;

--------------------------------------------------------------------
PROCEDURE set_savepoint_pregc AS
BEGIN
  savepoint pregc;
END set_savepoint_pregc;

--------------------------------------------------------------------
PROCEDURE rollback_pregc AS
BEGIN
  rollback to pregc;
END rollback_pregc;

END STORE_UTILS;
/
