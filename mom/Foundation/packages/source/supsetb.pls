CREATE OR REPLACE PACKAGE BODY SUPPLIER_SETUP_SQL AS
-------------------------------------------------------------------------------
FUNCTION LOCK_SUPS (O_error_message IN OUT VARCHAR2,
                    I_supplier      IN     SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_table        VARCHAR2(30);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_SUPS is
      select 'x'
        from sups
       where supplier = I_supplier
         for update nowait;

BEGIN
   L_table := 'SUPS';
   open C_LOCK_SUPS;
   close C_LOCK_SUPS;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('SUPPLIER_LOCKED',
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SUPPLIER_SETUP_SQL.LOCK_SUPS',
                                             to_char(SQLCODE));
      return FALSE;

END LOCK_SUPS;
-------------------------------------------------------------------------------
FUNCTION DEL_CHILD_RECORDS(O_error_message   IN OUT  VARCHAR2,
                           I_supplier        IN      SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);
   L_program                  VARCHAR2(100) := 'SUPPLIER_SETUP_SQL.DEL_CHILD_RECORDS';
   L_table                    VARCHAR2(30);
   L_exists_ind               VARCHAR2(1);
   L_procedure_key            VARCHAR2(30) := 'DEL_ENT_TRIB_SUBS';
   L_country_id               VARCHAR2(3);
   L_source_entity            VARCHAR2(6)  := 'SUPP';
   L_source_id                VARCHAR2(10)  := I_supplier;
   L_source_type              VARCHAR2(6);
   L_item                     VARCHAR2(25);
  
   cursor C_SUPS_ADD is
      select 'x'
        from addr
       where module = 'SUPP'
         and key_value_1 = to_char(I_supplier)
         for update nowait;

   cursor C_SUPS_ADD_TL is
      select 'x'
        from addr_tl
       where addr_key in (select addr_key
                            from addr
                           where key_value_1=to_char(I_supplier)
                             and module='SUPP')
         for update nowait;

   cursor C_SUP_REPL_DAY is
      select 'x'
        from sup_repl_day
       where sup_dept_seq_no in (select sup_dept_seq_no
                                 from sup_inv_mgmt
                                 where supplier = I_supplier)
         for update nowait;

   cursor C_SUP_INV_MGMT is
      select 'x'
        from sup_inv_mgmt
       where supplier = I_supplier
                           for update nowait;

   cursor C_SUP_BRACKET_COST is
      select 'x'
        from sup_bracket_cost
       where supplier = I_supplier
                           for update nowait;

   cursor C_REQ_DOC is
      select 'x'
        from req_doc
       where module = 'SUPP'
         and key_value_1 = to_char(I_supplier)
         for update nowait;

   cursor C_SUP_IMPORT_ATTR is
      select 'x'
        from sup_import_attr
       where supplier = I_supplier
         for update nowait;

   cursor C_SUP_TRAITS_MATRIX is
      select 'x'
        from sup_traits_matrix
       where supplier = I_supplier
         for update nowait;

   cursor C_EXP_PROF_HEAD is
      select 'x'
        from exp_prof_head
       where module = 'SUPP'
         and key_value_1 = to_char(I_supplier)
         for update nowait;

   cursor C_EXP_PROF_DETAIL is
      select 'x'
        from exp_prof_detail
       where exp_prof_key in (select exp_prof_key
                              from exp_prof_head
                              where key_value_1 = to_char(I_supplier))
         for update nowait;

   cursor C_PARTNER_ORG_UNIT is
      select 'x'
        from partner_org_unit
       where partner = I_supplier
         and partner_type in ('S','U')
         for update nowait;

   cursor C_SUPS_L10N_EXT is
      select 'x'
        from sups_l10n_ext
       where supplier = I_supplier
         for update nowait;
         
   cursor C_SUPS_CFA_EXT is
      select 'x'
        from sups_cfa_ext
       where supplier = I_supplier
         for update nowait;         

    cursor C_LOCK_ADDR_CFA_EXT is
       select 'x'
         from addr_cfa_ext
        where exists (select 'x'
                        from addr
                       where key_value_1 = to_char(I_supplier)
                         and module      = 'SUPP')
          for update nowait;
BEGIN
   ---Delete from the SUPS_L10N_EXT table
   L_table := 'SUPS_L10N_EXT';
   open C_SUPS_L10N_EXT;
   close C_SUPS_L10N_EXT;
   ---
   delete from sups_l10n_ext
         where supplier = to_char(I_supplier);
   ---
   if L10N_SQL.CALL_EXEC_FUNC_FND(O_error_message,
                                  L_exists_ind,
                                  L_procedure_key,
                                  L_country_id,
                                  L_source_entity,
                                  L_source_id,
                                  L_source_type,
                                  L_item) = FALSE then
      return FALSE;
   end if;
   
   --- delete from CFA sups extension table before deleting from sups
  L_table := 'SUPS_CFA_EXT';
  SQL_LIB.SET_MARK('OPEN',
                    'C_SUPS_CFA_EXT',
                    'SUPS_CFA_EXT',
                    'SUPP: '||to_char(I_supplier));
   open C_SUPS_CFA_EXT;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUPS_CFA_EXT',
                    'SUPS_CFA_EXT',
                    'SUPP: '||to_char(I_supplier));
   close C_SUPS_CFA_EXT;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SUPS_CFA_EXT',
                    'SUPP: '||to_char(I_supplier));

   delete from SUPS_CFA_EXT
     where supplier = to_char(I_supplier);      
  
   --- Delete from CFA extension table before deleting from addr

   L_table := 'ADDR_CFA_EXT';
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ADDR_CFA_EXT',
                    'ADDR_CFA_EXT',
                    'SUPP: '||to_char(I_supplier));
   open C_LOCK_ADDR_CFA_EXT;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ADDR_CFA_EXT',
                    'ADDR_CFA_EXT',
                    'SUPP: '||to_char(I_supplier));
   close C_LOCK_ADDR_CFA_EXT;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ADDR_CFA_EXT',
                    'SUPP: '||to_char(I_supplier));

   delete from addr_cfa_ext
         where addr_key in (select addr_key
                              from addr
                             where key_value_1 = to_char(I_supplier)
                               and module      = 'SUPP');   
   ---Delete from the ADDR_TL table
   L_table := 'ADDR_TL';
   open C_SUPS_ADD_TL;
   close C_SUPS_ADD_TL;
   ---
   delete from addr_tl
         where addr_key in (select addr_key
                              from addr
                             where key_value_1=to_char(I_supplier)
                               and module='SUPP');
   ---
   ---Delete from the ADDR table
   L_table := 'ADDR';
   open C_SUPS_ADD;
   close C_SUPS_ADD;
   ---
   delete from addr
         where module = 'SUPP'
           and key_value_1 = to_char(I_supplier);
   ---
   ---Delete from the tables sup_repl_day
   L_table := 'SUP_REPL_DAY';    
   open C_SUP_REPL_DAY;
   close C_SUP_REPL_DAY;
   ---
   delete from sup_repl_day
    where sup_dept_seq_no in (select sup_dept_seq_no
                              from sup_inv_mgmt
                              where supplier = I_supplier);
   ---
   ---Delete from the sup_bracket_cost table
   L_table := 'SUP_BRACKET_COST';
   open C_SUP_BRACKET_COST;
   close C_SUP_BRACKET_COST;
   ---
   delete from sup_bracket_cost
         where supplier = I_supplier;
   ---
   ---Delete from the sup_inv_mgmt table
   L_table := 'SUP_INV_MGMT';
   open C_SUP_INV_MGMT;
   close C_SUP_INV_MGMT;
   ---
   delete from sup_inv_mgmt
         where supplier = I_supplier;
   ---
   ---Delete from the req_doc table
   L_table := 'REQ_DOC';
   open C_REQ_DOC;
   close C_REQ_DOC;
   ---
   delete from req_doc
         where module = 'SUPP'
           and key_value_1 = to_char(I_supplier);
   ---
   ---Delete from the sup_import_attr table
   L_table := 'SUP_IMPORT_ATTR';
   open C_SUP_IMPORT_ATTR;
   close C_SUP_IMPORT_ATTR;
   --- 
   delete from sup_import_attr
         where supplier = I_supplier;
   ---
   ---Delete from the sup_traits_matrix table
   L_table := 'SUP_TRAITS_MATRIX';
   open C_SUP_TRAITS_MATRIX;
   close C_SUP_TRAITS_MATRIX;
   ---
   delete from sup_traits_matrix
         where supplier = I_supplier;
   ---
   --- delete the record off of exp_prof_detail table
   L_table := 'EXP_PROF_DETAIL';
   open C_EXP_PROF_DETAIL;
   close C_EXP_PROF_DETAIL;
   ---
   delete from exp_prof_detail
         where exp_prof_key in (select exp_prof_key
                                from exp_prof_head
                                where key_value_1 = to_char(I_supplier));
   ---
   ---delete from the exp_prof_head table
   L_table := 'EXP_PROF_HEAD';
   open C_EXP_PROF_HEAD;
   close C_EXP_PROF_HEAD;
   ---
   delete from exp_prof_head
         where module = 'SUPP'
           and key_value_1 = to_char(I_supplier);
   ---
   ---delete from the Partner Org Unit table
   L_table := 'PARTNER_ORG_UNIT';
   open C_PARTNER_ORG_UNIT;
   close C_PARTNER_ORG_UNIT;
   ---
   delete from partner_org_unit
         where partner_type in ('S','U')
           and partner = I_supplier;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=
            SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                                'Supplier: ' || to_char(I_supplier),
                                NULL);
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END DEL_CHILD_RECORDS;
---------------------------------------------------------------------------
FUNCTION INSERT_SUPPLIER_SITE_ADDRESS (O_error_message     IN OUT   VARCHAR2,
                                       I_supplier          IN       SUPS.SUPPLIER%TYPE,
                                       I_supplier_parent   IN       SUPS.SUPPLIER_PARENT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'SUPP_SETUP_SQL.INSERT_SUPPLIER_SITE_ADDRESS';

BEGIN
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier', 
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_supplier_parent is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier_parent', 
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   insert into addr (addr_key,
                     module,
                     key_value_1,
                     key_value_2,
                     seq_no,
                     addr_type,
                     primary_addr_ind,
                     add_1,
                     add_2,
                     add_3,
                     city,
                     state,
                     country_id,
                     jurisdiction_code,
                     post,
                     contact_name,
                     contact_phone,
                     contact_telex,
                     contact_fax,
                     contact_email,
                     oracle_vendor_site_id,
                     edi_addr_chg,
                     county)
             (select addr_sequence.NEXTVAL,
                     ps.module,
                     to_char(I_supplier),
                     ps.key_value_2,
                     decode(s.seq,NULL,ps.seq_no,s.seq+1),
                     ps.addr_type,
                     decode(s.prim,NULL,'Y','Y','Y','N'),
                     ps.add_1,
                     ps.add_2,
                     ps.add_3,
                     ps.city,
                     ps.state,
                     ps.country_id,
                     ps.jurisdiction_code,
                     ps.post,
                     ps.contact_name,
                     ps.contact_phone,
                     ps.contact_telex,
                     ps.contact_fax,
                     ps.contact_email,
                     ps.oracle_vendor_site_id,
                     ps.edi_addr_chg,
                     ps.county
                from addr ps, 
                     (select addr_type, 
                             MAX(seq_no) seq, 
                             MAX(primary_addr_ind) prim 
                        from addr 
                       where key_value_1 = to_char(I_supplier)
                       group by addr_type) s
               where ps.module = 'SUPP'
                 and ps.key_value_1 = to_char(I_supplier_parent)
                 and ps.primary_addr_ind = 'Y'
                 and ps.addr_type=s.addr_type(+));
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END INSERT_SUPPLIER_SITE_ADDRESS;
---------------------------------------------------------------------
FUNCTION DEL_SUPS_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_supplier        IN       SUPS_L10N_EXT.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   
   L_program       VARCHAR2(50) := 'SUPPLIER_SETUP_SQL.DEL_SUPS_ATTRIB';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SUPS_L10N_EXT is
      select 'x'
        from sups_l10n_ext
       where supplier = I_supplier
         for update nowait;
   
BEGIN
   
   L_table := 'SUPS_L10N_EXT';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_SUPS_L10N_EXT',
                    L_table,
                    'supplier: '||I_supplier);

   open C_LOCK_SUPS_L10N_EXT;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_SUPS_L10N_EXT',
                    L_table,
                    'supplier: '||I_supplier);

   close C_LOCK_SUPS_L10N_EXT;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'supplier: '||I_supplier);

   delete from sups_l10n_ext
         where supplier = I_supplier;
   ---
   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'Supplier: ' || to_char(I_supplier),
                                             NULL);
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END DEL_SUPS_ATTRIB;
---------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_supplier_id       IN       SUPS.SUPPLIER%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(75)   := 'SUPPLIER_SETUP_SQL.CUSTOM_VAL';
   L_custom_obj_rec      CUSTOM_OBJ_REC :=  CUSTOM_OBJ_REC();

BEGIN

   L_custom_obj_rec.function_key:= I_function_key;
   L_custom_obj_rec.call_seq_no:= I_seq_no;
   L_custom_obj_rec.supplier:= I_supplier_id;


   --call the custom code for client specific order approval
   if CALL_CUSTOM_SQL.EXEC_FUNCTION(O_error_message,
                                    L_custom_obj_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CUSTOM_VAL;
--------------------------------------------------------------------------------------------
END SUPPLIER_SETUP_SQL;
/


