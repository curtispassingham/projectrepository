
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DAILY_PURGE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
--  Function Name  :  CHECK_EXISTS
--  Purpose        :  Check if given inputs exist on daily purge.
--  Input Values   :  key_value and table name
--  Return Value   :  O_exists - indicates if entered values exist on daily_purge
----------------------------------------------------------------
FUNCTION CHECK_EXISTS (O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_key_value     IN     DAILY_PURGE.KEY_VALUE%TYPE,
                       I_table_name    IN     DAILY_PURGE.TABLE_NAME%TYPE) return BOOLEAN;
----------------------------------------------------------------
--  Function Name  :  INSERT_RECORD
--  Purpose        :  Inserts inputted values onto daily_purge.
--  Input Values   :  key_value, table name, delete_type and delete_order
----------------------------------------------------------------
FUNCTION INSERT_RECORD (O_error_message  IN OUT VARCHAR2,
                        I_key_value      IN     DAILY_PURGE.KEY_VALUE%TYPE,
                        I_table_name     IN     DAILY_PURGE.TABLE_NAME%TYPE,
                        I_delete_type    IN     DAILY_PURGE.DELETE_TYPE%TYPE,
                        I_delete_order   IN     DAILY_PURGE.DELETE_ORDER%TYPE) return BOOLEAN;

----------------------------------------------------------------
--    Name: ITEM_PARENT_GRANDPARENT_EXIST
-- Purpose: Check if item or its parent/grandparent exist on daily purge.
----------------------------------------------------------------
FUNCTION ITEM_PARENT_GRANDPARENT_EXIST(O_error_message IN OUT VARCHAR2,
                                       O_exists        IN OUT BOOLEAN,
                                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                       I_table_name    IN     DAILY_PURGE.TABLE_NAME%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
--   Name   : ITEM_UPDATE_STATUS
--   Purpose: Modify the status of the item or its parent/grandparent when the item
--            is cancelled in online
---------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_STATUS(O_error_message  IN OUT VARCHAR2,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE)                        
RETURN BOOLEAN ;
---------------------------------------------------------------------------------
--   Name   : CONTENTS_ITEM_EXISTS
--   Purpose: Check if a contents item exist that the container item is attached
--            to
---------------------------------------------------------------------------------
FUNCTION CONTENTS_ITEM_EXISTS(O_error_message  IN OUT VARCHAR2,
                              O_exists         IN OUT BOOLEAN,
                              I_item           IN     ITEM_MASTER.ITEM%TYPE)                        
RETURN BOOLEAN ;
---------------------------------------------------------------------------------
FUNCTION REF_ITEM_COUNT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_rec_count      IN OUT   NUMBER,
                        I_item           IN       ITEM_MASTER.ITEM%TYPE,
                        I_table_name     IN       DAILY_PURGE.TABLE_NAME%TYPE)
RETURN BOOLEAN ;
--------------------------------------------------------------------------
FUNCTION CHECK_PRIMARY_REF_ITEM_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                    I_table_name      IN       DAILY_PURGE.TABLE_NAME%TYPE)
RETURN BOOLEAN ;                                    
--------------------------------------------------------------------------
--  Function Name  :  ALL_RECORDS_DELETED
--  Purpose        :  Check if all other records are pending deletion.
--  Input Values   :  I_item and table name
--  Return Value   :  O_exists - indicates if all records are pending deletion
--------------------------------------------------------------------------
FUNCTION ALL_RECORDS_DELETED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_table_name      IN       DAILY_PURGE.TABLE_NAME%TYPE)
RETURN BOOLEAN ;                                    
--------------------------------------------------------------------------
END;
/


