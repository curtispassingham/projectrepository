CREATE OR REPLACE PACKAGE CORESVC_BRAND AUTHID CURRENT_USER AS
   template_key     CONSTANT VARCHAR2(255)         := 'BRAND_DATA';
   action_new                VARCHAR2(25)          := 'NEW';
   action_mod                VARCHAR2(25)          := 'MOD';
   action_del                VARCHAR2(25)          := 'DEL';

   BRAND_sheet               VARCHAR2(255)         := 'BRAND';
   BRAND$Action              NUMBER                := 1;
   BRAND$BRAND_NAME          NUMBER                := 2;
   BRAND$BRAND_DESCRIPTION   NUMBER                := 3;

   BRAND_TL_sheet               VARCHAR2(255)         := 'BRAND_TL';
   BRAND_TL$Action              NUMBER                := 1;
   BRAND_TL$Lang                NUMBER                := 2;
   BRAND_TL$BRAND_NAME          NUMBER                := 3;
   BRAND_TL$BRAND_DESCRIPTION   NUMBER                := 4;

   
   sheet_name_trans          S9T_PKG.trans_map_typ;
   action_column             VARCHAR2(255)         := 'ACTION';
   template_category         CODE_DETAIL.CODE%TYPE := 'RMSITM';
   TYPE BRAND_rec_tab IS TABLE OF BRAND%ROWTYPE;
------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
------------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER )
   RETURN BOOLEAN;
------------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER )
   RETURN BOOLEAN;
------------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)
   RETURN VARCHAR2;
------------------------------------------------------------------------------   
END CORESVC_BRAND;
/