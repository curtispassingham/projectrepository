CREATE OR REPLACE PACKAGE CORESVC_FLTR_GRP_HIER AUTHID CURRENT_USER AS
------------------------------------------------------------------
   template_key            CONSTANT VARCHAR2(25) := 'FLTR_GRP_HIER_DATA';
   action_new                       VARCHAR2(25) := 'NEW';
   action_mod                       VARCHAR2(25) := 'MOD';
   action_del                       VARCHAR2(25) := 'DEL';
   FLTR_GRP_ORG_sheet               VARCHAR2(25) := 'FILTER_GROUP_ORG';
   FLTR_GRP_ORG$Action              NUMBER       :=1;
   FLTR_GRP_ORG$FILTER_ORG_ID       NUMBER       :=4;
   FLTR_GRP_ORG$FILTER_ORG_LEVEL    NUMBER       :=3;
   FLTR_GRP_ORG$SEC_GROUP_ID        NUMBER       :=2;
   TYPE FLTR_GRP_ORG_rec_tab IS TABLE OF FILTER_GROUP_ORG%ROWTYPE;

   FLTR_GRP_MERCH_sheet             VARCHAR2(25) := 'FILTER_GROUP_MERCH';
   FLTR_GRP_MERCH$Action            NUMBER       :=1;
   FLTR_GRP_MERCH$ID_SUBCLASS       NUMBER       :=6;
   FLTR_GRP_MERCH$ID_CLASS          NUMBER       :=5;
   FLTR_GRP_MERCH$FLTR_MRCH_ID      NUMBER       :=4;
   FLTR_GRP_MERCH$FLTR_MRCH_LEVEL   NUMBER       :=3;
   FLTR_GRP_MERCH$SEC_GROUP_ID      NUMBER       :=2;
   TYPE FLTR_GRP_MERCH_rec_tab IS TABLE OF FILTER_GROUP_MERCH%ROWTYPE;
   
   action_column       VARCHAR2(255) := 'ACTION';
   template_category   CODE_DETAIL.CODE%TYPE := 'RMSSEC';
   sheet_name_trans    S9T_PKG.trans_map_typ;
  -----------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
  -----------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
  ----------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
  ---------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
  ----------------------------------------------------------------------------    
END CORESVC_FLTR_GRP_HIER;
/
