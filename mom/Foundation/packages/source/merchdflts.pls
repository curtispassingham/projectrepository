
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE MERCH_DEFAULT_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
--- Procedure:  CHECK_DUP_MERCH_DEFAULTS
--- Purpose:	This function will check for duplicate records on the merch_heir_defaults table.

-----------------------------------------------------------------------------------
FUNCTION GET_SEQ_NO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_seq_no          IN OUT   MERCH_HIER_DEFAULT.SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION CHECK_DUP_MERCH_DEFAULTS (O_error_message IN OUT VARCHAR2,
                                   O_dup_exist     IN OUT BOOLEAN,
                                   I_dialog        IN     MERCH_HIER_DEFAULT.DIALOG%TYPE,
                                   I_info          IN     MERCH_HIER_DEFAULT.INFO%TYPE,
                                   I_dept          IN     MERCH_HIER_DEFAULT.DEPT%TYPE,
                                   I_class         IN     MERCH_HIER_DEFAULT.CLASS%TYPE,
                                   I_subclass      IN     MERCH_HIER_DEFAULT.SUBCLASS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--- Procedure:  EXPAND_DOWN
---Purpose:     This function will expand down to the subclass record. If a dept is passed
---             in, using and insert/select statement insert a record with the passed info,
---             available and required indicators and dialog = ITEMS on the merch_hier_default
---             for each class and subclass within that department, where records do not already
---             exist on the merch_hier_defaults table. If dept and class are passed in, insert
---             a record for each subclass within that dept/class, where records do not really
---             exist on the merch_hier_defaults table.
-----------------------------------------------------------------------------------
FUNCTION EXPAND_DOWN (O_error_message IN OUT VARCHAR2,
                      I_info          IN     MERCH_HIER_DEFAULT.INFO%TYPE,
                      I_dept          IN     DEPS.DEPT%TYPE,
                      I_class         IN     CLASS.CLASS%TYPE,
                      I_available_ind IN     MERCH_HIER_DEFAULT.AVAILABLE_IND%TYPE,
                      I_required_ind  IN     MERCH_HIER_DEFAULT.REQUIRED_IND%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--- Procedure:  GET_REQ_INDS
---Purpose:     This function gets all the required indicators for each info. All possible
---             values for info are located on the codes tables with code_type = 'MHDI'
-----------------------------------------------------------------------------------
FUNCTION GET_REQ_INDS (O_error_message          IN OUT VARCHAR2,
                      O_loc_req_ind             IN OUT VARCHAR2,
                      O_season_req_ind          IN OUT VARCHAR2,
                      O_impattrib_req_ind       IN OUT VARCHAR2,
                      O_docs_req_ind            IN OUT VARCHAR2,
                      O_hts_req_ind             IN OUT VARCHAR2,
                      O_tariff_req_ind          IN OUT VARCHAR2,
                      O_exp_req_ind             IN OUT VARCHAR2,
                      O_timeline_req_ind        IN OUT VARCHAR2,
                      O_tickets_req_ind         IN OUT VARCHAR2,
                      O_image_req_ind           IN OUT VARCHAR2,
                      O_sub_tr_items_req_ind    IN OUT VARCHAR2,
                      O_dimension_req_ind       IN OUT VARCHAR2,
                      O_diffs_req_ind           IN OUT VARCHAR2,
                      O_mfg_rec_req_ind         IN OUT VARCHAR2,
                      O_pack_sz_req_ind         IN OUT VARCHAR2,
                      O_retail_lb_req_ind       IN OUT VARCHAR2,
                      O_handling_req_ind        IN OUT VARCHAR2,
                      O_handl_temp_req_ind      IN OUT VARCHAR2,
                      O_wastage_req_ind         IN OUT VARCHAR2,
                      O_comments_req_ind        IN OUT VARCHAR2,
                      I_dept                    IN     DEPS.DEPT%TYPE,
                      I_class                   IN     CLASS.CLASS%TYPE,
                      I_subclass                IN     SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION DEFAULTS_EXIST(O_error_message IN OUT  VARCHAR2,
                        O_exists        IN OUT  BOOLEAN,
                        I_dept          IN      DEPS.DEPT%TYPE,
                        I_class         IN      CLASS.CLASS%TYPE,
                        I_subclass      IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION COPY_DEFAULTS(O_error_message  IN OUT  VARCHAR2,
                       I_from_dept      IN      DEPS.DEPT%TYPE,
                       I_from_class     IN      CLASS.CLASS%TYPE,
                       I_from_subclass  IN      SUBCLASS.SUBCLASS%TYPE,
                       I_to_dept        IN      DEPS.DEPT%TYPE,
                       I_to_class       IN      CLASS.CLASS%TYPE,
                       I_to_subclass    IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END;
/


