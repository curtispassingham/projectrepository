/******************************************************************************
* Service Name     : SupplierService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/SupplierService/v1
* Description      : 
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE SUPPLIERSERVICEPROVIDERIMPL AUTHID CURRENT_USER AS


/******************************************************************************
 *
 * Operation       : createSupplierDesc
 * Description     : 
         Create new supplier. This includes creation of parent supplier info,
         Supplier sites info, org unit association and Addresses.
          
 * 
 * Input           : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            SupplierDesc object consists of inputs to parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierRef/v1
 * Description     : 
            SupplierRef object consists of information about the newly created Supplier.This includes parent supplier Ids,
            Supplier site ids,Address ids,Supplier Xref keys.The AIA Integration layer stores the supplier xref keys along
            with supplier Ids for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the Supplier object already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createSupplierDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : createSupSiteUsingSupplierDesc
 * Description     : 
         Create a new SupplierSite.This includes creation of Supplier site info, org unit association and Addresses.
         Parent Supplier already exists in the system for these supplier sites.
          
 * 
 * Input           : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            SupplierDesc object consists of inputs to parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierRef/v1
 * Description     : 
            SupplierRef object consists of information about the newly created Supplier.This includes parent supplier Ids,
            Supplier site ids,Address ids,Supplier Xref keys.The AIA Integration layer stores the supplier xref keys along
            with supplier Ids for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the supplier site object already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createSupSiteUsingSupplierDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : createSupSiteAddrUsingSupplier
 * Description     : 
         Create a new SupplierSite address.This includes creation of supplier site Addresses.
         Parent Supplier, supplier site and org unit associations already exists in the system for these supplier sites.
          
 * 
 * Input           : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            SupplierDesc object consists of inputs to parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierRef/v1
 * Description     : 
            SupplierRef object consists of information about the newly created Supplier.This includes parent supplier Ids,
            Supplier site ids,Address ids,Supplier Xref keys.The AIA Integration layer stores the supplier xref keys along
            with supplier Ids for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the supplier site address object already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createSupSiteAddrUsingSupplier(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : updateSupplierDesc
 * Description     : 
         Update Supplier.This includes updates to parent supplier info,
         Supplier sites info, org unit associations and Addresses.
          
 * 
 * Input           : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            SupplierDesc object consists of inputs to parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            The output object returns updated SupplierDesc object consists of parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updateSupplierDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : updateSupSiteUsingSupplierDesc
 * Description     : 
         Update SupSite Using SupplierDesc.This includes updates to supplier sites info, org unit associations and Addresses.
          
 * 
 * Input           : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            SupplierDesc object consists of inputs to parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            The output object returns updated SupplierDesc object consists of parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier site object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updateSupSiteUsingSupplierDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : updateSupSiteOrgUnitUsingSuppl
 * Description     : 
         Update OrgUnit Using SupplierDesc.This includes updates to org unit associations especially the primary pay site information.
          
 * 
 * Input           : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            SupplierDesc object consists of inputs to parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            The output object returns updated SupplierDesc object consists of parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier org unit object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updateSupSiteOrgUnitUsingSuppl(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : updateSupSiteAddrUsingSupplier
 * Description     : 
         Update SupSite Address Using SupplierDesc.This includes updates to supplier site Addresses.
          
 * 
 * Input           : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            SupplierDesc object consists of inputs to parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            The output object returns updated SupplierDesc object consists of parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier site address object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updateSupSiteAddrUsingSupplier(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : findSupplierDesc
 * Description     : Find SupplierDesc. 
 * 
 * Input           : "RIB_SupplierRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierRef/v1
 * Description     : 
            SupplierRef object consists of input information for finding the Supplier.This includes parent supplier Ids or
            Supplier site ids.
            
 * 
 * Output          : "RIB_SupplierDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierDesc/v1
 * Description     : 
            SupplierDesc object consists of inputs to parent supplier, supplier sites,
            org unit associations and addresses.The supplierDesc also contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE findSupplierDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierRef_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : deleteSupplierDesc
 * Description     : Delete SupplierDesc. 
 * 
 * Input           : "RIB_SupplierRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierRef/v1
 * Description     : 
            SupplierRef object consists of input information for deleting the Supplier.This includes parent supplier Id or
            Supplier site id.
            
 * 
 * Output          : "RIB_SupplierRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierRef/v1
 * Description     : 
            SupplierRef object consists of deleted parent supplier Id or Supplier site id.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE deleteSupplierDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierRef_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : createSupplierColDesc
 * Description     : 
         Create new supplier collection. This includes creation of collection of parent supplier info,
         Supplier site info, org unit association and Addresses.
          
 * 
 * Input           : "RIB_SupplierColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierColDesc/v1
 * Description     : 
            SupplierCollectionDesc object consists of inputs to collection of parent suppliers, supplier sites,
            org unit associations and addresses.The supplierDesc contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierColRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierColRef/v1
 * Description     : 
            SupplierCollectionRef object consists of collection of information about the newly created Supplier.This includes collection of parent supplier Ids,
            Supplier site ids,Address ids,Supplier Xref keys.The AIA Integration layer stores the supplier xref keys along
            with supplier Ids for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createSupplierColDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierColDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierColRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : updateSupplierColDesc
 * Description     : 
         Update Supplier collection.This includes updates to parent supplier info,
         Supplier sites info, org unit associations and Addresses within the collection of the suppliers.
          
 * 
 * Input           : "RIB_SupplierColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierColDesc/v1
 * Description     : 
            SupplierCollectionDesc object consists of inputs to collection of parent suppliers, supplier sites,
            org unit associations and addresses.The supplierDesc contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Output          : "RIB_SupplierColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierColDesc/v1
 * Description     : 
            The output object returns updated SupplierCollectionDesc object consists of collection of parent suppliers, supplier sites,
            org unit associations and addresses.The SupplierCollectionDesc contains the supplier xref keys
            that are used in the AIA integration layer for reference.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updateSupplierColDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierColDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierColDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : findSupplierColDesc
 * Description     : Find Collection of SupplierDesc. 
 * 
 * Input           : "RIB_SupplierColRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierColRef/v1
 * Description     : 
            SupplierCollectionRef object consists of input information for finding the collection of suppliers.This includes parent supplier Ids or
            Supplier site ids.
            
 * 
 * Output          : "RIB_SupplierColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierColDesc/v1
 * Description     : 
            SupplierCollectionDesc object consists of collection of parent suppliers, supplier sites,
            org unit associations and addresses.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE findSupplierColDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierColRef_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierColDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : deleteSupplierColDesc
 * Description     : Delete Collection of SupplierDesc. 
 * 
 * Input           : "RIB_SupplierColRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierColRef/v1
 * Description     : 
            SupplierCollectionRef object consists of input information for deleting the Supplier collection.This includes parent supplier Id or
            Supplier site id.
            
 * 
 * Output          : "RIB_SupplierColRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/SupplierColRef/v1
 * Description     : 
            SupplierCollectionRef object consists of collection of deleted parent supplier Ids or Supplier site ids.
            
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the supplier object does not exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE deleteSupplierColDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_SupplierColRef_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_SupplierColRef_REC"
                          );
/******************************************************************************/



END SupplierServiceProviderImpl;
/
 