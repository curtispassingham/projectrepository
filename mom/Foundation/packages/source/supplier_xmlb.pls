CREATE OR REPLACE PACKAGE BODY SUPPLIER_XML AS

/*** API State Variables ***/

   L_supplier   NUMBER(10)   := NULL;
   L_doc_type   VARCHAR2(15) := NULL;

/*** Private Internal Program Definitions***/

FUNCTION ADD_SUPPLIER(root      IN OUT   XMLDOM.DOMELEMENT,
                      I_sup     IN       SUPS%ROWTYPE,
                      O_status     OUT   VARCHAR2,
                      O_text       OUT   VARCHAR2)
RETURN BOOLEAN;

FUNCTION DELETE_SUPPLIER(root       IN OUT   XMLDOM.DOMELEMENT,
                         I_supplier IN       SUPS.SUPPLIER%TYPE,
                         O_status      OUT   VARCHAR2,
                         O_text        OUT   VARCHAR2)
RETURN BOOLEAN;

FUNCTION ADD_ADDRESS(root       IN OUT   XMLDOM.DOMELEMENT,
                     I_addr     IN       ADDR%ROWTYPE,
                     O_status      OUT   VARCHAR2,
                     O_text        OUT   VARCHAR2)
RETURN BOOLEAN;

FUNCTION DELETE_ADDRESS(root         IN OUT   XMLDOM.DOMELEMENT,
                        I_supplier   IN       SUPS.SUPPLIER%TYPE,
                        I_seq_no     IN       ADDR.SEQ_NO%TYPE,
                        I_addr_type  IN       ADDR.ADDR_TYPE%TYPE,
                        O_status              OUT VARCHAR2,
                        O_text                OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION ADD_PARTNER_ORG_UNIT(root       IN OUT   XMLDOM.DOMELEMENT,
                              I_pou      IN       PARTNER_ORG_UNIT%ROWTYPE,
                              O_status      OUT   VARCHAR2,
                              O_text        OUT   VARCHAR2)
RETURN BOOLEAN;

FUNCTION DELETE_PARTNER_ORG_UNIT(root          IN OUT   XMLDOM.DOMELEMENT,
                                 I_org_unit    IN       PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE,
                                 O_status         OUT   VARCHAR2,
                                 O_text           OUT   VARCHAR2)
RETURN BOOLEAN;

/*** Public Program Bodies***/
--------------------------------------------------------------------------------
PROCEDURE BUILD_SUPPLIER(I_sups     IN       SUPS%ROWTYPE,
                         I_event    IN       VARCHAR2,
                         O_msg         OUT   CLOB,
                         O_status      OUT   VARCHAR2,
                         O_text        OUT   VARCHAR2)
IS
   L_valid         BOOLEAN            := FALSE;
   L_error_cause   VARCHAR2(1)        := NULL;
   L_sups          SUPS%ROWTYPE       := NULL;
   root            XMLDOM.DOMELEMENT;

BEGIN
   L_sups := I_sups;
   ---
   if I_event = 'D' then
      L_doc_type := RMSMFM_SUPPLIER.VEND_REF_MSG;
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
         return;
      end if;
      ---
      if not delete_supplier(root,
                             L_sups.supplier,
                             O_status,
                             O_text) then
         return;
      end if;
      ---
      if not API_LIBRARY.WRITE_DOCUMENT(O_status,
                                        O_text,
                                        O_msg,
                                        root) then
         return;
      end if;
   else
      L_doc_type := RMSMFM_SUPPLIER.VEND_HDR_DESC_MSG;
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
         return;
      end if;
      ---
      if not add_supplier(root,
                          L_sups,
                          O_status,
                          O_text) then
         return;
      end if;
      ---
      if not API_LIBRARY.WRITE_DOCUMENT(O_status,
                                        O_text,
                                        O_msg,
                                        root) then
         return;
      end if;
   end if;
   ---
   O_status := API_CODES.SUCCESS;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.BUILD_SUPPLIER');

END BUILD_SUPPLIER;
--------------------------------------------------------------------------------
PROCEDURE BUILD_ADDRESS(I_addr      IN       ADDR%ROWTYPE,
                        I_event     IN       VARCHAR2,
                        O_msg          OUT   CLOB,
                        O_status       OUT   VARCHAR2,
                        O_text         OUT   VARCHAR2)
IS
   L_valid         BOOLEAN            := FALSE;
   L_error_cause   VARCHAR2(1)        := NULL;
   L_addr          ADDR%ROWTYPE       := NULL;
   length_var      NUMBER(15)         := 0;
   root            XMLDOM.DOMELEMENT;

BEGIN
   L_addr := I_addr;
   ---
   if I_event = 'D' then
      L_doc_type := RMSMFM_SUPPLIER.VEND_ADDR_REF_MSG;
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
         return;
      end if;
      ---
      if not delete_address(root,
                            L_addr.key_value_1,
                            L_addr.seq_no,
                            L_addr.addr_type,
                            O_status,
                            O_text) then
         return;
      end if;
      ---
      if not API_LIBRARY.WRITE_DOCUMENT(O_status,
                                        O_text,
                                        O_msg,
                                        root) then
         return;
      end if;
   else
      L_doc_type := RMSMFM_SUPPLIER.VEND_ADDR_DESC_MSG;
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
        return;
      end if;
      ---
      if not add_address(root,
                         L_addr,
                         O_status,
                         O_text) then
        return;
      end if;
      ---
      if not API_LIBRARY.WRITE_DOCUMENT(O_status,
                                        O_text,
                                        O_msg,
                                        root) then
         return;
      end if;
   end if;
   ---
   O_status := API_CODES.SUCCESS;

EXCEPTION
   when OTHERS then
     API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.BUILD_ADDRESS');

END BUILD_ADDRESS;
--------------------------------------------------------------------------------
PROCEDURE BUILD_ORG_UNIT(I_pou      IN    PARTNER_ORG_UNIT%ROWTYPE,
                         I_event    IN    VARCHAR2,
                         O_msg      OUT   CLOB,
                         O_status   OUT   VARCHAR2,
                         O_text     OUT   VARCHAR2)
IS

   L_pou           PARTNER_ORG_UNIT%ROWTYPE       := NULL;
   root            XMLDOM.DOMELEMENT;

BEGIN
   -- Partner Org Unit modifications will not be published.
   -- No system interfaced using RIB is currently using primary_pay_site information.

   L_pou := I_pou;
   ---
   if I_event = 'D' then
      L_doc_type := RMSMFM_SUPPLIER.VEND_OU_REF_MSG;
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
         return;
      end if;
      ---
      if not delete_partner_org_unit(root,
                                     L_pou.org_unit_id,
                                     O_status,
                                     O_text) then
         return;
      end if;
      ---
      if not API_LIBRARY.WRITE_DOCUMENT(O_status,
                                        O_text,
                                        O_msg,
                                        root) then
         return;
      end if;
   else
      L_doc_type := RMSMFM_SUPPLIER.VEND_OU_DESC_MSG;
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
        return;
      end if;
      ---
      if not add_partner_org_unit(root,
                                  L_pou,
                                  O_status,
                                  O_text) then
        return;
      end if;
      ---
      if not API_LIBRARY.WRITE_DOCUMENT(O_status,
                                        O_text,
                                        O_msg,
                                        root) then
         return;
      end if;
   end if;
   ---
   O_status := API_CODES.SUCCESS;

EXCEPTION
   when OTHERS then
     API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.BUILD_ORG_UNIT');
END BUILD_ORG_UNIT;
--------------------------------------------------------------------------------
-- GET_KEYS - gets the return allow indicator.

FUNCTION GET_KEYS(O_error_message      OUT   VARCHAR2,
                  O_valid              OUT   BOOLEAN,
                  O_ret_allow_ind      OUT   VARCHAR2,
                  I_ret_allow_ind   IN       VARCHAR2,
                  I_supplier        IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_ret_allow_ind    SUPS.RET_ALLOW_IND%TYPE;

   cursor C_GET_RET_ALLOW_IND is
      select ret_allow_ind
        from sups
       where supplier = I_supplier;

BEGIN
   if I_ret_allow_ind is NULL then
      open C_GET_RET_ALLOW_IND;
      fetch C_GET_RET_ALLOW_IND into L_ret_allow_ind;
      close C_GET_RET_ALLOW_IND;
      O_ret_allow_ind := L_ret_allow_ind;
   else
      O_ret_allow_ind := I_ret_allow_ind;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPPLIER_XML.GET_KEYS',
                                            to_char(SQLCODE));
      return FALSE;
END GET_KEYS;


      /*** Private Program Bodies ***/
--------------------------------------------------------------------------------
FUNCTION ADD_SUPPLIER(root      IN OUT   XMLDOM.DOMELEMENT,
                     I_sup      IN       SUPS%ROWTYPE,
                     O_status      OUT   VARCHAR2,
                     O_text        OUT   VARCHAR2)
   RETURN BOOLEAN AS

BEGIN

   rib_xml.addElement( root, 'supplier', I_sup.SUPPLIER);
   rib_xml.addElement( root, 'sup_name', I_sup.SUP_NAME);
   rib_xml.addElement( root, 'sup_name_secondary', I_sup.SUP_NAME_SECONDARY);
   rib_xml.addElement( root, 'contact_name', I_sup.CONTACT_NAME);
   rib_xml.addElement( root, 'contact_phone', I_sup.CONTACT_PHONE);
   rib_xml.addElement( root, 'contact_fax', I_sup.CONTACT_FAX);
   rib_xml.addElement( root, 'contact_pager', I_sup.CONTACT_PAGER);
   rib_xml.addElement( root, 'sup_status', I_sup.SUP_STATUS);
   rib_xml.addElement( root, 'qc_ind', I_sup.QC_IND);
   rib_xml.addElement( root, 'qc_pct', I_sup.QC_PCT);
   rib_xml.addElement( root, 'qc_freq', I_sup.QC_FREQ);
   rib_xml.addElement( root, 'vc_ind', I_sup.VC_IND);
   rib_xml.addElement( root, 'vc_pct', I_sup.VC_PCT);
   rib_xml.addElement( root, 'vc_freq', I_sup.VC_FREQ);
   rib_xml.addElement( root, 'currency_code', I_sup.CURRENCY_CODE);
   rib_xml.addElement( root, 'lang', I_sup.LANG);
   rib_xml.addElement( root, 'terms', I_sup.TERMS);
   rib_xml.addElement( root, 'freight_terms', I_sup.FREIGHT_TERMS);
   rib_xml.addElement( root, 'ret_allow_ind', I_sup.RET_ALLOW_IND);
   rib_xml.addElement( root, 'ret_auth_req', I_sup.RET_AUTH_REQ);
   rib_xml.addElement( root, 'ret_min_dol_amt', I_sup.RET_MIN_DOL_AMT);
   rib_xml.addElement( root, 'ret_courier', I_sup.RET_COURIER);
   rib_xml.addElement( root, 'handling_pct', I_sup.HANDLING_PCT);
   rib_xml.addElement( root, 'edi_po_ind', I_sup.EDI_PO_IND);
   rib_xml.addElement( root, 'edi_po_chg', I_sup.EDI_PO_CHG);
   rib_xml.addElement( root, 'edi_po_confirm', I_sup.EDI_PO_CONFIRM);
   rib_xml.addElement( root, 'edi_asn', I_sup.EDI_ASN);
   rib_xml.addElement( root, 'edi_sales_rpt_freq', I_sup.EDI_SALES_RPT_FREQ);
   rib_xml.addElement( root, 'edi_supp_available_ind', I_sup.EDI_SUPP_AVAILABLE_IND);
   rib_xml.addElement( root, 'edi_contract_ind', I_sup.EDI_CONTRACT_IND);
   rib_xml.addElement( root, 'edi_invc_ind', I_sup.EDI_INVC_IND);
   rib_xml.addElement( root, 'cost_chg_pct_var', I_sup.COST_CHG_PCT_VAR);
   rib_xml.addElement( root, 'cost_chg_amt_var', I_sup.COST_CHG_AMT_VAR);
   rib_xml.addElement( root, 'replen_approval_ind', I_sup.REPLEN_APPROVAL_IND);
   rib_xml.addElement( root, 'ship_method', I_sup.SHIP_METHOD);
   rib_xml.addElement( root, 'payment_method', I_sup.PAYMENT_METHOD);
   rib_xml.addElement( root, 'contact_telex', I_sup.CONTACT_TELEX);
   rib_xml.addElement( root, 'contact_email', I_sup.CONTACT_EMAIL);
   rib_xml.addElement( root, 'settlement_code', I_sup.SETTLEMENT_CODE);
   rib_xml.addElement( root, 'pre_mark_ind', I_sup.PRE_MARK_IND);
   rib_xml.addElement( root, 'auto_appr_invc_ind', I_sup.AUTO_APPR_INVC_IND);
   rib_xml.addElement( root, 'dbt_memo_code', I_sup.DBT_MEMO_CODE);
   rib_xml.addElement( root, 'freight_charge_ind', I_sup.FREIGHT_CHARGE_IND);
   rib_xml.addElement( root, 'auto_appr_dbt_memo_ind', I_sup.AUTO_APPR_DBT_MEMO_IND);
   rib_xml.addElement( root, 'inv_mgmt_lvl', I_sup.INV_MGMT_LVL);
   rib_xml.addElement( root, 'backorder_ind', I_sup.BACKORDER_IND);
   rib_xml.addElement( root, 'vat_region', I_sup.VAT_REGION);
   rib_xml.addElement( root, 'prepay_invc_ind', I_sup.PREPAY_INVC_IND);
   rib_xml.addElement( root, 'service_perf_req_ind', I_sup.SERVICE_PERF_REQ_IND);
   rib_xml.addElement( root, 'invc_pay_loc', I_sup.INVC_PAY_LOC);
   rib_xml.addElement( root, 'invc_receive_loc', I_sup.INVC_RECEIVE_LOC);
   rib_xml.addElement( root, 'addinvc_gross_net', I_sup.ADDINVC_GROSS_NET);
   rib_xml.addElement( root, 'delivery_policy', I_sup.DELIVERY_POLICY);
   rib_xml.addElement( root, 'comment_desc', I_sup.COMMENT_DESC);
   rib_xml.addElement( root, 'default_item_lead_time', I_sup.DEFAULT_ITEM_LEAD_TIME);
   rib_xml.addElement( root, 'duns_number', I_sup.DUNS_NUMBER);
   rib_xml.addElement( root, 'duns_loc', I_sup.DUNS_LOC);
   rib_xml.addElement( root, 'bracket_costing_ind', I_sup.BRACKET_COSTING_IND);
   rib_xml.addElement( root, 'vmi_order_status', I_sup.VMI_ORDER_STATUS);
   rib_xml.addElement( root, 'dsd_supplier_ind', I_sup.DSD_IND);
   rib_xml.addElement( root, 'sup_qty_level', I_sup.SUP_QTY_LEVEL);
   rib_xml.addElement( root, 'supplier_parent', I_sup.SUPPLIER_PARENT);
   ---
   O_status := API_CODES.SUCCESS;
   ---
   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.ADD_SUPPLIER');
      return FALSE;

END ADD_SUPPLIER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SUPPLIER(root         IN OUT   XMLDOM.DOMELEMENT,
                         I_supplier   IN       SUPS.SUPPLIER%TYPE,
                         O_status        OUT   VARCHAR2,
                         O_text          OUT   VARCHAR2)
   RETURN BOOLEAN AS

BEGIN

   rib_xml.addElement( root, 'supplier', I_supplier);
   ---
   O_status := API_CODES.SUCCESS;
   ---
   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.DELETE_SUPPLIER');
      return FALSE;

END DELETE_SUPPLIER;
--------------------------------------------------------------------------------
FUNCTION ADD_ADDRESS(root       IN OUT   XMLDOM.DOMELEMENT,
                     I_addr     IN       ADDR%ROWTYPE,
                     O_status      OUT   VARCHAR2,
                     O_text        OUT   VARCHAR2)
   RETURN BOOLEAN AS

BEGIN

   rib_xml.addElement( root, 'module', I_addr.MODULE);
   rib_xml.addElement( root, 'key_value_1', I_addr.KEY_VALUE_1);
   rib_xml.addElement( root, 'key_value_2', I_addr.KEY_VALUE_2);
   rib_xml.addElement( root, 'seq_no', I_addr.SEQ_NO);
   rib_xml.addElement( root, 'addr_type', I_addr.ADDR_TYPE);
   rib_xml.addElement( root, 'primary_addr_ind', I_addr.PRIMARY_ADDR_IND);
   rib_xml.addElement( root, 'add_1', I_addr.ADD_1);
   rib_xml.addElement( root, 'add_2', I_addr.ADD_2);
   rib_xml.addElement( root, 'add_3', I_addr.ADD_3);
   rib_xml.addElement( root, 'city', I_addr.CITY);
   rib_xml.addElement( root, 'state', I_addr.STATE);
   rib_xml.addElement( root, 'country_id', I_addr.COUNTRY_ID);
   rib_xml.addElement( root, 'post', I_addr.POST);
   rib_xml.addElement( root, 'contact_name', I_addr.CONTACT_NAME);
   rib_xml.addElement( root, 'contact_phone', I_addr.CONTACT_PHONE);
   rib_xml.addElement( root, 'contact_telex', I_addr.CONTACT_TELEX);
   rib_xml.addElement( root, 'contact_fax', I_addr.CONTACT_FAX);
   rib_xml.addElement( root, 'contact_email', I_addr.CONTACT_EMAIL);
   rib_xml.addElement( root, 'oracle_vendor_site_id', I_addr.ORACLE_VENDOR_SITE_ID);
   ---
   O_status := API_CODES.SUCCESS;
   ---
   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.ADD_ADDRESS');
      return FALSE;

END ADD_ADDRESS;
--------------------------------------------------------------------------------
FUNCTION DELETE_ADDRESS(root          IN OUT   XMLDOM.DOMELEMENT,
                        I_supplier    IN       SUPS.SUPPLIER%TYPE,
                        I_seq_no      IN       ADDR.SEQ_NO%TYPE,
                        I_addr_type   IN       ADDR.ADDR_TYPE%TYPE,
                        O_status         OUT   VARCHAR2,
                        O_text           OUT   VARCHAR2)
   RETURN BOOLEAN AS

BEGIN

   rib_xml.addElement( root, 'supplier', I_supplier);
   rib_xml.addElement( root, 'seq_no', I_seq_no);
   rib_xml.addElement( root, 'addr_type', I_addr_type);
   ---
   O_status := API_CODES.SUCCESS;
   ---
   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.DELETE_ADDRESS');
      return FALSE;

END DELETE_ADDRESS;
--------------------------------------------------------------------------------
FUNCTION ADD_PARTNER_ORG_UNIT(root       IN OUT   XMLDOM.DOMELEMENT,
                              I_pou      IN       PARTNER_ORG_UNIT%ROWTYPE,
                              O_status      OUT   VARCHAR2,
                              O_text        OUT   VARCHAR2)
   RETURN BOOLEAN AS

BEGIN

   rib_xml.addElement( root, 'org_unit_id', I_pou.ORG_UNIT_ID);
   rib_xml.addElement( root, 'primary_pay_site_ind', I_pou.PRIMARY_PAY_SITE);

   ---
   O_status := API_CODES.SUCCESS;
   ---
   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.ADD_PARTNER_ORG_UNIT');
      return FALSE;

END ADD_PARTNER_ORG_UNIT;
--------------------------------------------------------------------------------
FUNCTION DELETE_PARTNER_ORG_UNIT(root          IN OUT   XMLDOM.DOMELEMENT,
                                 I_org_unit    IN      PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE,
                                 O_status         OUT   VARCHAR2,
                                 O_text           OUT   VARCHAR2)
   RETURN BOOLEAN AS

BEGIN

   rib_xml.addElement( root, 'org_unit_id', I_org_unit);
   ---
   O_status := API_CODES.SUCCESS;
   ---
   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'SUPPLIER_XML.DELETE_PARTNER_ORG_UNIT');
      return FALSE;

END DELETE_PARTNER_ORG_UNIT;
--------------------------------------------------------------------------------
END SUPPLIER_XML;
/