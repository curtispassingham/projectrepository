CREATE OR REPLACE PACKAGE CORESVC_COMP_PRICE AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
   template_key            CONSTANT VARCHAR2(255)         := 'COMP_PRICE_DATA';
   template_category                CODE_DETAIL.CODE%TYPE := 'RMSPCO';
   action_column                    VARCHAR2(255)         := 'ACTION';
   action_new                       VARCHAR2(25)          := 'NEW';
   action_mod                       VARCHAR2(25)          := 'MOD';
   action_del                       VARCHAR2(25)          := 'DEL';
   COMP_SHOP_LIST_sheet             VARCHAR2(255)         := 'COMP_SHOP_LIST';
   COMP_SHOP_LIST$Action            NUMBER                := 1;
   COMP_SHOP_LIST$SHOPPER           NUMBER                := 2;
   COMP_SHOP_LIST$SHOP_DATE         NUMBER                := 3;
   COMP_SHOP_LIST$ITEM              NUMBER                := 4;
   COMP_SHOP_LIST$ITEM_DESC         NUMBER                := 5;
   COMP_SHOP_LIST$REF_ITEM          NUMBER                := 6;
   COMP_SHOP_LIST$COMPETITOR        NUMBER                := 7;
   COMP_SHOP_LIST$COMP_NAME         NUMBER                := 8;
   COMP_SHOP_LIST$COMP_STORE        NUMBER                := 9;
   COMP_SHOP_LIST$COMP_STORE_NAME   NUMBER                := 10;
   COMP_SHOP_LIST$REC_DATE          NUMBER                := 11;
   COMP_SHOP_LIST$COMP_RETAIL       NUMBER                := 12;
   COMP_SHOP_LIST$COMP_RTL_TYP      NUMBER                := 13;
   COMP_SHOP_LIST$MULTI_UNITS       NUMBER                := 14;
   COMP_SHOP_LIST$MULTI_UNIT_RTL    NUMBER                := 15;
   COMP_SHOP_LIST$PROM_START_DATE   NUMBER                := 16;
   COMP_SHOP_LIST$PROM_END_DATE     NUMBER                := 17;
   COMP_SHOP_LIST$OFFER_TYPE        NUMBER                := 18;
   sheet_name_trans                 S9T_PKG.TRANS_MAP_TYP;
   TYPE COMP_SHOP_LIST_rec_tab IS TABLE OF COMP_SHOP_LIST%ROWTYPE;

   COMP_PRICE_HIST_sheet            VARCHAR2(255)         := 'COMP_PRICE_HIST';
   COMP_PRICE_HIST$Action           NUMBER                := 1;
   COMP_PRICE_HIST$REC_DATE         NUMBER                := 5;
   COMP_PRICE_HIST$COMP_RETAIL      NUMBER                := 6;
   COMP_PRICE_HIST$COMP_RTL_TYP     NUMBER                := 7;
   COMP_PRICE_HIST$MULTI_UNITS      NUMBER                := 8;
   COMP_PRICE_HIST$MULTI_UNIT_RTL   NUMBER                := 9;
   COMP_PRICE_HIST$PROM_STRT_DATE   NUMBER                := 10;
   COMP_PRICE_HIST$PROM_END_DATE    NUMBER                := 11;
   COMP_PRICE_HIST$OFFER_TYPE       NUMBER                := 12;
   COMP_PRICE_HIST$ITEM             NUMBER                := 2;
   COMP_PRICE_HIST$REF_ITEM         NUMBER                := 3;
   COMP_PRICE_HIST$COMP_STORE       NUMBER                := 4;
   TYPE COMP_PRICE_HIST_rec_tab IS TABLE OF COMP_PRICE_HIST%ROWTYPE;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,                         
					      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)               
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,                   
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)    
return VARCHAR2;
----------------------------------------------------------------------------------------------
END CORESVC_COMP_PRICE;
/