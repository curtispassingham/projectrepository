
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUPP_PREISSUE_SQL AUTHID CURRENT_USER AS 
-------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists        IN OUT BOOLEAN,
                           I_supplier      IN     SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION APPLY_SUPP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_supplier      IN     SUPP_PREISSUE.SUPPLIER%TYPE,
                    I_qty           IN     SUPP_PREISSUE.QTY%TYPE,
                    I_expiry_days   IN     SUPP_PREISSUE.EXPIRY_DAYS%TYPE,
                    I_frequency     IN     SUPP_PREISSUE.FREQUENCY%TYPE,
                    I_next_gen_date IN     SUPP_PREISSUE.NEXT_GEN_DATE%TYPE,
                    I_create_id     IN     SUPP_PREISSUE.CREATE_ID%TYPE,
                    I_exists        IN     BOOLEAN)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_SUPP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_supplier      IN OUT ORD_PREISSUE.SUPPLIER%TYPE,
                  I_order_no      IN     ORD_PREISSUE.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ORDSUPP_FILTER_LIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_diff_ind        IN OUT VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
END;
/
