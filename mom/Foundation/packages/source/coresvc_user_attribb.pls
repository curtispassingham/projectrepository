create or replace PACKAGE BODY CORESVC_USER_ATTRIB AS
   cursor C_SVC_USER_ATTRIB(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_user_attrib.rowid  AS pk_user_attrib_rid,
             st.rowid AS st_rid,
             uat_lan_fk.rowid    AS uat_lan_fk_rid,
             st.user_id,
             st.user_name,
             st.lang,
             st.store_default,
             st.user_phone,
             st.user_fax,
             st.user_pager,
             st.user_email,
             st.default_printer,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_user_attrib st,
             user_attrib pk_user_attrib,
             lang uat_lan_fk,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.user_id         = pk_user_attrib.user_id (+)
         and st.lang        = uat_lan_fk.lang (+)
;
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   USER_ATTRIB_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                             :=s9t_pkg.get_sheet_names(I_file_id);
   USER_ATTRIB_cols                     :=s9t_pkg.get_col_names(I_file_id,USER_ATTRIB_sheet);
   USER_ATTRIB$Action                   := USER_ATTRIB_cols('ACTION');
   USER_ATTRIB$USER_ID                  := USER_ATTRIB_cols('USER_ID');
   USER_ATTRIB$USER_NAME                := USER_ATTRIB_cols('USER_NAME');
   USER_ATTRIB$LANG                     := USER_ATTRIB_cols('LANG');
   USER_ATTRIB$STORE_DEFAULT            := USER_ATTRIB_cols('STORE_DEFAULT');
   USER_ATTRIB$USER_PHONE               := USER_ATTRIB_cols('USER_PHONE');
   USER_ATTRIB$USER_FAX                 := USER_ATTRIB_cols('USER_FAX');
   USER_ATTRIB$USER_PAGER               := USER_ATTRIB_cols('USER_PAGER');
   USER_ATTRIB$USER_EMAIL               := USER_ATTRIB_cols('USER_EMAIL');
   USER_ATTRIB$DEFAULT_PRINTER          := USER_ATTRIB_cols('DEFAULT_PRINTER');
END POPULATE_NAMES;
-----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_USER_ATTRIB( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = USER_ATTRIB_sheet )
   select s9t_row(s9t_cells(CORESVC_USER_ATTRIB.action_mod ,
                           user_id,
                           user_name,
                           lang,
                           store_default,
                           user_phone,
                           user_fax,
                           user_pager,
                           user_email,
                           default_printer
                           ))
     from user_attrib ;
END POPULATE_USER_ATTRIB;
--------------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
   cursor C_USER_LANG is
      select lang
        from user_attrib
       where user_id = get_user;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(USER_ATTRIB_sheet);
   L_file.sheets(l_file.get_sheet_index(USER_ATTRIB_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'USER_ID'
                                                                                            ,'USER_NAME'
                                                                                            ,'LANG'
                                                                                            ,'STORE_DEFAULT'
                                                                                            ,'USER_PHONE'
                                                                                            ,'USER_FAX'
                                                                                            ,'USER_PAGER'
                                                                                            ,'USER_EMAIL'
                                                                                            ,'DEFAULT_PRINTER'
);
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
---------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_USER_ATTRIB.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_USER_ATTRIB(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_USER_ATTRIB( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_USER_ATTRIB.process_id%TYPE) IS
   TYPE svc_USER_ATTRIB_col_typ IS TABLE OF SVC_USER_ATTRIB%ROWTYPE;
   L_temp_rec SVC_USER_ATTRIB%ROWTYPE;
   svc_USER_ATTRIB_col svc_USER_ATTRIB_col_typ :=NEW svc_USER_ATTRIB_col_typ();
   L_process_id SVC_USER_ATTRIB.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_USER_ATTRIB%ROWTYPE;
      L_pk_columns    VARCHAR2(255)  := 'User Id';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
 
   cursor C_MANDATORY_IND is
      select
             USER_ID_mi,
             USER_NAME_mi,
             LANG_mi,
             STORE_DEFAULT_mi,
             USER_PHONE_mi,
             USER_FAX_mi,
             USER_PAGER_mi,
             USER_EMAIL_mi,
             DEFAULT_PRINTER_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'USER_ATTRIB'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'USER_ID' AS USER_ID,
                                         'USER_NAME' AS USER_NAME,
                                         'LANG' AS LANG,
                                         'STORE_DEFAULT' AS STORE_DEFAULT,
                                         'USER_PHONE' AS USER_PHONE,
                                         'USER_FAX' AS USER_FAX,
                                         'USER_PAGER' AS USER_PAGER,
                                         'USER_EMAIL' AS USER_EMAIL,
                                         'DEFAULT_PRINTER' AS DEFAULT_PRINTER,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       USER_ID_dv,
                       USER_NAME_dv,
                       LANG_dv,
                       STORE_DEFAULT_dv,
                       USER_PHONE_dv,
                       USER_FAX_dv,
                       USER_PAGER_dv,
                       USER_EMAIL_dv,
                       DEFAULT_PRINTER_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'USER_ATTRIB'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'USER_ID' AS USER_ID,
                                                      'USER_NAME' AS USER_NAME,
                                                      'LANG' AS LANG,
                                                      'STORE_DEFAULT' AS STORE_DEFAULT,
                                                      'USER_PHONE' AS USER_PHONE,
                                                      'USER_FAX' AS USER_FAX,
                                                      'USER_PAGER' AS USER_PAGER,
                                                      'USER_EMAIL' AS USER_EMAIL,
                                                      'DEFAULT_PRINTER' AS DEFAULT_PRINTER,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.USER_ID := rec.USER_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'USER_ID ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.USER_NAME := rec.USER_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'USER_NAME ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'LANG ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.STORE_DEFAULT := rec.STORE_DEFAULT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'STORE_DEFAULT ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.USER_PHONE := rec.USER_PHONE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'USER_PHONE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.USER_FAX := rec.USER_FAX_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'USER_FAX ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.USER_PAGER := rec.USER_PAGER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'USER_PAGER ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.USER_EMAIL := rec.USER_EMAIL_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'USER_EMAIL ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DEFAULT_PRINTER := rec.DEFAULT_PRINTER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'USER_ATTRIB ' ,
                            NULL,
                           'DEFAULT_PRINTER ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(USER_ATTRIB$Action)                 AS Action,
          r.get_cell(USER_ATTRIB$USER_ID)                AS USER_ID,
          r.get_cell(USER_ATTRIB$USER_NAME)              AS USER_NAME,
          r.get_cell(USER_ATTRIB$LANG)                   AS LANG,
          r.get_cell(USER_ATTRIB$STORE_DEFAULT)          AS STORE_DEFAULT,
          r.get_cell(USER_ATTRIB$USER_PHONE)             AS USER_PHONE,
          r.get_cell(USER_ATTRIB$USER_FAX)               AS USER_FAX,
          r.get_cell(USER_ATTRIB$USER_PAGER)             AS USER_PAGER,
          r.get_cell(USER_ATTRIB$USER_EMAIL)             AS USER_EMAIL,
          r.get_cell(USER_ATTRIB$DEFAULT_PRINTER)        AS DEFAULT_PRINTER,
          r.get_row_seq()                                AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(USER_ATTRIB_sheet)
  )
   LOOP
      L_temp_rec := null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.USER_ID := rec.USER_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'USER_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.USER_NAME := rec.USER_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'USER_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.STORE_DEFAULT := rec.STORE_DEFAULT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'STORE_DEFAULT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.USER_PHONE := rec.USER_PHONE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'USER_PHONE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.USER_FAX := rec.USER_FAX;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'USER_FAX',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.USER_PAGER := rec.USER_PAGER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'USER_PAGER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.USER_EMAIL := rec.USER_EMAIL;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'USER_EMAIL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEFAULT_PRINTER := rec.DEFAULT_PRINTER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
USER_ATTRIB_sheet,
                            rec.row_seq,
                            'DEFAULT_PRINTER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_USER_ATTRIB.action_new then
         L_temp_rec.USER_ID := NVL( L_temp_rec.USER_ID,L_default_rec.USER_ID);
         L_temp_rec.USER_NAME := NVL( L_temp_rec.USER_NAME,L_default_rec.USER_NAME);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.STORE_DEFAULT := NVL( L_temp_rec.STORE_DEFAULT,L_default_rec.STORE_DEFAULT);
         L_temp_rec.USER_PHONE := NVL( L_temp_rec.USER_PHONE,L_default_rec.USER_PHONE);
         L_temp_rec.USER_FAX := NVL( L_temp_rec.USER_FAX,L_default_rec.USER_FAX);
         L_temp_rec.USER_PAGER := NVL( L_temp_rec.USER_PAGER,L_default_rec.USER_PAGER);
         L_temp_rec.USER_EMAIL := NVL( L_temp_rec.USER_EMAIL,L_default_rec.USER_EMAIL);
         L_temp_rec.DEFAULT_PRINTER := NVL( L_temp_rec.DEFAULT_PRINTER,L_default_rec.DEFAULT_PRINTER);
      end if;
      if not (
            L_temp_rec.USER_ID is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         USER_ATTRIB_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;

      end if;
      if NOT L_error then
         svc_USER_ATTRIB_col.extend();
         svc_USER_ATTRIB_col(svc_USER_ATTRIB_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_USER_ATTRIB_col.COUNT SAVE EXCEPTIONS
      merge into SVC_USER_ATTRIB st
      using(select
                  (case
                   when l_mi_rec.USER_ID_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.USER_ID IS NULL
                   then mt.USER_ID
                   else s1.USER_ID
                   end) AS USER_ID,
                  (case
                   when l_mi_rec.USER_NAME_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.USER_NAME IS NULL
                   then mt.USER_NAME
                   else s1.USER_NAME
                   end) AS USER_NAME,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  (case
                   when l_mi_rec.STORE_DEFAULT_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.STORE_DEFAULT IS NULL
                   then mt.STORE_DEFAULT
                   else s1.STORE_DEFAULT
                   end) AS STORE_DEFAULT,
                  (case
                   when l_mi_rec.USER_PHONE_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.USER_PHONE IS NULL
                   then mt.USER_PHONE
                   else s1.USER_PHONE
                   end) AS USER_PHONE,
                  (case
                   when l_mi_rec.USER_FAX_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.USER_FAX IS NULL
                   then mt.USER_FAX
                   else s1.USER_FAX
                   end) AS USER_FAX,
                  (case
                   when l_mi_rec.USER_PAGER_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.USER_PAGER IS NULL
                   then mt.USER_PAGER
                   else s1.USER_PAGER
                   end) AS USER_PAGER,
                  (case
                   when l_mi_rec.USER_EMAIL_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.USER_EMAIL IS NULL
                   then mt.USER_EMAIL
                   else s1.USER_EMAIL
                   end) AS USER_EMAIL,
                  (case
                   when l_mi_rec.DEFAULT_PRINTER_mi    = 'N'
                    and svc_USER_ATTRIB_col(i).action = CORESVC_USER_ATTRIB.action_mod
                    and s1.DEFAULT_PRINTER IS NULL
                   then mt.DEFAULT_PRINTER
                   else s1.DEFAULT_PRINTER
                   end) AS DEFAULT_PRINTER,
                  null as dummy
              from (select
                          svc_USER_ATTRIB_col(i).USER_ID AS USER_ID,
                          svc_USER_ATTRIB_col(i).USER_NAME AS USER_NAME,
                          svc_USER_ATTRIB_col(i).LANG AS LANG,
                          svc_USER_ATTRIB_col(i).STORE_DEFAULT AS STORE_DEFAULT,
                          svc_USER_ATTRIB_col(i).USER_PHONE AS USER_PHONE,
                          svc_USER_ATTRIB_col(i).USER_FAX AS USER_FAX,
                          svc_USER_ATTRIB_col(i).USER_PAGER AS USER_PAGER,
                          svc_USER_ATTRIB_col(i).USER_EMAIL AS USER_EMAIL,
                          svc_USER_ATTRIB_col(i).DEFAULT_PRINTER AS DEFAULT_PRINTER,
                          null as dummy
                      from dual ) s1,
            USER_ATTRIB mt
             where
                  mt.USER_ID (+)     = s1.USER_ID   and
                  1 = 1 )sq
                on (
                    st.USER_ID      = sq.USER_ID and
                    svc_USER_ATTRIB_col(i).ACTION IN (CORESVC_USER_ATTRIB.action_mod,CORESVC_USER_ATTRIB.action_del))
      when matched then
      update
         set process_id      = svc_USER_ATTRIB_col(i).process_id ,
             chunk_id        = svc_USER_ATTRIB_col(i).chunk_id ,
             row_seq         = svc_USER_ATTRIB_col(i).row_seq ,
             action          = svc_USER_ATTRIB_col(i).action ,
             process$status  = svc_USER_ATTRIB_col(i).process$status ,
             lang              = sq.lang ,
             default_printer              = sq.default_printer ,
             user_fax              = sq.user_fax ,
             user_email              = sq.user_email ,
             user_phone              = sq.user_phone ,
             user_name              = sq.user_name ,
             store_default              = sq.store_default ,
             user_pager              = sq.user_pager ,
             create_id       = svc_USER_ATTRIB_col(i).create_id ,
             create_datetime = svc_USER_ATTRIB_col(i).create_datetime ,
             last_upd_id     = svc_USER_ATTRIB_col(i).last_upd_id ,
             last_upd_datetime = svc_USER_ATTRIB_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             user_id ,
             user_name ,
             lang ,
             store_default ,
             user_phone ,
             user_fax ,
             user_pager ,
             user_email ,
             default_printer ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_USER_ATTRIB_col(i).process_id ,
             svc_USER_ATTRIB_col(i).chunk_id ,
             svc_USER_ATTRIB_col(i).row_seq ,
             svc_USER_ATTRIB_col(i).action ,
             svc_USER_ATTRIB_col(i).process$status ,
             sq.user_id ,
             sq.user_name ,
             sq.lang ,
             sq.store_default ,
             sq.user_phone ,
             sq.user_fax ,
             sq.user_pager ,
             sq.user_email ,
             sq.default_printer ,
             svc_USER_ATTRIB_col(i).create_id ,
             svc_USER_ATTRIB_col(i).create_datetime ,
             svc_USER_ATTRIB_col(i).last_upd_id ,
             svc_USER_ATTRIB_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            USER_ATTRIB_sheet,
                            svc_USER_ATTRIB_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;
END PROCESS_S9T_USER_ATTRIB;
------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER
                      )
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_USER_ATTRIB.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_file(I_file_id,TRUE);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_USER_ATTRIB(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION POST_EXEC_USER_ATTRIB_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_user_attrib_temp_rec     IN       C_SVC_USER_ATTRIB%ROWTYPE)
RETURN BOOLEAN is
   L_program VARCHAR2(255):= 'CORESVC_USER_ATTRIB.EXEC_USER_ATTRIB_DEL';
   L_table   VARCHAR2(255):= 'SVC_USER_ATTRIB';
BEGIN

	if not LOC_PROD_SECURITY_SQL.INSERT_SEC_USER(O_error_message,
  	                                             I_user_attrib_temp_rec.user_id) then
	   WRITE_ERROR(I_user_attrib_temp_rec.process_id,
				   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
				   I_user_attrib_temp_rec.chunk_id,
				   L_table,
				   I_user_attrib_temp_rec.row_seq,
				   'USER_ID',
				   O_error_message);
    end if;
    return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END POST_EXEC_USER_ATTRIB_INS;
-----------------------------------------------------------------------------------
FUNCTION EXEC_USER_ATTRIB_INS(  L_user_attrib_temp_rec   IN       USER_ATTRIB%ROWTYPE,
                                O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rec                    IN       C_SVC_USER_ATTRIB%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_USER_ATTRIB.EXEC_USER_ATTRIB_INS';
   L_table   VARCHAR2(255):= 'SVC_USER_ATTRIB';
BEGIN
   SAVEPOINT USER_ATTRIB_INS;

   insert
     into user_attrib
   values L_user_attrib_temp_rec;

   if POST_EXEC_USER_ATTRIB_INS(O_error_message,
							                  I_rec) = false then
	   WRITE_ERROR(I_rec.process_id,
				   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
				   I_rec.chunk_id,
				   L_table,
				   I_rec.row_seq,
				   'USER_ID',
				   O_error_message);
				   ROLLBACK TO SAVEPOINT USER_ATTRIB_INS;
	   return false;			  						   
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_USER_ATTRIB_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_USER_ATTRIB_UPD( L_user_attrib_temp_rec   IN   USER_ATTRIB%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_USER_ATTRIB.EXEC_USER_ATTRIB_UPD';
   L_table   VARCHAR2(255):= 'SVC_USER_ATTRIB';
BEGIN
   update user_attrib
      set row = L_user_attrib_temp_rec
    where 1 = 1
      and user_id = L_user_attrib_temp_rec.user_id
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_USER_ATTRIB_UPD;
------------------------------------------------------------------------------------------------------
FUNCTION PRE_EXEC_USER_ATTRIB_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_user_attrib_temp_rec     IN       C_SVC_USER_ATTRIB%ROWTYPE)
RETURN BOOLEAN is
   L_program VARCHAR2(255):= 'CORESVC_USER_ATTRIB.EXEC_USER_ATTRIB_DEL';
   L_table   VARCHAR2(255):= 'SVC_USER_ATTRIB';
BEGIN

	if not LOC_PROD_SECURITY_SQL.DELETE_USR_SEC(O_error_message,
  	                                            I_user_attrib_temp_rec.user_id) then
	   WRITE_ERROR(I_user_attrib_temp_rec.process_id,
				   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
				   I_user_attrib_temp_rec.chunk_id,
				   L_table,
				   I_user_attrib_temp_rec.row_seq,
				   'USER_ID',
				   O_error_message);
    end if;
    return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END PRE_EXEC_USER_ATTRIB_DEL;
-----------------------------------------------------------------------------------
FUNCTION EXEC_USER_ATTRIB_DEL(  L_user_attrib_temp_rec   IN       USER_ATTRIB%ROWTYPE ,
                                O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
								                I_rec                    IN       C_SVC_USER_ATTRIB%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_USER_ATTRIB.EXEC_USER_ATTRIB_DEL';
   L_table   VARCHAR2(255):= 'SVC_USER_ATTRIB';
BEGIN
   if not LOC_PROD_SECURITY_SQL.LOCK_USR_SEC(O_error_message,
      	                                     I_rec.user_id) then 
	   WRITE_ERROR(I_rec.process_id,
				   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
				   I_rec.chunk_id,
				   L_table,
				   I_rec.row_seq,
				   'USER_ID',
				   O_error_message);
	   return false;		
											 
   end if;											 

   if PRE_EXEC_USER_ATTRIB_DEL(O_error_message,
							                 I_rec) = false then
	   WRITE_ERROR(I_rec.process_id,
				   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
				   I_rec.chunk_id,
				   L_table,
				   I_rec.row_seq,
				   'USER_ID',
				   O_error_message);
	   return false;		
	  						   
   end if;
   
   delete
     from user_attrib
    where 1 = 1
      and user_id = L_user_attrib_temp_rec.user_id;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_USER_ATTRIB_DEL;
-------------------------------------------------------------------------------
FUNCTION PROCESS_USER_ATTRIB( I_process_id   IN   SVC_USER_ATTRIB.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_USER_ATTRIB.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_USER_ATTRIB.PROCESS_USER_ATTRIB';
   L_USER_ATTRIB_temp_rec USER_ATTRIB%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_USER_ATTRIB';
   L_error_message   VARCHAR2(600);
   L_exist           BOOLEAN;
   L_default         USER_ATTRIB.STORE_DEFAULT%TYPE;
   L_store           STORE.STORE%TYPE;
   L_store_name      varchar2(255);
   L_valid           BOOLEAN := FALSE;
   L_printer_desc    varchar2(255); 
   L_active_ind      varchar2(1);
   L_user            varchar2(1);
   
   CURSOR C_default_store(I_user_id user_attrib.user_id%type) IS
      select store_default 
        from user_attrib
       where user_id = I_user_id;

   CURSOR C_store_check IS
      select '1'
        from store
       where store = L_default;
   cursor C_USER_EXIST(I_user_id user_attrib.user_id%type) is
     select 'Y'
       from user_attrib
      where user_id = I_user_id;

BEGIN
   FOR rec IN c_svc_USER_ATTRIB(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      ---------------------------Duplicate check
      open C_USER_EXIST(rec.user_id);
      fetch C_USER_EXIST into L_user;
      close C_USER_EXIST;
      if rec.action = action_new then
         if L_user = 'Y' then       
   			 WRITE_ERROR(I_process_id,
        						 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
        						 I_chunk_id,
        						 L_table,
        						 rec.row_seq,
        						 'USER_ID',
        						 'USER_EXISTS');
			L_error :=TRUE;			 
		 end if;
		 L_exist:=null;
		 --------------------------------------------If user does not exists in all_user table
		 if not LOC_PROD_SECURITY_SQL.NEW_USER(L_error_message,
   	                                           L_exist,
   	                                           rec.user_id) then
				
			WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'USER_ID',
                     L_error_message);
			L_error :=TRUE;									 											   
		 else
			 if L_exist = FALSE then
				WRITE_ERROR(I_process_id,
							 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
							 I_chunk_id,
							 L_table,
							 rec.row_seq,
							 'USER_ID',
							 'INV_USER_ID');
				L_error :=TRUE;			
			 end if;
			 
		 end if;	
		 
	  end if;
      --------------------------------Invalid check
      L_user:=null;
      open C_USER_EXIST(rec.user_id);
      fetch C_USER_EXIST into L_user;
      close C_USER_EXIST;
      
      if rec.action IN (action_mod,action_del) then
         if nvl(L_user,'-1') <> 'Y' then
    				WRITE_ERROR(I_process_id,
        							 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
        							 I_chunk_id,
        							 L_table,
        							 rec.row_seq,
        							 'USER_ID',
        							 'INV_USER_ID');
        		L_error :=TRUE;			
			 end if;
			 open C_default_store(rec.user_id);
       fetch C_default_store into L_default;
   	   close C_default_store;
			 
   	         if L_default is not NULL then
   	        	 open C_store_check;
   	        	 fetch C_store_check into L_store;
               if C_store_check%NOTFOUND then
                  close C_store_check;
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table,
                               rec.row_seq,
                               'USER_ID',
                               'USER_RECORD');
                   L_error :=TRUE;
               end if;
   	        	 close C_store_check;  	

   	         end if;
		 end if;		 

	   if rec.lang is not null
	     and rec.uat_lan_fk_rid is NULL then         
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                    'LANG_EXIST');
         L_error :=TRUE;
      end if;
	  
	  if rec.store_default is not null then
		 if not STORE_ATTRIB_SQL.GET_NAME(L_error_message,
     	                                rec.store_default,
  	                                  L_store_name) then
			 WRITE_ERROR(I_process_id,
						 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
						 I_chunk_id,
						 L_table,
						 rec.row_seq,
						 'STORE_DEFAULT',
						 L_error_message);
			 L_error :=TRUE;
         else
		    L_store_name:= null;
			if((FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(L_error_message,
                                                 L_valid,
   	  	                                         L_store_name,
   	  	                                         rec.store_default) = FALSE)
				 or (L_valid = FALSE)) then
				 WRITE_ERROR(I_process_id,
							 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
							 I_chunk_id,
							 L_table,
							 rec.row_seq,
							 'STORE_DEFAULT',
							 L_error_message);
				 L_error :=TRUE;			   
            end if;  
		 end if;	  
	  end if;
	  
    if NOT(  rec.USER_NAME  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'USER_NAME',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
    if NOT(  rec.LANG  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                    'LANG_IS_NULL');
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_user_attrib_temp_rec.default_printer              := rec.default_printer;
         L_user_attrib_temp_rec.user_email                   := rec.user_email;
         L_user_attrib_temp_rec.user_pager                   := rec.user_pager;
         L_user_attrib_temp_rec.user_fax                     := rec.user_fax;
         L_user_attrib_temp_rec.user_phone                   := rec.user_phone;
         L_user_attrib_temp_rec.store_default                := rec.store_default;
         L_user_attrib_temp_rec.lang                         := rec.lang;
         L_user_attrib_temp_rec.user_name                    := rec.user_name;
         L_user_attrib_temp_rec.user_id                      := rec.user_id;
         L_user_attrib_temp_rec.create_id                    := USER;
         L_user_attrib_temp_rec.create_datetime              := SYSDATE;

         if rec.action = action_new then
            if EXEC_USER_ATTRIB_INS(   L_user_attrib_temp_rec,
                                             L_error_message,
                                             rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_USER_ATTRIB_UPD( L_user_attrib_temp_rec,
                                             L_error_message
                                             )=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_USER_ATTRIB_DEL( L_user_attrib_temp_rec,
                                             L_error_message,rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_user_attrib st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_user_attrib st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_user_attrib st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_default_store%ISOPEN then
	     close C_default_store;
	  end if;
	  
	  if C_store_check%ISOPEN then
	     close C_store_check;
	  end if;
	  
	  if c_svc_USER_ATTRIB%ISOPEN then
	     close c_svc_USER_ATTRIB;
	  end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_USER_ATTRIB;
-------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete 
     from svc_user_attrib 
    where process_id=I_process_id;
END;

-------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_USER_ATTRIB.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_USER_ATTRIB(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   commit;
   return TRUE;
EXCEPTION
   when OTHERS then
   if c_get_err_count%ISOPEN then
      close c_get_err_count;
   end if;
   if c_get_warn_count%ISOPEN then
      close c_get_warn_count;
   end if;
   CLEAR_STAGING_DATA(I_process_id);

   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_USER_ATTRIB;
/
