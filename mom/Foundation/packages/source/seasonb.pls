CREATE OR REPLACE PACKAGE BODY SEASON_SQL AS
--------------------------------------------------------------------
FUNCTION NEXT_SEASON( O_error_message  IN OUT  VARCHAR2,
                      O_season_id      IN OUT  SEASONS.SEASON_ID%TYPE)
                   RETURN BOOLEAN IS

   L_wrap_sequence_number   SEASONS.SEASON_ID%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);


   cursor C_SEASON_ID_EXISTS is
       select 'x'
         from seasons
        where season_id = O_season_id;

BEGIN
   LOOP
      SQL_LIB.SET_MARK('SELECT',
                       NULL,
                       'SYS.DUAL',
                       NULL);

      select SEASON_SEQUENCE.NEXTVAL
        into O_season_id
        from sys.dual;

      if L_first_time = 'Yes' then
          L_wrap_sequence_number := O_season_id;
          L_first_time := 'No';
      elsif O_season_id = L_wrap_sequence_number then
         O_error_message := 'NO_SEASON_NUMBERS';
         RETURN FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN',
                       'C_SEASON_ID_EXISTS',
                       'SEASONS',
                       'SEASON ID: ' || to_char(O_season_id));

      open  C_SEASON_ID_EXISTS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_SEASON_ID_EXISTS',
                       'SEASONS',
                       'SEASON ID: ' || to_char(O_season_id));

      fetch C_SEASON_ID_EXISTS into L_dummy;

      if C_SEASON_ID_EXISTS%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_SEASON_ID_EXISTS',
                          'SEASONS',
                          'SEASON ID: ' || to_char(O_season_id));

          close C_SEASON_ID_EXISTS;
          RETURN TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_SEASON_ID_EXISTS',
                       'SEASONS',
                       'SEASON ID: ' || to_char(O_season_id));

      close C_SEASON_ID_EXISTS;
   END LOOP;

   RETURN FALSE;

EXCEPTION
    WHEN OTHERS THEN
       O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                              SQLERRM,
                                              'SEASON_SQL.NEXT_SEASON',
                                              to_char(SQLCODE));

       RETURN FALSE;

END NEXT_SEASON;

---------------------------------------------------------------------------------------------
FUNCTION GET_SEASON_DESC( O_error_message  IN OUT  VARCHAR2,
                          O_season_desc    IN OUT  SEASONS.SEASON_DESC%TYPE,
                          I_season_id      IN      SEASONS.SEASON_ID%TYPE)
                       RETURN BOOLEAN IS

   cursor C_GET_SEASON_DESC is
      select season_desc
        from v_seasons_tl
       where season_id = I_season_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SEASON_DESC',
                    'V_SEASONS_TL',
                    'SEASON ID: ' || to_char(I_season_id));

   open  C_GET_SEASON_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SEASON_DESC',
                    'V_SEASONS_TL',
                    'SEASON ID: ' || to_char(I_season_id));

   fetch C_GET_SEASON_DESC into O_season_desc;

   if C_GET_SEASON_DESC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SEASON_DESC',
                    'V_SEASONS_TL',
                    'SEASON ID: ' || to_char(I_season_id));

      close C_GET_SEASON_DESC;
      O_error_message := SQL_LIB.CREATE_MSG( 'SEASON_ERROR',
                                             to_char(I_season_id),
                                             NULL,
                                             NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SEASON_DESC',
                    'V_SEASONS_TL',
                    'SEASON ID: ' || to_char(I_season_id));

   close C_GET_SEASON_DESC;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.GET_SEASON_DESC',
                                             to_char(SQLCODE));
      RETURN FALSE;

END GET_SEASON_DESC;

---------------------------------------------------------------------------------------------
FUNCTION GET_PHASE_DESC( O_error_message  IN OUT  VARCHAR2,
                         O_phase_desc     IN OUT  PHASES.PHASE_DESC%TYPE,
                         I_season_id      IN      PHASES.SEASON_ID%TYPE,
                         I_phase_id       IN      PHASES.PHASE_ID%TYPE)
                         RETURN BOOLEAN IS

   cursor C_GET_PHASE_DESC is
      select phase_desc
        from v_phases_tl
       where season_id  = I_season_id
         and phase_id   = I_phase_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PHASE_DESC',
                    'V_PHASES_TL',
                    'SEASON ID: '  || to_char(I_season_id) ||
                    ', PHASE ID: ' || to_char(I_phase_id));

   open  C_GET_PHASE_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PHASE_DESC',
                    'V_PHASES_TL',
                    'SEASON ID: '  || to_char(I_season_id) ||
                    ', PHASE ID: ' || to_char(I_phase_id));

   fetch C_GET_PHASE_DESC into O_phase_desc;

   if C_GET_PHASE_DESC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PHASE_DESC',
                    'V_PHASES_TL',
                    'SEASON ID: '  || to_char(I_season_id) ||
                    ', PHASE ID: ' || to_char(I_phase_id));

      close C_GET_PHASE_DESC;

      O_error_message := SQL_LIB.CREATE_MSG( 'PHASE_ERROR',
                                             to_char(I_phase_id),
                                             NULL,
                                             NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PHASE_DESC',
                    'V_PHASES_TL',
                    'SEASON ID: '  || to_char(I_season_id) ||
                    ', PHASE ID: ' || to_char(I_phase_id));

   close C_GET_PHASE_DESC;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.GET_PHASE_DESC',
                                             to_char(SQLCODE));
      RETURN FALSE;

END GET_PHASE_DESC;

---------------------------------------------------------------------------------------------
FUNCTION ASSIGN_DEFAULTS( O_error_message  IN OUT  VARCHAR2,
                          I_item           IN      ITEM_SEASONS.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_dummy           VARCHAR2(1)                           := NULL;
   L_user            ITEM_SEASONS.LAST_UPDATE_ID%TYPE      := GET_USER;
   L_sysdate         ITEM_SEASONS.CREATE_DATETIME%TYPE     := sysdate;
   L_seq_no          ITEM_SEASONS.ITEM_SEASON_SEQ_NO%TYPE  := 0;
   L_prev_season_id  ITEM_SEASONS.SEASON_ID%TYPE           := -1;
   L_prev_phase_id   ITEM_SEASONS.PHASE_ID%TYPE            := -1;
   ---
   cursor C_PARENT_DIFF is
      select 'x'
        from item_seasons s,
             item_master i
       where i.item = I_item
         and s.item = i.item_parent
         and (    i.diff_1 = s.diff_id 
              or  i.diff_2 = s.diff_id
              or  i.diff_3 = s.diff_id
              or  i.diff_4 = s.diff_id)
         and ROWNUM = 1;
   
   cursor C_ITEM_SEASON_INSERT (I_p_gp_ind  IN  VARCHAR2) is
      select season_id,
             phase_id
        from item_seasons s,
             item_master i
       where i.item = I_item
         and (   (I_p_gp_ind = 'P' and s.item = i.item_parent)
              or (I_p_gp_ind = 'G' and s.item = i.item_grandparent))
         and (   s.diff_id = i.diff_1 
              or s.diff_id = i.diff_2
              or s.diff_id = i.diff_3
              or s.diff_id = i.diff_4)
       order by 1,2;

   cursor C_PARENT_NO_DIFF is
      select 'x'
        from item_seasons s,
             item_master i
       where i.item = I_item
         and s.item = i.item_parent
         and s.diff_id is NULL
     and ROWNUM = 1;

   cursor C_GRANDPARENT_DIFF is
      select 'x'
        from item_seasons s,
             item_master i
       where i.item = I_item
         and s.item = i.item_grandparent
         and (    i.diff_1 = s.diff_id 
              or  i.diff_2 = s.diff_id
              or  i.diff_3 = s.diff_id
              or  i.diff_4 = s.diff_id)
         and ROWNUM = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_PARENT_DIFF',
                    'ITEM_MASTER, ITEM_SEASONS',
                    'ITEM: ' ||I_item);
   ---
   open C_PARENT_DIFF;
   SQL_LIB.SET_MARK('FETCH',
                    'C_PARENT_DIFF',
                    'ITEM_MASTER, ITEM_SEASONS',
                    'ITEM: ' ||I_item);
   fetch C_PARENT_DIFF into L_dummy;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_PARENT_DIFF',
                    'ITEM_MASTER, ITEM_SEASONS',
                    'ITEM: ' ||I_item);
   close C_PARENT_DIFF;
   ---
   if L_dummy = 'x' then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'ITEM_SEASONS',
                       ' ITEM: '||I_item);
      ---
      -- Since a season/phase combination may appear more than once on the
      -- item parent (due to multiple diffs), loop through all possible
      -- records and insert those that have not been inserted previously.
      -- This is necessary since diffs are not inserted for item children.
      for rec in C_ITEM_SEASON_INSERT('P') loop
         if (rec.season_id != L_prev_season_id) or
            (rec.phase_id  != L_prev_phase_id) then
            
            L_seq_no := L_seq_no + 1;
            insert into item_seasons(item,
                                     season_id,
                                     phase_id,
                                     item_season_seq_no,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     create_id)
                              values(I_item,
                                     rec.season_id,
                                     rec.phase_id,
                                     L_seq_no,
                                     L_sysdate,
                                     L_sysdate,
                                     L_user,
                                     L_user);
            L_prev_season_id := rec.season_id;
            L_prev_phase_id  := rec.phase_id;
         end if;
      end loop;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_PARENT_NO_DIFF',
                       'ITEM_MASTER, ITEM_SEASONS',
                       'ITEM: ' ||I_item);
      ---
      open C_PARENT_NO_DIFF;
      SQL_LIB.SET_MARK('FETCH',
                       'C_PARENT_NO_DIFF',
                       'ITEM_MASTER, ITEM_SEASONS',
                       'ITEM: ' ||I_item);
      fetch C_PARENT_NO_DIFF into L_dummy;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PARENT_NO_DIFF',
                       'ITEM_MASTER, ITEM_SEASONS',
                       'ITEM: ' ||I_item);
      close C_PARENT_NO_DIFF;

      if L_dummy = 'x' then
         SQL_LIB.SET_MARK('INSERT',
                           NULL,
                          'ITEM_SEASONS',
                          'ITEM:'||I_item);
         ---
         -- There is no need to loop through records here since the only time there
         -- are multiple rows that will affect child items is if there are multiple diffs
         -- on the parent.
         insert into item_seasons(item,
                                  season_id,
                                  phase_id,
                                  item_season_seq_no,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  create_id)
                           select I_item,
                                  season_id,
                                  phase_id,
                                  rownum,
                                  L_sysdate,
                                  L_sysdate,
                                  L_user,
                                  L_user
                             from item_seasons s,
                                  item_master i
                            where i.item = I_item
                              and s.item = i.item_parent
                              and s.diff_id is NULL;
      else
         SQL_LIB.SET_MARK('OPEN',
                          'C_GRANDPARENT_DIFF',
                          'ITEM_MASTER, ITEM_SEASONS',
                          'ITEM: ' ||I_item);
         ---
         open C_GRANDPARENT_DIFF;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GRANDPARENT_DIFF',
                          'ITEM_MASTER, ITEM_SEASONS',
                          'ITEM: ' ||I_item);
         fetch C_GRANDPARENT_DIFF into L_dummy;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GRANDPARENT_DIFF',
                          'ITEM_MASTER, ITEM_SEASONS',
                          'ITEM: ' ||I_item);
         close C_GRANDPARENT_DIFF;

         if L_dummy = 'x' then
            SQL_LIB.SET_MARK('INSERT',
                              NULL,
                             'ITEM_SEASONS',
                             'ITEM:'||I_item);
            ---
            -- Since a season/phase combination may appear more than once on the
            -- item grandparent (due to multiple diffs), loop through all possible
            -- records and insert those that have not been inserted previously.
            -- This is necessary since diffs are not inserted for item grandchildren.
            for rec in C_ITEM_SEASON_INSERT('G') loop
               if (rec.season_id != L_prev_season_id) or
                  (rec.phase_id  != L_prev_phase_id) then
            
                  L_seq_no := L_seq_no + 1;
                  insert into item_seasons(item,
                                           season_id,
                                           phase_id,
                                           item_season_seq_no,
                                           create_datetime,
                                           last_update_datetime,
                                           last_update_id,
                                           create_id)
                                    values(I_item,
                                           rec.season_id,
                                           rec.phase_id,
                                           L_seq_no,
                                           L_sysdate,
                                           L_sysdate,
                                           L_user,
                                           L_user);
                  L_prev_season_id := rec.season_id;
                  L_prev_phase_id  := rec.phase_id;
               end if;
            end loop;
         else
            SQL_LIB.SET_MARK('INSERT',
                              NULL,
                             'ITEM_SEASONS',
                             'ITEM:'||I_item);
            ---
            -- There is no need to loop through records here since the only time there
            -- are multiple rows that will affect grandchild items is if there are multiple
            -- diffs on the grandparent.
            insert into item_seasons(item,
                                     season_id,
                                     phase_id,
                                     item_season_seq_no,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     create_id)
                              select I_item,
                                     season_id,
                                     phase_id,
                                     rownum,
                                     L_sysdate,
                                     L_sysdate,
                                     L_user,
                                     L_user
                                from item_seasons s,
                                     item_master i
                               where i.item = I_item
                                 and s.item = i.item_grandparent
                                 and s.diff_id is NULL;
         end if;
      end if;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.ASSIGN_DEFAULTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ASSIGN_DEFAULTS;
---------------------------------------------------------------------------------------------
FUNCTION PERFORM_MC( O_error_message       IN OUT  VARCHAR2,
                     O_reject              IN OUT  BOOLEAN,
                     I_itemlist            IN      SKULIST_DETAIL.SKULIST%TYPE,
                     I_new_season_id       IN      PHASES.SEASON_ID%TYPE,
                     I_new_phase_id        IN      PHASES.PHASE_ID%TYPE,
                     I_existing_season_id  IN      PHASES.SEASON_ID%TYPE,
                     I_existing_phase_id   IN      PHASES.PHASE_ID%TYPE,
                     I_action              IN      VARCHAR2)
                     RETURN BOOLEAN IS

   L_item        SKULIST_DETAIL.ITEM%TYPE         := NULL;
   L_level       SKULIST_DETAIL.ITEM_LEVEL%TYPE   := NULL;
   L_success     BOOLEAN;

   cursor C_EXPLODE_ITEMLIST is
      select item,
             item_level,
             tran_level
        from skulist_detail
       where skulist = I_itemlist
       order by item_level;

-------------------------------------------------------------------------------------------------
FUNCTION INSERT_RECORD (O_success  IN OUT  BOOLEAN,
                        I_item     IN      ITEM_SEASONS.ITEM%TYPE)
                        RETURN BOOLEAN IS

   L_next_seq_no        ITEM_SEASONS.ITEM_SEASON_SEQ_NO%TYPE;
   L_user           VARCHAR2(30) := GET_USER;

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'ITEM_SEASONS',
                    'SEASON ID: '  || to_char(I_new_season_id) ||
                    ', PHASE ID: ' || to_char(I_new_phase_id)  ||
                    ', ITEM: '     ||I_item);

   if SEASON_SQL.NEXT_SEQ_NO (O_error_message,
                              L_next_seq_no,
                              I_item) = FALSE then
   RETURN FALSE;
   end if;

   insert into item_seasons(season_id,
                            phase_id,
                            item,
                            item_season_seq_no,
                            create_datetime,
                            last_update_datetime,
                            last_update_id)
                     values(I_new_season_id,
                            I_new_phase_id,
                            I_item,
                            L_next_seq_no,
                            sysdate,
                            sysdate,
                            L_user);

   O_success := TRUE;

   RETURN TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then

      O_success := FALSE;

      RETURN TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.INSERT_RECORD',
                                             to_char(SQLCODE));
      RETURN FALSE;

END INSERT_RECORD;

-------------------------------------------------------------------------------------------------
FUNCTION DELETE_CHANGE_RECORD (I_item  IN  ITEM_SEASONS.ITEM%TYPE)
                       RETURN BOOLEAN IS

   L_delete        BOOLEAN        := FALSE;
   L_dummy         VARCHAR2(1);
   Record_Locked   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_CHECK_ITEM_DUPLICATES is
      select 'x'
        from item_seasons
       where season_id = I_new_season_id
         and phase_id  = I_new_phase_id
         and item      = I_item
         and diff_id is NULL;



   cursor C_LOCK_ITEM_SEASONS is
      select 'x'
        from item_seasons
       where season_id = I_existing_season_id
         and phase_id  = I_existing_phase_id
         and item      = I_item
         and diff_id is NULL
         for update nowait;

BEGIN
   if I_action = 'D' then
      L_delete  := TRUE;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_ITEM_DUPLICATES',
                       'ITEM_SEASONS',
                       'SEASON ID: '  || to_char(I_new_season_id) ||
                       ', PHASE ID: ' || to_char(I_new_phase_id)  ||
                       ', ITEM: '     ||I_item);

      open C_CHECK_ITEM_DUPLICATES;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_ITEM_DUPLICATES',
                       'ITEM_SEASONS',
                       'SEASON ID: '  || to_char(I_new_season_id) ||
                       ', PHASE ID: ' || to_char(I_new_phase_id)  ||
                       ', ITEM: '     ||I_item);

      fetch C_CHECK_ITEM_DUPLICATES into L_dummy;

      if C_CHECK_ITEM_DUPLICATES%FOUND then
         L_delete := TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_ITEM_DUPLICATES',
                       'ITEM_SEASONS',
                       'SEASON ID: '  || to_char(I_new_season_id) ||
                       ', PHASE ID: ' || to_char(I_new_phase_id)  ||
                       ', ITEM: '     ||I_item);

      close C_CHECK_ITEM_DUPLICATES;

      if not L_delete then
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_ITEM_SEASONS',
                          'ITEM_SEASONS',
                          'SEASON ID: '  || to_char(I_existing_season_id) ||
                          ', PHASE ID: ' || to_char(I_existing_phase_id)  ||
                          ', ITEM: '     ||I_item);

         open C_LOCK_ITEM_SEASONS;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_ITEM_SEASONS',
                          'ITEM_SEASONS',
                          'SEASON ID: '  || to_char(I_existing_season_id) ||
                          ', PHASE ID: ' || to_char(I_existing_phase_id)  ||
                          ', ITEM: '     ||I_item);

         close C_LOCK_ITEM_SEASONS;

         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'ITEM_SEASONS',
                          'SEASON ID: '  || to_char(I_existing_season_id) ||
                          ', PHASE ID: ' || to_char(I_existing_phase_id)  ||
                          ', ITEM: '     ||I_item);

         update item_seasons
            set season_id            = I_new_season_id,
                phase_id             = I_new_phase_id,
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where season_id            = I_existing_season_id
            and phase_id             = I_existing_phase_id
            and item                 = I_item
            and diff_id is NULL;
      end if;
   end if;
   ---
   if L_delete then
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_SEASONS',
                       'ITEM_SEASONS',
                       'SEASON ID: '  || to_char(I_existing_season_id) ||
                       ', PHASE ID: ' || to_char(I_existing_phase_id)  ||
                       ', ITEM: '     ||I_item);

      open C_LOCK_ITEM_SEASONS;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_SEASONS',
                       'ITEM_SEASONS',
                       'SEASON ID: '  || to_char(I_existing_season_id) ||
                       ', PHASE ID: ' || to_char(I_existing_phase_id)  ||
                       ', ITEM: '     ||I_item);

      close C_LOCK_ITEM_SEASONS;

      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'ITEM_SEASONS',
                       'SEASON ID: '  || to_char(I_existing_season_id) ||
                       ', PHASE ID: ' || to_char(I_existing_phase_id)  ||
                       ', ITEM: '     ||I_item);

      delete from item_seasons
       where season_id = I_existing_season_id
         and phase_id  = I_existing_phase_id
         and item      = I_item
         and diff_id is NULL;
   end if;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG( 'ITEM_SEASONS_REC_LOCK',
                                             to_char(I_existing_season_id),
                                             to_char(I_existing_phase_id),
                                             I_item);

      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.DELETE_CHANGE_RECORD',
                                             to_char(SQLCODE));
      RETURN FALSE;

END DELETE_CHANGE_RECORD;

-------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEMS(I_item           IN  ITEM_SEASONS.ITEM%TYPE)
                                   RETURN BOOLEAN IS

   L_item  ITEM_MASTER.ITEM%TYPE          := NULL;

   cursor C_EXPLODE_ITEMS is
      select item
        from item_master
       where (item_parent = I_item or item_grandparent = I_item)
       and tran_level >= item_level
       order by item_level;

BEGIN
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXPLODE_ITEMS',
                    'ITEM_MASTER',
                    'ITEM_PARENT OR ITEM_GRANDPARENT ' ||I_item);

   FOR C_rec_item in C_EXPLODE_ITEMS LOOP
      L_item := C_rec_item.item;

      if I_action = 'A' then
         if INSERT_RECORD(L_success,
                          L_item) = FALSE then
            RETURN FALSE;
         end if;
      else
         if DELETE_CHANGE_RECORD(L_item) = FALSE then
            RETURN FALSE;
         end if;
      end if;
   END LOOP;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.EXPLODE_ITEMS',
                                             to_char(SQLCODE));
      RETURN FALSE;

END EXPLODE_ITEMS;

-------------------------------------------------------------------------------------------------
BEGIN
   O_reject := FALSE;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXPLODE_ITEMLIST',
                    'SKULIST_DETAIL',
                    'ITEM LIST: ' || to_char(I_itemlist));

   FOR C_rec_item in C_EXPLODE_ITEMLIST LOOP
      L_item       := C_rec_item.item;
      L_level      := C_rec_item.tran_level - C_rec_item.item_level;


      if I_action = 'A' then
         if INSERT_RECORD(L_success,
                          L_item) = FALSE then
            RETURN FALSE;
         end if;

         if L_level > 0 and L_success then
            if EXPLODE_ITEMS(L_item) = FALSE then
               RETURN FALSE;
            end if;
         end if;
      else
         if DELETE_CHANGE_RECORD(L_item) = FALSE then
            RETURN FALSE;
         end if;

         if L_level > 0 then
            if EXPLODE_ITEMS(L_item) = FALSE then
               RETURN FALSE;
            end if;
         end if;
      end if;

   END LOOP;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.PERFORM_MC',
                                             to_char(SQLCODE));
      RETURN FALSE;

END PERFORM_MC;

---------------------------------------------------------------------------------------------
FUNCTION CHECK_SEASON( O_error_message  IN OUT  VARCHAR2,
                       O_exists         IN OUT  BOOLEAN,
                       I_item           IN      ITEM_SEASONS.ITEM%TYPE)
                       RETURN BOOLEAN IS

   L_dummy  VARCHAR2(1);

   cursor C_CHECK_SEASON is
      select 'x'
        from item_seasons
       where item = I_item
       and ROWNUM = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_SEASON',
                    'ITEM_SEASONS',
                    'ITEM: ' ||I_item);

   open  C_CHECK_SEASON;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_SEASON',
                    'ITEM_SEASONS',
                    'ITEM: ' ||I_item);

   fetch C_CHECK_SEASON into L_dummy;
   if C_CHECK_SEASON%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_SEASON',
                    'ITEM_SEASONS',
                    'ITEM: ' ||I_item);

   close C_CHECK_SEASON;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.CHECK_SEASON',
                                             to_char(SQLCODE));
      RETURN FALSE;

END CHECK_SEASON;

---------------------------------------------------------------------------------------------
FUNCTION SEASON_PHASE_EXISTS( O_error_message  IN OUT  VARCHAR2,
                          O_exists         IN OUT  BOOLEAN,
                          I_season_id      IN      PHASES.SEASON_ID%TYPE,
                          I_phase_id       IN      PHASES.PHASE_ID%TYPE)
                          RETURN BOOLEAN IS

   L_dummy         VARCHAR2(1);

   cursor C_EXISTS is
      select 'x'
        from item_seasons
       where season_id = I_season_id
         and phase_id = nvl(I_phase_id, phase_id);

BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'ITEM_SEASONS',
                    'SEASON ID: ' || to_char(I_season_id));

   open  C_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'ITEM_SEASONS',
                    'SEASON ID: ' || to_char(I_season_id));

   fetch C_EXISTS into L_dummy;
      if C_EXISTS%FOUND then
         O_exists := TRUE;
      end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    'ITEM_SEASONS',
                    'SEASON ID: ' || to_char(I_season_id));

   close C_EXISTS;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'SEASON_SQL.RELATION_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;

END SEASON_PHASE_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CREATE_PHASE_ITEM_RELATION(O_error_message        IN OUT   VARCHAR2,
                                    O_item_season_seq_no   IN OUT   item_seasons.item_season_seq_no%TYPE,
                                    I_season_id            IN       phases.season_id%TYPE,
                                    I_phase_id             IN       phases.phase_id%TYPE,
                                    I_item                 IN       item_master.item%TYPE)
RETURN BOOLEAN IS

   cursor C_FIND_RELATION is
      select 'x'
        from item_seasons
       where season_id = I_season_id
         and phase_id = I_phase_id
         and item = I_item
         and diff_id is NULL;

   L_dummy              VARCHAR2(1);
   L_user         VARCHAR2(30) := GET_USER;
   L_next_seq_no        ITEM_SEASONS.ITEM_SEASON_SEQ_NO%TYPE;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_FIND_RELATION',
                    'ITEM_SEASONS',
                    'ITEM: ' ||I_item ||
                    ', SEASON ID: ' || to_char(I_season_id) ||
                    ', PHASE ID: ' || to_char(I_phase_id));
   open C_FIND_RELATION;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_FIND_RELATION',
                    'ITEM_SEASONS',
                    'ITEM: ' ||I_item ||
                    ', SEASON ID: ' || to_char(I_season_id) ||
                    ', PHASE ID: ' || to_char(I_phase_id));
   fetch C_FIND_RELATION into L_dummy;
   ---
   if C_FIND_RELATION%NOTFOUND then
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FIND_RELATION',
                       'ITEM_SEASONS',
                       'ITEM: ' ||I_item ||
                       ', SEASON ID: ' || to_char(I_season_id) ||
                       ', PHASE ID: ' || to_char(I_phase_id));
      close C_FIND_RELATION;
      ---
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'ITEM_SEASONS',
                       'ITEM: ' ||I_item ||
                       ', SEASON ID: ' || to_char(I_season_id) ||
                       ', PHASE ID: ' || to_char(I_phase_id));

      if SEASON_SQL.NEXT_SEQ_NO (O_error_message,
                                 L_next_seq_no,
                                 I_item) = FALSE then
                  RETURN FALSE;
      end if;
      ---
      O_item_season_seq_no := L_next_seq_no;
      ---
      insert into item_seasons
                 (item,
                  season_id,
                  phase_id,
                  diff_id,
                  item_season_seq_no,
                  create_datetime,
                  last_update_datetime,
                  last_update_id)
           values(I_item,
                  I_season_id,
                  I_phase_id,
                  NULL,
                  L_next_seq_no,
                  sysdate,
                  sysdate,
                  L_user);
   ---
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FIND_RELATION',
                       'ITEM_SEASONS',
                       'ITEM: ' ||I_item ||
                       ', SEASON ID: ' || to_char(I_season_id) ||
                       ', PHASE ID: ' || to_char(I_phase_id));
      close C_FIND_RELATION;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SEASON_SQL.CREATE_PHASE_ITEM_RELATION',
                                            to_char(SQLCODE));
      RETURN FALSE;

END CREATE_PHASE_ITEM_RELATION;
------------------------------------------------------------------------------------------
FUNCTION COPY_ITEM_SEASONS(O_error_message IN OUT VARCHAR2,
                           I_to_item       IN     item_master.item%TYPE,
                           I_from_item     IN     item_master.item%TYPE)
                           RETURN BOOLEAN IS

   L_item_season_seq_no     ITEM_SEASONS.ITEM_SEASON_SEQ_NO%TYPE;

   cursor C_GET_SEASONS is
      select season_id,
             phase_id
        from item_seasons
       where item = I_from_item;

BEGIN

   SQL_LIB.SET_MARK('FETCH', 'C_GET_SEASONS', 'ITEM_SEASONS', 'ITEM: '||I_from_item);
   for rec in C_GET_SEASONS LOOP
      if NOT CREATE_PHASE_ITEM_RELATION(O_error_message,
                                        L_item_season_seq_no,
                                        rec.season_id,
                                        rec.phase_id,
                                        I_to_item) then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SEASON_SQL.COPY_ITEM_SEASONS',
                                            to_char(SQLCODE));
      RETURN FALSE;

END COPY_ITEM_SEASONS;
------------------------------------------------------------------------------------------
FUNCTION APPLY_ITEM_DIFF  (O_error_message IN OUT VARCHAR2,
                           I_item          IN ITEM_SEASONS.ITEM%TYPE,
                           I_diff_id       IN ITEM_SEASONS.DIFF_ID%TYPE,
                           I_action            IN VARCHAR2)
   RETURN BOOLEAN IS


   L_item              item_master.item%TYPE;
   L_season_id     item_seasons.season_id%TYPE;
   L_phase_id      item_seasons.phase_id%TYPE;
   L_item_seq_no   item_seasons.item_season_seq_no%TYPE;
   L_table         VARCHAR2(30);
   L_user          VARCHAR2(30) := GET_USER;
   L_season        item_seasons.phase_id%TYPE;
   L_phase         item_seasons.phase_id%TYPE;
   RECORD_LOCKED   EXCEPTION;

   cursor C_ITEM is
      select item
        from item_master
       where (item_parent  = I_item or item_grandparent = I_item)
         and ((diff_1   = I_diff_id  or I_diff_id is null)
         or  (diff_2   = I_diff_id  or I_diff_id is null))
         and tran_level >= item_level;

   cursor C_SEASON_PHASE is
      select season_id,
             phase_id
        from item_seasons
       where item = I_item
         and (diff_id  = I_diff_id  or I_diff_id is NULL);


   cursor C_ITEM_SEASON_PHASE is
      select ril.item
        from repl_item_loc ril
       where ril.item in (select item
                            from item_master
                           where (item_parent = I_item or item_grandparent = I_item)
                             and ((diff_1   = I_diff_id  or I_diff_id is null)
                              or (diff_2   = I_diff_id  or I_diff_id is null)))
         and not exists (select 'x'
                           from item_seasons
                          where (item    = ril.item_parent or item = ril.item_grandparent)
                            and season_id = nvl(ril.season_id, season_id)
                            and phase_id  = nvl(ril.phase_id,phase_id))
         for update nowait;

BEGIN
   ---Ensure that all parameters except for I_diff_id are not null
   if I_item is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_item',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_action is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_action',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_action = 'O' then
      open C_ITEM_SEASON_PHASE;
      fetch C_ITEM_SEASON_PHASE into L_item;
      if C_ITEM_SEASON_PHASE%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SEAS_ON_REPL', NULL, NULL, NULL);
         close C_ITEM_SEASON_PHASE;
         RETURN FALSE;
      else
         delete from item_seasons ss
               where exists (select 'x'
                               from item_master im
                              where ss.item = im.item
                                and (im.item_parent = I_item or im.item_grandparent = I_item)
                                and ((im.diff_1   = I_diff_id  or I_diff_id is null)
                                 or (im.diff_2   = I_diff_id  or I_diff_id is null)))
               and not exists (select 'x'
                                 from repl_item_loc
                                where item    = ss.item
                                  and season_id = nvl(ss.season_id,season_id)
                                  and phase_id  = nvl(ss.phase_id, phase_id));
      end if;
      close C_ITEM_SEASON_PHASE;
   end if;

   if I_action in ('A', 'O') then
      FOR recs in C_ITEM LOOP
         L_item       := recs.item;
         ---Insert records at the item level.
         FOR records1 in C_SEASON_PHASE LOOP
            L_season    := records1.season_id;
            L_phase     := records1.phase_id;
            BEGIN
               if SEASON_SQL.NEXT_SEQ_NO(O_error_message,
                                         L_item_seq_no,
                                         L_item) = FALSE then
                  RETURN FALSE;
               end if;

               SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SEASONS',' ITEM: ' ||I_item);
               insert into item_seasons(item,
                                        season_id,
                                        phase_id,
                                        item_season_seq_no,
                                        diff_id,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id)
                                values (L_item,
                                        L_season,
                                        L_phase,
                                        L_item_seq_no,
                                        NULL,
                                        sysdate,
                                        sysdate,
                                        L_user);
            EXCEPTION
               when DUP_VAL_ON_INDEX then
                  NULL;
            END;
         END LOOP;
      END LOOP;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SEASON_SQL.APPLY_ITEM_DIFF',
                                            to_char(SQLCODE));
      RETURN FALSE;

END APPLY_ITEM_DIFF;
-------------------------------------------------------------------------------------------
FUNCTION NEXT_SEQ_NO(O_error_message         IN OUT VARCHAR2,
                     O_next_seq_no           OUT item_seasons.item_season_seq_no%TYPE,
                     I_item                  IN  item_seasons.item%TYPE)
   RETURN BOOLEAN IS

   cursor C_NEXT_SEQ_NO is
      select nvl(max(item_season_seq_no) + 1, 1)
        from item_seasons
       where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_NEXT_SEQ_NO',
                    'ITEM_SEASONS',
                    'I_ITEM: '  ||I_item);

   open C_NEXT_SEQ_NO;

   SQL_LIB.SET_MARK('FETCH',
                    'C_NEXT_SEQ_NO',
                    'ITEM_SEASONS',
                    'I_ITEM: '  ||I_item);

   fetch C_NEXT_SEQ_NO into O_next_seq_no;

   if C_NEXT_SEQ_NO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_NEXT_SEQ_NO',
                       'ITEM_SEASONS',
                       'I_ITEM: '  ||I_item);

      close C_NEXT_SEQ_NO;
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_NEXT_SEQ_NO',
                    'ITEM_SEASONS',
                    'I_ITEM: '  ||I_item);

   close C_NEXT_SEQ_NO;
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SEASON_SQL.NEXT_SEQ_NO',
                                            to_char(SQLCODE));
      RETURN FALSE;

END NEXT_SEQ_NO;
------------------------------------------------------------------------------------------
FUNCTION ITEM_SEASON_PHASE_EXISTS(O_error_message         IN OUT VARCHAR2,
                                  O_exists                IN OUT BOOLEAN,
                                  O_season_desc           IN OUT seasons.season_desc%TYPE,
                                  O_phase_desc            IN OUT phases.phase_desc%TYPE,
                                  I_season                IN     seasons.season_id%TYPE,
                                  I_phase                 IN     phases.phase_id%TYPE,
                                  I_item                  IN     item_master.item%TYPE,
                                  I_diff_id               IN     item_seasons.diff_id%TYPE)
   RETURN BOOLEAN IS

   L_dummy      VARCHAR2(1);

   cursor C_SEASON_PHASE is
      select 'x'
         from item_seasons
           where item    = I_item
           and season_id = I_season
           and phase_id  = nvl(I_phase, phase_id)
           and (diff_id   = I_diff_id or I_diff_id is NULL);


BEGIN
   if I_season is not NULL then
      open C_SEASON_PHASE;
      fetch C_SEASON_PHASE into L_dummy;
      if C_SEASON_PHASE%FOUND then
         if not SEASON_SQL.GET_SEASON_DESC(O_error_message,
                                           O_season_desc,
                                           I_season) then
            return FALSE;
         end if;

         if I_phase is not NULL then
            if not SEASON_SQL.GET_PHASE_DESC(O_error_message,
                                             O_phase_desc,
                                             I_season,
                                             I_phase) then
               return FALSE;
            end if;
         end if;

         O_exists := TRUE;
      else
         O_exists := FALSE;
         close C_SEASON_PHASE;
         RETURN TRUE;
      end if;
      close C_SEASON_PHASE;
      RETURN TRUE;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SEASON_SQL.ITEM_SEASON_PHASE_EXISTS',
                                            to_char(SQLCODE));
      RETURN FALSE;

END ITEM_SEASON_PHASE_EXISTS;
------------------------------------------------------------------------------------------------------------
FUNCTION GET_PHASE_PERIOD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_start_date      IN OUT   PHASES.START_DATE%TYPE,
                          O_end_date        IN OUT   PHASES.END_DATE%TYPE,
                          I_season_id       IN       SEASONS.SEASON_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_PHASE_PERIOD is
      select min(start_date),
             max(end_date)
        from phases
       where season_id = I_season_id;

BEGIN
   if I_season_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_season_id',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PHASE_PERIOD',
                    'PHASES',
                    'I_SEASON_ID: '||I_season_id);

   open C_GET_PHASE_PERIOD;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PHASE_PERIOD',
                    'PHASES',
                    'I_SEASON_ID: '||I_season_id);

   fetch C_GET_PHASE_PERIOD into O_start_date,
                                 O_end_date;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PHASE_PERIOD',
                    'PHASES',
                    'I_SEASON_ID: '||I_season_id);

   close C_GET_PHASE_PERIOD;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SEASON_SQL.GET_PHASE_PERIOD',
                                            to_char(SQLCODE));
      return FALSE;

END GET_PHASE_PERIOD;
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPER_USER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_valid         IN OUT BOOLEAN)
   RETURN BOOLEAN AS

   L_program   VARCHAR2(64) := 'SEASON_SQL.CHECK_SUPER_USER';
   L_dummy     VARCHAR2(1)  := 'N';

   cursor C_SUPER_USER is
      select 'Y'
        from filter_group_org f, sec_user_group sug, sec_user su
       where sug.group_id = f.sec_group_id
         and sug.user_seq = su.user_seq
         and su.database_user_id = SYS_CONTEXT('USERENV', 'SESSION_USER')
         and rownum = 1       
      union
      select 'Y'
        from filter_group_merch f, sec_user_group sug, sec_user su
       where sug.group_id = f.sec_group_id
         and sug.user_seq = su.user_seq
         and su.database_user_id = SYS_CONTEXT('USERENV', 'SESSION_USER')
         and rownum = 1;

BEGIN
   open C_SUPER_USER;
   fetch C_SUPER_USER into L_dummy;
   close C_SUPER_USER;
   if L_dummy = 'Y' then
      O_valid := FALSE;
   else -- the user is not attached to any security groups and is a superuser
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_SUPER_USER;
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_SEASON_PERIOD( O_error_message  IN OUT  VARCHAR2,
                              O_exists         IN OUT  VARCHAR2,
                              I_season         IN     seasons.season_id%TYPE,
                              I_phase          IN     phases.phase_id%TYPE)
RETURN BOOLEAN AS
  L_program   VARCHAR2(64) := 'SEASON_SQL.CHECK_SEASON_PERIOD';
  L_dummy     VARCHAR2(1)  := 'N';

   cursor C_CHECK_PHASE_DATE is
      select 'Y'
        from phases
       where season_id  = I_season
         and phase_id   = I_phase
         and end_date   > get_vdate;
BEGIN
      open C_CHECK_PHASE_DATE;
      fetch C_CHECK_PHASE_DATE into L_dummy;
      close C_CHECK_PHASE_DATE;
      
      O_exists := L_dummy;
      return TRUE;

     EXCEPTION
     when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
    
    END CHECK_SEASON_PERIOD;
    ------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_SEASON_EXPIRED(O_error_message IN OUT VARCHAR2,
                              O_exists        IN OUT VARCHAR2,
                              I_season        IN     seasons.season_id%TYPE)
RETURN BOOLEAN AS
  L_program   VARCHAR2(64) := 'SEASON_SQL.CHECK_SEASON_EXPIRED';
  L_dummy     VARCHAR2(1)  := 'N';
   cursor C_CHECK_SEASON_EXPIRED is
      select 'Y'
        from  seasons 
       where season_id=I_season
         and end_date<get_vdate;
BEGIN
      open C_CHECK_SEASON_EXPIRED;
      fetch C_CHECK_SEASON_EXPIRED into L_dummy;
      close C_CHECK_SEASON_EXPIRED;
      O_exists :=L_dummy;
       return true;
 
      EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
END CHECK_SEASON_EXPIRED;
END SEASON_SQL;
/
