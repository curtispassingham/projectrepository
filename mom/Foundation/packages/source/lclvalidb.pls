CREATE OR REPLACE PACKAGE BODY LOCLIST_VALIDATE_SQL AS
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCLIST(O_error_message   IN OUT VARCHAR2,
                          O_exists          IN OUT BOOLEAN,
                          I_loc_list        IN OUT LOC_LIST_HEAD.LOC_LIST%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(50)   := 'LOCLIST_VALIDATE_SQL.VALIDATE_LOCLIST';
   L_exists VARCHAR2(1) := NULL;

   cursor C_LOCLIST is
      select 'x' 
        from loc_list_head
       where loc_list = I_loc_list;
BEGIN
   if I_loc_list is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program, NULL, NULL);
      return FALSE;
   end if;

   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_LOCLIST','LOC_LIST_HEAD',
                    'loc_list: '||I_loc_list);
   open C_LOCLIST;
   SQL_LIB.SET_MARK('FETCH','C_LOCLIST','LOC_LIST_HEAD',
                    'loc_list: '||I_loc_list);
   fetch C_LOCLIST into L_exists;
   if C_LOCLIST%NOTFOUND then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_LIST',NULL,NULL,NULL);
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_LOCLIST','LOC_LIST_HEAD',
                    'loc_list: '||I_loc_list);
   close C_LOCLIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END VALIDATE_LOCLIST;
--------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_VALUE(O_error_message   IN OUT VARCHAR2,
                        I_element         IN     LOC_LIST_CRITERIA.ELEMENT%TYPE,
                        I_value           IN     LOC_LIST_CRITERIA.VALUE%TYPE)
   return BOOLEAN is
      L_program          VARCHAR2(50)   := 'LOCLIST_VALIDATE_SQL.VALIDATE_VALUE';
      L_desc             VARCHAR2(150);
      L_exists           VARCHAR2(5);
      L_exists_boolean   BOOLEAN;
      L_tax_info_tbl     OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
      L_tax_info_rec     OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();


   cursor C_DISTRICT is
      select 'x'
        from district
       where district = to_number(I_value);   
   cursor C_TSFZONE is
      select 'x'
        from tsfzone 
       where transfer_zone = to_number(I_value);

   cursor C_COST_ZONE is
      select 'x'
        from cost_zone 
       where zone_id      = to_number(I_value);

   cursor C_STORE_GRADE is
      select 'x'
        from store_grade
       where store_grade  = I_value;

BEGIN
   if I_element is NULL or I_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program, NULL, NULL);
      return FALSE;
   end if;

   if I_element = 'SN' then      -- Store Number --
      if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                   to_number(I_value),
                                   L_desc) = FALSE then
         return FALSE;
      end if;
   elsif I_element in ('WN','DW') then   -- Warehouse Number, Default Warehouse --
      if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                to_number(I_value),
                                L_desc) = FALSE then
         return FALSE;
      end if;
   elsif I_element = 'ST' then    -- State --
      if GEOGRAPHY_SQL.VALIDATE_STATE(I_value, 
                                      L_exists, 
                                      O_error_message)= FALSE then
         return FALSE;
      end if;
      if L_exists = 'N' then  
         O_error_message := SQL_LIB.CREATE_MSG('INV_STATE', NULL, NULL, NULL);
         return FALSE;
      end if;
   elsif I_element = 'CTY' then  -- Country ID --
      if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                       I_value,
                                       L_desc) = FALSE then
         return FALSE;
      end if;
   elsif I_element = 'LT' then  -- Location Traits --
      if LOC_TRAITS_SQL.GET_DESC(O_error_message,
                                 L_desc,
                                 to_number(I_value)) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TRAIT', NULL, NULL, NULL);
         return FALSE;
      end if;
   elsif I_element = 'DT' then   -- District --
      SQL_LIB.SET_MARK('OPEN','C_DISTRICT','DISTRICT','disrtict: '||I_value);
      open C_DISTRICT;
      SQL_LIB.SET_MARK('FETCH','C_DISTRICT','DISTRICT','disrtict: '||I_value);
      fetch C_DISTRICT into L_exists;
      if C_DISTRICT%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRICT', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE','C_DISTRICT','DISTRICT','disrtict: '||I_value);
         close C_DISTRICT;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_DISTRICT','DISTRICT','disrtict: '||I_value);
      close C_DISTRICT;
   elsif I_element = 'VR' then  -- Vat Region --
      L_tax_info_tbl.EXTEND();
      L_tax_info_rec.from_tax_region := TO_NUMBER(I_value);
      L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
      
      if TAX_SQL.GET_TAX_REGION_DESC(O_error_message,
                                     L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
      
      if L_tax_info_tbl.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VAT_REGION',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      else
         L_desc := L_tax_info_tbl(L_tax_info_tbl.COUNT).from_tax_region_desc;
      end if;

   elsif I_element = 'TSF' then  -- Transfer Zone --
      SQL_LIB.SET_MARK('OPEN','C_TSFZONE','TSFZONE','transfer_zone: '||I_value);
      open C_TSFZONE;
      SQL_LIB.SET_MARK('FETCH','C_TSFZONE','TSFZONE','transfer_zone: '||I_value);
      fetch C_TSFZONE into L_exists;
      if C_TSFZONE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TRAN_ZONE', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE','C_TSFZONE','TSFZONE','transfer_zone: '||I_value);
         close C_TSFZONE;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_TSFZONE','TSFZONE','transfer_zone: '||I_value);
      close C_TSFZONE;                                 
   elsif I_element = 'CZG' then  -- Cost Zone Group --
      if COST_ZONE_ATTRIB_SQL.GET_ZONE_GROUP_DESC(to_number(I_value),
                                                  L_desc,
                                                  O_error_message) = FALSE then
         return FALSE;
      end if;
   elsif I_element = 'CZ' then  -- Cost Zone --
      SQL_LIB.SET_MARK('OPEN','C_COST_ZONE','COST_ZONE','zone_id: '||I_value);
      open C_COST_ZONE;
      SQL_LIB.SET_MARK('FETCH','C_COST_ZONE','COST_ZONE','zone_id: '||I_value);
      fetch C_COST_ZONE into L_exists;
      if C_COST_ZONE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_COST_ZONE', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE','C_COST_ZONE','COST_ZONE','zone_id: '||I_value);
         close C_COST_ZONE;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_COST_ZONE','COST_ZONE','zone_id: '||I_value);
      close C_COST_ZONE;
   elsif I_element = 'SGG' then  -- Store Grade Group --
      if STORE_GRADE_VALIDATE_SQL.GET_GROUP_DESC(O_error_message,
                                                 L_desc,
                                                 to_number(I_value)) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_GRADE_GROUP', NULL, NULL, NULL);
         return FALSE;
      end if;
   elsif I_element = 'SG' then   -- Store Grade --
      SQL_LIB.SET_MARK('OPEN','C_STORE_GRADE','STORE_GRADE','store_grade: '||I_value);
      open C_STORE_GRADE;
      SQL_LIB.SET_MARK('FETCH','C_STORE_GRADE','STORE_GRADE','store_grade: '||I_value);
      fetch C_STORE_GRADE into L_exists;
      if C_STORE_GRADE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_STORE_GRADE', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE','C_STORE_GRADE','STORE_GRADE','store_grade: '||I_value);
         close C_STORE_GRADE;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_STORE_GRADE','STORE_GRADE','store_grade: '||I_value);
      close C_STORE_GRADE;
   elsif I_element = 'CUR' then  -- Currency --
      if CURRENCY_SQL.GET_NAME(O_error_message,
                               I_value,
                               L_desc) = FALSE then
         return FALSE;
      end if;
   elsif I_element = 'LNG' then  -- Language --
      if LANGUAGE_SQL.GET_NAME(O_error_message,
                               to_number(I_value),
                               L_desc) = FALSE then
         return FALSE;
      end if;
   elsif  I_element = 'SC' then  -- Store Class --
      if I_value not in ('A','B','C','D','E') then
         O_error_message := SQL_LIB.CREATE_MSG('STORE_CLASS_A_TO_E', NULL, NULL, NULL);
         return FALSE;
      end if;
   elsif I_element = 'SF' then   -- Store Format --
      if STORE_ATTRIB_SQL.FORMAT_NAME(O_error_message,
                                      to_number(I_value),
                                      L_desc) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END VALIDATE_VALUE;
---------------------------------------------------------------------------------------------------------
FUNCTION DETAIL_EXISTS(O_error_message     IN OUT VARCHAR2,
                       O_exists            IN OUT BOOLEAN,
                       I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN IS

   L_dummy      VARCHAR2(1);
   L_program    VARCHAR2(50)   := 'LOCLIST_VALIDATE_SQL.DETAIL_EXISTS';

   cursor C_EXISTS is
      select 'x' 
        from loc_list_detail
       where loc_list = I_loc_list;

BEGIN
   if I_loc_list is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program, NULL, NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN','C_EXISTS',NULL,NULL);
   open C_EXISTS;
   ---
   SQL_LIB.SET_MARK('FECTH','C_EXISTS',NULL,NULL);
   fetch C_EXISTS into L_dummy;
   ---
   if C_EXISTS%NOTFOUND then 
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS',NULL,NULL);
   close C_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOCLIST_VALIDATE_SQL.DETAIL_EXISTS',
                                            to_char(SQLCODE));
   return FALSE;
END DETAIL_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EDIT_EXISTS(O_error_message   IN OUT   VARCHAR2,
                           O_exist           IN OUT   BOOLEAN,
                           I_loc_list        IN       LOC_LIST_HEAD.LOC_LIST%TYPE,
                           I_user_id         IN       LOC_LIST_HEAD.CREATE_ID%TYPE) 
   RETURN BOOLEAN IS

   L_dummy      VARCHAR2(1);

    cursor C_EDIT_EXISTS is
    select 'x'
      from loc_list_head
     where loc_list = I_loc_list
       and (create_id = I_user_id
            or user_security_ind = 'N');

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_EDIT_EXISTS', 'LOC_LIST_HEAD', 'LOCLIST_ID: '||I_loc_list);
   open C_EDIT_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EDIT_EXISTS', 'LOC_LIST_HEAD', 'LOCLIST_ID: '||I_loc_list);                         
   fetch C_EDIT_EXISTS into L_dummy;
   ---
   if C_EDIT_EXISTS%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EDIT_EXISTS', 'LOC_LIST_HEAD', 'LOCLIST_ID: '||I_loc_list);
   close C_EDIT_EXISTS;
   ---
   return TRUE;
EXCEPTION 
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'LOCLIST_VALIDATE_SQL.CHECK_EDIT_EXISTS',
                                             to_char(SQLCODE));
   return FALSE;
END CHECK_EDIT_EXISTS;
-------------------------------------------------------------------------------------------------------
END LOCLIST_VALIDATE_SQL;
/


