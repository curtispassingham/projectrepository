
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ITEM_TARIFF_SQL AS

FUNCTION COPY_DOWN_PARENT_TARIFF (O_error_message  IN OUT VARCHAR2,
                                  I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(65) := 'ITEM_TARIFF_SQL.COPY_DOWN_PARENT_TARIFF';
   L_table         VARCHAR2(65);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_ELIGIBLE_TARIFF is
      select 'x'
        from cond_tariff_treatment
       where item in (select item
                        from item_master
                       where (item_parent = I_item
                          or item_grandparent = I_item)
                         and item_level <= tran_level)
         for update nowait;


BEGIN

   L_table := 'COND_TARIFF_TREATMENT';

   open  C_LOCK_ITEM_ELIGIBLE_TARIFF;
   close C_LOCK_ITEM_ELIGIBLE_TARIFF;

   SQL_LIB.SET_MARK('DELETE', NULL, 'COND_TARIFF_TREATMENT',
                'Item: ' || I_item);

   delete cond_tariff_treatment
       where item in (select item
                        from item_master
                       where (item_parent = I_item
                          or item_grandparent = I_item)
                         and item_level <= tran_level);
   ---
   ---
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'IA',
                             I_item,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'COND_TARIFF_TREATMENT',
                'Item: ' || I_item);

   insert into cond_tariff_treatment (item,
                                      tariff_treatment,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id)
                               select im.item,
                                      ct.tariff_treatment,
                                      sysdate,
                                      sysdate,
                                      user
                                 from cond_tariff_treatment ct,
                                      item_master im
                                where ct.item = I_item
                                  and (im.item_parent = ct.item
                                       or im.item_grandparent = ct.item)
                                  and item_level <= tran_level;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                           L_table,
                                           I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COPY_DOWN_PARENT_TARIFF;
---------------------------------------------------------------------------------------------
END ITEM_TARIFF_SQL;
/


