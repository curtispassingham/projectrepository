CREATE OR REPLACE PACKAGE RMSSUB_XPRCEVENT_VALIDATE AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message      IN OUT          RTK_ERRORS.RTK_TEXT%TYPE, 
                       I_message            IN              "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XPRCEVENT_VALIDATE;
/
