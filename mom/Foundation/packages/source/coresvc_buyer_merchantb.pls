create or replace PACKAGE BODY CORESVC_BUYER_MERCHANT AS
   cursor C_SVC_BUYER(I_process_id NUMBER,
                      I_chunk_id   NUMBER) is
      select pk_buyer.rowid  as pk_buyer_rid,
             st.rowid        as st_rid,
             st.buyer_fax,
             st.buyer_phone,
             st.buyer_name,
             st.buyer,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             upper(st.action) as action,
             st.process$status
        from svc_buyer st,
             buyer     pk_buyer
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.buyer      = pk_buyer.buyer (+);

   cursor C_SVC_MERCHANT(I_process_id NUMBER,I_chunk_id NUMBER) is
      select pk_merchant.rowid  AS pk_merchant_rid,
             st.merch_fax,
             st.merch_phone,
             st.merch_name,
             st.merch,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_merchant st,
             merchant pk_merchant
       where st.process_id    = I_process_id
         and st.chunk_id      = I_chunk_id
         and st.merch         = pk_merchant.merch (+);
         
   c_svc_MERCHANT_rec                  C_SVC_MERCHANT%ROWTYPE;
   TYPE LP_errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab LP_errors_tab_typ;
   TYPE LP_s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab LP_s9t_errors_tab_typ;
   LP_user                             SVCPROV_CONTEXT.USER_NAME%TYPE     := get_user;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS( I_sheet_name      IN       VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2)IS

BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );

   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)IS

BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
------------------------------------------------------------------------------------------------------
FUNCTION P_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error           IN OUT   BOOLEAN,
                  I_rec             IN       C_SVC_BUYER%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                          := 'CORESVC_BUYER_MERCHANT.P_DELETE';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE     := 'SVC_BUYER';
   L_var     VARCHAR2(1)      :=  NULL;
   cursor C_ORDHEAD is
      select 'X'
        from ordhead
       where ordhead.buyer = I_rec.buyer;

   cursor C_DEPS is
      select 'x'
        from deps
       where deps.buyer = I_rec.buyer;

   cursor C_GROUPS is
      select 'x'
        from groups
       where groups.buyer = I_rec.buyer;

   cursor C_DIVISION is
      select 'x'
        from division
       where division.buyer = I_rec.buyer;
       
   cursor C_STORE_GRADE_GROUP is
      select 'x'
        from store_grade_group
       where store_grade_group.buyer = I_rec.buyer;
BEGIN
   open C_DIVISION;
   fetch C_DIVISION into L_var;
   if C_DIVISION%FOUND then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'BUYER',
                  'DEL_BUYER_DIV_ER');
      O_error := TRUE;
   end if; 
   close C_DIVISION;
    
   open C_GROUPS;
   fetch C_GROUPS into L_var;
   if C_GROUPS %FOUND then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'BUYER',
                  'DEL_BUYER_GRP_ER');
      O_error := TRUE;
   end if;
   close C_GROUPS;
      
   open C_DEPS;
   fetch C_DEPS into L_var;
   if C_DEPS %FOUND then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'BUYER',
                  'DEL_BUYER_DEPT_ER');
      O_error := TRUE;
   end if; 
   close C_DEPS;
   
   open C_ORDHEAD;
   fetch C_ORDHEAD into L_var;
   if C_ORDHEAD%FOUND then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'BUYER',
                  'DEL_BUYER_ORD_ER');
      O_error := TRUE;
   end if;
   close C_ORDHEAD;
   
   open C_STORE_GRADE_GROUP;
   fetch C_STORE_GRADE_GROUP into L_var;
   if C_STORE_GRADE_GROUP%FOUND then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'BUYER',
                  'DEL_STORE_GRADE_ER');
      O_error := TRUE;
   end if;
   close C_STORE_GRADE_GROUP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_DIVISION%ISOPEN then
         close C_DIVISION;
      end if;
      if C_GROUPS%ISOPEN then
         close C_GROUPS;
      end if;
      if C_DEPS%ISOPEN then
         close C_DEPS;
      end if;
      if C_ORDHEAD%ISOPEN then
         close C_ORDHEAD;
      end if;
      if C_STORE_GRADE_GROUP%ISOPEN then
         close C_STORE_GRADE_GROUP;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END P_DELETE;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_BUYER_VAL(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error         IN OUT   BOOLEAN,
                           I_rec           IN       C_SVC_BUYER%ROWTYPE) 
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                        := 'CORESVC_BUYER_MERCHANT.PROCESS_BUYER_VAL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_BUYER';
BEGIN

   if I_rec.action=action_del then
      if P_DELETE(O_error_message,
                  O_error,
                  I_rec)=FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_BUYER_VAL;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES( I_file_id NUMBER )IS
   L_sheets s9t_pkg.names_map_typ;
   buyer_cols s9t_pkg.names_map_typ;
   merchant_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets               := s9t_pkg.get_sheet_names(I_file_id);
   buyer_cols             := s9t_pkg.get_col_names(I_file_id,
                                                   buyer_sheet);
   buyer$Action           := buyer_cols('ACTION');
   buyer$buyer            := buyer_cols('BUYER');
   buyer$buyer_name       := buyer_cols('BUYER_NAME');
   buyer$buyer_phone      := buyer_cols('BUYER_PHONE'); 
   buyer$buyer_fax        := buyer_cols('BUYER_FAX');

   merchant_cols          := S9T_PKG.GET_COL_NAMES(I_file_id,
                                                   merchant_sheet);
   merchant$action        := merchant_cols('ACTION');
   merchant$merch         := merchant_cols('MERCH');
   merchant$merch_name    := merchant_cols('MERCH_NAME');
   merchant$merch_phone   := merchant_cols('MERCH_PHONE');
   merchant$merch_fax     := merchant_cols('MERCH_FAX');

END POPULATE_NAMES;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_BUYER( I_file_id IN NUMBER )IS
BEGIN
   insert 
     into TABLE
      (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id    = I_file_id
         and ss.sheet_name = buyer_sheet)
      select s9t_row(s9t_cells( CORESVC_BUYER_MERCHANT.action_mod,
                                buyer,
                                buyer_name,
                                buyer_phone,
                                buyer_fax ))
    from buyer ;
END POPULATE_BUYER;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_MERCHANT( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id     = I_file_id
                    and ss.sheet_name  = merchant_sheet )
          select s9t_row(s9t_cells(CORESVC_BUYER_MERCHANT.action_mod,
                            merch,
                            merch_name,
                            merch_phone,
                            merch_fax
                           ))
     from merchant ;
END POPULATE_MERCHANT;
--------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER )IS
   L_file s9t_file;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||sysdate||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(buyer_sheet);
   L_file.sheets(L_file.get_sheet_index(buyer_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                   'BUYER',
                                                                                   'BUYER_NAME',
                                                                                   'BUYER_PHONE',
                                                                                   'BUYER_FAX');

   L_file.add_sheet(merchant_sheet);
   L_file.sheets(L_file.get_sheet_index(merchant_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                      'MERCH' ,
                                                                                      'MERCH_NAME',
                                                                                      'MERCH_PHONE',
                                                                                      'MERCH_FAX'
                                                                                      );
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id            IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind  IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_BUYER_MERCHANT.CREATE_S9T';
   L_file s9t_file;        
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_BUYER(O_file_id);
      POPULATE_MERCHANT(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          CORESVC_BUYER_MERCHANT.template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file)= FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_BUYER(I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                            I_process_id IN   SVC_BUYER.PROCESS_ID%TYPE)IS
   TYPE svc_buyer_col_typ IS TABLE OF SVC_BUYER%ROWTYPE;
   L_temp_rec    SVC_BUYER%ROWTYPE;
   svc_buyer_col svc_buyer_col_typ          :=NEW svc_buyer_col_typ();
   L_process_id  SVC_BUYER.PROCESS_ID%TYPE;
   L_error       BOOLEAN                    :=FALSE;
   L_default_rec SVC_BUYER%ROWTYPE;
   cursor C_MANDATORY_IND is
      select buyer_fax_mi,
             buyer_phone_mi,
             buyer_name_mi,
             buyer_mi,
             1 as dummy
        from(select column_key,
                    mandatory
               from s9t_tmpl_cols_def
              where template_key = CORESVC_BUYER_MERCHANT.template_key
                and wksht_key    = 'BUYER'
      )PIVOT (MAX(mandatory)   as   mi FOR (column_key) IN (
              'BUYER_FAX'      as   buyer_fax,
              'BUYER_PHONE'    as   buyer_phone,
              'BUYER_NAME'     as   buyer_name,
              'BUYER'          as   buyer,
               null            as   dummy));
   
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_BUYER';
   L_pk_columns    VARCHAR2(255)  := 'Buyer';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select buyer_fax_dv,
                      buyer_phone_dv,
                      buyer_name_dv,
                      buyer_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_BUYER_MERCHANT.template_key
                          and wksht_key    = 'BUYER'
               )PIVOT (MAX(default_value)   as dv 
                       FOR(column_key) IN ('BUYER_FAX'          as   buyer_fax,
                                           'BUYER_PHONE'        as   buyer_phone,
                                           'BUYER_NAME'         as   buyer_name,
                                           'BUYER'              as   buyer,
                                            NULL                as   dummy)))
   LOOP
      BEGIN
         L_default_rec.buyer_fax := rec.buyer_fax_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'BUYER',
                             NULL,
                            'BUYER_FAX',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.buyer_phone := rec.buyer_phone_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'BUYER',
                             NULL,
                            'BUYER_PHONE',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.buyer_name := rec.buyer_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'BUYER',
                             NULL,
                            'BUYER_NAME',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.buyer := rec.buyer_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'BUYER',
                             NULL,
                            'BUYER',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(buyer$action)      as   action,
                     r.get_cell(buyer$buyer_fax)   as   buyer_fax,
                     r.get_cell(buyer$buyer_phone) as   buyer_phone,
                     r.get_cell(buyer$buyer_name)  as   buyer_name,
                     r.get_cell(buyer$buyer)       as   buyer,
                     r.get_row_seq()               as   row_seq
                from s9t_folder sf,                             
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(buyer_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            buyer_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.buyer_fax := rec.buyer_fax;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            buyer_sheet,
                            rec.row_seq,
                            'BUYER_FAX',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.buyer_phone := rec.buyer_phone;
      EXCEPTION
         when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             buyer_sheet,
                             rec.row_seq,
                             'BUYER_PHONE',
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.buyer_name := rec.buyer_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            buyer_sheet,
                            rec.row_seq,
                            'BUYER_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.buyer := rec.buyer;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            buyer_sheet,
                            rec.row_seq,
                            'BUYER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_BUYER_MERCHANT.action_new then
         L_temp_rec.buyer_fax   := NVL( L_temp_rec.buyer_fax,L_default_rec.buyer_fax);
         L_temp_rec.buyer_phone := NVL( L_temp_rec.buyer_phone,L_default_rec.buyer_phone);
         L_temp_rec.buyer_name  := NVL( L_temp_rec.buyer_name,L_default_rec.buyer_name);
         L_temp_rec.buyer       := NVL( L_temp_rec.buyer,L_default_rec.buyer);
      end if;
      --because null is allwed for buyer on insert
      if L_temp_rec.buyer is null 
         and L_temp_rec.action in (action_mod,action_del) then
         WRITE_S9T_ERROR( I_file_id,
                          BUYER_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_buyer_col.EXTEND();
         svc_buyer_col(svc_buyer_col.COUNT()):=L_temp_rec;
       end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_buyer_col.COUNT SAVE EXCEPTIONS 
      merge into SVC_BUYER st 
      using(select(case
                   when L_mi_rec.buyer_fax_mi    = 'N'
                    and svc_buyer_col(i).action  = CORESVC_BUYER_MERCHANT.action_mod
                    and s1.buyer_fax             IS NULL
                   then mt.buyer_fax
                   else s1.buyer_fax
                   end) as buyer_fax,
                  (case
                   when L_mi_rec.buyer_phone_mi = 'N'
                    and svc_buyer_col(i).action = CORESVC_BUYER_MERCHANT.action_mod
                    and s1.buyer_phone             is NULL
                   then mt.buyer_phone
                   else s1.buyer_phone
                   end) as buyer_phone,
                  (case
                   when L_mi_rec.buyer_name_mi    = 'N'
                    and svc_buyer_col(i).action = CORESVC_BUYER_MERCHANT.action_mod
                    and s1.buyer_name             is NULL
                   then mt.buyer_name
                   else s1.buyer_name
                   end) as buyer_name,
                  (case
                   when L_mi_rec.buyer_mi    = 'N'
                    and svc_buyer_col(i).action = CORESVC_BUYER_MERCHANT.action_mod
                    and s1.buyer             is NULL
                   then mt.buyer
                   else s1.buyer
                   end) as buyer,
                   null as dummy
              from(select svc_buyer_col(i).buyer_fax   as buyer_fax,
                          svc_buyer_col(i).buyer_phone as buyer_phone,
                          svc_buyer_col(i).buyer_name  as buyer_name,
                          svc_buyer_col(i).buyer       as buyer,
                          null as dummy
                     from dual) s1,
                          buyer mt
             where mt.buyer (+) = s1.buyer   
               and  1           = 1) sq 
                on (st.buyer = sq.buyer and
                    svc_buyer_col(i).action IN (CORESVC_BUYER_MERCHANT.action_mod,CORESVC_BUYER_MERCHANT.action_del))
      when matched then 
      update
         set process_id        = svc_buyer_col(i).process_id ,
             action            = svc_buyer_col(i).action,
             chunk_id          = svc_buyer_col(i).chunk_id ,
             row_seq           = svc_buyer_col(i).row_seq ,
             process$status    = svc_buyer_col(i).process$status ,
             buyer_phone       = sq.buyer_phone ,
             buyer_name        = sq.buyer_name ,
             buyer_fax         = sq.buyer_fax ,
             create_id         = svc_buyer_col(i).create_id ,
             create_datetime   = svc_buyer_col(i).create_datetime ,
             last_upd_id       = svc_buyer_col(i).last_upd_id ,
             last_upd_datetime = svc_buyer_col(i).last_upd_datetime 
      when NOT matched then
         insert(process_id ,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                buyer_fax ,
                buyer_phone ,
                buyer_name ,
                buyer ,
                create_id ,
                create_datetime ,
                last_upd_id ,
                last_upd_datetime)
          values(svc_buyer_col(i).process_id ,
                 svc_buyer_col(i).chunk_id ,
                 svc_buyer_col(i).row_seq ,
                 svc_buyer_col(i).action ,
                 svc_buyer_col(i).process$status ,
                 sq.buyer_fax ,
                 sq.buyer_phone ,
                 sq.buyer_name ,
                 sq.buyer ,
                 svc_buyer_col(i).create_id ,
                 svc_buyer_col(i).create_datetime ,
                 svc_buyer_col(i).last_upd_id ,
                 svc_buyer_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            
            WRITE_S9T_ERROR( I_file_id,
                            BUYER_sheet,
                            svc_buyer_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_BUYER;
------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_MERCHANT( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id IN   SVC_MERCHANT.PROCESS_ID%TYPE) IS
   
   TYPE svc_MERCHANT_col_typ is TABLE OF SVC_MERCHANT%ROWTYPE;
   
   L_temp_rec       SVC_MERCHANT%ROWTYPE;
   svc_merchant_col svc_merchant_col_typ            :=NEW svc_merchant_col_typ();
   L_process_id     SVC_MERCHANT.PROCESS_ID%TYPE;
   L_error          BOOLEAN                         :=FALSE;
   L_default_rec    SVC_MERCHANT%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select merch_mi,
             merch_name_mi,
             merch_phone_mi,
             merch_fax_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                                =  template_key
                 and wksht_key                                   = 'MERCHANT'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('MERCH'       as merch,
                                            'MERCH_NAME'  as merch_name,
                                            'MERCH_PHONE' as merch_phone,
                                            'MERCH_FAX'   as merch_fax,
                                             null         as dummy));
   L_mi_rec   C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
 L_table         VARCHAR2(30)   := 'SVC_MERCHANT';
   L_pk_columns    VARCHAR2(255)  := 'Merchandiser';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   FOR rec IN (select  merch_fax_dv,
                       merch_phone_dv,
                       merch_name_dv,
                       merch_dv,
                       null as dummy
                 FROM (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                     = 'MERCHANT'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'MERCH'       as merch,
                                                      'MERCH_NAME'  as merch_name,
                                                      'MERCH_PHONE' as merch_phone,
                                                      'MERCH_FAX'   as merch_fax,
                                                       NULL         as dummy)))
   LOOP
   BEGIN
      L_default_rec.merch_fax := rec.merch_fax_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'MERCHANT',
                          NULL,
                         'MERCH_FAX',
                          NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.merch_phone := rec.merch_phone_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'MERCHANT',
                          NULL,
                         'MERCH_PHONE',
                          NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.merch_name := rec.merch_name_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'MERCHANT',
                          NULL,
                         'MERCH_NAME',
                          NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.merch := rec.merch_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'MERCHANT',
                          NULL,
                         'MERCH',
                          NULL,
                         'INV_DEFAULT');
   END;
   END LOOP;
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(SELECT r.get_cell(merchant$action)               as action,
                     r.get_cell(merchant$merch)                as merch,
                     r.get_cell(merchant$merch_name)           as merch_name,
                     r.get_cell(merchant$merch_phone)          as merch_phone,
                     r.get_cell(merchant$merch_fax)            as merch_fax,
                     r.get_row_seq()                           as row_seq
                FROM s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               WHERE sf.file_id    = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(MERCHANT_sheet)
             )
   LOOP
      L_temp_rec                   := NULL;     
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
    
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            MERCHANT_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.merch_fax := rec.merch_fax;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            MERCHANT_sheet,
                            rec.row_seq,
                            'MERCH_FAX',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.merch_phone := rec.merch_phone;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            MERCHANT_sheet,
                            rec.row_seq,
                            'MERCH_PHONE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.merch_name := rec.merch_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            MERCHANT_sheet,
                            rec.row_seq,
                            'MERCH_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.merch := rec.merch;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            MERCHANT_sheet,
                            rec.row_seq,
                            'MERCH',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_BUYER_MERCHANT.action_new then
         L_temp_rec.MERCH_FAX   := NVL( L_temp_rec.merch_fax   ,L_default_rec.merch_fax);
         L_temp_rec.MERCH_PHONE := NVL( L_temp_rec.merch_phone ,L_default_rec.merch_phone);
         L_temp_rec.MERCH_NAME  := NVL( L_temp_rec.merch_name  ,L_default_rec.merch_name);
         L_temp_rec.MERCH       := NVL( L_temp_rec.merch       ,L_default_rec.merch);
      end if;
      if not (L_temp_rec.MERCH is not null and
            1 = 1) then
         WRITE_S9T_ERROR( I_file_id,
                          MERCHANT_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_merchant_col.extend();
         svc_merchant_col(svc_merchant_col.COUNT()):=L_temp_rec;
      end if;
   end loop;
   BEGIN
      forall i IN 1..svc_merchant_col.COUNT SAVE EXCEPTIONS
      Merge into SVC_MERCHANT st 
      USING(select (case
                    when L_mi_rec.MERCH_mi    = 'N'
                     and svc_merchant_col(i).action = CORESVC_BUYER_MERCHANT.action_mod
                     and s1.MERCH             IS NULL
                    then mt.MERCH
                    else s1.MERCH
                 END) AS MERCH,
                  (case
                   when L_mi_rec.merch_name_mi     = 'N'
                    and svc_merchant_col(i).action = CORESVC_BUYER_MERCHANT.action_mod
                    and s1.merch_name             is null
                   then mt.merch_name
                   else s1.merch_name
                 end) as merch_name,
                   (case
                    when L_mi_rec.merch_phone_mi    = 'N'
                     and svc_merchant_col(i).action = CORESVC_BUYER_MERCHANT.action_mod
                     and s1.merch_phone             IS NULL
                    then mt.merch_phone
                    else s1.merch_phone
                 end) as merch_phone,
                  (case
                   when L_mi_rec.merch_fax_mi      = 'N'
                    and svc_merchant_col(i).action = CORESVC_BUYER_MERCHANT.action_mod
                    and s1.merch_fax             IS NULL
                   then mt.merch_fax
                   else s1.merch_fax
                 end) as merch_fax,
                 null as dummy
            from (select svc_merchant_col(i).merch       as merch,
                         svc_merchant_col(i).merch_name  as merch_name,
                         svc_merchant_col(i).merch_phone as merch_phone,
                         svc_merchant_col(i).merch_fax   as merch_fax,
                         null as dummy
                    from dual
                  ) s1,
                  merchant mt
           where mt.merch (+)     = s1.merch   
             and 1 = 1) sq on (st.merch      = sq.merch 
                               and svc_merchant_col(i).action IN (CORESVC_BUYER_MERCHANT.action_mod,
                                                                  CORESVC_BUYER_MERCHANT.action_del)
                              )
          when matched then
             update
                set process_id        = svc_merchant_col(i).process_id ,
                    chunk_id          = svc_merchant_col(i).chunk_id ,
                    row_seq           = svc_merchant_col(i).row_seq ,
                    action            = svc_merchant_col(i).action,
                    process$status    = svc_merchant_col(i).process$status ,
                    merch_name        = sq.merch_name ,
                    merch_phone       = sq.merch_phone ,
                    merch_fax         = sq.merch_fax ,
                    create_id         = svc_merchant_col(i).create_id ,
                    create_datetime   = svc_merchant_col(i).create_datetime ,
                    last_upd_id       = svc_merchant_col(i).last_upd_id ,
                    last_upd_datetime = svc_merchant_col(i).last_upd_datetime 
          when not matched then
             insert(process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    merch ,
                    merch_name ,
                    merch_phone ,
                    merch_fax ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime
                   )
            values(svc_merchant_col(i).process_id ,
                   svc_merchant_col(i).chunk_id ,
                   svc_merchant_col(i).row_seq ,
                   svc_merchant_col(i).action ,
                   svc_merchant_col(i).process$status ,
                   sq.merch ,
                   sq.merch_name ,
                   sq.merch_phone ,
                   sq.merch_fax ,
                   svc_merchant_col(i).create_id ,
                   svc_merchant_col(i).create_datetime ,
                   svc_merchant_col(i).last_upd_id ,
                   svc_merchant_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
            LOOP
               L_error_code:=sql%bulk_exceptions(i).error_code;
       if L_error_code=1 then
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
       end if;
               WRITE_S9T_ERROR( I_file_id,
                          MERCHANT_sheet,
                          svc_merchant_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
    END LOOP;
  END;
END PROCESS_S9T_MERCHANT;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_COUNT     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)              := 'CORESVC_BUYER_MERCHANT.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         S9T_PKG.NAMES_MAP_TYP;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR     EXCEPTION;
   PRAGMA     EXCEPTION_INIT(MAX_CHAR, -01706);   
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;  
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
       POPULATE_NAMES(I_file_id);
       sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                               L_file.user_lang);
       PROCESS_S9T_BUYER(I_file_id,
                         I_process_id);
       PROCESS_S9T_MERCHANT(I_file_id,
                            I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert 
        into s9t_errors 
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if O_error_count    = 0 THEN
     L_process_status := 'PS';
  else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
        file_id    = I_file_id
  where process_id = I_process_id;
  commit;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
     rollback;
  O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT', 
                                               NULL, 
                                               NULL, 
                                               NULL);
  Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
  WRITE_S9T_ERROR(I_file_id,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        'INV_FILE_FORMAT');
  O_error_count := LP_s9t_errors_tab.count();
  forall i IN 1..O_error_count
     insert 
             into s9t_errors 
           values LP_s9t_errors_tab(i);
     update svc_process_tracker
     set status       = 'PE',
         file_id      = i_file_id
   where process_id   = i_process_id;
     COMMIT;
     return FALSE;
   
      when MAX_CHAR then
         ROLLBACK;
         O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
         Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
         write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
         O_error_count := Lp_s9t_errors_tab.count();
         forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);
   
         update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
          where process_id = I_process_id;
         COMMIT;
      return FALSE;
      
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_BUYER_INS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_buyer_temp_rec  IN       BUYER%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_BUYER_MERCHANT.EXEC_BUYER_INS';
BEGIN
   insert 
     into buyer 
   values I_buyer_temp_rec;
   return TRUE;      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;   
END EXEC_BUYER_INS;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_BUYER_UPD( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_buyer_temp_rec  IN       BUYER%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_BUYER_MERCHANT.EXEC_BUYER_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BUYER';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_LOCK_BUYER is
      select 'x'
        from buyer
       where buyer = I_buyer_temp_rec.buyer
         for update nowait;
BEGIN
   open  C_LOCK_BUYER;
   close C_LOCK_BUYER;
   update buyer 
      set row = I_buyer_temp_rec
    where 1 = 1
      and buyer = I_buyer_temp_rec.buyer;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_buyer_temp_rec.buyer,  
                                             NULL); 
   when OTHERS then
      if C_LOCK_BUYER%ISOPEN then
      close C_LOCK_BUYER;
   end if;
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXEC_BUYER_UPD;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_BUYER_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_buyer_temp_rec  IN       BUYER%ROWTYPE )
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_BUYER_MERCHANT.EXEC_BUYER_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BUYER';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_BUYER is
      select 'x'
        from buyer
       where buyer = I_buyer_temp_rec.buyer
         for update nowait;
BEGIN
   open  C_LOCK_BUYER;
   close C_LOCK_BUYER;
   delete 
     from buyer 
    where 1 = 1
      and buyer = I_buyer_temp_rec.buyer;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_buyer_temp_rec.buyer,  
                                             NULL); 
   when OTHERS then
      if C_LOCK_BUYER%ISOPEN then
      close C_LOCK_BUYER;
   end if;
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_BUYER_DEL;
-----------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_BUYER.PROCESS_ID%TYPE) IS

BEGIN
   delete 
     from svc_buyer 
    where process_id= I_process_id;

   Delete 
     from svc_merchant 
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_BUYER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id      IN       SVC_BUYER.PROCESS_ID%TYPE,
                       I_chunk_id        IN       SVC_BUYER.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                          := 'CORESVC_BUYER_MERCHANT.PROCESS_BUYER';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE     := 'SVC_BUYER';
   L_error          BOOLEAN;
   L_process_error  BOOLEAN                               := FALSE;
   L_buyer_temp_rec BUYER%ROWTYPE;
   L_buyer          NUMBER(4,0);
   L_return_code    VARCHAR2(6);
BEGIN
   FOR rec IN C_SVC_BUYER(I_process_id,
                          I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;
      if rec.action is NULL 
         or rec.action NOT IN (action_new,
                               action_mod,
                               action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action IN (action_new) 
         and rec.pk_buyer_rid is NOT NULL
         and rec.buyer is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'BUYER',
                     'BUYER_ALREADY_EXIST');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del) 
         and rec.pk_buyer_rid is NULL
         and rec.buyer is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'BUYER',
                     'BUYER_NOT_EXIST');
         L_error := TRUE;
      end if;
      if rec.action IN (action_new,action_mod) 
         and rec.buyer_name  is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'BUYER_NAME',
                     'ENTER_BUYER_NAME');
         L_error := TRUE;
      end if;
      if PROCESS_BUYER_VAL(O_error_message,
                           L_error,
                           rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;
    
      if NOT L_error 
         and rec.action=action_new
         and nvl(rec.buyer,0) = nvl(NULL,0) then
                                
         NEXT_BUYER_NUMBER(L_buyer,
                           L_return_code,
                           O_error_message); 
       if L_return_code ='FALSE' then
            
            WRITE_ERROR(rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        rec.chunk_id,
                        L_table,
                        rec.row_seq,
                        'BUYER',
                        O_error_message);
            L_error := TRUE;
         else
            
            L_buyer_temp_rec.buyer:=L_buyer;
         end if;
      end if;
    
      if NOT L_error then
         if rec.action IN (action_mod,action_del,action_new)
            and nvl(rec.buyer,0) <> nvl(NULL,0) then
            L_buyer_temp_rec.buyer := rec.buyer;
         end if;
         
         
         L_buyer_temp_rec.buyer_fax       := rec.buyer_fax;
         L_buyer_temp_rec.buyer_phone     := rec.buyer_phone;
         L_buyer_temp_rec.buyer_name      := rec.buyer_name;
         L_buyer_temp_rec.create_id       := GET_USER;
         L_buyer_temp_rec.create_datetime := SYSDATE;
         if rec.action = action_new then
            if EXEC_BUYER_INS(O_error_message,
                              L_buyer_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error:=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_BUYER_UPD(O_error_message,
                              L_buyer_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error:=TRUE;            
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_BUYER_DEL(O_error_message,
                              L_buyer_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error:=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_BUYER;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_MERCHANT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error         IN OUT BOOLEAN ,
                              I_rec           IN     C_SVC_MERCHANT%ROWTYPE)
RETURN BOOLEAN IS
   L_assoc_exists  BOOLEAN;
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_MERCHANT';
   L_var           VARCHAR2(1)                          := NULL;
   cursor C_DEPS is
      select 'x'
        from deps
       where deps.merch = I_rec.merch;

   cursor C_GROUPS is
      select 'x'
        from groups
       where groups.merch = I_rec.merch;

   cursor C_DIV is
      select 'x'
        from division
       where division.merch = I_rec.merch;

BEGIN
   if I_rec.action=action_del then
      open C_DIV;
      fetch C_DIV into L_var;
      if C_DIV%FOUND then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MERCH',
                     'CANT_DEL_MERCH_DIV');
         O_error :=TRUE;
      end if;
      close C_DIV;
      
      open C_GROUPS;
      fetch C_GROUPS into L_var;
      if C_GROUPS%FOUND then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MERCH',
                     'CANT_DEL_MERCH_GRP');
         O_error :=TRUE;
      end if;
      close C_GROUPS;
      
      open C_DEPS;
      fetch C_DEPS into L_var;
      if C_DEPS%FOUND then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MERCH',
                     'CANT_DEL_MERCH_DEPS');
         O_error :=TRUE;
      end if;
      close C_DEPS;
   end if;
   
   if I_rec.action    = action_new
      and I_rec.merch = 0 then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'MERCH',
                  'MERCH_NUM_ZERO');
      O_error :=TRUE;
   end if;
      
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             NULL, 
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_VAL_MERCHANT;
--------------------------------------------------------------------------------------
FUNCTION EXEC_MERCHANT_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_merchant_temp_rec   IN       MERCHANT%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                        := 'CORESVC_BUYER_MERCHANT.EXEC_MERCHANT_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_MERCHANT';
BEGIN
   insert
     into merchant
   values I_merchant_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_MERCHANT_INS;
-----------------------------------------------------------------------------------------
FUNCTION EXEC_MERCHANT_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_merchant_temp_rec   IN       MERCHANT%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                        := 'CORESVC_BUYER_MERCHANT.EXEC_MERCHANT_UPD';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_MERCHANT';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor c_merchant is
      select 'x'
        from merchant
       where merch = I_merchant_temp_rec.merch
         for update nowait;
BEGIN
   open c_merchant;
   close c_merchant;
   update merchant
      set row = I_merchant_temp_rec
    where merch = I_merchant_temp_rec.merch;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_merchant_temp_rec.merch,
                                             NULL);
      
      return FALSE;

   when OTHERS then                        
      if c_merchant%ISOPEN then
         close c_merchant;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_MERCHANT_UPD;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_MERCHANT_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_merchant_temp_rec   IN       MERCHANT%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_BUYER_MERCHANT.EXEC_MERCHANT_DEL';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_MERCHANT';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor c_merchant is
      select 'x'
        from merchant
       where merch = I_merchant_temp_rec.merch
         for update nowait;

BEGIN
   open c_merchant;
   close c_merchant;
   delete
     from merchant
    where merch = I_merchant_temp_rec.merch;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_merchant_temp_rec.merch,
                                             NULL);
      
      return FALSE;

   when OTHERS then
      if c_merchant%ISOPEN then
         close c_merchant;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_MERCHANT_DEL;
--------------------------------------------------------------------------------------------
FUNCTION PROCESS_MERCHANT(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE, 
                          I_process_id    IN      SVC_MERCHANT.PROCESS_ID%TYPE,
                          I_chunk_id      IN      SVC_MERCHANT.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_error             BOOLEAN;
   L_process_error     BOOLEAN;
   L_program           VARCHAR2(64)                       :='CORESVC_BUYER_MERCHANT.PROCESS_MERCHANT';
   L_merchant_temp_rec MERCHANT%ROWTYPE;
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  :='SVC_MERCHANT';
BEGIN
   FOR rec IN C_SVC_MERCHANT(I_process_id,
                             I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;
      
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      
      if rec.action = action_new
         and rec.pk_merchant_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MERCH',
                     'MERCH_EXIST');
         L_error :=TRUE;
      end if;
      
      if rec.action IN (action_new,action_mod)
         and rec.merch_name IS NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MERCH_NAME',
                     'ENTER_MERCH_NAME');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.merch is NOT NULL
         and rec.PK_MERCHANT_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MERCH',
                     'MERCH_NOT_EXIST');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_new)
         and rec.merch is NOT NULL
         and rec.merch < 0 then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MERCH',
                     'GREATER_0');
         L_error :=TRUE;
      end if;
      
      if PROCESS_VAL_MERCHANT(O_error_message,
                              L_error,
                              rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
                           
      
      if NOT L_error then
         L_merchant_temp_rec.merch_fax               := rec.merch_fax;
         L_merchant_temp_rec.merch_phone             := rec.merch_phone;
         L_merchant_temp_rec.merch_name              := rec.merch_name;
         L_merchant_temp_rec.merch                   := rec.merch;
         L_merchant_temp_rec.create_id               := LP_user;
         L_merchant_temp_rec.create_datetime         := SYSDATE;
         
         if rec.action = action_new then
            if EXEC_MERCHANT_INS( O_error_message,
                                  L_merchant_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_MERCHANT_UPD( O_error_message,
                                  L_merchant_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_MERCHANT_DEL( O_error_message,
                                  L_merchant_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_MERCHANT;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                    := 'CORESVC_BUYER_MERCHANT.PROCESS';
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';

BEGIN
   LP_errors_tab := NEW LP_errors_tab_typ();
   if PROCESS_BUYER(O_error_message,
                    I_process_id,
                    I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_MERCHANT(O_error_message,
                       I_process_id,
                       I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_COUNT := LP_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert 
        into svc_admin_upld_er
      values LP_errors_tab( i );
   LP_errors_tab := NEW LP_errors_tab_typ();
   
      if O_error_count=0 then
      L_process_status:='PS';
   else
      L_process_status:='PE';
   end if;
   
   update svc_process_tracker
      set status =(case 
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=SYSDATE
    where process_id=I_process_id;
   COMMIT; 
   CLEAR_STAGING_DATA(I_process_id);

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS;
END CORESVC_BUYER_MERCHANT;
----------------------------------------------------
/