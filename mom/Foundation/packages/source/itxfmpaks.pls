
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_XFORM_PACK_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------
-- Function Name: NEXT_XFORM_ID
--       Purpose: Generates the next tsf_xform_id from the sequence TSF_XFORM_ID_SEQ.

FUNCTION NEXT_XFORM_ID(O_error_message    IN OUT VARCHAR2,
                       O_return_code      IN OUT BOOLEAN,
                       O_tsf_xform_id     IN OUT TSF_XFORM.TSF_XFORM_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: NEXT_XFORM_DETAIL_ID
--       Purpose: Generates the next tsf_xform_detail_id from the sequence 
--                TSF_XFORM_DETAIL_ID_SEQ.

FUNCTION NEXT_XFORM_DETAIL_ID(O_error_message         IN OUT VARCHAR2,
                              O_return_code           IN OUT BOOLEAN,
                              O_tsf_xform_detail_id   IN OUT TSF_XFORM_DETAIL.TSF_XFORM_DETAIL_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: NEXT_PACKING_ID
--       Purpose: Generates the next tsf_packing_id from the sequence TSF_PACKING_ID_SEQ.

FUNCTION NEXT_PACKING_ID(O_error_message    IN OUT VARCHAR2,
                         O_return_code      IN OUT BOOLEAN,
                         O_tsf_packing_id   IN OUT TSF_PACKING.TSF_PACKING_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: NEXT_PACK_SET_NO
--       Purpose: Generates the next available set number for TSF_PACKING.

FUNCTION NEXT_PACK_SET_NO(O_error_message  IN OUT VARCHAR2,
                          O_set_no         IN OUT TSF_PACKING.SET_NO%TYPE,
                          I_tsf_no         IN     TSF_PACKING.TSF_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: NEXT_PACKING_DETAIL_ID
--       Purpose: Generates the next tsf_packing_detail_id from the sequence TSF_PACKING_DETAIL_ID_SEQ.

FUNCTION NEXT_PACKING_DETAIL_ID(O_error_message           IN OUT VARCHAR2,
                                O_return_code             IN OUT BOOLEAN,
                                O_tsf_packing_detail_id   IN OUT TSF_PACKING_DETAIL.TSF_PACKING_DETAIL_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------- 
-- Function Name: VALID_XFORM_ITEM
--       Purpose: Validates whether the input item is eligible to be tranformed.

FUNCTION VALID_XFORM_ITEM(O_error_message   IN OUT VARCHAR2,
                          O_valid           IN OUT BOOLEAN,
                          O_item_desc       IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                          O_qty             IN OUT TSFDETAIL.TSF_QTY%TYPE,
                          I_tsf_no          IN     TSFDETAIL.TSF_NO%TYPE,
                          I_item            IN     TSFDETAIL.ITEM%TYPE,
                          I_diff_id         IN     TSF_XFORM_REQUEST.DIFF_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: XFORM_ITEM
--       Purpose: This function transforms and places transformation information on the table 
--                tsf_xform_detail_temp. Called by the form itxforms.

FUNCTION XFORM_ITEM(O_error_message      IN OUT VARCHAR2,
                    O_partial_xform      IN OUT BOOLEAN,
                    I_tsf_no             IN     TSF_XFORM.TSF_NO%TYPE,
                    I_finisher           IN     TSFHEAD.FROM_LOC%TYPE,
                    I_finisher_type      IN     TSFHEAD.FROM_LOC_TYPE%TYPE,
                    I_to_loc             IN     TSFHEAD.TO_LOC%TYPE,
                    I_to_loc_type        IN     TSFHEAD.TO_LOC_TYPE%TYPE,
                    I_to_item            IN     TSFDETAIL.ITEM%TYPE,
                    I_to_diff_id         IN     TSF_XFORM_REQUEST.DIFF_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: CREATE_TSF_XFORM
--       Purpose: This function copies item transformation data from tsf_xform_detail_temp 
--                table into the resultant tables tsf_xform/tsf_xform_detail for the specific
--                transfer and then remove corresponding data from temporary table.

FUNCTION CREATE_TSF_XFORM(O_error_message   IN OUT VARCHAR2,
                          I_tsf_no          IN     TSF_XFORM.TSF_NO%TYPE)
RETURN BOOLEAN;  
------------------------------------------------------------------------------------------
-- Function Name: DELETE_TSF_XFORM_HEAD
--       Purpose: This function is called from the Xform Results screen when the 
--                'Delete' button is clicked.  It will delete the header info from 
--                the TSF_XFORM table.

FUNCTION DELETE_TSF_XFORM_HEAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_xform_id    IN     TSF_XFORM.TSF_XFORM_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: DELETE_TSF_XFORM_REQUEST
--       Purpose: This function deletes transformation request for a specific transfer.

FUNCTION DELETE_TSF_XFORM_REQUEST(O_error_message    IN OUT VARCHAR2,
                                  I_tsf_no           IN     TSF_XFORM_REQUEST.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: GENERATE_TSF_PACKING_HEAD
--       Purpose: This function insert an entry into the table tsf_packing and bring back
--                the tsf_packing_id and set_no.
FUNCTION GENERATE_TSF_PACKING_HEAD(O_error_message    IN OUT VARCHAR2,
                                   O_tsf_packing_id   IN OUT TSF_PACKING.TSF_PACKING_ID%TYPE,
                                   O_set_no           IN OUT TSF_PACKING.SET_NO%TYPE,
                                   I_tsf_no           IN     TSF_PACKING.TSF_NO%TYPE)
RETURN BOOLEAN;   
------------------------------------------------------------------------------------------
-- Function Name: DELETE_TSF_PACKING_DETAIL
--       Purpose: This function deletes the whole set of tsf_packing_detail if exists.
FUNCTION DELETE_TSF_PACKING_DETAIL(O_error_message    IN OUT VARCHAR2,
                                   I_tsf_packing_id   IN     TSF_PACKING.TSF_PACKING_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: VALID_PACKING_ITEM
--       Purpose: Validates whether the input item is eligible to be packed.

FUNCTION VALID_PACKING_ITEM(O_error_message   IN OUT VARCHAR2,
                            O_valid           IN OUT BOOLEAN,
                            O_item_desc       IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                            O_qty             IN OUT TSFDETAIL.TSF_QTY%TYPE,
                            I_tsf_no          IN     TSFDETAIL.TSF_NO%TYPE,
                            I_item            IN     TSFDETAIL.ITEM%TYPE,
                            I_diff_id         IN     TSF_PACKING_DETAIL.DIFF_ID%TYPE)
RETURN BOOLEAN;          
------------------------------------------------------------------------------------------
-- Function Name: EXPLODE_PACK
--       Purpose: For a given pack, it will place all of its components and quantities onto
--                the pack results table.

FUNCTION EXPLODE_PACK(O_error_message      IN OUT VARCHAR2,
                      I_pack_no            IN     TSF_PACKING_DETAIL.ITEM%TYPE,
                      I_tsf_packing_id     IN     TSF_PACKING_DETAIL.TSF_PACKING_ID%TYPE,
                      I_tsf_no             IN     TSF_XFORM.TSF_NO%TYPE,
                      I_qty                IN     TSF_PACKING_DETAIL.QTY%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------ 
-- Function Name: DELETE_TSF_PACKING
--       Purpose: This function is called from the Packing Results screen when the 
--                'Delete' button is clicked.  It will delete the "set" of items from the 
--                TSF_PACKING and TSF_PACKING_DETAIL tables.

FUNCTION DELETE_TSF_PACKING(O_error_message   IN OUT VARCHAR2,
                            I_tsf_no          IN     TSF_PACKING.TSF_NO%TYPE,
                            I_set_no          IN     TSF_PACKING.SET_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: TSF_PACKING_ITEM_EXISTS
--       Purpose: This function checks to see whether or not there exists a record in the 
--                table tsf_packing_detail for a specific tsf_packing_id/record_type/item.

FUNCTION TSF_PACKING_ITEM_EXISTS(O_error_message   IN OUT VARCHAR2,
                                 O_EXISTS          IN OUT BOOLEAN,
                                 I_tsf_packing_id  IN     TSF_PACKING_DETAIL.TSF_PACKING_ID%TYPE,
                                 I_record_type     IN     TSF_PACKING_DETAIL.RECORD_TYPE%TYPE,
                                 I_item            IN     TSF_PACKING_DETAIL.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: CHECK_DUPLICATE_XFORM_REQUEST
--       Purpose: Check for duplicate from items on TSF_XFORM_REQUEST

FUNCTION CHECK_DUPLICATE_XFORM_REQUEST(O_error_message  IN OUT VARCHAR2,
                                       O_duplicate      IN OUT BOOLEAN,
                                       I_tsf_no         IN     TSF_XFORM_REQUEST.TSF_NO%TYPE,
                                       I_item           IN     TSF_XFORM_REQUEST.ITEM%TYPE,
                                       I_diff_id        IN     TSF_XFORM_REQUEST.DIFF_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: BUILD_PACK
--       Purpose: To build the specific pack based on the input transfer. The indicator
--                O_bulk_item_left identifies whether there leaves some bulk items after 
--                building pack; O_short_qty_ind equals TRUE only when there is not enough 
--                quantity of from-items to build the pack(when function returns FALSE).

FUNCTION BUILD_PACK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_bulk_item_left  IN OUT BOOLEAN,
                    O_short_qty_ind   IN OUT BOOLEAN,
                    I_tsf_no          IN TSF_PACKING.TSF_NO%TYPE,
                    I_tsf_packing_id  IN TSF_PACKING.TSF_PACKING_ID%TYPE,
                    I_to_pack_no      IN PACKITEM.PACK_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Function Name:  GET_NAME
--- Purpose:        Gets the descriptions of the items in the transformation process.
-------------------------------------------------------------------------------------------
FUNCTION GET_XFORM_ITEM_DESC(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             O_from_item_desc   IN OUT    ITEM_MASTER.ITEM_DESC%TYPE,
                             O_to_item_desc     IN OUT    ITEM_MASTER.ITEM_DESC%TYPE,
                             I_from_item        IN        ITEM_MASTER.ITEM%TYPE,
                             I_to_item          IN        ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Function Name:  GET_FILTER_DIFF_INFO
--- Purpose:        Validates diff and gets description for filter.
-------------------------------------------------------------------------------------------
FUNCTION GET_FILTER_DIFF_INFO(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                              O_diff_desc        IN OUT    DIFF_IDS.DIFF_DESC%TYPE,
                              I_tsf_no           IN OUT    TSFHEAD.TSF_NO%TYPE,
                              I_to_from          IN        VARCHAR2,
                              I_diff             IN        DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Function Name:  GET_FILTER_ITEM_INFO
--- Purpose:        Validates item and gets description for filter.
-------------------------------------------------------------------------------------------
FUNCTION GET_FILTER_ITEM_INFO(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                              O_item_desc        IN OUT    ITEM_MASTER.ITEM_DESC%TYPE,
                              I_tsf_no           IN OUT    TSFHEAD.TSF_NO%TYPE,
                              I_to_from          IN        VARCHAR2,
                              I_item             IN        ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: TSF_XFORM_EXISTS
--       Purpose: Checks to see if item transformation records exist for a transfer.
--                Called from tsffind.fmb.

FUNCTION TSF_XFORM_EXISTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_xform_exist     IN OUT BOOLEAN,
                          I_tsf_no          IN     TSF_PACKING.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: XFORM_PACKING_EXIST
--       Purpose: This function will check if transformation or packing instructions exist.

FUNCTION XFORM_PACKING_EXIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_xform_exists    IN OUT BOOLEAN,
                             O_packing_exists  IN OUT BOOLEAN,
                             I_tsf_no          IN     TSFDETAIL.TSF_NO%TYPE,
                             I_item            IN     TSFDETAIL.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: FIND_SET
--       Purpose: Given a tsf_no, item, and it's type, determine and return what packing set
--                it is a part of.

FUNCTION FIND_SET (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_set           IN OUT TSF_PACKING.SET_NO%TYPE,
                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                   I_item_type     IN     TSF_PACKING_DETAIL.RECORD_TYPE%TYPE,
                   I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: DELETE_SET
--       Purpose: To delete a packing set.

FUNCTION DELETE_SET (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_pack_id       IN     TSF_PACKING.TSF_PACKING_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEM
--       Purpose: To check to see if the item entered in the search block of the packing
--                results screen is a valid item.  

FUNCTION VALIDATE_ITEM (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists        IN OUT BOOLEAN,
                        I_item_id       IN OUT ITEM_MASTER.ITEM%TYPE,
                        I_item_type     IN OUT TSF_PACKING_DETAIL.RECORD_TYPE%TYPE,
                        I_tsf_no        IN OUT TSF_PACKING.TSF_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Function Name:  DELETE_XFORM_PACK
--- Purpose:        Deletes details from packing and xform tables.
--------------------------------------------------------------------------------------------
FUNCTION DELETE_XFORM_PACK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_to_item         IN     ITEM_MASTER.ITEM%TYPE,
                           I_from_item       IN     ITEM_MASTER.ITEM%TYPE,
                           I_tsf_no          IN     TSF_PACKING.TSF_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION TSF_XFORM_PACK_ITEM_EXISTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_to_item_exist   IN OUT BOOLEAN,
                                    O_from_item_exist IN OUT BOOLEAN,
                                    I_tsf_no          IN     TSF_PACKING.TSF_NO%TYPE,
                                    I_to_item         IN     ITEM_MASTER.ITEM%TYPE,
                                    I_from_item       IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
--- Function Name:  UPDATE_XFORM_QTY
--- Purpose:        Update the Item transformation Qtys.

FUNCTION UPDATE_XFORM_QTY (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_xform_item      IN OUT TSF_XFORM_DETAIL.TO_ITEM%TYPE,
                           I_xform_tsf_no    IN     TSFDETAIL.TSF_NO%TYPE,
                           I_xform_item      IN     TSF_XFORM_DETAIL.FROM_ITEM%TYPE,
                           I_xform_qty       IN     TSF_XFORM_DETAIL.FROM_QTY%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--- Function Name:  UPDATE_XFORM_PACK_RESULT
--- Purpose:        Update the Item transformation and Packing records when the transfer
---                 quantity is changed from the Transfer Detail form.

FUNCTION UPDATE_XFORM_PACK_RESULT (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_tsf_no              IN     TSFDETAIL.TSF_NO%TYPE,
                                   I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                   I_changed_qty         IN     TSF_PACKING_DETAIL.QTY%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: GET_XFORM_TO_ITEM
--       Purpose: This function queries the tsf_xform_detail table to get the to_item for the 
--                from_item parameter.  If found, O_exists is true.  Otherwise, O_exists is 
--                false.
FUNCTION GET_XFORM_TO_ITEM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           O_to_item        IN OUT ITEM_MASTER.ITEM%TYPE,
                           I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                           I_from_item      IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: POPULATE_PACKING_FROM_ITEMS
--       Purpose: Pupolate "from-items" in tsfpack form to tsf_packing_detail table as 'F' 
--                record, if it is a parent, blow down to sku level.

FUNCTION POPULATE_PACKING_FROM_ITEMS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_tsf_no         IN     TSF_PACKING.TSF_NO%TYPE,
                                     I_tsf_packing_id IN     TSF_PACKING.TSF_PACKING_ID%TYPE,
                                     I_item           IN     TSF_PACKING_DETAIL.ITEM%TYPE,
                                     I_diff_id        IN     TSF_PACKING_DETAIL.DIFF_ID%TYPE,
                                     I_qty            IN     TSF_PACKING_DETAIL.QTY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: DELETE_PACKING_FROM_ITEMS
--       Purpose: Delete "from-items" from tsf_packing_detail table, if it is a parent item,
--                delete all its children.

FUNCTION DELETE_PACKING_FROM_ITEMS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_tsf_no         IN     TSF_PACKING.TSF_NO%TYPE,
                                   I_tsf_packing_id IN     TSF_PACKING.TSF_PACKING_ID%TYPE,
                                   I_item           IN     TSF_PACKING_DETAIL.ITEM%TYPE,
                                   I_diff_id        IN     TSF_PACKING_DETAIL.DIFF_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: POPULATE_XFORM_REQUEST
--       Purpose: This function will populate from-items on tsfxform.fmb into the tsf_xform_request
--                table as 'F' record type for the sake of excluding the selected items on the 
--                from items LOV.

FUNCTION POPULATE_XFORM_REQUEST(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                                I_item           IN     TSF_XFORM_REQUEST.ITEM%TYPE,
                                I_diff_id        IN     TSF_XFORM_REQUEST.DIFF_ID%TYPE,
                                I_qty            IN     TSF_XFORM_REQUEST.QTY%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: DELETE_XFORM_REQUEST_ITEM
--       Purpose: Delete the selected item from the tsf_xform_request table. Called by the delete
--                button on the form block of the form tsfxform.

FUNCTION DELETE_XFORM_REQUEST_ITEM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                                   I_item           IN     TSF_XFORM_REQUEST.ITEM%TYPE,
                                   I_diff_id        IN     TSF_XFORM_REQUEST.DIFF_ID%TYPE)
RETURN BOOLEAN; 
-----------------------------------------------------------------------------------------------
-- Function Name: UNIT_RETAIL_EXISTS
--       Purpose: Check to see whether the input item has set up unit retail at the specific
--                location. For a pack item, blow down to component level; for parent item,
--                check all the children. Only all the involved item/components/children has
--                set up unit_retail, then O_exist will return TRUE; otherwise, return FALSE.

FUNCTION UNIT_RETAIL_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         IN OUT BOOLEAN,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_diff_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                            I_loc            IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_PACK_REC
--       Purpose: To check what records has been populated for the pack.
-------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_REC(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_packing_rec_exists           OUT BOOLEAN,
                           O_packing_from_item_exists     OUT BOOLEAN,
                           O_packing_to_item_exists       OUT BOOLEAN,
                           I_tsf_no                    IN     TSF_PACKING.TSF_NO%TYPE,
                           I_tsf_packing_id            IN     TSF_PACKING.TSF_PACKING_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------

END ITEM_XFORM_PACK_SQL;
/
