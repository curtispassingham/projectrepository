create or replace PACKAGE BODY CORESVC_CURRENCIES AS
   cursor C_SVC_CURRENCIES(I_process_id NUMBER,
                           I_chunk_id NUMBER) is
      select pk_currencies.rowid  as pk_currencies_rid,
             st.rowid             as st_rid,
             UPPER(st.currency_code) as currency_code,
             st.currency_desc,
             UPPER(st.currency_cost_fmt) as currency_cost_fmt,
             UPPER(st.currency_rtl_fmt) as currency_rtl_fmt,
             st.currency_cost_dec,
             st.currency_rtl_dec,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             upper(st.action)      as action,
             st.process$status
        from svc_currencies st,
             currencies pk_currencies
       where st.process_id            = I_process_id
         and st.chunk_id              = I_chunk_id
         and UPPER(st.currency_code)         = pk_currencies.currency_code (+);

   Type CURRENCIES_TL_TAB IS TABLE OF CURRENCIES_TL%ROWTYPE;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
---------------------------------------cursor for CURRENCY_RATES-----------------------------
   cursor C_SVC_CURRENCY_RATES(I_process_id NUMBER,
                               I_chunk_id NUMBER)is
      select pk_currency_rates.rowid                                 as pk_currency_rates_rid,
             st.rowid                                                as st_rid,
             crt_cur_fk.rowid                                        as crt_cur_fk_rid,
             UPPER(st.exchange_rate)                                 as exchange_rate,
             st.exchange_type,
             st.effective_date,
             UPPER(st.currency_code)                                 as currency_code,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             LEAD(upper(st.currency_code)) over (order by upper(st.currency_code)) as next_curr_code,
             upper(st.action) as action,
                     ROW_NUMBER() OVER (PARTITION BY upper(st.currency_code) ORDER BY upper(st.currency_code)) as c_rank,
             st.process$status
        from svc_currency_rates st,
             currency_rates pk_currency_rates,
             currencies crt_cur_fk
       where st.process_id      = I_process_id
         and st.chunk_id        = I_chunk_id
         and st.exchange_type   = pk_currency_rates.exchange_type (+)
         and st.effective_date  = pk_currency_rates.effective_date (+)
         and upper(st.currency_code)   = pk_currency_rates.currency_code (+)
         and upper(st.currency_code)   = crt_cur_fk.currency_code (+)
       order by st.currency_code,c_rank;

   TYPE LP_errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab LP_errors_tab_typ;
   TYPE LP_s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab       LP_s9t_errors_tab_typ;
   LP_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   LP_primary_currency     CURRENCIES.CURRENCY_CODE%TYPE;
   LP_participating_ind    BOOLEAN;
   LP_prim_euro_ind        VARCHAR2(1);
   LP_vdate                PERIOD.VDATE%TYPE                   := GET_VDATE;
   L_rowseq_count               NUMBER;
   c                            NUMBER      := 1;
   TYPE L_row_seq_tab_type IS TABLE OF SVC_CURRENCY_RATES.ROW_SEQ%TYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   L_row_seq_tab                       L_row_seq_tab_type;

   LP_primary_lang    LANG.LANG%TYPE;

----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2)IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key             :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
---------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
------------------------------------------------------------------------------
FUNCTION PROCESS_CURRENCIES_VAL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error           IN OUT   BOOLEAN,
                                 I_rec             IN       C_SVC_CURRENCIES%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_CURRENCIES.PROCESS_CURRENCIES_VAL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CURRENCIES';
   L_length        NUMBER(3)                         := 0;
   L_position      NUMBER(3)                         := 1;
   L_dec           NUMBER(3)                         := 0;
   L_currency_code CURRENCIES.CURRENCY_CODE%TYPE;
   cursor C_CHECK_RATE_BOTH(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
      select currency_code
        from currencies
       where (currency_code NOT in (select currency_rates.currency_code
                                     from currency_rates,
                                          currencies
                                    where currencies.currency_code=CURR_CODE
                                      and currencies.currency_code = currency_rates.currency_code
                                      and currency_rates.exchange_type = 'C'
                                      and currency_rates.effective_date <= LP_vdate)
              or currency_code NOT in (select currency_rates.currency_code
                                         from currency_rates,
                                              currencies
                                        where currencies.currency_code=CURR_CODE
                                          and currencies.currency_code = currency_rates.currency_code
                                          and currency_rates.exchange_type = 'O'
                                          and currency_rates.effective_date <= LP_vdate))
        and currency_code = CURR_CODE;

   cursor C_CHECK_RATE(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
      select currency_code
        from currencies
       where currency_code NOT in (select currency_rates.currency_code
                                     from currency_rates,
                                          currencies
                                    where currencies.currency_code=CURR_CODE
                                      and currencies.currency_code = currency_rates.currency_code
                                      and currency_rates.exchange_type = 'O'
                                      and currency_rates.effective_date <= LP_vdate)
         and currency_code = CURR_CODE;


   cursor C_CHECK_RATE_BOTH_EURO(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
      select currency_code
        from currencies
       where currency_code NOT in ('ATS', 'BEF', 'FIM', 'FRF', 'DEM', 'IEP', 'ITL',
                                   'LUF', 'NLG', 'PTE', 'ESP','GRD')
         and ((currency_code NOT in (select currency_rates.currency_code
                                     from currency_rates,
                                          currencies
                                    where currencies.currency_code=CURR_CODE
                                      and currencies.currency_code = currency_rates.currency_code
                                      and currency_rates.exchange_type = 'C'
                                      and currency_rates.effective_date <= LP_vdate)
              or currency_code NOT in (select currency_rates.currency_code
                                         from currency_rates,
                                              currencies
                                        where currencies.currency_code=CURR_CODE
                                          and currencies.currency_code = currency_rates.currency_code
                                          and currency_rates.exchange_type = 'O'
                                          and currency_rates.effective_date <= LP_vdate)))
        and currency_code = CURR_CODE;

   cursor C_CHECK_RATE_EURO(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
      select currency_code
        from currencies
       where currency_code NOT in ('ATS', 'BEF', 'FIM', 'FRF', 'DEM', 'IEP', 'ITL',
                                   'LUF', 'NLG', 'PTE', 'ESP','GRD')
         and (currency_code NOT in (select currency_rates.currency_code
                                     from currency_rates,
                                          currencies
                                    where currencies.currency_code = currency_rates.currency_code
                                      and currency_rates.exchange_type = 'O'
                                      and currency_rates.effective_date <= LP_vdate))
         and currency_code = CURR_CODE;
BEGIN

   if I_rec.action IN (action_new,action_mod) then

      L_length := Length(I_rec.currency_cost_fmt);
      LOOP
         if SUBSTR(I_rec.currency_cost_fmt,
                   L_position,
                   1) = 'D' then
            LOOP
               if SUBSTR(I_rec.currency_cost_fmt,
                         L_position,
                         1) in ('9', '0') then
                  L_dec := L_dec + 1;
               end if;
               if L_length = L_position then
                  Exit;
               else
                  L_position := L_position + 1;
               end if;
            END LOOP;
         end if;
         L_position := L_position + 1;
         if L_length <= L_position then
            Exit;
         end if;
      END LOOP;
      if L_dec != I_rec.currency_cost_dec then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CURRENCY_COST_DEC',
                     'CURR_DEC_MASK');
         O_error:=TRUE;
      end if;
      L_position := 1;
      L_dec := 0;
      L_length := Length(I_rec.currency_rtl_fmt);
      LOOP
         if SUBSTR(I_rec.currency_rtl_fmt,
                   L_position,
                   1) = 'D' then
            LOOP
               if SUBSTR(I_rec.currency_rtl_fmt,
                         L_position,
                         1) in ('9', '0') then
                  L_dec := L_dec + 1;
               end if;
               if L_length = L_position then
                  Exit;
               else
                  L_position := L_position + 1;
               end if;
            END LOOP;
         end if;
         L_position := L_position + 1;
         if L_length <= L_position then
            Exit;
         end if;
      END LOOP;
      if L_dec != I_rec.currency_rtl_dec then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CURRENCY_RTL_DEC',
                     'CURR_DEC_MASK');
         O_error:=TRUE;

      end if;
   end if;

   if I_rec.action=action_del
      and I_rec.pk_currencies_rid is NOT NULL then
      if LP_prim_euro_ind = 'Y' then
         if LP_system_options_row.consolidation_ind = 'Y' then

            open C_CHECK_RATE_BOTH(I_rec.currency_code);
            fetch C_CHECK_RATE_BOTH into L_currency_code;
            if C_CHECK_RATE_BOTH%NOTFOUND then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'CURRENCY_CODE',
                           'DEL_NOT_ALLOWED');
               O_error:=TRUE;
               close C_CHECK_RATE_BOTH;
            end if;
         else
            open C_CHECK_RATE(I_rec.currency_code);
            fetch C_CHECK_RATE into L_currency_code;
            if C_CHECK_RATE%NOTFOUND then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'CURRENCY_CODE',
                           'DEL_NOT_ALLOWED');
               O_error:=TRUE;
               close C_CHECK_RATE;
            end if;
         end if;
      else
         if LP_system_options_row.consolidation_ind = 'Y' then
            open C_CHECK_RATE_BOTH_EURO(I_rec.currency_code);
            fetch C_CHECK_RATE_BOTH_EURO into L_currency_code;
            if C_CHECK_RATE_BOTH_EURO%NOTFOUND then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'CURRENCY_CODE',
                           'DEL_NOT_ALLOWED');
               O_error:=TRUE;
               close C_CHECK_RATE_BOTH_EURO;
            end if;
         else
            open C_CHECK_RATE_EURO(I_rec.currency_code);
            fetch C_CHECK_RATE_EURO into L_currency_code;
            if C_CHECK_RATE_EURO%NOTFOUND then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'CURRENCY_CODE',
                           'DEL_NOT_ALLOWED');
               O_error:=TRUE;
               close C_CHECK_RATE_EURO;
            end if;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CHECK_RATE%ISOPEN then
         close C_CHECK_RATE;
      end if;
      if C_CHECK_RATE_BOTH%ISOPEN then
         close C_CHECK_RATE_BOTH;
      end if;
      if C_CHECK_RATE_EURO%ISOPEN then
         close C_CHECK_RATE_EURO;
      end if;
      if C_CHECK_RATE_BOTH_EURO%ISOPEN then
         close C_CHECK_RATE_BOTH_EURO;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CURRENCIES_VAL;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CURRENCY_RATES_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error           IN OUT   BOOLEAN,
                                    I_rec             IN       C_SVC_CURRENCY_RATES%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                         := 'CORESVC_CURRENCIES.PROCESS_CURRENCY_RATES_VAL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    := 'SVC_CURRENCY_RATES';
   L_code_desc CODE_DETAIL.CODE_DESC%TYPE;
BEGIN
   if I_rec.action =action_new then
      if LP_system_options_row.consolidation_ind='Y' then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'EXTP',
                                       I_rec.exchange_type ,
                                       L_code_desc ) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'EXCHANGE_TYPE',
                        'CHK_CURR_RATES_EXC_TYPE');
            O_error :=TRUE;
         end if;
      else
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'OXTP',
                                       I_rec.exchange_type ,
                                       L_code_desc ) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'EXCHANGE_TYPE',
                        'CHK_CURR_RATES_EXC_TYPE');
            O_error :=TRUE;
         end if;
      end if;
      if I_rec.exchange_rate<=0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'EXCHANGE_RATE',
                     'GREATER_0');
         O_error :=TRUE;
      end if;
   end if;
    return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CURRENCY_RATES_VAL;
------------------------------------VALIDATION FOR CURR RATES------------------------------------------------------------------
FUNCTION PROCESS_CURR_RATES_AFTER_VAL( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_error            IN OUT   BOOLEAN,
                                       I_rec              IN       C_SVC_CURRENCY_RATES%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                         := 'CORESVC_CURRENCIES.PROCESS_CURRENCY_RATES_AFTER_VAL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    := 'SVC_CURRENCY_RATES';
   L_currency_code CURRENCIES.CURRENCY_CODE%TYPE;
   cursor C_CHECK_RATE_BOTH(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
      select currency_code
        from currencies
       where (currency_code NOT IN (select currency_rates.currency_code
                                     from currency_rates,
                                          currencies
                                    where currencies.currency_code = currency_rates.currency_code
                                      and currency_rates.exchange_type = 'C'
                                      and currency_rates.effective_date <= LP_vdate)
              or currency_code NOT IN (select currency_rates.currency_code
                                         from currency_rates,
                                              currencies
                                        where currencies.currency_code = currency_rates.currency_code
                                          and currency_rates.exchange_type = 'O'
                                          and currency_rates.effective_date <= LP_vdate))
        and currency_code=CURR_CODE;

   cursor C_CHECK_RATE(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
      select currency_code
        from currencies
       where currency_code NOT IN (select currency_rates.currency_code
                                     from currency_rates,
                                          currencies
                                    where currencies.currency_code = currency_rates.currency_code
                                      and currency_rates.exchange_type = 'O'
                                      and currency_rates.effective_date <= LP_vdate)
         and currency_code=CURR_CODE;

   cursor C_CHECK_RATE_BOTH_EURO(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
      select currency_code
        from currencies
       where currency_code NOT IN ('ATS', 'BEF', 'FIM', 'FRF', 'DEM', 'IEP', 'ITL',
                                   'LUF', 'NLG', 'PTE', 'ESP','GRD')
         and ((currency_code NOT IN (select currency_rates.currency_code
                                     from currency_rates,
                                          currencies
                                    where currencies.currency_code = currency_rates.currency_code
                                      and currency_rates.exchange_type = 'C'
                                      and currency_rates.effective_date <= LP_vdate)
              or currency_code NOT IN (select currency_rates.currency_code
                                         from currency_rates,
                                              currencies
                                        where currencies.currency_code = currency_rates.currency_code
                                          and currency_rates.exchange_type = 'O'
                                          and currency_rates.effective_date <= LP_vdate)))
         and currency_code=CURR_CODE;
   cursor C_CHECK_RATE_EURO(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
      select currency_code
        from currencies
       where currency_code NOT IN ('ATS', 'BEF', 'FIM', 'FRF', 'DEM', 'IEP', 'ITL',
                                   'LUF', 'NLG', 'PTE', 'ESP','GRD')
         and (currency_code NOT IN (select currency_rates.currency_code
                                     from currency_rates,
                                          currencies
                                    where currencies.currency_code = currency_rates.currency_code
                                      and currency_rates.exchange_type = 'O'
                                      and currency_rates.effective_date <= LP_vdate))
         and currency_code=CURR_CODE;

    cursor C_EXIST(CURR_CODE CURRENCIES.CURRENCY_CODE%TYPE) is
       select 'X'
          from currency_rates
         where currency_code=CURR_CODE;

   L_exist VARCHAR2(1) := NULL;
BEGIN
   L_exist := NULL;
   open C_EXIST(I_rec.currency_code);
     fetch C_EXIST into L_exist;
     close C_EXIST;
   if I_rec.currency_code <> NVL(I_rec.next_curr_code,'-1')
      and ( I_rec.action=action_new or (I_rec.action=action_del
                                        and L_exist is NOT NULL
                                        and LP_system_options_row.financial_ap is NULL
                                        and I_rec.pk_currency_rates_rid is NOT NULL)) then
      O_error := FALSE;
      L_rowseq_count := L_row_seq_tab.count();
          if LP_prim_euro_ind = 'Y' then
         if LP_system_options_row.consolidation_ind = 'Y' then
            open C_CHECK_RATE_BOTH(I_rec.currency_code);
            fetch C_CHECK_RATE_BOTH into L_currency_code;
            if C_CHECK_RATE_BOTH%FOUND then
               if L_rowseq_count > 0 then
                  for i in 1..L_rowseq_count
                  LOOP
                        WRITE_ERROR(I_rec.process_id,
                                SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                I_rec.chunk_id,
                                L_table,
                                L_row_seq_tab(i),
                                'EXCHANGE_TYPE',
                                'EXCH_RATES_REQ');
                  END LOOP;
               end if;
               ROLLBACK TO successful_curr_code;
               O_error:=TRUE;
               close C_CHECK_RATE_BOTH;
            else
               close C_CHECK_RATE_BOTH;
            end if;
         else
            open C_CHECK_RATE(I_rec.currency_code);
            fetch C_CHECK_RATE into L_currency_code;
            if C_CHECK_RATE%FOUND then
               if L_rowseq_count > 0 then
                  for i in 1..L_rowseq_count
                  LOOP
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              L_row_seq_tab(i),
                              'EXCHANGE_TYPE',
                              'REQ_OPER_EXCH_RATE');
                  END LOOP;
                         end if;
               ROLLBACK TO successful_curr_code;
               O_error:= TRUE;
               close C_CHECK_RATE;
            else
               close C_CHECK_RATE;
            end if;
         end if;
      else
         if LP_system_options_row.consolidation_ind = 'Y' then
            open C_CHECK_RATE_BOTH_EURO(I_rec.currency_code);
            fetch C_CHECK_RATE_BOTH_EURO into L_currency_code;
            if C_CHECK_RATE_BOTH_EURO%FOUND then
               if L_rowseq_count > 0 then
                  for i in 1..L_rowseq_count
                  LOOP
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              L_row_seq_tab(i),
                              'EXCHANGE_TYPE',
                              'EXCH_RATES_REQ');
                  END LOOP;
                         end if;
               ROLLBACK TO successful_curr_code;
               O_error:= TRUE;
               close C_CHECK_RATE_BOTH_EURO;
            else
               close C_CHECK_RATE_BOTH_EURO;
            end if;
         else
            open C_CHECK_RATE_EURO(I_rec.currency_code);
            fetch C_CHECK_RATE_EURO into L_currency_code;
            if C_CHECK_RATE_EURO%FOUND then
               if L_rowseq_count > 0 then
                  for i in 1..L_rowseq_count
                  LOOP
                               WRITE_ERROR(I_rec.process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_rec.chunk_id,
                                 L_table,
                                 L_row_seq_tab(i),
                                 'EXCHANGE_TYPE',
                                 'REQ_OPER_EXCH_RATE');
                  END LOOP;
                         end if;
               ROLLBACK TO successful_curr_code;
               O_error:= TRUE;
               close C_CHECK_RATE_EURO;
            else
               close C_CHECK_RATE_EURO;
            end if;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTIon
   when OTHERS then
      if C_CHECK_RATE%ISOPEN then
         close C_CHECK_RATE;
      end if;
      if C_CHECK_RATE_BOTH%ISOPEN then
         close C_CHECK_RATE_BOTH;
      end if;
      if C_CHECK_RATE_EURO%ISOPEN then
         close C_CHECK_RATE_EURO;
      end if;
      if C_CHECK_RATE_BOTH_EURO%ISOPEN then
         close C_CHECK_RATE_BOTH_EURO;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CURR_RATES_AFTER_VAL;
--------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   currencies_cols s9t_pkg.names_map_typ;
   currency_rates_cols s9t_pkg.names_map_typ;
   currencies_tl_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                            := s9t_pkg.get_sheet_names(I_file_id);
   currencies_cols                     := s9t_pkg.get_col_names(I_file_id,
                                                                CURRENCIES_sheet);
   CURRENCIES$ACTION                   := currencies_cols('ACTION');
   CURRENCIES$CURRENCY_CODE            := currencies_cols('CURRENCY_CODE');
   CURRENCIES$CURRENCY_DESC            := currencies_cols('CURRENCY_DESC');
   CURRENCIES$CURRENCY_COST_FMT        := currencies_cols('CURRENCY_COST_FMT');
   CURRENCIES$CURRENCY_RTL_FMT         := currencies_cols('CURRENCY_RTL_FMT');
   CURRENCIES$CURRENCY_COST_DEC        := currencies_cols('CURRENCY_COST_DEC');
   CURRENCIES$currency_rtl_dec         := currencies_cols('CURRENCY_RTL_DEC');
   currency_rates_cols                 := s9t_pkg.get_col_names(I_file_id,
                                                                CURRENCY_RATES_sheet);
   CURRENCY_RATES$ACTION               := currency_rates_cols('ACTION');
   CURRENCY_RATES$CURRENCY_CODE        := currency_rates_cols('CURRENCY_CODE');
   CURRENCY_RATES$EFFECTIVE_DATE       := currency_rates_cols('EFFECTIVE_DATE');
   CURRENCY_RATES$EXCHANGE_TYPE        := currency_rates_cols('EXCHANGE_TYPE');
   CURRENCY_RATES$EXCHANGE_RATE        := currency_rates_cols('EXCHANGE_RATE');
   currencies_TL_cols                  := s9t_pkg.get_col_names(I_file_id,
                                                                CURRENCIES_TL_sheet);
   CURRENCIES_TL$ACTION                := currencies_tl_cols('ACTION');
   CURRENCIES_TL$LANG                  := currencies_tl_cols('LANG');
   CURRENCIES_TL$CURRENCY_CODE         := currencies_tl_cols('CURRENCY_CODE');
   CURRENCIES_TL$CURRENCY_DESC         := currencies_tl_cols('CURRENCY_DESC');
END POPULATE_NAMES;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CURRENCIES(I_file_id   IN   NUMBER)IS
BEGIN
   insert
     into TABLE(select ss.s9t_rows
                  from s9t_folder sf,
                       TABLE(sf.s9t_file_obj.sheets) ss
                 where sf.file_id  = I_file_id
                   and ss.sheet_name = CURRENCIES_sheet)
   select s9t_row(s9t_cells(CORESVC_CURRENCIES.action_mod,
                            currency_code,
                            currency_desc,
                            currency_cost_fmt,
                            currency_rtl_fmt,
                            currency_cost_dec,
                            currency_rtl_dec))
     from CURRENCIES ;
END POPULATE_CURRENCIES;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CURRENCIES_TL(I_file_id   IN   NUMBER)IS
BEGIN
   insert
     into TABLE(select ss.s9t_rows
                  from s9t_folder sf,
                       TABLE(sf.s9t_file_obj.sheets) ss
                 where sf.file_id  = I_file_id
                   and ss.sheet_name = CURRENCIES_TL_sheet)
   select s9t_row(s9t_cells(CORESVC_CURRENCIES.action_mod,
                            lang,
                            currency_code,
                            currency_desc))
     from CURRENCIES_TL ;
END POPULATE_CURRENCIES_TL;
--------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CURRENCY_RATES(I_file_id IN NUMBER)IS
BEGIN
   insert
     into TABLE(select ss.s9t_rows
                  from s9t_folder sf,
                       TABLE(sf.s9t_file_obj.sheets) ss
                 where sf.file_id  = I_file_id
                   and ss.sheet_name = CURRENCY_RATES_sheet)
     select s9t_row(s9t_cells( NULL,
                               currency_code,
                               effective_date,
                               exchange_type,
                               exchange_rate ))
       from CURRENCY_RATES ;
END POPULATE_CURRENCY_RATES;
--------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER)IS
   L_file      s9t_file;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(CURRENCIES_sheet);
   L_file.sheets(L_file.get_sheet_index(CURRENCIES_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'CURRENCY_CODE',
                                                                                        'CURRENCY_DESC',
                                                                                        'CURRENCY_COST_FMT',
                                                                                        'CURRENCY_RTL_FMT',
                                                                                        'CURRENCY_COST_DEC',
                                                                                        'CURRENCY_RTL_DEC');

   L_file.add_sheet(CURRENCIES_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(CURRENCIES_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                           'LANG',
                                                                                           'CURRENCY_CODE',
                                                                                           'CURRENCY_DESC');

   L_file.add_sheet(CURRENCY_RATES_sheet);
   L_file.sheets(L_file.get_sheet_index(CURRENCY_RATES_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                            'CURRENCY_CODE',
                                                                                            'EFFECTIVE_DATE',
                                                                                            'EXCHANGE_TYPE',
                                                                                            'EXCHANGE_RATE');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN CHAR  DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_CURRENCIES.CREATE_S9T';
   L_file    s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_CURRENCIES(O_file_id);
      POPULATE_CURRENCY_RATES(O_file_id);
      POPULATE_CURRENCIES_TL(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CURRENCY_RATES(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_CURRENCY_RATES.PROCESS_ID%TYPE)is
   TYPE svc_currency_rates_col_typ is TABLE OF SVC_CURRENCY_RATES%ROWTYPE;
   L_temp_rec    SVC_CURRENCY_RATES%ROWTYPE;
   svc_currency_rates_col svc_currency_rates_col_typ :=NEW svc_CURRENCY_RATES_col_typ();
   L_process_id  SVC_CURRENCY_RATES.PROCESS_ID%TYPE;
   L_error       BOOLEAN := FALSE;
   L_default_rec SVC_CURRENCY_RATES%ROWTYPE;
   cursor C_MANDATORY_IND is
      select exchange_rate_mi,
             exchange_type_mi,
             effective_date_mi,
             currency_code_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key = ITEM_INDUCT_SQL.template_key
                 and wksht_key    = 'CURRENCY_RATES')
               PIVOT (MAX(mandatory) as mi
                      FOR(column_key) IN ('EXCHANGE_RATE'  as exchange_rate,
                                          'EXCAHNGE_TYPE'  as exchange_type,
                                          'EFFECTIVE_DATE' as effective_date,
                                          'CURRENCY_CODE'  as currency_code,
                                           NULL            as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exceptiON_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CURRENCY_RATES';
   L_pk_columns    VARCHAR2(255)  := 'Exchange Type,Effective Date,Currency Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN
      (select exchange_rate_dv,
              exchange_type_dv,
              effective_date_dv,
              currency_code_dv,
              NULL as dummy
         from(select column_key,
                     default_value
                from s9t_tmpl_cols_def
               where template_key = ITEM_INDUCT_SQL.template_key
                 and wksht_key    = 'CURRENCY_RATES')
               PIVOT (MAX(default_value) as dv
                      FOR(column_key) IN ('EXCHANGE_RATE'  as exchange_rate,
                                          'EXCAHNGE_TYPE'  as exchange_type,
                                          'EFFECTIVE_DATE' as effective_date,
                                          'CURRENCY_CODE'  as currency_code,
                                           NULL            as dummy)))

   LOOP
      BEGIN
         L_default_rec.exchange_rate := rec.exchange_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CURRENCY_RATES',
                            NULL,
                            'EXCHANGE_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.exchange_type := rec.exchange_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CURRENCY_RATES',
                            NULL,
                            'EXCHANGE_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effective_date := rec.effective_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CURRENCY_RATES',
                            NULL,
                            'EFFECTIVE_DATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN

         L_default_rec.currency_code := rec.currency_code_dv;
      EXCEPTION
         when OTHERS then

            WRITE_S9T_ERROR(I_file_id,
                            'CURRENCY_RATES',
                            NULL,
                            'CURRENCY_CODE',
                            NULL,
                            'INV_DEFAULT');

      END;
   END LOOP;

 --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(CURRENCY_RATES$ACTION)        as action,
                      r.get_cell(CURRENCY_RATES$EXCHANGE_RATE) as exchange_rate,
                      r.get_cell(CURRENCY_RATES$EXCHANGE_TYPE) as exchange_type,
                      r.get_cell(CURRENCY_RATES$EFFECTIVE_DATE)as effective_date,
                      UPPER(r.get_cell(CURRENCY_RATES$CURRENCY_CODE)) as currency_code,
                      r.get_row_seq()                          as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(CURRENCY_RATES_sheet))
   LOOP
       L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCY_RATES_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.exchange_rate := rec.exchange_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCY_RATES_sheet,
                            rec.row_seq,
                            'EXCHANGE_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.exchange_type := rec.exchange_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCY_RATES_sheet,
                            rec.row_seq,
                            'EXCHANGE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effective_date := TO_DATE(rec.effective_date,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCY_RATES_sheet,
                            rec.row_seq,
                            'EFFECTIVE_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_code := rec.currency_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCY_RATES_sheet,
                            rec.row_seq,
                            'CURRENCY_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_CURRENCIES.action_new then
         L_temp_rec.exchange_rate  := NVL( L_temp_rec.exchange_rate,L_default_rec.exchange_rate);
         L_temp_rec.exchange_type  := NVL( L_temp_rec.exchange_type,L_default_rec.exchange_type);
         L_temp_rec.effective_date := NVL( L_temp_rec.effective_date,L_default_rec.effective_date);
         L_temp_rec.currency_code  := NVL( L_temp_rec.currency_code,L_default_rec.currency_code);
      end if;
      if NOT (L_temp_rec.exchange_type is NOT NULL and
              L_temp_rec.effective_date is NOT NULL and
              L_temp_rec.currency_code is NOT NULL and
              1 = 1) Then
         WRITE_S9T_ERROR( I_file_id,
                          CURRENCY_RATES_sheet,
                          rec.row_seq,
                                  NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_currency_rates_col.EXTEND();
         svc_currency_rates_col(svc_currency_rates_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_currency_rates_col.count SAVE EXCEPTIONS
      merge into SVC_CURRENCY_RATES st
      using(select(case
                   when L_mi_rec.exchange_rate_mi    = 'N'
                    and svc_currency_rates_col(i).action = CORESVC_CURRENCIES.action_mod
                    and s1.exchange_rate             is NULL
                   then mt.exchange_rate
                   else s1.exchange_rate
                    end) as exchange_rate,
                  (case
                   when L_mi_rec.exchange_type_mi    = 'N'
                    and svc_currency_rates_col(i).action = CORESVC_CURRENCIES.action_mod
                    and s1.exchange_type             is NULL
                   then mt.exchange_type
                   else s1.exchange_type
                   end) as exchange_type,
                  (case
                   when L_mi_rec.effective_date_mi    = 'N'
                    and svc_currency_rates_col(i).action = CORESVC_CURRENCIES.action_mod
                    and s1.effective_date             is NULL
                   then mt.effective_date
                   else s1.effective_date
                   end) as effective_date,
                  (case
                   when L_mi_rec.currency_code_mi    = 'N'
                    and svc_currency_rates_col(i).action = CORESVC_CURRENCIES.action_mod
                    and s1.currency_code             is NULL
                   then mt.currency_code
                   else s1.currency_code
                    end) as currency_code,
                    NULL as dummy
               from (select svc_currency_rates_col(i).exchange_rate  as exchange_rate,
                            svc_currency_rates_col(i).exchange_type  as exchange_type,
                            svc_currency_rates_col(i).effective_date as effective_date,
                            svc_currency_rates_col(i).currency_code  as currency_code,
                            NULL as dummy
                       from dual) s1,
                    currency_rates mt
             where mt.exchange_type (+)     = s1.exchange_type   and
                   mt.effective_date (+)    = s1.effective_date  and
                   mt.currency_code (+)     = s1.currency_code   and
                   1 = 1) sq
                on (st.exchange_type      = sq.exchange_type  and
                    st.effective_date     = sq.effective_date and
                    st.currency_code      = sq.currency_code  and
                    svc_currency_rates_col(i).ACTION in action_del)
      when matched then
         update
            set process_id        = svc_currency_rates_col(i).process_id ,
                chunk_id          = svc_currency_rates_col(i).chunk_id ,
                     action            = svc_currency_rates_col(i).action,
                row_seq           = svc_currency_rates_col(i).row_seq ,
                process$status    = svc_currency_rates_col(i).process$status ,
                exchange_rate     = sq.exchange_rate ,
                create_id         = svc_currency_rates_col(i).create_id ,
                create_datetime   = svc_currency_rates_col(i).create_datetime ,
                last_upd_id       = svc_currency_rates_col(i).last_upd_id ,
                last_upd_datetime = svc_currency_rates_col(i).last_upd_datetime
      when NOT matched then
         insert(process_id ,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                exchange_rate ,
                exchange_type ,
                effective_date ,
                currency_code ,
                create_id ,
                create_datetime ,
                last_upd_id ,
                last_upd_datetime)
       values(svc_currency_rates_col(i).process_id ,
              svc_currency_rates_col(i).chunk_id ,
              svc_currency_rates_col(i).row_seq ,
              svc_currency_rates_col(i).action ,
              svc_currency_rates_col(i).process$status ,
              sq.exchange_rate ,
              sq.exchange_type ,
              sq.effective_date ,
              sq.currency_code ,
              svc_currency_rates_col(i).create_id ,
              svc_currency_rates_col(i).create_datetime ,
              svc_currency_rates_col(i).last_upd_id ,
              svc_currency_rates_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            CURRENCY_RATES_sheet,
                            svc_currency_rates_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_CURRENCY_RATES;
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CURRENCIES(I_file_id     IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id  IN   SVC_CURRENCIES.PROCESS_ID%TYPE)is

   TYPE svc_currencies_col_typ is TABLE OF SVC_CURRENCIES%ROWTYPE;
   L_temp_rec SVC_CURRENCIES%ROWTYPE;
   svc_currencies_col svc_currencies_col_typ :=NEW svc_currencies_col_typ();
   L_process_id SVC_CURRENCIES.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_CURRENCIES%ROWTYPE;
   cursor C_MANDATORY_IND is
      select currency_code_mi,
             currency_desc_mi,
             currency_cost_fmt_mi,
             currency_rtl_fmt_mi,
             currency_cost_dec_mi,
             currency_rtl_dec_mi,
             1 as dummy
    from (select column_key,
                 mandatory
            from s9t_tmpL_cols_def
           where template_key = template_key
             and wksht_key    = 'CURRENCIES')
         PIVOT (MAX(mandatory) as mi
           FOR (column_key) IN('CURRENCY_CODE'     as currency_code,
                               'CURRENCY_DESC'     as currency_desc,
                               'CURRENCY_COST_FMT' as currency_cost_fmt,
                               'CURRENCY_RTL_FMT'  as currency_rtl_fmt,
                               'CURRENCY_COST_DEC' as currency_cost_dec,
                               'currency_rtl_dec'  as currency_rtl_dec,
                               NULL                as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CURRENCIES';
   L_pk_columns    VARCHAR2(255)  := 'Currency Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select currency_code_dv,
                      currency_desc_dv,
                      currency_cost_fmt_dv,
                      currency_rtl_fmt_dv,
                      currency_cost_dec_dv,
                      currency_rtl_dec_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key = template_key
                          and wksht_key    = 'CURRENCIES')
              PIVOT (MAX(default_value) as dv
                FOR (column_key) IN('CURRENCY_CODE'     as currency_code,
                                    'CURRENCY_DESC'     as currency_desc,
                                    'CURRENCY_COST_FMT' as currency_cost_fmt,
                                    'CURRENCY_RTL_FMT'  as currency_rtl_fmt,
                                    'CURRENCY_COST_DEC' as currency_cost_dec,
                                    'currency_rtl_dec'  as currency_rtl_dec,
                                    NULL                as dummy)))

   LOOP
      BEGIN
         L_default_rec.currency_code := rec.currency_code_dv;
      EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           'CURRENCIES',
                           NULL,
                           'CURRENCY_CODE',
                           NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.currency_desc := rec.currency_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CURRENCIES',
                            NULL,
                            'CURRENCY_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.currency_cost_fmt := rec.currency_cost_fmt_dv;
      EXCEPTION
          when OTHERS then
          WRITE_S9T_ERROR(I_file_id,
                          'CURRENCIES',
                          NULL,
                          'CURRENCY_COST_FMT',
                          NULL,
                          'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.currency_rtl_fmt := rec.currency_rtl_fmt_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CURRENCIES',
                            NULL,
                            'CURRENCY_RTL_FMT',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.currency_cost_dec := rec.currency_cost_dec_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CURRENCIES',
                            NULL,
                            'CURRENCY_COST_DEC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.currency_rtl_dec := rec.currency_rtl_dec_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CURRENCIES',
                            NULL,
                            'CURRENCY_RTL_DEC',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

  --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(CURRENCIES$ACTION)                    as action,
                      UPPER(r.get_cell(CURRENCIES$CURRENCY_CODE))      as currency_code,
                      r.get_cell(CURRENCIES$CURRENCY_DESC)             as currency_desc,
                      UPPER(r.get_cell(CURRENCIES$CURRENCY_COST_FMT))  as currency_cost_fmt,
                      UPPER(r.get_cell(CURRENCIES$CURRENCY_RTL_FMT))   as currency_rtl_fmt,
                      r.get_cell(CURRENCIES$CURRENCY_COST_DEC)  as currency_cost_dec,
                      r.get_cell(CURRENCIES$currency_rtl_dec)   as currency_rtl_dec,
                      r.get_row_seq()                           as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(CURRENCIES_sheet))
   LOOP
       L_temp_rec                   :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_code := rec.currency_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                                  CURRENCIES_sheet,
                                          rec.row_seq,
                                          'CURRENCY_CODE',
                                          SQLCODE,
                                          SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
          L_temp_rec.currency_desc := rec.currency_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_sheet,
                            rec.row_seq,
                            'CURRENCY_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_cost_fmt := rec.currency_cost_fmt;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_sheet,
                                          rec.row_seq,
                                          'CURRENCY_COST_FMT',
                                          SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_rtl_fmt := rec.currency_rtl_fmt;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                                  CURRENCIES_sheet,
                                          rec.row_seq,
                            'CURRENCY_RTL_FMT',
                                          SQLCODE,
                                          SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_cost_dec := rec.currency_cost_dec;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           CURRENCIES_sheet,
                           rec.row_seq,
                           'CURRENCY_COST_DEC',
                           SQLCODE,
                           SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_rtl_dec := rec.currency_rtl_dec;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           CURRENCIES_sheet,
                           rec.row_seq,
                           'CURRENCY_RTL_DEC',
                           SQLCODE,
                           SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_CURRENCIES.action_new then
         L_temp_rec.currency_code     := NVL( L_temp_rec.currency_code,L_default_rec.currency_code);
         L_temp_rec.currency_desc     := NVL( L_temp_rec.currency_desc,L_default_rec.currency_desc);
         L_temp_rec.currency_cost_fmt := NVL( L_temp_rec.currency_cost_fmt,L_default_rec.currency_cost_fmt);
         L_temp_rec.currency_rtl_fmt  := NVL( L_temp_rec.currency_rtl_fmt,L_default_rec.currency_rtl_fmt);
         L_temp_rec.currency_cost_dec := NVL( L_temp_rec.currency_cost_dec,L_default_rec.currency_cost_dec);
         L_temp_rec.currency_rtl_dec  := NVL( L_temp_rec.currency_rtl_dec,L_default_rec.currency_rtl_dec);
      end if;
      if NOT (L_temp_rec.currency_code is NOT NULL
              and 1 = 1) then
         WRITE_S9T_ERROR( I_file_id,
                          CURRENCIES_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));

         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_currencies_col.EXTEND();
         svc_currencies_col(svc_currencies_col.COUNT()):=L_temp_rec;
      end if;
      END LOOP;
      BEGIN
         forall i IN 1..svc_currencies_col.count SAVE EXCEPTIONS
         merge into SVC_CURRENCIES st
         using(select(case
                      when L_mi_rec.currency_code_mi    = 'N'
                       and svc_currencies_col(i).action = CORESVC_CURRENCIES.action_mod
                       and s1.currency_code             is NULL
                      then mt.currency_code
                      else s1.currency_code
                       end) as currency_code,
                     (case
                      when L_mi_rec.currency_desc_mi    = 'N'
                       and svc_currencies_col(i).action = CORESVC_CURRENCIES.action_mod
                       and s1.currency_desc             is NULL
                      then mt.currency_desc
                      else s1.currency_desc
                       end) as currency_desc,
                     (case
                      when L_mi_rec.currency_cost_fmt_mi    = 'N'
                       and svc_currencies_col(i).action = coresvc_currencies.action_mod
                       and s1.currency_cost_fmt             is NULL
                      then mt.currency_cost_fmt
                      else s1.currency_cost_fmt
                       end) as currency_cost_fmt,
                     (case
                      when L_mi_rec.currency_rtl_fmt_mi    = 'N'
                       and svc_currencies_col(i).action = coresvc_currencies.action_mod
                       and s1.currency_rtl_fmt             is NULL
                      then mt.currency_rtl_fmt
                      else s1.currency_rtl_fmt
                       end) as currency_rtl_fmt,
                     (case
                      when L_mi_rec.currency_cost_dec_mi    = 'N'
                       and svc_currencies_col(i).action = coresvc_currencies.action_mod
                       and s1.currency_cost_dec             is NULL
                      then mt.currency_cost_dec
                      else s1.currency_cost_dec
                       end) as currency_cost_dec,
                     (case
                      when L_mi_rec.currency_rtl_dec_mi    = 'N'
                       and svc_currencies_col(i).action = coresvc_currencies.action_mod
                       and s1.currency_rtl_dec             is NULL
                      then mt.currency_rtl_dec
                      else s1.currency_rtl_dec
                       end) as currency_rtl_dec,
                     NULL as dummy
                from (select svc_currencies_col(i).currency_code     as currency_code,
                             svc_currencies_col(i).currency_desc     as currency_desc,
                             svc_currencies_col(i).currency_cost_fmt as currency_cost_fmt,
                             svc_currencies_col(i).currency_rtl_fmt  as currency_rtl_fmt,
                             svc_currencies_col(i).currency_cost_dec as currency_cost_dec,
                             svc_currencies_col(i).currency_rtl_dec  as currency_rtl_dec,
                             NULL as dummy
                        from dual) s1,
                        CURRENCIES mt
               where mt.currency_code (+)     = s1.currency_code
                 and 1 = 1) sq
                  on (st.currency_code      = sq.currency_code and
                      svc_currencies_col(i).action in (coresvc_currencies.action_mod,coresvc_currencies.action_del))
         when matched then
         update
            set process_id        = svc_currencies_col(i).process_id ,
                chunk_id          = svc_currencies_col(i).chunk_id ,
                row_seq           = svc_currencies_col(i).row_seq ,
                action            = svc_currencies_col(i).action,
                process$status    = svc_currencies_col(i).process$status ,
                currency_rtl_fmt  = sq.currency_rtl_fmt ,
                currency_cost_fmt = sq.currency_cost_fmt ,
                currency_rtl_dec  = sq.currency_rtl_dec ,
                currency_cost_dec = sq.currency_cost_dec ,
                currency_desc     = sq.currency_desc ,
                create_id         = svc_currencies_col(i).create_id ,
                create_datetime   = svc_currencies_col(i).create_datetime ,
                last_upd_id       = svc_currencies_col(i).last_upd_id ,
                last_upd_datetime = svc_currencies_col(i).last_upd_datetime
         when NOT matched then
            insert(process_id ,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   currency_code ,
                   currency_desc ,
                   currency_cost_fmt ,
                   currency_rtl_fmt ,
                   currency_cost_dec ,
                   currency_rtl_dec ,
                   create_id ,
                   create_datetime ,
                   last_upd_id ,
                   last_upd_datetime)
            values(svc_currencies_col(i).process_id ,
                   svc_currencies_col(i).chunk_id ,
                   svc_currencies_col(i).row_seq ,
                   svc_currencies_col(i).action ,
                   svc_currencies_col(i).process$status ,
                   sq.currency_code ,
                   sq.currency_desc ,
                   sq.currency_cost_fmt ,
                   sq.currency_rtl_fmt ,
                   sq.currency_cost_dec ,
                   sq.currency_rtl_dec ,
                   svc_currencies_col(i).create_id ,
                   svc_currencies_col(i).create_datetime ,
                   svc_currencies_col(i).last_upd_id ,
                   svc_currencies_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            CURRENCIES_sheet,
                            svc_currencies_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_CURRENCIES;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CURRENCIES_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_CURRENCIES_TL.PROCESS_ID%TYPE) IS

   TYPE svc_currencies_tl_col_TYP IS TABLE OF SVC_CURRENCIES_TL%ROWTYPE;
   L_temp_rec              SVC_CURRENCIES_TL%ROWTYPE;
   svc_currencies_tl_col   svc_currencies_tl_col_TYP := NEW svc_currencies_tl_col_TYP();
   L_process_id            SVC_CURRENCIES_TL.PROCESS_ID%TYPE;
   L_error                 BOOLEAN := FALSE;
   L_default_rec           SVC_CURRENCIES_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select currency_code_mi,
             currency_desc_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'CURRENCIES_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('CURRENCY_CODE'    as currency_code,
                                       'CURRENCY_DESC'    as currency_desc,
                                       'LANG'             as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CURRENCIES_TL';
   L_pk_columns    VARCHAR2(255)  := 'Currency Code, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select currency_code_dv,
                      currency_desc_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'CURRENCIES_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('CURRENCY_CODE' as currency_code,
                                                'CURRENCY_DESC' as currency_desc,
                                                'LANG'          as lang)))
   LOOP
      BEGIN
         L_default_rec.currency_code := rec.currency_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_TL_sheet ,
                            NULL,
                           'CURRENCY_CODE' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.currency_desc := rec.currency_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           CURRENCIES_TL_sheet ,
                            NULL,
                           'CURRENCY_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_TL_sheet ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(CURRENCIES_TL$ACTION))            as action,
                      UPPER(r.get_cell(CURRENCIES_TL$CURRENCY_CODE))     as currency_code,
                      r.get_cell(CURRENCIES_TL$CURRENCY_DESC)            as currency_desc,
                      r.get_cell(CURRENCIES_TL$LANG)                     as lang,
                      r.get_row_seq()                                    as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(CURRENCIES_TL_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_code := rec.currency_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_TL_sheet,
                            rec.row_seq,
                            'CURRENCY_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_desc := rec.currency_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_TL_sheet,
                            rec.row_seq,
                            'CURRENCY_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CURRENCIES_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_CURRENCIES.action_new then
         L_temp_rec.currency_code := NVL( L_temp_rec.currency_code,L_default_rec.currency_code);
         L_temp_rec.currency_desc := NVL( L_temp_rec.currency_desc,L_default_rec.currency_desc);
         L_temp_rec.lang          := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.currency_code is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         CURRENCIES_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_currencies_tl_col.extend();
         svc_currencies_tl_col(svc_currencies_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_currencies_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_CURRENCIES_TL st
      using(select
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_currencies_tl_col(i).action = CORESVC_CURRENCIES.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.currency_code_mi = 'N'
                    and svc_currencies_tl_col(i).action = CORESVC_CURRENCIES.action_mod
                    and s1.currency_code IS NULL then
                        mt.currency_code
                   else s1.currency_code
                   end) as currency_code,
                  (case
                   when l_mi_rec.currency_desc_mi = 'N'
                    and svc_currencies_tl_col(i).action = CORESVC_CURRENCIES.action_mod
                    and s1.currency_desc IS NULL then
                        mt.currency_desc
                   else s1.currency_desc
                   end) as currency_desc
              from (select svc_currencies_tl_col(i).currency_code as currency_code,
                           svc_currencies_tl_col(i).currency_desc as currency_desc,
                           svc_currencies_tl_col(i).lang          as lang
                      from dual) s1,
                   currencies_tl mt
             where mt.currency_code (+) = s1.currency_code
               and mt.lang (+)       = s1.lang) sq
                on (st.currency_code = sq.currency_code and
                    st.lang = sq.lang and
                    svc_currencies_tl_col(i).action in (coresvc_currencies.action_mod,coresvc_currencies.action_del))
      when matched then
      update
         set process_id        = svc_currencies_tl_col(i).process_id ,
             chunk_id          = svc_currencies_tl_col(i).chunk_id ,
             row_seq           = svc_currencies_tl_col(i).row_seq ,
             action            = svc_currencies_tl_col(i).action ,
             process$status    = svc_currencies_tl_col(i).process$status ,
             currency_desc     = sq.currency_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             currency_desc ,
             currency_code ,
             lang)
      values(svc_currencies_tl_col(i).process_id ,
             svc_currencies_tl_col(i).chunk_id ,
             svc_currencies_tl_col(i).row_seq ,
             svc_currencies_tl_col(i).action ,
             svc_currencies_tl_col(i).process$status ,
             sq.currency_desc ,
             sq.currency_code ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            CURRENCIES_TL_sheet,
                            svc_currencies_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_CURRENCIES_TL;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                           := 'CORESVC_CURRENCIES.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         S9T_PKG.NAMES_MAP_TYP;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR     EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC( O_error_message,
                         template_category,
                         L_file,
                         TRUE)= FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_CURRENCIES(I_file_id,
                             I_process_id);
      PROCESS_S9T_CURRENCY_RATES(I_file_id,
                                 I_process_id);
      PROCESS_S9T_CURRENCIES_TL(I_file_id,
                                I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if O_error_count    = 0 THEN
          L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
              file_id    = I_file_id
        where process_id = I_process_id;
     commit;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
        O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
        LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
        WRITE_S9T_ERROR(I_file_id,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        'INV_FILE_FORMAT');
        O_error_count := Lp_s9t_errors_tab.count();
        forall i IN 1..O_error_count
           insert
             into s9t_errors
           values lp_s9t_errors_tab(i);
           update svc_process_tracker
              set status       = 'PE',
                  file_id      = i_file_id
            where process_id   = i_process_id;
        commit;
        return FALSE;

      when MAX_CHAR then
         ROLLBACK;
         O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
         Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
         write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
         O_error_count := Lp_s9t_errors_tab.count();
         forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

         update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
          where process_id = I_process_id;
         COMMIT;
         return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CURRENCIES_INS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_currencies_temp_rec  IN       CURRENCIES%ROWTYPE )
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_CURRENCIES.EXEC_CURRENCIES_INS';
BEGIN
   insert
     into currencies
   values I_currencies_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CURRENCIES_INS;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CURRENCIES_UPD( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_currencies_temp_rec  IN       CURRENCIES%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_CURRENCIES.EXEC_CURRENCIES_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CURRENCIES';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_CURRENCY is
      select 'x'
        from currencies
       where currency_code = I_currencies_temp_rec.currency_code
         for update nowait;
BEGIN
   open C_LOCK_CURRENCY;
   close C_LOCK_CURRENCY;
   update currencies
      set row = I_currencies_temp_rec
    where 1 = 1
      and currency_code = I_currencies_temp_rec.currency_code;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_currencies_temp_rec.currency_code,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_CURRENCY%ISOPEN then
       close C_LOCK_CURRENCY;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CURRENCIES_UPD;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CURRENCIES_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_currencies_temp_rec  IN       CURRENCIES%ROWTYPE )
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_CURRENCIES.EXEC_CURRENCIES_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CURRENCIES';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_LOCK_CURRENCY is
      select 'x'
        from currencies
       where currency_code = I_currencies_temp_rec.currency_code
         for update nowait;

   cursor C_LOCK_CURRENCY_TL is
      select 'x'
        from currencies_tl
       where currency_code = I_currencies_temp_rec.currency_code
         for update nowait;
BEGIN
   open C_LOCK_CURRENCY_TL;
   close C_LOCK_CURRENCY_TL;

   open C_LOCK_CURRENCY;
   close C_LOCK_CURRENCY;

   delete
     from currencies_tl
    where currency_code = I_currencies_temp_rec.currency_code;

   delete
     from currencies
    where currency_code = I_currencies_temp_rec.currency_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_currencies_temp_rec.currency_code,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_CURRENCY_TL%ISOPEN then
         close C_LOCK_CURRENCY_Tl;
        end if;
      if C_LOCK_CURRENCY%ISOPEN then
         close C_LOCK_CURRENCY;
        end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CURRENCIES_DEL;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_CURRENCY_RATES_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_currency_rates_temp_rec  IN       CURRENCY_RATES%ROWTYPE )
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_CURRENCY_RATES.EXEC_CURRENCY_RATES_INS';
BEGIN
   insert
     into currency_rates
   values I_currency_rates_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CURRENCY_RATES_INS;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_CURRENCY_RATES_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_currency_rates_temp_rec  IN       CURRENCY_RATES%ROWTYPE )
RETURN BOOLEAN IS
  L_program        VARCHAR2(64)                      :='CORESVC_CURRENCIES.EXEC_CURRENCIES_DEL';
  L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CURRENCY_RATES';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_LOCK_CURRENCY_RATES is
      select 'x'
        from currency_rates
       where currency_code = I_currency_rates_temp_rec.currency_code
           and exchange_type = I_currency_rates_temp_rec.exchange_type
            and effective_date = I_currency_rates_temp_rec.effective_date
         for update nowait;
BEGIN
   open C_LOCK_CURRENCY_RATES;
   close C_LOCK_CURRENCY_RATES;
   delete
     from currency_rates
    where 1 = 1
      and currency_code = I_currency_rates_temp_rec.currency_code
           and exchange_type = I_currency_rates_temp_rec.exchange_type
            and effective_date = I_currency_rates_temp_rec.effective_date;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_currency_rates_temp_rec.currency_code,
                                             I_currency_rates_temp_rec.exchange_type);
      return FALSE;
   when OTHERS then
      if C_LOCK_CURRENCY_RATES%ISOPEN then
         close C_LOCK_CURRENCY_RATES;
        end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CURRENCY_RATES_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CURRENCIES_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_currencies_tl_ins_tab    IN       CURRENCIES_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_CURRENCIES.EXEC_CURRENCIES_TL_INS';

BEGIN
   if I_currencies_tl_ins_tab is NOT NULL and I_currencies_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_currencies_tl_ins_tab.COUNT()
         insert into currencies_tl
              values I_currencies_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CURRENCIES_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CURRENCIES_TL_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_currencies_tl_upd_tab   IN       CURRENCIES_TL_TAB,
                                I_currencies_tl_upd_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_CURRENCIES_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_CURRENCIES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_CURRENCIES_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CURRENCIES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CURRENCIES_TL_UPD(I_currency_code  CURRENCIES_TL.CURRENCY_CODE%TYPE,
                                   I_lang           CURRENCIES_TL.LANG%TYPE) is
      select 'x'
        from currencies_tl
       where currency_code = I_currency_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_currencies_tl_upd_tab is NOT NULL and I_currencies_tl_upd_tab.count > 0 then
      for i in I_currencies_tl_upd_tab.FIRST..I_currencies_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_currencies_tl_upd_tab(i).lang);
            L_key_val2 := 'Currency Code: '||to_char(I_currencies_tl_upd_tab(i).currency_code);
            open C_LOCK_CURRENCIES_TL_UPD(I_currencies_tl_upd_tab(i).currency_code,
                                          I_currencies_tl_upd_tab(i).lang);
            close C_LOCK_CURRENCIES_TL_UPD;
            
            update currencies_tl
               set currency_desc = I_currencies_tl_upd_tab(i).currency_desc,
                   last_update_id = I_currencies_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_currencies_tl_upd_tab(i).last_update_datetime
             where lang = I_currencies_tl_upd_tab(i).lang
               and currency_code = I_currencies_tl_upd_tab(i).currency_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_CURRENCIES_TL',
                           I_currencies_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CURRENCIES_TL_UPD%ISOPEN then
         close C_LOCK_CURRENCIES_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CURRENCIES_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CURRENCIES_TL_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_currencies_tl_del_tab   IN       CURRENCIES_TL_TAB,
                                I_currencies_tl_del_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_CURRENCIES_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_CURRENCIES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_CURRENCIES.EXEC_CURRENCIES_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CURRENCIES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CURRENCIES_TL_DEL(I_currency_code  CURRENCIES_TL.CURRENCY_CODE%TYPE,
                                   I_lang           CURRENCIES_TL.LANG%TYPE) is
      select 'x'
        from currencies_tl
       where currency_code = I_currency_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_currencies_tl_del_tab is NOT NULL and I_currencies_tl_del_tab.count > 0 then
      for i in I_currencies_tl_del_tab.FIRST..I_currencies_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_currencies_tl_del_tab(i).lang);
            L_key_val2 := 'Currency Code: '||to_char(I_currencies_tl_del_tab(i).currency_code);
            open C_LOCK_CURRENCIES_TL_DEL(I_currencies_tl_del_tab(i).currency_code,
                                          I_currencies_tl_del_tab(i).lang);
            close C_LOCK_CURRENCIES_TL_DEL;
            
            delete currencies_tl
             where lang = I_currencies_tl_del_tab(i).lang
               and currency_code = I_currencies_tl_del_tab(i).currency_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_CURRENCIES_TL',
                           I_currencies_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CURRENCIES_TL_DEL%ISOPEN then
         close C_LOCK_CURRENCIES_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CURRENCIES_TL_DEL;
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_CURRENCIES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_CURRENCIES.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_CURRENCIES.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program             VARCHAR2(64)                      := 'CORESVC_CURRENCIES.PROCESS_CURRENCIES';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CURRENCIES';
   L_error               BOOLEAN;
   L_currencies_temp_rec CURRENCIES%ROWTYPE;
   L_process_error       BOOLEAN           := FALSE;

BEGIN
   FOR rec IN C_SVC_CURRENCIES(I_process_id,I_chunk_id)
   LOOP
      L_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.pk_currencies_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CURRENCY_CODE',
                     'CURR_CODE_EXIST');
         L_error := TRUE;
      end if;
      if rec.currency_code  is NULL  then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CURRENCY_CODE',
                     'ENTER_CURRENCY_CODE');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.pk_currencies_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'CURRENCY_CODE',
                    'INV_CURR_CODE');
          L_error := TRUE;
      end if;
      if rec.action IN (action_new,action_mod) then
         if NOT( rec.currency_rtl_dec BETWEEN 0 and 4 ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CURRENCY_RTL_DEC',
                        'CHK_CURRENCY_RTL_DEC');
            L_error := TRUE;
         end if;
         if NOT( rec.currency_cost_dec BETWEEN 0 and 4 ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CURRENCY_COST_DEC',
                        'CHK_CURRENCY_COST_DEC');
            L_error := TRUE;
         end if;
         if rec.currency_desc  is NULL  then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CURRENCY_DESC',
                        'ENTER_CURRENCY_DESC');
            L_error :=TRUE;
         end if;
         if rec.currency_cost_fmt  is NULL  then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CURRENCY_COST_FMT',
                        'ENTER_CURRENCY_COST_FMT');
            L_error :=TRUE;
         end if;
         if rec.currency_rtl_fmt  is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CURRENCY_RTL_FMT',
                        'ENTER_CURRENCY_RTL_FMT');
            L_error :=TRUE;
         end if;
         if rec.currency_cost_dec  is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                       'CURRENCY_COST_DEC',
                       'ENTER_CURRENCY_COST_DEC');
            L_error :=TRUE;
         end if;
         if rec.currency_rtl_dec  is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CURRENCY_RTL_DEC',
                        'ENTER_CURRENCY_RTL_DEC');
            L_error := TRUE;
         end if;
      end if;
      if PROCESS_CURRENCIES_VAL(O_error_message,
                                L_error,
                                rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then

         L_currencies_temp_rec.currency_code              := rec.currency_code;
         L_currencies_temp_rec.currency_desc              := rec.currency_desc;
         L_currencies_temp_rec.currency_cost_fmt          := rec.currency_cost_fmt;
         L_currencies_temp_rec.currency_rtl_fmt           := rec.currency_rtl_fmt;
         L_currencies_temp_rec.currency_cost_dec          := rec.currency_cost_dec;
         L_currencies_temp_rec.currency_rtl_dec           := rec.currency_rtl_dec;
         L_currencies_temp_rec.create_id                  := GET_USER;
         L_currencies_temp_rec.create_datetime            := SYSDATE;
         if rec.action = action_new then
            if EXEC_CURRENCIES_INS( O_error_message,
                                    L_currencies_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_CURRENCIES_UPD( O_error_message,
                                    L_currencies_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_CURRENCIES_DEL( O_error_message,
                                    L_currencies_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
     end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_CURRENCIES;
-------------------------------------------------------------------------------------------
FUNCTION PROCESS_CURRENCY_RATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_CURRENCY_RATES.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_CURRENCY_RATES.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64)                       := 'CORESVC_CURRENCIES.PROCESS_CURRENCIES';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_CURRENCY_RATES';
   L_error                   BOOLEAN;
   L_currency_rates_temp_rec CURRENCY_RATES%ROWTYPE;
   L_process_error           BOOLEAN                            := FALSE;

BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   FOR rec IN C_SVC_CURRENCY_RATES(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;
          if rec.c_rank=1 then
             SAVEPOINT  successful_curr_code;
         L_row_seq_tab.DELETE;
         c:=1;
          end if;
      if ((rec.action NOT IN (action_new) and LP_system_options_row.financial_ap is NOT NULL)
         or (rec.action NOT IN (action_new,action_del) and LP_system_options_row.financial_ap is NULL))
               and rec.action is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.pk_currency_rates_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Exchange Type,Effective Date,Currency Code',
                     'DATE_EXISTS');
         L_error := TRUE;
      end if;
      if rec.action =action_del
         and rec.pk_currency_rates_rid is NULL
               and LP_system_options_row.financial_ap is NULL then
               WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Exchange Type,Effective Date,Currency Code',
                     'NO_CURRENCY_RATES');
         L_error := TRUE;
      end if;
          if rec.action=action_new
             and rec.crt_cur_fk_rid is NULL
         and rec.currency_code is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CURRENCY_CODE',
                     'CRT_CUR_FK');
         L_error := TRUE;
      end if;
      if rec.action=action_new
             and rec.exchange_rate  is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'EXCHANGE_RATE',
                     'MUST_ENTER_EXCHANGE_RATE');
         L_error := TRUE;
      end if;
      if PROCESS_CURRENCY_RATES_VAL(O_error_message,
                                    L_error,
                                    rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;
      if NOT L_error then
         L_currency_rates_temp_rec.currency_code              := rec.currency_code;
         L_currency_rates_temp_rec.effective_date             := rec.effective_date;
         L_currency_rates_temp_rec.exchange_type              := rec.exchange_type;
         L_currency_rates_temp_rec.exchange_rate              := rec.exchange_rate;
         L_currency_rates_temp_rec.create_id                  := GET_USER;
         L_currency_rates_temp_rec.create_datetime            := SYSDATE;

         if rec.action=action_new then
                  if EXEC_CURRENCY_RATES_INS( O_error_message,
                                        L_currency_rates_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error:=TRUE;
            end if;
         end if;
               if rec.action=action_del then
                  if EXEC_CURRENCY_RATES_DEL( O_error_message,
                                        L_currency_rates_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error:=TRUE;
            end if;
         end if;
               if NOT L_process_error then
                  L_row_seq_tab.extend();
            L_row_seq_tab(c)  := rec.row_seq;
            c:=c+1;
               end if;
      end if;

      if rec.action IN (action_new,action_del)
         and PROCESS_CURR_RATES_AFTER_VAL(O_error_message,
                                          L_error,
                                          rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_CURRENCY_RATES;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CURRENCIES_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_CURRENCIES_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_CURRENCIES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64) := 'CORESVC_CURRENCIES.PROCESS_CURRENCIES_TL';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CURRENCIES_TL';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CURRENCIES_TL';
   L_error                     BOOLEAN := FALSE;
   L_process_error             BOOLEAN := FALSE;
   L_currencies_tl_temp_rec    CURRENCIES_TL%ROWTYPE;
   L_currencies_tl_upd_rst     ROW_SEQ_TAB;
   L_currencies_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_CURRENCIES_TL(I_process_id NUMBER,
                              I_chunk_id   NUMBER) is
      select pk_currencies_tl.rowid  as pk_currencies_tl_rid,
             fk_currencies.rowid     as fk_currencies_rid,
             fk_lang.rowid           as fk_lang_rid,
             st.lang,
             st.currency_code,
             st.currency_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_currencies_tl  st,
             currencies         fk_currencies,
             currencies_tl      pk_currencies_tl,
             lang               fk_lang
       where st.process_id       =  I_process_id
         and st.chunk_id         =  I_chunk_id
         and st.currency_code    =  fk_currencies.currency_code (+)
         and st.lang             =  pk_currencies_tl.lang (+)
         and st.currency_code  =  pk_currencies_tl.currency_code (+)
         and st.lang             =  fk_lang.lang (+);

   TYPE SVC_CURRENCIES_TL is TABLE OF C_SVC_CURRENCIES_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_currencies_tl_tab        SVC_CURRENCIES_TL;

   L_currencies_tl_ins_tab         CURRENCIES_TL_TAB         := NEW CURRENCIES_TL_TAB();
   L_currencies_tl_upd_tab         CURRENCIES_TL_TAB         := NEW CURRENCIES_TL_TAB();
   L_currencies_tl_del_tab         CURRENCIES_TL_TAB         := NEW CURRENCIES_TL_TAB();

BEGIN
   if C_SVC_CURRENCIES_TL%ISOPEN then
      close C_SVC_CURRENCIES_TL;
   end if;

   open C_SVC_CURRENCIES_TL(I_process_id,
                            I_chunk_id);
   LOOP
      fetch C_SVC_CURRENCIES_TL bulk collect into L_svc_currencies_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_currencies_tl_tab.COUNT > 0 then
         FOR i in L_svc_currencies_tl_tab.FIRST..L_svc_currencies_tl_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_currencies_tl_tab(i).action is NULL
               or L_svc_currencies_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_currencies_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_currencies_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_currencies_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               CONTINUE;
            end if;

            -- check if primary key values already exist
            if L_svc_currencies_tl_tab(i).action = action_new
               and L_svc_currencies_tl_tab(i).pk_currencies_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_currencies_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_currencies_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_currencies_tl_tab(i).lang is NOT NULL
               and L_svc_currencies_tl_tab(i).currency_code is NOT NULL
               and L_svc_currencies_tl_tab(i).pk_currencies_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_currencies_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_currencies_tl_tab(i).action = action_new
               and L_svc_currencies_tl_tab(i).currency_code is NOT NULL
               and L_svc_currencies_tl_tab(i).fk_currencies_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_currencies_tl_tab(i).row_seq,
                            NULL,
                           'CRT_CUR_FK');
               L_error :=TRUE;
            end if;

            if L_svc_currencies_tl_tab(i).action = action_new
               and L_svc_currencies_tl_tab(i).lang is NOT NULL
               and L_svc_currencies_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_currencies_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_currencies_tl_tab(i).currency_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_currencies_tl_tab(i).row_seq,
                           'CURRENCY_CODE',
                           'ENTER_CURRENCY_CODE');
               L_error :=TRUE;
            end if;

            if L_svc_currencies_tl_tab(i).action in (action_new, action_mod) and L_svc_currencies_tl_tab(i).currency_desc is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_currencies_tl_tab(i).row_seq,
                           'CURRENCY_DESC',
                           'ENTER_CURRENCY_DESC');
               L_error :=TRUE;
            end if;

            if L_svc_currencies_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_currencies_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_currencies_tl_temp_rec.lang := L_svc_currencies_tl_tab(i).lang;
               L_currencies_tl_temp_rec.currency_code := L_svc_currencies_tl_tab(i).currency_code;
               L_currencies_tl_temp_rec.currency_desc := L_svc_currencies_tl_tab(i).currency_desc;
               L_currencies_tl_temp_rec.create_datetime := SYSDATE;
               L_currencies_tl_temp_rec.create_id := GET_USER;
               L_currencies_tl_temp_rec.last_update_datetime := SYSDATE;
               L_currencies_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_currencies_tl_tab(i).action = action_new then
                  L_currencies_tl_ins_tab.extend;
                  L_currencies_tl_ins_tab(L_currencies_tl_ins_tab.count()) := L_currencies_tl_temp_rec;
               end if;

               if L_svc_currencies_tl_tab(i).action = action_mod then
                  L_currencies_tl_upd_tab.extend;
                  L_currencies_tl_upd_tab(L_currencies_tl_upd_tab.count()) := L_currencies_tl_temp_rec;
                  L_currencies_tl_upd_rst(L_currencies_tl_upd_tab.count()) := L_svc_currencies_tl_tab(i).row_seq;
               end if;

               if L_svc_currencies_tl_tab(i).action = action_del then
                  L_currencies_tl_del_tab.extend;
                  L_currencies_tl_del_tab(L_currencies_tl_del_tab.count()) := L_currencies_tl_temp_rec;
                  L_currencies_tl_del_rst(L_currencies_tl_del_tab.count()) := L_svc_currencies_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_CURRENCIES_TL%NOTFOUND;
   END LOOP;
   close C_SVC_CURRENCIES_TL;

   if EXEC_CURRENCIES_TL_INS(O_error_message,
                             L_currencies_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_CURRENCIES_TL_UPD(O_error_message,
                             L_currencies_tl_upd_tab,
                             L_currencies_tl_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_CURRENCIES_TL_DEL(O_error_message,
                             L_currencies_tl_del_tab,
                             L_currencies_tl_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CURRENCIES_TL;
-----------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     NUMBER) IS

BEGIN
   delete
     from svc_currencies_tl
    where process_id= I_process_id;

   delete
     from svc_currencies
    where process_id= I_process_id;

   delete
     from svc_currency_rates
    where process_id= I_process_id;
END CLEAR_STAGING_DATA;

-------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                       := 'CORESVC_CURRENCIES.PROCESS';
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE    :='PS';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_CURRENCY_RATES';
   L_error           BOOLEAN;
   L_err_count       VARCHAR2(1);
   L_warn_count      VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW LP_errors_tab_typ();
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  0,
                  NULL,
                  O_error_message);
      L_error :=TRUE;
   end if;
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       LP_primary_currency) = FALSE then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  0,
                  'CURRENCY_CODE',
                  O_error_message);
      L_error :=TRUE;
   end if;
   if CURRENCY_SQL.CHECK_EMU_COUNTRIES(O_error_message,
                                       LP_participating_ind,
                                       LP_primary_currency) = FALSE then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  0,
                  'CURRENCY_CODE',
                  O_error_message);
      L_error :=TRUE;
   end if;
   if LP_primary_currency = 'EUR' or
      LP_participating_ind then
      LP_prim_euro_ind := 'Y';
   else
      LP_prim_euro_ind := 'N';
   end if;
   if PROCESS_CURRENCIES(O_error_message,
                         I_process_id,
                         I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_CURRENCY_RATES(O_error_message,
                             I_process_id,
                             I_chunk_id)= FALSE then
      return FALSE;
   end if;

   if PROCESS_CURRENCIES_TL(O_error_message,
                            I_process_id,
                            I_chunk_id)= FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);

   open  C_GET_ERR_COUNT;
   fetch C_GET_ERR_COUNT into L_err_count;
   close C_GET_ERR_COUNT;

   open  C_GET_WARN_COUNT;
   fetch C_GET_WARN_COUNT into L_warn_count;
   close C_GET_WARN_COUNT;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_CURRENCIES;
/
