
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XLOCTRT_SQL AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_LOCTRAIT
   -- Purpose      :  Inserts a record on the loc_traits table
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_LOCTRAIT
   -- Purpose      :  Updates a record on the loc_traits table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_LOCTRAIT
   -- Purpose      :  Deletes a record on the loc_traits table
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XLOCTRT_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XLOCTRT.LP_cre_type then
      if CREATE_LOCTRAIT(O_error_message,
                         I_loctrait_rec) = FALSE then
         return FALSE;
      end if;

   elsif I_message_type = RMSSUB_XLOCTRT.LP_mod_type then
      if MODIFY_LOCTRAIT(O_error_message,
                         I_loctrait_rec) = FALSE then
         return FALSE;
      end if;

   elsif I_message_type = RMSSUB_XLOCTRT.LP_del_type then
      if DELETE_LOCTRAIT(O_error_message,
                         I_loctrait_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XLOCTRT_SQL.CREATE_LOCTRAIT';

BEGIN

   if not LOC_TRAITS_SQL.INSERT_LOCTRAIT(O_error_message,
                                         I_loctrait_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_LOCTRAIT;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XLOCTRT_SQL.UPDATE_LOCTRAIT';

BEGIN

   if not LOC_TRAITS_SQL.UPDATE_LOCTRAIT(O_error_message,
                                         I_loctrait_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_LOCTRAIT;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XLOCTRT_SQL.DELETE_LOCTRAIT';

BEGIN

   if not LOC_TRAITS_SQL.DELETE_LOCTRAIT(O_error_message,
                                         I_loctrait_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_LOCTRAIT;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XLOCTRT_SQL;
/
