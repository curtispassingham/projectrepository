SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PACKITEM_VALIDATE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
-- Name: CHECK_DEPOSIT_COMPLEX
-- Purpose: Determines if the item passed to the function is a deposit
--          complex pack

FUNCTION CHECK_DEPOSIT_COMPLEX (O_error_message   IN OUT VARCHAR2,
                                O_dep_complex_ind    OUT VARCHAR2,
                                I_item            IN     PACKITEM.PACK_NO%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------
-- Name:    CHECK_FOR_SKUS
-- Purpose: Indicates if there is an pack component item for a specified pack.
--
FUNCTION CHECK_FOR_SKUS(O_error_message   IN OUT VARCHAR2,
                        O_item_ind        IN OUT VARCHAR2,
                        I_pack_no         IN     PACKITEM.PACK_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
-- Name:     CHECK_SKU
-- Purpose:  Indicates if a specified pack component item exists at a store
--           and/or warehouse with an item status of discontinued or deleted.
--
FUNCTION CHECK_SKU(O_error_message IN OUT  VARCHAR2,
                   I_item          IN      PACKITEM.ITEM%TYPE,
                   O_store_ind     IN OUT  BOOLEAN,
                   O_wh_ind        IN OUT  BOOLEAN)
   return BOOLEAN;
-----------------------------------------------------------------------------
-- Name:     SIMPLE_PACK_EXISTS
-- Purpose:  Indicates if a specified pack component item exists as part of any
--           simple pack. If a pack is also specified, the function indicates if
--           the specified pack component item exists as part of a simple pack
--           for a pack other than the specified pack.
--
FUNCTION SIMPLE_PACK_EXISTS(O_error_message   IN OUT VARCHAR2,
                            O_exists          IN OUT BOOLEAN,
                            I_item            IN     PACKITEM.ITEM%TYPE,
                            I_pack_no         IN     PACKITEM.PACK_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- Function Name:  VALIDATE_PACK
--- Purpose:        Validates that a deposit item complex contains the
---                 correct type and number of deposit items as components.
--- Author:         Faye Guerzon
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       I_item            IN       PACKITEM.PACK_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- Function Name:  CHECK_DUPLICATE_PACK
--- Purpose:        Checks if the passed simple pack item already exists.
--- Author:         Cristopher D. Bernardo
--------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE_PACK (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_pack_no         IN       PACKITEM.PACK_NO%TYPE,
                               I_item            IN       PACKITEM.ITEM%TYPE,
                               I_qty             IN       PACKITEM.PACK_QTY%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END PACKITEM_VALIDATE_SQL;
/
