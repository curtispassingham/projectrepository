create or replace
PACKAGE BODY TICKET_SQL AS
------------------------------------------------------------------------------
FUNCTION TICKET_TYPE_EXISTS(O_ERROR_MESSAGE IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_EXISTS        IN OUT BOOLEAN,
                            I_TICKET_TYPE   IN     TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE)
RETURN BOOLEAN IS

   L_dummy          VARCHAR2(1);

   cursor C_tth is
      select 'Y'
        from ticket_type_head
       where ticket_type_id=I_TICKET_TYPE;

BEGIN
   sql_lib.set_mark('OPEN','C_tth','TICKET_TYPE_HEAD','TICKET TYPE ID: '
    ||I_TICKET_TYPE);
   OPEN C_tth;
   sql_lib.set_mark('FETCH','C_tth','TICKET_TYPE_HEAD','TICKET TYPE ID: '
    ||I_TICKET_TYPE);
   FETCH C_tth INTO L_dummy;
   if C_tth%FOUND then
      O_exists:=TRUE;
   else
      O_exists:=FALSE;
   end if;
   sql_lib.set_mark('CLOSE','C_tth','TICKET_TYPE_HEAD','TICKET TYPE ID: '
    ||I_TICKET_TYPE);
   CLOSE C_tth;
   ---
   RETURN TRUE;

EXCEPTION
 when OTHERS then
    O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
        SQLERRM,
        'TICKET_SQL.TICKET_TYPE_EXISTS',
        TO_CHAR(SQLCODE));
   RETURN FALSE;
END TICKET_TYPE_EXISTS;
------------------------------------------------------------------------------
FUNCTION TICKET_ITEM_EXISTS(O_ERROR_MESSAGE IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_EXISTS        IN OUT BOOLEAN,
                            I_TICKET_TYPE   IN     TICKET_TYPE_DETAIL.TICKET_TYPE_ID%TYPE,
                            I_TICKET_ITEM   IN     TICKET_TYPE_DETAIL.TICKET_ITEM_ID%TYPE,
                            I_UDA           IN     TICKET_TYPE_DETAIL.UDA_ID%TYPE)

RETURN BOOLEAN IS
   L_dummy          VARCHAR2(1);

   cursor C_ttd is
      select 'Y'
        from ticket_type_detail
       where ticket_type_id=I_TICKET_TYPE
         and (ticket_item_id=I_TICKET_ITEM
              or uda_id=I_UDA);

BEGIN
   L_dummy:='N';

   sql_lib.set_mark('OPEN','C_ttd','TICKET_TYPE_DETAIL','TICKET TYPE ID: '
    ||I_TICKET_TYPE||', TICKET ITEM ID: '||I_TICKET_ITEM||', UDA ID: '||I_UDA);
   OPEN C_ttd;
   sql_lib.set_mark('FETCH','C_ttd','TICKET_TYPE_DETAIL','TICKET TYPE ID: '
    ||I_TICKET_TYPE||', TICKET ITEM ID: '||I_TICKET_ITEM||', UDA ID: '||I_UDA);
   FETCH C_ttd INTO L_dummy;
   if C_ttd%FOUND then
      O_exists:=TRUE;
   else
      O_exists:=FALSE;
   end if;
   sql_lib.set_mark('CLOSE','C_ttd','TICKET_TYPE_DETAIL','TICKET TYPE ID: '
    ||I_TICKET_TYPE||', TICKET ITEM ID: '||I_TICKET_ITEM||', UDA ID: '||I_UDA);
   CLOSE C_ttd;

   RETURN TRUE;
EXCEPTION
 when OTHERS then
    O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
        SQLERRM,
        'TICKET_SQL.TICKET_ITEM_EXISTS',
        TO_CHAR(SQLCODE));
   RETURN FALSE;
END TICKET_ITEM_EXISTS;
--------------------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_ticket_desc   IN OUT TICKET_TYPE_HEAD.TICKET_TYPE_DESC%TYPE,
                  O_exists        IN OUT BOOLEAN,
                  I_ticket_type   IN     TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE)
RETURN BOOLEAN IS

cursor C_TICKET_DESC is
   select ticket_type_desc
     from v_ticket_type_head_tl
    where ticket_type_id = I_ticket_type;

BEGIN
   if I_ticket_type is NULL then
     O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                          'I_ticket_type',
                                          'NULL',
                                          'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_TICKET_DESC','V_TICKET_TYPE_HEAD_TL',
                    'TICKET_TYPE_ID: '|| I_ticket_type);
   open C_TICKET_DESC;
   ---
   SQL_LIB.SET_MARK('FETCH','C_TICKET_DESC','V_TICKET_TYPE_HEAD_TL',
                    'TICKET_TYPE_ID: '|| I_ticket_type);
   fetch C_TICKET_DESC into O_ticket_desc;
   ---
   if C_TICKET_DESC%FOUND then
      ---
      SQL_LIB.SET_MARK('CLOSE','C_TICKET_DESC','V_TICKET_TYPE_HEAD_TL',
                       'TICKET_TYPE_ID: '|| I_ticket_type);
      close C_TICKET_DESC;
      ---
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'TICKET_SQL.GET_NAME',
                                             to_char(SQLCODE));
   RETURN FALSE;
END GET_NAME;
----------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_TICKET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_type       IN  VARCHAR2,
                            I_item_no         IN  ITEM_TICKET.ITEM%TYPE,
                            I_ticket_type_id  IN  TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE,
                            I_po_print_type   IN  ITEM_TICKET.PO_PRINT_TYPE%TYPE,
                            I_print_on_pc_ind IN  ITEM_TICKET.PRINT_ON_PC_IND%TYPE,
                            I_ticket_over_pct IN  ITEM_TICKET.TICKET_OVER_PCT%TYPE)
   RETURN BOOLEAN IS

   L_item           ITEM_MASTER.ITEM%TYPE;
   L_sysdate        DATE                     := SYSDATE;
   L_user_id        VARCHAR2(30) := GET_USER;
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_ITEM_LIST is
      select item
        from skulist_detail
       where skulist = I_item_no;

   cursor C_LOCK_ITEM_TICKET is
      select 'x'
        from item_ticket
       where item           = I_item_no
         and ticket_type_id = I_ticket_type_id
       for update nowait;

   cursor C_LOCK_ITEM_TICKET_LIST is
      select 'x'
        from item_ticket it
       where it.ticket_type_id = I_ticket_type_id
         and exists (select 'x'
                       from skulist_detail sd
                      where sd.item    = it.item
                        and sd.skulist = I_item_no)
       for update nowait;

BEGIN

   if I_item_type = 'ITEMLIST' then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_TICKET_LIST','ITEM_TICKET',
                       'ITEMLIST: '|| (I_item_no) ||
                       'TICKET_TYPE_ID: '|| I_ticket_type_id);
      open C_LOCK_ITEM_TICKET_LIST;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_TICKET_LIST','ITEM_TICKET',
                       'ITEMLIST: '|| (I_item_no) ||
                       'TICKET_TYPE_ID: '|| I_ticket_type_id);
      close C_LOCK_ITEM_TICKET_LIST;
      --
      --delete pre-existing itemlist items from item_ticket
      --before inserting new ticket info
      SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_TICKET','ITEMLIST: '||
                       (I_item_no)|| 'TICKET_TYPE_ID: '|| I_ticket_type_id);
      delete from item_ticket it
       where it.ticket_type_id = I_ticket_type_id
         and exists (select 'x'
                       from skulist_detail sd
                      where sd.item    = it.item
                        and sd.skulist = I_item_no);
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_ITEM_LIST','SKULIST_DETAIL',
                       'ITEMLIST: '|| (I_item_no));
      open C_GET_ITEM_LIST;
      ---
      LOOP
         SQL_LIB.SET_MARK('FETCH','C_GET_ITEM_LIST','SKULIST_DETAIL',
                          'ITEMLIST: '|| (I_item_no));
         fetch C_GET_ITEM_LIST into L_Item;
         EXIT WHEN C_GET_ITEM_LIST%NOTFOUND;
         ---
         BEGIN
            SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_TICKET','ITEMLIST: '||
                             (I_item_no)||' ITEM: '|| (L_Item)
                             ||' TICKET_TYPE_ID: '|| I_ticket_type_id);
            insert into item_ticket (item,
                                     ticket_type_id,
                                     po_print_type,
                                     print_on_pc_ind,
                                     ticket_over_pct,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id)
                             values (L_item,
                                     I_ticket_type_id,
                                     I_po_print_type,
                                     I_print_on_pc_ind,
                                     I_ticket_over_pct,
                                     L_sysdate,
                                     L_sysdate,
                                     L_user_id);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               null;
         END;
      END LOOP;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_LIST','SKULIST_DETAIL',
                       'ITEMLIST: '|| (I_item_no));
      close C_GET_ITEM_LIST;
   elsif I_item_type = 'ITEM' then
      -- lock and delete all of the skus in the style from item_ticket
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_TICKET','ITEM_TICKET',
                       'ITEM: '|| (I_item_no));
      open C_LOCK_ITEM_TICKET;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_TICKET','ITEM_TICKET',
                       'ITEM: '|| (I_item_no));
      close C_LOCK_ITEM_TICKET;
      /* delete pre-existing itemlist items from item_ticket */
      /* before inserting new ticket info */
      SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_TICKET',
                       'ITEM: '|| (I_item_no)
                       || ' TICKET_TYPE_ID: '|| I_ticket_type_id);
      delete from item_ticket
            where ticket_type_id = I_ticket_type_id
              and item           = I_item_no;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_TICKET',
                       ' Item: '|| (I_item_no)
                       ||' TICKET_TYPE_ID: '|| I_ticket_type_id);
      insert into item_ticket (item,
                               ticket_type_id,
                               po_print_type,
                               print_on_pc_ind,
                               ticket_over_pct,
                               create_datetime,
                               last_update_datetime,
                               last_update_id)
                       values (I_item_no,
                               I_ticket_type_id,
                               I_po_print_type,
                               I_print_on_pc_ind,
                               I_ticket_over_pct,
                               L_sysdate,
                               L_sysdate,
                               L_user_id);
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TICKET_REC_LOC',
                                            I_ticket_type_id,
                                            (I_item_no));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TICKET_SQL.INSERT_ITEM_TICKET',
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ITEM_TICKET;
------------------------------------------------------------------------------
FUNCTION REQUEST(O_ERROR_MESSAGE      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_LOC_TYPE           IN       CODE_DETAIL.CODE%TYPE,
                 I_LOCATION           IN       VARCHAR2,
                 I_ITEM_TYPE          IN       VARCHAR2,
                 I_ITEM               IN       ITEM_TICKET.ITEM%TYPE,
                 I_TICKET_TYPE        IN       TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE,
                 I_QUANTITY           IN       TICKET_REQUEST.QTY%TYPE,
                 I_COUNTRY            IN       TICKET_REQUEST.COUNTRY_OF_ORIGIN%TYPE,
                 I_PRINT_ONLINE_IND   IN       TICKET_REQUEST.PRINT_ONLINE_IND%TYPE)
RETURN BOOLEAN IS

   L_user_id  VARCHAR2(30) := get_user;
   L_sysdate  DATE         := sysdate;

   L_location             LOC_LIST_DETAIL.LOCATION%TYPE;

   L_stockholding_store   BOOLEAN;
   L_store_name           STORE.STORE_NAME%TYPE;
   L_store_type           STORE.STORE_TYPE%TYPE;

BEGIN
   ------------------------------------------
   -- Use correct var type for location lists
   -- so that PK is used on loc_list_detail
   ------------------------------------------
   if I_loc_type IN ( 'LLS','LLW') then
      L_location := TO_NUMBER(I_location);
   end if;

   if I_loc_type = 'S' then
      if FILTER_LOV_VALIDATE_SQL.STOCKHOLDING_STORE(O_error_message,
                                                    L_stockholding_store,
                                                    L_store_name,
                                                    L_store_type,
                                                    I_location) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_item_type = 'L' then
      sql_lib.set_mark('INSERT',NULL,'TICKET_REQUEST','ITEM TYPE: '
        ||I_ITEM_TYPE||' ITEM:'||I_ITEM||' LOCATION TYPE:'
        ||I_LOC_TYPE||' LOCATION:'||I_LOCATION);
      if (L_stockholding_store) or I_loc_type = 'W' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    I_loc_type,
                                    I_location,
                                    I_country,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from skulist_detail s,
                                    v_item_master i,
                                    item_ticket t
                              where s.skulist = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
								and i.status = 'A'
                                and t.ticket_type_id = I_ticket_type;
      elsif I_loc_type = 'C' then
          insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v.store,
                                    I_country,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store v,
                                    skulist_detail s,
                                    v_item_master i,
                                    item_ticket t
                              where v.store_class       = I_location
                                and s.skulist    = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';
      elsif I_loc_type = 'D' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v.store,
                                    I_country,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store v,
                                    skulist_detail s,
                                    item_master i,
                                    item_ticket t
                              where v.district         = I_location
                                and s.skulist = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';
      elsif I_loc_type = 'A' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v.store,
                                    I_country,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store v,
                                    skulist_detail s,
                                    item_master i,
                                    item_ticket t
                              where v.area    = I_location
                                and s.skulist = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';
      elsif I_loc_type = 'R' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store v,
                                    skulist_detail s,
                                    v_item_master i,
                                    item_ticket t
                              where v.region = I_location
                                and s.skulist = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';
      elsif I_loc_type = 'T' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store v,
                                    skulist_detail s,
                                    v_item_master i,
                                    item_ticket t
                              where v.transfer_zone    = I_location
                                and s.skulist = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';
      elsif I_loc_type = 'L' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    l.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from loc_traits_matrix l,
                                    v_store v,
                                    skulist_detail s,
                                    v_item_master i,
                                    item_ticket t
                              where l.loc_trait = I_location
                                and l.store     = v.store
                                and s.skulist      = I_item
                                and s.item         = i.item
                                and s.item_level   = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';
      elsif I_loc_type = 'DW' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store v,
                                    skulist_detail s,
                                    v_item_master i,
                                    item_ticket t
                              where v.default_wh             = I_location
                                and s.skulist = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';
      elsif I_loc_type = 'AS' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store v,
                                    skulist_detail s,
                                    v_item_master i,
                                    item_ticket t
                              where s.skulist = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';
      elsif I_loc_type = 'AW' then
          insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'W',
                                    v_wh.wh,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_wh,
                                    skulist_detail s,
                                    item_ticket t
                              where s.skulist = I_item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
                                and v_wh.stockholding_ind = 'Y';
      elsif I_loc_type = 'AL' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store v,
                                    skulist_detail s,
                                    v_item_master i,
                                    item_ticket t
                              where s.skulist = I_item
                                and s.item = i.item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type
								and i.status = 'A'
                                and v.stockholding_ind = 'Y';

         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select s.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'W',
                                    v_wh.wh,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_wh,
                                    skulist_detail s,
                                    item_ticket t
                              where s.skulist = I_item
                                and s.item_level = s.tran_level
                                and s.item = t.item
                                and t.ticket_type_id = I_ticket_type;
      --------------------------------------------------------------------------------
      -- Location List Store and Item List.
      -- For a given skulist get each item in the skulist and its respective location
      -- from the loc_list_detail table for the given loc_list
      --------------------------------------------------------------------------------
      elsif I_loc_type = 'LLS' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select i.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    lld.location,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from skulist_detail  s,
                                    loc_list_detail lld,
                                    item_master     i,
                                    item_loc        iloc,        -- item is stocked at location
                                    item_ticket     t
                               where s.skulist    = I_item      -- Item_List
                                 and s.item       = i.item
                                 and s.item_level = s.tran_level
                                 and lld.loc_list = L_location
                                 and lld.loc_type = 'S'
                                 and iloc.loc     = lld.location
                                 and iloc.item    = i.item
                                 and s.item       = t.item
                                 and t.ticket_type_id = I_ticket_type
								 and i.status = 'A'
                                 and EXISTS (select 'x'
                                               from store s
                                              where lld.location = s.store
                                                and s.stockholding_ind = 'Y');
      elsif I_loc_type = 'LLW' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select i.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'W',
                                    lld.location ,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from skulist_detail  s,
                                    loc_list_detail lld,
                                    item_master     i,
                                    item_loc        iloc,        -- item is stocked at location
                                    item_ticket     t
                               where s.skulist    = I_item   -- Item_List
                                 and s.item       = i.item
                                 and s.item_level = s.tran_level
                                 and lld.loc_list = L_location
                                 and lld.loc_type = 'W'
                                 and iloc.loc     = lld.location
                                 and iloc.item    = i.item
                                 and s.item       = t.item
								 and i.status = 'A'
                                 and t.ticket_type_id = I_ticket_type;
      elsif L_stockholding_store = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_LOC',
                                               I_location);
         return FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('LOC_TYPE_NOT_VALID',
                                               I_LOC_TYPE);
         return FALSE;
      end if;
   elsif I_item_type = 'I' then
      sql_lib.set_mark('INSERT',NULL,'TICKET_REQUEST','ITEM TYPE: '
        ||I_ITEM_TYPE||' ITEM:'||I_ITEM||' LOCATION TYPE:'
        ||I_LOC_TYPE||' LOCATION:'||I_location);
      if (L_stockholding_store) or I_loc_type = 'W' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                            values (I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    I_loc_type,
                                    I_location,
                                    I_country,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id);
      elsif I_loc_type = 'C' then
          insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v_store.store,
                                    I_country,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store
                              where v_store.store_class = I_location
                                and v_store.stockholding_ind = 'Y';
      elsif I_loc_type = 'A' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v_store.store,
                                    I_country,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store
                              where v_store.area = I_location
                                and v_store.stockholding_ind = 'Y';
      elsif I_loc_type = 'D' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v_store.store,
                                    I_country,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store
                              where v_store.district = I_location
                                and v_store.stockholding_ind = 'Y';
      elsif I_loc_type = 'R' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v_store.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store
                              where v_store.region  = I_location
                                and v_store.stockholding_ind = 'Y';
      elsif I_loc_type = 'T' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v_store.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store
                              where v_store.transfer_zone = I_location
                                and v_store.stockholding_ind = 'Y';
      elsif I_loc_type = 'L' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    loc_traits_matrix.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from loc_traits_matrix,
                                    v_store
                              where loc_traits_matrix.loc_trait = I_location
                                and loc_traits_matrix.store     = v_store.store
                                and v_store.stockholding_ind    = 'Y';
      elsif I_loc_type = 'DW' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v_store.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store
                              where default_wh = I_location
                                and v_store.stockholding_ind = 'Y';
      elsif I_loc_type = 'AS' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v_store.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store
                              where v_store.stockholding_ind = 'Y';
      elsif I_loc_type = 'AW' then
          insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                     I_quantity,
                                    'W',
                                    v_wh.wh,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_wh;
      elsif I_loc_type = 'AL' then
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    v_store.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store
                              where v_store.stockholding_ind = 'Y';
         insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                     I_quantity,
                                    'W',
                                    v_wh.wh,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_wh;
      ------------------
      -- Single Item
      ------------------
      ---------------------------------------------------
      -- Handle Location Lists for WH (LLW)
      ---------------------------------------------------
      elsif I_loc_type = 'LLW' then
          insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select I_item,
                                    I_ticket_type,
                                    I_quantity,
                                    'W',
                                    w.wh,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_wh  w,
                                    loc_list_detail lld,
                                    item_loc i                -- item is stocked at location
                               where w.wh         = lld.location
                                 and lld.loc_list = L_location
                                 and lld.loc_type = 'W'
                                 and w.wh         = i.loc
                                 and i.item       = I_item ;
      ---------------------------------------------------
      -- Handle Location Lists for STORE (LLS)
      ---------------------------------------------------
      elsif I_loc_type = 'LLS' then
          insert into ticket_request(item,
                                    ticket_type_id,
                                    qty,
                                    loc_type,
                                    location,
                                    country_of_origin,
                                    multi_units,
                                    multi_unit_retail,
                                    unit_retail,
                                    print_online_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select i.item,
                                    I_ticket_type,
                                    I_quantity,
                                    'S',
                                    s.store,
                                    I_country,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_print_online_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from v_store s,
                                    loc_list_detail lld,
                                    item_loc i               -- item is stocked at location
                               WHERE s.store      = lld.location
                                 and lld.loc_list = L_location
                                 and lld.loc_type = 'S'
                                 and s.store      = i.loc
                                 and i.item       = I_item
                                 and s.stockholding_ind = 'Y';
      elsif L_stockholding_store = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_LOC',
                                               I_location);
         return FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('LOC_TYPE_NOT_VALID',
                                               I_loc_type);
         return FALSE;
      end if;
   elsif I_item_type = 'P' then
      sql_lib.set_mark('INSERT',NULL,'TICKET_REQUEST','ORDER: '
        ||I_ITEM);

      insert into ticket_request(item,
                                 ticket_type_id,
                                 qty,
                                 loc_type,
                                 location,
                                 country_of_origin,
                                 order_no,
                                 multi_units,
                                 multi_unit_retail,
                                 unit_retail,
                                 print_online_ind,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
                          select NVL(ah.item, ol.item),
                                 I_ticket_type,
                                 ROUND(NVL(ad.qty_allocated,
                                     ol.qty_ordered)),
                                 'S',
                                 NVL(ad.to_loc,
                                     ol.location),
                                 NVL(I_country, os.origin_country_id),
                                 I_item,
                                 NULL,
                                 NULL,
                                 NULL,
                                 I_print_online_ind,
                                 L_sysdate,
                                 L_sysdate,
                                 L_user_id
                            from v_ordloc ol,
                                 alloc_header ah,
                                 alloc_detail ad,
                                 v_ordsku os,
                                 item_ticket t
                           where ol.order_no     = I_item
                             and ah.order_no(+)  = ol.order_no
                             and ah.wh(+)        = ol.location
                             and ol.order_no     = os.order_no
                             and ol.item         = os.item
                             and ah.item(+)      = ol.item
                             and ol.item         = t.item
                             and t.ticket_type_id = I_ticket_type
                             and ad.alloc_no(+)  = ah.alloc_no
                             and (ad.to_loc_type = 'S'
                              or (ol.loc_type = 'S'
                                  and ad.to_loc is NULL))
                             and ((ad.to_loc_type = 'S'
                                  and EXISTS (select 'x'
                                                from store s
                                               where ad.to_loc = s.store
                                                 and s.stockholding_ind = 'Y'))
                              or (ol.loc_type = 'S'
                                  and ad.to_loc is NULL
                                  and  EXISTS (select 'x'
                                                 from store s
                                                where ol.location = s.store
                                                  and s.stockholding_ind = 'Y')));

      insert into ticket_request(item,
                                 ticket_type_id,
                                 qty,
                                 loc_type,
                                 location,
                                 country_of_origin,
                                 order_no,
                                 multi_units,
                                 multi_unit_retail,
                                 unit_retail,
                                 print_online_ind,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
                          select ol.item,
                                 I_ticket_type,
                                 ol.qty_ordered,
                                 'W',
                                 ol.location,
                                 os.origin_country_id,
                                 I_item,
                                 NULL,
                                 NULL,
                                 NULL,
                                 I_print_online_ind,
                                 L_sysdate,
                                 L_sysdate,
                                 L_user_id
                            from v_ordloc ol,
                                 v_ordsku os,
                                 item_ticket t
                           where ol.order_no     = I_item
                             and ol.order_no     = os.order_no
                             and ol.item         = os.item
                             and ol.item         = t.item
                             and t.ticket_type_id = I_ticket_type;

   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'TICKET_SQL.GET_NAME',
                                             to_char(SQLCODE));
       return FALSE;
END REQUEST;
------------------------------------------------------------------------------
FUNCTION APPROVE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                 I_print_online_ind  IN TICKET_REQUEST.PRINT_ONLINE_IND%TYPE)
RETURN BOOLEAN IS

   L_user_id VARCHAR2(30) := get_user;
   L_sysdate DATE         := sysdate;
BEGIN
   sql_lib.set_mark('INSERT',NULL,'TICKET_REQUEST','Order: '
      ||I_order_no);
   insert into ticket_request(item,
                              ticket_type_id,
                              qty,
                              loc_type,
                              location,
                              country_of_origin,
                              order_no,
                              multi_units,
                              multi_unit_retail,
                              unit_retail,
                              print_online_ind,
                              create_datetime,
                              last_update_datetime,
                              last_update_id)
                       select item, 
                              ticket_type_id , 
                              sum(qty), 
                              location_type, 
                              location, 
                              origin_country_id,
                              I_order_no,
                              NULL,
                              NULL,
                              NULL,
                              I_print_online_ind,
                              L_sysdate,
                              L_sysdate,
                              L_user_id
                        from (select nvl(ah.item,ol.item) item, 
                             it.ticket_type_id ticket_type_id, 
                             round(nvl(ad.qty_allocated,ol.qty_ordered)* 
                                    (1 + nvl(it.ticket_over_pct, 0)/100)) qty, 
                             nvl(ad.to_loc_type,ol.loc_type) location_type, 
                             NVL(ad.to_loc,ol.location) location, 
                             os.origin_country_id origin_country_id, 
                             NULL, 
                             NULL, 
                             NULL 
                         from ordloc ol,
                              ordsku os,
                              alloc_header ah,
                              alloc_detail ad,
                              item_ticket it
                        where ol.order_no= I_order_no
                          and ah.order_no(+)= ol.order_no
                          and os.order_no = ol.order_no
                          and ah.wh(+) = ol.location
                          and ah.item(+)= ol.item
                          and os.item = ol.item
                          and ad.alloc_no(+) = ah.alloc_no
                          and it.item = ol.item
                          and it.po_print_type = 'A' 
               UNION ALL 
                      select vpq.item item, 
                             it.ticket_type_id ticket_type_id, 
                             round(nvl(ad.qty_allocated,ol.qty_ordered)*(vpq.qty)* 
                                      (1 + nvl(it.ticket_over_pct, 0)/100))qty, 
                             nvl(ad.to_loc_type,ol.loc_type) location_type, 
                             nvl(ad.to_loc,ol.location)location, 
                             os.origin_country_id origin_country_id, 
                             NULL, 
                             NULL, 
                             NULL 
                        from ordloc ol, 
                             ordsku os, 
                             alloc_header ah, 
                             alloc_detail ad, 
                             item_ticket it, 
                             item_master im, 
                             v_packsku_qty vpq 
                       where ol.order_no= I_order_no 
                         and ah.order_no(+)= ol.order_no 
                         and os.order_no = ol.order_no 
                         and ah.wh(+) = ol.location 
                         and ad.alloc_no(+) = ah.alloc_no 
                         and ah.item(+)= ol.item 
                         and os.item = ol.item 
                         and vpq.pack_no=os.item 
                         and os.item=im.item 
                         and im.pack_ind='Y' 
                         and it.item = vpq.item 
                         and it.po_print_type = 'A' 
                         and not exists(select 'Y' 
                                          from item_ticket it 
                                         where os.item=it.item 
                                           and rownum=1)) 
             group by item, 
                      ticket_type_id, 
                      location_type, 
                      location, 
                      origin_country_id, 
                      I_order_no, 
                      I_print_online_ind, 
                      L_sysdate, 
                      L_sysdate, 
                      L_user_id; 
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
    O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                        SQLERRM,
                                            'TICKET_SQL.APPROVE',
                                        to_char(SQLCODE));
   RETURN FALSE;
END APPROVE;
------------------------------------------------------------------------------
   FUNCTION COPY_TICKET(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_copy_from_item  IN     ITEM_TICKET.ITEM%TYPE,
                       I_copy_to_item    IN     ITEM_TICKET.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_user_id VARCHAR2(30) := get_user;
   L_sysdate DATE         := sysdate;

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                     NULL,
                    'ITEM_TICKET',
                     (I_copy_from_item));
   insert into item_ticket (item,
                            ticket_type_id,
                            po_print_type,
                            print_on_pc_ind,
                            ticket_over_pct,
                            create_datetime,
                            last_update_datetime,
                            last_update_id)
                     select I_copy_to_item,
                            ticket_type_id,
                            po_print_type,
                            print_on_pc_ind,
                            ticket_over_pct,
                            L_sysdate,
                            L_sysdate,
                            L_user_id
                       from item_ticket
                      where item = I_copy_from_item;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'TICKET_SQL.COPY_TICKET',
                                             to_char(SQLCODE));
       return FALSE;
END COPY_TICKET;
------------------------------------------------------------------------------
FUNCTION RECEIVE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                 I_item             IN     ITEM_TICKET.ITEM%TYPE,
                 I_store            IN     STORE.STORE%TYPE,
                 I_receipt_qty      IN     ORDLOC.QTY_RECEIVED%TYPE,
                 I_print_online_ind IN     TICKET_REQUEST.PRINT_ONLINE_IND%TYPE)
   RETURN BOOLEAN IS

   L_ticket_type          TICKET_REQUEST.TICKET_TYPE_ID%TYPE;
   L_qty                  TICKET_REQUEST.QTY%TYPE;
   L_country_of_origin    TICKET_REQUEST.COUNTRY_OF_ORIGIN%TYPE;
   L_sysdate              DATE         := sysdate;
   L_user_id              VARCHAR2(30) := get_user;

BEGIN
   SQL_LIB.SET_MARK('INSERT', NULL, 'ticket_request,item_ticket,ordhead','Item: '||
                    (I_item) || ' ORDER_NO: '|| to_char(I_order_no));

   insert into TICKET_REQUEST(item,
                              ticket_type_id,
                              qty,
                              loc_type,
                              location,
                              unit_retail,
                              multi_units,
                              multi_unit_retail,
                              country_of_origin,
                              order_no,
                              print_online_ind,
                              create_datetime,
                              last_update_datetime,
                              last_update_id)
                       select item,
                              ticket_type_id,
                              sum(qty),
                              location_type,
                              location,
                              NULL,
                              NULL,
                              NULL,
                              origin_country_id,
                              I_order_no,
                              I_print_online_ind,
                              L_sysdate,
                              L_sysdate,
                              L_user_id
                        from (select I_item item, 
                              it.ticket_type_id ticket_type_id, 
                              ROUND((1 + (NVL(it.ticket_over_pct, 0)/100)) * I_receipt_qty) qty, 
                              'S' location_type, 
                              I_store location, 
                              NULL, 
                              NULL, 
                              NULL, 
                              os.origin_country_id origin_country_id 

                         from item_ticket it,
                              ordsku os
                        where it.item          = I_item
                          and os.order_no      = I_order_no
                          and os.item          = it.item
                          and it.po_print_type = 'R' 
                       UNION ALL 
                       select vpq.item item, 
                              it.ticket_type_id ticket_type_id, 
                              ROUND((1 + (NVL(it.ticket_over_pct, 0)/100)) * I_receipt_qty*vpq.qty) qty, 
                              'S' location_type, 
                              I_store location, 
                              NULL, 
                              NULL, 
                              NULL, 
                              os.origin_country_id origin_country_id 
                         from item_ticket it, 
                              ordsku os, 
                              item_master im, 
                              v_packsku_qty vpq 
                        where os.order_no = I_order_no 
                          and vpq.pack_no=os.item 
                          and os.item=im.item 
                          and im.pack_ind='Y' 
                          and it.item = vpq.item 
                          and it.po_print_type = 'R' 
                          and not exists(select 'Y' 
                                           from item_ticket it 
                                          where os.item=it.item 
                                            and rownum=1)) 
                 group by item, 
                      ticket_type_id, 
                      location_type, 
                      location, 
                      origin_country_id, 
                      I_order_no, 
                      I_print_online_ind, 
                      L_sysdate, 
                      L_sysdate, 
                      L_user_id; 


   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'TICKET_SQL.RECEIVE',
                                             to_char(SQLCODE));
       return FALSE;
END RECEIVE;
------------------------------------------------------------------------------


--------------------------------------------------------------------------------
FUNCTION GET_ITEM_TICKET_INFO
             (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
              O_no_or_multi_ind IN OUT VARCHAR2,
              O_ticket_type_id  IN OUT ITEM_TICKET.TICKET_TYPE_ID%TYPE,
              O_po_print_type   IN OUT ITEM_TICKET.PO_PRINT_TYPE%TYPE,
              O_print_on_pc_ind IN OUT ITEM_TICKET.PRINT_ON_PC_IND%TYPE,
              O_print_over_pct  IN OUT ITEM_TICKET.TICKET_OVER_PCT%TYPE,
              I_ticket_type_id  IN     ITEM_TICKET.TICKET_TYPE_ID%TYPE,
              I_item            IN     ITEM_TICKET.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_ITEM_TICKET_INFO is
      select ticket_type_id,
             po_print_type,
             print_on_pc_ind,
             ticket_over_pct
        from item_ticket
       where item = I_item
         and ticket_type_id = nvl(I_ticket_type_id, ticket_type_id);

BEGIN

   if I_item is NULL then
      O_error_message:=sql_lib.create_msg('INVALID_PARM',
                                          'I_item',
                                          'NULL',
                                          'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ITEM_TICKET_INFO', 'ITEM_TICKET','I_ITEM');
   open C_GET_ITEM_TICKET_INFO;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_ITEM_TICKET_INFO', 'ITEM_TICKET','I_ITEM');
   fetch C_GET_ITEM_TICKET_INFO into O_ticket_type_id,
                                     O_po_print_type,
                                     O_print_on_pc_ind,
                                     O_print_over_pct;
      ---
      if C_GET_ITEM_TICKET_INFO%NOTFOUND then
         O_no_or_multi_ind := 'N';
      else
         SQL_LIB.SET_MARK('FETCH', 'C_GET_ITEM_TICKET_INFO', 'ITEM_TICKET','I_ITEM');
         fetch C_GET_ITEM_TICKET_INFO into O_ticket_type_id,
                                           O_po_print_type,
                                           O_print_on_pc_ind,
                                           O_print_over_pct;
         ---
         if C_GET_ITEM_TICKET_INFO%NOTFOUND then
            O_no_or_multi_ind := 'O';
         else
            O_no_or_multi_ind := 'M';
            O_ticket_type_id := NULL;
            O_po_print_type:= NULL;
            O_print_on_pc_ind:= NULL;
            O_print_over_pct:= NULL;
         end if;
         ---
      end if;
      ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ITEM_TICKET_INFO', 'ITEM_TICKET','I_ITEM');
   close C_GET_ITEM_TICKET_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message:=sql_lib.create_msg('PACKAGE_ERROR',
                                          SQLERRM,
                                          'TICKET_SQL.GET_ITEM_TICKET_INFO',
                                          to_char(SQLCODE));
      return FALSE;
END GET_ITEM_TICKET_INFO;
--------------------------------------------------------------------------------------------
FUNCTION INSERT_TICKET_REQUEST
      (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
       I_item              IN     TICKET_REQUEST.ITEM%TYPE,
       I_ticket_type_id    IN     TICKET_REQUEST.TICKET_TYPE_ID%TYPE,
       I_qty               IN     TICKET_REQUEST.QTY%TYPE,
       I_loc_type          IN     TICKET_REQUEST.LOC_TYPE%TYPE,
       I_location          IN     TICKET_REQUEST.LOCATION%TYPE,
       I_unit_retail       IN     TICKET_REQUEST.UNIT_RETAIL%TYPE,
       I_multi_units       IN     TICKET_REQUEST.MULTI_UNITS%TYPE,
       I_multi_unit_retail IN     TICKET_REQUEST.MULTI_UNIT_RETAIL%TYPE,
       I_country_of_origin IN     TICKET_REQUEST.COUNTRY_OF_ORIGIN%TYPE,
       I_order_no          IN     TICKET_REQUEST.ORDER_NO%TYPE,
       I_print_online_ind  IN     TICKET_REQUEST.PRINT_ONLINE_IND%TYPE,
       I_ticket_over_pct   IN     ITEM_TICKET.TICKET_OVER_PCT%TYPE)
 RETURN BOOLEAN IS

   L_item_level      ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level      ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_loc_type        TICKET_REQUEST.LOC_TYPE%TYPE;
   L_user_id         VARCHAR2(30) := get_user;
   L_sysdate         DATE         := sysdate;

BEGIN
   if I_item is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_item',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   elsif I_ticket_type_id is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_ticket_type_id',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   elsif I_location is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_location',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_loc_type is NULL then
      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_loc_type,
                                      I_location) = FALSE then
         return FALSE;
      end if;
   else
      L_loc_type:= I_loc_type;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                 L_item_level,
                                 L_tran_level,
                                 I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if L_item_level < L_tran_level then
      ---
      --I_qty is not used when exploding a parent to the tran. item level
      --because there is no way to determine how the qty should be
      --divided over the Items in the parent.  I_qty is used when
      --the a item is being used in the insert.
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'TICKET_REQUEST',I_ITEM);
      insert into ticket_request(item,
                                 ticket_type_id,
                                 qty,
                                 loc_type,
                                 location,
                                 print_online_ind,
                                 unit_retail,
                                 multi_units,
                                 multi_unit_retail,
                                 country_of_origin,
                                 order_no,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
                         (select item,
                                 I_ticket_type_id,
                                 CEIL(stock_on_hand+((nvl(I_ticket_over_pct,0)
                                 /100)*stock_on_hand)),
                                 L_loc_type,
                                 I_location,
                                 I_print_online_ind,
                                 I_unit_retail,
                                 I_multi_units,
                                 I_multi_unit_retail,
                                 I_country_of_origin,
                                 I_order_no,
                                 L_sysdate,
                                 L_sysdate,
                                 L_user_id
                            from item_loc_soh
                           where stock_on_hand != 0
                             and (item_parent    = I_item
                                  or item_grandparent = I_item)
                             and loc            = I_location
                             and loc_type       = L_loc_type);
   else
      SQL_LIB.SET_MARK('INSERT',NULL,'TICKET_REQUEST',I_ITEM);
      insert into ticket_request(item,
                                 ticket_type_id,
                                 qty,
                                 loc_type,
                                 location,
                                 print_online_ind,
                                 unit_retail,
                                 multi_units,
                                 multi_unit_retail,
                                 country_of_origin,
                                 order_no,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
                          values (I_item,
                                  I_ticket_type_id,
                                  CEIL(nvl(I_qty,1)+((nvl(I_ticket_over_pct,0)
                                  /100)*nvl(I_qty,1))),
                                  L_loc_type,
                                  I_location,
                                  I_print_online_ind,
                                  I_unit_retail,
                                  I_multi_units,
                                  I_multi_unit_retail,
                                  I_country_of_origin,
                                  I_order_no,
                                  L_sysdate,
                                  L_sysdate,
                                  L_user_id);
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TICKET_SQL.INSERT_TICKET_REQUEST',
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_TICKET_REQUEST;
--------------------------------------------------------------------------------------
FUNCTION INSERT_TICKET_TO_CHILDREN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                   I_item_type     IN     VARCHAR2,
                                   I_item_level    IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                                   I_tran_level    IN     ITEM_MASTER.TRAN_LEVEL%TYPE)
RETURN BOOLEAN IS

   L_item               ITEM_MASTER.ITEM%TYPE;
   L_sysdate            DATE                     := sysdate;
   L_user_id            VARCHAR2(30) := get_user;
   L_item_level         ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level         ITEM_MASTER.TRAN_LEVEL%TYPE;
   cursor C_GET_ITEMLIST_ITEMS is
      select sd.item,
             im.item_level,
             im.tran_level
        from skulist_detail sd,
             item_master im
       where sd.skulist = to_number(I_item)
         and sd.item    = im.item
         and im.tran_level >= im.item_level
         and exists (select 'x'
                       from item_ticket it
                      where it.item = im.item);
   ---
   cursor C_LOCK_ITEMS is
      select 'x'
        from item_ticket
       where exists (select 'x'
                       from item_master im
                      where im.item = item_ticket.item
                        and (im.item_parent = L_item
                              or im.item_grandparent = L_item))
         for update nowait;

BEGIN
   if I_item_type = 'ITEM' then
      L_item := I_item;
      ---
      open C_LOCK_ITEMS;
      close C_LOCK_ITEMS;
      ---
      if I_tran_level = (I_item_level + 2) then
         delete from item_ticket
          where exists (select 'x'
                          from item_master im
                         where im.item = item_ticket.item
                           and (im.item_parent = L_item
                                 or im.item_grandparent = L_item));
         ---
         insert into item_ticket (item,
                                  ticket_type_id,
                                  po_print_type,
                                  print_on_pc_ind,
                                  ticket_over_pct,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
                           select im.item,
                                  it.ticket_type_id,
                                  it.po_print_type,
                                  it.print_on_pc_ind,
                                  it.ticket_over_pct,
                                  L_sysdate,
                                  L_sysdate,
                                  L_user_id
                             from item_ticket it,
                                  item_master im
                            where (it.item = im.item_parent
                                   or it.item = im.item_grandparent)
                              and it.item = L_item;
      elsif I_tran_level = (I_item_level + 1) then
         delete from item_ticket
          where exists (select 'x'
                          from item_master im
                         where im.item = item_ticket.item
                           and im.item_parent = L_item);
         ---
         insert into item_ticket (item,
                                  ticket_type_id,
                                  po_print_type,
                                  print_on_pc_ind,
                                  ticket_over_pct,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
                           select im.item,
                                  it.ticket_type_id,
                                  it.po_print_type,
                                  it.print_on_pc_ind,
                                  it.ticket_over_pct,
                                  L_sysdate,
                                  L_sysdate,
                                  L_user_id
                             from item_ticket it,
                                  item_master im
                            where it.item = im.item_parent
                              and it.item = L_item;
      end if;
   elsif I_item_type = 'ITEMLIST' then
      for rec in C_GET_ITEMLIST_ITEMS LOOP
         L_item := rec.item;
         L_item_level := rec.item_level;
         L_tran_level := rec.tran_level;
         ---
         open C_LOCK_ITEMS;
         close C_LOCK_ITEMS;
         ---
         if L_tran_level = (L_item_level + 2) then
            delete from item_ticket
             where exists (select 'x'
                             from item_master im
                            where im.item = item_ticket.item
                              and (im.item_parent = L_item
                                    or im.item_grandparent = L_item));
            ---
            insert into item_ticket (item,
                                     ticket_type_id,
                                     po_print_type,
                                     print_on_pc_ind,
                                     ticket_over_pct,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id)
                              select im.item,
                                     it.ticket_type_id,
                                     it.po_print_type,
                                     it.print_on_pc_ind,
                                     it.ticket_over_pct,
                                     L_sysdate,
                                     L_sysdate,
                                     L_user_id
                                from item_ticket it,
                                     item_master im
                               where (it.item = im.item_parent
                                      or it.item = im.item_grandparent)
                                 and it.item = L_item;
         elsif L_tran_level = (L_item_level +1) then
            delete from item_ticket
             where exists (select 'x'
                             from item_master im
                            where im.item = item_ticket.item
                              and im.item_parent = L_item);
            ---
            insert into item_ticket (item,
                                     ticket_type_id,
                                     po_print_type,
                                     print_on_pc_ind,
                                     ticket_over_pct,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id)
                              select im.item,
                                     it.ticket_type_id,
                                     it.po_print_type,
                                     it.print_on_pc_ind,
                                     it.ticket_over_pct,
                                     L_sysdate,
                                     L_sysdate,
                                     L_user_id
                                from item_ticket it,
                                     item_master im
                               where it.item = im.item_parent
                                 and it.item = L_item;
         end if;
      END LOOP;
    end if;
    ---
    return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                           'TICKET_SQL.INSERT_TICKET_TO_CHILDREN',
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_TICKET_TO_CHILDREN;
-----------------------------------------------------------------------------------------------
FUNCTION TICKET_TYPE_ITEM_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists        IN OUT BOOLEAN,
                                 I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                 I_ticket_type   IN     ITEM_TICKET.TICKET_TYPE_ID%TYPE)
RETURN BOOLEAN IS

   L_exists             VARCHAR2(1)  := NULL;
   cursor C_EXISTS is
      select 'x'
        from item_ticket
       where ticket_type_id = I_ticket_type
         and item = I_item;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'ITEM_TICKET','I_ITEM');
   open C_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'ITEM_TICKET','I_ITEM');
   fetch C_EXISTS into L_exists;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'ITEM_TICKET', 'I_ITEM');
   close C_EXISTS;
   ---
   if L_exists = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                           'TICKET_SQL.TICKET_TYPE_ITEM_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
END TICKET_TYPE_ITEM_EXISTS;
--------------------------------------------------------------------------------------
FUNCTION NEW_PRICE_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_ticket_tbl    IN     TICKET_SQL.TICKET_TBL)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'TICKET_SQL.NEW_PRICE_CHANGE';

BEGIN

   SQL_LIB.SET_MARK('INSERT', NULL, 'TICKET_REQUEST', 'BULK INSERT');

   FORALL i in 1..I_ticket_tbl.COUNT
      insert into ticket_request
           values I_ticket_tbl(i);

   return TRUE;
 EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END NEW_PRICE_CHANGE;
--------------------------------------------------------------------------------------
FUNCTION UPDATE_PRICE_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ticket_tbl    IN     TICKET_SQL.TICKET_TBL)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'TICKET_SQL.UPDATE_PRICE_CHANGE';

BEGIN
   FOR i in I_ticket_tbl.first..I_ticket_tbl.last LOOP
      if not LOCK_TICKET(O_error_message,
                         I_ticket_tbl(i).item,
                         I_ticket_tbl(i).ticket_type_id,
                         I_ticket_tbl(i).location,
                         I_ticket_tbl(i).price_change_id) then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'TICKET_REQUEST',
                       'item: '||I_ticket_tbl(i).item||', loc: '||I_ticket_tbl(i).location||',
                       price_change_id: '||I_ticket_tbl(i).price_change_id);

      update ticket_request
         set qty             = I_ticket_tbl(i).qty,
             unit_retail     = I_ticket_tbl(i).unit_retail,
             multi_units     = I_ticket_tbl(i).multi_units,
             multi_unit_retail     = I_ticket_tbl(i).multi_unit_retail,
             last_update_datetime  = I_ticket_tbl(i).last_update_datetime,
             last_update_id        = I_ticket_tbl(i).last_update_id,
             price_change_eff_date = I_ticket_tbl(i).price_change_eff_date
       where item            = I_ticket_tbl(i).item
         and ticket_type_id  = I_ticket_tbl(i).ticket_type_id
         and location        = I_ticket_tbl(i).location
         and loc_type        = 'S'
         and price_change_id = I_ticket_tbl(i).price_change_id;

   end LOOP;

   return TRUE;

 EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END UPDATE_PRICE_CHANGE;
--------------------------------------------------------------------------------------
FUNCTION DELETE_PRICE_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ticket_tbl    IN     TICKET_SQL.TICKET_TBL)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'TICKET_SQL.DELETE_PRICE_CHANGE';

BEGIN
   FOR i in I_ticket_tbl.first..I_ticket_tbl.last LOOP
      if not LOCK_TICKET(O_error_message,
                         I_ticket_tbl(i).item,
                         I_ticket_tbl(i).ticket_type_id,
                         I_ticket_tbl(i).location,
                         I_ticket_tbl(i).price_change_id) then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'TICKET_REQUEST',
                       'item: '||I_ticket_tbl(i).item||', loc: '||I_ticket_tbl(i).location||',
                       price_change_id: '||I_ticket_tbl(i).price_change_id);

      delete from ticket_request
       where item            = I_ticket_tbl(i).item
         and ticket_type_id  = I_ticket_tbl(i).ticket_type_id
         and location        = I_ticket_tbl(i).location
         and loc_type        = 'S'
         and price_change_id = I_ticket_tbl(i).price_change_id;

   end LOOP;

   return TRUE;

 EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END DELETE_PRICE_CHANGE;
--------------------------------------------------------------------------------------
FUNCTION LOCK_TICKET(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_item            IN     TICKET_REQUEST.ITEM%TYPE,
                     I_ticket_type_id  IN     TICKET_REQUEST.TICKET_TYPE_ID%TYPE,
                     I_location        IN     TICKET_REQUEST.LOCATION%TYPE,
                     I_price_change_id IN     TICKET_REQUEST.PRICE_CHANGE_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'TICKET_SQL.LOCK_TICKET';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TICKET_REQUEST is
      select 'x'
        from ticket_request
       where item            = I_item
         and ticket_type_id  = I_ticket_type_id
         and location        = I_location
         and loc_type        = 'S'
         and price_change_id = I_price_change_id
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_TICKET_REQUEST',
                    'TICKET_REQUEST',
                    'item: '||I_item||', loc: '||TO_CHAR(I_location)||', price_change_id: '
                    ||TO_CHAR(I_price_change_id));
   open C_LOCK_TICKET_REQUEST;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_TICKET_REQUEST',
                    'TICKET_REQUEST',
                    'item: '||I_item||', loc: '||TO_CHAR(I_location)||', price_change_id: '
                    ||TO_CHAR(I_price_change_id));

   close C_LOCK_TICKET_REQUEST;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'TICKET_REQUEST',
                                            'item: '||I_item||', loc: '||TO_CHAR(I_location)||
                                            ', price_change_id: '||TO_CHAR(I_price_change_id),
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_TICKET;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_TICKET(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists           IN OUT   BOOLEAN,
                              I_ticket_type_id   IN       ITEM_TICKET.TICKET_TYPE_ID%TYPE,
                              I_item             IN       ITEM_TICKET.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'TICKET_SQL.VALIDATE_ITEM_TICKET';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_ITEM_TICKET is
      select 'Y'
        from item_ticket
       where ticket_type_id = I_ticket_type_id
         and item = I_item;

BEGIN

   L_dummy:='N';

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_TICKET',
                    'ITEM_TICKET',
                    'TICKET TYPE ID: '||I_ticket_type_id||', TICKET ITEM ID: '||I_item);
   open C_CHECK_ITEM_TICKET;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ITEM_TICKET',
                    'ITEM_TICKET',
                    'TICKET TYPE ID: '||I_ticket_type_id||', TICKET ITEM ID: '||I_item);
   fetch C_CHECK_ITEM_TICKET into L_dummy;

   if C_CHECK_ITEM_TICKET%FOUND then
      O_exists:= TRUE;
   else
      O_exists:= FALSE;
      O_error_message := 'INV_TCKT_TYPE';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ITEM_TICKET',
                    'ITEM_TICKET',
                    'TICKET TYPE ID: '||I_ticket_type_id||', TICKET ITEM ID: '||I_item);
   close C_CHECK_ITEM_TICKET;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END VALIDATE_ITEM_TICKET;
--------------------------------------------------------------------------------
END TICKET_SQL;
/
