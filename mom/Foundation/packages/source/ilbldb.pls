CREATE OR REPLACE PACKAGE BODY ITEMLIST_BUILD_SQL AS
----------------------------------------------------------
FUNCTION CLEAR_LIST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is
   ---
   L_program              VARCHAR2(64) := 'ITEMLIST_BUILD_SQL.GET_NAME';
   L_table                VARCHAR2(20) := NULL;
   L_exists               BOOLEAN;
   L_vdate                PERIOD.VDATE%TYPE := GET_VDATE;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   ---
   cursor C_HEADER_LOCK is
      select 'x'
        from SKULIST_DETAIL
       where skulist = I_itemlist
         for update nowait;

   cursor C_SIT_EXPLODE_LOCK is
      select 'x'
        from SIT_EXPLODE
       where skulist = I_itemlist
         for update nowait;
   ---
BEGIN

   ---
   open C_HEADER_LOCK;
   close C_HEADER_LOCK;

   delete from skulist_detail
    where skulist = I_itemlist;

   delete from skulist_dept_class_subclass
    where skulist_dept_class_subclass.skulist = I_itemlist;

   ---
   if SIT_SQL.SIT_EXISTS(O_error_message,
                         L_exists,
                         I_itemlist,
                         NULL) = FALSE then
      return FALSE;
   end if;

   if L_exists then
      if SIT_SQL.COPY_SIT_CONFLICT(O_error_message,
                                   I_itemlist,
                                   NULL) =  FALSE then
         return FALSE;
      end if;
      ---
      open C_SIT_EXPLODE_LOCK;
      close C_SIT_EXPLODE_LOCK;
      ---
      delete from sit_explode
      where skulist = I_itemlist;
   end if;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_HEADER_LOCK%ISOPEN then
         close C_HEADER_LOCK;
         L_table := 'SKULIST_DETAIL';
      elsif C_SIT_EXPLODE_LOCK%ISOPEN then
         close C_SIT_EXPLODE_LOCK;
         L_table := 'SIT_EXPLODE';
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_itemlist));
      RETURN FALSE;

   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END CLEAR_LIST;
----------------------------------------------------------
FUNCTION REBUILD_LIST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_no_items      IN OUT NUMBER,
                       I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is

   L_program                VARCHAR2(64) := 'ITEMLIST_BUILD_SQL.REBUILD_LIST';

   L_table                  VARCHAR2(20) := NULL;
   L_action_type            SKULIST_CRITERIA.ACTION_TYPE%TYPE;
   L_pack_ind               SKULIST_DETAIL.PACK_IND%TYPE;
   L_item                   SKULIST_CRITERIA.ITEM%TYPE;
   L_item_level             SKULIST_CRITERIA.ITEM_LEVEL%TYPE;
   L_item_parent            SKULIST_CRITERIA.ITEM_PARENT%TYPE;
   L_item_grandparent       SKULIST_CRITERIA.ITEM_GRANDPARENT%TYPE;
   L_uda_id                 SKULIST_CRITERIA.UDA_ID%TYPE;
   L_uda_min_date           SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE;
   L_uda_max_date           SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE;
   L_uda_value              SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE;
   L_supplier               SKULIST_CRITERIA.SUPPLIER%TYPE;
   L_dept                   SKULIST_CRITERIA.DEPT%TYPE;
   L_class                  SKULIST_CRITERIA.CLASS%TYPE;
   L_subclass               SKULIST_CRITERIA.SUBCLASS%TYPE;
   L_diff_1                 SKULIST_CRITERIA.DIFF_1%TYPE;
   L_diff_2                 SKULIST_CRITERIA.DIFF_2%TYPE;
   L_diff_3                 SKULIST_CRITERIA.DIFF_3%TYPE;
   L_diff_4                 SKULIST_CRITERIA.DIFF_4%TYPE;
   L_season_id              SKULIST_CRITERIA.SEASON_ID%TYPE;
   L_phase_id               SKULIST_CRITERIA.PHASE_ID%TYPE;
   L_sellable_ind           ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE;
   L_vdate                  PERIOD.VDATE%TYPE := NULL;
   L_rowid                  ROWID;
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_SKULIST_DETAIL_ROWID is
     select rowid
       from skulist_detail
      where skulist_detail.item       = L_item
        and skulist_detail.skulist    = I_itemlist
          for update nowait;

   cursor C_CRITERIA is
      select action_type,
             item,
             item_level,
             item_parent,
             item_grandparent,
             uda_id,
             uda_value_min_date,
             uda_value_max_date,
             uda_value_lov,
             supplier,
             dept,
             class,
             subclass,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             season_id,
             phase_id
        from skulist_criteria
       where skulist = I_itemlist
    order by seq_no;

   cursor C_HEAD_LOCK is
      select 'x'
        from skulist_head
       where skulist = I_itemlist
         for update nowait;

BEGIN

   L_vdate := DATES_SQL.GET_VDATE;
   open C_HEAD_LOCK;
   close C_HEAD_LOCK;

   update SKULIST_HEAD
      set LAST_REBUILD_DATE = L_vdate
    where skulist = I_itemlist;

   if ITEMLIST_BUILD_SQL.CLEAR_LIST(O_error_message,
                                    I_itemlist) = FALSE then
      RETURN FALSE;
   end if;

   for rec in C_CRITERIA LOOP
      L_action_type := rec.action_type;
      L_item := rec.item;
      L_item_level := rec.item_level;
      L_item_parent := rec.item_parent;
      L_item_grandparent := rec.item_grandparent;
      L_uda_id := rec.uda_id;
      L_uda_min_date := rec.uda_value_min_date;
      L_uda_max_date := rec.uda_value_max_date;
      L_uda_value := rec.uda_value_lov;
      L_supplier := rec.supplier;
      L_dept := rec.dept;
      L_class := rec.class;
      L_subclass := rec.subclass;
      L_diff_1 := rec.diff_1;
      L_diff_2 := rec.diff_2;
      L_diff_3 := rec.diff_3;
      L_diff_4 := rec.diff_4;
      L_season_id := rec.season_id;
      L_phase_id := rec.phase_id;
      L_pack_ind := NULL;
      ---
      if L_item is not NULL then
         if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                          L_pack_ind,
                                          L_sellable_ind,
                                          L_orderable_ind,
                                          L_pack_type,
                                          L_item) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      ---If Action_Type = 'A'dd then call SKULIST_ADD---
      if L_action_type = 'A' then
         if ITEMLIST_ADD_SQL.SKULIST_ADD(I_itemlist,
                                         L_pack_ind,
                                         L_item,
                                         L_item_parent,
                                         L_item_grandparent,
                                         L_dept,
                                         L_class,
                                         L_subclass,
                                         L_supplier,
                                         L_diff_1,
                                         L_diff_2,
                                         L_diff_3,
                                         L_diff_4,
                                         L_uda_id,
                                         L_uda_value,
                                         L_uda_max_date,
                                         L_uda_min_date,
                                         L_season_id,
                                         L_phase_id,
                                         'N',
                                         'N',
                                         L_item_level,
                                         O_no_items,
                                         O_error_message) = FALSE then
            RETURN FALSE;
         end if;
      elsif L_action_type = 'D' then
         if L_item is not NULL then
            if ITEMLIST_BUILD_SQL.DELETE_SINGLE_ITEM(O_error_message,
                                                     L_item,
                                                     I_itemlist) = FALSE then
               RETURN FALSE;
            end if;
         else
            if ITEMLIST_DELETE_SQL.SKULIST_DELETE(I_itemlist,
                                                  L_pack_ind,
                                                  L_item,
                                                  L_item_parent,
                                                  L_item_grandparent,
                                                  L_dept,
                                                  L_class,
                                                  L_subclass,
                                                  L_supplier,
                                                  L_diff_1,
                                                  L_diff_2,
                                                  L_diff_3,
                                                  L_diff_4,
                                                  L_uda_id,
                                                  L_uda_value,
                                                  L_uda_max_date,
                                                  L_uda_min_date,
                                                  L_season_id,
                                                  L_phase_id,
                                                  'N',
                                                  'N',
                                                  L_item_level,
                                                  O_no_items,
                                                  O_error_message) = FALSE then
               RETURN FALSE;
            end if;
         end if;
      end if;
   END LOOP;
   ---
   if SIT_SQL.REBUILD_SIT_CONFLICT(O_error_message,
                                   I_itemlist,
                                   NULL) = FALSE then
         return FALSE;
      end if;

   if ITEMLIST_ATTRIB_SQL.GET_ITEM_COUNT(O_error_message,
                                         I_itemlist,
                                         O_no_items) = FALSE then
      RETURN FALSE;
   end if;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_HEAD_LOCK%ISOPEN then
         close C_HEAD_LOCK;
         L_table := 'SKULIST_HEAD';
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_itemlist));
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END REBUILD_LIST;
----------------------------------------------------------
FUNCTION GET_MAX_SEQUENCE_NO (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_sequence_no   IN OUT SKULIST_CRITERIA.SEQ_NO%TYPE,
                              I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(64) := 'ITEMLIST_BUILD_SQL.GET_MAX_SEQUENCE_NO';

   cursor C_SEQUENCE_NO is
      select nvl(max(seq_no) + 1,1)
        from skulist_criteria
       where skulist = I_itemlist;

BEGIN
   open C_SEQUENCE_NO;
   fetch C_SEQUENCE_NO into O_sequence_no;
   close C_SEQUENCE_NO;

   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END GET_MAX_SEQUENCE_NO;
----------------------------------------------------------
FUNCTION LOCK_RECORDS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is

   L_program      VARCHAR2(64) := 'ITEMLIST_BUILD_SQL.LOCK_RECORDS';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_SKULIST_HEAD is
      select 'x'
        from skulist_head
       where skulist = I_itemlist
         for update nowait;

BEGIN

   open C_GET_SKULIST_HEAD;
   close C_GET_SKULIST_HEAD;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('SKULIST_REC_LOCK',
                                            to_char(I_itemlist),
                                            NULL,
                                            NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END LOCK_RECORDS;
----------------------------------------------------------
FUNCTION INSERT_CRITERIA(I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE,
                         I_seq_no            IN     SKULIST_CRITERIA.SEQ_NO%TYPE,
                         I_action_type       IN     SKULIST_CRITERIA.ACTION_TYPE%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_item_parent       IN     ITEM_MASTER.ITEM%TYPE,
                         I_item_grandparent  IN     ITEM_MASTER.ITEM%TYPE,
                         I_uda_id            IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                         I_uda_min_date      IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                         I_uda_max_date      IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                         I_uda_value_lov     IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                         I_supplier          IN     SKULIST_CRITERIA.SUPPLIER%TYPE,
                         I_dept              IN     SKULIST_CRITERIA.DEPT%TYPE,
                         I_class             IN     SKULIST_CRITERIA.CLASS%TYPE,
                         I_subclass          IN     SKULIST_CRITERIA.SUBCLASS%TYPE,
                         I_diff_1            IN     SKULIST_CRITERIA.DIFF_1%TYPE,
                         I_diff_2            IN     SKULIST_CRITERIA.DIFF_2%TYPE,
                         I_diff_3            IN     SKULIST_CRITERIA.DIFF_3%TYPE,
                         I_diff_4            IN     SKULIST_CRITERIA.DIFF_4%TYPE,
                         I_season_id         IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                         I_phase_id          IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                         I_item_level        IN     SKULIST_CRITERIA.ITEM_LEVEL%TYPE,
                         O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN is

   L_program        VARCHAR2(64) := 'ITEMLIST_BUILD_SQL.INSERT_CRITERIA';

BEGIN

   SQL_LIB.SET_MARK('INSERT',NULL,'SKULIST_CRITERIA','ITEMLIST: '||I_itemlist||' SEQ_NO: '||to_char(I_seq_no));
    insert into SKULIST_CRITERIA
               (skulist,
                seq_no,
                action_type,
                item_level,
                item,
                item_parent,
                item_grandparent,
                uda_id,
                uda_value_min_date,
                uda_value_max_date,
                uda_value_lov,
                supplier,
                dept,
                class,
                subclass,
                diff_1,
                diff_2,
                diff_3,
                diff_4,
                season_id,
                phase_id,
                last_update_datetime,
                last_update_id,
                create_datetime)
      values(I_itemlist,
             I_seq_no,
             I_action_type,
             I_item_level,
             I_item,
             I_item_parent,
             I_item_grandparent,
             I_uda_id,
             I_uda_min_date,
             I_uda_max_date,
             I_uda_value_lov,
             I_supplier,
             I_dept,
             I_class,
             I_subclass,
             I_diff_1,
             I_diff_2,
             I_diff_3,
             I_diff_4,
             I_season_id,
             I_phase_id,
             sysdate,
             get_user,
             sysdate);

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END INSERT_CRITERIA;
----------------------------------------------------------
FUNCTION DELETE_CRITERIA(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_itemlist          IN     SKULIST_CRITERIA.SKULIST%TYPE,
                         I_item              IN     SKULIST_CRITERIA.ITEM%TYPE)
   RETURN BOOLEAN is

   L_program        VARCHAR2(64) := 'ITEMLIST_BUILD_SQL.DELETE_CRITERIA';

BEGIN

   SQL_LIB.SET_MARK('DELETE', NULL, 'SKULIST_CRITERIA',
                    'ITEMLIST: '||I_itemlist||' Item: '|| I_item);
   delete from SKULIST_CRITERIA
    where skulist = I_itemlist
      and item = I_item;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DELETE_CRITERIA;
----------------------------------------------------------
FUNCTION COPY_CRITERIA(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_itemlist_old      IN     SKULIST_HEAD.SKULIST%TYPE,
                       I_itemlist_new      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is

   L_program    VARCHAR2(255)   := 'ITEMLIST_BUILD_SQL.COPY_CRITERIA';

BEGIN

   insert into SKULIST_CRITERIA
               (skulist,
                seq_no,
                action_type,
                item_level,
                item,
                item_parent,
                item_grandparent,
                uda_id,
                uda_value_min_date,
                uda_value_max_date,
                uda_value_lov,
                supplier,
                dept,
                class,
                subclass,
                diff_1,
                diff_2,
                diff_3,
                diff_4,
                season_id,
                phase_id,
                last_update_datetime,
                last_update_id,
                create_datetime)
      select I_itemlist_new,
             seq_no,
             action_type,
             item_level,
             item,
             item_parent,
             item_grandparent,
             uda_id,
             uda_value_min_date,
             uda_value_max_date,
             uda_value_lov,
             supplier,
             dept,
             class,
             subclass,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             season_id,
             phase_id,
             sysdate,
             get_user,
             sysdate
        from skulist_criteria
       where skulist = I_itemlist_old;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END COPY_CRITERIA;
----------------------------------------------------------
FUNCTION INSERT_SINGLE_ITEM(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_itemlist           IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is

   L_program                  VARCHAR2(64)            := 'ITEMLIST_BUILD_SQL.INSERT_SINGLE_ITEM';
   L_itemlist_tpgitem_exists  VARCHAR2(1)                 := 'N';
   L_skulist                  SKULIST_HEAD.SKULIST%TYPE;
   L_new_skulist              SKULIST_HEAD.SKULIST%TYPE;
   L_itemlist_desc            SKULIST_HEAD.SKULIST_DESC%TYPE;
   L_pack_ind                 ITEM_MASTER.PACK_IND%TYPE;
   L_item_level               ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level               ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_create_date              SKULIST_HEAD.CREATE_DATE%TYPE;
   L_create_id                SKULIST_HEAD.CREATE_ID%TYPE;
   L_rebuild_date             SKULIST_HEAD.LAST_REBUILD_DATE%TYPE;
   L_static_ind               SKULIST_HEAD.STATIC_IND%TYPE;
   L_comment_desc             SKULIST_HEAD.COMMENT_DESC%TYPE;

   L_username                 USER_ROLE_PRIVS.USERNAME%TYPE := GET_USER;
   L_vdate                    PERIOD.VDATE%TYPE := GET_VDATE;
   L_sellable_ind             ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind            ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type                ITEM_MASTER.PACK_TYPE%TYPE;

BEGIN

   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;
   if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                 L_item_level,
                                 L_tran_level,
                                 I_item) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'SKULIST_DETAIL','ITEMLIST: '||to_char(I_itemlist));
   insert into skulist_detail(skulist,
                              item_level,
                              tran_level,
                              item,
                              pack_ind,
                              insert_id,
                              insert_date,
                              last_update_datetime,
                              last_update_id,
                              create_datetime)
                       values(I_itemlist,
                              L_item_level,
                              L_tran_level,
                              I_item,
                              L_pack_ind,
                              L_username,
                              L_vdate,
                              sysdate,
                              L_username,
                              sysdate);

   if SIT_SQL.INSERT_ITEM(O_error_message,
                          I_itemlist,
                          I_item) = FALSE then
      return FALSE;
   end if;

   RETURN TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      NULL;
      RETURN TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END INSERT_SINGLE_ITEM;
----------------------------------------------------------
FUNCTION DELETE_SINGLE_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE,
                            I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is

   L_program      VARCHAR2(64)      := 'ITEMLIST_BUILD_SQL.DELETE_SINGLE_ITEM';
   L_rowid        ROWID;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_SKULIST_DETAIL_ROWID is
     select rowid
       from skulist_detail
      where skulist_detail.item    = I_item
        and skulist_detail.skulist = I_itemlist
        for update nowait;

BEGIN
   open  C_GET_SKULIST_DETAIL_ROWID;
   fetch C_GET_SKULIST_DETAIL_ROWID into L_rowid;
   close C_GET_SKULIST_DETAIL_ROWID;

   delete from skulist_detail
    where rowid = L_rowid;

   if SIT_SQL.DELETE_ITEM(O_error_message,
                          I_itemlist,
                          I_item) = FALSE then
      return FALSE;
   end if;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('SKULIST_DETAIL_REC_LOCK',
                                            to_char(I_itemlist),
                                            NULL,
                                            NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DELETE_SINGLE_ITEM;
----------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION SKULIST_DEPT_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist             IN OUT BOOLEAN,
                            I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE,
                            I_merch_level       IN     SYSTEM_OPTIONS.SKULIST_ORG_LEVEL_CODE%TYPE,
                            I_merch_id          IN     DEPS.DEPT%TYPE,
                            I_merch_class_id    IN     CLASS.CLASS%TYPE,
                            I_merch_subclass_id IN     SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS



   L_program         VARCHAR2(64)        := 'ITEMLIST_BUILD_SQL.SKULIST_DEPT_EXIST';

   cursor c_skulist_dept_exist is
      select 'Y'
        from skulist_dept sd,
             v_deps vd,
             v_subclass vsc
       where sd.dept  = vsc.dept
         and sd.class = vsc.class
         and sd.skulist = I_itemlist
         and ((I_merch_level = 'D' and vd.division = I_merch_id and vd.dept = vsc.dept)
           or (I_merch_level = 'G' and vd.group_no = I_merch_id and vd.dept = vsc.dept)
           or (I_merch_level = 'P' and vd.dept     = I_merch_id and vd.dept = vsc.dept)
           or (I_merch_level = 'C' and vd.dept     = I_merch_id and vd.dept = vsc.dept and vsc.class = I_merch_class_id)
           or (I_merch_level = 'S' and vd.dept     = I_merch_id and vd.dept = vsc.dept and vsc.class = I_merch_class_id and sd.subclass = I_merch_subclass_id));

   L_skulist_dept_exist     VARCHAR2(1) := 'N';

BEGIN
   ---
   if I_itemlist is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_itemlist',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_merch_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_merch_level',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_merch_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_merch_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open c_skulist_dept_exist;
   fetch c_skulist_dept_exist into L_skulist_dept_exist;
   close c_skulist_dept_exist;
   ---
   if L_skulist_dept_exist = 'Y' then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END SKULIST_DEPT_EXIST;
---------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION INSERT_SKULIST_DEPT(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE,
                             I_merch_level       IN     SYSTEM_OPTIONS.SKULIST_ORG_LEVEL_CODE%TYPE,
                             I_merch_id          IN     DEPS.DEPT%TYPE,
                             I_merch_class_id    IN     CLASS.CLASS%TYPE,
                             I_merch_subclass_id IN     SUBCLASS.SUBCLASS%TYPE)
   RETURN BOOLEAN is
   ---
   L_program         VARCHAR2(64)        := 'ITEMLIST_BUILD_SQL.INSERT_SKULIST_DEPT';


BEGIN

   insert into skulist_dept(skulist,
                            dept,
                            class,
                            subclass)
                    select I_itemlist,
                           vsc.dept,
                           vsc.class,
                           vsc.subclass
                      from v_deps v,
                           v_subclass vsc
                     where ((I_merch_level = 'D' and v.division = I_merch_id and v.dept = vsc.dept)
                         or (I_merch_level = 'G' and v.group_no = I_merch_id and v.dept = vsc.dept)
                         or (I_merch_level = 'P' and v.dept     = I_merch_id and v.dept = vsc.dept)
                         or (I_merch_level = 'C' and v.dept     = I_merch_id and v.dept = vsc.dept and vsc.class = I_merch_class_id)
                         or (I_merch_level = 'S' and v.dept     = I_merch_id and v.dept = vsc.dept and vsc.class = I_merch_class_id and vsc.subclass = I_merch_subclass_id))
                       and not exists (select 'x'
                                         from skulist_dept sd
                                        where sd.dept     = vsc.dept
                                          and sd.class    = vsc.class
                                          and sd.subclass = vsc.subclass
                                          and sd.skulist  = I_itemlist);
   ---
   RETURN TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END INSERT_SKULIST_DEPT;
----------------------------------------------------------
FUNCTION COPY_SKULIST_DEPT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_skulist_old      IN     SKULIST_HEAD.SKULIST%TYPE,
                           I_skulist_new      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is
   ---
   L_program         VARCHAR2(64)        := 'ITEMLIST_BUILD_SQL.COPY_SKULIST_DEPT';

BEGIN
   ---
   insert into skulist_dept(skulist,
                            dept,
                            class,
                            subclass)
                    select I_skulist_new,
                           sd.dept,
                           sd.class,
                           sd.subclass
                      from skulist_dept sd
                     where sd.skulist = I_skulist_old;
   ---
   RETURN TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END COPY_SKULIST_DEPT;
----------------------------------------------------------
END ITEMLIST_BUILD_SQL;
/
