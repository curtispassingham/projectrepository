
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_VENDORCRE AS

PROGRAM_ERROR   EXCEPTION;

-------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status          IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2);
-------------------------------------------------------------------------
-------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code    IN OUT  VARCHAR2,
                   O_error_message  IN OUT  VARCHAR2,
                   I_message        IN      CLOB)
IS

BEGIN

   if RMSSUB_SUPPLIER.CONSUME(O_status_code,
                              O_error_message,
                              I_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    'RMSSUB_VENDORCRE.CONSUME');
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    'RMSSUB_VENDORCRE.CONSUME');
END CONSUME;
-------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status          IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2)
IS

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
    when OTHERS then
       IO_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'RMSSUB_VENDORCRE.HANDLE_ERRORS',
                                              to_char(SQLCODE));

       API_LIBRARY.HANDLE_ERRORS(O_status,
                                 IO_error_message,
                                 API_LIBRARY.FATAL_ERROR,
                                 'RMSSUB_VENDORCRE.HANDLE_ERRORS');

END HANDLE_ERRORS;
-------------------------------------------------------------------------
END RMSSUB_VENDORCRE;
/
