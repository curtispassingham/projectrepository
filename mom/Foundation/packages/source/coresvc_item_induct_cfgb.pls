create or replace PACKAGE BODY CORESVC_ITEM_INDUCT_CGF AS
   cursor C_SVC_ITEM_INDUCT_CONFIG(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             st.rowid AS st_rid,
             st.max_items_for_dnld,
             st.max_items_for_sync_dnld,
             st.max_file_size_for_upld,
             st.max_file_size_for_sync_upld,
             st.max_cc_for_sync_dnld,
             st.max_cc_for_dnld,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_item_induct_config st,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
;
   cursor C_SVC_CORESVC_ITEM_CONFIG(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             st.rowid AS st_rid,
             st.cascade_iud_item_supplier,
             st.max_threads,
             st.wait_btwn_threads,
             st.cascade_iim_details,
             st.max_chunk_size,
             st.proc_err_retention_days,
             st.cascade_uda_details,
             st.cascade_iud_item_supp_country,
             st.cascade_iud_isc_dimensions,
             st.cascade_iud_ismc,
             st.isc_update_all_locs,
             st.isc_update_all_child_locs,
             st.cascade_vat_item,
             st.max_item_resv_qty,
             st.max_item_expiry_days,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_coresvc_item_config st,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
;
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
---------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
---------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   ITEM_INDUCT_CONFIG_cols s9t_pkg.names_map_typ;
   CORESVC_ITEM_CONFIG_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                                    := s9t_pkg.get_sheet_names(I_file_id);
   ITEM_INDUCT_CONFIG_cols                     := s9t_pkg.get_col_names(I_file_id,ITEM_INDUCT_CONFIG_sheet);
   ITM_INT_CFG$Action                          := ITEM_INDUCT_CONFIG_cols('ACTION');
   ITM_INT_CFG$MAX_ITEMS_FOR_DNLD              := ITEM_INDUCT_CONFIG_cols('MAX_ITEMS_FOR_DNLD');
   ITM_INT_CFG$MX_ITM_FR_SNC_DNLD              := ITEM_INDUCT_CONFIG_cols('MAX_ITEMS_FOR_SYNC_DNLD');
   ITM_INT_CFG$MX_FL_SZ_FR_UPLD                := ITEM_INDUCT_CONFIG_cols('MAX_FILE_SIZE_FOR_UPLD');
   ITM_INT_CFG$MX_FL_SZ_FR_SNC_UP              := ITEM_INDUCT_CONFIG_cols('MAX_FILE_SIZE_FOR_SYNC_UPLD');
   ITM_INT_CFG$MX_CC_FR_SYNC_DNLD              := ITEM_INDUCT_CONFIG_cols('MAX_CC_FOR_SYNC_DNLD');
   ITM_INT_CFG$MAX_CC_FOR_DNLD                 := ITEM_INDUCT_CONFIG_cols('MAX_CC_FOR_DNLD');
   CORESVC_ITEM_CONFIG_cols                    := s9t_pkg.get_col_names(I_file_id,CORESVC_ITEM_CONFIG_sheet);
   CORESVC_ITEM_CONFIG$Action                  := CORESVC_ITEM_CONFIG_cols('ACTION');
   CRSV_ITM_CFG$CSCD_IUD_ITM_SUPP              := CORESVC_ITEM_CONFIG_cols('CASCADE_IUD_ITEM_SUPPLIER');
   CRSV_ITM_CFG$MAX_THREADS                    := CORESVC_ITEM_CONFIG_cols('MAX_THREADS');
   CRSV_ITM_CFG$WAIT_BTWN_THREADS              := CORESVC_ITEM_CONFIG_cols('WAIT_BTWN_THREADS');
   CRSV_ITM_CFG$CSCD_IIM_DETAILS               := CORESVC_ITEM_CONFIG_cols('CASCADE_IIM_DETAILS');
   CRSV_ITM_CFG$MAX_CHUNK_SIZE                 := CORESVC_ITEM_CONFIG_cols('MAX_CHUNK_SIZE');
   CRSV_ITM_CFG$PRC_ERR_RTNTN_DAY              := CORESVC_ITEM_CONFIG_cols('PROC_ERR_RETENTION_DAYS');
   CRSV_ITM_CFG$CSCD_UDA_DETAILS               := CORESVC_ITEM_CONFIG_cols('CASCADE_UDA_DETAILS');
   CRSV_ITM_CFG$CSCD_IUITMSPP_CTR              := CORESVC_ITEM_CONFIG_cols('CASCADE_IUD_ITEM_SUPP_COUNTRY');
   CRSV_ITM_CFG$CSCDIUDISC_DIM                 := CORESVC_ITEM_CONFIG_cols('CASCADE_IUD_ISC_DIMENSIONS');
   CRSV_ITM_CFG$CSCD_IUD_ISMC                  := CORESVC_ITEM_CONFIG_cols('CASCADE_IUD_ISMC');
   CRSV_ITM_CFG$ISC_UPD_ALL_LC                 := CORESVC_ITEM_CONFIG_cols('ISC_UPDATE_ALL_LOCS');
   CRSV_ITM_CFG$ISC_UPD_AL_CHD_LC              := CORESVC_ITEM_CONFIG_cols('ISC_UPDATE_ALL_CHILD_LOCS');
   CRSV_ITM_CFG$CASCADE_VAT_ITEM               := CORESVC_ITEM_CONFIG_cols('CASCADE_VAT_ITEM');
   CRSV_ITM_CFG$MAX_ITEM_RESV_QTY              := CORESVC_ITEM_CONFIG_cols('MAX_ITEM_RESV_QTY');
   CRSV_ITM_CFG$MAX_ITM_EXPRY_DAY              := CORESVC_ITEM_CONFIG_cols('MAX_ITEM_EXPIRY_DAYS');
END POPULATE_NAMES;
---------------------------------------------------------------
PROCEDURE POPULATE_ITEM_INDUCT_CONFIG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = ITEM_INDUCT_CONFIG_sheet )
   select s9t_row(s9t_cells(CORESVC_ITEM_INDUCT_CGF.action_mod ,
                           max_items_for_dnld,
                           max_items_for_sync_dnld,
                           max_file_size_for_upld,
                           max_file_size_for_sync_upld,
                           max_cc_for_sync_dnld,
                           max_cc_for_dnld
                           ))
     from item_induct_config ;
END POPULATE_ITEM_INDUCT_CONFIG;
---------------------------------------------------------
PROCEDURE POPULATE_CORESVC_ITEM_CONFIG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = CORESVC_ITEM_CONFIG_sheet )
   select s9t_row(s9t_cells(CORESVC_ITEM_INDUCT_CGF.action_mod ,
                           max_chunk_size,
						   proc_err_retention_days,
						   cascade_uda_details,
						   cascade_iud_item_supp_country,
						   cascade_iud_isc_dimensions,
						   cascade_iud_ismc,
						   isc_update_all_locs,
						   isc_update_all_child_locs,
						   cascade_vat_item,
						   max_item_resv_qty,
						   max_item_expiry_days,
						   max_threads,
						   wait_btwn_threads,
						   cascade_iim_details,
						   cascade_iud_item_supplier
                          
                           ))
     from coresvc_item_config ;
END POPULATE_CORESVC_ITEM_CONFIG;
------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
   cursor C_USER_LANG is
      select lang
        from user_attrib
       where user_id = get_user;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(ITEM_INDUCT_CONFIG_sheet);
   L_file.sheets(l_file.get_sheet_index(ITEM_INDUCT_CONFIG_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'MAX_ITEMS_FOR_DNLD'
                                                                                            ,'MAX_ITEMS_FOR_SYNC_DNLD'
                                                                                            ,'MAX_FILE_SIZE_FOR_UPLD'
                                                                                            ,'MAX_FILE_SIZE_FOR_SYNC_UPLD'
                                                                                            ,'MAX_CC_FOR_SYNC_DNLD'
                                                                                            ,'MAX_CC_FOR_DNLD');
   L_file.add_sheet(CORESVC_ITEM_CONFIG_sheet);
   L_file.sheets(l_file.get_sheet_index(CORESVC_ITEM_CONFIG_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                                 'MAX_CHUNK_SIZE'
                                                                                            ,'PROC_ERR_RETENTION_DAYS'
                                                                                            ,'CASCADE_UDA_DETAILS'
                                                                                            ,'CASCADE_IUD_ITEM_SUPP_COUNTRY'
                                                                                            ,'CASCADE_IUD_ISC_DIMENSIONS'
                                                                                            ,'CASCADE_IUD_ISMC'
                                                                                            ,'ISC_UPDATE_ALL_LOCS'
                                                                                            ,'ISC_UPDATE_ALL_CHILD_LOCS'
                                                                                            ,'CASCADE_VAT_ITEM'
                                                                                            ,'MAX_ITEM_RESV_QTY'
                                                                                            ,'MAX_ITEM_EXPIRY_DAYS'
                                                                                            ,'MAX_THREADS'
                                                                                            ,'WAIT_BTWN_THREADS'
                                                                                            ,'CASCADE_IIM_DETAILS'
																							,'CASCADE_IUD_ITEM_SUPPLIER'
);
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_ITEM_INDUCT_CGF.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;   

   if I_template_only_ind = 'N' then
      POPULATE_ITEM_INDUCT_CONFIG(O_file_id);
      POPULATE_CORESVC_ITEM_CONFIG(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ITEM_INDUCT_CONFIG( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_ITEM_INDUCT_CONFIG.process_id%TYPE) IS
   TYPE svc_ITEM_INDUCT_CONFIG_col_typ IS TABLE OF SVC_ITEM_INDUCT_CONFIG%ROWTYPE;
   L_temp_rec SVC_ITEM_INDUCT_CONFIG%ROWTYPE;
   svc_ITEM_INDUCT_CONFIG_col svc_ITEM_INDUCT_CONFIG_col_typ :=NEW svc_ITEM_INDUCT_CONFIG_col_typ();
   L_process_id SVC_ITEM_INDUCT_CONFIG.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_ITEM_INDUCT_CONFIG%ROWTYPE;
      L_pk_columns    VARCHAR2(255)  := 'Item Induction Configuration';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             MAX_ITEMS_FOR_DNLD_mi,
             MAX_ITEMS_FOR_SYNC_DNLD_mi,
             MAX_FILE_SIZE_FOR_UPLD_mi,
             MAX_FILE_SIZE_FOR_SYNC_UPLD_mi,
             MAX_CC_FOR_SYNC_DNLD_mi,
             MAX_CC_FOR_DNLD_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'ITEM_INDUCT_CONFIG'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'MAX_ITEMS_FOR_DNLD' AS MAX_ITEMS_FOR_DNLD,
                                         'MAX_ITEMS_FOR_SYNC_DNLD' AS MAX_ITEMS_FOR_SYNC_DNLD,
                                         'MAX_FILE_SIZE_FOR_UPLD' AS MAX_FILE_SIZE_FOR_UPLD,
                                         'MAX_FILE_SIZE_FOR_SYNC_UPLD' AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
                                         'MAX_CC_FOR_SYNC_DNLD' AS MAX_CC_FOR_SYNC_DNLD,
                                         'MAX_CC_FOR_DNLD' AS MAX_CC_FOR_DNLD,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       MAX_ITEMS_FOR_DNLD_dv,
                       MAX_ITEMS_FOR_SYNC_DNLD_dv,
                       MAX_FILE_SIZE_FOR_UPLD_dv,
                       MAX_FILE_SIZE_FOR_SYNC_UPLD_dv,
                       MAX_CC_FOR_SYNC_DNLD_dv,
                       MAX_CC_FOR_DNLD_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'ITEM_INDUCT_CONFIG'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'MAX_ITEMS_FOR_DNLD' AS MAX_ITEMS_FOR_DNLD,
                                                      'MAX_ITEMS_FOR_SYNC_DNLD' AS MAX_ITEMS_FOR_SYNC_DNLD,
                                                      'MAX_FILE_SIZE_FOR_UPLD' AS MAX_FILE_SIZE_FOR_UPLD,
                                                      'MAX_FILE_SIZE_FOR_SYNC_UPLD' AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
                                                      'MAX_CC_FOR_SYNC_DNLD' AS MAX_CC_FOR_SYNC_DNLD,
                                                      'MAX_CC_FOR_DNLD' AS MAX_CC_FOR_DNLD,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.MAX_ITEMS_FOR_DNLD := rec.MAX_ITEMS_FOR_DNLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'ITEM_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_ITEMS_FOR_DNLD ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_ITEMS_FOR_SYNC_DNLD := rec.MAX_ITEMS_FOR_SYNC_DNLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'ITEM_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_ITEMS_FOR_SYNC_DNLD ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_FILE_SIZE_FOR_UPLD := rec.MAX_FILE_SIZE_FOR_UPLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'ITEM_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_FILE_SIZE_FOR_UPLD ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD := rec.MAX_FILE_SIZE_FOR_SYNC_UPLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'ITEM_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_FILE_SIZE_FOR_SYNC_UPLD ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_CC_FOR_SYNC_DNLD := rec.MAX_CC_FOR_SYNC_DNLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'ITEM_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_CC_FOR_SYNC_DNLD ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_CC_FOR_DNLD := rec.MAX_CC_FOR_DNLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'ITEM_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_CC_FOR_DNLD ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(ITM_INT_CFG$Action)      AS Action,
          r.get_cell(ITM_INT_CFG$MAX_ITEMS_FOR_DNLD)              AS MAX_ITEMS_FOR_DNLD,
          r.get_cell(ITM_INT_CFG$MX_ITM_FR_SNC_DNLD)              AS MAX_ITEMS_FOR_SYNC_DNLD,
          r.get_cell(ITM_INT_CFG$MX_FL_SZ_FR_UPLD)              AS MAX_FILE_SIZE_FOR_UPLD,
          r.get_cell(ITM_INT_CFG$MX_FL_SZ_FR_SNC_UP)              AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
          r.get_cell(ITM_INT_CFG$MX_CC_FR_SYNC_DNLD)              AS MAX_CC_FOR_SYNC_DNLD,
          r.get_cell(ITM_INT_CFG$MAX_CC_FOR_DNLD)              AS MAX_CC_FOR_DNLD,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(ITEM_INDUCT_CONFIG_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
ITEM_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_ITEMS_FOR_DNLD := rec.MAX_ITEMS_FOR_DNLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
ITEM_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_ITEMS_FOR_DNLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_ITEMS_FOR_SYNC_DNLD := rec.MAX_ITEMS_FOR_SYNC_DNLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
ITEM_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_ITEMS_FOR_SYNC_DNLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_FILE_SIZE_FOR_UPLD := rec.MAX_FILE_SIZE_FOR_UPLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
ITEM_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_FILE_SIZE_FOR_UPLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD := rec.MAX_FILE_SIZE_FOR_SYNC_UPLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
ITEM_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_FILE_SIZE_FOR_SYNC_UPLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_CC_FOR_SYNC_DNLD := rec.MAX_CC_FOR_SYNC_DNLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
ITEM_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_CC_FOR_SYNC_DNLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_CC_FOR_DNLD := rec.MAX_CC_FOR_DNLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
ITEM_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_CC_FOR_DNLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_ITEM_INDUCT_CGF.action_new then
         L_temp_rec.MAX_ITEMS_FOR_DNLD := NVL( L_temp_rec.MAX_ITEMS_FOR_DNLD,L_default_rec.MAX_ITEMS_FOR_DNLD);
         L_temp_rec.MAX_ITEMS_FOR_SYNC_DNLD := NVL( L_temp_rec.MAX_ITEMS_FOR_SYNC_DNLD,L_default_rec.MAX_ITEMS_FOR_SYNC_DNLD);
         L_temp_rec.MAX_FILE_SIZE_FOR_UPLD := NVL( L_temp_rec.MAX_FILE_SIZE_FOR_UPLD,L_default_rec.MAX_FILE_SIZE_FOR_UPLD);
         L_temp_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD := NVL( L_temp_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD,L_default_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD);
         L_temp_rec.MAX_CC_FOR_SYNC_DNLD := NVL( L_temp_rec.MAX_CC_FOR_SYNC_DNLD,L_default_rec.MAX_CC_FOR_SYNC_DNLD);
         L_temp_rec.MAX_CC_FOR_DNLD := NVL( L_temp_rec.MAX_CC_FOR_DNLD,L_default_rec.MAX_CC_FOR_DNLD);
      end if;
      if not (
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
						 ITEM_INDUCT_CONFIG_sheet,
						 rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
 
      end if;
      if NOT L_error then
         svc_ITEM_INDUCT_CONFIG_col.extend();
         svc_ITEM_INDUCT_CONFIG_col(svc_ITEM_INDUCT_CONFIG_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_ITEM_INDUCT_CONFIG_col.COUNT SAVE EXCEPTIONS
      merge into SVC_ITEM_INDUCT_CONFIG st
      using(select
                  (case
                   when l_mi_rec.MAX_ITEMS_FOR_DNLD_mi    = 'N'
                    and svc_ITEM_INDUCT_CONFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_ITEMS_FOR_DNLD IS NULL
                   then mt.MAX_ITEMS_FOR_DNLD
                   else s1.MAX_ITEMS_FOR_DNLD
                   end) AS MAX_ITEMS_FOR_DNLD,
                  (case
                   when l_mi_rec.MAX_ITEMS_FOR_SYNC_DNLD_mi    = 'N'
                    and svc_ITEM_INDUCT_CONFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_ITEMS_FOR_SYNC_DNLD IS NULL
                   then mt.MAX_ITEMS_FOR_SYNC_DNLD
                   else s1.MAX_ITEMS_FOR_SYNC_DNLD
                   end) AS MAX_ITEMS_FOR_SYNC_DNLD,
                  (case
                   when l_mi_rec.MAX_FILE_SIZE_FOR_UPLD_mi    = 'N'
                    and svc_ITEM_INDUCT_CONFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_FILE_SIZE_FOR_UPLD IS NULL
                   then mt.MAX_FILE_SIZE_FOR_UPLD
                   else s1.MAX_FILE_SIZE_FOR_UPLD
                   end) AS MAX_FILE_SIZE_FOR_UPLD,
                  (case
                   when l_mi_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD_mi    = 'N'
                    and svc_ITEM_INDUCT_CONFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_FILE_SIZE_FOR_SYNC_UPLD IS NULL
                   then mt.MAX_FILE_SIZE_FOR_SYNC_UPLD
                   else s1.MAX_FILE_SIZE_FOR_SYNC_UPLD
                   end) AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
                  (case
                   when l_mi_rec.MAX_CC_FOR_SYNC_DNLD_mi    = 'N'
                    and svc_ITEM_INDUCT_CONFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_CC_FOR_SYNC_DNLD IS NULL
                   then mt.MAX_CC_FOR_SYNC_DNLD
                   else s1.MAX_CC_FOR_SYNC_DNLD
                   end) AS MAX_CC_FOR_SYNC_DNLD,
                  (case
                   when l_mi_rec.MAX_CC_FOR_DNLD_mi    = 'N'
                    and svc_ITEM_INDUCT_CONFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_CC_FOR_DNLD IS NULL
                   then mt.MAX_CC_FOR_DNLD
                   else s1.MAX_CC_FOR_DNLD
                   end) AS MAX_CC_FOR_DNLD,
                  null as dummy
              from (select
                          svc_ITEM_INDUCT_CONFIG_col(i).MAX_ITEMS_FOR_DNLD AS MAX_ITEMS_FOR_DNLD,
                          svc_ITEM_INDUCT_CONFIG_col(i).MAX_ITEMS_FOR_SYNC_DNLD AS MAX_ITEMS_FOR_SYNC_DNLD,
                          svc_ITEM_INDUCT_CONFIG_col(i).MAX_FILE_SIZE_FOR_UPLD AS MAX_FILE_SIZE_FOR_UPLD,
                          svc_ITEM_INDUCT_CONFIG_col(i).MAX_FILE_SIZE_FOR_SYNC_UPLD AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
                          svc_ITEM_INDUCT_CONFIG_col(i).MAX_CC_FOR_SYNC_DNLD AS MAX_CC_FOR_SYNC_DNLD,
                          svc_ITEM_INDUCT_CONFIG_col(i).MAX_CC_FOR_DNLD AS MAX_CC_FOR_DNLD,
                          null as dummy
                      from dual ) s1,
            ITEM_INDUCT_CONFIG mt
             where
                  1 = 1 )sq
                on (
                    svc_ITEM_INDUCT_CONFIG_col(i).ACTION IN (CORESVC_ITEM_INDUCT_CGF.action_mod,CORESVC_ITEM_INDUCT_CGF.action_del))
      when matched then
      update
         set process_id      = svc_ITEM_INDUCT_CONFIG_col(i).process_id ,
             chunk_id        = svc_ITEM_INDUCT_CONFIG_col(i).chunk_id ,
             row_seq         = svc_ITEM_INDUCT_CONFIG_col(i).row_seq ,
             action          = svc_ITEM_INDUCT_CONFIG_col(i).action ,
             process$status  = svc_ITEM_INDUCT_CONFIG_col(i).process$status ,
             max_items_for_dnld              = sq.max_items_for_dnld ,
             max_file_size_for_sync_upld              = sq.max_file_size_for_sync_upld ,
             max_file_size_for_upld              = sq.max_file_size_for_upld ,
             max_cc_for_sync_dnld              = sq.max_cc_for_sync_dnld ,
             max_cc_for_dnld              = sq.max_cc_for_dnld ,
             max_items_for_sync_dnld              = sq.max_items_for_sync_dnld ,
             create_id       = svc_ITEM_INDUCT_CONFIG_col(i).create_id ,
             create_datetime = svc_ITEM_INDUCT_CONFIG_col(i).create_datetime ,
             last_upd_id     = svc_ITEM_INDUCT_CONFIG_col(i).last_upd_id ,
             last_upd_datetime = svc_ITEM_INDUCT_CONFIG_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             max_items_for_dnld ,
             max_items_for_sync_dnld ,
             max_file_size_for_upld ,
             max_file_size_for_sync_upld ,
             max_cc_for_sync_dnld ,
             max_cc_for_dnld ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_ITEM_INDUCT_CONFIG_col(i).process_id ,
             svc_ITEM_INDUCT_CONFIG_col(i).chunk_id ,
             svc_ITEM_INDUCT_CONFIG_col(i).row_seq ,
             svc_ITEM_INDUCT_CONFIG_col(i).action ,
             svc_ITEM_INDUCT_CONFIG_col(i).process$status ,
             sq.max_items_for_dnld ,
             sq.max_items_for_sync_dnld ,
             sq.max_file_size_for_upld ,
             sq.max_file_size_for_sync_upld ,
             sq.max_cc_for_sync_dnld ,
             sq.max_cc_for_dnld ,
             svc_ITEM_INDUCT_CONFIG_col(i).create_id ,
             svc_ITEM_INDUCT_CONFIG_col(i).create_datetime ,
             svc_ITEM_INDUCT_CONFIG_col(i).last_upd_id ,
             svc_ITEM_INDUCT_CONFIG_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            ITEM_INDUCT_CONFIG_sheet,
                            svc_ITEM_INDUCT_CONFIG_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;

END PROCESS_S9T_ITEM_INDUCT_CONFIG;
-----------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CORESVC_ITEM_CNFIG( I_file_id    IN   s9t_folder.file_id%TYPE,
                                          I_process_id IN   SVC_CORESVC_ITEM_CONFIG.process_id%TYPE) IS
   TYPE svc_CORESVC_ITM_CNFIG_col_typ IS TABLE OF SVC_CORESVC_ITEM_CONFIG%ROWTYPE;
   L_temp_rec SVC_CORESVC_ITEM_CONFIG%ROWTYPE;
   svc_CORESVC_ITEM_CNFIG_col svc_CORESVC_ITM_CNFIG_col_typ :=NEW svc_CORESVC_ITM_CNFIG_col_typ();
   L_process_id SVC_CORESVC_ITEM_CONFIG.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_CORESVC_ITEM_CONFIG%ROWTYPE;
      L_pk_columns    VARCHAR2(255)  := 'Coresvc Item Configuration';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             CASCADE_IUD_ITEM_SUPPLIER_mi,
             MAX_THREADS_mi,
             WAIT_BTWN_THREADS_mi,
             CASCADE_IIM_DETAILS_mi,
             MAX_CHUNK_SIZE_mi,
             PROC_ERR_RETENTION_DAYS_mi,
             CASCADE_UDA_DETAILS_mi,
             CASCADE_IUD_ITEM_SUPP_CNTRY_mi,
             CASCADE_IUD_ISC_DIMENSIONS_mi,
             CASCADE_IUD_ISMC_mi,
             ISC_UPDATE_ALL_LOCS_mi,
             ISC_UPDATE_ALL_CHILD_LOCS_mi,
             CASCADE_VAT_ITEM_mi,
             MAX_ITEM_RESV_QTY_mi,
             MAX_ITEM_EXPIRY_DAYS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'CORESVC_ITEM_CONFIG'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'CASCADE_IUD_ITEM_SUPPLIER' AS CASCADE_IUD_ITEM_SUPPLIER,
                                         'MAX_THREADS' AS MAX_THREADS,
                                         'WAIT_BTWN_THREADS' AS WAIT_BTWN_THREADS,
                                         'CASCADE_IIM_DETAILS' AS CASCADE_IIM_DETAILS,
                                         'MAX_CHUNK_SIZE' AS MAX_CHUNK_SIZE,
                                         'PROC_ERR_RETENTION_DAYS' AS PROC_ERR_RETENTION_DAYS,
                                         'CASCADE_UDA_DETAILS' AS CASCADE_UDA_DETAILS,
                                         'CASCADE_IUD_ITEM_SUPP_COUNTRY' AS CASCADE_IUD_ITEM_SUPP_CNTRY,
                                         'CASCADE_IUD_ISC_DIMENSIONS' AS CASCADE_IUD_ISC_DIMENSIONS,
                                         'CASCADE_IUD_ISMC' AS CASCADE_IUD_ISMC,
                                         'ISC_UPDATE_ALL_LOCS' AS ISC_UPDATE_ALL_LOCS,
                                         'ISC_UPDATE_ALL_CHILD_LOCS' AS ISC_UPDATE_ALL_CHILD_LOCS,
                                         'CASCADE_VAT_ITEM' AS CASCADE_VAT_ITEM,
                                         'MAX_ITEM_RESV_QTY' AS MAX_ITEM_RESV_QTY,
                                         'MAX_ITEM_EXPIRY_DAYS' AS MAX_ITEM_EXPIRY_DAYS,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       CASCADE_IUD_ITEM_SUPPLIER_dv,
                       MAX_THREADS_dv,
                       WAIT_BTWN_THREADS_dv,
                       CASCADE_IIM_DETAILS_dv,
                       MAX_CHUNK_SIZE_dv,
                       PROC_ERR_RETENTION_DAYS_dv,
                       CASCADE_UDA_DETAILS_dv,
                       CASCADE_IUD_ITEM_SUPP_CNTRY_dv,
                       CASCADE_IUD_ISC_DIMENSIONS_dv,
                       CASCADE_IUD_ISMC_dv,
                       ISC_UPDATE_ALL_LOCS_dv,
                       ISC_UPDATE_ALL_CHILD_LOCS_dv,
                       CASCADE_VAT_ITEM_dv,
                       MAX_ITEM_RESV_QTY_dv,
                       MAX_ITEM_EXPIRY_DAYS_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'CORESVC_ITEM_CONFIG'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'CASCADE_IUD_ITEM_SUPPLIER' AS CASCADE_IUD_ITEM_SUPPLIER,
                                                      'MAX_THREADS' AS MAX_THREADS,
                                                      'WAIT_BTWN_THREADS' AS WAIT_BTWN_THREADS,
                                                      'CASCADE_IIM_DETAILS' AS CASCADE_IIM_DETAILS,
                                                      'MAX_CHUNK_SIZE' AS MAX_CHUNK_SIZE,
                                                      'PROC_ERR_RETENTION_DAYS' AS PROC_ERR_RETENTION_DAYS,
                                                      'CASCADE_UDA_DETAILS' AS CASCADE_UDA_DETAILS,
                                                      'CASCADE_IUD_ITEM_SUPP_COUNTRY' AS CASCADE_IUD_ITEM_SUPP_CNTRY,
                                                      'CASCADE_IUD_ISC_DIMENSIONS' AS CASCADE_IUD_ISC_DIMENSIONS,
                                                      'CASCADE_IUD_ISMC' AS CASCADE_IUD_ISMC,
                                                      'ISC_UPDATE_ALL_LOCS' AS ISC_UPDATE_ALL_LOCS,
                                                      'ISC_UPDATE_ALL_CHILD_LOCS' AS ISC_UPDATE_ALL_CHILD_LOCS,
                                                      'CASCADE_VAT_ITEM' AS CASCADE_VAT_ITEM,
                                                      'MAX_ITEM_RESV_QTY' AS MAX_ITEM_RESV_QTY,
                                                      'MAX_ITEM_EXPIRY_DAYS' AS MAX_ITEM_EXPIRY_DAYS,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.CASCADE_IUD_ITEM_SUPPLIER := rec.CASCADE_IUD_ITEM_SUPPLIER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'CASCADE_IUD_ITEM_SUPPLIER ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_THREADS := rec.MAX_THREADS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'MAX_THREADS ' ,
                          NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.WAIT_BTWN_THREADS := rec.WAIT_BTWN_THREADS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'WAIT_BTWN_THREADS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CASCADE_IIM_DETAILS := rec.CASCADE_IIM_DETAILS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'CASCADE_IIM_DETAILS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_CHUNK_SIZE := rec.MAX_CHUNK_SIZE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'MAX_CHUNK_SIZE ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.PROC_ERR_RETENTION_DAYS := rec.PROC_ERR_RETENTION_DAYS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'PROC_ERR_RETENTION_DAYS ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CASCADE_UDA_DETAILS := rec.CASCADE_UDA_DETAILS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'CASCADE_UDA_DETAILS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CASCADE_IUD_ITEM_SUPP_COUNTRY := rec.CASCADE_IUD_ITEM_SUPP_CNTRY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'CASCADE_IUD_ITEM_SUPP_COUNTRY ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CASCADE_IUD_ISC_DIMENSIONS := rec.CASCADE_IUD_ISC_DIMENSIONS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'CASCADE_IUD_ISC_DIMENSIONS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CASCADE_IUD_ISMC := rec.CASCADE_IUD_ISMC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'CASCADE_IUD_ISMC ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ISC_UPDATE_ALL_LOCS := rec.ISC_UPDATE_ALL_LOCS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'ISC_UPDATE_ALL_LOCS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ISC_UPDATE_ALL_CHILD_LOCS := rec.ISC_UPDATE_ALL_CHILD_LOCS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'ISC_UPDATE_ALL_CHILD_LOCS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CASCADE_VAT_ITEM := rec.CASCADE_VAT_ITEM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'CASCADE_VAT_ITEM ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_ITEM_RESV_QTY := rec.MAX_ITEM_RESV_QTY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'MAX_ITEM_RESV_QTY ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MAX_ITEM_EXPIRY_DAYS := rec.MAX_ITEM_EXPIRY_DAYS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CORESVC_ITEM_CONFIG ' ,
                            NULL,
                           'MAX_ITEM_EXPIRY_DAYS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(CORESVC_ITEM_CONFIG$Action)      AS Action,
          r.get_cell(CRSV_ITM_CFG$CSCD_IUD_ITM_SUPP)              AS CASCADE_IUD_ITEM_SUPPLIER,
          r.get_cell(CRSV_ITM_CFG$MAX_THREADS)              AS MAX_THREADS,
          r.get_cell(CRSV_ITM_CFG$WAIT_BTWN_THREADS)              AS WAIT_BTWN_THREADS,
          r.get_cell(CRSV_ITM_CFG$CSCD_IIM_DETAILS)              AS CASCADE_IIM_DETAILS,
          r.get_cell(CRSV_ITM_CFG$MAX_CHUNK_SIZE)              AS MAX_CHUNK_SIZE,
          r.get_cell(CRSV_ITM_CFG$PRC_ERR_RTNTN_DAY)              AS PROC_ERR_RETENTION_DAYS,
          r.get_cell(CRSV_ITM_CFG$CSCD_UDA_DETAILS)              AS CASCADE_UDA_DETAILS,
          r.get_cell(CRSV_ITM_CFG$CSCD_IUITMSPP_CTR)              AS CASCADE_IUD_ITEM_SUPP_COUNTRY,
          r.get_cell(CRSV_ITM_CFG$CSCDIUDISC_DIM)              AS CASCADE_IUD_ISC_DIMENSIONS,
          r.get_cell(CRSV_ITM_CFG$CSCD_IUD_ISMC)              AS CASCADE_IUD_ISMC,
          r.get_cell(CRSV_ITM_CFG$ISC_UPD_ALL_LC)              AS ISC_UPDATE_ALL_LOCS,
          r.get_cell(CRSV_ITM_CFG$ISC_UPD_AL_CHD_LC)              AS ISC_UPDATE_ALL_CHILD_LOCS,
          r.get_cell(CRSV_ITM_CFG$CASCADE_VAT_ITEM)              AS CASCADE_VAT_ITEM,
          r.get_cell(CRSV_ITM_CFG$MAX_ITEM_RESV_QTY)              AS MAX_ITEM_RESV_QTY,
          r.get_cell(CRSV_ITM_CFG$MAX_ITM_EXPRY_DAY)              AS MAX_ITEM_EXPIRY_DAYS,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(CORESVC_ITEM_CONFIG_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CASCADE_IUD_ITEM_SUPPLIER := rec.CASCADE_IUD_ITEM_SUPPLIER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'CASCADE_IUD_ITEM_SUPPLIER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_THREADS := rec.MAX_THREADS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_THREADS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.WAIT_BTWN_THREADS := rec.WAIT_BTWN_THREADS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'WAIT_BTWN_THREADS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CASCADE_IIM_DETAILS := rec.CASCADE_IIM_DETAILS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'CASCADE_IIM_DETAILS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_CHUNK_SIZE := rec.MAX_CHUNK_SIZE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_CHUNK_SIZE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.PROC_ERR_RETENTION_DAYS := rec.PROC_ERR_RETENTION_DAYS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'PROC_ERR_RETENTION_DAYS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CASCADE_UDA_DETAILS := rec.CASCADE_UDA_DETAILS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'CASCADE_UDA_DETAILS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CASCADE_IUD_ITEM_SUPP_COUNTRY := rec.CASCADE_IUD_ITEM_SUPP_COUNTRY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'CASCADE_IUD_ITEM_SUPP_COUNTRY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CASCADE_IUD_ISC_DIMENSIONS := rec.CASCADE_IUD_ISC_DIMENSIONS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'CASCADE_IUD_ISC_DIMENSIONS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CASCADE_IUD_ISMC := rec.CASCADE_IUD_ISMC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'CASCADE_IUD_ISMC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ISC_UPDATE_ALL_LOCS := rec.ISC_UPDATE_ALL_LOCS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'ISC_UPDATE_ALL_LOCS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ISC_UPDATE_ALL_CHILD_LOCS := rec.ISC_UPDATE_ALL_CHILD_LOCS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'ISC_UPDATE_ALL_CHILD_LOCS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CASCADE_VAT_ITEM := rec.CASCADE_VAT_ITEM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'CASCADE_VAT_ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_ITEM_RESV_QTY := rec.MAX_ITEM_RESV_QTY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_ITEM_RESV_QTY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_ITEM_EXPIRY_DAYS := rec.MAX_ITEM_EXPIRY_DAYS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CORESVC_ITEM_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_ITEM_EXPIRY_DAYS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_ITEM_INDUCT_CGF.action_new then
         L_temp_rec.CASCADE_IUD_ITEM_SUPPLIER := NVL( L_temp_rec.CASCADE_IUD_ITEM_SUPPLIER,L_default_rec.CASCADE_IUD_ITEM_SUPPLIER);
         L_temp_rec.MAX_THREADS := NVL( L_temp_rec.MAX_THREADS,L_default_rec.MAX_THREADS);
         L_temp_rec.WAIT_BTWN_THREADS := NVL( L_temp_rec.WAIT_BTWN_THREADS,L_default_rec.WAIT_BTWN_THREADS);
         L_temp_rec.CASCADE_IIM_DETAILS := NVL( L_temp_rec.CASCADE_IIM_DETAILS,L_default_rec.CASCADE_IIM_DETAILS);
         L_temp_rec.MAX_CHUNK_SIZE := NVL( L_temp_rec.MAX_CHUNK_SIZE,L_default_rec.MAX_CHUNK_SIZE);
         L_temp_rec.PROC_ERR_RETENTION_DAYS := NVL( L_temp_rec.PROC_ERR_RETENTION_DAYS,L_default_rec.PROC_ERR_RETENTION_DAYS);
         L_temp_rec.CASCADE_UDA_DETAILS := NVL( L_temp_rec.CASCADE_UDA_DETAILS,L_default_rec.CASCADE_UDA_DETAILS);
         L_temp_rec.CASCADE_IUD_ITEM_SUPP_COUNTRY := NVL( L_temp_rec.CASCADE_IUD_ITEM_SUPP_COUNTRY,L_default_rec.CASCADE_IUD_ITEM_SUPP_COUNTRY);
         L_temp_rec.CASCADE_IUD_ISC_DIMENSIONS := NVL( L_temp_rec.CASCADE_IUD_ISC_DIMENSIONS,L_default_rec.CASCADE_IUD_ISC_DIMENSIONS);
         L_temp_rec.CASCADE_IUD_ISMC := NVL( L_temp_rec.CASCADE_IUD_ISMC,L_default_rec.CASCADE_IUD_ISMC);
         L_temp_rec.ISC_UPDATE_ALL_LOCS := NVL( L_temp_rec.ISC_UPDATE_ALL_LOCS,L_default_rec.ISC_UPDATE_ALL_LOCS);
         L_temp_rec.ISC_UPDATE_ALL_CHILD_LOCS := NVL( L_temp_rec.ISC_UPDATE_ALL_CHILD_LOCS,L_default_rec.ISC_UPDATE_ALL_CHILD_LOCS);
         L_temp_rec.CASCADE_VAT_ITEM := NVL( L_temp_rec.CASCADE_VAT_ITEM,L_default_rec.CASCADE_VAT_ITEM);
         L_temp_rec.MAX_ITEM_RESV_QTY := NVL( L_temp_rec.MAX_ITEM_RESV_QTY,L_default_rec.MAX_ITEM_RESV_QTY);
         L_temp_rec.MAX_ITEM_EXPIRY_DAYS := NVL( L_temp_rec.MAX_ITEM_EXPIRY_DAYS,L_default_rec.MAX_ITEM_EXPIRY_DAYS);
      end if;
      if NOT L_error then
         svc_CORESVC_ITEM_CNFIG_col.extend();
         svc_CORESVC_ITEM_CNFIG_col(svc_CORESVC_ITEM_CNFIG_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_CORESVC_ITEM_CNFIG_col.COUNT SAVE EXCEPTIONS
      merge into SVC_CORESVC_ITEM_CONFIG st
      using(select
                  (case
                   when l_mi_rec.CASCADE_IUD_ITEM_SUPPLIER_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.CASCADE_IUD_ITEM_SUPPLIER IS NULL
                   then mt.CASCADE_IUD_ITEM_SUPPLIER
                   else s1.CASCADE_IUD_ITEM_SUPPLIER
                   end) AS CASCADE_IUD_ITEM_SUPPLIER,
                  (case
                   when l_mi_rec.MAX_THREADS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_THREADS IS NULL
                   then mt.MAX_THREADS
                   else s1.MAX_THREADS
                   end) AS MAX_THREADS,
                  (case
                   when l_mi_rec.WAIT_BTWN_THREADS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.WAIT_BTWN_THREADS IS NULL
                   then mt.WAIT_BTWN_THREADS
                   else s1.WAIT_BTWN_THREADS
                   end) AS WAIT_BTWN_THREADS,
                  (case
                   when l_mi_rec.CASCADE_IIM_DETAILS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.CASCADE_IIM_DETAILS IS NULL
                   then mt.CASCADE_IIM_DETAILS
                   else s1.CASCADE_IIM_DETAILS
                   end) AS CASCADE_IIM_DETAILS,
                  (case
                   when l_mi_rec.MAX_CHUNK_SIZE_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_CHUNK_SIZE IS NULL
                   then mt.MAX_CHUNK_SIZE
                   else s1.MAX_CHUNK_SIZE
                   end) AS MAX_CHUNK_SIZE,
                  (case
                   when l_mi_rec.PROC_ERR_RETENTION_DAYS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.PROC_ERR_RETENTION_DAYS IS NULL
                   then mt.PROC_ERR_RETENTION_DAYS
                   else s1.PROC_ERR_RETENTION_DAYS
                   end) AS PROC_ERR_RETENTION_DAYS,
                  (case
                   when l_mi_rec.CASCADE_UDA_DETAILS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.CASCADE_UDA_DETAILS IS NULL
                   then mt.CASCADE_UDA_DETAILS
                   else s1.CASCADE_UDA_DETAILS
                   end) AS CASCADE_UDA_DETAILS,
                  (case
                   when l_mi_rec.CASCADE_IUD_ITEM_SUPP_CNTRY_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.CASCADE_IUD_ITEM_SUPP_COUNTRY IS NULL
                   then mt.CASCADE_IUD_ITEM_SUPP_COUNTRY
                   else s1.CASCADE_IUD_ITEM_SUPP_COUNTRY
                   end) AS CASCADE_IUD_ITEM_SUPP_COUNTRY,
                  (case
                   when l_mi_rec.CASCADE_IUD_ISC_DIMENSIONS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.CASCADE_IUD_ISC_DIMENSIONS IS NULL
                   then mt.CASCADE_IUD_ISC_DIMENSIONS
                   else s1.CASCADE_IUD_ISC_DIMENSIONS
                   end) AS CASCADE_IUD_ISC_DIMENSIONS,
                  (case
                   when l_mi_rec.CASCADE_IUD_ISMC_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.CASCADE_IUD_ISMC IS NULL
                   then mt.CASCADE_IUD_ISMC
                   else s1.CASCADE_IUD_ISMC
                   end) AS CASCADE_IUD_ISMC,
                  (case
                   when l_mi_rec.ISC_UPDATE_ALL_LOCS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.ISC_UPDATE_ALL_LOCS IS NULL
                   then mt.ISC_UPDATE_ALL_LOCS
                   else s1.ISC_UPDATE_ALL_LOCS
                   end) AS ISC_UPDATE_ALL_LOCS,
                  (case
                   when l_mi_rec.ISC_UPDATE_ALL_CHILD_LOCS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.ISC_UPDATE_ALL_CHILD_LOCS IS NULL
                   then mt.ISC_UPDATE_ALL_CHILD_LOCS
                   else s1.ISC_UPDATE_ALL_CHILD_LOCS
                   end) AS ISC_UPDATE_ALL_CHILD_LOCS,
                  (case
                   when l_mi_rec.CASCADE_VAT_ITEM_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.CASCADE_VAT_ITEM IS NULL
                   then mt.CASCADE_VAT_ITEM
                   else s1.CASCADE_VAT_ITEM
                   end) AS CASCADE_VAT_ITEM,
                  (case
                   when l_mi_rec.MAX_ITEM_RESV_QTY_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_ITEM_RESV_QTY IS NULL
                   then mt.MAX_ITEM_RESV_QTY
                   else s1.MAX_ITEM_RESV_QTY
                   end) AS MAX_ITEM_RESV_QTY,
                  (case
                   when l_mi_rec.MAX_ITEM_EXPIRY_DAYS_mi    = 'N'
                    and svc_CORESVC_ITEM_CNFIG_col(i).action = CORESVC_ITEM_INDUCT_CGF.action_mod
                    and s1.MAX_ITEM_EXPIRY_DAYS IS NULL
                   then mt.MAX_ITEM_EXPIRY_DAYS
                   else s1.MAX_ITEM_EXPIRY_DAYS
                   end) AS MAX_ITEM_EXPIRY_DAYS,
                  null as dummy
              from (select
                          svc_CORESVC_ITEM_CNFIG_col(i).CASCADE_IUD_ITEM_SUPPLIER AS CASCADE_IUD_ITEM_SUPPLIER,
                          svc_CORESVC_ITEM_CNFIG_col(i).MAX_THREADS AS MAX_THREADS,
                          svc_CORESVC_ITEM_CNFIG_col(i).WAIT_BTWN_THREADS AS WAIT_BTWN_THREADS,
                          svc_CORESVC_ITEM_CNFIG_col(i).CASCADE_IIM_DETAILS AS CASCADE_IIM_DETAILS,
                          svc_CORESVC_ITEM_CNFIG_col(i).MAX_CHUNK_SIZE AS MAX_CHUNK_SIZE,
                          svc_CORESVC_ITEM_CNFIG_col(i).PROC_ERR_RETENTION_DAYS AS PROC_ERR_RETENTION_DAYS,
                          svc_CORESVC_ITEM_CNFIG_col(i).CASCADE_UDA_DETAILS AS CASCADE_UDA_DETAILS,
                          svc_CORESVC_ITEM_CNFIG_col(i).CASCADE_IUD_ITEM_SUPP_COUNTRY AS CASCADE_IUD_ITEM_SUPP_COUNTRY,
                          svc_CORESVC_ITEM_CNFIG_col(i).CASCADE_IUD_ISC_DIMENSIONS AS CASCADE_IUD_ISC_DIMENSIONS,
                          svc_CORESVC_ITEM_CNFIG_col(i).CASCADE_IUD_ISMC AS CASCADE_IUD_ISMC,
                          svc_CORESVC_ITEM_CNFIG_col(i).ISC_UPDATE_ALL_LOCS AS ISC_UPDATE_ALL_LOCS,
                          svc_CORESVC_ITEM_CNFIG_col(i).ISC_UPDATE_ALL_CHILD_LOCS AS ISC_UPDATE_ALL_CHILD_LOCS,
                          svc_CORESVC_ITEM_CNFIG_col(i).CASCADE_VAT_ITEM AS CASCADE_VAT_ITEM,
                          svc_CORESVC_ITEM_CNFIG_col(i).MAX_ITEM_RESV_QTY AS MAX_ITEM_RESV_QTY,
                          svc_CORESVC_ITEM_CNFIG_col(i).MAX_ITEM_EXPIRY_DAYS AS MAX_ITEM_EXPIRY_DAYS,
                          null as dummy
                      from dual ) s1,
            CORESVC_ITEM_CONFIG mt
             where
                  1 = 1 )sq
                on (
                    svc_CORESVC_ITEM_CNFIG_col(i).ACTION IN (CORESVC_ITEM_INDUCT_CGF.action_mod,CORESVC_ITEM_INDUCT_CGF.action_del))
      when matched then
      update
         set process_id      = svc_CORESVC_ITEM_CNFIG_col(i).process_id ,
             chunk_id        = svc_CORESVC_ITEM_CNFIG_col(i).chunk_id ,
             row_seq         = svc_CORESVC_ITEM_CNFIG_col(i).row_seq ,
             action          = svc_CORESVC_ITEM_CNFIG_col(i).action ,
             process$status  = svc_CORESVC_ITEM_CNFIG_col(i).process$status ,
             max_chunk_size              = sq.max_chunk_size ,
             max_item_expiry_days              = sq.max_item_expiry_days ,
             wait_btwn_threads              = sq.wait_btwn_threads ,
             cascade_iud_ismc              = sq.cascade_iud_ismc ,
             max_item_resv_qty              = sq.max_item_resv_qty ,
             max_threads              = sq.max_threads ,
             isc_update_all_child_locs              = sq.isc_update_all_child_locs ,
             proc_err_retention_days              = sq.proc_err_retention_days ,
             cascade_iud_isc_dimensions              = sq.cascade_iud_isc_dimensions ,
             cascade_iud_item_supplier              = sq.cascade_iud_item_supplier ,
             cascade_iud_item_supp_country              = sq.cascade_iud_item_supp_country ,
             cascade_iim_details              = sq.cascade_iim_details ,
             cascade_uda_details              = sq.cascade_uda_details ,
             cascade_vat_item              = sq.cascade_vat_item ,
             isc_update_all_locs              = sq.isc_update_all_locs ,
             create_id       = svc_CORESVC_ITEM_CNFIG_col(i).create_id ,
             create_datetime = svc_CORESVC_ITEM_CNFIG_col(i).create_datetime ,
             last_upd_id     = svc_CORESVC_ITEM_CNFIG_col(i).last_upd_id ,
             last_upd_datetime = svc_CORESVC_ITEM_CNFIG_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             cascade_iud_item_supplier ,
             max_threads ,
             wait_btwn_threads ,
             cascade_iim_details ,
             max_chunk_size ,
             proc_err_retention_days ,
             cascade_uda_details ,
             cascade_iud_item_supp_country ,
             cascade_iud_isc_dimensions ,
             cascade_iud_ismc ,
             isc_update_all_locs ,
             isc_update_all_child_locs ,
             cascade_vat_item ,
             max_item_resv_qty ,
             max_item_expiry_days ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_CORESVC_ITEM_CNFIG_col(i).process_id ,
             svc_CORESVC_ITEM_CNFIG_col(i).chunk_id ,
             svc_CORESVC_ITEM_CNFIG_col(i).row_seq ,
             svc_CORESVC_ITEM_CNFIG_col(i).action ,
             svc_CORESVC_ITEM_CNFIG_col(i).process$status ,
             sq.cascade_iud_item_supplier ,
             sq.max_threads ,
             sq.wait_btwn_threads ,
             sq.cascade_iim_details ,
             sq.max_chunk_size ,
             sq.proc_err_retention_days ,
             sq.cascade_uda_details ,
             sq.cascade_iud_item_supp_country ,
             sq.cascade_iud_isc_dimensions ,
             sq.cascade_iud_ismc ,
             sq.isc_update_all_locs ,
             sq.isc_update_all_child_locs ,
             sq.cascade_vat_item ,
             sq.max_item_resv_qty ,
             sq.max_item_expiry_days ,
             svc_CORESVC_ITEM_CNFIG_col(i).create_id ,
             svc_CORESVC_ITEM_CNFIG_col(i).create_datetime ,
             svc_CORESVC_ITEM_CNFIG_col(i).last_upd_id ,
             svc_CORESVC_ITEM_CNFIG_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            CORESVC_ITEM_CONFIG_sheet,
                            svc_CORESVC_ITEM_CNFIG_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;
END PROCESS_S9T_CORESVC_ITEM_CNFIG;
--------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_ITEM_INDUCT_CGF.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_ITEM_INDUCT_CONFIG(I_file_id,I_process_id);
      PROCESS_S9T_CORESVC_ITEM_CNFIG(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
-------------------------------------------------------------------------
FUNCTION EXEC_ITEM_INDUCT_CONFIG_UPD( L_item_induct_config_temp_rec   IN   ITEM_INDUCT_CONFIG%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_ITEM_INDUCT_CGF.EXEC_ITEM_INDUCT_CONFIG_UPD';
   L_table   VARCHAR2(255):= 'SVC_ITEM_INDUCT_CONFIG';
BEGIN
   update item_induct_config
      set row = L_item_induct_config_temp_rec
    where 1 = 1
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ITEM_INDUCT_CONFIG_UPD;
---------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_INDUCT_CONFIG( I_process_id   IN   SVC_ITEM_INDUCT_CONFIG.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_ITEM_INDUCT_CONFIG.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_ITEM_INDUCT_CGF.PROCESS_ITEM_INDUCT_CONFIG';
   L_error_message VARCHAR2(600);
   L_ITEM_INDUCT_CONFIG_temp_rec ITEM_INDUCT_CONFIG%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_ITEM_INDUCT_CONFIG';
BEGIN
   FOR rec IN c_svc_ITEM_INDUCT_CONFIG(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
    if NOT(  rec.MAX_ITEMS_FOR_DNLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_ITEMS_FOR_DNLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_ITEMS_FOR_SYNC_DNLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_ITEMS_FOR_SYNC_DNLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_FILE_SIZE_FOR_UPLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_FILE_SIZE_FOR_UPLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_FILE_SIZE_FOR_SYNC_UPLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_FILE_SIZE_FOR_SYNC_UPLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_CC_FOR_SYNC_DNLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_CC_FOR_SYNC_DNLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_CC_FOR_DNLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_CC_FOR_DNLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_item_induct_config_temp_rec.max_cc_for_dnld                          := rec.max_cc_for_dnld;
         L_item_induct_config_temp_rec.max_cc_for_sync_dnld                     := rec.max_cc_for_sync_dnld;
         L_item_induct_config_temp_rec.max_file_size_for_sync_upld              := rec.max_file_size_for_sync_upld;
         L_item_induct_config_temp_rec.max_file_size_for_upld                   := rec.max_file_size_for_upld;
         L_item_induct_config_temp_rec.max_items_for_sync_dnld                  := rec.max_items_for_sync_dnld;
         L_item_induct_config_temp_rec.max_items_for_dnld                       := rec.max_items_for_dnld;

         if rec.action = action_mod then
            if EXEC_ITEM_INDUCT_CONFIG_UPD( L_item_induct_config_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_item_induct_config st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_item_induct_config st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_item_induct_config st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_ITEM_INDUCT_CONFIG%ISOPEN then
	     close c_svc_ITEM_INDUCT_CONFIG;
	  end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_ITEM_INDUCT_CONFIG;
------------------------------------------------------------------------------
FUNCTION EXEC_CORESVC_ITEM_CONFIG_UPD( L_coresvc_item_config_temp_rec   IN   CORESVC_ITEM_CONFIG%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_ITEM_INDUCT_CGF.EXEC_CORESVC_ITEM_CONFIG_UPD';
   L_table   VARCHAR2(255):= 'SVC_CORESVC_ITEM_CONFIG';
BEGIN
   update coresvc_item_config
      set row = L_coresvc_item_config_temp_rec
    where 1 = 1
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CORESVC_ITEM_CONFIG_UPD;

-------------------------------------------------------------------------------------
FUNCTION PROCESS_CORESVC_ITEM_CONFIG( I_process_id   IN   SVC_CORESVC_ITEM_CONFIG.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_CORESVC_ITEM_CONFIG.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_CORESVC_ITEM_CONFIG.PROCESS_CORESVC_ITEM_CONFIG';
   L_error_message VARCHAR2(600);
   L_CORESVC_ITEM_CONFIG_temp_rec CORESVC_ITEM_CONFIG%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_CORESVC_ITEM_CONFIG';
BEGIN
   FOR rec IN c_svc_CORESVC_ITEM_CONFIG(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
    if NOT( rec.CASCADE_IUD_ITEM_SUPPLIER IN  ( 'Y','N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'CASCADE_IUD_ITEM_SUPPLIER',
                                               NULL,
                                               NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IUD_ITEM_SUPPLIER',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_IUD_ITEM_SUPPLIER  IS NOT NULL ) then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IUD_ITEM_SUPPLIER',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT( rec.CASCADE_IIM_DETAILS IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'CASCADE_IIM_DETAILS',
                                               NULL,
                                               NULL); 

		WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IIM_DETAILS',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_IIM_DETAILS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IIM_DETAILS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.WAIT_BTWN_THREADS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WAIT_BTWN_THREADS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_THREADS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_THREADS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_VAT_ITEM IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'CASCADE_VAT_ITEM',
                                               NULL,
                                               NULL); 

		WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_VAT_ITEM',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.ISC_UPDATE_ALL_CHILD_LOCS IN  ( 'Y','N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'ISC_UPDATE_ALL_CHILD_LOCS',
                                               NULL,
                                               NULL); 

		WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ISC_UPDATE_ALL_CHILD_LOCS',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.ISC_UPDATE_ALL_LOCS IN  ( 'Y','N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'ISC_UPDATE_ALL_LOCS',
                                               NULL,
                                               NULL); 

		WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ISC_UPDATE_ALL_LOCS',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_IUD_ISMC IN  ( 'Y','N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'CASCADE_IUD_ISMC',
                                               NULL,
                                               NULL); 

		WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IUD_ISMC',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_IUD_ISC_DIMENSIONS IN  ( 'Y','N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'CASCADE_IUD_ISC_DIMENSIONS',
                                               NULL,
                                               NULL); 
 
		WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IUD_ISC_DIMENSIONS',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_IUD_ITEM_SUPP_COUNTRY IN  ( 'Y','N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'CASCADE_IUD_ITEM_SUPP_COUNTRY',
                                               NULL,
                                               NULL); 

		WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IUD_ITEM_SUPP_COUNTRY',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_UDA_DETAILS IN  ( 'Y','N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'CASCADE_UDA_DETAILS',
                                               NULL,
                                               NULL); 

		WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_UDA_DETAILS',
                    L_error_message);
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_ITEM_EXPIRY_DAYS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_ITEM_EXPIRY_DAYS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_ITEM_RESV_QTY  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_ITEM_RESV_QTY',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_VAT_ITEM  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_VAT_ITEM',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.ISC_UPDATE_ALL_CHILD_LOCS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ISC_UPDATE_ALL_CHILD_LOCS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.ISC_UPDATE_ALL_LOCS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ISC_UPDATE_ALL_LOCS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_IUD_ISMC  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IUD_ISMC',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_IUD_ISC_DIMENSIONS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IUD_ISC_DIMENSIONS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_IUD_ITEM_SUPP_COUNTRY  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_IUD_ITEM_SUPP_COUNTRY',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.CASCADE_UDA_DETAILS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CASCADE_UDA_DETAILS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.PROC_ERR_RETENTION_DAYS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'PROC_ERR_RETENTION_DAYS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;

	  if NOT(  rec.MAX_CHUNK_SIZE  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_CHUNK_SIZE',
                     'ENTER_REC');
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_coresvc_item_config_temp_rec.max_item_expiry_days              := rec.max_item_expiry_days;
         L_coresvc_item_config_temp_rec.max_item_resv_qty                 := rec.max_item_resv_qty;
         L_coresvc_item_config_temp_rec.cascade_vat_item                  := rec.cascade_vat_item;
         L_coresvc_item_config_temp_rec.isc_update_all_child_locs         := rec.isc_update_all_child_locs;
         L_coresvc_item_config_temp_rec.isc_update_all_locs               := rec.isc_update_all_locs;
         L_coresvc_item_config_temp_rec.cascade_iud_ismc                  := rec.cascade_iud_ismc;
         L_coresvc_item_config_temp_rec.cascade_iud_isc_dimensions        := rec.cascade_iud_isc_dimensions;
         L_coresvc_item_config_temp_rec.cascade_iud_item_supp_country     := rec.cascade_iud_item_supp_country;
         L_coresvc_item_config_temp_rec.cascade_uda_details               := rec.cascade_uda_details;
         L_coresvc_item_config_temp_rec.proc_err_retention_days           := rec.proc_err_retention_days;
         L_coresvc_item_config_temp_rec.max_chunk_size                    := rec.max_chunk_size;
         L_coresvc_item_config_temp_rec.cascade_iim_details               := rec.cascade_iim_details;
         L_coresvc_item_config_temp_rec.wait_btwn_threads                 := rec.wait_btwn_threads;
         L_coresvc_item_config_temp_rec.max_threads                       := rec.max_threads;
         L_coresvc_item_config_temp_rec.cascade_iud_item_supplier         := rec.cascade_iud_item_supplier;

         if rec.action = action_mod then
            if EXEC_CORESVC_ITEM_CONFIG_UPD( L_coresvc_item_config_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_coresvc_item_config st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_coresvc_item_config st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_coresvc_item_config st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_CORESVC_ITEM_CONFIG%isopen then
	     close c_svc_CORESVC_ITEM_CONFIG;
	  end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CORESVC_ITEM_CONFIG;
-----------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete 
     from svc_coresvc_item_config 
    where process_id=I_process_id;

   delete 
     from svc_item_induct_config 
    where process_id=I_process_id;

END;

-----------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_ITEM_INDUCT_CGF.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';

BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_ITEM_INDUCT_CONFIG(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   commit;
   if PROCESS_CORESVC_ITEM_CONFIG(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   commit;
   O_error_count := LP_errors_tab.COUNT();

   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;


   return TRUE;
EXCEPTION
   when OTHERS then
      if c_get_err_count%isopen then
	     close c_get_err_count;
	  end if;
      if c_get_warn_count%isopen then
	     close c_get_warn_count;
	  end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_ITEM_INDUCT_CGF;
/