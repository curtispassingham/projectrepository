CREATE OR REPLACE PACKAGE BODY CORESVC_SEC_USER AS
----------------------------------------------------------------------------------------------
   cursor C_SVC_SEC_USER_ROLE(I_process_id NUMBER,
                              I_chunk_id NUMBER) is
      select pk_sec_user_role.rowid  AS pk_sec_user_role_rid,
             st.rowid AS st_rid,
             sur_rrp_fk.rowid    AS sur_rrp_fk_rid,
             sur_seu_fk.rowid    AS sur_seu_fk_rid,
             st.user_seq,
             st.role,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_sec_user_role st,
             sec_user_role pk_sec_user_role,
             rtk_role_privs sur_rrp_fk,
             sec_user sur_seu_fk
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.PROCESS$STATUS = 'N'
         and st.user_seq   = pk_sec_user_role.user_seq (+)
         and upper(st.role)       = upper(pk_sec_user_role.role (+))
         and st.user_seq   = sur_seu_fk.user_seq (+)
         and upper(st.role)       = upper(sur_rrp_fk.role (+));
         
   cursor C_SVC_SEC_USER(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
         select seu_app_user_uk.rowid  AS seu_app_user_uk_rid,
             pk_sec_user.rowid  AS pk_sec_user_rid,
             st.rowid AS st_rid,
             seu_mgr_usr_seq_fk.rowid    AS seu_mgr_usr_seq_fk_rid,             
             st.application_user_id,
             st.database_user_id,
             st.user_seq,             
             st.allocation_user_ind,
             st.reim_user_ind,
             st.resa_user_ind,
             st.rms_user_ind,
             st.manager,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_sec_user st,
             sec_user seu_app_user_uk,
             sec_user pk_sec_user,
             sec_user seu_mgr_usr_seq_fk             
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id        
         and st.user_seq         = pk_sec_user.user_seq (+)
         and upper(st.action) in ('MOD','DEL')
         and NVL(upper(st.application_user_id),'-1')  = NVL(upper(seu_app_user_uk.application_user_id (+)),'-1') 
         and st.user_seq = seu_app_user_uk.USER_SEQ(+)
         and upper(st.manager)        = upper(seu_mgr_usr_seq_fk.user_seq (+))
      UNION
       select seu_app_user_uk.rowid  AS seu_app_user_uk_rid,
             NULL AS pk_sec_user_rid,
             st.rowid AS st_rid,
             NULL    AS seu_mgr_usr_seq_fk_rid,             
             st.application_user_id,
             st.database_user_id,
             st.user_seq,             
             st.allocation_user_ind,
             st.reim_user_ind,
             st.resa_user_ind,
             st.rms_user_ind,
             st.manager,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_sec_user st,
             sec_user seu_app_user_uk             
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id 
         and upper(st.action)='NEW'         
         and NVL(upper(st.application_user_id),'-1') = NVL(upper(seu_app_user_uk.application_user_id (+)),'-1') 
         order by row_seq asc;
         
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );   
   
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E')IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   SEC_USER_ROLE_cols s9t_pkg.names_map_typ;
   SEC_USER_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                        :=s9t_pkg.get_sheet_names(I_file_id);
   SEC_USER_ROLE_cols              :=s9t_pkg.get_col_names(I_file_id,SEC_USER_ROLE_sheet);
   SEC_USER_ROLE$Action            := SEC_USER_ROLE_cols('ACTION');
   SEC_USER_ROLE$USER_SEQ          := SEC_USER_ROLE_cols('USER_SEQ');
   SEC_USER_ROLE$ROLE              := SEC_USER_ROLE_cols('ROLE');
   SEC_USER_cols                   :=s9t_pkg.get_col_names(I_file_id,SEC_USER_sheet);
   SEC_USER$Action                 := SEC_USER_cols('ACTION');
   SEC_USER$APPLICATION_USER_ID    := SEC_USER_cols('APPLICATION_USER_ID');
   SEC_USER$DATABASE_USER_ID       := SEC_USER_cols('DATABASE_USER_ID');
   SEC_USER$USER_SEQ               := SEC_USER_cols('USER_SEQ');   
   SEC_USER$ALLOCATION_USER_IND    := SEC_USER_cols('ALLOCATION_USER_IND');
   SEC_USER$REIM_USER_IND          := SEC_USER_cols('REIM_USER_IND');
   SEC_USER$RESA_USER_IND          := SEC_USER_cols('RESA_USER_IND');
   SEC_USER$RMS_USER_IND           := SEC_USER_cols('RMS_USER_IND');
   SEC_USER$MANAGER                := SEC_USER_cols('MANAGER');
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_SEC_USER_ROLE( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SEC_USER_ROLE_sheet )
   select s9t_row(s9t_cells(CORESVC_SEC_USER.action_mod ,
                           user_seq,
                           role
                           ))
     from sec_user_role ;
END POPULATE_SEC_USER_ROLE;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_SEC_USER( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SEC_USER_sheet )
   select s9t_row(s9t_cells(CORESVC_SEC_USER.action_mod ,
                           user_seq, 
                           application_user_id,
                           database_user_id,
                           manager,
                           rms_user_ind,
                           resa_user_ind,
                           reim_user_ind,
                           allocation_user_ind                     
                           ))
     from sec_user ;
END POPULATE_SEC_USER;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_FILE.user_lang    := get_user_lang;
   L_file.add_sheet(SEC_USER_ROLE_sheet);
   L_file.sheets(l_file.get_sheet_index(SEC_USER_ROLE_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'USER_SEQ'
                                                                                            ,'ROLE'
                                                                                          );  
   L_file.add_sheet(SEC_USER_sheet);
   L_file.sheets(l_file.get_sheet_index(SEC_USER_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                      ,'USER_SEQ'
                                                                                      ,'APPLICATION_USER_ID'
                                                                                      ,'DATABASE_USER_ID'
                                                                                      ,'MANAGER'
                                                                                      ,'RMS_USER_IND'
                                                                                      ,'RESA_USER_IND'
                                                                                      ,'REIM_USER_IND'
                                                                                      ,'ALLOCATION_USER_IND' 
                                                                                     );
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_SEC_USER.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                          O_file_id,
                          template_category,
                          template_key) = FALSE then
         return FALSE;
   end if;
   
   if I_template_only_ind = 'N' then
      POPULATE_SEC_USER_ROLE(O_file_id);
      POPULATE_SEC_USER(O_file_id);
      COMMIT;
   end if;
   
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                     template_category,
                     L_file) = FALSE then
         return FALSE;
   end if;                  
                     
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SEC_USER_ROLE( I_file_id    IN   s9t_folder.file_id%TYPE,
                                     I_process_id IN   SVC_SEC_USER_ROLE.process_id%TYPE) 
IS
                                     
   TYPE svc_SEC_USER_ROLE_col_typ IS TABLE OF SVC_SEC_USER_ROLE%ROWTYPE;
   L_temp_rec SVC_SEC_USER_ROLE%ROWTYPE;
   svc_SEC_USER_ROLE_col svc_SEC_USER_ROLE_col_typ :=NEW svc_SEC_USER_ROLE_col_typ();
   L_process_id SVC_SEC_USER_ROLE.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_SEC_USER_ROLE%ROWTYPE;
   cursor C_MANDATORY_IND is
      select
             USER_SEQ_mi,
             ROLE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = template_key
                 and wksht_key     = 'SEC_USER_ROLE'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('USER_SEQ' AS USER_SEQ,
                                            'ROLE' AS ROLE,
                                            null as dummy));
      l_mi_rec   c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns    VARCHAR2(255)  := 'USER_SEQ, ROLE';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
 
   FOR rec IN (select  USER_SEQ_dv,
                       ROLE_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = template_key
                          and wksht_key     = 'SEC_USER_ROLE'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'USER_SEQ' AS USER_SEQ,
                                                      'ROLE' AS ROLE,
                                                      NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.USER_SEQ := rec.USER_SEQ_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER_ROLE ' ,
                            NULL,
                           'USER_SEQ ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ROLE := rec.ROLE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER_ROLE ' ,
                            NULL,
                           'ROLE ' ,
                            NULL,
                          'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(SEC_USER_ROLE$Action)      AS Action,
          r.get_cell(SEC_USER_ROLE$USER_SEQ)              AS USER_SEQ,
          r.get_cell(SEC_USER_ROLE$ROLE)              AS ROLE,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(SEC_USER_ROLE_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_ROLE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.USER_SEQ := rec.USER_SEQ;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_ROLE_sheet,
                            rec.row_seq,
                            'USER_SEQ',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ROLE := rec.ROLE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_ROLE_sheet,
                            rec.row_seq,
                            'ROLE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SEC_USER.action_new then
         L_temp_rec.USER_SEQ := NVL( L_temp_rec.USER_SEQ,L_default_rec.USER_SEQ);
         L_temp_rec.ROLE := NVL( L_temp_rec.ROLE,L_default_rec.ROLE);
      end if;
      if not (
            L_temp_rec.USER_SEQ is NOT NULL and
            L_temp_rec.ROLE is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         SEC_USER_ROLE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                        SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_SEC_USER_ROLE_col.extend();
         svc_SEC_USER_ROLE_col(svc_SEC_USER_ROLE_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_SEC_USER_ROLE_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SEC_USER_ROLE st
      using(select
                  (case
                   when l_mi_rec.USER_SEQ_mi    = 'N'
                    and svc_SEC_USER_ROLE_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.USER_SEQ IS NULL
                   then mt.USER_SEQ
                   else s1.USER_SEQ
                   end) AS USER_SEQ,
                  (case
                   when l_mi_rec.ROLE_mi    = 'N'
                    and svc_SEC_USER_ROLE_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.ROLE IS NULL
                   then mt.ROLE
                   else s1.ROLE
                   end) AS ROLE,
                  null as dummy
              from (select
                          svc_SEC_USER_ROLE_col(i).USER_SEQ AS USER_SEQ,
                          svc_SEC_USER_ROLE_col(i).ROLE AS ROLE,
                          null as dummy
                      from dual ) s1,
            SEC_USER_ROLE mt
             where
                  mt.USER_SEQ (+)     = s1.USER_SEQ   and
                  mt.ROLE (+)     = s1.ROLE   and
                  1 = 1 )sq
                on (
                    st.USER_SEQ      = sq.USER_SEQ and
                    st.ROLE      = sq.ROLE and
                    svc_SEC_USER_ROLE_col(i).ACTION IN (CORESVC_SEC_USER.action_mod,CORESVC_SEC_USER.action_del))
      when matched then
      update
         set process_id      = svc_SEC_USER_ROLE_col(i).process_id ,
             chunk_id        = svc_SEC_USER_ROLE_col(i).chunk_id ,
             row_seq         = svc_SEC_USER_ROLE_col(i).row_seq ,
             action          = svc_SEC_USER_ROLE_col(i).action ,
             process$status  = svc_SEC_USER_ROLE_col(i).process$status ,
             create_id       = svc_SEC_USER_ROLE_col(i).create_id ,
             create_datetime = svc_SEC_USER_ROLE_col(i).create_datetime ,
             last_upd_id     = svc_SEC_USER_ROLE_col(i).last_upd_id ,
             last_upd_datetime = svc_SEC_USER_ROLE_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             user_seq ,
             role ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_SEC_USER_ROLE_col(i).process_id ,
             svc_SEC_USER_ROLE_col(i).chunk_id ,
             svc_SEC_USER_ROLE_col(i).row_seq ,
             svc_SEC_USER_ROLE_col(i).action ,
             svc_SEC_USER_ROLE_col(i).process$status ,
             sq.user_seq ,
             sq.role ,
             svc_SEC_USER_ROLE_col(i).create_id ,
             svc_SEC_USER_ROLE_col(i).create_datetime ,
             svc_SEC_USER_ROLE_col(i).last_upd_id ,
             svc_SEC_USER_ROLE_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
         end if;         
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_ROLE_sheet,
                            svc_SEC_USER_ROLE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SEC_USER_ROLE;
--------------------------------------------------------------------------------

PROCEDURE PROCESS_S9T_SEC_USER( I_file_id    IN   s9t_folder.file_id%TYPE,
                                I_process_id IN   SVC_SEC_USER.process_id%TYPE)
IS
   TYPE svc_SEC_USER_col_typ IS TABLE OF SVC_SEC_USER%ROWTYPE;
   L_temp_rec SVC_SEC_USER%ROWTYPE;
   svc_SEC_USER_col svc_SEC_USER_col_typ :=NEW svc_SEC_USER_col_typ();
   L_process_id SVC_SEC_USER.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_SEC_USER%ROWTYPE;
   cursor C_MANDATORY_IND is
      select
             APPLICATION_USER_ID_mi,
             DATABASE_USER_ID_mi,
             USER_SEQ_mi,             
             ALLOCATION_USER_IND_mi,
             REIM_USER_IND_mi,
             RESA_USER_IND_mi,
             RMS_USER_IND_mi,
             MANAGER_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'SEC_USER'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'APPLICATION_USER_ID' AS APPLICATION_USER_ID,
                                         'DATABASE_USER_ID' AS DATABASE_USER_ID,
                                         'USER_SEQ' AS USER_SEQ,                                         
                                         'ALLOCATION_USER_IND' AS ALLOCATION_USER_IND,
                                         'REIM_USER_IND' AS REIM_USER_IND,
                                         'RESA_USER_IND' AS RESA_USER_IND,
                                         'RMS_USER_IND' AS RMS_USER_IND,
                                         'MANAGER' AS MANAGER,
                                          null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns    VARCHAR2(255)  := 'USER_SEQ';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select  APPLICATION_USER_ID_dv,
                       DATABASE_USER_ID_dv,
                       USER_SEQ_dv,                       
                       ALLOCATION_USER_IND_dv,
                       REIM_USER_IND_dv,
                       RESA_USER_IND_dv,
                       RMS_USER_IND_dv,
                       MANAGER_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = template_key
                          and wksht_key     = 'SEC_USER'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'APPLICATION_USER_ID' AS APPLICATION_USER_ID,
                                                      'DATABASE_USER_ID' AS DATABASE_USER_ID,
                                                      'USER_SEQ' AS USER_SEQ,                                                      
                                                      'ALLOCATION_USER_IND' AS ALLOCATION_USER_IND,
                                                      'REIM_USER_IND' AS REIM_USER_IND,
                                                      'RESA_USER_IND' AS RESA_USER_IND,
                                                      'RMS_USER_IND' AS RMS_USER_IND,
                                                      'MANAGER' AS MANAGER,
                                                       NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.APPLICATION_USER_ID := rec.APPLICATION_USER_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER ' ,
                            NULL,
                           'APPLICATION_USER_ID ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DATABASE_USER_ID := rec.DATABASE_USER_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER ' ,
                            NULL,
                           'DATABASE_USER_ID ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.USER_SEQ := rec.USER_SEQ_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER ' ,
                            NULL,
                           'USER_SEQ ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ALLOCATION_USER_IND := rec.ALLOCATION_USER_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER ' ,
                            NULL,
                           'ALLOCATION_USER_IND ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.REIM_USER_IND := rec.REIM_USER_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER ' ,
                            NULL,
                           'REIM_USER_IND ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.RESA_USER_IND := rec.RESA_USER_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER ' ,
                            NULL,
                           'RESA_USER_IND ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.RMS_USER_IND := rec.RMS_USER_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER ' ,
                            NULL,
                           'RMS_USER_IND ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MANAGER := rec.MANAGER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_USER ' ,
                            NULL,
                           'MANAGER ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(SEC_USER$Action)      AS Action,
          r.get_cell(SEC_USER$APPLICATION_USER_ID)              AS APPLICATION_USER_ID,
          r.get_cell(SEC_USER$DATABASE_USER_ID)              AS DATABASE_USER_ID,
          r.get_cell(SEC_USER$USER_SEQ)              AS USER_SEQ,         
          r.get_cell(SEC_USER$ALLOCATION_USER_IND)              AS ALLOCATION_USER_IND,
          r.get_cell(SEC_USER$REIM_USER_IND)              AS REIM_USER_IND,
          r.get_cell(SEC_USER$RESA_USER_IND)              AS RESA_USER_IND,
          r.get_cell(SEC_USER$RMS_USER_IND)              AS RMS_USER_IND,
          r.get_cell(SEC_USER$MANAGER)              AS MANAGER,
          r.get_row_seq()                             AS row_seq,
          count(1)OVER (PARTITION BY UPPER(r.get_cell(SEC_USER$APPLICATION_USER_ID))) AS ROW_COUNT 
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(SEC_USER_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         if rec.ROW_COUNT>1 then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T'));
                L_error := TRUE;
          end if;   
       END;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.APPLICATION_USER_ID := rec.APPLICATION_USER_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            'APPLICATION_USER_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DATABASE_USER_ID := rec.DATABASE_USER_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            'DATABASE_USER_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.USER_SEQ := rec.USER_SEQ;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            'USER_SEQ',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ALLOCATION_USER_IND := rec.ALLOCATION_USER_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            'ALLOCATION_USER_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REIM_USER_IND := rec.REIM_USER_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            'REIM_USER_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RESA_USER_IND := rec.RESA_USER_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            'RESA_USER_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RMS_USER_IND := rec.RMS_USER_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            'RMS_USER_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MANAGER := rec.MANAGER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            rec.row_seq,
                            'MANAGER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SEC_USER.action_new then
         L_temp_rec.APPLICATION_USER_ID := NVL( L_temp_rec.APPLICATION_USER_ID,L_default_rec.APPLICATION_USER_ID);
         L_temp_rec.DATABASE_USER_ID := NVL( L_temp_rec.DATABASE_USER_ID,L_default_rec.DATABASE_USER_ID);
         L_temp_rec.USER_SEQ := NVL( L_temp_rec.USER_SEQ,L_default_rec.USER_SEQ);         
         L_temp_rec.ALLOCATION_USER_IND := NVL( L_temp_rec.ALLOCATION_USER_IND,L_default_rec.ALLOCATION_USER_IND);
         L_temp_rec.REIM_USER_IND := NVL( L_temp_rec.REIM_USER_IND,L_default_rec.REIM_USER_IND);
         L_temp_rec.RESA_USER_IND := NVL( L_temp_rec.RESA_USER_IND,L_default_rec.RESA_USER_IND);
         L_temp_rec.RMS_USER_IND := NVL( L_temp_rec.RMS_USER_IND,L_default_rec.RMS_USER_IND);
         L_temp_rec.MANAGER := NVL( L_temp_rec.MANAGER,L_default_rec.MANAGER);
      end if;
      if not (
            L_temp_rec.USER_SEQ is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         SEC_USER_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_SEC_USER_col.extend();
         svc_SEC_USER_col(svc_SEC_USER_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_SEC_USER_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SEC_USER st
      using(select
                  (case
                   when l_mi_rec.APPLICATION_USER_ID_mi    = 'N'
                    and svc_SEC_USER_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.APPLICATION_USER_ID IS NULL
                   then mt.APPLICATION_USER_ID
                   else s1.APPLICATION_USER_ID
                   end) AS APPLICATION_USER_ID,
                  (case
                   when l_mi_rec.DATABASE_USER_ID_mi    = 'N'
                    and svc_SEC_USER_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.DATABASE_USER_ID IS NULL
                   then mt.DATABASE_USER_ID
                   else s1.DATABASE_USER_ID
                   end) AS DATABASE_USER_ID,
                  (case
                   when l_mi_rec.USER_SEQ_mi    = 'N'
                    and svc_SEC_USER_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.USER_SEQ IS NULL
                   then mt.USER_SEQ
                   else s1.USER_SEQ
                   end) AS USER_SEQ,
                  (case
                   when l_mi_rec.ALLOCATION_USER_IND_mi    = 'N'
                    and svc_SEC_USER_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.ALLOCATION_USER_IND IS NULL
                   then mt.ALLOCATION_USER_IND
                   else s1.ALLOCATION_USER_IND
                   end) AS ALLOCATION_USER_IND,
                  (case
                   when l_mi_rec.REIM_USER_IND_mi    = 'N'
                    and svc_SEC_USER_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.REIM_USER_IND IS NULL
                   then mt.REIM_USER_IND
                   else s1.REIM_USER_IND
                   end) AS REIM_USER_IND,
                  (case
                   when l_mi_rec.RESA_USER_IND_mi    = 'N'
                    and svc_SEC_USER_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.RESA_USER_IND IS NULL
                   then mt.RESA_USER_IND
                   else s1.RESA_USER_IND
                   end) AS RESA_USER_IND,
                  (case
                   when l_mi_rec.RMS_USER_IND_mi    = 'N'
                    and svc_SEC_USER_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.RMS_USER_IND IS NULL
                   then mt.RMS_USER_IND
                   else s1.RMS_USER_IND
                   end) AS RMS_USER_IND,
                  (case
                   when l_mi_rec.MANAGER_mi    = 'N'
                    and svc_SEC_USER_col(i).action = CORESVC_SEC_USER.action_mod
                    and s1.MANAGER IS NULL
                   then mt.MANAGER
                   else s1.MANAGER
                   end) AS MANAGER,
                  null as dummy
              from (select
                          svc_SEC_USER_col(i).APPLICATION_USER_ID AS APPLICATION_USER_ID,
                          svc_SEC_USER_col(i).DATABASE_USER_ID AS DATABASE_USER_ID,
                          svc_SEC_USER_col(i).USER_SEQ AS USER_SEQ,                          
                          svc_SEC_USER_col(i).ALLOCATION_USER_IND AS ALLOCATION_USER_IND,
                          svc_SEC_USER_col(i).REIM_USER_IND AS REIM_USER_IND,
                          svc_SEC_USER_col(i).RESA_USER_IND AS RESA_USER_IND,
                          svc_SEC_USER_col(i).RMS_USER_IND AS RMS_USER_IND,
                          svc_SEC_USER_col(i).MANAGER AS MANAGER,
                          null as dummy
                      from dual ) s1,
            SEC_USER mt
             where
                  mt.USER_SEQ (+)     = s1.USER_SEQ   and
                  mt.APPLICATION_USER_ID (+)     = s1.APPLICATION_USER_ID   and
                  mt.DATABASE_USER_ID (+)     = s1.DATABASE_USER_ID   and
                  1 = 1 )sq
                on (
                    st.USER_SEQ      = sq.USER_SEQ and
                    st.APPLICATION_USER_ID      = sq.APPLICATION_USER_ID and
                    st.DATABASE_USER_ID      = sq.DATABASE_USER_ID and
                    svc_SEC_USER_col(i).ACTION IN (CORESVC_SEC_USER.action_mod,CORESVC_SEC_USER.action_del))
      when matched then
      update
         set process_id      = svc_SEC_USER_col(i).process_id ,
             chunk_id        = svc_SEC_USER_col(i).chunk_id ,
             row_seq         = svc_SEC_USER_col(i).row_seq ,
             action          = svc_SEC_USER_col(i).action ,
             process$status  = svc_SEC_USER_col(i).process$status ,
             manager              = sq.manager ,             
             allocation_user_ind              = sq.allocation_user_ind ,
             rms_user_ind              = sq.rms_user_ind ,                  
             reim_user_ind              = sq.reim_user_ind ,
             resa_user_ind              = sq.resa_user_ind ,
             create_id       = svc_SEC_USER_col(i).create_id ,
             create_datetime = svc_SEC_USER_col(i).create_datetime ,
             last_upd_id     = svc_SEC_USER_col(i).last_upd_id ,
             last_upd_datetime = svc_SEC_USER_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             application_user_id ,
             database_user_id ,
             user_seq ,             
             allocation_user_ind ,
             reim_user_ind ,
             resa_user_ind ,
             rms_user_ind ,
             manager ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_SEC_USER_col(i).process_id ,
             svc_SEC_USER_col(i).chunk_id ,
             svc_SEC_USER_col(i).row_seq ,
             svc_SEC_USER_col(i).action ,
             svc_SEC_USER_col(i).process$status ,
             sq.application_user_id ,
             sq.database_user_id ,
             sq.user_seq ,            
             sq.allocation_user_ind ,
             sq.reim_user_ind ,
             sq.resa_user_ind ,
             sq.rms_user_ind ,
             sq.manager ,
             svc_SEC_USER_col(i).create_id ,
             svc_SEC_USER_col(i).create_datetime ,
             svc_SEC_USER_col(i).last_upd_id ,
             svc_SEC_USER_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;         
         
            WRITE_S9T_ERROR(I_file_id,
                            SEC_USER_sheet,
                            svc_SEC_USER_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SEC_USER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_SEC_USER.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   MAX_CHAR         EXCEPTION;
   PRAGMA           EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                         template_category,
                         L_file,
                         TRUE) = FALSE then
       return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);                                         
      PROCESS_S9T_SEC_USER_ROLE(I_file_id,I_process_id);
      PROCESS_S9T_SEC_USER(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
           values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
      
   when OTHERS then    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
                                             
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_CHANNELS.PROCESS_ID%TYPE) IS

BEGIN
   Delete
     from svc_sec_user_role
    where process_id= I_process_id;

   Delete
     from svc_sec_user
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;
--------------------------------------------------------------------------------
FUNCTION PROCESS_SEC_USER_ROLE_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_error           IN OUT   BOOLEAN,
                                   I_rec             IN       C_SVC_SEC_USER_ROLE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_USER.PROCESS_SEC_USER_ROLE_VAL';
BEGIN
 --WRITE YOUR LOGIC HERE
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SEC_USER_ROLE_VAL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_USER_ROLE_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sec_user_role_temp_rec   IN       SEC_USER_ROLE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_USER.EXEC_SEC_USER_ROLE_INS';
   
BEGIN
   insert
     into sec_user_role
   values I_sec_user_role_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_USER_ROLE_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_USER_ROLE_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sec_user_role_temp_rec   IN       SEC_USER_ROLE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_USER.EXEC_SEC_USER_ROLE_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   cursor C_SEC_USER_ROLE_LOCK is
      select 'X'
        from sec_user_role  
       where user_seq = I_sec_user_role_temp_rec.user_seq
         and role = I_sec_user_role_temp_rec.role
       for update nowait;  
   
BEGIN
   open  C_SEC_USER_ROLE_LOCK;
   close C_SEC_USER_ROLE_LOCK;
   update sec_user_role
      set row = I_sec_user_role_temp_rec
    where user_seq = I_sec_user_role_temp_rec.user_seq
      and role = I_sec_user_role_temp_rec.role;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SEC_USER_ROLE',
                                                               I_sec_user_role_temp_rec.user_seq,
                                                               I_sec_user_role_temp_rec.role);
      return FALSE;
   when OTHERS then
      if C_SEC_USER_ROLE_LOCK%ISOPEN then
         close C_SEC_USER_ROLE_LOCK;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_USER_ROLE_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_USER_ROLE_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sec_user_role_temp_rec   IN       SEC_USER_ROLE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_USER.EXEC_SEC_USER_ROLE_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   cursor C_SEC_USER_ROLE_LOCK is
      select 'X'
        from sec_user_role  
       where user_seq = I_sec_user_role_temp_rec.user_seq
         and role = I_sec_user_role_temp_rec.role
       for update nowait;  
  
BEGIN
   open  C_SEC_USER_ROLE_LOCK;
   close C_SEC_USER_ROLE_LOCK;
   delete
     from sec_user_role
    where user_seq = I_sec_user_role_temp_rec.user_seq
      and role = I_sec_user_role_temp_rec.role;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SEC_USER_ROLE',
                                                               I_sec_user_role_temp_rec.user_seq,
                                                               I_sec_user_role_temp_rec.role);
   when OTHERS then
      if C_SEC_USER_ROLE_LOCK%ISOPEN then
         close C_SEC_USER_ROLE_LOCK;
      end if;  
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_USER_ROLE_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_SEC_USER_ROLE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id    IN     SVC_SEC_USER_ROLE.PROCESS_ID%TYPE,
                               I_chunk_id      IN     SVC_SEC_USER_ROLE.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_SEC_USER.PROCESS_SEC_USER_ROLE';
   L_error_message VARCHAR2(600);
   L_SEC_USER_ROLE_temp_rec SEC_USER_ROLE%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_SEC_USER_ROLE';
BEGIN
   FOR rec IN c_svc_SEC_USER_ROLE(I_process_id,
                                  I_chunk_id)
   LOOP
      L_error               := FALSE;      
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action = action_new
         and rec.PK_SEC_USER_ROLE_rid is NOT NULL 
         and rec.user_seq is NOT NULL then
         
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'USER_SEQ',
                     'USERSEQ_EXISTS');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_SEC_USER_ROLE_rid is NULL 
         and rec.user_seq is not NULL then
         
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'USER_SEQ',
                     'INV_USERSEQ');
         L_error :=TRUE;
      end if;
      if rec.sur_rrp_fk_rid is NULL then
         
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ROLE',
                     'ROLE_INVALID');
         L_error :=TRUE;
      end if;
      if rec.sur_seu_fk_rid is NULL then
         
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'USER_SEQ',
                     'INV_USERSEQ');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
         and rec.USER_SEQ  IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'USER_SEQ',
                     'SVC_COL_REQD');
         L_error :=TRUE;
      end if;
    if rec.action IN (action_new,action_mod)
       and rec.role is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ROLE',
                     'SVC_COL_REQD');
         L_error :=TRUE;
      end if;
      if PROCESS_SEC_USER_ROLE_VAL(O_error_message,
                                   L_error,
                                   rec ) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     L_error_message);
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_sec_user_role_temp_rec.user_seq          := rec.user_seq;
         L_sec_user_role_temp_rec.role              := rec.role;
         L_sec_user_role_temp_rec.create_id         := GET_USER;
         L_sec_user_role_temp_rec.create_datetime   := SYSDATE;
         
         if rec.action = action_new then
            if EXEC_SEC_USER_ROLE_INS(O_error_message,
                                      L_sec_user_role_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_SEC_USER_ROLE_UPD(O_error_message,
                                      L_sec_user_role_temp_rec )=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_SEC_USER_ROLE_DEL( O_error_message,
                                       L_sec_user_role_temp_rec )=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_sec_user_role st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_sec_user_role st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_sec_user_role st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SEC_USER_ROLE;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SEC_USER_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error           IN OUT   BOOLEAN,
                              I_rec             IN       C_SVC_SEC_USER%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_USER.PROCESS_SEC_USER_VAL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_SEC_USER';
   L_exists  VARCHAR2(1);
   cursor C_EXISTS is
      select 'x'
        from sec_user_group
       where user_seq = I_rec.user_seq;
   cursor C_VAL_MANAGER is
      select 'x' 
        from sec_user
       where user_seq = I_rec.manager;  
   
BEGIN
 
 if I_rec.action in (action_new,action_mod) then
    if I_rec.user_seq = I_rec.manager then
       WRITE_ERROR(I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                   NULL,
                   'INV_MANAGER');
          O_error :=TRUE;
     end if;
  end if;
 
 if I_rec.action = action_del then
    open  c_exists;
    fetch c_exists into L_exists;
       if c_exists%found then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     'CHILD_EXISTS');
          O_error :=TRUE;
       end if;        
    close c_exists;    
 end if;
 
 if I_rec.action in (action_new,action_mod) then
    if I_rec.application_user_id is null and I_rec.database_user_id is null then
       O_error_message := SQL_LIB.CREATE_MSG('INV_COMBO',
                                             'SEC_USER',
                                             'APPLICATION_USER_ID',
                                             'DATABASE_USER_ID');    
      
       WRITE_ERROR(I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                   NULL,
                   O_error_message);
       O_error :=TRUE;     
    end if;  
 end if;
 
 if I_rec.manager is not null then
    open  c_val_manager;
    fetch c_val_manager into L_exists;
       if c_val_manager%notfound then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MANAGER',
                     'INV_MANAGER');
          O_error :=TRUE;
       end if;        
    close c_val_manager;    
 end if;
     

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_EXISTS%ISOPEN then
         close C_EXISTS;
      end if;
      if C_VAL_MANAGER%ISOPEN then
         close C_VAL_MANAGER;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SEC_USER_VAL;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_USER_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,  
                           I_sec_user_temp_rec   IN       SEC_USER%ROWTYPE
                           )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_USER.EXEC_SEC_USER_INS';
   L_table   VARCHAR2(255):= 'SVC_SEC_USER';
BEGIN
   insert
     into sec_user
   values I_sec_user_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_USER_INS;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_USER_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                           I_sec_user_temp_rec   IN       SEC_USER%ROWTYPE
                           )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_USER.EXEC_SEC_USER_UPD';
   L_table   VARCHAR2(255):= 'SVC_SEC_USER';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   
   cursor C_SEC_USER_LOCK is
      select 'X'
           from sec_user
          where user_seq = I_sec_user_temp_rec.user_seq
            and NVL(application_user_id,'-1') = NVL(I_sec_user_temp_rec.application_user_id,'-1')
            and NVL(database_user_id,'-1') = NVL(I_sec_user_temp_rec.database_user_id,'-1')
      for update nowait;
BEGIN
   open  C_SEC_USER_LOCK;
   close C_SEC_USER_LOCK;
   
   update sec_user
      set row = I_sec_user_temp_rec
    where user_seq = I_sec_user_temp_rec.user_seq
      and NVL(application_user_id,'-1') = NVL(I_sec_user_temp_rec.application_user_id,'-1')
      and NVL(database_user_id,'-1') = NVL(I_sec_user_temp_rec.database_user_id,'-1');    
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
         O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                  'SEC_USER',
                                                                   I_sec_user_temp_rec.user_seq,
                                                                   NULL);
      return FALSE;

   when OTHERS then
      if C_SEC_USER_LOCK%ISOPEN then
         close C_SEC_USER_LOCK;
      end if;   
   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_USER_UPD;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_USER_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,  
                           I_sec_user_temp_rec   IN       SEC_USER%ROWTYPE,
                           I_process_id          IN       SVC_SEC_USER.PROCESS_ID%TYPE,
                           I_chunk_id            IN       SVC_SEC_USER.CHUNK_ID%TYPE,
                           I_row_seq             IN       SVC_ADMIN_UPLD_ER.row_seq%TYPE
                           )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_USER.EXEC_SEC_USER_DEL';
   L_table   VARCHAR2(255):= 'SVC_SEC_USER';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   Cursor c_child_rec is
      select user_seq,
             role,
             process_id,
             chunk_id,
             row_seq
        from svc_sec_user_role
       where user_seq = I_sec_user_temp_rec.user_seq
         and action = action_del
         and process_id = I_process_id
         and chunk_id = I_chunk_id;
   Cursor C_SEC_USER_ROLE_LOCK(I_user_seq IN SEC_USER_ROLE.USER_SEQ%TYPE,
                               I_role     IN SEC_USER_ROLE.ROLE%TYPE) is 
      select 'X' 
        from sec_user_role
       where user_seq = I_user_seq
         and role = I_role
      for update nowait;
   Cursor C_SEC_USER_LOCK is 
      select 'X'
        from sec_user
       where user_seq = I_sec_user_temp_rec.user_seq
         and NVL(application_user_id,'-1') = NVL(I_sec_user_temp_rec.application_user_id,'-1')
         and NVL(database_user_id,'-1') = NVL(I_sec_user_temp_rec.database_user_id,'-1')
         and not exists (select 1 from sec_user_role where user_seq = I_sec_user_temp_rec.user_seq)
     for update nowait;     
       
   
BEGIN
   L_table := 'SEC_USER_ROLE';   
   for rec in c_child_rec loop
      open  C_SEC_USER_ROLE_LOCK(rec.user_seq,rec.role) ;
      close C_SEC_USER_ROLE_LOCK;
      
      delete from sec_user_role 
       where user_seq = rec.user_seq
         and role = rec.role;
     if sql%rowcount > 0 then    
        update svc_sec_user_role
           set process$status = 'P'
         where row_seq = rec.row_seq
           and user_seq = rec.user_seq
           and role = rec.role
           and process_id = rec.process_id
           and chunk_id = rec.chunk_id;
      end if;      
   end loop;
   L_table := 'SEC_USER'; 
   open  C_SEC_USER_LOCK;
   close C_SEC_USER_LOCK;
   delete
     from sec_user
    where user_seq = I_sec_user_temp_rec.user_seq
      and NVL(application_user_id,'-1') = NVL(I_sec_user_temp_rec.application_user_id,'-1')
      and NVL(database_user_id,'-1') = NVL(I_sec_user_temp_rec.database_user_id,'-1')
      and not exists (select 1 from sec_user_role where user_seq = I_sec_user_temp_rec.user_seq);
   if sql%rowcount = 0 then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  NULL,
                 'CHILD_EXISTS');   
   end if;
      
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                L_table,
                                                                I_sec_user_temp_rec.user_seq,
                                                                NULL);
      return FALSE;
   when OTHERS then
      if C_SEC_USER_LOCK%ISOPEN then
         close C_SEC_USER_LOCK;
      end if;
      if C_SEC_USER_ROLE_LOCK%ISOPEN then
         close C_SEC_USER_LOCK;
      end if; 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_USER_DEL;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SEC_USER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN   SVC_SEC_USER.PROCESS_ID%TYPE,
                          I_chunk_id        IN   SVC_SEC_USER.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_SEC_USER.PROCESS_SEC_USER';
   L_error_message VARCHAR2(600);
   L_SEC_USER_temp_rec SEC_USER%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_SEC_USER';   
BEGIN
   FOR rec IN c_svc_SEC_USER(I_process_id,
                             I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action = action_new
         and rec.SEU_APP_USER_UK_rid is NOT NULL then
        
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                    'DUP_APPLICATION_USER_ID');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.SEU_APP_USER_UK_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'APPLICATION_USER_ID',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_SEC_USER_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                    'INV_USERSEQ');
         L_error :=TRUE;
      end if;
    if NOT( rec.ALLOCATION_USER_IND IN  ( 'Y','N' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ALLOCATION_USER_IND',
                     'INV_Y_N_IND');
         L_error :=TRUE;
      end if;
    if NOT( rec.REIM_USER_IND IN  ( 'Y','N' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'REIM_USER_IND',
                     'INV_Y_N_IND');
         L_error :=TRUE;
      end if;
    if NOT( rec.RESA_USER_IND IN  ( 'Y','N' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'RESA_USER_IND',
                     'INV_Y_N_IND');
         L_error :=TRUE;
      end if;
    if NOT( rec.RMS_USER_IND IN  ( 'Y','N' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'RMS_USER_IND',
                     'INV_Y_N_IND');
         L_error :=TRUE;
      end if;
    if NOT(  rec.ALLOCATION_USER_IND  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ALLOCATION_USER_IND',
                     'SVC_COL_REQD');
         L_error :=TRUE;
      end if;
    if NOT(  rec.REIM_USER_IND  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'REIM_USER_IND',
                     'SVC_COL_REQD');
         L_error :=TRUE;
      end if;
    if NOT(  rec.RESA_USER_IND  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'RESA_USER_IND',
                    'SVC_COL_REQD');
         L_error :=TRUE;
      end if;
    if NOT(  rec.RMS_USER_IND  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'RMS_USER_IND',
                    'SVC_COL_REQD');
         L_error :=TRUE;
      end if;
    if NOT(  rec.USER_SEQ  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'USER_SEQ',
                     'SVC_COL_REQD');
         L_error :=TRUE;
      end if;
      
      if PROCESS_SEC_USER_VAL(O_error_message,
                              L_error,
                              rec ) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then         
         L_sec_user_temp_rec.user_seq               := rec.user_seq;         
         L_sec_user_temp_rec.allocation_user_ind    := rec.allocation_user_ind;         
         L_sec_user_temp_rec.reim_user_ind          := rec.reim_user_ind;         
         L_sec_user_temp_rec.resa_user_ind          := rec.resa_user_ind;         
         L_sec_user_temp_rec.rms_user_ind           := rec.rms_user_ind;         
         L_sec_user_temp_rec.manager                := rec.manager;         
         L_sec_user_temp_rec.application_user_id    := rec.application_user_id;
         L_sec_user_temp_rec.database_user_id       := rec.database_user_id;
         L_sec_user_temp_rec.create_id              := GET_USER;
         L_sec_user_temp_rec.create_datetime        := sysdate;
         if rec.action = action_new then
            if EXEC_SEC_USER_INS(O_error_message,   
                                 L_sec_user_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then           
            if EXEC_SEC_USER_UPD(O_error_message, 
                                 L_sec_user_temp_rec )=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_SEC_USER_DEL(O_error_message, 
                                 L_sec_user_temp_rec,
                                 I_process_id,
                                 I_chunk_id,
                                 rec.row_seq)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_sec_user st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_sec_user st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_sec_user st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
                                             
      return FALSE;
END PROCESS_SEC_USER;
------------------------------------------------------------------------------------------------
FUNCTION PRE_PROCESS_SEC_USER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_SEC_USER.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_SEC_USER.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_SEC_USER.PRE_PROCESS_SEC_USER';
   L_seq   SEC_USER.USER_SEQ%TYPE;
   cursor C_PRE_PROCESS is
      select st.rowid AS st_rid,            
             st.application_user_id,
             st.database_user_id,
             st.user_seq,             
             st.allocation_user_ind,
             st.reim_user_ind,
             st.resa_user_ind,
             st.rms_user_ind,
             st.manager,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_sec_user st
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
       order by st.row_seq asc;  


BEGIN
   FOR rec IN C_PRE_PROCESS
   LOOP
      if rec.action = action_new then
         L_seq := sec_user_sequence.NEXTVAL;
         update svc_sec_user_role 
            set user_seq = L_seq
          where user_seq = rec.user_seq 
            and process_id = I_process_id 
            and chunk_id = I_chunk_id;
                  
         update svc_sec_user
            set manager  = L_seq                
          where manager = rec.user_seq
            and process_id = I_process_id 
            and chunk_id = I_chunk_id;
            
        update svc_sec_user
            set user_seq  = L_seq                
          where user_seq = rec.user_seq
            and process_id = I_process_id 
            and chunk_id = I_chunk_id;             
      end if;   
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END PRE_PROCESS_SEC_USER;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_SEC_USER.PROCESS';
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PRE_PROCESS_SEC_USER(O_error_message,
                           I_process_id,
                           I_chunk_id)=FALSE then
      return FALSE;
   end if;   
   if PROCESS_SEC_USER(O_error_message,
                       I_process_id,
                       I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_SEC_USER_ROLE(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;
   
   if L_err_count is NOT NULL then
      L_process_status := 'PE'; 
   else
      L_process_status := 'PS';
   end if;
   
  update svc_process_tracker
     set status =(case
                  when status='PE'
                  then 'PE'
                  else L_process_status
                  end),
         action_date=sysdate
   where process_id=I_process_id;   
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_ERR_COUNT%ISOPEN then
         close C_GET_ERR_COUNT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
------------------------------------------------------------------------------------------------
END CORESVC_SEC_USER;
/
