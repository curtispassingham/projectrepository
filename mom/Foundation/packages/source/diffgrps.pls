
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFF_GROUP_SQL AUTHID CURRENT_USER AS

TYPE diff_group_detail_tbl IS TABLE OF DIFF_GROUP_DETAIL%ROWTYPE;
TYPE diff_group_rec        IS RECORD(diff_group_head_row   DIFF_GROUP_HEAD%ROWTYPE,
                                     diff_group_details    DIFF_GROUP_DETAIL_TBL);
-------------------------------------------------------------------------------
-- Name:    EXIST
-- Purpose: To check for existence of a given diff group.
-------------------------------------------------------------------------------
FUNCTION EXIST(O_error_message   IN OUT VARCHAR2,
               O_exist           IN OUT BOOLEAN,
               I_diff_group_id   IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    GET_INFO
-- Purpose: To retrieve header information for a given diff group.
-------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message   IN OUT VARCHAR2,
                  O_group_desc      IN OUT DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                  O_diff_type       IN OUT DIFF_GROUP_HEAD.DIFF_TYPE%TYPE,
                  I_diff_group_id   IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                  I_diff_1	         IN     ITEM_MASTER.DIFF_1%TYPE DEFAULT NULL,
                  I_diff_2          IN     ITEM_MASTER.DIFF_2%TYPE DEFAULT NULL,
                  I_diff_3          IN     ITEM_MASTER.DIFF_3%TYPE DEFAULT NULL,
                  I_diff_4          IN     ITEM_MASTER.DIFF_4%TYPE DEFAULT NULL
                  )
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    CHECK_DELETE
-- Purpose: To determine whether any records exist on ITEM_MASTER for diff id - DIFF1, DIFF2, DIFF_3 or
--          DIFF_4. Also checks that the group doesn't exist in a range or a template.
--          TRUE or FALSE value is returned in O_exists.
-------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT VARCHAR2,
                      O_exists          IN OUT BOOLEAN,
                      I_diff_group_id   IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    DELETE_DIFF_GROUP_DETAILS
-- Purpose: To delete from diff_group_detail all records for the input diff id - DIFF1, DIFF2, DIFF3 or DIFF_4.
-------------------------------------------------------------------------------
FUNCTION DELETE_DIFF_GROUP_DETAILS(O_error_message   IN OUT VARCHAR2,
                                   I_diff_group_id   IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    DETAILS_NO_EXIST
-- Purpose: This function checks for DIFF_GROUP_ID that have no details.
--          The parameter O_no_details will be populated with a TRUE or FALSE
--          based upon the existance of details.  True - details exist.  False - details do not exist
-------------------------------------------------------------------------------
FUNCTION DETAILS_NO_EXIST(O_error_message IN OUT VARCHAR2,
                          O_no_details    IN OUT BOOLEAN,
                          O_diff_group_id IN OUT DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    DUP_DIFF_ID
-- Purpose: This function will check if the entered diff_id/diff_group_id combination exists in the
--          DIFF_GROUP_DETAIL table.  True is returned for O_exists.
-------------------------------------------------------------------------------
FUNCTION DUP_DIFF_ID(O_error_message   IN OUT VARCHAR2,
                     O_exists          IN OUT BOOLEAN,
                     I_diff_id         IN     DIFF_GROUP_DETAIL.DIFF_ID%TYPE,
                     I_diff_group_id   IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    CHECK_DELETE_DETAILS
-- Purpose: This function will check if the entered diff_id/diff_group_id combination exists in the
--          ITEM_MASTER table. Also checks for this combination on ranges and templates.
--          True is returned for O_exists.
-------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_DETAILS(O_error_message   IN OUT VARCHAR2,
                              O_exists          IN OUT BOOLEAN,
                              I_diff_id         IN     DIFF_GROUP_DETAIL.DIFF_ID%TYPE,
                              I_diff_group_id   IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    DELETE_GRP_NO_DETAILS
-- Purpose: This function will take a Diff Group ID as input and will delete that record
--          from the table diff_group_head
-------------------------------------------------------------------------------
FUNCTION DELETE_GRP_NO_DETAILS(O_error_message   IN OUT VARCHAR2,
                               I_diff_group_id   IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_DIFF_GROUP_DETLS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid          IN OUT  BOOLEAN,
                              O_diff_type      IN OUT  DIFF_IDS.DIFF_TYPE%TYPE,
                              O_diff_desc      IN OUT  DIFF_IDS.DIFF_DESC%TYPE,
                              O_display_seq    IN OUT  DIFF_GROUP_DETAIL.DISPLAY_SEQ%TYPE,
                              I_diff_group     IN      DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                              I_diff_id        IN      DIFF_IDS.DIFF_ID%TYPE, 
                              I_item           IN      ITEM_MASTER.ITEM%TYPE DEFAULT NULL
                              )
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function    : INSERT_DIFF_GROUP
-- Purpose     : Takes in a diff_group_id table record and inserts all of
--               it's contents into the DIFF_GROUP_HEAD and DIFF_GROUP_DETAIL tables.
--Created      : 11-Aug-03 Eldonelle Dolloso
-------------------------------------------------------------------------------
FUNCTION INSERT_DIFF_GROUP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diff_group_rec    IN       DIFF_GROUP_SQL.DIFF_GROUP_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function    : INSERT_DETAIL
-- Purpose     : Takes in a diff_group_id table record and inserts all of
--               it's contents into the DIFF_GROUP_DETAIL table.
--Created      : 11-Aug-03 Eldonelle Dolloso
-------------------------------------------------------------------------------
FUNCTION INSERT_DETAIL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diff_group_detail_tbl   IN       DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function    : UPDATE_HEAD
-- Purpose     : Takes in a diff_group_id table record and updates all of
--               it's contents into the DIFF_GROUP_HEAD table.
--Created      : 11-Aug-03 Eldonelle Dolloso
-------------------------------------------------------------------------------
FUNCTION UPDATE_HEAD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_diff_group_head_rec   IN       DIFF_GROUP_HEAD%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function    : UPDATE_DETAIL
-- Purpose     : Takes in a diff_group_id table record and updates all of
--               it's contents into the DIFF_GROUP_DETAIL table.
--Created      : 11-Aug-03 Eldonelle Dolloso
-------------------------------------------------------------------------------
FUNCTION UPDATE_DETAIL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diff_group_detail_tbl   IN       DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function    : DELETE_DETAIL
-- Purpose     : This public function will perform the actual delete of a DIFF_GROUP_DETAIL record
--               for a diff_group/diff_id combination.
--Created      : 11-Aug-03 Eldonelle Dolloso
-------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diff_group_detail_tbl   IN       DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function    : DIFF_GROUP_EXISTS
-- Purpose     : This public function will check if a diff_id or diff_group_id is uniques to both
--               diff_ids and diff_group_head tables.
-- Created     : 27-Aug-03 Eldonelle Dolloso
-------------------------------------------------------------------------------
FUNCTION DIFF_GROUP_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exist           IN OUT   BOOLEAN,
                           I_diffgrp_id      IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function    : GET_DIFF_TYPE
-- Purpose     : This public function will retrieve the diff_type of a diff_id.
-- Created     : 03-Oct-03 Eldonelle Dolloso
-------------------------------------------------------------------------------
FUNCTION GET_DIFF_TYPE(O_error_message   IN OUT   VARCHAR2,
                       O_diff_type       IN OUT   DIFF_IDS.DIFF_TYPE%TYPE,
                       I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function    : GET_DIFF_GROUP_TYPE
-- Purpose     : This public function will retrieve the diff_type of a diff_group_id.
-- Created     : 09-Oct-03 Eldonelle Dolloso
-------------------------------------------------------------------------------
FUNCTION GET_DIFF_GROUP_TYPE(O_error_message   IN OUT   VARCHAR2,
                             O_diff_type       IN OUT   DIFF_GROUP_HEAD.DIFF_TYPE%TYPE,
                             I_diff_grp_id     IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
END DIFF_GROUP_SQL;
/


