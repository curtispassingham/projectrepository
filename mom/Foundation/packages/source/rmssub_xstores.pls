
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XSTORE AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
--PUBLIC VARIABLES
----------------------------------------------------------------------------
   LP_cre_type           VARCHAR2(15) := 'xstorecre';
   LP_mod_type           VARCHAR2(15) := 'xstoremod';
   LP_del_type           VARCHAR2(15) := 'xstoredel';

   LP_loctrt_cre_type    VARCHAR2(15) := 'xstoreloctrtcre';
   LP_loctrt_del_type    VARCHAR2(15) := 'xstoreloctrtdel';

   LP_wt_cre_type        VARCHAR2(15) := 'xstorewtcre';
   LP_wt_del_type        VARCHAR2(15) := 'xstorewtdel';
   
   LP_addr_cre_type      VARCHAR2(15) := 'xstoreaddrcre';
   LP_addr_mod_type      VARCHAR2(15) := 'xstoreaddrmod';
   LP_addr_del_type      VARCHAR2(15) := 'xstoreaddrdel';
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_XSTORE;
/
