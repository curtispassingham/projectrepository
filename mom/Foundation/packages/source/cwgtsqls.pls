CREATE OR REPLACE PACKAGE CATCH_WEIGHT_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: CALC_AV_COST_CATCH_WEIGHT
-- Purpose      : Calculates the average cost given added qty and cost of that qty
--                for a simple pack catch weight item.
--------------------------------------------------------------------------------
FUNCTION CALC_AV_COST_CATCH_WEIGHT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_new_av_cost     IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                                   I_soh_curr        IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   I_av_cost_curr    IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                   I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   I_total_cost      IN       ORDLOC.UNIT_COST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CONVERT_WEIGHT
-- Purpose      : This function converts I_weight from I_weight_uom to item's cost uom as defined on 
--                ITEM_SUPP_COUNTRY table for the item's primary supplier and 
--                primary country. 
--------------------------------------------------------------------------------
FUNCTION CONVERT_WEIGHT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_weight_cw_uom    IN OUT   ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        O_catch_weight_uom IN OUT   ITEM_SUPP_COUNTRY.COST_UOM%TYPE,
                        I_item             IN       ITEM_MASTER.ITEM%TYPE,
                        I_weight           IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        I_weight_uom       IN       UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CALC_AVERAGE_WEIGHT
-- Purpose      : This function calculates the new average weight for an item based on the 
--                current stock on hand and current average weight (in cost uom) and the weight
--                (in I_weight_uom) of the additional quantity. If I_weight_uom is NULL, 
--                I_weight is in item's cost uom. I_soh_curr and I_qty are both in item's standard uom. 
--                I_avg_weight_curr and the new average weight are both in item's cost uom. 
--------------------------------------------------------------------------------
FUNCTION CALC_AVERAGE_WEIGHT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_avg_weight_new    IN OUT   ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_item              IN       ITEM_LOC_SOH.ITEM%TYPE,
                             I_loc               IN       ITEM_LOC_SOH.LOC%TYPE,
                             I_loc_type          IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                             I_soh_curr          IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_avg_weight_curr   IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_qty    	         IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,  
                             I_weight 	         IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE, 
                             I_weight_uom        IN       UOM_CLASS.UOM%TYPE,
                             I_recalc_ind        IN       BOOLEAN DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CALC_TOTAL_COST
-- Purpose      : This function will calculate the total cost of a simple pack catch weight item.
--                I_qty is the number of packs. I_weight is the weight of I_qty. 
--                If I_weight_uom is NULL, I_weight is in the pack item's cost unit of measure. 
--                If I_weight and I_weight_uom are both NULL, the average weight of the pack item 
--                at the location will be used.
--------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_total_cost      IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                         I_pack_item       IN       ITEM_LOC_SOH.ITEM%TYPE,
                         I_loc             IN       ITEM_LOC_SOH.LOC%TYPE,
                         I_loc_type        IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                         I_pack_unit_cost  IN       ITEM_LOC_SOH.AV_COST%TYPE,
                         I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE, 
                         I_weight_uom      IN       UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CALC_TOTAL_COST
-- Purpose      : This function will calculate the total cost of a simple pack catch weight item.
--                I_weight_cuom is the weight of the pack item in cuom.
--                I_comp_unit_cost is the component item's unit cost.
--                Pack's nominal weight will be used to calculate the total cost.
--------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_COST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_total_cost       IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                         I_pack_item        IN       ITEM_LOC_SOH.ITEM%TYPE,
                         I_comp_unit_cost   IN       ITEM_LOC_SOH.AV_COST%TYPE,
                         I_weight_cw_uom    IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                         I_receipt_qty      IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: PRORATE_WEIGHT
-- Purpose      : This function returns the weight of I_qty in cost uom. 
--                If I_total_weight is defined, convert it to cost uom and
--                prorate it for I_qty; otherwise, use pack's average weight
--                to derive the weight and return cost uom.
--------------------------------------------------------------------------------
FUNCTION PRORATE_WEIGHT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_weight_cw_uom      IN OUT   ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        O_catch_weight_uom   IN OUT   ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE,
                        I_pack_no            IN       ITEM_MASTER.ITEM%TYPE,
                        I_loc                IN       ITEM_LOC_SOH.LOC%TYPE,
                        I_loc_type           IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                        I_total_weight       IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        I_total_weight_uom   IN       UOM_CLASS.UOM%TYPE,
                        I_total_qty          IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_qty                IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CALC_COMP_UPDATE_QTY
-- Purpose      : This function returns the qty to update the stock buckets 
--                for a simple pack catch weight component item. If component's 
--                standard uom is Eaches, ITEM_LOC_SOH will be updated with unit qty
--                based on V_PACKSKU_QTY; if it is Mass uom, ITEM_LOC_SOH will be 
--                updated with the weight converted to component's standard uom.
--------------------------------------------------------------------------------
FUNCTION CALC_COMP_UPDATE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_upd_qty         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_comp_item       IN       ITEM_MASTER.ITEM%TYPE,
                              I_unit_qty        IN       V_PACKSKU_QTY.QTY%TYPE,
                              I_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_weight_uom      IN       UOM_CLASS.UOM%type)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CALC_COMP_UPDATE_QTY
-- Purpose      : This is an overloaded function that returns the qty to update the
--                stock buckets with for a simple pack catch weight component item. 
--                If weight and weight_uom are not passed in, it uses the pack item's 
--                average weight to derive weight.
--------------------------------------------------------------------------------
FUNCTION CALC_COMP_UPDATE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_upd_qty         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_comp_item       IN       ITEM_MASTER.ITEM%TYPE,
                              I_comp_unit_qty   IN       V_PACKSKU_QTY.QTY%TYPE,
                              I_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_weight_uom      IN       UOM_CLASS.UOM%TYPE,
                              I_pack_no         IN       V_PACKSKU_QTY.PACK_NO%TYPE,
                              I_location        IN       ITEM_LOC_SOH.LOC%TYPE,
                              I_loc_type        IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                              I_pack_unit_qty   IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CALC_TOTAL_RETAIL
-- Purpose      : This  function calculates and returns the total retail 
--                based on weight.
--                If weight and weight_uom are not passed in, it uses the pack item's 
--                average weight to derive weight.
--------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_RETAIL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_comp_item              IN       ITEM_MASTER.ITEM%TYPE,    
                           I_qty                    IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_unit_retail            IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                           I_location               IN       ITEM_LOC_SOH.LOC%TYPE,
                           I_loc_type               IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                           I_weight                 IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                           I_weight_uom             IN       UOM_CLASS.UOM%TYPE,
                           O_total_retail           IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           I_post_weight_variance   IN       BOOLEAN DEFAULT FALSE,
                           I_inbound                IN       BOOLEAN DEFAULT TRUE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: POST_WEIGHT_VARIANCE
-- Purpose      : This  function posts the retail variance based on the weight variance.
--------------------------------------------------------------------------
FUNCTION POST_WEIGHT_VARIANCE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item                IN       ITEM_MASTER.ITEM%TYPE,
                              I_location            IN       ITEM_LOC.LOC%TYPE,
                              I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_units               IN       TRAN_DATA.UNITS%TYPE,
                              I_ref_no_1            IN       TRAN_DATA.REF_NO_1%TYPE,
                              I_ref_no_2            IN       TRAN_DATA.REF_NO_2%TYPE,
                              I_total_weight_old    IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_total_weight_new    IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_weight_uom          IN       UOM_CLASS.UOM%TYPE,
                              I_unit_retail         IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                              I_tran_date           IN       PERIOD.VDATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: DETERMINE_CATCH_WEIGHT_TYPE
-- Purpose      : To return the Catch Weight Type depending upon the Order type 
--                and sale Type from Item_Master. This is required by ReIM
--                for Catch Weight Functionality.
--------------------------------------------------------------------------------
FUNCTION DETERMINE_CATCH_WEIGHT_TYPE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_catch_weight_type   IN OUT   ITEM_MASTER.CATCH_WEIGHT_TYPE%TYPE,
                                     I_item                IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END CATCH_WEIGHT_SQL;
/
