CREATE OR REPLACE PACKAGE BODY CORESVC_FCUSTOMER_UPLOAD_SQL AS
---------------------------------------------------------------------------------------------------------------------
-- GLOBAL VARIABLES
---------------------------------------------------------------------------------------------------------------------
   LP_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE := NULL;
   LP_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE := NULL;

---------------------------------------------------------------------------------------------------------------------
-- PL/SQL COLLECTIONS
---------------------------------------------------------------------------------------------------------------------
TYPE TYP_tran_validation_rec IS RECORD
(
   rowid                VARCHAR2(20),
   process_id           SVC_FCUSTUPLD_THEAD.PROCESS_ID%TYPE,
   fcust_seq_no         SVC_FCUSTUPLD_THEAD.FCUST_SEQ_NO%TYPE,
   thead_count          NUMBER(10),
   tdetl_count          NUMBER(10),
   ttail_count          NUMBER(10),
   tran_rec_counter     NUMBER(10),
   thead_cust_grp_id    SVC_FCUSTUPLD_THEAD.F_CUSTOMER_GROUP_ID%TYPE,
   thead_cust_grp_name  SVC_FCUSTUPLD_THEAD.F_CUSTOMER_GROUP_NAME%TYPE,
   thead_msg_type       SVC_FCUSTUPLD_THEAD.MESSAGE_TYPE%TYPE
 );

TYPE TYP_tran_validation_tbl is TABLE of TYP_tran_validation_rec INDEX BY BINARY_INTEGER;

TYPE TYP_tran_detl_vals_rec IS RECORD
(
   process_id             SVC_FCUSTUPLD_TDETL.PROCESS_ID%TYPE,
   fcust_seq_no           SVC_FCUSTUPLD_TDETL.FCUST_SEQ_NO%TYPE,
   rowid                  VARCHAR2(20),
   tdetl_msg_type         SVC_FCUSTUPLD_TDETL.MESSAGE_TYPE%TYPE,
   thead_msg_type         SVC_FCUSTUPLD_THEAD.MESSAGE_TYPE%TYPE,
   tdetl_cust_id          SVC_FCUSTUPLD_TDETL.F_CUSTOMER_ID%TYPE,
   tdetl_cust_name        SVC_FCUSTUPLD_TDETL.F_CUSTOMER_NAME%TYPE,
   tdetl_credit_ind       SVC_FCUSTUPLD_TDETL.CREDIT_IND%TYPE,
   tdetl_auto_approve_ind SVC_FCUSTUPLD_TDETL.AUTO_APPROVE_IND%TYPE,
   wf_customer_id         WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
   store_customer_id      STORE.WF_CUSTOMER_ID%TYPE
);


TYPE TYP_tran_detl_vals_tbl is TABLE of TYP_tran_detl_vals_rec INDEX BY BINARY_INTEGER;

TYPE TYP_valid_trans_tbl is TABLE of SVC_FCUSTUPLD_THEAD.FCUST_SEQ_NO%TYPE 
INDEX BY BINARY_INTEGER;

TYPE TYP_valid_thead_rec IS RECORD
(
  rowid                    VARCHAR2(20),
  message_type             SVC_FCUSTUPLD_THEAD.MESSAGE_TYPE%TYPE,
  customer_grp_id          SVC_FCUSTUPLD_THEAD.F_CUSTOMER_GROUP_ID%TYPE,
  customer_grp_name        SVC_FCUSTUPLD_THEAD.F_CUSTOMER_GROUP_NAME%TYPE
);

TYPE TYP_valid_tdetl_rec IS RECORD
(
   rowid                    VARCHAR2(20),
   message_type             SVC_FCUSTUPLD_TDETL.MESSAGE_TYPE%TYPE,
   tdetl_customer_id        SVC_FCUSTUPLD_TDETL.F_CUSTOMER_ID%TYPE,
   tdetl_customer_name      SVC_FCUSTUPLD_TDETL.F_CUSTOMER_NAME%TYPE,
   credit_ind               SVC_FCUSTUPLD_TDETL.CREDIT_IND%TYPE,
   auto_approve_ind         SVC_FCUSTUPLD_TDETL.AUTO_APPROVE_IND%TYPE
 );

TYPE TYP_valid_tdetl_tbl is TABLE of TYP_valid_tdetl_rec INDEX BY BINARY_INTEGER;

---------------------------------------------------------------------------------------------------------------------
-- PRIVATE PROCEDURES AND FUNCTIONS
---------------------------------------------------------------------------------------------------------------------
-- Update the process/chunk status to error
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id      IN SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                      I_chunk_id        IN SVC_FCUSTUPLD_THEAD.CHUNK_ID%TYPE,
                      I_error_message   IN VARCHAR2);
----------------------------------------------------------------------------------------------
-- Write errors to the the parameter tables
-- Only captured errors of a functional or known technical reason is written
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR_PARAMS(I_error_message   IN VARCHAR2,
                             I_process_id      IN SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                             I_table_name      IN VARCHAR2 DEFAULT NULL);
----------------------------------------------------------------------------------------------
-- Write reject status to the the parameter tables
-- Only captured errors of a functional is written
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_REJECT_PARAMS(I_error_message     IN VARCHAR2,
                              I_process_id        IN SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_fcust_seq_no      IN SVC_FCUSTUPLD_FHEAD.FCUST_SEQ_NO%TYPE,
                              I_table_name        IN VARCHAR2 DEFAULT NULL,
                              I_rowid             IN VARCHAR2 DEFAULT NULL
                             ) ;
----------------------------------------------------------------------------------------------
-- Write success status to the parameter tables
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS_PARAMS(O_error_message     IN OUT NOCOPY VARCHAR2,
                              I_process_id        IN     SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_fcust_seq_no      IN     SVC_FCUSTUPLD_FHEAD.FCUST_SEQ_NO%TYPE
                             );
----------------------------------------------------------------------------------------------
-- Write success status to the status table
------------------------------------------------------------------------------------------------------------   
PROCEDURE WRITE_SUCCESS(O_error_message     IN OUT NOCOPY VARCHAR2,
                        I_process_id        IN             SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE
                       );
--------------------------------------------------------------------------------------------------------
 
FUNCTION INITIALIZE_PROCESS_STATUS(O_error_message   IN OUT NOCOPY VARCHAR2,
                                   I_process_id      IN     SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                                   I_reference_id    IN     SVC_FCUSTUPLD_STATUS.REFERENCE_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.INITIALIZE_PROCESS_STATUS';

BEGIN

   -- Validate required input
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   -- Insert initial status record with chunk 1
   insert into svc_fcustupld_status(process_id,
                                    chunk_id,
                                    reference_id,
                                    status,
                                    last_update_datetime)
                             values(I_process_id,
                                    1, --chunk_id
                                    I_reference_id,
                                    CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD, --new status
                                    sysdate);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END INITIALIZE_PROCESS_STATUS;
------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id      IN SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                      I_chunk_id        IN SVC_FCUSTUPLD_THEAD.CHUNK_ID%TYPE,
                      I_error_message   IN VARCHAR2) AS
                      
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program     VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.WRITE_ERROR';
    
   cursor C_LOCK_SVC_FCUSTUPLD_STATUS is
      select 'x'
        from svc_fcustupld_status s
       where process_id = I_process_id
         and chunk_id   = I_chunk_id
         for update of s.status, s.error_msg nowait;

BEGIN
     
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SVC_FCUSTUPLD_STATUS','SVC_FCUSTUPLD_STATUS',NULL);
   open C_LOCK_SVC_FCUSTUPLD_STATUS;
   
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SVC_FCUSTUPLD_STATUS','SVC_FCUSTUPLD_STATUS',NULL);
   close C_LOCK_SVC_FCUSTUPLD_STATUS;

   update svc_fcustupld_status
      set status     = CORESVC_FCUSTOMER_UPLOAD_SQL.ERROR,
          error_msg  = I_error_message
    where process_id = I_process_id
      and chunk_id   = I_chunk_id;

   commit;

EXCEPTION
   when OTHERS then
      if C_LOCK_SVC_FCUSTUPLD_STATUS%ISOPEN then
         close C_LOCK_SVC_FCUSTUPLD_STATUS;
      end if;
      
      rollback;
END;

------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR_PARAMS(I_error_message   IN VARCHAR2,
                             I_process_id      IN SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                             I_table_name      IN VARCHAR2 DEFAULT NULL) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program             VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.WRITE_ERROR_PARAMS';

   cursor C_LOCK_FHEAD is
      select 'x'
        from svc_fcustupld_fhead sfh
       where sfh.process_id = I_process_id
         for update of sfh.error_msg, sfh.status nowait;
        
   cursor C_LOCK_FTAIL is
      select 'x'
        from svc_fcustupld_ftail sft
       where sft.process_id = I_process_id
         for update of sft.error_msg, sft.status nowait;
         
BEGIN

   if I_table_name = CORESVC_FCUSTOMER_UPLOAD_SQL.FHEAD then
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_FHEAD','SVC_FCUSTUPLD_FHEAD',NULL);
      open C_LOCK_FHEAD;
      
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_FHEAD','SVC_FCUSTUPLD_FHEAD',NULL);
      close C_LOCK_FHEAD;
      ---
     
      update svc_fcustupld_fhead sfh
         set sfh.error_msg = I_error_message,
             sfh.status = CORESVC_FCUSTOMER_UPLOAD_SQL.ERROR,
             sfh.last_update_datetime = SYSDATE
       where sfh.process_id = I_process_id;
        
      ---
   end if;
   
  if I_table_name = CORESVC_FCUSTOMER_UPLOAD_SQL.FTAIL then
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_FTAIL','SVC_FCUSTUPLD_FTAIL',NULL);
      open C_LOCK_FTAIL;
      
      SQL_LIB.SET_MARK('OPEN','C_LOCK_FTAIL','SVC_FCUSTUPLD_FTAIL',NULL);
      close C_LOCK_FTAIL;
      ---
      
      update svc_fcustupld_ftail sft
         set sft.error_msg = I_error_message,
             sft.status = CORESVC_FCUSTOMER_UPLOAD_SQL.ERROR
       where sft.process_id = I_process_id;
        
      ---
   end if;

   commit;

EXCEPTION
   when OTHERS then
      if C_LOCK_FHEAD%ISOPEN then
         close C_LOCK_FHEAD;
      end if;
      
      if C_LOCK_FTAIL%ISOPEN then
         close C_LOCK_FTAIL;
      end if;
      
      rollback;
END;

-------------------------------------------------------------------------------------------------
PROCEDURE WRITE_REJECT_PARAMS(I_error_message     IN VARCHAR2,
                              I_process_id        IN SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_fcust_seq_no      IN SVC_FCUSTUPLD_FHEAD.FCUST_SEQ_NO%TYPE,
                              I_table_name        IN VARCHAR2 DEFAULT NULL,
                              I_rowid             IN VARCHAR2 DEFAULT NULL
                             ) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program             VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.WRITE_REJECT_PARAMS';
         
   cursor C_LOCK_THEAD is
     select 'x'
       from svc_fcustupld_thead sth
      where sth.process_id   = I_process_id
        and sth.fcust_seq_no = I_fcust_seq_no
        and sth.rowid = I_rowid
        for update of sth.error_msg, sth.status nowait;
    
     cursor C_LOCK_TDETL is
       select 'x'
         from svc_fcustupld_tdetl std
        where std.process_id   = I_process_id
          and std.fcust_seq_no = I_fcust_seq_no
          and std.rowid = I_rowid
          for update of std.error_msg, std.status nowait;
 
    cursor C_LOCK_TTAIL is
       select 'x'
         from svc_fcustupld_ttail stt
        where stt.process_id = I_process_id
          and stt.fcust_seq_no = I_fcust_seq_no
          for update of stt.error_msg, stt.status nowait;
    
  BEGIN
 
     if I_table_name = CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD then
      ---
         open C_LOCK_THEAD;
         close C_LOCK_THEAD;
      ---
                 
         update SVC_FCUSTUPLD_THEAD sth
            set sth.error_msg = sth.error_msg || I_error_message,
                sth.status = CORESVC_FCUSTOMER_UPLOAD_SQL.REJECTED
          where sth.process_id = I_process_id
            and sth.fcust_seq_no= I_fcust_seq_no
            and sth.rowid = I_rowid;
      ---
     end if;
   
     if I_table_name = CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL then
         ---
         open C_LOCK_TDETL;
         close C_LOCK_TDETL;
         ---
         
         update SVC_FCUSTUPLD_TDETL std
            set std.error_msg = std.error_msg||I_error_message,
                std.status = CORESVC_FCUSTOMER_UPLOAD_SQL.REJECTED
          where std.process_id = I_process_id
            and std.fcust_seq_no= I_fcust_seq_no
            and std.rowid = I_rowid;
         
        ---
     end if;
   
     if I_table_name = CORESVC_FCUSTOMER_UPLOAD_SQL.TTAIL then
            ---
         open C_LOCK_TTAIL;
         close C_LOCK_TTAIL;
            ---
         update SVC_FCUSTUPLD_TTAIL stt
            set stt.error_msg = stt.error_msg||I_error_message,
                stt.status = CORESVC_FCUSTOMER_UPLOAD_SQL.REJECTED
          where stt.process_id = I_process_id
            and stt.fcust_seq_no= I_fcust_seq_no;
            ---
     end if;

     commit;

EXCEPTION
   when OTHERS then
   if C_LOCK_THEAD%ISOPEN then
      close C_LOCK_THEAD;
   end if;
   
   if C_LOCK_TDETL%ISOPEN then
      close C_LOCK_TDETL;
   end if;
      
   if C_LOCK_TTAIL%ISOPEN then
       close C_LOCK_TTAIL;
   end if;
    
   rollback;
END;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PARAMETER_TABLES(O_error_message   IN OUT NOCOPY VARCHAR2,
                                   I_process_id      IN            SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

  cursor C_GET_TRAN_REC is
     select th.rowid,
            th.process_id,
            th.fcust_seq_no ,
	    count(distinct th.rowid) ,
	    count(distinct td.rowid)  ,
	    count(distinct tt.rowid) ,
	    nvl(max(tt.tran_record_counter),0) ,
	    max(th.f_customer_group_id),
	    max(th.f_customer_group_name),
	    max(th.message_type)
       from svc_fcustupld_thead th,
	    svc_fcustupld_tdetl td,
	    svc_fcustupld_ttail tt
      where th.process_id     = td.process_id (+)
        and th.process_id     = tt.process_id (+)
        and th.fcust_seq_no   = td.fcust_seq_no(+)
	and th.fcust_seq_no   = tt.fcust_seq_no(+)
	and th.process_id = I_process_id
	and nvl(th.status,'N') = CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD
	and nvl(td.status,'N') = CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD
	and nvl(tt.status,'N') = CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD
   group by th.fcust_seq_no, th.process_id,th.rowid;
     
  cursor C_GET_TRAN_TDETL_REC is
      select std.process_id,
             std.fcust_seq_no,
             std.rowid,
             std.message_type,
             sth.message_type,
             std.f_customer_id,
             std.f_customer_name,
             std.credit_ind,
             std.auto_approve_ind,
             wfc.wf_customer_id,
               s.wf_customer_id as store_customer_id
            from svc_fcustupld_tdetl std,
                 svc_fcustupld_thead sth,
                 wf_customer wfc ,
                 (select distinct wf_customer_id from store) s
           where std.f_customer_id   = wfc.wf_customer_id(+)
             and std.process_id      = sth.process_id(+)
             and std.fcust_seq_no    = sth.fcust_seq_no(+) 
             and std.f_customer_id   = s.wf_customer_id (+)
             and std.status          = CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD
             and std.process_id      = I_process_id;
        
 --Oprhans
  cursor C_GET_TDETL_ORPHANS is
     select count(1) 
       from svc_fcustupld_tdetl
      where process_id = I_process_id
        and fcust_seq_no <1;
      
  cursor C_GET_TTAIL_ORPHANS is
     select count(1) 
       from svc_fcustupld_ttail
      where process_id = I_process_id
        and fcust_seq_no <1 ;
        
  L_tran_validation_tbl     TYP_tran_validation_tbl;
  L_tran_detl_vals_tbl      TYP_tran_detl_vals_tbl;
  L_program                 VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.VALIDATE_PARAMETER_TABLES';
  L_table                   VARCHAR2(255);
  L_tdetl_orphans_cnt       NUMBER(10);
  L_ttail_orphans_cnt       NUMBER(10);

BEGIN
   O_error_message := NULL;
    
   -- check for orphan TTAIL records.
   SQL_LIB.SET_MARK('OPEN','C_GET_TTAIL_ORPHANS','SVC_FCUSTUPLD_TTAIL',NULL);
   open C_GET_TTAIL_ORPHANS;
   
   SQL_LIB.SET_MARK('FETCH','C_GET_TTAIL_ORPHANS','SVC_FCUSTUPLD_TTAIL',NULL);
   fetch C_GET_TTAIL_ORPHANS into L_ttail_orphans_cnt;
   
   SQL_LIB.SET_MARK('CLOSE','C_GET_TTAIL_ORPHANS','SVC_FCUSTUPLD_TTAIL',NULL);
   close C_GET_TTAIL_ORPHANS;
   
   O_error_message := NULL;
   
   if L_ttail_orphans_cnt > 0 then
      O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_ORPHAN_TTAIL',
         	                  			            NULL,
         	                  			            NULL,
                                                                    NULL)|| ';' ;
          update svc_fcustupld_ttail
             set status='R',
                 error_msg = O_error_message
          where process_id = I_process_id
         and fcust_seq_no < 1;
   end if;
   
   L_table  := 'SVC_FCUSTUPLD_THEAD, SVC_FCUSTUPLD_TDETL, SVC_FCUSTUPLD_TTAIL, WF_CUSTOMER_GROUP';
   
   SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_REC',L_table,NULL);
   open C_GET_TRAN_REC;
   
   SQL_LIB.SET_MARK('FETCH','C_GET_TRAN_REC',L_table,NULL);
   fetch C_GET_TRAN_REC BULK COLLECT into L_tran_validation_tbl;
   
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRAN_REC',L_table,NULL);
   close C_GET_TRAN_REC;
     
     ------------------------------------------------------------------------------------------
      --THEAD VALIDATIONS
     ------------------------------------------------------------------------------------------
    if (L_tran_validation_tbl is NOT NULL and L_tran_validation_tbl.COUNT>0) then  
     
      for i in L_tran_validation_tbl.first .. L_tran_validation_tbl.last loop
          
           
         O_error_message := NULL;
        
         --Missing THEAD
         if L_tran_validation_tbl(i).ttail_count > 1 then
            O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_TTAIL_COUNT',
	                  			           	          L_tran_validation_tbl(i).process_id,
	                  			           	          L_tran_validation_tbl(i).fcust_seq_no,
                                                                          NULL)|| ';' ;
         end if;
          
         -- Customer_group_id validation
         if L_tran_validation_tbl(i).thead_cust_grp_id is NULL then
            O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_GRP_ID_REQD',
            			           	                          NULL,
            			           	                          NULL,
                                                                          NULL)|| ';' ;
         end if;
      
         -- Customer group name validation.
         if (L_tran_validation_tbl(i).thead_msg_type !=  CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_HEAD and 
             L_tran_validation_tbl(i).thead_cust_grp_name is NULL ) then
            O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_GRP_NAME_REQD',
            			           	                          NULL,
            			           	                          NULL,
                                                                          NULL)|| ';' ;
         end if;
         
         --Customer group id must be greater than zero.
         if L_tran_validation_tbl(i).thead_cust_grp_id <=0 then
	    O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_GRP_ID_GREAT_0',
	             			           	                  NULL,
	             			           	                  NULL,
	                                                                  NULL)|| ';' ;
         end if;
      
         -- Message type validation
         if L_tran_validation_tbl(i).thead_msg_type is NULL or 
            L_tran_validation_tbl(i).thead_msg_type not in  (CORESVC_FCUSTOMER_UPLOAD_SQL.ADD_HEAD,CORESVC_FCUSTOMER_UPLOAD_SQL.UPD_HEAD,CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_HEAD) then
            O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_GRP_MSG_TYPE',
	           	                                                  NULL,
	           	                                                  NULL,
	                                                                  NULL)|| ';' ;
         end if;
    
        --
         if O_error_message is NOT NULL then
         
            WRITE_REJECT_PARAMS(O_error_message, 
                                I_process_id,
                                L_tran_validation_tbl(i).fcust_seq_no,
                                CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD,
                                L_tran_validation_tbl(i).rowid);
         end if;
        
     ------------------------------------------------------------------------------------------
      	   --           TTAIL VALIDATIONS
     -------------------------------------------------------------------------------------------
      	         
      	 O_error_message := NULL;
      	        
      	 -- TTAIL less than one validation.        
      	 if L_tran_validation_tbl(i).ttail_count < 1 then
      	    O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_MISSING_TTAIL',
      	                      	                                          L_tran_validation_tbl(i).process_id,
      	                      	                                          L_tran_validation_tbl(i).fcust_seq_no,
      	                                                                  NULL) || ';' ;
      	                                                                      
      	                                                               
      	 end if;
      	    	         
      	 --TTAIL more than one validation
      	 if L_tran_validation_tbl(i).ttail_count > 1 then
            O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_TTAIL_COUNT',
      			                      	                          L_tran_validation_tbl(i).process_id,
      			                      	                          L_tran_validation_tbl(i).fcust_seq_no,
      			                                                  NULL) || ';' ;
      			                                                                      
      					                                    
      	 end if;
      	 
      	 --Transaction record count with no.of transaction details.
      	 if NVL(L_tran_validation_tbl(i).tran_rec_counter,0) !=  L_tran_validation_tbl(i).tdetl_count then
      	    O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_TTAIL_CNT_NOMATCH',
      	   	                                                          L_tran_validation_tbl(i).process_id,
									  L_tran_validation_tbl(i).fcust_seq_no,
      			                                                  NULL) || ';' ;
      	 end if;
      	         
      	                  
         if O_error_message is NOT NULL then
                                              
             WRITE_REJECT_PARAMS(O_error_message, 
                                 I_process_id,
                                 L_tran_validation_tbl(i).fcust_seq_no,
                                 CORESVC_FCUSTOMER_UPLOAD_SQL.TTAIL);
                                        
             WRITE_REJECT_PARAMS(O_error_message, 
      		                 I_process_id,
      		                 L_tran_validation_tbl(i).fcust_seq_no,
                                 CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD,
                                 L_tran_validation_tbl(i).rowid);
         end if; 
                     
     end loop;
   end if;

 ------------------------------------------------------------------------------------------
         --TDETL VALIDATIONS
 ------------------------------------------------------------------------------------------
          
     O_error_message := NULL;  
        
     -- Check for orphan TDETL records.
         
     SQL_LIB.SET_MARK('OPEN','C_GET_TDETL_ORPHANS','SVC_FCUSTUPLD_TDETL',NULL);
     open C_GET_TDETL_ORPHANS;
     
     SQL_LIB.SET_MARK('FETCH','C_GET_TDETL_ORPHANS','SVC_FCUSTUPLD_TDETL',NULL);
     fetch C_GET_TDETL_ORPHANS into L_tdetl_orphans_cnt;
     
     SQL_LIB.SET_MARK('CLOSE','C_GET_TDETL_ORPHANS','SVC_FCUSTUPLD_TDETL',NULL);
     close C_GET_TDETL_ORPHANS;
        
     if L_tdetl_orphans_cnt > 0 then
        
        O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_ORPHAN_TDETL',
                        			           	      NULL,
           	                  			     	      NULL,
                                                                      NULL)|| ';' ;
        update svc_fcustupld_tdetl
           set status='R',
               error_msg = O_error_message
         where process_id = I_process_id
           and fcust_seq_no < 1;
              
        --no thead updates, as orphans do not have theads.
                       
     end if;
       
     
     L_table := 'SVC_FCUSTUPLD_TDETL,WF_CUSTOMER';
     
     SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_TDETL_REC',L_table,NULL);
     open C_GET_TRAN_TDETL_REC;
     
     SQL_LIB.SET_MARK('FETCH','C_GET_TRAN_TDETL_REC',L_table,NULL);
     fetch C_GET_TRAN_TDETL_REC BULK COLLECT into L_tran_detl_vals_tbl;
     
     SQL_LIB.SET_MARK('CLOSE','C_GET_TRAN_TDETL_REC',L_table,NULL);
     close C_GET_TRAN_TDETL_REC;
     
   if (L_tran_detl_vals_tbl is NOT NULL and L_tran_detl_vals_tbl.COUNT>0) then 
     for i in L_tran_detl_vals_tbl.first .. L_tran_detl_vals_tbl.last loop
     
   
           O_error_message := NULL;
           
          -- Invalid customer id
          
             if L_tran_detl_vals_tbl(i).tdetl_cust_id is NULL then
                O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_CUSTOMER_ID_REQD',
          	       	                                     NULL,
     						             NULL,
                                                             NULL);
             end if;
             
             
          -- Customer id should be greater than zero.   
             
            if L_tran_detl_vals_tbl(i).tdetl_cust_id <=0 then
	       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_CUSTOMER_GREAT_0',
	               	       	                            NULL,
	          					    NULL,
	                                                    NULL);
            end if;
             
                 
           --Invalid customer name
           
           if (L_tran_detl_vals_tbl(i).tdetl_msg_type !=CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_DETL and
               L_tran_detl_vals_tbl(i).tdetl_cust_name is NULL) then
              O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_CUSTOMER_NAME_REQD',
	        	       	                                            NULL,
	   						                    NULL,
                                                                            NULL)|| ';' ;
           end if;
                 
           --Invalid message type
           if L_tran_detl_vals_tbl(i).tdetl_msg_type is NULL or 
              L_tran_detl_vals_tbl(i).tdetl_msg_type NOT in (CORESVC_FCUSTOMER_UPLOAD_SQL.ADD_DETL,CORESVC_FCUSTOMER_UPLOAD_SQL.UPD_DETL,CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_DETL) then
              O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_MSG_TYPE',
     	       	                                                            NULL,
						                            NULL,
                                                                            NULL)|| ';' ;
           end if;
        
           --Invalid credit ind
           if (L_tran_detl_vals_tbl(i).tdetl_msg_type !=CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_DETL and
               L_tran_detl_vals_tbl(i).tdetl_credit_ind is NULL or 
               L_tran_detl_vals_tbl(i).tdetl_credit_ind NOT IN ('Y','N') )then
              O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_CREDIT_IND',
     	       	                                                            NULL,
						  		            NULL,
                                                                            NULL)|| ';' ;
           end if;  
        
           --Invalid Auto Approve Ind
           if (L_tran_detl_vals_tbl(i).tdetl_msg_type !=CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_DETL and 
               L_tran_detl_vals_tbl(i).tdetl_auto_approve_ind is NULL or 
               L_tran_detl_vals_tbl(i).tdetl_auto_approve_ind NOT IN ('Y','N') )then
	      O_error_message := O_error_message||SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_AUTO_APPR_IND',
	     	       	                                                    NULL,
							  		    NULL,
                                                                            NULL)|| ';' ;
           end if; 
                   
           --Message type mismatch for add.
           if (L_tran_detl_vals_tbl(i).thead_msg_type =CORESVC_FCUSTOMER_UPLOAD_SQL.ADD_HEAD and L_tran_detl_vals_tbl(i).tdetl_msg_type !=CORESVC_FCUSTOMER_UPLOAD_SQL.ADD_DETL ) then
              O_error_message := O_error_message || SQL_LIB.GET_MESSAGE_TEXT('FCUST_MSGTYP_MISMATCH_ADD',
	      	      		     	       	                              L_tran_detl_vals_tbl(i).process_id,
	      	      							      L_tran_detl_vals_tbl(i).fcust_seq_no,
                                                                              NULL)|| ';' ; 
           
           end if;
           
           --Message type mismatch for delete.
           if (L_tran_detl_vals_tbl(i).thead_msg_type =CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_HEAD and L_tran_detl_vals_tbl(i).tdetl_msg_type !=CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_DETL ) then
	       O_error_message := O_error_message || SQL_LIB.GET_MESSAGE_TEXT('FCUST_MSGTYP_MISMATCH_DEL',
	   	      	      		     	       	                       L_tran_detl_vals_tbl(i).process_id,
	   	      	      						       L_tran_detl_vals_tbl(i).fcust_seq_no,
	                                                                       NULL)|| ';' ; 
	              
           end if;
          
           
           --Customer cannot be deleted if it is associated to a store.
           if L_tran_detl_vals_tbl(i).tdetl_msg_type = CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_DETL and
              L_tran_detl_vals_tbl(i).store_customer_id is NOT NULL then
              
              O_error_message := O_error_message || SQL_LIB.GET_MESSAGE_TEXT('CUST_ID_NO_DEL',
		     	       	                                              L_tran_detl_vals_tbl(i).store_customer_id,
								 	      NULL,
                                                                              NULL)|| ';' ;  
              
           end if;
           
           if O_error_message is NOT NULL then
                WRITE_REJECT_PARAMS(O_error_message, 
	                            I_process_id,
	                            L_tran_detl_vals_tbl(i).fcust_seq_no,
	                            CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL,
	                            L_tran_detl_vals_tbl(i).rowid 
	                            );
           end if;
      --  end if;
        
                 
    end loop;
    
   --At the end of TDETL validations, THEAD status should be updated.
        update svc_fcustupld_thead
	   set status ='R'
	 where fcust_seq_no in ( select distinct fcust_seq_no 
	                           from svc_fcustupld_tdetl 
	                          where status='R'
	                            and process_id=I_process_id)
          and process_id=I_process_id;
          
    end if;

 return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_TRAN_REC%ISOPEN then
         close C_GET_TRAN_REC;
      end if;
      
      if C_GET_TRAN_TDETL_REC%ISOPEN then
         close C_GET_TRAN_TDETL_REC;
      end if;
      
      if C_GET_TDETL_ORPHANS%ISOPEN then
         close C_GET_TDETL_ORPHANS;
      end if;
      
      if C_GET_TTAIL_ORPHANS%ISOPEN then
         close C_GET_TTAIL_ORPHANS;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
 return false;
 END;
--------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FATAL(O_error_message   IN OUT NOCOPY VARCHAR2,
                        I_process_id      IN            SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.VALIDATE_FATAL';
   L_fhead_cnt     NUMBER(10);
   L_ftail_cnt     NUMBER(10);
   L_thead_cnt     NUMBER(10);
   L_tdetl_cnt     NUMBER(10);
   L_ttail_cnt     NUMBER(10);
   L_ftail_rec_cnt SVC_FCUSTUPLD_FTAIL.FILE_RECORD_COUNTER%TYPE;
   L_file_type     SVC_FCUSTUPLD_FHEAD.FILE_TYPE%TYPE;
   L_tran_validation_tbl     TYP_tran_validation_tbl;
   L_tran_detl_vals_tbl      TYP_tran_detl_vals_tbl;
   L_table        VARCHAR2(150);
   
        
   cursor C_GET_FILE_REC is
      select * from
         (select count(*) as fhead_cnt from svc_fcustupld_fhead where process_id=I_process_id and status =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD ) ,
         (select file_type as file_type from svc_fcustupld_fhead where process_id=I_process_id and status =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD ) ,
         (select count(*) as ftail_cnt from svc_fcustupld_ftail where process_id=I_process_id and status =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD ) ,
         (select NVL(max(file_record_counter),0) as ftail_rec_cnt from svc_fcustupld_ftail where process_id=I_process_id and status =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD ), 
         (select count(*) as thead_cnt from svc_fcustupld_thead where process_id=I_process_id and status =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD ) ,
         (select count(*) as tdetl_cnt from svc_fcustupld_tdetl where process_id=I_process_id and status =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD ) ,
         (select count(*) as ttail_cnt from svc_fcustupld_ttail where process_id=I_process_id and status =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD ) ;
               
BEGIN

   -------------------------------------------------------------------------------------
   -- VALIDATE REQUIRED INPUT
   -------------------------------------------------------------------------------------
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   -------------------------------------------------------------------------------------
   -- FCUSTUPLD_FHEAD and FCUSTUPLD_FTAIL VALIDATIONS
   -------------------------------------------------------------------------------------
    L_table := 'SVC_FCUSTUPLD_FHEAD,SVC_FCUSTUPLD_FTAIL,SVC_FCUSTUPLD_THEAD,SVC_FCUSTUPLD_TDETL,SVC_FCUSTUPLD_TTAIL';
    
    SQL_LIB.SET_MARK('OPEN','C_GET_FILE_REC',L_table,NULL);
    open C_GET_FILE_REC;
    
    SQL_LIB.SET_MARK('FETCH','C_GET_FILE_REC',L_table,NULL);
    fetch C_GET_FILE_REC into L_fhead_cnt,L_file_type,L_ftail_cnt,L_ftail_rec_cnt,L_thead_cnt,L_tdetl_cnt,L_ttail_cnt;
    
    SQL_LIB.SET_MARK('CLOSE','C_GET_FILE_REC',L_table,NULL);
    close C_GET_FILE_REC;
    
    --check if FHEAD is missing
    if L_fhead_cnt <1 then -- This error will be caught in the GET_PROCESS_ID function in ksh itself.
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_MISSING_FHEAD',
                                                    I_process_id,
                                                    NULL,
                                                    NULL);
                     
       --- Update the status of the process id/chunk to error
	   WRITE_ERROR(I_process_id,
	               1, --chunk_id
                       O_error_message);
         
       -- Update the parameter table
          WRITE_ERROR_PARAMS(O_error_message,
                             I_process_id, 
                            -- 0 ,-- fcust_seq_no for FHEAD
                             CORESVC_FCUSTOMER_UPLOAD_SQL.FHEAD);
        
       return false;
    end if;
   
    --more than 1 FHEAD check
    if L_fhead_cnt >1 then
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_FHEAD_COUNT',
                                                    I_process_id,
						    NULL,
                                                    NULL);
       -- Update the status of the process id/chunk to error
       	  WRITE_ERROR(I_process_id,
       	              1, --chunk_id
                      O_error_message);
                             
       -- Update the parameter table
	  WRITE_ERROR_PARAMS(O_error_message,
	                     I_process_id, 
	                     --0 ,-- fcust_seq_no for FHEAD
                             CORESVC_FCUSTOMER_UPLOAD_SQL.FHEAD);
        
          return false;
    end if; 
             
    -- File type Validation
    if L_file_type != 'FCUST' then
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_FILE_TYPE',
                                                    I_process_id,
						    NULL,
                                                    NULL);
       -- Update the status of the process id/chunk to error
       	  WRITE_ERROR(I_process_id,
       	              1, --chunk_id
                      O_error_message);
       -- Update the parameter table
          WRITE_ERROR_PARAMS(O_error_message,
                             I_process_id, 
                            -- 0 ,-- fcust_seq_no for FHEAD
                             CORESVC_FCUSTOMER_UPLOAD_SQL.FHEAD);
            
          return false;
    end if;
 
    -- Missing FTAIL Validation
    if L_ftail_cnt < 1 then
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_MISSING_FTAIL',
                                                    I_process_id,
						    NULL,
                                                    NULL);
       -- Update the status of the process id/chunk to error
       	  WRITE_ERROR(I_process_id,
       	              1, --chunk_id
                      O_error_message);
                             
                             
       -- Update the parameter table
          WRITE_ERROR_PARAMS(O_error_message,
                             I_process_id, 
                            -- 0 ,-- fcust_seq_no for FTAIL
                             CORESVC_FCUSTOMER_UPLOAD_SQL.FTAIL);
        
          return false;
    end if; 

    --More than FTAIL Validation.
    if L_ftail_cnt > 1 then
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_INV_FTAIL_COUNT',
                                                    I_process_id,
						    NULL,
                                                    NULL);
       -- Update the status of the process id/chunk to error
          WRITE_ERROR(I_process_id,
		      1, --chunk_id
                      O_error_message);
                             
       -- Update the parameter table
	  WRITE_ERROR_PARAMS(O_error_message,
	                     I_process_id, 
	                     --0 ,-- fcust_seq_no for FTAIL
                             CORESVC_FCUSTOMER_UPLOAD_SQL.FTAIL);
          return false;
    end if; 
  
    --Check if there is no THEAD, but TDETL or TTAIL is present
    if L_thead_cnt = 0 and (L_tdetl_cnt!=0 or L_ttail_cnt!=0) then
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_NO_THEAD_IN_FILE',
                                                    I_process_id,
						    NULL,
                                                    NULL);
       -- Update the status of the process id/chunk to error
	  WRITE_ERROR(I_process_id,
	              1, --chunk_id
                      O_error_message);
                                     
       -- Update the parameter table
	  WRITE_ERROR_PARAMS(O_error_message,
	                     I_process_id, 
	                    -- 0 ,-- fcust_seq_no for FHEAD
                             CORESVC_FCUSTOMER_UPLOAD_SQL.FHEAD);
              
          return false;
    end if; 
  
    -- Check if FTAIL record counter is matching the no. of records in the file.
    if L_ftail_rec_cnt != (L_thead_cnt+L_tdetl_cnt+L_ttail_cnt) then
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_FTAIL_CNT_NOMATCH',
	                                            I_process_id,
						    NULL,
                                                    NULL);
	                                                  
       -- Update the status of the process id/chunk to error
	  WRITE_ERROR(I_process_id,
	              1, --chunk_id
                      O_error_message);
       -- Update the parameter table
          WRITE_ERROR_PARAMS(O_error_message,
                             I_process_id, 
	                    -- 0 ,-- fcust_seq_no for FTAIL
                             CORESVC_FCUSTOMER_UPLOAD_SQL.FTAIL);
	  return false;
    end if; 

return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_FILE_REC%ISOPEN then
         close C_GET_FILE_REC;
      end if;
     
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_FATAL;
---------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TRANSACTION(O_error_message   IN OUT NOCOPY VARCHAR2,
                         I_process_id      IN            SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

cursor C_GET_VALID_TRANS is
   select distinct sth.fcust_seq_no
     from svc_fcustupld_thead sth
    where sth.process_id= I_process_id
      and sth.status    = CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD
    order by sth.fcust_seq_no;

L_program             VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.GET_TRANSACTION';
L_valid_trans         TYP_valid_trans_tbl;
L_table_name          VARCHAR2(50);
L_error_message       VARCHAR2(4000);
L_customer_group_id   WF_CUSTOMER.WF_CUSTOMER_GROUP_ID%TYPE;
L_customer_id         WF_CUSTOMER.WF_CUSTOMER_ID%TYPE;
L_rowid               VARCHAR2(20);
      
BEGIN
   
   SQL_LIB.SET_MARK('OPEN','C_GET_VALID_TRANS','SVC_FCUSTUPLD_THEAD',NULL);
   open C_GET_VALID_TRANS;
   
   SQL_LIB.SET_MARK('FETCH','C_GET_VALID_TRANS','SVC_FCUSTUPLD_THEAD',NULL);
   fetch C_GET_VALID_TRANS BULK COLLECT into L_valid_trans;
   
   SQL_LIB.SET_MARK('CLOSE','C_GET_VALID_TRANS','SVC_FCUSTUPLD_THEAD',NULL);
   close C_GET_VALID_TRANS;

   if (L_valid_trans is NOT NULL and L_valid_trans.COUNT>0) then

      for i in L_valid_trans.first .. L_valid_trans.last loop

         SAVEPOINT start_tran_process;
         
         L_table_name        := NULL;
         L_error_message     := NULL;
         L_rowid             := NULL;
         L_customer_group_id := NULL;
         L_customer_id       := NULL;
         
         if process_transaction(L_error_message ,                   --output
                                L_rowid  ,                          --output
                                L_table_name,                       --output
                                L_valid_trans(i),                   --input
                                I_process_id                        --input
                                ) = false then
        
             ROLLBACK TO start_tran_process;
                        
            if L_error_message is NOT NULL then
                  
               if L_table_name = CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD then
               
                                    
                  WRITE_REJECT_PARAMS(L_error_message, 
		        	      I_process_id,
		                      L_valid_trans(i),
                                      CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD,
                                      L_rowid
                                      );
                  
               elsif L_table_name = CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL then
                                            
                  WRITE_REJECT_PARAMS(L_error_message, 
		  	              I_process_id,
		  	              L_valid_trans(i),
		  	              CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL,
		  	              L_rowid
		  	              ); 
	                            
	          update svc_fcustupld_thead
		     set status ='R'
		   where fcust_seq_no in (select distinct fcust_seq_no 
		                            from svc_fcustupld_tdetl 
		                           where status='R'
		                             and process_id=I_process_id)
                     and process_id=I_process_id;
               end if;
            end if;
         end if;
         
      --write the success for the transaction if everything is fine.
           
           L_error_message := NULL;
                      
           WRITE_SUCCESS_PARAMS(L_error_message,
                                I_process_id,
                                L_valid_trans(i));
             
      end loop;
      
      -- write sucess for the entire process id.
           L_error_message := NULL;
           
           WRITE_SUCCESS(L_error_message,
                         I_process_id);
   end if;
        
return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_VALID_TRANS%ISOPEN then
         close C_GET_VALID_TRANS;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TRANSACTION;
----------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TRANSACTION (O_error_message     OUT VARCHAR2,
                              O_rowid             OUT VARCHAR2,
                              O_table_name        OUT VARCHAR2,
                              I_fcust_seq_no      IN  SVC_FCUSTUPLD_FHEAD.FCUST_SEQ_NO%TYPE,
                              I_process_id        IN  SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

cursor C_VALID_THEAD is
   select sth.rowid,
          sth.message_type,
          sth.f_customer_group_id, 
          sth.f_customer_group_name
    from  svc_fcustupld_thead sth
   where  sth.process_id   =I_process_id
     and  sth.fcust_seq_no = I_fcust_seq_no
     and  sth.status       =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD;

cursor C_VALID_TDETL is
   select std.rowid,
          std.message_type,
          std.f_customer_id,
          std.f_customer_name,
          std.credit_ind,
          std.auto_approve_ind
    from  svc_fcustupld_tdetl std
   where std.process_id   =I_process_id
     and  std.fcust_seq_no =I_fcust_seq_no
     and  std.status       =CORESVC_FCUSTOMER_UPLOAD_SQL.NEW_RECORD;
 
 L_program  VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.PROCESS_TRANSACTION';
 L_valid_detls   TYP_valid_tdetl_tbl ;
 L_valid_thead   TYP_valid_thead_rec; 
 L_error_message     VARCHAR2(4000);
 
 CHILD_EXISTS EXCEPTION;
 PRAGMA EXCEPTION_INIT(CHILD_EXISTS, -2292);

BEGIN
   -- Header insert and updates
   SQL_LIB.SET_MARK('OPEN','C_VALID_THEAD','SVC_FCUSTUPLD_THEAD',NULL);
   open C_VALID_THEAD;
   
   SQL_LIB.SET_MARK('FETCH','C_VALID_THEAD','SVC_FCUSTUPLD_THEAD',NULL);
   fetch C_VALID_THEAD into L_valid_thead;
   
   SQL_LIB.SET_MARK('CLOSE','C_VALID_THEAD','SVC_FCUSTUPLD_THEAD',NULL);
   close C_VALID_THEAD;
   
   O_error_message:= NULL;
   
   -- Populate retry parameters
      select retry_lock_attempts,
             retry_wait_time
        into LP_retry_lock_attempts,
             LP_retry_wait_time
        from rms_plsql_batch_config
       where program_name = 'CORESVC_FCUSTOMER_UPLOAD_SQL'
         and rownum = 1;
   
   if L_valid_thead.message_type =CORESVC_FCUSTOMER_UPLOAD_SQL.ADD_HEAD then
      
         BEGIN
            insert into wf_customer_group(wf_customer_group_id,
                                          wf_customer_group_name)
                                   values(L_valid_thead.customer_grp_id,
                                          L_valid_thead.customer_grp_name
                                          );
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_rowid := L_valid_thead.rowid;
               O_table_name := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
               
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CUST_GROUP_ID_EXISTS',
	       	      		     	       	            NULL,
	       	      				            NULL,
                                                            NULL); 
                                                            
               return FALSE;
            when OTHERS then
               O_rowid := L_valid_thead.rowid;
               O_table_name := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
	       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
	       	                                      SQLERRM,
	    	                                      L_program,
	    	                                      to_char(SQLCODE));
               return FALSE;
          END;
   
   elsif L_valid_thead.message_type =CORESVC_FCUSTOMER_UPLOAD_SQL.UPD_HEAD then
      
      if LOCK_WF_CUSTOMER_GROUP_TABLE(L_error_message,               
	  	     	       	      L_valid_thead.customer_grp_id) = false then
	  	     	       	      
         if L_error_message is NOT NULL then
	    O_error_message := L_error_message;
	    O_rowid         := L_valid_thead.rowid;
            O_table_name    := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
	 end if;
                
         return false;
                                 
      end if;
   
         update wf_customer_group 
            set wf_customer_group_name = L_valid_thead.customer_grp_name
          where wf_customer_group_id   = L_valid_thead.customer_grp_id;
       
          if SQL%NOTFOUND then
             O_rowid             := L_valid_thead.rowid;
             O_table_name        := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
             
             O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_NO_REC_TO_UPD',
	         	     	      		         'WF_CUSTOMER_GROUP',
	     		     	      		          NULL,
                                                          NULL); 
                                                           
             return FALSE;  
          end if;
       end if;
 
   --insert, update and delete of the detail records.
   SQL_LIB.SET_MARK('OPEN','C_VALID_TDETL','SVC_FCUSTUPLD_TDETL',NULL);
   open C_VALID_TDETL;
   
   SQL_LIB.SET_MARK('FETCH','C_VALID_TDETL','SVC_FCUSTUPLD_TDETL',NULL);
   fetch C_VALID_TDETL BULK COLLECT into L_valid_detls;
   
   SQL_LIB.SET_MARK('CLOSE','C_VALID_TDETL','SVC_FCUSTUPLD_TDETL',NULL);
   close C_VALID_TDETL;
   
   if (L_valid_detls is NOT NULL and L_valid_detls.COUNT>0) then
   
      for i in L_valid_detls.first .. L_valid_detls.last loop
     
         if  L_valid_detls(i).message_type =CORESVC_FCUSTOMER_UPLOAD_SQL.ADD_DETL then               
      
            BEGIN
               insert into wf_customer(wf_customer_id,
	                               wf_customer_name,
	                               credit_ind,
	                               auto_approve_ind,
	                               wf_customer_group_id
	                               )
	                       values(L_valid_detls(i).tdetl_customer_id,
                                      L_valid_detls(i).tdetl_customer_name,
                                      L_valid_detls(i).credit_ind,
                                      L_valid_detls(i).auto_approve_ind,
                                      L_valid_thead.customer_grp_id
                                     );
            EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CUST_ID_EXISTS',
	       		     	      		     	    L_valid_detls(i).tdetl_customer_id,
	       		     	      			    NULL,
                                                            NULL); 
               
	       O_rowid             := L_valid_detls(i).rowid;
	       O_table_name        := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
               return FALSE;
            when OTHERS then
               O_rowid             := L_valid_detls(i).rowid;
	       O_table_name        := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
	       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
	                                              SQLERRM,
	                                              L_program,
	                                              to_char(SQLCODE));
               return FALSE;
            END;
         elsif L_valid_detls(i).message_type =CORESVC_FCUSTOMER_UPLOAD_SQL.UPD_DETL then
         
              if LOCK_WF_CUSTOMER_TABLE(L_error_message,                    --output                   
	     	       	                L_valid_detls(i).tdetl_customer_id  --input
	       	                        ) = false then
	       	                              
	         if L_error_message is NOT NULL then
	            O_error_message := L_error_message;
	            O_rowid         := L_valid_detls(i).rowid;
		    O_table_name    := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
	         end if;
	          
	          return FALSE;
	       
             end if;
        
            update wf_customer 
               set wf_customer_name     = L_valid_detls(i).tdetl_customer_name,
                   credit_ind           = L_valid_detls(i).credit_ind,
                   auto_approve_ind     = L_valid_detls(i).auto_approve_ind
             where wf_customer_id       = L_valid_detls(i).tdetl_customer_id
               and wf_customer_group_id = L_valid_thead.customer_grp_id; 
               
             if SQL%NOTFOUND then
                O_table_name := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
		O_rowid      := L_valid_detls(i).rowid;
		
		O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_NO_REC_TO_UPD',
				   		            'WF_CUSTOMER',
				   		     	     NULL,
                                                             NULL); 
	        return FALSE;
             end if;
               
         elsif L_valid_detls(i).message_type =CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_DETL then
            if LOCK_WF_CUSTOMER_TABLE_TL(L_error_message,                    --output
                                         L_valid_detls(i).tdetl_customer_id  --input
                                         ) = false then
                              
               if L_error_message is NOT NULL then
                  O_error_message := L_error_message;
                  O_rowid         := L_valid_detls(i).rowid;
                  O_table_name    := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
               end if;
               
               return FALSE;
            end if;
         
            if LOCK_WF_CUSTOMER_TABLE(L_error_message,                    --output                   
	   	     	       	      L_valid_detls(i).tdetl_customer_id  --input
	       	                     ) = false then
	    	          
	          if L_error_message is NOT NULL then
	            O_error_message := L_error_message;
	            O_rowid         := L_valid_detls(i).rowid;
		    O_table_name    := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
	          end if;
			        
	         return FALSE;
	       	                                      
             end if;
         
               BEGIN
                  delete from wf_customer_tl
                  where wf_customer_id in  (select wf_customer_id
                                              from wf_customer
                                             where wf_customer_id       = L_valid_detls(i).tdetl_customer_id
                                               and wf_customer_group_id = L_valid_thead.customer_grp_id);
                    
                  delete from wf_customer
                  where wf_customer_id       = L_valid_detls(i).tdetl_customer_id
                    and wf_customer_group_id = L_valid_thead.customer_grp_id;
                    
                  if SQL%NOTFOUND then
                      O_table_name  := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
	              O_rowid := L_valid_detls(i).rowid;
	              
	              O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_NO_REC_TO_DEL',
		      		  		     		   'WF_CUSTOMER',
		      		  		     		    NULL,
                                                                    NULL); 
                                                                    
	              return FALSE;  
	          end if;
               EXCEPTION
               when child_exists then
                   O_rowid :=  L_valid_detls(i).rowid;
	           O_table_name := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
	           
	           O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CUST_ID_NO_DEL',
		   		     		     	      	L_valid_detls(i).tdetl_customer_id,
		   		     		     	      	NULL,
                                                                NULL);
	           return FALSE;
	       when OTHERS then
	          O_rowid :=  L_valid_detls(i).rowid;
	          O_table_name := CORESVC_FCUSTOMER_UPLOAD_SQL.TDETL;
	          O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
	                                                 SQLERRM,
	                                                 L_program,
	                                                 to_char(SQLCODE));
                  return FALSE;
               END;
         end if;
      end loop;
   end if;
   
    -- deletion of master records
        
   if L_valid_thead.message_type =CORESVC_FCUSTOMER_UPLOAD_SQL.DEL_HEAD then
      BEGIN
         if LOCK_WF_CUSTOMER_GRP_TABLE_TL(L_error_message,               --output
                                         L_valid_thead.customer_grp_id  --input
                                         ) = false then
        
                     if L_error_message is NOT NULL then
               O_error_message := L_error_message;
                    O_rowid         := L_valid_thead.rowid;
                    O_table_name    := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
                end if;
                
                 return FALSE;
                                          
          end if;
      
         if LOCK_WF_CUSTOMER_GROUP_TABLE(L_error_message,               --output
	                                 L_valid_thead.customer_grp_id  --input
	                                 ) = false then
	
                if L_error_message is NOT NULL then
	  	   O_error_message := L_error_message;
	       	   O_rowid         := L_valid_thead.rowid;
	       	   O_table_name    := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
	        end if;
	        
	         return FALSE;
	                                  
          end if;
          delete from wf_customer_group_tl 
           where wf_customer_group_id = L_valid_thead.customer_grp_id;
          
         delete from wf_customer_group 
          where wf_customer_group_id = L_valid_thead.customer_grp_id;
         
         if SQL%NOTFOUND then
            O_rowid             := L_valid_thead.rowid;
	    O_table_name        := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
	    	    
	    O_error_message := SQL_LIB.GET_MESSAGE_TEXT('FCUST_NO_REC_TO_DEL',
	    		     		     	      	'WF_CUSTOMER_GROUP',
	    		     		     	      	 NULL,
                                                         NULL); 
                                                         
	    return FALSE;   
         end if;
      EXCEPTION
         when child_exists then
            O_rowid := L_valid_thead.rowid;
	    O_table_name        := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
	    
	    O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_CUST_GROUP_NO_DEL',
	    		     	      		     	 L_valid_thead.customer_grp_id,
	    		     	      			 NULL,
                                                         NULL); 
	    return FALSE;
	 when OTHERS then
	    O_rowid := L_valid_thead.rowid;
	    O_table_name        := CORESVC_FCUSTOMER_UPLOAD_SQL.THEAD;
	    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
	                                           SQLERRM,
	                                           L_program,
	                                           to_char(SQLCODE));
            return FALSE;
      END;
      
   end if;
   
return TRUE;

EXCEPTION
   when OTHERS then
      if C_VALID_THEAD%ISOPEN then
         close C_VALID_THEAD;
      end if;
      
      if C_VALID_TDETL%ISOPEN then
         close C_VALID_TDETL;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_TRANSACTION;
----------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_WF_CUSTOMER_GRP_TABLE_TL(O_error_message     IN OUT NOCOPY VARCHAR2,
                                       I_customer_group_id IN     SVC_FCUSTUPLD_THEAD.F_CUSTOMER_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(100) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.LOCK_WF_CUSTOMER_GRP_TL_TABLE';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_WF_CUSTOMER_GROUP_TL is
      select 'X'
        from wf_customer_group_tl
       where wf_customer_group_id = I_customer_group_id
         for update  nowait;
    
 BEGIN
     -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
      for i in 1..(LP_retry_lock_attempts - 1) loop
      ---
         BEGIN
            open C_LOCK_WF_CUSTOMER_GROUP_TL;
            close C_LOCK_WF_CUSTOMER_GROUP_TL;
            L_locked_acquired := 'Y';
            exit;
         EXCEPTION
            when RECORD_LOCKED then
            NULL;
         END;
         -- Sleep before the next retry
         DBMS_LOCK.SLEEP(LP_retry_wait_time);
      ---
      end loop;

     -- Retry the lock for the last time if previous attempts failed
      if L_locked_acquired != 'Y' then
         open C_LOCK_WF_CUSTOMER_GROUP_TL;
         close C_LOCK_WF_CUSTOMER_GROUP_TL;
      end if;
      
     return TRUE;

EXCEPTION
   when RECORD_LOCKED then     
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_CUSTOMER_GRP_REC_LOCK',
                                  'WF_CUSTOMER_GROUP_TL',
                                   I_customer_group_id,
                                                   NULL); 
      
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_WF_CUSTOMER_GRP_TABLE_TL;
--------------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_WF_CUSTOMER_GROUP_TABLE(O_error_message     IN OUT NOCOPY VARCHAR2,
                                      I_customer_group_id IN     SVC_FCUSTUPLD_THEAD.F_CUSTOMER_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.LOCK_WF_CUSTOMER_GROUP_TABLE';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_WF_CUSTOMER_GROUP is
      select 'X'
        from wf_customer_group
       where wf_customer_group_id = I_customer_group_id
         for update  nowait;
    
 BEGIN
     -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
      for i in 1..(LP_retry_lock_attempts - 1) loop
      ---
         BEGIN
            open C_LOCK_WF_CUSTOMER_GROUP;
            close C_LOCK_WF_CUSTOMER_GROUP;
            L_locked_acquired := 'Y';
            exit;
         EXCEPTION
            when RECORD_LOCKED then
            NULL;
         END;
         -- Sleep before the next retry
         DBMS_LOCK.SLEEP(LP_retry_wait_time);
      ---
      end loop;

     -- Retry the lock for the last time if previous attempts failed
      if L_locked_acquired != 'Y' then
         open C_LOCK_WF_CUSTOMER_GROUP;
         close C_LOCK_WF_CUSTOMER_GROUP;
      end if;
      
     return TRUE;

EXCEPTION
   when RECORD_LOCKED then     
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_CUSTOMER_GRP_REC_LOCK',
      		   		     		  'WF_CUSTOMER_GROUP',
      		   		     		   I_customer_group_id,
                                                   NULL); 
      
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_WF_CUSTOMER_GROUP_TABLE;
--------------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_WF_CUSTOMER_TABLE_TL(O_error_message     IN OUT NOCOPY VARCHAR2,
                                   I_customer_id       IN     SVC_FCUSTUPLD_TDETL.FCUST_SEQ_NO%TYPE
                                   )
RETURN BOOLEAN IS

   L_program           VARCHAR2(100) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.LOCK_WF_CUSTOMER_TL_TABLES';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

  cursor C_LOCK_WF_CUSTOMER_TL is
     select 'X' 
       from wf_customer_tl
      where wf_customer_id = I_customer_id
      for update  nowait;
    
 BEGIN
     -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
      FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
      ---
         BEGIN
            open C_LOCK_WF_CUSTOMER_TL;
            close C_LOCK_WF_CUSTOMER_TL;
            L_locked_acquired := 'Y';
            exit;
         EXCEPTION
           when RECORD_LOCKED then
           NULL;
         END;
         -- Sleep before the next retry
         DBMS_LOCK.SLEEP(LP_retry_wait_time);
      ---
      END LOOP;

      -- Retry the lock for the last time if previous attempts failed
      if L_locked_acquired != 'Y' then
         open C_LOCK_WF_CUSTOMER_TL;
         close C_LOCK_WF_CUSTOMER_TL;
      end if;  
      

  return TRUE;

EXCEPTION
   when RECORD_LOCKED then           
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_CUSTOMER_REC_LOCK',
                                                  'WF_CUSTOMER_TL',
                                                  I_customer_id,
                                                  NULL); 
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_WF_CUSTOMER_TABLE_TL;
--------------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_WF_CUSTOMER_TABLE(O_error_message     IN OUT NOCOPY VARCHAR2,
                                I_customer_id       IN     SVC_FCUSTUPLD_TDETL.FCUST_SEQ_NO%TYPE
                                )
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.LOCK_WF_CUSTOMER_TABLES';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

  cursor C_LOCK_WF_CUSTOMER is
     select 'X' 
       from wf_customer
      where wf_customer_id = I_customer_id
      for update  nowait;
    
 BEGIN
     -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
      FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
      ---
         BEGIN
            open C_LOCK_WF_CUSTOMER;
            close C_LOCK_WF_CUSTOMER;
            L_locked_acquired := 'Y';
            exit;
         EXCEPTION
           when RECORD_LOCKED then
           NULL;
         END;
         -- Sleep before the next retry
         DBMS_LOCK.SLEEP(LP_retry_wait_time);
      ---
      END LOOP;

      -- Retry the lock for the last time if previous attempts failed
      if L_locked_acquired != 'Y' then
         open C_LOCK_WF_CUSTOMER;
         close C_LOCK_WF_CUSTOMER;
      end if;  
      

  return TRUE;

EXCEPTION
   when RECORD_LOCKED then           
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_CUSTOMER_REC_LOCK',
      		  		  		  'WF_CUSTOMER',
      		  		  		   I_customer_id,
                                                   NULL); 
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_WF_CUSTOMER_TABLE;
--------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS(O_error_message     IN OUT NOCOPY VARCHAR2,
                       I_process_id         IN             SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE
                       ) AS
                       
PRAGMA AUTONOMOUS_TRANSACTION;

  cursor C_WRITE_SUCCESS is
     select * from 
             (select 'X' from svc_fcustupld_thead where process_id = I_process_id and status='R'
              UNION
              select 'X' from svc_fcustupld_tdetl where process_id = I_process_id and status='R'
              UNION
              select 'X' from svc_fcustupld_ttail where process_id = I_process_id and status='R'
              UNION
              select 'X' from svc_fcustupld_fhead where process_id = I_process_id and status='E'
              UNION
              select 'X' from svc_fcustupld_ftail where process_id = I_process_id and status='E');
  
  L_status VARCHAR2(1);
  
  L_program VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.WRITE_SUCCESS';
  
  L_table VARCHAR2(255); 
              
  BEGIN
   L_table  := 'SVC_FCUSTUPLD_THEAD,SVC_FCUSTUPLD_TDETL,SVC_FCUSTUPLD_TTAIL,SVC_FCUSTUPLD_FHEAD,SVC_FCUSTUPLD_FTAIL';
   
   SQL_LIB.SET_MARK('OPEN','C_WRITE_SUCCESS',L_table,NULL);
   open C_WRITE_SUCCESS;
   
   SQL_LIB.SET_MARK('FETCH','C_WRITE_SUCCESS',L_table,NULL);
   fetch C_WRITE_SUCCESS into L_status;
   
   SQL_LIB.SET_MARK('CLOSE','C_WRITE_SUCCESS',L_table,NULL);
   close C_WRITE_SUCCESS;
   
   if L_status is NULL then
   
      update svc_fcustupld_fhead 
      set status =  CORESVC_FCUSTOMER_UPLOAD_SQL.PROCESSED
      where process_id = I_process_id;
      
        update svc_fcustupld_ftail 
            set status =  CORESVC_FCUSTOMER_UPLOAD_SQL.PROCESSED
          where process_id = I_process_id;
      
      
       update svc_fcustupld_status 
          set status =  CORESVC_FCUSTOMER_UPLOAD_SQL.PROCESSED
        where process_id = I_process_id;
      
   end if;
   
   commit;
   
EXCEPTION
   when OTHERS then
   rollback;
   
END WRITE_SUCCESS;
-------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS_PARAMS(O_error_message     IN OUT NOCOPY VARCHAR2,
                              I_process_id        IN     SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_fcust_seq_no      IN     SVC_FCUSTUPLD_FHEAD.FCUST_SEQ_NO%TYPE
                             ) AS

PRAGMA AUTONOMOUS_TRANSACTION;

cursor C_LOCK_THEAD is
   select 'X' from svc_fcustupld_thead
    where process_id = I_process_id
      and fcust_seq_no = I_fcust_seq_no
      for update of status;

cursor C_LOCK_TDETL is 
   select 'X' from svc_fcustupld_tdetl
    where process_id = I_process_id
      and fcust_seq_no = I_fcust_seq_no
      for update of status;
      
  
cursor C_LOCK_TTAIL is
   select 'X' from svc_fcustupld_ttail
    where  process_id = I_process_id
      and fcust_seq_no = I_fcust_seq_no
      for update of status;
      

cursor C_STATUS is
    select status from svc_fcustupld_thead
     where process_id = I_process_id
       and fcust_seq_no = I_fcust_seq_no;

L_status  VARCHAR2(1);

L_program VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.WRITE_SUCCESS_PARAMS';
   
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_STATUS','SVC_FCUSTUPLD_THEAD',NULL);
   open C_STATUS;
   
   SQL_LIB.SET_MARK('FETCH','C_STATUS','SVC_FCUSTUPLD_THEAD',NULL);
   fetch C_STATUS into L_status;
   
   SQL_LIB.SET_MARK('CLOSE','C_STATUS','SVC_FCUSTUPLD_THEAD',NULL);
   close C_STATUS;

   if L_status is NOT NULL and L_status = 'N' then

      open C_LOCK_THEAD;
      close C_LOCK_THEAD;
      
      update svc_fcustupld_thead 
         set status =  CORESVC_FCUSTOMER_UPLOAD_SQL.PROCESSED
       where process_id = I_process_id
         and fcust_seq_no = I_fcust_seq_no;
           
      open C_LOCK_TDETL;
      close C_LOCK_TDETL;
      
      update svc_fcustupld_tdetl 
          set status =  CORESVC_FCUSTOMER_UPLOAD_SQL.PROCESSED
        where process_id = I_process_id
          and fcust_seq_no = I_fcust_seq_no;
      
      open C_LOCK_TTAIL;
      close C_LOCK_TTAIL;
      
      update svc_fcustupld_ttail 
          set status =  CORESVC_FCUSTOMER_UPLOAD_SQL.PROCESSED
        where process_id = I_process_id
          and fcust_seq_no = I_fcust_seq_no;
   end if;
 commit;

EXCEPTION
  when OTHERS then
  rollback;
  
END WRITE_SUCCESS_PARAMS;
-------------------------------------------------------------------------------------------------------
FUNCTION PURGE_FCUSTUPLD_TABLES(O_error_message IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS 

cursor C_RETENTION_DAYS is
   select fdn_stg_retention_days
     from system_options;

L_program VARCHAR2(64) := 'CORESVC_FCUSTOMER_UPLOAD_SQL.PURGE_FCUSTUPLD_TABLES';
L_retention_days number(3);
     

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_RETENTION_DAYS','SYSTEM_OPTIONS',NULL);
   open C_RETENTION_DAYS;
   
   SQL_LIB.SET_MARK('FETCH','C_RETENTION_DAYS','SYSTEM_OPTIONS',NULL);
   fetch C_RETENTION_DAYS into L_retention_days;
   
   SQL_LIB.SET_MARK('CLOSE','C_RETENTION_DAYS','SYSTEM_OPTIONS',NULL);
   close C_RETENTION_DAYS;
   

   delete from svc_fcustupld_ftail 
    where process_id in (select distinct process_id 
                           from svc_fcustupld_fhead
                          where last_update_datetime < get_vdate - L_retention_days);
         
   delete from svc_fcustupld_thead 
    where process_id in (select distinct process_id 
                           from svc_fcustupld_fhead
                          where last_update_datetime < get_vdate - L_retention_days);
                                                                
   delete from svc_fcustupld_tdetl 
    where process_id in (select distinct process_id 
                           from svc_fcustupld_fhead
                          where last_update_datetime < get_vdate - L_retention_days);
                                                                
   delete from svc_fcustupld_ttail 
    where process_id in (select distinct process_id 
                           from svc_fcustupld_fhead
                          where last_update_datetime < get_vdate - L_retention_days);
                                                                
   delete from svc_fcustupld_status 
    where process_id in (select distinct process_id 
                           from svc_fcustupld_fhead
                          where last_update_datetime < get_vdate - L_retention_days);
         
   delete from svc_fcustupld_fhead 
    where last_update_datetime < get_vdate - L_retention_days;
         
 return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_FCUSTUPLD_TABLES;

--------------------------------------------------------------------------------------------------------------------------
END CORESVC_FCUSTOMER_UPLOAD_SQL;
/
