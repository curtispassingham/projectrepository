
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEPT_VALIDATE_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------
-- Name:    EXIST
-- Purpose: Determines if the given department exists.
-- Created By: Darcie Miller, 26-JUL-96.
---------------------------------------------------------------
   FUNCTION EXIST(O_error_message	IN OUT VARCHAR2,
                  I_dept		IN     NUMBER,
		  O_exist		IN OUT BOOLEAN)
            return BOOLEAN;
---------------------------------------------------------------
--Name:       DEPT_IN_HIER
--Purpose:    To check the existence of a department for a given
--            group/division combination.
--Created by: Erica Oesting, 23-April-98.
---------------------------------------------------------------
FUNCTION DEPT_IN_HIER (O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_group         IN     deps.group_no%TYPE,
                       I_division      IN     groups.division%TYPE,
                       I_dept          IN     deps.dept%TYPE)
                          RETURN BOOLEAN;
---------------------------------------------------------------

END DEPT_VALIDATE_SQL;
/


