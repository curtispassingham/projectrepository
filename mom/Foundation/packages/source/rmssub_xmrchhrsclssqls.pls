
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRSCLS_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_subclass_rec    IN       SUBCLASS%ROWTYPE,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRSCLS_SQL;
/

