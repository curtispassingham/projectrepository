
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE CLASS_VALIDATE_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------
-- Name:      EXIST
-- Purpose:   Determines if a given class exists within a 
--		  given department.
-- Created By: Darcie Miller, 26-JUL-96.
---------------------------------------------------------------
   FUNCTION EXIST(O_error_message IN OUT VARCHAR2,
                  I_dept          IN     NUMBER,
                  I_class         IN     NUMBER,
		  O_exist	  IN OUT BOOLEAN)
            return BOOLEAN;

END CLASS_VALIDATE_SQL;
/


