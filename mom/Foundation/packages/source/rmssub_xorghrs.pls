
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XORGHR AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
--PUBLIC VARIABLES
----------------------------------------------------------------------------
   LP_cre_type         VARCHAR2(15) := 'xorghrcre';
   LP_mod_type         VARCHAR2(15) := 'xorghrmod';
   LP_del_type         VARCHAR2(15) := 'xorghrdel';
   
   LP_loctrt_cre_type  VARCHAR2(15) := 'xorghrloctrtcre';
   LP_loctrt_del_type  VARCHAR2(15) := 'xorghrloctrtdel';
   
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code   IN OUT   VARCHAR2,
                  O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN       RIB_OBJECT,
                  I_message_type  IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_XORGHR;
/