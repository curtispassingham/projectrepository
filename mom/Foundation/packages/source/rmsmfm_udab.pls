
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSMFM_UDA AS

PROCEDURE ADDTOQ(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 I_message_type IN  UDA_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_uda_id       IN  UDA.UDA_ID%TYPE,
                 I_uda_value    IN  UDA_VALUES.UDA_VALUE%TYPE,
                 I_display_type IN  UDA_MFQUEUE.DISPLAY_TYPE%TYPE,
                 I_message      IN  CLOB)
IS

   /* This procedure takes a message in CLOB format and adds it to the
      message queue table.
   */

BEGIN
   insert into uda_mfqueue(seq_no,
                           pub_status,
                           message_type,
                           uda_id,
                           uda_value,
                           display_type,
                           message)
                     values(uda_mfsequence.NEXTVAL,
                            'U',
                            I_message_type,
                            I_uda_id,
                            I_uda_value,
                            I_display_type,
                            I_message);

    O_status := API_CODES.SUCCESS;


EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_UDA.ADDTOQ');

END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code  OUT VARCHAR2,
                 O_error_msg    OUT VARCHAR2,
                 O_message_type OUT UDA_MFQUEUE.MESSAGE_TYPE%TYPE,
                 O_message      OUT CLOB,
                 O_uda_id       OUT UDA.UDA_ID%TYPE,
                 O_uda_value    OUT UDA_VALUES.UDA_VALUE%TYPE,
                 O_display_type OUT UDA_MFQUEUE.DISPLAY_TYPE%TYPE)
IS

   /* This procedure fetches the row from the message queue table that has
      the lowest sequence number.  The message is retrieved, then the row
      is removed from the queue.
   */
   L_rowid  ROWID;

   cursor C_GET_MESSAGE is
      select message_type,
             uda_id,
             uda_value,
             display_type,
             message,
             rowid
        from uda_mfqueue
       where seq_no = (select min(seq_no)
                         from uda_mfqueue)
         for update nowait;

BEGIN
   open C_GET_MESSAGE;
   fetch C_GET_MESSAGE into O_message_type,
                            O_uda_id,
                            O_uda_value,
                            O_display_type,
                            O_message,
                            L_rowid;
 
   


   if C_GET_MESSAGE%NOTFOUND then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.SUCCESS;
   end if;

   close C_GET_MESSAGE;

   delete from uda_mfqueue 
    where rowid = L_rowid;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_UDA.GETNXT');

END GETNXT;
--------------------------------------------------------------------------------
END RMSMFM_UDA;
/
