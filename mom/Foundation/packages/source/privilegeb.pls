create or replace PACKAGE BODY PRIVILEGE_SQL AS

LP_session_user    VARCHAR2(30);

LP_division        DEPT_TBL;
LP_group_no        DEPT_TBL;
LP_dept            DEPT_TBL;
LP_class           DEPT_TBL;
LP_subclass        DEPT_TBL;

-- Local function
----------------------------------------------------------------------------------------------------
FUNCTION CHECK_GLOBAL_SECURITY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_global_sec_exists IN OUT BOOLEAN)

RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_USER_ACCESS (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_valid         IN OUT BOOLEAN,
                          I_privilege     IN     VARCHAR2)
   RETURN BOOLEAN AS

   L_program   VARCHAR2(64) := 'PRIVILEGE_SQL.GET_ACCESS';
   L_dummy     VARCHAR2(1)  := 'N';

   cursor C_ACCESS is
      select 'X'
        from code_detail cd,
             sec_user_group sug,
             sec_user su
       where cd.code_type = I_privilege
         and cd.code = TO_CHAR(sug.group_id)
         and sug.user_seq = su.user_seq
         and su.database_user_id = SYS_CONTEXT('USERENV', 'SESSION_USER');

   cursor C_FILTER_USER is
      select 'Y'
        from filter_group_org f, sec_user_group sug, sec_user su
       where sug.group_id = f.sec_group_id
         and sug.user_seq = su.user_seq
         and su.database_user_id = SYS_CONTEXT('USERENV', 'SESSION_USER')
         and rownum = 1
      union
       select 'Y'
        from sec_group_loc_matrix sglm, sec_user_group sug, sec_user su
       where sug.group_id = sglm.group_id
         and sug.user_seq = su.user_seq
         and su.database_user_id = SYS_CONTEXT('USERENV', 'SESSION_USER')
         and rownum = 1         
      union
      select 'Y'
        from filter_group_merch f, sec_user_group sug, sec_user su
       where sug.group_id = f.sec_group_id
         and sug.user_seq = su.user_seq
         and su.database_user_id = SYS_CONTEXT('USERENV', 'SESSION_USER')
         and rownum = 1;
BEGIN
   open C_ACCESS;
   fetch C_ACCESS into L_dummy;
   if C_ACCESS%NOTFOUND then
      O_valid := FALSE;
      open C_FILTER_USER;
      fetch C_FILTER_USER into L_dummy;
      close C_FILTER_USER;
      if L_dummy = 'Y' then
         O_valid := FALSE;
      else -- the user is not attached to any security groups and is a superuser
         O_valid := TRUE;
      end if;
   else
      O_valid := TRUE;
   end if;

   close C_ACCESS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_USER_ACCESS;
----------------------------------------------------------------------------------------------------
FUNCTION COMPARE_USER_SECURITY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_update_user_id_ok IN OUT BOOLEAN,
                               I_create_user_id    IN     VARCHAR2,
                               I_update_user_id    IN     VARCHAR2)

RETURN BOOLEAN IS

   FUNCTION_NAME                CONSTANT   VARCHAR2(61) := 'PRIVILEGE_SQL.COMPARE_USER_SECURITY';

   DEPT_LEVEL                   CONSTANT   FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE := 'P';
   GROUP_LEVEL                  CONSTANT   FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE := 'G';
   DIVISION_LEVEL               CONSTANT   FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE := 'D';

   DISTRICT_LEVEL               CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'D';
   REGION_LEVEL                 CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'R';
   AREA_LEVEL                   CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'A';
   CHAIN_LEVEL                  CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'C';
   WAREHOUSE_LEVEL              CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'W';
   INTERNAL_FINISHER_LEVEL      CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'I';
   EXTERNAL_FINISHER_LEVEL      CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'E';
   TSF_ENTITY_LEVEL             CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'T';
   ORG_UNIT_LEVEL               CONSTANT   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE     := 'O';
   DISTRICT_SELECTED            CONSTANT   NUMBER(1) := 1;
   WAREHOUSE_SELECTED           CONSTANT   NUMBER(1) := 2;
   INTERNAL_FINISHER_SELECTED   CONSTANT   NUMBER(1) := 3;
   EXTERNAL_FINISHER_SELECTED   CONSTANT   NUMBER(1) := 4;
   TSF_ENTITY_SELECTED          CONSTANT   NUMBER(1) := 5;
   ORG_UNIT_SELECTED            CONSTANT   NUMBER(1) := 6;

   L_found                                 BOOLEAN;

/*
The cursor C_MERCH explodes the merchandise security definitions of the user that created the deal
down to the lowest level, ie. Dept.  It does the same explosion for the user that is attempting to
update the deal.  It then subtracts (using the SQL MINUS clause) the update user's security profile
from the creation user's profile.  If, following this subtraction, the cursor still returns a row,
this means that the creation user must have a privilege that the update user doesn't have.  In this
case, O_update_user_id_ok will be returned as FALSE.  Otherwise it will be returned as TRUE.

A similar approach is taken with the security profile of the 2 users for the organisational
hierarchy - see cursor C_ORG, below.  The organisational hierarchy security definitions are
exploded out to the lowest level, ie. District, Warehouse, Internal Finisher, External Finisher
and Transfer Entity.
*/

   cursor   C_MERCH
       is
  (select   fgm.filter_merch_id
     from   filter_group_merch     fgm,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgm.sec_group_id
      and   fgm.filter_merch_level = DEPT_LEVEL
union all
   select   dep.dept
     from   deps                   dep,
            filter_group_merch     fgm,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgm.sec_group_id
      and   fgm.filter_merch_level = GROUP_LEVEL
      and   fgm.filter_merch_id    = dep.group_no
union all
   select   dep.dept
     from   deps                   dep,
            groups                 gro,
            filter_group_merch     fgm,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgm.sec_group_id
      and   fgm.filter_merch_level = DIVISION_LEVEL
      and   fgm.filter_merch_id    = gro.division
      and   gro.group_no           = dep.group_no)
    minus
  (select   fgm.filter_merch_id
     from   filter_group_merch     fgm,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgm.sec_group_id
      and   fgm.filter_merch_level = DEPT_LEVEL
union all
   select   dep.dept
     from   deps                   dep,
            filter_group_merch     fgm,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgm.sec_group_id
      and   fgm.filter_merch_level = GROUP_LEVEL
      and   fgm.filter_merch_id    = dep.group_no
union all
   select   dep.dept
     from   deps                   dep,
            groups                 gro,
            filter_group_merch     fgm,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgm.sec_group_id
      and   fgm.filter_merch_level = DIVISION_LEVEL
      and   fgm.filter_merch_id    = gro.division
      and   gro.group_no           = dep.group_no);

   c_merch_row   C_MERCH%ROWTYPE;

/*
See the comment above cursor C_MERCH for an explanation of cursor C_ORG.
*/

   cursor   C_ORG
       is
  (select   fgo.filter_org_id,
            DISTRICT_SELECTED
     from   filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = DISTRICT_LEVEL
union all
   select   dis.district,
            DISTRICT_SELECTED
     from   district               dis,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = REGION_LEVEL
      and   fgo.filter_org_id      = dis.region
union all
   select   dis.district,
            DISTRICT_SELECTED
     from   district               dis,
            region                 reg,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = AREA_LEVEL
      and   fgo.filter_org_id      = reg.area
      and   reg.region             = dis.region
union all
   select   dis.district,
            DISTRICT_SELECTED
     from   district               dis,
            region                 reg,
            area                   are,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = CHAIN_LEVEL
      and   fgo.filter_org_id      = are.chain
      and   are.area               = reg.area
      and   reg.region             = dis.region
union all
   select   wah.wh,
            WAREHOUSE_SELECTED
     from   wh                     wah,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = WAREHOUSE_LEVEL
      and   fgo.filter_org_id      = wah.wh
      and   wah.finisher_ind       = 'N'
union all
   select   wah.wh,
            INTERNAL_FINISHER_SELECTED
     from   wh                     wah,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = INTERNAL_FINISHER_LEVEL
      and   fgo.filter_org_id      = wah.wh
      and   wah.finisher_ind       = 'Y'
union all
   select   To_Number(prt.partner_id),
            EXTERNAL_FINISHER_SELECTED
     from   partner                prt,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = EXTERNAL_FINISHER_LEVEL
      and   fgo.filter_org_id      = prt.partner_id
      and   prt.partner_type       = 'E'
union all
   select   To_Number(tsn.tsf_entity_id),
            TSF_ENTITY_SELECTED
     from   tsf_entity             tsn,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = TSF_ENTITY_LEVEL
      and   fgo.filter_org_id      = tsn.tsf_entity_id
union all
   select   To_Number(org_unit_id) ,
            ORG_UNIT_SELECTED
     from   org_unit               ogu,
            filter_group_org       fgo,
            sec_user_group         sug,
			sec_user               su 
      where   su.database_user_id    = I_create_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = ORG_UNIT_LEVEL
      and   fgo.filter_org_id      = ogu.org_unit_id)      
    minus
  (select   fgo.filter_org_id,
            DISTRICT_SELECTED
     from   filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = DISTRICT_LEVEL
union all
   select   dis.district,
            DISTRICT_SELECTED
     from   district               dis,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = REGION_LEVEL
      and   fgo.filter_org_id      = dis.region
union all
   select   dis.district,
            DISTRICT_SELECTED
     from   district               dis,
            region                 reg,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = AREA_LEVEL
      and   fgo.filter_org_id      = reg.area
      and   reg.region             = dis.region
union all
   select   dis.district,
            DISTRICT_SELECTED
     from   district               dis,
            region                 reg,
            area                   are,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = CHAIN_LEVEL
      and   fgo.filter_org_id      = are.chain
      and   are.area               = reg.area
      and   reg.region             = dis.region
union all
   select   wah.wh,
            WAREHOUSE_SELECTED
     from   wh                     wah,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = WAREHOUSE_LEVEL
      and   fgo.filter_org_id      = wah.wh
      and   wah.finisher_ind       = 'N'
union all
   select   wah.wh,
            INTERNAL_FINISHER_SELECTED
     from   wh                     wah,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = INTERNAL_FINISHER_LEVEL
      and   fgo.filter_org_id      = wah.wh
      and   wah.finisher_ind       = 'Y'
union all
   select   To_Number(prt.partner_id),
            EXTERNAL_FINISHER_SELECTED
     from   partner                prt,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = EXTERNAL_FINISHER_LEVEL
      and   fgo.filter_org_id      = prt.partner_id
      and   prt.partner_type       = 'E'
union all
   select   To_Number(tsn.tsf_entity_id),
            TSF_ENTITY_SELECTED
     from   tsf_entity             tsn,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = TSF_ENTITY_LEVEL
      and   fgo.filter_org_id      = tsn.tsf_entity_id
union all
   select   To_Number(org_unit_id) ,
            ORG_UNIT_SELECTED
     from   org_unit               ogu,
            filter_group_org       fgo,
            sec_user_group         sug,
            sec_user               su     
        
    where   su.database_user_id    = I_update_user_id
      and   su.user_seq            = sug.user_seq
      and   sug.group_id           = fgo.sec_group_id
      and   fgo.filter_org_level   = ORG_UNIT_LEVEL
      and   fgo.filter_org_id      = ogu.org_unit_id);      

   c_org_row   C_ORG%ROWTYPE;

BEGIN

   O_update_user_id_ok := FALSE;

   open  C_MERCH;

   fetch C_MERCH
   into  c_merch_row;

   L_found := C_MERCH%FOUND;

   close C_MERCH;

   if L_found then
      O_update_user_id_ok := FALSE;
      return TRUE;
   end if;

   open  C_ORG;

   fetch C_ORG
   into  c_org_row;

   L_found := C_ORG%FOUND;

   close C_ORG;

   if L_found then
      O_update_user_id_ok := FALSE;
      return TRUE;
   end if;

   O_update_user_id_ok := TRUE;

   return TRUE;

EXCEPTION
   when OTHERS then
      return FALSE;
END COMPARE_USER_SECURITY;
----------------------------------------------------------------------------------------------------
FUNCTION ALLOW_FIXED_DEAL_SEC(I_deal_no          IN   FIXED_DEAL.DEAL_NO%TYPE,
                              I_merch_ind        IN   FIXED_DEAL.MERCH_IND%TYPE,
                              I_create_user_id   IN   FIXED_DEAL.USER_ID%TYPE,
                              I_update_user_id   IN   FIXED_DEAL.USER_ID%TYPE)
   RETURN NUMBER IS

   ACCESS_DENIED     CONSTANT   NUMBER(1)   := 0;
   ACCESS_GRANTED    CONSTANT   NUMBER(1)   := 1;

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_update_id_ok    BOOLEAN;
   L_merch           VARCHAR2(1);
   L_org             VARCHAR2(1);

   cursor C_MERCH is
   select 'x'
     from fixed_deal_merch fdm
    where fdm.deal_no = I_deal_no      
      and exists (select 'x'
                    from v_subclass v
                   where v.dept = fdm.dept
                     and (v.class = fdm.class
                          or fdm.class IS NULL)
                     and (v.subclass = fdm.subclass
                          or fdm.subclass IS NULL)
                     and rownum = 1)
      and rownum      = 1;

   cursor C_ORG is
   select 'x'
     from fixed_deal_merch_loc fdml
    where fdml.deal_no  = I_deal_no
      and fdml.loc_type = 'S'
      and fdml.location not in (select store
                                  from v_store)
      and rownum        = 1
    union all
   select 'x'
     from fixed_deal_merch_loc fdml
    where fdml.deal_no  = I_deal_no
      and fdml.loc_type = 'W'
      and fdml.location not in (select wh
                                  from v_wh)
      and rownum        = 1;

BEGIN

   if (I_deal_no        is NULL
   or  I_merch_ind      is NULL
   or  I_create_user_id is NULL
   or  I_update_user_id is NULL) then

      return ACCESS_DENIED;
   end if;

   if I_merch_ind = 'Y' then
      open  C_MERCH;
      fetch C_MERCH into L_merch;
      close C_MERCH;

      if L_merch is NULL then
         return ACCESS_DENIED;
      end if;

      open  C_ORG;
      fetch C_ORG into L_org;
      close C_ORG;

      if L_org is not NULL then
         return ACCESS_DENIED;
      end if;

      return ACCESS_GRANTED;

   else
      if I_update_user_id = I_create_user_id then
         return ACCESS_GRANTED;
      else
         if PRIVILEGE_SQL.COMPARE_USER_SECURITY(L_error_message,
                                                L_update_id_ok,
                                                I_create_user_id,
                                                I_update_user_id) = FALSE then
            return ACCESS_DENIED;
         end if;

         if L_update_id_ok then
            return ACCESS_GRANTED;
         else
            return ACCESS_DENIED;
         end if;
      end if;
   end if;

   return ACCESS_DENIED;

EXCEPTION
   when OTHERS then
      return ACCESS_DENIED;
END ALLOW_FIXED_DEAL_SEC;
----------------------------------------------------------------------------------------------------
FUNCTION ALLOW_COMPLEX_DEAL_SEC(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_allow_deal_access IN OUT BOOLEAN,
                                I_deal_id           IN     DEAL_HEAD.DEAL_ID%TYPE,
                                I_create_id         IN     DEAL_HEAD.CREATE_ID%TYPE,
                                I_order_no          IN     DEAL_HEAD.ORDER_NO%TYPE,
                                I_security_ind      IN     DEAL_HEAD.SECURITY_IND%TYPE,
                                I_update_id         IN     DEAL_HEAD.CREATE_ID%TYPE)

RETURN BOOLEAN IS

   FUNCTION_NAME    CONSTANT   VARCHAR2(36) := 'PRIVILEGE_SQL.ALLOW_COMPLEX_DEAL_SEC';

   L_found                     BOOLEAN;

/*
The aim of this cursor, is to tell us if the user attempting to update the deal has security
access to *any* DEAL_ITEMLOC record on a combination of both the merchandise and organisation
hierarchy.

It retrieves each DEAL_ITEMLOC record for the deal where EXCL_IND=N.  It then joins the
relevant level of the merchandise hierarchy to V_ITEM_MASTER and the relevant level of the
organisational hierarchy to V_LOCATION.  As both of these views are "security-enabled", only
those rows that the user has security access for are returned.  The cursor further checks that
the merch/org combination found on DEAL_ITEMLOC is not excluded (EXCL_IND=Y) by another
DEAL_ITEMLOC record on the same deal.
*/

   L_security_data_exists  BOOLEAN;

   cursor C_DLI(I_division GROUPS.DIVISION%TYPE,
                I_group_no GROUPS.GROUP_NO%TYPE,
                I_dept     SUBCLASS.DEPT%TYPE,
                I_class    SUBCLASS.CLASS%TYPE,
                I_subclass SUBCLASS.SUBCLASS%TYPE) is
      select 'x'
        from deal_itemloc d,
             (select 'S' loc_type, s.chain, s.area, s.region, s.district, s.store loc
                from v_store s
               where stockholding_ind = 'Y'
               union all
              select 'W',0,0,0,0,w.physical_wh
                from v_wh w
               where w.stockholding_ind = 'Y') vl
       where deal_id = I_deal_id 
         and excl_ind = 'N'
         and (NVL(d.org_level,0) = 0 
              or (d.org_level = 1 and vl.chain = d.chain) 
              or (d.org_level = 2 and vl.area = d.area) 
              or (d.org_level = 3 and vl.region = d.region) 
              or (d.org_level = 4 and vl.district = d.district) 
              or (d.org_level = 5 and vl.loc = d.location and d.loc_type = 'S')
              or (vl.loc = d.location and d.loc_type = 'W'))
         and (d.merch_level = 1 
              or (d.merch_level = 2 and I_division = d.division)
              or (d.merch_level = 3 and I_group_no = d.group_no) 
              or (d.merch_level = 4 and I_dept = d.dept) 
              or (d.merch_level = 5 and I_dept = d.dept and I_class = d.class) 
              or (d.merch_level >= 6  and I_dept = d.dept and I_class = d.class and I_subclass = d.subclass))
         and not exists (select 1
                           from deal_itemloc d2
                          where d2.deal_id = d.deal_id
                            and d2.excl_ind = 'Y'
                            and (NVL(d2.org_level,0) = 0 
                                 or (d2.org_level = 1 and vl.chain = d2.chain) 
                                 or (d2.org_level = 2 and vl.area = d2.area) 
                                 or (d2.org_level = 3 and vl.region = d2.region) 
                                 or (d2.org_level = 4 and vl.district = d2.district) 
                                 or (d2.org_level = 5 and vl.loc = d2.location and d2.loc_type = 'S')
                                 or (vl.loc = d2.location and d2.loc_type = 'W'))
                            and (d2.merch_level = 1 
                                 or (d2.merch_level = 2 and I_division = d2.division) 
                                 or (d2.merch_level = 3 and I_group_no = d2.group_no) 
                                 or (d2.merch_level = 4 and I_dept = d2.dept) 
                                 or (d2.merch_level = 5 and I_dept = d2.dept and I_class = d2.class) 
                                 or (d2.merch_level = 6 and I_dept = d2.dept and I_class = d2.class and I_subclass = d2.subclass)))
         and rownum = 1;

   c_dli_row   C_DLI%ROWTYPE;

   cursor   C_DLD
       is
   select   1
     from   deal_detail           dld
    where   dld.deal_id           = I_deal_id
      and   dld.tran_discount_ind = 'Y';

   c_dld_row   C_DLD%ROWTYPE;

   cursor   C_VOHE
       is
   select   1
     from   v_ordhead             vohe
    where   vohe.order_no         = I_order_no;

   c_vohe_row   C_VOHE%ROWTYPE;

BEGIN

   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_deal_id',
                                            'NULL');
      return FALSE;
   end if;

   if I_create_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_create_id',
                                            'NULL');
      return FALSE;
   end if;

   if I_security_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_security_ind',
                                            'NULL');
      return FALSE;
   end if;

   if I_update_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_update_id',
                                            'NULL');
      return FALSE;
   end if;

   O_allow_deal_access := FALSE;

   if I_security_ind = 'Y' then

      if PRIVILEGE_SQL.COMPARE_USER_SECURITY(O_error_message,
                                             O_allow_deal_access,
                                             I_create_id,
                                             I_update_id) = FALSE then
         return FALSE;
      end if;

   else

      if I_order_no is null then

         open  C_DLD;
         fetch C_DLD into c_dld_row;
         L_found := C_DLD%FOUND;
         close C_DLD;

         if L_found then
            O_allow_deal_access := TRUE;
         else
            if PRIVILEGE_SQL.CHECK_GLOBAL_SECURITY(O_error_message,
                                                   L_security_data_exists) = FALSE then
               return FALSE;
            end if;

            if L_security_data_exists then
               FOR i IN LP_division.FIRST..LP_division.LAST LOOP
                  open  C_DLI(LP_division(i),
                              LP_group_no(i),
                              LP_dept(i),
                              LP_class(i),
                              LP_subclass(i));
                  fetch C_DLI into c_dli_row;
                  if C_DLI%FOUND then 
                     O_allow_deal_access := TRUE;
                     exit;
                  end if;
                  close C_DLI;
               END LOOP;
            end if;

         end if;

      else

         open  C_VOHE;
         fetch C_VOHE into c_vohe_row;
         O_allow_deal_access := C_VOHE%FOUND;
         close C_VOHE;

      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return FALSE;
END ALLOW_COMPLEX_DEAL_SEC;
----------------------------------------------------------------------------------------------------
FUNCTION CHECK_GLOBAL_SECURITY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_global_sec_exists IN OUT BOOLEAN)

RETURN BOOLEAN IS

   FUNCTION_NAME    CONSTANT   VARCHAR2(36) := 'PRIVILEGE_SQL.CHECK_GLOBAL_SECURITY';

   cursor C_MERCH is
      select distinct v.division,
                      v.group_no,
                      v.dept,
                      v.class,
                      v.subclass
        from v_item_master v;

BEGIN

   if LP_session_user is NULL then

      O_global_sec_exists := FALSE;

      open C_MERCH;
      fetch C_MERCH BULK COLLECT into LP_division,
                                      LP_group_no,
                                      LP_dept,
                                      LP_class,
                                      LP_subclass;
      close C_MERCH;

      if NVL(LP_division.COUNT,0) > 0 or LP_division.COUNT is not NULL then
         O_global_sec_exists := TRUE;
         LP_session_user := USER;
      else 
         --- No security data exists for user
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               'ERR_SEC',
                                               FUNCTION_NAME,
                                               NULL);

      end if;

   else
      O_global_sec_exists := TRUE;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return FALSE;
END CHECK_GLOBAL_SECURITY;
----------------------------------------------------------------------------------------------------
FUNCTION POP_DBA_ROLE_PRIVS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
  
   L_program          CONSTANT   VARCHAR2(36) := 'PRIVILEGE_SQL.POP_DBA_ROLE_PRIVS';

   cursor C_GET_ROLE is
      select granted_role
        from dba_role_privs
       where grantee = SYS_CONTEXT ('USERENV', 'SESSION_USER');

BEGIN
   open  C_GET_ROLE;
   fetch C_GET_ROLE BULK COLLECT into role_tabl;
   close C_GET_ROLE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END POP_DBA_ROLE_PRIVS;
----------------------------------------------------------------------------------------------------
END PRIVILEGE_SQL;
/