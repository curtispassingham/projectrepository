-- File Name : CORESVC_SUP_AVAIL_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_SUP_AVAIL AUTHID CURRENT_USER AS
   template_key                            CONSTANT VARCHAR2(255)    := 'SUP_AVAIL_DATA';
   action_new                              VARCHAR2(25)              := 'NEW';
   action_mod                              VARCHAR2(25)              := 'MOD';
   action_del                              VARCHAR2(25)              := 'DEL';
   SUP_AVAIL_sheet                         VARCHAR2(255)             := 'SUP_AVAIL';
   SUP_AVAIL$Action                        NUMBER                    :=1;
   SUP_AVAIL$LAST_DECLARED_DATE            NUMBER                    :=7;
   SUP_AVAIL$LAST_UPDATE_DATE              NUMBER                    :=6;
   SUP_AVAIL$QTY_AVAIL                     NUMBER                    :=5;
   SUP_AVAIL$REF_ITEM                      NUMBER                    :=4;
   SUP_AVAIL$ITEM                          NUMBER                    :=3;
   SUP_AVAIL$SUPPLIER                      NUMBER                    :=2;
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255)                                    := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE                           :='RMSINV';
   TYPE SUP_AVAIL_rec_tab IS TABLE OF SUP_AVAIL%ROWTYPE;
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
								I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
						  I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
END CORESVC_SUP_AVAIL;
/
