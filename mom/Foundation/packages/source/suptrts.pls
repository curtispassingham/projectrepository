
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUP_TRAITS_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
---NAME:        TRAIT_EXISTS
---PURPOSE:     Checks the sup_traits table to ensure that
---             the supplier trait exists.

FUNCTION TRAIT_EXISTS(O_error_message IN OUT VARCHAR2,
                      O_trait_exists  IN OUT BOOLEAN,
                      I_sup_trait     IN     sup_traits.sup_trait%TYPE)
                      RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
---NAME:        SUP_TRAIT_DESC
---PURPOSE:     Gets the supplier trait description.

FUNCTION SUP_TRAIT_DESC(O_error_message IN OUT VARCHAR2,
                        I_sup_trait     IN     sup_traits.sup_trait%TYPE,
                        O_trait_desc    IN OUT VARCHAR2)
                        RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
---NAME:        MASTER_SUPPLIER
---PURPOSE:     Gets the values in the master_supplier_ind and
---             master_supplier fields from sup_traits.

FUNCTION MASTER_SUPPLIER(O_error_message  IN OUT VARCHAR2,
                         I_sup_trait      IN     sup_traits.sup_trait%TYPE,
                         O_master_sup_ind IN OUT sup_traits.master_sup_ind%TYPE,
                         O_master_sup     IN OUT sup_traits.master_sup%TYPE)
                         RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
---NAME:        RELATION_EXISTS
---PURPOSE:     Checks for a relationship between a supplier and a trait.
---             If one is not found, but the passed in trait is a master suplier,
--              a check is done for a relationship between the supplier and another
---             master supplier.   

FUNCTION RELATION_EXISTS(O_error_message   IN OUT VARCHAR2,
                         O_relation_exists IN OUT BOOLEAN,
                         O_master_exists   IN OUT BOOLEAN,
                         I_supplier        IN     sup_traits_matrix.supplier%TYPE,
                         I_sup_trait       IN     sup_traits.sup_trait%TYPE,
                         I_master_sup_ind  IN     sup_traits.master_sup_ind%TYPE)
                         RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
---NAME:        ROWS_EXIST
---PURPOSE:     Checks for a rows on the supplier traits matrix table.
---             Does check either by supplier or trait.

FUNCTION ROWS_EXIST(O_error_message   IN OUT VARCHAR2,
                    O_rows_exist      IN OUT BOOLEAN,
                    I_supplier        IN     sup_traits_matrix.supplier%TYPE,
                    I_sup_trait       IN     sup_traits.sup_trait%TYPE)
                    RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
---NAME:        DELETE_TRAIT
---PURPOSE:     Deletes supplier trait from sup_traits_matrix and sup_trait.

FUNCTION DELETE_TRAIT(O_error_message   IN OUT VARCHAR2,
                      I_sup_trait       IN     sup_traits.sup_trait%TYPE)
                      RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
---NAME:        VALIDATE_MASTER_SUP
---PURPOSE:     Checks if suppliers associated with the inputted supplier trait exist under 
---             an already existing master trait.    
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MASTER_SUP(O_error_message   IN OUT VARCHAR2,
                             O_exists          IN OUT BOOLEAN,
                             I_sup_trait       IN     SUP_TRAITS.SUP_TRAIT%TYPE)
                             RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END SUP_TRAITS_SQL;
/


