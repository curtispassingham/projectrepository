-- File Name : CORESVC_NON_MERCH_CODE_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_NON_MERCH_CODE AUTHID CURRENT_USER AS
   template_key                 CONSTANT VARCHAR2(255)          := 'NON_MERCH_CODE_DATA';
   template_category            CONSTANT VARCHAR2(255)          := 'RMSFIN';
   action_column                         VARCHAR2(255)          := 'ACTION';
   action_new                            VARCHAR2(25)           := 'NEW';
   action_mod                            VARCHAR2(25)           := 'MOD';
   action_del                            VARCHAR2(25)           := 'DEL';
   non_mer_comp_sheet                    VARCHAR2(255)          := 'NON_MERCH_CODE_COMP';
   non_mer_comp$action                   NUMBER                 :=1;
   non_mer_comp$comp_id                  NUMBER                 :=3;
   non_mer_comp$non_merch_code           NUMBER                 :=2;

   TYPE NON_MER_COMP_rec_tab IS TABLE OF NON_MERCH_CODE_COMP%ROWTYPE;

   non_mer_tl_sheet                      VARCHAR2(255)          := 'NON_MERCH_CODE_HEAD_TL';
   non_mer_tl$action                     NUMBER                 :=1;
   non_mer_tl$non_merch_code_desc        NUMBER                 :=4;
   non_mer_tl$non_merch_code             NUMBER                 :=2;

   TYPE NON_MER_TL_rec_tab IS TABLE OF NON_MERCH_CODE_HEAD_TL%ROWTYPE;
   sheet_name_trans S9T_PKG.TRANS_MAP_TYP;

   non_mer_lang_sheet                    VARCHAR2(255)          := 'NON_MERCH_CODE_HEAD_LTL';
   non_mer_lang$action                   NUMBER                 := 1;
   non_mer_lang$lang                     NUMBER                 := 2;
   non_mer_lang$non_merch_code           NUMBER                 := 3;
   non_mer_lang$nmc_desc                 NUMBER                 := 4;

   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN     CHAR                    DEFAULT 'N')
     RETURN BOOLEAN;

   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
     RETURN BOOLEAN;

   FUNCTION process(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
     RETURN BOOLEAN;

   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
     RETURN VARCHAR2;

END CORESVC_NON_MERCH_CODE;
/
