
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PRIORITY_GROUP_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- Function Name : NEXT_PRIORITY_GROUP_ID_NUMBER
-- Purpose       : This function is used to generate a new priority group id.
--------------------------------------------------------------------------------
FUNCTION NEXT_PRIORITY_GROUP_ID_NUMBER (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_priority_group_id  IN OUT PRIORITY_GROUP.PRIORITY_GROUP_ID%TYPE)
   RETURN BOOLEAN;
   
--------------------------------------------------------------------------------
-- Function Name : CHECK_DUPLICATE_PRIORITY
-- Purpose       : This function checks if the priority entered by the user is 
--                 already present in priority_group table . 
--------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE_PRIORITY (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists             IN OUT BOOLEAN,
                                   I_priority_group_id  IN     PRIORITY_GROUP.PRIORITY_GROUP_ID%TYPE,
                                   I_priority           IN     PRIORITY_GROUP.PRIORITY%TYPE)
   RETURN BOOLEAN;
   
--------------------------------------------------------------------------------
-- Function Name : CHECK_LOCATION_PRIORITY
-- Purpose       : This function checks if the location which is being inserted 
--                 is already present in PRIORITY_LOCATIONS table.
--------------------------------------------------------------------------------
FUNCTION CHECK_LOCATION_PRIORITY(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists             IN OUT BOOLEAN,
                                 I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                                 I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE)
   RETURN BOOLEAN;             
                       
--------------------------------------------------------------------------------
-- Function Name : CHECK_SAME_LOC_PRIORITY_GROUP
-- Purpose       : This function checks if the location being inserted is already 
--                 present in PRIORITY_LOCATIONS table with the same priority group.
-------------------------------------------------------------------------------- 
FUNCTION CHECK_SAME_LOC_PRIORITY_GROUP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists             IN OUT BOOLEAN,
                                       I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                                       I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE)                                       
   RETURN BOOLEAN;       
   
--------------------------------------------------------------------------------
-- Function Name : ADD_LOCATIONS_PRIORITY_GROUP
-- Purpose       : This function adds locations to the PRIORITY_GROUP_LOCATIONS 
--                 table if the location is not already present.
--------------------------------------------------------------------------------
FUNCTION ADD_LOCATIONS_PRIORITY_GROUP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_diff_priority_grp  IN OUT VARCHAR2,
                                      I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                                      I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE,
                                      I_location_type      IN     PRIORITY_GROUP_LOCATIONS.LOCATION_TYPE%TYPE,
                                      I_create_date        IN     PRIORITY_GROUP_LOCATIONS.CREATE_DATE%TYPE,
                                      I_update_date        IN     PRIORITY_GROUP_LOCATIONS.LAST_UPDATE_DATE%TYPE,
                                      I_create_user_id     IN     PRIORITY_GROUP_LOCATIONS.CREATE_USER_ID%TYPE,      
                                      I_update_user_id     IN     PRIORITY_GROUP_LOCATIONS.LAST_UPDATE_USER_ID%TYPE)
   RETURN BOOLEAN;       
                                                         
--------------------------------------------------------------------------------
-- Function Name : UPDATE_PRIORITY_GROUP
-- Purpose       : This function update the priority_group_id of an already existing 
--                 location.
--------------------------------------------------------------------------------
FUNCTION UPDATE_PRIORITY_GROUP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                               I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE,
                               I_location_type      IN     PRIORITY_GROUP_LOCATIONS.LOCATION_TYPE%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name : LOCS_EXIST_IN_LOCLIST
-- Purpose       : This function checks if the input location list contains 
--                 locations.
--------------------------------------------------------------------------------
FUNCTION LOCS_EXIST_IN_LOCLIST(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_loc_list           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE)

   RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name : DELETE_PRIORITY_GRP_LOC
-- Purpose       : This function deletes a priority group location.
--------------------------------------------------------------------------------
FUNCTION DELETE_PRIORITY_GRP_LOC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                                 I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE)

   RETURN BOOLEAN;
   
--------------------------------------------------------------------------------
-- Function Name : DELETE_PRIORITY_GRP
-- Purpose       : This function deletes a priority group location.
-------------------------------------------------------------------------------- 
FUNCTION DELETE_PRIORITY_GRP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_priority_group_id  IN     PRIORITY_GROUP.PRIORITY_GROUP_ID%TYPE)
                             
      RETURN BOOLEAN;                             

--------------------------------------------------------------------------------
-- Function Name: DELETE_PRIORITY_GRP_TL
-- Purpose:       This function delete records from priority_group_tl table for given priority_group_id.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_PRIORITY_GROUP_TL (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_priority_group_id  IN       PRIORITY_GROUP_TL.PRIORITY_GROUP_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END;
/
