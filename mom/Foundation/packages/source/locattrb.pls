
CREATE OR REPLACE PACKAGE BODY LOCATION_ATTRIB_SQL AS

--------------------------------------------------------------------------------
-- These module level variables will be used to over load the build group type
-- where clause function.  One will store the name of the location column (loc,
-- location, from_loc, or to_loc).  The other will store the type of the location
-- (loc_type, from_loc_type, or to_loc_type).
LP_loc_or_location  VARCHAR2(9)  := 'location';
LP_loc_type         VARCHAR2(30) := 'loc_type';

--------------------------------------------------------------------------------------------
FUNCTION GET_TYPE(O_error_message IN OUT    VARCHAR2,
                  O_loc_type      IN OUT    VARCHAR2,
                  I_location      IN        store.store%TYPE)
                  return BOOLEAN IS

   cursor C_GET_TYPE is
      select 'S'
        from store
       where store = I_location
         and rownum = 1
       UNION ALL
      select DECODE(org_entity_type, 'R', 'W', org_entity_type)
        from wh
       where wh = I_location
         and rownum = 1
       UNION ALL 
      select 'E'
        from partner
       where partner_id   = TO_CHAR(I_location)
         and partner_type = 'E'
         and rownum = 1;

BEGIN

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   O_loc_type := NULL;

   SQL_LIB.SET_MARK('OPEN', 'C_GET_TYPE', 'store/wh/partner', 'store/wh/partner: '||to_char(I_location));
   open C_GET_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TYPE', 'store/wh/partner', 'store/wh/partner: '||to_char(I_location));
   fetch C_GET_TYPE into O_loc_type;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TYPE', 'store/wh/partner', 'store/wh/partner: '||to_char(I_location));
   close C_GET_TYPE;

   if O_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_LOC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_TYPE',
                                             to_char(SQLCODE));
      RETURN FALSE;

END GET_TYPE;
--------------------------------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message IN OUT   VARCHAR2,
                  O_loc_name      IN OUT   VARCHAR2,
                  I_location      IN       VARCHAR2,
                  I_loc_type      IN       VARCHAR2)
                  return BOOLEAN IS

   L_loc_type       VARCHAR2(3) := I_loc_type;

BEGIN

   O_loc_name := NULL;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if (I_loc_type = 'PW') then
      L_loc_type := 'W';
   end if;

   if L_loc_type is NULL then
      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_loc_type,
                                      I_location) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                   I_location,
                                   O_loc_name) = FALSE then
         return FALSE;
      end if;

   elsif L_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                I_location,
                                O_loc_name) = FALSE then
         return FALSE;
      end if;
      
   elsif L_loc_type in ('M', 'X') then
      if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                I_location,
                                O_loc_name) = FALSE then
         return FALSE;
      end if;

   elsif L_loc_type = 'D' then
      if ORGANIZATION_ATTRIB_SQL.DISTRICT_NAME(O_error_message,
                                               I_location,
                                               O_loc_name) = FALSE then
         return FALSE;
      end if;

   elsif L_loc_type = 'R' then
      if ORGANIZATION_ATTRIB_SQL.REGION_NAME(O_error_message,
                                             I_location,
                                             O_loc_name) = FALSE then
         return FALSE;
      end if;
   elsif L_loc_type = 'T' then
      if TSF_ATTRIB_SQL.TSF_ZONE_DESC(O_error_message,
                                      I_location,
                                      O_loc_name) = FALSE then
         return FALSE;
      end if;

      null;

   elsif L_loc_type = 'L' then
      if LOC_TRAITS_SQL.GET_DESC(O_error_message,
                                 O_loc_name,
                                 I_location) = FALSE then
         return FALSE;
      end if;

   elsif L_loc_type = 'A' then
      if ORGANIZATION_ATTRIB_SQL.AREA_NAME(O_error_message,
                                           I_location,
                                           O_loc_name) = FALSE then
         return FALSE;
      end if;

   elsif L_loc_type = 'C' then
      if ORGANIZATION_ATTRIB_SQL.CHAIN_NAME(O_error_message,
                                            I_location,
                                            O_loc_name) = FALSE then
         return FALSE;
      end if;

   elsif L_loc_type = 'R' then
      if ORGANIZATION_ATTRIB_SQL.REGION_NAME(O_error_message,
                                             I_location,
                                             O_loc_name) = FALSE then
         return FALSE;
      end if;
      
   elsif L_loc_type = 'LLS' then
      if LOCLIST_ATTRIBUTE_SQL.GET_DESC(O_error_message,
                                        O_loc_name,
                                        I_location) = FALSE then
         return FALSE;
      end if;
   elsif L_loc_type = 'LLW' then
      if LOCLIST_ATTRIBUTE_SQL.GET_DESC(O_error_message,
                                        O_loc_name,
                                        I_location) = FALSE then
         return FALSE;
      end if;
   elsif L_loc_type = 'E' then

      if PARTNER_SQL.GET_DESC(O_error_message,
                              O_loc_name,
                              I_location,
                              L_loc_type) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_NAME',
                                             to_char(SQLCODE));
      RETURN FALSE;

END GET_NAME;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION BUILD_GROUP_TYPE_WHERE_CLAUSE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_where_clause    IN OUT   VARCHAR2,
                                       I_group_type      IN       VARCHAR2,
                                       I_group_value     IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'LOCATION_ATTRIB_SQL.BUILD_GROUP_TYPE_WHERE_CLAUSE';

BEGIN

   if I_group_type = 'S' then
      O_where_clause := LP_loc_or_location||' = '||to_number(I_group_value)
                     ||' and '||LP_loc_type||' = ''S''';

   elsif I_group_type = 'W' then
      O_where_clause := LP_loc_or_location||' = '||to_number(I_group_value)
                     ||' and '||LP_loc_type||' = ''W''';

   elsif I_group_type = 'C' then
      O_where_clause := LP_loc_or_location||' in (select store '
                     ||                                ' from v_store '
                     ||                               ' where store_class = '''||I_group_value||''') '
                     || ' and ' || LP_loc_type||'= ''S''';

   elsif I_group_type = 'D' then
      O_where_clause := LP_loc_or_location||' in (select store '
                     ||                           ' from v_store '
                     ||                          ' where district = '||to_number(I_group_value)||') '
                     || ' and '||LP_loc_type||' = ''S''';

   elsif I_group_type = 'R' then
      O_where_clause := LP_loc_or_location||' in (select store '
                     ||                           ' from v_store '
                     ||                          ' where region = '||to_number(I_group_value)||') '
                     || ' and '||LP_loc_type||' = ''S''';

   elsif I_group_type = 'A' then
      O_where_clause := LP_loc_or_location||' in (select store '
                     ||                           ' from v_store '
                     ||                          ' where area = '||to_number(I_group_value)||') '
                     || ' and ' ||LP_loc_type||' = ''S''';


   elsif I_group_type = 'T' then
      O_where_clause := LP_loc_or_location||' in (select store '
                     ||                           ' from v_store '
                     ||                          ' where transfer_zone = '||to_number(I_group_value)||') '
                     || ' and ' ||LP_loc_type||' = ''S''';

   elsif I_group_type = 'L' then
      O_where_clause := LP_loc_or_location||' in (select store '
                     ||                           ' from loc_traits_matrix '
                     ||                          ' where loc_trait = '||to_number(I_group_value)||') '
                     || ' and '||LP_loc_type||' = ''S''';

   elsif I_group_type = 'DW' then
      O_where_clause := LP_loc_or_location||' in (select store '
                     ||                           ' from v_store '
                     ||                          ' where default_wh = '||to_number(I_group_value)||') '
                     || ' and '||LP_loc_type||' = ''S''';

   elsif I_group_type = 'LL' then
      O_where_clause := LP_loc_or_location||' in (select location '
                     ||                           ' from loc_list_detail '
                     ||                          ' where loc_list = '||to_number(I_group_value)||') ';

   elsif I_group_type = 'LLS' then
      O_where_clause := LP_loc_or_location||' in (select location '
                     ||                           ' from loc_list_detail '
                     ||                          ' where loc_list = '||to_number(I_group_value)
                     ||                            ' and '||LP_loc_type||' = ''S'')';

   elsif I_group_type = 'LLW' then
      O_where_clause := LP_loc_or_location||' in (select location '
                     ||                           ' from loc_list_detail '
                     ||                          ' where loc_list = '||to_number(I_group_value)
                     ||                            ' and '||LP_loc_type||' = ''W'')';

   elsif I_group_type = 'PW' then
      O_where_clause := LP_loc_or_location||' in (select wh '
                     ||                           ' from v_wh '
                     ||                          ' where physical_wh = '||to_number(I_group_value)||')';
   
   elsif I_group_type = 'AS' then
      O_where_clause := LP_loc_type||' = ''S''';

   elsif I_group_type = 'AW' then
      O_where_clause := LP_loc_type||' = ''W''';

   elsif I_group_type = 'AI' then   -- Internal finisher
      O_where_clause := LP_loc_or_location||' in (select finisher_id from v_internal_finisher)' || ' and ' || 'loc_type = ''W''';

   elsif I_group_type = 'AE' then   -- External finisher
      O_where_clause := LP_loc_or_location||' in (select finisher_id from v_external_finisher)' || ' and ' || 'loc_type = ''E''';

   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      RETURN FALSE;

END BUILD_GROUP_TYPE_WHERE_CLAUSE;
------------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION BUILD_GROUP_TYPE_WHERE_CLAUSE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_where_clause      IN OUT   VARCHAR2,
                                       I_group_type        IN       VARCHAR2,
                                       I_group_value       IN       VARCHAR2,
                                       I_loc_or_location   IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'LOCATION_ATTRIB_SQL.BUILD_GROUP_TYPE_WHERE_CLAUSE';

BEGIN

   if UPPER(I_loc_or_location) in ('LOC', 'LOCATION','FROM_LOC','TO_LOC') then
      LP_loc_or_location := I_loc_or_location;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                            'I_loc_or_location',
                                            I_loc_or_location,
                                            'LOCATION_ATTRIB_SQL.BUILD_GROUP_TYPE_WHERE_CLAUSE');
      RETURN FALSE;
   end if;

   -- Used to support from_loc and to_loc
   if UPPER(LP_loc_or_location) = 'FROM_LOC' then
     LP_loc_type := 'FROM_LOC_TYPE';
   elsif UPPER(LP_loc_or_location) = 'TO_LOC' then
     LP_loc_type := 'TO_LOC_TYPE';
   end if;

   if BUILD_GROUP_TYPE_WHERE_CLAUSE(O_error_message,
                                     O_where_clause,
                                     I_group_type,
                                     I_group_value) = FALSE then
      return FALSE;
   end if;

   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END BUILD_GROUP_TYPE_WHERE_CLAUSE;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_WH
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION BUILD_GROUP_TYPE_VIRTUAL_WHERE(O_error_message   IN OUT   VARCHAR2,
                                        O_where_clause    IN OUT   VARCHAR2,
               I_loc_or_location IN       VARCHAR2,
                                        I_group_type      IN       VARCHAR2,
                                        I_group_value     IN       VARCHAR2)
return BOOLEAN IS

-- Valid values for I_loc_or_location are 'loc' and 'location'.
-- This parameter will be used to build the query if passed in.

BEGIN
   LP_loc_or_location := I_loc_or_location;

   if I_group_type = 'W' then
      O_where_clause := LP_loc_or_location
                        || ' in (select w2.wh from v_wh w, v_wh w2'
                        || ' where w.wh = '
                        || to_number(I_group_value)
                        || ' and (w.wh = w2.wh or w.physical_wh = w2.physical_wh)'
                        || ' and (w2.wh != '
                        || to_number(I_group_value)
                        || ' and w2.stockholding_ind = ''Y'''
                        || '))';
   elsif I_group_type = 'LLW' then
      O_where_clause := LP_loc_or_location
                        || ' in (select distinct w2.wh'
                        || ' from loc_list_detail l,'
                        || ' v_wh w,'
                        || ' v_wh w2'
                        || ' where l.loc_list = '
                        || to_number(I_group_value)
                        || ' and l.location = w.wh'
                        || ' and (w2.physical_wh = w.wh or'
                        || ' (w2.physical_wh = w.physical_wh '
                        || ' and w2.stockholding_ind = ''Y'') ))';
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.BUILD_GROUP_TYPE_VIRTUAL_WHERE',
                                             to_char(SQLCODE));
      RETURN FALSE;
END BUILD_GROUP_TYPE_VIRTUAL_WHERE;
--------------------------------------------------------------------------------------------
FUNCTION GET_STORE_CURRENCY(O_error_message         OUT VARCHAR2,
                            O_currency_code         OUT currencies.currency_code%TYPE,
                            I_store                 IN  store.store%TYPE)
return BOOLEAN IS

cursor C_GET_STORE_CURRENCY is
   select currency_code
     from store
    where store = I_store;

BEGIN
   open C_GET_STORE_CURRENCY;
   fetch C_GET_STORE_CURRENCY into O_currency_code;
   close C_GET_STORE_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_STORE_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_STORE_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_WH_CURRENCY(O_error_message OUT VARCHAR2,
                         O_currency_code OUT currencies.currency_code%TYPE,
                         I_wh            IN  wh.wh%TYPE)
return BOOLEAN IS

cursor C_GET_WH_CURRENCY is
   select currency_code
     from wh
    where wh = I_wh;

BEGIN
   open C_GET_WH_CURRENCY;
   fetch C_GET_WH_CURRENCY into O_currency_code;
   close C_GET_WH_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_WH_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_WH_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_STORE_CLASS_CURRENCY(O_error_message          OUT VARCHAR2,
                                  O_multi_currencies_exist OUT BOOLEAN,
                                  O_currency_code          OUT currencies.currency_code%TYPE,
                                  I_store_class            IN  store.store_class%TYPE)
return BOOLEAN IS

cursor C_GET_STORE_CLASS_CURRENCY is
   select s1.currency_code
     from store s1
    where s1.store_class = I_store_class
      and not exists(select 'x'
                       from store s2
                      where s2.store_class = I_store_class
                        and s2.currency_code != s1.currency_code);

BEGIN

   /*
    * This function assumes that there are stores in the passed in store class.
    * If there are no stores within the store class, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no stores and
    * therefore no multiple currencies.
    */

   open C_GET_STORE_CLASS_CURRENCY;
   fetch C_GET_STORE_CLASS_CURRENCY into O_currency_code;

   if C_GET_STORE_CLASS_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_STORE_CLASS_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_STORE_CLASS_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_STORE_CLASS_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_DISTRICT_CURRENCY(O_error_message          OUT VARCHAR2,
                               O_multi_currencies_exist OUT BOOLEAN,
                               O_currency_code          OUT currencies.currency_code%TYPE,
                               I_district               IN  store.district%TYPE)
return BOOLEAN IS

cursor C_GET_DISTRICT_CURRENCY is
   select s1.currency_code
     from store s1
    where s1.district = I_district
      and not exists(select 'x'
                       from store s2
                      where s2.district = I_district
                        and s2.currency_code != s1.currency_code);

BEGIN

   /*
    * This function assumes that there are stores in the passed in district.
    * If there are no stores within the district, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no stores and
    * therefore no multiple currencies.
    */

   open C_GET_DISTRICT_CURRENCY;
   fetch C_GET_DISTRICT_CURRENCY into O_currency_code;

   if C_GET_DISTRICT_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_DISTRICT_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_DISTRICT_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_DISTRICT_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_REGION_CURRENCY(O_error_message          OUT VARCHAR2,
                             O_multi_currencies_exist OUT BOOLEAN,
                             O_currency_code          OUT currencies.currency_code%TYPE,
                             I_region                 IN  region.region%TYPE)
return BOOLEAN IS

cursor C_GET_REGION_CURRENCY is
   select s1.currency_code
     from store s1,
          district d1
    where s1.district = d1.district
      and d1.region   = I_region
      and not exists(select 'x'
                       from store s2,
                            district d2
                      where s2.district       = d2.district
                        and d2.region         = d1.region
                        and s2.currency_code != s1.currency_code);

BEGIN

   /*
    * This function assumes that there are stores in the passed in region.
    * If there are no stores within the region, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no stores and
    * therefore no multiple currencies.
    */

   open C_GET_REGION_CURRENCY;
   fetch C_GET_REGION_CURRENCY into O_currency_code;

   if C_GET_REGION_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_REGION_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_REGION_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_REGION_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_AREA_CURRENCY(O_error_message          OUT VARCHAR2,
                           O_multi_currencies_exist OUT BOOLEAN,
                           O_currency_code          OUT currencies.currency_code%TYPE,
                           I_area                   IN  area.area%TYPE)
return BOOLEAN IS

cursor C_GET_AREA_CURRENCY is
   select s1.currency_code
     from store s1,
          district d1,
          region r1
    where s1.district = d1.district
      and d1.region   = r1.region
      and r1.area     = I_area
      and not exists( select 'x'
                        from store s2,
                             district d2,
                             region r2
                       where s2.district       = d2.district
                         and d2.region         = r2.region
                         and r2.area           = r1.area
                         and s2.currency_code != s1.currency_code);

BEGIN

   /*
    * This function assumes that there are stores in the passed in area.
    * If there are no stores within the area, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no stores and
    * therefore no multiple currencies.
    */

   open C_GET_AREA_CURRENCY;
   fetch C_GET_AREA_CURRENCY into O_currency_code;

   if C_GET_AREA_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_AREA_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_AREA_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_AREA_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ZONE_CURRENCY(O_error_message          OUT VARCHAR2,
                               O_multi_currencies_exist OUT BOOLEAN,
                               O_currency_code          OUT currencies.currency_code%TYPE,
                               I_transfer_zone          IN  store.transfer_zone%TYPE)
return BOOLEAN IS

cursor C_GET_TSF_ZONE_CURRENCY is
   select s1.currency_code
     from store s1
    where s1.transfer_zone = I_transfer_zone
      and not exists(select 'x'
                       from store s2
                      where s2.transfer_zone = I_transfer_zone
                        and s2.currency_code != s1.currency_code);

BEGIN

   /*
    * This function assumes that there are stores in the passed in transfer zone.
    * If there are no stores within the transfer zone, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no stores and
    * therefore no multiple currencies.
    */

   open C_GET_TSF_ZONE_CURRENCY;
   fetch C_GET_TSF_ZONE_CURRENCY into O_currency_code;

   if C_GET_TSF_ZONE_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_TSF_ZONE_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_TSF_ZONE_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_TSF_ZONE_CURRENCY;
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
FUNCTION GET_LOC_TRAIT_CURRENCY(O_error_message          OUT VARCHAR2,
                                O_multi_currencies_exist OUT BOOLEAN,
                                O_currency_code          OUT currencies.currency_code%TYPE,
                                I_loc_trait              IN  loc_traits.loc_trait%TYPE)
return BOOLEAN IS

cursor C_GET_LOC_TRAIT_CURRENCY is
   select s1.currency_code
     from store s1,
          loc_traits_matrix l1
    where l1.store = s1.store
      and l1.loc_trait = I_loc_trait
      and not exists(select 'x'
                       from store s2,
                            loc_traits_matrix l2
                      where l2.store = s2.store
                        and l2.loc_trait = I_loc_trait
                        and s2.currency_code != s1.currency_code);

BEGIN

   /*
    * This function assumes that there are stores in the passed in location trait.
    * If there are no stores within the location trait, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no stores and
    * therefore no multiple currencies.
    */

   open C_GET_LOC_TRAIT_CURRENCY;
   fetch C_GET_LOC_TRAIT_CURRENCY into O_currency_code;

   if C_GET_LOC_TRAIT_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_LOC_TRAIT_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_LOC_TRAIT_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_LOC_TRAIT_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_WH_CURRENCY(O_error_message          OUT VARCHAR2,
                                 O_multi_currencies_exist OUT BOOLEAN,
                                 O_currency_code          OUT currencies.currency_code%TYPE,
                                 I_default_wh             IN  store.default_wh%TYPE)
return BOOLEAN IS

cursor C_GET_DEFAULT_WH_CURRENCY is
   select s1.currency_code
     from store s1
    where s1.default_wh = I_default_wh
      and not exists(select 'x'
                       from store s2
                      where s2.default_wh = I_default_wh
                        and s2.currency_code != s1.currency_code);

BEGIN

   /*
    * This function assumes that there are stores for the passed in default warehouse.
    * If there are no stores for the default warehouse, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no stores and
    * therefore no multiple currencies.
    */

   open C_GET_DEFAULT_WH_CURRENCY;
   fetch C_GET_DEFAULT_WH_CURRENCY into O_currency_code;

   if C_GET_DEFAULT_WH_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_DEFAULT_WH_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_DEFAULT_WH_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_DEFAULT_WH_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_LOCLIST_CURRENCY(O_error_message          IN OUT VARCHAR2,
                              O_multi_currencies_exist IN OUT BOOLEAN,
                              O_currency_code          IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                              I_loc_list               IN     LOC_LIST_DETAIL.LOC_LIST%TYPE,
                              I_loc_type               IN     LOC_LIST_DETAIL.LOC_TYPE%TYPE)
return BOOLEAN IS
---
L_current_loc_type     LOC_LIST_DETAIL.LOC_TYPE%TYPE;
L_store_loc_type_desc  CODE_DETAIL.CODE_DESC%TYPE;
L_wh_loc_type_desc     CODE_DETAIL.CODE_DESC%TYPE;
L_store_exists_ind     VARCHAR2(1)               := 'N';
L_wh_exists_ind        VARCHAR2(1)               := 'N';
L_store_currency_code  STORE.CURRENCY_CODE%TYPE  := NULL;
L_wh_currency_code     WH.CURRENCY_CODE%TYPE     := NULL;
---
cursor C_LOC_EXISTS is
   select 'Y'
     from loc_list_detail
    where loc_type = L_current_loc_type
      and loc_list = I_loc_list;
---
cursor C_GET_STORE_LOCLIST_CURRENCY is
   select s1.currency_code
     from store s1,
          loc_list_detail lld1
    where lld1.location = s1.store
      and lld1.loc_type = 'S'
      and lld1.loc_list = I_loc_list
      and not exists( select 'x'
                        from store s2,
                             loc_list_detail lld2
                       where lld2.location     = s2.store
                         and lld2.loc_list     = lld1.loc_list
                         and lld2.loc_type     = lld1.loc_type
                         and s2.currency_code != s1.currency_code );
---
cursor C_GET_WH_LOCLIST_CURRENCY is
   select w1.currency_code
     from wh w1,
          loc_list_detail lld1
    where lld1.location = w1.wh
      and lld1.loc_type = 'W'
      and lld1.loc_list = I_loc_list
      and not exists( select 'x'
                        from wh w2,
                             loc_list_detail lld2
                       where lld2.location     = w2.wh
                         and lld2.loc_list     = lld1.loc_list
                         and lld2.loc_type     = lld1.loc_type
                         and w2.currency_code != w1.currency_code );
BEGIN
   /*
    * This function assumes that there are locations in the passed in location list.
    * If there are no locations within the location list or if there are no locations
    * within the location list for the specified location type, the function will return false.
    */
   if I_loc_list is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_LOC_LIST',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_loc_type is NULL then
      --- Check if stores, warehouses, or both exist for the location list.
      L_current_loc_type := 'S';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      open C_LOC_EXISTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      fetch C_LOC_EXISTS into L_store_exists_ind;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      close C_LOC_EXISTS;
      ---
      L_current_loc_type := 'W';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      open C_LOC_EXISTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      fetch C_LOC_EXISTS into L_wh_exists_ind;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      close C_LOC_EXISTS;
      ---
      if L_store_exists_ind != 'Y' and L_wh_exists_ind != 'Y' then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'LOTP',
                                       'S',
                                       L_store_loc_type_desc) = FALSE then
            return FALSE;
         end if;
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'LOTP',
                                       'W',
                                       L_wh_loc_type_desc) = FALSE then
            return FALSE;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('NO_LOC_LIST_LOCS_FOR_TYPE',
                                               to_char(I_loc_list),
                                               L_store_loc_type_desc ||'/'|| L_wh_loc_type_desc,
                                               NULL);
         return FALSE;
      end if;
   elsif I_loc_type = 'S' then
      --- Check if stores exist for the location list.
      L_current_loc_type := 'S';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      open C_LOC_EXISTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      fetch C_LOC_EXISTS into L_store_exists_ind;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      close C_LOC_EXISTS;
      if L_store_exists_ind != 'Y' then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'LOTP',
                                       'S',
                                       L_store_loc_type_desc) = FALSE then
            return FALSE;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('NO_LOC_LIST_LOCS_FOR_TYPE',
                                               to_char(I_loc_list),
                                               L_store_loc_type_desc,
                                               NULL);
         return FALSE;
      end if;
   elsif I_loc_type = 'W' then
      --- Check if warehouses exist for the location list.
      L_current_loc_type := 'W';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      open C_LOC_EXISTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      fetch C_LOC_EXISTS into L_wh_exists_ind;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOC_EXISTS',
                       'LOC_LIST_DETAIL',
                       'LOC_LIST: '||to_char(I_loc_list));
      close C_LOC_EXISTS;
      if L_wh_exists_ind != 'Y' then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'LOTP',
                                       'W',
                                       L_wh_loc_type_desc) = FALSE then
            return FALSE;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('NO_LOC_LIST_LOCS_FOR_TYPE',
                                               to_char(I_loc_list),
                                               L_wh_loc_type_desc,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---
   if L_store_exists_ind = 'Y' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_STORE_LOCLIST_CURRENCY',
                       'LOC_LIST_DETAIL, STORE',
                       'LOC_LIST: '||to_char(I_loc_list));
      open C_GET_STORE_LOCLIST_CURRENCY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_STORE_LOCLIST_CURRENCY',
                       'LOC_LIST_DETAIL, STORE',
                       'LOC_LIST: '||to_char(I_loc_list));
      fetch C_GET_STORE_LOCLIST_CURRENCY into L_store_currency_code;
      if C_GET_STORE_LOCLIST_CURRENCY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_STORE_LOCLIST_CURRENCY',
                          'LOC_LIST_DETAIL, STORE',
                          'LOC_LIST: '||to_char(I_loc_list));
         close C_GET_STORE_LOCLIST_CURRENCY;
         O_multi_currencies_exist := TRUE;
         return TRUE;
      else
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_STORE_LOCLIST_CURRENCY',
                          'LOC_LIST_DETAIL, STORE',
                          'LOC_LIST: '||to_char(I_loc_list));
         close C_GET_STORE_LOCLIST_CURRENCY;
      end if;
   end if;
   ---
   if L_wh_exists_ind = 'Y' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_WH_LOCLIST_CURRENCY',
                       'LOC_LIST_DETAIL, WH',
                       'LOC_LIST: '||to_char(I_loc_list));
      open C_GET_WH_LOCLIST_CURRENCY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_WH_LOCLIST_CURRENCY',
                       'LOC_LIST_DETAIL, WH',
                       'LOC_LIST: '||to_char(I_loc_list));
      fetch C_GET_WH_LOCLIST_CURRENCY into L_wh_currency_code;
      if C_GET_WH_LOCLIST_CURRENCY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_WH_LOCLIST_CURRENCY',
                          'LOC_LIST_DETAIL, WH',
                          'LOC_LIST: '||to_char(I_loc_list));
         close C_GET_WH_LOCLIST_CURRENCY;
         O_multi_currencies_exist := TRUE;
         return TRUE;
      else
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_WH_LOCLIST_CURRENCY',
                          'LOC_LIST_DETAIL, WH',
                          'LOC_LIST: '||to_char(I_loc_list));
         close C_GET_WH_LOCLIST_CURRENCY;
      end if;
   end if;
   ---
   if L_store_exists_ind = 'Y' and L_wh_exists_ind = 'Y' then
      if L_store_currency_code != L_wh_currency_code then
         O_multi_currencies_exist := TRUE;
         return TRUE;
      end if;
   end if;
   ---
   O_multi_currencies_exist := FALSE;
   O_currency_code          := NVL(L_store_currency_code, L_wh_currency_code);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_LOCLIST_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_LOCLIST_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_ALL_STORES_CURRENCY(O_error_message          OUT VARCHAR2,
                                O_multi_currencies_exist OUT BOOLEAN,
                                O_currency_code          OUT currencies.currency_code%TYPE)
return BOOLEAN IS

cursor C_GET_ALL_STORES_CURRENCY is
   select s1.currency_code
     from store s1
    where not exists(select 'x'
                       from store s2
                      where s2.currency_code != s1.currency_code);

BEGIN

   /*
    * This function assumes that there are stores in the system.
    * If there are no stores within the system, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no stores and
    * therefore no multiple currencies.
    */

   open C_GET_ALL_STORES_CURRENCY;
   fetch C_GET_ALL_STORES_CURRENCY into O_currency_code;

   if C_GET_ALL_STORES_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_ALL_STORES_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_ALL_STORES_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_ALL_STORES_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_ALL_WH_CURRENCY(O_error_message         OUT VARCHAR2,
                            O_multi_currencies_exist OUT BOOLEAN,
                            O_currency_code          OUT currencies.currency_code%TYPE)
return BOOLEAN IS

cursor C_GET_ALL_WH_CURRENCY is
   select w1.currency_code
     from wh w1
    where not exists(select 'x'
                       from wh w2
                      where w2.currency_code != w1.currency_code);

BEGIN

   /*
    * This function assumes that there are warehouses in the system.
    * If there are no warehouses within the system, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no warehouses and
    * therefore no multiple currencies.
    */

   open C_GET_ALL_WH_CURRENCY;
   fetch C_GET_ALL_WH_CURRENCY into O_currency_code;

   if C_GET_ALL_WH_CURRENCY%NOTFOUND then
      O_multi_currencies_exist := TRUE;
   else
      O_multi_currencies_exist := FALSE;
   end if;

   close C_GET_ALL_WH_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_ALL_WH_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_ALL_WH_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_ALL_LOCS_CURRENCY(O_error_message         OUT VARCHAR2,
                              O_multi_currencies_exist OUT BOOLEAN,
                              O_currency_code          OUT currencies.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_CHECK_MULTICURRENCY IS
   select 'x'
     from store s,
          wh w
    where s.currency_code != w.currency_code;

cursor C_GET_CURRENCY IS
   select currency_code
     from store;

BEGIN

   /*
    * This function assumes that there are locations in the system.
    * If there are no locations within the system, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no locations and
    * therefore no multiple currencies.
    */

   open C_CHECK_MULTICURRENCY;
   fetch C_CHECK_MULTICURRENCY into L_dummy;

   if C_CHECK_MULTICURRENCY%NOTFOUND then
      O_multi_currencies_exist := FALSE;

      open C_GET_CURRENCY;
      fetch C_GET_CURRENCY into O_currency_code;
      close C_GET_CURRENCY;
   else
      O_multi_currencies_exist := TRUE;
   end if;

   close C_CHECK_MULTICURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_ALL_LOCS_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_ALL_LOCS_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_LOCATION_CURRENCY(O_error_message         OUT VARCHAR2,
                               O_multi_currency_exists OUT BOOLEAN,
                               O_currency_code         OUT currencies.currency_code%TYPE,
                               I_group_type            IN  code_detail.code%TYPE,
                               I_group_value           IN  VARCHAR2)
return BOOLEAN IS

   I_loc_type VARCHAR2(1);
   L_partner_desc           PARTNER.PARTNER_DESC%TYPE;
   L_status                 PARTNER.STATUS%TYPE;
   L_principle_country_id   PARTNER.PRINCIPLE_COUNTRY_ID%TYPE;
   L_mfg_id                 PARTNER.MFG_ID%TYPE;
   L_lang                   PARTNER.LANG%TYPE;

BEGIN

   /*
    * This function assumes that there are locations within the passed in group type.
    * If there are no locations within the group type, the multi_currences_exist
    * indicator will return TRUE despite the fact that there are no locations and
    * therefore no multiple currencies.
    */

   if I_group_type = 'S' then
      O_multi_currency_exists := FALSE;

      if GET_STORE_CURRENCY(O_error_message,
                            O_currency_code,
                            I_group_value) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type in('W','PW') then
      O_multi_currency_exists := FALSE;

      if GET_WH_CURRENCY(O_error_message,
                         O_currency_code,
                         I_group_value) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'C' then
      if GET_STORE_CLASS_CURRENCY(O_error_message,
                                  O_multi_currency_exists,
                                  O_currency_code,
                                  I_group_value) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'D' then
      if GET_DISTRICT_CURRENCY(O_error_message,
                               O_multi_currency_exists,
                               O_currency_code,
                               I_group_value) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'R' then
      if GET_REGION_CURRENCY(O_error_message,
                             O_multi_currency_exists,
                             O_currency_code,
                             I_group_value) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'A' then
      if GET_AREA_CURRENCY(O_error_message,
                           O_multi_currency_exists,
                           O_currency_code,
                           I_group_value) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'T' then
      if GET_TSF_ZONE_CURRENCY(O_error_message,
                                    O_multi_currency_exists,
                                    O_currency_code,
                                    I_group_value) = FALSE then
         return FALSE;
       end if;


   elsif I_group_type = 'L' then
      if GET_LOC_TRAIT_CURRENCY(O_error_message,
                                O_multi_currency_exists,
                                O_currency_code,
                                I_group_value) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'DW' then
      if GET_DEFAULT_WH_CURRENCY(O_error_message,
                                 O_multi_currency_exists,
                                 O_currency_code,
                                 I_group_value) = FALSE then
         return FALSE;
       end if;
   elsif (I_group_type = 'LLS') or (I_group_type = 'LLW') then

      if I_group_type = 'LLS' then
         I_loc_type := 'S';
      else
         I_loc_type := 'W';
      end if;

      if GET_LOCLIST_CURRENCY(O_error_message,
                              O_multi_currency_exists,
                              O_currency_code,
                              I_group_value,
                              I_loc_type) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'AS' then
      if GET_ALL_STORES_CURRENCY(O_error_message,
                                 O_multi_currency_exists,
                                 O_currency_code) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'AW' then
      if GET_ALL_WH_CURRENCY(O_error_message,
                             O_multi_currency_exists,
                             O_currency_code) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'AL' then
      if GET_ALL_LOCS_CURRENCY(O_error_message,
                               O_multi_currency_exists,
                               O_currency_code) = FALSE then
         return FALSE;
       end if;
   elsif I_group_type = 'E' then
      if PARTNER_SQL.GET_INFO(O_error_message,
                              L_partner_desc,
                              O_currency_code,
                              L_status,
                              L_principle_country_id,
                              L_mfg_id,
                              L_lang,
                              I_group_value,
                              'E') = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.GET_LOCATION_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_LOCATION_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION STORE_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                               O_exists        OUT BOOLEAN,
                               I_store         IN  store.store%TYPE,
                               I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_STORE_CURRENCY is
   select 'x'
     from store
    where store = I_store
      and currency_code = I_currency_code;

BEGIN
   open C_GET_STORE_CURRENCY;
   fetch C_GET_STORE_CURRENCY into L_dummy;

   if C_GET_STORE_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_STORE_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.STORE_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END STORE_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
FUNCTION WH_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                            O_exists        OUT BOOLEAN,
                            I_wh            IN  wh.wh%TYPE,
                            I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_WH_CURRENCY is
   select 'x'
     from wh
    where wh = I_wh
      and currency_code = I_currency_code;

BEGIN
   open C_GET_WH_CURRENCY;
   fetch C_GET_WH_CURRENCY into L_dummy;

   if C_GET_WH_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_WH_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.WH_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END WH_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION STORE_CLASS_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                     O_exists        OUT BOOLEAN,
                                     I_store_class   IN  store.store_class%TYPE,
                                     I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_STORE_CLASS_CURRENCY is
   select 'x'
     from v_store vs,
          store s
    where vs.store = s.store
      and s.store_class = I_store_class
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_STORE_CLASS_CURRENCY;
   fetch C_GET_STORE_CLASS_CURRENCY into L_dummy;

   if C_GET_STORE_CLASS_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_STORE_CLASS_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.STORE_CLASS_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END STORE_CLASS_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION DISTRICT_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                  O_exists        OUT BOOLEAN,
                                  I_district      IN  store.district%TYPE,
                                  I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_DISTRICT_CURRENCY is
   select 'x'
     from v_store vs,
          store s
    where vs.store = s.store
      and vs.district = I_district
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_DISTRICT_CURRENCY;
   fetch C_GET_DISTRICT_CURRENCY into L_dummy;

   if C_GET_DISTRICT_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_DISTRICT_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.DISTRICT_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END DISTRICT_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION REGION_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                O_exists        OUT BOOLEAN,
                                I_region        IN  region.region%TYPE,
                                I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_REGION_CURRENCY is
   select 'x'
     from v_store vs,
          store s
    where vs.store        = s.store
      and vs.region       = I_region
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_REGION_CURRENCY;
   fetch C_GET_REGION_CURRENCY into L_dummy;

   if C_GET_REGION_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_REGION_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.REGION_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END REGION_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION AREA_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                              O_exists        OUT BOOLEAN,
                              I_area          IN  area.area%TYPE,
                              I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_AREA_CURRENCY is
   select 'x'
     from v_store vs,
          store s
    where vs.store        = s.store
      and vs.area         = I_area
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_AREA_CURRENCY;
   fetch C_GET_AREA_CURRENCY into L_dummy;

   if C_GET_AREA_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_AREA_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.AREA_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END AREA_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION TSF_ZONE_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                  O_exists        OUT BOOLEAN,
                                  I_transfer_zone IN  store.transfer_zone%TYPE,
                                  I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_TSF_ZONE_CURRENCY is
   select 'x'
     from v_store vs,
          store s
    where vs.store        = s.store
      and s.transfer_zone = I_transfer_zone
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_TSF_ZONE_CURRENCY;
   fetch C_GET_TSF_ZONE_CURRENCY into L_dummy;

   if C_GET_TSF_ZONE_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_TSF_ZONE_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.TSF_ZONE_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END TSF_ZONE_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION LOC_TRAIT_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                   O_exists        OUT BOOLEAN,
                                   I_loc_trait     IN  loc_traits.loc_trait%TYPE,
                                   I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_LOC_TRAIT_CURRENCY is
   select 'x'
     from v_store vs,
          store s,
          loc_traits_matrix l
    where l.store = vs.store
      and vs.store = s.store
      and l.loc_trait = I_loc_trait
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_LOC_TRAIT_CURRENCY;
   fetch C_GET_LOC_TRAIT_CURRENCY into L_dummy;

   if C_GET_LOC_TRAIT_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_LOC_TRAIT_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.LOC_TRAIT_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END LOC_TRAIT_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION DEFAULT_WH_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                    O_exists        OUT BOOLEAN,
                                    I_default_wh    IN  store.default_wh%TYPE,
                                    I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_DEFAULT_WH_CURRENCY is
   select 'x'
     from v_store vs,
          store s
    where vs.store        = s.store
      and s.default_wh    = I_default_wh
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_DEFAULT_WH_CURRENCY;
   fetch C_GET_DEFAULT_WH_CURRENCY into L_dummy;

   if C_GET_DEFAULT_WH_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_DEFAULT_WH_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.DEFAULT_WH_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END DEFAULT_WH_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION LOCLIST_ST_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                    O_exists        OUT BOOLEAN,
                                    I_loc_list      IN  loc_list_detail.loc_list%TYPE,
                                    I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_LOCLIST_ST_CURRENCY is
   select 'x'
     from v_store vs,
          store s,
          loc_list_detail l
    where l.location = vs.store
      and vs.store   = s.store
      and l.loc_type = 'S'
      and l.loc_list = I_loc_list
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_LOCLIST_ST_CURRENCY;
   fetch C_GET_LOCLIST_ST_CURRENCY into L_dummy;

   if C_GET_LOCLIST_ST_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_LOCLIST_ST_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.LOCLIST_ST_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END LOCLIST_ST_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_WH
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION LOCLIST_WH_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                    O_exists        OUT BOOLEAN,
                                    I_loc_list      IN  loc_list_detail.loc_list%TYPE,
                                    I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_LOCLIST_WH_CURRENCY is
   select 'x'
     from v_wh vw,
          loc_list_detail l
    where l.location = vw.wh
      and l.loc_type = 'W'
      and l.loc_list = I_loc_list
      and vw.currency_code = I_currency_code;

BEGIN
   open C_GET_LOCLIST_WH_CURRENCY;
   fetch C_GET_LOCLIST_WH_CURRENCY into L_dummy;

   if C_GET_LOCLIST_WH_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_LOCLIST_WH_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.LOCLIST_WH_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END LOCLIST_WH_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION ALL_STORES_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                    O_exists        OUT BOOLEAN,
                                    I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_ALL_STORES_CURRENCY is
   select 'x'
     from v_store vs,
          store s
    where vs.store = s.store
      and s.currency_code = I_currency_code;

BEGIN
   open C_GET_ALL_STORES_CURRENCY;
   fetch C_GET_ALL_STORES_CURRENCY into L_dummy;

   if C_GET_ALL_STORES_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_ALL_STORES_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.ALL_STORES_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ALL_STORES_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_WH
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION ALL_WH_CURRENCY_EXISTS(O_error_message OUT VARCHAR2,
                                O_exists        OUT BOOLEAN,
                                I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

L_dummy VARCHAR2(1);

cursor C_GET_ALL_WH_CURRENCY is
   select 'x'
     from v_wh
    where currency_code = I_currency_code;

BEGIN
   open C_GET_ALL_WH_CURRENCY;
   fetch C_GET_ALL_WH_CURRENCY into L_dummy;

   if C_GET_ALL_WH_CURRENCY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_ALL_WH_CURRENCY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.ALL_WH_CURRENCY_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ALL_WH_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
FUNCTION LOC_CURRENCY_EXISTS(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        OUT BOOLEAN,
                             I_group_type    IN  code_detail.code%TYPE,
                             I_group_value   IN  VARCHAR2,
                             I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN IS

   L_program    VARCHAR2(64) := 'LOCATION_ATTRIB_SQL.LOC_CURRENCY_EXISTS';
   I_loc_type   VARCHAR2(1);

BEGIN
   if I_group_type = 'S' then
      if STORE_CURRENCY_EXISTS(O_error_message,
                               O_exists,
                               I_group_value,
                               I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'W' then
      if WH_CURRENCY_EXISTS(O_error_message,
                            O_exists,
                            I_group_value,
                            I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'C' then
      if STORE_CLASS_CURRENCY_EXISTS(O_error_message,
                                     O_exists,
                                     I_group_value,
                                     I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'D' then
      if DISTRICT_CURRENCY_EXISTS(O_error_message,
                                  O_exists,
                                  I_group_value,
                                  I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'R' then
      if REGION_CURRENCY_EXISTS(O_error_message,
                                O_exists,
                                I_group_value,
                                I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'A' then
      if AREA_CURRENCY_EXISTS(O_error_message,
                              O_exists,
                              I_group_value,
                              I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'T' then
      if TSF_ZONE_CURRENCY_EXISTS(O_error_message,
                                  O_exists,
                                  I_group_value,
                                  I_currency_code) = FALSE then
         return FALSE;
      end if;

   elsif I_group_type = 'L' then
      if LOC_TRAIT_CURRENCY_EXISTS(O_error_message,
                                   O_exists,
                                   I_group_value,
                                   I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'DW' then
      if DEFAULT_WH_CURRENCY_EXISTS(O_error_message,
                                    O_exists,
                                    I_group_value,
                                    I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'LLS' then
      if LOCLIST_ST_CURRENCY_EXISTS(O_error_message,
                                    O_exists,
                                    I_group_value,
                                    I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'LLW' then
      if LOCLIST_WH_CURRENCY_EXISTS(O_error_message,
                                    O_exists,
                                    I_group_value,
                                    I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'AS' then
      if ALL_STORES_CURRENCY_EXISTS(O_error_message,
                                    O_exists,
                                    I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'AW' then
      if ALL_WH_CURRENCY_EXISTS(O_error_message,
                                O_exists,
                                I_currency_code) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END LOC_CURRENCY_EXISTS;
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_VWH(O_error_message   IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_wh              IN      WH.WH%TYPE,    
                    I_email           IN      WH.EMAIL%TYPE,
                    I_vat_region      IN      WH.VAT_REGION%TYPE,
                    I_org_hier_type   IN      WH.ORG_HIER_TYPE%TYPE,
                    I_org_hier_value  IN      WH.ORG_HIER_VALUE%TYPE,
                    I_currency_code   IN      WH.CURRENCY_CODE%TYPE,
                    I_break_pack_ind  IN      WH.BREAK_PACK_IND%TYPE,
                    I_redist_wh_ind   IN      WH.REDIST_WH_IND%TYPE,
                    I_delivery_policy IN      WH.DELIVERY_POLICY%TYPE,
                    I_duns_number     IN      WH.DUNS_NUMBER%TYPE,
                    I_duns_loc        IN           WH.DUNS_LOC%TYPE,
                    I_inbound_handling_days   IN   WH.INBOUND_HANDLING_DAYS%TYPE
                    )
   RETURN BOOLEAN IS

   L_table       VARCHAR2(30) := 'WH';
   L_program     VARCHAR2(64) := 'LOCATION_ATTRIB_SQL.UPDATE_VWH';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_WH IS
      SELECT 'x'
        FROM wh
       WHERE physical_wh = I_wh
         FOR UPDATE NOWAIT;

BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_currency_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_currency_code',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_break_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_break_pack_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_redist_wh_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_redist_wh_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_delivery_policy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_delivery_policy',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_inbound_handling_days is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_inbound_handling_days',
                                            L_program,
                                            NULL);
      return FALSE;
   end if; 
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WH', 'wh', 'physical_wh: ' || I_wh);
   open C_LOCK_WH;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WH', 'wh', NULL);
   close C_LOCK_WH;

      SQL_LIB.SET_MARK('UPDATE', 'NULL', 'wh', 'wh: ' || I_wh );

      UPDATE WH
         SET 
             email           = I_email,
             vat_region      = I_vat_region,
             org_hier_type   = I_org_hier_type,
             org_hier_value  = I_org_hier_value,
             currency_code   = I_currency_code,
             break_pack_ind  = I_break_pack_ind,
             redist_wh_ind   = I_redist_wh_ind,
             delivery_policy = I_delivery_policy,
             duns_number     = I_duns_number,
             duns_loc              = I_duns_loc,
             inbound_handling_days = I_inbound_handling_days
       WHERE physical_wh     = I_wh
         and wh             != I_wh;
   RETURN TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_wh,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_VWH;
-------------------------------------------------------------------------
FUNCTION CHECK_STOCKHOLDING(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_stockhold_ind IN OUT WH.STOCKHOLDING_IND%TYPE,
                            I_loc           IN     VARCHAR2,
                            I_loc_type      IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_dummy STORE.STORE_NAME%TYPE;

BEGIN
   if (CHECK_STOCKHOLDING(O_error_message,
                          O_stockhold_ind,
                          L_dummy,
                          I_loc,
                          I_loc_type) = FALSE) then
      RETURN FALSE;
   end if;

   RETURN TRUE;
END CHECK_STOCKHOLDING;
-------------------------------------------------------------------------
FUNCTION CHECK_STOCKHOLDING(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_stockhold_ind IN OUT WH.STOCKHOLDING_IND%TYPE,
                            O_loc_name      IN OUT VARCHAR2,
                            I_loc           IN     VARCHAR2,
                            I_loc_type      IN     VARCHAR2)
   RETURN BOOLEAN IS

   CURSOR C_WH is
      SELECT 'x'
        FROM wh
       WHERE wh = I_loc
         AND stockholding_ind = 'Y';

   CURSOR C_STORE is
      SELECT 'x'
        FROM store
       WHERE store = I_loc
         AND stockholding_ind = 'Y';

   CURSOR C_STORE_CLASS is
     SELECT 'x'
       FROM store
      WHERE store_class      = I_loc
        AND stockholding_ind = 'Y';

   CURSOR C_DISTRICT is
     SELECT 'x'
       FROM store
      WHERE district         = I_loc
        AND stockholding_ind = 'Y';

   CURSOR C_REGION is
     SELECT 'x'
       FROM store s,
            district d,
            region r
      WHERE s.district         = d.district
        AND d.region           = r.region
        AND r.region           = I_loc
        AND s.stockholding_ind = 'Y';

   CURSOR C_TRANSFER_ZONE is
      SELECT 'x'
       FROM store
      WHERE transfer_zone    = I_loc
        AND stockholding_ind = 'Y';

   CURSOR C_LOC_TRAITS is
      SELECT 'x'
        FROM store s,
             loc_traits_matrix l
       WHERE s.store          = l.store
         AND l.loc_trait      = I_loc
         AND stockholding_ind = 'Y';

   CURSOR C_DEFAULT_WAREHOUSE is
      SELECT 'x'
        FROM store
       WHERE default_wh       = I_loc
         AND stockholding_ind = 'Y';

   CURSOR C_ALL_STORES is
      SELECT 'x'
        FROM store
       WHERE stockholding_ind = 'Y';

   CURSOR C_LOC_LIST_ST is
      SELECT 'x'
        FROM store s,
             loc_list_detail l
       WHERE s.store            = l.location
         AND l.loc_type         = 'S'
         AND l.loc_list         = I_loc
         AND s.stockholding_ind = 'Y';

   CURSOR C_LOC_LIST_WH is
         SELECT 'x'
           FROM loc_list_detail l
       WHERE l.loc_type = 'W'
         AND l.loc_list = I_loc;

   CURSOR C_EXTERNAL_FINISHER is
      SELECT 'x'
        FROM partner
       WHERE partner_id = I_loc
         AND partner_type = 'E';

   CURSOR C_ALL_EXTERNAL_FINISHERS is
      SELECT 'x'
        FROM partner
       WHERE partner_type = 'E';

   L_dummy    varchar2(1);

BEGIN

   if (I_loc_type not in ('AW','AS','C','AE')) then
      if (GET_NAME(O_error_message,
                   O_loc_name,
                   I_loc,
                   I_loc_type) = FALSE) then
         return FALSE;
      end if;
   elsif I_loc_type = 'C' then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'CSTR',
                                    I_loc,
                                    O_loc_name) = FALSE then
         return FALSE;
      end if;
   end if;

   if (I_loc_type = 'W') then
      open C_WH;
      fetch C_WH into L_dummy;
      if (C_WH%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_WH;
   elsif (I_loc_type = 'S') then
      open C_STORE;
      fetch C_STORE into L_dummy;
      if (C_STORE%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_STORE;
   elsif (I_loc_type = 'C') then
      open C_STORE_CLASS;
      fetch C_STORE_CLASS into L_dummy;
      if (C_STORE_CLASS%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_STORE_CLASS;
   elsif (I_loc_type = 'R') then
      open C_REGION;
      fetch C_REGION into L_dummy;
      if (C_REGION%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_REGION;
   elsif (I_loc_type = 'D') then
      open C_DISTRICT;
      fetch C_DISTRICT into L_dummy;
      if (C_DISTRICT%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_DISTRICT;
   elsif (I_loc_type = 'T') then
      open C_TRANSFER_ZONE;
      fetch C_TRANSFER_ZONE into L_dummy;
      if (C_TRANSFER_ZONE%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_TRANSFER_ZONE;
   elsif (I_loc_type = 'L') then
      open C_LOC_TRAITS;
      fetch C_LOC_TRAITS into L_dummy;
      if (C_LOC_TRAITS%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_LOC_TRAITS;
   elsif (I_loc_type = 'DW') then
      open C_DEFAULT_WAREHOUSE;
      fetch C_DEFAULT_WAREHOUSE into L_dummy;
      if (C_DEFAULT_WAREHOUSE%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_DEFAULT_WAREHOUSE;
   elsif (I_loc_type = 'AS') then
      open C_ALL_STORES;
      fetch C_ALL_STORES into L_dummy;
      if (C_ALL_STORES%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_ALL_STORES;
   elsif (I_loc_type = 'LLS') then
      open C_LOC_LIST_ST;
      fetch C_LOC_LIST_ST into L_dummy;
      if (C_LOC_LIST_ST%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_LOC_LIST_ST;
   elsif (I_loc_type = 'LLW') then
      open C_LOC_LIST_WH;
      fetch C_LOC_LIST_WH into L_dummy;
      if (C_LOC_LIST_WH%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;
      close C_LOC_LIST_WH;

   elsif (I_loc_type = 'AW') then
      O_stockhold_ind := 'Y';

   elsif (I_loc_type = 'E') then
      open C_EXTERNAL_FINISHER;
      fetch C_EXTERNAL_FINISHER into L_dummy;
      if (C_EXTERNAL_FINISHER%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;

   elsif (I_loc_type = 'AE') then
      open C_ALL_EXTERNAL_FINISHERS;
      fetch C_ALL_EXTERNAL_FINISHERS into L_dummy;
      if (C_ALL_EXTERNAL_FINISHERS%FOUND) then
         O_stockhold_ind := 'Y';
      else
         O_stockhold_ind := 'N';
      end if;

   end if;

   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.check_stockholding',
                                             to_char(SQLCODE));
      RETURN FALSE;
END check_stockholding;
-------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC_HIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_loc_name        IN OUT WH.WH_NAME%TYPE,
                             I_loc             IN     VARCHAR2,
                             I_loc_type        IN     VARCHAR2)
   RETURN BOOLEAN IS

   CURSOR C_WH is
      select distinct 'X'
        from item_loc_hist il, wh wh
       where wh.wh = I_loc
         and wh.wh = il.loc
         and wh.stockholding_ind = 'Y';


   CURSOR C_STORE is
      SELECT distinct 'X'
        FROM item_loc_hist il, store s
       WHERE s.store = I_loc
         AND s.store = il.loc;

   L_dummy    varchar2(1);

BEGIN

   if (I_loc_type not in ('S','W')) then
      if (GET_NAME(O_error_message,
                   O_loc_name,
                   I_loc,
                   I_loc_type) = FALSE) then
         return FALSE;
      end if;
   end if;

   if (I_loc_type = 'W') then
      open C_WH;
      fetch C_WH into L_dummy;
         if (C_WH%FOUND) then
            if (GET_NAME(O_error_message,
                         O_loc_name,
                         I_loc,
                         I_loc_type) = FALSE) then
                return FALSE;
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_HIST',NULL,NULL,NULL);
            close C_WH;
            return FALSE;
         end if;
      close C_WH;
   elsif (I_loc_type = 'S') then
      open C_STORE;
      fetch C_STORE into L_dummy;
         if (C_STORE%FOUND) then
            if (GET_NAME(O_error_message,
                         O_loc_name,
                         I_loc,
                         I_loc_type) = FALSE) then
                return FALSE;
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_HIST',NULL,NULL,NULL);
            close C_STORE;
            return FALSE;
         end if;
      close C_STORE;
   end if;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.check_item_loc_hist',
                                             to_char(SQLCODE));
      RETURN FALSE;
END check_item_loc_hist;

-------------------------------------------------------------------------
FUNCTION GET_ENTITY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_entity_id       IN OUT TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                    O_entity_name     IN OUT TSF_ENTITY.TSF_ENTITY_DESC%TYPE,
                    I_loc             IN     VARCHAR2,
                    I_loc_type        IN     VARCHAR2)
RETURN BOOLEAN IS

   CURSOR C_WH is
      select e.tsf_entity_id, e.tsf_entity_desc
        from wh wh, tsf_entity e
       where wh.wh = I_loc
         and wh.tsf_entity_id = e.tsf_entity_id;

   CURSOR C_STORE is
      select e.tsf_entity_id, e.tsf_entity_desc
        from store s, tsf_entity e
       where s.store = I_loc
         and s.tsf_entity_id = e.tsf_entity_id;

   CURSOR C_EXTERNAL_FINISHER is
      select e.tsf_entity_id, e.tsf_entity_desc
        from partner p, tsf_entity e
       where p.partner_id = I_loc
         and p.tsf_entity_id = e.tsf_entity_id
         and p.partner_type = 'E';


BEGIN

   if I_loc IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('LOC_REQ', null, null, null);
      return FALSE;
   end if;

   if I_loc_type = 'W' then
      OPEN C_WH;
      FETCH C_WH into O_entity_id, O_entity_name;
      if C_WH%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ENTITY_FOUND_FOR_LOC',I_loc,NULL,NULL);
         CLOSE C_WH;
         return FALSE;
      end if;
      CLOSE C_WH;
   elsif I_loc_type = 'S' then
      OPEN C_STORE;
      FETCH C_STORE into O_entity_id, O_entity_name;
      if C_STORE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ENTITY_FOUND_FOR_LOC',I_loc,NULL,NULL);
         CLOSE C_STORE;
         return FALSE;
      end if;
      CLOSE C_STORE;
   elsif I_loc_type = 'E' then
      OPEN C_EXTERNAL_FINISHER;
      FETCH C_EXTERNAL_FINISHER into O_entity_id, O_entity_name;
      if C_EXTERNAL_FINISHER%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ENTITY_FOUND_FOR_LOC',I_loc,NULL,NULL);
         CLOSE C_EXTERNAL_FINISHER;
         return FALSE;
      end if;
      CLOSE C_EXTERNAL_FINISHER;
   else
      O_error_message := SQL_LIB.CREATE_MSG('LOC_TYPE_NOT_VALID',I_loc_type,NULL,NULL);
   end if;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'LOCATION_ATTRIB_SQL.get_entity',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_ENTITY;

-------------------------------------------------------------------------
FUNCTION GET_FINISHER_NAME(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists        IN OUT BOOLEAN,
                           O_finisher_name IN OUT PARTNER.PARTNER_DESC%TYPE,
                           I_finisher      IN     VARCHAR2,
                           I_finisher_type IN     VARCHAR2)
RETURN BOOLEAN IS

BEGIN
   if I_finisher_type = 'I' then
      if WH_ATTRIB_SQL.CHECK_VWH(O_error_message,
                                 O_exists,
                                 I_finisher,
                                 'N') = FALSE then
         return FALSE;
      end if;

      if NOT O_exists then  --not a VHW
         O_error_message := SQL_LIB.CREATE_MSG('INV_INT_FINISHER',null,null,null);
         return FALSE;
      end if;
      ---
      if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                      O_exists,
                                      O_finisher_name,
                                      I_finisher) = FALSE then
         return FALSE;
      end if;

      if NOT O_exists then  --not a finisher
         O_error_message := SQL_LIB.CREATE_MSG('INV_INT_FINISHER',null,null,null);
         return FALSE;
      end if;

   elsif I_finisher_type = 'E' then
      if PARTNER_SQL.GET_DESC(O_error_message,
                              O_finisher_name,
                              I_finisher,
                              'E') = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

END GET_FINISHER_NAME;
-------------------------------------------------------------------------
FUNCTION ID_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists           IN OUT   BOOLEAN,
                    I_location_id      IN       NUMBER)
   RETURN BOOLEAN IS

   cursor C_LOC_ID_EXISTS is
      select 'x'
        from store
       where store = I_location_id
         and rownum = 1
       union all
      select 'x'
        from store_add
       where store = I_location_id
         and rownum = 1
       union all
      select 'x'
        from wh
       where wh = I_location_id
         and rownum = 1
       union all
      select 'x'
        from partner
       where partner_id = TO_CHAR(I_location_id)
         and partner.partner_type = 'E'
         and rownum = 1;

   L_program     VARCHAR2(64) := 'LOCATION_ATTRIB_SQL.ID_EXISTS';
   L_exists      VARCHAR2(1)  := 'Y';

BEGIN
   open C_LOC_ID_EXISTS;
   fetch C_LOC_ID_EXISTS into L_exists;
   close C_LOC_ID_EXISTS;

   O_exists := (L_exists = 'x');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END ID_EXISTS;
-------------------------------------------------------------------------
FUNCTION CHECK_TSF_ENTITY_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_ok_to_change  IN OUT BOOLEAN,
                                 I_location_id   IN     NUMBER)
   RETURN BOOLEAN IS

   L_sumqty      NUMBER;
   L_dummy       VARCHAR2(1);
   L_program     VARCHAR2(64) := 'LOCATION_ATTRIB_SQL.CHECK_TSF_ENTITY_CHANGE';

   cursor C_LOCATION_CHECK is
      select 'x'
        from ordhead
       where location = I_location_id
         and status != 'C'
         and rownum = 1
       union
      select 'x'
        from ordhead oh,
             ordloc ol
       where ol.location = I_location_id
         and oh.status != 'C'
         and oh.order_no = ol.order_no
         and rownum = 1
       union
      select 'x'
        from tsfhead
       where (from_loc = I_location_id
          or to_loc = I_location_id)
         and status not in ('C', 'D')
         and rownum = 1
       union
      select 'x'
        from alloc_header
       where wh = I_location_id
         and status != 'C'
         and rownum = 1
       union
      select 'x'
        from alloc_header ah,
             alloc_detail ad
       where ad.to_loc = I_location_id
         and ah.status != 'C'
         and ah.alloc_no = ad.alloc_no
         and rownum = 1;

   cursor C_SOH_QTY_CHECK is
      select sum(stock_on_hand + in_transit_qty + tsf_reserved_qty + tsf_expected_qty)
        from item_loc_soh
       where loc = I_location_id;

BEGIN
   if I_location_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG( 'REQUIRED_INPUT_IS_NULL',
                                             'I_location_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_LOCATION_CHECK;

   fetch C_LOCATION_CHECK into L_dummy;

   if C_LOCATION_CHECK%FOUND then
      O_ok_to_change := FALSE;
   else
      open C_SOH_QTY_CHECK;
      fetch C_SOH_QTY_CHECK into L_sumqty;
      if (L_sumqty > 0) then
         O_ok_to_change := FALSE;
      else
         O_ok_to_change := TRUE;
      end if;
      close C_SOH_QTY_CHECK;
   end if;

   close C_LOCATION_CHECK;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CHECK_TSF_ENTITY_CHANGE;
-------------------------------------------------------------------------
FUNCTION CHECK_CLOSE_DATE(I_store_wh        IN LOCATION_CLOSED.LOCATION%TYPE,
                          I_loc_type        IN LOCATION_CLOSED.LOC_TYPE%TYPE,
                          I_date            IN DATE,
                          O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_open_close_ind  IN OUT VARCHAR2)
RETURN BOOLEAN IS

L_dummy          VARCHAR2(1)  := NULL;
L_program        VARCHAR2(64) := 'LOCATION_ATTRIB_SQL.CHECK_CLOSE_DATE';

cursor C_LOCATION_CLOSED is
      select 'x'
        from location_closed
       where location = I_store_wh
         and loc_type =I_loc_type
		 and close_date = I_date
         and recv_ind = 'Y';
BEGIN     
   open C_LOCATION_CLOSED;
   fetch C_LOCATION_CLOSED into L_dummy;
   close C_LOCATION_CLOSED;

   if L_dummy is not null then     
         if I_LOC_TYPE ='S' then
            O_error_message :=
            sql_lib.create_msg('STORE_CLOSED',to_char(I_STORE_WH ),NULL,NULL);
            O_open_close_ind := 'C';
         else
            O_error_message :=
            sql_lib.create_msg('WAREHOUSE_CLOSED',to_char(I_STORE_WH ),NULL,NULL);
            O_open_close_ind := 'C';
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_CLOSE_DATE;
-------------------------------------------------------------------------
FUNCTION CHECK_STORE_WH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists          IN OUT BOOLEAN)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'LOCATION_ATTRIB_SQL.CHECK_STORE_WH';
   L_exists              VARCHAR2(1)  := 'N';

cursor C_VWH_EXISTS is
   select 'Y'
     from store
union all
   select 'Y'
     from wh
    where wh <> physical_wh;

BEGIN
   open C_VWH_EXISTS;
   fetch C_VWH_EXISTS into L_exists;

   if C_VWH_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_VWH_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_STORE_WH;        
--------------------------------------------------------------------------
FUNCTION GET_LOC_COUNTRY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_country_id      IN OUT COUNTRY.COUNTRY_ID%TYPE,
                         I_store_wh        IN     LOCATION_CLOSED.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'LOCATION_ATTRIB_SQL.GET_LOC_COUNTRY';
   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;

   cursor C_GET_COUNTRY is
      select country_id
        from addr
       where module in ('ST','WH','WFST')
         and key_value_1 = DECODE(module, 'WH' ,(select physical_wh from wh where wh = TO_CHAR(I_store_wh))
                                               ,TO_CHAR(I_store_wh))
         and primary_addr_ind = 'Y'
         and addr_type        = DECODE(module,'WFST','07','01');


BEGIN
   O_country_id := NULL;
   --
   open  C_GET_COUNTRY;
   fetch C_GET_COUNTRY into O_country_id;
   close C_GET_COUNTRY;
   --
   if O_country_id is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS (O_error_message,
                                                L_system_options_row) = FALSE then
         return FALSE;
      end if;
      --
      O_country_id := L_system_options_row.base_country_id;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_LOC_COUNTRY;
-------------------------------------------------------------------------
END LOCATION_ATTRIB_SQL;
/
