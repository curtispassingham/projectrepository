CREATE OR REPLACE PACKAGE CORESVC_XPRICE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
STATUS_NEW            VARCHAR2(1) := 'N';
STATUS_IN_PROGRESS    VARCHAR2(1) := 'I';
STATUS_EXPLODED       VARCHAR2(1) := 'X';
STATUS_ERROR          VARCHAR2(1) := 'E';
STATUS_PROCESSED      VARCHAR2(1) := 'P';
STATUS_SKIPPED        VARCHAR2(1) := 'S';

TYPE XPRICE_TEMP_TBL is TABLE of SVC_PRICING_EVENT_TEMP%ROWTYPE INDEX BY BINARY_INTEGER;

-------------------------------------------------------------------------------
--    Name: EXPLODE_DETAILS
-- Purpose: This function will explode pricing records down to Transactional
--          Item and store/wh level.Called from RIB Subscription API.
-------------------------------------------------------------------------------
FUNCTION EXPLODE_DETAILS(O_error_message   IN OUT VARCHAR2,
                         I_process_id      IN     NUMBER)
return BOOLEAN;
-------------------------------------------------------------------------------
--    Name: EXPLODE_DETAILS
-- Purpose: Overloaded form of previous function.Called from Batch.
-------------------------------------------------------------------------------
FUNCTION EXPLODE_DETAILS(O_error_message   IN OUT VARCHAR2,
                         I_effective_date  IN     DATE)
return BOOLEAN;
-------------------------------------------------------------------------------
--    Name: PROCESS_DETAILS
-- Purpose: This function will process item loc record for the given price event.
--          Called from RIB Subscription API.
-------------------------------------------------------------------------------
FUNCTION PROCESS_DETAILS(O_error_message   IN OUT VARCHAR2,
                         I_process_id      IN     NUMBER)
return BOOLEAN;
-------------------------------------------------------------------------------
--    Name: PROCESS_DETAILS
-- Purpose: Overloaded form of previous function.Called from Batch.
-------------------------------------------------------------------------------
FUNCTION PROCESS_DETAILS(O_error_message   IN OUT VARCHAR2,
                         I_thread_val      IN     NUMBER,
                         I_chunk_id        IN     NUMBER,
                         I_batch_ind       IN     VARCHAR2 DEFAULT 'Y')
return BOOLEAN;
-------------------------------------------------------------------------------
--    Name: CHUNK_PRICE_EVENT
-- Purpose: This function contains the threading and chunking logic for batch 
--          processing of Price Events
-------------------------------------------------------------------------------
FUNCTION CHUNK_PRICE_EVENT(O_error_message   IN OUT   NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------
END CORESVC_XPRICE_SQL;
/