
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
   CREATE OR REPLACE PACKAGE DIFF_ID_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
-- Function Name: CHECK_DELETE
-- Purpose	: Checks to see if the diff_id a user is attempting to delete
--                exists on the ITEM_MASTER or the DIFF_GROUP_HEAD tables.
-- Calls	: <none>
-- Input Values   : O_error_message          --an IN OUT parameter
--                : O_items_exist            --an IN OUT parameter
--                : O_group_details_exist    --an IN OUT parameter
--                : O_range_details_exist    --an IN OUT parameter
--                : I_diff_id                --an IN parameter
-- Created	: 15-Aug-00  Girish Tekwani
----------------------------------------------------------------
   FUNCTION CHECK_DELETE (O_error_message             IN OUT    VARCHAR2,
                          O_items_exist               IN OUT    BOOLEAN,
                          O_group_details_exist       IN OUT    BOOLEAN,
                          O_range_details_exist       IN OUT    BOOLEAN,
                          I_diff_id                   IN        DIFF_IDS.DIFF_ID%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------
-- Function Name: DIFF_ID_EXISTS
-- Purpose      : Checks to see if the passed in diff_id exists on DIFF_IDS or
--                DIFF_GROUP_HEAD tables.
-- Calls        : <none>
-- Input Values : O_error_message          --an IN OUT parameter
--              : O_exists                 --an IN OUT parameter
--              : I_diff_id                --an IN parameter
-- Created      : 27-Sep-00  Girish Tekwani
----------------------------------------------------------------
   FUNCTION DIFF_ID_EXISTS (O_error_message         IN OUT VARCHAR2,
                            O_exists                IN OUT BOOLEAN,
                            I_diff_id               IN     DIFF_IDS.DIFF_ID%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function    : INSERT_DIFFID
-- Purpose     : Takes in a diff_id table record and inserts all of
--               it's contents into the DIFF_IDS table.
-- Created     : 06-Aug-03 Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION INSERT_DIFFID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec    IN     DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function    : UPDATE_DIFFID
-- Purpose     : Takes in a diff_id table record and updates all of
--               it's contents into the DIFF_IDS table.
-- Created     : 06-Aug-03 Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION UPDATE_DIFFID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec    IN     DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function    : DELETE_DIFFID
-- Purpose     : Takes in a diff_id table record and deletes all of
--               it's contents from the DIFF_IDS table.
-- Created     : 06-Aug-03 Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION DELETE_DIFFID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec    IN     DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   END DIFF_ID_SQL;
/


