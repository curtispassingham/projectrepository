CREATE OR REPLACE PACKAGE RMSSUB_XPRCEVENT AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
--PUBLIC VARIABLES
---------------------------------------------------------------------------------------------
 LP_prc_chg_cre           VARCHAR2(30) := 'xprceventcre';
 LP_prc_chg_mod           VARCHAR2(30) := 'xprceventmod';
 LP_prc_chg_del           VARCHAR2(30) := 'xprceventdel'; 
   
---------------------------------------------------------------------------------------------
--PUBLIC FUNCTION
---------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2);
---------------------------------------------------------------------------------------------
END RMSSUB_XPRCEVENT;
/
