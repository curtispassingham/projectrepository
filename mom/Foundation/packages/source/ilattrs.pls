
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEMLIST_ATTRIB_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------
-- Function:    GET_NAME
-- Purpose:     To validate the skulist that is passed to the function and pass back the name of
--              the itemlist to the calling form.
-------------------------------------------------------------------
FUNCTION GET_NAME (O_error_message   IN OUT   VARCHAR2,
                   I_itemlist        IN       NUMBER,
                   O_list_name       IN OUT   VARCHAR2)
   RETURN BOOLEAN;

-------------------------------------------------------------------
-- Function Name: GET_ITEM_COUNT
-- Purpose      : get the total number of items in a skulist.
-- Calls        : None
-------------------------------------------------------------------
FUNCTION GET_ITEM_COUNT(O_error_message   IN OUT   VARCHAR2,
                        I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE,
                        O_count           IN OUT   NUMBER)
   RETURN BOOLEAN;

-------------------------------------------------------------------
-- Function Name: GET_DEPOSIT_ITEM_COUNT
-- Purpose      : get the total number of deposit items in a skulist.
-- Calls        : None
-------------------------------------------------------------------
FUNCTION GET_DEPOSIT_ITEM_COUNT(O_error_message   IN OUT   VARCHAR2,
                                I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE,
                                I_deposit_type    IN       ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE,
                                O_count           IN OUT   NUMBER)
   RETURN BOOLEAN;

-------------------------------------------------------------------
-- Function Name: GET_HEADER_INFO
-- Purpose      : get the header information, with hierarchy filtering values.
-- Calls        : None
-------------------------------------------------------------------
FUNCTION GET_HEADER_INFO (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_skulist_desc            IN OUT   SKULIST_HEAD.SKULIST_DESC%TYPE,
                          O_create_date             IN OUT   SKULIST_HEAD.CREATE_DATE%TYPE,
                          O_last_rebuild_date       IN OUT   SKULIST_HEAD.LAST_REBUILD_DATE%TYPE,
                          O_create_id               IN OUT   SKULIST_HEAD.CREATE_ID%TYPE,
                          O_static_ind              IN OUT   SKULIST_HEAD.STATIC_IND%TYPE,
                          O_comment_desc            IN OUT   SKULIST_HEAD.COMMENT_DESC%TYPE,
                          O_filter_org_id           IN OUT   SKULIST_HEAD.FILTER_ORG_ID%TYPE,
                          I_itemlist                IN       SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------
-- Function Name: GET_HEADER_INFO
-- Purpose      : get the header information
-- Calls        : None
-------------------------------------------------------------------
FUNCTION GET_HEADER_INFO (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_skulist_desc            IN OUT   SKULIST_HEAD.SKULIST_DESC%TYPE,
                          O_create_date             IN OUT   SKULIST_HEAD.CREATE_DATE%TYPE,
                          O_last_rebuild_date       IN OUT   SKULIST_HEAD.LAST_REBUILD_DATE%TYPE,
                          O_create_id               IN OUT   SKULIST_HEAD.CREATE_ID%TYPE,
                          O_static_ind              IN OUT   SKULIST_HEAD.STATIC_IND%TYPE,
                          O_comment_desc            IN OUT   SKULIST_HEAD.COMMENT_DESC%TYPE,
                          I_itemlist                IN       SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------
-- Function Name: ITEM_ON_CONSIGNMENT_EXISTS
-- Purpose      : Check if any items of passed in itemlist are on consignment.
-- Calls        : None
-------------------------------------------------------------------
FUNCTION ITEM_ON_CONSIGNMENT_EXISTS(O_error_message   IN OUT   VARCHAR2,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------
-- Function Name: ITEM_ON_CONSIGNMENT_EXISTS
-- Purpose      : Check if any items of passed in itemlist are on consignment.
-- Calls        : None
-------------------------------------------------------------------
FUNCTION PACK_ITEM_EXISTS(O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------

-- Name   : ITEM_ON_CRITERIA
-- Purpose: This function checks to see if the item is on the
--          skulist criteria.
----------------------------------------------------------------
FUNCTION ITEM_ON_CRITERIA(O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          O_action_type     IN OUT   SKULIST_CRITERIA.ACTION_TYPE%TYPE,
                          I_skulist         IN       SKULIST_CRITERIA.SKULIST%TYPE,
                          I_item            IN       SKULIST_CRITERIA.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name   : CRITERIA_EXISTS
-- Purpose: This function checks to see if criteria exists for
--          the given skulist.
----------------------------------------------------------------
FUNCTION CRITERIA_EXISTS(O_error_message   IN OUT   VARCHAR2,
                         O_exists          IN OUT   BOOLEAN,
                         I_skulist         IN       SKULIST_CRITERIA.SKULIST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name   : SKULIST_DEPT_EXISTS
-- Purpose: This function set the variable O_dept_exists = TRUE if
--          I_dept exists in skulist_dept where skulist = I_itemlist
--          else set it to FALSE.
----------------------------------------------------------------
FUNCTION SKULIST_DEPT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_dept_exists     IN OUT   BOOLEAN,
                             I_itemlist        IN       SKULIST_DEPT.SKULIST%TYPE,
                             I_dept            IN       SKULIST_DEPT.DEPT%TYPE,
                             I_class           IN       SKULIST_DEPT.CLASS%TYPE DEFAULT NULL,
                             I_subclass        IN       SKULIST_DEPT.SUBCLASS%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name   : ITEM_IN_SKULIST
-- Purpose         : This function will verify whether or not a given item
--                   exists in an itemlist. It will also return the skulist
--                   id that the item belongs to.
-- Calls           : UPDATE_ITEM_LIST
-- Created by      : Portia Dela Rosa
-- Date            : 09-Sept-03
----------------------------------------------------------------
FUNCTION ITEM_IN_SKULIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item            IN       SKULIST_DETAIL.ITEM%TYPE,
                         I_dept            IN       SKULIST_DEPT.DEPT%TYPE,
                         I_class           IN       SKULIST_DEPT.CLASS%TYPE,
                         I_subclass        IN       SKULIST_DEPT.SUBCLASS%TYPE)
 RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: UPDATE_ITEM_LIST
-- Purpose      : This function will update the SKULIST_DEPT table by adding the new department
--                of an item that is included in an item list.
-- Created by   : Portia Dela Rosa
-- Date         : 09-Sept-03
----------------------------------------------------------------
FUNCTION UPDATE_ITEM_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_skulist         IN       SKULIST_HEAD.SKULIST%TYPE,
                          I_dept            IN       DEPS.DEPT%TYPE,
                          I_class           IN       SKULIST_DEPT.CLASS%TYPE,
                          I_subclass        IN       SKULIST_DEPT.SUBCLASS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: GET_INVENTORY_ITEM_COUNT
-- Purpose      : Get the total number of items in a skulist that are inventory items.
-- Calls        : None
-------------------------------------------------------------------
FUNCTION GET_INVENTORY_ITEM_COUNT(O_error_message   IN OUT   VARCHAR2,
                                  I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE,
                                  O_count           IN OUT   NUMBER)
   RETURN BOOLEAN;
----------------------------------------------------------------
FUNCTION NON_VENDOR_PACKS_EXIST(O_error_message   IN OUT   VARCHAR2,
                                O_exists             OUT   BOOLEAN,
                                I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE)
  RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: DELETE_SKULIST_HEAD_TL
-- Purpose      : This function is used to delete values from SKULIST_HEAD_TL.
-- Calls        : None
-------------------------------------------------------------------
FUNCTION DELETE_SKULIST_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_skulist         IN       SKULIST_HEAD_TL.SKULIST%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------
END ITEMLIST_ATTRIB_SQL;
/


