CREATE OR REPLACE PACKAGE BODY PRICING_SQL AS

LP_markup           NUMBER := 11;
LP_markdown_cancel  NUMBER := 14;

--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------
FUNCTION BULK_INSERT_TRAN_DATA_PACK(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_data_rec     IN       TRAN_DATA_REC,
                                    I_item_attributes   IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                                    I_loc_type          IN       TRAN_DATA.LOC_TYPE%TYPE,
                                    I_program_name      IN       VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION BULK_UPDATE_ITEM_LOC_PC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item_locs       IN       PRICE_HIST_SQL.ITEM_LOCS_REC,
                                 I_new_prices      IN       PRICE_HIST_SQL.NEW_PRICE_REC)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'PRICING_SQL.BULK_UPDATE_ITEM_LOC_PC';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_user            VARCHAR2(20) := GET_USER;
   L_date            DATE         := SYSDATE;

   L_item_loc_rowid  ROWID;

   cursor C_LOCK_ITMLOC is
      select 'x'
        from item_loc
       where rowid = L_item_loc_rowid
         for update nowait;

BEGIN

   FOR i in I_item_locs.item_loc_rowids.FIRST..I_item_locs.item_loc_rowids.LAST LOOP
      L_item_loc_rowid := I_item_locs.item_loc_rowids(i);

      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITMLOC','item',null);
      open  C_LOCK_ITMLOC;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITMLOC','item',null);
      close C_LOCK_ITMLOC;
   end LOOP;

   FORALL i in I_item_locs.item_loc_rowids.FIRST..I_item_locs.item_loc_rowids.LAST
      update item_loc
         set unit_retail          = I_new_prices.standard_unit_retail,
             regular_unit_retail  = I_new_prices.standard_unit_retail,
             selling_unit_retail  = I_new_prices.selling_unit_retail,
             selling_uom          = I_new_prices.selling_uom,
             multi_units          = I_new_prices.multi_units,
             multi_unit_retail    = I_new_prices.multi_unit_retail,
             multi_selling_uom    = I_new_prices.multi_selling_uom,
             clear_ind            = I_new_prices.clearance_ind,
             last_update_datetime = L_date,
             last_update_id       = L_user
       where rowid = I_item_locs.item_loc_rowids(i);
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                               NULL,
                                                               NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BULK_UPDATE_ITEM_LOC_PC;
--------------------------------------------------------------------------------
FUNCTION BULK_INSERT_TRAN_DATA_PACK(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_data_rec     IN       TRAN_DATA_REC,
                                    I_item_attributes   IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                                    I_loc_type          IN       TRAN_DATA.LOC_TYPE%TYPE,
                                    I_program_name      IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'PRICING_SQL.BULK_INSERT_TRAN_DATA_PACK';

   L_tomorrow        DATE := GET_VDATE + 1;
   L_tran_code       TRAN_DATA.TRAN_CODE%TYPE;
   L_total_cost      TRAN_DATA.TOTAL_COST%TYPE := NULL;

BEGIN

   -- Packs are not optimized for performance for tran_data inserts. 
   -- It's calling STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT, instead of 
   -- STKLEDGR_PRICING_SQL.BULK_INSERT_TRAN_DATA. 

   if STKLEDGR_SQL.INIT_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if I_tran_data_rec.items is not NULL then
      FOR i in I_tran_data_rec.items.FIRST..I_tran_data_rec.items.LAST LOOP
         L_tran_code := I_tran_data_rec.tran_codes(i);

         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_tran_data_rec.items(i),
                                                I_item_attributes.dept,
                                                I_item_attributes.class,
                                                I_item_attributes.subclass,
                                                I_tran_data_rec.locs(i),
                                                I_loc_type,
                                                L_tomorrow,
                                                L_tran_code,
                                                NULL,  --  adj_code
                                                I_tran_data_rec.units(i),
                                                L_total_cost,
                                                I_tran_data_rec.total_retails(i),
                                                NULL,  --  ref_no1
                                                NULL,  --  ref_no2
                                                NULL,  --  tsf_source_location
                                                NULL,  --  tsf_source_loc_type
                                                I_tran_data_rec.old_unit_retails(i),
                                                I_tran_data_rec.new_unit_retails(i),
                                                NULL,  --  source_dept
                                                NULL,  --  source_class
                                                NULL,  --  source_subclass
                                                I_program_name,
                                                NULL) = FALSE then  --   gl_ref_no

            return FALSE;
         end if;

      end LOOP;
   end if;
   ---
   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BULK_INSERT_TRAN_DATA_PACK;
--------------------------------------------------------------------------------
FUNCTION BULK_INSERT_SUP_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sup_data_rec    IN       SUP_DATA_REC)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'PRICING_SQL.BULK_INSERT_SUP_DATA';

   L_tomorrow        DATE := GET_VDATE+1;

BEGIN

   if I_sup_data_rec.depts is not NULL then
      FORALL i in I_sup_data_rec.depts.FIRST..I_sup_data_rec.depts.LAST
         insert into sup_data(dept,
                              supplier,
                              day_date,
                              tran_type,
                              amount)
                       values(I_sup_data_rec.depts(i),
                              I_sup_data_rec.primary_suppliers(i),
                              L_tomorrow,
                              I_sup_data_rec.tran_types(i),
                              I_sup_data_rec.amounts(i));
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BULK_INSERT_SUP_DATA;
---------------------------------------------------------------------------------------------
END PRICING_SQL;
/
