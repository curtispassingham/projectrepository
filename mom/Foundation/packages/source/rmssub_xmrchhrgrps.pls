
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRGRP AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_cre_type VARCHAR(15) := 'xmrchhrgrpcre';
   LP_mod_type VARCHAR(15) := 'xmrchhrgrpmod';
   LP_del_type VARCHAR(15) := 'xmrchhrgrpdel';

----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRGRP;
/
