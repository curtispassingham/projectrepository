CREATE OR REPLACE PACKAGE BODY CORESVC_COSTCHG AS

   TYPE errors_tab_typ IS TABLE OF CORESVC_COSTCHG_ERR%ROWTYPE;
   LP_errors_tab            errors_tab_typ;
   LP_system_options        SYSTEM_OPTIONS%ROWTYPE;
   LP_user                  SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;
   LP_var_comp_basis        CORESVC_COSTCHG_CONFIG.VARIANCE_COMPARISON_BASIS%TYPE;
   LP_vdate                 DATE;
   LP_cost_prior_cre_days   FOUNDATION_UNIT_OPTIONS.COST_PRIOR_CREATE_DAYS%TYPE;

   CSDL_del_tab        CSDL_rec_tab;
   CSD_del_tab         CSD_rec_tab;

--------------------------------------------------------------------------------
FUNCTION CHUNK_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GEN_CC_NUMBERS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CSH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id      IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                     I_chunk_id        IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE )
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CSDL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE,
                      I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL_LOC.CHUNK_ID%TYPE )
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CSD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                     I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE )
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION UPDATE_RECALC_ORD_IND(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id            IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                               I_chunk_id              IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE,
                               I_cost_change           IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                               I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                               I_supplier              IN       SUPS.SUPPLIER%TYPE,
                               I_origin_country        IN       COUNTRY.COUNTRY_ID%TYPE,
                               I_recalc_ord_ind        IN       COST_CHANGE_TEMP.RECALC_ORD_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION UPDATE_BRACKET_DTLS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_bracket_uom1        IN     SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                             I_cost_change         IN     SVC_COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                             I_item                IN     SVC_COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                             I_supplier            IN     SVC_COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                             I_origin_country_id   IN     SVC_COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID%TYPE,
                             I_loc                 IN     SVC_COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_supplier        IN       SUPS.SUPPLIER%TYPE,
                            I_table           IN       VARCHAR2,
                            I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE,
                            I_row_seq         IN       SVC_COST_SUSP_SUP_DETAIL.ROW_SEQ%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       ITEM_MASTER.ITEM%TYPE,
                        I_supplier        IN       SUPS.SUPPLIER%TYPE,
                        I_table           IN       VARCHAR2,
                        I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE,
                        I_row_seq         IN       SVC_COST_SUSP_SUP_DETAIL.ROW_SEQ%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_COUNTRY(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_origin_country_id  IN       COUNTRY.COUNTRY_ID%TYPE,
                          I_item               IN       ITEM_MASTER.ITEM%TYPE,
                          I_supplier           IN       SUPS.SUPPLIER%TYPE,
                          I_table              IN       VARCHAR2,
                          I_process_id         IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                          I_chunk_id           IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE,
                          I_row_seq            IN       SVC_COST_SUSP_SUP_DETAIL.ROW_SEQ%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item                IN       SVC_COST_SUSP_SUP_DETAIL_LOC.ITEM%TYPE,
                           I_loc_type            IN       SVC_COST_SUSP_SUP_DETAIL_LOC.LOC_TYPE%TYPE,
                           I_loc                 IN       SVC_COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                           I_table               IN       VARCHAR2,
                           I_process_id          IN       SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE,
                           I_chunk_id            IN       SVC_COST_SUSP_SUP_DETAIL_LOC.CHUNK_ID%TYPE,
                           I_row_seq             IN       SVC_COST_SUSP_SUP_DETAIL_LOC.ROW_SEQ%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION APPLY_CHANGE (O_error_message       IN OUT   VARCHAR2,
                       I_process_id          IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                       I_chunk_id            IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE,
                       I_row_seq             IN       SVC_COST_SUSP_SUP_DETAIL.ROW_SEQ%TYPE,
                       I_cost_change         IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE,
                       I_change_type         IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE_TYPE%TYPE,
                       I_change_amount       IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE_VALUE%TYPE,
                       I_supplier            IN       SVC_COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                       I_item                IN       SVC_COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                       I_origin_country_id   IN       SVC_COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION APPLY_CHANGE_LOC (O_error_message    IN OUT VARCHAR2,
                           I_process_id       IN     SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE,
                           I_chunk_id         IN     SVC_COST_SUSP_SUP_DETAIL_LOC.CHUNK_ID%TYPE,
                           I_row_seq          IN     SVC_COST_SUSP_SUP_DETAIL_LOC.ROW_SEQ%TYPE,
                           I_cost_change      IN     COST_CHANGE_LOC_TEMP.COST_CHANGE%TYPE,
                           I_supplier         IN     COST_CHANGE_LOC_TEMP.SUPPLIER%TYPE,
                           I_country          IN     COST_CHANGE_LOC_TEMP.ORIGIN_COUNTRY_ID%TYPE,
                           I_item             IN     COST_CHANGE_LOC_TEMP.ITEM%TYPE,
                           I_loc_type         IN     COST_CHANGE_LOC_TEMP.LOC_TYPE%TYPE,
                           I_location         IN     VARCHAR2,
                           I_bracket_value    IN     COST_CHANGE_LOC_TEMP.BRACKET_VALUE1%TYPE,
                           I_change_type      IN     VARCHAR2,
                           I_change_amount    IN     NUMBER)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CC_VALUE (O_error_message       IN OUT   VARCHAR2,
                            I_change_type         IN       COST_SUSP_SUP_DETAIL.COST_CHANGE_TYPE%TYPE,
                            I_change_amount       IN       COST_SUSP_SUP_DETAIL.COST_CHANGE_VALUE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_COST_CHANGE(O_error_message  IN OUT VARCHAR2,
                                   I_cost_change    IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION PROCESS_DELETES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL_LOC.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_COST_CONFLICTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_conflicts_exist IN OUT   BOOLEAN,
                              I_active_date     IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                              I_cost_change     IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE,
                              I_row_seq         IN       SVC_COST_SUSP_SUP_HEAD.ROW_SEQ%TYPE,
                              I_process_id      IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION APPROVAL_CHECK(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_approve                IN OUT   BOOLEAN,
                        I_active_date            IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                        I_cost_change            IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                        I_row_seq                IN       SVC_COST_SUSP_SUP_HEAD.ROW_SEQ%TYPE,
                        I_process_id             IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                        I_chunk_id               IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION SUBMIT_CHECK(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_submit                 IN OUT   BOOLEAN,
                      I_active_date            IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                      I_cost_change            IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                      I_row_seq                IN       SVC_COST_SUSP_SUP_HEAD.ROW_SEQ%TYPE,
                      I_process_id             IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                      I_chunk_id               IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CALL_FC_ENGINE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_change     IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                        I_action_type     IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_VARIANCES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_approve         IN OUT   BOOLEAN,
                              I_cost_change     IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE,
                              I_supplier        IN       SVC_COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                              I_item            IN       SVC_COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                              I_location        IN       SVC_COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                              I_country         IN       SVC_COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE,
                              I_item_level_ind  IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION SUBMIT_APPROVE_COST_CHANGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   CORESVC_ITEM_ERR.PROCESS_ID%TYPE,
                      I_error_seq     IN   CORESVC_ITEM_ERR.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   CORESVC_ITEM_ERR.CHUNK_ID%TYPE,
                      I_table_name    IN   CORESVC_ITEM_ERR.TABLE_NAME%TYPE,
                      I_row_seq       IN   CORESVC_ITEM_ERR.ROW_SEQ%TYPE,
                      I_column_name   IN   CORESVC_ITEM_ERR.COLUMN_NAME%TYPE,
                      I_error_msg     IN   CORESVC_ITEM_ERR.ERROR_MSG%TYPE,
                      I_item          IN   ITEM_SUPP_COUNTRY.ITEM%TYPE DEFAULT NULL,
                      I_supplier      IN   ITEM_SUPP_COUNTRY.SUPPLIER%TYPE DEFAULT NULL,
                      I_country_id    IN   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE DEFAULT NULL,
                      I_error_type    IN   CORESVC_ITEM_ERR.ERROR_TYPE%TYPE DEFAULT 'E')
IS

BEGIN

   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   LP_errors_tab(LP_errors_tab.COUNT()).item        := I_item;
   LP_errors_tab(LP_errors_tab.COUNT()).supplier    := I_supplier;
   LP_errors_tab(LP_errors_tab.COUNT()).country_id  := I_country_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CLEANUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255)   := 'CORESVC_COSTCHG.PROCESS_CLEANUP';
   L_cc_exists   VARCHAR2(1);

   cursor C_GET_INV_CC is
      select svh.cost_change
        from svc_cost_susp_sup_head svh,
             cost_susp_sup_head ch
       where svh.process_id = I_process_id
         and svh.cost_change = ch.cost_change;

   cursor C_EXIST_CC_DTL (I_cost_change  IN COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE) is
      select 'Y'
        from cost_susp_sup_detail
       where cost_change = I_cost_change
         and rownum = 1
       union
      select 'Y'
        from cost_susp_sup_detail_loc
       where cost_change = I_cost_change
         and rownum = 1;

   cursor C_NO_DETAIL_HEADER is
      select sch.row_seq,
             sch.cost_change
        from svc_cost_susp_sup_head sch
       where sch.process_id = I_process_id
         and sch.action = 'NEW'
         and not exists (select 1
                           from svc_cost_susp_sup_detail scd
                          where scd.cost_change = sch.cost_change
                            and rownum = 1
                          union all
                         select 1
                           from svc_cost_susp_sup_detail_loc scdl
                          where scdl.cost_change = sch.cost_change
                            and rownum = 1);

   cursor C_NO_HEADER is
      select scd.row_seq,
             scd.cost_change
        from svc_cost_susp_sup_detail scd
       where scd.process_id = I_process_id
         and scd.action = 'NEW'
         and not exists (select 1
                           from svc_cost_susp_sup_head sch
                          where scd.cost_change = sch.cost_change
                            and rownum = 1)
       union all
      select scdl.row_seq,
             scdl.cost_change
        from svc_cost_susp_sup_detail_loc scdl
       where scdl.process_id = I_process_id
         and scdl.action = 'NEW'
         and not exists (select 1
                           from svc_cost_susp_sup_head sch
                          where scdl.cost_change = sch.cost_change
                            and rownum = 1);

BEGIN
   FOR rec in C_NO_DETAIL_HEADER LOOP
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  'SVC_COST_SUSP_SUP_HEAD',
                  rec.row_seq,
                  NULL,
                  'NO_COST_CHG_DETAIL');
   END LOOP;

   FOR rec in C_NO_HEADER LOOP
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  'SVC_COST_SUSP_SUP_DETAIL',
                  rec.row_seq,
                  NULL,
                  'NO_COST_CHG_HEAD');
   END LOOP;

   -- Update the status of svc_cost_susp_sup_head with no detail records.
   update svc_cost_susp_sup_head sch
      set sch.process$status = 'E',
          last_upd_id = LP_user,
          last_upd_datetime = sysdate
    where sch.process_id = I_process_id
      and sch.action = 'NEW'
      and not exists (select 1
                        from svc_cost_susp_sup_detail scd
                       where scd.cost_change = sch.cost_change
                         and rownum = 1
                       union all
                      select 1
                        from svc_cost_susp_sup_detail_loc scdl
                       where scdl.cost_change = sch.cost_change
                         and rownum = 1);

   -- Update the status of svc_cost_susp_sup_detail with no header records.
   update svc_cost_susp_sup_detail scd
      set scd.process$status = 'E',
          last_upd_id = LP_user,
          last_upd_datetime = sysdate
    where scd.process_id = I_process_id
      and scd.action = 'NEW'
      and not exists (select 1
                        from svc_cost_susp_sup_head sch
                       where scd.cost_change = sch.cost_change
                         and rownum = 1);

   -- Update the status of svc_cost_susp_sup_detail_loc with no header records.
   update svc_cost_susp_sup_detail_loc scdl
      set scdl.process$status = 'E',
          last_upd_id = LP_user,
          last_upd_datetime = sysdate
    where scdl.process_id = I_process_id
      and scdl.action = 'NEW'
      and not exists (select 1
                        from svc_cost_susp_sup_head sch
                       where scdl.cost_change = sch.cost_change
                         and rownum = 1);

   -- Delete all cost_susp_sup_head with no successfully inserted
   -- cost_susp_sup_detail/cost_susp_sup_detail_loc record.

   FOR rec in C_GET_INV_CC LOOP
      L_cc_exists := NULL;
      open C_EXIST_CC_DTL(rec.cost_change);
      fetch C_EXIST_CC_DTL into L_cc_exists;
      close C_EXIST_CC_DTL;

      if L_cc_exists is NULL then
         delete from cost_susp_sup_head
          where cost_change = rec.cost_change;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CLEANUP;
--------------------------------------------------------------------------------
PROCEDURE EXEC_CSH_CFA_STG_DEL(I_process_id           IN       NUMBER,
                               I_chunk_id             IN       NUMBER,
                               I_cost_change          IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)  IS
   cursor C_RECS_DELETE is
     select sc.process_id ,
            sc.chunk_id ,
            sc.row_seq ,
            sc.action ,
            sc.base_rms_table ,
            sc.group_set_view_name ,
            sc.keys_col ,
            sc.attrs_col ,
            gs.group_set_id,
            gs.default_func,
            sf.template_key,
            sc.rowid as rid
       from svc_cfa_ext          sc,
            cfa_attrib_group_set gs,
            svc_process_tracker  sp,
            s9t_folder           sf
      where sc.process_id          = I_process_id
        and chunk_id               = I_chunk_id
        and sp.process_id          = I_process_id
        and sp.file_id             = sf.file_id (+)
        and sc.group_set_view_name = gs.group_set_view_name;

BEGIN
   open C_RECS_DELETE;
   close C_RECS_DELETE;
   FOR rec IN C_RECS_DELETE
   LOOP
      if key_val_pairs_pkg.get_attr(rec.KEYS_COL,'COST_CHANGE') = I_cost_change then
         delete from svc_cfa_ext
            where row_seq = rec.row_seq;
      end if;
   END LOOP;
END EXEC_CSH_CFA_STG_DEL;
--------------------------------------------------------------------------------
PROCEDURE EXEC_CSH_INS(I_ins_tab   IN   CSH_rec_tab) IS

BEGIN

   FORALL i IN 1..I_ins_tab.COUNT()
      insert into cost_susp_sup_head values I_ins_tab(i);

END EXEC_CSH_INS;
--------------------------------------------------------------------------------
PROCEDURE EXEC_CSH_UPD(I_upd_tab IN CSH_rec_tab) IS

BEGIN

   FORALL i IN 1..I_upd_tab.COUNT()
      update cost_susp_sup_head
         set status = I_upd_tab(i).status,
             cost_change_desc = NVL(I_upd_tab(i).cost_change_desc,cost_change_desc),
             reason = NVL(I_upd_tab(i).reason,reason),
             active_date = NVL(I_upd_tab(i).active_date,active_date),
             approval_date = I_upd_tab(i).approval_date,
             approval_id = I_upd_tab(i).approval_id
       where cost_change = I_upd_tab(i).cost_change;

END EXEC_CSH_UPD;
--------------------------------------------------------------------------------
PROCEDURE EXEC_CSH_DEL( I_process_id   IN       NUMBER,
                        I_chunk_id     IN       NUMBER,
                        I_del_tab      IN   CSH_rec_tab) IS

BEGIN
   FOR i IN 1..I_del_tab.COUNT()
   LOOP
      EXEC_CSH_CFA_STG_DEL(I_process_id,
                           I_chunk_id,
                           I_del_tab(i).cost_change);
      update cost_susp_sup_head
         set status = I_del_tab(i).status
       where cost_change = I_del_tab(i).cost_change;
   END LOOP;
END EXEC_CSH_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CSH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id      IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                     I_chunk_id        IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE )
RETURN BOOLEAN IS

   L_error                 BOOLEAN;
   L_val_reason            VARCHAR2(1);

   CSH_temp_rec            COST_SUSP_SUP_HEAD%ROWTYPE;
   CSH_ins_tab             CSH_rec_tab   := NEW CSH_rec_tab();
   CSH_upd_tab             CSH_rec_tab   := NEW CSH_rec_tab();
   CSH_del_tab             CSH_rec_tab   := NEW CSH_rec_tab();

   L_table                 VARCHAR2(255)   := 'SVC_COST_SUSP_SUP_HEAD';
   L_program               VARCHAR2(255)   := 'CORESVC_COSTCHG.PROCESS_CSH';
   L_min_days              DATE;

   ERROR_VALIDATION        EXCEPTION;

   cursor C_SVC_CSH is
      select pk_cost_susp_sup_head.rowid                as pk_cost_susp_sup_head_rid,
             pk_cost_susp_sup_head.cost_change_origin   as old_cost_change_origin,
             pk_cost_susp_sup_head.cost_change_desc     as old_cost_change_desc,
             pk_cost_susp_sup_head.status               as old_status,
             pk_cost_susp_sup_head.reason               as old_reason,
             pk_cost_susp_sup_head.active_date          as old_active_date,
             cph_ccr_fk.rowid                           as cph_ccr_fk_rid,
             st.cost_change,
             st.cost_change_desc,
             st.reason,
             st.active_date,
             st.status,
             st.cost_change_origin,
             st.approval_date,
             st.approval_id,
             st.process_id,
             st.row_seq,
             st.rowid as svc_rowid,
             UPPER(st.action) as action,
             st.process$status
        from svc_cost_susp_sup_head st,
             cost_susp_sup_head pk_cost_susp_sup_head,
             cost_chg_reason cph_ccr_fk
       where st.process_id    = I_process_id
         and st.chunk_id      = I_chunk_id
         and st.cost_change   = pk_cost_susp_sup_head.cost_change (+)
         and st.reason        = cph_ccr_fk.reason (+)
         and st.action is NOT NULL;

   c_svc_CSH_rec   C_SVC_CSH%ROWTYPE;

   cursor C_FOUNDATION_UNIT_OPTIONS is
      select f.cost_prior_create_days,
             p.vdate
        from foundation_unit_options f,
             period p;

   cursor C_GET_VALID_REASON (I_reason   IN COST_CHG_REASON.REASON%TYPE) is
      select 'x'
        from cost_chg_reason
       where reason = I_reason;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   open C_FOUNDATION_UNIT_OPTIONS;
   fetch C_FOUNDATION_UNIT_OPTIONS into LP_cost_prior_cre_days,
                                        LP_vdate;
   close C_FOUNDATION_UNIT_OPTIONS;

   if LP_cost_prior_cre_days < 1 then
      LP_cost_prior_cre_days := 1;
   end if;

   L_min_days := LP_cost_prior_cre_days + LP_vdate;

   FOR rec in C_SVC_CSH
   LOOP
      L_error := FALSE;

      if rec.action is NOT NULL and
         rec.action NOT in (ACTION_NEW, ACTION_MOD, ACTION_DEL) then
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'S9T_INVALID_ACTION');
         L_error := TRUE;
      end if;

      if rec.action = ACTION_NEW and
         rec.PK_COST_SUSP_SUP_HEAD_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'PK_COST_SUSP_SUP_HEAD');
         L_error := TRUE;
      end if;

      if rec.action in (ACTION_MOD, ACTION_DEL) and
         rec.PK_COST_SUSP_SUP_HEAD_rid is NULL then
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'PK_CSSH_MISSING');
         L_error := TRUE;
      end if;

      if rec.CPH_CCR_FK_rid is NULL then
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'CPH_CCR_FK');
         L_error := TRUE;
      end if;

      if rec.action in (ACTION_NEW,ACTION_MOD) then
         if rec.cost_change_origin is NULL then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COST_CHANGE_ORIGIN',
                        'FIELD_NOT_NULL');
            L_error := TRUE;
         end if;

         if rec.status is NULL then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'STATUS',
                        'FIELD_NOT_NULL');
            L_error := TRUE;
         end if;

         -- allowed statuses
         if rec.status NOT in ('W', 'S', 'A', 'E', 'R', 'C', 'D') then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'INV_CC_STATUS');
            L_error := TRUE;
         end if;

         if rec.active_date is NULL then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ACTIVE_DATE',
                        'FIELD_NOT_NULL');
            L_error := TRUE;
         elsif rec.active_date < L_min_days then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ACTIVE_DATE',
                        'INV_ACTIVE_DATE');
            L_error := TRUE;
         end if;

         if rec.reason is NULL then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'REASON',
                        'FIELD_NOT_NULL');
            L_error := TRUE;
         end if;

         if rec.cost_change_desc is NULL then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COST_CHANGE_DESC',
                        'FIELD_NOT_NULL');
            L_error := TRUE;
         end if;

         if rec.cost_change is NULL then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COST_CHANGE',
                        'FIELD_NOT_NULL');
            L_error := TRUE;
         end if;
      end if;

      -- Check if cost change exists
      if rec.action in (ACTION_MOD,ACTION_DEL) and rec.old_status is NULL then
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'INV_COST_CHANGE');
         L_error := TRUE;
      elsif rec.action = ACTION_NEW and rec.old_status is NOT NULL then
         -- Write error 'Cost change number already exist.'
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'COST_CHG_EXISTS_II');
         L_error := TRUE;
      end if;

      if rec.action = ACTION_NEW or (rec.action = ACTION_MOD and (rec.status = 'W' or rec.old_status = 'W')) then
         if rec.reason in (1, 2, 3) then
            -- Write error 'Cost change reason is reserved for maintaining bracket costing.'
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'CC_REASON_RESERVED');
            L_error := TRUE;
         else
            L_val_reason := NULL;
            open C_GET_VALID_REASON(rec.reason);
            fetch C_GET_VALID_REASON into L_val_reason;
            close C_GET_VALID_REASON;

            if L_val_reason is NULL then
              WRITE_ERROR(I_process_id,
                          CORESVC_COSTCHG_ESEQ.nextval,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                          'REASON',
                          'INV_REASON_CODE');
              L_error := TRUE;
            end if;
         end if;

         if rec.cost_change_origin NOT in ('SKU', 'SUP') THEN
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COST_CHANGE_ORIGIN',
                        'INV_COST_CHG_ORIGIN');
            L_error := TRUE;
         end if;
      end if;

      if rec.action = ACTION_NEW and rec.status NOT IN ('W','A','S') then
         -- Write error 'Cost change can be uploaded only in Worksheet, 'Submitted or Approved status.'
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     'INV_COST_CHG_STATUS');
         L_error := TRUE;
      elsif rec.action = ACTION_MOD then
         if rec.cost_change_origin is NOT NULL and rec.old_cost_change_origin <> rec.cost_change_origin then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COST_CHANGE_ORIGIN',
                        'FIELD_NO_MOD');
            L_error := TRUE;
         end if;
         -- only edit these fields if the CC is in worksheet status or the cost change is being brought to worksheet status
         if rec.old_status <> 'W' and rec.status <> 'W' then
            -- non-editable fields
            if rec.cost_change_desc is NOT NULL and rec.old_cost_change_desc <> rec.cost_change_desc then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE_DESC',
                           'FIELD_NO_MOD');
               L_error := TRUE;
            end if;

            if rec.reason is NOT NULL and rec.old_reason <> rec.reason then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'REASON',
                           'FIELD_NO_MOD');
               L_error := TRUE;
            end if;

            if rec.active_date is NOT NULL and rec.old_active_date <> rec.active_date then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'ACTIVE_DATE',
                           'FIELD_NO_MOD');
               L_error := TRUE;
            end if;
         end if;

         -- error out if cost change is deleted
         if rec.old_status = 'D' then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'COST_CHANGE_DELETED');
            L_error := TRUE;
         end if;
         -- Only rejected, submitted or worksheet cost changes can be deleted.
         if (rec.status = 'D' or rec.action = ACTION_DEL) and
            rec.old_status not in ('R','S','W') then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'STATUS',
                        'INV_CC_STATUS_MOD_D');
            L_error := TRUE;
         end if;
         -- Only approved cost changes can be cancelled.
         if rec.status = 'C' and rec.old_status != 'A' then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'STATUS',
                        'INV_CC_STATUS_MOD_C');
            L_error := TRUE;
         end if;
         -- Only worksheet, submitted, approved or rejected cost changes can be set back to worksheet.
         if rec.status = 'W' and rec.old_status not in ('W','S','A','R') then 
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'STATUS',
                        'INV_CC_STATUS_MOD_W');
            L_error := TRUE;
         end if;
         -- Only submitted cost changes can be rejected.
         if rec.status = 'R' and rec.old_status != 'S' then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'STATUS',
                        'INV_CC_STATUS_MOD_R');
            L_error := TRUE;
         end if;
         -- Only worksheet cost changes can be submitted.
         if rec.status = 'S' and rec.old_status != 'W' then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'STATUS',
                        'INV_CC_STATUS_MOD_S');
            L_error := TRUE;
         end if;
      end if;

      if L_error = FALSE then
         if rec.action = ACTION_NEW then
            if rec.status in ('S', 'A') then -- set to 'W' first then submit/approve at the end of processing
               rec.status := 'W';
            end if;
            CSH_temp_rec.cost_change         := rec.cost_change;
            CSH_temp_rec.cost_change_desc    := rec.cost_change_desc;
            CSH_temp_rec.reason              := rec.reason;
            CSH_temp_rec.active_date         := rec.active_date;
            CSH_temp_rec.status              := rec.status;
            CSH_temp_rec.cost_change_origin  := rec.cost_change_origin;
            if rec.status <> 'A' then
               CSH_temp_rec.approval_date       := NULL;
               CSH_temp_rec.approval_id         := NULL;
            end if;
            CSH_temp_rec.create_id           := LP_user;
            CSH_temp_rec.create_date         := SYSDATE;

            CSH_ins_tab.extend;
            CSH_ins_tab(CSH_ins_tab.count()) := CSH_temp_rec;
         end if;

         if rec.action = ACTION_MOD and rec.old_status not in ('C') then
            CSH_temp_rec.cost_change         := rec.cost_change;
            CSH_temp_rec.status              := rec.status;
            CSH_temp_rec.approval_date       := NULL;
            CSH_temp_rec.approval_id         := NULL;

            if rec.status = 'W' or rec.old_status = 'W' then
               CSH_temp_rec.cost_change_desc    := rec.cost_change_desc;
               CSH_temp_rec.reason              := rec.reason;
               CSH_temp_rec.active_date         := rec.active_date;
            end if;
            
            if rec.old_status = 'A' then
               if rec.status = 'A' then
                  CSH_temp_rec.approval_date       := rec.approval_date;
                  CSH_temp_rec.approval_id         := rec.approval_id;
               elsif rec.status in ('C','W') then
                  if CALL_FC_ENGINE(O_error_message,
                                    rec.cost_change,
                                    'REM_CC') = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;
            
            if rec.old_status in ('R','S','W') and (rec.status = 'D' or rec.action = ACTION_DEL) then
               if CALL_FC_ENGINE(O_error_message,
                                 rec.cost_change,
                                 'REM_CC') = FALSE then
                  return FALSE;
               end if;
            end if;

            if rec.status = 'S' and rec.old_status = 'W' or
               rec.status = 'A' and rec.old_status in ('W', 'S') then
               CSH_temp_rec.status  := 'W'; -- set to 'W' first then submit/approve at the end of processing
            end if;

            CSH_upd_tab.extend;
            CSH_upd_tab(CSH_upd_tab.count()) := CSH_temp_rec;
         end if;
         --
         if rec.action = ACTION_DEL then
            CSH_temp_rec.COST_CHANGE         := rec.cost_change;
            CSH_temp_rec.STATUS              := 'D';

            CSH_del_tab.extend;
            CSH_del_tab(CSH_del_tab.count()) := CSH_temp_rec;
         end if;

      else
         update svc_cost_susp_sup_head
            set process$status = 'E',
                last_upd_id = LP_user,
                last_upd_datetime = sysdate
          where rowid = rec.svc_rowid;
      end if;

   END LOOP;

   if CSH_ins_tab is NOT NULL and CSH_ins_tab.COUNT > 0 then
      EXEC_CSH_INS(CSH_ins_tab);
   end if;

   if CSH_upd_tab is NOT NULL and CSH_upd_tab.COUNT > 0 then
      EXEC_CSH_UPD(CSH_upd_tab);
   end if;

   if CSH_del_tab is NOT NULL and CSH_del_tab.COUNT > 0 then
      EXEC_CSH_DEL(I_process_id,
                   I_chunk_id,
                   CSH_del_tab);
   end if;

   update svc_cost_susp_sup_head
      set process$status = 'P',
          last_upd_id = LP_user,
          last_upd_datetime = sysdate
    where process_id     = I_process_id
      and chunk_id       = I_chunk_id
      and action is NOT NULL
      and process$status = 'N';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CSH;
--------------------------------------------------------------------------------
PROCEDURE EXEC_CSDL_DEL(I_del_rec   IN   SVC_COST_SUSP_SUP_DETAIL_LOC%ROWTYPE)IS

BEGIN
   -- stores
   delete from cost_susp_sup_detail_loc
    where cost_change       = I_del_rec.cost_change
      and item              = I_del_rec.item
      and supplier          = I_del_rec.supplier
      and origin_country_id = I_del_rec.origin_country_id
      and loc               = NVL(I_del_rec.loc,loc)
      and loc_type          = NVL(I_del_rec.loc_type,loc_type)
      and loc_type          = 'S';

   -- warehouses
   delete from cost_susp_sup_detail_loc
    where cost_change       = I_del_rec.cost_change
      and item              = I_del_rec.item
      and supplier          = I_del_rec.supplier
      and origin_country_id = I_del_rec.origin_country_id
      and (loc in (select wh
                    from wh
                   where wh <> physical_wh
                     and physical_wh = I_del_rec.loc)
           or I_del_rec.loc_type is NULL)
      and loc_type          = NVL(I_del_rec.loc_type,loc_type)
      and loc_type          = 'W';

END EXEC_CSDL_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CSDL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE,
                      I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL_LOC.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_error                   BOOLEAN      := FALSE;

   CSDL_temp_rec             SVC_COST_SUSP_SUP_DETAIL_LOC%ROWTYPE;
   CSDL_ins_tab              CSDL_rec_tab := NEW CSDL_rec_tab();
   CSDL_upd_tab              CSDL_rec_tab := NEW CSDL_rec_tab();

   L_table                   VARCHAR2(255)    :='SVC_COST_SUSP_SUP_DETAIL_LOC';
   L_program                 VARCHAR2(255)    :='CORESVC_COSTCHG.PROCESS_CSDL';
   L_locs_exist              BOOLEAN          := FALSE;
   L_exists                  VARCHAR2(1);
   L_valid                   BOOLEAN          := FALSE;
   L_validate                VARCHAR2(1) := 'N';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_iscl_exists             VARCHAR2(1);
   L_prev_supplier           COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE := NULL;
   L_loc_type                COST_SUSP_SUP_DETAIL_LOC.LOC_TYPE%TYPE;
   L_deliv_country_id        COST_SUSP_SUP_DETAIL_LOC.DELIVERY_COUNTRY_ID%TYPE;
   L_prev_cost_change        COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;
   L_v_locs_exist            VARCHAR2(1)   := 'N';
   L_bracket_ind             SUPS.BRACKET_COSTING_IND%TYPE;
   L_count                   NUMBER(10)   := 0;
   L_bracket_type1           SUP_INV_MGMT.BRACKET_TYPE1%TYPE;
   L_bracket_uom1            SUP_INV_MGMT.BRACKET_UOM1%TYPE;
   L_bracket_type2           SUP_INV_MGMT.BRACKET_TYPE2%TYPE;
   L_bracket_uom2            SUP_INV_MGMT.BRACKET_UOM2%TYPE;
   L_sup_dept_seq_no         SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE;
   L_type_desc               CODE_DETAIL.CODE_DESC%TYPE;
   L_cost_change_origin      COST_SUSP_SUP_HEAD.COST_CHANGE_ORIGIN%TYPE;

   cursor C_SVC_CSDL is
      select distinct wh_uk_cssdl.cost_change    as wh_uk_cssdl_rid,
             st_uk_cssdl.rowid                   as st_uk_cssdl_rid,
             wh_css_isl_fk.loc                   as wh_css_isl_fk_rid,
             st_css_isl_fk.rowid                 as st_css_isl_fk_rid,
             country.rowid                       as fk_country_rid,
             cssh.rowid                          as fk_cssh_rid,
             cssh.status                         as current_status,
             wh_uk_cssdl.cost_change_type        as old_cost_change_type_wh,
             wh_uk_cssdl.cost_change_value       as old_cost_change_value_wh,
             wh_uk_cssdl.recalc_ord_ind          as old_recalc_ord_ind_wh,
             wh_uk_cssdl.delivery_country_id     as old_delivery_country_wh,
             st_uk_cssdl.cost_change_type        as old_cost_change_type_st,
             st_uk_cssdl.cost_change_value       as old_cost_change_value_st,
             st_uk_cssdl.recalc_ord_ind          as old_recalc_ord_ind_st,
             st_uk_cssdl.delivery_country_id     as old_delivery_country_st,
             st.bracket_uom1,
             st.bracket_value2,
             st.cost_change_type,
             st.cost_change_value,
             st.recalc_ord_ind,
             st.default_bracket_ind,
             st.delivery_country_id,
             st.cost_change,
             st.supplier,
             st.origin_country_id,
             st.item,
             st.loc_type,
             st.loc,
             st.bracket_value1,
             st.process_id,
             st.row_seq,
             st.rowid                             as svc_rowid,
             UPPER(st.action) as action,
             st.process$status,
             sh.active_date,
             sh.reason,
             sh.status
        from svc_cost_susp_sup_detail_loc st,
             svc_cost_susp_sup_head sh,
             cost_susp_sup_head cssh,
             country,
             (select cssdl.cost_change,
                     physical_wh loc,
                     'W' loc_type,
                     cssdl.item,
                     cssdl.supplier,
                     cssdl.origin_country_id,
                     cssdl.cost_change_type,
                     cssdl.cost_change_value,
                     cssdl.recalc_ord_ind,
                     cssdl.delivery_country_id
                from wh,
                     cost_susp_sup_detail_loc cssdl
               where wh.wh = cssdl.loc
                 and wh.wh <> wh.physical_wh
                 and cssdl.loc_type = 'W') wh_uk_cssdl,
             (select cssdl.cost_change,
                     store loc,
                     'S' loc_type,
                     cssdl.item,
                     cssdl.supplier,
                     cssdl.origin_country_id,
                     cssdl.cost_change_type,
                     cssdl.cost_change_value,
                     cssdl.recalc_ord_ind,
                     cssdl.delivery_country_id
                from store s,
                     cost_susp_sup_detail_loc cssdl
               where s.store = cssdl.loc
                 and cssdl.loc_type = 'S') st_uk_cssdl,
             (select physical_wh loc,
                     'W' loc_type,
                     iscl.item,
                     iscl.supplier,
                     iscl.origin_country_id
                from wh,
                     item_supp_country_loc iscl
               where wh.wh = iscl.loc
                 and wh.wh <> wh.physical_wh
                 and iscl.loc_type = 'W') wh_css_isl_fk,
             (select store loc,
                     'S' loc_type,
                     iscl.item,
                     iscl.supplier,
                     iscl.origin_country_id
                from store s,
                     item_supp_country_loc iscl
               where s.store = iscl.loc
                 and iscl.loc_type = 'S') st_css_isl_fk
       where st.process_id          = I_process_id
         and st.chunk_id            = I_chunk_id
         and st.process_id          = sh.process_id (+)
         and st.chunk_id            = sh.chunk_id (+)
         and st.cost_change         = sh.cost_change (+)
         and st.cost_change         = wh_uk_cssdl.cost_change (+)
         and st.item                = wh_uk_cssdl.item (+)
         and st.supplier            = wh_uk_cssdl.supplier (+)
         and st.origin_country_id   = wh_uk_cssdl.origin_country_id (+)
         and st.loc                 = wh_uk_cssdl.loc (+)
         and st.loc_type            = wh_uk_cssdl.loc_type (+)
         and st.cost_change         = st_uk_cssdl.cost_change (+)
         and st.item                = st_uk_cssdl.item (+)
         and st.supplier            = st_uk_cssdl.supplier (+)
         and st.origin_country_id   = st_uk_cssdl.origin_country_id (+)
         and st.loc                 = st_uk_cssdl.loc (+)
         and st.loc_type            = st_uk_cssdl.loc_type (+)
         and st.item                = wh_css_isl_fk.item (+)
         and st.supplier            = wh_css_isl_fk.supplier (+)
         and st.origin_country_id   = wh_css_isl_fk.origin_country_id (+)
         and st.loc                 = wh_css_isl_fk.loc (+)
         and st.loc_type            = wh_css_isl_fk.loc_type (+)
         and st.item                = st_css_isl_fk.item (+)
         and st.supplier            = st_css_isl_fk.supplier (+)
         and st.origin_country_id   = st_css_isl_fk.origin_country_id (+)
         and st.loc                 = st_css_isl_fk.loc (+)
         and st.loc_type            = st_css_isl_fk.loc_type (+)
         and st.delivery_country_id = country.country_id (+)
         and st.cost_change         = cssh.cost_change (+)
         and st.action is NOT NULL;

   c_svc_CSDL_rec   C_SVC_CSDL%ROWTYPE;

   cursor C_GET_ORIGIN(I_cost_change   COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE) is
      select cost_change_origin
        from cost_susp_sup_head
       where cost_change = I_cost_change;

   cursor C_CSSD_EXISTS (I_origin_country_id   IN   COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID%TYPE,
                         I_supplier            IN   COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                         I_cost_change         IN   COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                         I_item                IN   COST_SUSP_SUP_DETAIL.ITEM%TYPE) is
      select 'x'
        from cost_susp_sup_detail
       where cost_change = I_cost_change
         and item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id;

   cursor C_GET_CC_TEMP is
      select distinct(cost_change)
        from cost_change_loc_temp
       where exists (select 1
                       from svc_cost_susp_sup_head
                      where process_id = I_process_id
                        and chunk_id = I_chunk_id);

BEGIN
   CSDL_del_tab := NEW CSDL_rec_tab();
   FOR rec in C_SVC_CSDL LOOP
      L_exists := NULL;
      L_error  := FALSE;
      -- Check if cost change head exists
      open C_GET_ORIGIN(rec.cost_change);
      fetch C_GET_ORIGIN into L_cost_change_origin;
      close C_GET_ORIGIN;

      if NVL(L_prev_cost_change,rec.cost_change) = rec.cost_change then
         if NVL(L_prev_supplier,rec.supplier) != rec.supplier and L_cost_change_origin = 'SUP' then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        l_table,
                        rec.row_seq,
                        'SUPPLIER',
                        'INV_REC_ONE_SUP',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            update svc_cost_susp_sup_detail_loc
               set process$status = 'E',
                   last_upd_id = LP_user,
                   last_upd_datetime = sysdate
             where rowid = rec.svc_rowid;
         else
            L_validate := 'Y';
         end if;
      else
         L_validate := 'Y';
      end if;
      if L_validate = 'Y' then
         L_prev_supplier := rec.supplier;
         L_prev_cost_change := rec.cost_change;
         if rec.action IS NOT NULL and
            rec.action NOT in (ACTION_NEW,ACTION_MOD,ACTION_DEL) then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        l_table,
                        rec.row_seq,
                        NULL,
                        'S9T_INVALID_ACTION',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;
         end if;

         if rec.action = ACTION_NEW then
            if rec.loc_type = 'W' and rec.wh_uk_cssdl_rid is NOT NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'UK_COST_SUSP_SUP_DETAIL',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            elsif rec.loc_type = 'S' and rec.st_uk_cssdl_rid is NOT NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'UK_COST_SUSP_SUP_DETAIL',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;
         elsif rec.action in (ACTION_MOD, ACTION_DEL) then
            if rec.loc_type = 'W' and rec.wh_uk_cssdl_rid is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'UK_CSSDL_MISSING',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            elsif rec.loc_type = 'S' and rec.st_uk_cssdl_rid is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'UK_CSSDL_MISSING',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;
         end if;

         if rec.action = ACTION_MOD and rec.current_status != 'W' then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'DETL_MOD_NOT_ACCPETED',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;

            -- non-editable fields
            if rec.loc_type = 'S' then
               if rec.cost_change_type is NOT NULL and rec.old_cost_change_type_st <> rec.cost_change_type then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'COST_CHANGE_TYPE',
                              'FIELD_NO_MOD',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;

               if rec.cost_change_value is NOT NULL and rec.old_cost_change_value_st <> rec.cost_change_value then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'COST_CHANGE_VALUE',
                              'FIELD_NO_MOD',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;

               if rec.recalc_ord_ind is NOT NULL and rec.old_recalc_ord_ind_st <> rec.recalc_ord_ind then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'RECALC_ORD_IND',
                              'FIELD_NO_MOD',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;

               if rec.delivery_country_id is NOT NULL and rec.old_delivery_country_st <> rec.delivery_country_id then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'DELIVERY_COUNTRY_ID',
                              'FIELD_NO_MOD',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := FALSE;
               end if;
            elsif rec.loc_type = 'W' then
               if rec.cost_change_type is NOT NULL and rec.old_cost_change_type_wh <> rec.cost_change_type then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'COST_CHANGE_TYPE',
                              'FIELD_NO_MOD',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;

               if rec.cost_change_value is NOT NULL and rec.old_cost_change_value_wh <> rec.cost_change_value then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'COST_CHANGE_VALUE',
                              'FIELD_NO_MOD',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;

               if rec.recalc_ord_ind is NOT NULL and rec.old_recalc_ord_ind_wh <> rec.recalc_ord_ind then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'RECALC_ORD_IND',
                              'FIELD_NO_MOD',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;

               if rec.delivery_country_id is NOT NULL and rec.old_delivery_country_wh <> rec.delivery_country_id then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'DELIVERY_COUNTRY_ID',
                              'FIELD_NO_MOD',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := FALSE;
               end if;
            end if;
         end if;

         -- Loc Validation
         if VALIDATE_LOCATION(O_error_message,
                              rec.item,
                              rec.loc_type,
                              rec.loc,
                              L_table,
                              I_process_id,
                              I_chunk_id,
                              rec.row_seq) = FALSE then
            L_error := TRUE;
         end if;

         if rec.action = ACTION_NEW or 
            (rec.action = ACTION_MOD and rec.current_status = 'W') then
            L_error            := FALSE;
            L_deliv_country_id := rec.delivery_country_id;

            -- Error out if combination does not exist in ITEM_SUPP_COUNTRY_LOC
            if rec.loc_type = 'S' and rec.st_css_isl_fk_rid is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           l_table,
                           rec.row_seq,
                           NULL,
                           'CSS_ISL_FK',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            elsif rec.loc_type = 'W' and rec.wh_css_isl_fk_rid is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           l_table,
                           rec.row_seq,
                           NULL,
                           'CSS_ISL_FK',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            -- Error out if cost change header does not exist.
            if rec.fk_cssh_rid is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           l_table,
                           rec.row_seq,
                           NULL,
                           'NO_COST_CHG_HEAD',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if rec.recalc_ord_ind is NULL then
               rec.recalc_ord_ind := 'N';
            end if;

            if rec.recalc_ord_ind NOT in ('Y','N') then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'RECALC_ORD_IND',
                           'INV_RECALC_ORD_IND',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            -- Validate Supplier
            if VALIDATE_SUPPLIER(O_error_message,
                                 rec.supplier,
                                 L_table,
                                 I_process_id,
                                 I_chunk_id,
                                 rec.row_seq) = FALSE then
               L_error := TRUE;
            end if;

            -- Item Validation
            if VALIDATE_ITEM(O_error_message,
                             rec.item,
                             rec.supplier,
                             L_table,
                             I_process_id,
                             I_chunk_id,
                             rec.row_seq) = FALSE then
               L_error := TRUE;
            end if;

            if ITEMLOC_ATTRIB_SQL.LOCATIONS_EXIST(O_error_message,
                                                  rec.item,
                                                  NULL,
                                                  L_locs_exist) = FALSE then
               return FALSE;
            end if;

            if L_locs_exist = FALSE then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'NO_COST_CHANGE_LOC_TEMP',
                           rec.item);
               L_error := TRUE;
            end if;

            -- Country Validation
            if VALIDATE_COUNTRY(O_error_message,
                                rec.origin_country_id,
                                rec.item,
                                rec.supplier,
                                L_table,
                                I_process_id,
                                I_chunk_id,
                                rec.row_seq) = FALSE then
               L_error := TRUE;
            end if;

            if rec.cost_change is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE',
                           'FIELD_NOT_NULL',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;
            if rec.cost_change_type is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE_TYPE',
                           'FIELD_NOT_NULL',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            elsif rec.cost_change_type not in ('A','F','P') then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANBGE_TYPE',
                           'INV_COST_CHG_TYPE',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if VALIDATE_CC_VALUE (O_error_message,
                                  rec.cost_change_type,
                                  rec.cost_change_value) = FALSE then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE_VALUE',
                           O_error_message,
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            -- Bracket costing validation
            if LP_system_options.bracket_costing_ind = 'Y' then

               L_bracket_ind := NULL;
               if SUPP_ATTRIB_SQL.GET_BRACKET_COSTING_IND(L_error_message,
                                                          L_bracket_ind,
                                                          rec.supplier) = FALSE then
                  return FALSE;
               end if;

               if L_bracket_ind = 'N' then
                  if L_deliv_country_id is NOT NULL then
                     if LP_system_options.default_tax_type = 'GTAX' then
                        if rec.fk_country_rid is NULL then
                           -- Write error 'Invalid delivery country id.'
                           WRITE_ERROR(I_process_id,
                                       CORESVC_COSTCHG_ESEQ.nextval,
                                       I_chunk_id,
                                       L_table,
                                       rec.row_seq,
                                       'DELIVER_COUNTRY_ID',
                                       'INV_DELIV_CTRY_ID',
                                       rec.item,
                                       rec.supplier,
                                       rec.origin_country_id);
                           L_error := TRUE;
                        end if;
                     else
                        WRITE_ERROR(I_process_id,
                                    CORESVC_COSTCHG_ESEQ.nextval,
                                    I_chunk_id,
                                    L_table,
                                    rec.row_seq,
                                    'DELIVER_COUNTRY_ID',
                                    'DELIV_CTRY_NULL',
                                    rec.item,
                                    rec.supplier,
                                    rec.origin_country_id);
                        L_error := TRUE;
                     end if;
                  end if;
               elsif L_bracket_ind = 'Y' and L_deliv_country_id is NOT NULL then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'DELIVER_COUNTRY_ID',
                              'DELIV_CTRY_NULL',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;
               -- get bracket_uom, bracket_value1
               if L_bracket_ind = 'Y' and rec.loc_type = 'W' then
                  if ITEM_BRACKET_COST_SQL.GET_BRACKET(L_error_message,
                                                       L_bracket_type1,
                                                       L_bracket_uom1,
                                                       L_bracket_type2,
                                                       L_bracket_uom2,
                                                       L_sup_dept_seq_no,
                                                       rec.supplier,
                                                       NULL,
                                                       rec.loc) = FALSE then
                     return FALSE;
                  end if;

                  if L_bracket_uom1 is NULL then
                     L_bracket_uom1 := L_bracket_type1;
                  end if;
               end if;
            elsif LP_system_options.bracket_costing_ind = 'N' then
               L_bracket_ind := 'N';

               if L_deliv_country_id is NOT NULL then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'DELIVER_COUNTRY_ID',
                              'DELIV_CTRY_NULL',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;
            end if;

            if rec.item is NOT NULL and rec.supplier is NOT NULL and rec.origin_country_id is NOT NULL and rec.action = ACTION_NEW then
               L_count := 0;
               if ITEM_ATTRIB_SQL.GET_RECORD_COUNT_COST(O_error_message,
                                                        L_count,
                                                        rec.cost_change,
                                                        rec.item,
                                                        rec.supplier,
                                                        rec.origin_country_id) = FALSE then
                  return FALSE;
               end if;

               if L_count > 0 then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              'REC_EXIST',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                   L_error := TRUE;
               end if;
            end if;

            -- process detail records for all valid data entry.  Insert data to COST_CHANGE_TEMP table
            if L_error = FALSE then
               L_exists := NULL;
               -- Check if cost change detail exists
               open C_CSSD_EXISTS(rec.origin_country_id,
                                  rec.supplier,
                                  rec.cost_change,
                                  rec.item);
               fetch C_CSSD_EXISTS into L_exists;
               close C_CSSD_EXISTS;

               if L_exists is NOT NULL then
                  delete from cost_susp_sup_detail
                   where cost_change = rec.cost_change
                     and item = rec.item
                     and supplier = rec.supplier
                     and origin_country_id = rec.origin_country_id;
               end if;

               if (rec.action in (ACTION_NEW, ACTION_MOD) and (rec.status in ('W','S','A') or rec.current_status = 'W')) then
                  --- Insert records in the cost_change_loc_temp for the item/supplier if they don't exist
                  L_locs_exist := FALSE;
                  if COST_CHANGE_SQL.CC_TEMP_LOCS_EXIST(L_error_message,
                                                        L_locs_exist,
                                                        rec.cost_change,
                                                        rec.item,
                                                        rec.supplier,
                                                        rec.origin_country_id,
                                                        L_deliv_country_id) = FALSE then
                     return FALSE;
                  end if;

                  if L_locs_exist = FALSE then
                     if COST_CHANGE_SQL.POP_TEMP_DETAIL_LOC_SEC(O_error_message,
                                                                L_valid,
                                                                L_v_locs_exist,
                                                                action_new,
                                                                rec.cost_change,
                                                                rec.supplier,
                                                                rec.origin_country_id,
                                                                rec.item,
                                                                rec.reason,
                                                                L_deliv_country_id,
                                                                'BC') = FALSE then
                        return FALSE;
                     end if;

                     --- Inform user if no Cost_Change_Loc_Temp record can be created
                     --- because of the user's security associations
                     if L_v_locs_exist = 'N' then
                        WRITE_ERROR(I_process_id,
                                    CORESVC_COSTCHG_ESEQ.nextval,
                                    I_chunk_id,
                                    L_table,
                                    rec.row_seq,
                                    NULL,
                                    O_error_message,
                                    rec.item,
                                    rec.supplier,
                                    rec.origin_country_id);
                        L_error := TRUE;
                     end if;
                     ---
                     --- Inform user if no Cost_Change_Loc_Temp records where created
                     ---
                     if NOT L_valid then
                        WRITE_ERROR(I_process_id,
                                    CORESVC_COSTCHG_ESEQ.nextval,
                                    I_chunk_id,
                                    L_table,
                                    rec.row_seq,
                                    NULL,
                                    'NO_COST_CHANGE_LOC_TEMP',
                                    rec.item,
                                    rec.supplier,
                                    rec.origin_country_id);
                        L_error := TRUE;
                     end if;
                  end if;

                  if rec.loc_type = 'DW' then
                     L_loc_type := 'W';
                  else
                     L_loc_type := rec.loc_type;
                  end if;
                  if APPLY_CHANGE_LOC(L_error_message,
                                      I_process_id,
                                      I_chunk_id,
                                      rec.row_seq,
                                      rec.cost_change,
                                      rec.supplier,
                                      rec.origin_country_id,
                                      rec.item,
                                      L_loc_type,
                                      rec.loc,
                                      NULL,
                                      rec.cost_change_type,
                                      rec.cost_change_value) = FALSE then
                     return FALSE;
                  end if;

                  if NOT COST_CHANGE_SQL.UPDATE_CC_DETAIL_TEMP(L_error_message,
                                                               rec.item,
                                                               rec.supplier,
                                                               rec.origin_country_id,
                                                               rec.delivery_country_id) then
                     return FALSE;
                  end if;
                  if COST_CHANGE_SQL.UPDATE_RECALC_ORD_IND_AT_LOC(L_error_message,
                                                                  rec.item,
                                                                  rec.supplier,
                                                                  rec.origin_country_id,
                                                                  rec.cost_change,
                                                                  rec.recalc_ord_ind,
                                                                  rec.delivery_country_id) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if L_bracket_ind = 'Y' then
                  if UPDATE_BRACKET_DTLS(L_error_message,
                                         L_bracket_uom1,
                                         rec.cost_change,
                                         rec.item,
                                         rec.supplier,
                                         rec.origin_country_id,
                                         rec.loc) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 CORESVC_COSTCHG_ESEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 rec.row_seq,
                                 NULL,
                                 L_error_message,
                                 rec.item,
                                 rec.supplier,
                                 rec.origin_country_id);
                     L_error := TRUE;
                  end if;
               end if;
            end if;
         end if;

         if rec.action = ACTION_DEL and rec.current_status != 'W' then
            -- Write error 'Cannot delete detail record as Cost Change is not in Worksheet status.'
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'CC_NOT_IN_W_STATUS',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;
         end if;

         if rec.action = ACTION_DEL and L_error = FALSE then
            CSDL_temp_rec.cost_change           := rec.cost_change;
            CSDL_temp_rec.item                  := rec.item;
            CSDL_temp_rec.supplier              := rec.supplier;
            CSDL_temp_rec.origin_country_id     := rec.origin_country_id;
            CSDL_temp_rec.loc_type              := rec.loc_type;
            CSDL_temp_rec.loc                   := rec.loc;
            CSDL_temp_rec.row_seq               := rec.row_seq;

            CSDL_del_tab.extend;
            CSDL_del_tab(CSDL_del_tab.count()):= CSDL_temp_rec;
         end if;

         if L_error then
             update svc_cost_susp_sup_detail_loc
                set process$status = 'E',
                    last_upd_id = LP_user,
                    last_upd_datetime = sysdate
              where rowid = rec.svc_rowid;
         end if;
      end if;
   END LOOP;

   FOR rec in C_GET_CC_TEMP LOOP
      if INSERT_UPDATE_COST_CHANGE(L_error_message,
                                   rec.cost_change) = FALSE then
         return FALSE;
      end if;

      if NOT COST_CHANGE_SQL.DELETE_COST_CHANGE_LOC_TEMP(L_error_message,
                                                         rec.cost_change) then
         return FALSE;
      end if;
   END LOOP;

   update svc_cost_susp_sup_detail_loc
      set process$status = 'P',
          last_upd_id = LP_user,
          last_upd_datetime = sysdate
    where process_id     = I_process_id
      and chunk_id       = I_chunk_id
      and action is NOT NULL
      and process$status = 'N';
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CSDL;
--------------------------------------------------------------------------------
PROCEDURE EXEC_CSD_DEL(I_del_rec   IN   SVC_COST_SUSP_SUP_DETAIL%ROWTYPE) IS

BEGIN

   delete from cost_susp_sup_detail
    where cost_change         = I_del_rec.cost_change
      and item                = I_del_rec.item
      and supplier            = I_del_rec.supplier
      and origin_country_id   = I_del_rec.origin_country_id;

END EXEC_CSD_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CSD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                     I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   CSDL_temp_rec         SVC_COST_SUSP_SUP_DETAIL_LOC%ROWTYPE;
   CSD_temp_rec          SVC_COST_SUSP_SUP_DETAIL%ROWTYPE;
   CSD_ins_tab           CSD_rec_tab     := NEW CSD_rec_tab();
   CSD_upd_tab           CSD_rec_tab     := NEW CSD_rec_tab();

   L_table               VARCHAR2(255)   :='SVC_COST_SUSP_SUP_DETAIL';
   L_program             VARCHAR2(255)   :='CORESVC_COSTCHG.PROCESS_CSD';
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_error               BOOLEAN   := FALSE;
   L_detail_loc_exists   VARCHAR2(1);
   L_prev_cost_change    COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE := NULL;
   L_prev_supplier       COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE := NULL;
   L_deliv_country_id    COST_SUSP_SUP_DETAIL.DELIVERY_COUNTRY_ID%TYPE;
   L_bracket_ind         SUPS.BRACKET_COSTING_IND%TYPE;
   L_count               NUMBER(10);
   L_valid               BOOLEAN          := FALSE;
   L_validate            VARCHAR2(1) := 'N';
   L_bracket_type1       SUP_INV_MGMT.BRACKET_TYPE1%TYPE;
   L_bracket_uom1        SUP_INV_MGMT.BRACKET_UOM1%TYPE;
   L_bracket_type2       SUP_INV_MGMT.BRACKET_TYPE2%TYPE;
   L_bracket_uom2        SUP_INV_MGMT.BRACKET_UOM2%TYPE;
   L_sup_dept_seq_no     SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE;

   cursor C_SVC_CSD is
      select uk_cost_susp_sup_detail.rowid               as uk_cost_susp_sup_detail_rid,
             cpd_cph_fk.rowid                            as cpd_cph_fk_rid,
             cpd_cph_fk.cost_change_origin               as old_cost_change_origin,
             cpd_cph_fk.status                           as current_status,
             cpd_isc_fk.rowid                            as cpd_isc_fk_rid,
             country.rowid                               as fk_country_rid,
             uk_cost_susp_sup_detail.cost_change_type    as old_cost_change_type,
             uk_cost_susp_sup_detail.cost_change_value   as old_cost_change_value,
             uk_cost_susp_sup_detail.recalc_ord_ind      as old_recalc_ord_ind,
             uk_cost_susp_sup_detail.delivery_country_id as old_delivery_country_id,
             st.cost_change,
             st.supplier,
             st.origin_country_id,
             st.item,
             st.bracket_value1,
             st.bracket_uom1,
             st.bracket_value2,
             st.cost_change_type,
             st.cost_change_value,
             st.recalc_ord_ind,
             st.default_bracket_ind,
             st.delivery_country_id,
             st.process_id,
             st.row_seq,
             st.rowid as svc_rowid,
             UPPER(st.action) as action,
             st.process$status,
             sh.active_date,
             sh.reason,
             sh.status
        from svc_cost_susp_sup_detail st,
             svc_cost_susp_sup_head sh,
             cost_susp_sup_detail uk_cost_susp_sup_detail,
             cost_susp_sup_head cpd_cph_fk,
             item_supp_country cpd_isc_fk,
             country
       where st.process_id            = I_process_id
         and st.chunk_id              = I_chunk_id
         and st.process_id            = sh.process_id (+)
         and st.chunk_id              = sh.chunk_id (+)
         and st.cost_change           = sh.cost_change (+)
         and st.item                  = uk_cost_susp_sup_detail.item (+)
         and st.origin_country_id     = uk_cost_susp_sup_detail.origin_country_id (+)
         and st.supplier              = uk_cost_susp_sup_detail.supplier (+)
         and st.cost_change           = uk_cost_susp_sup_detail.cost_change (+)
         and st.item                  = cpd_isc_fk.item (+)
         and st.origin_country_id     = cpd_isc_fk.origin_country_id (+)
         and st.supplier              = cpd_isc_fk.supplier (+)
         and st.cost_change           = cpd_cph_fk.cost_change (+)
         and st.delivery_country_id   = country.country_id (+)
         and st.action is NOT NULL;

   c_svc_CSD_rec   C_SVC_CSD%ROWTYPE;

   cursor C_CHECK_CC_DTL_LOC (I_origin_country_id   IN   COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE,
                              I_supplier            IN   COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                              I_cost_change         IN   COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE,
                              I_item                IN   COST_SUSP_SUP_DETAIL.ITEM%TYPE) is
      select 'x'
        from cost_susp_sup_detail_loc
       where cost_change = I_cost_change
         and item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id
         and rownum = 1;

   cursor C_GET_CC_TEMP is
      select distinct(cost_change)
        from cost_change_temp
       where exists (select 1
                       from svc_cost_susp_sup_head
                      where process_id = I_process_id
                        and chunk_id = I_chunk_id);

BEGIN
   CSD_del_tab := NEW CSD_rec_tab();
   FOR rec in C_SVC_CSD LOOP
      L_error := FALSE;
      if NVL(L_prev_cost_change,rec.cost_change) = rec.cost_change then
         if NVL(L_prev_supplier,rec.supplier) != rec.supplier and rec.old_cost_change_origin = 'SUP' then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        l_table,
                        rec.row_seq,
                        'SUPPLIER',
                        'INV_REC_ONE_SUP',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;
            update svc_cost_susp_sup_detail
               set process$status = 'E',
                   last_upd_id = LP_user,
                   last_upd_datetime = sysdate
             where rowid = rec.svc_rowid;
         else
            L_validate := 'Y';
         end if;
      else
         L_validate := 'Y';
      end if;

      if L_validate = 'Y' then
         L_prev_supplier := rec.supplier;
         L_prev_cost_change := rec.cost_change;
         if rec.action IS NOT NULL and
            rec.action NOT in (ACTION_NEW,ACTION_MOD,ACTION_DEL) then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        l_table,
                        rec.row_seq,
                        NULL,
                        'S9T_INVALID_ACTION',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;
         end if;

         if rec.action = ACTION_NEW and
            rec.UK_COST_SUSP_SUP_DETAIL_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'UK_COST_SUSP_SUP_DETAIL',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;
         end if;

         if rec.action in (ACTION_MOD, ACTION_DEL) and
            rec.UK_COST_SUSP_SUP_DETAIL_rid is NULL then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'UK_CSSD_MISSING',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;
         end if;

         if rec.action = ACTION_MOD and rec.current_status != 'W' then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'DETL_MOD_NOT_ACCPETED',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;

            -- non-editable fields
            if rec.cost_change_type is NOT NULL and rec.old_cost_change_type <> rec.cost_change_type then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE_TYPE',
                           'FIELD_NO_MOD',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if rec.cost_change_value is NOT NULL and rec.old_cost_change_value <> rec.cost_change_value then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE_VALUE',
                           'FIELD_NO_MOD',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if rec.recalc_ord_ind is NOT NULL and rec.old_recalc_ord_ind <> rec.recalc_ord_ind then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'RECALC_ORD_IND',
                           'FIELD_NO_MOD',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if rec.delivery_country_id is NOT NULL and rec.old_delivery_country_id <> rec.delivery_country_id then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'DELIVERY_COUNTRY_ID',
                           'FIELD_NO_MOD',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := FALSE;
            end if;
         end if;

         if rec.action = ACTION_NEW or 
            (rec.action = ACTION_MOD and (rec.status in ('W','S','A') or rec.current_status = 'W')) then 
            L_deliv_country_id := rec.delivery_country_id;
            if rec.CPD_CPH_FK_rid is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           l_table,
                           rec.row_seq,
                           NULL,
                           'NO_COST_CHG_HEAD',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if rec.CPD_ISC_FK_rid is NULL then
               write_error(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'CPD_ISC_FK',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if rec.recalc_ord_ind is NULL then
               rec.recalc_ord_ind := 'N';
            end if;

            if rec.recalc_ord_ind NOT in ('Y','N') then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'RECALC_ORD_IND',
                           'INV_RECALC_ORD_IND',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;
            -- Validate Supplier
            if VALIDATE_SUPPLIER(O_error_message,
                                 rec.supplier,
                                 L_table,
                                 I_process_id,
                                 I_chunk_id,
                                 rec.row_seq) = FALSE then
               L_error := TRUE;
            end if;
            -- Item Validation
            if VALIDATE_ITEM(O_error_message,
                             rec.item,
                             rec.supplier,
                             L_table,
                             I_process_id,
                             I_chunk_id,
                             rec.row_seq) = FALSE then
               L_error := TRUE;
            end if;
            -- Country Validation
            if VALIDATE_COUNTRY(O_error_message,
                                rec.origin_country_id,
                                rec.item,
                                rec.supplier,
                                L_table,
                                I_process_id,
                                I_chunk_id,
                                rec.row_seq) = FALSE then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'ORIGIN_COUNTRY_ID',
                           O_error_message,
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if rec.cost_change is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE',
                           'FIELD_NOT_NULL',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if rec.cost_change_type is NULL then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE_TYPE',
                           'FIELD_NOT_NULL',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            elsif rec.cost_change_type not in ('A','F','P') then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANBGE_TYPE',
                           'INV_COST_CHG_TYPE',
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if VALIDATE_CC_VALUE (O_error_message,
                                  rec.cost_change_type,
                                  rec.cost_change_value) = FALSE then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COST_CHANGE_VALUE',
                           O_error_message,
                           rec.item,
                           rec.supplier,
                           rec.origin_country_id);
               L_error := TRUE;
            end if;

            if LP_system_options.bracket_costing_ind = 'Y' then
               L_bracket_ind := NULL;

               if SUPP_ATTRIB_SQL.GET_BRACKET_COSTING_IND(L_error_message,
                                                          L_bracket_ind,
                                                          rec.supplier) = FALSE then
                  return FALSE;
               end if;

               if L_bracket_ind = 'N' then
                  if L_deliv_country_id is NOT NULL then
                     if LP_system_options.default_tax_type = 'GTAX' then
                        if rec.fk_country_rid is NULL then
                           -- Write error 'Invalid delivery country id.'
                           WRITE_ERROR(I_process_id,
                                       CORESVC_COSTCHG_ESEQ.nextval,
                                       I_chunk_id,
                                       L_table,
                                       rec.row_seq,
                                       'DELIVERY_COUNTRY_ID',
                                       'INV_DELIV_CTRY_ID',
                                       rec.item,
                                       rec.supplier,
                                       rec.origin_country_id);
                           L_error := TRUE;
                        end if;
                     else
                        WRITE_ERROR(I_process_id,
                                    CORESVC_COSTCHG_ESEQ.nextval,
                                    I_chunk_id,
                                    L_table,
                                    rec.row_seq,
                                    'DELIVERY_COUNTRY_ID',
                                    'DELIV_CTRY_NULL',
                                    rec.item,
                                    rec.supplier,
                                    rec.origin_country_id);
                        L_error := TRUE;
                     end if;
                  end if;
               elsif L_bracket_ind = 'Y' and L_deliv_country_id is not NULL then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'DELIVERY_COUNTRY_ID',
                              'DELIV_CTRY_NULL',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;
               -- get bracket_uom, bracket_value1
               if L_bracket_ind = 'Y' then
                  if ITEM_BRACKET_COST_SQL.GET_BRACKET(L_error_message,
                                                       L_bracket_type1,
                                                       L_bracket_uom1,
                                                       L_bracket_type2,
                                                       L_bracket_uom2,
                                                       L_sup_dept_seq_no,
                                                       rec.supplier,
                                                       NULL,
                                                       NULL) = FALSE then
                     return FALSE;
                  end if;

                  if L_bracket_uom1 is NULL then
                     L_bracket_uom1 := L_bracket_type1;
                  end if;
               end if;
            elsif LP_system_options.bracket_costing_ind = 'N' then
               L_bracket_ind := 'N';
               if L_deliv_country_id is NOT NULL then
                  if LP_system_options.default_tax_type = 'GTAX' then
                     if rec.fk_country_rid is NULL then
                        -- Write error 'Invalid delivery country id.'
                        WRITE_ERROR(I_process_id,
                                    CORESVC_COSTCHG_ESEQ.nextval,
                                    I_chunk_id,
                                    L_table,
                                    rec.row_seq,
                                    'DELIVERY_COUNTRY_ID',
                                    'INV_DELIV_CTRY_ID',
                                    rec.item,
                                    rec.supplier,
                                    rec.origin_country_id);
                        L_error := TRUE;
                     end if;
                  else
                     WRITE_ERROR(I_process_id,
                                 CORESVC_COSTCHG_ESEQ.nextval,
                                 I_chunk_id,
                                 L_table,
                                 rec.row_seq,
                                 'DELIVER_COUNTRY_ID',
                                 'DELIV_CTRY_NULL',
                                 rec.item,
                                 rec.supplier,
                                 rec.origin_country_id);
                     L_error := TRUE;
                  end if;
               end if;
            end if;

            if rec.item is NOT NULL and rec.supplier is NOT NULL and rec.origin_country_id is NOT NULL and rec.action = ACTION_NEW then
               L_count := 0;

               if ITEM_ATTRIB_SQL.GET_RECORD_COUNT_COST(O_error_message,
                                                        L_count,
                                                        rec.cost_change,
                                                        rec.item,
                                                        rec.supplier,
                                                        rec.origin_country_id) = FALSE then
                  return FALSE;
               end if;

               if L_count > 0 then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.nextval,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              'REC_EXIST',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                   L_error := TRUE;
               end if;
            end if;

            if L_error = FALSE then
            -- process detail records for all valid data entry.  Insert data to COST_CHANGE_TEMP table
               if COST_CHANGE_SQL.POP_TEMP_DETAIL_SEC(L_error_message,
                                                      L_valid,
                                                      action_new,
                                                      rec.cost_change,
                                                      rec.supplier,
                                                      rec.origin_country_id,
                                                      rec.item,
                                                      'N',
                                                      rec.delivery_country_id) = FALSE then
                  return FALSE;
               end if;

               if NOT L_valid and L_bracket_ind = 'Y' then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              'NO_COST_CHANGE_BKT_TEMP',
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;

               if APPLY_CHANGE(L_error_message,
                               I_process_id,
                               I_chunk_id,
                               rec.row_seq,
                               rec.cost_change,
                               rec.cost_change_type,
                               rec.cost_change_value,
                               rec.supplier,
                               rec.item,
                               rec.origin_country_id) = FALSE then
                  return FALSE;
               end if;

               if UPDATE_RECALC_ORD_IND(L_error_message,
                                        I_process_id,
                                        I_chunk_id,
                                        rec.cost_change,
                                        rec.item,
                                        rec.supplier,
                                        rec.origin_country_id,
                                        rec.recalc_ord_ind) = FALSE then
                  WRITE_ERROR(I_process_id,
                              CORESVC_COSTCHG_ESEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              L_error_message,
                              rec.item,
                              rec.supplier,
                              rec.origin_country_id);
                  L_error := TRUE;
               end if;

               if L_bracket_ind = 'Y' then
                  if UPDATE_BRACKET_DTLS(L_error_message,
                                         L_bracket_uom1,
                                         rec.cost_change,
                                         rec.item,
                                         rec.supplier,
                                         rec.origin_country_id,
                                         NULL) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 CORESVC_COSTCHG_ESEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 rec.row_seq,
                                 NULL,
                                 L_error_message,
                                 rec.item,
                                 rec.supplier,
                                 rec.origin_country_id);
                     L_error := TRUE;
                  end if;
               end if;
            end if;
         end if; 

         L_detail_loc_exists := NULL;
         -- Check if cost change detail loc exists
         open C_CHECK_CC_DTL_LOC(rec.origin_country_id,
                                 rec.supplier,
                                 rec.cost_change,
                                 rec.item);
         fetch C_CHECK_CC_DTL_LOC into L_detail_loc_exists;
         close C_CHECK_CC_DTL_LOC;

         if L_detail_loc_exists is NOT NULL and rec.action != ACTION_DEL then
            -- Write error 'Cannot create cost change detail since location detail already exists.'
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'COST_CHG_DTL_LOC_EXIST',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;
         end if;

         if rec.action = ACTION_DEL and rec.current_status != 'W' then
            -- Write error 'Cannot delete detail record as Cost Change is not in Worksheet status.'
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        'CC_NOT_IN_W_STATUS',
                        rec.item,
                        rec.supplier,
                        rec.origin_country_id);
            L_error := TRUE;
         end if;

         if rec.action = ACTION_DEL and L_error = FALSE then
            CSD_temp_rec.cost_change         := rec.cost_change;
            CSD_temp_rec.item                := rec.item;
            CSD_temp_rec.supplier            := rec.supplier;
            CSD_temp_rec.origin_country_id   := rec.origin_country_id;
            CSD_temp_rec.row_seq             := rec.row_seq;

            CSD_del_tab.extend;
            CSD_del_tab(CSD_del_tab.count()):= CSD_temp_rec;
         end if;

         if L_error then
             update svc_cost_susp_sup_detail
                set process$status = 'E',
                    last_upd_id = LP_user,
                    last_upd_datetime = sysdate
              where rowid = rec.svc_rowid;
         end if;
      end if;
   END LOOP;

   FOR rec in C_GET_CC_TEMP LOOP
      if INSERT_UPDATE_COST_CHANGE(L_error_message,
                                   rec.cost_change) = FALSE then
         return FALSE;
      end if;

      if NOT COST_CHANGE_SQL.DELETE_COST_CHANGE_TEMP(L_error_message,
                                                     rec.cost_change) then
         return FALSE;
      end if;
   END LOOP;

   update svc_cost_susp_sup_detail
      set process$status = 'P',
          last_upd_id = LP_user,
          last_upd_datetime = sysdate
    where process_id     = I_process_id
      and chunk_id       = I_chunk_id
      and process$status = 'N'
      and action is NOT NULL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CSD;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CHUNK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id      IN       NUMBER,
                       I_chunk_id        IN       NUMBER,
                       O_error_count        OUT   NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(255):='CORESVC_COSTCHG.PROCESS_CHUNK';
   L_err_tab   CORESVC_CFLEX.TYP_ERR_TAB;
BEGIN

   LP_errors_tab := NEW errors_tab_typ();

   if GEN_CC_NUMBERS(O_error_message,
                     I_process_id,
                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_CSH(O_error_message,
                  I_process_id,
                  I_chunk_id) = FALSE then
       return FALSE;
   end if;

   if PROCESS_CSDL(O_error_message,
                   I_process_id,
                   I_chunk_id) = FALSE then
       return FALSE;
   end if;

   if PROCESS_CSD(O_error_message,
                  I_process_id,
                  I_chunk_id) = FALSE then
       return FALSE;
   end if;

   if CORESVC_CFLEX.PROCESS_CFA(O_error_message,
                                L_err_tab,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   FOR i in 1..L_err_tab.count
   LOOP
      WRITE_ERROR(I_process_id,
                  CORESVC_ITEM_ESEQ.NEXTVAL,
                  I_chunk_id,
                  L_err_tab(i).view_name,
                  L_err_tab(i).row_seq,
                  L_err_tab(i).attrib,
                  L_err_tab(i).err_msg);
   END LOOP;   

   if PROCESS_DELETES(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_CLEANUP(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
       return FALSE;
   end if;

   O_error_count := LP_errors_tab.count();

   FORALL i IN 1..O_error_count
      insert into coresvc_costchg_err values LP_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_CHUNK;
--------------------------------------------------------------------------------
FUNCTION CHUNK_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(255) := 'CORESVC_COSTCHG.CHUNK_DATA';

   cursor C_CHUNKS is
      SELECT cost_change,
             ceil(rownum/max_chunk_size) as chunk_id
        FROM svc_cost_susp_sup_head,
             coresvc_costchg_config
       where process_id = I_process_id;

   TYPE c_recs_type IS TABLE OF C_CHUNKS%ROWTYPE;

   c_recs   C_RECS_TYPE;

BEGIN

   open C_CHUNKS;

   LOOP
      fetch C_CHUNKS BULK COLLECT INTO c_recs LIMIT 1000;

      FORALL i IN 1..c_recs.count
         update svc_cost_susp_sup_head
            set chunk_id    = c_recs(i).chunk_id
          where process_id  = I_process_id
            and cost_change = c_recs(i).cost_change;
      ---
      FORALL i IN 1..c_recs.count
         update svc_cfa_ext
            set chunk_id    = c_recs(i).chunk_id
          where process_id  = I_process_id
            and key_val_pairs_pkg.get_attr(keys_col,'COST_CHANGE') = to_char(c_recs(i).cost_change);
      ---
      FORALL i IN 1..c_recs.count
         update svc_cost_susp_sup_detail
            set chunk_id    = c_recs(i).chunk_id
          where process_id  = I_process_id
            and cost_change = c_recs(i).cost_change;
      ---
      FORALL i IN 1..c_recs.count
         update svc_cost_susp_sup_detail_loc
            set chunk_id    = c_recs(i).chunk_id
          where process_id  = I_process_id
            and cost_change = c_recs(i).cost_change;
      ---
      delete from coresvc_costchg_chunks
       where process_id = I_process_id;
      ---
      FORALL i IN 1..c_recs.count
         merge into coresvc_costchg_chunks spc
         using (select I_process_id         as process_id,
                       c_recs(i).chunk_id   as chunk_id
                  from dual) sq
            ON (sq.process_id = spc.process_id and
                sq.chunk_id   = spc.chunk_id)
          when matched then
             update set cost_change_count = cost_change_count + 1
          when not matched then
             insert (process_id,
                     chunk_id,
                     cost_change_count,
                     status)
             values (sq.process_id,
                     sq.chunk_id,
                     1,
                     'N');
      ---
      commit;
      exit when C_CHUNKS%NOTFOUND;
  END LOOP;

  close C_CHUNKS;
  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHUNK_DATA;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_process_id      IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255) := 'CORESVC_COSTCHG.PROCESS';
   L_error_count      NUMBER        := 0;
   L_total_errors     NUMBER        := 0;
   L_tot_error_cnt    NUMBER := 0;
   L_warning_cnt      NUMBER := 0;
   L_chunk_status     CORESVC_COSTCHG_CHUNKS.status%type;
   L_process_status   CORESVC_COSTCHG_CHUNKS.status%type;
       
   cursor C_ERRORS_EXIST is
      select COUNT(error_type) tot_err_count,
             NVL(SUM(DECODE(error_type, 'W', 1, 0)), 0) warn_count
        from coresvc_costchg_err
       where process_id = I_process_id;

   L_errors_exist   VARCHAR2(1):=   'N';

BEGIN
   select variance_comparison_basis
     into LP_var_comp_basis
     from coresvc_costchg_config;
   
   if CHUNK_DATA(O_error_message,I_process_id)= FALSE then
      return FALSE;
   end if;

   LP_errors_tab := NEW errors_tab_typ();

   for rec in (select chunk_id, 
                      rowid as rid
                 from coresvc_costchg_chunks
                where process_id = I_process_id
                order by chunk_id)
   LOOP
      if PROCESS_CHUNK(O_error_message,
                       I_process_id,
                       rec.chunk_id,
                       L_error_count) = FALSE THEN
         return FALSE;
      end if;

      if L_error_count  > 0 then
         L_chunk_status := 'E';
      else
         L_chunk_status := 'P';
      end if;

      L_total_errors := L_total_errors + L_error_count;

      update coresvc_costchg_chunks
         set status = L_chunk_status
       where rowid = rec.rid;

      commit;
   END LOOP;

   if SUBMIT_APPROVE_COST_CHANGE(O_error_message,
                                 I_process_id) = FALSE then
      return FALSE;
   end if;

   FORALL i IN 1..Lp_errors_tab.count
      insert into coresvc_costchg_err values Lp_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();

   open C_ERRORS_EXIST;
   fetch C_ERRORS_EXIST into L_tot_error_cnt,
                             L_warning_cnt;
   close C_ERRORS_EXIST;
   ---
   if L_tot_error_cnt = 0 then
      L_process_status := 'PS';
   elsif L_tot_error_cnt > 0 then
      if L_tot_error_cnt = L_warning_cnt then
         L_process_status := 'PW';
      elsif L_tot_error_cnt != L_warning_cnt then
         L_process_status := 'PE';
      end if;
   end if;

   update svc_process_tracker
      set status = (case
                       when status = 'PE'
                          then 'PE'
                       when status = 'PW'
                          then 'PW'
                       else L_process_status
                    end),
           action_date = sysdate
   where process_id = I_process_id;

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
FUNCTION UPDATE_RECALC_ORD_IND(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id            IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                               I_chunk_id              IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE,
                               I_cost_change           IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                               I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                               I_supplier              IN       SUPS.SUPPLIER%TYPE,
                               I_origin_country        IN       COUNTRY.COUNTRY_ID%TYPE,
                               I_recalc_ord_ind        IN       COST_CHANGE_TEMP.RECALC_ORD_IND%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program              VARCHAR2(61) := 'CORESVC_COST_CHANGE.UPDATE_RECALC_ORD_IND';

BEGIN
   update cost_change_temp
      set recalc_ord_ind      = I_recalc_ord_ind
    where item                = I_item
      and supplier            = I_supplier
      and origin_country_id   = I_origin_country
      and cost_change         = I_cost_change;

   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_RECALC_ORD_IND;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_supplier        IN       SUPS.SUPPLIER%TYPE,
                            I_table           IN       VARCHAR2,
                            I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE,
                            I_row_seq         IN       SVC_COST_SUSP_SUP_DETAIL.ROW_SEQ%TYPE)
   RETURN BOOLEAN IS

   L_valid               BOOLEAN          := FALSE;
   L_sups_row            V_SUPS%ROWTYPE;
   L_table               VARCHAR2(255)    := I_table;
   L_program             VARCHAR2(255)    :='CORESVC_COSTCHG.VALIDATE_SUPPLIER';
   L_error               BOOLEAN;

BEGIN

   -- Check if supplier is valid
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(O_error_message,
                                                L_valid,
                                                L_sups_row,
                                                I_supplier) = FALSE then
      -- Write error 'Invalid supplier.'
      return FALSE;
   end if;

   if L_valid = FALSE then
      if LP_system_options.supplier_sites_ind = 'Y' and L_sups_row.sup_name is NULL then
         -- Write error 'Invalid supplier.'
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     I_row_seq,
                     NULL,
                     'INV_SUPPLIER_SITE');
         L_error := TRUE;
      else
         -- Write error 'Invalid supplier.'
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     I_row_seq,
                     NULL,
                     'INV_SUPPLIER');
         L_error := TRUE;
      end if;
   else
      if LP_system_options.supplier_sites_ind = 'Y' and L_sups_row.sup_name is NULL then
         -- Write error 'Invalid supplier.'
         WRITE_ERROR(I_process_id,
                     CORESVC_COSTCHG_ESEQ.nextval,
                     I_chunk_id,
                     L_table,
                     I_row_seq,
                     NULL,
                     'INV_SUPPLIER_SITE');
         L_error := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_SUPPLIER;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       ITEM_MASTER.ITEM%TYPE,
                        I_supplier        IN       SUPS.SUPPLIER%TYPE,
                        I_table           IN       VARCHAR2,
                        I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE,
                        I_row_seq         IN       SVC_COST_SUSP_SUP_DETAIL.ROW_SEQ%TYPE)
   RETURN BOOLEAN IS

   L_valid               BOOLEAN          := FALSE;
   L_item_master         V_ITEM_MASTER%ROWTYPE;
   L_prim_supp           SUPS.SUPPLIER%TYPE;
   L_country             ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_pack_ind            ITEM_MASTER.PACK_IND%TYPE;
   L_pack_type           ITEM_MASTER.PACK_TYPE%TYPE;
   L_orderable           ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_sellable            ITEM_MASTER.SELLABLE_IND%TYPE;
   L_table               VARCHAR2(255)    := I_table;
   L_program             VARCHAR2(255)    :='CORESVC_COSTCHG.VALIDATE_ITEM';
   L_error               BOOLEAN;

BEGIN

   L_valid := FALSE;
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_ITEM_MASTER(O_error_message,
                                                   L_valid,
                                                   L_item_master,
                                                   I_item) = FALSE then
      return FALSE;
   end if;

   if L_valid = FALSE then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  'ITEM',
                  O_error_message,
                  I_item,
                  I_supplier);
      L_error := TRUE;
   end if;

   if NOT ITEM_SUPP_COUNTRY_SQL.GET_PRIM_SUPP_CNTRY(O_error_message,
                                                    L_prim_supp,
                                                    L_country,
                                                    I_item) then
      return FALSE;
   end if;

   if L_prim_supp is NULL and L_country is NULL then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  'ITEM',
                  'ITEM_SUP_REL_NOT_EXIST',
                  I_item,
                  I_supplier);
      L_error := TRUE;
   end if;

   -- Only approved items can be placed on a cost change
   if L_item_master.status != 'A' then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  'ITEM',
                  'ITEM_STATUS',
                  I_item,
                  I_supplier);
      L_error := TRUE;
   end if;

   -- Item must be a transaction level or parent item
   if L_item_master.item_level > L_item_master.tran_level then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  'ITEM',
                  'INV_ITEM_LEVEL',
                  I_item,
                  I_supplier);
      L_error := TRUE;
   end if;

   --- Check for pack type, Buyer pack is not allowed
   if NOT ITEM_ATTRIB_SQL.GET_PACK_INDS (O_error_message,
                                         L_pack_ind,
                                         L_sellable,
                                         L_orderable,
                                         L_pack_type,
                                         I_item) then
      return FALSE;
   end if;

   if (L_pack_ind = 'Y' and NVL(L_pack_type,'N') = 'B') then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  'ITEM',
                  'NON_ORD_CC',
                  I_item,
                  I_supplier);
      L_error := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ITEM;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_COUNTRY(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_origin_country_id  IN       COUNTRY.COUNTRY_ID%TYPE,
                          I_item               IN       ITEM_MASTER.ITEM%TYPE,
                          I_supplier           IN       SUPS.SUPPLIER%TYPE,
                          I_table              IN       VARCHAR2,
                          I_process_id         IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                          I_chunk_id           IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE,
                          I_row_seq            IN       SVC_COST_SUSP_SUP_DETAIL.ROW_SEQ%TYPE)
   RETURN BOOLEAN IS

   L_valid               BOOLEAN          := FALSE;
   L_table               VARCHAR2(255)    := I_table;
   L_program             VARCHAR2(255)    := 'CORESVC_COSTCHG.VALIDATE_COUNTRY';
   L_error               BOOLEAN;

BEGIN

   L_valid := FALSE;
   -- Item-Country Validation
   if SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                             L_valid,
                                             I_item,
                                             I_supplier,
                                             I_origin_country_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_COUNTRY;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item                IN       SVC_COST_SUSP_SUP_DETAIL_LOC.ITEM%TYPE,
                           I_loc_type            IN       SVC_COST_SUSP_SUP_DETAIL_LOC.LOC_TYPE%TYPE,
                           I_loc                 IN       SVC_COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                           I_table               IN       VARCHAR2,
                           I_process_id          IN       SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE,
                           I_chunk_id            IN       SVC_COST_SUSP_SUP_DETAIL_LOC.CHUNK_ID%TYPE,
                           I_row_seq             IN       SVC_COST_SUSP_SUP_DETAIL_LOC.ROW_SEQ%TYPE)
   RETURN BOOLEAN IS

   L_valid                   BOOLEAN          := FALSE;
   L_table                   VARCHAR2(255)    := I_table;
   L_program                 VARCHAR2(255)    := 'CORESVC_COSTCHG.VALIDATE_LOCATION';
   L_error                   BOOLEAN;
   L_wh_flag                 BOOLEAN;

   L_valid_loc               BOOLEAN;
   L_loc_name                V_STORE.STORE_NAME%TYPE;
   L_stockholding_ind        V_STORE.STOCKHOLDING_IND%TYPE;
   L_company_exist           BOOLEAN := FALSE;
   L_cc_status               COST_SUSP_SUP_HEAD.STATUS%TYPE;
   L_val_loc_type            VARCHAR2(1) := NULL;

   cursor C_VAL_LOC_TYPE is
     select 'x'
       from code_detail
      where code_type = 'GRTC'
        and code = I_loc_type;
BEGIN

   open C_VAL_LOC_TYPE;
   fetch C_VAL_LOC_TYPE into L_val_loc_type;
   close C_VAL_LOC_TYPE;

   if L_val_loc_type is NULL then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  'LOC_TYPE',
                  'INV_LOC_TYPE',
                  I_item);
      L_error := TRUE;
   end if;

   if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION(O_error_message,
                                                L_valid_loc,
                                                L_loc_name,
                                                L_stockholding_ind,
                                                I_loc) = FALSE then
      return FALSE;
   end if;

   if not L_valid_loc then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  NULL,
                  'INVALID_LOCATION',
                  I_item);
      L_error := TRUE;
   else
      if I_loc_type in ('DW', 'W') then
         if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                    L_wh_flag,
                                    I_loc) = FALSE then
            return FALSE;
          end if;

         if L_wh_flag = FALSE then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        I_row_seq,
                        'LOCATION',
                        'VIRTUAL_WH_NOT_ALLOWED',
                        I_item);
            L_error := TRUE;
         end if;

         if ITEMLOC_ATTRIB_SQL.ITEM_WH_EXIST(O_error_message,
                                             I_item,
                                             I_loc,
                                             L_valid) = FALSE then
             return FALSE;
         end if;

         if L_valid = FALSE then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        I_row_seq,
                        'LOCATION',
                        'NO_ITEM_WH',
                        I_item);
            L_error := TRUE;
         end if;
      end if;

      if I_loc_type ='S' then
         if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                             I_item,
                                             I_loc,
                                             L_valid) = FALSE then
            return FALSE;
         end if;
         if L_valid = FALSE then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        I_row_seq,
                        'LOCATION',
                        'INV_SKU_LOC_SHORT',
                        I_item);
            L_error := TRUE;
         end if;
         L_loc_name := NULL;
         if FILTER_LOV_VALIDATE_SQL.COMPANY_STORE(O_error_message,
                                                  L_company_exist,
                                                  L_loc_name,
                                                  I_loc) = FALSE then
            return FALSE;
         end if;
         ---
         if L_company_exist = FALSE then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        I_chunk_id,
                        L_table,
                        I_row_seq,
                        'LOCATION',
                        'MUST_BE_COMP_STORE',
                        I_item);
            L_error := TRUE;
         end if;
      end if;
   end if;

   if L_error then
      update SVC_COST_SUSP_SUP_DETAIL_LOC
        set process$status = 'E',
            last_upd_id = LP_user,
            last_upd_datetime = sysdate
      where process_id = I_process_id
        and chunk_id = I_chunk_id
        and row_seq = I_row_seq;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_LOCATION;
--------------------------------------------------------------------------------
FUNCTION GEN_CC_NUMBERS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(75) := 'CORESVC_COSTCHG.GEN_CC_NUMBERS';
   
   -- This cursor fetches records that need new cost_change numbers to be generated
   cursor C_NEW_CC is
      select sch.cost_change,
             sch.rowid AS rid,
             sch.row_seq
        from svc_cost_susp_sup_head sch
       where sch.process_id    = I_process_id
         and sch.chunk_id      = I_chunk_id
         and sch.action        = ACTION_NEW;

   TYPE C_NEW_CC_TAB_TYP IS TABLE OF C_NEW_CC%ROWTYPE;

   C_NEW_CC_TAB      C_NEW_CC_TAB_TYP;
   L_error           BOOLEAN   :=FALSE;
   L_new_cc_no       SVC_COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;
   L_return_code     VARCHAR2(5);

BEGIN

   open C_NEW_CC;
   fetch C_NEW_CC BULK COLLECT into C_NEW_CC_TAB;
   close C_NEW_CC;

   FOR i IN 1..C_NEW_CC_TAB.COUNT() LOOP

      L_error           := FALSE;
      L_return_code     := 'TRUE';
      O_error_message   := NULL;
      L_new_cc_no       := NULL;

      -- Generate new cost_change number
      NEXT_COSTCHG_NUMBER(L_new_cc_no,
                          L_return_code,
                          O_error_message);

     if L_return_code = 'FALSE' then
        L_error := TRUE;
     else
        update svc_cost_susp_sup_detail_loc
           set cost_change = L_new_cc_no
         where process_id = I_process_id
           and chunk_id    = I_chunk_id
           and cost_change = C_NEW_CC_TAB(i).cost_change
           and action      = ACTION_NEW;

        update svc_cost_susp_sup_detail
           set cost_change = L_new_cc_no
         where process_id = I_process_id
           and chunk_id   = I_chunk_id
           and cost_change = C_NEW_CC_TAB(i).cost_change
           and action      = ACTION_NEW;

        update svc_cfa_ext
           set keys_col = key_val_pairs(key_value_pair('COST_CHANGE',L_new_cc_no))
         where process_id = I_process_id
           and chunk_id   = I_chunk_id
           and key_val_pairs_pkg.get_attr(keys_col,'COST_CHANGE') = to_char(C_NEW_CC_TAB(i).cost_change);

        update svc_cost_susp_sup_head
           set cost_change = L_new_cc_no,
               last_upd_id = LP_user,
               last_upd_datetime = sysdate
         where rowid = C_NEW_CC_TAB(i).rid;
     end if;


     if L_error then
       update svc_cost_susp_sup_head
          set process$status = 'E',
              last_upd_id = LP_user,
              last_upd_datetime = sysdate
        where rowid = C_NEW_CC_TAB(i).rid;
        WRITE_ERROR(I_process_id,
                    CORESVC_COSTCHG_ESEQ.NEXTVAL,
                    I_chunk_id,
                    'SVC_COST_SUSP_SUP_HEAD',
                    C_NEW_CC_TAB(i).row_seq,
                    NULL,
                    O_error_message);
     end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GEN_CC_NUMBERS;
--------------------------------------------------------------------------------
FUNCTION UPDATE_BRACKET_DTLS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_bracket_uom1        IN     SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                             I_cost_change         IN     SVC_COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                             I_item                IN     SVC_COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                             I_supplier            IN     SVC_COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                             I_origin_country_id   IN     SVC_COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID%TYPE,
                             I_loc                 IN     SVC_COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(75) := 'CORESVC_COSTCHG.UPDATE_BRACKET_DTLS';

BEGIN

   if I_loc is NULL then
      update cost_change_temp
         set default_bracket_ind = 'Y',
             bracket_uom = I_bracket_uom1
       where cost_change = I_cost_change
         and item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id;
    else
      update cost_change_loc_temp
         set default_bracket_ind = 'Y',
             bracket_uom = I_bracket_uom1
       where cost_change = I_cost_change
         and item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id
         and location = I_loc;
    end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_BRACKET_DTLS;
------------------------------------------------------------------------------------------
FUNCTION APPLY_CHANGE (O_error_message       IN OUT   VARCHAR2,
                       I_process_id          IN       SVC_COST_SUSP_SUP_DETAIL.PROCESS_ID%TYPE,
                       I_chunk_id            IN       SVC_COST_SUSP_SUP_DETAIL.CHUNK_ID%TYPE,
                       I_row_seq             IN       SVC_COST_SUSP_SUP_DETAIL.ROW_SEQ%TYPE,
                       I_cost_change         IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE,
                       I_change_type         IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE_TYPE%TYPE,
                       I_change_amount       IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE_VALUE%TYPE,
                       I_supplier            IN       SVC_COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                       I_item                IN       SVC_COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                       I_origin_country_id   IN       SVC_COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_error               BOOLEAN := FALSE;
   ---
   L_program              VARCHAR2(75) := 'CORESVC_COSTCHG.APPLY_CHANGE';
   L_bracket_value1       COST_CHANGE_TEMP.BRACKET_VALUE1%TYPE;
   ---
   L_unit_cost_new        COST_CHANGE_TEMP.UNIT_COST_NEW%TYPE;
   L_unit_cost_old        COST_CHANGE_TEMP.UNIT_COST_OLD%TYPE;
   L_unit_cost_cuom_new   COST_CHANGE_TEMP.UNIT_COST_CUOM_NEW%TYPE;
   L_unit_cost_cuom_old   COST_CHANGE_TEMP.UNIT_COST_CUOM_OLD%TYPE;
   L_delivery_country_id  COUNTRY.COUNTRY_ID%TYPE;
   ---
   L_table                VARCHAR2(30);
   RECORD_LOCKED          EXCEPTION;

   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
   ---

   cursor C_PROCESS_COST_CHANGE_TEMP is
      select NVL(unit_cost_orig, unit_cost_old) unit_cost_old,
             NVL(unit_cost_cuom_orig, unit_cost_cuom_old) unit_cost_cuom_old,
             bracket_value1,
             delivery_country_id,
             rowid
        from cost_change_temp
       where cost_change = I_cost_change
         and item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id;

   ---
   cursor C_LOCK_COST_CHANGE_TEMP is
      select 'x'
        from cost_change_temp
       where supplier              = I_supplier
         and origin_country_id     = I_origin_country_id
         and item                  = I_item
         and NVL(bracket_value1,0) = L_bracket_value1
         for update nowait;
    ---

BEGIN
   if I_change_type = 'P' and I_change_amount < -100 then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.NEXTVAL,
                  I_chunk_id,
                  'SVC_COST_SUSP_SUP_HEAD',
                  I_row_seq,
                  'COST_CHANGE_VALUE',
                  'U/P_COST_NOT_NEG',
                  I_item,
                  I_supplier,
                  I_origin_country_id);
      L_error := TRUE;
   end if;
   ---
   If I_change_type = 'F' and I_change_amount < 0 then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.NEXTVAL,
                  I_chunk_id,
                  'SVC_COST_SUSP_SUP_HEAD',
                  I_row_seq,
                  'COST_CHANGE_VALUE',
                  'U/P_COST_NOT_NEG',
                  I_item,
                  I_supplier,
                  I_origin_country_id);
      L_error := TRUE;
   end if;
   ---

   -- Update the rows in the COST_CHANGE_TEMP
   -- per supplier/origin_country/item combination.

   if L_error = FALSE then
      FOR rec in C_PROCESS_COST_CHANGE_TEMP LOOP
         L_bracket_value1      := NVL(rec.bracket_value1,0);

         L_unit_cost_old       := rec.unit_cost_old;
         L_unit_cost_cuom_old  := rec.unit_cost_cuom_old;
         L_delivery_country_id := rec.delivery_country_id;

         if I_change_type = 'P' then
            L_unit_cost_cuom_new := L_unit_cost_cuom_old * (1 + I_change_amount/100);

         elsif I_change_type = 'A' then
            L_unit_cost_cuom_new  := L_unit_cost_cuom_old + I_change_amount;

         elsif I_change_type = 'F' then
            L_unit_cost_cuom_new  := I_change_amount;
         end if;

         if (L_unit_cost_cuom_new < 0) then
            WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.NEXTVAL,
                        I_chunk_id,
                        'SVC_COST_SUSP_SUP_HEAD',
                        I_row_seq,
                        'COST_CHANGE_VALUE',
                        'U/P_COST_NOT_NEG',
                        I_item,
                        I_supplier,
                        I_origin_country_id);
            L_error := TRUE;
         end if;

         if L_error = FALSE then
            L_unit_cost_new      := L_unit_cost_cuom_new;

            if ITEM_SUPP_COUNTRY_SQL.CONVERT_COST(O_error_message,
                                                  L_unit_cost_new,
                                                  I_item,
                                                  I_supplier,
                                                  I_origin_country_id,
                                                  'C') = FALSE then
                return FALSE;
            end if;

            L_table := 'COST_CHANGE_TEMP';

            open C_LOCK_COST_CHANGE_TEMP;
            close C_LOCK_COST_CHANGE_TEMP;

            update cost_change_temp
               set unit_cost_cuom_new            = L_unit_cost_cuom_new,
                   unit_cost_new                 = L_unit_cost_new,
                   cost_change_type              = I_change_type,
                   cost_change_value             = I_change_amount,
                   unit_cost_old                 = L_unit_cost_old,
                   unit_cost_cuom_old            = L_unit_cost_cuom_old
             where rowid = rec.rowid;
         else
            delete from cost_change_temp
             where rowid = rec.rowid;

            update svc_cost_susp_sup_detail
               set process$status = 'E',
                   last_upd_id = LP_user,
                   last_upd_datetime = sysdate
             where process_id = I_process_id
               and chunk_id = I_chunk_id
               and supplier = I_supplier
               and origin_country_id = I_origin_country_id
               and item = I_item;
         end if;
      END LOOP;
   else
      delete from cost_change_temp
       where  cost_change = I_cost_change
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id
         and item = I_item;

      update svc_cost_susp_sup_detail
         set process$status = 'E',
             last_upd_id = LP_user,
             last_upd_datetime = sysdate
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id
         and item = I_item;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_supplier) || ', ' || I_origin_country_id,
                                            I_item);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'APPLY_CHANGE',
                                             to_char(SQLCODE));
      return FALSE;
END APPLY_CHANGE;
------------------------------------------------------------------------------------------
FUNCTION APPLY_CHANGE_LOC (O_error_message    IN OUT VARCHAR2,
                           I_process_id       IN     SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE,
                           I_chunk_id         IN     SVC_COST_SUSP_SUP_DETAIL_LOC.CHUNK_ID%TYPE,
                           I_row_seq          IN     SVC_COST_SUSP_SUP_DETAIL_LOC.ROW_SEQ%TYPE,
                           I_cost_change      IN     COST_CHANGE_LOC_TEMP.COST_CHANGE%TYPE,
                           I_supplier         IN     COST_CHANGE_LOC_TEMP.SUPPLIER%TYPE,
                           I_country          IN     COST_CHANGE_LOC_TEMP.ORIGIN_COUNTRY_ID%TYPE,
                           I_item             IN     COST_CHANGE_LOC_TEMP.ITEM%TYPE,
                           I_loc_type         IN     COST_CHANGE_LOC_TEMP.LOC_TYPE%TYPE,
                           I_location         IN     VARCHAR2,
                           I_bracket_value    IN     COST_CHANGE_LOC_TEMP.BRACKET_VALUE1%TYPE,
                           I_change_type      IN     VARCHAR2,
                           I_change_amount    IN     NUMBER)
   RETURN BOOLEAN IS
   ---
   L_new_unit_cost        COST_CHANGE_LOC_TEMP.UNIT_COST_NEW%TYPE;
   ---
   L_unit_cost_new        COST_CHANGE_TEMP.UNIT_COST_NEW%TYPE;
   L_unit_cost_old        COST_CHANGE_TEMP.UNIT_COST_OLD%TYPE;
   L_unit_cost_cuom_new   COST_CHANGE_TEMP.UNIT_COST_CUOM_NEW%TYPE;
   L_unit_cost_cuom_old   COST_CHANGE_TEMP.UNIT_COST_CUOM_OLD%TYPE;
   L_loc_type             VARCHAR(5);
   L_loc_type_SW          VARCHAR2(1);
   L_table                VARCHAR2(50) := 'COST_CHANGE_LOC_TEMP';
   L_error                BOOLEAN := FALSE;

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   cursor C_PROCESS_COST_CHANGE_LOC_AL is
      select location,
             loc_type,
             unit_cost_orig unit_cost_old,
             unit_cost_cuom_orig unit_cost_cuom_old
        from cost_change_loc_temp
       where cost_change       = I_cost_change
         and item              = I_item
         and origin_country_id = I_country
         and supplier          = I_supplier;

   cursor C_LOCK_COST_CHANGE_LOC_AL is
      select 'x'
        from cost_change_loc_temp
       where item              = I_item
         and origin_country_id = I_country
         and supplier          = I_supplier;

   cursor C_PROCESS_COST_CHANGE_LOC_ASAW is
      select location,
             NVL(unit_cost_orig, unit_cost_old) unit_cost_old,
             NVL(unit_cost_cuom_orig, unit_cost_cuom_old) unit_cost_cuom_old
        from cost_change_loc_temp
       where loc_type          = L_loc_type_SW
         and item              = I_item
         and origin_country_id = I_country
         and supplier          = I_supplier;

   cursor C_LOCK_COST_CHANGE_LOC_ASAW is
      select 'x'
        from cost_change_loc_temp
       where loc_type          = L_loc_type_SW
         and item              = I_item
         and origin_country_id = I_country
         and supplier          = I_supplier;

   cursor C_PROCESS_COST_CHANGE_LOC_SW is
      select NVL(unit_cost_orig, unit_cost_old) unit_cost_old,
             NVL(unit_cost_cuom_orig, unit_cost_cuom_old) unit_cost_cuom_old
        from cost_change_loc_temp
       where loc_type          = L_loc_type_SW
         and location          = TO_NUMBER(I_location)
         and item              = I_item
         and origin_country_id = I_country
         and supplier          = I_supplier;

   cursor C_LOCK_COST_CHANGE_LOC_SW is
      select 'x'
        from cost_change_loc_temp
       where cost_change       = I_cost_change
         and loc_type          = L_loc_type_SW
         and location          = TO_NUMBER(I_location)
         and item              = I_item
         and origin_country_id = I_country
         and supplier          = I_supplier;

   cursor C_PROCESS_COST_CHANGE_LOC is
      select location,
             NVL(unit_cost_orig, unit_cost_old) unit_cost_old,
             NVL(unit_cost_cuom_orig, unit_cost_cuom_old) unit_cost_cuom_old
        from cost_change_loc_temp
       where location in (select store
                            from store
                           where store_class        = I_location
                             and L_loc_type         = 'C'
                           UNION
                          select store
                            from store
                           where district           = TO_NUMBER(I_location)
                             and L_loc_type         = 'D'
                           UNION
                          select s.store
                            from store    s,
                                 district d,
                                 region   r
                           where d.district         = s.district
                             and r.region           = d.region
                             and r.area             = TO_NUMBER(I_location)
                             and L_loc_type         = 'A'
                           UNION
                          select s.store
                            from store    s,
                                 district d
                           where d.district         = s.district
                             and d.region           = TO_NUMBER(I_location)
                             and L_loc_type         = 'R'
                           UNION
                          select location
                            from loc_list_detail
                           where loc_list           = TO_NUMBER(I_location)
                             and L_loc_type         = 'LLS'
                           UNION
                          select s.store
                            from store s,
                                 loc_traits_matrix ltm
                           where ltm.store          = s.store
                             and ltm.loc_trait      = TO_NUMBER(I_location)
                             and L_loc_type         = 'L'
                           UNION
                          select distinct wh.physical_wh
                            from wh,
                                 loc_list_detail l
                           where (l.location        = wh.wh
                                  or wh.physical_wh = l.location)
                             and loc_list           = TO_NUMBER(I_location)
                             and L_loc_type         = 'LLW')
         and item              = I_item
         and origin_country_id = I_country
         and supplier          = I_supplier
         and loc_type          = L_loc_type_SW;

   cursor C_LOCK_COST_CHANGE_LOC is
      select 'x'
        from cost_change_loc_temp
       where location in (select store
                            from store
                           where store_class   = I_location
                             and L_loc_type    = 'C'
                           UNION
                          select store
                            from store
                           where district      = TO_NUMBER(I_location)
                             and L_loc_type    = 'D'
                           UNION
                          select s.store
                            from store    s,
                                 district d,
                                 region   r
                           where d.district    = s.district
                             and r.region      = d.region
                             and r.area        = TO_NUMBER(I_location)
                             and L_loc_type    = 'A'
                           UNION
                          select s.store
                            from store    s,
                                 district d
                           where d.district    = s.district
                             and d.region      = TO_NUMBER(I_location)
                             and L_loc_type    = 'R'
                           UNION
                          select location
                            from loc_list_detail
                           where loc_list      = TO_NUMBER(I_location)
                             and L_loc_type    = 'LLS'
                           UNION
                          select s.store
                            from store s,
                                 loc_traits_matrix ltm
                           where ltm.store     = s.store
                             and ltm.loc_trait = TO_NUMBER(I_location)
                             and L_loc_type    = 'L'
                           UNION
                          select distinct wh.physical_wh
                            from wh,
                                 loc_list_detail l
                           where (l.location        = wh.wh
                                  or wh.physical_wh = l.location)
                             and loc_list           = TO_NUMBER(I_location)
                             and L_loc_type    = 'LLW')
         and item              = I_item
         and origin_country_id = I_country
         and supplier          = I_supplier
         and loc_type          = L_loc_type_SW;

BEGIN

   if I_loc_type = 'AL' then
      NULL;
   elsif I_loc_type in ('AS','S') then
      L_loc_type_SW := 'S';
   elsif I_loc_type in ('AW','W','LLW') then
      L_loc_type    := I_loc_type;
      L_loc_type_SW := 'W';
   elsif I_loc_type in ('C','D','A','R','LLS','L') then
      L_loc_type    := I_loc_type;
      L_loc_type_SW := 'S';
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',NULL,NULL,NULL);
         return FALSE;
   end if;
   ---
   if I_change_type = 'P' and I_change_amount < -100 then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.NEXTVAL,
                  I_chunk_id,
                  'SVC_COST_SUSP_SUP_HEAD',
                  I_row_seq,
                  'COST_CHANGE_VALUE',
                  'U/P_COST_NOT_NEG',
                  I_item,
                  I_supplier,
                  I_country);
      L_error := TRUE;
   end if;
   ---
   If I_change_type = 'F' and I_change_amount < 0 then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.NEXTVAL,
                  I_chunk_id,
                  'SVC_COST_SUSP_SUP_HEAD',
                  I_row_seq,
                  'COST_CHANGE_VALUE',
                  'U/P_COST_NOT_NEG',
                  I_item,
                  I_supplier,
                  I_country);
      L_error := TRUE;
   end if;
   ---

   if L_error = FALSE then
      if I_loc_type = 'AL' then                     -- for ALL locations
         FOR rec in C_PROCESS_COST_CHANGE_LOC_AL LOOP
            L_unit_cost_old       := rec.unit_cost_old;
            L_unit_cost_cuom_old  := rec.unit_cost_cuom_old;

            if I_change_type = 'P' then
               L_unit_cost_cuom_new := L_unit_cost_cuom_old * (1 + I_change_amount/100);
            elsif I_change_type = 'A' then
               L_unit_cost_cuom_new := L_unit_cost_cuom_old + I_change_amount;
            elsif I_change_type = 'F' then
               L_unit_cost_cuom_new := I_change_amount;
            end if;

            if (L_unit_cost_cuom_new < 0) then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.NEXTVAL,
                           I_chunk_id,
                           'SVC_COST_SUSP_SUP_HEAD',
                           I_row_seq,
                           'COST_CHANGE_VALUE',
                           'U/P_COST_NOT_NEG',
                           I_item,
                           I_supplier,
                           I_country);
               L_error := TRUE;
            end if;

            if L_error = FALSE then
               L_unit_cost_new  := L_unit_cost_cuom_new;

               if ITEM_SUPP_COUNTRY_SQL.CONVERT_COST(O_error_message,
                                                     L_unit_cost_new,
                                                     I_item,
                                                     I_supplier,
                                                     I_country,
                                                     'C') = FALSE then
                   return FALSE;
               end if;

               open  C_LOCK_COST_CHANGE_LOC_AL;
               close C_LOCK_COST_CHANGE_LOC_AL;

               update cost_change_loc_temp
                  set unit_cost_new      = L_unit_cost_new,
                      unit_cost_cuom_new = L_unit_cost_cuom_new,
                      cost_change_type   = I_change_type,
                      cost_change_value  = I_change_amount,
                      unit_cost_old      = L_unit_cost_old,
                      unit_cost_cuom_old = L_unit_cost_cuom_old
                where item               = I_item
                  and origin_country_id  = I_country
                  and supplier           = I_supplier
                  and location           = rec.location
                  and loc_type           = rec.loc_type;
            else
               delete from cost_change_loc_temp
                where cost_change       = I_cost_change
                  and item              = I_item
                  and supplier          = I_supplier
                  and origin_country_id = I_country
                  and location          = I_location;

               update svc_cost_susp_sup_detail_loc
                  set process$status    = 'E',
                      last_upd_id = LP_user,
                      last_upd_datetime = sysdate
                where process_id        = I_process_id
                  and chunk_id          = I_chunk_id
                  and cost_change       = I_cost_change
                  and item              = I_item
                  and supplier          = I_supplier
                  and origin_country_id = I_country
                  and loc               = I_location;
            end if;
         END LOOP;
      elsif I_loc_type in ('AS','AW') then              -- ALL STORES or ALL WAREHOUSE
         FOR rec in C_PROCESS_COST_CHANGE_LOC_ASAW LOOP
            L_unit_cost_old       := rec.unit_cost_old;
            L_unit_cost_cuom_old  := rec.unit_cost_cuom_old;

            if I_change_type    = 'P' then
               L_unit_cost_cuom_new := L_unit_cost_cuom_old * (1 + I_change_amount/100);
            elsif I_change_type = 'A' then
               L_unit_cost_cuom_new := L_unit_cost_cuom_old + I_change_amount;
            elsif I_change_type = 'F' then
               L_unit_cost_cuom_new := I_change_amount;
            end if;

            if (L_unit_cost_cuom_new < 0) then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.NEXTVAL,
                           I_chunk_id,
                           'SVC_COST_SUSP_SUP_HEAD',
                           I_row_seq,
                           'COST_CHANGE_VALUE',
                           'U/P_COST_NOT_NEG',
                           I_item,
                           I_supplier,
                           I_country);
               L_error := TRUE;
            end if;

            if L_error = FALSE then
               L_unit_cost_new  := L_unit_cost_cuom_new;

               if ITEM_SUPP_COUNTRY_SQL.CONVERT_COST(O_error_message,
                                                     L_unit_cost_new,
                                                     I_item,
                                                     I_supplier,
                                                     I_country,
                                                     'C') = FALSE then
                   return FALSE;
               end if;

               open  C_LOCK_COST_CHANGE_LOC_ASAW;
               close C_LOCK_COST_CHANGE_LOC_ASAW;

               update cost_change_loc_temp
                  set unit_cost_new      = L_unit_cost_new,
                      unit_cost_cuom_new = L_unit_cost_cuom_new,
                      cost_change_type   = I_change_type,
                      cost_change_value  = I_change_amount,
                      unit_cost_old      = L_unit_cost_old,
                      unit_cost_cuom_old = L_unit_cost_cuom_old
                where item               = I_item
                  and origin_country_id  = I_country
                  and supplier           = I_supplier
                  and location           = rec.location
                  and loc_type           = L_loc_type_SW;
            else
               delete from cost_change_loc_temp
                where cost_change       = I_cost_change
                  and item              = I_item
                  and supplier          = I_supplier
                  and origin_country_id = I_country
                  and location          = I_location;

               update svc_cost_susp_sup_detail_loc
                  set process$status    = 'E',
                      last_upd_id       = LP_user,
                      last_upd_datetime = sysdate
                where process_id        = I_process_id
                  and chunk_id          = I_chunk_id
                  and cost_change       = I_cost_change
                  and item              = I_item
                  and supplier          = I_supplier
                  and origin_country_id = I_country
                  and loc               = I_location;
            end if;
         end LOOP;
      elsif I_loc_type in ('S','W') then                    -- STORE or WAREHOUSE
         ---
         FOR rec in C_PROCESS_COST_CHANGE_LOC_SW LOOP
            L_unit_cost_old       := rec.unit_cost_old;
            L_unit_cost_cuom_old  := rec.unit_cost_cuom_old;

            if I_change_type    = 'P' then
               L_unit_cost_cuom_new := L_unit_cost_cuom_old * (1 + I_change_amount/100);
            elsif I_change_type = 'A' then
               L_unit_cost_cuom_new := L_unit_cost_cuom_old + I_change_amount;
            elsif I_change_type = 'F' then
               L_unit_cost_cuom_new := I_change_amount;
            end if;

            if (L_unit_cost_cuom_new < 0) then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.NEXTVAL,
                           I_chunk_id,
                           'SVC_COST_SUSP_SUP_HEAD',
                           I_row_seq,
                           'COST_CHANGE_VALUE',
                           'U/P_COST_NOT_NEG',
                           I_item,
                           I_supplier,
                           I_country);
               L_error := TRUE;
            end if;

            if L_error = FALSE then
               L_unit_cost_new  := L_unit_cost_cuom_new;

               if ITEM_SUPP_COUNTRY_SQL.CONVERT_COST(O_error_message,
                                                     L_unit_cost_new,
                                                     I_item,
                                                     I_supplier,
                                                     I_country,
                                                     'C') = FALSE then
                   return FALSE;
               end if;

               open  C_LOCK_COST_CHANGE_LOC_SW;
               close C_LOCK_COST_CHANGE_LOC_SW;

               update cost_change_loc_temp
                  set unit_cost_new      = L_unit_cost_new,
                      unit_cost_cuom_new = L_unit_cost_cuom_new,
                      cost_change_type   = I_change_type,
                      cost_change_value  = I_change_amount,
                      unit_cost_old      = L_unit_cost_old,
                      unit_cost_cuom_old = L_unit_cost_cuom_old
                where cost_change        = I_cost_change
                  and item               = I_item
                  and origin_country_id  = I_country
                  and supplier           = I_supplier
                  and location           = TO_NUMBER(I_location)
                  and loc_type           = L_loc_type_SW;
            else
               delete from cost_change_loc_temp
                where cost_change       = I_cost_change
                  and item              = I_item
                  and supplier          = I_supplier
                  and origin_country_id = I_country
                  and location          = I_location;

               update svc_cost_susp_sup_detail_loc
                  set process$status    = 'E',
                      last_upd_id       = LP_user,
                      last_upd_datetime = sysdate
                where process_id        = I_process_id
                  and chunk_id          = I_chunk_id
                  and cost_change       = I_cost_change
                  and item              = I_item
                  and supplier          = I_supplier
                  and origin_country_id = I_country
                  and loc               = I_location;
            end if;
         end LOOP;
         ---
      elsif I_loc_type in ('C','D','A','R','LLS','LLW','L') then
         ---
         FOR rec in C_PROCESS_COST_CHANGE_LOC LOOP
            L_unit_cost_old       := rec.unit_cost_old;
            L_unit_cost_cuom_old  := rec.unit_cost_cuom_old;

            if I_change_type    = 'P' then
               L_unit_cost_cuom_new := L_unit_cost_cuom_old * (1 + I_change_amount/100);
            elsif I_change_type = 'A' then
               L_unit_cost_cuom_new := L_unit_cost_cuom_old + I_change_amount;
            elsif I_change_type = 'F' then
               L_unit_cost_cuom_new := I_change_amount;
            end if;

            if (L_unit_cost_cuom_new < 0) then
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.NEXTVAL,
                           I_chunk_id,
                           'SVC_COST_SUSP_SUP_HEAD',
                           I_row_seq,
                           'COST_CHANGE_VALUE',
                           'U/P_COST_NOT_NEG',
                           I_item,
                           I_supplier,
                           I_country);
               L_error := TRUE;
            end if;

            if L_error = FALSE then
               L_unit_cost_new  := L_unit_cost_cuom_new;

               if ITEM_SUPP_COUNTRY_SQL.CONVERT_COST(O_error_message,
                                                     L_unit_cost_new,
                                                     I_item,
                                                     I_supplier,
                                                     I_country,
                                                     'C') = FALSE then
                   return FALSE;
               end if;

               open  C_LOCK_COST_CHANGE_LOC;
               close C_LOCK_COST_CHANGE_LOC;

               update cost_change_loc_temp
                  set unit_cost_new      = L_unit_cost_new,
                      unit_cost_cuom_new = L_unit_cost_cuom_new,
                      cost_change_type   = I_change_type,
                      cost_change_value  = I_change_amount,
                      unit_cost_old      = L_unit_cost_old,
                      unit_cost_cuom_old = L_unit_cost_cuom_old
                where item               = I_item
                  and origin_country_id  = I_country
                  and supplier           = I_supplier
                  and location           = rec.location
                  and loc_type           = L_loc_type_SW;
            else
               delete from cost_change_loc_temp
                where cost_change       = I_cost_change
                  and item              = I_item
                  and supplier          = I_supplier
                  and origin_country_id = I_country
                  and location          = I_location;

               update svc_cost_susp_sup_detail_loc
                  set process$status    = 'E',
                      last_upd_id       = LP_user,
                      last_upd_datetime = sysdate
                where process_id        = I_process_id
                  and chunk_id          = I_chunk_id
                  and cost_change       = I_cost_change
                  and item              = I_item
                  and supplier          = I_supplier
                  and origin_country_id = I_country
                  and loc               = I_location;
            end if;
         end LOOP;
         ---
      end if;
   else
      delete from cost_change_loc_temp
       where cost_change       = I_cost_change
         and item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_country
         and location          = I_location;

      update svc_cost_susp_sup_detail_loc
         set process$status    = 'E',
             last_upd_id       = LP_user,
             last_upd_datetime = sysdate
       where process_id        = I_process_id
         and chunk_id          = I_chunk_id
         and cost_change       = I_cost_change
         and item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_country
         and loc               = I_location;
   end if;

   ---
   return TRUE;
   ---
EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_supplier) || ', ' || I_country,
                                            I_item);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'APPLY_CHANGE_LOC',
                                             to_char(SQLCODE));
      return FALSE;
END APPLY_CHANGE_LOC;
------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CC_VALUE (O_error_message       IN OUT   VARCHAR2,
                            I_change_type         IN       COST_SUSP_SUP_DETAIL.COST_CHANGE_TYPE%TYPE,
                            I_change_amount       IN       COST_SUSP_SUP_DETAIL.COST_CHANGE_VALUE%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(75) := 'CORESVC_COSTCHG.VALIDATE_CC_VALUE';

   L_bd         NUMBER := 0;
   L_ad         NUMBER := 0;

   L_error      BOOLEAN := FALSE;

   cursor C_GET_CHANGE_AMOUNT is
     select (length(trunc(abs(I_change_amount)))) bd,
            (length(abs(I_change_amount) - trunc(abs(I_change_amount))) -1) ad
       from DUAL;
BEGIN

   open C_GET_CHANGE_AMOUNT;
   fetch C_GET_CHANGE_AMOUNT into L_bd,
                                  L_ad;
   close C_GET_CHANGE_AMOUNT;

   if I_change_type = 'P' then
      if L_bd > 9 or L_ad > 2 then
         O_error_message := 'COST_CHANGE_P_FORMAT';
         return FALSE;
      end if;
   else
      if L_bd > 13 or L_ad > 4 then
         O_error_message := 'COST_CHANGE_F_A_FORMAT';
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'APPLY_CHANGE',
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_CC_VALUE;
------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_COST_CHANGE(O_error_message  IN OUT VARCHAR2,
                                   I_cost_change    IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
   RETURN BOOLEAN IS

BEGIN
   merge into cost_susp_sup_detail cd using (select cost_change,
                                                    supplier,
                                                    origin_country_id,
                                                    item,
                                                    bracket_value1,
                                                    bracket_uom,
                                                    bracket_value2,
                                                    unit_cost_new,
                                                    cost_change_type,
                                                    cost_change_value,
                                                    NVL(default_bracket_ind,'N') default_bracket_ind,
                                                    recalc_ord_ind,
                                                    dept,
                                                    sup_dept_seq_no,
                                                    delivery_country_id
                                               from cost_change_temp
                                              where unit_cost_new is NOT NULL
                                                and cost_change = I_cost_change) ct
      on (cd.cost_change = ct.cost_change
          and cd.item = ct.item
          and cd.supplier = ct.supplier
          and cd.origin_country_id = ct.origin_country_id)
    when matched then
       update set cd.bracket_value1      = ct.bracket_value1,
                  cd.bracket_uom1        = ct.bracket_uom,
                  cd.bracket_value2      = ct.bracket_value2,
                  cd.unit_cost           = ct.unit_cost_new,
                  cd.cost_change_type    = ct.cost_change_type,
                  cd.cost_change_value   = ct.cost_change_value,
                  cd.default_bracket_ind = ct.default_bracket_ind,
                  cd.recalc_ord_ind      = ct.recalc_ord_ind
    when not matched then
       insert (cost_change,
               supplier,
               origin_country_id,
               item,
               bracket_value1,
               bracket_uom1,
               bracket_value2,
               unit_cost,
               cost_change_type,
               cost_change_value,
               default_bracket_ind,
               recalc_ord_ind,
               dept,
               sup_dept_seq_no,
               delivery_country_id)
       values (ct.cost_change,
               ct.supplier,
               ct.origin_country_id,
               ct.item,
               ct.bracket_value1,
               ct.bracket_uom,
               ct.bracket_value2,
               ct.unit_cost_new,
               ct.cost_change_type,
               ct.cost_change_value,
               ct.default_bracket_ind,
               ct.recalc_ord_ind,
               ct.dept,
               ct.sup_dept_seq_no,
               ct.delivery_country_id);
   --- For warehouses

   merge into cost_susp_sup_detail_loc cl using (select distinct cc.cost_change,
                                                        cc.supplier,
                                                        cc.origin_country_id,
                                                        cc.item,
                                                        cc.loc_type,
                                                        iscl.loc,
                                                        cc.bracket_value1,
                                                        cc.bracket_uom,
                                                        cc.bracket_value2,
                                                        cc.unit_cost_new,
                                                        cost_change_type,
                                                        cost_change_value,
                                                        NVL(cc.default_bracket_ind,'N')default_bracket_ind,
                                                        cc.recalc_ord_ind,
                                                        cc.dept,
                                                        cc.sup_dept_seq_no,
                                                        cc.delivery_country_id
                                                   from cost_change_loc_temp cc,
                                                        item_supp_country_loc iscl,
                                                        wh
                                                  where cc.item              = iscl.item
                                                    and cc.supplier          = iscl.supplier
                                                    and cc.origin_country_id = iscl.origin_country_id
                                                    and cc.location          = wh.physical_wh
                                                    and iscl.loc             = wh.wh
                                                    and cc.loc_type          = 'W'
                                                    and cc.unit_cost_new is NOT NULL
                                                    and cc.cost_change       = I_cost_change) clt
     on (cl.cost_change = clt.cost_change
         and cl.supplier = clt.supplier
         and cl.item = clt.item
         and cl.origin_country_id = clt.origin_country_id
         and cl.loc = clt.loc)
   when matched then
      update set cl.bracket_value1      = clt.bracket_value1,
                 cl.bracket_uom1        = clt.bracket_uom,
                 cl.bracket_value2      = clt.bracket_value2,
                 cl.unit_cost           = clt.unit_cost_new,
                 cl.cost_change_type    = clt.cost_change_type,
                 cl.cost_change_value   = clt.cost_change_value,
                 cl.default_bracket_ind = clt.default_bracket_ind,
                 cl.recalc_ord_ind      = clt.recalc_ord_ind
   when not matched then
      insert (cost_change,
              supplier,
              origin_country_id,
              item,
              loc_type,
              loc,
              bracket_value1,
              bracket_uom1,
              bracket_value2,
              unit_cost,
              cost_change_type,
              cost_change_value,
              default_bracket_ind,
              recalc_ord_ind,
              dept,
              sup_dept_seq_no,
              delivery_country_id)
       values (clt.cost_change,
               clt.supplier,
               clt.origin_country_id,
               clt.item,
               clt.loc_type,
               clt.loc,
               clt.bracket_value1,
               clt.bracket_uom,
               clt.bracket_value2,
               clt.unit_cost_new,
               clt.cost_change_type,
               clt.cost_change_value,
               clt.default_bracket_ind,
               clt.recalc_ord_ind,
               clt.dept,
               clt.sup_dept_seq_no,
               clt.delivery_country_id);
   -- for stores
   merge into cost_susp_sup_detail_loc cl using (select distinct cost_change,
                                                        supplier,
                                                        origin_country_id,
                                                        item,
                                                        loc_type,
                                                        location,
                                                        bracket_value1,
                                                        bracket_uom,
                                                        bracket_value2,
                                                        unit_cost_new,
                                                        cost_change_type,
                                                        cost_change_value,
                                                        NVL(default_bracket_ind,'N')default_bracket_ind,
                                                        recalc_ord_ind,
                                                        dept,
                                                        sup_dept_seq_no,
                                                        delivery_country_id
                                                   from cost_change_loc_temp
                                                  where loc_type = 'S'
                                                    and unit_cost_new is NOT NULL
                                                    and cost_change       = I_cost_change) clt
     on (cl.cost_change       = clt.cost_change
         and cl.item              = clt.item
         and cl.supplier          = clt.supplier
         and cl.origin_country_id = clt.origin_country_id
         and cl.loc               = clt.location)
   when matched then
      update set cl.bracket_value1      = clt.bracket_value1,
                 cl.bracket_uom1        = clt.bracket_uom,
                 cl.bracket_value2      = clt.bracket_value2,
                 cl.unit_cost           = clt.unit_cost_new,
                 cl.cost_change_type    = clt.cost_change_type,
                 cl.cost_change_value   = clt.cost_change_value,
                 cl.default_bracket_ind = clt.default_bracket_ind,
                 cl.recalc_ord_ind      = clt.recalc_ord_ind
   when not matched then
      insert (cost_change,
              supplier,
              origin_country_id,
              item,
              loc_type,
              loc,
              bracket_value1,
              bracket_uom1,
              bracket_value2,
              unit_cost,
              cost_change_type,
              cost_change_value,
              default_bracket_ind,
              recalc_ord_ind,
              sup_dept_seq_no,
              dept,
              delivery_country_id)
       values (clt.cost_change,
               clt.supplier,
               clt.origin_country_id,
               clt.item,
               clt.loc_type,
               clt.location,
               clt.bracket_value1,
               clt.bracket_uom,
               clt.bracket_value2,
               clt.unit_cost_new,
               clt.cost_change_type,
               clt.cost_change_value,
               clt.default_bracket_ind,
               clt.recalc_ord_ind,
               clt.dept,
               clt.sup_dept_seq_no,
               clt.delivery_country_id);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'INSERT_UPDATE_COST_CHANGE',
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_UPDATE_COST_CHANGE;
--------------------------------------------------------------------------------
FUNCTION PROCESS_DELETES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_COST_SUSP_SUP_DETAIL_LOC.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(255) := 'CORESVC_COSTCHG.PROCESS_DELETES';
   L_error                   BOOLEAN;
   L_cost_change_loc         SVC_COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE;
   L_cost_change_det         SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE;
   L_item                    SVC_COST_SUSP_SUP_DETAIL_LOC.ITEM%TYPE;
   L_supplier                SVC_COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE;
   L_origin_country_id       SVC_COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE;
   L_loc                     SVC_COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE;
   L_loc_type                SVC_COST_SUSP_SUP_DETAIL_LOC.LOC_TYPE%TYPE;
   L_pwh                     WH.PHYSICAL_WH%TYPE;

   cursor C_CHECK_COST_DETAIL_LOC(I_cost_change   SVC_COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                                  I_item          SVC_COST_SUSP_SUP_DETAIL_LOC.ITEM%TYPE,
                                  I_supplier      SVC_COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE,
                                  I_ctry          SVC_COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE,
                                  I_loc           SVC_COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                                  I_loc_type      SVC_COST_SUSP_SUP_DETAIL_LOC.LOC_TYPE%TYPE) is
      select cl.cost_change,
             cl.item,
             cl.supplier,
             cl.origin_country_id,
             locs.physical_loc,
             cl.loc_type
        from cost_susp_sup_detail_loc cl,
             (select physical_wh physical_loc,
                     wh loc,
                     'W' loc_type
                from wh
               union all
              select store physical_loc,
                     store loc,
                     'S' loc_type
                from store) locs
       where locs.loc = cl.loc
         and locs.loc_type = cl.loc_type
         and cl.cost_change = I_cost_change
       minus
      select cost_change,
             item,
             supplier,
             origin_country_id,
             loc,
             loc_type
        from svc_cost_susp_sup_detail_loc
       where cost_change = I_cost_change
         and item = I_item
         and supplier = I_supplier
         and origin_country_id = I_ctry
         and loc = NVL(I_loc, loc)
         and loc_type = NVL(I_loc_type, loc_type)
         and process_id = I_process_id
         and chunk_id = I_chunk_id
         and process$status <> 'E'
         and action = 'DEL'
       order by cost_change;

   cursor C_CHECK_COST_DETAIL(I_cost_change   SVC_COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                              I_item          SVC_COST_SUSP_SUP_DETAIL_LOC.ITEM%TYPE,
                              I_supplier      SVC_COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE,
                              I_ctry          SVC_COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE) is
      select cost_change,
             item,
             supplier,
             origin_country_id
        from cost_susp_sup_detail
       where cost_change = I_cost_change
       minus
      select cost_change,
             item,
             supplier,
             origin_country_id
        from svc_cost_susp_sup_detail
       where cost_change = I_cost_change
         and item = I_item
         and supplier = I_supplier
         and origin_country_id = I_ctry
         and process_id = I_process_id
         and chunk_id = I_chunk_id
         and process$status <> 'E'
         and action = 'DEL'
       order by cost_change;


BEGIN
   -- ensure that there will be at least one record remaining in cost_susp_sup_detail and cost_susp_sup_detail_loc
   if CSDL_del_tab is NOT NULL and CSDL_del_tab.COUNT > 0 then
      FOR i in 1..CSDL_del_tab.COUNT LOOP
         open C_CHECK_COST_DETAIL_LOC(CSDL_del_tab(i).cost_change,
                                      CSDL_del_tab(i).item,
                                      CSDL_del_tab(i).supplier,
                                      CSDL_del_tab(i).origin_country_id,
                                      CSDL_del_tab(i).loc,
                                      CSDL_del_tab(i).loc_type);
         fetch C_CHECK_COST_DETAIL_LOC into L_cost_change_loc,
                                            L_item,
                                            L_supplier,
                                            L_origin_country_id,
                                            L_loc,
                                            L_loc_type;
         close C_CHECK_COST_DETAIL_LOC;

         if L_cost_change_loc is NULL then -- This leaves the CSSDL table empty.  Check if CSSD table will have one remaining record.
            open C_CHECK_COST_DETAIL(CSDL_del_tab(i).cost_change,
                                     CSDL_del_tab(i).item,
                                     CSDL_del_tab(i).supplier,
                                     CSDL_del_tab(i).origin_country_id);
            fetch C_CHECK_COST_DETAIL into L_cost_change_det,
                                           L_item,
                                           L_supplier,
                                           L_origin_country_id;
            close C_CHECK_COST_DETAIL;

            if L_cost_change_det is NULL then -- there won't be any cost change details left.  error out this record
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           'SVC_COST_SUSP_SUP_DETAIL_LOC',
                           CSDL_del_tab(i).row_seq,
                           NULL,
                           'REQ_COST_CHANGE_DETAIL',
                           CSDL_del_tab(i).item,
                           CSDL_del_tab(i).supplier,
                           CSDL_del_tab(i).origin_country_id);

               update svc_cost_susp_sup_detail_loc
                  set process$status    = 'E',
                      last_upd_id = LP_user,
                      last_upd_datetime = sysdate
                where process_id        = I_process_id
                  and chunk_id          = I_chunk_id
                  and cost_change       = CSDL_del_tab(i).cost_change
                  and item              = CSDL_del_tab(i).item
                  and supplier          = CSDL_del_tab(i).supplier
                  and origin_country_id = CSDL_del_tab(i).origin_country_id
                  and loc               = CSDL_del_tab(i).loc
                  and loc_type          = CSDL_del_tab(i).loc_type;
            else
               EXEC_CSDL_DEL(CSDL_del_tab(i));
            end if;
         else -- there will remain at least 1 record, execute the delete of this record
            EXEC_CSDL_DEL(CSDL_del_tab(i));
         end if;
      END LOOP;
   end if;

   if CSD_del_tab is NOT NULL and CSD_del_tab.COUNT > 0 then
      FOR i in 1..CSD_del_tab.COUNT LOOP
         open C_CHECK_COST_DETAIL(CSD_del_tab(i).cost_change,
                                  CSD_del_tab(i).item,
                                  CSD_del_tab(i).supplier,
                                  CSD_del_tab(i).origin_country_id);
         fetch C_CHECK_COST_DETAIL into L_cost_change_det,
                                        L_item,
                                        L_supplier,
                                        L_origin_country_id;
         close C_CHECK_COST_DETAIL;

         if L_cost_change_det is NULL then -- this leaves the CSSD table empty.  need to check if CSSDL table will have one remaining record.
            open C_CHECK_COST_DETAIL_LOC(CSD_del_tab(i).cost_change,
                                         CSD_del_tab(i).item,
                                         CSD_del_tab(i).supplier,
                                         CSD_del_tab(i).origin_country_id,
                                         NULL,
                                         NULL);
            fetch C_CHECK_COST_DETAIL_LOC into L_cost_change_loc,
                                               L_item,
                                               L_supplier,
                                               L_origin_country_id,
                                               L_loc,
                                               L_loc_type;
            close C_CHECK_COST_DETAIL_LOC;

            if L_cost_change_loc is NULL then -- There won't be any cost change details left.  Error out this record
               WRITE_ERROR(I_process_id,
                           CORESVC_COSTCHG_ESEQ.nextval,
                           I_chunk_id,
                           'SVC_COST_SUSP_SUP_DETAIL',
                           CSD_del_tab(i).row_seq,
                           NULL,
                           'REQ_COST_CHANGE_DETAIL',
                           CSD_del_tab(i).item,
                           CSD_del_tab(i).supplier,
                           CSD_del_tab(i).origin_country_id);

               update svc_cost_susp_sup_detail
                  set process$status    = 'E',
                      last_upd_id = LP_user,
                      last_upd_datetime = sysdate
                where process_id        = I_process_id
                  and chunk_id          = I_chunk_id
                  and cost_change       = CSD_del_tab(i).cost_change
                  and item              = CSD_del_tab(i).item
                  and supplier          = CSD_del_tab(i).supplier
                  and origin_country_id = CSD_del_tab(i).origin_country_id;
            else
               EXEC_CSD_DEL(CSD_del_tab(i));
            end if;
         else -- there will remain at least 1 record, execute the delete of this record
            EXEC_CSD_DEL(CSD_del_tab(i));
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DELETES;
--------------------------------------------------------------------------------
FUNCTION CHECK_COST_CONFLICTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_conflicts_exist IN OUT   BOOLEAN,
                              I_active_date     IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                              I_cost_change     IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE,
                              I_row_seq         IN       SVC_COST_SUSP_SUP_HEAD.ROW_SEQ%TYPE,
                              I_process_id      IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
return BOOLEAN IS

   L_program                    VARCHAR2(255) := 'CORESVC_COSTCHG.CHECK_COST_CONFLICTS';
   L_table                      VARCHAR2(30) := 'SVC_COST_SUSP_SUP_HEAD';
   L_error                      BOOLEAN;
   L_conflicts                  BOOLEAN;
   L_buyer_pack_conflicts       BOOLEAN;

BEGIN
  
   if COST_CHANGE_SQL.CHECK_COST_CONFLICTS(O_error_message,
                                           L_conflicts,
                                           I_cost_change,
                                           I_active_date) = FALSE then
      return FALSE;
   end if;
   
   if L_conflicts then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  NULL,
                  'COST_CONFLICT',
                  NULL,
                  NULL,
                  NULL,
                  'W');
      O_conflicts_exist := TRUE;
   end if;

   if COST_CHANGE_SQL.CHECK_BUYER_PACK_CONFLICTS(O_error_message,
                                                 L_buyer_pack_conflicts,
                                                 I_cost_change,
                                                 I_active_date) = FALSE then
      return FALSE;
   end if;
   
   if L_buyer_pack_conflicts then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  NULL,
                  'BUYER_PACK_CONFLICT',
                  NULL,
                  NULL,
                  NULL,
                  'W');
      O_conflicts_exist := TRUE;
   end if;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_COST_CONFLICTS;
--------------------------------------------------------------------------------
FUNCTION APPROVAL_CHECK(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_approve                IN OUT   BOOLEAN,
                        I_active_date            IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                        I_cost_change            IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                        I_row_seq                IN       SVC_COST_SUSP_SUP_HEAD.ROW_SEQ%TYPE,
                        I_process_id             IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                        I_chunk_id               IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
return BOOLEAN IS

   L_program                    VARCHAR2(255) := 'CORESVC_COSTCHG.APPROVAL_CHECK';
   L_table                      VARCHAR2(30) := 'SVC_COST_SUSP_SUP_HEAD';
   L_conflicts_exist            BOOLEAN;
   L_min_act_date               DATE;
   
BEGIN
   if I_active_date = LP_vdate or I_active_date < (LP_cost_prior_cre_days + LP_vdate) then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  I_active_date,
                  'CANNOT_APPROVE_EXP_CC',
                  NULL,
                  NULL,
                  NULL,
                  'W');
      O_approve := FALSE;
   end if;
   
   if CHECK_COST_CONFLICTS(O_error_message,
                           L_conflicts_exist,
                           I_active_date,
                           I_cost_change,
                           I_row_seq,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE; 
   end if;
   
   if L_conflicts_exist = TRUE then
      O_approve := FALSE;
   end if;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END APPROVAL_CHECK;
--------------------------------------------------------------------------------
FUNCTION SUBMIT_CHECK(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_submit                 IN OUT   BOOLEAN,
                      I_active_date            IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                      I_cost_change            IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                      I_row_seq                IN       SVC_COST_SUSP_SUP_HEAD.ROW_SEQ%TYPE,
                      I_process_id             IN       SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%TYPE,
                      I_chunk_id               IN       SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%TYPE)
return BOOLEAN IS

   L_program                    VARCHAR2(255) := 'CORESVC_COSTCHG.SUBMIT_CHECK';
   L_table                      VARCHAR2(30) := 'SVC_COST_SUSP_SUP_HEAD';
   L_conflicts_exist            BOOLEAN;

BEGIN
   O_submit := TRUE;
   if I_active_date < LP_vdate then
      WRITE_ERROR(I_process_id,
                  CORESVC_COSTCHG_ESEQ.nextval,
                  I_chunk_id,
                  L_table,
                  I_row_seq,
                  I_active_date,
                  'CANNOT_SUBMIT_CC',
                  NULL,
                  NULL,
                  NULL,
                  'W'); 
      O_submit := FALSE;
   end if;
   
   if CHECK_COST_CONFLICTS(O_error_message,
                           L_conflicts_exist,
                           I_active_date,
                           I_cost_change,
                           I_row_seq,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE; 
   end if;
   
   if L_conflicts_exist = TRUE then
      O_submit := FALSE;
   end if;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SUBMIT_CHECK;
--------------------------------------------------------------------------------
FUNCTION CALL_FC_ENGINE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_change     IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                        I_action_type     IN       VARCHAR2)
return BOOLEAN IS

   L_program                    VARCHAR2(255) := 'CORESVC_COSTCHG.CALL_FC_ENGINE';
   L_table                      VARCHAR2(30) := 'SVC_COST_SUSP_SUP_HEAD';
   L_cost_event_process_id      COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_cost_changes_rec           OBJ_CC_COST_EVENT_REC;
   L_cost_changes_tbl           OBJ_CC_COST_EVENT_TBL;
   
BEGIN
   if I_action_type = 'ADD_CC' then
      L_cost_changes_tbl := new OBJ_CC_COST_EVENT_TBL();
      L_cost_changes_rec := new OBJ_CC_COST_EVENT_REC(I_cost_change,
                                                      'N'); --Cost Change records will not be sourced from the temp tables.
      L_cost_changes_tbl.EXTEND;
      L_cost_changes_tbl(L_cost_changes_tbl.COUNT) := L_cost_changes_rec;
      if FUTURE_COST_EVENT_SQL.ADD_COST_CHANGE_EVENT(O_error_message,
                                                     L_cost_event_process_id,
                                                     'ADD',
                                                     L_cost_changes_tbl,
                                                     'Y',
                                                     LP_user) = FALSE then
         return FALSE;
      end if;
   elsif I_action_type = 'REM_CC' then
      L_cost_changes_tbl := new OBJ_CC_COST_EVENT_TBL();
      L_cost_changes_rec := new OBJ_CC_COST_EVENT_REC(I_cost_change,
                                                      'N'); --Cost Change records will not be sourced from the temp tables.
      L_cost_changes_tbl.EXTEND;
      L_cost_changes_tbl(L_cost_changes_tbl.COUNT) := L_cost_changes_rec;
      if FUTURE_COST_EVENT_SQL.ADD_COST_CHANGE_EVENT(O_error_message,
                                                     L_cost_event_process_id,
                                                     'REM',
                                                     L_cost_changes_tbl,
                                                     'Y',
                                                     LP_user) = FALSE then
         return FALSE;
      end if;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALL_FC_ENGINE;
--------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_VARIANCES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_approve         IN OUT   BOOLEAN,
                              I_cost_change     IN       SVC_COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE,
                              I_supplier        IN       SVC_COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                              I_item            IN       SVC_COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                              I_location        IN       SVC_COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                              I_country         IN       SVC_COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE,
                              I_item_level_ind  IN       VARCHAR2)
return BOOLEAN IS

   L_program                    VARCHAR2(255) := 'CORESVC_COSTCHG.CHECK_SUPP_VARIANCES';
   L_error                      BOOLEAN;
   L_cost_pct_var               SUPS.COST_CHG_PCT_VAR%TYPE;
   L_cost_amt_var               SUPS.COST_CHG_AMT_VAR%TYPE;
   L_exists_amt                 VARCHAR2(1);
   L_exists_pct                 VARCHAR2(1);
   
   cursor C_SUPS is
      select nvl(cost_chg_pct_var,0),
             nvl(cost_chg_amt_var,0)
        from sups
       where sups.supplier = I_supplier;
   ---
   cursor C_VAR_AMOUNT_ITEM is
      select 'X'
        from item_supp_country isc,
             cost_susp_sup_detail cssd
       where cssd.cost_change = I_cost_change
         and cssd.item = I_item
         and cssd.supplier = I_supplier
         and cssd.origin_country_id = I_country
         and cssd.unit_cost > (isc.unit_cost + L_cost_amt_var)
         and cssd.item = isc.item
         and cssd.supplier = isc.supplier
         and cssd.origin_country_id = isc.origin_country_id
   UNION ALL
      select 'X'
        from item_supp_country isc,
             cost_susp_sup_detail cssd
       where cssd.cost_change = I_cost_change
         and cssd.item = I_item
         and cssd.supplier = I_supplier
         and cssd.origin_country_id = I_country
         and cssd.unit_cost < (isc.unit_cost - L_cost_amt_var)
         and cssd.item = isc.item
         and cssd.supplier = isc.supplier
         and cssd.origin_country_id = isc.origin_country_id;
   ---
   cursor C_VAR_PCT_ITEM is
      select 'X'
        from item_supp_country isc,
             cost_susp_sup_detail cssd
       where cssd.cost_change = I_cost_change
         and cssd.item = I_item
         and cssd.supplier = I_supplier
         and cssd.origin_country_id = I_country
         and cssd.unit_cost > (isc.unit_cost + (isc.unit_cost * (L_cost_pct_var/100)))
         and cssd.item = isc.item
         and cssd.supplier = isc.supplier
         and cssd.origin_country_id = isc.origin_country_id
   UNION ALL
      select 'X'
        from item_supp_country isc,
             cost_susp_sup_detail cssd
       where cssd.cost_change = I_cost_change
         and cssd.item = I_item
         and cssd.supplier = I_supplier
         and cssd.origin_country_id = I_country
         and cssd.unit_cost < (isc.unit_cost - (isc.unit_cost * (L_cost_pct_var/100)))
         and cssd.item = isc.item
         and cssd.supplier = isc.supplier
         and cssd.origin_country_id = isc.origin_country_id;
   ---
   cursor C_VAR_AMOUNT is
      select 'X'
        from item_supp_country_loc iscl,
             cost_susp_sup_detail_loc cssdl
       where cssdl.cost_change = I_cost_change
         and cssdl.item = I_item
         and cssdl.supplier = I_supplier
         and cssdl.loc = I_location
         and cssdl.origin_country_id = I_country
         and cssdl.unit_cost > (iscl.unit_cost + L_cost_amt_var)
         and cssdl.item = iscl.item
         and cssdl.supplier = iscl.supplier
         and cssdl.origin_country_id = iscl.origin_country_id
         and cssdl.loc = iscl.loc
   UNION ALL
      select 'X'
        from item_supp_country_loc iscl,
             cost_susp_sup_detail_loc cssdl
       where cssdl.cost_change = I_cost_change
         and cssdl.item = I_item
         and cssdl.supplier = I_supplier
         and cssdl.loc = I_location
         and cssdl.origin_country_id = I_country
         and cssdl.unit_cost < (iscl.unit_cost - L_cost_amt_var)
         and cssdl.item = iscl.item
         and cssdl.supplier = iscl.supplier
         and cssdl.origin_country_id = iscl.origin_country_id
         and cssdl.loc = iscl.loc;
   ---
   cursor C_VAR_PCT is
      select 'X'
        from item_supp_country_loc iscl,
             cost_susp_sup_detail_loc cssdl
       where cssdl.cost_change = I_cost_change
         and cssdl.item = I_item
         and cssdl.supplier = I_supplier
         and cssdl.loc = I_location
         and cssdl.origin_country_id = I_country
         and cssdl.unit_cost > (iscl.unit_cost + (iscl.unit_cost * (L_cost_pct_var/100)))
         and cssdl.item = iscl.item
         and cssdl.supplier = iscl.supplier
         and cssdl.origin_country_id = iscl.origin_country_id
         and cssdl.loc = iscl.loc
   UNION ALL
      select 'X'
        from item_supp_country_loc iscl,
             cost_susp_sup_detail_loc cssdl
       where cssdl.cost_change = I_cost_change
         and cssdl.item = I_item
         and cssdl.supplier = I_supplier
         and cssdl.loc = I_location
         and cssdl.origin_country_id = I_country
         and cssdl.unit_cost < (iscl.unit_cost - (iscl.unit_cost * (L_cost_pct_var/100)))
         and cssdl.item = iscl.item
         and cssdl.supplier = iscl.supplier
         and cssdl.origin_country_id = iscl.origin_country_id
         and cssdl.loc = iscl.loc;
   ---

BEGIN
   --- Verify that input variables are correctly populated
   if I_cost_change is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_cost_change', 
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
 
   if I_item_level_ind is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_item_level_ind', 
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_supplier', 
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   
   if I_item is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_item', 
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   
   if I_item_level_ind = 'N' and I_location is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_location', 
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;

   --- Set the approval flag equal to False.  This function
   --- will set this flag to TRUE if any of the checks pass.
   O_approve := FALSE;

   --- retrieve the supplier costing variances
   open C_SUPS;
   fetch C_SUPS into L_cost_pct_var,
                     L_cost_amt_var;

   --- Check supplier tolerances for an EDI cost change at the item level.
   if I_item_level_ind = 'Y' then

      --- The new cost must fall within both percent and dollar variance
      --- of the old cost if 'B'oth is passed in.
      if LP_var_comp_basis = 'B' then
         open C_VAR_AMOUNT_ITEM;
         fetch C_VAR_AMOUNT_ITEM into L_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then check to see if the cost is outside of the percent variance
         if C_VAR_AMOUNT_ITEM%NOTFOUND then
            open C_VAR_PCT_ITEM;
            fetch C_VAR_PCT_ITEM into L_exists_pct;

            --- If the cost is still not outside of the variance then
            --- set O_approve equal to TRUE
            if C_VAR_PCT_ITEM%NOTFOUND then
               O_approve := TRUE;
            end if;

            close C_VAR_PCT_ITEM;
         end if;
         close C_VAR_AMOUNT_ITEM;

      end if;  --- end if  LP_var_comp_basis = 'B' 

      --- The new cost must fall within either percent or dollar variance
      --- of the old cost if 'E'ither is passed in.
      if LP_var_comp_basis = 'E' then
         open C_VAR_AMOUNT_ITEM;
         fetch C_VAR_AMOUNT_ITEM into L_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then update O_approve to TRUE
         if C_VAR_AMOUNT_ITEM%NOTFOUND then
            O_approve := TRUE;
         
         --- If the amount is outside of the supplier dollar variance then
         --- check to see if it is outside the percent variance
         else
            open C_VAR_PCT_ITEM;
            fetch C_VAR_PCT_ITEM into L_exists_pct;

            --- If the cost is not outside of the variance then
            --- set O_approve equal to TRUE
            if C_VAR_PCT_ITEM%NOTFOUND then
               O_approve := TRUE;
            end if;

            close C_VAR_PCT_ITEM;
         end if;

         close C_VAR_AMOUNT_ITEM;
      end if;  --- end if LP_var_comp_basis = 'E' 

      --- The new cost must fall within dollar variance of the old cost 
      --- if 'D'ollar is passed in.
      if LP_var_comp_basis = 'D' then
            
         open C_VAR_AMOUNT_ITEM;
         fetch C_VAR_AMOUNT_ITEM into L_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then update O_approve to TRUE
         if C_VAR_AMOUNT_ITEM%NOTFOUND then
            O_approve := TRUE;
         end if;

         close C_VAR_AMOUNT_ITEM;
      end if;  --- end if LP_var_comp_basis = 'D' 

      --- The new cost must fall within percent variance of the old cost
      --- if 'P'ercent is passed in.
      if LP_var_comp_basis = 'P' then
         
         open C_VAR_PCT_ITEM;
         fetch C_VAR_PCT_ITEM into L_exists_pct;

         --- If the cost is not outside of the variance then
         --- set O_approve equal to TRUE
         if C_VAR_PCT_ITEM%NOTFOUND then
            O_approve := TRUE;
         end if;

         close C_VAR_PCT_ITEM;    
      end if;  --- end if LP_var_comp_basis = 'P' 
       
   else --- If the cost change is at the item/bracket, item/location, or item/location/bracket
   
      -- Find if the costs for the bracket or location are out of the tolerance limits.
      -- If they are, set O_approve to False.  If no records are found out of tolerance ranges, then
      -- set O_approve True.

      O_approve := FALSE;

      --- The new cost must fall within both percent and dollar variance
      --- of the old cost if 'B'oth is passed in.
      if LP_var_comp_basis = 'B' then
         open C_VAR_AMOUNT;
         fetch C_VAR_AMOUNT into L_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then check to see if the cost is outside of the percent variance
         if C_VAR_AMOUNT%NOTFOUND then
            open C_VAR_PCT;
            fetch C_VAR_PCT into L_exists_pct;

            --- If the cost is still not outside of the variance then
            --- set O_approve equal to TRUE
            if C_VAR_PCT%NOTFOUND then
               O_approve := TRUE;
            end if;

            close C_VAR_PCT;
         end if;
         close C_VAR_AMOUNT;

      end if;  --- end if  LP_var_comp_basis = 'B' 

      --- The new cost must fall within either percent or dollar variance
      --- of the old cost if 'E'ither is passed in.
      if LP_var_comp_basis = 'E' then
         open C_VAR_AMOUNT;
         fetch C_VAR_AMOUNT into L_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then update O_approve to TRUE
         if C_VAR_AMOUNT%NOTFOUND then
            O_approve := TRUE;
         
         --- If the amount is outside of the supplier dollar variance then
         --- check to see if it is outside the percent variance
         else
            open C_VAR_PCT;
            fetch C_VAR_PCT into L_exists_pct;

            --- If the cost is not outside of the variance then
            --- set O_approve equal to TRUE
            if C_VAR_PCT%NOTFOUND then
               O_approve := TRUE;
            end if;

            close C_VAR_PCT;
         end if;

         close C_VAR_AMOUNT;
      end if;  --- end if LP_var_comp_basis = 'E' 

      --- The new cost must fall within dollar variance of the old cost 
      --- if 'D'ollar is passed in.
      if LP_var_comp_basis = 'D' then
            
         open C_VAR_AMOUNT;
         fetch C_VAR_AMOUNT into L_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then update O_approve to TRUE
         if C_VAR_AMOUNT%NOTFOUND then
            O_approve := TRUE;
         end if;

         close C_VAR_AMOUNT;
      end if;  --- end if LP_var_comp_basis = 'D' 

      --- The new cost must fall within percent variance of the old cost
      --- if 'P'ercent is passed in.
      if LP_var_comp_basis = 'P' then
         
         open C_VAR_PCT;
         fetch C_VAR_PCT into L_exists_pct;

         --- If the cost is not outside of the variance then
         --- set O_approve equal to TRUE
         if C_VAR_PCT%NOTFOUND then
            O_approve := TRUE;
         end if;

         close C_VAR_PCT;    
      end if;  --- end if LP_var_comp_basis = 'P' 

   end if;  

   close C_SUPS;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_SUPP_VARIANCES;
--------------------------------------------------------------------------------
FUNCTION SUBMIT_APPROVE_COST_CHANGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_process_id      IN       SVC_COST_SUSP_SUP_DETAIL_LOC.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(255) := 'CORESVC_COSTCHG.SUBMIT_APPROVE_COST_CHANGE';
   L_approve                 BOOLEAN;
   L_submit                  BOOLEAN;
   L_approve_cost_change     BOOLEAN := NULL;
   L_key_value_1             COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked, -54);
   
   cursor C_GET_CC_FOR_APPROVAL (p_cost_change IN COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)is
      select cssh.cost_change,
             sch.active_date,
             'Y' as item_level_ind,
             cssd.supplier,
             cssd.item,
             cssd.origin_country_id,
             NULL as location,
             sch.row_seq,
             sch.chunk_id
        from svc_cost_susp_sup_head sch,
             cost_susp_sup_head cssh,
             cost_susp_sup_detail cssd
       where sch.cost_change = cssh.cost_change
         and sch.action in (ACTION_NEW, ACTION_MOD)
         and sch.process$status = 'P'
         and cssh.status <> 'A'
         and sch.status = 'A'
         and sch.cost_change = cssd.cost_change
         and sch.process_id = I_process_id
         and sch.cost_change = p_cost_change
   union all
      select cssh.cost_change,
             sch.active_date,
             'N' as item_level_ind,
             cssdl.supplier,
             cssdl.item,
             cssdl.origin_country_id,
             cssdl.loc,
             sch.row_seq,
             sch.chunk_id
        from svc_cost_susp_sup_head sch,
             cost_susp_sup_head cssh,
             cost_susp_sup_detail_loc cssdl
       where sch.cost_change = cssh.cost_change
         and sch.action in (ACTION_NEW, ACTION_MOD)
         and sch.process$status = 'P'
         and cssh.status <> 'A'
         and sch.status = 'A'
         and sch.cost_change = cssdl.cost_change
         and sch.process_id = I_process_id
         and sch.cost_change = p_cost_change         
       order by 1;
       
   cursor C_GET_CC_FOR_SUBMISSION is
      select sch.cost_change,
             sch.row_seq,
             sch.chunk_id,
             sch.active_date
        from svc_cost_susp_sup_head sch,
             cost_susp_sup_head cssh
       where sch.cost_change = cssh.cost_change
         and sch.process$status = 'P'
         and sch.action in (ACTION_NEW, ACTION_MOD)
         and cssh.status = 'W'
         and sch.status = 'S'
         and sch.process_id = I_process_id
         and not exists (select '1'
                           from coresvc_costchg_err err
                          where err.process_id = I_process_id
                            and err.error_type = 'E');
                            
    cursor C_GET_CC_FOR_APPROVAL_HEAD is
      select cssh.cost_change,
             sch.active_date,
             sch.row_seq,
             sch.chunk_id
        from svc_cost_susp_sup_head sch,
             cost_susp_sup_head cssh
       where sch.cost_change = cssh.cost_change
         and sch.action in (ACTION_NEW, ACTION_MOD)
         and sch.process$status = 'P'
         and cssh.status <> 'A'
         and sch.status = 'A'
         and sch.process_id = I_process_id
         and not exists (select '1'
                           from coresvc_costchg_err err
                          where err.process_id = I_process_id
                            and err.error_type = 'E')
       order by 1;                              
       
   cursor C_LOCK_COST_SUSP_SUP_HEAD(I_cost_change   COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE) is
      select 'X'
        from cost_susp_sup_head
       where cost_change = I_cost_change;
         
   TYPE CC_TAB IS TABLE OF C_GET_CC_FOR_APPROVAL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_cost_change_tab   CC_TAB;
   
   TYPE CC_SUB_TAB IS TABLE OF C_GET_CC_FOR_SUBMISSION%ROWTYPE INDEX BY BINARY_INTEGER;
   L_cost_change_submit_tab   CC_SUB_TAB;
   
   TYPE CC_TAB_HEAD IS TABLE OF C_GET_CC_FOR_APPROVAL_HEAD%ROWTYPE INDEX BY BINARY_INTEGER;
   L_cost_change_tab_head   CC_TAB_HEAD;
         
BEGIN
   open C_GET_CC_FOR_SUBMISSION;
   fetch C_GET_CC_FOR_SUBMISSION bulk collect into L_cost_change_submit_tab;
   close C_GET_CC_FOR_SUBMISSION;
   if L_cost_change_submit_tab is NOT NULL and L_cost_change_submit_tab.COUNT > 0 then
      FOR i in L_cost_change_submit_tab.FIRST..L_cost_change_submit_tab.LAST LOOP
         if SUBMIT_CHECK(O_error_message,
                         L_submit,
                         L_cost_change_submit_tab(i).active_date,
                         L_cost_change_submit_tab(i).cost_change,
                         L_cost_change_submit_tab(i).row_seq,
                         I_process_id,
                         L_cost_change_submit_tab(i).chunk_id) = FALSE then
            return FALSE;
         end if;
          if L_submit = TRUE then
            if COST_CHANGE_SQL.CUSTOM_VAL( O_error_message,
                                           L_cost_change_submit_tab(i).cost_change,
                                           NULL,
                                           NULL,
                                           NULL,
                                           L_cost_change_submit_tab(i).active_date,
                                           'COST_CHANGE_SUBMIT_UI',
                                           1 ) = FALSE then
                if O_error_message like '%PACKAGE_ERROR%' then
                   return FALSE;
                else
                    WRITE_ERROR(I_process_id,
                                CORESVC_COSTCHG_ESEQ.nextval,
                                L_cost_change_submit_tab(i).chunk_id,
                                'SVC_COST_SUSP_SUP_HEAD',
                                L_cost_change_submit_tab(i).row_seq,
                                L_cost_change_submit_tab(i).active_date,
                                O_error_message,
                                 NULL,
                                 NULL,
                                 NULL,
                                 'W');
                     L_submit := FALSE;
                 end if;
            end if;
        end if;
         if L_submit then
            
            open C_LOCK_COST_SUSP_SUP_HEAD(L_cost_change_submit_tab(i).cost_change);
            close C_LOCK_COST_SUSP_SUP_HEAD;
            update cost_susp_sup_head
               set status = 'S'
             where cost_change = L_cost_change_submit_tab(i).cost_change;
         end if;
      END LOOP;
   end if;
   
   open C_GET_CC_FOR_APPROVAL_HEAD;
   fetch C_GET_CC_FOR_APPROVAL_HEAD bulk collect into L_cost_change_tab_head;
   close C_GET_CC_FOR_APPROVAL_HEAD;
   
   if L_cost_change_tab_head is NOT NULL and L_cost_change_tab_head.COUNT > 0 then
      FOR i in L_cost_change_tab_head.FIRST..L_cost_change_tab_head.LAST LOOP
        L_approve_cost_change := TRUE;
        
         open C_GET_CC_FOR_APPROVAL (L_cost_change_tab_head(i).cost_change);
         fetch C_GET_CC_FOR_APPROVAL bulk collect into L_cost_change_tab;
         close C_GET_CC_FOR_APPROVAL; 
         if L_cost_change_tab is NOT NULL and L_cost_change_tab.COUNT > 0 then
            FOR i in L_cost_change_tab.FIRST..L_cost_change_tab.LAST LOOP  
              if CHECK_SUPP_VARIANCES(O_error_message,
                                 L_approve,
                                 L_cost_change_tab(i).cost_change,
                                 L_cost_change_tab(i).supplier,
                                 L_cost_change_tab(i).item,
                                 L_cost_change_tab(i).location,
                                 L_cost_change_tab(i).origin_country_id,
                                 L_cost_change_tab(i).item_level_ind) = FALSE then
                return FALSE;
              end if;
         
               if L_approve = FALSE then
                  L_approve_cost_change := FALSE;                 
                  WRITE_ERROR(I_process_id,
                        CORESVC_COSTCHG_ESEQ.nextval,
                        L_cost_change_tab(i).chunk_id,
                        'SVC_COST_SUSP_SUP_HEAD',
                        L_cost_change_tab(i).row_seq,
                        NULL,
                        'FAILED_SUPP_VAR_CHECK',
                        L_cost_change_tab(i).item,
                        L_cost_change_tab(i).supplier,
                        L_cost_change_tab(i).origin_country_id,
                        'W');
               end if;               
            END LOOP;             
         end if; 
        if L_approve_cost_change = TRUE then        
           if APPROVAL_CHECK(O_error_message,
                             L_approve,
                             L_cost_change_tab_head(i).active_date,
                             L_cost_change_tab_head(i).cost_change,
                             L_cost_change_tab_head(i).row_seq,
                             I_process_id,
                             L_cost_change_tab_head(i).chunk_id) = FALSE then
               return FALSE;
           end if;

           if L_approve = FALSE then
              L_approve_cost_change := FALSE;
              continue;
           end if;
        end if;   
        if L_approve_cost_change = TRUE then
            if COST_CHANGE_SQL.CUSTOM_VAL( O_error_message,
                                           L_cost_change_tab_head(i).cost_change,
                                           NULL,
                                           NULL,
                                           NULL,
                                           L_cost_change_tab_head(i).active_date,
                                           'COST_CHANGE_APPROVE_UI',
                                           1 ) = FALSE then
                if O_error_message like '%PACKAGE_ERROR%' then
                   return FALSE;
                else
                    WRITE_ERROR(I_process_id,
                                CORESVC_COSTCHG_ESEQ.nextval,
                                L_cost_change_tab_head(i).chunk_id,
                                'SVC_COST_SUSP_SUP_HEAD',
                                L_cost_change_tab_head(i).row_seq,
                                L_cost_change_tab_head(i).active_date,
                                O_error_message,
                                 NULL,
                                 NULL,
                                 NULL,
                                 'W');
                     L_approve_cost_change := FALSE;
                end if;
            end if;
        end if;
         if L_approve_cost_change = TRUE then
            
            L_key_value_1 := L_cost_change_tab_head(i).cost_change;
            open C_LOCK_COST_SUSP_SUP_HEAD(L_cost_change_tab_head(i).cost_change);
            close C_LOCK_COST_SUSP_SUP_HEAD;
               
            update cost_susp_sup_head
               set status = 'A',
                   approval_date = sysdate,
                   approval_id = LP_user
             where cost_change = L_cost_change_tab_head(i).cost_change;
             
            if CALL_FC_ENGINE(O_error_message,
                              L_cost_change_tab_head(i).cost_change,
                              'ADD_CC') = FALSE then
               return FALSE;
            end if; 
          end if;
      END LOOP;
   end if;



   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_COST_SUSP_SUP_HEAD%ISOPEN then
         close C_LOCK_COST_SUSP_SUP_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'COST_SUSP_SUP_HEAD',
                                             L_key_value_1,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_COST_SUSP_SUP_HEAD%ISOPEN then
         close C_LOCK_COST_SUSP_SUP_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SUBMIT_APPROVE_COST_CHANGE;

--------------------------------------------------------------------------------
FUNCTION COMPLETE_SVC_CSH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN       NUMBER)
RETURN BOOLEAN IS

   L_program       VARCHAR2(75) := 'CORESVC_COSTCHG.COMPLETE_SVC_CSH';
   L_max_row_seq   SVC_COST_SUSP_SUP_HEAD.ROW_SEQ%TYPE  := 0;

   cursor C_GET_MAX_ROW_SEQ is
      select MAX(row_seq)
        from svc_cost_susp_sup_head
       where process_id = I_process_id;

BEGIN
   open C_GET_MAX_ROW_SEQ;
   fetch C_GET_MAX_ROW_SEQ
    into L_max_row_seq;
   close C_GET_MAX_ROW_SEQ;

  L_max_row_seq := NVL(L_max_row_seq, 0);

  merge into svc_cost_susp_sup_head sch using
    (with cost_changes as(select cost_change
                            from svc_cost_susp_sup_detail
                           where process_id = I_process_id
                           union
                          select cost_change
                            from svc_cost_susp_sup_detail_loc
                           where process_id = I_process_id
                           union
                          select to_number(key_val_pairs_pkg.get_attr(keys_col,'COST_CHANGE'))
                            from svc_cfa_ext
                           where process_id = I_process_id)
   select cc.cost_change,
          NVL(csh.cost_change_desc, sch.cost_change_desc)     as cost_change_desc,
          NVL(csh.reason, sch.reason)                         as reason,
          NVL(csh.active_date, sch.active_date)               as active_date,
          NVL(csh.status, sch.status)                         as status,
          NVL(csh.cost_change_origin, sch.cost_change_origin) as cost_change_origin,
          rownum                                              as rel_row_seq
     from cost_changes cc,
          svc_cost_susp_sup_head sch,
          cost_susp_sup_head csh
    where cc.cost_change = sch.cost_change (+)
      and cc.cost_change = csh.cost_change (+)) sq ON (sch.cost_change = sq.cost_change)
   when matched then
      update
         set row_seq = case
                          when process_id = I_process_id
                          then row_seq
                          else L_max_row_seq + sq.rel_row_seq
                       end,
             process_id = I_process_id
   when NOT matched then
      insert (process_id,
              chunk_id,
              row_seq,
              action,
              process$status,
              cost_change,
              cost_change_desc,
              reason,
              active_date,
              status,
              cost_change_origin,
              create_id,
              create_datetime,
              last_upd_id,
              last_upd_datetime)
      values (I_process_id,
              1,
              L_max_row_seq + sq.rel_row_seq,
              NULL,
              'P',
              sq.cost_change,
              sq.cost_change_desc,
              sq.reason,
              sq.active_date,
              sq.status,
              sq.cost_change_origin,
              LP_user,
              sysdate,
              LP_user,
              sysdate);
   commit;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
      return FALSE;
END COMPLETE_SVC_CSH;
--------------------------------------------------------------------------------
END CORESVC_COSTCHG;
/