CREATE OR REPLACE PACKAGE FOUNDATION_SQL AUTHID CURRENT_USER IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : poscoup (Form Module)  
  -- Source Object            : PROCEDURE  -> P_GET_COUPON_DETAIL
  ---------------------------------------------------------------------------------------------
FUNCTION GET_COUPON_DETAIL(O_error_message             IN OUT   VARCHAR2,
						   G_PRIM_LANG                 IN       VARCHAR2 ,
						   G_USER_LANG                 IN       VARCHAR2,
						   I_TI_CLASS                  IN OUT   NUMBER,
						   I_TI_CLASS_NAME             IN OUT   VARCHAR2,
						   I_TI_COUPON_DESC            IN OUT   VARCHAR2,
						   I_TI_COUPON_ID              IN       NUMBER,
						   I_TI_DEPT                   IN OUT   NUMBER,
						   I_TI_DEPT_NAME              IN OUT   VARCHAR2,
						   I_TI_EFFECTIVE_DATE_FROM    IN OUT   DATE,
						   I_TI_EXPIRATION_DATE_FROM   IN OUT   DATE,
						   I_TI_GROUP                  IN OUT   NUMBER,
						   I_TI_GROUP_NAME             IN OUT   VARCHAR2,
						   I_TI_ITEM                   IN OUT   VARCHAR2,
						   I_TI_ITEM_NAME              IN OUT   VARCHAR2,
						   I_TI_MODIFY_ID              IN OUT   VARCHAR2,
						   I_TI_SUBCLASS               IN OUT   NUMBER,
						   I_TI_SUBCLASS_NAME          IN OUT   VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : poscoup (Form Module) 
  -- Source Object            : PROCEDURE  -> P_RECORD_LOCKING
  ---------------------------------------------------------------------------------------------
FUNCTION RECORD_LOCKING_POSCOUP(O_error_message   IN OUT   VARCHAR2,
                                I_COUPON_ID01     IN       NUMBER,
                                I_COUPON_ID       IN       NUMBER)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : poscoup (Form Module) 
  -- Source Object            : PROCEDURE  -> P_GET_TOTAL_ITEMS
  ---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_ITEMS_POSCOUP(O_error_message   IN OUT   VARCHAR2,
                                 I_COUPON_ID       IN       NUMBER,
                                 I_TI_TOTAL_ITEMS  IN OUT   NUMBER)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : poscoup (Form Module) 
  -- Source Object            : PROCEDURE  -> P_GET_TOTAL_STORES
  ---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_STORES(O_error_message     IN OUT   VARCHAR2,
                          I_COUPON_ID         IN       NUMBER,
                          I_TI_TOTAL_STORES   IN OUT   NUMBER)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : posmcrit (Form Module) 
  -- Source Object            : PROCEDURE  -> P_GET_ITEM_DETAIL
  ---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DETAIL(O_error_message         IN OUT   VARCHAR2,
                         I_TI_ITEM01             IN       VARCHAR2,
						 I_TI_ITEM_DESC01        IN       VARCHAR2,
						 I_TI_CLASS              IN OUT   NUMBER,
						 I_TI_CLASS_DESC         IN OUT   VARCHAR2,
						 I_TI_DEPT               IN OUT   NUMBER,
						 I_TI_DEPT_DESC          IN OUT   VARCHAR2,
						 I_TI_GROUP              IN OUT   NUMBER,
						 I_TI_GROUP_DESC         IN OUT   VARCHAR2,
						 I_TI_ITEM               IN       VARCHAR2,
						 I_TI_ITEM_DESC          IN       VARCHAR2,
						 I_TI_SUBCLASS           IN OUT   NUMBER,
						 I_TI_SUBCLASS_DESC      IN OUT   VARCHAR2,
						 P_IntrnlVrblsGpClss     IN OUT   NUMBER,
						 P_IntrnlVrblsGpDptVle   IN OUT   NUMBER,
						 SYSTEM_CURRENT_BLOCK    IN       VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : posmcrit (Form Module) 
  -- Source Object            : PROCEDURE  -> P_RECORD_LOCKING
  ---------------------------------------------------------------------------------------------
FUNCTION RECORD_LOCKING_POSMCRIT(O_error_message        IN OUT   VARCHAR2,
                                 P_PM_POS_CONFIG_ID     IN       NUMBER,
                                 P_PM_POS_CONFIG_TYPE   IN       VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : pospres (Form Module) 
  -- Source Object            : FUNCTION   -> F_CHECK_DUPLICATES
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATES(O_error_message         IN OUT   VARCHAR2,
                          O_ret                      OUT   VARCHAR2,
						  I_LI_AFTER_TIME         IN       VARCHAR2,
						  I_LI_BEFORE_TIME        IN       VARCHAR2,
						  I_TI_DATE               IN       DATE,
						  I_TI_POS_PROD_REST_ID   IN       NUMBER,
						  DAY_NO                           VARCHAR2)
   return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : pospres (Form Module) 
  -- Source Object            : PROCEDURE  -> P_GET_TOTAL_ITEMS
  ---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_ITEMS_POSPRES(O_error_message      IN OUT   VARCHAR2,
								 I_POS_PROD_REST_ID   IN       NUMBER,
								 I_TI_TOTAL_ITEMS     IN OUT   NUMBER)
   return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : pospres (Form Module) 
  -- Source Object            : PROCEDURE  -> P_GET_TOTAL_SITES
  ---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_SITES(O_error_message      IN OUT   VARCHAR2,
						 I_POS_PROD_REST_ID   IN       NUMBER,
						 I_TI_TOTAL_STORES    IN OUT   NUMBER)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : posstore (Form Module) 
  -- Source Object            : PROCEDURE  -> P_COUNT_STORES
  ---------------------------------------------------------------------------------------------
FUNCTION COUNT_STORES(O_error_message           IN OUT   VARCHAR2,
					  I_CB_PARTIAL_VIEW         IN OUT   VARCHAR2,
					  I_TI_TOTAL_STORES         IN OUT   NUMBER,
					  P_PM_POS_CONFIG_ID        IN       NUMBER,
					  P_PM_POS_CONFIG_TYPE      IN       VARCHAR2,
					  P_IntrnlVrblsGpFrmStrtp   IN OUT   VARCHAR2,
					  P_IntrnlVrblsGpStreCnt    IN OUT   NUMBER)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : rolepriv (Form Module) 
  -- Source Object            : PROCEDURE  -> FORM_VALIDATION.ROLE
  ---------------------------------------------------------------------------------------------
FUNCTION ROLE(O_error_message   IN OUT   VARCHAR2,
              I_ROLE            IN       VARCHAR2)
   return BOOLEAN;
END FOUNDATION_SQL;
/