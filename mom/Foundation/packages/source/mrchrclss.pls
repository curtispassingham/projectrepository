
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE MERCH_RECLASS_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------
   -- Function    : INSERT_RECLASS
   -- Purpose     : Takes in a PEND_MERCH_HIER table record and inserts all of
   --               it's contents into the PEND_MERCH_HIER table.
---------------------------------------------------------------------
FUNCTION INSERT_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : UPDATE_RECLASS
   -- Purpose     : Updates the PEND_MERCH_HIER table with the values from the
   --               PEND_MERCH_HIER table record
---------------------------------------------------------------------
FUNCTION UPDATE_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_RECLASS
   -- Purpose     : Deletes a particular PEND_MERCH_HIER record from the PEND_MERCH_HIER table
---------------------------------------------------------------------
FUNCTION DELETE_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   -- Function Name: DETERMINE_HIERARCHY
   -- Purpose      : This function will help determine the correct merch hierarhcy for query,
   --                update and delete records on PEND_MERCH_HIER. 
   --                It will return a null parent for hier types other than class and subclass,
   --                and will return a null grandparent for hier types other than subclass.
-------------------------------------------------------------------------------------------------------
FUNCTION DETERMINE_HIERARCHY(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_hier_id               IN OUT   PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                             O_hier_parent_id        IN OUT   PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                             O_hier_grandparent_id   IN OUT   PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                             I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                             I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                             I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                             I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END MERCH_RECLASS_SQL;
/
