



CREATE OR REPLACE PACKAGE UOM_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------
FUNCTION SUPPLIER_UOM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_lwh_uom        IN OUT ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                      O_weight_uom     IN OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                      O_liquid_uom     IN OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  WITHIN_CLASS
--Purpose      :  Converts UOMs into others within the same class.  When used
--                as a public function, it will not accept item and supplier as
--                input parameters; this will cause the Miscellaneous portion of
--                the conversion logic to fail (it functions normally when
--                called from within the UOM_SQL package).
-------------------------------------------------------------------------------
FUNCTION WITHIN_CLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_to_value      IN OUT NUMBER,
                      I_to_uom        IN     UOM_CLASS.UOM%TYPE,
                      I_from_value    IN     NUMBER,
                      I_from_uom      IN     UOM_CLASS.UOM%TYPE,
                      I_uom_class     IN     UOM_CLASS.UOM_CLASS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  CONVERT
--Purpose      :  Evaluate whether the classes are the same or different, and
--                then it will call the corresponding internal function to
--                convert between the given unit of measures.  If I_item is
--                inputted with I_supplier, I_origin_country NULL, the function
--                will find the origin country for the item's primary supplier
--                and base conversion on the associated measurements.  However,
--                supplier and origin country should be inputted if possible.
-------------------------------------------------------------------------------
FUNCTION CONVERT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_to_value           IN OUT   NUMBER,
                 I_to_uom             IN       UOM_CONVERSION.TO_UOM%TYPE,
                 I_from_value         IN       NUMBER,
                 I_from_uom           IN       UOM_CONVERSION.FROM_UOM%TYPE,
                 I_item               IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                 I_supplier           IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                 I_origin_country     IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                 I_calling_function   IN       VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  UOM_EXISTS
--Purpose      :  Check if the uom passed in is a valid unit of measure or a
--                valid uom within a class if the class is passed in.
-------------------------------------------------------------------------------
FUNCTION UOM_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists        IN OUT BOOLEAN,
                    I_uom           IN     UOM_CLASS.UOM%TYPE,
                    I_uom_class     IN     UOM_CLASS.UOM_CLASS%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  GET_DESC
--Purpose      :  Get the description relating to a given unit of measure.
-------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_uom_desc      IN OUT UOM_CLASS_TL.UOM_DESC_TRANS%TYPE,
                  I_uom           IN     UOM_CLASS.UOM%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  GET_CLASS
--Purpose      :  Get the class relating to a given unit of measure.
-------------------------------------------------------------------------------
FUNCTION GET_CLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_uom_class     IN OUT UOM_CLASS.UOM_CLASS%TYPE,
                   I_uom           IN     UOM_CLASS.UOM%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  CHECK_CLASS
--Purpose      :  Check if the two unit of measures are of the same class.
-------------------------------------------------------------------------------
FUNCTION CHECK_CLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_same_class    IN OUT BOOLEAN,
                     I_first_uom     IN     UOM_CLASS.UOM%TYPE,
                     I_second_uom    IN     UOM_CLASS.UOM%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  VALID_STANDARD_UOM
--Purpose      :  Check if the inputted UOM can be used as a standard UOM.  The
--                criterion for a valid standard UOM is that the UOM is not in
--                the 'MISC' or the 'PACK' class.
-------------------------------------------------------------------------------
FUNCTION VALID_STANDARD_UOM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid         IN OUT BOOLEAN,
                            I_standard_uom  IN     UOM_CLASS.UOM%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_UOM_FROM_DESC(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_uom           OUT uom_class.uom%TYPE,
                           O_uom_class     OUT uom_class.uom_class%TYPE,
                           I_uom_desc      IN  UOM_CLASS_TL.uom_desc_trans%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_CONVERSION(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_convert         IN OUT BOOLEAN,
                             I_from_uom_class  IN     UOM_CLASS.UOM_CLASS%TYPE,
                             I_to_uom_class    IN     UOM_CLASS.UOM_CLASS%TYPE,
                             I_supp_lwh_dim    IN     ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                             I_supp_weight_dim IN     ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                             I_supp_lvol_uom   IN     ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------
-- Function: VALID_UOM_FOR_ITEMS
-- Purpose:  Checks that dimension records are entered for the passed item, item in the passed
--           dept, or items in the passed item list and the to and from UOM.
-- Notes:    Either an item, dept, or item list must be passed, but only one of them.
--------------------------------------------------------------------------------
FUNCTION VALID_UOM_FOR_ITEMS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT BOOLEAN,
                             I_from_uom      IN     UOM_CLASS.UOM%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE,
                             I_diff_id       IN     ITEM_MASTER.DIFF_1%TYPE,
                             I_dept          IN     DEPS.DEPT%TYPE,
                             I_class         IN     CLASS.CLASS%TYPE,
                             I_subclass      IN     SUBCLASS.SUBCLASS%TYPE,
                             I_item_list     IN     SKULIST_HEAD.SKULIST%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function: GET_CONV_FACTOR
-- Purpose:  Retrieves the conversion factor for a from_uom/to_uom relationship.
--
---------------------------------------------------------------------------------
FUNCTION GET_CONV_FACTOR(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_conv_factor   IN OUT UOM_CONVERSION.FACTOR%TYPE,
                         I_from_uom      IN     UOM_CONVERSION.FROM_UOM%TYPE,
                         I_to_uom        IN     UOM_CONVERSION.TO_UOM%TYPE)

   return BOOLEAN;
---------------------------------------------------------------------------------
--Function Name:  CONVERT
--Purpose      :  Evaluate whether the classes are the same or different, and
--                then it will call the corresponding internal function to
--                convert between the given unit of measures.  If I_item is
--                inputted with I_supplier, I_origin_country NULL, the function
--                will find the origin country for the item's primary supplier
--                and base conversion on the associated measurements.  However,
--                supplier and origin country should be inputted if possible.
-------------------------------------------------------------------------------
FUNCTION CONVERT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_to_value       IN OUT NUMBER,
                 I_to_uom         IN     UOM_CONVERSION.TO_UOM%TYPE,
                 I_from_value     IN     NUMBER,
                 I_from_uom       IN     UOM_CONVERSION.FROM_UOM%TYPE,
                 I_item           IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                 I_supplier       IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                 I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                 I_pack_type      IN     ITEM_MASTER.PACK_TYPE%TYPE,
                 I_to_class       IN     UOM_CLASS.UOM_CLASS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION CONVERT_WRAPPER(I_unit_retail   IN   ITEM_LOC.REGULAR_UNIT_RETAIL%TYPE,
                         I_to_uom        IN   UOM_CONVERSION.TO_UOM%TYPE,
                         I_from_uom      IN   UOM_CONVERSION.FROM_UOM%TYPE,
                         I_item          IN   ITEM_SUPP_COUNTRY.ITEM%TYPE)
RETURN NUMBER;
---------------------------------------------------------------------------------
--Function Name:  UOM_LANG_EXIST
--Purpose      :  Checks if the records available in the UOM_CLASS_TL table.
-------------------------------------------------------------------------------
FUNCTION UOM_LANG_EXIST
return CHAR;
---------------------------------------------------------------------------------
--Function Name:  GET_UOM_LANG
--Purpose      :  Gets the translated value for the UOM in the table UOM_CLASS_TL and
--                if not present assign the standard uom value to the output 
--                parameter.
-------------------------------------------------------------------------------
FUNCTION GET_UOM_LANG (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_standard_uom    IN     UOM_CLASS.UOM%TYPE,
                       O_uom_trans       IN OUT UOM_CLASS_TL.UOM_TRANS%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
--Function Name:  LOOKUP_UOM
--Purpose      :  Gets the corresponding UOM value from the translated value.
-------------------------------------------------------------------------------
FUNCTION LOOKUP_UOM   (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_standard_uom_trans    IN     UOM_CLASS.UOM%TYPE,
                       O_uom                   IN OUT UOM_CLASS_TL.UOM_TRANS%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------
---------------------------------------------------------------------------------
--Function Name:  GET_UOM
--Purpose      :  Gets the corresponding UOM value from the translated value in case of upload 
--                and the translated value for the corresponding UOM in case of download.
-------------------------------------------------------------------------------
FUNCTION GET_UOM   (I_uom  IN     UOM_CLASS.UOM%TYPE,
                    I_mode IN     VARCHAR2)
return VARCHAR2;
-------------------------------------------------------------------------------
END UOM_SQL;
/
